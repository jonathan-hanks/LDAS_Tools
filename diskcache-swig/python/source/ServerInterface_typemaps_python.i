/*
 * LDASTools diskcache - Tools for querying a collection of files on disk
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools diskcache is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools diskcache is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#if defined(SWIGPYTHON)
%typemap(in) port_type
{
  $1 = PyInt_AsLong($input);
}
%apply port_type { time_type };
%typemap(in, numinputs=0) filenames_rds_results_type& ( filenames_rds_results_type temp ) {
  $1 = &temp;
}

%typemap(argout) filenames_rds_results_type&
{
  //---------------------------------------------------------------------
  // Create a tuple of the file names.
  //---------------------------------------------------------------------
  PyObject*	t = PyTuple_New( ($1)->size( ) );
  int		offset = 0;

  for ( filenames_rds_results_type::const_iterator
	  cur = ($1)->begin( ),
	  last = ($1)->end( );
	cur != last;
	++cur, ++offset )
  {
    PyObject*	s = PyString_FromString( cur->c_str( ) );
    PyTuple_SetItem( t, offset, s );
  }
  //---------------------------------------------------------------------
  // Return the results to the caller.
  //---------------------------------------------------------------------
  if ( (!$result) || ($result == Py_None) )
  {
    $result = t;
  }
  else
  {
    $result = PySequence_Concat( $result, t );
  }
  //---------------------------------------------------------------------
  // Cleanup
  //---------------------------------------------------------------------
  // Py_DECREF( t );
}
#endif /* defined(SWIGPYTHON) */
