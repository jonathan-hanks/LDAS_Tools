/*
 * LDASTools diskcache - Tools for querying a collection of files on disk
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools diskcache is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools diskcache is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#if defined(SWIGTCL)
%include "dc_tcl.swig"

%{
#include <string>

#include "genericAPI/TCL.hh"

#include "diskcacheAPI/Cache/HotDirectory.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "CommandsTCL.hh"
#include "diskcachecmd.hh"

   using namespace std;

   using namespace diskCache::TCL::Commands;

   using ::ScanMountPointListContinuously;
   using diskCache::Cache::HotDirectory;

  static void
  set_rwlock_timeout_ms( int Value )
  {
    diskCache::RWLOCK_TIMEOUT = ( Value * 1000 )
      + ( ( ( ( Value * 10000 ) % 10 ) >= 5 )	/* rounding */
	  ? 1 : 0 );
  }
%}

%init %{
    //-------------------------------------------------------------------
    // Generate the symbol table for this interpreter
    //-------------------------------------------------------------------
    GenericAPI::TCL::SymbolMapper::Create( interp );

    //-------------------------------------------------------------------
    // Map TCL symbols to C++ symbols
    //-------------------------------------------------------------------
    {
      typedef GenericAPI::SymbolMapper::func_type func_type;
      typedef void (*func_hd_type)( HotDirectory::timestamp_type );

      struct sm_type {
          const char*                           s_var_name;
          GenericAPI::SymbolMapper::arg_type    s_func_proto;
          func_type                             s_func;
      };

      static const sm_type sm [] = {
	  //-------------------------------------------------------------
	  { "::CACHE_WRITE_DELAY_SECS",
	    GenericAPI::SymbolMapper::ARGS_I,
            (func_type)( SetCacheWriteDelaySeconds ) },
	  //-------------------------------------------------------------
          { "::DISKCACHE_ENABLE_STATVFS",
	    GenericAPI::SymbolMapper::ARGS_I,
            (func_type)( diskCache::Symbols::DISKCACHE_ENABLE_STATVFS::Set ) },
	  //-------------------------------------------------------------
          { "::DISKCACHE_HASHFILE_NAME_ASCII",
	    GenericAPI::SymbolMapper::ARGS_S,
            (func_type)( diskCache::DumpCacheDaemon::HASH_FILENAME_ASCII::Set ) },
	  //-------------------------------------------------------------
          { "::DISKCACHE_HASHFILE_NAME_BINARY",
	    GenericAPI::SymbolMapper::ARGS_S,
            (func_type)( diskCache::DumpCacheDaemon::HASH_FILENAME_BINARY::Set ) },
	  //-------------------------------------------------------------
	  { "::EXCLUDE_THESE_DIRS_FROM_UPDATES",
	    GenericAPI::SymbolMapper::ARGS_LS,
            (func_type)( set_excluded_dirs ) },
	  //-------------------------------------------------------------
	  { "::HOT_DIRECTORY_AGE_SEC",
	    GenericAPI::SymbolMapper::ARGS_I,
            (func_type)( func_hd_type( HotDirectory::HotLowerBound ) ) },
	  //-------------------------------------------------------------
          { "::HOT_DIRECTORY_SCAN_INTERVAL_SEC",
	    GenericAPI::SymbolMapper::ARGS_I,
            (func_type)( func_hd_type( HotDirectory::ScanInterval ) ) },
	  //-------------------------------------------------------------
          { "::MOUNT_PT",
            GenericAPI::SymbolMapper::ARGS_LS,
            (func_type)( set_mount_pt) },
	  //-------------------------------------------------------------
          { "::MOUNT_PT_LOOP_INTERVAL_MS",
            GenericAPI::SymbolMapper::ARGS_I,
            (func_type)( set_scan_mount_points_daemon_interval ) },
	  //-------------------------------------------------------------
          { "::NUMBER_OF_RUNNING_THREADS_PERMITTED",
            GenericAPI::SymbolMapper::ARGS_I,
            (func_type)( set_concurrency ) },
	  //-------------------------------------------------------------
          { "::RWLOCK_INTERVAL_MS",
	    GenericAPI::SymbolMapper::ARGS_I,
            (func_type)LDASTools::AL::ReadWriteLock::Interval },
	  //-------------------------------------------------------------
          { "::RWLOCK_TIMEOUT_MS",
	    GenericAPI::SymbolMapper::ARGS_I,
            (func_type)set_rwlock_timeout_ms }
      };

      for ( INT_4U
                cur = 0,
                last = sizeof( sm ) / sizeof( *sm );
            cur != last;
            ++cur )
      {
          GenericAPI::SymbolMapper::
              Add( sm[ cur ].s_var_name,
                   sm[ cur ].s_func_proto,
                   sm[ cur ].s_func );
      }
    }
%}

%typemap(in,numinputs=0) Tcl_Interp *Interp
{
  $1 = interp;
}

   //--------------------------------------------------------------------
   //   
   //: Get ASCII representation of the current frame hash in Tcl-layer requested 
   //: format.   
   //
   //!usage: set string [ getDirCache ]
   //   
   //!param: const char* ifo - An ifo to look up. Default is "all".
   //!param: const char* type - A type to look up. Default is "all".
   //      
   //!return: string - A list of all identified subdirectories that contain frame 
   //+        data with a Tcl-layer requested metadata, such as:
   //+        DirectoryName,IFO,Type,NumberOfFramesPerFile(always 1),dT(file), 
   //+        MTIME NUMBER_OF_FRAMES_IN_DIR {StartTime StopTime ...}  
   //   
   //--------------------------------------------------------------------
  string
  getDirCache( const char* ifo = "all",
               const char* type = "all" );

  
  tid*
  getDirCache_t( const char* ifo = "all",
                 const char* type = "all",
                 Tcl_Interp* interp = 0,
                 const char* flag = "" );

  string
  getDirCache_r( tid* TID );

  //---------------------------------------------------------------------
  //: Obtain the list of file name extensions
  //
  //!usage: set ext_list [ getFileExtList ]
  //
  //!param: none  
  //
  //!return: string - list of file extensions
  //
  //---------------------------------------------------------------------
  string getFileExtList( );

  //---------------------------------------------------------------------
  /// \brief Get filename based on search criteria
  ///
  /// Get TCL formatted lists of file names for data matching the specified
  /// IFO and Type within the bounds of the query.
  ///
  /// \param[in] ifo_type_str
  ///     A space delimited list of IFO-Type strings
  /// \param[in] start_time
  ///     Query start time.
  /// \param[in] stop_time
  ///     Query stop time.
  /// \param[in] Extension
  ///     Query file extension
  ///
  /// \return
  ///     A list for each mount point with data file names:
  ///     { mntpt1 { /f1 /f2 ... /fN } mntpt2 { /f1 /f2 ... /fN } ... mntptN { } }  
  ///
  /// \example set string [ getFileNames ifo_type_str query_start query_stop ]
  //---------------------------------------------------------------------
  string
  getFileNames( const char* IfoType,
		const unsigned int Start,
		const unsigned int Stop,
		const char* Extension );

  //---------------------------------------------------------------------
  /// \brief Frame query.
  ///   
  /// This function is used to search global frame data hash for a specific data.
  ///   
  ///   
  /// \param[in] Ifo
  ///     Data IFO.
  /// \param[in] Type
  ///     Data Type.
  /// \param[in] Start
  ///     Data start time.
  /// \param[in] Stop
  ///     Data stop time.
  /// \param[in] Extension
  ///     Query file extension
  /// \param[in] AllowGaps
  ///     Flag to indicate if data gaps are allowed.
  ///     Set to TRUE (1) if allowed,
  ///     to FALSE (0) - if not.
  ///   
  /// \return
  ///     Tcl formated string: { frame files }{ errors if any }.
  //                    
  /// \example set string [ getFrameFiles ifo type start_time stop_time gaps_allowed ]   
  //---------------------------------------------------------------------
  string
  getFrameFiles( const char* Ifo, const char* Type,
		 const unsigned int Start, 
		 const unsigned int Stop,
		 const char* Extension,
		 const bool AllowGaps );

  tid*
  getFrameFiles_t( const char* Ifo, const char* Type,
                   const unsigned int Start, 
                   const unsigned int Stop,
                   const char* Extension,
                   const bool AllowGaps,
                   Tcl_Interp* interp = 0,
                   const char* flag = "" );

  string
  getFrameFiles_r( tid* TID );

  //-----------------------------------------------------------------------
  //
  //: Get TCL formatted lists of intervals for data matching the specified
  //: IFO and Type within the bounds of the query.
  //
  //!usage: set string [ getIntervalsList ifo_type_str query_start query_stop ]
  //
  //!param: const char* ifo_type_str - A space delimited list of IFO-Type strings.
  //!param: const unsigned int start_time - query start time.
  //!param: const unsigned int stop_time - query stop time.
  //
  //!return: string - A list for each IFO-Type pair with data intervals:
  //+  IFO-Type1 { i1_start i1_stop ... iN_start iN_stop } ... IFO-TypeN { ... }
  //
  //-----------------------------------------------------------------
  string
  getIntervalsList( const char* IfoType,
                    const unsigned int Start,
                    const unsigned int Stop,
                    const char* Extension );

  tid*
  getIntervalsList_t( const char* IfoType,
                      const unsigned int Start,
                      const unsigned int Stop,
                      const char* Extensions,
                      Tcl_Interp* interp = 0,
                      const char* flag = "" );

  string
  getIntervalsList_r( tid* TID );
  
  //---------------------------------------------------------------------
  /// \brief RDS frame query
  ///   
  /// This command finds frame files that satisfy createRDS user request. 
  /// If "will_resample" flag is set to TRUE, requested time range will be
  /// automatically expanded by frame dt at the beginning and at the end.         
  ///   
  /// \param[in] ifo
  ///     Data IFO.    
  /// \param[in] type
  ///     Data Type.       
  /// \param[in] start_time
  ///     Data start time.
  /// \param[in] stop_time
  ///     Data stop time.
  /// \param[in] Extension
  ///     Query file extension
  /// \param[in] will_resample
  ///     Flag to indicate if requested data will be
  ///     resampled. Set to TRUE (1) if will be resampled,
  ///     to FALSE (0) if not.
  ///
  /// \code
  ///   set tidp [ getRDSFrameFiles_t ifo type start_time stop_time Extension will_resample ]   
  /// \endcode
  //---------------------------------------------------------------------
  string
  getRDSFrameFiles( const char* ifo,const char* type,
                    const unsigned int start_time, 
                    const unsigned int stop_time,
                    const char* Extension,
                    const bool will_resample );

  //---------------------------------------------------------------------
  /// \brief Threaded RDS frame query
  ///   
  /// This command finds frame files that satisfy createRDS user request. 
  /// If "will_resample" flag is set to TRUE, requested time range will be
  /// automatically expanded by frame dt at the beginning and at the end.         
  ///   
  /// \param[in] ifo
  ///     Data IFO.    
  /// \param[in] type
  ///     Data Type.       
  /// \param[in] start_time
  ///     Data start time.
  /// \param[in] stop_time
  ///     Data stop time.
  /// \param[in] Extension
  ///     Query file extension
  /// \param[in] will_resample
  ///     Flag to indicate if requested data will be
  ///     resampled. Set to TRUE (1) if will be resampled,
  ///     to FALSE (0) if not.
  ///
  /// \code
  ///   set tidp [ getRDSFrameFiles_t ifo type start_time stop_time Extension will_resample ]
  /// \endcode
  //---------------------------------------------------------------------
  tid*
  getRDSFrameFiles_t( const char* ifo,const char* type,
                      const unsigned int start_time, 
                      const unsigned int stop_time,
                      const char* Extension,
                      const bool will_resample, 
                      Tcl_Interp* interp = 0, const char* flag = "" );

  //---------------------------------------------------------------------
  /// \brief Threaded RDS frame query.
  ///   
  /// This command finds frame files that satisfy createRDS user request. 
  /// If "will_resample" flag is set to TRUE, requested time range will be
  /// automatically expanded by frame dt at the beginning and at the end.   
  ///      
  /// \param[in] tid
  ///     Thread identifier.
  ///
  /// \return
  ///     Tcl formated string: { frame files }{ errors if any }.   
  ///
  /// \code
  ///   set string [ getRDSFrameFiles_r tidp ]
  /// \endcode
  //---------------------------------------------------------------------
  string getRDSFrameFiles_r( tid* );
   
  //---------------------------------------------------------------------
  /// \brief Read content of global frame data hash from binary file.   
  ///
  /// Tcl layer can specify different files for read and write operations. 
  /// This function forces all read and write operations to be sequencial.
  ///   
  /// \param[in] filename
  ///      Name of the file to read frame hash from.
  ///      Default is an empty string (C++ will use default file name).      
  ///
  /// \note
  ///    This function destroys existing hash before reading a new one
  ///    from the given file. Caller must assure there are no running
  ///    threads that might access global frame data hash at the time
  ///    "readDirCache" is called.
  ///   
  /// \code
  ///   readDirCache
  /// \endcode
  //---------------------------------------------------------------------
%inline %{
  void 
  readDirCache( const char* filename = "" )
  {
     CacheRead( filename );
  }
%}

   
  //---------------------------------------------------------------------
  /// \brief Continuously scan list of mount points for changes.
  ///
  /// This is the main task to start scanning for new information that
  /// should be cached.
  ///
  /// \code
  /// ScanMountPointListContinuously
  /// \endcode
  //---------------------------------------------------------------------
  void ScanMountPointListContinuously( );

  //---------------------------------------------------------------------
  /// \brief Update the list of extensions to cache by SDGTx engine
  //---------------------------------------------------------------------
  void
  updateFileExtList( const char* Extensions );
     
  //---------------------------------------------------------------------
  /// \brief Update the list of mount points
  //---------------------------------------------------------------------
  string
  updateMountPtList( const char* dir_list,
                     const bool enable_global_check = false );
#endif /* defined(SWIGTCL) */
