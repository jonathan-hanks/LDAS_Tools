/*
 * LDASTools diskcache - Tools for querying a collection of files on disk
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools diskcache is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools diskcache is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

// -*- Mode: C++; c-basic-offset: 2; -*-
#if defined(SWIGTCL)
%{
  #include "genericAPI/SymbolMapperTCL.hh"
%}

%include "ldas_tcl.i"
%include "dc_tcl.swig"

//=======================================================================
// TCL
//=======================================================================
%init %{
    Tcl_PkgProvide( interp, "diskscan", "@LDAS_PACKAGE_VERSION@" );

    //-------------------------------------------------------------------
    // Generate the symbol table for this interpreter
    //-------------------------------------------------------------------
    GenericAPI::TCL::SymbolMapper::Create( interp );

    //-------------------------------------------------------------------
    // Map TCL symbols to C++ symbols
    //-------------------------------------------------------------------
    {
      typedef GenericAPI::SymbolMapper::func_type func_type;

      GenericAPI::SymbolMapper::
	Add( "::CACHE_WRITE_DELAY_SECS",
	     GenericAPI::SymbolMapper::ARGS_I,
	     (func_type)SetCacheWriteDelay );
      GenericAPI::SymbolMapper::
	Add( "::DISKCACHE_HASHFILE_NAME_BINARY",
	     GenericAPI::SymbolMapper::ARGS_S,
	     (func_type)SetFilenameBinary );
      GenericAPI::SymbolMapper::
	Add( "::DISKCACHE_HASHFILE_NAME_ASCII",
	     GenericAPI::SymbolMapper::ARGS_S,
	     (func_type)SetFilenameAscii );
      GenericAPI::SymbolMapper::
	Add( "::EXCLUDE_THESE_DIRS_FROM_UPDATES",
	     GenericAPI::SymbolMapper::ARGS_LS,
	     (func_type)( set_excluded_dirs ) );
      GenericAPI::SymbolMapper::
	Add( "::MOUNT_PT",
	     GenericAPI::SymbolMapper::ARGS_LS,
	     (func_type)( set_mount_pt ) );
      GenericAPI::SymbolMapper::
	Add( "::MOUNT_PT_LOOP_INTERVAL_MS",
	     GenericAPI::SymbolMapper::ARGS_I,
	     (func_type)set_scan_mount_points_daemon_interval );
      GenericAPI::SymbolMapper::
	Add( "::NUMBER_OF_RUNNING_THREADS_PERMITTED",
	     GenericAPI::SymbolMapper::ARGS_I,
	     (func_type)set_concurrency );
    }
%}

%include "genericAPI/tidcmd.swig"

//-----------------------------------------------------------------------
// Typemapse - Tcl
//-----------------------------------------------------------------------
%typemap(out) string, std::string
{
  Tcl_SetStringObj( Tcl_GetObjResult( interp ),
                    const_cast< char* >( $1.c_str( ) ),
                    -1 );
}

//-----------------------------------------------------------------------
// Exception handling - Tcl
//-----------------------------------------------------------------------
%exception
{
    Tcl_Obj* tcl_result( Tcl_GetObjResult( interp ) );      
    try
    {
        $function;
    }
    catch( const LdasException& exc )
    {
       SwigException e( exc ); 
       e.setTcl( interp, tcl_result );
       return TCL_ERROR;
    }      
    catch ( SwigException& e )
    {
        e.setTcl( interp, tcl_result );
        return TCL_ERROR;
    }
    catch ( std::runtime_error& e )
    {
      std::string	msg("runtime error: ");

      msg += e.what();
      Tcl_SetStringObj( tcl_result, (char*)msg.c_str(), msg.length());
      Tcl_AddObjErrorInfo( interp, e.what(), strlen(e.what()));
      Tcl_SetErrorCode( interp, "1", NULL );
      return TCL_ERROR;
    }
    catch ( std::exception& e )
    {
      std::string	msg("standard exception: ");

      msg += e.what();
      Tcl_SetStringObj( tcl_result, (char*)msg.c_str(), msg.length());
      Tcl_AddObjErrorInfo( interp, e.what(), strlen(e.what()));
      Tcl_SetErrorCode( interp, "1", NULL );
      return TCL_ERROR;
    }
    catch( ... )
    {
      std::string	error("Unknown exception caught: ");
      std::string	info("Unable to retrieve more information about this exception");

      error += info;
      Tcl_SetStringObj( tcl_result, (char*)error.c_str(), error.length());
      Tcl_AddObjErrorInfo( interp, info.c_str(), info.length());
      Tcl_SetErrorCode( interp, "1", NULL );
      return TCL_ERROR;
    }
}

#endif // defined(SWIGTCL)
