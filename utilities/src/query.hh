//
// LDASTools utilities - A collection of utilities base on LDASTools Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools utilities is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools utilities is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <regex>

#include "frameAPI/createRDS.hh"

#define POSITION_DESC_TCL 0
#define POSITION_IFO_TCL 1
#define POSITION_TIME_RANGE_TCL 3
#define POSITION_CHANNEL_LIST_TCL 4
#define POSITION_QUERY_LIST_SIZE_TCL 5

namespace LDAS
{
    namespace createRDS
    {
        class query
        {
        public:
            typedef std::vector< std::string > channel_container_type;
            typedef LDASTools::AL::GPSTime     time_type;

            query( const std::string );

            std::string            ifo;
            std::string            desc;
            time_type              start;
            time_type              end;
            std::string            ext;
            channel_container_type channels;
        };
    } // namespace createRDS
} // namespace LDAS
