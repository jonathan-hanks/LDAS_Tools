//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "query.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/join.hpp>

using LDAS::createRDS::query;

struct query_global_fixture
{
    query_global_fixture( )
    {
        //-------------------------------------------------------------------
        // Initialize frameCPP library
        //-------------------------------------------------------------------
        FrameCPP::Initialize( );
        //-------------------------------------------------------------------
        // Setup logging as is needed by query class
        //-------------------------------------------------------------------
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        static char cwd_buffer[ 2048 ];
        if ( getcwd( cwd_buffer,
                     sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) ) ==
             (const char*)NULL )
        {
            exit( 1 );
        }
        std::string                       cwd( cwd_buffer );
        GenericAPI::Log::stream_file_type fs( new GenericAPI::Log::StreamFile );
        LDASTools::AL::Log::stream_type   s;

        s = fs;
        fs->FilenameExtension( GenericAPI::LogFormatter( )->FileExtension( ) );
        GenericAPI::LogFormatter( )->Stream( s );
        GenericAPI::setLogTag( "test_query" );
        GenericAPI::LoggingInfo::LogDirectory( cwd );

        std::cout << "global setup" << std::endl;
    }

    ~query_global_fixture( )
    {
        std::cout << "global teardown" << std::endl;
    }
};

BOOST_GLOBAL_FIXTURE( query_global_fixture );

BOOST_AUTO_TEST_CASE( query_simple )
{
    std::string test_query_string(
        "{ " // open TCL list
        "H1_R " // desc
        "H " // ifo
        "{} " // unknown
        "1051388672-1051388927 " // time range
        "Chan(H0:PEM-EX_DUST_VEA1_300NM_PCF," //
        "H0:PEM-EX_DUST_VEA1_500NM_PCF)" // channel list
        " }" // Close TCL list
    );
    static const std::string      ifo_expected( "H" );
    static const std::string      desc_expected( "H1_R" );
    static const query::time_type start_expected( 1051388672, 0 );
    static const query::time_type end_expected( 1051388927, 0 );
    static const size_t           channels_container_size_expected( 2 );

    query simple_query( test_query_string );

    BOOST_CHECK_MESSAGE( simple_query.ifo == ifo_expected,
                         "query.ifo is '" << simple_query.ifo
                                          << "' instead of '" << ifo_expected
                                          << "'" );
    BOOST_CHECK_MESSAGE( simple_query.desc == desc_expected,
                         "query.desc is '" << simple_query.desc
                                           << "' instead of '" << desc_expected
                                           << "'" );
    BOOST_CHECK_MESSAGE( simple_query.start == start_expected,
                         "query.start is '" << simple_query.start
                                            << "' instead of '"
                                            << start_expected << "'" );
    BOOST_CHECK_MESSAGE( simple_query.end == end_expected,
                         "query.end is '" << simple_query.end
                                          << "' instead of '" << end_expected
                                          << "'" );
    BOOST_CHECK_MESSAGE(
        simple_query.channels.size( ) == channels_container_size_expected,
        "query.channels.size( ) is '"
            << simple_query.channels.size( ) << "' instead of '"
            << channels_container_size_expected << "'" );
    if ( simple_query.channels.size( ) == channels_container_size_expected )
    {
        const std::string channels_0_expected(
            "H0:PEM-EX_DUST_VEA1_300NM_PCF" );
        const std::string channels_1_expected(
            "H0:PEM-EX_DUST_VEA1_500NM_PCF" );

        BOOST_CHECK_MESSAGE( simple_query.channels[ 0 ] == channels_0_expected,
                             "query.channels.resampling[ 0 ] is '"
                                 << simple_query.channels[ 0 ]
                                 << "' instead of '" << channels_0_expected
                                 << "'" );
    }
}

BOOST_AUTO_TEST_CASE( query_resample )
{
    std::string test_query_string(
        "{ " // open TCL list
        "H1_R " // desc
        "H " // ifo
        "{} " // unknown
        "1051388672-1051388927 " // time range
        "Chan(H0:PEM-EX_DUST_VEA1_300NM_PCF!4," //
        "H0:PEM-EX_DUST_VEA1_500NM_PCF!2)" // channel list
        " }" // Close TCL list
    );
    static const std::string      ifo_expected( "H" );
    static const std::string      desc_expected( "H1_R" );
    static const query::time_type start_expected( 1051388672, 0 );
    static const query::time_type end_expected( 1051388927, 0 );
    static const size_t           channels_container_size_expected( 2 );

    query simple_query( test_query_string );

    BOOST_CHECK_MESSAGE( simple_query.ifo == ifo_expected,
                         "query.ifo is '" << simple_query.ifo
                                          << "' instead of '" << ifo_expected
                                          << "'" );
    BOOST_CHECK_MESSAGE( simple_query.desc == desc_expected,
                         "query.desc is '" << simple_query.desc
                                           << "' instead of '" << desc_expected
                                           << "'" );
    BOOST_CHECK_MESSAGE( simple_query.start == start_expected,
                         "query.start is '" << simple_query.start
                                            << "' instead of '"
                                            << start_expected << "'" );
    BOOST_CHECK_MESSAGE( simple_query.end == end_expected,
                         "query.end is '" << simple_query.end
                                          << "' instead of '" << end_expected
                                          << "'" );
    BOOST_CHECK_MESSAGE(
        simple_query.channels.size( ) == channels_container_size_expected,
        "query.channels.size( ) is '"
            << simple_query.channels.size( ) << "' instead of '"
            << channels_container_size_expected << "'" );
    if ( simple_query.channels.size( ) == channels_container_size_expected )
    {
        const std::string channels_0_expected(
            "H0:PEM-EX_DUST_VEA1_300NM_PCF!4" );
        const std::string channels_1_expected(
            "H0:PEM-EX_DUST_VEA1_500NM_PCF!2" );

        BOOST_CHECK_MESSAGE( simple_query.channels[ 0 ] == channels_0_expected,
                             "query.channels.resampling[ 0 ] is '"
                                 << simple_query.channels[ 0 ]
                                 << "' instead of '" << channels_0_expected
                                 << "'" );
    }
}

BOOST_AUTO_TEST_CASE( query_rename )
{
    std::string                   test_query_string( "{ " // open TCL list
                                   "H1_R " // desc
                                   "H " // ifo
                                   "{} " // unknown
                                   "1051388672-1051388927 " // time range
                                   "Chan(H0:PEM-EX_DUST_VEA1_300NM_PCF!4=H0:"
                                   "PEM-EX_DUST_VEA1_300NM_PCF_AR," //
                                   "H0:PEM-EX_DUST_VEA1_500NM_PCF!2=H0:PEM-EX_"
                                   "DUST_VEA1_500NM_PCF_AR)" // channel list
                                   " }" // Close TCL list
    );
    static const std::string      ifo_expected( "H" );
    static const std::string      desc_expected( "H1_R" );
    static const query::time_type start_expected( 1051388672, 0 );
    static const query::time_type end_expected( 1051388927, 0 );
    static const size_t           channels_container_size_expected( 2 );

    query simple_query( test_query_string );

    BOOST_CHECK_MESSAGE( simple_query.ifo == ifo_expected,
                         "query.ifo is '" << simple_query.ifo
                                          << "' instead of '" << ifo_expected
                                          << "'" );
    BOOST_CHECK_MESSAGE( simple_query.desc == desc_expected,
                         "query.desc is '" << simple_query.desc
                                           << "' instead of '" << desc_expected
                                           << "'" );
    BOOST_CHECK_MESSAGE( simple_query.start == start_expected,
                         "query.start is '" << simple_query.start
                                            << "' instead of '"
                                            << start_expected << "'" );
    BOOST_CHECK_MESSAGE( simple_query.end == end_expected,
                         "query.end is '" << simple_query.end
                                          << "' instead of '" << end_expected
                                          << "'" );
    BOOST_CHECK_MESSAGE(
        simple_query.channels.size( ) == channels_container_size_expected,
        "query.channels.size( ) is '"
            << simple_query.channels.size( ) << "' instead of '"
            << channels_container_size_expected << "'" );
    if ( simple_query.channels.size( ) == channels_container_size_expected )
    {
        const std::string channels_0_expected(
            "H0:PEM-EX_DUST_VEA1_300NM_PCF!4"
            "=H0:PEM-EX_DUST_VEA1_300NM_PCF_AR" );
        const std::string channels_1_expected(
            "H0:PEM-EX_DUST_VEA1_500NM_PCF!2"
            "=H0:PEM-EX_DUST_VEA1_500NM_PCF_AR" );

        BOOST_CHECK_MESSAGE( simple_query.channels[ 0 ] == channels_0_expected,
                             "query.channels.resampling[ 0 ] is '"
                                 << simple_query.channels[ 0 ]
                                 << "' instead of '" << channels_0_expected
                                 << "'" );
    }
}
