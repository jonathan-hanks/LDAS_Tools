//
// LDASTools utilities - A collection of utilities base on LDASTools Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools utilities is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools utilities is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <cstdlib>

#include <fstream>
#include <list>
#include <string>
#include <sstream>
#include <vector>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/CommandLineOptions.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/MkDir.hh"
#include "ldastoolsal/Sed.hh"

#include "genericAPI/TCL.hh"
#include "genericAPI/swigexception.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#include "diskcacheAPI/MetaCommands.hh"
#include "diskcacheAPI/ServerInterface.hh"

#include "query.hh"

/*
 * ./createRDS.exe \
 * --framequery "{ { H1_R H {} 1051388672-1051388927 \
 *                   Chan(H0:PEM-EX_DUST_VEA1_300NM_PCF,H0:PEM-EX_DUST_VEA1_500NM_PCF,
 * ... } }"
 * --usertype { PEM_RDS_A6 \
 * --outputdir /archive/frames/A6/PEM_RDS/LHO/H-PEM_RDS_A6-10513
 * --usejobdirs 0
 * --compressiontype zero_suppress_otherwise_gzip
 * --compressionlevel 1
 * --filechecksum 1
 * --frametimecheck 1
 * --framedatavalid 0
 * --framesperfile 1
 * --secperframe 256
 * --allowshortframes 1
 * --generatechecksum 1
 * --fillmissingdatavalid 0
 * --md5sumregexp "s,frames,meta/frames,i"
 */

using LDASTools::AL::MemChecker;
using LDASTools::Cmd::MkDir;
using LDASTools::Cmd::Sed;

using GenericAPI::IsLogging;
using GenericAPI::LogEntryGroup_type;
using GenericAPI::queueLogEntry;

using LDAS::createRDS::query;

typedef std::vector< std::string > filenames_type;

typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
typedef CommandLineOptions::Option        Option;
typedef CommandLineOptions::OptionSet     OptionSet;

using FrameAPI::createRDSSet;

typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;
typedef diskCache::ServerInterface ServerInterface;

static inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

class CommandLine : public ServerInfo,
                    public FrameAPI::RDS::FileOptions,
                    protected CommandLineOptions
{
public:
    using CommandLineOptions::empty;
    using CommandLineOptions::Pop;
    typedef CommandLineOptions::option_type option_type;

    typedef FrameAPI::RDS::Options::seconds_per_frame_type
        seconds_per_frame_type;

    typedef std::string                     diskcache_host_type;
    typedef INT_4U                          diskcache_port_type;
    typedef query                           frame_query_type;
    typedef std::vector< frame_query_type > frame_queries_type;

    CommandLine( int ArgC, char** ArgV );

    const filenames_type& Filenames( ) const;

    const frame_queries_type& FrameQueries( ) const;

    void Usage( int ExitValue ) const;

    void operator( )( );

private:
    enum option_types
    {
        OPT_ALLOW_SHORT_FRAMES,
        OPT_ANALYSIS_READY_CHANNEL_NAME,
        OPT_ANALYSIS_READY_MASK,
        OPT_DESCRIPTION,
        OPT_LOG_DIRECTORY,
        OPT_LOG_DEBUG_LEVEL,
        OPT_DIRECTORY_OUTPUT_FRAMES,
        OPT_DIRECTORY_OUTPUT_MD5SUM,
        OPT_DIRECTORY_OUTPUT_MD5SUM_REGEXP,
        OPT_DISKCACHE_HOST,
        OPT_DISKCACHE_PORT,
        OPT_FILL_MISSING_DATA_VALID_ARRAY,
        OPT_FRAME_COMPRESSION_LEVEL,
        OPT_FRAME_COMPRESSION_METHOD,
        OPT_FRAME_FILES,
        OPT_FRAME_QUERY,
        OPT_FRAMES_PER_FILE,
        OPT_GENERATE_FRAME_CHECKSUM,
        OPT_HELP,
        OPT_NO_HISTORY,
        OPT_PADDING,
        OPT_SECONDS_PER_FRAME,
        OPT_VERIFY_CHECKSUM_OF_FRAME,
        OPT_VERIFY_CHECKSUM_OF_STREAM,
        OPT_VERIFY_DATA_VALID,
        OPT_VERIFY_FILENAME_METADATA,
        OPT_VERIFY_TIME_RANGE,
        OPT_WITHOUT_HISTORY_RECORD
    };

    OptionSet options;

    filenames_type filenames;

    frame_queries_type frame_queries;

    void parse_frame_files( const std::string& Filenames );

    void parse_frame_query( const std::string& Query );
};

//=======================================================================
//=======================================================================

static void
no_memory( )
{
    //---------------------------------------------------------------------
    // When there is no memory left, just surrender for another day
    // for there really is no safe action to take
    //---------------------------------------------------------------------
    std::cerr << "Failed to allocate memory!" << std::endl;
    std::exit( 1 );
}

//=======================================================================
//=======================================================================

int
main( int ArgC, char** ArgV )
{
    static const char* caller = "main";

    int                 exit_code = 0;
    MemChecker::Trigger gc_trigger( true );

    //---------------------------------------------------------------------
    // Protect ourselves from memory exhaustion
    //---------------------------------------------------------------------
    std::set_new_handler( no_memory );

    //---------------------------------------------------------------------
    // Make sure the frameCPP library is ready for use.
    //---------------------------------------------------------------------
    FrameCPP::Initialize( );

    try
    {
        //-------------------------------------------------------------------
        // Variables for logging
        //-------------------------------------------------------------------
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );

        static char cwd_buffer[ 2048 ];
        if ( getcwd( cwd_buffer,
                     sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) ) ==
             (const char*)NULL )
        {
            exit( 1 );
        }
        std::string                       cwd( cwd_buffer );
        GenericAPI::Log::stream_file_type fs( new GenericAPI::Log::StreamFile );
        LDASTools::AL::Log::stream_type   s;

        s = fs;
        fs->FilenameExtension( GenericAPI::LogFormatter( )->FileExtension( ) );
        GenericAPI::LogFormatter( )->Stream( s );
        GenericAPI::setLogTag( "createRDS" );
        GenericAPI::LoggingInfo::LogDirectory( cwd );

        CommandLine     cl( ArgC, ArgV );
        ServerInterface server;

        cl( );

        std::cerr << "CERR_DEBUG:Parsed the command lines" << std::endl;
        std::cerr << "CERR_DEBUG:LogDirectory: "
                  << GenericAPI::LoggingInfo::LogDirectory( ) << std::endl;

        {
            std::ostringstream msg;

            char** a = ArgV;

            msg << "Started createRDS with the following options:";

            while ( *a )
            {
                msg << " " << *a;
                ++a;
            }

            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_OK,
                                       0,
                                       caller,
                                       "STARTUP" );
            GenericAPI::SyncLog( );
        }

        if ( cl.Filenames( ).size( ) <= 0 )
        {
            server.Server( cl.Hostname( ), cl.Port( ) );
        }

        //-------------------------------------------------------------------
        // Loop over each of the frame queries
        //-------------------------------------------------------------------
        for ( CommandLine::frame_queries_type::const_iterator
                  cur = cl.FrameQueries( ).begin( ),
                  last = cl.FrameQueries( ).end( );
              cur != last;
              ++cur )
        {
            filenames_type filenames;
            bool           resampling = false;

            cl.OutputTimeStart( cur->start.GetSeconds( ) );
            cl.OutputTimeEnd( cur->end.GetSeconds( ) );

            FrameAPI::channel_container_type channels;

            for ( query::channel_container_type::const_iterator
                      cur_cl_chan = cur->channels.begin( ),
                      last_cl_chan = cur->channels.end( );
                  cur_cl_chan != last_cl_chan;
                  ++cur_cl_chan )
            {
                channels.ParseChannelName( *cur_cl_chan );
            }
            if ( cl.Filenames( ).size( ) )
            {
                QUEUE_LOG_MESSAGE( "cl.Filenames( ): start",
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CMD_CREATE_RDS" );
                filenames = cl.Filenames( );
                QUEUE_LOG_MESSAGE( "cl.Filenames( ): stop",
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CMD_CREATE_RDS" );
            }
            else
            {
                QUEUE_LOG_MESSAGE(
                    "server.FilenamesRDS( ): start"
                        << " ifo: " << cur->ifo << " desc: " << cur->desc
                        << " time_start:" << cl.OutputTimeStart( )
                        << " time_end: " << cl.OutputTimeEnd( )
                        << " ext: " << cur->ext << " resampling: " << resampling
                        << " filenames size: " << filenames.size( ),
                    MT_DEBUG,
                    30,
                    caller,
                    "CMD_CREATE_RDS" );
                if ( resampling && ( cl.Padding( ) == 0 ) )
                {
                    //-------------------------------------------------------------
                    // Do resampling by requesting an extra filename on either
                    //   side of the request
                    //-------------------------------------------------------------
                    server.FilenamesRDS( filenames,
                                         cur->ifo,
                                         cur->desc,
                                         cl.OutputTimeStart( ),
                                         cl.OutputTimeEnd( ),
                                         resampling,
                                         cur->ext );
                }
                else
                {
                    //-------------------------------------------------------------
                    // Request a fixed amount of data. If resampling, the
                    // padding
                    //   is added to each side of the request.
                    //-------------------------------------------------------------
                    server.FilenamesRDS( filenames,
                                         cur->ifo,
                                         cur->desc,
                                         cl.OutputTimeStart( ) - cl.Padding( ),
                                         cl.OutputTimeEnd( ) + cl.Padding( ),
                                         false,
                                         cur->ext );
                }
                QUEUE_LOG_MESSAGE( " server.FilenamesRDS( ): stop"
                                       << " filenames size: "
                                       << filenames.size( ),
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CMD_REATE_RDS" );
            }

            if ( IsLogging( LogEntryGroup_type::MT_DEBUG, 30 ) )
            {
                for ( filenames_type::const_iterator
                          cur_fname = filenames.begin( ),
                          last_fname = filenames.end( );
                      cur_fname != last_fname;
                      ++cur_fname )
                {
                    std::ostringstream msg;

                    msg << "filename: " << *cur_fname;
                    queueLogEntry( msg.str( ),
                                   LogEntryGroup_type::MT_DEBUG,
                                   30,
                                   caller,
                                   "CMD_CREATE_RDS" );
                }
            }
            if ( cl.SecondsPerFrame( ) == 0 )
            {
                cl.SecondsPerFrame(
                    ( cl.OutputTimeEnd( ) - cl.OutputTimeStart( ) ) /
                    cl.FramesPerFile( ) );
            }
            for ( query::channel_container_type::const_iterator
                      cur_cl_chan = cur->channels.begin( ),
                      last_cl_chan = cur->channels.end( );
                  cur_cl_chan != last_cl_chan;
                  ++cur_cl_chan )
            {
                FrameAPI::RDS::ExpandChannelList(
                    channels.names, channels.resampling, filenames.front( ) );
            }
            QUEUE_LOG_MESSAGE( "Description: "
                                   << cl.OutputType( )
                                   << " start: " << cl.OutputTimeStart( )
                                   << " end: " << cl.OutputTimeEnd( ),
                               MT_DEBUG,
                               30,
                               caller,
                               "CMD_CREATE_RDS" );
            createRDSSet( filenames, channels, cl );
            QUEUE_LOG_MESSAGE( "Completed: Description: "
                                   << cl.OutputType( )
                                   << " start: " << cl.OutputTimeStart( )
                                   << " end: " << cl.OutputTimeEnd( ),
                               MT_DEBUG,
                               30,
                               caller,
                               "CMD_CREATE_RDS" );
        }
    }
    catch ( const SwigException& E )
    {
        QUEUE_LOG_MESSAGE( " Caught SwigException:"
                               << " Result: " << E.getResult( )
                               << " Info: " << E.getInfo( ),
                           MT_ERROR,
                           0,
                           caller,
                           "CMD_CREATE_RDS" );
        exit_code = 1;
    }
    catch ( const std::exception& E )
    {
        QUEUE_LOG_MESSAGE( " Caught exception: " << E.what( ),
                           MT_ERROR,
                           0,
                           caller,
                           "CMD_CREATE_RDS" );
        exit_code = 1;
    }
    catch ( ... )
    {
        QUEUE_LOG_MESSAGE( " Caught an unknown exception",
                           MT_ERROR,
                           0,
                           caller,
                           "CMD_CREATE_RDS" );
        exit_code = 1;
    }

    GenericAPI::SyncLog( );
    return exit_code;
}

//=======================================================================
// Class CommandLine
//=======================================================================
CommandLine::CommandLine( int ArgC, char** ArgV )
    : CommandLineOptions( ArgC, ArgV )
{
    //---------------------------------------------------------------------
    // Setup the options that will be recognized.
    //---------------------------------------------------------------------
    std::ostringstream desc;

    options.Synopsis( "[options]" );
    options.Summary( "This creates a reduced data set." );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_HELP, "help", Option::ARG_NONE, "Display this message" ) );

    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_ALLOW_SHORT_FRAMES,
                         "allow-short-frames",
                         Option::ARG_REQUIRED,
                         "Specify if short frames are allowed to be generated",
                         "boolean" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_ANALYSIS_READY_CHANNEL_NAME,
                         "analysis-ready-channel-name",
                         Option::ARG_REQUIRED,
                         "Specify the channel name to use as part of determination if data is in an analysis ready state.",
                         "channel_name" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_ANALYSIS_READY_MASK,
                         "analysis-ready-mask",
                         Option::ARG_REQUIRED,
                         "Specify the mask to use as part of determination if data is in an analysis ready state.",
                         "bit_mask" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_DESCRIPTION,
                         "description",
                         Option::ARG_REQUIRED,
                         "Specify the description portion of filename.",
                         "string" ) );
    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_DIRECTORY_OUTPUT_FRAMES,
                "directory-output-frames",
                Option::ARG_REQUIRED,
                "The output directory where to store the resulting frames.",
                "directory" ) );
    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_DIRECTORY_OUTPUT_MD5SUM,
                "directory-output-md5sum",
                Option::ARG_REQUIRED,
                "The output directory where to store the md5sum checksum.",
                "directory" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_DIRECTORY_OUTPUT_MD5SUM_REGEXP,
                         "directory-output-md5sum-regexp",
                         Option::ARG_REQUIRED,
                         "Regular expression describing how to modify the "
                         "--directory-output-frames"
                         " option for outputing the md5sum checksums",
                         "directory" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_DISKCACHE_HOST,
                         "diskcache-host",
                         Option::ARG_REQUIRED,
                         "The host name which is running the diskcache server",
                         "hostname" ) );

    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_DISKCACHE_PORT,
                         "diskcache-port",
                         Option::ARG_REQUIRED,
                         "The number of the port for the diskcache server",
                         "int" ) );
    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_FILL_MISSING_DATA_VALID_ARRAY,
                "fill-missing-data-valid-array",
                Option::ARG_REQUIRED,
                "Specify if the data valid array should be generated if it is"
                " missing from the input data",
                "boolean" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option(
        OPT_FRAME_COMPRESSION_METHOD,
        "compression-type",
        Option::ARG_REQUIRED,
        "Compression method to use when writing the frame to the stream",
        "string" ) );
    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_FRAME_COMPRESSION_LEVEL,
                "compression-level",
                Option::ARG_REQUIRED,
                "Level of compression for compression methods that support it.",
                "integer" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_FRAME_FILES,
                         "frame-files",
                         Option::ARG_REQUIRED,
                         "A comma separated list of frame files",
                         "string" ) );
    //.....................................................................
    //
    // query = { <type> <ifo> <start-time>-<end-time>
    // Chan(<channel-name>[,<channel-name>]) }
    //   query_list = { <query> [<query> ...] }
    //
    // ex: { { H1_R H {} 1051388672-1051388927
    //         Chan(H0:PEM-EX_DUST_VEA1_300NM_PCF,H0:PEM-EX_DUST_VEA1_500NM_PCF,
    //         ... } }"
    //.....................................................................
    options.Add( Option( OPT_FRAME_QUERY,
                         "frame-query",
                         Option::ARG_REQUIRED,
                         "A TCL formatted list of frame queries"
                         " {{<FrameType> <IFO> <UNKNOWN> <Time Range> "
                         "Chan(<channel list>)} ...}",
                         "string" ) );

    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_FRAMES_PER_FILE,
                         "frames-per-file",
                         Option::ARG_REQUIRED,
                         "Specify the number of frames to store in each file",
                         "iteger" ) );
    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_GENERATE_FRAME_CHECKSUM,
                "generate-frame-checksum",
                Option::ARG_REQUIRED,
                "Specify if the frame checksum value should be generated",
                "boolean" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_LOG_DIRECTORY,
                         "log-directory",
                         Option::ARG_REQUIRED,
                         "Specify the directory to store logging information.",
                         "direcory" ) );
    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_LOG_DEBUG_LEVEL,
                "log-debug-level",
                Option::ARG_REQUIRED,
                "Specify the level of debugging information to generate.",
                "int" ) );
    //.....................................................................
    //.....................................................................
    options.Add(
        Option( OPT_PADDING,
                "padding",
                Option::ARG_REQUIRED,
                "Specify the maximum number of seconds of data preceding"
                " and following the requested data range needed to seed"
                " a resampling requests.",
                "padding" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_SECONDS_PER_FRAME,
                         "seconds-per-frame",
                         Option::ARG_REQUIRED,
                         "Specify the number of seconds to store in each frame",
                         "iteger" ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_VERIFY_CHECKSUM_OF_FRAME,
                         "verify-checksum-of-frame",
                         Option::ARG_REQUIRED,
                         "Require verification of the frame checksum.",
                         "boolean." ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_VERIFY_CHECKSUM_OF_STREAM,
                         "verify-checksum-of-stream",
                         Option::ARG_REQUIRED,
                         "Require verification of the stream checksum.",
                         "boolean." ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_VERIFY_DATA_VALID,
                         "verify-data-valid",
                         Option::ARG_REQUIRED,
                         "Require verification of the data valid field.",
                         "boolean." ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_VERIFY_FILENAME_METADATA,
                         "verify-filename-metadata",
                         Option::ARG_REQUIRED,
                         "Require verification of the filename metadata.",
                         "boolean." ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_VERIFY_TIME_RANGE,
                         "verify-time-range",
                         Option::ARG_REQUIRED,
                         "Require verification of the time range",
                         "boolean." ) );
    //.....................................................................
    //.....................................................................
    options.Add( Option( OPT_WITHOUT_HISTORY_RECORD,
                         "without-history-record",
                         Option::ARG_NONE,
                         "Suppress the creation of the RDS history record in "
                         "the output frames." ) );
}

inline const filenames_type&
CommandLine::Filenames( ) const
{
    return filenames;
}

inline const CommandLine::frame_queries_type&
CommandLine::FrameQueries( ) const
{
    return frame_queries;
}

inline void
CommandLine::Usage( int ExitValue ) const
{
    std::cerr << "Usage: " << ProgramName( ) << options << std::endl;
    depart( ExitValue );
}

inline void
CommandLine::operator( )( )
{
    static const char* caller = "CommandLine::operator()()";

    //---------------------------------------------------------------------
    // Parse the options specified on the command line
    //---------------------------------------------------------------------
    try
    {
        std::string name;
        std::string value;
        bool        parsing( true );
        std::string log_directory;
        int         log_debug_level = -2;
        std::string md5sum_regexp;

        //-------------------------------------------------------------------
        // Parse the option flags specified on the command line
        //-------------------------------------------------------------------
        while ( parsing )
        {
            const int id( Parse( options, name, value ) );

            switch ( id )
            {
            case CommandLineOptions::OPT_END_OF_OPTIONS:
                parsing = false;
                break;
            case OPT_HELP:
            {
                Usage( 0 );
            }
            break;
            case OPT_ALLOW_SHORT_FRAMES:
            {
                AllowShortFrames( InterpretBoolean( value ) );
            }
            case OPT_ANALYSIS_READY_MASK:
              {
                std::istringstream mask_stream( value );
                analysis_ready_mask_type mask;
                mask_stream >> mask;
                AnalysisReadyMask( mask );
              }
            break;
            case OPT_ANALYSIS_READY_CHANNEL_NAME:
              {
                AnalysisReadyChannelName( value );
              }
              break;
            case OPT_DESCRIPTION:
            {
                OutputType( value );
            }
            break;
            case OPT_DIRECTORY_OUTPUT_FRAMES:
            {
                DirectoryOutputFrames( value );
            }
            break;
            case OPT_DIRECTORY_OUTPUT_MD5SUM:
            {
                DirectoryOutputMD5Sum( value );
            }
            break;
            case OPT_DIRECTORY_OUTPUT_MD5SUM_REGEXP:
            {
                md5sum_regexp = value;
            }
            break;
            case OPT_DISKCACHE_HOST:
            {
                Hostname( value );
            }
            break;
            case OPT_DISKCACHE_PORT:
            {
                ServerInfo::port_type port;

                std::istringstream sport( value );

                sport >> port;
                Port( port );
            }
            break;
            case OPT_FILL_MISSING_DATA_VALID_ARRAY:
            {
                FillMissingDataValidArray( InterpretBoolean( value ) );
            }
            break;
            case OPT_FRAME_COMPRESSION_LEVEL:
            {
                std::istringstream     svalue( value );
                compression_level_type lvalue;

                svalue >> lvalue;
                CompressionLevel( lvalue );
            }
            break;
            case OPT_FRAME_COMPRESSION_METHOD:
            {
                CompressionMethod( value );
            }
            break;
            case OPT_FRAME_FILES:
            {
                parse_frame_files( value );
            }
            break;
            case OPT_FRAME_QUERY:
            {
                parse_frame_query( value );
            }
            break;
            case OPT_FRAMES_PER_FILE:
            {
                std::istringstream   v( value );
                frames_per_file_type lvalue;

                v >> lvalue;
                FramesPerFile( lvalue );
            }
            break;
            case OPT_GENERATE_FRAME_CHECKSUM:
            {
                GenerateFrameChecksum( InterpretBoolean( value ) );
            }
            break;
            case OPT_LOG_DEBUG_LEVEL:
            {
                std::istringstream slevel( value );

                slevel >> log_debug_level;

                GenericAPI::setLogDebugLevel( log_debug_level );
            }
            break;
            case OPT_LOG_DIRECTORY:
            {
                std::cerr << "CERR_DEBUG: Setting log directory to: " << value
                          << std::endl;
                log_directory = value;

                GenericAPI::LoggingInfo::LogDirectory( value );
            }
            break;
            case OPT_PADDING:
            {
                std::istringstream sv( value );

                int v;

                sv >> v;
                Padding( v );
            }
            break;
            case OPT_SECONDS_PER_FRAME:
            {
                seconds_per_frame_type s;
                std::istringstream     v( value );

                v >> s;
                SecondsPerFrame( s );
            }
            break;
            case OPT_VERIFY_CHECKSUM_OF_FRAME:
            {
                VerifyChecksumPerFrame( InterpretBoolean( value ) );
            }
            break;
            case OPT_VERIFY_CHECKSUM_OF_STREAM:
            {
                VerifyChecksumOfStream( InterpretBoolean( value ) );
            }
            break;
            case OPT_VERIFY_DATA_VALID:
            {
                VerifyDataValid( InterpretBoolean( value ) );
            }
            break;
            case OPT_VERIFY_FILENAME_METADATA:
            {
                VerifyFilenameMetadata( InterpretBoolean( value ) );
            }
            break;
            case OPT_VERIFY_TIME_RANGE:
            {
                VerifyTimeRange( InterpretBoolean( value ) );
            }
            break;
            case OPT_WITHOUT_HISTORY_RECORD:
            {
                HistoryRecord( false );
            }
            break;
            }
        }
        //---------------------------------------------------------------------
        // Check if md5sum directory is described by regex
        //---------------------------------------------------------------------
        if ( ( md5sum_regexp.size( ) > 0 ) &&
             ( DirectoryOutputMD5Sum( ).size( ) <= 0 ) &&
             ( DirectoryOutputFrames( ).size( ) > 0 ) )
        {
            Sed sed( md5sum_regexp );

            DirectoryOutputMD5Sum( sed( DirectoryOutputFrames( ) ) );

            QUEUE_LOG_MESSAGE(
                "sed modified md5sum directory: " << DirectoryOutputMD5Sum( ),
                MT_DEBUG,
                30,
                caller,
                "CMD_CREATE_RDS" );
        }
    }
    catch ( const std::invalid_argument& Error )
    {
        throw;
    }
    catch ( ... )
    {
    }
    //---------------------------------------------------------------------
    // Save the list of files
    //---------------------------------------------------------------------
    while ( empty( ) == false )
    {
        filenames.push_back( Pop( ) );
    }
    //---------------------------------------------------------------------
    // Try to ensure the directories exist
    //---------------------------------------------------------------------
    if ( DirectoryOutputFrames( ).empty( ) == false )
    {
        try
        {

            MkDir d( 0755, MkDir::OPT_MAKE_PARENT_DIRECTORIES );

            d( DirectoryOutputFrames( ) );
        }
        catch ( const std::exception& except )
        {
            QUEUE_LOG_MESSAGE( "Unable to create directory: "
                                   << DirectoryOutputFrames( ) << " ("
                                   << except.what( ) << ")",
                               MT_WARN,
                               0,
                               caller,
                               "CMD_CREATE_RDS" );
        }
    }
    if ( ( DirectoryOutputMD5Sum( ).empty( ) == false ) &&
         ( DirectoryOutputMD5Sum( ).compare( DirectoryOutputFrames( ) ) != 0 ) )
    {
        try
        {
            MkDir d( 0755, MkDir::OPT_MAKE_PARENT_DIRECTORIES );

            d( DirectoryOutputMD5Sum( ) );
        }
        catch ( const std::exception& except )
        {
            QUEUE_LOG_MESSAGE( "Unable to create directory: "
                                   << DirectoryOutputMD5Sum( ) << " ("
                                   << except.what( ) << ")",
                               MT_WARN,
                               0,
                               caller,
                               "CMD_CREATE_RDS" );
        }
    }
}

void
CommandLine::parse_frame_files( const std::string& Filenames )
{
    static const char* caller = "CommandLine::parse_frame_files";

    if ( Filenames.size( ) )
    {
        size_type pos( 0 );
        size_type found_pos;

        while ( pos != std::string::npos )
        {
            found_pos = Filenames.find( ',', pos );
            filenames.push_back( Filenames.substr( pos, found_pos - pos ) );
            pos = found_pos;
            if ( pos != std::string::npos )
            {
                ++pos;
            }
            QUEUE_LOG_MESSAGE( "filename: " << filenames.back( ),
                               MT_DEBUG,
                               30,
                               caller,
                               "CMD_CREATE_RDS" );
        }
    }
}

void
CommandLine::parse_frame_query( const std::string& Query )
{
    static const char* caller = "CommandLine::parse_frame_query";

    std::list< std::string > fq;

    QUEUE_LOG_MESSAGE( "parse_frame_query: Query: " << Query,
                       MT_DEBUG,
                       30,
                       caller,
                       "CMD_CREATE_RDS" );
    GenericAPI::TCL::ParseList( Query.c_str( ), fq );

    for ( std::list< std::string >::const_iterator cur = fq.begin( ),
                                                   last = fq.end( );
          cur != last;
          ++cur )
    {
        QUEUE_LOG_MESSAGE( "parse_frame_query: cur: " << *cur,
                           MT_DEBUG,
                           30,
                           caller,
                           "CMD_CREATE_RDS" );
        frame_queries.push_back( query( *cur ) );
    }
    QUEUE_LOG_MESSAGE(
        "parse_frame_query: finished", MT_DEBUG, 30, caller, "CMD_CREATE_RDS" );
}
