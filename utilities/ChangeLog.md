# Release 2.7.1 - July 31, 2023
    - Used routines from frameAPI to read channel names from file.

# Release 2.7.0 - July 10, 2023
    - Removed duplicate summary line from rpm spec file (closes #163)
    - Modified channel query syntax to accept renaming (closes #170)

# Release 2.6.8 - January 17, 2023
    - Relink to framecpp 3.x

# Release 2.6.7 - September 29, 2021
    - Many corrections to support building on RHEL 8

# Release 2.6.5 - August 14, 2019
  - Corrected Portfile to have proper description (Closes #55)
  - Added generation of Doxygen documentation (Closes #72)

# Release 2.6.4 - January  8, 2019
  - Converted to use new IFrameStream::GetTOC() return type

# Release 2.6.3 - December 6, 2018
  - Addressed packaging issues

# Release 2.6.2 - November 27, 2018
  - Added requirement of C++ 2011 standard
  - Standardize source code format by using clang-format
  - Updated to cmake_helper 1.0.4

# Release 2.6.1 - June 22, 2018
  - Updated packaging rules to have build time dependency on specific
    versions of LDAS Tools packages

# Release 2.6.0 - June 19, 2018
  - Removed hand rolled smart pointers in favor of boost smart pointers

# Release 2.5.1 - September 9, 2016
  - Added --disable-warnings-as-errors to allow compilation on systems
    where warning messages have not yet been addressed
  - Added conditional setting of DESTDIR in python.mk to prevent install
    issues.
  - Added support for new padding capability.
  - Modified channel name detection algorithm to assume channel name if
    pattern fails to open as a file.

# Release 2.5.0 - April 7, 2016
  - Official release of splitting LDASTools into separate source packages

# Release 2.4.99.1 - March 11, 2016
  - Corrections to spec files.

# Release 2.4.99.0 - March 3, 2016
  - Separated code into independant source distribution
