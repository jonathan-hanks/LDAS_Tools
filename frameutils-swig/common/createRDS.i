/*
 * LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo Frame
 * Files
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools Frame Utilities is free software; you may redistribute it and/or
 * modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools Frame Utilities is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


%module(package="LDAStools") LDASframe

%{
#include <string>
#include <vector>

#include "frameAPI/createRDS.hh"

using namespace FrameAPI;
 typedef FrameAPI::RDS::Options::rds_level_type rds_level_type;
 typedef ::FrameAPI::RDS::FileOptions Options;

%}

%include "std_string.i"
%include "std_vector.i"

%include "ldastoolsal/types.hh"

%import "Frame.i"

%import "frameAPI/createRDS.hh"

%apply swig_vector_string_type { frame_file_container_type };

typedef CREATERDS_START_TYPE start_type;
typedef CREATERDS_END_TYPE end_type;

class Options
{
public:
  Options( );

  const std::string& DirectoryOutputFrames( ) const;
  
  const std::string& DirectoryOutputMD5Sum( ) const;

  const std::string& OutputDirectory( ) const;

  const std::string& MD5SumOutputDirectory( ) const;

  void DirectoryOutputFrames( const std::string& Value );

  void DirectoryOutputMD5Sum( const std::string& Value );

  bool AllowShortFrames( ) const;

  void AllowShortFrames( bool );

  compression_level_type CompressionLevel( ) const;

  compression_method_type CompressionMethod( ) const;

  bool CreateChecksumPerFrame( ) const;

  bool FillMissingDataValidArray( ) const;

  frames_per_file_type FramesPerFile( ) const;

  void FramesPerFile( frames_per_file_type FPF );

  bool GenerateFrameChecksum( ) const;

  bool HistoryRecord( ) const;

  end_type OutputTimeEnd( ) const;

  void OutputTimeEnd( end_type Value);

  start_type OutputTimeStart( ) const;

  void OutputTimeStart( start_type Value );

  const std::string& OutputType( ) const;

  void OutputType( const std::string& Value );

  rds_level_type RDSLevel( ) const;

  void RDSLevel( rds_level_type Level );

  seconds_per_frame_type SecondsPerFrame( ) const;

  void SecondsPerFrame( seconds_per_frame_type SPF );

  bool VerifyChecksum( ) const;

  bool VerifyChecksumOfFrame( ) const;

  bool VerifyChecksumPerFrame( ) const;

  bool VerifyChecksumOfStream( ) const;

  bool VerifyDataValid( ) const;

  bool VerifyFilenameMetadata( ) const;

  bool VerifyTimeRange( ) const;

  LDAS_PROPERTY_INIT( )

  LDAS_PROPERTY_READ_WRITE("allow_short_frames",
			   AllowShortFrames,AllowShortFrames);
  LDAS_PROPERTY_READ_WRITE("compression_level",
			   CompressionLevel, CompressionLevel );
  LDAS_PROPERTY_READ_WRITE("compression_type",
			   CompressionMethod, CompressionMethod );
  LDAS_PROPERTY_READ_WRITE("description",
			   OutputType, OutputType );
  LDAS_PROPERTY_READ_WRITE("directory_output_frames",
			   DirectoryOutputFrames, DirectoryOutputFrames );
  LDAS_PROPERTY_READ_WRITE("directory_output_md5sum",
			   DirectoryOutputMD5Sum, DirectoryOutputMD5Sum );
  LDAS_PROPERTY_READ_WRITE("fill_missing_data_valid_array",
			   FillMissingDataValidArray, FillMissingDataValidArray );
  LDAS_PROPERTY_READ_WRITE("frames_per_file",
			   FramesPerFile, FramesPerFile );
  LDAS_PROPERTY_READ_WRITE("generate_frame_checksum",
			   GenerateFrameChecksum, GenerateFrameChecksum );
  LDAS_PROPERTY_READ_WRITE("outputTimeStart",
			   OutputTimeStart,OutputTimeStart);
  LDAS_PROPERTY_READ_WRITE("outputTimeEnd",
			   OutputTimeEnd,OutputTimeEnd);
  LDAS_PROPERTY_READ_WRITE("seconds_per_frame",
			   SecondsPerFrame, SecondsPerFrame );
  LDAS_PROPERTY_READ_WRITE("without_history_record",
			   HistoryRecord, HistoryRecord );
};

Frame
createRDSFrame( frame_file_container_type FrameFiles,
		start_type Start,
		end_type End,
		channel_container_type Channels );

#if 1
void
createRDSSet( frame_file_container_type FrameFiles,
	      channel_container_type Channels,
	      const Options& Opts );
#endif /* 0 */
