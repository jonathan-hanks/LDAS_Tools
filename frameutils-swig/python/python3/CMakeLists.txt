if ( NOT DEFINED ENABLE_SWIG_PYTHON3 OR ENABLE_SWIG_PYTHON3 )
  if ( NOT PYTHON3_VERSION )
    set(PYTHON3_VERSION 3)
  endif ()

  #======================================================================
  # Find python and key modules
  #======================================================================

  cx_python(
      VERSION ${PYTHON3_VERSION}
      PREFIX PYTHON3 )

  if (PYTHON3LIBS_FOUND)
    #----------------------------------------------------------------------
    # include some additonal functions/macros
    #----------------------------------------------------------------------

    include( ${CMAKE_SOURCE_DIR}/python/source/testsuite.cmake )

    #----------------------------------------------------------------------
    # All pieces needed have been found
    #----------------------------------------------------------------------

    set( ENABLE_SWIG_PYTHON3 "yes"
      CACHE INTERNAL "Enable building of SWIG bindings for Python" )
    message(STATUS "Generating Python ${PYTHON_VERSION_STRING} extensions for ${PROJECT_NAME}")

    #----------------------------------------------------------------------
    # Build the python 2 bindings
    #----------------------------------------------------------------------

    include_directories(
      BEFORE
      ${CMAKE_CURRENT_SOURCE_DIR}
      ${PROJECT_SOURCE_DIR}/python/source
      ${PROJECT_BINARY_DIR}/python/source
      ${PROJECT_SOURCE_DIR}/common
      ${PROJECT_BINARY_DIR}/common
      ${PROJECT_BINARY_DIR}/include
      ${LDASTOOLS_INCLUDE_DIRS}
      ${PYTHON3_INCLUDE_PATH}
      ${CMAKE_INSTALL_FULL_INCLUDEDIR}
      )
    unset(DASH_I_INCLUDE_PATHS)
    unset( EXTRA_INCLUDE_PATH )
    if ( "${LDASTOOLS_INCLUDE_DIRS}" STREQUAL "" )
      set( EXTRA_INCLUDE_PATH "/usr/include" )
    endif ( )
    foreach( path ${INCLUDE_PATH} ${EXTRA_INCLUDE_PATH})
      list(APPEND DASH_I_INCLUDE_PATHS "-I${path}")
    endforeach( )

    # cx_msg_debug_variable( LDASTOOLS_LIBRARIES_FULL_PATH )

    cx_swig_python_module(
      PREFIX         PYTHON3
      INTERFACE_FILE ${PROJECT_SOURCE_DIR}/python/source/LDASframe_Python.i
      MODULE         LDASframe
      MODULE_DIR     LDAStools
      CXX
      SWIG_FLAGS     ${DASH_I_INCLUDE_PATHS}
      LINK_LIBRARIES ${LDASTOOLS_LIBRARIES_FULL_PATH}
      )

    #====================================================================
    # Do unit tests to verify the interface
    #====================================================================
    do_python_tests(
      PREFIX            PYTHON3
      NAMESPACE         python3
      PYTHON_MODULE_DIR ${CMAKE_CURRENT_BINARY_DIR}
      TEST_SOURCE_DIR   ${CMAKE_SOURCE_DIR}/python/source
      )
  else( )
    #--------------------------------------------------------------------
    # All pieces needed have NOT been found
    #--------------------------------------------------------------------
    message(STATUS "Not generating Python 3 extensions for ${PROJECT_NAME}")
  endif( )

endif ( NOT DEFINED ENABLE_SWIG_PYTHON3 OR ENABLE_SWIG_PYTHON3 )
