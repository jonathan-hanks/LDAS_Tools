# -*- conding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug )
include( Autotools/ArchiveX/cx_ctest_add_python_test )

#------------------------------------------------------------------------
# add_python_test_
#
# Parameters:
#   target: name of test without the PYTHON_NAMESPACE prefix.
#   ARGN:   command and parameters of test to be executed
#
# Environment:
#   PYTHON_PREFIX    - Prefix to use for python variables    
#   PYTHON_NAMESPACE - namespace to identify python (ex: python3 )
#   TEST_SOURCE_DIR  - Directory containing the python script
#------------------------------------------------------------------------
function( add_python_test_ target )
    set(options
        )
    set(oneValueArgs
        DEPENDS
        )
    set(multiValueArgs
        )
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    list(GET ARG_UNPARSED_ARGUMENTS 0 cmd )
    list(REMOVE_AT ARG_UNPARSED_ARGUMENTS 0)

    cx_ctest_add_python_test(
        TARGET _${PYTHON_NAMESPACE}${target}
        COMMAND "${TEST_SOURCE_DIR}/${cmd}" ${ARG_UNPARSED_ARGUMENTS}
        PYTHON_EXECUTABLE ${${PYTHON_PREFIX}_EXECUTABLE}
        PYTHON_MODULE_DIR ${PYTHON_MODULE_DIR}
        PYTHON_PATH ${CMAKE_SOURCE_DIR}/python/source ${LDASTOOLS_LIBRARY_DIRS}
        DEPENDS ${ARG_DEPENDS}
        )
    cx_msg_debug( "Adding dependencies: ${ARG_DEPENDS}" )

endfunction( )

#-----------------------------------------------------------------------
# 
#-----------------------------------------------------------------------
function( do_python_tests )
    set(options)
    set(oneValueArgs NAMESPACE PREFIX PYTHON_MODULE_DIR TEST_SOURCE_DIR)
    set(multiValueArgs)
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if ( NOT ARG_PREFIX )
        set( ARG_PREFIX PYTHON )
    endif( )

    if ( ARG_NAMESPACE )
        set( PYTHON_NAMESPACE ${ARG_NAMESPACE} )
    else ( )
        set( PYTHON_NAMESPACE "python" )
    endif ( )
    if ( ARG_PYTHON_MODULE_DIR )
        set( PYTHON_MODULE_DIR ${ARG_PYTHON_MODULE_DIR} )
    endif ( )
    if ( ARG_TEST_SOURCE_DIR )
        set( TEST_SOURCE_DIR ${ARG_TEST_SOURCE_DIR} )
    else ( )
        set( TEST_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} )
    endif ( )

    set( PYTHON_EXEC ${${ARG_PREFIX}_EXECUTABLE} )

    #====================================================================
    # Dependecies
    #====================================================================

    #====================================================================
    # These tests require a minimum of Python
    #====================================================================
endfunction( )
