//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "MountPointStatus.cc"

#include "ldastoolsal/unittest.h"

LDASTools::Testing::UnitTest Test;

void
Unmanaged( )
{
    bool status = false;

    try
    {
        mount_points_type m;
        m[ "/junk1" ] = NULL;
        m[ "/junk3" ] = NULL;

        mount_points_type::const_iterator i = m.Locate( "/junk2" );

        status = ( i == m.end( ) );
    }
    catch ( const std::range_error& e )
    {
        status = true;
    }
    catch ( ... )
    {
        status = false;
    }
    Test.Check( status ) << "Testing of unmanaged directory" << std::endl;
}

void
UnmanagedHead( )
{
    bool status = false;

    try
    {
        mount_points_type m;
        m[ "/junk1" ] = NULL;
        m[ "/junk3" ] = NULL;

        mount_points_type::const_iterator i = m.Locate( "/aaa" );

        status = ( i == m.end( ) );
    }
    catch ( const std::range_error& e )
    {
        status = true;
    }
    catch ( ... )
    {
        status = false;
    }
    Test.Check( status ) << "Testing of unmanaged head directory" << std::endl;
}

void
UnmanagedTail( )
{
    bool status = false;

    try
    {
        mount_points_type m;
        m[ "/junk1" ] = NULL;
        m[ "/junk3" ] = NULL;

        mount_points_type::const_iterator i = m.Locate( "/zzz" );

        status = ( i == m.end( ) );
    }
    catch ( const std::range_error& e )
    {
        status = true;
    }
    catch ( ... )
    {
        status = false;
    }
    Test.Check( status ) << "Testing of unmanaged tail directory" << std::endl;
}

void
RootDirectory( )
{
    bool status = false;

    try
    {
        mount_points_type m;
        m[ "/junk1" ] = NULL;
        m[ "/junk2" ] = NULL;
        m[ "/junk3" ] = NULL;

        mount_points_type::const_iterator i = m.Locate( "/junk2" );

        status = ( ( i != m.end( ) ) && ( i->first.compare( "/junk2" ) == 0 ) );
    }
    catch ( ... )
    {
        status = false;
    }
    Test.Check( status ) << "Testing of root directory support" << std::endl;
}

void
SubDirectory( )
{
    bool status = false;

    try
    {
        mount_points_type m;
        m[ "/junk1" ] = NULL;
        m[ "/junk2" ] = NULL;
        m[ "/junk3" ] = NULL;

        mount_points_type::const_iterator i =
            m.Locate( "/junk2/stuff/under/dir" );

        status = ( ( i != m.end( ) ) && ( i->first.compare( "/junk2" ) == 0 ) );
    }
    catch ( ... )
    {
        status = false;
    }
    Test.Check( status ) << "Testing of subdirectory support" << std::endl;
}

void
SubDirectory2( )
{
    bool status = false;

    try
    {
        mount_points_type m;
        m[ "/junk1" ] = NULL;
        m[ "/junk2/stuff" ] = NULL;
        m[ "/junk2/su" ] = NULL;
        m[ "/junk3" ] = NULL;

        mount_points_type::const_iterator i =
            m.Locate( "/junk2/stuff/under/dir" );

        status = ( ( i != m.end( ) ) &&
                   ( i->first.compare( "/junk2/stuff" ) == 0 ) );
    }
    catch ( ... )
    {
        status = false;
    }
    Test.Check( status ) << "Testing of subdirectory support (2)" << std::endl;
}

void
SubDirectory3( )
{
    bool status = false;

    try
    {
        mount_points_type m;
        m[ "/junk1" ] = NULL;
        m[ "/junk2/stu" ] = NULL;
        m[ "/junk2/stuff" ] = NULL;
        m[ "/junk3" ] = NULL;

        mount_points_type::const_iterator i =
            m.Locate( "/junk2/stu/under/dir" );

        status =
            ( ( i != m.end( ) ) && ( i->first.compare( "/junk2/stu" ) == 0 ) );

        i = m.Locate( "/junk2/stuff/under/dir" );
        status = ( status && ( i != m.end( ) ) &&
                   ( i->first.compare( "/junk2/stuff" ) == 0 ) );
    }
    catch ( ... )
    {
        status = false;
    }
    Test.Check( status ) << "Testing of subdirectory support (3)" << std::endl;
}

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

#if 0 || 1
    Unmanaged( );
    UnmanagedHead( );
    UnmanagedTail( );
    RootDirectory( );
    SubDirectory( );
    SubDirectory2( );
#endif /* 0 */
    SubDirectory3( );

    Test.Exit( );

    return 1; // Should never get here so exit with error if it happens
}
