# Release NEXT -
  - Initialize the logging debug level
  - Added unit test for parsing of TCL constructs

# Release 2.7.2 - August 13, 2021
  - Replaced flex with boost::tokenizer (closes #105)

# Release 2.7.3 - September 29, 2021
    - Many corrections to support building on RHEL 8

# Release 2.7.1 - March 9, 2021
  * Packaging corrections

# Release 2.7.0 - August 14, 2019
  - Converted to CMake (Closes #34)
  - Added description text to Portfile (Closes #55)
  - Corrected Portfile to have proper description (Closes #55)
  - Removed close() from socket stream.
  - Removed IsLogging( int, int, std::string const& ) from Logging.
  - Removed LogFileDefault() from Logging.
  - Removed stat( char const*, struct stat* ) from Stat.
  - Updated ldasgen shared library to 5.0.0
  - Updated Stat shared library to 3.0.0

# Release 2.6.3 - December 6, 2018
  - Addressed packaging issues

# Release 2.6.2 - November 27, 2018
  - Added requirement of C++ 2011 standard
  - Standardize source code format by using clang-format

# Release 2.6.1 - June 22, 2018
  - Updated packaging rules to have build time dependency on specific
    versions of LDAS Tools packages

# Release 2.6.0 - June 19, 2018
  - Removed hand rolled smart pointers in favor of boost smart pointers
  - Changed version of ldasgen library to 4:0:0

# Release 2.5.1 - September 9, 2016
  - Added --disable-warnings-as-errors to allow compilation on systems
    where warning messages have not yet been addressed
  - Added conditional setting of DESTDIR in python.mk to prevent install
    issues.

# Release 2.5.0 - April 7, 2016
  - Official release of splitting LDASTools into separate source packages

# Release 2.4.99.1 - March 11, 2016

# Release 2.4.99.0 - March 3, 2016
  - Separated code into independant source distribution
