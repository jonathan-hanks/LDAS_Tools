//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__MOUNT_POINT_STATUS_HH
#define GENERIC_API__MOUNT_POINT_STATUS_HH

#include <algorithm>
#include <list>
#include <map>
#include <string>
#include <vector>

#include "ldastoolsal/Directory.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/Thread.hh"

namespace GenericAPI
{

    //=====================================================================
    /// \brief Monitors a list of directories.
    ///
    /// The main focus of this class is to provide methods which maintain
    /// information about a group of directories.
    //=====================================================================
    class MountPointStatus
    {
    public:
        enum state_type
        {
            STATE_OFFLINE,
            STATE_ONLINE
        };

        //-------------------------------------------------------------------
        /// \brief Container for a set of mount points.
        ///
        /// This type is used by several routines to pass a set of
        /// directories on which to operate.
        //-------------------------------------------------------------------
        typedef std::list< std::string > mount_point_list_type;

        //-------------------------------------------------------------------
        /// \brief Initialize the class
        ///
        /// \return
        ///     True if initialization completes successfully,
        ///     false otherwise.
        //-------------------------------------------------------------------
        static bool Initialize( );

        //-------------------------------------------------------------------
        /// \brief Adds directories to set of managed directories
        ///
        /// \param[in] MountPoint
        ///	    A group of directories to be added.
        ///
        /// \return
        ///     Nothing
        //-------------------------------------------------------------------
        static void Add( const mount_point_list_type& MountPoint );

        //-------------------------------------------------------------------
        /// \brief Checks if directory is being managed
        ///
        /// \param[in] Dir
        ///     The name of a file or directory on the file system
        ///
        /// \return
        ///     If their is an exact match, then true is returned,
        ///     false otherwise.
        //-------------------------------------------------------------------
        static bool IsDirManaged( const std::string& Dir );

        //-------------------------------------------------------------------
        /// \brief Removes directories from the set of managed directories.
        ///
        /// \param[in] MountPoint
        ///	    A group of directories to be removed.
        ///
        /// \return
        ///     Nothing
        //-------------------------------------------------------------------
        static void Remove( const mount_point_list_type& MountPoint );

        //-------------------------------------------------------------------
        /// \brief Resets the set of directories to be managed.
        ///
        /// \param[in] MountPoint
        ///	    A group of directories to be managed.
        ///
        /// \return
        ///     Nothing
        //-------------------------------------------------------------------
        static void Set( const mount_point_list_type& MountPoint );

        //-------------------------------------------------------------------
        /// \brief Resets the set of directories to be managed.
        ///
        /// \param[in] MountPoint
        ///	    A group of directories to be managed.
        ///
        /// \return
        ///     Nothing
        //-------------------------------------------------------------------
        static void Set( const std::vector< std::string >& MountPoint );

        //-------------------------------------------------------------------
        /// \brief Returns the on-line status of the file system
        ///
        /// \param[in] Path
        ///	    The Path to a file or directory on the file system for
        ///     which the on-line status is being queried.
        ///
        /// \return
        ///     The on-line status of the file system.
        //-------------------------------------------------------------------
        static state_type Status( const std::string& Path );

        static void Offline( const std::string& Path );
    }; /* class MountPointStatus */

    //=====================================================================
    // MountPointStatus
    //=====================================================================

    inline void
    MountPointStatus::Set( const std::vector< std::string >& MountPoint )
    {
        mount_point_list_type mp;

        std::copy(
            MountPoint.begin( ), MountPoint.end( ), std::back_inserter( mp ) );
        Set( mp );
    }
} // namespace GenericAPI

#endif /* GENERIC_API__MOUNT_POINT_STATUS_HH */
