//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef LDAS_TOOLS__LDAS_GEN__MONITOR_MEMORY_HH
#define LDAS_TOOLS__LDAS_GEN__MONITOR_MEMORY_HH

#include <genericAPI/Status.hh>

namespace GenericAPI
{
    namespace Status
    {
        class MonitorMemory : public Monitor< long >
        {
        public:
            typedef int  flag_type;
            typedef long memory_size_type;
            enum
            {
                MEMORY_VIRTUAL = 0x0001,
                MEMORY_PHYSICAL = 0x0002
            };

            MonitorMemory( flag_type Memory );

            virtual void operator( )( frequency_type Frequency );

        private:
            flag_type flag;
        };
    } // namespace Status
} // namespace GenericAPI

#endif /* LDAS_TOOLS__LDAS_GEN__MONITOR_MEMORY_HH */
