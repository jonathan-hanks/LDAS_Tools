//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <stdexcept>

#include "genericAPI/StatStream.hh"

namespace GenericAPI
{
    int
    StatStream::Request( int                ReadFD,
                         int                WriteFD,
                         const std::string& Filename,
                         struct stat&       Buf ) const
    {
        //-------------------------------------------------------------------
        // :TODO: transmit the request on the socket
        //-------------------------------------------------------------------
        int len = Filename.length( );

        write( WriteFD, &len, sizeof( len ) );
        write( WriteFD, Filename.c_str( ), len );
        //-------------------------------------------------------------------
        // :TODO: Read the responce
        //-------------------------------------------------------------------
        int retval;
        int errcode;

        read( ReadFD, &retval, sizeof( retval ) );
        if ( retval == 0 )
        {
            // Successfully processed request
            read( ReadFD, &Buf, sizeof( Buf ) );
        }
        else
        {
            // Error condition.
            read( ReadFD, &errcode, sizeof( errcode ) );
            errno = errcode;
        }
        return retval;
    }

    void
    StatStream::Process( int ReadFD, int WriteFD ) const
    {
        int  len;
        char buffer[ 1024 ];

        //-------------------------------------------------------------------
        // Read the request
        //-------------------------------------------------------------------
        read( ReadFD, &len, sizeof( int ) );
        if ( len > 0 )
        {
            read( ReadFD, buffer, len );
        }
        buffer[ len ] = '\0';
        //-------------------------------------------------------------------
        // Process the request
        //-------------------------------------------------------------------
        struct stat buf;
        int         retval;
        int         errcode;

        retval = lstat( buffer, &buf );
        errcode = errno;

        //-------------------------------------------------------------------
        // Write the responce
        //-------------------------------------------------------------------
        write( WriteFD, &retval, sizeof( retval ) );
        if ( retval == 0 )
        {
            // Successfully processed request
            write( WriteFD, &buf, sizeof( buf ) );
        }
        else
        {
            write( WriteFD, &errcode, sizeof( errcode ) );
        }
    }

    void
    StatStream::read( int FD, void* Buffer, int BufferSize ) const
    {
        int   status = 0;
        char* buffer = static_cast< char* >( Buffer );
        int   offset = 0;

        do
        {
            status = ::read( FD, &buffer[ offset ], BufferSize - offset );
            offset += status;
        } while ( ( status > 0 ) && ( offset < BufferSize ) );
        if ( status <= 0 )
        {
            throw std::underflow_error( "Read less than 1 byte" );
        }
    }

    void
    StatStream::write( int FD, const void* Buffer, int BufferSize ) const
    {
        int status = ::write( FD, Buffer, BufferSize );

        if ( status <= 0 )
        {
            throw std::underflow_error( "Wrote less than 1 byte" );
        }
    }
} // namespace GenericAPI
