//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__LSTAT_BASE_HH
#define GENERIC_API__LSTAT_BASE_HH

#include <sys/types.h>
#include <sys/stat.h>

namespace LDASTools
{
    namespace AL
    {
        class Directory;
    }
} // namespace LDASTools

namespace GenericAPI
{
    class StatBase
    {
    public:
        enum debug_info
        {
            STAT_DEBUG_GENERAL
        };

        typedef struct stat              stat_buf_type;
        typedef LDASTools::AL::Directory directory_type;

        virtual ~StatBase( );

        virtual std::string
        Debug( debug_info DebugInfo = STAT_DEBUG_GENERAL ) const = 0;

        virtual void Init( ) = 0;

        virtual int LStat( const std::string& FileName,
                           stat_buf_type&     Buf ) const = 0;

        virtual int LStat( const directory_type& Dir,
                           const std::string&    RelFileName,
                           stat_buf_type&        Buf ) const = 0;

        virtual int Stat( const std::string& FileName,
                          stat_buf_type&     Buf ) const = 0;

        virtual int Stat( const directory_type& Dir,
                          const std::string&    RelFileName,
                          stat_buf_type&        Buf ) const = 0;

        virtual StatBase* vnew( ) const = 0;
    };
} // namespace GenericAPI

#endif /* GENERIC_API__LSTAT_BASE_HH */
