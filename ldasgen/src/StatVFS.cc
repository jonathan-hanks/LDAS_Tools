//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/types.h>
#include <sys/statvfs.h>

#include <fcntl.h>

#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/ErrorLog.hh"
#include "ldastoolsal/Pool.hh"
#include "ldastoolsal/Singleton.hh"
#include "ldastoolsal/SystemCallTask.hh"
#include "ldastoolsal/Timeout.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/MountPointStatus.hh"
#include "genericAPI/StatVFS.hh"

using LDASTools::AL::ErrorLog;
using LDASTools::AL::MemChecker;
using LDASTools::AL::Pool;
using LDASTools::AL::StdErrLog;
using LDASTools::AL::SystemCallTask;
using LDASTools::AL::Timeout;
using LDASTools::AL::TIMEOUT_COMPLETED;
using LDASTools::AL::TimeoutException;

namespace GenericAPI
{
    extern int lstat_interupt( );
    extern int lstat_timeout( );
} // namespace GenericAPI

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
namespace
{
    //---------------------------------------------------------------------
    /// \brief Perform statvfs system call
    ///
    /// This class wraps the statvfs system call.
    /// The benifit of using this call is that the user is protected
    //---------------------------------------------------------------------
    class lStatVFS : public SystemCallTask
    {
    public:
        typedef struct statvfs buffer_type;
        //-------------------------------------------------------------------
        /// \brief Specify the file to be used in statvfs call
        ///
        /// \param[in] Filename
        ///     The pathname of any file within the mounted filesystem.
        //-------------------------------------------------------------------
        void Reset( const std::string& Filename );

        //-------------------------------------------------------------------
        /// \brief Syncronize the callers buffer with the local buffer.
        //-------------------------------------------------------------------
        void Sync( buffer_type& StatVFSBuffer );

    protected:
        //-------------------------------------------------------------------
        /// \brief Do the statvfs call
        ///
        /// \return
        ///     Upon successful completion, the value zero is returned.
        ///     Upon failure, the value -1 is returned and errno
        ///     is set.
        //-------------------------------------------------------------------
        virtual int eval( );

    private:
        //-------------------------------------------------------------------
        /// \brief The pathname of any file within the mounted filesystem.
        //-------------------------------------------------------------------
        std::string m_filename;
        //-------------------------------------------------------------------
        /// \brief Buffer used to return information about the file system.
        ///
        /// This buffer is used locally to make the call thread safe.
        /// If the native call were to use the caller's buffer directly,
        /// then in the situation where the native call times out,
        /// the application would be vulnerable to memory corruption
        /// as the callers buffer would go out of scope and the system
        /// would reclaim the resources.
        //-------------------------------------------------------------------
        buffer_type m_buffer_ts;
    };

    typedef boost::shared_ptr< lStatVFS > value_type;

    //---------------------------------------------------------------------
    /// \brief Collection of reusable statvfs buffers
    //---------------------------------------------------------------------
    class StatVFSPool : public Pool< value_type >
    {
        SINGLETON_TS_DECL( StatVFSPool );
    };

    //---------------------------------------------------------------------
    /// \brief Collection of statvfs buffers that have timed out
    ///
    /// This is only a stop gap solution to prevent the memory resource
    /// from being recycled while being used by another thread.
    ///
    /// \todo
    ///    Instead of collecting these buffers in a pool where they will
    ///    be leaking memory, the thread needs to take ownership of the
    ///    object and destroy it once the thread completes.
    //---------------------------------------------------------------------
    class StatVFSTrashPool : public Pool< value_type >
    {
        SINGLETON_TS_DECL( StatVFSTrashPool );
    };

    //---------------------------------------------------------------------
    /// \brief Create a new instance for use in the pool
    ///
    /// This routine is used to create an new instance of the lStatVFS
    /// class if none are available in the pool.
    //---------------------------------------------------------------------
    value_type
    create_statvfs( )
    {
        value_type retval( new lStatVFS );

        return retval;
    }

    //---------------------------------------------------------------------
    /// This actually does the system call.
    /// The local buffer is used to prevent memory corruuption in the
    /// instances where the system call times out.
    //---------------------------------------------------------------------
    int
    lStatVFS::eval( )
    {
        std::ostringstream name;

        name << "SystemCall: statvfs( " << m_filename << ")";

        taskName( name.str( ).c_str( ) );
        return ::statvfs( m_filename.c_str( ), &m_buffer_ts );
    }

    //---------------------------------------------------------------------
    /// Simply record caller's information for future use.
    //---------------------------------------------------------------------
    inline void
    lStatVFS::Reset( const std::string& Filename )
    {
        m_filename = Filename;
    }

    //---------------------------------------------------------------------
    /// Syncronize the callers buffer with the local buffer being
    /// careful to ensure that the caller has specified a buffer
    /// into which the results are to be saved.
    ///
    /// \note
    ///     This call should only be made if Request returns successful.
    ///     It is the responsibility of the developer to ensure this
    ///     condition.
    //---------------------------------------------------------------------
    inline void
    lStatVFS::Sync( buffer_type& StatVFSBuffer )
    {
        StatVFSBuffer = m_buffer_ts;
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    SINGLETON_TS_INST( StatVFSPool );

    StatVFSPool::StatVFSPool( )
    {
        MemChecker::Append( singleton_suicide,
                            "<anonymous>::StatVFSPool::singleton_suicide",
                            5 );
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    SINGLETON_TS_INST( StatVFSTrashPool );

    StatVFSTrashPool::StatVFSTrashPool( )
    {
        MemChecker::Append( singleton_suicide,
                            "<anonymous>::StatVFSTrashPool::singleton_suicide",
                            5 );
    }

} // namespace

namespace GenericAPI
{
    //---------------------------------------------------------------------
    /// This function is a wrapper for the system level statvfs call.
    /// The use of this call adds the additional error condition
    /// of the call timing out.
    /// This is useful when querying NFS servers that are unresponsive.
    //---------------------------------------------------------------------
    int
    StatVFS( const std::string& Filename, lStatVFS::buffer_type& StatVFSBuffer )
    {
        static const char* method_name = "GenericAPI::StatVFS";

        int retval = -1;

        if ( MountPointStatus::Status( Filename ) ==
             MountPointStatus::STATE_OFFLINE )
        {
            //---------------------------------------------------------
            // File system is currently flagged as being offline.
            // Don't waste our time on this blocking system call
            //---------------------------------------------------------
            errno = EINTR;
            return retval;
        }

        //-----------------------------------------------------------
        // Get entry from pool to reduce system allocation overhead
        //-----------------------------------------------------------
        value_type statvfs(
            StatVFSPool::Instance( ).Request( create_statvfs ) );
        int status = TIMEOUT_COMPLETED;

        if ( statvfs )
        {
            statvfs->Reset( Filename );
            try
            {
                //---------------------------------------------------------
                // PR#341
                // Carefully setup to only wait a certain amount of time
                // for this call to complete.
                //---------------------------------------------------------
                Timeout( statvfs, lstat_timeout( ), status );

                //---------------------------------------------------------
                // This buffer is not hung on the system request so
                // recycle it.
                //---------------------------------------------------------
            }
            catch ( const TimeoutException& Exception )
            {
                errno = EINTR;

                std::ostringstream msg;

                msg << "TimeoutException for: " << Filename << " ("
                    << Exception.what( ) << ")";

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    0,
                    method_name,
                    "CXX_INTERFACE" );
                if ( StdErrLog.IsOpen( ) )
                {
                    StdErrLog( ErrorLog::DEBUG,
                               __FILE__,
                               __LINE__,
                               Exception.what( ) );
                }
            }
            catch ( const std::exception& Exception )
            {
                std::ostringstream msg;

                msg << "std::exception for: " << Filename << " ("
                    << Exception.what( ) << ")";

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    0,
                    method_name,
                    "CXX_INTERFACE" );
                if ( StdErrLog.IsOpen( ) )
                {
                    StdErrLog( ErrorLog::DEBUG,
                               __FILE__,
                               __LINE__,
                               Exception.what( ) );
                }
            }
            catch ( ... )
            {
                std::ostringstream msg;

                msg << "unknown exception for: " << Filename;

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    0,
                    method_name,
                    "CXX_INTERFACE" );
                // :TODO: Unknown exception
            }
            //-----------------------------------------------------------------
            // Need to see how the call completed.
            //-----------------------------------------------------------------
            if ( status & TIMEOUT_COMPLETED )
            {
                //---------------------------------------------------------------
                // Transfer results so caller can make use of them.
                //---------------------------------------------------------------
                retval = statvfs->SystemReturnCode( );
                errno = statvfs->SystemErrNo( );
                if ( retval == 0 )
                {
                    statvfs->Sync( StatVFSBuffer );
                }
                //---------------------------------------------------------------
                // Recycle this memory for future use.
                //---------------------------------------------------------------
                StatVFSPool::Instance( ).Relinquish( statvfs );
            }
            else
            {
                //---------------------------------------------------------------
                // Put onto the trash heap to prevent the memory from being
                // recycled and causing memory corruption.
                //---------------------------------------------------------------
                StatVFSTrashPool::Instance( ).Relinquish( statvfs );
            }
        }
        if ( errno == EINTR )
        {
            //---------------------------------------------------------
            // An interupted system call means that the task took too
            // long to complete. Register this file system as being
            // offline.
            //---------------------------------------------------------
            MountPointStatus::Offline( Filename );
        }
        return retval;
    }

} // namespace GenericAPI
