//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <fcntl.h>
#include <netdb.h>
#include <unistd.h>

#include <cctype>
#include <cstdio>
#include <ctime>

#include <algorithm>

#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/System.hh"

#include "genericAPI/LogHTML.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/Stat.hh"
#include "genericAPI/LDASplatform.hh"
#if 0
#include "genericAPI/TclSymbolTable.hh"
#endif /* 0 */

#ifndef AI_NUMERICSERV
#define AI_NUMERICSERV AI_NUMERICHOST
#endif
#ifndef AI_ADDRCONFIG
#define AI_ADDRCONFIG 0
#endif

using LDASTools::AL::MutexLock;
using LDASTools::AL::unordered_map;
using LDASTools::System::ErrnoMessage;

using namespace GenericAPI;
using GenericAPI::Log::HTML;
using LDASTools::AL::GPSTime;

typedef unordered_map< std::string, const char* > site_container_type;

#if DEPRICATED
static const pthread_t main_thread = pthread_self( );
#endif /* DEPRICATED */

//-----------------------------------------------------------------------
/// \brief Format gif image appropriate for log entry
//-----------------------------------------------------------------------
static const std::string gif_html( HTML::message_type            MT,
                                   const LDASTools::AL::GPSTime& TimeStamp );
//-----------------------------------------------------------------------
/// \brief Format gif information for legend section of web pages.
//-----------------------------------------------------------------------
static const std::string gif_legend( HTML::message_type MT );

#if DEPRICATED
typedef struct addrinfo* addr_info_type;

/// \cond ignore_legacy_ldas
static void           getaddrinfo_for_manager( );
static addr_info_type manager_addr_info = (addr_info_type)NULL;
static void           manager_command( const std::string& Command );
static int            manager_connect( );
/// \endcond
#endif /* DEPRICATED */

namespace GenericAPI
{
    namespace Log
    {
        HTML::HTML( const std::string& BaseName )
            : ::GenericAPI::Log::LDAS( BaseName )
        {
        }

        HTML*
        HTML::Clone( const std::string& BaseName ) const
        {
            return new HTML( BaseName );
        }

        const char*
        HTML::FileExtension( ) const
        {
            static const char* extension = ".html";

            return extension;
        }

        std::string
        HTML::FormatJobInfo( const std::string& JobInfo ) const
        {
            std::ostringstream retval;

            try
            {
                //-----------------------------------------------------------------
                // Create hyperlinks to job files.
                //-----------------------------------------------------------------
                const std::string run_code( LDASplatform::RunCode( ) );

                if ( ( run_code.length( ) > 0 ) &&
                     ( JobInfo.compare( 0, run_code.length( ), run_code ) ==
                       0 ) )
                {
                    int                jobid;
                    std::istringstream jobid_stream(
                        JobInfo.substr( run_code.length( ) ) );

                    jobid_stream >> jobid;
                    if ( jobid_stream.eof( ) )
                    {
                        //-------------------------------------------------------------
                        // It matches the regex ^{run_code}\d+$
                        //-------------------------------------------------------------
                        retval << "<a href=\"" << LDASplatform::HTTP_URL( )
                               << "/" << run_code << "_" << ( jobid / 10000 )
                               << "/" << JobInfo << "/user_command.html\">"
                               << "<b><i>" << JobInfo << "</i></b></a>";
                    }
                }
            }
            catch ( ... )
            {
                retval.str( "" );
            }
            if ( retval.str( ).length( ) == 0 )
            {
                retval << "<b><i><font color=\"green\">" << JobInfo
                       << "</font></i></b>";
            }

            return retval.str( );
        }

        void
        HTML::Message( message_type       MessageType,
                       level_type         Level,
                       const std::string& Caller,
                       const std::string& JobInfo,
                       const std::string& Message )
        {
#if DEPRICATED
            getaddrinfo_for_manager( );
#endif /* DEPRICATED */
            //-------------------------------------------------------------------
            // Verify that the message should be logged
            //-------------------------------------------------------------------
            //    if ( ShouldLog( MessageType, Level ) )
            {
                //-----------------------------------------------------------------
                // OK, this message should be logged.
                // Format the message for logging and then get it into the queue
                //-----------------------------------------------------------------
                std::ostringstream     msg;
                LDASTools::AL::GPSTime t;

                t.Now( );

                msg
                    //---------------------------------------------------------------
                    // GIF image
                    //---------------------------------------------------------------
                    << gif_html( MessageType, t )
                    //---------------------------------------------------------------
                    // Time stamp
                    //---------------------------------------------------------------
                    << " <b>" << t.GetSeconds( ) << "</b>";
                if ( *( JobInfo.begin( ) ) == '<' )
                {
                    //---------------------------------------------------------------
                    // JobInfo has already been html formatted
                    //---------------------------------------------------------------
                    msg << " " << JobInfo;
                }
                else
                {
                    //---------------------------------------------------------------
                    // JobInfo has not been formatted for HTML.
                    // Do only the very basic
                    //---------------------------------------------------------------
                    msg << " <b><i><font color=\"green\">" << JobInfo
                        << "</font></i></b>";
                }
                //-----------------------------------------------------------------
                msg
                    //---------------------------------------------------------------
                    // Caller
                    //---------------------------------------------------------------
                    << " <b><i><font color=\"red\">" << Caller
                    << "</font></i></b>"
                    //---------------------------------------------------------------
                    // Body of message
                    //---------------------------------------------------------------
                    << " <tt>";
                if ( Message.length( ) == 0 )
                {
                    msg << "no message passed for logging";
                }
                else
                {
                    //---------------------------------------------------------------
                    // Need to have temporary copy for mutations along the way
                    //---------------------------------------------------------------
                    std::string body( Message );
                    // std::string	tmp_body;
                    //---------------------------------------------------------------
                    // As the final step, cleanup of whitespaces and non
                    // printable characters within body.
                    //---------------------------------------------------------------
                    bool ws = false;

                    for ( std::string::const_iterator cur = body.begin( ),
                                                      last = body.end( );
                          cur != last;
                          ++cur )
                    {
                        if ( isspace( *cur ) && ( ws == false ) )
                        {
                            msg << " ";
                            ws = true;
                            continue;
                        }
                        else if ( !isprint( *cur ) )
                        {
                            msg << ".";
                        }
                        else
                        {
                            msg << *cur;
                        }
                        ws = false;
                    }
                    msg << "</tt>"
                        << "<br>" << std::endl;
                }
                //-----------------------------------------------------------------
                // Send the message to the queue
                //-----------------------------------------------------------------
                Log::Message( MessageType, Level, msg.str( ) );
            }
        }

#if DEPRICATEE
        /// \cond ignore_legacy_ldas
        void
        HTML::TclInitialization( Tcl_Interp* Interp )
        {
            getaddrinfo_for_manager( );
        }
        /// \endcond
#endif /* DEPRICATED */

        void
        HTML::html_header( )
        {
            //-------------------------------------------------------------------
            // Gather information
            //-------------------------------------------------------------------
            const char* host =
                ( getenv( "HOST" ) ) ? getenv( "HOST" ) : "localhost";

            //-------------------------------------------------------------------
            //
            //-------------------------------------------------------------------
            std::string site( siteInfoLookup( LDASplatform::LDASSystem( ) ) );
            std::string special_log_header( "" );

            std::string title;

            title = "The ";
            title += LDASplatform::AppName( );
            title += " API on ";
            title += host;
            title += " at ";
            title += site;

            std::ostringstream preamble;
            preamble << "<HTML> "
                     << "<HEAD> "
                     << "<TITLE>" << title << "</TITLE> "
                     << "<BODY BGCOLOR=\"#DDDDDD\" TEXT=\"#000000\"> "
                     << "<h3>" << title << "</h3> "
                     << "<b><i>Legend:</i></b><br> "
                     << gif_legend( HTML::MT_GREEN ) << "<br> "
                     << gif_legend( HTML::MT_YELLOW ) << "<br> "
                     << gif_legend( HTML::MT_ORANGE ) << "<br> "
                     << gif_legend( HTML::MT_RED ) << "<br> "
                     << gif_legend( HTML::MT_BLUE ) << "<br> "
                     << gif_legend( HTML::MT_PURPLE ) << "<br> "
                     << gif_legend( HTML::MT_MAIL ) << "<br> "
                     << gif_legend( HTML::MT_PHONE ) << "<p>"
                     << special_log_header << "<p><b><i>Link:</i></b> "
                     << "<a href=\"APIstatus.html\">API Status Page for "
                     << site << "</a> "
                     << "<p>" << std::endl;
            writeDirect( preamble.str( ) );
        }

        void
        HTML::html_trailer( )
        {
            std::ostringstream postamble;

            postamble << "</BODY> </HTML>" << std::endl;
            writeDirect( postamble.str( ) );
        }
    } // namespace Log
} // namespace GenericAPI

//=======================================================================
// Local members
//=======================================================================
struct gif_info
{
    enum alt_type
    {
        ALT_TIME,
        ALT_EMAIL
    };

    const char*    s_name;
    const char*    s_alt_desc;
    const char*    s_description;
    const int      s_width;
    const int      s_height;
    const alt_type s_alt_info;
};

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
const gif_info&
gif_lookup( HTML::message_type MT )
{
    //---------------------------------------------------------------------
    //
    //---------------------------------------------------------------------
    typedef unordered_map< int, const gif_info* > gif_container_type;
    static gif_container_type                     gifs;

    static bool initialized = false;
    if ( initialized == false )
    {
        static MutexLock::baton_type gif_baton;

        MutexLock l( gif_baton, __FILE__, __LINE__ );
        if ( initialized == false )
        {
            static const gif_info gif_info_table[] = {
                //---------------------------------------------------------------
                // 0
                //---------------------------------------------------------------
                { "ball_green.gif",
                  "green ball",
                  "Normal status or debugging message",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 1
                //---------------------------------------------------------------
                { "ball_yellow.gif",
                  "yellow ball",
                  "Notable condition which may be a non-fatal error",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 2
                //---------------------------------------------------------------
                { "ball_red.gif",
                  "red ball",
                  "Error condition which fatal to job",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 3
                //---------------------------------------------------------------
                { "mail.gif",
                  "email",
                  "Condition requires email notification of the responsible "
                  "administrator of this API",
                  27,
                  16,
                  gif_info::ALT_EMAIL },
                //---------------------------------------------------------------
                // 4
                //---------------------------------------------------------------
                { "mail.gif",
                  "email",
                  "Condition requires email notification of the responsible "
                  "administrator of this API",
                  27,
                  16,
                  gif_info::ALT_EMAIL },
                //---------------------------------------------------------------
                // 5
                //---------------------------------------------------------------
                { "telephone.gif",
                  "telephone",
                  "Condition requires phone notification of the responsible "
                  "administrator of this API",
                  27,
                  27,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 6
                //---------------------------------------------------------------
                { "ball_blue.gif",
                  "blue ball",
                  "Notable condition which is not an error",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 7
                //---------------------------------------------------------------
                { "ball_purple.gif",
                  "purple ball",
                  "Currently undefined",
                  14,
                  12,
                  gif_info::ALT_TIME },
                //---------------------------------------------------------------
                // 8
                //---------------------------------------------------------------
                { "ball_orange.gif",
                  "orange ball",
                  "Error condition not fatal to job",
                  14,
                  12,
                  gif_info::ALT_TIME }
            };

            gifs[ HTML::MT_OK ] = &gif_info_table[ HTML::MT_OK ];
            gifs[ HTML::MT_WARN ] = &gif_info_table[ HTML::MT_WARN ];
            gifs[ HTML::MT_ERROR ] = &gif_info_table[ HTML::MT_ERROR ];
            gifs[ HTML::MT_EMAIL ] = &gif_info_table[ HTML::MT_EMAIL ];
            gifs[ HTML::MT_PHONE ] = &gif_info_table[ HTML::MT_PHONE ];
            gifs[ HTML::MT_DEBUG ] = &gif_info_table[ HTML::MT_DEBUG ];
            gifs[ HTML::MT_NOTE ] = &gif_info_table[ HTML::MT_NOTE ];
            gifs[ HTML::MT_ORANGE ] = &gif_info_table[ HTML::MT_ORANGE ];

            initialized = true;
        }
    }
    gif_container_type::iterator i = gifs.find( MT );
    if ( i == gifs.end( ) )
    {
        throw std::range_error( "No gif info" );
    }
    return *( i->second );
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
static const std::string
gif_legend( HTML::message_type MT )
{
    std::ostringstream desc;

    try
    {
        const gif_info& gi = gif_lookup( MT );

        desc << "<img src=\"" << gi.s_name << "\""
             << " width=\"" << gi.s_width << "\" height=\"" << gi.s_height
             << "\""
             << " alt=\"" << gi.s_alt_desc << "\" title=\"" << gi.s_alt_desc
             << "\""
             << "> " << gi.s_description;
    }
    catch ( ... )
    {
    }

    return desc.str( );
}

static const std::string
gif_html( HTML::message_type MT, const LDASTools::AL::GPSTime& TimeStamp )
{
    std::ostringstream gif;
    try
    {
        const gif_info& gi = gif_lookup( MT );

        std::ostringstream alt;

        switch ( gi.s_alt_info )
        {
        case gif_info::ALT_EMAIL:
        {
            alt << LoggingInfo::EMailNotify( LDASplatform::AppName( ) );
        }
        break;
        case gif_info::ALT_TIME:
            // Fall through to default case
        default:
            alt << Log::LDAS::FormatTime( Log::LDAS::TF_LOCAL, TimeStamp )
                << " &#013;"
                << Log::LDAS::FormatTime( Log::LDAS::TF_GMT, TimeStamp );
            break;
        };

        gif << "<img src=\"" << gi.s_name << "\""
            << " width=\"" << gi.s_width << "\" height=\"" << gi.s_height
            << "\""
            << " alt=\"" << alt.str( ) << "\" title=\"" << alt.str( ) << "\""
            << ">";
    }
    catch ( ... )
    {
    }

    return gif.str( );
}

#if DEPRICATED
/// \cond ignore_legacy_ldas
static void
manager_command( const std::string& Command )
{
    using namespace GenericAPI;
    using namespace GenericAPI::Symbols;

    LDAS_MANAGER_KEY::value_type manager_key;

    LDAS_MANAGER_KEY::Get( manager_key );

    if ( manager_key.empty( ) )
    {
        return;
    }
    //---------------------------------------------------------------------
    // Establish a connection with the manager
    //---------------------------------------------------------------------

    int s = manager_connect( );

    if ( s < 0 )
    {
        return;
    }
    try
    {
        //-------------------------------------------------------------------
        // Format and send the request
        //-------------------------------------------------------------------
        std::ostringstream request;

        request << manager_key << " NULL NULL"
                << " eval " << Command << std::endl;
        int b = ::write( s, request.str( ).c_str( ), request.str( ).length( ) );
        //-------------------------------------------------------------------
        // Read the responce
        //-------------------------------------------------------------------
        static const int RESULT_BUFFER_SIZE = 1024;
        char             result[ RESULT_BUFFER_SIZE ];

        while ( ( b = ::read( s, &result[ 0 ], RESULT_BUFFER_SIZE - 1 ) ) ==
                ( RESULT_BUFFER_SIZE - 1 ) )
        {
            //-----------------------------------------------------------------
            // Loop back over to read more from the manager.
            //-----------------------------------------------------------------
        }
        //-------------------------------------------------------------------
        // Close the connection
        //-------------------------------------------------------------------
        close( s );
    }
    catch ( ... )
    {
        //-------------------------------------------------------------------
        // Something bad has happened.
        // Do some cleanup
        //-------------------------------------------------------------------
        if ( s >= 0 )
        {
            close( s );
        }
        //-------------------------------------------------------------------
        // Continue with error
        //-------------------------------------------------------------------
        throw;
    }
}

static int
manager_connect( )
{
    int s = -1;

    getaddrinfo_for_manager( );
    if ( manager_addr_info != (addr_info_type)NULL )
    {
        for ( struct addrinfo* cur = manager_addr_info;
              cur != (struct addrinfo*)NULL;
              cur = cur->ai_next )
        {
            s = ::socket( cur->ai_family, cur->ai_socktype, cur->ai_protocol );
            if ( s < 0 )
            {
                continue;
            }

            if ( ::connect( s, cur->ai_addr, cur->ai_addrlen ) < 0 )
            {
                close( s );
                s = -1;
                continue;
            }

            break; /* okay we got one */
        }
    }

    return s;
}

//-----------------------------------------------------------------------
/// Retrieve the address information that is needed to connect to the
/// manager's emergency port.
///
/// \todo
///     Make this function callable outside of the main thread.
///     It has been noted not to work outside of the main thread
///     on CentOS 5.2.
//-----------------------------------------------------------------------
static void
getaddrinfo_for_manager( )
{
    using namespace GenericAPI;
    using namespace GenericAPI::Symbols;

    if ( manager_addr_info == (addr_info_type)NULL )
    {
        static MutexLock::baton_type baton;

        MutexLock l( baton, __FILE__, __LINE__ );
        if ( manager_addr_info != (addr_info_type)NULL )
        {
            // Has been set by another thread.
            return;
        }

        LDAS_MANAGER_PORT_EMERGENCY::value_type manager_port;
        LDAS_MANAGER_HOST::value_type           manager_host;

        LDAS_MANAGER_PORT_EMERGENCY::Get( manager_port );
        LDAS_MANAGER_HOST::Get( manager_host );

        if ( manager_host.empty( ) || ( manager_port == 0 ) )
        {
            return;
        }

        std::ostringstream manager_port_string;
        manager_port_string << manager_port;

        //-------------------------------------------------------------------
        // Establish a connection with the manager
        //-------------------------------------------------------------------

        addr_info_type  manager;
        struct addrinfo hints;

        ::memset( &hints, 0, sizeof( hints ) );
        hints.ai_flags = ( AI_NUMERICSERV | AI_ADDRCONFIG );
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;

        manager_addr_info = (addr_info_type)NULL;
        if ( ::getaddrinfo( manager_host.c_str( ),
                            manager_port_string.str( ).c_str( ),
                            &hints,
                            &manager ) == 0 )
        {
            manager_addr_info = manager;
        }
    }
}
/// \endcond
#endif /* DEPRICATED */
