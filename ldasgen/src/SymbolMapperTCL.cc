//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <tcl.h>

#include <iostream>
#include <list>
#include <sstream>

#include "ldastoolsal/types.hh"

#include "genericAPI/SymbolMapperTCL.hh"

namespace GenericAPI
{
    namespace TCL
    {
        SymbolMapper::SymbolMapper( Tcl_Interp* Interp ) : m_interp( Interp )
        {
        }

        SymbolMapper::~SymbolMapper( )
        {
        }

        void
        SymbolMapper::Create( Tcl_Interp* Interp )
        {
            if ( m_symbol_mapper == (GenericAPI::SymbolMapper*)NULL )
            {
                m_symbol_mapper = new SymbolMapper( Interp );
            }
        }

        void
        SymbolMapper::addSymbol( const std::string& Symbol,
                                 arg_type           Arg,
                                 func_type          Function )
        {
            static int flags = TCL_GLOBAL_ONLY | TCL_TRACE_WRITES |
                TCL_TRACE_UNSETS | TCL_TRACE_DESTROYED;

            std::string name1( Symbol );
            std::string name2;
            const char* n2;

            if ( name2.empty( ) )
            {
                n2 = (char*)NULL;
            }
            else
            {
                n2 = name2.c_str( );
            }
            //-----------------------------------------------------------------
            // Lookup the variable to see if it currently has a value.
            //-----------------------------------------------------------------
            Tcl_Obj* value = Tcl_GetVar2Ex( interp( ),
                                            name1.c_str( ),
                                            n2,
                                            ( flags & ( TCL_GLOBAL_ONLY ) ) );
            if ( value )
            {
                change_callback( (ClientData)this,
                                 interp( ),
                                 name1.c_str( ),
                                 n2,
                                 TCL_TRACE_WRITES );
            }
            //-----------------------------------------------------------------
            // Check if the variable is already being traced.
            //-----------------------------------------------------------------
            if ( Tcl_VarTraceInfo2( interp( ),
                                    name1.c_str( ),
                                    n2,
                                    flags,
                                    SymbolMapper::change_callback,
                                    (ClientData)this ) == (ClientData)NULL )
            {
                //---------------------------------------------------------------
                // Register callback for changes to the state of the variable
                //---------------------------------------------------------------
                Tcl_TraceVar2( interp( ),
                               name1.c_str( ),
                               n2,
                               flags,
                               SymbolMapper::change_callback,
                               (ClientData)this );
            }
        }

        char*
        SymbolMapper::change_callback( ClientData    CD,
                                       Tcl_Interp*   Interp,
                                       CONST84 char* Name1,
                                       CONST84 char* Name2,
                                       int           Flags )
        {
            std::ostringstream             var_name;
            GenericAPI::TCL::SymbolMapper* sm =
                reinterpret_cast< GenericAPI::TCL::SymbolMapper* >( CD );

            if ( sm == NULL )
            {
                return (char*)NULL;
            }

            var_name << Name1;
            if ( Name2 )
            {
                var_name << "(" << Name2 << ")";
            }

            if ( Flags & TCL_TRACE_WRITES )
            {
                //-----------------------------------------------------------------
                // The variable has changed its value.
                // Need to update the value in the symbol table.
                //-----------------------------------------------------------------
                const info* i = sm->lookup( var_name.str( ) );
                Tcl_Interp* interp = sm->interp( );

                if ( i )
                {
                    Tcl_Obj* value( Tcl_GetVar2Ex(
                        interp, Name1, Name2, Flags & TCL_GLOBAL_ONLY ) );

                    switch ( i->s_arg_type )
                    {
                    case ARGS_I:
                    {
                        typedef void ( *f_type )( INT_4U );
                        int    tcl_v;
                        INT_4U v;

                        Tcl_GetIntFromObj( interp, value, &tcl_v );
                        v = tcl_v;
                        ( *( (f_type)i->s_function ) )( v );
                    }
                    break;
                    case ARGS_S:
                    {
                        typedef void ( *f_type )( const std::string& );
                        std::string v;

                        v = Tcl_GetString( value );
                        ( *( (f_type)i->s_function ) )( v );
                    }
                    break;
                    case ARGS_LS:
                    {
                        typedef std::list< std::string > param_type;
                        typedef void ( *f_type )( const param_type& );
                        param_type v;
                        Tcl_Obj**  listobjv = (Tcl_Obj**)NULL;
                        int        nitems = 0;

                        if ( Tcl_ListObjGetElements(
                                 interp, value, &nitems, &listobjv ) !=
                             TCL_ERROR )
                        {
                            for ( int offset = 0; offset < nitems; offset++ )
                            {
                                v.push_back( Tcl_GetStringFromObj(
                                    listobjv[ offset ], 0 ) );
                            }
                            ( *( (f_type)i->s_function ) )( v );
                        }
                    }
                    break;
                    case ARGS_NS:
                    {
                        typedef void ( *f_type )( const std::string&,
                                                  const std::string& );
                        std::string v;

                        v = Tcl_GetString( value );
                        ( *( (f_type)i->s_function ) )( var_name.str( ), v );
                    }
                    break;
                    default:
                        break;
                    }
                }
            }
#if 0
      else if ( ( Flags & ( TCL_TRACE_UNSETS | TCL_TRACE_DESTROYED ) ) )
      {
	//-----------------------------------------------------------------
	// The variable is 
	//-----------------------------------------------------------------
	Unset( v.str( ) );
      }
#endif /* 0 */
            return (char*)NULL;
        }
    } // namespace TCL
} // namespace GenericAPI
