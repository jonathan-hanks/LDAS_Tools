//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "genericAPI/LogLDAS.hh"

extern "C" {
#include <sys/stat.h>
} // extern "C"

#include <fcntl.h>
#include <fstream>
#include <unistd.h>

#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/System.hh"

#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/Stat.hh"

using LDASTools::AL::MutexLock;
using LDASTools::AL::unordered_map;
using LDASTools::System::ErrnoMessage;

using LDASTools::AL::GPSTime;

typedef unordered_map< std::string, const char* > site_container_type;

static site_container_type& site_info( );

namespace GenericAPI
{
    namespace Log
    {
        std::string
        LDAS::FormatTime( time_format                   Format,
                          const LDASTools::AL::GPSTime& Time )
        {

            std::string retval;
            time_t      t = Time.GetSeconds( GPSTime::UTC );
            struct tm   ti;
            bool        success = false;

            const char* fmt = (const char*)NULL;

            switch ( Format )
            {
            case TF_GMT:
            {
                if ( ( success = ( gmtime_r( &t, &ti ) == &ti ) ) )
                {
                    fmt = "%x-%X GMT";
                }
            }
            break;
            case TF_LOCAL:
            {
                if ( ( success = ( localtime_r( &t, &ti ) == &ti ) ) )
                {
                    fmt = "%x-%X %Z";
                }
            }
            break;
            }

            if ( success )
            {

                char buffer[ 256 ];

                if ( strftime( buffer, sizeof( buffer ), fmt, &ti ) )
                {
                    retval = buffer;
                }
            }
            return retval;
        }

        std::string
        LDAS::LogFilename( const char* Extension, int Version )
        {
            const std::string ldas_log_dir( LoggingInfo::LogDirectory( ) );

            std::ostringstream filename;

            if ( ldas_log_dir.compare( "-" ) == 0 )
            {
                filename << ldas_log_dir;
            }
            else
            {
                filename << ldas_log_dir << "/LDAS" << LDASplatform::AppName( )
                         << ".log" << Extension;
            }

            return filename.str( );
        }

        void
        LDAS::onStreamClose( )
        {
            footer( );
        }

        void
        LDAS::onStreamOpen( )
        {
            header( );
        }

        void
        LDAS::header( )
        {
        }

        void
        LDAS::footer( )
        {
        }

        //---------------------------------------------------------------------
        /// Rotate the logs according to the LDAS rules
        //---------------------------------------------------------------------
#if 0
    void LDAS::
    rotate( )
    {
      LDASTools::AL::Log::
    } // rotate
#endif /* 0 */

        std::string
        LDAS::siteInfoLookup( const std::string& LDASSystemName )
        {
            size_t pos = LDASSystemName.find_last_of( "-" );
            if ( pos != std::string::npos )
            {
                site_container_type::const_iterator p =
                    ::site_info( ).find( LDASSystemName.substr( pos + 1 ) );
                if ( p != ::site_info( ).end( ) )
                {
                    return p->second;
                }
            }
            return LDASSystemName;
        }

    } // namespace Log
} // namespace GenericAPI

static site_container_type&
site_info( )
{
    static site_container_type site_container;

    if ( site_container.size( ) == 0 )
    {
        //-------------------------------------------------------------------
        // Make thread safe
        //-------------------------------------------------------------------
        static MutexLock::baton_type site_baton;

        MutexLock l( site_baton, __FILE__, __LINE__ );
        if ( site_container.size( ) == 0 )
        {
            site_container[ "wa" ] = "Hanford";
            site_container[ "la" ] = "Livingston";
            site_container[ "mit" ] = "LDAS_MIT";
            site_container[ "dev" ] = "Caltech-Dev";
            site_container[ "test" ] = "Caltech-Test";
            site_container[ "cit" ] = "Caltech-CIT";
            site_container[ "uwm" ] = "LDAS_UWM";
        }
    }
    return site_container;
}
