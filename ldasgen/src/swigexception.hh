#ifndef SwigExceptionHH
#define SwigExceptionHH

extern "C" {
#include <strings.h>
#include <stdio.h>
}

#include <sstream>

#include "ldastoolsal/ldasexception.hh"

//-----------------------------------------------------------------------------
//
//: SWIG Exception
//
// This class implements an exception for SWIG.  In the *API.swig files, there
// is a typemap which handles converting objects of this type into an exception
// for TCL.  TCL exceptions have two components, the message and the
// stack-trace.  The message is returned immediately (at least in the tclsh)
// while the stack-trace is obtained via the errorInfo variable.
//
// <p>This class has two data members: mResult, which is the message; and
// mInfo, which is put into the stack trace.
//
// <p>This class has a number of overloaded constructors in order to convert
// common exceptions into a SwigException.
//
class SwigException
{
public:
    /* Constructors */
    inline SwigException( const char* result, const char* file, int line )
        : mResult( result ), mInfo( location( file, line ) )
    {
    }
    inline SwigException( const char* result ) : mResult( result ), mInfo( )
    {
    }
    SwigException( const std::string& result, const char* file, int line )
        : mResult( result ), mInfo( location( file, line ) )
    {
    }
    explicit SwigException( const std::string& result,
                            const std::string& info = "" )
        : mResult( result ), mInfo( info )
    {
    }
    explicit SwigException( const std::string& result,
                            const std::string& info,
                            const char*        file,
                            int                line )
        : mResult( result ), mInfo( info + location( file, line ) )
    {
    }
    SwigException( const SwigException& e )
        : mResult( e.mResult ), mInfo( e.mInfo )
    {
    }
    SwigException( const std::bad_alloc& e, const char* file, int line );
    SwigException( const std::bad_alloc& e );
    SwigException( const LdasException& e );
    SwigException( const std::exception& e, const char* file, int line );
    SwigException( const std::exception& e );

    /* Accessors */
    const std::string&
    getResult( ) const
    {
        return mResult;
    }
    const std::string&
    getInfo( ) const
    {
        return mInfo;
    }
    static std::string os_error( void );

protected:
    /* Data Members */
    static std::string location( const char* file, int line );
    std::string        mResult;
    std::string        mInfo;
};

//-----------------------------------------------------------------------------
//
//: SwigException Constructor Macro
//
// This macro creates a SwigException object setting the file and line to
// correspond to the point in the file where the macro is used.
//
//! param: a
//
#if defined __GNUC__ && defined __sun__ && 3 == __GNUC__ && 2 == __GNUC_MINOR__
#define SWIGEXCEPTION( a ) SwigException( a )
#else
#define SWIGEXCEPTION( a ) SwigException( a, __FILE__, __LINE__ )
#endif

//-----------------------------------------------------------------------------
//
//: Constructor
//
// Convert a standard std::bad_alloc exception.
//
//! param: const std::bad_alloc& e
//! param: const char* file
//! param: int line
//
inline SwigException::SwigException( const std::bad_alloc& e,
                                     const char*           file,
                                     int                   line )
    : mResult( "std::bad_alloc:" ), mInfo( location( file, line ) )
{
}

//-----------------------------------------------------------------------------
//
//: Constructor
//
// Convert a standard std::bad_alloc exception.
//
//! param: const std::bad_alloc& e
//! param: const char* file
//! param: int line
//
inline SwigException::SwigException( const std::bad_alloc& e )
    : mResult( "std::bad_alloc" ), mInfo( )
{
}

//-----------------------------------------------------------------------------
//
//: Constructor
//
// Convert a standard exception.
//
//! param: const std::exception& e
//! param: const char* file
//! param: int line
//
inline SwigException::SwigException( const std::exception& e,
                                     const char*           file,
                                     int                   line )
    : mResult( "exception: " ), mInfo( e.what( ) + location( file, line ) )
{
}

//-----------------------------------------------------------------------------
//
//: Constructor
//
// Convert a standard exception.
//
//! param: const std::exception& e
//! param: const char* file
//! param: int line
//
inline SwigException::SwigException( const std::exception& e )
    : mResult( "exception: " ), mInfo( )
{
}

/*------------------*/
/* ErrAcceptFailure */
/*------------------*/

class ErrAcceptFailure : public SwigException
{
public:
    ErrAcceptFailure( );
    ErrAcceptFailure( const char* file, int line );
};

inline ErrAcceptFailure::ErrAcceptFailure( ) : SwigException( "accept_failure" )
{
}

inline ErrAcceptFailure::ErrAcceptFailure( const char* file, int line )
    : SwigException( "accept_failure", file, line )
{
}

#if defined __GNUC__ && defined __sun__ && 3 == __GNUC__ && 2 == __GNUC_MINOR__
#define ERRACCEPTFAILURE( ) ErrAcceptFailure( )
#else
#define ERRACCEPTFAILURE( ) ErrAcceptFailure( __FILE__, __LINE__ )
#endif

/*--------------------*/
/* ErrIdentifyFailure */
/*--------------------*/

class ErrIdentifyFailure : public SwigException
{
public:
    ErrIdentifyFailure( );
    ErrIdentifyFailure( const char* file, int line );
};

inline ErrIdentifyFailure::ErrIdentifyFailure( )
    : SwigException( "identify_failure" )
{
}

inline ErrIdentifyFailure::ErrIdentifyFailure( const char* file, int line )
    : SwigException( "identify_failure", file, line )
{
}

#if defined __GNUC__ && defined __sun__ && 3 == __GNUC__ && 2 == __GNUC_MINOR__
#define ERRIDENTIFYFAILURE( ) ErrIdentifyFailure( )
#else
#define ERRIDENTIFYFAILURE( ) ErrIdentifyFailure( __FILE__, __LINE__ )
#endif

/*------------------*/
/* ErrUnboundSocket */
/*------------------*/

class ErrUnboundSocket : public SwigException
{
public:
    ErrUnboundSocket( );
    ErrUnboundSocket( const char* file, int line );
};

inline ErrUnboundSocket::ErrUnboundSocket( ) : SwigException( "unbound_socket" )
{
}

inline ErrUnboundSocket::ErrUnboundSocket( const char* file, int line )
    : SwigException( "unbound_socket", file, line )
{
}

#if defined __GNUC__ && defined __sun__ && 3 == __GNUC__ && 2 == __GNUC_MINOR__
#define ERRUNBOUNDSOCKET( ) ErrUnboundSocket( )
#else
#define ERRUNBOUNDSOCKET( ) ErrUnboundSocket( __FILE__, __LINE__ )
#endif

/*----------------------*/
/* ErrUnconnectedSocket */
/*----------------------*/

class ErrUnconnectedSocket : public SwigException
{
public:
    ErrUnconnectedSocket( );
    ErrUnconnectedSocket( const char* file, int line );
};

inline ErrUnconnectedSocket::ErrUnconnectedSocket( )
    : SwigException( "unconnected_socket" )
{
}

inline ErrUnconnectedSocket::ErrUnconnectedSocket( const char* file, int line )
    : SwigException( "unconnected_socket", file, line )
{
}

#if defined __GNUC__ && defined __sun__ && 3 == __GNUC__ && 2 == __GNUC_MINOR__
#define ERRUNCONNECTEDSOCKET( ) ErrUnconnectedSocket( )
#else
#define ERRUNCONNECTEDSOCKET( ) ErrUnconnectedSocket( __FILE__, __LINE__ )
#endif

/*------------------*/
/* ErrInvalidSocket */
/*------------------*/

class ErrInvalidSocket : public SwigException
{
public:
    ErrInvalidSocket( );
    ErrInvalidSocket( const char* file, int line );
};

inline ErrInvalidSocket::ErrInvalidSocket( ) : SwigException( "invalid_socket" )
{
}

inline ErrInvalidSocket::ErrInvalidSocket( const char* file, int line )
    : SwigException( "invalid_socket", file, line )
{
}

#if defined __GNUC__ && defined __sun__ && 3 == __GNUC__ && 2 == __GNUC_MINOR__
#define ERRINVALIDSOCKET( ) ErrInvalidSocket( )
#else
#define ERRINVALIDSOCKET( ) ErrInvalidSocket( __FILE__, __LINE__ )
#endif

/*------------------*/
/* ErrInvalidServer */
/*------------------*/

class ErrInvalidServer : public SwigException
{
public:
    ErrInvalidServer( );
    ErrInvalidServer( const char* file, int line );
};

inline ErrInvalidServer::ErrInvalidServer( ) : SwigException( "invalid_server" )
{
}

inline ErrInvalidServer::ErrInvalidServer( const char* file, int line )
    : SwigException( "invalid_server", file, line )
{
}

#if defined __GNUC__ && defined __sun__ && 3 == __GNUC__ && 2 == __GNUC_MINOR__
#define ERRINVALIDSERVER( ) ErrInvalidServer( )
#else
#define ERRINVALIDSERVER( ) ErrInvalidServer( __FILE__, __LINE__ )
#endif

#define CATCHALLSWIG( )                                                        \
    catch ( SwigException & lm_e )                                             \
    {                                                                          \
        throw;                                                                 \
    }                                                                          \
    catch ( os_network_toolkit_error & lm_e )                                  \
    {                                                                          \
        throw OSPACEEXCEPTION( lm_e );                                         \
    }                                                                          \
    catch ( ... )                                                              \
    {                                                                          \
        throw SWIGEXCEPTION( "unexpected_exception" );                         \
    }

#define CATCHSWIGUNEXPECTED( )                                                 \
    catch ( ... )                                                              \
    {                                                                          \
        throw SWIGEXCEPTION( "unexpected_exception" );                         \
    }

#endif // SwigExceptionHH
