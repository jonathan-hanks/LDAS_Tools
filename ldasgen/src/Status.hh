//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef LDAS_TOOLS__LDAS_GEN__STATUS_HH
#define LDAS_TOOLS__LDAS_GEN__STATUS_HH

#include <list>
#include <map>
#include <queue>
#include <set>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/Singleton.hh"

namespace GenericAPI
{
    namespace Status
    {
        class MonitorInterface;

        //-------------------------------------------------------------------
        /// \brief The class managing the collection of raw data
        //-------------------------------------------------------------------
        class Recorder : public LDASTools::AL::Singleton< Recorder >
        {
        public:
            typedef size_t                                frequency_type;
            typedef size_t                                queue_length_type;
            typedef boost::shared_ptr< MonitorInterface > monitor_type;

            //-----------------------------------------------------------------
            /// \brief Default constructor
            //-----------------------------------------------------------------
            Recorder( );

            //-----------------------------------------------------------------
            /// \brief Add a monitor to the recorder
            ///
            /// \param[in] M
            ///     Reference to the monitor.
            //-----------------------------------------------------------------
            static void Add( monitor_type M );

        private:
            typedef std::set< monitor_type > interface_container_type;
            typedef std::map< frequency_type, interface_container_type >
                frequency_action_type;

            LDASTools::AL::MutexLock::baton_type baton;

            frequency_action_type frequency_actions;

            void add( monitor_type M );
        };

        template < typename DataType >
        class DataQueue : private std::queue< DataType >
        {
        public:
            typedef size_t                 size_type;
            typedef std::queue< DataType > internal_type;

            using std::queue< DataType >::front;
            using std::queue< DataType >::back;

            DataQueue( ) : max_size( 100 )
            {
            }

            DataQueue( size_type MaxSize ) : max_size( MaxSize )
            {
            }

            DataQueue( const DataQueue& Source ) : max_size( Source.max_size )
            {
            }

            void
            push( const DataType& Value )
            {
                internal_type::push( Value );
                if ( internal_type::size( ) > max_size )
                {
                    // Remove the oldest element
                    internal_type::pop( );
                }
            }

        private:
            size_type max_size;
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class MonitorInterface
        {
        public:
            typedef Recorder::frequency_type frequency_type;

            virtual std::list< frequency_type > Frequencies( ) const = 0;

            virtual void operator( )( frequency_type Frequency ) = 0;
        };

        //-------------------------------------------------------------------
        /// \brief The resource being monitored
        ///
        /// This interface class describes the basic information that
        /// needs to provide summary information.
        //-------------------------------------------------------------------
        template < typename DataType >
        class Monitor : public MonitorInterface
        {
        private:
            typedef DataQueue< DataType > data_container_type;
            typedef std::map< frequency_type, data_container_type >
                data_sets_type;

            data_sets_type data;

        public:
            typedef size_t size_type;

            typedef Recorder::frequency_type frequency_type;

            void
            AddDataQueue( frequency_type Frequency, size_type MaxLen )
            {
                data_container_type dq( MaxLen );

                data[ Frequency ] = dq;
            }

            void
            AddDataPoint( frequency_type Frequency, DataType Data )
            {
                typename data_sets_type::iterator d = data.find( Frequency );
                if ( d != data.end( ) )
                {
                    d->second.push( Data );
                }
            }

            virtual std::list< frequency_type >
            Frequencies( ) const
            {
                std::list< frequency_type > retval;

                for ( typename data_sets_type::const_iterator
                          cur = data.begin( ),
                          last = data.end( );
                      cur != last;
                      ++cur )
                {
                    retval.push_back( cur->first );
                }
                return retval;
            }
        };

    } // namespace Status
} // namespace GenericAPI

#endif /* LDAS_TOOLS__LDAS_GEN__STATUS_HH */
