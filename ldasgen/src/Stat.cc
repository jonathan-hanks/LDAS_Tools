//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "ldasgen_config.h"

extern "C" {
#include <string.h>
}

#include <sstream>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/ErrorLog.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/SignalHandler.hh"
#include "ldastoolsal/System.hh"
#include "ldastoolsal/SystemCallTask.hh"
#include "ldastoolsal/TaskThread.hh"
#include "ldastoolsal/Timeout.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/MountPointStatus.hh"
#include "genericAPI/Stat.hh"
#include "genericAPI/StatFork.hh"
#include "genericAPI/StatDirect.hh"
#include "genericAPI/StatPoolT.hh"

using LDASTools::AL::ErrorLog;
using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;
using LDASTools::AL::SignalHandler;
using LDASTools::AL::StdErrLog;
using LDASTools::AL::SystemCallTask;
using LDASTools::AL::Task;
using LDASTools::AL::TaskThread;
using LDASTools::AL::Thread;
using LDASTools::AL::Timeout;
using LDASTools::AL::TIMEOUT_COMPLETED;
using LDASTools::AL::TimeoutException;

using GenericAPI::MountPointStatus;
using GenericAPI::StatDirect;

// Timeout value for lstat
static int lstat_timeout_value = 5;

namespace
{
    static class Wakeup_ : public SignalHandler::Callback
    {
    public:
        typedef SignalHandler::signal_type signal_type;

        virtual void
        SignalCallback( signal_type Signal )
        {
        }
    } wakeup;

} // namespace

namespace
{
    //---------------------------------------------------------------------
    /// \brief Perform stat or lstat system call
    ///
    /// This class wraps the lstat and stat system call.
    /// The benifit of using this call is that the caller is protected
    /// from NFS servers that are hung.
    //---------------------------------------------------------------------
    class lStat : public SystemCallTask
    {
    public:
        typedef struct stat buffer_type;

        //-------------------------------------------------------------------
        /// \brief Specify the type of request.
        //-------------------------------------------------------------------
        enum mode_type
        {
            //-----------------------------------------------------------------
            /// Special case to handle initial condition
            //-----------------------------------------------------------------
            MODE_NONE,
            //-----------------------------------------------------------------
            /// Perform stat()
            //-----------------------------------------------------------------
            MODE_STAT,
            //-----------------------------------------------------------------
            /// Perform lstat()
            //-----------------------------------------------------------------
            MODE_LSTAT
        };

        lStat( );

        virtual ~lStat( );

        //-------------------------------------------------------------------
        /// \brief Action to be done when task completes
        //-------------------------------------------------------------------
        virtual void OnCompletion( int TaskThreadState );

        //-------------------------------------------------------------------
        /// \brief Initialize the instance for use.
        ///
        /// \param[in] Path
        ///     The pathname of any file within the mounted filesystem.
        ///
        /// \param[in] Mode
        ///     Variation of stat call to use.
        //-------------------------------------------------------------------
        void Reset( const std::string& Path, mode_type Mode );

        //-------------------------------------------------------------------
        /// \brief Syncronize the callers buffer with the local buffer.
        //-------------------------------------------------------------------
        void Sync( buffer_type& StatBuffer );

    protected:
        //-------------------------------------------------------------------
        /// \brief Do the stat or lstat system call
        ///
        /// \return
        ///     Upon successful completion, the value zero is returned.
        ///     Upon failure, the value -1 is returned and errno
        ///     is set.
        //-------------------------------------------------------------------
        virtual int eval( );

    private:
        mode_type   m_mode;
        std::string m_path;
        buffer_type m_statbuf;
    };

    typedef boost::shared_ptr< lStat > value_type;

    typedef GenericAPI::StatPoolT< lStat > SysCallStatPool_type;

    int stat_wrapper( const std::string&  Path,
                      lStat::buffer_type& StatBuffer,
                      lStat::mode_type    Mode,
                      const char* const   MethodName );

} // namespace

SINGLETON_INSTANCE_DEFINITION(
    LDASTools::AL::SingletonHolder< SysCallStatPool_type > )

namespace GenericAPI
{
    int
    init_lstat_interupt( )
    {
        static const Task::cancel_method imethod = Thread::CANCEL_ABANDON;
        static const Task::signal_type   interupt =
            SignalHandler::SIGNAL_TERMINATE;
        static bool initialized = false;

        if ( initialized == false )
        {
            static MutexLock::baton_type baton( __FILE__, __LINE__, true );

            MutexLock l( baton, __FILE__, __LINE__ );

            if ( initialized == false )
            {
                if ( ( imethod == Thread::CANCEL_BY_SIGNAL ) &&
                     ( SysCallStatPool_type::Interuptable( ) ) )
                {
                    SignalHandler::Register(
                        &wakeup, interupt ); // Initialize the signal handler
                }
                initialized = true;
            }
        }

        return interupt;
    }

    int
    lstat_timeout( )
    {
        return lstat_timeout_value;
    }

    int
    SetStatTimeout( int Value )
    {
        static const CHAR* const method_name( "GenericAPI::SetStatTimeout" );

        int retval = lstat_timeout_value;

        std::ostringstream msg;

        msg << "Modifying lstat timeout from: " << retval << " to: " << Value;
        queueLogEntry( msg.str( ),
                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                       0,
                       method_name,
                       "VARIABLE" );

        lstat_timeout_value = Value;

        return retval;
    }

    int
    Stat( const std::string& Filename, struct stat& StatBuffer )
    {
        return stat_wrapper(
            Filename, StatBuffer, lStat::MODE_STAT, "GenericAPI::Stat" );
    }

    int
    LStat( const std::string& Filename, struct stat& StatBuffer )
    {
        return stat_wrapper(
            Filename, StatBuffer, lStat::MODE_LSTAT, "GenericAPI::LStat" );
    }
} // namespace GenericAPI

namespace
{
    lStat::lStat( ) : m_mode( MODE_NONE )
    {
    }

    lStat::~lStat( )
    {
    }

    //---------------------------------------------------------------------
    /// This actually does the system call.
    /// The local buffer is used to prevent memory corruuption in the
    /// instances where the system call times out.
    //---------------------------------------------------------------------
    int
    lStat::eval( )
    {
        static const char  caller[] = "anonymous::" __FILE__ "::lStat::eval";
        std::ostringstream name;

        int retcode = -1;

        switch ( m_mode )
        {
        case MODE_STAT:
            name << "SystemCall: stat( " << m_path << ")";

            taskName( name.str( ).c_str( ) );
            retcode = stat( m_path.c_str( ), &m_statbuf );
            if ( retcode != 0 )
            {
                QUEUE_LOG_MESSAGE( "MODE_STAT:"
                                       << " path: " << m_path
                                       << " retcode: " << retcode
                                       << " errno: " << errno << " msg: "
                                       << LDASTools::System::ErrnoMessage( ),
                                   MT_NOTE,
                                   0,
                                   caller,
                                   "CXX" );
            }
            break;
        case MODE_LSTAT:
            name << "SystemCall: lstat( " << m_path << ")";

            taskName( name.str( ).c_str( ) );
            retcode = lstat( m_path.c_str( ), &m_statbuf );
            if ( retcode != 0 )
            {
                QUEUE_LOG_MESSAGE( "MODE_LSTAT:"
                                       << " path: " << m_path
                                       << " retcode: " << retcode
                                       << " errno: " << errno << " msg: "
                                       << LDASTools::System::ErrnoMessage( ),
                                   MT_NOTE,
                                   0,
                                   caller,
                                   "CXX" );
            }
            break;
        default:
            break;
        }
        return retcode;
    }

    //---------------------------------------------------------------------
    /// Actions to be taken depending on the thread state
    //---------------------------------------------------------------------
    void
    lStat::OnCompletion( int TaskThreadState )
    {
        if ( ( TaskThreadState ==
               LDASTools::AL::TaskThread::TASK_THREAD_EXITING ) &&
             ( errno == EINTR ) )
        {
            SysCallStatPool_type::Recycle( this );
        }
    }
    //---------------------------------------------------------------------
    /// Simply record caller's information for future use.
    //---------------------------------------------------------------------
    inline void
    lStat::Reset( const std::string& Path, mode_type Mode )
    {
        m_path = Path;
        m_mode = Mode;
    }

    //---------------------------------------------------------------------
    /// Syncronize the callers buffer with the local buffer being
    /// careful to ensure that the caller has specified a buffer
    /// into which the results are to be saved.
    ///
    /// \note
    ///     This call should only be made if Request returns successful.
    ///     It is the responsibility of the developer to ensure this
    ///     condition.
    //---------------------------------------------------------------------
    inline void
    lStat::Sync( buffer_type& StatBuffer )
    {
        StatBuffer = m_statbuf;
    }

    //---------------------------------------------------------------------
    /// \brief Core to perform timed stat or lstat system calls.
    //---------------------------------------------------------------------
    inline int
    stat_wrapper( const std::string&  Path,
                  lStat::buffer_type& StatBuffer,
                  lStat::mode_type    Mode,
                  const char* const   MethodName )
    {
        int retval = -1;

        if ( MountPointStatus::Status( Path ) ==
             MountPointStatus::STATE_OFFLINE )
        {
            errno = EINTR;
            return retval;
        }

        //-------------------------------------------------------------------
        // PR#3015 -
        // Be careful with the lstat call since it can lock up under
        //   nfs usage and not return until the server is once again
        //   available.
        // This condition is avoided by setting a maximum time for this
        //   call to return before causing the lstat call to be interupted.
        //-------------------------------------------------------------------
        //-----------------------------------------------------------
        // Get entry from pool to reduce system allocation overhead
        //-----------------------------------------------------------
        SysCallStatPool_type::element_type syscall =
            SysCallStatPool_type::Request( );

        int status = TIMEOUT_COMPLETED;

        if ( syscall )
        {
            syscall->Reset( Path, Mode );
            try
            {
                //---------------------------------------------------------
                // PR#341
                // Carefully setup to only wait a certain amount of time
                // for this call to complete.
                //---------------------------------------------------------
                Timeout( syscall, GenericAPI::lstat_timeout( ), status );
            }
            catch ( const TimeoutException& Exception )
            {
                errno = EINTR;

                std::ostringstream msg;

                msg << "TimeoutException for: " << Path << " ("
                    << Exception.what( ) << ")";

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_NOTE,
                    0,
                    MethodName,
                    "CXX_INTERFACE" );
                if ( StdErrLog.IsOpen( ) )
                {
                    StdErrLog( ErrorLog::DEBUG,
                               __FILE__,
                               __LINE__,
                               Exception.what( ) );
                }
            }
            catch ( const std::exception& Exception )
            {
                std::ostringstream msg;

                msg << "std::exception for: " << Path << " ("
                    << Exception.what( ) << ")";

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_NOTE,
                    0,
                    MethodName,
                    "CXX_INTERFACE" );
                if ( StdErrLog.IsOpen( ) )
                {
                    StdErrLog( ErrorLog::DEBUG,
                               __FILE__,
                               __LINE__,
                               Exception.what( ) );
                }
            }
            catch ( ... )
            {
                std::ostringstream msg;

                msg << "unknown exception for: " << Path;

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_NOTE,
                    0,
                    MethodName,
                    "CXX_INTERFACE" );
                // :TODO: Unknown exception
            }
            //-----------------------------------------------------------------
            // Need to see how the call completed.
            //-----------------------------------------------------------------
            if ( status & TIMEOUT_COMPLETED )
            {
                //---------------------------------------------------------------
                // Transfer results so caller can make use of them.
                //---------------------------------------------------------------
                retval = syscall->SystemReturnCode( );
                errno = syscall->SystemErrNo( );
                if ( retval != 0 )
                {
                    QUEUE_LOG_MESSAGE( "retval: " << retval
                                                  << " errno: " << errno,
                                       MT_NOTE,
                                       0,
                                       MethodName,
                                       "CXX" );
                }
                if ( retval == 0 )
                {
                    syscall->Sync( StatBuffer );
                }
                //---------------------------------------------------------------
                // Recycle this memory for future use.
                //---------------------------------------------------------------
                SysCallStatPool_type::Release( syscall );
            }
            else
            {
                //---------------------------------------------------------------
                // Put onto the trash heap to prevent the memory from being
                // recycled and causing memory corruption.
                //---------------------------------------------------------------
                SysCallStatPool_type::Trash( syscall );
            }
        }
        if ( errno == EINTR )
        {
            //---------------------------------------------------------
            // An interupted system call means that the task took too
            // long to complete. Register this file system as being
            // offline.
            //---------------------------------------------------------
            MountPointStatus::Offline( Path );
        }

        return retval;
    }

} // namespace
