//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__LSTAT_POOL_HH
#define GENERIC_API__LSTAT_POOL_HH

#include <list>
#include <iostream>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/ErrorLog.hh"
#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/ThreadPool.hh"

namespace GenericAPI
{

    template < typename StatInfoType >
    class StatPoolT
    {
    public:
        typedef typename boost::shared_ptr< StatInfoType > element_type;

        static void Cleanup( );

        static void Destroy( element_type Buffer );

        static std::ostream& Dump( std::ostream& Stream );

        static bool Interuptable( );

        static void Recycle( typename element_type::element_type* Buffer );

        static void Release( element_type Buffer );

        static element_type Request( );

        static void Trash( element_type Buffer );

    private:
        typedef std::list< element_type > pool_type;
        typedef LDASTools::AL::MutexLock  lock_type;

        lock_type::baton_type pool_lock;
        pool_type             available;
        pool_type             in_use;
        pool_type             trash_;

        void cleanup( );

        void destroy( element_type Buffer );

        std::ostream& dump( std::ostream& Stream );

        void release( element_type Buffer );

        void trash( element_type Buffer );

        void recycle( typename element_type::element_type* Buffer );

        element_type request( );
    };

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::Cleanup( )
    {
        LDASTools::AL::Singleton< StatPoolT< StatInfoType > >::Instance( )
            .cleanup( );
    }

    template < typename StatInfoType >
    bool
    StatPoolT< StatInfoType >::Interuptable( )
    {
        return true;
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::Recycle(
        typename element_type::element_type* Buffer )
    {
        LDASTools::AL::Singleton< StatPoolT< StatInfoType > >::Instance( )
            .recycle( Buffer );
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::Release( element_type Buffer )
    {
        LDASTools::AL::Singleton< StatPoolT< StatInfoType > >::Instance( )
            .release( Buffer );
    }

    template < typename StatInfoType >
    typename StatPoolT< StatInfoType >::element_type
    StatPoolT< StatInfoType >::Request( )
    {
        return LDASTools::AL::Singleton<
                   StatPoolT< StatInfoType > >::Instance( )
            .request( );
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::Trash( element_type Buffer )
    {
        LDASTools::AL::Singleton< StatPoolT< StatInfoType > >::Instance( )
            .trash( Buffer );
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::Destroy( element_type Buffer )
    {
        LDASTools::AL::Singleton< StatPoolT< StatInfoType > >::Instance( )
            .destroy( Buffer );
    }

    template < typename StatInfoType >
    std::ostream&
    StatPoolT< StatInfoType >::Dump( std::ostream& Stream )
    {
        return LDASTools::AL::Singleton<
                   StatPoolT< StatInfoType > >::Instance( )
            .dump( Stream );
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::cleanup( )
    {
        using namespace LDASTools::AL;

        static bool has_been_cleaned = false;

        if ( has_been_cleaned == false )
        {
            lock_type lock( pool_lock, __FILE__, __LINE__ );

            if ( has_been_cleaned == false )
            {
                if ( StdErrLog.IsOpen( ) )
                {
                    std::ostringstream msg;

                    msg << "StatPool::~StatPool";
                    StdErrLog(
                        ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
                }
                available.erase( available.begin( ), available.end( ) );
                in_use.erase( in_use.begin( ), in_use.end( ) );
                has_been_cleaned = true;
            } // if ( has_been_cleaned == false ) (after Mutex lock)
        } // if ( has_been_cleaned == false )
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::destroy( element_type Buffer )
    {
        using namespace LDASTools::AL;

        lock_type lock( pool_lock, __FILE__, __LINE__ );

        if ( StdErrLog.IsOpen( ) )
        {
            std::ostringstream msg;

            msg << "StatPool::destroy: Buffer: " << (void*)( Buffer.get( ) );
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
        }
        //-------------------------------------------------------------------
        // Take out of m_in_use list
        //-------------------------------------------------------------------
        /* erase the element */
        typename pool_type::size_type size = in_use.size( );

        in_use.remove( Buffer );
        if ( size != in_use.size( ) )
        {
            //-----------------------------------------------------------------
            // Destroy the process
            //-----------------------------------------------------------------
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::release: deleting: Buffer: "
                    << (void*)( Buffer.get( ) );
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
        }
    }

    template < typename StatInfoType >
    std::ostream&
    StatPoolT< StatInfoType >::dump( std::ostream& Stream )
    {
        lock_type lock( pool_lock, __FILE__, __LINE__ );

        typename pool_type::size_type used = in_use.size( );
        typename pool_type::size_type avail = available.size( );

        Stream << "Queue: " << used << " of " << ( used + avail )
               << " slots are used" << std::endl;

        for ( typename pool_type::const_iterator cur = in_use.begin( ),
                                                 last = in_use.end( );
              cur != last;
              ++cur )
        {
            Stream << "\tStatting directory: " << *cur << std::endl;
        }
        return Stream;
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::release( element_type Buffer )
    {
        using namespace LDASTools::AL;

        lock_type lock( pool_lock, __FILE__, __LINE__ );

        if ( StdErrLog.IsOpen( ) )
        {
            std::ostringstream msg;

            msg << "StatPool::release";
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
        }
        //-------------------------------------------------------------------
        // Take out of in_use list
        //-------------------------------------------------------------------
        /* erase the element */
        typename pool_type::size_type size = in_use.size( );

        in_use.remove( Buffer );
        if ( size != in_use.size( ) )
        {
            //-----------------------------------------------------------------
            // Put into available queue
            //-----------------------------------------------------------------
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::release: returned to a available pool";
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
            if ( !MemChecker::IsExiting( ) )
            {
                available.push_back( Buffer );
            }
        }
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::recycle(
        typename element_type::element_type* Buffer )
    {
        using namespace LDASTools::AL;

        element_type retval;

        for ( typename pool_type::iterator cur = trash_.begin( ),
                                           last = trash_.end( );
              cur != last;
              ++cur )
        {
            if ( cur->get( ) == Buffer )
            {
                retval = *cur;
                trash_.erase( cur );
                break;
            }
        }
        if ( retval )
        {
            available.push_back( retval );
        }
    }

    template < typename StatInfoType >
    typename StatPoolT< StatInfoType >::element_type
    StatPoolT< StatInfoType >::request( )
    {
        using namespace LDASTools::AL;

        lock_type    lock( pool_lock, __FILE__, __LINE__ );
        element_type retval;

        if ( available.size( ) == 0 )
        {
            //-----------------------------------------------------------------
            // Create new thread
            //-----------------------------------------------------------------
            retval.reset( new typename element_type::element_type );

            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::request: Created new";
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
        }
        else
        {
            //-----------------------------------------------------------------
            // Take one from the available list
            //-----------------------------------------------------------------
            retval = available.front( );
            available.pop_front( );
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::request: Reusing";
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
        }
        in_use.push_back( retval );
        return retval;
    }

    template < typename StatInfoType >
    void
    StatPoolT< StatInfoType >::trash( element_type Buffer )
    {
        using namespace LDASTools::AL;

        lock_type lock( pool_lock, __FILE__, __LINE__ );

        if ( StdErrLog.IsOpen( ) )
        {
            std::ostringstream msg;

            msg << "StatPool::trash";
            StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
        }
        //-------------------------------------------------------------------
        // Take out of in_use list
        //-------------------------------------------------------------------
        typename pool_type::size_type size = in_use.size( );

        /* erase the element */
        in_use.remove( Buffer );
        if ( size != in_use.size( ) )
        {
            //-----------------------------------------------------------------
            // Put into available queue
            //-----------------------------------------------------------------
            if ( StdErrLog.IsOpen( ) )
            {
                std::ostringstream msg;

                msg << "StatPool::trash: trashing";
                StdErrLog( ErrorLog::DEBUG, __FILE__, __LINE__, msg.str( ) );
            }
            if ( !MemChecker::IsExiting( ) )
            {
                trash_.push_back( Buffer );
            }
        }
    }

} // namespace GenericAPI

namespace std
{
    template < typename StatInfoType >
    ostream&
    operator<<( ostream&                                     Stream,
                const GenericAPI::StatPoolT< StatInfoType >& Pool )
    {
        return Pool.Dump( Stream );
    }
} // namespace std
#endif /* GENERIC_API__LSTAT_POOL_HH */
