//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__SYMBOL_MAPPER_HH
#define GENERIC_API__SYMBOL_MAPPER_HH

#include <string>

#include "ldastoolsal/unordered_map.hh"

namespace GenericAPI
{
    class SymbolMapper
    {
    public:
        //-------------------------------------------------------------------
        /// <ul>
        ///   <li><b>I</b> Integer parameter</li>
        ///   <li><b>LS</b> List of string parameters</li>
        ///   <li><b>NS</b> Variable name parameter</li>
        ///   <li><b>S</b> String parameter</li>
        /// </ul>
        //-------------------------------------------------------------------
        enum arg_type
        {
            ARGS_I,
            ARGS_S,
            ARGS_NS,
            ARGS_LS
        };

        typedef void ( *func_type )( ... );

        virtual ~SymbolMapper( );

        static inline void
        Add( const std::string& Symbol, arg_type Arg, func_type Function )
        {
            if ( m_symbol_mapper )
            {
                m_symbol_mapper->add( Symbol, Arg, Function );
                m_symbol_mapper->addSymbol( Symbol, Arg, Function );
            }
        }

        static void OnExit( );

    protected:
        class info
        {
        public:
            ~info( );

            arg_type  s_arg_type;
            func_type s_function;
        };

        typedef LDASTools::AL::unordered_map< std::string, info >
            symbol_table_type;

        static SymbolMapper* m_symbol_mapper;

        void add( const std::string& Symbol, arg_type Arg, func_type Function );

        virtual void addSymbol( const std::string& Symbol,
                                arg_type           Arg,
                                func_type          Function ) = 0;

        const info* lookup( const std::string& Symbol ) const;

    private:
        symbol_table_type m_symbol_table;
    };

    inline SymbolMapper::info::~info( )
    {
    }

} // namespace GenericAPI

#endif /* GENERIC_API__SYMBOL_MAPPER_HH */
