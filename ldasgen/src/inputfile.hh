//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GenericApiInputFileHH
#define GenericApiInputFileHH

#include <boost/shared_array.hpp>

#include "ldastoolsal/fstream.hh"
#include "genericAPI/file.hh"

//-----------------------------------------------------------------------------
//
//: Input File
//
// This class represents a file which is open for input.
//
// <p>The class is
// constructed by passing a filename and an open mode.  The constructor
// creates an underlying ifstream which represents the file.  This ifstream
// is available through an accessor.  The stream is closed when the destructor
// is called.
//
class InputFile : public File
{
public:
    typedef LDASTools::AL::ifstream ifstream;

    /* Constructor/Destructor */
    InputFile( const char* filename, std::ios::openmode m );
    InputFile( const char*        filename,
               std::ios::openmode m,
               unsigned int       buffer_size );
    InputFile( const char*        filename,
               std::ios::openmode m,
               bool               use_memory_mapped_io,
               unsigned int       buffer_size,
               char*              buffer = (char*)NULL );
    virtual ~InputFile( );

    /* Accessors */
    ifstream&    getifstream( );
    virtual bool is_open( );

private:
    // Buffer for stream when a size has been specified
    boost::shared_array< char > m_buffer;
    /* Data Members */
    ifstream mStream;

    void open_file( const char* filename, std::ios::openmode m );
};

//-----------------------------------------------------------------------------
//
//: Get the underlying stream
//
//! return: std::ifstream&
//
inline InputFile::ifstream&
InputFile::getifstream( )
{
    return mStream;
}

#endif // GenericApiInputFileHH
