//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <memory>

#include "ldastoolsal/MemChecker.hh"

#include "genericAPI/SymbolMapper.hh"

using LDASTools::AL::MemChecker;

static GenericAPI::SymbolMapper*
init( )
{
    using namespace GenericAPI;

    MemChecker::Append(
        SymbolMapper::OnExit, "GenericAPI::SymbolMapper::OnExit", 5 );
    return (SymbolMapper*)NULL;
}

namespace GenericAPI
{
    SymbolMapper* SymbolMapper::m_symbol_mapper = init( );

    SymbolMapper::~SymbolMapper( )
    {
    }

    void
    SymbolMapper::OnExit( )
    {
        if ( m_symbol_mapper )
        {
            delete m_symbol_mapper;
            m_symbol_mapper = (SymbolMapper*)NULL;
        }
    }

    void
    SymbolMapper::add( const std::string& Symbol,
                       arg_type           Arg,
                       func_type          Function )
    {
        symbol_table_type::iterator pos = m_symbol_table.find( Symbol );

        if ( pos == m_symbol_table.end( ) )
        {
            info i;

            i.s_arg_type = Arg;
            i.s_function = Function;

            m_symbol_table[ Symbol ] = i;
        }
        else
        {
            pos->second.s_arg_type = Arg;
            pos->second.s_function = Function;
        }
    }

    const SymbolMapper::info*
    SymbolMapper::lookup( const std::string& Symbol ) const
    {
        symbol_table_type::const_iterator pos = m_symbol_table.find( Symbol );

        if ( pos != m_symbol_table.end( ) )
        {
            return &( pos->second );
        }
        return (info*)NULL;
    }
} // namespace GenericAPI
