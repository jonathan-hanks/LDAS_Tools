//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GenericApiFileHH
#define GenericApiFileHH

#include <fstream>

#include "ldastoolsal/mutexlock.hh"

#include "swigexception.hh"

#undef GENERIC_API_FILE_IO_LOCK

#ifdef GENERIC_API_FILE_IO_LOCK
namespace General
{
    extern pthread_mutex_t IOMutex;
}
#endif /* GENERIC_API_FILE_IO_LOCK */

//-----------------------------------------------------------------------------
//
//: File
//
// This class represents a file for TCL.
//
class File
{
public:
    /* Destructor */
    virtual ~File( );

    //: Check if a file object is open.
    //
    // Returns true if the file is open.
    //
    //! return: bool
    //
    virtual bool is_open( ) = 0;
};

//-----------------------------------------------------------------------------
//
//: Destructor
//
// This destructor does nothing, it just defines the destructor to be virtual
// so that child classes can be destruted through a pointer or reference to the
// base class.
//
inline File::~File( )
{
}

#endif // GenericApiFileHH
