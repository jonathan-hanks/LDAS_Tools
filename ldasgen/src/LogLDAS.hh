//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__LOG_LDAS_HH
#define GENERIC_API__LOG_LDAS_HH

#include <string>

#include "ldastoolsal/Log.hh"

namespace GenericAPI
{
    namespace Log
    {
        //-------------------------------------------------------------------
        /// \brief LDAS Logging class
        //-------------------------------------------------------------------
        class LDAS : public ::LDASTools::AL::Log
        {
        public:
            //-----------------------------------------------------------------
            /// \brief Types of messages.
            //-----------------------------------------------------------------
            enum message_type
            {
                MT_OK = 0, ///< Nornal status.
                MT_WARN = 1, ///< Notable condition which is a non-fatal error.
                MT_ERROR = 2, ///< Fatal error condition.
                MT_EMAIL =
                    3, ///< Condition which requires operator intervention.
                MT_PHONE = 4, ///< Condition which requires immediate operator
                              ///< intervention.
                MT_DEBUG = 5, ///< Debugging message intended for developers.
                MT_NOTE = 6, ///< Notable condition which is not an error.
                MT_ORANGE, ///< Error condition not fatal to the job.
                MT_CERTMAIL, ///< Condition which requires operator
                             ///< intervention.

                MT_GREEN = MT_OK, ///< Alias for MT_OK
                MT_YELLOW = MT_WARN, ///< Alias for MT_WARN
                MT_RED = MT_ERROR, ///< Alias for MT_ERROR
                MT_MAIL = MT_EMAIL, ///< Alias for MT_EMAIL
                MT_PAGER = MT_PHONE, ///< Alias for MT_PHONE
                MT_BLUE = MT_DEBUG, ///< Alias for MT_DEBUG
                MT_PURPLE = MT_NOTE ///< Alias for MT_NOTE
            };

            enum time_format
            {
                TF_GMT, ///< Format for GMT time
                TF_LOCAL ///< Format for local time
            };

            LDAS( const std::string& BaseName );

            virtual LDAS* Clone( const std::string& BaseName ) const = 0;

            virtual const char* FileExtension( ) const = 0;

            virtual void Message( message_type       MessageType,
                                  level_type         Level,
                                  const std::string& Caller,
                                  const std::string& JobInfo,
                                  const std::string& Message ) = 0;

            static std::string FormatTime( time_format                   Format,
                                           const LDASTools::AL::GPSTime& Time );

            virtual std::string
            FormatJobInfo( const std::string& JobInfo ) const = 0;

            static std::string LogFilename( const char* Extension,
                                            int         Version );

        protected:
            virtual void header( );

            virtual void footer( );

            virtual void onStreamClose( );

            virtual void onStreamOpen( );

#if 0
      //-----------------------------------------------------------------
      /// \brief Rotate log files
      //-----------------------------------------------------------------
      virtual void rotate( );
#endif /* 0 */

            static std::string
            siteInfoLookup( const std::string& LDASSystemName );

            //-----------------------------------------------------------------
            /// \brief Send a message immediately to the logging stream
            //-----------------------------------------------------------------
            void writeDirect( const std::string& Message );
        };

        inline LDAS::LDAS( const std::string& BaseName )
            : ::LDASTools::AL::Log( BaseName )
        {
        }

        inline void
        LDAS::writeDirect( const std::string& Message )
        {
            m_stream->WriteMessage( Message );
        }
    } // namespace Log
} // namespace GenericAPI

#endif /* GENERIC_API__LOG_LDAS_HH */
