//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef GENERIC_API__LSTAT_POOL_HH
#define GENERIC_API__LSTAT_POOL_HH

#include <list>

#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Singleton.hh"

namespace GenericAPI
{

    class StatBase;

    class StatPool : public LDASTools::AL::Singleton< StatPool >
    {
        // Declare the common methods for Singleton class
    public:
        typedef StatBase        info_base_type;
        typedef info_base_type* info_type;
        typedef void ( *user_init_cb )( info_type Key );

        StatPool( );

        ~StatPool( );

        static void Cleanup( );

        static info_type StatType( );

        static info_type Request( );
        static void      Release( info_type Buffer );
        static void      Destroy( info_type Buffer );
        static void      UserInitCB( user_init_cb Callback );

    private:
        LDASTools::AL::MutexLock::baton_type m_pool_lock;
        user_init_cb                         m_user_init_cb;

        void      cleanup( );
        void      destroy( info_type Buffer );
        void      release( info_type Buffer );
        info_type request( );

        void user_init_callback( user_init_cb Callback );
    };
} // namespace GenericAPI

#endif /* GENERIC_API__LSTAT_POOL_HH */
