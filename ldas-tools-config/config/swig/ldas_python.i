/*
 * Copyright (C) 2018 California Institute of Technology
 *
 *  This program is free software; you may redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 (GPLv2) of the
 *  License or at your discretion, any later version.
 *
 *  This program is distributed in the hope that it will be useful, but
 *  without any warranty or even the implied warranty of merchantability
 *  or fitness for a particular purpose. See the GNU General Public
 *  License (GPLv2) for more details.
 *
 *  Neither the names of the California Institute of Technology (Caltech),
 *  The Massachusetts Institute of Technology (M.I.T), The Laser
 *  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 *  of its contributors may be used to endorse or promote products derived
 *  from this software without specific prior written permission.
 * 
 *  You should have received a copy of the licensing terms for this
 *  software included in the file LICENSE located in the top-level
 *  directory of this package. If you did not, you can view a copy at
 *  http://dcc.ligo.org/M1500244/LICENSE
 *
 */

#ifndef BUILD__SWIG__LDAS_PYTHON_I
#define BUILD__SWIG__LDAS_PYTHON_I 1

#define LDAS_DOC_PROTO_WITHOUT_TYPES "0"
#define LDAS_DOC_PROTO_WITH_TYPES "1"

/*
 */
%define Documentation(Proto,Brief,Full)
  %feature("autodoc",Proto);
  %feature("docstring") %{
    Brief

    Full
  %}
%enddef

/*
 */
%define LDASPythonDocStringStart
#if ! defined(_LDASDocStringStarted)
#define _LDASDocStringStarted 1
%feature("docstring") %{
#endif /* ! defined(_LDASDocStringStarted) */
%enddef /* LDASDocStartDocString */

/*
 */
%define LDASPythonDocStringStop
#if defined(_LDASDocStringStarted)
%}
#undef _LDASDocStringStarted
#endif /* _LDASDocStringStarted */
%enddef /* LDASPythonDocStringStop */

/*
 */
%define LDASDocBegin
#undef _LDAS_DOCSTRING
%enddef /* LDASDocBegin */

/*
 */
%define LDASDocEnd
%feature("docstring") %{
#if defined(LDASDocVarBrief)
LDASDocVarBrief
#undef LDASDocVarBrief
#endif /* LDASDocVarBrief */
#if defined(LDASDocVarDescription)
LDASDocVarDescription
#undef LDASDocVarDescription
#endif /* LDASDocVarBrief */
%}
%enddef /* LDASDocEnd */

/*
 */
%define LDASDocProto(Proto)
  %feature("autodoc",Proto)
%enddef

/*
 */
%define LDASDocBrief(Brief)
%#define LDASDocVarBrief Brief
%enddef /* LDASDocBrief(Brief) */

/*
 */
%define LDASDocDesc(Description)
%#define LDASDocVarDescription Description
%enddef

/*
 */
#define LDASDocParam(Name,Description) \
   Name: Description

%define LDASDoc(Function,Proto,Brief,Long,Params)
%feature("autodoc",Proto) Function;
%feature("docstring") Function %{
Brief

Long

Parameters:
Params
%};
%enddef /* LDASDoc */

%define LDASDocZeroParams(Function,Proto,Brief,Long)
%feature("autodoc",Proto) Function;
%feature("docstring") Function %{
Brief

Long
%};
%enddef /* LDASDoc */

#endif /* BUILD__SWIG__LDAS_PYTHON_I */
