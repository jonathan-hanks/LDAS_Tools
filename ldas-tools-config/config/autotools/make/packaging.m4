#------------------------------------------------------------------------
# MACPORTS -
#   These targets aid in validating the building of the package
#   for distributions that support MACPORTS packaging
#------------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


EXTRA_DIST+=Portfile.in
if BUILDING_MACPORTS
#------------------------------------------------------------------------
# Make sure that the testing environment is setup correctly.
# Based on section 4.6 - Local Portfile Repositories
#  ( url: https://guide.macports.org/chunked/development.local-repositories.html )
#------------------------------------------------------------------------
PORT_PKG_DIR=/opt/local/var/macports/distfiles/$(PACKAGE_NAME)
PORT_CONFIG=/opt/local/etc/macports/sources.conf
local_port_repository:
	@grep "file://$(abs_builddir)/test_port" $(PORT_CONFIG) 1>/dev/null 2>&1; \
	 if test ! $$? = 0; \
	 then \
	   echo "FATAL: Please modify $(PORT_CONFIG) to include $(abs_builddir)/test_port" 1>&2; \
	   exit 1; \
	 fi
	@mkdir -p $(abs_builddir)/test_port/science

test_port/science/Portfile: $(srcdir)/Portfile.in dist
	$(SED) \
	  -e 's,[@]RMD160[@],'`openssl rmd160 $(DIST_ARCHIVES)| sed -e 's/^.*= //'`',g' \
	  -e 's,[@]SHA256[@],'`openssl sha256 $(DIST_ARCHIVES)| sed -e 's/^.*= //'`',g' \
	  -e 's,[@]PACKAGE_VERSION[@],$(PACKAGE_VERSION),g' \
	< $< > $@

macports: local_port_repository test_port/science/Portfile
	@echo "Building MACPORTS package..."
	cp $(DIST_ARCHIVES) $(PORT_PKG_DIR)/.
	( cd $(abs_builddir)/test_port/science; $(MACPORTSBUILD) lint --nitpick ldas-tools )
	( cd $(abs_builddir)/test_port/science; $(MACPORTSBUILD) clean )
	( cd $(abs_builddir)/test_port/science; $(MACPORTSBUILD) install )
else
macports:
	@echo \
	"To build an MACPORTS package, you must have the program port "; \
	exit 1
endif

#------------------------------------------------------------------------
# RPM -
#   These targets aid in validating the building of the package
#   for distributions that support RPM packaging
#------------------------------------------------------------------------

if BUILDING_RPM
rpm: dist
	@echo "Building RPM package..."; \
	$(RPMBUILD) -ta --clean $(PACKAGE)-$(VERSION).tar.gz
else
rpm:
	@echo \
	"To build an RPM package, you must have the program rpmbuild "; \
	exit 1
endif

#------------------------------------------------------------------------
# DEB -
#   These targets aid in validating the building of the package
#   for distributions that support DEB packaging
#------------------------------------------------------------------------

if BUILDING_DEB
DEBSOURCEPKGORIG=$(PACKAGE)_$(VERSION).orig.tar
DEBSOURCEPKG=$(DEBSOURCEPKGORIG).gz
DEBSOURCEDIR=$(PACKAGE)-$(VERSION)
deb: dist
	@echo "Building Debian package..."; \
	if test ! -d $(srcdir)/debian; then \
		echo "The files needed for building a Debian package are not" \
		"included by default in the distribution. To build a package, check" \
		"out the project from source control."; \
		exit 1; \
	fi; \
	mv $(PACKAGE)-$(VERSION).tar.gz $(DEBSOURCEPKG) && \
	GZIP=$(GZIP_ENV) gzip -dc $(DEBSOURCEPKG) | $(am__untar) && \
	$(MKDIR_P) $(DEBSOURCEDIR)/debian && \
	cp -R \
		$(srcdir)/debian/. \
		$(DEBSOURCEDIR)/debian && \
	cd $(DEBSOURCEDIR) && \
	$(DEBBUILD) -rfakeroot -D -us -uc -b; \
	cd $(srcdir); \
	rm --force $(DEBSOURCEPKG); \
	rm --recursive --force $(DEBSOURCEDIR)
else
deb:
	@echo \
	"To build a Debian package, you must have the program dpkg-buildpackage "; \
	exit 1
endif

debian-dist:
	cp -r $(srcdir)/debian $(distdir)

valgrind-dist:
	cp -r $(srcdir)/valgrind $(distdir)

#------------------------------------------------------------------------
# Copy the distribution to various locations
#------------------------------------------------------------------------
distribute: dist
	cp $(PACKAGE)-$(VERSION).tar.gz $(HOME)/public_html/.
	gsiscp $(PACKAGE)-$(VERSION).tar.gz ldas-pcdev1.ligo.caltech.edu:public_html/.
	scp $(PACKAGE)-$(VERSION).tar.gz install@ldas-sw.ligo.caltech.edu:/export/ldcg_server/htdocs/packages

__init__.py: Makefile
	touch __init__.py

