#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#------------------------------------------------------------------------
# Support data race analysis without recompiling
#------------------------------------------------------------------------

dataracecheck-recursive:

ifneq ($(RECURSIVE_TARGETS),dataracecheck-recursive)
dataracecheck-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=dataracecheck-recursive dataracecheck-recursive
endif

dataracecheck: dataracecheck-recursive dataracecheck-am

dataracecheck-am: all-am

#------------------------------------------------------------------------
# Support memory corruption/usage analysis without recompiling
#------------------------------------------------------------------------

memcheck-recursive:

ifneq ($(RECURSIVE_TARGETS),memcheck-recursive)
memcheck-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=memcheck-recursive memcheck-recursive
endif

memcheck: memcheck-recursive memcheck-am

memcheck-am: all-am

#------------------------------------------------------------------------
# Support thead safety analysis without recompiling
#------------------------------------------------------------------------

mtsafe-recursive:

ifneq ($(RECURSIVE_TARGETS),mtsafe-recursive)
mtsafe-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=mtsafe-recursive mtsafe-recursive
endif

mtsafe: mtsafe-recursive  mtsafe-am

mtsafe-am: all-am

#------------------------------------------------------------------------
# Support profiling analysis without recompiling
#------------------------------------------------------------------------

profile-recursive:

ifneq ($(RECURSIVE_TARGETS),profile-recursive)
profile-recursive:
	$(MAKE) $(AM_MAKEFLAGS) RECURSIVE_TARGETS=profile-recursive profile-recursive
endif

profile: profile-recursive profile-am

profile-am: all-am

#========================================================================
# Include files to support targets
#------------------------------------------------------------------------

ifneq ($(VALGRIND),)
  VALGRIND_LD_LIBRARY_PATH=$(MEMORY_DEBUG_LD_LIBRARY_PATH)
  include $(LDAS_DIRECTORY_MAKE_INCLUDE)/valgrind.mk
endif
ifneq ($(LIBUMEM),)
  include $(LDAS_DIRECTORY_MAKE_INCLUDE)/libumem.mk
endif

