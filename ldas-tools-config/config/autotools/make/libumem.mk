#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


MEMORY_DEBUG_TESTS ?= $(TESTS)
MEMORY_DEBUG_TESTS_ENVIRONMENT ?= $(TESTS_ENVIRONMENT)

memcheck: libumem-memcheck
mtsafe:

libumem-memcheck:
	if test -x /usr/bin/bcheck; \
	then $(MAKE) libumem-memcheck-bcheck; \
	else $(MAKE) libumem-memcheck-no-bcheck; \
	fi

libumem-memcheck-bcheck:
	$(MAKE) \
	  LIBUMEM_PROG=/usr/bin/bcheck \
	  LIBUMEM_TOOL=libumem-memcheck \
          LIBUMEM_ENV="-all" \
	  libumem-doit

libumem-memcheck-no-bcheck:
	$(MAKE) \
	  LIBUMEM_PROG=env \
	  LIBUMEM_TOOL=libumem-memcheck \
          LIBUMEM_ENV="UMEM_DEBUG=default,firewall=1 UMEM_OPTIONS=backend=mmap UMEM_LOGGING=transaction" \
	  libumem-doit

libumem-doit: $(MEMORY_DEBUG_TESTS)
	echo $(MEMORY_DEBUG_LD_LIBRARY_PATH)
	@for x in $(MEMORY_DEBUG_TESTS);			\
	do							\
	  if test -x ".libs/lt-$$x";				\
	  then							\
	    cmd=".libs/lt-$$x";					\
	  elif test -x ".libs/$$x";				\
	  then							\
	    cmd=".libs/$$x";					\
	  else							\
	    cmd="./$$x";					\
	  fi;							\
	  echo $(MEMORY_DEBUG_TESTS_ENVIRONMENT)		\
	    env 						\
	    LD_LIBRARY_PATH_64="$(MEMORY_DEBUG_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH_64}" \
	    LD_PRELOAD_64=$(LIBUMEM)				\
	    $(LIBUMEM_PROG) $(LIBUMEM_ENV)                      \
            $$cmd;						\
	  $(MEMORY_DEBUG_TESTS_ENVIRONMENT)			\
	    env 						\
	    LD_LIBRARY_PATH_64="$(MEMORY_DEBUG_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH}" \
	    LD_PRELOAD_64=$(LIBUMEM)				\
	    $(LIBUMEM_PROG) $(LIBUMEM_ENV)                      \
            $$cmd > $$cmd.$(LIBUMEM_TOOL) 2>&1;			\
	done
