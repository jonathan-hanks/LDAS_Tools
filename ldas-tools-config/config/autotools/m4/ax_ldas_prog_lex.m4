dnl======================================================================
dnl AX_LDAS_PROG_LEX
dnl   Check on how to setup lex
dnl Depends:
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_PROG_LEX],
[ AM_PROG_LEX
  case x$LEX in
  x/*)
    ;;
  *)
    AC_PATH_PROGS(LEX,flex lex)
    ;;
  esac
  case x$LEX in
  x/usr/bin*)
    ;;
  *)
    tmp="`echo $LEX | sed -e 's,/bin/.*$,/include,g'`"
    echo $LEX_CPPFLAGS | grep "${tmp}" > /dev/null 2>&1
    case $? in
    0)
      ;;
    *)
      LEX_CPPFLAGS="${LEX_CPPFLAGS} -I${tmp}"
    esac
    ;;
  esac
  $LEX --version > /dev/null 2>&1
  AS_CASE([$?],
          [0],[LDAS_CHECK_HEADER_FLEX_LEXER_H
	       dnl------------------------------------------------------
	       dnl If the version of flex installed on the destination
	       dnl  machine is different from what generated the
	       dnl  sources, then remove the sources so they will be
	       dnl  regenerated.
	       dnl------------------------------------------------------
    	       save_CPPFLAGS=${CPPFLAGS}
	       CPPFLAGS="${LEX_CPPFLAGS} ${CPPFLAGS}"
    	       AC_EGREP_CPP([yy_current_buffer],
    	                    [ #include <FlexLexer.h>
    		            ],
    		            [ AC_DEFINE([HAVE_YY_CURRENT_BUFFER],
                                        [1],
			                [Defined if yy_current_buffer is in yyFlexLexer class])
    		            ],
    		            [ AC_DEFINE([YY_CURRENT_BUFFER_EQUIV],
                                        [(yy_buffer_stack[[yy_buffer_stack_top]])],
                                        [Defined as the equivelent for yy_current_buffer])
    		            ])
	       AC_LANG_PUSH([C++])
	       AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
	                          #include <FlexLexer.h>
				  ],
				  [
				    return 0;
				  ])
		                 ],
				 [_ldas_flex_lexer_h_broken="0"],
				 [_ldas_flex_lexer_h_broken="1"])
	       AC_LANG_POP([C++])
    	       CPPFLAGS=${save_CPPFLAGS}
   	       unset save_CPPFLAGS
	       AS_IF([test x$_ldas_flex_lexer_h_broken != "x1"],
	             [ldas_using_flex="1"])
	       ])
  AC_SUBST([LEX_CPPFLAGS])
  AM_CONDITIONAL([LDAS_USING_FLEX],[test "x$ldas_using_flex" = "x1"])
])
