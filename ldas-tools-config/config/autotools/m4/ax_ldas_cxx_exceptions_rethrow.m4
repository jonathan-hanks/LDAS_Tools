dnl ---------------------------------------------------------------------
dnl  AX_LDAS_CXX_EXCEPTIONS_RETHROW
dnl     Check if C++ supports exceptions rethrowing
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_CXX_EXCEPTIONS_RETHROW],[
  dnl -------------------------------------------------------------------------
  dnl Check how well compiler handles exceptions
  dnl -------------------------------------------------------------------------

  AC_MSG_CHECKING(if compiler supports rethrowing of exceptions)
  AC_TRY_RUN([
  #include <iostream>
  #include <stdexcept>

  //
  // Extended bad_exception class with info on exception no handled in the
  // throw specifier
  //

  class unhandled: public std::bad_exception
  {
  public:
    unhandled( const std::string& what )
      : m_what( what )
    {
    }
  
    ~unhandled() throw() {}
  
    virtual const char*
    what() const throw()
    {
      return m_what.c_str();
    }
  
  private:
    std::string m_what;
  };

  //
  // New handler for unexpected exceptions
  //

  void
  handle_unexpected( void )
  {
    try {
      throw;
    }
    catch( const std::logic_error& e )
    {
	exit( 0 );
    }
  }

  //
  // Problem do to logic_error not being in throw specification
  //

  void
  c3( void ) throw ( std::bad_exception )
  {
    throw std::logic_error("c3");
  }

  int
  main()
  {
    std::set_unexpected( handle_unexpected );
  
    try {
      c3();
    }
    catch ( const std::logic_error& e )
    {
      exit (1);
    }
    catch ( const std::bad_exception& e )
    {
      exit (0);
    }
    return( 1 );
  }
  ],[AC_DEFINE([HAVE_RETHROW_WORKING_IN_BAD_EXCEPTION],
               [1],
	       [Defined if retrow works in bad_exception])
  AC_MSG_RESULT(yes)],[AC_MSG_RESULT(no)])

])
