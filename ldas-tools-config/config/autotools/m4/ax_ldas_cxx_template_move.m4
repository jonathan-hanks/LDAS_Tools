dnl ---------------------------------------------------------------------
dnl  AX_LDAS_CXX_UNIQUE_PTR
dnl     Check if C++ supports move template (2011 standard)
dnl ---------------------------------------------------------------------
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_CXX_TEMPLATE_MOVE],[
  AC_LANG_PUSH([C++])
  AC_TRY_COMPILE([
   #include<algorithm>
   #include<string>
   #include<utility>
   #include<vector>
  ],
  [std::vector<std::string> foo;
   foo.push_back( "air" );
   foo.push_back( "water" );
   foo.push_back( "fire" );
   foo.push_back( "earth" );
   std::vector<std::string> bar(4);
   foo = std::move(bar);
  ],[
  AC_DEFINE([HAVE_TEMPLATE_MOVE],[1],dnl
	    [Defined if the move template is available])
  HAVE_TEMPLATE_MOVE=1
  ],[HAVE_TEMPLATE_MOVE=0])
  AC_SUBST([HAVE_TEMPLATE_MOVE])
  AC_LANG_POP([C++])
])
