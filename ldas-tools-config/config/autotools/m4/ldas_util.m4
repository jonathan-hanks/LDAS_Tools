#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#

LDAS_DIRECTORY_MAKE_INCLUDE=[$(top_srcdir)/config/autotools/make]
AC_SUBST([LDAS_DIRECTORY_MAKE_INCLUDE])

AC_DEFUN([AC_LDAS_VAR_APPEND],[
  case "$2" in
  -*) opt="";;
  *)
    case $1 in
    *LDFLAGS) opt="-L" ;;
    *CXXFLAGS|*CFLAGS) opt="-I" ;;
    *) opt="";;
    esac
    ;;
  esac
  case x$3 in
  x) ;;
  xLITERAL) opt="" ;;
  *) opt="$3" ;;
  esac
  opt="${opt}$2"
  st="[[ |	]]";
  echo "[$]$1" |  egrep "^${opt}${st}|${st}${opt}${st}|${st}$opt[$]|^${opt}[$]"
  if test $? -ne 0
  then
    $1="[$]$1 ${opt}"
  fi
])

AC_DEFUN([AC_LDAS_VAR_PREPEND],[
  case "$2" in
  -*) opt="";;
  *)
    case $1 in
    *LDFLAGS) opt="-L" ;;
    *CXXFLAGS|*CFLAGS) opt="-I" ;;
    *) opt="";;
    esac
    ;;
  esac
  case x$3 in
  x) ;;
  xLITERAL) opt="" ;;
  *) opt="$3"
  esac
  opt="${opt}$2"
  st="[[ |	]]";
  echo "[$]$1" |  egrep "^${opt}${st}|${st}${opt}${st}|${st}${opt}\\[$]|^${opt}\\[$]"
  if test $? -ne 0
  then
    $1="[$]$1 ${opt}"
  fi
])

AC_DEFUN([AC_LDAS_VAR_APPEND_COND],[
  if test $1
  then
    AC_LDAS_VAR_APPEND($2,$3)
  fi
])

AC_DEFUN([AC_LDAS_VAR_PREPEND_COND],[
  if test $1
  then
    AC_LDAS_VAR_PREPEND($2,$3)
  fi
])

