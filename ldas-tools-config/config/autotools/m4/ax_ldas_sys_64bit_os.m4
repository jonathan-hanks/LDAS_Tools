dnl======================================================================
dnl AX_LDAS_SYS_64BIT_OS
dnl   Check to see if 64bit binaries are to be generated
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_SYS_64BIT_OS],[dnl
  dnl ========================================================
  dnl Checking for 64-bit OS
  dnl ========================================================
  AC_LANG_PUSH(C)
  AC_MSG_CHECKING(for 64-bit OS)
  AC_TRY_RUN([
    int main () {
    if (sizeof(void*) == 8) {
      return 0;
    }
    return 1; } ],
    [result="yes"],
    [result="no"],
    [result="maybe"])
  AC_MSG_RESULT("$result")
  AS_IF([test "$result" = "yes"],[
    AC_DEFINE([HAVE_64BIT_OS],[1],[Defined if being compiled as a 64bit executable])
    HAVE_64BIT_OS=1
    AS_CASE([$build_os],dnl
            [solaris*],[dnl
	      AS_CASE([$build_cpu],
	              [x86_64],[LIB_64_DIR="/amd64"],
	              [i86pc],[LIB_64_DIR="/amd64"],
	              [i386],[LIB_64_DIR="/amd64"],
		      [sparc*],[LIB_64_DIR="/sparcv9"])
            ],
	    [linux*],[dnl
	      AS_CASE([$build_cpu],
	              [x86_64],[LIB_64_DIR="64"],
		      [LIB_64_DIR="64"])dnl AS_CASE([$build_cpu])
            ],
	    [LIB_64_DIR="64"] dnl - default
    ) dnl AS_CASE([$build_os])
  ]) dnl AS_IF
  AC_SUBST(HAVE_64BIT_OS)
  AC_SUBST([LIB_64_DIR])
  AC_LANG_POP(C)
]) dnl LDAS_SYS_64BIT_OS
