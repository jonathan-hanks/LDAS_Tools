dnl -*- mode: m4 -*-
dnl----------------------------------------------------------------------
dnl This is a common set of macros to be used in the configuration of
dnl	the LDAS software
dnl----------------------------------------------------------------------
dnl
dnl LICENSE
dnl
dnl  Copyright (C) 2018 California Institute of Technology
dnl
dnl  This program is free software; you may redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 (GPLv2) of the
dnl  License or at your discretion, any later version.
dnl
dnl  This program is distributed in the hope that it will be useful, but
dnl  without any warranty or even the implied warranty of merchantability
dnl  or fitness for a particular purpose. See the GNU General Public
dnl  License (GPLv2) for more details.
dnl
dnl  Neither the names of the California Institute of Technology (Caltech),
dnl  The Massachusetts Institute of Technology (M.I.T), The Laser
dnl  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
dnl  of its contributors may be used to endorse or promote products derived
dnl  from this software without specific prior written permission.
dnl 
dnl  You should have received a copy of the licensing terms for this
dnl  software included in the file LICENSE located in the top-level
dnl  directory of this package. If you did not, you can view a copy at
dnl  http://dcc.ligo.org/M1500244/LICENSE
dnl
dnl



dnl ---------------------------------------------------------------------
dnl  Check for optimization specification
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_ARG_WITH_OPTIMIZATION],[dnl
  AC_ARG_WITH([optimization],
	[AS_HELP_STRING([--with-optimization],
	                [Compiler optimization (extreme|high|medium|low|none)])],
	[],[with_optimization="high"])
	case x$CC in
	x) ;; dnl ignore because it has not been set
	*)
	  case x${with_optimization} in
	  xnone) dnl -- none
	    ldas_prog_cc_optimization=$ldas_prog_cc_optimization_none
            ;;
	  xextreme) dnl -- extreme
            ldas_prog_cc_optimization=$ldas_prog_cc_optimization_extreme
	    ;;
	  xhigh) dnl -- high
	    ldas_prog_cc_optimization=$ldas_prog_cc_optimization_high
   	    ;;
          xlow) dnl -- low
            ldas_prog_cc_optimization=$ldas_prog_cc_optimization_low
            ;;
	  xdefault) dnl -- default
            ldas_prog_cc_optimization=$ldas_prog_cc_optimization_default
            ;;
          *) dnl -- all other cases
	    ldas_prog_cc_optimization=$ldas_prog_cc_optimization_medium
	    ;;
	  esac
	  ;;
	esac
	case x$CXX in
	x) ;; dnl ignore because it has not been set
	*)
	  case x${with_optimization} in
	  xnone) dnl -- none
	    ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_none
            ;;
	  xextreme) dnl -- extreme
            ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_extreme
	    ;;
	  xhigh) dnl -- high
	    ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_high
   	    ;;
          xlow) dnl -- low
            ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_low
            ;;
	  xdefault) dnl -- default
            ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_default
            ;;
          *) dnl -- all other cases
	    ldas_prog_cxx_optimization=$ldas_prog_cxx_optimization_medium
	    ;;
	  esac
	  ;;
	esac
  dnl -------------------------------------------------------------------
  CFLAGS="$LDAS_DEFS $CFLAGS"
  echo "$CFLAGS" | egrep -e ${ldas_prog_cc_optimization_key}'[[0-9]]' > /dev/null 2>&1
  AS_IF([test $? -ne 0],
  	[CFLAGS="$CFLAGS ${ldas_prog_cc_optimization}"],
	[CFLAGS="`echo $CFLAGS | sed -e 's/'${ldas_prog_cc_optimization_key}'[[0-9]]/'${ldas_prog_cc_optimization}'/g'`"])
  dnl --------------------------------------------------------------
  echo "$LDAS_DEFS $CXXFLAGS" | egrep -e ${ldas_prog_cxx_optimization_key}'[[0-9]]' > /dev/null 2>&1
  AS_IF([test $? -ne 0],
        [CXXFLAGS="$LDAS_DEFS $CXXFLAGS ${ldas_prog_cxx_optimization}"],
	[CXXFLAGS="`echo $LDAS_DEFS $CXXFLAGS | sed -e 's/'${ldas_prog_cxx_optimization_key}'[[0-9]]/'${ldas_prog_cxx_optimization}'/g'`"])
  dnl --------------------------------------------------------------
  AC_MSG_CHECKING([optimization for $CC])
  AC_MSG_RESULT([${ldas_prog_cc_optimization}])
  AC_MSG_CHECKING([optimization for $CXX])
  AC_MSG_RESULT([${ldas_prog_cxx_optimization}])
])

dnl======================================================================
dnl ---- Every thing beyond this point is subject to removal.
dnl======================================================================

dnl======================================================================
dnl Get the list of recursive targets automatically supplied by automake
dnl======================================================================

AC_DEFUN([LDAS_CHECK_AUTOMAKE_RECURSIVE_TARGETS],
  [AC_CACHE_CHECK([find recursive targets used by automake],
    [ldas_cv_automake_recursive_targets],
    [ dnl ---------------------------------------------------------------
      dnl  Find all of the recursive targets
      dnl ---------------------------------------------------------------
     ldas_cv_automake_recursive_targets="`egrep -e '^(.*):.*-recursive$' ${srcdir}/Makefile.in | egrep -v -e '^(mostlyclean|clean|distclean|maintainer-clean):' | sed -e 's/:.*$/-recursive/' | tr '\n' ' '`"
    ])
  PROJECT_RECURSIVE_TARGETS="${ldas_cv_automake_recursive_targets} $PROJECT_RECURSIVE_TARGETS"
  AC_SUBST(PROJECT_RECURSIVE_TARGETS)]
)

dnl======================================================================
dnl LDAS_CALC_VERSION - Calculate the version number
dnl======================================================================

AC_DEFUN([LDAS_INIT_AUTOMAKE],
[
if test x$LDAS_VERSION = x
then
	LDAS_VERSION=$2
	export LDAS_VERSION
fi
AM_INIT_AUTOMAKE($1,$2)
])


dnl======================================================================
dnl AC_LDAS_INIT([top]) - Initializes the LDAS macro set. If top is
dnl	passed to the macro, then the current directory will be 
dnl	taken as the top most directory of the LDAS system.
dnl======================================================================

AC_DEFUN([AC_LDAS_INIT],
[
  dnl -------------------------------------------------------------------
  dnl  Get the cononical name of everything
  dnl -------------------------------------------------------------------
  dnl -------------------------------------------------------------------
  dnl  Information about the current system for which LDAS is being
  dnl    configured.
  dnl -------------------------------------------------------------------
  ldas_kernel=`uname -s`
  ldas_processor=`uname -p`
  ldas_hardware=`uname -m`
  dnl -------------------------------------------------------------------
  dnl  Establish the name to use for the dynamic library loader
  dnl -------------------------------------------------------------------
  case ${ldas_kernel} in
  Darwin)
    OS_LIBRARY_PATH=DYLD_LIBRARY_PATH
    ;;
  *)
    OS_LIBRARY_PATH=LD_LIBRARY_PATH
    ;;
  esac
  AC_SUBST([OS_LIBRARY_PATH])
  dnl -------------------------------------------------------------------
  dnl Misc. Info
  dnl -------------------------------------------------------------------
  ldas_top=""
  if test -n "$1" && test "$1" = "top"
  then
    ldas_top="."
    touch .ldas_top
  else
    rm -f .ldas_top > /dev/null 2>&1
    for dir in .. ../.. ../../.. ../../../..
    do
      if test -f ${dir}/.ldas_top
      then
  	ldas_top="${dir}"
  	break
      fi
    done
  fi
  ldas_top_srcdir='[$][(]top_srcdir[)]/'${ldas_top}
  ldas_top_builddir='[$][(]top_builddir[)]/'${ldas_top}
  dnl -------------------------------------------------------------------
  dnl  
  dnl -------------------------------------------------------------------
  AM_CONDITIONAL(LDAS_NEVER_BUILD,test xyes = xno)
  LDAS_ENABLE_LDAS_DOCUMENTATION
  dnl -------------------------------------------------------------------
  dnl  Check if user has custom requests
  dnl -------------------------------------------------------------------
  AC_LDAS_WITH_EXTRA_CXXFLAGS
  AC_LDAS_WITH_EXTRA_LDFLAGS
  AC_LDAS_ENABLE_AUTO_LDCG_INCLUSION
  AC_LDAS_PROG_CC
  LDAS_SYS_64BIT_OS
  dnl -- Automatically add /ldcg into the build
  AS_IF([test "x${enable_auto_ldcg_inclusion}" = xyes -a -d /ldcg/],
  	[AS_IF([test "x${HAVE_64BIT_OS}" = "x1"],
	       [AC_LDAS_VAR_APPEND([LDFLAGS],
	                            [-Wl,${RPATH},/ldcg/lib${LIB_64_DIR}])
  	        AC_LDAS_VAR_APPEND([LDFLAGS],
	                           [-L/ldcg/lib${LIB_64_DIR}])
                AC_LDAS_VAR_APPEND([CFLAGS],[/ldcg/include64])
                AC_LDAS_VAR_APPEND([CXXFLAGS],[/ldcg/include64])
	        AC_MSG_RESULT([Auto LDCG inclusion is 64bit OS])
	       ],
	       [ AC_MSG_RESULT([Auto LDCG inclusion is not 64bit OS])])
         AC_LDAS_VAR_APPEND([CFLAGS],[/ldcg/include])
         AC_LDAS_VAR_APPEND([CXXFLAGS],[/ldcg/include])
         AC_LDAS_VAR_APPEND([LDFLAGS],[-Wl,${RPATH},/ldcg/lib])
         AC_LDAS_VAR_APPEND([LDFLAGS],[-L/ldcg/lib])])
  AC_LDAS_WITH_LDFLAGS
  dnl -------------------------------------------------------------------
  dnl Export meaningful information
  dnl -------------------------------------------------------------------
  AC_SUBST(ldas_top)
  AC_SUBST(ldas_top_srcdir)
  AC_SUBST(ldas_top_builddir)

  dnl -------------------------------------------------------------------
  dnl Some variable that may or may not be set, but will be used.
  dnl -------------------------------------------------------------------
  AC_SUBST(EXTRA_LIBS)
  LDAS_LIB_STLPORT
  dnl -------------------------------------------------------------------
  dnl  Additional recursive targets
  dnl -------------------------------------------------------------------
  LDAS_CHECK_AUTOMAKE_RECURSIVE_TARGETS
  PROJECT_RECURSIVE_TARGETS="memcheck-recursive $PROJECT_RECURSIVE_TARGETS"
  AM_CONDITIONAL(LDAS_BUILD_OS_DARWIN,test x$ldas_kernel = xDarwin)
])

dnl======================================================================
dnl AC_LDAS_FINISH - Things to be done at the very end for LDAS
dnl======================================================================

AC_DEFUN([AC_LDAS_FINISH],
[
  dnl -------------------------------------------------------------------
  dnl Check for large file support
  dnl -------------------------------------------------------------------
  AC_SYS_LARGEFILE
  dnl -------------------------------------------------------------------
  dnl -- Check for documentation tools
  dnl -------------------------------------------------------------------
  LDAS_PROG_DOXYGEN
  dnl -------------------------------------------------------------------
  dnl -- Check for the presence of Valgrind
  dnl -------------------------------------------------------------------
  LDAS_PROG_VALGRIND
  LDAS_LIB_UMEM
  LDAS_LIB_STLPORT_FINISH
  AC_SUBST(LDAS_EMPTY)
  dnl ===================================================================
  dnl  Specify memory checker
  dnl ===================================================================
  AC_ARG_WITH([memory-checking],
	      [AS_HELP_STRING([--with-memory-checking[[=no]]],
			      [enable extended runtime memory checking])],
	      [],
              [with_memory_checking="no"])
  case x$with_memory_checking in
  xno)
    AC_MSG_CHECKING(for memory checking)
    AC_MSG_RESULT(no)
    ;;
  x|xyes)
    AC_LANG_PUSH(C++)
    AC_MSG_CHECKING(for memory checking C++)
    ldas_memory_checking_flags=""
    ldas_orig_CXXFLAGS="$CXXFLAGS"
    dnl -----------------------------------------------------------------
    dnl  -fmudflapth
    dnl -----------------------------------------------------------------
    CXXFLAGS="-fmudflapth $ldas_orig_CXXFLAGS"
    AC_RUN_IFELSE([AC_LANG_PROGRAM([],[int main( ) { return( 0 ); }])],
	          [ ldas_memory_checking_flags="-fmudflapth $ldas_memory_checking_flags" ],[],[])
    dnl -----------------------------------------------------------------
    dnl  -bounds-check
    dnl -----------------------------------------------------------------
    CXXFLAGS="-fbounds-check $ldas_orig_CXXFLAGS"
    AC_RUN_IFELSE([AC_LANG_PROGRAM([],[int main( ) { return( 0 ); }])],
	          [ ldas_memory_checking_flags="-fbounds-check $ldas_memory_checking_flags" ],[],[])
    dnl -----------------------------------------------------------------
    dnl  -stack-check
    dnl -----------------------------------------------------------------
    CXXFLAGS="-fstack-check $ldas_orig_CXXFLAGS"
    AC_RUN_IFELSE([AC_LANG_PROGRAM([],[int main( ) { return( 0 ); }])],
	          [ ldas_memory_checking_flags="-fstack-check $ldas_memory_checking_flags" ],[],[])
    dnl -----------------------------------------------------------------
    dnl Finish with memory checks
    dnl -----------------------------------------------------------------
    CXXFLAGS="$ldas_memory_checking_flags $ldas_orig_CXXFLAGS"
    AC_MSG_RESULT($ldas_memory_checking_flags)
    AC_LANG_POP(C++)
    AC_LANG_PUSH(C)
    AC_MSG_CHECKING(for memory checking C)
    ldas_memory_checking_flags=""
    ldas_orig_CFLAGS="$CFLAGS"
    dnl -----------------------------------------------------------------
    dnl  -fmudflapth
    dnl -----------------------------------------------------------------
    CFLAGS="-fmudflapth $ldas_orig_CFLAGS"
    AC_RUN_IFELSE([AC_LANG_PROGRAM([],[int main( ) { return( 0 ); }])],
	          [ ldas_memory_checking_flags="-fmudflapth $ldas_memory_checking_flags" ],[],[])
    dnl -----------------------------------------------------------------
    dnl  -bounds-check
    dnl -----------------------------------------------------------------
    CFLAGS="-fbounds-check $ldas_orig_CFLAGS"
    AC_RUN_IFELSE([AC_LANG_PROGRAM([],[int main( ) { return( 0 ); }])],
	          [ ldas_memory_checking_flags="-fbounds-check $ldas_memory_checking_flags" ],[],[])
    dnl -----------------------------------------------------------------
    dnl  -stack-check
    dnl -----------------------------------------------------------------
    CFLAGS="-fstack-check $ldas_orig_CFLAGS"
    AC_RUN_IFELSE([AC_LANG_PROGRAM([],[int main( ) { return( 0 ); }])],
	          [ ldas_memory_checking_flags="-fstack-check $ldas_memory_checking_flags" ],[],[])
    dnl -----------------------------------------------------------------
    dnl Finish with memory checks
    dnl -----------------------------------------------------------------
    CFLAGS="$ldas_memory_checking_flags $ldas_orig_CFLAGS"
    AC_MSG_RESULT($ldas_memory_checking_flags)
    AC_LANG_POP(C)
    ;;
  esac
  dnl ===================================================================
  dnl  Specify Arch optimiations
  dnl ===================================================================
  AC_MSG_CHECKING(for architecture optimization)
  AC_ARG_WITH([opt-arch],
	      [AS_HELP_STRING([--with-opt-arch[[=no]]],
	                      [enable cpu archtecture optimization])],
                [],
                [with_opt_arch="no"])
  case x$with_opt_arch in
  xno)
    ;;
  x|xyes)
    case "`uname -m`" in
    i686)
      opt_arch="pentium4";
      ;;
    sun4u)
      opt_arch="v8plus";
      ;;
    esac
    ;;
  *)
    opt_arch="$with_opt_arch";
    ;;
  esac
  case x$with_opt_arch in
  xno)
    ;;
  *)
    case x$LDAS_CC_FLAVOR in
    xgnu)
      case "`uname -m`" in
      i*86)
        opt_arch_flag="-march=";
        ;;
      sun4u)
        opt_arch_flag="-mfpu -mno-unaligned-doubles -Wa,-xarch=v8plusb -m";
        ;;
      esac
      CFLAGS="$opt_arch_flag$opt_arch $CFLAGS"
      ;;
    xSunPRO)
      case x$opt_arch in
      xv8plus)
        CFLAGS="-xarch=${opt_arch}a $CFLAGS"
        ;;
      esac
      ;;
    esac
    case x$LDAS_CXX_FLAVOR in
    xgnu)
      case "`uname -m`" in
      i*86)
        opt_arch_flag="-march=";
        ;;
      sun4u)
        opt_arch_flag="-mfpu -mno-unaligned-doubles -Wa,-xarch=v8plusb -m";
        ;;
      esac
      CXXFLAGS="$opt_arch_flag$opt_arch $CXXFLAGS"
      ;;
    xSunPRO)
      case x$opt_arch in
      xv8plus)
        CXXFLAGS="-xarch=${opt_arch}a $CXXFLAGS"
        ;;
      esac
      ;;
    esac
    ;;
  esac
  case x$opt_arch in
  x)
    AC_MSG_RESULT(none)
    ;;
  *)
    AC_MSG_RESULT($opt_arch)
    ;;
  esac

  dnl ===================================================================
  dnl  Specify warning message level
  dnl ===================================================================
  CFLAGS="${ldas_prog_cc_warning} ${CFLAGS}"
  CXXFLAGS="${ldas_prog_cxx_warning} ${CXXFLAGS}"

if test "x$LDAS_LIBTOOL_VERSION_FLAG" = x
then
  LDAS_LIBTOOL_VERSION_FLAG="`echo ${PACKAGE_VERSION_NUMERIC} | sed -e 's/\([[0-9]][[0-9]]*\)\.\([[0-9]][[0-9]]*\)\.\([[0-9]][[0-9]]*\)$/-release \1 -version-info \2:\3:0/g'`";
  export LDAS_LIBTOOL_VERSION_FLAG;
fi
if test "x$LDAS_SO" = x
then
  LDAS_SO="`echo ${PACKAGE_VERSION_NUMERIC} | sed -e 's/\([[0-9]][[0-9]]*\)\.\([[0-9]][[0-9]]*\)\.\([[0-9]][[0-9]]*\)[[A-Za-z]]*/-\1.so.\2.0.\3/g'`";
  export LDAS_SO;
fi
dnl =====================================================================
dnl  Optimization
dnl =====================================================================
AC_LDAS_ARG_WITH_OPTIMIZATION
dnl -----------------------------------------------------------------------
dnl -- figure out which directories really exist
dnl -----------------------------------------------------------------------
if test -n "${subdirs}"
then
  for dir in ${subdirs}
  do
    if test -d ${srcdir}/${dir}
    then
      ldas_finish_subdirs="${ldas_finish_subdirs} ${dir}"
    fi
  done
  subdirs="${ldas_finish_subdirs}"
fi

AC_REQUIRE([AC_LDAS_CHECK_LIB_THREAD])

dnl -- check for unrolling of inlined functions
AC_ARG_ENABLE(inlining,
	[AS_HELP_STRING([--disable-inlining],
	  	        [Build without inlining])],
	[],
	[enable_inlining=default])

case x${enable_inlining} in
xno)
  if test "${GCC}" = "yes"
  then
    CFLAGS="${CFLAGS} -fno-inline"
  fi
  if test "${GXX}" = "yes"
  then
    CXXFLAGS="${CXXFLAGS} -fno-inline"
  fi
  ;;
xyes)
  if test "${GCC}" = "yes"
  then
    FLAGS="${CFLAGS} -finline"
  fi
  if test "${GXX}" = "yes"
  then
    CXXFLAGS="${CXXFLAGS} -finline"
  fi
  ;;
*)
  ;;
esac

dnl -- rearrange prefix directories

CXXFLAGS="`echo $CXXFLAGS |  tr ' \011' '\012\012' | grep -v -- '-[[IL]]'${prefix} |  tr '\012' ' '``echo $CXXFLAGS | tr ' \011' '\012\012' | grep -- '-[[IL]]'${prefix} | sort | uniq | tr '\012' ' '`"
CPPFLAGS="`echo $CPPFLAGS |  tr ' \011' '\012\012' | grep -v -- '-[[IL]]'${prefix} |  tr '\012' ' '``echo $CPPFLAGS | tr ' \011' '\012\012' | grep -- '-[[IL]]'${prefix} | sort | uniq | tr '\012' ' '`"
LDFLAGS="`echo $LDFLAGS |  tr ' \011' '\012\012' | grep -v -- '-[[IL]]'${prefix} |  tr '\012' ' '``echo $LDFLAGS | tr ' \011' '\012\012' | grep -- '-[[IL]]'${prefix} | sort | uniq | tr '\012' ' '`"

AC_LDAS_WITH_PROFILE
AC_LDAS_WITH_LDAS_TARGET
AC_LDAS_WITH_MOUNT_PT
AC_SUBST(LDAS_PACKAGE_NAME)
AC_SUBST(PACKAGE_VERSION)
AC_SUBST(PACKAGE_VERSION_NUMERIC)
AC_SUBST(LDAS_LIBTOOL_VERSION_FLAG)
AC_SUBST(LDAS_SO)
  AM_CONDITIONAL(LDAS_STATIC_BUILD,test x${enable_shared} = xno)
]) dnl AC_LDAS_FINISH

dnl======================================================================
dnl Checking for the existence of certain libraries
dnl======================================================================


   
dnl======================================================================
dnl Checking arguments that were passed to configure
dnl======================================================================

dnl----------------------------------------------------------------------
dnl Routines for appending to certain variables
dnl----------------------------------------------------------------------

AC_DEFUN([AC_LDAS_PREFIX_LDFLAGS],
[ for lib in $1
  do
    AC_LDAS_VAR_PREPEND(LDFLAGS,${lib})
  done
])

AC_DEFUN([AC_LDAS_PREFIX_INCLUDE_PATH],
[ for inc in $1
  do
    AC_LDAS_VAR_APPEND(CPPFLAGS,${inc},-I)
  done
])

dnl======================================================================
dnl AC_LDAS_ENABLE_AUTO_LDCG_INCLUSION
dnl======================================================================

AC_DEFUN([AC_LDAS_ENABLE_AUTO_LDCG_INCLUSION],
	 [AC_ARG_ENABLE([auto-ldcg-inclusion],
			[AS_HELP_STRING([--enable-auto-ldcg-inclusion],
			                [enables/disables inclusion of /ldcg])],
                        , dnl Action if true
	                [enable_auto_ldcg_inclusion="yes"])])

dnl======================================================================
dnl AC_LDAS_ENABLE_AUTO_LDCG_DOUMENTATION
dnl======================================================================

AC_DEFUN([LDAS_ENABLE_LDAS_DOCUMENTATION],
	 [AC_ARG_ENABLE([ldas-documentation],
			[AS_HELP_STRING([--enable-ldas-documentation],
			                [enables/disables generation of LDAS documentation])],
                        , dnl Action if true
	                [enable_ldas_documentation="yes"])

  AM_CONDITIONAL([LDAS_ENABLED_LDAS_DOCUMENTATION],
	         [test x$enable_ldas_documentation = xyes])
])

dnl======================================================================
dnl AC_LDAS_WITH_MOUNTPT
dnl======================================================================

AC_DEFUN([AC_LDAS_WITH_MOUNT_PT],
[AC_ARG_WITH(
	[mountpt], 
	[AS_HELP_STRING([--with-mountpt],
          		[Specify frame builder directory])],
	[ 
		MOUNT_PT="$mountpt";
	],[
		MOUNT_PT="/imports/frames";
	])
	AC_SUBST(MOUNT_PT)
])

dnl======================================================================
dnl AC_LDAS_WITH_CCMALLOC
dnl======================================================================

AC_DEFUN([AC_LDAS_WITH_CCMALLOC],
[AC_ARG_WITH(
	[ccmalloc], 
	[AS_HELP_STRING([--with-ccmalloc],
	         	[Link to ccmalloc when running tests])],
	[ AC_CHECK_LIB( 
		ccmalloc,
		main,
		[ EXTRA_TEST_FLAGS='-lccmalloc -ldl' ],
		[ AC_MSG_WARN( ccmalloc not found )  ])
	])
	AC_SUBST(EXTRA_TEST_FLAGS)
])

dnl======================================================================
dnl AC_LDAS_WITH_EXTRA_CXXFLAGS
dnl======================================================================

AC_DEFUN([AC_LDAS_WITH_EXTRA_CXXFLAGS],
[AC_ARG_WITH(
	[extra_cxxflags], 
	[  --with-extra-cxxflags   Additional C++ compiler flags],
	[ if test -n "${with_extra_cxxflags}"
	  then
	    AC_LDAS_VAR_APPEND(CXXFLAGS,${with_extra_cxxflags},LITERAL)
	  fi
	],)
])

dnl======================================================================
dnl AC_LDAS_WITH_EXTRA_LDFLAGS
dnl======================================================================

AC_DEFUN([AC_LDAS_WITH_EXTRA_LDFLAGS],
[AC_ARG_WITH(
	extra_ldflags, 
	[  --with-extra-ldflags    Additional linker flags],
	[ if test -n "${with_extra_ldflags}"
	  then
	    LDFLAGS="$LDFLAGS ${with_extra_ldflags}";
	  fi
	],)
])


dnl======================================================================
dnl AC_LDAS_WITH_LDFLAGS
dnl======================================================================

AC_DEFUN([AC_LDAS_WITH_LDFLAGS],
[AC_ARG_WITH(
	ldflags, 
	[  --with-ldflags          Replace linker flags],
	[ if test -n "${with_ldflags}"
	  then
	    LDFLAGS="${with_ldflags}";
	  fi
	],)
])

dnl======================================================================
dnl AC_LDAS_WITH_LDAS_TARGET
dnl======================================================================

AC_DEFUN([AC_LDAS_WITH_LDAS_TARGET],
[AC_ARG_WITH(
	ldas_target,
	[  --with-ldas-target=[production|development|system_test]
                          Create target specific LDAS resource file ],
	[ case "${with_ldas_target}" in
	  production) BASE_PORT="11000";;
	  system_test) BASE_PORT="13000";;
	  development|*) BASE_PORT="12000";;
	  esac
	],[BASE_PORT="12000"])
	AC_SUBST(BASE_PORT)
])

dnl======================================================================
dnl AC_LDAS_WITH_PROFLE
dnl======================================================================

AC_DEFUN([AC_LDAS_WITH_PROFILE],
[AC_ARG_WITH(
	profile, 
	[  --with-profile=[prof|gprof] 
                          Compile for profiling ],
	[ if test "${with_profile}" = "prof"
	  then
	    CXXFLAGS="$CXXFLAGS -p";
	  elif  test "${with_profile}" = "gprof"
	  then
	    CXXFLAGS="$CXXFLAGS -pg";
	  else
	    AC_MSG_ERROR( Supported profiling modes: prof gprof. );
	  fi
	],)
])


dnl----------------------------------------------------------------------
dnl Check for ldas package directory
dnl AC_LDAS_CHECK_PACKAGE_DIR
dnl----------------------------------------------------------------------

AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DIR],
[
  define([ldas_tmp_var_have_src_dir],patsubst(translit($2,[A-Z],[a-z]),[^],[ac_ldas_have_src_dir_]))
  AS_IF([test "x$with_$2" != xno],
  	[ #
          if test -d ${srcdir}/${ldas_top}/$1
  	  then
    	    ldas_tmp_var_have_src_dir=yes
          fi
	  #--------------------------------------------------------------
	  # Check to see if the package is available
	  #--------------------------------------------------------------
	  if test "x$with_$2" != xcheck -a x$ldas_tmp_var_have_src_dir != xyes
	  then
            AC_MSG_FAILURE([--with-$2 was given, but test for directory $2 failed])
          fi
	  AS_IF([test x$ldas_tmp_var_have_src_dir != xyes],
	        [dnl ----------------------------------------------------
	    	 dnl  The with option was checking and now it is known
	    	 dnl    to be false, so record for later.
	    	 dnl ----------------------------------------------------
	         with_$2="no"])
	  ])
  define([ldas_tmp_var_have_package_dir],patsubst(translit($2,[a-z],[A-Z]),[^],[HAVE_PACKAGE_DIR_]))
  AM_CONDITIONAL(ldas_tmp_var_have_package_dir, test x$ldas_tmp_var_have_src_dir = xyes)
  undefine([ldas_tmp_var_have_package_dir])
  undefine([ldas_tmp_var_have_src_dir])

])

dnl----------------------------------------------------------------------
dnl Check for ldas packages
dnl AC_LDAS_CHECK_PACKAGE(SourceDir,LaFileName,[InstallDir])
dnl----------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_PACKAGE],
[
  define([ldas_tmp_var],patsubst(translit(patsubst($2,[^lib],[]),[A-Z],[a-z]),[[.]],[_]))
  define([ldas_tmp_var_pkg],translit(patsubst(patsubst($2,[^lib],[]),[.la$],[]),[a-z],[A-Z]))
  AC_LDAS_CHECK_PACKAGE_DIR($1,ldas_tmp_var_pkg)
  if test -d ${ldas_top}/$1
  then
    ac_ldas_check_package_inc='[$][(]ldas_top_builddir[)]/include [$][(]ldas_top_builddir[)]/$1';
    ac_ldas_check_package_lib='[$][(]ldas_top_builddir[)]/$1/$2';
  elif test -d ${prefix}/include/$3 && test -d ${prefix}/lib/$3
  then
    ac_ldas_check_package_inc=${prefix}/include;
    ac_ldas_check_package_lib=${prefix}/lib/$3/$2;
  else
    ac_ldas_check_package_inc="";
    ac_ldas_check_package_lib="";
  fi
  #----------------------------------------------------------------------
  # Check to see if the package is available
  #----------------------------------------------------------------------
  if test -n "${ac_ldas_check_package_inc}" \
	&& test -n "${ac_ldas_check_package_lib}"
  then
    AC_LDAS_PREFIX_INCLUDE_PATH(${ac_ldas_check_package_inc})
    ldas_tmp_var=${ac_ldas_check_package_lib}
    AC_SUBST(ldas_tmp_var)
  fi
  undefine([ldas_tmp_var_pkg])
  undefine([ldas_tmp_var])
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_DIR_DBACCESS
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DIR_DBACCESS],
[ AC_ARG_WITH([dbaccess],
              [AS_HELP_STRING([--with-dbaccess],
              [compile dbaccess support @<:@default=check@:>@])],
              [],
	      [with_dbaccess=check])
  AC_LDAS_CHECK_PACKAGE_DIR(lib/dbaccess/src,dbaccess)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_DBACCESS
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DBACCESS],
[ AC_LDAS_CHECK_PACKAGE_DIR_DBACCESS
  AS_IF([test "x$with_dbaccess" != xno],
	[ LDAS_CHECK_PACKAGE(lib/dbaccess/src,libdbaccess.la)
          if test "x$with_dbaccess" != xcheck -a x$dbaccess_la = x ; then
            AC_MSG_FAILURE([--with-dbaccess was given, but test for dbaccess failed])
          fi
          if test x$dbaccess_la != x
          then
            AC_DEFINE([HAVE_LDAS_PACKAGE_DBACCESS],[1],dnl
	              [Defined if the LDAS DBACCESS package is available])
	  else
	    with_dbaccess=no
          fi])
  AM_CONDITIONAL(HAVE_PACKAGE_DBACCESS, test x$dbaccess_la != x)
])

AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DIR_FILTERS],
[ AC_ARG_WITH([filters],
              [AS_HELP_STRING([--with-filters],
              [compile filters support @<:@default=check@:>@])],
              [],
              [with_filters=check])
  AC_LDAS_CHECK_PACKAGE_DIR(lib/filters/src,filters)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_FILTERS
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_FILTERS],
[ AC_LDAS_CHECK_PACKAGE_DIR_FILTERS
  AS_IF([test "x$with_filters" != xno],
	[ LDAS_CHECK_PACKAGE(lib/filters/src,libfilters.la)
          if test "x$with_filters" != xcheck -a x$filters_la = x ; then
            AC_MSG_FAILURE([--with-filters was given, but test for filters failed])
          fi
          if test x$filters_la != x
          then
            AC_DEFINE([HAVE_LDAS_PACKAGE_FILTERS],[1],dnl
	              [Defined if the LDAS FILTERS package is available])
	  else
	    with_filters=no
          fi])
  AM_CONDITIONAL(HAVE_PACKAGE_FILTERS, test x$filters_la != x)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_DIR_FRAMECPP
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DIR_FRAMECPP],
[ AC_ARG_WITH([framecpp],
              [AS_HELP_STRING([--with-framecpp],
              [compile framecpp support @<:@default=check@:>@])],
              [],
              [with_framecpp=check])
  AC_LDAS_CHECK_PACKAGE_DIR(lib/framecpp/src/OOInterface,framecpp)
])

AC_DEFUN([AC_LDAS_CHECK_PACKAGE_FRAMECPP],
[ AC_LDAS_CHECK_PACKAGE_DIR_FRAMECPP
  AS_IF([test "x$with_framecpp" != xno],
	[ LDAS_CHECK_PACKAGE(lib/framecpp/src/OOInterface,libframecpp.la)
          if test "x$with_framecpp" != xcheck -a x$framecpp_la = x ; then
            AC_MSG_FAILURE([--with-framecpp was given, but test for framecpp failed])
          fi
          if test x$framecpp_la != x
          then
            AC_DEFINE([HAVE_LDAS_PACKAGE_FRAMECPP],[1],dnl
	              [Defined if the LDAS FRAMECPP package is available])
	  else
	    with_framecpp=no
          fi])
  AM_CONDITIONAL(HAVE_PACKAGE_FRAMECPP, test x$framecpp_la != x)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_DIR_ILWD
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DIR_ILWD],
[ AC_ARG_WITH([ilwd],
              [AS_HELP_STRING([--with-ilwd],
              [compile ilwd support @<:@default=check@:>@])],
              [],
              [with_ilwd=check])
  AC_LDAS_CHECK_PACKAGE_DIR(lib/ilwd/src,ilwd)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_ILWD
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_ILWD],
[ AC_LDAS_CHECK_PACKAGE_DIR_ILWD
  AS_IF([test "x$with_ilwd" != xno],
	[ LDAS_CHECK_PACKAGE(lib/ilwd/src,libilwd.la)
          if test "x$with_ilwd" != xcheck -a x$ilwd_la = x ; then
            AC_MSG_FAILURE([--with-ilwd was given, but test for ilwd failed])
          fi
          if test x$ilwd_la != x
          then
            AC_DEFINE([HAVE_LDAS_PACKAGE_ILWD],[1],dnl
	              [Defined if the LDAS ILWD package is available])
	  else
	    with_ilwd=no
          fi])
  AM_CONDITIONAL(HAVE_PACKAGE_ILWD, test x$ilwd_la != x)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_DIR_ILWDFCS
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DIR_ILWDFCS],
[ AC_ARG_WITH([ilwdfcs],
              [AS_HELP_STRING([--with-ilwdfcs],
              [compile ilwdfcs support @<:@default=check@:>@])],
              [],
              [with_ilwdfcs=check])
  AC_LDAS_CHECK_PACKAGE_DIR(lib/ilwdfcs/src,ilwdfcs)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_ILWDFCS
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_ILWDFCS],
[ AC_LDAS_CHECK_PACKAGE_DIR_ILWDFCS
  AS_IF([test "x$with_ilwdfcs" != xno],
	[ LDAS_CHECK_PACKAGE(lib/ilwdfcs/src,libilwdfcs.la)
          if test "x$with_ilwdfcs" != xcheck -a x$ilwdfcs_la = x ; then
            AC_MSG_FAILURE([--with-ilwdfcs was given, but test for ilwdfcs failed])
          fi
          if test x$ilwdfcs_la != x
          then
            AC_DEFINE([HAVE_LDAS_PACKAGE_ILWDFCS],[1],dnl
	              [Defined if the LDAS ILWDFCS package is available])
	  else
	    with_ilwdfcs=no
          fi])
  AM_CONDITIONAL(HAVE_PACKAGE_ILWDFCS, test x$ilwdfcs_la != x)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_DIR_MIME
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_DIR_MIME],
[ AC_ARG_WITH([mime],
              [AS_HELP_STRING([--with-mime],
              [compile mime support @<:@default=check@:>@])],
              [],
              [with_mime=check])
  AC_LDAS_CHECK_PACKAGE_DIR(lib/mime/src,mime)
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_MIME
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CHECK_PACKAGE_MIME],
[ AC_LDAS_CHECK_PACKAGE_DIR_MIME
          
  AS_IF([test "x$with_mime" != xno],
	[ LDAS_CHECK_PACKAGE(lib/mime/src,libmime.la)
          if test "x$with_mime" != xcheck -a x$mime_la = x ; then
            AC_MSG_FAILURE([--with-mime was given, but test for mime failed])
          fi
          if test x$mime_la != x
          then
            AC_DEFINE([HAVE_LDAS_PACKAGE_MIME],[1],dnl
	              [Defined if the LDAS MIME package is available])
	  else
	    with_mime=no
          fi])
  AM_CONDITIONAL(HAVE_PACKAGE_MIME, test x$mime_la != x)
])

dnl----------------------------------------------------------------------
dnl LDAS_CHECK_PACKAGE_GENERIC_API
dnl----------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_PACKAGE_GENERIC_API],
[ LDAS_CHECK_PACKAGE(api/genericAPI/so/src,libgenericAPI.la,genericAPI)
  AM_CONDITIONAL(HAVE_PACKAGE_GENERIC_API, test x$genericapi_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,libgenericCore.la,genericCore)
  AM_CONDITIONAL(HAVE_PACKAGE_GENERIC_CORE, test x$genericcore_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,libgenericILwd.la,genericILwd)
  AM_CONDITIONAL(HAVE_PACKAGE_GENERIC_ILWD, test x$genericilwd_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,libApiCommon.la)
  AM_CONDITIONAL(HAVE_PACKAGE_API_COMMON, test x$apicommon_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,libApiCommonNS.la)
  AM_CONDITIONAL(HAVE_PACKAGE_API_COMMON_NS, test x$apicommonns_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,libStat.la)
  AM_CONDITIONAL(HAVE_PACKAGE_STAT, test x$Stat_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,libtcltid.la)
  AM_CONDITIONAL(HAVE_PACKAGE_TCL_TID, test x$tcltid_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,liblog_mt.la)
  AM_CONDITIONAL(HAVE_PACKAGE_LOG_MT, test x$log_mt_la != x)
  LDAS_CHECK_PACKAGE(api/genericAPI/so/src,liblog_python.la)
  AM_CONDITIONAL(HAVE_PACKAGE_LOG_PYTHON, test x$log_python_la != x)
  if test x$genericAPI_la != x
  then
    AC_DEFINE([HAVE_LDAS_PACKAGE_GENERIC_API],[1],dnl
	[Defined if the LDAS genericAPI package is available])
  fi
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_DISKCACHE_API
dnl----------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_PACKAGE_DISKCACHE_API],
[ LDAS_CHECK_PACKAGE(api/diskcacheAPI/so/src,libdiskcacheAPI.la,diskcacheAPI)
  AM_CONDITIONAL(HAVE_PACKAGE_DISKCACHE_API, test x$diskcacheapi_la != x)
  LDAS_CHECK_PACKAGE(api/diskcacheAPI/so/src,libdiskcache.la,diskcache)
  AM_CONDITIONAL(HAVE_PACKAGE_DISKCACHE, test x$diskcache_la != x)
  if test x$diskcacheAPI_la != x
  then
    AC_DEFINE([HAVE_LDAS_PACKAGE_DISKCACHE_API],[1],dnl
	[Defined if the LDAS diskcacheAPI package is available])
  fi
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CHECK_PACKAGE_FRAME_API
dnl----------------------------------------------------------------------
AC_DEFUN([LDAS_CHECK_PACKAGE_FRAME_API],
[ LDAS_CHECK_PACKAGE(api/frameAPI/so/src,libframeAPI.la,frameAPI)
  AM_CONDITIONAL(HAVE_PACKAGE_FRAME_API, test x$frameapi_la != x)
  LDAS_CHECK_PACKAGE(api/frameAPI/so/src,libframeAPICore.la,frameAPICore)
  AM_CONDITIONAL(HAVE_PACKAGE_FRAME_API_CORE, test x$frameapicore_la != x)
  if test x$frameAPI_la != x
  then
    AC_DEFINE([HAVE_LDAS_PACKAGE_FRAME_API],[1],dnl
	[Defined if the LDAS frameAPI package is available])
  fi
])

dnl----------------------------------------------------------------------
dnl AC_LDAS_CONFIG_SUBDIRS
dnl----------------------------------------------------------------------
AC_DEFUN([AC_LDAS_CONFIG_SUBDIRS],
[ if test x${ac_ldas_have_src_dir_$1} = xyes
  then
    AC_CONFIG_SUBDIRS($1)
  fi
])

dnl======================================================================
dnl Misc ...
dnl======================================================================

AC_DEFUN([AC_LDAS_LINK_CONFIG],
[ if test ! -f ${ldas_top}/include/$1/"`basename $2`"
  then
    if test ! -d "${ldas_top}/include/$1"
    then
      mkdir -p "${ldas_top}/include/$1"
    fi
    dest=${ldas_top}/include/$1/"`basename $2`"
    if test -f ${dest} -o -d ${dest} -o -h ${dest}
    then
	rm -rf ${dest}
    fi
    ln -s "`pwd`/$2" ${dest}
  fi
  AC_LDAS_PREFIX_INCLUDE_PATH('[$][(]ldas_top_builddir[)]/include')
])

dnl----------------------------------------------------------------------
dnl :TODO: This macro doesn't work
dnl AM_LDAS_VARIABLE_SET(VARIABLE [, ACTION-IF-FOUND
dnl	 [, ACTION-IF-NOT-FOUND]])
dnl----------------------------------------------------------------------

AC_DEFUN([AM_LDAS_VARIABLE_SET],
[ if test -n [$]$1;
  then if [$2]; then "$2"; fi
  else if [$3]; then "$3"; fi])


AC_DEFUN([AC_LDAS_CHECK_LIB],
[ ac_save_LIBS="$LIBS"
  LIBS="$1  $LIBS"
  cat > conftest.$ac_ext <<EOF
#line 2304 "configure"
#include "confdefs.h"

int main() {
$2()
; return 0; }
EOF
  if { (eval echo configure:2311: \"$ac_link\") 1>&5; (eval $ac_link) 2>&5; } && test -s conftest${ac_exeext}; then
    rm -rf conftest*
    $3
  else
    echo "configure: failed program was:" >&5
    cat conftest.$ac_ext >&5
    rm -rf conftest*
    $4
  fi
  rm -f conftest*
])


dnl ---------------------------------------------------------------------
dnl CUS_PATH_ODBC_DIRECT
dnl ---------------------------------------------------------------------

AC_DEFUN([CUS_PATH_ODBC_DIRECT],
[test -z "$odbc_direct_test_library" && odbc_direct_test_library=iodbc
test -z "$odbc_direct_test_func" && odbc_direct_test_func=SQLAllocEnv
test -z "$odbc_test_include" && odbc_direct_test_include=isql.h
  for ac_dir in		\
    /ldcg/include \
    ; \
  do
    if test -r "$ac_dir/$odbc_direct_test_include"; then
      no_odbc= ac_odbc_includes=$ac_dir;
      break;
    fi
  done

# Check for the libraries.
# See if we find them without any special options.
# Don't add to $LIBS permanently.

ac_save_LIBS="$LIBS"
LIBS="-l$odbc_direct_test_library $LIBS"
# First see if replacing the include by lib works.
for ac_dir in `echo "$ac_odbc_includes" | sed s,include,lib${LIB_64_DIR},` \
  /ldcg/lib${LIB_64_DIR} \
  ; \
do
  for ac_extension in a so sl; do
    if test -r $ac_dir/lib${odbc_direct_test_library}.$ac_extension; then
      no_odbc= ac_odbc_libraries=$ac_dir
      break 2
    fi
  done
done
LIBS=$ac_save_LIBS])

dnl ---------------------------------------------------------------------
dnl AC_LDAS_ODBC_FILES
dnl ---------------------------------------------------------------------

AC_DEFUN([AC_LDAS_ODBC_FILES],
[AC_REQUIRE_CPP()dnl

AC_ARG_WITH(odbc-includes, [  --with-odbc-includes=DIR
                          ODBC include files are in DIR])
if test -z "$with_odbc_includes"; then
  odbc_includes=NONE
else
  odbc_includes=$with_odbc_includes
fi
AC_ARG_WITH(odbc-libraries, [  --with-odbc-libraries=DIR
                          ODBC library files are in DIR])
if test -z "$with_odbc_libraries"; then
  odbc_libraries=NONE
else
  odbc_libraries=$with_odbc_libraries
fi
AC_ARG_WITH(odbc-dm, [  --with-odbc-dm=ODBC_DRIVER_MANAGER
                          Specify the flavor of the ODBC Driver Manager (DB2 or IODBC)])
if test -z "$with_odbc_dm"; then
  odbc_dm=IODBC
else
  odbc_dm=$with_odbc_dm
fi
case ${odbc_dm} in
  DB2)
    ;;
  IODBC)
    ;;
  *)
    # previously ERROR level message
    AC_MSG_WARN(Unsupported ODBC Driver Manager \`${odbc_dm}'; --with-odbc-dm option);
    ;;
esac
ODBC_DM="$odbc_dm"
AC_SUBST(ODBC_DM)

AC_MSG_CHECKING(for ODBC)
AC_ARG_WITH(odbc, [  --with-odbc             enable ODBC tests])
if test "x$with_odbc" = xno; then
  no_odbc=yes
else
  if test "x$odbc_includes" != xNONE && test "x$odbc_libraries" != xNONE; then
    no_odbc=
  else
    AC_CACHE_VAL(ac_cv_path_odbc,
      [# One or both of these vars are not set, and ther is no cached value.
      no_odbc=yes
      CUS_PATH_ODBC_DIRECT

      if test "$no_odbc" = yes; then
        ac_cv_path_odbc="no_odbc=yes"
      else
        ac_cv_path_odbc="no_odbc= ac_odbc_includes=$ac_odbc_includes ac_odbc_libraries=$ac_odbc_libraries"
      fi
      ])dnl
  fi
  eval "$ac_cv_path_odbc"
fi

if test "$no_odbc" = yes; then
  AC_MSG_RESULT(no)
  if  test "$odbc_dm" = "IODBC" ; then
    # previously ERROR level message
    AC_MSG_WARN(ODBC files not found)
  fi
else
  test "x$odbc_includes" = xNONE && odbc_includes=$ac_odbc_includes
  test "x$odbc_libraries" = xNONE && odbc_libraries=$ac_odbc_libraries
  ac_cv_path_odbc="no_odbc= ac_odbc_includes=$odbc_includes ac_odbc_libraries=$odbc_libraries"
  AC_MSG_RESULT([libraries $odbc_libraries, headers $odbc_includes])
fi

if test -n "$odbc_includes"; then
  IODBC_CFLAGS="-DIODBC -I$odbc_includes"
fi
AC_SUBST(IODBC_CFLAGS)
if test -n "$odbc_libraries"; then
  IODBC_LDFLAGS="-Wl,${RPATH},$odbc_libraries -L$odbc_libraries"
  IODBC_LIBS="-liodbc"
fi
AC_SUBST(IODBC_LDFLAGS)
AC_SUBST(IODBC_LIBS)
])

dnl ---------------------------------------------------------------------
dnl CUS_PATH_DB2_DIRECT
dnl ---------------------------------------------------------------------

AC_DEFUN([CUS_PATH_DB2_DIRECT],
[
test -z "$db2_direct_test_func" && db2_direct_test_func=SQLAllocEnv
test -z "$db2_test_include" && db2_direct_test_include=sqlcli.h
inc_dir="include"
lib_dir="lib"
db2_root_dirs="\
    /ldcg \
    /ldcg/IBMdb2 \
    /opt/IBMdb2/V6.1 \
    /opt/IBMdb2/V5.0"

  for dir in $db2_root_dirs
  do
    if test x$enable_64bit = xyes \
      -a -r "$dir/${inc_dir}64/$db2_direct_test_include"
    then
      no_db2= ac_db2_includes=$dir/${inc_dir}64;
      break
    fi
    if test -r "$dir/$inc_dir/$db2_direct_test_include"
    then
      no_db2= ac_db2_includes=$dir/$inc_dir;
      break
    fi
  done

# Check for the libraries.
# See if we find them without any special options.
# Don't add to $LIBS permanently.

ac_save_LIBS="$LIBS"
# First see if replacing the include by lib works.
for dir in `echo "$ac_db2_includes" | sed s,/$inc_dir,,` $db2_root_dirs
do
  case ${dir} in
  */V5.0)
    db2_direct_test_library="odbc"
    ;;
  *)
    db2_direct_test_library="db2"
    ;;
  esac
  LIBS=${ac_save_LIBS}
  LIBS="-l$db2_direct_test_library $LIBS"
  for ac_extension in a so sl; do
    if test x$enable_64bit = xyes \
	-a -r $dir/${lib_dir}64/lib${db2_direct_test_library}.$ac_extension
    then
      no_db2= ac_db2_libraries=$dir/${lib_dir}64
      break 2
    fi
    if test -r $dir/$lib_dir/lib${db2_direct_test_library}.$ac_extension
    then
      no_db2= ac_db2_libraries=$dir/$lib_dir
      break 2
    fi
  done
done
LIBS=$ac_save_LIBS])

dnl ---------------------------------------------------------------------
dnl AC_LDAS_DB2_FILES
dnl ---------------------------------------------------------------------

AC_DEFUN([AC_LDAS_DB2_FILES],
[AC_REQUIRE_CPP()dnl

AC_ARG_WITH(db2-includes, [  --with-db2-includes=DIR DB2 include files are in DIR])
if test -z "$with_db2_includes"; then
  db2_includes=NONE
else
  db2_includes=$with_db2_includes
fi
AC_ARG_WITH(db2-libraries, [  --with-db2-libraries=DIR
                          DB2 library files are in DIR])
if test -z "$with_db2_libraries"; then
  db2_libraries=NONE
else
  db2_libraries=$with_db2_libraries
fi

AC_MSG_CHECKING(for DB2)
AC_ARG_WITH(db2, [  --with-db2              enable DB2 tests])
if test "x$with_db2" = xno; then
  no_db2=yes
else
  if test "x$db2_includes" != xNONE && test "x$db2_libraries" != xNONE; then
    no_db2=
  else
    AC_CACHE_VAL(ac_cv_path_db2,
      [# One or both of these vars are not set, and ther is no cached value.
      no_db2=yes
      CUS_PATH_DB2_DIRECT

      if test "$no_db2" = yes; then
        ac_cv_path_db2="no_db2=yes"
      else
        ac_cv_path_db2="no_db2= ac_db2_includes=$ac_db2_includes ac_db2_libraries=$ac_db2_libraries"
      fi
      ])dnl
  fi
  eval "$ac_cv_path_db2"
fi

if test "$no_db2" = yes; then
  AC_MSG_RESULT(no)
  if  test "$odbc_dm" = "DB2" ; then
    # previously ERROR level message
    AC_MSG_WARN(DB2 files not found)
  fi
else
  test "x$db2_includes" = xNONE && db2_includes=$ac_db2_includes
  test "x$db2_libraries" = xNONE && db2_libraries=$ac_db2_libraries
  ac_cv_path_db2="no_db2= ac_db2_includes=$db2_includes ac_db2_libraries=$db2_libraries"
  AC_MSG_RESULT([libraries $db2_libraries, headers $db2_includes])
fi

if test -n "$db2_includes"; then
  DB2_CFLAGS="-DDB2 -I$db2_includes"
fi
AC_SUBST(DB2_CFLAGS)
if test -n "$db2_libraries"
then
  DB2_LDFLAGS="-Wl,${RPATH},$db2_libraries -L$db2_libraries"
  case "${DB2_LDFLAGS}" in
  */V5.0/*)
    DB2_LIBS="-lodbc -lodbcinst"
    ;;
  *)
    DB2_LIBS="-ldb2"
    ;;
  esac
fi
if test "x$db2_libraries" = xNONE; then
      DB2_LIBS_DIR=
else
      DB2_LIBS_DIR=$db2_libraries
fi
AC_SUBST(DB2_LIBS_DIR)
AC_SUBST(DB2_LDFLAGS)
AC_SUBST(DB2_LIBS)
])


dnl ---------------------------------------------------------------------
dnl AC_LDAS_DATABASE_FILES
dnl ---------------------------------------------------------------------

AC_DEFUN([AC_LDAS_DATABASE_FILES],
[
  AC_ARG_ENABLE(metadata-api,[  --enable-metadata-api[=yes]
                          build the metadata API],dnl
  ,enable_metadata_api="yes")
  case "${enable_metadata_api}" in
    no)
	AC_SUBST(DB2_LIBS_DIR)
	AC_SUBST(DB2_LDFLAGS)
	AC_SUBST(DB2_LIBS)
	;;
    *)	AC_LDAS_ODBC_FILES
	AC_LDAS_DB2_FILES
	;;
  esac
  AM_CONDITIONAL([ENABLE_METADATA_API],[test "${enable_metadata_api}" = "yes"])
])


dnl =====================================================================
dnl   General routine for searching out supported languages
dnl =====================================================================
AC_DEFUN([LDAS_PACKAGE_LANGUAGE],
[
  AC_ARG_WITH([$1],[  --with-$1=<dir>  Directory where $1 is installed],
    [
    ],
    [
      dnl ---------------------------------------------------------------
      dnl  Determine subdir base on file type
      dnl ---------------------------------------------------------------
      case $3 in
      *.h) subdir=include;;
      esac
      dnl ---------------------------------------------------------------
      dnl  Making an educated guess as to where Language is installed
      dnl ---------------------------------------------------------------
      for path in `echo $PATH | \
	  sed -e 's/s*bin:/'$subdir':/g' -e 's/:/ /g' -e 's/s*bin$/'$subdir'/'`
      do
	if test $2 $path/$3
	then
	  with_$1="$path"
	  break
        fi
      done
    ])
  AC_MSG_CHECKING(for $1 installation)
  case x${with_$1} in
  x|xno)
    AC_MSG_RESULT(no)
    with_$1="" # Normalize the result
    ;;
  *)
    if test $2 ${with_$1}/$3
    then
      AC_MSG_RESULT(${with_$1})
      AC_DEFINE(translit(HAVE_$1,[a-z],[A-Z]),dnl
	        [1],dnl
                [Defined if the scripting language $1 is supported])
    else
      AC_MSG_ERROR([no (${with_$1})])
    fi
    ;;
  esac
])
dnl =====================================================================
dnl  Look for TCL scripting language support
dnl =====================================================================
dnl  Look for Python scripting language support
dnl   - This has now been moved to packages.m4
dnl =====================================================================
AC_DEFUN([LDAS_PACKAGE_PYTHON_DEPRICATED],
[
  AC_ARG_ENABLE([python],
    [ --enable-python  Enable the use of python],
    [],
    [enable_python=check] )
  case x$enable_python in
  xno)
    unset PYTHON
    unset PYTHON_CONFIG
    ;;
  *)
    AC_PATH_PROG( PYTHON, python )
    AC_PATH_PROG( PYTHON_CONFIG, python-config )
    ;;
  esac
  if ! test -f "$PYTHON"
  then
    unset PYTHON
  fi
  AC_SUBST(PYTHON)
  case x${PYTHON} in
  x)
    unset PYTHON
    ;;
  *)
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Figure out which version of python is installed
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AC_MSG_CHECKING(for python version)
    PYTHON_VERSION="`${PYTHON} -c \"import sys; print( sys.version[[:3]] )\"`"
    AC_SUBST(PYTHON_VERSION)
    AC_MSG_RESULT(${PYTHON_VERSION})
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Figure out the prefix for python
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AC_MSG_CHECKING(for python prefix)
    PYTHON_PREFIX="`${PYTHON} -c \"import sys; print( sys.prefix )\"`"
    AC_SUBST(PYTHON_PREFIX)
    AC_MSG_RESULT(${PYTHON_PREFIX})
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Figure out the exe_prefix for python
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AC_MSG_CHECKING(for python exec_prefix)
    PYTHON_EXEC_PREFIX="`${PYTHON} -c \"import sys; print( sys.exec_prefix )\"`"
    AC_SUBST(PYTHON_EXEC_PREFIX)
    AC_MSG_RESULT(${PYTHON_EXEC_PREFIX})
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Figure out the platform for python
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AC_MSG_CHECKING(for python platform)
    PYTHON_PLATFORM="`${PYTHON} -c \"import sys; print( sys.platform )\"`"
    AC_SUBST(PYTHON_PLATFORM)
    AC_MSG_RESULT(${PYTHON_PLATFORM})
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Figure out the include directive for python
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AC_MSG_CHECKING(for python includes)
    case x$PYTHON_CONFIG in
    x)
	dnl -------------------------------------------------------------
	dnl Get the information the hard way
	dnl -------------------------------------------------------------
        PYTHON_INCLUDES="-I`echo ${PYTHON} | \
	  sed 's,/python$,,g' | \
	  sed 's,/amd64,64,g' | \
	  sed 's,/64,64,g' | \
	  sed 's,/bin\([[^/]]*\),/include\1,g'`/python$PYTHON_VERSION";
	;;
    *)
	dnl -------------------------------------------------------------
	dnl Newer version of python have the python-config script
	dnl -------------------------------------------------------------
    	PYTHON_INCLUDES="`$PYTHON_CONFIG --includes`"
	;;
    esac
    AC_SUBST(PYTHON_INCLUDES)
    AC_MSG_RESULT(${PYTHON_INCLUDES})
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Figure out the platform for python
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AC_MSG_CHECKING(for python lib)
    PYTHON_LIB="`${PYTHON} -c 'import distutils.sysconfig ; print distutils.sysconfig.get_python_lib( )'`"
    AC_SUBST(PYTHON_LIB)
    AC_MSG_RESULT(${PYTHON_LIB})
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Check for certain Python packages
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Python Package - ctypes
    dnl -----------------------------------------------------------------
    AC_MSG_CHECKING(for python package ctypes)
    if test -z "`${PYTHON} -c 'import ctypes' 2>&1`"
    then
      ldas_have_python_package_ctypes="yes"
    else
      ldas_have_python_package_ctypes="no"
    fi
    AC_MSG_RESULT($ldas_have_python_package_ctype)
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Python Package - numpy
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    AC_MSG_CHECKING(for pythong package numpy)
    if test -z "`${PYTHON} -c 'import numpy' 2>&1`"
    then
      ldas_have_python_package_numpy="yes"
      AC_MSG_RESULT($ldas_have_python_package_numpy)
      ldas_cppflags=${CPPFLAGS}
      PYTHON_NUMPY_INCLUDE="`${PYTHON} -c 'import numpy; print numpy.get_include()' 2>/dev/null | sed -e 's/^[[[].\(.*\).[]]]\$/\\1/g'`"
      if test x${PYTHON_NUMPY_INCLUDE} != x
      then
        PYTHON_NUMPY_INCLUDE="-I${PYTHON_NUMPY_INCLUDE}"
      fi
      CPPFLAGS="${PYTHON_NUMPY_INCLUDE} ${PYTHON_INCLUDES} $CPPFLAGS"
      AC_CHECK_HEADERS(numpy/arrayobject.h,[],[],
	[#include <Python.h>
        ])
      CPPFLAGS=${ldas_cppflags}
      AC_SUBST(PYTHON_NUMPY_INCLUDE)
    else
      ldas_have_python_package_numpy="no"
      AC_MSG_RESULT($ldas_have_python_package_numpy)
    fi
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Check for certain python include files
    dnl :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    dnl  Python include - bufferobject.h
    dnl -----------------------------------------------------------------
  esac
  AM_CONDITIONAL(HAVE_PYTHON,test -n "${PYTHON_INCLUDES}")
  AM_CONDITIONAL(HAVE_PYTHON_PACKAGE_CTYPE,test "${ldas_have_python_package_ctype}" = "yes")
  AM_CONDITIONAL(HAVE_PYTHON_PACKAGE_NUMPY,test "${ldas_have_python_package_numpy}" = "yes")
])
