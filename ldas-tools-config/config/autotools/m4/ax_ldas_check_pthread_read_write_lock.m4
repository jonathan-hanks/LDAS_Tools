dnl======================================================================
dnl LDAS_CHECK_PTHREAD_READ_WRITE_LOCK - See if anything special has
dnl	to be done to compile pthread's read/write locks
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#



AC_DEFUN([AX_LDAS_CHECK_PTHREAD_READ_WRITE_LOCK],
[
AC_CHECK_HEADER(pthread.h)
AC_MSG_CHECKING(for additional flags to compile read/write locking for threads)
AC_CACHE_VAL(ldas_cv_pthread_read_write_lock_option,
[ldas_cv_pthread_read_write_lock_option=no
AC_LANG_SAVE
AC_LANG_C
ac_save_CFLAGS="$CFLAGS"
ac_save_CXXFLAGS="$CXXFLAGS"
for ac_arg in ""
do
  CFLAGS="$ac_save_CFLAGS $ac_arg"
  AC_TRY_COMPILE(
  [#include <pthread.h>
  ],[
  pthread_rwlock_t l;
  pthread_rwlock_init( &l, (pthread_rwlockattr_t*)NULL );
  ],
  [ldas_cv_pthread_read_write_lock_option="$ac_arg"; break; ],
  [ldas_cv_pthread_read_write_lock_option="unsupported"])
done
CFLAGS="$ac_save_CFLAGS"
])
if test -z "$ldas_cv_pthread_read_write_lock_option"
then
  AC_MSG_RESULT([none needed])
else
  AC_MSG_RESULT([$ldas_cv_pthread_read_write_lock_option])
fi
case "x$ldas_cv_pthread_read_write_lock_option" in
  xno|xunsupported) ;;
  x)
    AC_DEFINE([HAVE_PTHREAD_RW_LOCK],
	      [1],
	      [Defined if pthread read/write locks supported])
    ;;
  *)
    AC_DEFINE([HAVE_PTHREAD_RW_LOCK],
	      [1],
	      [Defined if pthread read/write locks supported])
    CFLAGS="$CFLAGS $ldas_cv_pthread_read_write_lock_option"
    CXXFLAGS="$CXXFLAGS $ldas_cv_pthread_read_write_lock_option"
    ;;
esac
AC_LANG_RESTORE
])
