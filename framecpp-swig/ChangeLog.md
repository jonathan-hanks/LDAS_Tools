# Release NEXT -
    - Upgraded to SWIG 4.1 syntax

# Release 3.0.0 - December 22, 2022
    - Removed ULeapS from FrTOC (closes #131)
    - Removed ULeapS from FrameH (closes #128)
    - Removed Detectors as it is now part of FrDetector

# Release 2.6.14 - July 11, 2022
    - Converted from PyString to PyUnicode (closes computing/ldastools/LDAS_Tools#146)

# Release 2.6.13 - June 17, 2022
    - Added python test for ticket #114
    - Added ability to create FrEvent with params from swig bindings (closes #57)
    - Removed explicate self referencing Provides and Obsolete rules
      from spec file (closes #126)
    - Removed support for Python 2 (closes #139)

# Release 2.6.12 - September 29, 2021
    - Many corrections to support building on RHEL 8

# Release 2.6.11 - August 13, 2021
  - Added python test to verify proper construction of FrProcData structure (Closes #108)

# Release 2.6.10  - March 9, 2021
  - Packaging corrections

# Release 2.6.9 - August 14, 2019
  - Corrected Portfile to have proper description (Closes #55)
  - Added testing of FrSimData.data attribute (Closes #61)

# Release 2.6.8 - February  2, 2019
  - Corrections for conda build on OSX
  - Parameterized python build rules to minimize copy/paste errors

# Release 2.6.7 - January  8, 2019
  - Removed one deep copy of Table of Contents
  - Resolved Python memory leak resulting from use of FrTOC element
  - Added support to access delta time recorded in FrTOC (closes #52)
  - Enhanced handling of C++ exceptions for PYTHON and generic SWIG (closes #51)

# Release 2.6.6 - December 6, 2018
  - Addressed packaging issues

# Release 2.6.5 - November 27, 2018
  - Added missing COPYING file (fixes #23)
  - Updated to cmake_helper 1.0.4
  - Updated for Python3 used by Buster
  - Added GetUnits to FrAdcData (fixes #28)
  - Added FrHistory to FrVect (fixes #45)
  - Added pyframecpp-dump (fixes #46)
  - Modified GetDataUncompressed to return numpy array (fixes #47)

# Release 2.6.4 - August 27, 2018
  - Removed erronously installed README (closes ticket #17)
  - Enhancements to further support for Pythyon 3

# Release 2.6.3 - July 6, 2018
  - Corrections for Red Hat packaging

# Release 2.6.2 - June 27, 2018
  - Updated Conflicts specifier
  - Updated macro used for Python 3 package version

# Release 2.6.1 - June 22, 2018
  - Updated MODULE_DIR to LDASTools (closes ticket #6)
  - Updated python package dependencies (closes ticket #7)
  - Updated packaging rules to have build time dependency on ldas-tools-cmake (closes ticket #9)
  - Corrected Description syntax inside of debian/control (closes ticket #10)
  - Updated packaging rules to have build time dependency on specific
    versions of LDAS Tools packages

# Release 2.6.0 - June 19, 2018
  - Extending FrDetector interface to include a constructor with all
	detector information explicitly defined.
  - Added GEO, KAGRA, and TAMA detectors
