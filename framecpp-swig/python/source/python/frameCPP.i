/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#if defined(SWIGPYTHON)

%feature( "autodoc", "1" );

 // =====================================================================
 //  Setup for Python
 // =====================================================================

%{
#include "framecpp/IFrameStream.hh"
#include "framecpp/OFrameStream.hh"
#include "framecpp/GPSTime.hh"

using namespace FrameCPP;

%}

%{
#include <wchar.h>
%}

 // =====================================================================
 //  Pull in additional language specific rules
 // =====================================================================

%import "framecpp/python/frameCPPMacros.i";

%include "framecpp/python/DataTypes.i";
%include "framecpp/python/GPSTime.i";

%include "framecpp/python/FrEvent.i";
%include "framecpp/python/FrProcData.i";
%include "framecpp/python/FrTOC.i";
%include "framecpp/python/FrTOCAdc.i";
%include "framecpp/python/FrTOCProc.i";
%include "framecpp/python/FrTOCSim.i";
%include "framecpp/python/FrVect.i";

 // =====================================================================
 //  Pull in the core wrappings
 // =====================================================================

%include "framecpp/frameCPP.i";

#endif /* defined(SWIGPYTHON) */
