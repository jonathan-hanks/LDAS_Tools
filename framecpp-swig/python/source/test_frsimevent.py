"""! @brief Test script validating the slope value of the FrAdcData structure."""

import unittest
from LDAStools import frameCPP;

# unittest will test all the methods whose name starts with 'test'
class TestFrSimEvent( unittest.TestCase ):

    #
    # Validation of the method return types
    #
    def test_method_return_types( self ):
        simEvent = frameCPP.FrSimEvent( )
        self.assertIsInstance( simEvent.GetName( ), str )
        self.assertIsInstance( simEvent.GetComment( ), str )
        self.assertIsInstance( simEvent.GetInputs( ), str )
        self.assertIsInstance( simEvent.GetGTime( ), tuple )
        self.assertIsInstance( simEvent.GetTimeBefore( ), float )
        self.assertIsInstance( simEvent.GetTimeAfter( ), float )
        self.assertIsInstance( simEvent.GetAmplitude( ), float )
        self.assertIsInstance( simEvent.GetParam( ), list )

    #
    # Validation of the types for attributes
    #
    def test_attribute_types( self ):
        simEvent = frameCPP.FrSimEvent( )
        self.assertIsInstance( simEvent.name, str )
        self.assertIsInstance( simEvent.comment, str )
        self.assertIsInstance( simEvent.inputs, str )
        self.assertIsInstance( simEvent.GTime, tuple )
        self.assertIsInstance( simEvent.timeBefore, float )
        self.assertIsInstance( simEvent.timeAfter, float )
        self.assertIsInstance( simEvent.amplitude, float )
        self.assertIsInstance( simEvent.params, list )

    def test_constructor_minimal( self ):
        with self.assertRaises( Warning ):
            frameCPP.FrSimEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, [] )
            raise Warning( "Successfully constructed object" )

    # https://git.ligo.org/computing/ldastools/LDAS_Tools/-/issues/146
    def test_constructor_params_list(self):
        params = [
            ("param1", 1.0),
            ("param2", 2.0),
        ]
        e = frameCPP.FrEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, 0, -1, '', params)
        self.assertListEqual(params, e.params)

    # https://git.ligo.org/computing/ldastools/LDAS_Tools/-/issues/146
    def test_constructor_params_dict(self):
        params = {
            "param1": 1.0,
            "param2": 2.0,
        }
        e = frameCPP.FrEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, 0, -1, '', params)
        self.assertDictEqual(params, dict(e.params))

    # https://git.ligo.org/computing/ldastools/LDAS_Tools/-/issues/146
    def test_constructor_params_tuple(self):
        params = (
            ( "param1", 1.0),
            ( "param2", 2.0),
        )
        e = frameCPP.FrEvent('test', '', '', frameCPP.GPSTime(0, 0), 0, 0, 0, 0, -1, '', params)
        self.assertTupleEqual(params, tuple( e.params) )

# running of the tests
if __name__ == '__main__':
    unittest.main( )
