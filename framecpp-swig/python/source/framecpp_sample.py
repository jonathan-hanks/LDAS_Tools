#! /usr/bin/env python

# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

##
#

#------------------------------------------------------------------------
#------------------------------------------------------------------------
from optparse import OptionParser

from LDAStools import frameCPP

VERSION = 8
START = 600000000
DT = 1
RUN = -1
FRAME = 0
ULEAPS = 32

channel_id = 1

DATA_TYPE_STRING = {
    frameCPP.FrVect.FR_VECT_C : 'FR_VECT_C',
    frameCPP.FrVect.FR_VECT_1U : 'FR_VECT_1U',
    frameCPP.FrVect.FR_VECT_2S : 'FR_VECT_2S',
    frameCPP.FrVect.FR_VECT_2U : 'FR_VECT_2U',
    frameCPP.FrVect.FR_VECT_4S : 'FR_VECT_4S',
    frameCPP.FrVect.FR_VECT_4U : 'FR_VECT_4U',
    frameCPP.FrVect.FR_VECT_8S : 'FR_VECT_8S',
    frameCPP.FrVect.FR_VECT_8U : 'FR_VECT_8U',
    frameCPP.FrVect.FR_VECT_4R : 'FR_VECT_4R',
    frameCPP.FrVect.FR_VECT_8R : 'FR_VECT_8R',
    frameCPP.FrVect.FR_VECT_8C : 'FR_VECT_8C',
    frameCPP.FrVect.FR_VECT_16C : 'FR_VECT_16C'
    }

#------------------------------------------------------------------------
# Determine how to get the raw data
#------------------------------------------------------------------------
use_ctypes = False
use_numpy = False

try:
    #--------------------------------------------------------------------
    # NumPy has a much better interface
    #--------------------------------------------------------------------
    import numpy
    use_numpy = True
except ImportError:
    try:
        #----------------------------------------------------------------
        # Use raw C pointer
        #----------------------------------------------------------------
        import ctypes
        use_ctypes = True
    except ImportError:
        use_ctypes = False

def ConvertCData( Type, Data ):
    step = 1
    if Type == frameCPP.FrVect.FR_VECT_C:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_byte ) )
    elif Type == frameCPP.FrVect.FR_VECT_1U:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_ubyte ) )
    elif Type == frameCPP.FrVect.FR_VECT_2S:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_short ) )
    elif Type == frameCPP.FrVect.FR_VECT_2U:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_ushort ) )
    elif Type == frameCPP.FrVect.FR_VECT_4S:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_int ) )
    elif Type == frameCPP.FrVect.FR_VECT_4U:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_uint ) )
    elif Type == frameCPP.FrVect.FR_VECT_8S:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_longlong ) )
    elif Type == frameCPP.FrVect.FR_VECT_8U:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_ulonglong ) )
    elif Type == frameCPP.FrVect.FR_VECT_4R:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_float ) )
    elif Type == frameCPP.FrVect.FR_VECT_8R:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_double ) )
    elif Type == frameCPP.FrVect.FR_VECT_8C:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_float ) )
        step = 2
    elif Type == frameCPP.FrVect.FR_VECT_16C:
        datap = ctypes.cast( Data.__long__( ), ctypes.POINTER( ctypes.c_double ) )
        step = 2
    else:
        print ( " WARNING: Currently don't support printing of %s" ) % ( DATA_TYPE_STRING[ Type ] )
        # \todo
        #   Would be better to throw an exception instead of returning
        return ( 0, 0 )
    return (datap, step)

class Range:
    def __init__( self, DataType, Start, Stop, Inc ):
        self.data_type = DataType
        self.start = Start
        self.stop = Stop
        self.inc = Inc
        
    def Samples( self ):
        retval = int( ( self.stop - self.start ) / self.inc )
        return retval

    type_map = {
        frameCPP.FrVect.FR_VECT_2S : '2S',
        frameCPP.FrVect.FR_VECT_2U : '2U',
        frameCPP.FrVect.FR_VECT_4S : '4S',
        frameCPP.FrVect.FR_VECT_4U : '4U',
        frameCPP.FrVect.FR_VECT_8S : '8S',
        frameCPP.FrVect.FR_VECT_8U : '8U',
        frameCPP.FrVect.FR_VECT_4R : '4R',
        frameCPP.FrVect.FR_VECT_8R : '8R',
        ## \todo add default case of unknown
        }
        

def create_ramp_data ( Name, Range ):
    #--------------------------------------------------------------------    
    # Create a new vector object
    #--------------------------------------------------------------------    
    samples = Range.Samples( )
    sampleRate = 1.0 / samples
    dims = frameCPP.Dimension( samples, sampleRate, "UnitX", 0.0 )

    vector = frameCPP.FrVect( Name,
                              Range.data_type,
                              1,dims,
                              "UnitY" )
    #--------------------------------------------------------------------    
    # Figure out how to obtain a reference to the data
    #--------------------------------------------------------------------    
    if ( use_numpy ):
        #----------------------------------------------------------------
        # Use the ArrayObject interface provided by NumPy
        #----------------------------------------------------------------
        datap = vector.GetDataArray( )
        step = 1
    elif ( use_ctypes ):
        #----------------------------------------------------------------
        # Use the raw C pointer
        #----------------------------------------------------------------
        data = vector.GetDataUncompressed( )
        ( datap, step ) = ConvertCData( Range.data_type, data.__long__( ) )
        if ( step == 0 ):
            return
    else:
        #----------------------------------------------------------------
        # Not a recognized interface type
        #----------------------------------------------------------------
        return
    #--------------------------------------------------------------------    
    # Ramp the data
    #--------------------------------------------------------------------    
    x = Range.start
    n = 0
    while x < Range.stop:
        datap[ n ] = x
        if step > 1:
            datap[ n + 1 ] = x
        n += step
        x += Range.inc
    return vector

def create_channel_adc( Caller, Range, FrameH ):
    global channel_id
    data_type = Range.type_map[ Range.data_type ]
    name = 'Z1:ADC-%s-%s-%g-%g-%g' % ( Caller, data_type,
                                       Range.start,
                                       Range.stop,
                                       Range.inc )
    channel = frameCPP.FrAdcData( name,
                                  0, channel_id,
                                  16,
                                  Range.Samples( ) )
    channel_id = channel_id + 1
    vect = create_ramp_data( name, Range )
    channel.AppendData( vect )
    FrameH.AppendFrAdcData( channel )
    return channel

def create_channel_adc_invalid( Caller, Range, FrameH ):
    #--------------------------------------------------------------------
    # Show how to set some of the internal parts of the FrAdcData class
    #--------------------------------------------------------------------
    global channel_id
    data_type = Range.type_map[ Range.data_type ]
    name = 'Z1:ADC-Invalid-%s-%s-%g-%g-%g' % ( Caller, data_type,
                                               Range.start,
                                               Range.stop,
                                               Range.inc )
    channel = frameCPP.FrAdcData( name,
                                  0, channel_id,
                                  16,
                                  Range.Samples( ) )
    channel.dataValid = 2
    channel_id = channel_id + 1
    vect = create_ramp_data( name, Range )
    channel.AppendData( vect )
    FrameH.AppendFrAdcData( channel )
    return channel

def create_channel_proc( Caller, Range, FrameH ):
    data_type = Range.type_map[ Range.data_type ]
    name = 'Z1:PROC-%s-%s-%g-%g-%g' % ( Caller, data_type,
                                        Range.start,
                                        Range.stop,
                                        Range.inc )
    channel = frameCPP.FrProcData( name,
                                   'Sample proc channel',
                                   frameCPP.FrProcData.TIME_SERIES,
                                   frameCPP.FrProcData.UNKNOWN_SUB_TYPE,
                                   0.0,
                                   1.0,
                                   0.0,
                                   0.0,
                                   0.0,
                                   0 )
    vect = create_ramp_data( name, Range )
    channel.AppendData( vect )
    channel.tRange = 1.0
    # channel.AppendAuxParam( "AuxParamPI", 3.1415 )
    # channel.auxParam = { "AuxParamPI":3.1415, "AuxParam2PI":6.283 }
    channel.auxParam = [ ("AuxParamPI", 3.1415), ( "AuxParam2PI", 6.283 ) ]
    FrameH.AppendFrProcData( channel )
    return channel

def create_channel_sim( Caller, Range, FrameH ):
    data_type = Range.type_map[ Range.data_type ]
    name = 'Z1:SIM-%s-%s-%g-%g-%g' % ( Caller, data_type,
                                       Range.start,
                                       Range.stop,
                                       Range.inc )
    size = ( ( Range.stop - Range.start ) / Range.inc );
    channel = frameCPP.FrSimData( name,
                                  'Sample sim channel',
                                  size,
                                  0.0,
                                  0.0,
                                  0.0 )
    vect = create_ramp_data( name, Range )
    channel.AppendData( vect )
    FrameH.AppendFrSimData( channel )
    return channel

def create_channel_set( CreateChannel, FrameH ):
    range = Range( frameCPP.FrVect.FR_VECT_4R, 0.0, 256.0, 0.25 )
    channel = CreateChannel( "Ramp", range, FrameH )
    
    range = Range( frameCPP.FrVect.FR_VECT_8R, 0.0, 256.0, 1.0 )
    channel = CreateChannel( "Ramp", range, FrameH )
    
    range = Range( frameCPP.FrVect.FR_VECT_8S, -1024, 1024, 2 )
    channel = CreateChannel( "Ramp", range, FrameH )
    
    return

#------------------------------------------------------------------------
# Create a frame and add channels to the frame
#------------------------------------------------------------------------
def create_frame():
    start = frameCPP.GPSTime( START, 0 )

    frame = frameCPP.FrameH()
    frame.SetName( "LIGO" )
    frame.SetRun( RUN )
    frame.SetGTime( start )
    frame.SetDt( DT )

    #--------------------------------------------------------------------

    now = frameCPP.GPSTime( );
    now.Now( );
    
    history = frameCPP.FrHistory( "PySampleFrame",
                                  now.seconds,
                                  "Example of how to create frames in Python" )
    frame.AppendFrHistory( history )

    #--------------------------------------------------------------------
    # Just for kicks, put in detector information for:
    #   Hanford, Livingston, and Virgo.
    # Normally, you would only put in the detector information for the
    #   sites referenced by the channels which compose the frame.
    #--------------------------------------------------------------------
    d = frameCPP.GetDetector( frameCPP.DETECTOR_LOCATION_H1, start );
    frame.AppendFrDetector( d );
    d = frameCPP.GetDetector( frameCPP.DETECTOR_LOCATION_H2, start );
    frame.AppendFrDetector( d );
    d = frameCPP.GetDetector( frameCPP.DETECTOR_LOCATION_L1, start );
    frame.AppendFrDetector( d );
    d = frameCPP.GetDetector( frameCPP.DETECTOR_LOCATION_V1, start );
    frame.AppendFrDetector( d );

    #--------------------------------------------------------------------
    # More fun by adding an FrEvent
    #--------------------------------------------------------------------
    event_params_desc = [ ('event_param_1', 1.0), ('event_param_2', 2.0)]
    event_params = frameCPP.FrEventParamList( )
    for n, v in event_params_desc:
        event_params.append( frameCPP.FrEventParam( n, v ) )
    event = frameCPP.FrEvent( "event_1",
                              "This is the first event",
                              "xy",
                              now,
                              4.0,
                              6.0,
                              0,
                              0,
                              0.5,
                              "unlikely",
                              event_params )
    frame.AppendFrEvent( event )
    
    #--------------------------------------------------------------------

    create_channel_set( create_channel_adc, frame );
    create_channel_set( create_channel_adc_invalid, frame );
    create_channel_set( create_channel_proc, frame );
    create_channel_set( create_channel_sim, frame );
    
    #--------------------------------------------------------------------

    return frame
    
#------------------------------------------------------------------------
# Main entry point
#------------------------------------------------------------------------
def framecpp_sample_main():
    parser = OptionParser( )

    (options, args) = parser.parse_args( )
    #--------------------------------------------------------------------
    # Create the file
    #--------------------------------------------------------------------
    filename = 'Z-Sample_Python_ver%d-%d-%d.gwf' % ( VERSION, START, DT )
    framestream = frameCPP.OFrameFStream( filename )
    #--------------------------------------------------------------------
    # Create the frame with all of the data
    #--------------------------------------------------------------------
    frame = create_frame( )

    framestream.WriteFrame( frame,
                            frameCPP.FrVect.RAW,
                            0 )
    print( filename )

#------------------------------------------------------------------------
#------------------------------------------------------------------------
if __name__ == "__main__":
    framecpp_sample_main( )
