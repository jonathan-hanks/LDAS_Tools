#! /usr/bin/env python

# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE

##
#

#------------------------------------------------------------------------
# This is a sample script based on user request.
#------------------------------------------------------------------------
from __future__ import division
import sys

from LDAStools import frameCPP
import numpy as np

__author__ = "Nickolas Fotopoulos <nickolas.fotopoulos@ligo.org>"


#------------------------------------------------------------------------
# Routine to dump the contents of the FrameH structure
#------------------------------------------------------------------------
def dumpAdcData( source ):
    print( "FrAdcData:    name: ", source.name )
    print( "           comment: ", source.comment )
    print( "     channelNumber: ", source.channelNumber )
    print( "      channelGroup: ", source.channelGroup )
    print( "             nBits: ", source.nBits )
    print( "              bias: ", source.bias )
    print( "             slope: ", source.slope )
    print( "             units: ", source.units )
    print( "        sampleRate: ", source.sampleRate )
    print( "        timeOffset: ", source.timeOffset )
    print( "            fShift: ", source.fShift )
    print( "             phase: ", source.phase )
    print( "         dataValid: ", source.dataValid )
    print( "        data(size): ", len( source.data ) )
    print( "        data(type): ", type( source.data ) )
    sys.stdout.flush( )

def dumpFrameH( frameh ):
    print( "FrameH:       name: ", frameh.name )
    print( "               run: ", frameh.run )
    print( "             frame: ", frameh.frame )
    print( "       dataQuality: ", frameh.dataQuality )
    print( "             GTime: ", frameh.GTime )
    print( "                dt: ", frameh.dt )
    sys.stdout.flush( )
    return

def dumpDetector( source ):
    print( "FrDetector:   name: ", source.name )
    print( "            prefix: %c%c" % ( source.prefix[0], source.prefix[1] ) )
    sys.stdout.flush( )

def dumpSimData( source ):
    print( "FrSimData:           name: ", source.name )
    print( "                  comment: ", source.comment )
    print( "               sampleRate: ", source.sampleRate )
    print( "               timeOffset: ", source.timeOffset )
    print( "                   fShift: ", source.fShift )
    print( "                    phase: ", source.phase )
    print( "               data(size): ", len( source.data ) )
    print( "               data(type): ", type( source.data ) )
    sys.stdout.flush( )

def dumpTable( source ):
    print( "FrTable:      name: ", source.name )
    print( "           comment: ", source.comment )
    print( "       len(column): ", len( source.column ) )
    sys.stdout.flush( )

def dumpTOC( source ):
    print( "            nFrame: ", source.nFrame )
    print( "            GTimeS: ", source.GTimeS )
    print( "            GTimeN: ", source.GTimeN )
    print( "                dt: ", source.dt )
    sys.stdout.flush( )

def dumpVect( source ):
    print( "FrVect:       name: ", source.name )
    sys.stdout.flush( )

#
# Functions to navigate the filestream
# XXX: Use once reference counting bug is fixed.
#
def iter_vect(stream):
    "Return an iterator of FrVect objects found in stream."
    toc = stream.GetTOC()
    dumpTOC( toc )
    frameCount = toc.nFrame

    for frameNum in range( frameCount ):
        # grab vectors from AdcData
        toc_adc = toc.GetADC()
        for key in sorted(toc_adc):
            adc = stream.ReadFrAdcData(frameNum, key)
            dumpAdcData( adc )
            try:
                for v in adc.data:
                    yield v
            except:
                pass

        # grab vectors from ProcData
        for channelName in toc.GetProc( ):
            proc = stream.ReadFrProcData(frameNum, channelName )
            try:
                for v in proc.data:
                    yield v
            except:
                pass

        for v in ( [] ):
            yield v

        # grab vectors from SimData
        for channelName in toc.GetSim( ):
            sim = stream.ReadFrSimData(frameNum, channelName )
            dumpSimData( sim )
            try:
                for v in sim.data:
                    yield v
            except:
                pass

#
# This is an example of how to read FrEvents
#
def frgetevents(filename, name):
    fs = frameCPP.IFrameFStream(filename)
    frame_count = fs.GetNumberOfFrames( )
    toc = fs.GetTOC( )
    print ( "IFrameFStream.GetNumberOfFrames(): %d" % (frame_count) )
    print( "NFrame: ", toc.GetNFrame( ) )
    for frame_index in range( ( frame_count - 1 ) ):
        event = fs.ReadFrEvent( frame_index, name )
        print( "FrEvent[%d]: " % frame_index )
        print( "FrEvent.name: %s" % event.name )
        print( "FrEvent.comment: %s" % event.comment )
        print( "FrEvent.inputs: %s " % event.inputs )
        print( "FrEvent.GTime: %s" % str( event.GTime ) )
        print( "FrEvent.timeBefore: %g" % event.timeBefore )
        print( "FrEvent.timeAfter: %g" % event.timeAfter )
        print( "FrEvent.eventStatus: %g" % event.eventStatus )
        print( "FrEvent.amplitude: %g" % event.amplitude )
        print( "FrEvent.probability: %g" % event.probability )
        print( "FrEvent.statistics: %s" % event.statistics )
        print( "FrEvent.params: %s" % str( event.params ) )

#
# This is an example of how to read FrSimEvents
#
def frgetsimevents(filename, name):
    fs = frameCPP.IFrameFStream(filename)
    frame_count = fs.GetNumberOfFrames( )
    toc = fs.GetTOC( )
    print( "IFrameFStream.GetNumberOfFrames(): %d" % (frame_count) )
    print( "NFrame: ", toc.GetNFrame( ) )
    for frame_index in range( toc.GetNFrame( ) ):
        event = fs.ReadFrSimEvent( frame_index, name )
        print( "FrSimEvent.name: ", event.name )
        print( "FrSimEvent.comment", event.comment )
        print( "FrSimEvent.inputs", event.inputs )
        print( "FrSimEvent.GTime", event.GTime )
        print( "FrSimEvent.timeBefore", event.timeBefore )
        print( "FrSimEvent.timeAfter", event.timeAfter )
        print( "FrSimEvent.amplitude", event.amplitude )
        print( "FrSimEvent.params", event.params )

#
# The high-level functions that end-users will call
#
def frgetvect1d(filename, channel, method, start=None, end=None):
    fs = frameCPP.IFrameFStream(filename)

    #---------------------------------------------------------------------
    # Show what is being done
    #---------------------------------------------------------------------
    print( "META_DATA: filename: ", filename )
    print( "META_DATA:  channel: ", channel )
    print( "META_DATA:   method: ", method )
    print( "META_DATA:    start: ", start )
    print( "META_DATA:      end: ", end )

    #---------------------------------------------------------------------
    # Print information about the frame stream
    #---------------------------------------------------------------------
    print( "Here 1" )
    print( "Frame Specification: ", fs.Version( ) )
    print( "Here 2" )
    sys.stdout.flush( )
    print( "Frame Library: ", fs.FrameLibrary( ), " (", fs.FrameLibraryName( ), ")" )
    sys.stdout.flush( )
    print( "Frame Minor: ", fs.LibraryRevision( ) )
    sys.stdout.flush( )

    #---------------------------------------------------------------------
    # Print information about the table of contents
    #---------------------------------------------------------------------
    print( "TOC: nFrame: ", fs.GetTOC( ).nFrame )
    print( "TOC: GTimeS: ", fs.GetTOC( ).GTimeS )
    print( "TOC: GTimeN: ", fs.GetTOC( ).GTimeN )
    print( "TOC: FrAdcData Info: ", fs.GetTOC( ).GetADC( ) )
    print( "TOC: FrProcData Info: ", fs.GetTOC( ).GetProc( ),
               " Length: ", len( fs.GetTOC( ).GetProc( ) ) )
    print( "TOC: FrSimData Info: ", fs.GetTOC( ).GetSim( ),
           " Length: ", len( fs.GetTOC( ).GetSim( ) ) )
    sys.stdout.flush( )

    #---------------------------------------------------------------------
    #
    #---------------------------------------------------------------------
    sev_list = []  # each row will be (start, end, vector)

    if ( method == 1 ):
        #----------------------------------------------------------------
        # This shows how read multiple channels
        #----------------------------------------------------------------
        # XXX: hold references in the local scope to proc and adc structures due to refcount bug;
        #      replace this block with iter_vect eventually
        vects = []
        toc = fs.GetTOC()
        procs = []
        for channelName in toc.GetProc( ):
            # Read the channel toc_proc.Name(i) from frame 0 (zero) as
            #   indexed from the beginning of the file.
            proc = fs.ReadFrProcData(0, channelName )
            procs.append(proc)
            for v in proc.data:
                vects.append(v)

        toc_adc = toc.GetADC()
        adcs = []
        for key in sorted(toc_adc.iterkeys()):
            adc = fs.ReadFrAdcData(0, key)  # what is this 0??
            adcs.append(adc)
            for v in adc.data:
                vects.append(v)
        # end XXX
    elif ( method == 2 ):
        #----------------------------------------------------------------
        # This shows how read a single channel
        #----------------------------------------------------------------
        vects = []
        sims  = []
        procs = []
        adcs  = []
        adc = fs.ReadFrAdcData( 0, channel )
        if adc:
            adcs.append( adc )
            for v in adc.data:
                vects.append(v)
        else:
            proc = fs.ReadFrProcData( 0, channel )
            if proc:
                procs.append( proc )
                for v in proc.data:
                    vects.append(v)
            else:
                sim = fs.ReadFrSimData( 0, channel )
                if sim:
                    sims.append( sim )
                    for v in sim.data:
                        vects.append(v)
                # end if - sim
            # end if - proc
        # end if - adc
    elif ( method == 3 ):
        #----------------------------------------------------------------
        # This shows how read a single channel without preserving
        #----------------------------------------------------------------
        vects = []
        adc = fs.ReadFrAdcData( 0, channel )
        if adc:
            for v in adc.data:
                vects.append(v)
        else:
            proc = fs.ReadFrProcData( 0, channel )
            if proc:
                for v in proc.data:
                    vects.append(v)
            else:
                sim = fs.ReadFrSimData( 0, channel )
                if sim:
                    for v in sim.data:
                        vects.append(v)
                # end if - sim
            # end if - proc
        # end if - adc
    elif ( method == 4 ):
        vects = list(iter_vect(fs))
    else:
        print( "Unknown method: ", method )
        assert( False )

    # check for channel separately for extra debugging help to user
    vects = [vect for vect in vects if vect.name == channel]
    if len(vects) == 0:
        raise( ValueError, "In file %s, vector %s not found [method: %d]." % (filename, channel, method) )

    # now filter to vectors that overlap the requested region
    for vect in vects:
        if vect.nDim != 1:
            raise( NotImplementedError, "Multiple dimensions are not yet supported." )
        dim = vect.GetDim(0)
        startX = dim.startX
        vect_end = startX + dim.dx * dim.nx
        if (end is None or startX < end) and (start is None or vect_end > start):
            sev_list.append((startX, vect_end, vect))
    if len(sev_list) == 0:
        raise( ValueError, "In file %s, vector %s not found between %d and %d."
               % (filename, channel, start or "-infinity", end or "infinity") )
    sev_list.sort()  # sorts by time first

    # extract useful quantities
    vects = [vect for _, _, vect in sev_list]
    dim = vects[0].GetDim(0)
    dx = dim.dx
    true_start = int(round((start - dim.startX) / dx)) * dx + dim.startX
    true_end = int(round((end - vects[-1].GetDim(0).startX) / dx) * dx + vects[-1].GetDim(0).startX) / dx
    expected_num_samples = int(round((true_end - true_start) / dx))

    # sanity checks
    if not all(vect.GetDim(0).dx == dx for vect in vects):
        raise( ValueError, "differing sample rates detected" )
    for (_, old_end, _), (new_start, _, _) in zip(sev_list, sev_list[1:]):
        if old_end != new_start:
            raise( ValueError, "The vectors do not form a contiguous block. "
                   "There is a gap between %f and %f." % (old_end, new_start) )

    # extract data
    # NB: extract first array separately to get dtype for the output array
    if start is None: start = sev_list[0][0]
    if end is None: end = sev_list[-1][1]

    now = start
    offset = 0
    v_start, v_end, vect = sev_list[0]
    first_array = vect.GetDataArray()
    start_ind = int(round((now - v_start) / dx))
    end_ind = int(round((min(v_end, end) - v_start) / dx))
    out_data = np.zeros(expected_num_samples, dtype=first_array.dtype)
    out_data[:end_ind - start_ind + 1] = first_array[start_ind:end_ind]
    del first_array
    now = min(v_end, end)
    offset += end_ind - start_ind
    for v_start, v_end, vect in sev_list[1:]:
        start_ind = int(round((now - v_start) / dx))
        assert start_ind >= 0, "gap detected"
        end_ind = int(round((min(v_end, end) - v_start) / dx))
        out_data[offset:offset + end_ind - start_ind + 1] = vect.GetDataArray()[start_ind:end_ind]
        now = min(v_end, end)
        offset += end_ind - start_ind
    assert offset == expected_num_samples

    return (out_data, true_start, 0.0, dim.dx, dim.unitX, vect.unitY)

def frameHistory( filename ):
    #--------------------------------------------------------------------
    # Example of how to read the frames sequentially from a file
    #--------------------------------------------------------------------
    # * Open the frame file
    #--------------------------------------------------------------------
    fs = frameCPP.IFrameFStream(filename)
    #--------------------------------------------------------------------
    # * Loop over the frames
    #--------------------------------------------------------------------
    frameh = fs.ReadFrameNSubset( 0, frameCPP.IFrameFStream.HISTORY | \
                                      frameCPP.IFrameFStream.DETECT_SIM | \
                                      frameCPP.IFrameFStream.DETECT_PROC | \
                                      frameCPP.IFrameFStream.TYPE | \
                                      frameCPP.IFrameFStream.USER | \
                                      frameCPP.IFrameFStream.AUX_DATA | \
                                      frameCPP.IFrameFStream.AUX_TABLE \
                                      )
    print( "frameh type: %s\n" % type( frameh ) )
    if ( frameh ):
        #----------------------------------------------------------------
        print( "length of type: %d" % len( frameh.type ) );
        for t in frameh.type:
            dumpVect( t )
        #----------------------------------------------------------------
        print( "length of user: %d" % len( frameh.user ) );
        for u in frameh.user:
            dumpVect( u )
        #----------------------------------------------------------------
        print( "length of detectSim: %d" % len( frameh.detectSim ) );
        for d in frameh.detectSim:
            dumpDetector( d )
        #----------------------------------------------------------------
        print( "length of detectProc: %d" % len( frameh.detectProc ) );
        for d in frameh.detectProc:
            dumpDetector( d )
        #----------------------------------------------------------------
        print( "length of history: %d" % len( frameh.history ) );
        for h in frameh.history:
            print(  "history.name: %s" % ( h.name ) )
            print(  "history.comment: %s" % ( h.comment ) )
        #----------------------------------------------------------------
        print( "length of auxData: %d" % len( frameh.auxData ) );
        for a in frameh.auxData:
            dumpVect( a )
        #----------------------------------------------------------------
        print( "length of auxTable: %d" % len( frameh.auxTable ) );
        for a in frameh.auxTable:
            dumpTable( a )
    return

def frameFrSimData( filename ):
    #--------------------------------------------------------------------
    # Example of how to read the FrSimData sequentially from a file
    #  introduced as a verification test for issue 61
    #--------------------------------------------------------------------
    # * Open the frame file
    #--------------------------------------------------------------------
    fs = frameCPP.IFrameFStream(filename)
    frame_count = fs.GetNumberOfFrames( )
    #--------------------------------------------------------------------
    # * Loop over the frames
    #--------------------------------------------------------------------
    for frame_index in range( frame_count ):
        sim_data = fs.ReadFrSimData( frame_index, 'SimData1' )
        print( "sim_data type: %s\n" % type( sim_data ) )
        if ( sim_data ):
            #------------------------------------------------------------
            dumpSimData( sim_data )
    return

def frameIteration( filename ):
    #--------------------------------------------------------------------
    # Example of how to read the frames sequentially from a file
    #--------------------------------------------------------------------
    # * Open the frame file
    #--------------------------------------------------------------------
    fs = frameCPP.IFrameFStream(filename)
    #--------------------------------------------------------------------
    # * Loop over the frames
    #--------------------------------------------------------------------
    # frameh = fs.ReadNextFrame( )
    frameh = fs.ReadFrameN( 0 )
    print( "frameh type: %s\n" % type( frameh ) )
    dumpFrameH( frameh )
    return

if __name__ == "__main__":
    filename = sys.argv[ 1 ]
    for i in range( 4, 5 ):
        # Try something made from frameCPP
        frgetevents(filename, 'TesT')
        frgetsimevents(filename, 'TesT')
        vect = frgetvect1d(filename, \
                           "TesT_ZERO_SUPPRESS_0_COMPLEX_8", \
                           i, start=7.2, end=100.1)
        print( len(vect[0]) )
        print( vect )
    #--------------------------------------------------------------------
    # Test reading frames sequentially
    #--------------------------------------------------------------------
    frameIteration(filename)
    #--------------------------------------------------------------------
    # Test reading FrHistory sequentially
    #--------------------------------------------------------------------
    frameHistory(filename)
    #--------------------------------------------------------------------
    # Test reading FrSimData sequentially
    #--------------------------------------------------------------------
    frameFrSimData(filename)
