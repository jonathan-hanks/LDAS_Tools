/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


/** \cond IGNORE_BY_DOXYGEN */

#if defined( DOCSTRING )
%module( docstring=DOCSTRING, package="LDAStools") frameCPP
#else
%module( package="LDAStools") frameCPP
#endif

%include <boost_shared_ptr.i>

%include "framecpp/frameCPPMacros.i"

%include "framecpp/DataTypes.i"
%include "framecpp/GPSTime.i"
%include "framecpp/STRING.i"
%include "framecpp/Dimension.i"

%include "framecpp/Container.i"
%include "framecpp/SearchContainer.i"

%shared_ptr(FrameH)
%shared_ptr(FrAdcData)
%shared_ptr(FrDetector)
%shared_ptr(FrEvent)
%shared_ptr(FrHistory)
%shared_ptr(FrProcData)
%shared_ptr(FrSimData)
%shared_ptr(FrSimEvent)
%shared_ptr(FrTable)
%shared_ptr(FrVect)

%include "framecpp/FrVect.i"
%include "framecpp/FrAdcData.i"
%include "framecpp/FrDetector.i"
%include "framecpp/FrEvent.i"
%include "framecpp/FrHistory.i"
%include "framecpp/FrProcData.i"
%include "framecpp/FrSimData.i"
%include "framecpp/FrSimEvent.i"
%include "framecpp/FrTable.i"
%include "framecpp/FrTOC.i"

%include "framecpp/FrameH.i"

%include "framecpp/IFrameFStream.i"
%include "framecpp/OFrameFStream.i"

/** \endcond IGNORE_BY_DOXYGEN */
