/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef FRAME_H_I
#define FRAME_H_I

%include <attribute.i>
%include <boost_shared_ptr.i>

%{
#include <string>

#include "ldastoolsal/types.hh"
#include "framecpp/FrameH.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrVect.hh"

using namespace FrameCPP;
%}

%import "framecpp/FrameCPP.hh"
%import "framecpp/FrameH.hh"

%import "FrDetector.i"
%import "FrVect.i"
%import "GPSTime.i"

%import "SearchContainer.i"


%shared_ptr(FrameH)

%feature("autodoc",
"""
Frame Header implementation of the frame specification

  Attributes:
     name         Name of project or other experiment descrition.
                  (e.g., GEO; LIGO; VIRGO; TAMA; ... )
     run          Run number (number < 0 reserved for simulated data);
                  monotonic for experimental runs.
     frame        Frame number, monotonically increasing until end of
                  run, re-starting from 0 wit heach new run.
     dataQuality  A logical 32-bit word to denote top level quality of data.
                  Lowest order bits are reserved in pairs for the various
                  GW detectors.
     dt           Frame length in seconds.
""" ) FrameH;

class FrameH
{
public:

  TYPEDEF_CONTAINER(FrVect,type_type);
  TYPEDEF_CONTAINER(FrVect,user_type);
  TYPEDEF_CONTAINER(FrDetector,detectSim_type);
  TYPEDEF_CONTAINER(FrDetector,detectProc_type);
  TYPEDEF_CONTAINER(FrHistory,history_type);
  TYPEDEF_CONTAINER(FrVect,auxData_type);
  TYPEDEF_CONTAINER(FrTable,auxTable_type);

  typedef FRAME_H_RUN_TYPE run_type;
  typedef FRAME_H_FRAME_TYPE frame_type;
  typedef FRAME_H_DATA_QUALITY_TYPE dataQuality_type;
  typedef FRAME_H_DT_TYPE dt_type;

  const std::string& GetName( ) const;
  run_type GetRun( ) const;
  frame_type GetFrame( ) const;
  dataQuality_type GetDataQuality( ) const;
  const GPSTime& GetGTime( ) const;
  dt_type GetDt( ) const;

  void SetName( const std::string& Name );
  void SetRun( INT_4S Run );
  void SetGTime( const GPSTime& Start );
  void SetDt( REAL_8 Dt );

  %extend {
#if 0 || 1
    const type_type& RefType( );
    const user_type& RefUser( );
    const detectSim_type& RefDetectSim( );
    const detectProc_type& RefDetectProc( );
    const history_type& RefHistory( );
    const auxData_type& RefAuxData( );
    const auxTable_type& RefAuxTable( );
#endif /* 0 */

    void
    AppendFrAdcData( const FrAdcData& Data )
    {
      FrameH::rawData_type raw( self->GetRawData( ) );

      if ( ! raw )
      {
	raw.reset( new FrameH::rawData_type::element_type );

	self->SetRawData( raw );
      }
      raw->RefFirstAdc( ).append( Data );
    }

    void
    AppendFrAdcData( boost::shared_ptr< FrAdcData > Data )
    {
      FrameH::rawData_type raw( self->GetRawData( ) );

      if ( ! raw )
      {
	raw.reset( new FrameH::rawData_type::element_type );

	self->SetRawData( raw );
      }
      raw->RefFirstAdc( ).append( Data );
    }

    void
    AppendFrDetector( const FrDetector& Data )
    {
      self->RefDetectProc( ).append( Data );
    }

    void
    AppendFrDetector( boost::shared_ptr< FrDetector > Data )
    {
      self->RefDetectProc( ).append( Data );
    }

    void
    AppendFrEvent( const FrEvent& Data )
    {
      self->RefEvent( ).append( Data );
    }

    void
    AppendFrHistory( const FrHistory& Data )
    {
      self->RefHistory( ).append( Data );
    }

    void
    AppendFrHistory( boost::shared_ptr< FrHistory > Data )
    {
      self->RefHistory( ).append( Data );
    }

    void
    AppendFrProcData( const FrProcData& Data )
    {
      self->RefProcData( ).append( Data );
    }

    void
    AppendFrProcData( boost::shared_ptr< FrProcData > Data )
    {
      self->RefProcData( ).append( Data );
    }

    void
    AppendFrSimData( const FrSimData& Data )
    {
      self->RefSimData( ).append( Data );
    }

    void
      AppendFrSimData( boost::shared_ptr< FrSimData > Data )
    {
      self->RefSimData( ).append( Data );
    }

    void
      Write( OFrameFStream& Stream,
	     FrVect::compression_scheme_type Compression = FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP,
	     FrVect::compression_level_type Level = 6 )
    {
      OFrameFStream::stream_type*	s = Stream.Stream( );

      if ( s )
      {
	boost::shared_ptr< FrameH >
	  frame( new FrameH( *self ) );

	s->WriteFrame( frame, Compression, Level );
      }
    }
  } // %extend

};

%attribute( FrameH, const std::string&, name, GetName, SetName)
%attribute( FrameH, run_type, run, GetRun, SetRun)
%attribute( FrameH, frame_type, frame, GetFrame)
%attribute( FrameH, dataQuality_type, dataQuality, GetDataQuality)
%attribute( FrameH, const GPSTime&, GTime, GetGTime )
%attribute( FrameH, dt_type, dt, GetDt, SetDt)

%attribute( FrameH, const type_type&, type, RefType )
%attribute( FrameH, const user_type&, user, RefUser )
%attribute( FrameH, const detectSim_type&, detectSim, RefDetectSim )
%attribute( FrameH, const detectProc_type&, detectProc, RefDetectProc )
%attribute( FrameH, const history_type&, history, RefHistory )
%attribute( FrameH, const auxData_type&, auxData, RefAuxData )
%attribute( FrameH, const auxTable_type&, auxTable, RefAuxTable )

#if ! SWIGIMPORTED

MAKE_REF(FrameH,Type,type)
MAKE_REF(FrameH,User,user)
MAKE_REF(FrameH,DetectSim,detectSim)
MAKE_REF(FrameH,DetectProc,detectProc)
MAKE_REF(FrameH,History,history)
MAKE_REF(FrameH,AuxData,auxData)
MAKE_REF(FrameH,AuxTable,auxTable)

#endif /* SWIGIMPORTED */

#endif /* FRAME_H_I */
