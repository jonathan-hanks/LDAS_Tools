/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef SWIG__COMMON__FR_TABLE_I
#define SWIG__COMMON__FR_TABLE_I

%module(package="LDAStools") frameCPP

%include <attribute.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrTable.hh"

using namespace FrameCPP;
%}

%import "framecpp/FrameCPP.hh"
%import "framecpp/FrTable.hh"

%import "SearchContainer.i"
%import "FrVect.i"

%feature("autodoc",
"""
FrTable implementation of the frame specification.

  Attributes:

    name           Name of simulated data.
    comment        Comment.
    column         Array of columns of table.
""" ) FrTable;

class FrTable
{
public:
  TYPEDEF_CONTAINER(FrVect,column_type);

  FrTable( );
  FrTable( const std::string& name, INT_4U nrows );

  const std::string& GetName() const; 
  const std::string& GetComment( ) const;

  %extend {
    const column_type& RefColumn( );
  }
};

%attribute( FrTable, const std::string&, name, GetName );
%attribute( FrTable, const std::string&, comment, GetComment );
%attribute( FrTable, const column_type&, column, RefColumn );

#if ! SWIGIMPORTED

MAKE_REF(FrTable,Column,column)

CONTAINER_WRAP(FrTable,FrTable)

#endif /* SWIGIMPORTED */

#endif /* SWIG__COMMON__FR_TABLE_I */
