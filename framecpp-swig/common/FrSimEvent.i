/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef FR_SIM_EVENT_I
#define FR_SIM_EVENT_I

%module(package="LDAStools") frameCPP

%include <attribute.i>
%include <std_pair.i>
%include <std_string.i>
%include <std_vector.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrSimEvent.hh"

// SWIG thinks that Param_type is a global class, so we need to trick the C++
// compiler into understanding this so called global type.
typedef FrameCPP::FrSimEvent::Param_type FrSimEventParam;

// SWIG thinks that Parameters_type is a global class, so we need to trick the C++
// compiler into understanding this so called global type.
typedef FrameCPP::FrSimEvent::ParamList_type FrSimEventParamList;

%}

%import "framecpp/FrameCPP.hh"
%import "framecpp/FrSimEvent.hh"

%import "Container.i"

%feature("autodoc",
"""
FrSimEvent (SimEvent) implementation of the frame specification.

  Attributes:

    name         Name of event.
    comment      Description of event.
    inputs       Input channels and filter parameters to event
                 process.
    GTime        GPS time corresponiding to reference value of
                 event, as defined by the search algorithm.
    timeBefore   Signal duration before (seconds).
    timeAfter    Signal duration after (seconds).
    amplitude    Continuous output amplitude returned by event.
    params       Array of additional event parameters.
""" ) FrSimEvent;

%apply std::string {FR_SIM_EVENT_NAME_TYPE};
%apply double { REAL_8 };

class FrSimEvent
{
public:
  typedef FR_SIM_EVENT_NAME_TYPE		    name_type;
  typedef FR_SIM_EVENT_COMMENT_TYPE		  comment_type;
  typedef FR_SIM_EVENT_INPUTS_TYPE		  inputs_type;
  typedef FR_SIM_EVENT_TIME_TYPE		    time_type;
  typedef FR_SIM_EVENT_TIME_BEFORE_TYPE	timeBefore_type;
  typedef FR_SIM_EVENT_TIME_AFTER_TYPE	timeAfter_type;
  typedef FR_SIM_EVENT_AMPLITUDE_TYPE		amplitude_type;

  FrSimEvent( );

  FrSimEvent( const name_type& name,
	      const comment_type& comment,
	      const inputs_type& inputs,
	      const time_type& time,
	      const timeBefore_type timeBefore,
	      const timeAfter_type timeAfter,
	      const amplitude_type amplitude,
	      const Parameters_type& parameters );

  const name_type& GetName() const;
  const comment_type& GetComment( ) const;
  const inputs_type& GetInputs( ) const;
  const time_type& GetGTime( ) const;
  timeBefore_type GetTimeBefore( ) const;
  timeAfter_type GetTimeAfter( ) const;
  amplitude_type GetAmplitude( ) const;
  const Parameters_type& GetParam( ) const;

};

%attribute( FrSimEvent, %arg( const name_type& ), name, GetName );
%attribute( FrSimEvent, %arg( const comment_type& ), comment, GetComment );
%attribute( FrSimEvent, %arg( const inputs_type& ), inputs, GetInputs );
%attribute( FrSimEvent, %arg( const time_type& ), GTime, GetGTime);
%attribute( FrSimEvent, %arg( timeBefore_type ), timeBefore, GetTimeBefore);
%attribute( FrSimEvent, %arg( timeAfter_type ), timeAfter, GetTimeAfter);
%attribute( FrSimEvent, %arg( amplitude_type ), amplitude, GetAmplitude)
%attribute( FrSimEvent, %arg( const Parameters_type& ), params, GetParam);

CONTAINER_WRAP(FrSimEvent,FrSimEvent)

#endif /* FR_SIM_EVENT_I */
