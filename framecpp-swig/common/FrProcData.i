/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef FR_PROC_DATA_I
#define FR_PROC_DATA_I

%module(package="LDAStools") frameCPP

%include <attribute.i>
%include <boost_shared_ptr.i>
%include <std_string.i>
%include <std_map.i>
%include <std_vector.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrProcData.hh"

using FrameCPP::FrProcData;

typedef FrameCPP::FrProcData::AuxParamList_type AuxParamList_type;
typedef FrameCPP::FrProcData::AuxParam_type AuxParam_type;

typedef  std::vector< AuxParam_type > AuxParameters_type;
%}

%import "framecpp/FrameCPP.hh"
%import "framecpp/FrHistory.hh"
%import "framecpp/FrProcData.hh"
%import "framecpp/FrVect.hh"

%import "Container.i"

%shared_ptr(FrProcData)

typedef struct {
} AuxParam_type;

%template(AuxParameters_type) std::vector< AuxParam_type >;

%feature("autodoc",
"""
FrProcData (Post-processed Data) implementation of the frame specification.

  Attributes:

    name        Data or channel name.
    comment     Comment.
    type        Type of data object.
    subType     Subtye for f-Series (TBD for other types).
    timeOffset  Offset of 1st sample relative to the frame start
                time (seconds). Must be positive and smaller than
                the frame length.
    tRange      Duration of sampled data (tStop-tStart).
    fShift      fShift is the frequency in the origional data
                that corresponds to 0 Hz in the heterodyned series.
                In multidimensional objects this applies to the first
                frequency dimension.
    phase       Phase of hetrodyning signal at start of dataset
                (radians, 0 if unknown).
    fRange      Frequency range (= fMax - fMin, 0 if unknown).
    BW          Resolution bandwidth.
    auxParam    Array of auxiliary parameters.
    data        Data vector.
    history     Channel history

""" ) FrProcData;

class FrProcData
{
public:
  TYPEDEF_CONTAINER(FrVect,aux_type);
  TYPEDEF_CONTAINER(FrTable,table_type);
  TYPEDEF_CONTAINER(FrHistory,history_type);

  typedef FR_PROC_DATA_TIME_OFFSET_TYPE	timeOffset_type;
  typedef FR_PROC_DATA_T_RANGE_TYPE	tRange_type;
  typedef FR_PROC_DATA_F_SHIFT_TYPE	fShift_type;
  typedef FR_PROC_DATA_PHASE_TYPE	phase_type;
  typedef FR_PROC_DATA_F_RANGE_TYPE	fRange_type;
  typedef FR_PROC_DATA_BW_TYPE		BW_type;

  iterable( FrProcData, data_type );

  enum type_type {
    UNKNOWN_TYPE = FrameCPP::FrProcData::UNKNOWN_TYPE,
    TIME_SERIES = FrameCPP::FrProcData::TIME_SERIES,
    FREQUENCY_SERIES = FrameCPP::FrProcData::FREQUENCY_SERIES,
    OTHER_1D_SERIES_DATA = FrameCPP::FrProcData::OTHER_1D_SERIES_DATA,
    TIME_FREQUENCY = FrameCPP::FrProcData::TIME_FREQUENCY,
    WAVELETS = FrameCPP::FrProcData::WAVELETS,
    MULTI_DIMENSIONAL = FrameCPP::FrProcData::MULTI_DIMENSIONAL
  };

  enum subType_type {
    UNKNOWN_SUB_TYPE = FrameCPP::FrProcData::UNKNOWN_SUB_TYPE,
    //---------------------------------------------------------------
    // Subtype for fSeries
    //---------------------------------------------------------------
    DFT = FrameCPP::FrProcData::DFT,
    AMPLITUDE_SPECTRAL_DENSITY = FrameCPP::FrProcData::AMPLITUDE_SPECTRAL_DENSITY,
    POWER_SPECTRAL_DENSITY = FrameCPP::FrProcData::POWER_SPECTRAL_DENSITY,
    CROSS_SPECTRAL_DENSITY = FrameCPP::FrProcData::CROSS_SPECTRAL_DENSITY,
    COHERENCE = FrameCPP::FrProcData::COHERENCE,
    TRANSFER_FUNCTION = FrameCPP::FrProcData::TRANSFER_FUNCTION
  };

  FrProcData( );
  FrProcData( const std::string& Name,
              const std::string& Comment,
              type_type Type,
              subType_type SubType,
              timeOffset_type TimeOffset,
              tRange_type TRange,
              fShift_type FShift,
              phase_type Phase,
              fRange_type FRange,
              BW_type BW );

  const std::string& GetName() const; 
  const std::string& GetComment( ) const;
  type_type GetType( ) const;
  subType_type GetSubType( ) const;
  timeOffset_type GetTimeOffset( ) const;
  tRange_type GetTRange( ) const;
  fShift_type GetFShift( ) const;
  phase_type GetPhase( ) const;
  fRange_type GetFRange( ) const;
  BW_type GetBW( ) const;
  const AuxParameters_type& GetAuxParam( ) const;

  void SetComment( const std::string& Value );
  void SetTimeOffset( timeOffset_type Value );
  void SetTRange( tRange_type Value );
  void SetAuxParam( const AuxParameters_type& Value );

  %extend {
    void
    AppendData( FrVect& Data )
    {
      self->RefData( ).append( Data );
    }

    void
    AppendAuxParam( const char* Name, double Value )
    {
      self->AppendAuxParam( FrProcData::AuxParam_type( Name, Value ) );
    }

    void
    AppendHistory( FrHistory& History )
    {
      self->RefHistory( ).append( History );
    }

    size_t RefDataSize( );
    data_type& RefData( );

    size_t RefAuxSize( );
    aux_type& RefAux( );

    size_t RefHistorySize( );
    history_type& RefHistory( );

    size_t RefTableSize( );
    table_type& RefTable( );
  }
};

%attribute( FrProcData, const std::string&, name, GetName );
%attribute( FrProcData, std::string, comment, GetComment, SetComment );
%attribute( FrProcData, type_type, type, GetType );
%attribute( FrProcData, subType_type, subType, GetSubType );
%attribute( FrProcData, timeOffset_type, timeOffset, GetTimeOffset, SetTimeOffset );
%attribute( FrProcData, tRange_type, tRange, GetTRange, SetTRange );
%attribute( FrProcData, fShift_type, fShift, GetFShift );
%attribute( FrProcData, phase_type, phase, GetPhase );
%attribute( FrProcData, fRange_type, fRange, GetFRange );
%attribute( FrProcData, BW_type, BW, GetBW );
%attribute( FrProcData, const AuxParameters_type&, auxParam, GetAuxParam, SetAuxParam );

%attribute( FrProcData, data_type&, data, RefData );
%attribute( FrProcData, aux_type&, aux, RefAux );
%attribute( FrProcData, table_type&, table, RefTable );
%attribute( FrProcData, history_type&, history, RefHistory );

#if ! SWIGIMPORTED

MAKE_REF( FrProcData, Data, data );
MAKE_REF( FrProcData, Aux, aux );
MAKE_REF( FrProcData, Table, table );
MAKE_REF( FrProcData, History, history );

CONTAINER_WRAP(FrProcData,FrProcData)

#endif /* ! SWIGIMPORTED */

#endif /* FR_PROC_DATA_I */
