
/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#ifndef SWIG__COMMON__FR_DETECTOR_I
#define SWIG__COMMON__FR_DETECTOR_I

%module(package="LDAStools") frameCPP

%include <attribute.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrDetector.hh"

using namespace FrameCPP;
%}

%import "framecpp/FrDetector.hh"

%shared_ptr(FrDetector)

%feature("autodoc",
"""
FrDetector (Detector Data) implementation of the frame specification.

  Attributes:

    name          Instrument name as described in the appendix
                  of the current frame specification document.
    prefix        Channel prefix for this detector as described in
                  the frame specification appendix.
    longitude     Detector vertex longitude, geographical coordinates:
                  radians; Value > 0 => E of Greenwich
    lattiude      Detector vertex latitude, geographical coordinates:
                  radians; Value > 0 => N of Equator
    elevation     Vertex elevation, meters, relative to WGS84 elipsoid.
    armXazimuth   Orientation of X arm, measured in radians Eas of North
                  (0<= Arm X azimuth < 2pi)
    armYazimuth   Orientation of Y arm, measured in radians Eas of North
                  (0<= Arm Y azimuth < 2pi)
    armXaltitude  Altitude (pitch) angle of X arm, measured in radians
                  above horizon (local tangent to WGS84 ellipsoid).
    armYaltitude  Altitude (pitch) angle of Y arm, measured in radians
                  above horizon (local tangent to WGS84 ellipsoid).
    armXmidpoint  Distance between the detector vertex and the middle
                  of the X cavity (meters) (should be zero for bars)
    armYmidpoint  Distance between the detector vertex and the middle
                  of the Y cavity (meters) (should be zero for bars)
""" ) FrDetector;

class FrDetector
{
public:
  typedef enum
    {
      DQO_TAMA_300, ///< TAMA 300
      DQO_VIRGO, ///< VIRGO
      DQO_GEO_600, ///< GEO 600
      DQO_LHO_4K, ///< LIGO LHO 4 km
      DQO_LLO_4K, ///< LIGO LLO 4 km
      DQO_CIT_40, ///< Caltech 40 meters
      DQO_ACIGA, ///< ACIGA
      DQO_KAGRA, ///< KAGRA
      DQO_LIGO_INDIA, ///< LIGO India
      DQO_DYNAMIC_01,
      DQO_DYNAMIC_02,
      DQO_DYNAMIC_03,
      DQO_DYNAMIC_04,
      DQO_DYNAMIC_05,
      DQO_DYNAMIC_06,
      DQO_DYNAMIC_07,
      DQO_UNSET,
      DQO_CALTECH_40_METERS ///< Caltech 40 meters
    } dataQualityOffsets;

  typedef FR_DETECTOR_LONGITUDE_TYPE      longitude_type;
  typedef FR_DETECTOR_LATITUDE_TYPE       latitude_type;
  typedef FR_DETECTOR_ELEVATION_TYPE      elevation_type;
  typedef FR_DETECTOR_ARM_X_AZIMUTH_TYPE  armXazimuth_type;
  typedef FR_DETECTOR_ARM_Y_AZIMUTH_TYPE  armYazimuth_type;
  typedef FR_DETECTOR_ARM_X_ALTITUDE_TYPE armXaltitude_type;
  typedef FR_DETECTOR_ARM_Y_ALTITUDE_TYPE armYaltitude_type;
  typedef FR_DETECTOR_ARM_X_MIDPOINT_TYPE armXmidpoint_type;
  typedef FR_DETECTOR_ARM_Y_MIDPOINT_TYPE armYmidpoint_type;

  // --------------------------------------------------------------------
  /// Default constructor
  // --------------------------------------------------------------------
  FrDetector( );
  // --------------------------------------------------------------------
  /// Explicit constructor
  // --------------------------------------------------------------------
  FrDetector( const std::string& Name,
	      const char* Prefix,
	      const longitude_type Longitude,
	      const latitude_type Latitude,
	      const elevation_type Elevation,
	      const armXazimuth_type ArmXazimuth,
	      const armYazimuth_type ArmYazimuth,
	      const armXaltitude_type ArmXaltitude,
	      const armYaltitude_type ArmYaltitude,
	      const armXmidpoint_type ArmXmidpoint,
	      const armYmidpoint_type ArmYmidpoint );
  //---------------------------------------------------------------------
  /// \brief Retrieve the name of the instrument.
  ///
  /// \return
  ///     The name of the instrument.
  //---------------------------------------------------------------------
  const std::string& GetName( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the prefix of the instrument.
  ///
  /// \return
  ///     The prefix for the instrument.
  //---------------------------------------------------------------------
  const CHAR* GetPrefix( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the longitude of the detector vertex.
  ///
  /// \return
  ///     The longitude of the detector vertex.
  //---------------------------------------------------------------------
  longitude_type GetLongitude( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the latitude of the detector vertex.
  ///
  /// \return
  ///     The latitude of the detector vertex.
  //---------------------------------------------------------------------
  latitude_type GetLatitude( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the vertex elevation of the detector.
  ///
  /// \return
  ///     The vertex elevation of the detector.
  //---------------------------------------------------------------------
  elevation_type GetElevation( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the orientation of X arm of the detector.
  ///
  /// \return
  ///     The orientation of the X arm of the detector.
  //---------------------------------------------------------------------
  armXazimuth_type GetArmXazimuth( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the orientation of Y arm of the detector.
  ///
  /// \return
  ///     The orientation of the Y arm of the detector.
  //---------------------------------------------------------------------
  armYazimuth_type GetArmYazimuth( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the altitude angle of X arm of the detector.
  ///
  /// \return
  ///     The altitude angle of the X arm of the detector.
  //---------------------------------------------------------------------
  armXaltitude_type GetArmXaltitude( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the altitude angle of Y arm of the detector.
  ///
  /// \return
  ///     The altitude angle of the Y arm of the detector.
  //---------------------------------------------------------------------
  armYaltitude_type GetArmYaltitude( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the midpoint of the X arm of the detector.
  ///
  /// \return
  ///     The midpoint of the X arm of the detector.
  //---------------------------------------------------------------------
  armXmidpoint_type GetArmXmidpoint( ) const;

  //---------------------------------------------------------------------
  /// \brief Retrieve the midpoint of the Y arm of the detector.
  ///
  /// \return
  ///     The midpoint of the Y arm of the detector.
  //---------------------------------------------------------------------
  armYmidpoint_type GetArmYmidpoint( ) const;

  static boost::shared_ptr< FrDetector >
    GetDetector( FrDetector::dataQualityOffsets Offset );
};

%attribute( FrDetector, const std::string&, name, GetName );
%attribute( FrDetector, const CHAR*, prefix, GetPrefix );
%attribute( FrDetector, longitude_type, longitude, GetLongitude );
%attribute( FrDetector, latitude_type, latitude, GetLatitude );
%attribute( FrDetector, elevation_type, elevation, GetElevation );
%attribute( FrDetector, armXazimuth_type, armXazimuth, GetArmXazimuth );
%attribute( FrDetector, armYazimuth_type, armYazimuth, GetArmYazimuth );
%attribute( FrDetector, armXaltitude_type, armXaltitude, GetArmXaltitude );
%attribute( FrDetector, armYaltitude_type, armYaltitude, GetArmYaltitude );
%attribute( FrDetector, armXmidpoint_type, armXmidpoint, GetArmXmidpoint );
%attribute( FrDetector, armYmidpoint_type, armYmidpoint, GetArmYmidpoint );

#if ! SWIGIMPORTED
SEARCH_CONTAINER_WRAP(FrDetector,FrDetector,GetName);
#endif /* SWIGIMPORTED */

#endif /* SWIG__COMMON__FR_DETECTOR_I */
