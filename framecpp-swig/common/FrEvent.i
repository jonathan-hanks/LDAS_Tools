/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef FR_EVENT_I
#define FR_EVENT_I

%module(package="LDAStools") frameCPP

%include <attribute.i>
%include <std_pair.i>
%include <std_string.i>
%include <std_vector.i>

%{
#include "ldastoolsal/types.hh"
#include "framecpp/FrEvent.hh"

// SWIG thinks that Param_type is a global class, so we need to trick the C++
// compiler into understanding this so called global type.
typedef FrameCPP::FrEvent::Param_type Param_type;

// SWIG thinks that ParamList_type is a global class, so we need to trick the C++
// compiler into understanding this so called global type.
typedef FrameCPP::FrEvent::ParamList_type ParamList_type;

%}

%import "framecpp/FrameCPP.hh"
%import "framecpp/FrEvent.hh"

%import "Container.i"
%import "DataTypes.i"

%shared_ptr(FrEvent)

%apply std::string {FR_EVENT_PARAM_NAME_TYPE};
%apply double { REAL_8 };

// %template( EventParam_type ) std::pair< FR_EVENT_PARAM_NAME_TYPE, FR_EVENT_PARAM_VALUE_TYPE >;
// %template( EventParams_type ) std::vector< std::pair< FR_EVENT_PARAM_NAME_TYPE, FR_EVENT_PARAM_VALUE_TYPE > >;

%feature("autodoc",
"""
FrEvent (Event) implementation of the frame specification.

  Attributes:

    name         Name of event.
    comment      Description of event.
    inputs       Input channels and filter parameters to event
                 process.
    GTime        GPS time corresponiding to reference value of
                 event, as defined by the search algorithm.
    timeBefore   Signal duration before (seconds).
    timeAfter    Signal duration after (seconds).
    eventStatus  Defined by event search algorithm.
    amplitude    Continuous output amplitude returned by event.
    probability  Likelihood estimate of event, if available
                 (probability = -1 if cannot be estimated).
    statistics   Statistical description of event, if relevant
                 or available.
    params       Array of additional event parameters.
""" ) FrEvent;

class FrEvent
{
public:
  typedef FR_EVENT_NAME_TYPE		name_type;
  typedef FR_EVENT_COMMENT_TYPE		comment_type;
  typedef FR_EVENT_INPUTS_TYPE		inputs_type;
  typedef FR_EVENT_TIME_TYPE		time_type;
  typedef FR_EVENT_TIME_BEFORE_TYPE	timeBefore_type;
  typedef FR_EVENT_TIME_AFTER_TYPE	timeAfter_type;
  typedef FR_EVENT_EVENT_STATUS_TYPE	eventStatus_type;
  typedef FR_EVENT_AMPLITUDE_TYPE	amplitude_type;
  typedef FR_EVENT_PROBABILITY_TYPE	probability_type;
  typedef FR_EVENT_STATISTICS_TYPE	statistics_type;

  FrEvent( );

  FrEvent( const name_type& name,
	   const comment_type& comment,
	   const inputs_type& inputs,
	   const time_type& time,
	   const timeBefore_type timeBefore,
	   const timeAfter_type timeAfter,
	   const eventStatus_type eventStatus,
	   const amplitude_type amplitude,
	   const probability_type prob,
	   const statistics_type& statistics,
     const Parameters_type& parameters );

  const name_type& GetName() const;
  const comment_type& GetComment( ) const;
  const inputs_type& GetInputs( ) const;
  const time_type& GetGTime( ) const;
  timeBefore_type GetTimeBefore( ) const;
  timeAfter_type GetTimeAfter( ) const;
  eventStatus_type GetEventStatus( ) const;
  amplitude_type GetAmplitude( ) const;
  probability_type GetProbability( ) const;
  const statistics_type& GetStatistics( ) const;
  const Parameters_type& GetParam( ) const;

};

%attribute( FrEvent, const name_type&, name, GetName );
%attribute( FrEvent, const comment_type&, comment, GetComment );
%attribute( FrEvent, const inputs_type&, inputs, GetInputs );
%attribute( FrEvent, const time_type&, GTime, GetGTime );
%attribute( FrEvent, timeBefore_type, timeBefore, GetTimeBefore );
%attribute( FrEvent, timeAfter_type, timeAfter, GetTimeAfter );
%attribute( FrEvent, eventStatus_type, eventStatus, GetEventStatus );
%attribute( FrEvent, amplitude_type, amplitude, GetAmplitude );
%attribute( FrEvent, probability_type, probability, GetProbability );
%attribute( FrEvent, const statistics_type&, statistics, GetStatistics );
%attribute( FrEvent, const Parameters_type&, params, GetParam );

CONTAINER_WRAP(FrEvent,FrEvent)

#endif /* FR_EVENT_I */
