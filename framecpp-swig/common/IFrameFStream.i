/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef IFRAME_FSTREAM_I
#define IFRAME_FSTREAM_I

%module(package="LDAStools") frameCPP

%{
#include "framecpp/IFrameStream.hh"
%}

%import "framecpp/Common/FrHeader.hh"
%import "FrameH.i"

class IFrameFStream
{
public:
  enum sub_element_type {
    HISTORY = FrameCPP::FrameH::HISTORY,
    DETECT_SIM = FrameCPP::FrameH::DETECT_SIM,
    DETECT_PROC = FrameCPP::FrameH::DETECT_PROC,
    AUX_DATA = FrameCPP::FrameH::AUX_DATA,
    AUX_TABLE = FrameCPP::FrameH::AUX_TABLE,
    TYPE = FrameCPP::FrameH::TYPE,
    USER = FrameCPP::FrameH::USER
  };

  typedef FR_HEADER__VERSION_TYPE			version_type;
  typedef FR_HEADER__FRAME_LIBRARY_TYPE			frame_library_type;
  typedef FR_HEADER__LIBRARY_REVISION_TYPE		library_revision_type;

  typedef FrameCPP::Common::IFrameStream::size_type	size_type;
  typedef boost::shared_ptr< FrDetector >	        fr_detector_type;
  typedef boost::shared_ptr< FrEvent >		        fr_event_type;
  typedef boost::shared_ptr< FrAdcData >		fr_adc_data_type;
  typedef boost::shared_ptr< FrProcData >	        fr_proc_data_type;
  typedef boost::shared_ptr< FrSimData >		fr_sim_data_type;
  typedef boost::shared_ptr< FrSimEvent >	        fr_sim_event_type;
  typedef boost::shared_ptr< FrameH >		        frame_h_type;
  typedef INT_4U                                        fr_event_offset_type;
  typedef INT_4U                                        fr_sim_event_offset_type;
  typedef boost::shared_ptr< const FrTOC >		        toc_ret_type;

  IFrameFStream( const char* Filename );

  frame_library_type FrameLibrary( ) const;

  std::string FrameLibraryName( ) const;

  size_type GetNumberOfFrames( ) const;

  toc_ret_type GetTOC( ) const;

  library_revision_type LibraryRevision( ) const;

  fr_detector_type ReadDetector( const std::string& Name );

  fr_event_type ReadFrEvent( INT_4U Frame, const std::string& Channel );

  fr_event_type ReadFrEvent( const std::string& EventType, fr_event_offset_type Offset );

  fr_adc_data_type ReadFrAdcData( INT_4U Frame, const std::string& Channel );

  fr_proc_data_type ReadFrProcData( INT_4U Frame, const std::string& Channel );

  fr_sim_data_type ReadFrSimData( INT_4U Frame, const std::string& Channel );

  fr_sim_event_type ReadFrSimEvent( INT_4U Frame, const std::string& Channel );

  fr_sim_event_type ReadFrSimEvent( const std::string& EventType, fr_sim_event_offset_type Offset );

  frame_h_type ReadFrameN( INT_4U Index );

  frame_h_type ReadFrameNSubset( INT_4U Index, INT_4U Elements );

  frame_h_type ReadNextFrame( );

  version_type Version( ) const;

};

#endif /* IFRAME_FSTREAM_I */
