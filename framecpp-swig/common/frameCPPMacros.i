/*
 * LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools frameCPP is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools frameCPP is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */


#ifndef SWIG__COMMON__FRAMECPP_MACROS_I
#define SWIG__COMMON__FRAMECPP_MACROS_I

#ifndef MAKE_REF
%define MAKE_REF(KLASS,BASE,base)

%{
  const KLASS ## :: ## base ## _type&
  KLASS ## _Ref ## BASE( KLASS * Source )
  {
    if ( Source->Ref ## BASE( ).size( ) <= 0 )
    {
      throw std::length_error( #KLASS "::" #base " no data available" );
    }
    return Source->Ref ## BASE( );
  }

  size_t KLASS ## _Ref ## BASE ## Size( KLASS * Source )
  {
    return Source->Ref ## BASE( ).size( );
  }
%}
%enddef
#endif /* MAKE_REF */

#ifndef TYPEDEF_CONTAINER
%define TYPEDEF_CONTAINER(KLASS, NAME)
  using FrameCPP::KLASS::NAME
%enddef
#endif /* TYPEDEF_CONTAINER */

#ifndef iterable
%define iterable( KLASS, Type )
  using FrameCPP::KLASS::Type;
%enddef
#endif /* iterable */

#ifdef ldas_swig_exception_handler
ldas_swig_exception_handler( );
#else /* ldas_swig_exception_handler */
/*
 * Generic handling of exceptions
 */
%exception
{
  //---------------------------------------------------------------------
  // Ensure that no exceptions leave the C++ layer
  //---------------------------------------------------------------------
  try
  {
    $action;
  }
  //-------------------------------------------------------------------
  // Derived from std::logic_failure
  //-------------------------------------------------------------------
  catch ( const std::invalid_argument& Exception )
  {
    SWIG_exception( SWIG_ValueError, const_cast<char*>( e.what() ) );
    goto fail;
  }
  catch ( const std::out_of_range& Exception )
  {
    SWIG_exception( SWIG_IndexError, const_cast<char*>( e.what() ) );
    goto fail;
  }
  //-------------------------------------------------------------------
  // Derived from std::runtime_error
  //-------------------------------------------------------------------
  catch ( const std::overflow_error& Exception )
  {
    SWIG_exception( SWIG_OverflowError, const_cast<char*>( e.what() ) );
    goto fail;
  }
  catch ( const std::range_error& Exception )
  {
    SWIG_exception( SWIG_ValueError, const_cast<char*>( e.what() ) );
    goto fail;
  }
  //-------------------------------------------------------------------
  // Derived from std::exception
  //-------------------------------------------------------------------
  catch ( const std::bad_alloc& Exception )
  {
    SWIG_exception( SWIG_MemoryError, const_cast<char*>( e.what() ) );
    goto fail;
  }
  catch ( const std::runtime_error& Exception )
  {
    SWIG_exception( SWIG_RuntimeError, const_cast<char*>( e.what() ) );
    goto fail;
  }
  //-------------------------------------------------------------------
  // Base
  //-------------------------------------------------------------------
  catch ( const std::exception& Exception )
  {
    SWIG_exception( SWIG_RuntimeError, const_cast<char*>( e.what() ) );
    goto fail;
  }
  catch ( ... )
    {
      SWIG_exception( SWIG_RuntimeError, "Unknown exception thrown" ) );
      goto fail;
    }
}
#endif /* ldas_swig_exception_handler */

#endif /* SWIG__COMMON__FRAMECPP_MACROS_I */
