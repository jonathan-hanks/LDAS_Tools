#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------

#========================================================================
#========================================================================

set( SWIG_FILES
  LDASlogging.i
  )

cx_link_headers( genericAPI ${SWIG_FILES} )

install( FILES ${SWIG_FILES}
  DESTINATION "${CMAKE_INSTALL_FULL_INCLUDEDIR}/genericAPI"
  )
