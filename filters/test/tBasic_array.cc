//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include <sys/types.h>

#include <complex>

#include "ldastoolsal/unittest.h"

#include "filters/basic_array.hh"

using namespace std;
using namespace LDASTools::Testing;

using Filters::basic_array;

UnitTest Test;

template < class T >
void
Test0( )
{
    {
        basic_array< T > v( 0 );
    }

    {
        basic_array< T > v( 1 );
    }

    {
        basic_array< T > v( 100 );
    }

    {
        const basic_array< T > v( 0 );
    }

    {
        const basic_array< T > v( 1 );
    }

    {
        const basic_array< T > v( 100 );
    }

    {
        basic_array< T >* p = new basic_array< T >( 0 );
        delete p;
    }

    {
        basic_array< T >* p = new basic_array< T >( 1 );
        delete p;
    }

    {
        basic_array< T >* p = new basic_array< T >( 100 );
        delete p;
    }

    Test.Check( true ) << "Construction and destruction" << endl;
}

template < class T >
void
Test1( )
{
    {
        basic_array< T > v( 0 );
        delete[] v.release( );
    }

    {
        basic_array< T > v( 1 );
        delete[] v.release( );
    }

    {
        basic_array< T > v( 100 );
        delete[] v.release( );
    }

    {
        basic_array< T >* p = new basic_array< T >( 0 );
        delete[] p->release( );
        delete p;
    }

    {
        basic_array< T >* p = new basic_array< T >( 1 );
        delete[] p->release( );
        delete p;
    }

    {
        basic_array< T >* p = new basic_array< T >( 100 );
        delete[] p->release( );
        delete p;
    }

    Test.Check( true ) << "Construction and destruction after release" << endl;
}

template < class T >
void
Test2( const size_t size )
{
    bool pass = true;

    // Non-const
    {
        basic_array< T > v( size );
        for ( size_t k = 0; k < size; ++k )
        {
            v[ k ] = k;
        }

        for ( size_t k = 0; k < size; ++k )
        {
            const T val = k;
            pass = pass & ( v[ k ] == val );
        }
    }

    Test.Check( pass ) << "Non-const operator[]" << endl;

    pass = true;

    // Const
    {
        basic_array< T > v0( size );
        for ( size_t k = 0; k < size; ++k )
        {
            v0[ k ] = k;
        }

        const basic_array< T >& v = v0;

        for ( size_t k = 0; k < size; ++k )
        {
            const T val = k;
            pass = pass & ( v[ k ] == val );
        }
    }

    Test.Check( pass ) << "Const operator[]" << endl;

    pass = true;

    // Non-const
    {
        basic_array< T > v( size );
        for ( size_t k = 0; k < size; ++k )
        {
            *( v + k ) = k;
        }

        for ( size_t k = 0; k < size; ++k )
        {
            const T val = k;
            pass = pass & ( *( v + k ) == val );
        }
    }

    Test.Check( pass ) << "Non-const T*" << endl;

    pass = true;

    // Const
    {
        basic_array< T > v0( size );
        for ( size_t k = 0; k < size; ++k )
        {
            *( v0 + k ) = k;
        }

        const basic_array< T >& v = v0;

        for ( size_t k = 0; k < size; ++k )
        {
            const T val = k;
            pass = pass & ( *( v + k ) == val );
        }
    }

    Test.Check( pass ) << "Const T*" << endl;
}

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

    if ( Test.IsVerbose( ) )
    {
        cout << "$Id: tBasic_array.cc,v 1.4 2005/11/15 18:35:19 emaros Exp $"
             << std::endl
             << std::flush;
    }

    Test0< int >( );
    Test0< float >( );
    Test0< double >( );
    Test0< complex< float > >( );
    Test0< complex< double > >( );

    Test1< int >( );
    Test1< float >( );
    Test1< double >( );
    Test1< complex< float > >( );
    Test1< complex< double > >( );

    Test2< int >( 1 );
    Test2< float >( 1 );
    Test2< double >( 1 );
    Test2< complex< float > >( 1 );
    Test2< complex< double > >( 1 );

    Test2< int >( 50 );
    Test2< float >( 50 );
    Test2< double >( 50 );
    Test2< complex< float > >( 50 );
    Test2< complex< double > >( 50 );

    Test.Exit( );
}
