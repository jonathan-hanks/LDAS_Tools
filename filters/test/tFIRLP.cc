//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

//! author="Eric Rotthoff/L. S. Finn"
// Test code for FIRLP, KFIRLP

#include <filters_config.h>

#include <string>
#include <stdexcept>
#include <fstream>
#include <valarray>

#include "ldastoolsal/unittest.h"

#include "filters/HannWindow.hh"
#include "filters/RectangularWindow.hh"
#include "filters/FIRLP.hh"

LDASTools::Testing::UnitTest Test;

using namespace std;
using namespace Filters;

// Tests to make sure that given valid data correct results are produced
void
testNominal( )
{
    valarray< double > t( 11 ), r( 11 );

    std::string basename( "tFIRLPH.dat" );
    std::string path;
    if ( getenv( "DATADIR" ) != 0 )
        path = getenv( "DATADIR" );
    else
        path += ".";

    if ( path.size( ) != 0 )
        path += "/";
    path += basename;
    ifstream inputH( path.c_str( ) );

    if ( !inputH )
    {
        Test.Check( false ) << "Missing input file" << endl;
    }

    std::string basename1( "tFIRLPR.dat" );
    std::string path1;
    if ( getenv( "DATADIR" ) != 0 )
        path1 = getenv( "DATADIR" );
    else
        path1 += ".";

    if ( path1.size( ) != 0 )
        path1 += "/";
    path1 += basename1;
    ifstream inputR( path1.c_str( ) );

    if ( !inputR )
    {
        Test.Check( false ) << "Missing input file" << endl;
    }

    // :TODO: Need to check that files actually exist and are properly opened.
    // :TODO: Need to check that correct number elements read

    int i = 0;

    while ( inputH && ( i < t.size( ) ) )
    {
        inputH >> t[ i ];
        i++;
    }
    i = 0;
    while ( inputR && ( i < r.size( ) ) )
    {
        inputR >> r[ i ];
        i++;
    }

    // General testing strategy
    // Create an instance
    // Find the transfer function
    // Compare with known result for given parameters
    // Passes test if results agree to absolute (not fractional) level 1e-10

    valarray< double > b;
    try
    {
        FIRLP test( .9, 10 );
        Test.Check( true, "FIRLP: Initializing default Hanning Filter" );
        test.apply( b );
    }
    catch ( std::exception& e )
    {
        Test.Check( false ) << "FIRLP: Initializing default Hanning Filter ("
                            << e.what( ) << ")" << std::endl;
        b.resize( t.size( ) );
        b = 0.0;
    }

    valarray< double > c;
    try
    {
        RectangularWindow hey;
        FIRLP             test1( .7, 10, hey );
        Test.Check( true, "FIRLP: Initializing Rectangular Filter" );
        test1.apply( c );
    }
    catch ( std::exception& e )
    {
        Test.Check( false )
            << "FIRLP: Initializing Rectangular Filter: " << e.what( )
            << std::endl;
        c.resize( r.size( ) );
        c = 0.0;
    }

    bool btrue = true;
    bool ctrue = true;

    for ( size_t j = 0; j < t.size( ); j++ )
    {
        btrue = btrue && fabs( t[ j ] - b[ j ] ) < 1e-6;
    }
    for ( size_t j = 0; j < r.size( ); j++ )
    {
        ctrue = ctrue && fabs( r[ j ] - c[ j ] ) < 1e-6;
    }

    Test.Check( ctrue, "FIRLP: Rectangular Window design matches Matlab" );
    if ( !ctrue )
    {
        for ( size_t j = 0; j < r.size( ); j++ )
            Test.Message( ) << r[ j ] << "   " << c[ j ] << "   "
                            << r[ j ] - c[ j ] << std::endl;
    }
    Test.Check( btrue, "FIRLP: Hanning Window design matches Matlab" );
    if ( !btrue )
    {
        for ( size_t j = 0; j < t.size( ); j++ )
            Test.Message( ) << t[ j ] << "   " << b[ j ] << "   "
                            << t[ j ] - b[ j ] << std::endl;
    }
}

// Test copy constructor, assignment operator
void
testCopy( )
{

    // Copy constructor general test strategy:
    // Create a class instance and apply it to find the transfer function.
    // Create a second class instance, using the copy constructor.
    // Apply the second (copied) instance to find the transfer function.
    // Compare the transfer functions for *exact* equality.
    // Passes test if exact equality holds

    // Assignment operator general testing strategy:
    // Create a class instance and apply it to find the transfer function.
    // Create a second class instance, with different parameters
    // Apply the second instance to find a transfer function. Discard
    // Assign the first instance to the second instance.
    // Apply the second (assigned to) instance to find the transfer function.
    // Compare the two transfer functions for *exact* equality.
    // Passes test if exact equality holds

    FIRLP              test( .9, 10 );
    valarray< double > t0;

    test.apply( t0 );

    // Check copy constructor: ok if filter action identical
    {
        valarray< double > t1;
        FIRLP              copy1( test );
        copy1.apply( t1 );

        bool tvalue( true );

        tvalue = t0.size( ) == t1.size( ); // Check length

        if ( tvalue )
            for ( size_t j = 0; j < t0.size( ); j++ )
                tvalue =
                    tvalue && t0[ j ] == t1[ j ]; // exact equality required

        Test.Check( tvalue, "FIRLP copy constructor" );
    }

    // Check assignment operator: ok if filter action identical
    {
        valarray< double > t1;
        FIRLP              copy1( .23, 5 );
        copy1.apply( t1 );
        copy1 = test;
        copy1.apply( t1 );
        bool tvalue( true );

        tvalue = t0.size( ) == t1.size( ); // check size

        if ( tvalue )
            for ( size_t j = 0; j < t0.size( ); j++ )
                tvalue =
                    tvalue && t0[ j ] == t1[ j ]; // exact equality required

        Test.Check( tvalue, "FIRLP assignment operator" );
    }
}

// Test mutators, accessors

// [[ERIC: IMPORTANT TO TEST ALSO THAT GET CORRECT FILTER TRANSER FUNCTION
// AFTER PARAMETERS CHANGED.]]

void
testMutator( )
{
    valarray< double > k2( 19 );

    std::string basename( "tFIRLPH2.dat" );
    std::string path;
    if ( getenv( "DATADIR" ) != 0 )
        path = getenv( "DATADIR" );
    else
        path += ".";

    if ( path.size( ) != 0 )
        path += "/";
    path += basename;
    ifstream inputK2( path.c_str( ) );

    if ( !inputK2 )
    {
        Test.Check( false ) << "Missing input file" << endl;
    }

    int i = 0;
    while ( inputK2 && ( i < k2.size( ) ) )
    {
        inputK2 >> k2[ i ];
        i++;
    }
    valarray< double > temp( 1 );
    FIRLP              test( .9, 10 );
    test.apply( temp );

    string what( "FIRLP: Mutate stop-band edge" );

    try
    {
        test.setFc( 0.42 );
        Test.Check( test.getFc( ) == 0.42, what );
    }
    catch ( std::invalid_argument& d )
    {
        Test.Check( false ) << what << " (" << d.what( ) << ")" << std::endl;
    }

    what = "FIRLP: Mutate filter order";
    try
    {
        test.setOrder( 18 );
        Test.Check( test.getOrder( ) == 18, what );
    }
    catch ( std::invalid_argument& i )
    {
        Test.Check( false ) << what << " (" << i.what( ) << ")" << std::endl;
    }
    test.apply( temp );
    what = "FIRLP: Mutated filter matches Matlab generated filter";
    bool check = true;
    for ( unsigned int j = 0; j < temp.size( ); j++ )
        check = check && ( temp[ j ] - k2[ j ] ) < 1e-10;
    Test.Check( check, what );
}

// Test for correct excptions on invalid arguments
// to the constructors

void
tryFIRLP( const string& what, double f, int order )
{
    try
    {
        FIRLP firlp( f, order );
        Test.Check( false, what );
    }
    catch ( std::exception& e )
    {
        Test.Check( true ) << what << " (" << e.what( ) << ")" << std::endl;
    }
}

void
testCExceptions( )
{
    string what;
    what = "FIRLP: non-positive transition frequency";
    tryFIRLP( what, 0, 10 );

    what = "FIRLP: unity transition frequency";
    tryFIRLP( what, 1, 10 );

    what = "FIRLP: order == 0";
    tryFIRLP( what, 0.5, 0 );
}

// Test that mutators throw correct exceptions
void
testMExceptions( )
{
    valarray< double > temp( 1 );
    FIRLP              test( .25, 10 );
    test.apply( temp );

    string what;

    what = "FIRLP: Set passband edge to 0 frequency";
    try
    {
        test.setFc( 0 );
        Test.Check( false, what );
    }
    catch ( std::invalid_argument& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }

    what = "FIRLP: Set passband edge to Nyquist frequency";
    try
    {
        test.setFc( 1 );
        Test.Check( false, what );
    }
    catch ( std::invalid_argument& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }

    what = "FIRLP: Set filter order to 0";
    try
    {
        test.setOrder( 0 );
        Test.Check( false, what );
    }
    catch ( std::invalid_argument& d )
    {
        Test.Check( true ) << what << " (" << d.what( ) << ")" << std::endl;
    }
}

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

    if ( Test.IsVerbose( ) )
        cout << "$Id: tFIRLP.cc,v 1.4 2005/11/15 18:35:19 emaros Exp $"
             << std::endl;

    testNominal( );
    testCopy( );
    testMutator( ); //
    testCExceptions( );
    testMExceptions( );

    Test.Exit( );
}
