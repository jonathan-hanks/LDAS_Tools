//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// Testing program for the LinFilt class
//! author="Charlie Shapiro, Lee Samuel Finn"

// $Id: tLinFilt.cc,v 1.11 2005/11/15 18:35:19 emaros Exp $

#include <filters_config.h>

#include <iostream>
#include <complex>
#include <stdexcept>
#include <string>
#include <vector>

#include "ldastoolsal/unittest.h"

#include "filters/LinFilt.hh"

using namespace std;
using namespace LDASTools::Testing;
using namespace Filters;

UnitTest Test;

const size_t gInlen = 50;

const double gIndata[ gInlen ] = {
    -1.128343864320229,  -1.349277543102495,   -0.261101623061621,
    0.9534654455048185,  0.128644430046645,    0.656467513885396,
    -1.167819364726639,  -0.4606051795061504,  -0.2624399528383327,
    -1.213152068493907,  -1.319436998109537,   0.9312175149954361,
    0.01124489638413373, -0.6451458156911702,  0.8057287931123757,
    0.2316260107804365,  -0.9897596716820042,  1.339585700610388,
    0.2895020345384132,  1.478917057681278,    1.138028012858371,
    -0.6841385851363396, -1.291936044965938,   -0.07292627626364673,
    -0.3305988798927643, -0.8436276391547997,  0.4977696641827825,
    1.488490470903483,   -0.5464758947676226,  -0.8467581638830595,
    -0.2463365280848998, 0.6630241458559077,   -0.8541973744689799,
    -1.201314815339041,  -0.1198694280573872,  -0.06529401484158653,
    0.4852955559165439,  -0.5954909026194759,  -0.1496677438244753,
    -0.4347519311525334, -0.07933022302342058, 1.535152266122148,
    -0.6064828592772656, -1.34736267385024,    0.46938311986633,
    -0.9035669426177764, 0.03587963872947693,  -0.6275312199668315,
    0.535397954249106,   0.552883517423822
};

void
mkpath( std::string& path )
{
    const char* const datadir = getenv( "DATADIR" );
    if ( datadir != 0 )
        path = datadir;
    else
        path = ".";
}

template < class T >
void
read_data( const string& filename, valarray< T >& data )
{
    std::string path;
    mkpath( path );
    if ( path.size( ) != 0 )
        path += "/";
    path += filename;
    std::ifstream in( path.c_str( ) );

    if ( !in )
    {
        Test.Check( false )
            << "Couldn't open input file " << path.c_str( ) << endl;
    }

    vector< T > datavec;
    T           val;
    in >> val;
    while ( in )
    {
        datavec.push_back( val );
        in >> val;
    }

    data.resize( datavec.size( ) );
    copy( datavec.begin( ), datavec.end( ), &data[ 0 ] );
}

template < class T >
double
tol( )
{
    return 1.0e-05;
}

template <>
double
tol< double >( )
{
    return 1.0e-07;
}

template < class TCoeffs, class TIn >
bool
testTrivial( )
{
    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    bool pass = true;

    valarray< TCoeffs > b( 1.0, 1 );
    valarray< TCoeffs > a( 4.0, 1 );

    LinFilt< TCoeffs, TIn > lf( b, a );

    valarray< TIn > x( 100 );
    for ( size_t k = 0; k < x.size( ); k++ )
    {
        x[ k ] = 4 * k;
    }

    valarray< TOut > y;
    lf.apply( y, x );

    for ( size_t k = 0; k < y.size( ); k++ )
    {
        pass = pass && ( y[ k ] == static_cast< const TOut >( k ) );
    }

    // Copy constructor
    {
        valarray< TCoeffs > b( 10 );
        valarray< TCoeffs > a( 8 );

        fill( std::begin( b ), std::end( b ), TCoeffs( 2 ) );
        fill( std::begin( a ), std::end( a ), TCoeffs( 3.5 ) );

        LinFilt< TCoeffs, TIn > lf0( b, a );
        LinFilt< TCoeffs, TIn > lf1( lf0 );

        valarray< TIn > in( gInlen );
        copy( gIndata, gIndata + gInlen, &in[ 0 ] );

        valarray< TOut > out0;
        valarray< TOut > out1;

        lf0.apply( out0, in );
        lf1.apply( out1, in );

        for ( size_t k = 0; k < out0.size( ); ++k )
        {
            pass = pass && ( out0[ k ] == out1[ k ] );
        }
    }

    // operator=
    {
        valarray< TCoeffs > b( 10 );
        valarray< TCoeffs > a( 8 );

        fill( std::begin( b ), std::end( b ), TCoeffs( 2 ) );
        fill( std::begin( a ), std::end( a ), TCoeffs( 3.5 ) );

        LinFilt< TCoeffs, TIn > lf0( b, a );
        LinFilt< TCoeffs, TIn > lf1( valarray< TCoeffs >( 20.0, 10 ) );

        lf1 = lf0;

        valarray< TIn > in( gInlen );
        copy( gIndata, gIndata + gInlen, &in[ 0 ] );

        valarray< TOut > out0;
        valarray< TOut > out1;

        lf0.apply( out0, in );
        lf1.apply( out1, in );

        for ( size_t k = 0; k < out0.size( ); ++k )
        {
            pass = pass && ( out0[ k ] == out1[ k ] );
        }
    }

    return pass;
}

bool
testExcept( const std::string&       name,
            const valarray< float >& a,
            const valarray< float >& b )
{
    bool pass = true;

    std::string aname( "LinFilt(b,a): " );
    aname += name;

    try
    {
        LinFilt< float, float > state( b, a );
        pass = false;
        Test.Check( pass ) << aname << "(no exception thrown)" << std::endl;
    }
    catch ( std::invalid_argument& r )
    {
        pass = true;
        Test.Check( pass ) << aname << "(" << r.what( ) << ")" << std::endl;
    }
    catch ( std::exception& r )
    {
        pass = false;
        Test.Check( pass ) << aname << "unexpected std::exception ("
                           << r.what( ) << ")" << std::endl
                           << std::flush;
    }
    catch ( ... )
    {
        pass = false;
        Test.Check( pass ) << aname << "unexpected exception" << std::endl;
    }

    aname = "LinFilt(b,a): ";
    aname += name;

    bool lfpass = true;
    try
    {
        LinFilt< float, float > lf( b, a );
        lfpass = false;
        Test.Check( lfpass ) << aname << "(no exception thrown)" << std::endl;
    }
    catch ( std::invalid_argument& r )
    {
        lfpass = true;
        Test.Check( lfpass ) << aname << "(" << r.what( ) << ")" << std::endl;
    }
    catch ( std::exception& r )
    {
        lfpass = false;
        Test.Check( lfpass ) << aname << "unexpected std::exception ("
                             << r.what( ) << ")" << std::endl
                             << std::flush;
    }
    catch ( ... )
    {
        lfpass = false;
        Test.Check( lfpass ) << aname << "unexpected exception" << std::endl;
    }

    return lfpass && pass;
}

bool
testExceptions( )
{

    bool              allPass = true;
    valarray< float > b( 1.0, 5 );
    valarray< float > a( 0.0, 3 );

    // a[0] = 0;

    allPass = allPass && testExcept( "a[0] == 0", a, b );

#if 0
  // b = 0;

  a[0] = 5;
  b = 0;

  allPass = allPass && testExcept("b == 0", a, b);
#endif

    // b.size() == 0;

    b.resize( 0 );

    allPass = allPass && testExcept( "b.size() == 0", a, b );

    // a.size() == 0;

    b.resize( 5 );
    b = 1;
    a.resize( 0 );

    allPass = allPass && testExcept( "a.size() == 0", a, b );

    a.resize( 4 );
    a = 1;
    b.resize( 5 );
    b = 1;

    // test exception on input size == 0
    a.resize( 2 );
    a = 1;
    b.resize( 2 );
    b = 1;

    bool pass( false );
    try
    {
        try
        {
            LinFilt< float, float > lf( b, a );
            valarray< float >       x( 0 );
            valarray< float >       y( 1 );
            try
            {
                lf.apply( y, x );
            }
            catch ( std::exception& r )
            {
                pass = true;
            }
            Test.Check( pass ) << "zero length input to apply()" << std::endl
                               << std::flush;
        }
        catch ( std::exception& r )
        {
            Test.Check( false )
                << "Failed at LinFilt(state) : " << r.what( ) << std::endl;
            throw;
        }
    }
    catch ( ... )
    {
        Test.Check( false ) << "Failed at LinFilt state(b,a);" << std::endl;
        throw;
    }

    return pass && allPass;
}

template < class TCoeffs, class TIn >
bool
testFIR( const std::string& ttstr )
{
    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    bool allPass = true;

    {
        const size_t n = 11;

        valarray< TCoeffs > b( 5 );
        for ( size_t k = 0; k < b.size( ); k++ )
        {
            b[ k ] = k;
        }

        const valarray< TCoeffs > a( 1.0, 1 );

        // Results from matlab
        TIn ax[ n ] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };
        const valarray< TIn > x( ax, n );

        TOut ay[ n ] = { 0, -5, -14, -26, -40, -30, -20, -10, 0, 10, 20 };
        const valarray< TOut > y0( ay, n );

        LinFilt< TCoeffs, TIn > lf( b, a );
        valarray< TOut >        y;

        lf.apply( y, x );

        bool pass = true;
        for ( size_t k = 0; k < y0.size( ); k++ )
        {
            if ( y0[ k ] != y[ k ] )
                pass = false;
        }

        Test.Check( pass ) << "FIR filter " << ttstr << std::endl;

        allPass = allPass && pass;
    }

    // FIR filter with split input
    {
        valarray< TCoeffs > b( 5 );
        for ( size_t k = 0; k < b.size( ); k++ )
        {
            b[ k ] = k;
        }

        const valarray< TCoeffs > a( 1.0, 1 );

        // Results from matlab
        const int       nx = 11;
        TIn             ax[ nx ] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };
        valarray< TIn > x( ax, nx );

        TOut ay[ nx ] = { 0, -5, -14, -26, -40, -30, -20, -10, 0, 10, 20 };
        const valarray< TOut > y0( ay, nx );

        const valarray< TIn >& xconst( x );
        const std::slice       y1( 0, 5, 1 );
        const std::slice       y2( 5, nx - 5, 1 );

        valarray< TIn > x1( xconst[ y1 ] );
        valarray< TIn > x2( xconst[ y2 ] );

        valarray< TOut > y( nx );
        valarray< TOut > yp;

        LinFilt< TCoeffs, TIn > lf( b, a );

        lf.apply( yp, x1 );
        y[ y1 ] = yp;

        valarray< TOut > ya;

        LinFilt< TCoeffs, TIn > la( lf );

        la.apply( ya, x2 );

        lf.apply( yp, x2 );

        y[ y2 ] = yp;

        bool pass( yp.size( ) == ya.size( ) );
        Test.Check( pass ) << "FIR filter state (output length) " << ttstr
                           << std::endl
                           << std::flush;

        pass = true;
        for ( size_t k = 0; k < yp.size( ); k++ )
        {
            if ( std::abs( yp[ k ] - ya[ k ] ) > 1e-6 )
                pass = false;
        }

        Test.Check( pass ) << "FIR filter state (values) " << ttstr << std::endl
                           << std::flush;

        pass = true;
        for ( size_t k = 0; k < y0.size( ); k++ )
        {
            if ( y0[ k ] != y[ k ] )
                pass = false;
        }

        Test.Check( pass ) << "FIR filter, split input" << ttstr << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    // FIR filter with 3-way split input, all possible combinations
    {
        bool pass = true;

        valarray< TCoeffs > b( 5 );
        for ( size_t k = 0; k < b.size( ); k++ )
        {
            b[ k ] = k;
        }

        const valarray< TCoeffs > a( 1.0, 1 );

        const int       nx = 11;
        TIn             ax[ nx ] = { -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5 };
        valarray< TIn > x( ax, nx );

        // Results from matlab
        TOut ay[ nx ] = { 0, -5, -14, -26, -40, -30, -20, -10, 0, 10, 20 };
        const valarray< TOut > yCheck( ay, nx );

        for ( size_t k0 = 1; k0 < nx - 2; ++k0 )
        {
            for ( size_t k1 = 1; k1 < nx - k0 - 1; ++k1 )
            {
                for ( size_t k2 = 1; k2 < nx - k0 - k1; ++k2 )
                {
                    const valarray< TIn >& xconst( x );
                    const slice            s0( 0, k0, 1 );
                    const slice            s1( k0, k1, 1 );
                    const slice            s2( k0 + k1, k2, 1 );

                    const valarray< TIn > x0 = xconst[ s0 ];
                    const valarray< TIn > x1 = xconst[ s1 ];
                    const valarray< TIn > x2 = xconst[ s2 ];

                    valarray< TOut > y0;
                    valarray< TOut > y1;
                    valarray< TOut > y2;

                    LinFilt< TCoeffs, TIn > lf( b, a );

                    lf.apply( y0, x0 );
                    lf.apply( y1, x1 );
                    lf.apply( y2, x2 );

                    valarray< TOut > y( k0 + k1 + k2 );

                    y[ s0 ] = y0;
                    y[ s1 ] = y1;
                    y[ s2 ] = y2;

                    for ( size_t k = 0; k < y.size( ); ++k )
                    {
                        if ( yCheck[ k ] != y[ k ] )
                        {
                            pass = false;
                        }
                    }
                }
            }
        }

        Test.Check( pass ) << "FIR filter, 3-way split input " << ttstr
                           << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    return allPass;
}

template < class TCoeffs, class TIn >
bool
testIIR( const std::string& ttstr )
{

    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    bool allPass = true;

    {
        valarray< TCoeffs > b( 1.0, 1 );

        TCoeffs             aa[] = { 1, 0.5 };
        valarray< TCoeffs > a( aa, 2 );

        // Results from matlab
        TIn              ax[] = { 0, 0, 1, 1, 1, 1, 0, 0, 0, 0 };
        valarray< TIn >  x( ax, 10 );
        TOut             ay[] = { 0,     0,       1,          0.5,         0.75,
                      0.625, -0.3125, 0.3125 / 2, -0.3125 / 4, 0.3125 / 8 };
        valarray< TOut > y0( ay, 10 );

        LinFilt< TCoeffs, TIn > lf( b, a );
        valarray< TOut >        y;
        lf.apply( y, x );

        bool pass = true;
        for ( size_t k = 0; k < y0.size( ); k++ )
        {
            if ( y0[ k ] != y[ k ] )
                pass = false;
        }
        Test.Check( pass ) << "all-pole filter " << ttstr << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    // all-pole filter with split input
    {
        valarray< TCoeffs > b( 1.0, 1 );

        TCoeffs             aa[] = { 1, 0.5 };
        valarray< TCoeffs > a( aa, 2 );

        // Results from matlab
        const int        nx( 10 );
        TIn              ax[] = { 0, 0, 1, 1, 1, 1, 0, 0, 0, 0 };
        valarray< TIn >  x( ax, 10 );
        TOut             ay[] = { 0,     0,       1,          0.5,         0.75,
                      0.625, -0.3125, 0.3125 / 2, -0.3125 / 4, 0.3125 / 8 };
        valarray< TOut > y0( ay, 10 );

        std::slice y1( 0, 5, 1 );
        std::slice y2( 5, nx - 5, 1 );

        valarray< TOut > y( nx );
        valarray< TOut > yp;

        const valarray< TIn >& xconst( x );
        valarray< TIn >        x1( xconst[ y1 ] );
        valarray< TIn >        x2( xconst[ y2 ] );

        LinFilt< TCoeffs, TIn > lf( b, a );

        lf.apply( yp, x1 );
        y[ y1 ] = yp;

        lf.apply( yp, x2 );
        y[ y2 ] = yp;

        bool pass = true;
        for ( size_t k = 0; k < y0.size( ); k++ )
            if ( y0[ k ] != y[ k ] )
                pass = false;
        Test.Check( pass ) << "all-pole filter, split input " << ttstr
                           << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    // all-pole filter with 3-way split input, all possible combinations
    {
        bool pass = true;

        valarray< TCoeffs > b( 1.0, 1 );

        TCoeffs             aa[] = { 1, 0.5 };
        valarray< TCoeffs > a( aa, 2 );

        // Results from matlab
        const int       nx( 10 );
        TIn             ax[] = { 0, 0, 1, 1, 1, 1, 0, 0, 0, 0 };
        valarray< TIn > x( ax, 10 );
        TOut            ay[] = { 0,     0,       1,          0.5,         0.75,
                      0.625, -0.3125, 0.3125 / 2, -0.3125 / 4, 0.3125 / 8 };

        const valarray< TOut > yCheck( ay, nx );

        for ( size_t k0 = 1; k0 < nx - 2; ++k0 )
        {
            for ( size_t k1 = 1; k1 < nx - k0 - 1; ++k1 )
            {
                for ( size_t k2 = 1; k2 < nx - k0 - k1; ++k2 )
                {
                    const valarray< TIn >& xconst( x );
                    const slice            s0( 0, k0, 1 );
                    const slice            s1( k0, k1, 1 );
                    const slice            s2( k0 + k1, k2, 1 );

                    const valarray< TIn > x0 = xconst[ s0 ];
                    const valarray< TIn > x1 = xconst[ s1 ];
                    const valarray< TIn > x2 = xconst[ s2 ];

                    valarray< TOut > y0;
                    valarray< TOut > y1;
                    valarray< TOut > y2;

                    valarray< TOut > y( k0 + k1 + k2 );

                    LinFilt< TCoeffs, TIn > lf( b, a );

                    lf.apply( y0, x0 );
                    lf.apply( y1, x1 );
                    lf.apply( y2, x2 );

                    y[ s0 ] = y0;
                    y[ s1 ] = y1;
                    y[ s2 ] = y2;

                    for ( size_t k = 0; k < y.size( ); k++ )
                    {
                        if ( yCheck[ k ] != y[ k ] )
                            pass = false;
                    }
                }
            }
        }

        Test.Check( pass ) << "all-pole filter, 3-way split input " << ttstr
                           << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    return allPass;
}

template < class TCoeffs, class TIn >
bool
checkGenericFilter( const valarray< TIn >&    x,
                    const valarray< double >& yd,
                    LinFilt< TCoeffs, TIn >&  lf,
                    const string&             name )
{
    const double TOL = tol< TIn >( );

    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    valarray< TIn > y( x.size( ) );

    lf.apply( y, x );

    bool pass = true;
    for ( size_t k = 0; k < y.size( ); k++ )
    {
        const double diff = abs( TOut( yd[ k ] ) - y[ k ] );
        if ( diff > TOL )
        {
            pass = false;
        }
#if 0
    cerr << "yd = " << yd[k] << "  "
         << "y = " << y[k] << "  "
         << "diff = " << diff << endl;
#endif
    }

    Test.Check( pass ) << name << std::endl;

    return pass;
}

template < class TCoeffs, class TIn >
bool
testVersusMatlab( const std::string& ttstr )
{

    bool allPass = true;

    // A notch filter at 960 Hz designed in Matlab by Julien Sylvestre
    const TCoeffs aa[] = { 1.0,
                           -5.58368595698463,
                           13.3772104609805,
                           -17.5580224777875,
                           13.3089834651036,
                           -5.52687494560021,
                           0.984777210644897 };

    const TCoeffs bb[] = { 0.992359416060609, -5.55522598414829,
                           13.3431261522984,  -17.5581314120648,
                           13.3431261522984,  -5.55522598414829,
                           0.992359416060609 };

    const valarray< TCoeffs > a( aa, 7 );
    const valarray< TCoeffs > b( bb, 7 );

    const valarray< TCoeffs > a1( 1.0, 1 );
    const valarray< TCoeffs > b1( 0.000001, 1 );

    // We read the input as float because it was generated as REAL_4
    // in LDAS
    valarray< TIn > inData;
    read_data( "tLinFiltIn16384.dat", inData );

    {
        valarray< double > outData;
        read_data( "tLinFiltFIR16384.dat", outData );

        LinFilt< TCoeffs, TIn > lf( b, a1 );
        allPass = allPass &
            checkGenericFilter(
                      inData, outData, lf, "FIR vs. Matlab " + ttstr );
    }

    {
        valarray< double > outData;
        read_data( "tLinFiltIIR16384.dat", outData );

        LinFilt< TCoeffs, TIn > lf( b, a );
        allPass = allPass &
            checkGenericFilter(
                      inData, outData, lf, "IIR vs. Matlab " + ttstr );
    }

    {
        valarray< double > outData;
        read_data( "tLinFiltAP16384.dat", outData );

        LinFilt< TCoeffs, TIn > lf( b1, a );
        allPass = allPass &
            checkGenericFilter(
                      inData, outData, lf, "All-poles vs. Matlab " + ttstr );
    }

    return allPass;
}

//
// Check that filtering float data gets the same result as
// upcasting the input to double, filtering, and downcasting to float
//
template < class TCoeffs >
bool
checkFloatFilter( const valarray< float >&   x,
                  const valarray< TCoeffs >& b,
                  const valarray< TCoeffs >& a,
                  const string&              name )
{
    valarray< float >         y;
    LinFilt< TCoeffs, float > lf( b, a );
    lf.apply( y, x );

    valarray< double > xd( x.size( ) );
    for ( size_t k = 0; k < x.size( ); ++k )
    {
        xd[ k ] = x[ k ];
    }

    LinFilt< TCoeffs, double > lfd( b, a );
    valarray< double >         yd;

    lfd.apply( yd, xd );

    bool pass = true;
    for ( size_t k = 0; k < y.size( ); k++ )
    {
        const double diff = abs( float( yd[ k ] ) - y[ k ] );
        if ( diff > 1.0e-7 )
        {
            pass = false;
        }
#if 0
    cerr << "yd = " << yd[k] << "  "
         << "y = " << y[k] << "  "
         << "diff = " << diff << endl;
#endif
    }

    Test.Check( pass ) << name << std::endl;

    return pass;
}

template < class TCoeffs >
bool
testFloatFilter( const std::string& ttstr )
{

    bool allPass = true;

    // A notch filter at 960 Hz designed in Matlab by Julien Sylvestre
    const TCoeffs aa[] = { 1,
                           -5.58368595698463,
                           13.3772104609805,
                           -17.5580224777875,
                           13.3089834651036,
                           -5.52687494560021,
                           0.984777210644897 };

    const TCoeffs bb[] = { 0.992359416060609, -5.55522598414829,
                           13.3431261522984,  -17.5581314120648,
                           13.3431261522984,  -5.55522598414829,
                           0.992359416060609 };

    const valarray< TCoeffs > a( aa, 7 );
    const valarray< TCoeffs > b( bb, 7 );

    const valarray< TCoeffs > a1( 1.0, 1 );
    const valarray< TCoeffs > b1( 1.0, 1 );

    // We read the input as float because it was generated as REAL_4
    // in LDAS
    valarray< float > inData( 16384 );
    read_data( "tLinFiltIn16384.dat", inData );

    allPass =
        allPass & checkFloatFilter( inData, b, a1, "FIR single-precision" );

    allPass =
        allPass & checkFloatFilter( inData, b, a, "IIR single-precision" );

    allPass = allPass &
        checkFloatFilter( inData, b1, a, "All-poles single-precision" );

    return allPass;
}

// Std::Complex version of FIR
template < class PCoeffs, class PIn >
bool
testCFIR( const std::string& ttstr )
{
    typedef std::complex< PCoeffs > TCoeffs;
    typedef std::complex< PIn >     TIn;

    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    bool allPass = true;
    {
        valarray< TCoeffs > b( 5 );
        for ( size_t k = 0; k < 5; k++ )
        {
            b[ k ] = k;
        }

        valarray< TCoeffs > a( 1.0, 1 );

        TIn ax[] = { TIn( -5, 1.4142136 ), TIn( -4, 1.4142136 ),
                     TIn( -3, 1.4142136 ), TIn( -2, 1.4142136 ),
                     TIn( -1, 1.4142136 ), TIn( 0, 1.4142136 ),
                     TIn( 1, 1.4142136 ),  TIn( 2, 1.4142136 ),
                     TIn( 3, 1.4142136 ),  TIn( 4, 1.4142136 ),
                     TIn( 5, 1.4142136 ) };

        // Construct a std::complex sequence with imag = 0
        valarray< TIn > x( ax, 11 );

        // Results from matlab
        TOut ay[] = { TOut( 0, 0 ),           TOut( -5, 1.4142136 ),
                      TOut( -14, 4.2426407 ), TOut( -26, 8.4852814 ),
                      TOut( -40, 14.142136 ), TOut( -30, 14.142136 ),
                      TOut( -20, 14.142136 ), TOut( -10, 14.142136 ),
                      TOut( 0, 14.142136 ),   TOut( 10, 14.142136 ),
                      TOut( 20, 14.142136 ) };

        valarray< TOut > y0( ay, 11 );

        LinFilt< TCoeffs, TIn > lf( b, a );
        valarray< TOut >        y;

        lf.apply( y, x );

        bool pass = true;
        for ( size_t k = 0; k < y0.size( ); k++ )
        {
            const double diff = std::abs( y0[ k ] - y[ k ] );
            if ( diff > 1E-05 )
            {
                pass = false;
                Test.Message( )
                    << "k: " << k << "  y: " << y[ k ] << "  y0: " << y0[ k ]
                    << "  diff = " << diff << std::endl;
            }
        }
        Test.Check( pass ) << "FIR filter " << ttstr << std::endl;

        allPass = allPass && pass;
    }

    return allPass;
}

template < class PCoeffs, class PIn >
bool
testCIIR( const std::string& ttstr )
{
    typedef std::complex< PCoeffs > TCoeffs;
    typedef std::complex< PIn >     TIn;

    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    bool allPass = true;

    {
        // filter set-up

        valarray< TCoeffs > a( 2 );
        a[ 0 ] = 1.0;
        a[ 1 ] = -0.5;

        valarray< TCoeffs > b( 1 );
        b[ 0 ] = 1;

        const TIn x_raw[] = {
            TIn( 2, 1 ),
            TIn( 0, 0 ),
            TIn( 0, 0 ),
            TIn( 0, 0 ),
        };

        const TOut y_raw[] = {
            TOut( 2, 1 ), TOut( 1, 0.5 ), TOut( 0.5, 0.25 ), TOut( 0.25, 0.125 )
        };

        // continuous input

        valarray< TIn > x( x_raw, 4 );

        valarray< TOut > y_expected( y_raw, 4 );

        LinFilt< TCoeffs, TIn > filter( b, a );

        valarray< TOut > y;

        filter.apply( y, x );

        bool pass = true;

        for ( unsigned int i = 0; i < y_expected.size( ); ++i )
        {
            // Test.Message() << y[i] << " ?= " << y_expected[i] << std::endl;
            pass = pass && ( y[ i ] == y_expected[ i ] );
        }

        allPass = allPass && pass;

        Test.Check( pass ) << "IIR filter " << ttstr << std::endl;

        // split input

        valarray< TIn > x_1( x_raw, 2 );
        valarray< TIn > x_2( x_raw + 2, 2 );

        valarray< TOut > y_expected_1( y_raw, 2 );
        valarray< TOut > y_expected_2( y_raw + 2, 2 );

        // Reset the filter
        filter.reset( );

        pass = true;

        filter.apply( y, x_1 );

        for ( unsigned int i = 0; i < y_expected_1.size( ); ++i )
        {
            // Test.Message() << y[i] << " ?= " << y_expected_1[i] << std::endl;
            pass = pass && ( y[ i ] == y_expected_1[ i ] );
        }

        filter.apply( y, x_2 );

        for ( unsigned int i = 0; i < y_expected_1.size( ); ++i )
        {
            // Test.Message() << y[i] << " ?= " << y_expected_2[i] << std::endl;
            pass = pass && ( y[ i ] == y_expected_2[ i ] );
        }

        allPass = allPass && pass;

        Test.Check( pass ) << "split IIR filter " << ttstr << std::endl;
    }

    return allPass;
}

template < class TCoeffs, class TIn >
bool
testPZFilter( const std::string& ttstr )
{
    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    TCoeffs                   ab[] = { 1, 0.5, 0.25 };
    const valarray< TCoeffs > b( ab, 3 );

    TCoeffs                   aa[] = { 1, 0.5 };
    const valarray< TCoeffs > a( aa, 2 );

    // Results from matlab
    TIn                   ax[] = { 0, 0, 1, 1, 1, 1, 0, 0, 0, 0 };
    const size_t          nx = 10;
    const valarray< TIn > x( ax, nx );
    TOut                  ay[] = {
        0, 0, 1, 1, 1.25, 1.125, 0.1875, 0.15625, -0.078125, 0.0390625
    };
    const valarray< TOut > yCheck( ay, nx );

    bool allPass = true;
    {
        LinFilt< TCoeffs, TIn > lf( b, a );
        valarray< TOut >        y;

        lf.apply( y, x );

        bool pass = true;
        for ( size_t k = 0; k < yCheck.size( ); k++ )
        {
            if ( yCheck[ k ] != y[ k ] )
                pass = false;
        }
        Test.Check( pass ) << "full filter " << ttstr << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    // full filter with split input
    {
        std::slice y1( 0, 5, 1 );
        std::slice y2( 5, 5, 1 );

        LinFilt< TCoeffs, TIn > lf( b, a );

        valarray< TOut > y( 10 );
        valarray< TOut > yp;
        valarray< TIn >  x1( x[ y1 ] );
        valarray< TIn >  x2( x[ y2 ] );

        lf.apply( yp, x1 );
        y[ y1 ] = yp;

        lf.apply( yp, x2 );
        y[ y2 ] = yp;

        bool pass = true;
        for ( size_t k = 0; k < yCheck.size( ); k++ )
        {
            if ( yCheck[ k ] != y[ k ] )
                pass = false;
        }
        Test.Check( pass ) << "full filter, split input " << ttstr << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    // PZ filter with 3-way split input, all possible combinations
    {
        bool pass = true;

        for ( size_t k0 = 1; k0 < nx - 2; ++k0 )
        {
            for ( size_t k1 = 1; k1 < nx - k0 - 1; ++k1 )
            {
                for ( size_t k2 = 1; k2 < nx - k0 - k1; ++k2 )
                {
                    const slice s0( 0, k0, 1 );
                    const slice s1( k0, k1, 1 );
                    const slice s2( k0 + k1, k2, 1 );

                    const valarray< TIn > x0 = x[ s0 ];
                    const valarray< TIn > x1 = x[ s1 ];
                    const valarray< TIn > x2 = x[ s2 ];

                    valarray< TOut > y0;
                    valarray< TOut > y1;
                    valarray< TOut > y2;

                    const size_t     sz = k0 + k1 + k2;
                    valarray< TOut > y( sz );

                    LinFilt< TCoeffs, TIn > lf( b, a );

                    lf.apply( y0, x0 );
                    lf.apply( y1, x1 );
                    lf.apply( y2, x2 );

                    y[ s0 ] = y0;
                    y[ s1 ] = y1;
                    y[ s2 ] = y2;

                    for ( size_t k = 0; k < y.size( ); k++ )
                    {
                        if ( yCheck[ k ] != y[ k ] )
                            pass = false;
                    }
                }
            }
        }

        Test.Check( pass ) << "PZ filter, 3-way split input " << ttstr
                           << std::endl
                           << std::flush;

        allPass = allPass && pass;
    }

    return allPass;
}

template < class PCoeffs, class PIn >
bool
testCPZFilter( const std::string& ttstr )
{
    typedef std::complex< PCoeffs > TCoeffs;
    typedef std::complex< PIn >     TIn;

    typedef typename LinFiltTraits< TCoeffs, TIn >::OutType TOut;

    bool allPass = true;

    {
        // filter set-up

        valarray< TCoeffs > a( 3 );
        a[ 0 ] = 1.0;
        a[ 1 ] = 0.5;
        a[ 2 ] = 0.25;

        valarray< TCoeffs > b( 2 );
        b[ 0 ] = 1.0;
        b[ 1 ] = 0.5;

        const TIn x_raw[] = {
            TIn( 2, 1 ),
            TIn( 0, 0 ),
            TIn( 0, 0 ),
            TIn( 0, 0 ),
        };

        const TOut y_raw[] = {
            TOut( 2, 1 ), TOut( 0, 0 ), TOut( -0.5, -0.25 ), TOut( 0.25, 0.125 )
        };

        // continuous input

        valarray< TIn > x( x_raw, 4 );

        valarray< TOut > y_expected( y_raw, 4 );

        LinFilt< TCoeffs, TIn > filter( b, a );

        valarray< TOut > y;

        filter.apply( y, x );

        bool pass = true;

        for ( unsigned int i = 0; i < y_expected.size( ); ++i )
        {
            // Test.Message() << y[i] << " ?= " << y_expected[i] << std::endl;
            pass = pass && ( y[ i ] == y_expected[ i ] );
        }

        allPass = allPass && pass;

        Test.Check( pass ) << "full filter " << ttstr << std::endl;

        // split input

        valarray< TIn > x_1( x_raw, 2 );
        valarray< TIn > x_2( x_raw + 2, 2 );

        valarray< TOut > y_expected_1( y_raw, 2 );
        valarray< TOut > y_expected_2( y_raw + 2, 2 );

        filter.reset( );

        pass = true;

        filter.apply( y, x_1 );

        for ( unsigned int i = 0; i < y_expected_1.size( ); ++i )
        {
            // Test.Message() << y[i] << " ?= " << y_expected_1[i] << std::endl;
            pass = pass && ( y[ i ] == y_expected_1[ i ] );
        }

        filter.apply( y, x_2 );

        for ( unsigned int i = 0; i < y_expected_1.size( ); ++i )
        {
            // Test.Message() << y[i] << " ?= " << y_expected_2[i] << std::endl;
            pass = pass && ( y[ i ] == y_expected_2[ i ] );
        }

        allPass = allPass && pass;

        Test.Check( pass ) << "split full filter " << ttstr << std::endl;
    }

    return allPass;
}

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

    if ( Test.IsVerbose( ) )
    {
        cout << "$Id: tLinFilt.cc,v 1.11 2005/11/15 18:35:19 emaros Exp $"
             << std::endl
             << std::flush;
    }

    try
    {
        testTrivial< float, float >( );
        testTrivial< float, double >( );
        testTrivial< float, complex< float > >( );
        testTrivial< float, complex< double > >( );

        testTrivial< double, float >( );
        testTrivial< double, double >( );
        testTrivial< double, complex< float > >( );
        testTrivial< double, complex< double > >( );

        testTrivial< complex< float >, float >( );
        testTrivial< complex< float >, double >( );
        testTrivial< complex< float >, complex< float > >( );
        testTrivial< complex< float >, complex< double > >( );

        testTrivial< complex< double >, float >( );
        testTrivial< complex< double >, double >( );
        testTrivial< complex< double >, complex< float > >( );
        testTrivial< complex< double >, complex< double > >( );

        testExceptions( );

        testFIR< float, float >( "<float, float>" );
        testFIR< float, double >( "<float, double>" );
        testFIR< float, complex< float > >( "<float, complex<float>" );
        testFIR< float, complex< double > >( "<float, complex<double> >" );

        testFIR< double, float >( "<double, float>" );
        testFIR< double, double >( "<double, double>" );
        testFIR< double, complex< float > >( "<double, complex<float> >" );
        testFIR< double, complex< double > >( "<double, complex<double> >" );

        testFIR< complex< double >, float >( "complex<double>, float" );
        testFIR< complex< double >, double >( "complex<double>, double" );
        testFIR< complex< double >, complex< float > >(
            "complex<double>, complex<float>" );
        testFIR< complex< double >, complex< double > >(
            "complex<double>, complex<double>" );

        testIIR< float, float >( "<float, float>" );
        testIIR< float, double >( "<float, double>" );
        testIIR< float, complex< float > >( "<float, complex<float>" );
        testIIR< float, complex< double > >( "<float, complex<double> >" );

        testIIR< double, float >( "<double, float>" );
        testIIR< double, double >( "<double, double>" );
        testIIR< double, complex< float > >( "<double, complex<float> >" );
        testIIR< double, complex< double > >( "<double, complex<double> >" );

        testIIR< complex< double >, float >( "complex<double>, float" );
        testIIR< complex< double >, double >( "complex<double>, double" );
        testIIR< complex< double >, complex< float > >(
            "complex<double>, complex<float>" );
        testIIR< complex< double >, complex< double > >(
            "complex<double>, complex<double>" );

        testVersusMatlab< double, float >( "<double, float>" );
        testVersusMatlab< double, double >( "<double, double>" );

        testFloatFilter< double >( "<double>" );

        testCFIR< float, float >( "<complex<float>, complex<float>" );
        testCFIR< float, double >( "<complex<float>, complex<double>" );
        testCFIR< double, float >( "<complex<double>, complex<float>" );
        testCFIR< double, double >( "<complex<double>, complex<double>" );

        testCIIR< float, float >( "<complex<float>, complex<float>" );
        testCIIR< float, double >( "<complex<float>, complex<double>" );
        testCIIR< double, float >( "<complex<double>, complex<float>" );
        testCIIR< double, double >( "<complex<double>, complex<double>" );

        testPZFilter< float, float >( "<float, float>" );
        testPZFilter< float, double >( "<float, double>" );
        testPZFilter< float, complex< float > >( "<float, complex<float> >" );
        testPZFilter< float, complex< double > >( "<float, complex<double> >" );

        testPZFilter< double, float >( "<double, float>" );
        testPZFilter< double, double >( "<double, double>" );
        testPZFilter< double, complex< float > >( "<double, complex<float> >" );
        testPZFilter< double, complex< double > >(
            "<double, complex<double> >" );

        testPZFilter< complex< float >, float >( "<complex<float>, float>" );
        testPZFilter< complex< float >, double >( "<complex<float>, double>" );
        testPZFilter< complex< float >, complex< float > >(
            "<complex<float>, complex<float> >" );
        testPZFilter< complex< float >, complex< double > >(
            "<complex<float>, complex<double> >" );

        testPZFilter< complex< double >, float >( "<complex<double>, float>" );
        testPZFilter< complex< double >, double >(
            "<complex<double>, double>" );
        testPZFilter< complex< double >, complex< float > >(
            "<complex<double>, complex<float> >" );
        testPZFilter< complex< double >, complex< double > >(
            "<complex<double>, complex<double> >" );

        testCPZFilter< float, float >( "<float, float>" );
        testCPZFilter< float, double >( "<float, double>" );

        testCPZFilter< double, float >( "<double, float>" );
        testCPZFilter< double, double >( "<double, double>" );
    }
    catch ( const std::exception& e )
    {
        Test.Check( false ) << "Caught exception: " << e.what( ) << endl;
    }

    Test.Exit( );
}
