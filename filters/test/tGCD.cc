//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include "ldastoolsal/unittest.h"

#include "filters/gcd.hh"

using namespace std;
using namespace LDASTools::Testing;

UnitTest Test;

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

    if ( Test.IsVerbose( ) )
    {
        cout << "$Id: tGCD.cc,v 1.2 2005/11/15 18:35:19 emaros Exp $"
             << std::endl
             << std::flush;
    }

    Test.Check( Filters::gcd( 1, 1 ) == 1 ) << "gcd(1,1) == 1" << endl;
    Test.Check( Filters::gcd( 13, 13 ) == 13 ) << "gcd(13,13) == 13" << endl;
    Test.Check( Filters::gcd( 5, 35 ) == 5 ) << "gcd(5,35) == 5" << endl;
    Test.Check( Filters::gcd( 1309, 105 ) == 7 )
        << "gcd(1309,105) == 7" << endl;

    Test.Exit( );
}
