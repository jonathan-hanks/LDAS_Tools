#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#=========================================================================
# Overrides
#=========================================================================

DESTDIR ?= /

LDAS_PYTHON_NAMESPACE ?= $(NAMESPACE)
ifneq ($(LDAS_PYTHON_USE_NAMESPACE),no)
ifneq ($(LDAS_PYTHON_NAMESPACE),)
pkgpythondir=$(pythondir)/$(LDAS_PYTHON_NAMESPACE)
pkgpyexecdir=$(pyexecdir)/$(LDAS_PYTHON_NAMESPACE)
endif # NAMESPACE
endif # LDAS_PYTHON_USE_NAMESPACE

#=========================================================================
# Suplimental rules
#=========================================================================
clean-local: clean-local-python

clean-local-python:
	@rm -rf *_python.stamp *_python_wrap.cc

#=========================================================================
# Rules needed when building Python extensions
#=========================================================================

.SECONDEXPANSION:
%_python.stamp: $$($$*_SWIG) Makefile $$($$*_SWIG_INCLUDES) $$($$*_SWIG_AUXDEPS)
	@rm -f $*_python.stamp
	@touch $*_python.stamp.tmp
	$(AM_V_GEN)$(SWIG) $(SWIGFLAGS) \
		-shadow \
	        -c++ \
	        -importall \
	        -Wall \
		-ignoremissing \
		-I$(srcdir) \
		-I$(top_srcdir)/config/swig $(PYTHON_INCLUDES) \
		 $(AM_CPPFLAGS) $(PKG_SWIGFLAGS) \
		-python -classic \
		-o $*_python_wrap.cc $<
	@mv $*_python.stamp.tmp $*_python.stamp

#........................................................................
# This handles parallel cleanup safely
#........................................................................
.SECONDEXPANSION:
%_python_wrap.cc: %_python.stamp
	@if test -f $@ ; then :; else \
	  trap 'rm -rf $*_python.lock $*_python.stamp' 1 2 13 15; \
	  if mkdir $*_python.lock 2>/dev/null ; then \
	    $(MAKE) $(AM_MAKEFLAGS) $*_python.stamp ; \
	    result=$$?; rm -rf $*_python.lock ; exit $$result; \
	  else \
	    while test -d $*_python.lock; do sleep 1; done; \
	    test -f $*_python.stamp ; \
	  fi; \
	fi
