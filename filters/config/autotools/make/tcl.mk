#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


#=========================================================================
# Rules for building the tcl index file
#=========================================================================
createPkgIndex: $(ldas_top_srcdir)/build/make-macros/tcl.mk
	echo '#!/bin/sh' > $@
	echo '# the next line restarts using tclsh'\\ >> $@
	echo 'exec tclsh "$$0" "$$@"' >> $@
	echo '#------------------------------------------------------------------------' >> $@
	echo '# This creates the pkgIndex file needed by tcl scripts' >> $@
	echo '#------------------------------------------------------------------------' >> $@
	echo 'eval pkg_mkIndex $$argv' >> $@
	chmod 0755 $@

#=========================================================================
# Rules needed when building TCL extensions
#=========================================================================

.SECONDEXPANSION:
%_tcl.stamp: $$($$*_SWIG) Makefile $($$*_SWIG_INCLUDES)
	@rm -f $*_tcl.stamp
	@touch $*_tcl.stamp.tmp
	$(AM_V_GEN)$(SWIG) $(SWIGFLAGS) \
		$(TCL_INCLUDE_SPEC) -I$(srcdir) \
		-I$(ldas_top_srcdir)/build/swig \
		-I$(ldas_top_builddir)/include $(AM_CPPFLAGS) \
		-tcl8 -c++ -pkgversion $(LDAS_PACKAGE_VERSION) \
		-o $*_tcl_wrap.cc $<
	@mv $*_tcl.stamp.tmp $*_tcl.stamp

#........................................................................
# This handles parallel cleanup safely
#........................................................................
%_tcl_wrap.cc: $$($$*_SWIG) Makefile $($$*_SWIG_INCLUDES)
	@if test -f $@ ; then :; else \
	  trap 'rm -rf $*_tcl.lock $*_tcl.stamp' 1 2 13 15; \
	  if mkdir $*_tcl.lock 2>/dev/null ; then \
	    rm -f $*_tcl.stamp; \
	    $(MAKE) $(AM_MAKEFLAGS) $*_tcl.stamp ; \
	    result=$$?; rm -rf $*_tcl.lock ; exit $$result; \
	  else \
	    while test -d $*_tcl.lock; do sleep 1; done; \
	    test -f $*_tcl.stamp ; \
	  fi; \
	fi
