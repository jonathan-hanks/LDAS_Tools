#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#

AC_DEFUN([AX_LDAS_CHECK_FUNC_SWAP],
[
AC_MSG_CHECKING([for $1])
AC_LINK_IFELSE([AC_LANG_PROGRAM([
#include <stdlib.h>
#if HAVE_BYTESWAP_H
#include <byteswap.h>
#else /* HAVE_BYTESWAP_H */
#include <sys/types.h>
#if HAVE_SYS_BYTEORDER_H
#include <sys/byteorder.h>
#endif /* HAVE_SYS_BYTEORDER_H */
#if HAVE_MACHINE_BSWAP_H
#include <machine/bswap.h>
#endif /* HAVE_MACHINE_BSWAP_H */
#endif /* HAVE_BYTESWAP_H */
#if HAVE_LIBKERN_OSBYTEORDER_H
#include <libkern/OSByteOrder.h>
#endif /* HAVE_LIBKERN_OSBYTEORDER_H */
],[
#if SIZEOF_SHORT == 2
#define int16 short
#endif
#if SIZEOF_INT == 4
#define int32 int
#endif
#if SIZEOF_LONG == 8
#define int64 long
#elif SIZEOF_LONG_LONG == 8
#define int64 long long
#endif

unsigned $3 buf;
(void)$1( buf )
])],[dnl TRUE
AC_DEFINE([$2],[1],[Defined if $1 is available])
$2=1
AC_MSG_RESULT([yes])
],[dnl FALSE
$2=0
AC_MSG_RESULT([no])
] ) dnl AC_LINK_IFELSE
AC_SUBST([$2])
] ) dnl AC_DEFUN - LDAS_CHECK_FUNC_SWAP
