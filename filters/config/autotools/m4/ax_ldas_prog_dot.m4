dnl======================================================================
dnl AX_LDAS_PROG_DOT
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_PROG_DOT],
[ AC_CACHE_CHECK([whether dot command works],
	         [ldas_cv_shell_dot],
	         [AC_ARG_WITH([dot],
			      [AS_HELP_STRING([--with-dot],
					      [enable support for the dot command])],
			      [],
			      [with_dot=yes])
		  AS_IF([test "x$with_dot" = xyes],
		  	[AC_PATH_PROGS( ldas_cv_shell_dot, dot )],
			[AS_IF([test "x$with_dot" != xno],
			       [ldas_cv_shell_dot="$with_dot"])])
  	          case x$ldas_cv_shell_dot in
                  x) ldas_cv_shell_dot="no";;
                  *) ( $ldas_cv_shell_dot < /dev/null ) 2>/dev/null || ldas_cv_shell_dot=""
	             ;;
		  esac])
  DOT="$ldas_cv_shell_dot"
  case x$DOT in
  x|xno) unset DOT;;
  *)
    DOT_PATH="`echo $DOT | sed -e 's,/[^/]*$,,'`"
    ;;
  esac
  AC_SUBST([DOT_PATH])
  AM_CONDITIONAL([HAVE_DOT],[test x$DOT != x])
])
