dnl======================================================================
dnl AX_LDAS_PROG_ABI_DUMPER
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_PROG_ABI_DUMPER],
[ AC_CACHE_CHECK([whether abi-dumper command works],
	         [ldas_cv_shell_abi_dumper],
	         [AC_ARG_WITH([abi-dumper],
			      [AS_HELP_STRING([--with-abi-dumper],
					      [enable support for the abi-dumper command])],
			      [],
			      [with_abi_dumper=yes])
		  AS_IF([test "x$with_abi_dumper" = xyes],
		  	[AC_PATH_PROGS( ldas_cv_shell_abi_dumper, abi-dumper )],
			[AS_IF([test "x$with_abi_dumper" != xno],
			       [ldas_cv_shell_abi_dumper="$with_abi_dumper"])])
  	          case x$ldas_cv_shell_abi_dumper in
                  x) ldas_cv_shell_abi_dumper="no";;
                  *) ( $ldas_cv_shell_abi_dumper < /dev/null ) 2>/dev/null || ldas_cv_shell_abi_dumper=""
	             ;;
		  esac])
  ABI_DUMPER="$ldas_cv_shell_abi_dumper"
  case x$ABI_DUMPER in
  x|xno) unset ABI_DUMPER;;
  *)
    ABI_DUMPER_PATH="`echo $ABI_DUMPER | sed -e 's,/[^/]*$,,'`"
    ;;
  esac
  AC_SUBST([ABI_DUMPER_PATH])
  AC_SUBST([ABI_DUMPER])
  AM_CONDITIONAL([HAVE_ABI_DUMPER],[test x$ABI_DUMPER != x])
])
