//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// $Id: FIRLP.cc,v 1.8 2005/11/15 18:35:19 emaros Exp $

#include <filters_config.h>

// System includes
#include <memory>
#include <stdexcept>

// Filter includes
#include "ldastoolsal/toss.hh"
#include "filters/FIRLP.hh"
#include "filters/Sinc.hh"

using LDASTools::Error::toss;

namespace Filters
{

    FIRLP::FIRLP( const double  fc,
                  const int     order,
                  const Window& win /* = HannWindow() */ )
        // The Window.Clone() method returns a ptr to storage created by new.
        // Use auto_ptr to manage the storage
        : m_win( win.Clone( ) )
    {
        setFc( fc );
        setOrder( order );
    }

    FIRLP::FIRLP( const FIRLP& firlp )
        : m_length( firlp.m_length ), m_fc( firlp.m_fc ),
          m_win( firlp.m_win->Clone( ) )
    {
    }

    const FIRLP&
    FIRLP::operator=( const FIRLP& rhs )
    {
        // protect against assignment to self
        if ( this != &rhs )
        {
            m_length = rhs.m_length;
            m_fc = rhs.m_fc;
            m_win.reset( rhs.m_win->Clone( ) );
        }

        return *this;
    }

    FIRLP::~FIRLP( )
    {
    }

    void
    FIRLP::setFc( const double fc )
    {
        if ( ( fc <= 0 ) || ( fc >= 1 ) )
        {
            toss< std::invalid_argument >(
                "FIRLP", __FILE__, __LINE__, "stop-band edge not in (0,1)" );
        }

        m_fc = fc;
    }

    void
    FIRLP::setOrder( const int order )
    {
        if ( order <= 0 )
        {
            toss< std::invalid_argument >(
                "FIRLP", __FILE__, __LINE__, "Order less than one" );
        }

        // Safe to copy int to size_t since we checked that order was positive
        m_length = order + 1;
    }

    template < class T >
    void
    FIRLP::apply( std::valarray< T >& tmp ) const
    {
        if ( tmp.size( ) != m_length )
        {
            tmp.resize( m_length );
        }

        for ( size_t i = 0; i < m_length; i++ )
        {
            tmp[ i ] =
                Filters::Sinc( m_fc * ( i - double( m_length - 1 ) / 2 ) );
        }

        // multiplies tmp by window and returns result in tmp
        // Note that it is safe to do windowing in place like this
        m_win->apply( tmp );

        // Normalisation chosen so that DC component is passed unchanged
        tmp /= tmp.sum( );
    }

    /** \cond ignore */
    template void FIRLP::apply( std::valarray< double >& ) const;

    template void
    FIRLP::apply( std::valarray< std::complex< double > >& ) const;
    /** \endcond ignore */

} // namespace Filters
