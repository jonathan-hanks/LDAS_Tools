//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include "filters/LDASConstants.hh"
#include "filters/HannWindow.hh"

extern "C" {
#include <math.h>
}

namespace Filters
{

    std::string
    HannWindow::name( ) const
    {
        return std::string( "Hann Window" );
    }

    double
    HannWindow::param( ) const
    {
        return 0.0;
    }

    double
    HannWindow::element( const size_t i ) const
    {
        const size_t n = size( );

        if ( n == 1 )
        {
            return 1.0;
        }
        else
        {
            double x = 2 * LDAS_PI * i / ( n - 1 );
            return 0.5 * ( 1.0 - cos( x ) );
        }
    }

    HannWindow*
    HannWindow::Clone( ) const
    {
        return new HannWindow( *this );
    }

} // namespace Filters
