//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef VALARRAY_UTILS_HH
#define VALARRAY_UTILS_HH

#include <complex>
#include <typeinfo>
#include <valarray>

namespace Filters
{

    namespace Private
    {
        template < class OutType, class InType >
        inline void
        valarray_copy_assign( std::valarray< OutType >&      out,
                              const std::valarray< InType >& in )
        {
            // Not the same type, need to do translations
            std::valarray< InType >& inRef =
                const_cast< std::valarray< InType >& >( in );

            std::copy( std::begin( inRef ), std::end( inRef ), std::begin( out ) );
        }

        template <>
        inline void
        valarray_copy_assign( std::valarray< int >&       out,
                              const std::valarray< int >& in )
        {
            // Since they are the same type, just do a copy
            out = in;
        }

        template <>
        inline void
        valarray_copy_assign( std::valarray< float >&       out,
                              const std::valarray< float >& in )
        {
            // Since they are the same type, just do a copy
            out = in;
        }

        template <>
        inline void
        valarray_copy_assign( std::valarray< double >&       out,
                              const std::valarray< double >& in )
        {
            // Since they are the same type, just do a copy
            out = in;
        }
        template <>
        inline void
        valarray_copy_assign( std::valarray< std::complex< float > >&       out,
                              const std::valarray< std::complex< float > >& in )
        {
            // Since they are the same type, just do a copy
            out = in;
        }

        template <>
        inline void
        valarray_copy_assign(
            std::valarray< std::complex< double > >&       out,
            const std::valarray< std::complex< double > >& in )
        {
            // Since they are the same type, just do a copy
            out = in;
        }

    } // namespace Private

    /// \brief retrieve the real component of a valarray
    ///
    /// \param[in] Data
    ///    Data set from which to extract the real compoenent.
    ///
    /// \return
    ///   real component of Data
    ///
    template < class Type >
    std::valarray< Type > real( const std::valarray< Type >& Data );

    /// \brief retrieve the real component of a valarray
    ///
    /// \param[in] Data
    ///   Data set from which to extract the real compoenent.
    ///
    /// \return
    ///   real component of Data
    //
    template < class Type >
    std::valarray< Type >
    real( const std::valarray< std::complex< Type > >& Data );

    /// \brief retrieve the imaginary component of a valarray
    ///
    /// \param[in] Data
    ///   Data set from which to extract the imaginary compoenent.
    ///
    /// \return
    ///   imaginary component of Data
    //
    template < class Type >
    std::valarray< Type > imag( const std::valarray< Type >& Data );

    /// \brief retrieve the imaginary component of a valarray
    ///
    /// \param[in] Data
    ///   Data set from which to extract the imaginary compoenent.
    ///
    /// \return
    ///   imaginary component of Data
    //
    template < class Type >
    std::valarray< Type >
    imag( const std::valarray< std::complex< Type > >& Data );

    /// \brief calculate the absolute value of a valarray
    template < class T >
    std::valarray< T > abs( const std::valarray< std::complex< T > >& in );

    /// \brief calculate the arg of a valarray
    template < class T >
    std::valarray< T > arg( const std::valarray< std::complex< T > >& in );

    /// \brief retrieve the conjugate of a valarray
    template < class Type >
    std::valarray< Type > conj( const std::valarray< Type >& Data );

    /// \brief retrieve the conjugate of a valarray
    template < class Type >
    std::valarray< std::complex< Type > >
    conj( const std::valarray< std::complex< Type > >& Data );

    /// \brief copy from an arbitrary valarray to an arbitrary valarray
    ///
    /// \param[out] out
    ///   target
    /// \param[in] in
    ///   source
    //
    template < class OutType, class InType >
    inline void
    valarray_copy( std::valarray< OutType >&      out,
                   const std::valarray< InType >& in )
    {
        if ( out.size( ) != in.size( ) )
        {
            out.resize( in.size( ) );
        }

        Private::valarray_copy_assign( out, in );
    }

    //
    /// \brief Specialisation to deal with complex<double> to complex<float>
    //
    template <>
    inline void
    valarray_copy( std::valarray< std::complex< float > >&        out,
                   const std::valarray< std::complex< double > >& in )
    {
        if ( out.size( ) != in.size( ) )
        {
            out.resize( in.size( ) );
        }

        for ( size_t k = 0; k < in.size( ); ++k )
        {
            out[ k ] =
                std::complex< float >( in[ k ].real( ), in[ k ].imag( ) );
        }
    }

    template < class Type >
    inline const std::valarray< Type >&
    valarray_copy_slice( const std::slice&            Slice,
                         const std::valarray< Type >& In,
                         std::valarray< Type >&       Out )
    {
        for ( size_t x = Slice.start( ),
                     x_stride = Slice.stride( ),
                     y = 0,
                     y_end = Slice.size( );
              y < y_end;
              x += x_stride, ++y )
        {
            Out[ y ] = In[ x ];
        }
        return Out;
    }
} // namespace Filters

#endif /* VALARRAY_UTILS_HH */
