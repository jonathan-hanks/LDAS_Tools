//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include <stdexcept>

#include "filters/FilterConstants.hh"
#include "filters/Window.hh"
#include "filters/valarray_utils.hh"

namespace
{

    // Return true if T is a single precision object, false otherwise
    template < class T >
    inline bool
    is_float( )
    {
        return false;
    }

    template <>
    inline bool
    is_float< float >( )
    {
        return true;
    }

    template <>
    inline bool
    is_float< std::complex< float > >( )
    {
        return true;
    }

} // namespace

namespace Filters
{

    Window::Window( )
        : m_f_window( ), m_d_window( ), m_mean( 0 ), m_rms( 0 ),
          m_is_float( true )
    {
    }

    Window::~Window( )
    {
    }

    const Window&
    Window::operator=( const Window& rhs )
    {
        if ( &rhs != this )
        {
            m_f_window.resize( rhs.m_f_window.size( ) );
            if ( rhs.m_f_window.size( ) != 0 )
            {
                m_f_window = rhs.m_f_window;
            }

            m_d_window.resize( rhs.m_d_window.size( ) );
            if ( rhs.m_d_window.size( ) != 0 )
            {
                m_d_window = rhs.m_d_window;
            }

            m_mean = rhs.m_mean;
            m_rms = rhs.m_rms;
            m_is_float = rhs.m_is_float;
        }

        return *this;
    }

    void
    Window::populate( )
    {
        const size_t n = size( );

        m_mean = 0.0;
        m_rms = 0.0;

        if ( n != 0 )
        {
            // Do calculation of mean and rms at double precision
            // but window values are only stored at the actual precision.
            // May change this later...
            if ( m_is_float )
            {
                for ( size_t i = 0; i < n; i++ )
                {
                    const double value = element( i );
                    m_f_window[ i ] = value;
                    m_rms += value * value;
                    m_mean += value;
                }
            }
            else
            {
                for ( size_t i = 0; i < n; i++ )
                {
                    const double value = element( i );
                    m_d_window[ i ] = value;
                    m_rms += value * value;
                    m_mean += value;
                }
            }

            m_mean /= n;
            m_rms = std::sqrt( m_rms / n );
        }
    }

    void
    Window::resize( const size_t n )
    {
        if ( n > Filters::MaximumWindowLength )
        {
            throw std::length_error( "window length > maximum allowed value" );
        }

        if ( m_is_float )
        {
            m_f_window.resize( n );
        }
        else
        {
            m_d_window.resize( n );
        }

        populate( );
    }

    void
    Window::multiply_by_window( std::valarray< float >& out )
    {
        out *= m_f_window;
    }

    void
    Window::multiply_by_window( std::valarray< double >& out )
    {
        out *= m_d_window;
    }

    void
    Window::multiply_by_window( std::valarray< std::complex< float > >& out )
    {
        for ( size_t i = 0; i < out.size( ); i++ )
        {
            out[ i ] *= m_f_window[ i ];
        }
    }

    void
    Window::multiply_by_window( std::valarray< std::complex< double > >& out )
    {
        for ( size_t i = 0; i < out.size( ); i++ )
        {
            out[ i ] *= m_d_window[ i ];
        }
    }

    template < class T >
    void
    Window::apply( std::valarray< T >& x )
    {
        if ( is_float< T >( ) != m_is_float )
        {
            // change to the other precision
            m_d_window.resize( 0 );
            m_f_window.resize( 0 );

            m_is_float = !m_is_float;
        }

        // Resize the window if the input size is different
        if ( size( ) != x.size( ) )
        {
            resize( x.size( ) );
        }

        multiply_by_window( x );
    }

    template < class TOut, class TIn >
    void
    Window::apply( std::valarray< TOut >& out, const std::valarray< TIn >& in )
    {
        valarray_copy( out, in );
        apply( out );
    }

    template < class TOut, class TIn >
    void
    Window::operator( )( std::valarray< TOut >&      out,
                         const std::valarray< TIn >& in )
    {
        valarray_copy( out, in );
        apply( out );
    }

    // Instantiations

    /** \cond ignore */
    template void Window::apply( std::valarray< float >&,
                                 const std::valarray< float >& );
    template void
    Window::apply( std::valarray< std::complex< float > >&,
                   const std::valarray< std::complex< float > >& );

    template void Window::apply( std::valarray< float >& );
    template void Window::apply( std::valarray< std::complex< float > >& );
    template void Window::apply( std::valarray< double >& );
    template void Window::apply( std::valarray< std::complex< double > >& );

    template void Window::apply( std::valarray< double >&,
                                 const std::valarray< double >& );
    template void
    Window::apply( std::valarray< std::complex< double > >&,
                   const std::valarray< std::complex< double > >& );

    template void Window::operator( )( std::valarray< float >&,
                                       const std::valarray< float >& );
    template void         Window::
                          operator( )( std::valarray< std::complex< float > >&,
                 const std::valarray< std::complex< float > >& );

    template void Window::operator( )( std::valarray< double >&,
                                       const std::valarray< double >& );
    template void         Window::
                          operator( )( std::valarray< std::complex< double > >&,
                 const std::valarray< std::complex< double > >& );
    /** \endcond ignore */

} // namespace Filters
