//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// $Id: LDASConstants.cc,v 1.2 2005/11/15 18:35:19 emaros Exp $

// Adapted from LALConstants.h from LAL verson 0.3

// DESCRIPTION
// A number of useful constants for LAL.  Computational constants are
// taken from the IEEE standard 754 for binary arithmetic.
// Mathematical constants are from the GNU C math.h header file.
// Physical constants, astrophysical parameters, and cosmological
// parameters are taken from the paper by Particle Data Group,
// R. M. Barnett et al., Phys. Rev. D v.54 p.1, 1996

// Computational constants (dimensionless)

#include <filters_config.h>

#include "filters/LDASConstants.hh"

const int LDAS_REAL4_MANT = 24; // Bits of precision in the mantissa of a REAL4
const double LDAS_REAL4_MAX = 3.40282347e+38; // Largest REAL4
const double LDAS_REAL4_MIN = 1.17549435e-38; // Smallest nonzero REAL4
const double LDAS_REAL4_EPS = 1.19209290e-07; // 0.5^(LDAS_REAL4_MANT-1)
// I.e. the difference between 1 and the next resolveable REAL4
const int LDAS_REAL8_MANT = 53; // Bits of precision in the mantissa of a REAL8
const double LDAS_REAL8_MAX = 1.7976931348623157e+308; // Largest REAL8
const double LDAS_REAL8_MIN = 2.2250738585072014e-308; // Smallest nonzero REAL8
const double LDAS_REAL8_EPS = 2.2204460492503131e-16; // 0.5^(LDAS_REAL8_MANT-1)
// I.e. the difference between 1 and the next resolveable REAL8

// Mathematical constants (dimensionless)

const double LDAS_E = 2.7182818284590452353602874713526625L; // e
const double LDAS_LOG2E = 1.4426950408889634073599246810018922L; // log_2 e
const double LDAS_LOG10E = 0.4342944819032518276511289189166051L; // log_10 e
const double LDAS_LN2 = 0.6931471805599453094172321214581766L; // log_e 2
const double LDAS_LN10 = 2.3025850929940456840179914546843642L; // log_e 10
const double LDAS_SQRT2 = 1.4142135623730950488016887242096981L; // sqrt(2)
const double LDAS_SQRT1_2 = 0.7071067811865475244008443621048490L; // 1/sqrt(2)
const double LDAS_GAMMA = 0.5772156649015328606065120900824024L; // gamma
// Assuming we're not near a black hole or in Tennessee...
const double LDAS_PI = 3.1415926535897932384626433832795029L; // pi
const double LDAS_TWOPI = 6.2831853071795864769252867665590058L; // 2*pi
const double LDAS_PI_2 = 1.5707963267948966192313216916397514L; // pi/2
const double LDAS_PI_4 = 0.7853981633974483096156608458198757L; // pi/4
const double LDAS_1_PI = 0.3183098861837906715377675267450287L; // 1/pi
const double LDAS_2_PI = 0.6366197723675813430755350534900574L; // 2/pi
const double LDAS_2_SQRTPI =
    1.1283791670955125738961589031215452L; // 2/sqrt(pi)

// Physical constants, defined (SI)

const double LDAS_C_SI = 299792458; // Speed of light in vacuo, m s^-1
const double LDAS_EPSILON0_SI = 8.8541878176203898505365630317107503e-12;
// Permittivity of free space, C^2 N^-1 m^-2
const double LDAS_MU0_SI = 1.2566370614359172953850573533118012e-6;
// Permeability of free space, N A^-2
const double LDAS_GEARTH_SI = 9.80665; // Standard gravity, m s^-2

// Physical constants, measured (SI or dimensionless)

const double LDAS_G_SI = 6.67259e-11; // Gravitational constant, N m^2 kg^-2
const double LDAS_H_SI = 6.6260755e-34; // Planck constant, J s
const double LDAS_HBAR_SI = 1.05457266e-34; // Reduced Planck constant, J s
const double LDAS_MPL_SI = 2.17671e-8; // Planck mass, kg
const double LDAS_LPL_SI = 1.61605e-35; // Planck length, m
const double LDAS_TPL_SI = 5.39056e-44; // Planck time, s
const double LDAS_K_SI = 1.380658e-23; // Boltzmann constant, J K^-1
const double LDAS_R_SI = 8.314511; // Ideal gas constant, J K^-1
const double LDAS_MOL = 6.0221367e23; // Avogadro constant, dimensionless
const double LDAS_BWIEN_SI = 2.897756e-3; // Wien displacement law constant, m K
const double LDAS_SIGMA_SI =
    5.67051e-8; // Stefan-Boltzmann constant, W m^-2 K^-4
const double LDAS_AMU_SI = 1.6605402e-27; // Atomic mass unit, kg
const double LDAS_MP_SI = 1.6726231e-27; // Proton mass, kg
const double LDAS_ME_SI = 9.1093897e-31; // Electron mass, kg
const double LDAS_QE_SI = 1.60217733e-19; // Electron charge, C
const double LDAS_ALPHA =
    7.297354677e-3; // Fine structure constant, dimensionless
const double LDAS_RE_SI = 2.81794092e-15; // Classical electron radius, m
const double LDAS_LAMBDAE_SI = 3.86159323e-13; // Electron Compton wavelength, m
const double LDAS_AB_SI = 5.29177249e-11; // Bohr radius, m
const double LDAS_MUB_SI = 9.27401543e-24; // Bohr magneton, J T^-1
const double LDAS_MUN_SI = 5.05078658e-27; // Nuclear magneton, J T^-1

// Astrophysical parameters (SI)

const double LDAS_REARTH_SI = 6.378140e6; // Earth equatorial radius, m
const double LDAS_MEARTH_SI = 5.97370e24; // Earth mass, kg
const double LDAS_RSUN_SI = 6.960e8; // Solar equatorial radius, m
const double LDAS_MSUN_SI = 1.98892e30; // Solar mass, kg
const double LDAS_MRSUN_SI = 1.47662504e3; // Geometrized solar mass, m
const double LDAS_MTSUN_SI = 4.92549095e-6; // Geometrized solar mass, s
const double LDAS_LSUN_SI = 3.846e26; // Solar luminosity, W
const double LDAS_AU_SI = 1.4959787066e11; // Astronomical unit, m
const double LDAS_PC_SI = 3.0856775807e16; // Parsec, m
const double LDAS_YRTROP_SI = 31556925.2; // Tropical year (1994), s
const double LDAS_YRSID_SI = 31558149.8; // Sidereal year (1994), s
const double LDAS_DAYSID_SI = 86164.09053; // Mean sidereal day, s
const double LDAS_LYR_SI = 9.46052817e15; // ``Tropical'' lightyear (1994), m

// Cosmological parameters (SI)

const double LDAS_H0FAC_SI =
    3.2407792903e-18; // Hubble constant prefactor, s^-1
const double LDAS_H0_SI = 2e-18; // Approximate Hubble constant, s^-1
// Hubble constant H0 = h0*HOFAC, where h0 is around 0.65
const double LDAS_RHOCFAC_SI = 1.68860e-9; // Critical density prefactor, J m^-3
const double LDAS_RHOC_SI = 7e-10; // Approximate critical density, J m^-3
// Critical density RHOC = h0*h0*RHOCFAC, where h0 is around 0.65
const double LDAS_TCBR_SI = 2.726; // Cosmic background radiation temperature, K
const double LDAS_VCBR_SI =
    3.695e5; // Solar velocity with respect to CBR, m s^-1
const double LDAS_RHOCBR_SI = 4.177e-14; // Energy density of CBR, J m^-3
const double LDAS_NCBR_SI = 4.109e8; // Number density of CBR photons, m^-3
const double LDAS_SCBR_SI = 3.993e-14; // Entropy density of CBR, J K^-1 m^-3
