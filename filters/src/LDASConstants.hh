//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// $Id: LDASConstants.hh,v 1.1 2002/02/06 20:34:20 Philip.Charlton Exp $

// Adapted from LALConstants.h from LAL verson 0.3

// DESCRIPTION
// A number of useful constants for LAL.  Computational constants are
// taken from the IEEE standard 754 for binary arithmetic.
// Mathematical constants are from the GNU C math.h header file.
// Physical constants, astrophysical parameters, and cosmological
// parameters are taken from the paper by Particle Data Group,
// R. M. Barnett et al., Phys. Rev. D v.54 p.1, 1996

#ifndef LDASCONSTANTS_HH
#define LDASCONSTANTS_HH

// Computational constants (dimensionless)

extern const int
                    LDAS_REAL4_MANT; // Bits of precision in the mantissa of a REAL4
extern const double LDAS_REAL4_MAX; // Largest REAL4
extern const double LDAS_REAL4_MIN; // Smallest nonzero REAL4
extern const double LDAS_REAL4_EPS; // 0.5^(LDAS_REAL4_MANT-1)
// I.e. the difference between 1 and the next resolveable REAL4
extern const int
                    LDAS_REAL8_MANT; // Bits of precision in the mantissa of a REAL8
extern const double LDAS_REAL8_MAX; // Largest REAL8
extern const double LDAS_REAL8_MIN; // Smallest nonzero REAL8
extern const double LDAS_REAL8_EPS; // 0.5^(LDAS_REAL8_MANT-1)
// I.e. the difference between 1 and the next resolveable REAL8

// Mathematical constants (dimensionless)

extern const double LDAS_E; // e
extern const double LDAS_LOG2E; // log_2 e
extern const double LDAS_LOG10E; // log_10 e
extern const double LDAS_LN2; // log_e 2
extern const double LDAS_LN10; // log_e 10
extern const double LDAS_SQRT2; // sqrt(2)
extern const double LDAS_SQRT1_2; // 1/sqrt(2)
extern const double LDAS_GAMMA; // gamma
extern const double LDAS_PI; // pi
extern const double LDAS_TWOPI; // 2*pi
extern const double LDAS_PI_2; // pi/2
extern const double LDAS_PI_4; // pi/4
extern const double LDAS_1_PI; // 1/pi
extern const double LDAS_2_PI; // 2/pi
extern const double LDAS_2_SQRTPI; // 2/sqrt(pi)

// Physical constants, defined (SI)

extern const double LDAS_C_SI; // Speed of light in vacuo, m s^-1
extern const double LDAS_EPSILON0_SI;
// Permittivity of free space, C^2 N^-1 m^-2
extern const double LDAS_MU0_SI;
// Permeability of free space, N A^-2
extern const double LDAS_GEARTH_SI; // Standard gravity, m s^-2

// Physical extern constants, measured (SI or dimensionless)

extern const double LDAS_G_SI; // Gravitational constant, N m^2 kg^-2
extern const double LDAS_H_SI; // Planck constant, J s
extern const double LDAS_HBAR_SI; // Reduced Planck constant, J s
extern const double LDAS_MPL_SI; // Planck mass, kg
extern const double LDAS_LPL_SI; // Planck length, m
extern const double LDAS_TPL_SI; // Planck time, s
extern const double LDAS_K_SI; // Boltzmann constant, J K^-1
extern const double LDAS_R_SI; // Ideal gas constant, J K^-1
extern const double LDAS_MOL; // Avogadro constant, dimensionless
extern const double LDAS_BWIEN_SI; // Wien displacement law constant, m K
extern const double LDAS_SIGMA_SI; // Stefan-Boltzmann constant, W m^-2 K^-4
extern const double LDAS_AMU_SI; // Atomic mass unit, kg
extern const double LDAS_MP_SI; // Proton mass, kg
extern const double LDAS_ME_SI; // Electron mass, kg
extern const double LDAS_QE_SI; // Electron charge, C
extern const double LDAS_ALPHA; // Fine structure constant, dimensionless
extern const double LDAS_RE_SI; // Classical electron radius, m
extern const double LDAS_LAMBDAE_SI; // Electron Compton wavelength, m
extern const double LDAS_AB_SI; // Bohr radius, m
extern const double LDAS_MUB_SI; // Bohr magneton, J T^-1
extern const double LDAS_MUN_SI; // Nuclear magneton, J T^-1

// Astrophysical parameters (SI)

extern const double LDAS_REARTH_SI; // Earth equatorial radius, m
extern const double LDAS_MEARTH_SI; // Earth mass, kg
extern const double LDAS_RSUN_SI; // Solar equatorial radius, m
extern const double LDAS_MSUN_SI; // Solar mass, kg
extern const double LDAS_MRSUN_SI; // Geometrized solar mass, m
extern const double LDAS_MTSUN_SI; // Geometrized solar mass, s
extern const double LDAS_LSUN_SI; // Solar luminosity, W
extern const double LDAS_AU_SI; // Astronomical unit, m
extern const double LDAS_PC_SI; // Parsec, m
extern const double LDAS_YRTROP_SI; // Tropical year (1994), s
extern const double LDAS_YRSID_SI; // Sidereal year (1994), s
extern const double LDAS_DAYSID_SI; // Mean sidereal day, s
extern const double LDAS_LYR_SI; // ``Tropical'' lightyear (1994), m

// Cosmological parameters (SI)

extern const double LDAS_H0FAC_SI; // Hubble constant prefactor, s^-1
extern const double LDAS_H0_SI; // Approximate Hubble constant, s^-1
// Hubble constant H0 = h0*HOFAC, where h0 is around 0.65
extern const double LDAS_RHOCFAC_SI; // Critical density prefactor, J m^-3
extern const double LDAS_RHOC_SI; // Approximate critical density, J m^-3
// Critical density RHOC = h0*h0*RHOCFAC, where h0 is around 0.65
extern const double LDAS_TCBR_SI; // Cosmic background radiation temperature, K
extern const double LDAS_VCBR_SI; // Solar velocity with respect to CBR, m s^-1
extern const double LDAS_RHOCBR_SI; // Energy density of CBR, J m^-3
extern const double LDAS_NCBR_SI; // Number density of CBR photons, m^-3
extern const double LDAS_SCBR_SI; // Entropy density of CBR, J K^-1 m^-3

#endif // LDASCONSTANTS_H
