//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include <stdexcept>

#include "ldastoolsal/compiler.h"

#include "filters/Interpolate.hh"

using namespace std;

namespace
{

    const char* rcsId LDAS_UNUSED_VAR =
        "@(#) $Id: Interpolate.cc,v 1.9 2007/10/25 19:28:32 emaros Exp $";

    //
    // Create an appropriate filter to act on a valarray<T>
    //
    template < class T >
    Filters::LinFilt< double, T >
    createFilter( const double alpha, const size_t order )
    {
        valarray< double > b( double( 0 ), order + 1 );

        // No checking of params needed - the design function will
        // throw exception if order or alpha are invalid
        Filters::designInterpolatingFilter( b, alpha );

        return Filters::LinFilt< double, T >( b );
    }

} // namespace

namespace Filters
{

    template < class T >
    Interpolate< T >::Interpolate( const double alpha, const size_t order )
        : m_alpha( alpha ), m_filt( createFilter< T >( alpha, order ) )
    {
    }

    template < class T >
    void
    Interpolate< T >::apply( std::valarray< T >& x )
    {
        m_filt.apply( x );
    }

    template < class T >
    void
    designInterpolatingFilter( valarray< T >& b, const double alpha )
    {
        // Must use signed arithmetic
        const int N = b.size( );

        if ( N <= 0 )
        {
            throw std::invalid_argument(
                "Filters::designInterpolatingFilter() - "
                "array of coefficients must have size > 0" );
        }

        if ( ( alpha <= 0 ) || ( alpha >= 1 ) )
        {
            throw std::invalid_argument(
                "Filters::designInterpolatingFilter() - "
                "alpha must satisfy 0 < alpha < 1" );
        }

        // This combination is used in the loop so save it
        const T alphaTmp = alpha + ( N - 1 ) / 2;

        // Set all of b to unity first
        b = T( 1 );

        for ( int k = 0; k < N; ++k )
        {
            // Calculate b[k] as an iterated product
            for ( int l = 0; l < N; ++l )
            {
                if ( l != ( N - 1 - k ) )
                {
                    b[ k ] *= ( alphaTmp - T( l ) ) / T( N - 1 - l - k );
                }
            }
        }
    }

    template class Interpolate< float >;
    template class Interpolate< double >;

    template class Interpolate< complex< float > >;
    template class Interpolate< complex< double > >;

    template void designInterpolatingFilter< float >( valarray< float >& b,
                                                      double alpha );
    template void designInterpolatingFilter< double >( valarray< double >& b,
                                                       double alpha );

    template void designInterpolatingFilter< complex< float > >(
        valarray< complex< float > >& b, double alpha );
    template void designInterpolatingFilter< complex< double > >(
        valarray< complex< double > >& b, double alpha );

} // namespace Filters
