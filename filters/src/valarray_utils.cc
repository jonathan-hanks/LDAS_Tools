//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include "filters/valarray_utils.hh"

namespace Filters
{
    template < class Type >
    std::valarray< Type >
    real( const std::valarray< Type >& Data )
    {
        return Data;
    }

    template < class Type >
    std::valarray< Type >
    real( const std::valarray< std::complex< Type > >& Data )
    {
        std::valarray< Type > ret( Data.size( ) );

        for ( size_t i = 0; i < Data.size( ); i++ )
        {
            ret[ i ] = Data[ i ].real( );
        }
        return ret;
    }

    template < class Type >
    std::valarray< Type >
    imag( const std::valarray< Type >& Data )
    {
        std::valarray< Type > ret( Type( 0 ), Data.size( ) );

        return ret;
    }

    template < class Type >
    std::valarray< Type >
    imag( const std::valarray< std::complex< Type > >& Data )
    {
        std::valarray< Type > ret( Data.size( ) );

        for ( size_t i = 0; i < Data.size( ); i++ )
        {
            ret[ i ] = Data[ i ].imag( );
        }
        return ret;
    }

    template < class T >
    std::valarray< T >
    abs( const std::valarray< std::complex< T > >& in )
    {
        std::valarray< T > ret( in.size( ) );

        for ( size_t k = 0; k < in.size( ); ++k )
        {
            ret[ k ] = std::abs( in[ k ] );
        }

        return ret;
    }

    template < class T >
    std::valarray< T >
    arg( const std::valarray< std::complex< T > >& in )
    {
        std::valarray< T > ret( in.size( ) );

        for ( size_t k = 0; k < in.size( ); ++k )
        {
            ret[ k ] = std::arg( in[ k ] );
        }

        return ret;
    }

    template < class Type >
    std::valarray< Type >
    conj( const std::valarray< Type >& Data )
    {
        return Data;
    }

    template < class Type >
    std::valarray< std::complex< Type > >
    conj( const std::valarray< std::complex< Type > >& Data )
    {
        //-------------------------------------------------------------------
        // Increase portability by introduction of temporary variable.
        //-------------------------------------------------------------------
        typename std::complex< Type > ( *func )(
            const typename std::complex< Type >& ) = std::conj;
        return Data.apply( func );
    }

    template std::valarray< float > real( const std::valarray< float >& Data );
    template std::valarray< double >
    real( const std::valarray< double >& Data );

    template std::valarray< float >
    real( const std::valarray< std::complex< float > >& Data );

    template std::valarray< double >
    real( const std::valarray< std::complex< double > >& Data );

    template std::valarray< float > imag( const std::valarray< float >& Data );
    template std::valarray< double >
    imag( const std::valarray< double >& Data );

    template std::valarray< float >
    imag( const std::valarray< std::complex< float > >& Data );

    template std::valarray< double >
    imag( const std::valarray< std::complex< double > >& Data );

    template std::valarray< float >
    abs( const std::valarray< std::complex< float > >& Data );

    template std::valarray< double >
    abs( const std::valarray< std::complex< double > >& Data );

    template std::valarray< float >
    arg( const std::valarray< std::complex< float > >& Data );

    template std::valarray< double >
    arg( const std::valarray< std::complex< double > >& Data );

    template std::valarray< float > conj( const std::valarray< float >& Data );
    template std::valarray< double >
    conj( const std::valarray< double >& Data );

    template std::valarray< std::complex< float > >
    conj( const std::valarray< std::complex< float > >& Data );

    template std::valarray< std::complex< double > >
    conj( const std::valarray< std::complex< double > >& Data );

} // namespace Filters
