//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// $Id: LinFilt.cc,v 1.17 2006/11/07 22:25:47 emaros Exp $

#include <filters_config.h>

#include <iostream>
#include <stdexcept>
#include <string>

#include "filters/valarray_utils.hh"
#include "filters/LinFilt.hh"

namespace
{

    //
    // An attempt at an optimised implementation of
    //
    //    v2 = alpha * v0 + v1
    //
    // where v0 and v1 are vectors (with the same length) and alpha
    // is a scalar
    //
    template < class T,
               typename T0Iterator,
               typename T1Iterator,
               typename T2Iterator >
    inline void
    mul_and_add( const T&   alpha,
                 T0Iterator v0first,
                 T0Iterator v0last,
                 T1Iterator v1first,
                 T2Iterator v2first )
    {
        while ( v0first < v0last )
        {
            *v2first++ = alpha * ( *v0first++ ) + *v1first++;
        }
    }

} // namespace

namespace Filters
{

    using namespace std;

    template < class TCoeffs, class TIn >
    LinFilt< TCoeffs, TIn >::LinFilt( const std::valarray< TCoeffs >& b,
                                      const std::valarray< TCoeffs >& a )
        : m_b( TCo( 0 ), b.size( ) ), m_a( TCo( 0 ), a.size( ) )
    {
        // Check sizes
        if ( m_b.size( ) == 0 )
        {
            throw std::invalid_argument( "LinFilt: b.size() == 0" );
        }

        if ( m_a.size( ) == 0 )
        {
            throw std::invalid_argument( "LinFilt: a.size() == 0" );
        }

        // Copy contents
        valarray_copy( m_b, b );
        valarray_copy( m_a, a );

        // Check first "a" coefficient is non-zero
        const TCo a0 = m_a[ 0 ];
        if ( a0 == TCo( 0 ) )
        {
            throw std::invalid_argument( "LinFilt: a[0] == 0" );
        }

        // Make a and b canonical
        if ( a0 != TCo( 1 ) )
        {
            m_b /= a0;
            m_a /= a0;
        }

        // Resizing of z is deferred until a and b are checked
        //
        // NOTE:
        // To simplify the vector operations, z is one sample
        // longer than strictly needed, but the last value should always
        // be zero.
        m_z.resize( std::max( b.size( ), a.size( ) ), TState( 0 ) );
    }

    template < class TCoeffs, class TIn >
    LinFilt< TCoeffs, TIn >::~LinFilt( )
    {
    }

    template < class TCoeffs, class TIn >
    const LinFilt< TCoeffs, TIn >&
    LinFilt< TCoeffs, TIn >::operator=( const LinFilt& rhs )
    {
        if ( this != &rhs )
        {
            m_b.resize( rhs.m_b.size( ) );
            valarray_copy( m_b, rhs.m_b );

            m_a.resize( rhs.m_a.size( ) );
            valarray_copy( m_a, rhs.m_a );

            m_z.resize( rhs.m_z.size( ) );
            valarray_copy( m_z, rhs.m_z );
        }

        return *this;
    }

    template < class TCoeffs, class TIn >
    bool
    LinFilt< TCoeffs, TIn >::operator==( const LinFilt& rhs )
    {
        if ( this == &rhs )
        {
            return true;
        }
        bool retval = ( m_a.size( ) == rhs.m_a.size( ) );

        if ( retval )
        {
            retval =
                std::equal( std::begin( m_a ),
                            std::end( m_a ),
                            std::begin( const_cast< LinFilt& >( rhs ).m_a ) );
        }
        if ( retval && ( retval = ( m_b.size( ) == rhs.m_b.size( ) ) ) )
        {
            retval =
                std::equal( std::begin( m_b ),
                            std::end( m_b ),
                            std::begin( const_cast< LinFilt& >( rhs ).m_b ) );
        }
        if ( retval && ( retval = ( m_z.size( ) == rhs.m_z.size( ) ) ) )
        {
            retval =
                std::equal( std::begin( m_z ),
                            std::end( m_z ),
                            std::begin( const_cast< LinFilt& >( rhs ).m_z ) );
        }

        return retval;
    }

    template < class TCoeffs, class TIn >
    LinFilt< TCoeffs, TIn >*
    LinFilt< TCoeffs, TIn >::Clone( ) const
    {
        return new LinFilt< TCoeffs, TIn >( *this );
    }

    template < class TCoeffs, class TIn >
    void
    LinFilt< TCoeffs, TIn >::getB( std::valarray< TCoeffs >& b ) const
    {
        b.resize( m_b.size( ) );
        valarray_copy( b, m_b );
    }

    template < class TCoeffs, class TIn >
    void
    LinFilt< TCoeffs, TIn >::getA( std::valarray< TCoeffs >& a ) const
    {
        a.resize( m_a.size( ) );
        valarray_copy( a, m_a );
    }

    template < class TCoeffs, class TIn >
    void
    LinFilt< TCoeffs, TIn >::getZ( std::valarray< TState >& z ) const
    {
        z.resize( m_z.size( ) );
        z = m_z;
    }

    template < class TCoeffs, class TIn >
    void
    LinFilt< TCoeffs, TIn >::reset( )
    {
        m_z = TState( 0 );
    }

    template < class TCoeffs, class TIn >
    void
    LinFilt< TCoeffs, TIn >::apply( std::valarray< TOut >& x )
    {
        const size_t xSize = x.size( );

        if ( xSize == 0 )
        {
            throw std::invalid_argument(
                "LinFilt::apply(): zero length input" );
        }

        const size_t aSize = m_a.size( );
        const size_t bSize = m_b.size( );

        if ( ( aSize > 1 ) && ( bSize > 1 ) ) // General IIR filter
        {

            for ( size_t k = 0; k < xSize; ++k )
            {
                // Store old x[k] for later
                const TState x_old = x[ k ];

                // Calculate x'[k], being careful to store it in x_k at
                // double precision for later use
                const TState x_k = x_old * m_b[ 0 ] + m_z[ 0 ];

                // Store the new value of x[k] - may involve loss of precision
                x[ k ] = TOut( x_k );

                // Calculate new z values

                // z += x[k]*b - x'[k]*a;

                // First shift z left by 1
                copy(
                    std::begin( m_z ) + 1, std::end( m_z ), std::begin( m_z ) );

                // Add x[k]*b to z
                mul_and_add( x_old,
                             std::begin( m_b ) + 1,
                             std::end( m_b ),
                             std::begin( m_z ),
                             std::begin( m_z ) );

                // Add -x'[k]*a to z
                mul_and_add( -x_k,
                             std::begin( m_a ) + 1,
                             std::end( m_a ),
                             std::begin( m_z ),
                             std::begin( m_z ) );
            }
        }
        else if ( bSize > 1 ) // FIR filter
        {

            for ( size_t k = 0; k < xSize; ++k )
            {
                // Store old x[k] for later
                const TState x_old = x[ k ];

                // Calculate x'[k]
                //
                // Construction of TOut is needed to allow conversion from
                // complex<double> to complex<float>
                //
                x[ k ] = TOut( x_old * m_b[ 0 ] + m_z[ 0 ] );

                // Calculates new z values
                //
                // z[slice(0, bSize - 1, 1)] =
                //     x[k] * b[slice(1, bSize - 1, 1)] + z[slice(1, bSize - 1,
                //     1)]
                //
                mul_and_add( x_old,
                             std::begin( m_b ) + 1,
                             std::end( m_b ),
                             std::begin( m_z ) + 1,
                             std::begin( m_z ) );
            }
        }
        else if ( aSize > 1 ) // All-pole filter
        {

            for ( size_t k = 0; k < xSize; ++k )
            {
                // Store old x[k] for later
                const TState x_old = x[ k ];

                // Calculate x'[k], being careful to store it in x_k at
                // double precision for later use
                const TState x_k = x_old * m_b[ 0 ] + m_z[ 0 ];

                // Store the new value of x[k] - may involve loss of precision
                x[ k ] = TOut( x_k );

                // Calculates new z values
                //
                // z[slice(0, aSize - 1, 1)] =
                //     -x'[k] * a[slice(1, aSize - 1, 1)] + z[slice(1, aSize -
                //     1, 1)]
                //
                mul_and_add( -x_k,
                             std::begin( m_a ) + 1,
                             std::end( m_a ),
                             std::begin( m_z ) + 1,
                             std::begin( m_z ) );
            }
        }
        else // aSize == bSize == 1, trivial filter
        {
            x *= TOut( m_b[ 0 ] );
        }
    }

    template < class TCoeffs, class TIn >
    void
    LinFilt< TCoeffs, TIn >::apply( std::valarray< TOut >&      y,
                                    const std::valarray< TIn >& x )
    {
        if ( x.size( ) == 0 )
        {
            throw std::invalid_argument(
                "LinFilt::apply(): zero length input" );
        }

        valarray_copy( y, x );

        apply( y );
    }

    // Single precision coefficients
    template class LinFilt< float, float >;
    template class LinFilt< float, double >;
    template class LinFilt< float, complex< float > >;
    template class LinFilt< float, complex< double > >;

    template class LinFilt< complex< float >, float >;
    template class LinFilt< complex< float >, double >;
    template class LinFilt< complex< float >, complex< float > >;
    template class LinFilt< complex< float >, complex< double > >;

    // Double precision coefficients
    template class LinFilt< double, float >;
    template class LinFilt< double, double >;
    template class LinFilt< double, complex< float > >;
    template class LinFilt< double, complex< double > >;

    template class LinFilt< complex< double >, float >;
    template class LinFilt< complex< double >, double >;
    template class LinFilt< complex< double >, complex< float > >;
    template class LinFilt< complex< double >, complex< double > >;

} // namespace Filters
