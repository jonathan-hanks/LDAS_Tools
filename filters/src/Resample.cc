//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// $Id: Resample.cc,v 1.31 2006/10/13 19:32:40 emaros Exp $

#include <filters_config.h>

#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>

#include "ldastoolsal/toss.hh"

#include "filters/Resample.hh"
#include "filters/LinFilt.hh"
#include "filters/FIRLP.hh"
#include "filters/KaiserWindow.hh"
#include "filters/gcd.hh"
#include "filters/LDASConstants.hh"
#include "filters/valarray_utils.hh"

using namespace std;

using LDASTools::Error::toss;

namespace
{
    // Stolen from Stroustrup p. 531 for use in STL algorithms
    template < class T >
    T*
    delete_ptr( const T* const p )
    {
        delete p;
        return 0;
    }

    template < class T >
    T*
    duplicate_obj( const T* const p )
    {
        T* retval = new T( *p );
        return retval;
    }

    //
    // strided_copy - a utility for emulating strides with C-style arrays
    //
    // The valarray operation
    //
    //     out[slice(start0, size, stride0)] = in[slice(start1, size, stride1)]
    //
    // is equivalent to
    //
    //     strided_copy(&in[start1], size, stride1, &out[start0], stride0);
    //
    template < class In, class Out >
    void
    strided_copy( In           first,
                  size_t       size,
                  const size_t stride1,
                  Out          res,
                  const size_t stride0 )
    {
        while ( size > 0 )
        {
            --size;
            *res = *first;
            res += stride0;
            first += stride1;
        }
    }

} // namespace

namespace Filters
{

    ResampleBase::ResampleBase( const int    p,
                                const int    q,
                                const int    n,
                                const double beta )
    {
        // As of ldas-0.6.0, "resampling" by unity is allowed and is just
        // a pass-through

        if ( p < 1 )
        {
            toss< std::invalid_argument >(
                "ResampleBase", __FILE__, __LINE__, "p < 1" );
        }

        if ( q < 1 )
        {
            toss< std::invalid_argument >(
                "ResampleBase", __FILE__, __LINE__, "q < 1" );
        }

        if ( n < 1 )
        {
            toss< std::invalid_argument >(
                "ResampleBase", __FILE__, __LINE__, "n < 1" );
        }

        if ( beta < 0 )
        {
            toss< std::invalid_argument >(
                "ResampleBase", __FILE__, __LINE__, "beta < 0.0" );
        }

        // Normalise p and q
        const int divisor = Filters::gcd( p, q );
        m_p = p / divisor;
        m_q = q / divisor;

        m_n = n;
        m_beta = beta;

        if ( ( m_p == 1 ) && ( m_q == 1 ) )
        {
            m_nOrder = 1;
            m_delay = 0;
        }
        else // m_p != 1 or m_q != 1
        {
            // Even order has center tap
            m_nOrder = 2 * m_n * std::max( m_p, m_q );

            // Add compensating delay so that we hit the center tap on
            // downsample Requires that center tap is in m_lf[ ?*m_q + m_q-1 ]
            // (see Resample.apply())
            // Requires (ctap - m_q + 1) % q == 0
            // Add delay to filter so that this is true
            // Recall: m_nOrder = 2*n*max(p, q)
            const int ctap = m_nOrder / 2; // center tap index
            m_delay = ( (double)ctap ) / ( (double)m_q ); // total filter delay
        }
    }

    ResampleBase::ResampleBase( const int p, const int q )
    {
        if ( p < 1 )
        {
            toss< std::invalid_argument >(
                "ResampleBase", __FILE__, __LINE__, "p < 1" );
        }

        if ( q < 1 )
        {
            toss< std::invalid_argument >(
                "ResampleBase", __FILE__, __LINE__, "q < 1" );
        }

        // Normalise p and q
        const int divisor = Filters::gcd( p, q );
        m_p = p / divisor;
        m_q = q / divisor;

        m_n = 0;
        m_beta = 0.0;
        m_nOrder = 0;
    }

    ResampleBase::~ResampleBase( )
    {
    }

    bool
    ResampleBase::operator==( const ResampleBase& rhs )
    {
        return ( ( getN( ) == rhs.getN( ) ) &&
                 ( getNOrder( ) == rhs.getNOrder( ) ) &&
                 ( getBeta( ) == rhs.getBeta( ) ) &&
                 ( getDelay( ) == rhs.getDelay( ) ) );
    }

    int
    ResampleBase::getN( ) const
    {
        return m_n;
    }

    int
    ResampleBase::getNOrder( ) const
    {
        return m_nOrder;
    }

    double
    ResampleBase::getBeta( ) const
    {
        return m_beta;
    }

    double
    ResampleBase::getDelay( ) const
    {
        /// \note
        ///   might need to be changed for general resampling
        return m_delay;
    }

    //-------------------------------------------------------------------------

    template < class TIn >
    Resample< TIn >::Resample( const int    p,
                               const int    q,
                               const int    n,
                               const double beta )
        : ResampleBase( p, q, n, beta )
    {
        // Resizing the containers is deferred to here so we know p and q are
        // strictly positive
        m_orphan.resize( getP( ), 0 );

        // Set up the filter bank

        //
        // Calculate the pass-band edge for the filter designer.
        //
        // There are three possibilities:
        //
        // 1. Downsampling by 1/q. The procedure is to first filter out
        // everything above (1/q)*Nyquist and decimate, so fc = 1/q.
        //
        // 2. Upsampling by p/1. The procedure is to insert p-1 zeroes between
        // each point, then filter out everything above the original Nyquist
        // frequency. Since the new Nyquist' = p*Nyquist, and the filtering is
        // applied to the zero-filled data, fc = Nyquist/Nyquist' = 1/p.
        //
        // 3. General resampling p/q, (p != 1) && (q != 1) && (p != q).
        // The procedure is to upsample to Nyquist' = p*Nyquist as above,
        // filter out everything above the final Nyquist'' = (p/q)*Nyquist
        // and then decimate, so either fc = Nyquist''/Nyquist' = (p/q)/p = 1/q
        // or fc = Nyquist/Nyquist' = 1/p, whichever is the smaller cutoff
        // frequency.
        //
        // So, fc = 1/max(p, q)
        //
        const int    P = getP( );
        const int    Q = getQ( );
        const double fc = 1.0L / std::max( P, Q );

        valarray< double > b;

        if ( ( P == 1 ) && ( Q == 1 ) )
        {
            // Trivial filter, which won't be used anyway
            b.resize( 1 );
            b[ 0 ] = 1;
        }
        else // P != 1 or Q != 1
        {
            // Only need the filter designer for one set of coeffs,
            // so just use an anonymous instance
            FIRLP( fc, getNOrder( ), KaiserWindow( beta ) ).apply( b );
        }

        initFilters( b );
    }

    template < class TIn >
    Resample< TIn >::Resample( const int                      p,
                               const int                      q,
                               const std::valarray< double >& b )
        : ResampleBase( p, q )
    {
        const size_t bSize = b.size( );

        if ( bSize == 0 )
        {
            toss< std::invalid_argument >(
                "Resample",
                __FILE__,
                __LINE__,
                "zero length filter coefficient array" );
        }

        // Resizing the containers is deferred to here so we know p and q are
        // strictly positive
        m_orphan.resize( getP( ), 0 );

        // Set up the filter bank
        initFilters( b );
    }

    template < class TIn >
    Resample< TIn >::Resample( const Resample& rhs )
        : ResampleBase( rhs ), m_orphan( rhs.m_orphan ),
          m_lf( rhs.m_lf.size( ) )
    {
        // Note that m_lf is not copy-constructed, since it's a vector of
        // pointers - instead we need to make new copies of each element
#if WORKING
        transform( rhs.m_lf.begin( ),
                   rhs.m_lf.end( ),
                   m_lf.begin( ),
                   duplicate_obj< LinFilt< double, TOut > > );
#else /* WORKING */
        typename lf_type::iterator dest( m_lf.begin( ) );
        for ( typename lf_type::const_iterator cur = rhs.m_lf.begin( ),
                                               end = rhs.m_lf.end( );
              cur != end;
              ++cur, ++dest )
        {
            *dest = duplicate_obj( *cur );
        }
#endif /* WORKING */
    }

    template < class TIn >
    Resample< TIn >::~Resample( )
    {
        // Delete all the LinFilt objects
#if WORKING
        transform( m_lf.begin( ),
                   m_lf.end( ),
                   m_lf.begin( ),
                   delete_ptr< LinFilt< double, TOut > > );
#else /* WORKING */
        typename lf_type::iterator dest( m_lf.begin( ) );
        for ( typename lf_type::const_iterator cur = m_lf.begin( ),
                                               end = m_lf.end( );
              cur != end;
              ++cur, ++dest )
        {
            *dest = delete_ptr( *cur );
        }
#endif /* WORKING */
    }

    template < class TIn >
    void
    Resample< TIn >::initFilters( const std::valarray< double >& b )
    {
        if ( m_lf.size( ) != 0 )
        {
            toss< std::runtime_error >( "Resample::initFilters",
                                        __FILE__,
                                        __LINE__,
                                        "BUG: filters already initialised" );
        }

        const int p = getP( );
        const int q = getQ( );
        const int pq = p * q;

        //
        // (b.size() - 1) must be divisible by both p and q. Since p and q are
        // coprime, this is equivalent to (b.size() - 1) being divisible by p*q.
        // If not, we pad it out to the nearest multiple with zeroes.
        //

        valarray< double > bTmp( b );
        int                bsize = bTmp.size( );

        if ( ( bsize - 1 ) % pq != 0 )
        {
            bsize = ( ( bsize - 1 ) / pq + 1 ) * pq + 1;
            bTmp.resize( bsize, 0 );

            // copy the original b into bTmp, leaving the trailing points zero
            bTmp[ slice( 0, b.size( ), 1 ) ] = b;
        }
        // else, just use the original b. Don't need "else" statement
        // since bTmp was initialised from b

        m_lf.resize( pq );

        for ( int j = 0; j < p; ++j )
        {
            // totality of slices must cover b
            // skip through b with stride p: (bsize - j)/p elements
            // one more element if remainder non-zero
            const int bplen =
                ( bsize - j ) / p + ( ( ( bsize - j ) % p > 0 ) ? 1 : 0 );
            std::valarray< double > bp(
                static_cast< const std::valarray< double > >(
                    bTmp )[ slice( j, bplen, p ) ] );

            for ( int k = 0; k < q; ++k )
            {
                // totality of slices must cover bp
                // skip through bp with stride q: (bpsize - j)/q elements
                // one more element if remainder non-zero
                const int bpqlen =
                    ( bplen - k ) / q + ( ( ( bplen - k ) % q > 0 ) ? 1 : 0 );

                const int               offset = ( j * q ) + k;
                std::valarray< double > lfbp(
                    static_cast< const std::valarray< double > >(
                        bp )[ slice( k, bpqlen, q ) ] );

                m_lf[ offset ] = new LinFilt< double, TOut >( lfbp );
            }
        }
    }

    template < class TIn >
    const Resample< TIn >&
    Resample< TIn >::operator=( const Resample& rhs )
    {
        if ( &rhs != this )
        {
            // Trivial copies
            ResampleBase::operator=( rhs );

            // More complicated
            if ( m_orphan.size( ) != rhs.m_orphan.size( ) )
            {
                m_orphan.resize( rhs.m_orphan.size( ) );
            }

            if ( rhs.m_orphan.size( ) != 0 )
            {
                m_orphan = rhs.m_orphan;
            }

            // Even more complicated
            //
            // Need to delete all the existing LinFilts first
#if WORKING
            transform( m_lf.begin( ),
                       m_lf.end( ),
                       m_lf.begin( ),
                       delete_ptr< LinFilt< double, TOut > > );
#else /* WORKING */
            typename lf_type::iterator dest( m_lf.begin( ) );
            for ( typename lf_type::const_iterator cur = m_lf.begin( ),
                                                   end = m_lf.end( );
                  cur != end;
                  ++cur, ++dest )
            {
                *dest = delete_ptr( *cur );
            }
#endif /* WORKING */

            // Then resize and copy over the new ones
            m_lf.resize( rhs.m_lf.size( ) );
#if WORKING
            transform( rhs.m_lf.begin( ),
                       rhs.m_lf.end( ),
                       m_lf.begin( ),
                       duplicate_obj< LinFilt< double, TOut > > );
#else /* WORKING */
            dest = m_lf.begin( );
            for ( typename lf_type::const_iterator cur = rhs.m_lf.begin( ),
                                                   end = rhs.m_lf.end( );
                  cur != end;
                  ++cur, ++dest )
            {
                *dest = duplicate_obj( *cur );
            }
#endif /* WORKING */
        }

        return *this;
    }

    template < class TIn >
    bool
    Resample< TIn >::operator==( const Resample& rhs )
    {
        if ( this == &rhs )
        {
            return true;
        }

        bool retval = ( ( this->ResampleBase::operator==( rhs ) ) &&
                        ( m_lf.size( ) == rhs.m_lf.size( ) ) );

        if ( retval &&
             ( retval = ( m_orphan.size( ) == rhs.m_orphan.size( ) ) ) )
        {
            for ( unsigned int x = 0, end = m_orphan.size( );
                  retval && ( x != end );
                  ++x )
            {
                retval = ( m_orphan[ x ] == rhs.m_orphan[ x ] );
            }
        }
        if ( retval )
        {
            for ( typename lf_type::const_iterator lhs_cur = m_lf.begin( ),
                                                   rhs_cur = rhs.m_lf.begin( ),
                                                   rhs_end = rhs.m_lf.end( );
                  retval && ( rhs_cur != rhs_end );
                  ++lhs_cur, ++rhs_cur )
            {
                retval = ( *( *lhs_cur ) == *( *rhs_cur ) );
            }
        }
        return retval;
    }

    template < class TIn >
    Resample< TIn >*
    Resample< TIn >::Clone( ) const
    {
        return new /*Resample<TIn>::*/ Resample( *this );
    }

    template < class TIn >
    LinFilt< double, typename ResampleTraits< TIn >::OutType >&
    Resample< TIn >::getFilter( const int j, const int k ) const
    {
        return *( m_lf[ j * getQ( ) + k ] );
    }

    template < class TIn >
    void
    Resample< TIn >::reset( )
    {
        // Use the for_each algorithm to reset all member filters
        for_each( m_lf.begin( ), m_lf.end( ), mem_fn( &LinFiltBase::reset ) );

        // Reset the orphan
        m_orphan = TOut( 0 );
    }

    template < class TIn >
    void
    Resample< TIn >::apply( TOut* const      out,
                            const TIn* const in,
                            const size_t     inlen )
    {
        // This implementation makes a choice about what points to throw out
        // when downsampling. That choice is different than made in matlab
        // upfirdn().

        const int p = getP( );
        const int q = getQ( );

        // input length must be multiple of q
        if ( inlen == 0 )
        {
            toss< std::invalid_argument >(
                "Resample", __FILE__, __LINE__, "zero length input" );
        }

        // input length must be multiple of q
        if ( inlen % q != 0 )
        {
            toss< std::invalid_argument >(
                "Resample",
                __FILE__,
                __LINE__,
                "Input length must be a multiple of q" );
        }

        // output length is (p/q) * input length
        const size_t outlen = p * ( inlen / q );

        //
        // Since we accumulate values into "out" by successive additions, we
        // need to make sure that "out" is zero-filled initially.
        //
        fill( out, out + outlen, TOut( 0 ) );

        if ( ( p == 1 ) && ( q == 1 ) )
        {
            // Trivial case, pass through
            copy( in, in + inlen, out );
        }
        else if ( ( p == 1 ) && ( q > 1 ) )
        {
            // Downsampling - put this first as the most common case

            valarray< TOut > x_j( outlen );

            // special case for j = 0
            // x_j = in[slice(0, outlen, q)];
            strided_copy( in, outlen, q, &x_j[ 0 ], 1 );
            getFilter( 0, 0 ).apply( x_j );

            // out += x_j;
            transform( out, out + outlen, &x_j[ 0 ], out, plus< TOut >( ) );

            // Now add the old partial sum
            out[ 0 ] += m_orphan[ 0 ];

            // At this point we are done with the old values in the orphan,
            // reset it for the new loop (because we're accumulating)
            // To force (0,0) to be created for complex<> types
            TOut zero( 0 );
            m_orphan = zero;

            for ( int j = 1; j < q; ++j )
            {
                // x_j = in[slice(q - j, outlen, q)];
                strided_copy( in + q - j, outlen, q, &x_j[ 0 ], 1 );
                getFilter( 0, j ).apply( x_j );

                // out[slice(1, outlen - 1, 1)] += x_j[slice(0, outlen - 1, 1)];
                transform( out + 1,
                           out + outlen,
                           &x_j[ 0 ],
                           out + 1,
                           plus< TOut >( ) );
                m_orphan[ 0 ] += x_j[ outlen - 1 ];
            }

            // NOTE - don't need to multiply by p as p == 1
        }
        else if ( ( p > 1 ) && ( q == 1 ) )
        {
            // Upsampling

            valarray< TOut > temp( inlen );
            for ( int l = 0; l < p; ++l )
            {
                copy( in, in + inlen, &temp[ 0 ] );
                getFilter( l, 0 ).apply( temp );

                // out[slice(l, outlen/p, p)] = temp;
                strided_copy( &temp[ 0 ], temp.size( ), 1, out + l, p );
            }

            // out *= p;  // Correction factor for all cases where p != 1
            // :WARN: The transform function written below produces errors on
            // :WARN:   opteron boxes.
            // transform( out, out + outlen, &out[0],
            // bind2nd(multiplies<TOut>(), TOut(p))); :TRICKY: Use simple loop
            // to allow the code to work on all known :TRICKY:   platforms
            for ( size_t x = 0; x < outlen; ++x )
            {
                out[ x ] *= TOut( p );
            }
        }
        else // p > 1 && q > 1
        {
            // General resampling

            // :TODO: Get rid of this temporary
            valarray< TOut > tout( 0.0, outlen + p );

            for ( int l = 0; l < p; ++l )
            {
                valarray< TOut > y_l( 0.0, outlen / p + 1 );
                const int        r = ( q * l ) % p;
                const int        s = ( q * l - r ) / p;

                for ( int j = 0; j < q; ++j )
                {
                    valarray< TOut > x_lj( 0.0, outlen / p );

                    if ( s >= j )
                    {
                        // x_lj = in[slice(s-j, outlen/p, q)];
                        strided_copy(
                            in + s - j, x_lj.size( ), q, &x_lj[ 0 ], 1 );
                        getFilter( r, j ).apply( x_lj );
                        y_l[ slice( 0, outlen / p, 1 ) ] += x_lj;
                    }
                    else
                    {
                        // x_lj = in[slice(q + s - j, outlen/p, q)];
                        strided_copy(
                            in + q + s - j, x_lj.size( ), q, &x_lj[ 0 ], 1 );
                        getFilter( r, j ).apply( x_lj );
                        y_l[ slice( 1, outlen / p, 1 ) ] += x_lj;
                    }
                }

                tout[ slice( l, outlen / p + 1, p ) ] = y_l;
            }

            tout[ slice( 0, p, 1 ) ] += m_orphan;
            valarray_copy_slice( slice( outlen, p, 1 ), tout, m_orphan );

            // out = tout[slice(0, outlen, 1)];
            // out *= p;  // Correction factor for all cases where p != 1
            // :WARN: The transform function written below produces errors on
            // :WARN:   opteron boxes.
            // transform(&tout[0], &tout[outlen], out,
            // bind2nd(multiplies<TOut>(),p)); :TRICKY: Use simple loop to allow
            // the code to work on all known :TRICKY:   platforms
            for ( size_t x = 0; x < outlen; ++x )
            {
                out[ x ] = tout[ x ] * TOut( p );
            }
        }
    }

    template < class TIn >
    void
    Resample< TIn >::apply( std::valarray< TOut >&      out,
                            const std::valarray< TIn >& in )
    {
        const int    inlen = in.size( );
        const size_t outlen = getP( ) * ( inlen / getQ( ) );

        out.resize( outlen, 0 );

        // Get the address of the first element - ugly but necessary
        const TIn* const pIn =
            &( const_cast< valarray< TIn >& >( in ).operator[]( 0 ) );

        apply( &out[ 0 ], pIn, inlen );
    }

    template < class TIn >
    void
    Resample< TIn >::getB( std::valarray< double >& b ) const
    {
        // must reconstruct b from m_lf - not a trivial task

        const int p = getP( );
        const int q = getQ( );

        // Get the total size for b we need
        size_t bSize = 0;
        for ( int j = 0; j < p; ++j )
        {
            for ( int k = 0; k < q; ++k )
            {
                bSize += getFilter( j, k ).getBSize( );
            }
        }

        // Fill in b

        // Temporary storage for coeffs of each sub-filter
        std::valarray< double > filter;

        b.resize( bSize, 0 );
        for ( int j = 0; j < p; ++j )
        {
            for ( int k = 0; k < q; ++k )
            {
                getFilter( j, k ).getB( filter );
                for ( unsigned int i = 0; i < filter.size( ); ++i )
                {
                    b[ j + p * k + p * q * i ] = filter[ i ];
                }
            }
        }
    }

    // Instantiation requests - add more as needed

    template class Resample< short int >;
    template class Resample< int >;

    template class Resample< float >;
    template class Resample< double >;

    template class Resample< complex< float > >;
    template class Resample< complex< double > >;

} // namespace Filters
