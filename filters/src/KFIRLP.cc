//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

//! author: "Eric Rotthoff/L. S. Finn"

#include <filters_config.h>

extern "C" {
#include <math.h>
}

#include "filters/KFIRLP.hh"
#include "filters/Sinc.hh"

namespace Filters
{

    KFIRLP::KFIRLP( const double& t_fc,
                    const double& t_alpha,
                    const double& t_df )
    {
        setFc( t_fc );
        setDf( t_df );
        setAlpha( t_alpha );

        kwin.set_beta( getBeta( ) );
    }

    template < class T >
    void
    KFIRLP::apply( std::valarray< T >& b )
    {
        const int norder = getOrder( );
        b.resize( norder + 1 );

        const double dn = norder;
        for ( int i = 0; i <= norder; ++i )
        {
            b[ i ] = Sinc( fc * ( i - dn / 2 ) );
        }

        kwin.set_beta( getBeta( ) );

        // Window apply resizes tmp if necessary
        kwin.apply( b );

        // Normalise so DC is passed unchanged
        b /= b.sum( );
    }

    void
    KFIRLP::setFc( double t_fc )
    {
        if ( ( t_fc <= 0 ) || ( t_fc >= 1 ) )
        {
            throw std::domain_error( "KFIRLP:setFc() - "
                                     "transition band out-of-range" );
        }

        fc = t_fc;
    }

    void
    KFIRLP::setAlpha( double t_alpha )
    {
        if ( t_alpha <= 0 )
        {
            throw std::domain_error( "KFIRLP::setAlpha() - "
                                     "stop band depth <= 0" );
        }

        alpha = t_alpha;
    }

    void
    KFIRLP::setDf( double t_df )
    {
        if ( !( fabs( 2.0 * t_df - 1.0 ) < 1.0 ) )
        {
            throw std::domain_error( "KFIRLP::setDf() - "
                                     "Transition bandwidth out-of-range" );
        }

        df = t_df;
    }

    int
    KFIRLP::getOrder( ) const
    {
        double n = std::max( ( alpha - 7.95 ) / ( 2.285 * LDAS_PI * df ), 1.0 );
        n = ceil( n );

        return int( n );
    }

    double
    KFIRLP::getBeta( ) const
    {
        double rtn = 0;
        if ( alpha > 50 )
        {
            rtn = 0.1102 * ( alpha - 8.7 );
        }
        else if ( alpha > 21 )
        {
            rtn = 0.5842 * pow( alpha - 21.0, 0.4 ) + 0.07886 * ( alpha - 21 );
        }
        else
        {
            rtn = 0.0;
        }

        return rtn;
    }

    /** \cond ignore */
    template void KFIRLP::apply( std::valarray< double >& );

    template void KFIRLP::apply( std::valarray< std::complex< double > >& );
    /** \endcond ignore */

} // namespace Filters
