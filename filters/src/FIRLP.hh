//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FIRLP_HH
#define FIRLP_HH

#include <memory>

#include "filters/HannWindow.hh"

namespace Filters
{

    class FIRLP
    {

    public:
        // Constructors

        /// \brief "Provide everything" constructor
        ///
        /// \param[in] fc
        ///   pass-band edge (Nyquist frequency units)
        /// \param[in] order
        ///   filter order
        /// \param[in] win
        ///   window for filter design
        /// \exception invalid_argument
        ///   if fc <= 0 or fc >= 1
        /// \exception invalid_argument
        ///   if t_order <= 0
        FIRLP( const double  fc = 0.5,
               const int     order = 10,
               const Window& win = HannWindow( ) );

        /// \brief Copy constructor
        ///
        /// \param[in] firlp
        ///   FIRLP to duplicate
        FIRLP( const FIRLP& firlp );

        /// \brief Destructor
        ~FIRLP( );

        /// \brief Assignment operator
        //
        /// \param[in] rhs
        ///   instance to be assigned
        const FIRLP& operator=( const FIRLP& rhs );

        // Accessors

        /// \brief Return pass-band edge
        ///
        /// \return
        ///   pass band edge as fraction of Nyquist frequency
        double getFc( ) const;

        /// \brief Return filter order
        ///
        /// \return
        ///   filter order (length - 1)
        int getOrder( ) const;

        // Mutators

        /// \brief Set order
        ///
        /// param[in]
        ///   new filter order
        /// \exception
        ///   if order <= 0
        void setOrder( const int order );

        /// \brief Set pass-band edge
        //
        /// \param[in]
        ///   pass-band edge as fraction of Nyquist frequncy
        /// \exception
        ///   if fc <= 0 or fc >= 1
        void setFc( const double fc );

        /// \brief Design filter
        ///
        /// \param[in,out]
        ///   transfer function coefficients
        template < class T >
        void apply( std::valarray< T >& tmp ) const;

    private:
        size_t m_length; ///< length of window (order + 1)
        double m_fc; ///< ratio of cutoff freq to Nyquist freq

        std::unique_ptr< Window > m_win; // window
    };

    inline double
    FIRLP::getFc( ) const
    {
        return m_fc;
    }

    inline int
    FIRLP::getOrder( ) const
    {
        return ( m_length - 1 );
    }

} // namespace Filters

#endif // FIRLP_HH
