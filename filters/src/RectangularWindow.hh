//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef RECTANGULARWINDOW_HH
#define RECTANGULARWINDOW_HH

#include "filters/Window.hh"

namespace Filters
{

    //
    /// \brief A class representing a rectangular window
    //
    class RectangularWindow : public Window
    {

    public:
        //
        /// \brief Return window name
        ///
        /// \return
        ///   window name
        //
        virtual std::string name( ) const;

        //
        /// \brief Return window parameter
        ///
        /// \return
        ///   window parameter
        //
        virtual double param( ) const;

        //
        /// \brief Clone a window
        ///
        /// \return
        ///   current window
        //
        virtual RectangularWindow* Clone( ) const;

    private:
        //
        /// \brief Get the ith element of the window
        ///
        /// A rectangular window is defined by
        ///
        ///     w[i] = 1.0
        ///
        /// for i = 0,1, ... n-1.
        ///
        /// \param[in] i
        ///   element at which the window is evaluated
        ///
        /// \return
        ///   the window element w[i]
        //
        virtual double element( const size_t i ) const;
    };

} // namespace Filters

#endif // RECTANGULARWINDOW_HH
