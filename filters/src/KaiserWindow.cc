//
// LDASTools filtes - A library implementing filtering algorithms
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools filtes is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools filtes is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <filters_config.h>

#include <stdexcept>

#include "filters/KaiserWindow.hh"

namespace Filters
{

    KaiserWindow::KaiserWindow( const double beta )
    {
        set_beta( beta );
    }

    void
    KaiserWindow::set_beta( const double& beta )
    {
        if ( beta < 0 )
        {
            throw std::invalid_argument(
                "invalid Kaiser window beta parameter" );
        }

        // If beta changes we need to re-populate ourself
        m_beta = beta;
        m_I0beta = I0( m_beta );
        populate( );
    }

    std::string
    KaiserWindow::name( ) const
    {
        return std::string( "Kaiser Window" );
    }

    double
    KaiserWindow::param( ) const
    {
        return m_beta;
    }

    double
    KaiserWindow::element( const size_t i ) const
    {
        const size_t n = size( );

        if ( n == 1 )
        {
            return 1.0;
        }
        else
        {
            double x = ( i - ( n - 1 ) / 2.0 ) * 2.0 / ( n - 1 );
            return I0( m_beta * std::sqrt( 1 - x * x ) ) / m_I0beta;
        }
    }

    //-------------------------------------------------------------------------
    /// \brief Calculate I0(x)
    ///
    /// Calculates the value of the 0th order, modified Bessel function of the
    /// first kind using a power series expansion. (See "Handbook of Math.
    /// Functions," eds. Abramowitz and Stegun, 9.6.12 for details.)
    ///
    /// \note
    ///   the accuracy of the expansion is chosen to be 2e-9
    ///
    /// \param[in] x
    ///
    /// \return
    ///   I0(x)
    //
    double
    KaiserWindow::I0( const double& x ) const
    {
        double ds( 1.0 );
        double d( 0.0 );
        double s( 1.0 );

        do
        {
            d += 2.0;
            ds *= x * x / ( d * d );
            s += ds;
        } while ( ds > 2e-9 );

        return s;
    }

    KaiserWindow*
    KaiserWindow::Clone( ) const
    {
        return new KaiserWindow( *this );
    }

} // namespace Filters
