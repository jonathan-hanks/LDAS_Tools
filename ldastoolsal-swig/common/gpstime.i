#if ! defined(GENERAL__GPSTIME_I)
#define GENERAL__GPSTIME_I

%{
#include <sstream>
%}

class GPSTime
{
 public:

  GPSTime( );

  GPSTime( const INT_4U Seconds,
	   const INT_4U NanoSeconds );

  GPSTime( const GPSTime& Source );
};

#if defined(SWIGPYTHON)

%extend GPSTime {
  char * __str__() {
    static char tmp[ 1024 ];

    std::ostringstream	msg;
    msg << *($self);

    ::strcpy( tmp, msg.str( ).c_str( ) );
    
    return tmp;
  }
};

#endif /* defined(SWIGPYTHON) */

#endif /* ! defined(GENERAL__GPSTIME_I) */
