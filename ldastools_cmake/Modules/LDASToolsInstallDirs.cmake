# -*- conding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
include(GNUInstallDirs)

if ( NOT LDASTOOLS_INSTALL_DATADIR)
    set(LDASTOOLS_PROJECT_NAME "ldas-tools")
    set(LDASTOOLS_INSTALL_DATADIR "" CACHE PATH "package specific read-only architecture-independent data (DATADIR/PROJECT_NAME)")
    set(LDASTOOLS_INSTALL_DATADIR "${CMAKE_INSTALL_DATADIR}/${LDASTOOLS_PROJECT_NAME}")
endif ( NOT LDASTOOLS_INSTALL_DATADIR)

mark_as_advanced(
    LDASTOOLS_INSTALL_DATADIR
    )
