#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------

#========================================================================
# Define some functions that everyone will use
#------------------------------------------------------------------------
function(cond_set_policy POLICY_ID STATE)
  if ( POLICY ${POLICY_ID} )
    cmake_policy( SET ${POLICY_ID} ${STATE} )
  endif ( POLICY ${POLICY_ID} )
endfunction(cond_set_policy)

#========================================================================
# Basic manipulation of the search path
#------------------------------------------------------------------------

#========================================================================
# Preload common packages
#------------------------------------------------------------------------

include( CTest )
include( GNUInstallDirs )
include( LDASToolsInstallDirs )

include( cx_check_compile_flag )
include( cx_check_compile_hardening )

cx_reconfigure()

enable_testing()
