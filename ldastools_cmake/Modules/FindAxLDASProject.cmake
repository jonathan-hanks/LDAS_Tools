#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( Autotools/Internal/ci_set_policy )

ci_set_policy( CMP0048 NEW )

include( CMakeParseArguments )

function( cx_ldas_project )
  set(options)
  set(oneValueArgs
    PROJECT
    VERSION_MAJOR
    VERSION_MINOR
    VERSION_PATCH
    VERSION_TWEAK )
  set(multiValueArgs
    LANGUAGES )

  cmake_parse_arguments(
    ARG
      "${options}"
      "${oneValueArgs}"
      "${multiValueArgs}"
      ${ARGN}
      )

  #======================================================================
  # Project setup
  #----------------------------------------------------------------------

  PROJECT(${ARG_PROJECT} ${ARG_LANGUAGES})

  #======================================================================
  # Project version number handling
  #----------------------------------------------------------------------

  #----------------------------------------------------------------------
  # Supply default values for variables that must have a value
  #----------------------------------------------------------------------

  if (NOT ARG_VERSION_MAJOR )
    set( ARG_VERSION_MAJOR 0 )
  endif ( NOT ARG_VERSION_MAJOR )
  if (NOT ARG_VERSION_MINOR )
    set( ARG_VERSION_MINOR 0 )
  endif ( NOT ARG_VERSION_MINOR )
  if (NOT ARG_VERSION_PATCH )
    set( ARG_VERSION_PATCH 0 )
  endif ( NOT ARG_VERSION_PATCH )

  #----------------------------------------------------------------------
  # Define cmake variables locally
  #----------------------------------------------------------------------

  set( PROJECT_VERSION_MAJOR
    ${ARG_VERSION_MAJOR} )
  set( PROJECT_VERSION_MINOR
    ${ARG_VERSION_MINOR} )
  set( PROJECT_VERSION_PATCH
    ${ARG_VERSION_PATCH} )
  set( PROJECT_VERSION
    "${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}" )
  if( ARG_VERSION_TWEAK )
    set( PROJECT_VERSION_TWEAK
      ${ARG_VERSION_TWEAK} )
    set( PROJECT_VERSION
      "${PROJECT_VERSION}.${PROJECT_VERSION_TWEAK}" )
  endif( ARG_VERSION_TWEAK )

  #----------------------------------------------------------------------
  # Export to parent scope
  #----------------------------------------------------------------------

  set( PROJECT_VERSION_MAJOR
    ${PROJECT_VERSION_MAJOR}
    PARENT_SCOPE )
  set( PROJECT_VERSION_MINOR
    ${PROJECT_VERSION_MINOR}
    PARENT_SCOPE )
  set( PROJECT_VERSION_PATCH
    ${PROJECT_VERSION_PATCH}
    PARENT_SCOPE )
  if ( PROJECT_VERSION_TWEAK )
    set( PROJECT_VERSION_TWEAK
      ${PROJECT_VERSION_TWEAK}
      PARENT_SCOPE )
  endif ( PROJECT_VERSION_TWEAK )
  set( PROJECT_VERSION
    ${PROJECT_VERSION}
    PARENT_SCOPE )

  set( PROJECT_NAME
    ${PROJECT_NAME}
    PARENT_SCOPE )
  set( PROJECT_${PROJECT_NAME}_VERSION_MAJOR
    ${PROJECT_VERSION_MAJOR}
    PARENT_SCOPE )
  set( PROJECT_${PROJECT_NAME}_VERSION_MINOR
    ${PROJECT_VERSION_MINOR}
    PARENT_SCOPE )
  set( PROJECT_${PROJECT_NAME}_VERSION_PATCH
    ${PROJECT_VERSION_PATCH}
    PARENT_SCOPE )
  if ( PROJECT_VERSION_TWEAK )
    set( PROJECT_${PROJECT_NAME}_VERSION_TWEAK
      ${PROJECT_VERSION_TWEAK}
      PARENT_SCOPE )
  endif ( PROJECT_VERSION_TWEAK )
  set( PROJECT_${PROJECT_NAME}_VERSION
    ${PROJECT_VERSION}
    PARENT_SCOPE )

endfunction( cx_ldas_project )
