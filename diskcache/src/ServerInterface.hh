//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__SERVER_INTERFACE_HH
#define DISKCACHE_API__SERVER_INTERFACE_HH

#include "diskcacheAPI/MetaCommands.hh"

namespace diskCache
{
    //=====================================================================
    /// \brief Simple interface to a standalone diskcache server
    ///
    /// This class provides a simple interface to the diskcache server.
    //=====================================================================
    class ServerInterface
        : public MetaCommand::ClientServerInterface::ServerInfo
    {
    public:
        typedef MetaCommand::ClientServerInterface ::ServerInfo::port_type
                                           port_type;
        typedef INT_4U                     time_type;
        typedef std::vector< std::string > filenames_rds_results_type;

        static std::string DEFAULT_EXTENSION;

        //-------------------------------------------------------------------
        /// \brief Qury for a set of files that is appropriate for RDS
        /// generation
        ///
        /// \param[out] Results
        ///     Container of file names that match the query
        /// \param[in] IFO
        ///     Interferometer
        /// \param[in] Desc
        ///     Descriptive field
        /// \param[in] StartTime
        ///     Start time of request in GPS seconds.
        /// \param[in] EndTime
        ///     End time of request in GPS seconds.
        /// \param[in] Resampling
        ///     True if the list of channels needs to support resampling
        /// \param[in] Extension
        ///     The extension, including the period, of the type of file.
        ///
        /// \todo Need to return any remaining gap information
        //-------------------------------------------------------------------
        void FilenamesRDS( filenames_rds_results_type& Results,
                           const std::string&          IFO,
                           const std::string&          Desc,
                           time_type                   StartTime,
                           time_type                   EndTime,
                           bool                        Resampling,
                           const std::string& Extension = DEFAULT_EXTENSION );
        //-------------------------------------------------------------------
        /// \brief Configure the host and port fo the diskcache server
        //-------------------------------------------------------------------
        void Server( const std::string& Host, port_type Port );
    };

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline void
    ServerInterface::Server( const std::string& HostnameValue,
                             port_type          PortValue )
    {
        Port( PortValue );
        Hostname( HostnameValue );
    }
} // namespace diskCache
#endif /* DISKCACHE_API__SERVER_INTERFACE_HH */
