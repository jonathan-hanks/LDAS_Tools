//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <atomic>
#include <string>

#include "genericAPI/Logging.hh"
#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Stat.hh"

#include "diskcacheAPI/Common/Variables.hh"

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"

#include "diskcacheAPI/Cache/HotDirectory.hh"
#include "diskcacheAPI/Cache/SDGTxStreamASCII.hh"
#include "diskcacheAPI/Cache/SDGTxStreamBinary.hh"

#include "diskcache.hh"
#include "diskcachecmd.hh"
#include "diskcacheAPI.hh"
#include "MetaCommands.hh"
#include "MountPointScanner.hh"
#include "ScanMountPointsDaemon.hh"
#include "Variables.hh"

static void variables_init( );

using LDASTools::AL::MutexLock;

namespace
{
    template < typename FunctionType >
    char const*
    DefaultValue( FunctionType function )
    {
        static volatile std::atomic_bool initialized{ false };

        static std::ostringstream stream_default;

        if ( !initialized )
        {
            stream_default << function( );
            initialized = true;
        }
        return ( stream_default.str( ).c_str( ) );
    }

    template < typename FunctionType >
    void
    DefaultValue( FunctionType function, std::string& Default )
    {
        static volatile std::atomic_bool initialized{ false };

        if ( !initialized )
        {
            std::ostringstream stream_default;

            stream_default << function( );
            Default = stream_default.str( );
            initialized = true;
        }
    }

    //-------------------------------------------------------------------
    // CACHE_WRITE_DELAY
    //-------------------------------------------------------------------
    struct cache_write_delay_secs
    {
        static constexpr const char* VARIABLE_NAME{
            diskCache::Variables::VAR_NAME_CACHE_WRITE_DELAY_SECS
        };
        typedef decltype( diskCache::DumpCacheDaemon::Interval( ) ) value_type;

        static std::string const&
        DEFAULT( )
        {
            static std::string string_default;
            DefaultValue( static_cast< value_type ( * )( ) >( reader ),
                          string_default );
            return ( string_default );
        }

        static value_type
        reader( )
        {
            return ( diskCache::DumpCacheDaemon::Interval( ) / 1000 );
        }

        static void
        reader( std::ostringstream& Value )
        {
            Value << ( diskCache::DumpCacheDaemon::Interval( ) / 1000 );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = ( diskCache::DumpCacheDaemon::Interval( ) / 1000 );
        }

        static void
        writer( const std::string& Value )
        {
            INT_4U cache_write_delay;

            std::istringstream value_istream( Value );

            if ( value_istream >> cache_write_delay )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: "
                                                 << cache_write_delay,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                diskCache::DumpCacheDaemon::Interval( cache_write_delay *
                                                      1000 );
            }
        }
    };

    //-------------------------------------------------------------------
    // CONCURRENCY
    //-------------------------------------------------------------------
    struct concurrency
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_CONCURRENCY;
        static constexpr const char* DEFAULT = "1";

        static void
        reader( std::ostringstream& Value )
        {
            Value << diskCache::MountPointScanner::Concurrency( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = diskCache::MountPointScanner::Concurrency( );
        }

        static void
        writer( const std::string& Value )
        {
            INT_4U v;

            std::istringstream s( Value );

            if ( s >> v )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                ScanConcurrency( v );
            }
        }
    };

    //-------------------------------------------------------------------
    // DIRECTORY_TIMEOUT
    //-------------------------------------------------------------------
    struct directory_timeout
    {
        typedef decltype( LDASTools::AL::Directory::Timeout( ) ) value_type;
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_DIRECTORY_TIMEOUT;

        static std::string const&
        DEFAULT( )
        {
            static std::string string_default;
            DefaultValue( static_cast< value_type ( * )( ) >( reader ),
                          string_default );
            return ( string_default );
        }

        static value_type
        reader( )
        {
            return ( LDASTools::AL::Directory::Timeout( ) );
        }

        static void
        reader( std::ostringstream& Value )
        {
            Value << reader( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = LDASTools::AL::Directory::Timeout( );
        }

        static void
        writer( const std::string& Value )
        {
            INT_4U v = 0;

            std::istringstream s( Value );

            if ( ( s >> v ) && ( v > 0 ) )
                {
                    QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                       MT_NOTE,
                                       0,
                                       "setup_variables",
                                       "CXX" );
                    LDASTools::AL::Directory::Timeout( v );
                }
        }
    };

    //-------------------------------------------------------------------
    // HOT_DIRECTORY_AGE
    //-------------------------------------------------------------------
    struct hot_directory_age
    {
        typedef decltype( diskCache::Cache::HotDirectory::HotLowerBound( ) )
            value_type;
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_HOT_DIRECTORY_AGE;

        static std::string const&
        DEFAULT( )
        {
            static std::string string_default;
            DefaultValue( static_cast< value_type ( * )( ) >( reader ),
                          string_default );
            return ( string_default );
        }

        static value_type
        reader( )
        {
            return ( diskCache::Cache::HotDirectory::HotLowerBound( ) );
        }

        static void
        reader( std::ostringstream& Value )
        {
            Value << reader( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = diskCache::Cache::HotDirectory::HotLowerBound( );
        }

        static void
        writer( const std::string& Value )
        {
            std::istringstream                             vs( Value );
            diskCache::Cache::HotDirectory::timestamp_type v;

            if ( vs >> v )
            {

                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                diskCache::Cache::HotDirectory::HotLowerBound( v );
            }
        }
    };

    //-------------------------------------------------------------------
    // HOT_DIRECTORY_SCAN_INTERVAL
    //-------------------------------------------------------------------
    struct hot_directory_scan_interval
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_HOT_DIRECTORY_SCAN_INTERVAL;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            using diskCache::Cache::HotDirectory;

            Value << HotDirectory::ScanInterval( );
        }

        static void
        reader_any( boost::any& Value )
        {
            using diskCache::Cache::HotDirectory;

            Value = HotDirectory::ScanInterval( );
        }

        static void
        writer( const std::string& Value )
        {
            using diskCache::Cache::HotDirectory;

            std::istringstream           vs( Value );
            HotDirectory::timestamp_type v;

            if ( vs >> v )
            {

                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                HotDirectory::ScanInterval( v );
            }
        }
    };

    //-------------------------------------------------------------------
    // LOG
    //-------------------------------------------------------------------
    struct log
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_LOG;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << GenericAPI::LDASplatform::AppName( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = GenericAPI::LDASplatform::AppName( );
        }

        static void
        writer( const std::string& Value )
        {
            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            GenericAPI::LDASplatform::AppName( Value );
        }
    };

    //-------------------------------------------------------------------
    // LOG_ARCHIVE_DIRECTORY
    //-------------------------------------------------------------------
    struct log_archive_directory
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_LOG_ARCHIVE_DIRECTORY;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << GenericAPI::LoggingInfo::ArchiveDirectory( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = GenericAPI::LoggingInfo::ArchiveDirectory( );
        }

        static void
        writer( const std::string& Value )
        {
            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            GenericAPI::LoggingInfo::ArchiveDirectory( Value );
        }
    };

    //-------------------------------------------------------------------
    // LOG_DEBUG_LEVEL
    //-------------------------------------------------------------------
    struct log_debug_level
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_LOG_DEBUG_LEVEL;
        static constexpr const char* DEFAULT = "0";

        static void
        reader( std::ostringstream& Value )
        {
            Value << GenericAPI::LoggingInfo::DebugLevel( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = GenericAPI::LoggingInfo::DebugLevel( );
        }

        static void
        writer( const std::string& Value )
        {
            int                debug_level;
            std::istringstream dbg_level_stream( Value );

            if ( dbg_level_stream >> debug_level )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << debug_level,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                GenericAPI::LoggingInfo::DebugLevel( debug_level );
            }
        }
    };

    //-------------------------------------------------------------------
    // LOG_DIRECTORY
    //-------------------------------------------------------------------
    struct log_directory
    {
        typedef decltype( GenericAPI::LoggingInfo::LogDirectory( ) ) value_type;
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_LOG_DIRECTORY;

        static std::string const&
        DEFAULT( )
        {
            static std::string string_default;
            DefaultValue( static_cast< value_type ( * )( ) >( reader ),
                          string_default );
            return ( string_default );
        }

        static value_type
        reader( )
        {
            return ( GenericAPI::LoggingInfo::LogDirectory( ) );
        }

        static void
        reader( std::ostringstream& Value )
        {
            Value << reader( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = GenericAPI::LoggingInfo::LogDirectory( );
        }

        static void
        writer( const std::string& Value )
        {
            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            GenericAPI::LoggingInfo::LogDirectory( Value );
        }
    };

    //-------------------------------------------------------------------
    // LOG_FORMAT
    //-------------------------------------------------------------------
    struct log_format
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_LOG_FORMAT;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << GenericAPI::LoggingInfo::Format( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = GenericAPI::LoggingInfo::Format( );
        }

        static void
        writer( const std::string& Value )
        {
            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            GenericAPI::LoggingInfo::Format( Value );
        }
    };

    //-------------------------------------------------------------------
    // LOG_ROTATE_ENTRY_COUNT
    //-------------------------------------------------------------------
    struct log_rotate_entry_count
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_LOG_ROTATE_ENTRY_COUNT;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << GenericAPI::LoggingInfo::RotationCount( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = GenericAPI::LoggingInfo::RotationCount( );
        }

        static void
        writer( const std::string& Value )
        {
            int                v;
            std::istringstream s( Value );

            if ( s >> v )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                GenericAPI::LoggingInfo::RotationCount( v );
            }
        }
    };

    //-------------------------------------------------------------------
    // OUTPUT_ASCII
    //-------------------------------------------------------------------
    struct output_ascii
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_OUTPUT_ASCII;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << diskCache::DumpCacheDaemon::FilenameAscii( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = diskCache::DumpCacheDaemon::FilenameAscii( );
        }

        static void
        writer( const std::string& Value )
        {
            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            diskCache::DumpCacheDaemon::FilenameAscii( Value );
        }
    };

    //-------------------------------------------------------------------
    // OUTPUT_ASCII_VERSION
    //-------------------------------------------------------------------
    struct output_ascii_version
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_OUTPUT_ASCII_VERSION;
        static constexpr const char* DEFAULT = "0x00FF";

        static void
        reader( std::ostringstream& Value )
        {
            Value << diskCache::DumpCacheDaemon::ASCIIVersion( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = diskCache::DumpCacheDaemon::ASCIIVersion( );
        }

        static void
        writer( const std::string& Value )
        {
            diskCache::DumpCacheDaemon::version_type v;

            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            if ( Value.substr( 0, 2 ).compare( "0x" ) == 0 )
            {
                std::istringstream s( Value.substr( 2 ) );
                if ( !( s >> std::hex >> v ) )
                {
                    return;
                }
            }
            else if ( Value[ 0 ] == '0' )
            {
                std::istringstream s( Value.substr( 1 ) );
                if ( !( s >> std::oct >> v ) )
                {
                    return;
                }
            }
            else
            {
                std::istringstream s( Value );
                if ( !( s >> std::hex >> v ) )
                {
                    return;
                }
            }
            diskCache::DumpCacheDaemon::ASCIIVersion( v );
        }
    };

    //-------------------------------------------------------------------
    // OUTPUT_BINARY
    //-------------------------------------------------------------------
    struct output_binary
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_OUTPUT_BINARY;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << diskCache::DumpCacheDaemon::FilenameBinary( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = diskCache::DumpCacheDaemon::FilenameBinary( );
        }

        static void
        writer( const std::string& Value )
        {
            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            diskCache::DumpCacheDaemon::FilenameBinary( Value );
        }
    };

    //-------------------------------------------------------------------
    // OUTPUT_BINARY_VERSION
    //-------------------------------------------------------------------
    struct output_binary_version
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_OUTPUT_BINARY_VERSION;
        static constexpr const char* DEFAULT = "0x0101";

        static void
        reader( std::ostringstream& Value )
        {
            Value << diskCache::DumpCacheDaemon::BinaryVersion( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = diskCache::DumpCacheDaemon::BinaryVersion( );
        }
        static void
        writer( const std::string& Value )
        {
            diskCache::DumpCacheDaemon::version_type v;

            QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << Value,
                               MT_NOTE,
                               0,
                               "setup_variables",
                               "CXX" );
            if ( Value.substr( 0, 2 ).compare( "0x" ) == 0 )
            {
                std::istringstream s( Value.substr( 2 ) );
                if ( !( s >> std::hex >> v ) )
                {
                    return;
                }
            }
            else if ( Value[ 0 ] == '0' )
            {
                std::istringstream s( Value.substr( 1 ) );
                if ( !( s >> std::oct >> v ) )
                {
                    return;
                }
            }
            else
            {
                std::istringstream s( Value );
                if ( !( s >> std::hex >> v ) )
                {
                    return;
                }
            }
            diskCache::DumpCacheDaemon::BinaryVersion( v );
        }
    };

    //-------------------------------------------------------------------
    // RWLOCK_INTERVAL
    //-------------------------------------------------------------------
    struct rwlock_interval
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_RWLOCK_INTERVAL;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << LDASTools::AL::ReadWriteLock::Interval( -1 );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = LDASTools::AL::ReadWriteLock::Interval( -1 );
        }

        static void
        writer( const std::string& Value )
        {
            int v = -1;

            std::istringstream s( Value );

            if ( ( s >> v ) && ( v >= 0 ) )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                (void)LDASTools::AL::ReadWriteLock::Interval( v );
            }
        }
    };

    //-------------------------------------------------------------------
    // RWLOCK_TIMEOUT
    //-------------------------------------------------------------------
    struct rwlock_timeout
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_RWLOCK_TIMEOUT;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << LDASTools::AL::ReadWriteLock::Timeout( -1 );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = LDASTools::AL::ReadWriteLock::Timeout( -1 );
        }

        static void
        writer( const std::string& Value )
        {
            int v = -1;

            std::istringstream s( Value );

            if ( ( s >> v ) && ( v >= 0 ) )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                (void)LDASTools::AL::ReadWriteLock::Timeout( v );
            }
        }
    };

    //-------------------------------------------------------------------
    // SCAN_INTERVAL
    //-------------------------------------------------------------------
    struct scan_interval
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_SCAN_INTERVAL;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << diskCache::ScanMountPointsDaemon::Interval( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = diskCache::ScanMountPointsDaemon::Interval( );
        }

        static void
        writer( const std::string& Value )
        {
            INT_4U scan_interval;

            std::istringstream s( Value );

            if ( ( s >> scan_interval ) && ( scan_interval > 0 ) )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: "
                                                 << scan_interval,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                diskCache::ScanMountPointsDaemon::Interval( scan_interval );
            }
        }
    };

    //-------------------------------------------------------------------
    // STAT_TIMEOUT
    //-------------------------------------------------------------------
    struct stat_timeout
    {
        static constexpr const char* VARIABLE_NAME =
            diskCache::Variables::VAR_NAME_STAT_TIMEOUT;
        static constexpr const char* DEFAULT = "";

        static void
        reader( std::ostringstream& Value )
        {
            Value << GenericAPI::lstat_timeout( );
        }

        static void
        reader_any( boost::any& Value )
        {
            Value = GenericAPI::lstat_timeout( );
        }

        static void
        writer( const std::string& Value )
        {
            INT_4U v = 0;

            std::istringstream s( Value );

            if ( ( s >> v ) && ( v > 0 ) )
            {
                QUEUE_LOG_MESSAGE( VARIABLE_NAME << " set to: " << v,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                GenericAPI::SetStatTimeout( v );
            }
        }
    };
} // namespace

namespace diskCache
{
    //-----------------------------------------------------------------------------
    // Number of micro seconds to allow for lock acquisition
    // int RWLOCK_TIMEOUT = 10000;
    //-----------------------------------------------------------------------------
    int RWLOCK_TIMEOUT = 0;

    //-----------------------------------------------------------------------------
    /// This variable specifies the number of milliseconds to delay between
    /// successive scans of the list of mount points.
    ///
    /// The default value is 500 ( 0.5 sec. ).
    //-----------------------------------------------------------------------------
    INT_4U ScanInterval = 500;

    //-----------------------------------------------------------------------------
    /// The runtime initialization of the core ensures that many of the callback
    /// tables for various classes are properly initialized
    //-----------------------------------------------------------------------------
    void
    Initialize( )
    {
        typedef diskCache::Common::Registry::AlreadyRegisteredException
            AlreadyRegisteredException;

        variables_init( );
        diskCache::Cache::RegistrySingleton::id_type sdgtx_id(
            diskCache::Cache::RegistrySingleton::KEY_NULL );

        try
        {
            sdgtx_id = diskCache::Cache::SDGTx::RegisterSearchEngine( );
        }
        catch ( const AlreadyRegisteredException& Exception )
        {
            sdgtx_id = Exception.Id( );
        }
        //---------------------------------------------------------------------
        // Initialize ascii stream callbacks
        //---------------------------------------------------------------------
        Streams::OASCII::Writer( sdgtx_id, Cache::SDGTxDirectoryInfoToOASCII );
        //---------------------------------------------------------------------
        // Initialize binary stream callbacks
        //---------------------------------------------------------------------
        Streams::IBinary::Reader( sdgtx_id,
                                  Cache::SDGTxDirectoryInfoFromIBinary );
        Streams::OBinary::Writer( sdgtx_id,
                                  Cache::SDGTxDirectoryInfoToOBinary );
        //---------------------------------------------------------------------
        // Initialize indexing scheme
        //---------------------------------------------------------------------
    }

    void
    Teardown( )
    {
        diskCache::Common::Variables::Reset( );
        diskCache::MetaCommand::CommandTable::Teardown( );
    }

    static MutexLock::baton_type p_scanned_files_baton;
    static total_file_count_type p_scanned_files = 0;

    total_file_count_type
    ScannedFiles( )
    {
        MutexLock lock( p_scanned_files_baton, __FILE__, __LINE__ );

        return p_scanned_files;
    }

    void
    ScannedFiles( total_file_count_type Value )
    {
        MutexLock lock( p_scanned_files_baton, __FILE__, __LINE__ );

        if ( Value == SCANNED_FILES_RESET )
        {
            p_scanned_files = 0;
        }
        else
        {
            p_scanned_files += Value;
        }
    }
} // namespace diskCache

void
variables_init( )
{
    diskCache::Common::Variables::Init( //
        cache_write_delay_secs::VARIABLE_NAME,
        cache_write_delay_secs::reader,
        cache_write_delay_secs::reader_any,
        cache_write_delay_secs::writer,
        cache_write_delay_secs::DEFAULT( ) );

    diskCache::Common::Variables::Init( //
        concurrency::VARIABLE_NAME,
        concurrency::reader,
        concurrency::reader_any,
        concurrency::writer,
        concurrency::DEFAULT );

    diskCache::Common::Variables::Init( //
        directory_timeout::VARIABLE_NAME,
        directory_timeout::reader,
        directory_timeout::reader_any,
        directory_timeout::writer,
        directory_timeout::DEFAULT( ) );

    diskCache::Common::Variables::Init( //
        hot_directory_age::VARIABLE_NAME,
        hot_directory_age::reader,
        hot_directory_age::reader_any,
        hot_directory_age::writer,
        hot_directory_age::DEFAULT( ) );

    diskCache::Common::Variables::Init( //
        hot_directory_scan_interval::VARIABLE_NAME,
        hot_directory_scan_interval::reader,
        hot_directory_scan_interval::reader_any,
        hot_directory_scan_interval::writer,
        hot_directory_scan_interval::DEFAULT );

    diskCache::Common::Variables::Init( //
        log::VARIABLE_NAME,
        log::reader,
        log::reader_any,
        log::writer,
        log::DEFAULT );
    diskCache::Common::Variables::Cacheable( //
        log::VARIABLE_NAME,
        false );

    diskCache::Common::Variables::Init( //
        log_archive_directory::VARIABLE_NAME,
        log_archive_directory::reader,
        log_archive_directory::reader_any,
        log_archive_directory::writer,
        log_archive_directory::DEFAULT );
    diskCache::Common::Variables::Cacheable( //
        log_archive_directory::VARIABLE_NAME,
        false );

    diskCache::Common::Variables::Init( //
        log_debug_level::VARIABLE_NAME,
        log_debug_level::reader,
        log_debug_level::reader_any,
        log_debug_level::writer,
        log_debug_level::DEFAULT );
    diskCache::Common::Variables::Cacheable( //
        log_debug_level::VARIABLE_NAME,
        false );

    diskCache::Common::Variables::Init( //
        log_directory::VARIABLE_NAME,
        log_directory::reader,
        log_directory::reader_any,
        log_directory::writer,
        log_directory::DEFAULT( ) );
    diskCache::Common::Variables::Cacheable( //
        log_directory::VARIABLE_NAME,
        false );

    diskCache::Common::Variables::Init( //
        log_format::VARIABLE_NAME,
        log_format::reader,
        log_format::reader_any,
        log_format::writer,
        log_format::DEFAULT );
    diskCache::Common::Variables::Cacheable( //
        log_format::VARIABLE_NAME,
        false );

    diskCache::Common::Variables::Init( //
        log_rotate_entry_count::VARIABLE_NAME,
        log_rotate_entry_count::reader,
        log_rotate_entry_count::reader_any,
        log_rotate_entry_count::writer,
        log_rotate_entry_count::DEFAULT );
    diskCache::Common::Variables::Cacheable( //
        log_rotate_entry_count::VARIABLE_NAME,
        false );

    diskCache::Common::Variables::Init( //
        output_ascii::VARIABLE_NAME,
        output_ascii::reader,
        output_ascii::reader_any,
        output_ascii::writer,
        output_ascii::DEFAULT );

    diskCache::Common::Variables::Init( //
        output_ascii_version::VARIABLE_NAME,
        output_ascii_version::reader,
        output_ascii_version::reader_any,
        output_ascii_version::writer,
        output_ascii_version::DEFAULT );

    diskCache::Common::Variables::Init( //
        output_binary::VARIABLE_NAME,
        output_binary::reader,
        output_binary::reader_any,
        output_binary::writer,
        output_binary::DEFAULT );

    diskCache::Common::Variables::Init( //
        output_binary_version::VARIABLE_NAME,
        output_binary_version::reader,
        output_binary_version::reader_any,
        output_binary_version::writer,
        output_binary_version::DEFAULT );

    diskCache::Common::Variables::Init( //
        rwlock_interval::VARIABLE_NAME,
        rwlock_interval::reader,
        rwlock_interval::reader_any,
        rwlock_interval::writer,
        rwlock_interval::DEFAULT );

    diskCache::Common::Variables::Init( //
        rwlock_timeout::VARIABLE_NAME,
        rwlock_timeout::reader,
        rwlock_timeout::reader_any,
        rwlock_timeout::writer,
        rwlock_timeout::DEFAULT );

    diskCache::Common::Variables::Init( //
        scan_interval::VARIABLE_NAME,
        scan_interval::reader,
        scan_interval::reader_any,
        scan_interval::writer,
        scan_interval::DEFAULT );

    diskCache::Common::Variables::Init( //
        stat_timeout::VARIABLE_NAME,
        stat_timeout::reader,
        stat_timeout::reader_any,
        stat_timeout::writer,
        stat_timeout::DEFAULT );
}
