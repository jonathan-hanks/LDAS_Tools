//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <signal.h>
#include <unistd.h>

#include <cassert>
#include <cstdlib>

#include <iostream>
#include <list>
#include <sstream>
#include <stdexcept>
#include <typeinfo>

#include <boost/filesystem.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/DeadLockDetector.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/ldasexception.hh"
#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/SignalHandler.hh"

#include "genericAPI/swigexception.hh"
#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#include "diskcacheAPI/Cache/RegistrySingleton.hh"

#include "diskcacheAPI/IO.hh"
#include "diskcacheAPI/MetaCommands.hh"

#include "diskcacheAPI.hh"
#include "Commands.hh"

using LDASTools::AL::MemChecker;
using LDASTools::AL::Thread;

typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
typedef CommandLineOptions::Option        Option;
typedef CommandLineOptions::OptionSet     OptionSet;

typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;

using diskCache::MetaCommand::CommandTable;
using diskCache::MetaCommand::Daemon;
using diskCache::MetaCommand::Dump;
using diskCache::MetaCommand::Filenames;
using diskCache::MetaCommand::FilenamesRDS;
using diskCache::MetaCommand::Intervals;
using diskCache::MetaCommand::MountPointStats;
using diskCache::MetaCommand::Quit;
using diskCache::MetaCommand::Reconfigure;
using diskCache::MetaCommand::Scan;
using diskCache::MetaCommand::Status;

//=======================================================================
// Global variables
//=======================================================================

volatile sig_atomic_t exit_requested = 0;

//=======================================================================
// Signal handler routine
//=======================================================================

void
signal_handler( int sig )
{
    if ( sig == SIGSEGV && exit_requested )
    {
        return;
    }
    // Call the default signal handler for all other signals
    signal( sig, SIG_DFL );
    raise( sig );
}

void
register_signal_handlers( )
{
    struct sigaction sa;
    sa.sa_handler = signal_handler;
    sigemptyset( &sa.sa_mask );
    sa.sa_flags = 0;

    // Install the signal handler for SIGSEGV
    sigaction( SIGSEGV, &sa, NULL );
}

//=======================================================================
// Routine to setup certain variables
//=======================================================================
void
Initialize( )
{
    LDASTools::AL::ReadWriteLock::Timeout( 0 );
    GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
    diskCache::Initialize( );
}

void
Teardown( )
{
    GenericAPI::SyncLog( );
    GenericAPI::CloseLog( );
    diskCache::Teardown( );
}

//=======================================================================
//
//=======================================================================

void Usage( const CommandLineOptions::option_type& ProgramName,
            const OptionSet&                       Opts );

//=======================================================================
// Main
//=======================================================================
int
main( int ArgC, char** ArgV )
{
    static const char* caller = "main";

    int exit_code = 0;

    //---------------------------------------------------------------------
    // Make sure everything is ready for use
    //---------------------------------------------------------------------
    register_signal_handlers( );
    MemChecker::Trigger gc_trigger( true );

    try
    {
        Initialize( );
        Thread::StackSizeDefault( 64 * 1024 );
        {
            diskCache::Cache::SDGTx::file_extension_container_type ext;

            ext.push_back( ".gwf" );

            diskCache::Commands::updateFileExtList( ext );
        }
        ServerInfo Server;
        //---------------------------------------------------------------------
        // Variables for logging
        //---------------------------------------------------------------------
        static char cwd_buffer[ 2048 ];
        if ( getcwd( cwd_buffer,
                     sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) ) ==
             (char*)NULL )
        {
            throw std::runtime_error(
                "Unable to obtain the current working directory." );
        }

        std::string                       cwd( cwd_buffer );
        GenericAPI::Log::stream_file_type fs( new GenericAPI::Log::StreamFile );
        LDASTools::AL::Log::stream_type   s;

        s = fs;
        fs->FilenameExtension( GenericAPI::LogFormatter( )->FileExtension( ) );
        GenericAPI::LogFormatter( )->Stream( s );
        GenericAPI::setLogTag( "diskcache" );
        GenericAPI::LoggingInfo::LogDirectory( cwd );

        try
        {
            //-------------------------------------------------------------------
            // Collect information about the command line options
            //-------------------------------------------------------------------
            enum common_option_types
            {
                OPT_COMMON_CACHE_FILE,
                OPT_COMMON_LOG,
                OPT_COMMON_LOG_DIRECTORY,
                OPT_COMMON_LOG_ARCHIVE_DIRECTORY,
                OPT_COMMON_LOG_DEBUG_LEVEL,
                OPT_COMMON_LOG_FORMAT,
                OPT_COMMON_LOG_ROTATE_ENTRY_COUNT,
                OPT_COMMON_HELP,
                OPT_COMMON_HOST,
                OPT_COMMON_PORT,
                OPT_COMMON_VERSION
            };

            CommandLineOptions options( ArgC, ArgV );
            OptionSet          common_options;
            std::string        cache_filename;

            common_options.Synopsis(
                "[common_options]"
                " sub_command [command_options] [command_args]" );

            common_options.Summary(
                "This is a multi-purpose program that is intended"
                " to provide much information about a cache of"
                " file names." );

            common_options.Add(
                Option( OPT_COMMON_CACHE_FILE,
                        "cache-file",
                        Option::ARG_REQUIRED,
                        "Provide the file name of the cache file to use",
                        "filename" ) );
            common_options.Add(
                Option( OPT_COMMON_LOG,
                        "log",
                        Option::ARG_REQUIRED,
                        "Specify where the log messages should be written.",
                        "filename" ) );

            common_options.Add(
                Option( OPT_COMMON_LOG_DIRECTORY,
                        "log-directory",
                        Option::ARG_REQUIRED,
                        "Specify the directory to capture logging information",
                        "directory" ) );

            common_options.Add(
                Option( OPT_COMMON_LOG_ARCHIVE_DIRECTORY,
                        "log-archive-directory",
                        Option::ARG_REQUIRED,
                        "Specify the directory to archive logging information",
                        "directory" ) );

            common_options.Add( Option( OPT_COMMON_LOG_DEBUG_LEVEL,
                                        "log-debug-level",
                                        Option::ARG_REQUIRED,
                                        "Specify the debugging level",
                                        "level" ) );

            common_options.Add(
                Option( OPT_COMMON_LOG_FORMAT,
                        "log-format",
                        Option::ARG_REQUIRED,
                        "Specify the output format for the logs",
                        "<text|html>" ) );

            common_options.Add(
                Option( OPT_COMMON_LOG_ROTATE_ENTRY_COUNT,
                        "log-rotate-entry-count",
                        Option::ARG_REQUIRED,
                        "Specify the point at which to rotate the log files.",
                        "count" ) );

            common_options.Add( Option( OPT_COMMON_HELP,
                                        "help",
                                        Option::ARG_NONE,
                                        "Display this message" ) );

            common_options.Add(
                Option( OPT_COMMON_HOST,
                        "host",
                        Option::ARG_REQUIRED,
                        "Name of the host where the server resides",
                        "string" ) );

            common_options.Add( Option( OPT_COMMON_PORT,
                                        "port",
                                        Option::ARG_REQUIRED,
                                        "Port number on which to communicate",
                                        "integer" ) );

            common_options.Add(
                Option( OPT_COMMON_VERSION,
                        "version",
                        Option::ARG_NONE,
                        "Print version of software and then exit" ) );

            common_options.Add( Daemon::Options( ) );
            common_options.Add( Dump::Options( ) );
            common_options.Add( Filenames::Options( ) );
            common_options.Add( FilenamesRDS::Options( ) );
            common_options.Add( Intervals::Options( ) );
            common_options.Add( MountPointStats::Options( ) );
            common_options.Add( Scan::Options( ) );
            common_options.Add( Status::Options( ) );

            //-------------------------------------------------------------------
            // Parse the general arguments
            //-------------------------------------------------------------------
            try
            {
                std::string arg_name;
                std::string arg_value;
                bool        parsing = true;
                bool        dump_help = false;

                while ( parsing )
                {
                    int cmd_id =
                        options.Parse( common_options, arg_name, arg_value );

                    switch ( cmd_id )
                    {
                    case OPT_COMMON_CACHE_FILE:
                        cache_filename = arg_value;
                        break;
                    case OPT_COMMON_LOG:
                        GenericAPI::LDASplatform::AppName( arg_value );
                        break;
                    case OPT_COMMON_LOG_DIRECTORY:
                    {
                        boost::filesystem::create_directories( arg_value );
                        GenericAPI::LoggingInfo::LogDirectory( arg_value );
                        break;
                    }
                    case OPT_COMMON_LOG_ARCHIVE_DIRECTORY:
                    {
                        boost::filesystem::create_directories( arg_value );
                        GenericAPI::LoggingInfo::ArchiveDirectory( arg_value );
                        break;
                    }
                    case OPT_COMMON_LOG_DEBUG_LEVEL:
                    {
                        int                debug_level;
                        std::istringstream dbg_level_stream( arg_value );

                        dbg_level_stream >> debug_level;
                        GenericAPI::LoggingInfo::DebugLevel( debug_level );
                    }
                    break;
                    case OPT_COMMON_LOG_FORMAT:
                        GenericAPI::LoggingInfo::Format( arg_value );
                        break;
                    case OPT_COMMON_LOG_ROTATE_ENTRY_COUNT:
                    {
                        int                v;
                        std::istringstream s( arg_value );

                        s >> v;
                        GenericAPI::LoggingInfo::RotationCount( v );
                    }
                    break;
                    case OPT_COMMON_HELP:
                        dump_help = true;
                        parsing = false;
                        break;
                    case OPT_COMMON_HOST:
                    {
                        //-----------------------------------------------------------
                        // Setup the host where the server resides
                        //-----------------------------------------------------------
                        Server.Hostname( arg_value );
                    }
                    break;
                    case OPT_COMMON_PORT:
                    {
                        //-----------------------------------------------------------
                        // Setup the port where the server is listening for user
                        // requests
                        //-----------------------------------------------------------
                        std::istringstream    value_stream( arg_value );
                        ServerInfo::port_type port;

                        value_stream >> port;
                        Server.Port( port );
                    }
                    break;
                    case OPT_COMMON_VERSION:
                        std::cout << options.ProgramName( ) << " "
                                  << PACKAGE_VERSION << std::endl;
                        parsing = false;
                        exit( 0 );
                        break;
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    }
                }
                //-----------------------------------------------------------------
                // Check if help option was requested
                //-----------------------------------------------------------------
                if ( dump_help )
                {
                    if ( options.empty( ) == false )
                    {
                        const CommandLineOptions::option_type cmd(
                            options.Pop( ) );

                        switch ( CommandTable::Lookup( cmd ) )
                        {
                        case CommandTable::CMD_DAEMON:
                            Usage( options.ProgramName( ), Daemon::Options( ) );
                            break;
                        case CommandTable::CMD_DUMP:
                            Usage( options.ProgramName( ), Dump::Options( ) );
                            break;
                        case CommandTable::CMD_FILENAMES:
                            Usage( options.ProgramName( ),
                                   Filenames::Options( ) );
                            break;
                        case CommandTable::CMD_FILENAMES_RDS:
                            Usage( options.ProgramName( ),
                                   FilenamesRDS::Options( ) );
                            break;
                        case CommandTable::CMD_INTERVALS:
                            Usage( options.ProgramName( ),
                                   Intervals::Options( ) );
                            break;
                        case CommandTable::CMD_MOUNT_POINT_STATS:
                            Usage( options.ProgramName( ),
                                   MountPointStats::Options( ) );
                            break;
                        case CommandTable::CMD_SCAN:
                            Usage( options.ProgramName( ), Scan::Options( ) );
                            break;
                        case CommandTable::CMD_STATUS:
                            Usage( options.ProgramName( ), Status::Options( ) );
                            break;
                        default:
                            Usage( options.ProgramName( ), common_options );
                            break;
                        }
                    }
                    else
                    {
                        Usage( options.ProgramName( ), common_options );
                    }
                    exit( 0 );
                }
            }
            catch ( ... )
            {
            }
            //-------------------------------------------------------------------
            // Get the command and evaluate it.
            //-------------------------------------------------------------------
            if ( options.empty( ) == false )
            {
                //-----------------------------------------------------------------
                // Read the cache file
                //-----------------------------------------------------------------
                if ( cache_filename.empty( ) == false )
                {
                    //---------------------------------------------------------------
                    // Seed the  mount point manager with information from the
                    // binary stream
                    //---------------------------------------------------------------
                    LDASTools::AL::ifstream     ifs( cache_filename.c_str( ) );
                    diskCache::Streams::IBinary stream( ifs );

                    diskCache::Read( stream );
                    ifs.close( );
                }

                //-----------------------------------------------------------------
                // There is another command. Get it and take the appropriate
                // action
                //-----------------------------------------------------------------
                const CommandLineOptions::option_type cmd( options.Pop( ) );

                switch ( CommandTable::Lookup( cmd ) )
                {
                case CommandTable::CMD_QUIT:
                {
                    //-------------------------------------------------------------
                    // Quit the daemon
                    //-------------------------------------------------------------
                    Quit cmd( options, Server );

                    cmd( );
                }
                break;
                case CommandTable::CMD_DAEMON:
                {
                    //-------------------------------------------------------------
                    // Run as daemon
                    //-------------------------------------------------------------
                    std::string cfg_filename( SYSCONFDIR );
                    cfg_filename += "/ldas-tools.d/diskcache.cfg";
                    Daemon cmd( options,
                                Server,
                                cfg_filename,
                                cache_filename.empty( ) );

                    cmd( );
                }
                break;
                case CommandTable::CMD_DUMP:
                {
                    //-------------------------------------------------------------
                    // Dump the current contents of the cache
                    //-------------------------------------------------------------
                    Dump cmd( options, Server );

                    cmd( );
                }
                break;
                case CommandTable::CMD_FILENAMES:
                {
                    //-------------------------------------------------------------
                    // Dump the current contents of the cache
                    //-------------------------------------------------------------
                    QUEUE_LOG_MESSAGE( "Dumping filenames",
                                       MT_DEBUG,
                                       5,
                                       caller,
                                       "CMD_DISKCACHE" );

                    Filenames cmd( options, Server );

                    cmd( );
                }
                break;
                case CommandTable::CMD_FILENAMES_RDS:
                {
                    //-------------------------------------------------------------
                    // Dump the current contents of the cache
                    //-------------------------------------------------------------
                    FilenamesRDS cmd( options, Server );

                    cmd( );
                    if ( cmd.IsServer( ) == false )
                    {
                        const FilenamesRDS::query_results_type& ans =
                            cmd.Results( );
                        for ( FilenamesRDS::query_results_type::const_iterator
                                  cur = ans.begin( ),
                                  last = ans.end( );
                              cur != last;
                              ++cur )
                        {
                            std::cout << *cur << std::endl;
                        }
                    }
                }
                break;
                case CommandTable::CMD_INTERVALS:
                {
                    //-------------------------------------------------------------
                    // Dump the time range information
                    //-------------------------------------------------------------
                    Intervals cmd( options, Server );

                    cmd( );
                }
                break;
                case CommandTable::CMD_MOUNT_POINT_STATS:
                {
                    //-------------------------------------------------------------
                    // Dump the current contents of the cache
                    //-------------------------------------------------------------
                    MountPointStats cmd( options );

                    cmd( );
                }
                break;
                case CommandTable::CMD_RECONFIGURE:
                {
                    //-------------------------------------------------------------
                    // Re-read configuration file
                    //-------------------------------------------------------------
                    Reconfigure cmd( options, Server );

                    cmd( );
                }
                break;
                case CommandTable::CMD_SCAN:
                {
                    //-------------------------------------------------------------
                    // Dump the current contents of the cache
                    //-------------------------------------------------------------
                    Scan cmd( options );

                    cmd( );
                }
                break;
                case CommandTable::CMD_STATUS:
                {
                    //-------------------------------------------------------------
                    // Retrieve information about a running daemon
                    //-------------------------------------------------------------
                    Status cmd( options, Server );

                    cmd( );
                }
                break;
                default:
                {
                    std::ostringstream msg;

                    msg << "Unknown command: " << cmd;

                    throw std::runtime_error( msg.str( ) );
                }
                break;
                }
            }
        }
        catch ( const LdasException& Exception )
        {
            std::ostringstream result;
            std::ostringstream info;

            if ( Exception.getSize( ) == 0 )
            {
                result << "unkown_error";
            }
            else
            {
                size_t index = Exception.getSize( ) - 1;

                result << Exception[ index ].getMessage( );
                if ( Exception[ index ].getInfo( ).size( ) > 0 )
                {
                    result << std::string( ": " )
                           << Exception[ index ].getInfo( );
                }

                info << " [" << Exception[ index ].getFile( ) << ":"
                     << Exception[ index ].getLine( ) << "]";

                while ( index != 0 )
                {
                    --index;
                    info << "\n" << Exception[ index ].getMessage( );
                    if ( Exception[ index ].getInfo( ).size( ) > 0 )
                    {
                        info << ": ";
                    }
                    info << Exception[ index ].getInfo( ) << " ["
                         << Exception[ index ].getFile( ) << ":"
                         << Exception[ index ].getLine( ) << "]";
                }
            }

            std::ostringstream msg;

            msg << "Caught a LdasException" << result.str( );
            if ( ( info.str( ).empty( ) == false ) )
            {
                msg << " ( " << info.str( ) << " )";
            }
            QUEUE_LOG_MESSAGE(
                msg.str( ), MT_ERROR, 0, caller, "CMD_DISKCACHE" );
            exit_code = 1;
        }
        catch ( const SwigException& Exception )
        {
            QUEUE_LOG_MESSAGE( Exception.getResult( )
                                   << " ( " << Exception.getInfo( ) << " ) ",
                               MT_ERROR,
                               0,
                               caller,
                               "CMD_DISKCACHE" );
            exit_code = 1;
        }
        catch ( const std::exception& Exception )
        {
            QUEUE_LOG_MESSAGE(
                Exception.what( ), MT_ERROR, 0, caller, "CMD_DISKCACHE" );
#if DEAD_LOCK_DETECTOR_ENABLED
            LDASTools::AL::DeadLockDetector::Dump( );
#endif /* DEAD_LOCK_DETECTOR_ENABLED */
            exit_code = 1;
        }
        catch ( ... )
        {
            QUEUE_LOG_MESSAGE(
                "Unknown exception", MT_ERROR, 0, caller, "CMD_DISKCACHE" );
            exit_code = 1;
        }
        GenericAPI::SyncLog( );
    }
    catch ( const std::exception& Exc )
    {
        GenericAPI::SyncLog( );
        gc_trigger.DoGarbageCollection( );
        std::cerr << "FATAL: exception: " << Exc.what( ) << std::endl
                  << std::flush;
        exit_code = 98;
    }
    catch ( ... )
    {
        GenericAPI::SyncLog( );
        gc_trigger.DoGarbageCollection( );
        std::cerr << "FATAL: unknown exception" << std::endl << std::flush;
        exit_code = 99;
    }
    //---------------------------------------------------------------------
    // Leave with the appropriate exit code
    //---------------------------------------------------------------------
    Teardown( );
    gc_trigger.DoGarbageCollection( );
    exit_requested = 1;
    return exit_code;
}
//=======================================================================
// CommandTable
//=======================================================================

//=======================================================================
//
//=======================================================================
void
Usage( const CommandLineOptions::option_type& ProgramName,
       const OptionSet&                       Opts )
{
    std::cout << "Usage: " << ProgramName << Opts << std::endl;
}

/*! \page PDiskcacheCommand %Command - diskcache
  <!-- C O M M A N D S - d i s k c a c h e -->
  The diskcache command has two primary modes of operation.
  The first mode is a one pass mode.
  In this mode, the additional options on the command line represent
  the task to be performed and the command exits once completed.

  The second mode is a daemon mode.
  In this mode the diskcache starts up and maintains a cache of files
  based on the options passed on the command line.
  This cache can be queried and manipulated by input from standard input
  with the results being sent to standard output.

  <UL>
    <!-- ----------- -->
    <!-- D A E M O N -->
    <!-- ----------- -->
    <LI><B>daemon</B>
      The daemon sub command is intended to provide a continuous scanning
      mode.
      <TABLE>
        <TR>
          <TH>Option</TH><TH>Description</TH>
        </TR>
        <TR>
          <TD>--concurrency integer</TD>
          <TD>
            Number of mount points to scan concurrently
          </TD>
        </TR>
        <TR>
          <TD>--configuration-file filename</TD>
          <TD>
            Specify a file which contains configuration options.
          </TD>
        </TR>
        <TR>
          <TD>--excluded-directories list</TD>
          <TD>
            Comma separated list of directories not to be searched.
          </TD>
        </TR>
        <TR>
          <TD>--extensions list</TD>
          <TD>
            Comma seperated list of file extensions
          </TD>
        </TR>
        <TR>
          <TD>--log filename</TD>
          <TD>
            Specify where the log messages should be written.
          </TD>
        </TR>
        <TR>
          <TD>--log-directory directory</TD>
          <TD>
            Specify the directory to capture logging information.
          </TD>
        </TR>
        <TR>
          <TD>--mount-points list</TD>
          <TD>
            Comma seperated list of mount points to scan
          </TD>
        </TR>
        <TR>
          <TD>--output-ascii filename</TD>
          <TD>
            Filename for the ascii output; Use '-' to direct to standard out.
          </TD>
        </TR>
        <TR>
          <TD>--output-binary filename</TD>
            Filename for the binary output.
          <TD>
          </TD>
        </TR>
        <TR>
          <TD>--version-ascii version</TD>
          <TD>
            Version of the ascii cache dump format to use.
            By default, the latest version is used.
          </TD>
        </TR>
        <TR>
          <TD>--version-binary version</TD>
          <TD>
            Version of the binary cache dump format to use.
            By default, the latest version is used.
          </TD>
        </TR>
      </TABLE>
    <!-- ------- -->
    <!-- D U M P -->
    <!-- ------- -->
    <LI><B>dump</B>
      The dump sub command is intended to dump the contents of the memory
      cache. The quantity of data returned may be limited by the use of
      the following options.

      <TABLE>
        <TR>
          <TH>Option</TH><TH>Description</TH>
        </TR>
        <TR>
          <TD>--ifo pattern</TD>
          <TD>
            IFO pattern to use for search. (Default: all)
          </TD>
        </TR>
        <TR>
          <TD>--output-ascii filename</TD>
          <TD>
            Filename for the ascii output; '-' to direct to standard output
          </TD>
        </TR>
        <TR>
          <TD>--output-binary filename</TD>
          <TD>
            Filename for the binary output
          </TD>
        </TR>
        <TR>
          <TD>--type pattern</TD>
          <TD>
            Type pattern to use for search. (Default: all)
          </TD>
        </TR>
        <TR>
          <TD>--version version</TD>
          <TD>
            Version of the binary diskcache dump format to output
          </TD>
        </TR>
      </TABLE>
    <!-- ----------------- -->
    <!-- F I L E N A M E S -->
    <!-- ----------------- -->
    <LI><B>filenames</B>
      The filenames sub command is intended to query the memory cache.
      Several search options are available to restrict the set of filename
      returned.

      <TABLE>
        <TR>
          <TH>Option</TH><TH>Description</TH>
        </TR>
        <TR>
          <TD>--extension extension</TD>
          <TD>
            filename extension to search
          </TD>
        </TR>
        <TR>
          <TD>--ifo-type-list @<ifo@>-@<type@>- list</TD>
          <TD>
            comma seperated list of @<ifo@>-@<type@>- entries to search
          </TD>
        </TR>
        <TR>
          <TD>--start-time gps_time</TD>
          <TD>
            GPS start time of interest
          </TD>
        </TR>
        <TR>
          <TD>--end-time gps_time</TD>
          <TD>
            GPS end time of interest
          </TD>
        </TR>
      </TABLE>
    <!-- ------------------------- -->
    <!-- F I L E N A M E S - R D S -->
    <!-- ------------------------- -->
    <LI><B>filenames-rds</B>
      The filenames-rds sub command is intended to query the memory cache
      for filenames that can be used for createRDS command. Several search
      options are available to restrict the set of filename returned.

      <TABLE>
        <TR>
          <TH>Option</TH><TH>Description</TH>
        </TR>
        <TR>
          <TD>--extension extension</TD>
          <TD>
            filename extension to search
          </TD>
        </TR>
        <TR>
          <TD>--ifo ifo</TD>
          <TD>
            IFO
          </TD>
        </TR>
        <TR>
          <TD>--end-time gps_time</TD>
          <TD>
            GPS end time of interest
          </TD>
        </TR>
        <TR>
          <TD>--resample</TD>
          <TD>
            Specify that resampling will be done
          </TD>
        </TR>
        <TR>
          <TD>--start-time gps_time</TD>
          <TD>
            GPS start time of interest
          </TD>
        </TR>
        <TR>
          <TD>--type type</TD>
          <TD>
            Type specifier of the the filename.
          </TD>
        </TR>
      </TABLE>
    <!-- ----------------- -->
    <!-- I N T E R V A L S -->
    <!-- ----------------- -->
    <LI><B>intervals</B>
      The intervals sub command is intended to query the memory cache and
      return time intervals of file data.

      <TABLE>
        <TR>
          <TH>Option</TH><TH>Description</TH>
        </TR>
        <TR>
          <TD>--extension extension</TD>
          <TD>
            filename extension to search
          </TD>
        </TR>
        <TR>
          <TD>--ifo-type-list @<ifo@>-@<type@>- list</TD>
          <TD>
            comma seperated list of @<ifo@>-@<type@>- entries to search
          </TD>
        </TR>
        <TR>
          <TD>--start-time gps_time</TD>
          <TD>
            GPS start time of interest
          </TD>
        </TR>
        <TR>
          <TD>--end-time gps_time</TD>
          <TD>
            GPS end time of interest
          </TD>
        </TR>
      </TABLE>
    <!-- ----------------------------- -->
    <!-- M O U N T P O I N T S T A T S -->
    <!-- ----------------------------- -->
    <LI><B>mountPointStats</B>
      The mount-point-stats sub command is intended to get some
      statistical information the memory cache grouped by the mount
      points.

      <TABLE>
        <TR>
          <TH>Option</TH><TH>Description</TH>
        </TR>
        <TR>
          <TD>--ifo pattern</TD>
          <TD>
            IFO pattern to use for search. (Default: all)
          </TD>
        </TR>
        <TR>
          <TD>--type pattern</TD>
          <TD>
            Type pattern to use for search. (Default: all)
          </TD>
        </TR>
      </TABLE>
    <!-- ------- -->
    <!-- S C A N -->
    <!-- ------- -->
    <LI><B>scan</B>
      The scan sub command is intended to
      scan a set of directories for files of interest
      and generate a memory cache image.

      <TABLE>
        <TR>
          <TH>Option</TH>
          <TH>Description</TH>
        </TR>
        <TR>
          <TD>--concurrency &lt;integer&gt;</TD>
          <TD>
            Number of mount points to scan concurrently.
          </TD>
        </TR>
        <TR>
          <TD>--extensions &lt;list&gt;</TD>
          <TD>
            Comma seperated list of file extensions
          </TD>
        </TR>
        <TR>
          <TD>--mount-points &lt;list&gt;</TD>
          <TD>
            Comma seperated list of mount points to scan
          </TD>
        </TR>
        <TR>
          <TD>--output-ascii &lt;filename&gt;</TD>
          <TD>
            Filename for the ascii output; '-' to direct to standard output
          </TD>
        </TR>
        <TR>
          <TD>--output-binary &lt;filename&gt;</TD>
          <TD>
            Filename for the binary output
          </TD>
        </TR>
        <TR>
          <TD>--type &lt;pattern&gt;</TD>
          <TD>
            Type pattern to use for search. (Default: all)
          </TD>
        </TR>
        <TR>
          <TD>--version-ascii &lt;version&gt;</TD>
          <TD>
            Version of the ascii diskcache dump format to output
          </TD>
        </TR>
        <TR>
          <TD>--version-binary &lt;version&gt;</TD>
          <TD>
            Version of the binary diskcache dump format to output
          </TD>
        </TR>
      </TABLE>
      diskcache scan --concurrency=1 --extensions=.gwf --mount-points=${HOME}
  --output-ascii=cache.txt

      The above example shows how one can scan a directory looking for files
  with the .gwf extension under their home directory and dump the results in
  ASCII format to a file named cache.txt in the default dump format. Since there
  is only one top level directory, the concurrency doesn't need to be any
  greater than one.
    <!-- ----------- -->
    <!-- S T A T U S -->
    <!-- ----------- -->
    <LI><B>status</B>
      <TABLE>
      </TABLE>
  </UL>
*/
