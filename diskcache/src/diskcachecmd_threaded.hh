//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

//------------------------------------------------------------------------------
//
//: Write content of global frame data hash to binary file.
//
//! param: const char* bfilename - Name of the file to write binary frame hash
//! to.
//+       Default is an empty string (C++ will use default file name).
//
//! param: const char* afilename - Name of the file to write ascii frame hash
//! to.
//+       Default is an empty string (C++ will use default file name).
//
//! return: Nothing.
//
void writeDirCacheFilesInParallel( const char* bfilename = "",
                                   const char* afilename = "" );

#if defined( CREATE_THREADED1_DECL )
CREATE_THREADED1_DECL( getDirEnt, std::string, const std::string );
#endif /* defined( CREATE_THREADED1_DECL ) */

#if !FOR_PYTHON
CREATE_THREADED2V_DECL( writeDirCache,
                        const char*,
                        diskCache::Streams::Interface::version_type );
#endif /* ! FOR_PYTHON */

#if !FOR_PYTHON
CREATE_THREADED2V_DECL( writeDirCacheAscii,
                        const char*,
                        diskCache::Streams::Interface::version_type );
#endif /* ! FOR_PYTHON */

#if !FOR_PYTHON
CREATE_THREADED2V_DECL( writeDirCacheFiles, const char*, const char* );
#endif /* ! FOR_PYTHON */

#if !FOR_PYTHON
CREATE_THREADED1V_DECL( CacheRead, const char* );
#endif /* ! FOR_PYTHON */

#if !FOR_PYTHON
CREATE_THREADED0V_DECL( deleteDirCache );
#endif /* !FOR_PYTHON */
