//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <fstream>
#include <iomanip>

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "IO.hh"
#include "MetaCommands.hh"

typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;
using diskCache::MetaCommand::transfer_helper;

namespace
{
    class transfer : public transfer_helper
    {
    public:
        bool              ascii;
        std::stringstream ascii_out;
        bool              binary;
        std::stringstream binary_out;

        //-------------------------------------------------------------------
        /// \brief Default constructor
        //-------------------------------------------------------------------
        transfer( );

        //-------------------------------------------------------------------
        /// \brief read the reponce from the stream
        ///
        /// \param[in] Stream
        ///     The output stream from which to read the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream from which the responce was read.
        //-------------------------------------------------------------------
        std::istream& read( std::istream& Stream );

        //-------------------------------------------------------------------
        /// \brief write the reponce onto the stream
        ///
        /// \param[in] Stream
        ///     The output stream on which to write the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream on which the responce was written.
        //-------------------------------------------------------------------
        std::ostream& write( std::ostream& Stream );
    };

} // namespace

namespace diskCache
{
    namespace MetaCommand
    {
        //===================================================================
        // Dump
        //===================================================================
        OptionSet& Dump::m_options( Dump::init_options( ) );

        OptionSet&
        Dump::init_options( )
        {
            static OptionSet retval;

            retval.Synopsis( "Subcommand: dump" );

            retval.Summary(
                "The dump sub command is intended to dump the contents"
                " of the memory cache."
                " The quantity of data returned may be limited by the"
                " use of the following options." );

            retval.Add( Option( OPT_IFO,
                                "ifo",
                                Option::ARG_REQUIRED,
                                "IFO pattern to use for search. (Default: all)",
                                "pattern" ) );

            retval.Add( Option( OPT_OUTPUT_ASCII,
                                "output-ascii",
                                Option::ARG_REQUIRED,
                                "Filename for the ascii output; '-' to direct "
                                "to standard output",
                                "filename" ) );

            retval.Add( Option( OPT_OUTPUT_BINARY,
                                "output-binary",
                                Option::ARG_REQUIRED,
                                "Filename for the binary output",
                                "filename" ) );

            retval.Add(
                Option( OPT_TYPE,
                        "type",
                        Option::ARG_REQUIRED,
                        "Type pattern to use for search. (Default: all)",
                        "pattern" ) );

            retval.Add(
                Option( OPT_VERSION_ASCII,
                        "version-ascii",
                        Option::ARG_REQUIRED,
                        "Version of the ascii diskcache dump format to output",
                        "version" ) );
            retval.Add(
                Option( OPT_VERSION_BINARY,
                        "version-binary",
                        Option::ARG_REQUIRED,
                        "Version of the binary diskcache dump format to output",
                        "version" ) );

            return retval;
        }

        Dump::Dump( CommandLineOptions&                      Args,
                    const ClientServerInterface::ServerInfo& Server )
            : ClientServerInterface( Server ), m_args( Args ), m_ifo( "all" ),
              m_output_ascii( "" ), m_output_binary( "" ), m_type( "all" ),
              m_version_ascii( VERSION_DEFAULT_ASCII ),
              m_version_binary( VERSION_DEFAULT_BINARY )
        {
            if ( m_args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );

                while ( parsing )
                {
                    switch ( m_args.Parse( m_options, arg_name, arg_value ) )
                    {
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    case OPT_IFO:
                        m_ifo = arg_value;
                        break;
                    case OPT_TYPE:
                        m_type = arg_value;
                        break;
                    case OPT_OUTPUT_ASCII:
                        m_output_ascii = arg_value;
                        break;
                    case OPT_OUTPUT_BINARY:
                        m_output_binary = arg_value;
                        break;
                    case OPT_VERSION_ASCII:
                    {
                        std::istringstream version_stream( arg_value );
                        version_stream >> std::hex >> m_version_ascii >>
                            std::dec;
                    }
                    break;
                    case OPT_VERSION_BINARY:
                    {
                        std::istringstream version_stream( arg_value );
                        version_stream >> std::hex >> m_version_binary >>
                            std::dec;
                    }
                    break;
                    default:
                        break;
                    }
                }
            }
        }

        const OptionSet&
        Dump::Options( )
        {
            return m_options;
        }

        void
        Dump::evalClient( )
        {
            std::ostringstream cmd;
            std::streamsize    pwidth( cmd.width( ) );
            char               pfill( cmd.fill( ) );

            cmd << CommandTable::Lookup( CommandTable::CMD_DUMP );

            if ( !m_output_ascii.empty( ) )
            {
                const Option& opt( m_options[ OPT_OUTPUT_ASCII ] );
                cmd << " " << opt << " " << m_output_ascii;
                const Option& opt_ver( m_options[ OPT_VERSION_ASCII ] );
                cmd << " " << opt_ver << " "
                    << std::setiosflags( std::ios::showbase ) << std::setw( 4 )
                    << std::setfill( '0' ) << std::hex << m_version_ascii
                    << std::dec << std::setfill( pfill ) << std::setw( pwidth )
                    << std::resetiosflags( std::ios::showbase );
            }
            if ( !m_output_binary.empty( ) )
            {
                const Option& opt( m_options[ OPT_OUTPUT_BINARY ] );
                cmd << " --" << opt.ArgumentName( ) << " " << m_output_binary;
                const Option& opt_ver( m_options[ OPT_VERSION_BINARY ] );
                cmd << " --" << opt_ver.ArgumentName( ) << " "
                    << std::setiosflags( std::ios::showbase ) << std::setw( 4 )
                    << std::setfill( '0' ) << std::hex << m_version_binary
                    << std::dec << std::setfill( pfill ) << std::setw( pwidth )
                    << std::resetiosflags( std::ios::showbase );
            }
            cmd << std::endl;
            ServerRequest( cmd.str( ) );

            transfer responce;

            responce.read( *( serverRequestHandle( ) ) );
            //-----------------------------------------------------------------
            // Handle the ASCII dump
            //-----------------------------------------------------------------
            if ( m_output_ascii.empty( ) ||
                 ( m_output_ascii.compare( "-" ) == 0 ) )
            {
                std::cout << responce.ascii_out.str( );
            }
            else if ( m_output_ascii.empty( ) == false )
            {
                std::ofstream out( m_output_ascii.c_str( ) );
                out << responce.ascii_out.str( );
            }
            //-----------------------------------------------------------------
            // Handle the Binary dump
            //-----------------------------------------------------------------
            if ( m_output_binary.empty( ) == false )
            {
                std::ofstream out( m_output_binary.c_str( ),
                                   std::ios_base::out | std::ios_base::binary );
                out.write( responce.binary_out.str( ).c_str( ),
                           responce.binary_out.str( ).size( ) );
            }
        }

        void
        Dump::evalServer( )
        {
            transfer responce;
            //-----------------------------------------------------------------
            // Listening for requests over a socket.
            //-----------------------------------------------------------------
            if ( !m_output_ascii.empty( ) )
            {
                //---------------------------------------------------------------
                // Send the ascii dump over the socket
                //---------------------------------------------------------------
                diskCache::Streams::OASCII stream( responce.ascii_out,
                                                   m_version_ascii );

                responce.ascii = true;
                diskCache::Write( stream );
            }
            if ( !m_output_binary.empty( ) )
            {
                //---------------------------------------------------------------
                // Send the binary dump over the socket
                //---------------------------------------------------------------
                diskCache::Streams::OBinary stream( responce.binary_out,
                                                    m_version_binary );

                responce.binary = true;
                diskCache::Write( stream );
            }
            responce.write( *( clientHandle( ) ) );
        }

        void
        Dump::evalStandalone( )
        {
            //-----------------------------------------------------------------
            // Standalone mode
            //-----------------------------------------------------------------
            if ( m_output_ascii.empty( ) ||
                 ( m_output_ascii.compare( "-" ) == 0 ) )
            {
                //---------------------------------------------------------------
                // Dump the default mount point manager
                //---------------------------------------------------------------
                diskCache::Streams::OASCII stream( std::cout, m_version_ascii );

                diskCache::Write( stream );
            }
            else if ( m_output_ascii.empty( ) == false )
            {
                //---------------------------------------------------------------
                /// \todo
                ///     When the output is ascii, the query options should allow
                ///     the user to request a subset of the entire cache.
                //---------------------------------------------------------------
                //---------------------------------------------------------------
                // Dump the default mount point manager
                //---------------------------------------------------------------
                diskCache::Streams::OFStream file_stream( m_output_ascii );
                diskCache::Streams::OASCII   stream( file_stream,
                                                   m_version_ascii );

                diskCache::Write( stream );
            }
            if ( m_output_binary.empty( ) == false )
            {
                //---------------------------------------------------------------
                /// \todo
                ///     When the output is binary, the query options should
                ///     allow the user to request a subset of the entire cache.
                //---------------------------------------------------------------
                //---------------------------------------------------------------
                // Dump the default mount point manager
                //---------------------------------------------------------------
                diskCache::Streams::OFStream file_stream( m_output_binary );
                diskCache::Streams::OBinary  stream( file_stream,
                                                    m_version_binary );

                diskCache::Write( stream );
            }
        }

    } // namespace MetaCommand
} // namespace diskCache

namespace
{
    //===================================================================
    // transfer
    //===================================================================
    transfer::transfer( ) : ascii( false ), binary( false )
    {
    }

    std::istream&
    transfer::read( std::istream& Stream )
    {
        Blob( Stream, ascii, ascii_out );
        Blob( Stream, binary, binary_out );
        return Stream;
    }

    std::ostream&
    transfer::write( std::ostream& Stream )
    {
        Blob( Stream, ascii, ascii_out );
        Blob( Stream, binary, binary_out );
        Stream.flush( );
        return Stream;
    }
} // namespace
