//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__DIRECTORY_MANAGER_SINGLETON_HH
#define DISKCACHE__DIRECTORY_MANAGER_SINGLETON_HH

#include "ldastoolsal/ReadWriteSingleton.hh"

#include "diskcacheAPI/Cache/DirectoryManager.hh"

namespace diskCache
{
    namespace Cache
    {
        class QueryAnswer;
    }

    class MountPointManagerSingleton;

    //---------------------------------------------------------------------
    /// \brief Manages a collection of mount points.
    ///
    /// A collection of mount points is managed by this singleton class.
    /// Each mount point represents a starting point for data searches.
    /// The collection of mount points is order sensative in that
    /// directories that appear higher in the list will be prioritised
    /// when searching for data.
    //---------------------------------------------------------------------
    class DirectoryManagerSingleton : private Cache::DirectoryManager
    {
    public:
        DECLARE_READ_WRITE_SINGLETON( DirectoryManagerSingleton );

    public:
        typedef Cache::DirectoryManager::directory_ref_type directory_ref_type;

#if NEW_DIRECTORY_INTERFACE
        typedef Cache::DirectoryManager::ScanResults ScanResults;
#endif /* NEW_DIRECTORY_INTERFACE */

        static bool AddDirectory( directory_ref_type Directory );

        static void Find( const std::string& Root, Cache::QueryAnswer& Answer );

        //-------------------------------------------------------------------
        /// \brief Remove a directory from the collection
        //-------------------------------------------------------------------
        static void RemoveDirectoryRecursively( const std::string& Name );

        //-------------------------------------------------------------------
        /// \brief Recursively scan a managed directory.
        //-------------------------------------------------------------------
        static void Scan( const std::string& Root, ScanResults& Results );

        //-------------------------------------------------------------------
        /// \brief Initialize information from the stream.
        ///
        /// \param[in] Stream
        ///     Source of the information.
        //-------------------------------------------------------------------
        template < typename StreamT >
        static StreamT& Read( StreamT& Stream );

        //-------------------------------------------------------------------
        /// \brief Dump information to the stream.
        ///
        /// \param[in] Stream
        ///     Sink for the information.
        //-------------------------------------------------------------------
        template < typename OStreamT >
        static OStreamT& Write( OStreamT& Stream );

    private:
        friend class MountPointManagerSingleton;
    };

    inline bool
    DirectoryManagerSingleton::AddDirectory( directory_ref_type Directory )
    {
        return Instance( ).DirectoryManager::AddDirectory( Directory );
    }

    inline void
    DirectoryManagerSingleton::Find( const std::string&  Root,
                                     Cache::QueryAnswer& Answer )
    {
        Instance( ).DirectoryManager::Find( Root, Answer );
    }

    template < typename StreamT >
    inline StreamT&
    DirectoryManagerSingleton::Read( StreamT& Stream )
    {
        return Instance( ).DirectoryManager::Read( Stream );
    }

    inline void
    DirectoryManagerSingleton::RemoveDirectoryRecursively(
        const std::string& Name )
    {
        Instance( ).DirectoryManager::RemoveDirectoryRecursively( Name );
    }

    inline void
    DirectoryManagerSingleton::Scan( const std::string& Root,
                                     ScanResults&       Results )
    {
        Instance( ).DirectoryManager::Scan( Root, Results );
    }

    template < typename OStreamT >
    inline OStreamT&
    DirectoryManagerSingleton::Write( OStreamT& Stream )
    {
        return Instance( ).DirectoryManager::Write( Stream );
    }

} // namespace diskCache

#endif /* DISKCACHE__DIRECTORY_MANAGER_SINGLETON_HH */
