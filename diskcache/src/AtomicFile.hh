//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISK_CACHE__ATOMIC_FILE_HH
#define DISK_CACHE__ATOMIC_FILE_HH

#include <stdio.h>

#include <sstream>
#include <stdexcept>

#include "ldastoolsal/System.hh"

#include "genericAPI/Logging.hh"

#include "IO.hh"

#include "Streams/StreamsInterface.hh"

namespace diskCache
{
    template < typename STREAM, typename DCSTREAM >
    class AtomicFile
    {
    public:
        AtomicFile( const std::string& Filename );

        ~AtomicFile( );

        void Read( );

        void Write( Streams::Interface::version_type Version =
                        DCSTREAM::VERSION_DEFAULT );

    private:
        std::string m_filename;
        std::string m_filename_tmp;

        STREAM m_stream;

        static std::string tmp_filename( const std::string& Filename );
    };

    template < typename STREAM, typename DCSTREAM >
    inline AtomicFile< STREAM, DCSTREAM >::AtomicFile(
        const std::string& Filename )
        : m_filename( Filename )
    {
    }

    template < typename STREAM, typename DCSTREAM >
    inline AtomicFile< STREAM, DCSTREAM >::~AtomicFile( )
    {
        m_stream.close( );
    }

    template < typename STREAM, typename DCSTREAM >
    inline void
    AtomicFile< STREAM, DCSTREAM >::Read( )
    {
        m_stream.open( m_filename.c_str( ) );

        {
            DCSTREAM dc_stream( m_stream );
            ;

            diskCache::Read( dc_stream );
        }
        m_stream.close( );
    }

    template < typename STREAM, typename DCSTREAM >
    inline void
    AtomicFile< STREAM, DCSTREAM >::Write(
        Streams::Interface::version_type Version )
    {
        static const char* method_name =
            "diskCache::AtomicFile< STREAM, DCSTREAM >::Write";

        static const char  backup_ext[] = ".bak";
        std::ostringstream backup;

        try
        {
            QUEUE_LOG_MESSAGE( "m_filename: " << m_filename,
                               MT_DEBUG,
                               30,
                               method_name,
                               "CXX" );
            backup << m_filename << backup_ext;
            m_filename_tmp = tmp_filename( m_filename );

            QUEUE_LOG_MESSAGE( "Temporary filename: " << m_filename_tmp,
                               MT_DEBUG,
                               30,
                               method_name,
                               "CXX" );
            m_stream.open( m_filename_tmp.c_str( ) );
            if ( m_stream.is_open( ) )
            {

                DCSTREAM dc_stream( m_stream, Version );
                ;

                diskCache::Write( dc_stream );
                m_stream.close( );
                (void)( rename( m_filename.c_str( ), backup.str( ).c_str( ) ) );
                if ( rename( m_filename_tmp.c_str( ), m_filename.c_str( ) ) )
                {
                    // Creating backup copy failed:
                    std::ostringstream msg;
                    msg << "Could not create backup copy of already existing "
                           "cache file \'"
                        << m_filename << "\' (errno=" << strerror( errno )
                        << "). New cache file is stored as temporary \'"
                        << m_filename_tmp << '\'';
                    QUEUE_LOG_MESSAGE( "rename failed: " << msg.str( ),
                                       MT_DEBUG,
                                       10,
                                       method_name,
                                       "CXX" );
                    QUEUE_LOG_MESSAGE(
                        "last system error message: "
                            << LDASTools::System::ErrnoMessage( ),
                        MT_DEBUG,
                        10,
                        method_name,
                        "CXX" );
                    throw std::runtime_error( msg.str( ) );
                }
            }
            else
            {
                QUEUE_LOG_MESSAGE( "last system error message: "
                                       << LDASTools::System::ErrnoMessage( ),
                                   MT_DEBUG,
                                   10,
                                   method_name,
                                   "CXX" );
            }
        }
        catch ( const std::exception& Except )
        {
            QUEUE_LOG_MESSAGE(
                "Exiting with std::exception: " << Except.what( ),
                MT_DEBUG,
                10,
                method_name,
                "CXX" );
            QUEUE_LOG_MESSAGE( "last system error message: "
                                   << LDASTools::System::ErrnoMessage( ),
                               MT_DEBUG,
                               10,
                               method_name,
                               "CXX" );
            throw;
        }
        catch ( ... )
        {
            QUEUE_LOG_MESSAGE( "Exiting with unknown exception: ",
                               MT_DEBUG,
                               10,
                               method_name,
                               "CXX" );
            QUEUE_LOG_MESSAGE( "last system error message: "
                                   << LDASTools::System::ErrnoMessage( ),
                               MT_DEBUG,
                               10,
                               method_name,
                               "CXX" );
            throw;
        }
        QUEUE_LOG_MESSAGE( "Exiting normally: m_filename: " << m_filename,
                           MT_DEBUG,
                           30,
                           method_name,
                           "CXX" );
    }

    template < typename STREAM, typename DCSTREAM >
    inline std::string
    AtomicFile< STREAM, DCSTREAM >::tmp_filename( const std::string& Filename )
    {
        std::string retval( Filename );
        retval += ".tmp";

        return retval;
    }
} // namespace diskCache

#endif /* DISK_CACHE__ATOMIC_FILE_HH */
