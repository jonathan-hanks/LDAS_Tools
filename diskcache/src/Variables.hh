//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__VARIABLES_HH
#define DISKCACHE_API__VARIABLES_HH

#include <boost/any.hpp>
#include <boost/filesystem.hpp>

#include "ldastoolsal/Config.hh"

#include "diskcacheAPI/Common/Variables.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"
#include "diskcacheAPI/Streams/StreamsInterface.hh"
#include "diskcacheAPI/Commands.hh"
#include "diskcacheAPI/DumpCacheDaemon.hh"

namespace diskCache
{
    struct Variables
    {
        typedef int                      server_port_type;
        typedef boost::filesystem::path  filename_type;
        typedef INT_4U                   cache_write_delay_type;
        typedef std::list< std::string > excluded_container_type;
        typedef std::list< std::string > excluded_patterns_container_type;
        typedef diskCache::Cache::SDGTx::file_extension_container_type
            extensions_type;
        typedef MountPointManagerSingleton::mount_point_name_container_type
                                                            mount_points_type;
        typedef INT_4U                                      scan_interval_type;
        typedef diskCache::Streams::Interface::version_type version_type;
        typedef std::string                                 type_pattern_type;

        enum Variable
        {
            VAR_CONCURRENCY = 0x00001,
            VAR_CONFIGURATION_FILE = 0x00002,
            VAR_EXCLUDED_DIRECTORIES = 0x00004,
            VAR_EXTENSIONS = 0x00008,
            VAR_MOUNT_POINTS = 0x00010,
            VAR_OUTPUT_ASCII = 0x00020,
            VAR_OUTPUT_BINARY = 0x00040,
            VAR_VERSION_ASCII = 0x00080,
            VAR_VERSION_BINARY = 0x00100,
            VAR_SCAN_INTERVAL = 0x00200,
            VAR_STAT_TIMEOUT = 0x00400,
            VAR_CACHE_WRITE_DELAY = 0x00800,
            /// Number of millisecond interval
            VAR_RWLOCK_INTERVAL = 0x01000,
            /// Number of millisecond timeout
            VAR_RWLOCK_TIMEOUT = 0x02000,
            /// File name patterns to be excluded
            VAR_EXCLUDED_PATTERNS = 0x04000,
            /// Number of seconds for which a directory is considered hot
            VAR_HOT_DIRECTORY_AGE = 0x08000,
            /// Number of seconds between scans of hot directory list
            VAR_HOT_DIRECTORY_SCAN_INTERVAL = 0x10000,
            /// Number of seconds to wait for directory operations to
            /// complete
            VAR_DIRECTORY_TIMEOUT = 0x20000,
            /// Port where server is listening for new requests
            VAR_SERVER_PORT = 0x40000
        };

        class Config_ : public LDASTools::AL::Config
        {
        public:
            typedef LDASTools::AL::Config base_type;

            enum state_type
            {
                BLOCK_DAEMON,
                BLOCK_EXTENSIONS,
                BLOCK_EXCLUDED_DIRECTORIES,
                BLOCK_EXCLUDED_PATTERNS,
                BLOCK_MOUNT_POINTS,
                BLOCK_UNKNOWN,
                NON_BLOCK
            };

            Config_( Variables& Parser );

            void Parse( std::istream& Stream );

            virtual void ParseBlock( const std::string& Value );

            virtual void ParseKeyValue( const std::string& Key,
                                        const std::string& Value );

            virtual void ParseWord( const std::string& Value );

        protected:
            Variables& storage;
            state_type state;
        };

        static constexpr const char* VAR_NAME_CACHE_WRITE_DELAY_SECS{
            "CACHE_WRITE_DELAY_SECS"
        };
        static constexpr const char* VAR_NAME_CACHE_WRITE_DELAY{
            VAR_NAME_CACHE_WRITE_DELAY_SECS
        };
        static constexpr const char* VAR_NAME_CONCURRENCY{ "CONCURRENCY" };
        static constexpr const char* VAR_NAME_DIRECTORY_TIMEOUT{
            "DIRECTORY_TIMEOUT"
        };
        static constexpr const char* VAR_NAME_EXCLUDED_PATTERNS{
            "EXCLUDED_PATTERN"
        };
        static constexpr const char* VAR_NAME_HOT_DIRECTORY_AGE{
            "HOT_DIRECTORY_AGE_SEC"
        };
        static constexpr const char* VAR_NAME_HOT_DIRECTORY_SCAN_INTERVAL{
            "HOT_DIRECTORY_SCAN_INTERVAL_SEC"
        };
        static constexpr const char* VAR_NAME_LOG{ "LOG" };
        static constexpr const char* VAR_NAME_LOG_ARCHIVE_DIRECTORY{
            "LOG_ARCHIVE_DIRECTORY"
        };
        static constexpr const char* VAR_NAME_LOG_DEBUG_LEVEL{
            "LOG_DEBUG_LEVEL"
        };
        static constexpr const char* VAR_NAME_LOG_DIRECTORY{ "LOG_DIRECTORY" };
        static constexpr const char* VAR_NAME_LOG_FORMAT{ "LOG_FORMAT" };
        static constexpr const char* VAR_NAME_LOG_ROTATE_ENTRY_COUNT{
            "LOG_ROTATE_ENTRY_COUNT"
        };
        static constexpr const char* VAR_NAME_OUTPUT_ASCII{ "OUTPUT_ASCII" };
        static constexpr const char* VAR_NAME_OUTPUT_ASCII_VERSION{
            "OUTPUT_ASCII_VERSION"
        };
        static constexpr const char* VAR_NAME_OUTPUT_BINARY{ "OUTPUT_BINARY" };
        static constexpr const char* VAR_NAME_OUTPUT_BINARY_VERSION{
            "OUTPUT_BINARY_VERSION"
        };
        static constexpr const char* VAR_NAME_RWLOCK_INTERVAL{
            "RWLOCK_INTERVAL_MS"
        };
        static constexpr const char* VAR_NAME_RWLOCK_TIMEOUT{
            "RWLOCK_TIMEOUT_MS"
        };
        static constexpr const char* VAR_NAME_SCAN_INTERVAL{ "SCAN_INTERVAL" };
        static constexpr const char* VAR_NAME_SERVER_PORT{ "SERVER_PORT" };
        static constexpr const char* VAR_NAME_STAT_TIMEOUT{ "STAT_TIMEOUT" };

        void ParseConfigurationFile( std::istream& Stream );

        void ParseConfigurationFile( std::istream& Stream,
                                     Config_&      ConfigParser );

        void Setup( int Mask ) const;

    private:
    public:
        std::string                              GetOutputASCII( ) const;
        diskCache::DumpCacheDaemon::version_type GetOutputASCIIVersion( ) const;
        std::string                              GetOutputBinary( ) const;
        diskCache::DumpCacheDaemon::version_type
        GetOutputBinaryVersion( ) const;

        inline void
        push_excluded_directory( const std::string& Directory )
        {
            excluded_directories.emplace_back( Directory );
        }

        inline void
        push_excluded_pattern( const std::string& Pattern )
        {
            excluded_patterns.emplace_back( Pattern );
        }

        inline void
        push_extension( const std::string& Extension )
        {
            extensions.emplace_back( Extension );
        }

        inline void
        push_mount_point( const std::string& MountPoint )
        {
            mount_points.emplace_back( MountPoint );
        }

        inline void
        reset_extensions( )
        {
            extensions.clear( );
        }

        inline void
        reset_excluded_patterns( )
        {
            excluded_patterns.clear( );
        }

        inline void
        reset_mount_points( )
        {
            mount_points.clear( );
        }

        inline void
        set_type_pattern( const std::string& StringValue )
        {
            type_pattern = StringValue;
        }

    private:
        excluded_container_type          excluded_directories;
        excluded_patterns_container_type excluded_patterns;
        extensions_type                  extensions;
        mount_points_type                mount_points;
        type_pattern_type                type_pattern;
        version_type version_binary = Streams::Interface::VERSION_NONE;
    }; // struct Variables

    inline std::string
    Variables::GetOutputASCII( ) const
    {
        return (
            boost::any_cast< std::string >( diskCache::Common::Variables::Get(
                Variables::VAR_NAME_OUTPUT_ASCII ) ) );
    }

    inline std::string
    Variables::GetOutputBinary( ) const
    {
        return (
            boost::any_cast< std::string >( diskCache::Common::Variables::Get(
                Variables::VAR_NAME_OUTPUT_BINARY ) ) );
    }

} // namespace diskCache

#endif /* DISKCACHE_API__VARIABLES_HH */
