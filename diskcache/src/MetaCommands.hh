//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__META_COMMANDS_HH
#define DISKCACHE_API__META_COMMANDS_HH

#include <bitset>
#include <iomanip>
#include <list>
#include <string>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/CommandLineOptions.hh"
#include "ldastoolsal/Config.hh"
#include "ldastoolsal/Thread.hh"
#include "ldastoolsal/SignalHandler.hh"
#include "ldastoolsal/Singleton.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/SocketStream.hh"

#include "diskcacheAPI/Cache/SDGTx.hh"

#include "diskcacheAPI/DumpCacheDaemon.hh"
#include "diskcacheAPI/MountPointManagerSingleton.hh"
#include "diskcacheAPI/Variables.hh"

namespace diskCache
{
    namespace MetaCommand
    {
        typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
        typedef CommandLineOptions::Option        Option;
        typedef CommandLineOptions::OptionSet     OptionSet;
        typedef INT_4U                            time_type;
        typedef boost::shared_ptr< GenericAPI::Net::ServerSocketStream >
            server_type;
        typedef server_type::element_type::accept_stream_type
                                                                   server_responce_type;
        typedef boost::shared_ptr< GenericAPI::Net::SocketStream > client_type;
        typedef diskCache::Streams::Interface::version_type        version_type;
        typedef INT_4U scan_interval_type;
        typedef INT_4U stat_timeout_type;

        enum
        {
            VERSION_DEFAULT_BINARY =
                diskCache::Streams::OBinary::VERSION_DEFAULT
        };
        enum
        {
            VERSION_DEFAULT_ASCII = diskCache::Streams::OASCII::VERSION_DEFAULT
        };

        class transfer_helper
        {
        public:
            typedef INT_4U size_type;
            typedef CHAR_U bool_type;

            template < typename Type >
            inline std::istream&
            Input( std::istream& Stream, Type& Data )
            {
                Stream.read( reinterpret_cast< char* >( &Data ),
                             sizeof( Type ) );
                return Stream;
            }

            template < typename Type >
            inline std::ostream&
            Output( std::ostream& Stream, Type Data )
            {
                Stream.write( reinterpret_cast< const char* >( &Data ),
                              sizeof( Data ) );
                return Stream;
            }

            //---------------------------------------------------------------------
            /// \brief Read a raw block of information
            //---------------------------------------------------------------------
            inline std::istream&
            Blob( std::istream& Stream, bool& Available, std::iostream& Buffer )
            {
                bool_type a;

                Input( Stream, a );
                Available = a;
                if ( Available )
                {
                    size_type length;

                    Input( Stream, length );
                    if ( length > 0 )
                    {
                        std::string b;

                        std::streamsize offset( 0 );

                        b.resize( length );
                        do
                        {
                            Stream.read( &( b[ offset ] ),
                                         ( std::streamsize )(length)-offset );
                            offset += Stream.gcount( );
                        } while ( size_type( offset ) < length );
                        Buffer.write( &( b[ 0 ] ), length );
                        std::flush( Buffer );
                    }
                }

                return Stream;
            }

            //---------------------------------------------------------------------
            /// \brief Write a raw block of information
            //---------------------------------------------------------------------
            inline std::ostream&
            Blob( std::ostream& Stream, bool Available, std::iostream& Buffer )
            {
                bool_type a( Available );
                size_type len;

                Buffer.seekg( 0, std::ios_base::end );
                len = Buffer.tellg( );
                Buffer.seekg( 0, std::ios_base::beg );

                Output( Stream, a );
                if ( Available )
                {
                    Output( Stream, len );
                    if ( len > 0 )
                    {
                        Stream << Buffer.rdbuf( );
                    }
                }
                std::flush( Stream );
                return Stream;
            }
        };

        class ClientServerInterface
        {
        public:
            //-----------------------------------------------------------------
            // Server information
            //-----------------------------------------------------------------
            class ServerInfo
            {
            public:
                typedef int port_type;

                ServerInfo( );

                void Hostname( const std::string& Value );

                const std::string& Hostname( ) const;

                void Port( port_type Value );

                port_type Port( ) const;

            private:
                std::string hostname;
                port_type   port;
            };

            typedef ServerInfo::port_type port_type;

            ClientServerInterface( const ServerInfo& Server );

            virtual ~ClientServerInterface( );

            void ClientHandle( server_responce_type Value );

            bool IsServer( ) const;

            const ServerInfo& Server( ) const;

            const std::string& ServerHostname( ) const;

            port_type ServerPort( ) const;

            void ServerRequest( const std::string& Command );

            void operator( )( );

        protected:
            server_responce_type clientHandle( );

            void eval( );

            virtual void evalClient( ) = 0;

            virtual void evalServer( ) = 0;

            virtual void evalStandalone( ) = 0;

            client_type serverRequestHandle( );

        private:
            const ServerInfo& server;

            server_responce_type handle_client;

            client_type server_request_handle;
        };

        inline void
        ClientServerInterface::ServerInfo::Hostname( const std::string& Value )
        {
            hostname = Value;
        }

        inline const std::string&
        ClientServerInterface::ServerInfo::Hostname( ) const
        {
            return hostname;
        }

        inline void
        ClientServerInterface::ServerInfo::Port( port_type Value )
        {
            port = Value;
        }

        inline ClientServerInterface::ServerInfo::port_type
        ClientServerInterface::ServerInfo::Port( ) const
        {
            return port;
        }

        inline ClientServerInterface::ClientServerInterface(
            const ServerInfo& Server )
            : server( Server )
        {
        }

        inline bool
        ClientServerInterface::IsServer( ) const
        {
            return ( ( handle_client ) ? true : false );
        }

        inline const ClientServerInterface::ServerInfo&
        ClientServerInterface::Server( ) const
        {
            return server;
        }

        inline const std::string&
        ClientServerInterface::ServerHostname( ) const
        {
            return server.Hostname( );
        }

        inline ClientServerInterface::port_type
        ClientServerInterface::ServerPort( ) const
        {
            return server.Port( );
        }

        inline void
        ClientServerInterface::ServerRequest( const std::string& Command )
        {
            if ( !server_request_handle )
            {
                server_request_handle =
                    client_type( new client_type::element_type( ) );
                server_request_handle->open( ServerHostname( ), ServerPort( ) );
            }
            ( *server_request_handle ) << INT_4U( Command.size( ) );
            server_request_handle->write( &( Command[ 0 ] ), Command.size( ) );
            server_request_handle->flush( );
        }

        inline void
        ClientServerInterface::operator( )( )
        {
            eval( );
        }

        inline void
        ClientServerInterface::ClientHandle( server_responce_type Value )
        {
            handle_client = Value;
        }

        inline server_responce_type
        ClientServerInterface::clientHandle( )
        {
            return handle_client;
        }

        inline void
        ClientServerInterface::eval( )
        {
            if ( clientHandle( ) )
            {
                evalServer( );
            }
            else if ( ServerPort( ) >= 0 )
            {
                evalClient( );
            }
            else
            {
                evalStandalone( );
            }
        }

        inline client_type
        ClientServerInterface::serverRequestHandle( )
        {
            return server_request_handle;
        }
        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class CommandTable
        {
            SINGLETON_TS_DECL( CommandTable );

        public:
            enum command_type
            {
                CMD_UNKNOWN,
                CMD_DAEMON,
                CMD_DUMP,
                CMD_FILENAMES,
                CMD_FILENAMES_RDS,
                CMD_INTERVALS,
                CMD_MOUNT_POINT_STATS,
                CMD_QUIT,
                CMD_RECONFIGURE,
                CMD_SCAN,
                CMD_STATUS
            };

            //-----------------------------------------------------------------
            /// \brief Destructor to release system resources
            //-----------------------------------------------------------------
            ~CommandTable( );

            //-----------------------------------------------------------------
            /// \brief Translate from ascii to enumerated type
            ///
            /// \param[in] Name
            ///     The ascii form of the command
            ///
            /// \return
            ///     The appropriate value from the command_type enumeration.
            ///     If Name cannot be mapped, then the value CMD_UNKNOWN is
            ///     returned.
            //-----------------------------------------------------------------
            static command_type Lookup( const std::string& Name );

            static const char* Lookup( command_type Command );

            static void Teardown( );

        private:
            typedef LDASTools::AL::unordered_map< std::string, command_type >
                command_table_type;

            command_table_type command_table;

            command_type lookup( const std::string& Name );

            const char* lookup( command_type Command );
        };

        //-------------------------------------------------------------------
        /// \brief Daemon mode
        //-------------------------------------------------------------------
        class Daemon : public LDASTools::AL::Thread,
                       public LDASTools::AL::SignalHandler::Callback
        {
        public:
            typedef LDASTools::AL::SignalHandler::signal_type signal_type;

            class DaemonConfig_ : public diskCache::Variables::Config_
            {
            public:
              typedef diskCache::Variables::Config_ config_base_type;
              DaemonConfig_( Daemon& Command );

                virtual void ParseKeyValue( const std::string& Key,
                                            const std::string& Value );
            protected:
              Daemon& command;
            };

            //-----------------------------------------------------------------
            /// \brief Constructor
            ///
            /// \param[in] Args
            ///     Command line options to configure command.
            /// \param[in] Server
            ///     Information describing the server connection.
            /// \param[in] DefaultConfigurationFilename
            ///     Specify the default configuration filename.
            ///
            //-----------------------------------------------------------------
            Daemon( CommandLineOptions&                      Args,
                    const ClientServerInterface::ServerInfo& Server,
                    const std::string& DefaultConfigurationFilename =
                        std::string( "" ),
                    const bool Seedable = true );

            //-----------------------------------------------------------------
            /// \brief Destructor
            //-----------------------------------------------------------------
            ~Daemon( );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            static const OptionSet& Options( );

            //-----------------------------------------------------------------
            // @brief The port being monitored for requests
            //-----------------------------------------------------------------
            int ServerPort( ) const;

            //-----------------------------------------------------------------
            /// \brief Register signal handler
            //-----------------------------------------------------------------
            void ResetOnSignal( bool Value );

            virtual void SignalCallback( signal_type Signal );

            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            void operator( )( );

            static const char* VAR_NAME_EXCLUDED_PATTERNS;

          inline Variables& GetVariables( )
          {
            return( variables );
          }

        private:
            enum
            {
                OPT_CONCURRENCY,
                OPT_CONFIGURATION_FILE,
                OPT_DIRECTORY_TIMEOUT,
                OPT_EXCLUDED_DIRECTORIES,
                OPT_EXCLUDED_PATTERNS,
                OPT_EXTENSIONS,
                OPT_HOT_DIRECTORY_AGE,
                OPT_HOT_DIRECTORY_SCAN_INTERVAL,
                OPT_RWLOCK_INTERVAL,
                OPT_RWLOCK_TIMEOUT,
                OPT_SCAN_INTERVAL,
                OPT_STAT_TIMEOUT,
                OPT_MOUNT_POINTS,
                OPT_OUTPUT_ASCII,
                OPT_OUTPUT_BINARY,
                OPT_VERSION_ASCII,
                OPT_VERSION_BINARY
            };

            enum
            {
                VAR_CONCURRENCY = 0x00001,
                VAR_CONFIGURATION_FILE = 0x00002,
                VAR_EXCLUDED_DIRECTORIES = 0x00004,
                VAR_EXTENSIONS = 0x00008,
                VAR_MOUNT_POINTS = 0x00010,
                VAR_OUTPUT_ASCII = 0x00020,
                VAR_OUTPUT_BINARY = 0x00040,
                VAR_VERSION_ASCII = 0x00080,
                VAR_VERSION_BINARY = 0x00100,
                VAR_SCAN_INTERVAL = 0x00200,
                VAR_STAT_TIMEOUT = 0x00400,
                VAR_CACHE_WRITE_DELAY = 0x00800,
                /// Number of millisecond interval
                VAR_RWLOCK_INTERVAL = 0x01000,
                /// Number of millisecond timeout
                VAR_RWLOCK_TIMEOUT = 0x02000,
                /// File name patterns to be excluded
                VAR_EXCLUDED_PATTERNS = 0x04000,
                /// Number of seconds for which a directory is considered hot
                VAR_HOT_DIRECTORY_AGE = 0x08000,
                /// Number of seconds between scans of hot directory list
                VAR_HOT_DIRECTORY_SCAN_INTERVAL = 0x10000,
                /// Number of seconds to wait for directory operations to
                /// complete
                VAR_DIRECTORY_TIMEOUT = 0x20000
            };

            static const int HOT_VARIABLES = //
                VAR_CACHE_WRITE_DELAY | //
                VAR_CONCURRENCY | //
                VAR_EXCLUDED_DIRECTORIES | //
                VAR_EXCLUDED_PATTERNS | //
                VAR_EXTENSIONS | //
                VAR_HOT_DIRECTORY_AGE | //
                VAR_HOT_DIRECTORY_SCAN_INTERVAL | //
                VAR_MOUNT_POINTS | //
                VAR_RWLOCK_INTERVAL | //
                VAR_RWLOCK_TIMEOUT | //
                VAR_SCAN_INTERVAL | //
                VAR_STAT_TIMEOUT //
                ;

            static bool       daemon_mode;
            static OptionSet& m_options;

            static OptionSet& init_options( );

            //-----------------------------------------------------------------
            /// \brief Handle client side requests
            //-----------------------------------------------------------------
            void action( );

            void do_client_request( );

            bool process_cmd( CommandLineOptions& Options );

            bool read_command( char* Buffer, size_t BufferSize );

            CommandLineOptions m_args;

            typedef diskCache::Cache::SDGTx::file_extension_container_type
                extensions_type;
            typedef MountPointManagerSingleton::mount_point_name_container_type
                mount_points_type;

            Variables variables;

            std::string       configuration_filename;
            stat_timeout_type stat_timeout;
            server_type       server;
            //-----------------------------------------------------------------
            /// \brief
            ///
            /// \todo
            ///     This needs to be made thread safe by having a separate one
            ///     for each thread.
            //-----------------------------------------------------------------
            server_responce_type client;
            bool                 finished;
            bool                 reset_requested;

            const ClientServerInterface::ServerInfo& server_info;

            void cache_write_delay( const std::string& Delay );

            void set_concurrency( const std::string& Value );

            void set_output_ascii( const std::string& Value );

            void set_output_binary( const std::string& Value );

            void set_output_binary_version( const std::string& Value );

            void set_server_port( const std::string& Value );

            //-----------------------------------------------------------------
            /// \brief Establish the scanning interval
            ///
            /// \param[in] Value
            ///     The string representation of the number of milliseconds
            ///     to pause between scans of the mount point lists.
            //-----------------------------------------------------------------
            void set_scan_interval( const std::string& Value );

            //-----------------------------------------------------------------
            /// \brief Establish the stat timeout value
            ///
            /// \param[in] Value
            ///     The string representation of the number of seconds
            ///     to wait for the system stat call to return.
            //-----------------------------------------------------------------
            void set_stat_timeout( const std::string& Value );

            //-----------------------------------------------------------------
            /// \brief setup the daemon with configuration values
            ///
            /// \param[in] Mask
            ///     Restrict the variable set using mask composed of
            ///     ORing together the various VAR_ types.
            //-----------------------------------------------------------------
            void setup_variables( int Mask = ~0 );
            //-----------------------------------------------------------------
            /// \brief Reset the daemon
            //-----------------------------------------------------------------
            void reset( );
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class Dump : public ClientServerInterface
        {
        public:
            Dump( CommandLineOptions&                      Args,
                  const ClientServerInterface::ServerInfo& Server );

            static const OptionSet& Options( );

        protected:
            virtual void evalClient( );

            virtual void evalServer( );

            virtual void evalStandalone( );

        private:
            enum
            {
                OPT_IFO,
                OPT_OUTPUT_ASCII,
                OPT_OUTPUT_BINARY,
                OPT_TYPE,
                OPT_VERSION_ASCII,
                OPT_VERSION_BINARY
            };

            static OptionSet& m_options;

            CommandLineOptions m_args;

            std::string  m_ifo;
            std::string  m_output_ascii;
            std::string  m_output_binary;
            std::string  m_type;
            version_type m_version_ascii = VERSION_DEFAULT_ASCII;
            version_type m_version_binary = VERSION_DEFAULT_BINARY;

            static OptionSet& init_options( );
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class Filenames : public ClientServerInterface
        {
        public:
            Filenames( CommandLineOptions&                      Args,
                       const ClientServerInterface::ServerInfo& Server );

            static const OptionSet& Options( );

        protected:
            virtual void evalClient( );

            virtual void evalServer( );

            virtual void evalStandalone( );

            void process( std::ostream& Stream );

        private:
            enum
            {
                OPT_EXTENSION,
                OPT_IFO_TYPE_LIST,
                OPT_START_TIME,
                OPT_END_TIME
            };
            static OptionSet& m_options;

            static OptionSet& init_options( );

            CommandLineOptions m_args;

            std::string m_extension;
            std::string m_ifo_type_list;
            time_type   m_time_start;
            time_type   m_time_stop;
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class FilenamesRDS : public ClientServerInterface
        {
        public:
            typedef std::list< std::string > query_results_type;

            FilenamesRDS( CommandLineOptions&                      Args,
                          const ClientServerInterface::ServerInfo& Server );

            const query_results_type& Results( ) const;

            static const OptionSet& Options( );

        protected:
            virtual void evalClient( );

            virtual void evalServer( );

            virtual void evalStandalone( );

            void process( std::ostream& Stream );

        private:
            enum
            {
                OPT_EXTENSION,
                OPT_IFO,
                OPT_TYPE,
                OPT_START_TIME,
                OPT_END_TIME,
                OPT_RESAMPLE
            };
            static OptionSet& m_options;

            static OptionSet& init_options( );

            CommandLineOptions m_args;

            std::string m_extension;
            std::string m_ifo;
            bool        m_resample;
            time_type   m_time_start;
            time_type   m_time_stop;
            std::string m_type;

            query_results_type results;
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class Intervals : public ClientServerInterface
        {
        public:
            Intervals( CommandLineOptions&                      Args,
                       const ClientServerInterface::ServerInfo& Server );

            static const OptionSet& Options( );

        protected:
            virtual void evalClient( );

            virtual void evalServer( );

            virtual void evalStandalone( );

            void process( std::ostream& Stream );

        private:
            enum
            {
                OPT_IFO_TYPE_LIST,
                OPT_START_TIME,
                OPT_END_TIME,
                OPT_EXTENSION
            };

            static OptionSet& m_options;

            static OptionSet& init_options( );

            CommandLineOptions m_args;

            std::string m_extension;
            std::string m_ifo_type_list;
            time_type   m_time_start;
            time_type   m_time_stop;
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class MountPointStats
        {
        public:
            MountPointStats( CommandLineOptions& Args );

            void operator( )( );

            static const OptionSet& Options( );

        private:
            enum
            {
                OPT_IFO,
                OPT_TYPE
            };
            static OptionSet& m_options;

            static OptionSet& init_options( );

            CommandLineOptions m_args;

            std::string m_ifo;
            std::string m_type;
        };

        class Quit : public ClientServerInterface
        {
        public:
            std::string msg;

            Quit( CommandLineOptions&                      Args,
                  const ClientServerInterface::ServerInfo& Server );

            static const OptionSet& Options( );

        protected:
            virtual void evalClient( );

            virtual void evalServer( );

            virtual void evalStandalone( );

        private:
            static OptionSet& m_options;

            CommandLineOptions m_args;

            static OptionSet& init_options( );
        };

        //-------------------------------------------------------------------
        /// \brief Have daemon reread configuration file
        //-------------------------------------------------------------------
        class Reconfigure : public ClientServerInterface
        {
        public:
            std::string msg;

            Reconfigure( CommandLineOptions&                      Args,
                         const ClientServerInterface::ServerInfo& Server );

            void File( const std::string& Filename );

            static const OptionSet& Options( );

        protected:
            virtual void evalClient( );

            virtual void evalServer( );

            virtual void evalStandalone( );

        private:
            static OptionSet& m_options;

            CommandLineOptions m_args;

            static OptionSet& init_options( );
            std::string       filename;
        };

        //-------------------------------------------------------------------
        /// \brief Retrieve information from a running server
        ///
        /// This interface allows for the querying a live diskcache
        /// daemon process for certain pieces of information
        //-------------------------------------------------------------------
        class Status : public ClientServerInterface
        {
        public:
            //-----------------------------------------------------------------
            /// \brief Constructor
            ///
            /// Construct a new instance of the object
            //-----------------------------------------------------------------
            Status( CommandLineOptions&                      Args,
                    const ClientServerInterface::ServerInfo& Server );

            //-----------------------------------------------------------------
            /// \brief
            //-----------------------------------------------------------------
            static const OptionSet& Options( );

        protected:
            virtual void evalClient( );

            virtual void evalServer( );

            virtual void evalStandalone( );

        private:
            enum
            {
                OPT_DEVICES,
                OPT_HOTDIRS,
                /// \brief Request list of offline mountpoints
                OPT_OFFLINE,
                OPT_THREADS,
                OPT_VARIABLE,
                OPT_SIZE
            };
            typedef std::bitset< OPT_SIZE >  tasks_type;
            typedef std::list< std::string > variables_type;

            static OptionSet& options;

            CommandLineOptions args;
            tasks_type         tasks;
            variables_type     variables;

            static OptionSet& init_options( );
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        class Scan
        {
        public:
            Scan( CommandLineOptions& Args,
                  const std::string&  DefaultConfigurationFilename =
                      std::string( "" ) );

            void operator( )( );

            static const OptionSet& Options( );

        private:
            enum
            {
                OPT_CONCURRENCY,
                OPT_CONFIGURATION_FILE,
                OPT_EXTENSIONS,
                OPT_MOUNT_POINTS,
                OPT_OUTPUT_ASCII,
                OPT_OUTPUT_BINARY,
                OPT_RWLOCK_INTERVAL,
                OPT_TYPE,
                OPT_VERSION_ASCII,
                OPT_VERSION_BINARY
            };

            static OptionSet& m_options;

            CommandLineOptions m_args;
            std::string        configuration_filename;
            Variables          variables;

            static OptionSet& init_options( );
        };

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        inline CommandTable::command_type
        CommandTable::Lookup( const std::string& Name )
        {
            return Instance( ).lookup( Name );
        }

        inline const char*
        CommandTable::Lookup( command_type Command )
        {
            return Instance( ).lookup( Command );
        }

        inline CommandTable::command_type
        CommandTable::lookup( const std::string& Name )
        {
            command_table_type::const_iterator pos(
                command_table.find( Name ) );

            //-----------------------------------------------------------------
            // Check if the command exists in the command table
            //-----------------------------------------------------------------
            if ( pos != command_table.end( ) )
            {
                //---------------------------------------------------------------
                // It does; return the appropriate enumuration.
                //---------------------------------------------------------------
                return pos->second;
            }
            //-----------------------------------------------------------------
            // It does not so return unknown command.
            //-----------------------------------------------------------------
            return CMD_UNKNOWN;
        }

        inline const char*
        CommandTable::lookup( command_type Command )
        {
            static const char* not_found = "";

            command_table_type::const_iterator last( command_table.end( ) );
            for ( command_table_type::const_iterator cur(
                      command_table.begin( ) );
                  cur != last;
                  ++cur )
            {
                if ( cur->second == Command )
                {
                    return cur->first.c_str( );
                }
            }
            return not_found;
        }

        //===================================================================
        //===================================================================
        inline const FilenamesRDS::query_results_type&
        FilenamesRDS::Results( ) const
        {
            return results;
        }

    } // namespace MetaCommand
} // namespace diskCache

#endif /* DISKCACHE_API__META_COMMANDS_HH */
