//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__STREAMS__FSTREAM_HH
#define DISKCACHE__STREAMS__FSTREAM_HH

// General Header Files
#include "ldastoolsal/fstream.hh"

namespace diskCache
{
    namespace Streams
    {
        //-------------------------------------------------------------------
        /// \brief File stream
        ///
        /// This handles a file stream.
        /// As part of handling an output file stream,
        /// this class creates a temporary file.
        /// If there is no issues with the file,
        /// then the temporary file will be renamed.
        //-------------------------------------------------------------------
        class IFStream : private ::LDASTools::AL::ifstream
        {
        public:
            typedef ::LDASTools::AL::ifstream stream_type;

            //-----------------------------------------------------------------
            /// \brief Constructor
            ///
            /// \param[in] Filename
            ///     The source filename.
            //-----------------------------------------------------------------
            IFStream( const std::string& Filename );

        private:
            std::string m_filename;
        };

        //-------------------------------------------------------------------
        /// \brief File stream
        ///
        /// This handles a file stream.
        /// As part of handling an output file stream,
        /// this class creates a temporary file.
        /// If there is no issues with the file,
        /// then the temporary file will be renamed.
        //-------------------------------------------------------------------
        class OFStream : public ::LDASTools::AL::ofstream
        {
        public:
            typedef ::LDASTools::AL::ofstream stream_type;

            //-----------------------------------------------------------------
            /// \brief Constructor
            ///
            /// \param[in] Filename
            ///     The destination filename.
            //-----------------------------------------------------------------
            OFStream( const std::string& Filename );

            //-----------------------------------------------------------------
            /// \brief Destructor
            //-----------------------------------------------------------------
            ~OFStream( );

        private:
            //-----------------------------------------------------------------
            /// \brief Destination filename
            ///
            /// This is the name of the file.
            /// It is used as the base for the temporary name of the file.
            //-----------------------------------------------------------------
            std::string m_filename;
            //-----------------------------------------------------------------
            /// \brief Temporary filename
            ///
            /// This is the name of the temporary file.
            /// This makes the file creation atomic as the file
            /// is not available till after all the data is written
            /// without encountering an error.
            //-----------------------------------------------------------------
            std::string m_filename_tmp;
        };

    } // namespace Streams
} // namespace diskCache

#endif /* DISKCACHE__STREAMS__FSTREAM_HH */
