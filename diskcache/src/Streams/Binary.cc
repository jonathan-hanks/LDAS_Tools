//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#if HAVE_CONFIG_H
#include <diskcache_config.h>
#endif /* HAVE_CONFIG_H */

#include <list>
#include <string>

#include "ldastoolsal/types.hh"

#include "Binary.hh"

typedef INT_4U swap_type;

static const std::string TAG( "<DiSKCaCHeVerSIONed>" );
static const std::string TAG_OLD( "<DiSKCaCHe>" );
static const swap_type   SWAP_REFERENCE = 0x12345678;

namespace diskCache
{
    namespace Streams
    {
        //===================================================================
        // IBinary
        //===================================================================
        IBinary::reader_container_type IBinary::m_readers;

        IBinary::IBinary( std::istream& Source )
            : IInterface( Source ), m_swap( false )
        {
            //-----------------------------------------------------------------
            // Setup to throw an exception once the end of file is reached.
            //-----------------------------------------------------------------
            m_stream.exceptions( m_stream.exceptions( ) |
                                 std::istream::eofbit );
            //-----------------------------------------------------------------
            // Determine the version of the frame file
            //-----------------------------------------------------------------
            char buffer[ 256 ];
            std::fill( &buffer[ 0 ], &buffer[ 256 ], '\0' );
            if ( !m_stream.read( buffer, TAG_OLD.size( ) ) )
            {
                throw BadStream( );
            }
            buffer[ TAG_OLD.length( ) ] = '\0';
            //-----------------------------------------------------------------
            // Check to see if the buffer is of the old type
            //-----------------------------------------------------------------
            if ( TAG_OLD.compare( buffer ) == 0 )
            {
                version( VERSION_NONE );
                return;
            }
            //-----------------------------------------------------------------
            // Read a few more bytes to check for newer version
            //-----------------------------------------------------------------
            if ( !m_stream.read( &buffer[ TAG_OLD.size( ) ],
                                 TAG.size( ) - TAG_OLD.size( ) ) )
            {
                throw BadStream( );
            }
            buffer[ TAG.length( ) ] = '\0';
            if ( TAG.compare( buffer ) != 0 )
            {
                throw BadStream( );
            }
            //-----------------------------------------------------------------
            // Newer stream format with header information
            //-----------------------------------------------------------------
            swap_type    swap;
            version_type v;

            m_stream.read( reinterpret_cast< char* >( &swap ), sizeof( swap ) );
            if ( swap != SWAP_REFERENCE )
            {
                reverse< sizeof( swap ) >( &swap, 1 );
                if ( swap != SWAP_REFERENCE )
                {
                    throw SwapError( );
                }
                m_swap = true;
            }
            //-----------------------------------------------------------------
            // Read the stream version number
            //-----------------------------------------------------------------
            v = 0;
            *this >> v;
            version( v );
        } // namespace Streams

        IBinary::~IBinary( )
        {
        }

        //===================================================================
        // OBinary
        //===================================================================
        OBinary::writer_container_type OBinary::m_writers;

        OBinary::OBinary( std::ostream& Sink, version_type Version )
            : OInterface( Version ), m_stream( Sink )
        {
            //-----------------------------------------------------------------
            // Put the header into the file
            //-----------------------------------------------------------------
            if ( Version < VERSION_HEADER )
            {
                //---------------------------------------------------------------
                // Tag
                //---------------------------------------------------------------
                m_stream.write( TAG_OLD.c_str( ), TAG_OLD.size( ) );
            }
            else
            {
                //---------------------------------------------------------------
                // Tag
                //---------------------------------------------------------------
                m_stream.write( TAG.c_str( ), TAG.size( ) );
                //---------------------------------------------------------------
                // Basic header
                //---------------------------------------------------------------
                *this << SWAP_REFERENCE // Label the endianness
                      << Version // Record the version number
                    ;
            }

        } // OBinary::OBinary

        OBinary::~OBinary( )
        {
        }

        // template OBinary& OBinary::operator<< <std::string,std::list>( const
        // std::list< std::string >& Data );
    } // namespace Streams
} // namespace diskCache
