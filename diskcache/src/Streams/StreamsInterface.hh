//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__STREAMS__STREAMS_INTERFACE_HH
#define DISKCACHE__STREAMS__STREAMS_INTERFACE_HH

#include <iostream>
#include <stdexcept>

#include "ldastoolsal/types.hh"

namespace diskCache
{
    namespace Streams
    {
        //###################################################################
        ///
        //###################################################################
        class BadStream : public std::runtime_error
        {
        public:
            BadStream( );
        };

        inline BadStream::BadStream( )
            : std::runtime_error( "Unable to create stream" )
        {
        }

        //###################################################################
        /// \brief This class is the base class for objects to be streamed.
        //###################################################################
        class Streamable
        {
        public:
            typedef INT_4U size_type;

            virtual ~Streamable( );

            virtual size_type Count( ) const = 0;
        };

        inline Streamable::~Streamable( )
        {
        }

        //###################################################################
        /// \brief Interface for core stream.
        //###################################################################
        class Interface
        {
        public:
            enum version_tag_type
            {
                //---------------------------------------------------------------
                /// \brief Version before the header was introduced
                //---------------------------------------------------------------
                VERSION_NONE = 0x0000,
                //---------------------------------------------------------------
                /// \brief Version reserved for disk cache development
                ///
                /// Production systems and programs should never try to
                /// interpret or generate streams of this version.
                /// It is reserved for development purposes when the cache
                /// stream is being extended.
                //---------------------------------------------------------------
                VERSION_PROTOTYPE = 0xFFFF,
                //---------------------------------------------------------------
                /// \brief version without the header information
                ///
                /// This version number is hear to create versions of the
                /// cache file without versioning headers.
                //---------------------------------------------------------------
                VERSION_PRE_HEADER = 0x00FF,
                //---------------------------------------------------------------
                /// \brief First version with the header information
                ///
                /// This is when the header first appeared in the cache file.
                ///
                /// All subsequent version numbers must be greater than this
                /// which means that 0x0001 - 0x00FF are all invalid values.
                //---------------------------------------------------------------
                VERSION_HEADER = 0x0100,
                //---------------------------------------------------------------
                /// \brief Introduction of multiple file extensions
                ///
                /// Multiple file extensions allows for indexing of files
                /// in the S-D-G-T form that do not end in .gwf
                ///
                /// \note
                ///    This mode is still under development and should not
                ///    be used.
                //---------------------------------------------------------------
                VERSION_MULTIPLE_EXTENSIONS = 0x0101,
                //---------------------------------------------------------------
                /// \brief Introduction of online status
                ///
                /// File contains online status field.
                ///
                /// \note
                ///    This mode is still under development and should not
                ///    be used.
                //---------------------------------------------------------------
                VERSION_ONLINE_STATUS = 0x0102,
                //---------------------------------------------------------------
                /// \brief Introduction of generic indexes
                ///
                /// Generic indexes allow for caching more than files of the
                /// type S-D-G-T.gwf
                ///
                /// \note
                ///    This mode is still under development and should not
                ///    be used.
                //---------------------------------------------------------------
                VERSION_GENERIC_INDEXES = 0x0103
            };

            typedef INT_2U version_type;

            virtual ~Interface( );

            version_type Version( ) const;

        protected:
            void version( version_type Version );

        private:
            //-----------------------------------------------------------------
            /// \brief Current version
            ///
            /// This represents the current version of the stream type.
            /// It is used by both input and output streams.
            //-----------------------------------------------------------------
            version_type m_version;
        }; // class - Interface

        inline Interface::~Interface( )
        {
        }

        inline Interface::version_type
        Interface::Version( ) const
        {
            return m_version;
        }

        inline void
        Interface::version( version_type Version )
        {
            m_version = Version;
        }

        //###################################################################
        /// \brief Interface for input streams
        //###################################################################
        class IInterface : public Interface
        {
        public:
            IInterface( std::istream& Source );

            void clear( std::ios::iostate = std::ios::goodbit );

            bool eof( ) const;

        protected:
            std::istream& m_stream;
        };

        inline IInterface::IInterface( std::istream& Source )
            : m_stream( Source )
        {
        }

        inline void
        IInterface::clear( std::ios::iostate State )
        {
            m_stream.clear( State );
        }

        inline bool
        IInterface::eof( ) const
        {
            return m_stream.eof( );
        }

        //###################################################################
        /// \brief Interface for output streams
        //###################################################################
        class OInterface : public Interface
        {
        public:
            OInterface( version_type Version );
        };

        inline OInterface::OInterface( version_type Version )
        {
            version( Version );
        }

    } // namespace Streams
} // namespace diskCache
#endif /* DISKCACHE__STREAMS__STREAMS_INTERFACE_HH */
