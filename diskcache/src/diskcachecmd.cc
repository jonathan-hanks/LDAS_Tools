//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// System Header Files
#include <sys/time.h>
#include <sys/stat.h>
#include <time.h>

#include <iostream>
#include <iomanip>
#include <list>

#include "diskcache.hh"
#include "diskcachecmd.hh"

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/ConditionalVariable.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/ldasexception.hh"
#include "ldastoolsal/Task.hh"

// GenericAPI Header Files
#include "genericAPI/Logging.hh"
#include "genericAPI/Stat.hh"

// Local Header Files

#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/QueryParams.hh"
#include "diskcacheAPI/Cache/RegistrySingleton.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "AtomicFile.hh"
#include "Daemons.hh"
#include "DirectoryManagerSingleton.hh"
#include "MountPointScanner.hh"

using std::ios_base;
using std::istringstream;
using std::ostringstream;
using std::runtime_error;
using std::string;

using LDASTools::AL::MemChecker;
using LDASTools::AL::MutexLock;

using namespace diskCache;
using GenericAPI::LogEntryGroup_type;
using GenericAPI::queueLogEntry;
using LDASTools::AL::ifstream;
using LDASTools::AL::ofstream;

using diskCache::Cache::ExcludedDirectoriesSingleton;

namespace
{
    typedef diskCache::MountPointScanner::mount_point_container_type
        mount_point_container_type;

    //--------------------------------------------------------------------
    /// \brief Debug utility to track down execution time.
    //--------------------------------------------------------------------
    class Timer
    {
    public:
        Timer( );
        REAL_4 getDelta( const std::string& method );

    private:
        struct timeval mTv;
        INT_4S         mLastSec;
        INT_4S         mLastUsec;
    };

    Timer::Timer( )
    {
        ::gettimeofday( &mTv, NULL );
        mLastSec = mTv.tv_sec;
        mLastUsec = mTv.tv_usec;
    }

    REAL_4
    Timer::getDelta( const std::string& method )
    {
        ::gettimeofday( &mTv, NULL );

        REAL_4 delta( mTv.tv_sec - mLastSec +
                      ( mTv.tv_usec - mLastUsec ) * 1e-6 );

#ifdef DEBUG_TIME
        cout << "--->DEBUG_TIME: clock time {" << method << "}: " << delta
             << endl;

        mLastSec = mTv.tv_sec;
        mLastUsec = mTv.tv_usec;
#endif
        return delta;
    } // method - Timer::getDelta

#if OLD
    void
    query_mount_points( FrameQuery&        query,
                        const char*        ifo,
                        const char*        type,
                        const unsigned int start_time,
                        const unsigned int stop_time )
    {
#if WORKING
        // Get current MOUNT_PT list
        const vector< string > mount_pt_list(
            MountPointHash::getMountPtList( ) );

        // Fix for problem report #2349:
        // need to do a substring match for provided single character "ifo" -
        // match any ifo that contains that single character
        if ( strlen( ifo ) == 1 )
        {
            // Look up frame data under each MOUNT_PT entry
            for ( vector< string >::const_iterator
                      iter = mount_pt_list.begin( ),
                      end_iter = mount_pt_list.end( );
                  iter != end_iter;
                  ++iter )
            {
                // FIX PR 2390: new query method: pass start,stop into search
                if ( query( MountPointHash::getIFOTypeHashes(
                         *ifo, type, *iter, start_time, stop_time ) ) )
                {
                    break;
                }
            }
        }
        else
        {
            const string ifo_type( query.getIFOType( ) );

            // Look up frame data under each MOUNT_PT entry
            for ( vector< string >::const_iterator
                      iter = mount_pt_list.begin( ),
                      end_iter = mount_pt_list.end( );
                  iter != end_iter;
                  ++iter )
            {
                // FIX PR 2390: new query method: pass start,stop into search
                if ( query( MountPointHash::getIFOTypeHashes(
                         ifo_type, *iter, start_time, stop_time ) ) )
                {
                    break;
                }
            }
        }
#endif /* WORKING */
    } // function - query_mount_points
#endif /* OLD */

    inline void
    is_first_scan_complete( const char* const File, int Line )
    {
#if WORKING
        if ( diskCache::MountPointScanner::FirstScanComplete( ) == false )
        {
            std::ostringstream msg;

            msg << "The in memory database of the file systems is currently "
                   "unavailable"
                << " due to a request to do a full resync.";

            std::ostringstream info;
            info << "There are several reasons this may occur."
                 << " 1. When the diskcacheAPI is restarted."
                 << " 2. When a request has been made to rebuild the cache."
                 << " 3. When the list of mount points has been changed."
                 << " In all of these cases, the system must process each entry"
                 << " in the list of mount points once before resuming normal"
                 << " system processing.";
            throw LdasException( Library::DISKCACHEAPI,
                                 -1,
                                 msg.str( ),
                                 info.str( ),
                                 File,
                                 Line );
        }
#endif /* WORKING */
    }

    //====================================================================================
    //
    //====================================================================================
    std::string
    ms_format( const LDASTools::AL::GPSTime& DT )
    {
        std::ostringstream msg;

        INT_4U sec( DT.GetSeconds( ) );
        INT_4U hrs( sec / 3600 );
        sec %= 3600;
        INT_4U min( sec / 60 );
        sec %= 60;
        INT_4U ms = DT.GetNanoseconds( ) / 1000000;

        msg.fill( '0' );

        msg << std::setw( 2 ) << hrs << ":" << std::setw( 2 ) << min << ":"
            << std::setw( 2 ) << sec << "." << std::setw( 3 ) << ms;

        return msg.str( );
    }

} // namespace

//-----------------------------------------------------------------------
///
/// Get TCL formatted lists for each mount point with name or mount point,
/// number of directories and number of files for data matching the specified
/// ifo and type.  Return lists are of the form:
///   {mountpoint_name number_of_dirs number_of_files }
///
/// \param[out] Answer
///     Resulting hash numbers based on query parameters.
/// \param[in] Ifo
///     An ifo to look up. Default is "all".
/// \param[in] Type
///     A type to look up. Default is "all".
///
/// \return
///     A list for each mount point with name, number of
///     directories and number of files under that mount point:
///     {mountpoint_name number_of_dirs number_of files }
///
//-----------------------------------------------------------------------
void
getHashNumbers( diskCache::Cache::QueryAnswer& Answer,
                const char*                    Ifo,
                const char*                    Type )
{
    is_first_scan_complete( __FILE__, __LINE__ );

    // Debug timer
    Timer tracker;

    /// \todo Need to get getHashNumber queries working
#if 0
   data = MountPointHash::getHashNumbers( ifo, type );
#endif /* 0 */

    tracker.getDelta( "getHashNumbers()" );
}

//-----------------------------------------------------------------------
/// \brief Get TCL formatted lists of intervals for data matching
///        the specified and Type within the bounds of the query.
///
/// \param[out] Answer
///     Matching intervals based on query parameters.
/// \param[in] ifo_type_str
///     A space delimited list of IFO-Type strings
/// \param[in] query_start
///     Query start time.
/// \param[in] query_stop
///     Query stop time.
/// \param[in] Extension
///     Filename extension
///
/// \return
///     A list for each IFO-Type with data intervals:
///     IFO-Type1 { i1_start i1_stop ... iN_start iN_stop }
///     ... IFO-TypeN { ... }
//-----------------------------------------------------------------------
void
getIntervalsList( diskCache::Cache::QueryAnswer& Answer,
                  const char*                    ifo_type_str,
                  const INT_4U                   query_start,
                  const INT_4U                   query_stop,
                  const std::string&             Extension )
{
    is_first_scan_complete( __FILE__, __LINE__ );

    Timer tracker;

    // IFO Type delimiter
    static const char dash( '-' );

    // Copy and cast the passed in character array into a string object
    string raw_string( ifo_type_str );

    // string to fill with data and return
    string record( "" );

    // string buffer
    string buffer;

    // Insert the raw string into a stream to easily tokenize on whitespace
    istringstream ss( raw_string );

    std::ostringstream start_string;
    std::ostringstream stop_string;

    start_string << query_start;
    stop_string << query_stop;

    // Tokenize the string on whitespace
    while ( ss >> buffer )
    {
        // Strings to fill from tokenizing the raw_string
        string ifo( "" );
        string type( "" );

        // locate dash in IFO-Type string
        string::size_type pos = buffer.find( dash, 0 );

        // Extract IFO and Type component sub-strings
        ifo = buffer.substr( 0, pos );
        type = buffer.substr( pos + 1 );

        Cache::QueryParams q;

        q.AddParam( "index", Cache::SDGTx::AsciiId );

        q.AddParam( "query_variety", "interval" );
        q.AddParam( "site", ifo );
        q.AddParam( "description", type );
        q.AddParam( "start", start_string.str( ) );
        q.AddParam( "stop", stop_string.str( ) );

        try
        {
            Cache::RegistrySingleton::TranslateQuery( q, Answer );
            MountPointManagerSingleton::Find( Answer );
        }
        catch ( const Cache::QueryParams::MissingVariableError& Error )
        {
        }
    }

    tracker.getDelta( "getIntervalsList()" );
}

//----------------------------------------------------------------------------
//
//: Write content of global frame data hash to binary file.
//
//! param: const char* filename - Name of the file to write frame hash to.
//+       Default is NULL (C++ will use default file name).
//
//! return: Nothing.
//
void
writeDirCache( const char*                                 filename,
               diskCache::Streams::Interface::version_type Version )
{
    static const char* caller = "::writeDirCache";

    MountPointScanner::scanner_sync_ro_type scanner_sync_lock(
        MountPointScanner::SyncRO( ) );

    // Debug timer
    Timer tracker;

    {
        diskCache::AtomicFile< ofstream, diskCache::Streams::OBinary > file(
            filename );

        file.Write( ( Version == diskCache::Streams::Interface::VERSION_NONE )
                        ? ( diskCache::Streams::OBinary::VERSION_DEFAULT )
                        : Version );
    }

    REAL_4 t = tracker.getDelta( "writeDirCache()" );

    QUEUE_LOG_MESSAGE( "Wrote Binary Cache File: "
                           << filename << " in " << int( t ) << "."
                           << std::setw( 6 ) << std::setfill( '0' )
                           << ( int( ( t - int( t ) ) * 10e6 ) ) << " seconds",
                       MT_NOTE,
                       0,
                       caller,
                       "CXX" );
    return;
}

//----------------------------------------------------------------------------
//
//: Write content of global frame data hash to ascii file.
//
//! param: const char* filename - Name of the file to write frame hash to.
//+       Default is NULL (C++ will use default file name).
//
//! return: Nothing.
//
void
writeDirCacheAscii( const char*                                 filename,
                    diskCache::Streams::Interface::version_type Version )
{
    static const char* caller = "::writeDirCacheAscii";

    static MutexLock::baton_type key;
    static const CHAR* const     DefaultFileName( "frame_cache_dump2" );

    Timer tracker;

    MountPointScanner::scanner_sync_ro_type scanner_sync_lock(
        MountPointScanner::SyncRO( ) );

    MutexLock baton( key, __FILE__, __LINE__ );

    {
        diskCache::AtomicFile< ofstream, diskCache::Streams::OASCII > file(
            ( filename && *filename ) ? filename : DefaultFileName );

        file.Write( ( Version == diskCache::Streams::Interface::VERSION_NONE )
                        ? ( diskCache::Streams::OASCII::VERSION_DEFAULT )
                        : Version );
    }
    REAL_4 t = tracker.getDelta( "writeDirCacheAscii()" );

    QUEUE_LOG_MESSAGE( "Wrote ASCII Cache File: "
                           << filename << " in " << int( t ) << "."
                           << std::setw( 6 ) << std::setfill( '0' )
                           << ( int( ( t - int( t ) ) * 10e6 ) ) << " seconds",
                       MT_NOTE,
                       0,
                       caller,
                       "CXX" );
    return;
}

//-----------------------------------------------------------------------
///
/// \brief Write content of global frame data hash to ascii file.
///
/// \param[in] bfilename
///     Name of the file to write the binary frame hash to.
///     Default is NULL (C++ will use default file name).
///
/// \param[in] afilename
///     Name of the file to write the ascii frame hash to.
///     Default is NULL (C++ will use default file name).
///
//-----------------------------------------------------------------------
void
writeDirCacheFiles( const char* bfilename, const char* afilename )
{
    // Debug timer
    Timer tracker;

    writeDirCache( bfilename );
    writeDirCacheAscii( afilename );

    tracker.getDelta( "writeDirCacheFiles()" );

    return;
}

//------------------------------------------------------------------------------
//
//: Read content of global frame data hash from binary file.
//
// Tcl layer can specify different files for read and write operations.
// This function forces all read and write operations to be sequencial.
//
// ATTN: This function destroys existing hash before reading a new one from the
//       given file. Caller must assure there are no running threads that might
//       access global frame data hash at the time "readDirCache" is called.
//
//! param: const char* filename - Name of the file to read frame hash from.
//+       Default is NULL (C++ will use default file name).
//
//! return: Nothing.
//
void
CacheRead( const char* filename )
{
    // Debug timer
    Timer tracker;

    //----------------------------------------------------------------------------
    // Enscure serializatioin of reads and writes
    //----------------------------------------------------------------------------
    {
        DumpCacheDaemon::io_lock_type lock(
            DumpCacheDaemon::IOLock( ), __FILE__, __LINE__ );

        diskCache::AtomicFile< ifstream, diskCache::Streams::IBinary > file(
            filename );

        file.Read( );
    }

    tracker.getDelta( "CacheRead()" );

    return;
}

//----------------------------------------------------------------------------
//
//: Update list of excluded directories (as they appear in
//: the resource file): check if existing data hash relies on such directories
//:                     already, remove those directories recursively from
//:                     global hash.
//
//! usage: set dir_list [ excludedDirList dirs_to_exclude ]
//
//! param: const CHAR* dir_list - A list of directories to exclude (as they
//+                              appear in API resource file variable).
//
//! return: string - Sorted Tcl list of all removed subdirectories, followed
//+        by error messages if any:
//+        {Directories excluded by pattern 'dir_list': dir1 dir2 ...}
//+        {error1 error2 ...}
//
string
excludedDirList( const ExcludedDirectoriesSingleton::directory_container_type&
                     DirectoryList )
{
    // Debug timer
    Timer tracker;

    // Seed with list of directories to exclude
    const string result;

    ExcludedDirectoriesSingleton::Update( DirectoryList );

    tracker.getDelta( "excludedDirList()" );

    return result;
}

//
//: Delete global frame data hash.
//
// ATTN: This function destroys existing hash in memory!
//       Caller must assure there are no running threads that might
//       access global frame data hash at the time "deleteDirCache" is called.
//
//! return: Nothing.
//
void
deleteDirCache( )
{
    // Debug timer
    Timer tracker;

    MountPointManagerSingleton::Reset(
        MountPointManagerSingleton::RESET_CACHE );

    tracker.getDelta( "deleteDirCache()" );

    return;
}

//-----------------------------------------------------------------------
/// This routine does a single pass of the listed mount points.
//-----------------------------------------------------------------------
void
ScanConcurrency( INT_4U Concurrency )
{
    MountPointScanner::Concurrency( Concurrency );
}

//-----------------------------------------------------------------------
/// This routine does a single pass of the listed mount points.
//-----------------------------------------------------------------------
void
ScanMountPointList( MountPointScanner::ScanResults& Answer )
{
    static const char caller[] = "ScanMountPointList";
    //--------------------------------------------------------------------
    /// Obtain the list of mount points.
    //--------------------------------------------------------------------
    LDASTools::AL::GPSTime start;
    start.Now( );

    //--------------------------------------------------------------------
    // Loop over the mount points
    //--------------------------------------------------------------------
    MountPointScanner::mount_point_container_type mount_points(
        diskCache::MountPointManagerSingleton::MountPoints( ).Var( ) );
    {
        std::ostringstream msg;

        msg << "Entry: "
            << " Start the scan of mount points: " << mount_points.size( );
        GenericAPI::queueLogEntry( msg.str( ),
                                   GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                   30,
                                   caller,
                                   "CXX" );
    }

    MountPointScanner::Scan( mount_points, Answer );

    LDASTools::AL::GPSTime end;
    end.Now( );
    std::ostringstream     msg;
    LDASTools::AL::GPSTime dt( LDASTools::AL::GPSTime( ) + ( end - start ) );
    INT_4U                 ms = dt.GetNanoseconds( ) / 1000000;
    ms += dt.GetSeconds( ) * 1000;

    //--------------------------------------------------------------------
    /// The output needs to be in the following format
    /// 7 mount points, 0 directories, 0 files, scanned in 3010 ms
    /// (00:00:03.0010).
    //--------------------------------------------------------------------
    msg
        //-----------------------------------------------------------------
        /// \todo
        ///   Complete the population of the status message
        //-----------------------------------------------------------------
        << Answer.MountPointCount( ) << " mount points, "
        << Answer.DirectoryCount( ) << " directories, " << Answer.FileCount( )
        << " files,"
        << " scanned in " << ms << " ms "
        << "(" << ms_format( dt ) << ").";
    if ( MemChecker::IsExiting( ) == false )
    {
        GenericAPI::queueLogEntry( msg.str( ),
                                   GenericAPI::LogEntryGroup_type::MT_NOTE,
                                   0,
                                   caller,
                                   "SCAN_MOUNTPT" );
    }
}

//-----------------------------------------------------------------------
/// Create an image of the cache file on the storage media.
//----------------------------------------------------------------------
void
DumpCacheDaemonStart( )
{
    diskCache::Daemon::DumpCache::Launch( );
}

void
DumpCacheDaemonStop( )
{
    diskCache::Daemon::DumpCache::Terminate( );
}

//-----------------------------------------------------------------------
/// Scan the list of mount points in the background
//-----------------------------------------------------------------------
void
ScanMountPointListContinuously( )
{
    diskCache::Daemon::ScanMountPoints::Launch( );
}

void
ScanMountPointListContinuouslyStop( )
{
    diskCache::Daemon::ScanMountPoints::Terminate( );
}

//-----------------------------------------------------------------------
/// This function iterates over the list of mount points until they are
///   all available or until the maximum number of retries is reached.
//-----------------------------------------------------------------------
void
WaitOnMountPointsAvailable( )
{
    MountPointScanner::mount_point_container_type mount_points(
        diskCache::MountPointManagerSingleton::MountPoints( ).Var( ) );

    static const int MAX_RETRY_COUNT = 5;
    struct stat      stat_buf;
    int              retry_count = 0;
    if ( retry_count < MAX_RETRY_COUNT )
    {
        int counter = 0;

        for ( auto mount_point : mount_points )
        {
            try
            {
                GenericAPI::Stat( mount_point, stat_buf );
            }
            catch ( ... )
            {
                ++counter;
            }
        }
        if ( counter == 0 )
        {
            return;
        }
        ++retry_count;
    }
    throw( std::runtime_error( "Mount points not available" ) );
}

// END
