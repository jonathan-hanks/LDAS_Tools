//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2022 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <unordered_set>

#include "genericAPI/Logging.hh"
#include "genericAPI/LDASplatform.hh"

#include "diskcacheAPI/Common/Variables.hh"

#include "diskcacheAPI/Cache/HotDirectory.hh"

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "Cache/Devices.hh"

#include "Commands.hh"
#include "diskcachecmd.hh"
#include "IO.hh"
#include "MetaCommands.hh"
#include "MountPointScanner.hh"
#include "Variables.hh"

using diskCache::Commands::updateFileExtList;
using diskCache::Commands::updateMountPtList;

namespace diskCache
{
    namespace MetaCommand
    {
        //===================================================================
        // Scan
        //===================================================================
        OptionSet& Scan::m_options( Scan::init_options( ) );

        OptionSet&
        Scan::init_options( )
        {
            static OptionSet retval;

            retval.Synopsis( "Subcommand: scan" );

            retval.Summary( "The scan sub command is intended to"
                            " scan a set of directories for files of interest"
                            " and generate a memory cache image." );

            retval.Add( Option( OPT_CONCURRENCY,
                                "concurrency",
                                Option::ARG_REQUIRED,
                                "Number of mount points to scan concurrently",
                                "integer" ) );

            retval.Add( Option(
                OPT_CONFIGURATION_FILE,
                "configuration-file",
                Option::ARG_REQUIRED,
                "Name of file containing additional configuration information.",
                "filename" ) );

            retval.Add( Option( OPT_EXTENSIONS,
                                "extensions",
                                Option::ARG_REQUIRED,
                                "Comma seperated list of file extensions",
                                "list" ) );

            retval.Add( Option( OPT_MOUNT_POINTS,
                                "mount-points",
                                Option::ARG_REQUIRED,
                                "Comma seperated list of mount points to scan",
                                "list" ) );

            retval.Add( Option( OPT_OUTPUT_ASCII,
                                "output-ascii",
                                Option::ARG_REQUIRED,
                                "Filename for the ascii output; '-' to direct "
                                "to standard output",
                                "filename" ) );

            retval.Add( Option( OPT_OUTPUT_BINARY,
                                "output-binary",
                                Option::ARG_REQUIRED,
                                "Filename for the binary output",
                                "filename" ) );

            retval.Add(
                Option( OPT_TYPE,
                        "type",
                        Option::ARG_REQUIRED,
                        "Type pattern to use for search. (Default: all)",
                        "pattern" ) );

            retval.Add(
                Option( OPT_VERSION_ASCII,
                        "version-ascii",
                        Option::ARG_REQUIRED,
                        "Version of the ascii diskcache dump format to output",
                        "version" ) );
            retval.Add(
                Option( OPT_VERSION_BINARY,
                        "version-binary",
                        Option::ARG_REQUIRED,
                        "Version of the binary diskcache dump format to output",
                        "version" ) );
            return retval;
        }

        Scan::Scan( CommandLineOptions& Args,
                    const std::string&  DefaultConfigurationFilename )
            : m_args( Args )
        {
            Variables        variables;
            static const int PROCESS_VARIABLES = //
                Variables::VAR_CACHE_WRITE_DELAY | //
                Variables::VAR_CONCURRENCY | //
                Variables::VAR_DIRECTORY_TIMEOUT | //
                Variables::VAR_EXCLUDED_DIRECTORIES | //
                Variables::VAR_EXCLUDED_PATTERNS | //
                Variables::VAR_EXTENSIONS | //
                Variables::VAR_MOUNT_POINTS | //
                Variables::VAR_OUTPUT_ASCII | //
                Variables::VAR_OUTPUT_BINARY | //
                Variables::VAR_RWLOCK_INTERVAL | //
                Variables::VAR_RWLOCK_TIMEOUT | //
                Variables::VAR_STAT_TIMEOUT | //
                Variables::VAR_VERSION_ASCII | //
                Variables::VAR_VERSION_BINARY | //
                0 //
                ;

            {
                configuration_filename = DefaultConfigurationFilename;
                std::ifstream stream( configuration_filename.c_str( ) );

                if ( stream.is_open( ) )
                {
                    variables.ParseConfigurationFile( stream );
                }
            }
            if ( m_args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );
                using diskCache::Common::Variables;

                while ( parsing )
                {
                    switch ( m_args.Parse( m_options, arg_name, arg_value ) )
                    {
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    case OPT_CONCURRENCY:
                        Variables::Cache(
                            diskCache::Variables::VAR_NAME_CONCURRENCY,
                            arg_value );
                        break;
                    case OPT_CONFIGURATION_FILE:
                    {
                        configuration_filename = arg_value;
                        std::ifstream stream( configuration_filename.c_str( ) );

                        if ( stream.is_open( ) )
                        {
                            variables.ParseConfigurationFile( stream );
                        }
                    }
                    break;
                    case OPT_EXTENSIONS:
                    {
                        //-----------------------------------------------------------
                        // Generate list of extensions
                        //-----------------------------------------------------------
                        size_t pos = 0;
                        size_t end = 0;

                        variables.reset_extensions( );
                        while ( end != std::string::npos )
                        {
                            end = arg_value.find_first_of( ",", pos );

                            variables.push_extension(
                                arg_value.substr( pos,
                                                  ( ( end == std::string::npos )
                                                        ? end
                                                        : end - pos ) ) );
                            pos = end + 1;
                        }
                    }
                    break;
                    case OPT_MOUNT_POINTS:
                    {
                        //-----------------------------------------------------------
                        // Generate list of mount points
                        //-----------------------------------------------------------
                        size_t pos = 0;
                        size_t end = 0;

                        variables.reset_mount_points( );
                        while ( end != std::string::npos )
                        {
                            end = arg_value.find_first_of( ",", pos );
                            variables.push_mount_point(
                                arg_value.substr( pos,
                                                  ( ( end == std::string::npos )
                                                        ? end
                                                        : end - pos ) ) );
                            pos = end + 1;
                        }
                    }
                    break;
                    case OPT_TYPE:
                        variables.set_type_pattern( arg_value );
                        break;
                    case OPT_OUTPUT_ASCII:
                        Variables::Cache(
                            diskCache::Variables::VAR_NAME_OUTPUT_ASCII,
                            arg_value );
                        break;
                    case OPT_OUTPUT_BINARY:
                        Variables::Cache(
                            diskCache::Variables::VAR_NAME_OUTPUT_BINARY,
                            arg_value );
                        break;
                    case OPT_VERSION_ASCII:
                        Variables::Cache(
                            diskCache::Variables::VAR_NAME_OUTPUT_ASCII_VERSION,
                            arg_value );
                        break;
                    case OPT_VERSION_BINARY:
                        Variables::Cache( diskCache::Variables::
                                              VAR_NAME_OUTPUT_BINARY_VERSION,
                                          arg_value );
                        break;
                    default:
                        break;
                    }
                }
            }
            QUEUE_LOG_MESSAGE( "About to setup variables",
                               MT_NOTE,
                               0,
                               "Scan::Constructor",
                               "CXX" );
            GenericAPI::SyncLog( );
            variables.Setup( PROCESS_VARIABLES );
            QUEUE_LOG_MESSAGE( "Finished setup of variables",
                               MT_NOTE,
                               0,
                               "Scan::Constructor",
                               "CXX" );
            GenericAPI::SyncLog( );
        }

        const OptionSet&
        Scan::Options( )
        {
            return m_options;
        }

        void
        Scan::operator( )( )
        {
            QUEUE_LOG_MESSAGE(
                "Begin operator()", MT_NOTE, 0, "Scan::operator()", "CXX" );
            GenericAPI::SyncLog( );
            // WaitOnMountPointsAvailable( );
            DumpCacheDaemonStart( );
            //-----------------------------------------------------------------
            // Scan the directories
            //-----------------------------------------------------------------
            {
                diskCache::MountPointScanner::ScanResults results;
                ScanMountPointList( results );

                /// \todo format results for output to stream
                /// diskCache::ASCII::Translate( &std::cout, results );
            }
            DumpCacheDaemonStop( );

            //-----------------------------------------------------------------
            // Write the scanned data - ASCII
            //-----------------------------------------------------------------
            {
                auto output_ascii = variables.GetOutputASCII( );

                if ( output_ascii.empty( ) ||
                     ( output_ascii.compare( "-" ) == 0 ) )
                {
                    //-------------------------------------------------------------
                    // Dump the default mount point manager
                    //-------------------------------------------------------------
                    diskCache::Streams::OASCII stream(
                        std::cout, variables.GetOutputASCIIVersion( ) );

                    diskCache::Write( stream );
                }
                else if ( output_ascii.empty( ) == false )
                {
                    //-------------------------------------------------------------
                    /// \todo
                    ///     When the output is ascii, the query options should
                    ///     allow the user to request a subset of the entire
                    ///     cache.
                    //-------------------------------------------------------------
                    //-------------------------------------------------------------
                    // Dump the default mount point manager
                    //-------------------------------------------------------------
                    diskCache::Streams::OFStream file_stream( output_ascii );
                    diskCache::Streams::OASCII   stream(
                        file_stream, variables.GetOutputASCIIVersion( ) );

                    diskCache::Write( stream );
                }
            }
            //-----------------------------------------------------------------
            // Write the scanned data - Binary
            //-----------------------------------------------------------------
            {
                auto output_binary = variables.GetOutputBinary( );

                if ( output_binary.empty( ) == false )
                {
                    //-------------------------------------------------------------
                    /// \todo
                    ///     When the output is binary, the query options should
                    ///     allow the user to request a subset of the entire
                    ///     cache.
                    //-------------------------------------------------------------
                    //-------------------------------------------------------------
                    // Dump the default mount point manager
                    //-------------------------------------------------------------
                    diskCache::Streams::OFStream file_stream( output_binary );
                    diskCache::Streams::OBinary  stream(
                        file_stream, variables.GetOutputBinaryVersion( ) );

                    diskCache::Write( stream );
                }
            }
        }

#if 0
        inline void
        Scan::push_excluded_directory( const std::string& Directory )
        {
            excluded_directories.push_back( Directory );
        }

        inline void
        Scan::push_excluded_pattern( const std::string& Pattern )
        {
            excluded_patterns.push_back( Pattern );
        }

        inline void
        Scan::push_extension( const std::string& Extension )
        {
            m_extensions.push_back( Extension );
        }

        inline void
        Scan::push_mount_point( const std::string& MountPoint )
        {
            m_mount_points.push_back( MountPoint );
        }

        inline void
        Scan::set_output_ascii( const std::string& Value )
        {
            m_output_ascii = Value;
        }

        inline void
        Scan::set_output_binary( const std::string& Value )
        {
            m_output_binary = Value;
        }

        inline void
        Scan::set_output_binary_version( const std::string& Value )
        {
            if ( Value.substr( 0, 2 ).compare( "0x" ) == 0 )
            {
                std::istringstream s( Value.substr( 2 ) );
                s >> std::hex >> m_version_binary;
            }
            else if ( Value[ 0 ] == '0' )
            {
                std::istringstream s( Value.substr( 1 ) );
                s >> std::oct >> m_version_binary;
            }
            else
            {
                std::istringstream s( Value );
                s >> std::hex >> m_version_binary;
            }
        }

        inline void
        Scan::reset_excluded_patterns( )
        {
            excluded_patterns.resize( 0 );
        }
#endif /* 0 */

    } /* namespace MetaCommand */
} /* namespace diskCache */
