//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__MOUNT_POINT_MANAGER_SINGLETON_HH
#define DISKCACHE__MOUNT_POINT_MANAGER_SINGLETON_HH

#include "ldastoolsal/ReadWriteSingleton.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/LogHTML.hh"

#include "MountPointManager.hh"

namespace diskCache
{
    namespace Cache
    {
        class QueryAnswer;
    } // namespace Cache

    //---------------------------------------------------------------------
    /// \brief Manages a collection of mount points.
    ///
    /// A collection of mount points is managed by this singleton class.
    /// Each mount point represents a starting point for data searches.
    /// The collection of mount points is order sensative in that
    /// directories that appear higher in the list will be prioritised
    /// when searching for data.
    //---------------------------------------------------------------------
    class MountPointManagerSingleton : private MountPointManager
    {
    public:
        DECLARE_READ_WRITE_SINGLETON( MountPointManagerSingleton );

    public:
        typedef MountPointManager::mount_point_name_container_type
                                                 mount_point_name_container_type;
        typedef MountPointManager::UpdateResults UpdateResults;
        typedef MountPointManager::ScanResults   ScanResults;
        typedef MountPointManager::mount_point_names_ro_type
            mount_point_names_ro_type;

        using MountPointManager::RESET_CACHE;
        using MountPointManager::RESET_MANAGED_DIRECTORIES;

        //-------------------------------------------------------------------
        /// \brief Locate files contained within the collection
        //-------------------------------------------------------------------
        static void Find( Cache::QueryAnswer& Answer );

        //-------------------------------------------------------------------
        /// \brief Retrieve list of registered mount points
        //-------------------------------------------------------------------
        static const mount_point_names_ro_type MountPoints( );

        //-------------------------------------------------------------------
        /// \brief Reset the requested information
        ///
        /// This allows for the purging of certain information so as to
        /// force a refreshing of the information.
        ///
        /// \param[in] Flag
        ///     This is an ORed flag indicating what should be cleaned
        //-------------------------------------------------------------------
        static void Reset( int Flag );

        //-------------------------------------------------------------------
        /// \brief Request the scanning of a single mount point
        ///
        /// \param[in] MountPoint
        ///     The name of the mount point to scan.
        /// \param[out] Results
        ///     Information gathered during the scan.
        //-------------------------------------------------------------------
        static void Scan( const std::string& MountPoint, ScanResults& Results );

        //-------------------------------------------------------------------
        /// \brief Update the collection of managed mount points
        ///
        /// \param[in] MountPoints
        ///     Ordered collection of mount points to be managed.
        /// \param[out] Results
        ///     Results.
        ///
        //-------------------------------------------------------------------
        static void Update( const mount_point_name_container_type& MountPoints,
                            UpdateResults&                         Results );

        //-------------------------------------------------------------------
        /// \brief Dump information to the stream.
        ///
        /// \param[in] Stream
        ///     Sink for the information.
        //-------------------------------------------------------------------
        template < typename StreamT >
        static StreamT& Read( StreamT& Stream );

        //-------------------------------------------------------------------
        /// \brief Dump information to the stream.
        ///
        /// \param[in] Stream
        ///     Sink for the information.
        //-------------------------------------------------------------------
        template < typename StreamT >
        static StreamT& Write( StreamT& Stream );
    };

    inline void
    MountPointManagerSingleton::Find( Cache::QueryAnswer& Answer )
    {
        return Instance( ).MountPointManager::Find( Answer );
    }

    inline const MountPointManagerSingleton::mount_point_names_ro_type
    MountPointManagerSingleton::MountPoints( )
    {
        return Instance( ).MountPointManager::MountPoints( );
    }

    inline void
    MountPointManagerSingleton::Reset( int Flag )
    {
        static const char method_name[] =
            "diskCache::MountPointManagerSingleton::Reset";

        {
            std::ostringstream msg;

            msg << "Entry/Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        Instance( ).MountPointManager::Reset( Flag );
    }

    inline void
    MountPointManagerSingleton::Scan( const std::string& MountPoint,
                                      ScanResults&       Results )
    {
        static const char method_name[] =
            "diskCache::MountPointManagerSingleton::Scan";

        {
            std::ostringstream msg;

            msg << "Entry/Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        Instance( ).MountPointManager::Scan( MountPoint, Results );
    }

    inline void
    MountPointManagerSingleton::Update(
        const mount_point_name_container_type& MountPoints,
        UpdateResults&                         Results )
    {
        static const char method_name[] =
            "diskCache::MountPointManagerSingleton::Update";

        {
            std::ostringstream msg;

            msg << "Entry/Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        Instance( ).MountPointManager::Update( MountPoints, Results );
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    template < typename StreamT >
    inline StreamT&
    MountPointManagerSingleton::Read( StreamT& Stream )
    {
        static const char method_name[] =
            "diskCache::MountPointManagerSingleton::Read";

        {
            std::ostringstream msg;

            msg << "Entry/Exit"
                << " Instance: " << &( Instance( ) );
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        return Instance( ).MountPointManager::Read( Stream );
    }

    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    template < typename StreamT >
    inline StreamT&
    MountPointManagerSingleton::Write( StreamT& Stream )
    {
        static const char method_name[] =
            "diskCache::MountPointManagerSingleton::Write";

        {
            std::ostringstream msg;

            msg << "Entry/Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        return Instance( ).MountPointManager::Write( Stream );
    }

} // namespace diskCache

#endif /* DISKCACHE__MOUNT_POINT_MANAGER_SINGLETON_HH */
