//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__COMMON_VARIABLES_HH
#define DISKCACHE_API__COMMON_VARIABLES_HH

#include <memory>
#include <sstream>

#include <boost/any.hpp>

#include "ldastoolsal/Singleton.hh"

namespace diskCache
{
    namespace Common
    {
        class Variables : public LDASTools::AL::Singleton< Variables >
        {
        public:
            typedef void ( *reader_ss )( std::ostringstream& Value );
            typedef void ( *reader_any )( boost::any& Value );
            typedef void ( *writer_str )( const std::string& Value );

            Variables( );
            ~Variables( );

            static void NULL_READER_ANY( boost::any& Value );

            static void Init( const std::string& Var,
                              reader_ss          RFunc,
                              writer_str         WFunc,
                              const std::string& Default );

            static void Init( const std::string& Var,
                              reader_ss          ReadFunc,
                              reader_any         ReaderAnyFunc,
                              writer_str         WriteFunc,
                              const std::string& Default );

            static boost::any Cache( const std::string& Var );

            static void Cache( const std::string& Var,
                               const std::string& Value );

            static void Cacheable( const std::string& Var, bool Value );

            static boost::any Get( const std::string& Var );

            static void Get( const std::string&  Var,
                             std::ostringstream& Value );

            static void Set( const std::string& Var );

            static void Set( const std::string& Var, const std::string& Value );

            static void SetReader( const std::string& Var, reader_ss Func );

            static void SetWriter( const std::string& Var, writer_str Func );

            static void Reset( );

        private:
            class pdata_storage_type;
            typedef std::unique_ptr< pdata_storage_type > pdata_type;

            pdata_type pdata;
        };
    } // namespace Common
} // namespace diskCache

#endif /* DISKCACHE_API__COMMON_VARIABLES_HH */
