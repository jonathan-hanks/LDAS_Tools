//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- c-basic-offset: 2; indent-tabs-mode: nil; -*- */

#include <diskcache_config.h>

#include <fstream>
#include <iomanip>
#include <sstream>
#include <stdexcept>

#include "diskcacheAPI/Common/Variables.hh"

#include "diskcacheAPI/Cache/HotDirectory.hh"

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "Cache/Devices.hh"

#include "IO.hh"
#include "MetaCommands.hh"
#include "MountPointScanner.hh"

using LDASTools::AL::CommandLineOptions;

typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;
using diskCache::MetaCommand::transfer_helper;

namespace
{
    class transfer : public transfer_helper
    {
    public:
        std::string msg;

        //-------------------------------------------------------------------
        /// \brief Default constructor
        //-------------------------------------------------------------------
        transfer( );

        //-------------------------------------------------------------------
        /// \brief read the reponce from the stream
        ///
        /// \param[in] Stream
        ///     The output stream from which to read the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream from which the responce was read.
        //-------------------------------------------------------------------
        std::istream& read( std::istream& Stream );

        //-------------------------------------------------------------------
        /// \brief write the reponce onto the stream
        ///
        /// \param[in] Stream
        ///     The output stream on which to write the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream on which the responce was written.
        //-------------------------------------------------------------------
        std::ostream& write( std::ostream& Stream );
    };

    class deviceScraper : public diskCache::Cache::Devices::UnaryFunction
    {
    public:
        typedef diskCache::Cache::Devices::UnaryFunction::element_type
                       element_type;
        typedef INT_4U flag_type;

        static const flag_type FLAG_REFERENCED = 0x00000001;
        static const flag_type FLAG_ONLINE = 0x00000002;
        static const flag_type FLAG_OFFLINE = 0x00000004;
        static const flag_type FLAG_UNREFERENCED = 0x00000008;

        static const flag_type FLAGS_DEFAULT =
            ( FLAG_REFERENCED | FLAG_UNREFERENCED | FLAG_ONLINE |
              FLAG_OFFLINE );

        deviceScraper( flag_type Flag = FLAGS_DEFAULT ) : flags( Flag )
        {
        }

        virtual void
        operator( )( element_type Source )
        {
            if ( Source )
            {
                if ( ( ( ( flags & FLAG_REFERENCED ) && Source->Used( ) ) ||
                       ( ( flags & FLAG_UNREFERENCED ) &&
                         !Source->Used( ) ) ) &&
                     ( ( ( flags & FLAG_OFFLINE ) && Source->IsOffline( ) ) ||
                       ( ( flags & FLAG_ONLINE ) && !Source->IsOffline( ) ) ) )
                {
                    retval << "DEVICE: Path: " << Source->Path( )
                           << " Id: " << Source->Id( )
                           << " State: " << Source->StateStr( )
                           << " Referenced: " << Source->Used( ) << std::endl;
                }
            }
        }

        inline std::string
        str( ) const
        {
            return retval.str( );
        }

    private:
        flag_type          flags;
        std::ostringstream retval;
    };

    class hotdirScraper : public diskCache::Cache::HotDirectory::UnaryFunction
    {
    public:
        typedef diskCache::Cache::HotDirectory::UnaryFunction::element_type
            element_type;

        virtual void
        operator( )( const element_type& Dir )
        {
            time_t now = time( NULL );
            int    hours;
            int    minutes;
            int    seconds;

            diskCache::Cache::HotDirectory::shared_dir_info_type hl_dir =
                Dir.lock( );

            if ( hl_dir )
            {
                seconds = static_cast< int >( now - hl_dir->TimeModified( ) );
                hours = seconds / 3600;
                seconds = seconds % 3600;
                minutes = seconds / 60;
                seconds = seconds % 60;
                retval << "HOT: " << hl_dir->Fullname( )
                       << " modified: " << hours << ":" << std::setfill( '0' )
                       << std::setw( 2 ) << minutes << ":"
                       << std::setfill( '0' ) << std::setw( 2 ) << seconds
                       << " ago" << std::endl;
            }
        }

        inline std::string
        str( ) const
        {
            return retval.str( );
        }

    private:
        std::ostringstream retval;
    };

    class threadScraper : public LDASTools::AL::ThreadPool::UnaryFunction
    {
    public:
        typedef LDASTools::AL::ThreadPool::UnaryFunction::element_type
            element_type;

        virtual void
        operator( )( const element_type& Task, state State )
        {
            switch ( State )
            {
            case THREAD_POOL_TASK_ACTIVE:
                retval << "Thread: "
                       << ( Task.Name( ) ? Task.Name( ).get( ) : "<Unnamed>" )
                       << " ("
                       << ( Task.Type( ) ? Task.Type( ) : "<TypeUnknown>" )
                       << ")"
                       << " - " << Task.StateStr( Task.State( ) ) << std::endl;
                break;
            case THREAD_POOL_TASK_IDLE:
                retval << "Thread: "
                       << "IDLE"
                       << " - " << Task.StateStr( Task.State( ) ) << std::endl;
                break;
            case THREAD_POOL_TASK_ABANDONED:
                retval << "Thread: "
                       << "ABANDONED"
                       << " - " << Task.StateStr( Task.State( ) ) << std::endl;
                break;
            }
        }

        inline std::string
        str( ) const
        {
            return retval.str( );
        }

    private:
        std::ostringstream retval;
    };

    std::string var_lookup( const std::string& Name );
} // namespace

namespace diskCache
{
    namespace MetaCommand
    {
        //===================================================================
        // Dump
        //===================================================================
        OptionSet& Status::options( Status::init_options( ) );

        OptionSet&
        Status::init_options( )
        {
            static OptionSet retval;

            retval.Synopsis( "Subcommand: status" );

            retval.Summary(
                "The status sub command is intended to retrieve information"
                " from the daemon." );

            retval.Add( Option( OPT_DEVICES,
                                "devices",
                                Option::ARG_NONE,
                                "Retrieve information about devices." ) );
            retval.Add( Option( OPT_HOTDIRS,
                                "hot-directories",
                                Option::ARG_NONE,
                                "Retrieve information about directories with "
                                "recent additions." ) );
            retval.Add( Option(
                OPT_OFFLINE,
                "offline",
                Option::ARG_NONE,
                "Retrieve information about mountpoints that are offline" ) );
            retval.Add( Option(
                OPT_THREADS,
                "threads",
                Option::ARG_NONE,
                "Retrieve information about threads running on the server" ) );
            retval.Add( Option(
                OPT_VARIABLE,
                "variable",
                Option::ARG_REQUIRED,
                "Retrieve information about mountpoints that are offline",
                "variable_name" ) );
            return retval;
        }

        Status::Status( CommandLineOptions&                      Args,
                        const ClientServerInterface::ServerInfo& Server )
            : ClientServerInterface( Server ), args( Args )
        {
            if ( args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );

                while ( parsing )
                {
                    switch ( args.Parse( options, arg_name, arg_value ) )
                    {
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    case OPT_DEVICES:
                        tasks[ OPT_DEVICES ] = true;
                        break;
                    case OPT_HOTDIRS:
                        tasks[ OPT_HOTDIRS ] = true;
                        break;
                    case OPT_OFFLINE:
                        tasks[ OPT_OFFLINE ] = true;
                        break;
                    case OPT_THREADS:
                        tasks[ OPT_THREADS ] = true;
                        break;
                    case OPT_VARIABLE:
                    {
                        tasks[ OPT_VARIABLE ] = true;

                        std::istringstream ss( arg_value );
                        std::string        token;

                        while ( std::getline( ss, token, ',' ) )
                        {
                            variables.push_back( token );
                        }
                    }
                    break;
                    default:
                        break;
                    }
                }
            }
        }

        const OptionSet&
        Status::Options( )
        {
            return options;
        }

        void
        Status::evalClient( )
        {
            std::ostringstream cmd;

            cmd << CommandTable::Lookup( CommandTable::CMD_STATUS );

            if ( tasks[ OPT_DEVICES ] )
            {
                cmd << " --" << options[ OPT_DEVICES ].ArgumentName( );
            }
            if ( tasks[ OPT_OFFLINE ] )
            {
                cmd << " --" << options[ OPT_OFFLINE ].ArgumentName( );
            }
            if ( tasks[ OPT_HOTDIRS ] )
            {
                cmd << " --" << options[ OPT_HOTDIRS ].ArgumentName( );
            }
            if ( tasks[ OPT_THREADS ] )
            {
                cmd << " --" << options[ OPT_THREADS ].ArgumentName( );
            }
            if ( tasks[ OPT_VARIABLE ] )
            {
                cmd << " --" << options[ OPT_VARIABLE ].ArgumentName( ) << "=";
                bool first = true;
                for ( variables_type::const_iterator cur = variables.begin( ),
                                                     last = variables.end( );
                      cur != last;
                      ++cur )
                {
                    if ( !first )
                    {
                        cmd << ",";
                    }
                    else
                    {
                        first = false;
                    }
                    cmd << *cur;
                }
            }
            cmd << std::endl;

            ServerRequest( cmd.str( ) );

            transfer responce;

            responce.read( *( serverRequestHandle( ) ) );

            std::cout << responce.msg << std::endl;
        }

        void
        Status::evalServer( )
        {
            transfer responce;

            std::ostringstream retval;

            if ( tasks[ OPT_DEVICES ] )
            {
                deviceScraper scraper;

                diskCache::Cache::Devices::ForEach( scraper );

                retval << scraper.str( );
            }
            if ( tasks[ OPT_HOTDIRS ] )
            {
                hotdirScraper hds;

                diskCache::Cache::HotDirectory::ForEach( hds );

                retval << hds.str( );
            }
            if ( tasks[ OPT_OFFLINE ] )
            {
                deviceScraper scraper( deviceScraper::FLAG_REFERENCED |
                                       deviceScraper::FLAG_OFFLINE );

                diskCache::Cache::Devices::ForEach( scraper );

                retval << scraper.str( );
            }
            if ( tasks[ OPT_THREADS ] )
            {
                threadScraper ts;

                LDASTools::AL::ThreadPool::ForEach( ts );

                retval << ts.str( );
            }
            if ( tasks[ OPT_VARIABLE ] )
            {
                for ( variables_type::const_iterator cur = variables.begin( ),
                                                     last = variables.end( );
                      cur != last;
                      ++cur )
                {
                    retval << *cur << "=";
                    try
                    {
                        retval << var_lookup( *cur );
                    }
                    catch ( const std::exception& Error )
                    {
                        retval << "<EXCEPT:" << Error.what( ) << ">";
                    }
                    retval << std::endl;
                }
            }

            responce.msg = retval.str( );
            responce.write( *( clientHandle( ) ) );
        }

        void
        Status::evalStandalone( )
        {
            //-----------------------------------------------------------------
            // Standalone mode
            //
            // \note
            //    This should not be used.
            //-----------------------------------------------------------------
            std::cout << "WARN: This command is intended to be used against"
                         " an active diskcache daemon."
                      << std::endl;
        }

    } // namespace MetaCommand
} // namespace diskCache

namespace
{
    using namespace diskCache;

    //=====================================================================
    // transfer
    //=====================================================================
    transfer::transfer( )
    {
    }

    std::istream&
    transfer::read( std::istream& Stream )
    {
        bool a;

        std::stringstream in;

        Blob( Stream, a, in );
        if ( a )
        {
            msg = in.str( );
        }
        return Stream;
    }

    std::ostream&
    transfer::write( std::ostream& Stream )
    {
        std::stringstream out( msg );
        Blob( Stream, !msg.empty( ), out );
        return Stream;
    }

    std::string
    var_lookup( const std::string& Name )
    {
        std::ostringstream retval;

        try
        {
            diskCache::Common::Variables::Get( Name, retval );
        }
        catch ( const std::invalid_argument& Except )
        {
            std::ostringstream msg;

            msg << "Unknown variable: " << Name;
            throw std::range_error( msg.str( ) );
        }

        return retval.str( );
    }
} // namespace
