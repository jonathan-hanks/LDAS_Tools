//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__MOUNT_POINT_MANAGER_HH
#define DISKCACHE__MOUNT_POINT_MANAGER_HH

#include <set>

#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/ReadWriteSingleton.hh"
#include "ldastoolsal/types.hh"
#include "ldastoolsal/unordered_map.hh"

#include "diskcacheAPI/Streams/StreamsInterface.hh"

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/DirectoryManager.hh"

namespace diskCache
{
    namespace Cache
    {
        class QueryAnswer;
    }

    //---------------------------------------------------------------------
    /// \brief Manages a collection of mount points.
    ///
    /// A collection of mount points is managed by this singleton class.
    /// Each mount point represents a starting point for data searches.
    /// The collection of mount points is order sensative in that
    /// directories that appear higher in the list will be prioritised
    /// when searching for data.
    //---------------------------------------------------------------------
    class MountPointManager
    {
    public:
        //-------------------------------------------------------------------
        /// \brief Collection of mount point names
        ///
        /// This is used to ease the management of the collection of
        /// mount points when only the name is significant.
        /// This container does not impose any particular order.
        //-------------------------------------------------------------------
        typedef std::list< std::string > mount_point_name_container_type;
        typedef LDASTools::AL::ReadWriteLockVariable<
            LDASTools::AL::ReadWriteLock::READ,
            mount_point_name_container_type >
            mount_point_names_ro_type;

        typedef Cache::DirectoryManager directory_cache_type;

        //-------------------------------------------------------------------
        /// \brief Presents additional information regarding an update request.
        ///
        /// The information that is contained in this structure may be used
        /// in suplimental messages to report information to the user.
        //-------------------------------------------------------------------
        struct UpdateResults
        {
        public:
            typedef std::list< std::string > error_container_type;

            mount_point_name_container_type s_added;
            mount_point_name_container_type s_deleted;
            error_container_type            s_errors;
        };

        //-------------------------------------------------------------------
        /// \brief Reset the cached information
        //-------------------------------------------------------------------
        static const int RESET_CACHE = 0x0001;
        //-------------------------------------------------------------------
        /// \brief Reset the lists of managed directories
        //-------------------------------------------------------------------
        static const int RESET_MANAGED_DIRECTORIES = 0x0002;

        //-------------------------------------------------------------------
        /// \brief Addition information regarding a scan request.
        ///
        /// The information presented here may be benificial when
        /// producing verbose messages or logging results.
        //-------------------------------------------------------------------
        struct ScanResults
        {
        public:
            enum dir_state
            {
                DIRECTORY_STATE_DELETED,
                DIRECTORY_STATE_MODIFIED,
                DIRECTORY_STATE_NEW
            };
            struct stats
            {
                dir_state s_state;
                INT_4U    s_files_added;
                INT_4U    s_files_deleted;
            };
            typedef std::map< std::string, stats > directory_stats_type;

            void AddResults( const std::string& Directory,
                             dir_state          DirectoryState,
                             INT_4U             Added,
                             INT_4U             Deleted );

            template < typename Op >
            void operator( )( Op& Operation ) const;

        private:
            directory_stats_type m_stats;
        };

        //-------------------------------------------------------------------
        // \brief Default constructor
        //-------------------------------------------------------------------
        MountPointManager( );

        //-------------------------------------------------------------------
        /// \brief Establish the cache of directories
        //-------------------------------------------------------------------
        void DirectoryCache( directory_cache_type* Cache );

        //-------------------------------------------------------------------
        /// \brief Locate files contained within the collection
        //-------------------------------------------------------------------
        void Find( Cache::QueryAnswer& Answer ) const;

        //-------------------------------------------------------------------
        /// \brief Retrieve the names of the mount points
        //-------------------------------------------------------------------
        const mount_point_names_ro_type MountPoints( ) const;

        //-------------------------------------------------------------------
        /// \brief Retrieve information from the stream.
        ///
        /// \param[in] Stream
        ///     Source of the new information.
        //-------------------------------------------------------------------
        Streams::IInterface& Read( Streams::IInterface& Stream );

        //-------------------------------------------------------------------
        /// \brief Reset the requested information
        ///
        /// This allows for the purging of certain information so as to
        /// force a refreshing of the information.
        ///
        /// \param[in] Flag
        ///     This is an ORed flag indicating what should be cleaned
        //-------------------------------------------------------------------
        void Reset( int Flag );

        //-------------------------------------------------------------------
        /// \brief Request the scanning of a single mount point
        ///
        /// \param[in] MountPoint
        ///     The name of the mount point to scan.
        /// \param[out] Results
        ///     Information gathered during the scan.
        //-------------------------------------------------------------------
        void Scan( const std::string& MountPoint, ScanResults& Results );

        //-------------------------------------------------------------------
        /// \brief Update the collection of managed mount points
        ///
        /// \param[in] MountPoints
        ///     Ordered collection of mount points to be managed.
        /// \param[out] Results
        ///     Results.
        ///
        //-------------------------------------------------------------------
        void Update( const mount_point_name_container_type& MountPoints,
                     UpdateResults&                         Results );

#if 0 || 1
    //-------------------------------------------------------------------
    /// \brief Dump information to the stream.
    ///
    /// \param[in] Stream
    ///     Sink for the information.
    //-------------------------------------------------------------------
    template< typename StreamT >
    StreamT& Read( StreamT& Stream );

    //-------------------------------------------------------------------
    /// \brief Dump information to the stream.
    ///
    /// \param[in] Stream
    ///     Sink for the information.
    //-------------------------------------------------------------------
    template< typename OStreamT >
    OStreamT& Write( OStreamT& Stream );
#endif /* 0 */

    private:
        typedef LDASTools::AL::ReadWriteLockVariable<
            LDASTools::AL::ReadWriteLock::WRITE,
            mount_point_name_container_type >
                                        mount_point_names_rw_type;
        typedef std::set< std::string > excluded_mount_point_names_type;

        mount_point_name_container_type m_mount_points_dictionary_order;
        mount_point_name_container_type p_mount_points_search_order;
        excluded_mount_point_names_type excluded_mount_point_names;
        mutable mount_point_names_rw_type::baton_type
            p_mount_points_search_order_baton;

        directory_cache_type* m_directory_cache;

        const mount_point_names_ro_type mount_point_names_ro( ) const;
        mount_point_names_rw_type       mount_point_names_rw( );
    };

    //=====================================================================
    //
    //=====================================================================
    template < class Op >
    void
    MountPointManager::ScanResults::operator( )( Op& Operation ) const
    {
        for ( directory_stats_type::const_iterator cur = m_stats.begin( ),
                                                   last = m_stats.end( );
              cur != last;
              ++cur )
        {
            Operation( cur->first,
                       cur->second.s_state,
                       cur->second.s_files_added,
                       cur->second.s_files_deleted );
        }
    }

    //=====================================================================
    //=====================================================================
    inline void
    MountPointManager::ScanResults::AddResults( const std::string& Directory,
                                                dir_state          State,
                                                INT_4U             Added,
                                                INT_4U             Deleted )
    {
        stats& info( m_stats[ Directory ] );
        info.s_state = State, info.s_files_added = Added;
        info.s_files_deleted = Deleted;
    }
    //=====================================================================
    //=====================================================================
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    inline void
    MountPointManager::DirectoryCache( directory_cache_type* Cache )
    {
        m_directory_cache = Cache;
    }

    inline const MountPointManager::mount_point_names_ro_type
    MountPointManager::MountPoints( ) const
    {
        return mount_point_names_ro( );
    }

    inline const MountPointManager::mount_point_names_ro_type
    MountPointManager::mount_point_names_ro( ) const
    {
        return ( mount_point_names_ro_type( p_mount_points_search_order_baton,
                                            RWLOCK_TIMEOUT,
                                            p_mount_points_search_order,
                                            __FILE__,
                                            __LINE__ ) );
    }

    inline MountPointManager::mount_point_names_rw_type
    MountPointManager::mount_point_names_rw( )
    {
        return ( mount_point_names_rw_type( p_mount_points_search_order_baton,
                                            RWLOCK_TIMEOUT,
                                            p_mount_points_search_order,
                                            __FILE__,
                                            __LINE__ ) );
    }

} // namespace diskCache

#endif /* DISKCACHE__MOUNT_POINT_MANAGER_HH */
