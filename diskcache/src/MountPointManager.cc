//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include "genericAPI/Logging.hh"
// #include "genericAPI/MountPointStatus.hh"

#include "diskcacheAPI/Streams/Binary.hh"

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"

#include "MountPointManager.hh"
#include "DirectoryManagerSingleton.hh"
#include "MountPointScanner.hh"

using diskCache::Cache::ExcludedDirectoriesSingleton;

namespace
{
    class propigate
    {
    public:
        typedef diskCache::DirectoryManagerSingleton::ScanResults::
            file_count_type file_count_type;
        typedef diskCache::DirectoryManagerSingleton::ScanResults::
            directory_state directory_state;

        propigate( diskCache::MountPointManager::ScanResults& Dest )
            : m_dest( Dest )
        {
        }

        inline void
        operator( )( const std::string& DirectoryName,
                     directory_state    DirectoryState,
                     file_count_type    FilesAdded,
                     file_count_type    FilesDeleted )
        {
            diskCache::MountPointManager::ScanResults::dir_state ds(
                diskCache::MountPointManagerSingleton::ScanResults::
                    DIRECTORY_STATE_MODIFIED );

            switch ( DirectoryState )
            {
            case diskCache::Cache::Directory::DIRECTORY_REMOVED:
                ds = diskCache::MountPointManagerSingleton::ScanResults::
                    DIRECTORY_STATE_DELETED;
                break;
            case diskCache::Cache::Directory::DIRECTORY_UPDATED:
                ds = diskCache::MountPointManagerSingleton::ScanResults::
                    DIRECTORY_STATE_MODIFIED;
                break;
            case diskCache::Cache::Directory::DIRECTORY_NEW:
                ds = diskCache::MountPointManagerSingleton::ScanResults::
                    DIRECTORY_STATE_NEW;
                break;
            case diskCache::Cache::Directory::DIRECTORY_OFFLINE:
                //---------------------------------------------------------------
                // Ignore directories marked OFFLINE
                //---------------------------------------------------------------
                return;
            }
            m_dest.AddResults( DirectoryName, ds, FilesAdded, FilesDeleted );
        }

    private:
        diskCache::MountPointManager::ScanResults& m_dest;
    };

} // namespace

namespace diskCache
{
    using Cache::Directory;

    Streams::IBinary&
    operator>>( Streams::IBinary& Stream, MountPointManager& Data )
    {
        (void)Data.Read( Stream );
        return Stream;
    }

    //---------------------------------------------------------------------
    ///
    //---------------------------------------------------------------------
    MountPointManager::MountPointManager( )
        : m_directory_cache( (directory_cache_type*)NULL )
    {
    }

    //---------------------------------------------------------------------
    //
    //---------------------------------------------------------------------
    void
    MountPointManager::Find( Cache::QueryAnswer& Answer ) const
    {
        //-------------------------------------------------------------------
        // Start by making a list of roots so as not to have the list locked
        // for an extended period of time.
        //-------------------------------------------------------------------
        typedef std::list< std::string > roots_type;
        roots_type                       roots;

        {
            const mount_point_names_ro_type mpn_lock( mount_point_names_ro( ) );

            for ( mount_point_names_ro_type::element_type::const_iterator
                      cur = mpn_lock.Var( ).begin( ),
                      last = mpn_lock.Var( ).end( );
                  cur != last;
                  ++cur )
            {
                roots.push_back( *cur );
            }
        }
        //-------------------------------------------------------------------
        // Now that a list of starting points has been created,
        // start looking for the answer
        //-------------------------------------------------------------------
        for ( roots_type::const_iterator cur = roots.begin( ),
                                         last = roots.end( );
              cur != last;
              ++cur )
        {
            DirectoryManagerSingleton::Find( *cur, Answer );
            if ( Answer.IsCompleted( ) )
            {
                break;
            }
        }
        if ( Answer.IsResampled( ) )
        {
            //-----------------------------------------------------------------
            // Setup for resampling (Need to get an extra frame file on each
            //   side of the matched pattern).
            //-----------------------------------------------------------------
            Answer.Resample( );
            for ( roots_type::const_iterator cur = roots.begin( ),
                                             last = roots.end( );
                  cur != last;
                  ++cur )
            {
                DirectoryManagerSingleton::Find( *cur, Answer );
                if ( Answer.IsCompleted( ) )
                {
                    break;
                }
            }
        }
        Answer.Complete( );
    }

    //---------------------------------------------------------------------
    ///
    //---------------------------------------------------------------------
    Streams::IInterface&
    MountPointManager::Read( Streams::IInterface& Stream )
    {
        static const char method_name[] = "diskCache::MountPointManager::Read";

        {
            std::ostringstream msg;

            msg << "Entry";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        try
        {
            {
                std::ostringstream msg;

                msg << "Trying to read binary file";
                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    30,
                    method_name,
                    "CXX" );
            }
            Streams::IBinary& binary =
                dynamic_cast< Streams::IBinary& >( Stream );

            mount_point_name_container_type      mpn;
            Directory::excluded_directories_type xdirs;

            binary >> mpn // Read the list of mount points
                >> xdirs // Read list of directories to exclude
                ;
            //-----------------------------------------------------------------
            // \todo Obtain exclusive permissions to mount points
            //-----------------------------------------------------------------
            mount_point_names_rw_type mpn_lock( mount_point_names_rw( ) );
#if !NEW_DIRECTORY_INTERFACE
            mount_point_container_rw_type mp_lock( mount_points_rw( ) );
#endif /* ! NEW_DIRECTORY_INTERFACE */

            //-----------------------------------------------------------------
            // \todo Clear out all the old data
            //-----------------------------------------------------------------
            mpn_lock.Var( ) = mpn;
#if !NEW_DIRECTORY_INTERFACE
            mp_lock.Var( ).clear( );
#endif /* ! NEW_DIRECTORY_INTERFACE */
            //-----------------------------------------------------------------
            // \todo Reset the list of directories to exclude
            //-----------------------------------------------------------------
            //-----------------------------------------------------------------
            // \todo Set list of mount points
            //-----------------------------------------------------------------

            try
            {
                //---------------------------------------------------------------
                // \todo Read the remainer of the stream as Directory
                // information The directories in the stream are ordered in
                // search order
                //---------------------------------------------------------------
#if !NEW_DIRECTORY_INTERFACE
                while ( binary.Readable( ) )
                {
                    root_type root;
                    root->Read( binary );
                    //-------------------------------------------------------------
                    /// \todo
                    /// Place into the mount point list
                    //-------------------------------------------------------------
                }
#endif /* ! NEW_DIRECTORY_INTERFACE */
            }
            catch ( const std::istream::failure& Exception )
            {
                Stream.clear( );
            }
        }
        catch ( std::bad_cast& Exception )
        {
            //-----------------------------------------------------------------
            // Currently the input stream is unsupported for dumping additional
            // information. Quietly ignore this error.
            //-----------------------------------------------------------------
            Stream.clear( );
        }
        //-------------------------------------------------------------------
        // \todo
        // Push onto the stack the
        //-------------------------------------------------------------------
        {
            std::ostringstream msg;

            msg << "Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        return Stream;
    }
    //---------------------------------------------------------------------
    ///
    //---------------------------------------------------------------------
    void
    MountPointManager::Reset( int Flag )
    {
        static const char method_name[] = "diskCache::MountPointManager::Reset";

        {
            std::ostringstream msg;

            msg << "Entry";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        if ( Flag & RESET_CACHE )
        {
            {
                std::ostringstream msg;

                msg << "Starting to reset the mount points";
                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    30,
                    method_name,
                    "CXX" );
            }
#if NEW_DIRECTORY_INTERFACE
#else /* NEW_DIRECTORY_INTERFACE */
            mount_point_container_rw_type mp_vlock( mount_points_rw( ) );
            mount_point_container_type&   mp( mp_vlock.Var( ) );

            for ( mount_point_container_type::iterator cur = mp.begin( ),
                                                       last = mp.end( );
                  cur != last;
                  ++cur )
            {
                cur->second =
                    root_type( new root_type::element_type( cur->first ) );
            }
#endif /* NEW_DIRECTORY_INTERFACE */
        }
        {
            std::ostringstream msg;

            msg << "Exit";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
    }

    //---------------------------------------------------------------------
    ///
    //---------------------------------------------------------------------
    void
    MountPointManager::Update(
        const mount_point_name_container_type& MountPoints,
        UpdateResults&                         Results )
    {
        static const char method_name[] =
            "diskCache::MountPointManager::Update";

        std::list< std::string >  diff_results;
        mount_point_names_rw_type mpn_vlock( mount_point_names_rw( ) );

        {
            std::ostringstream msg;

            msg << "Entry: MountPoints.size( ): " << MountPoints.size( )
                << " mpn_vlock.size( ): " << mpn_vlock.Var( ).size( )
                << " m_mount_points_dictionary_order.size( ): "
                << m_mount_points_dictionary_order.size( );
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }

        //-------------------------------------------------------------------
        // Create copy of list and sort it by dictionary order
        //-------------------------------------------------------------------
        mount_point_name_container_type dictionary_order( MountPoints );

        {
            //-----------------------------------------------------------------
            // Exclude selected directories
            //-----------------------------------------------------------------
            mount_point_name_container_type non_excluded;
            std::string                     item;

            for ( mount_point_name_container_type::const_iterator
                      cur = dictionary_order.begin( ),
                      last = dictionary_order.end( );
                  cur != last;
                  ++cur )
            {
                bool               excluded = false;
                std::istringstream ine( *cur );

                while ( std::getline( ine, item, '/' ) )
                {
                    if ( item.size( ) <= 0 )
                    {
                        continue;
                    }
                    if ( ExcludedDirectoriesSingleton::IsExcluded( item ) )
                    {
                        {
                            std::ostringstream msg;

                            msg << "Excluding"
                                << " item length: " << item.size( )
                                << " item: " << item << " (fullpath: " << *cur
                                << " )";
                            GenericAPI::queueLogEntry(
                                msg.str( ),
                                GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                30,
                                method_name,
                                "CXX" );
                        }
                        excluded = true;
                        break;
                    }
                }
                if ( excluded )
                {
                    excluded_mount_point_names.insert( *cur );
                }
                else
                {
                    non_excluded.push_back( *cur );
                }
            }

            dictionary_order.swap( non_excluded );
        }

        dictionary_order.sort( );
        {
            std::ostringstream msg;

            msg << "dictionary_order: size: " << dictionary_order.size( );
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        // GenericAPI::MountPointStatus::Set( dictionary_order );
        {
            mount_point_name_container_type added;
            mount_point_name_container_type deleted;

            mount_point_name_container_type::const_iterator
                current = dictionary_order.begin( ),
                current_end = dictionary_order.end( ),
                previous = m_mount_points_dictionary_order.begin( ),
                previous_end = m_mount_points_dictionary_order.end( );
            int status = 0;

            while ( ( current != current_end ) && ( previous != previous_end ) )
            {
                status = previous->compare( *current );

                if ( status == 0 )
                {
                    //-------------------------------------------------------------
                    // When the two are the same, then the data just needs
                    // to migrate from the old to the new.
                    //-------------------------------------------------------------
                    ++current;
                    ++previous;
                }
                else if ( status < 0 )
                {
                    deleted.push_back( *previous );
                    ++previous;
                }
                else
                {
                    added.push_back( *current );
                    ++current;
                }
            }
            while ( previous != previous_end )
            {
                deleted.push_back( *previous );
                ++previous;
            }
            while ( current != current_end )
            {
                added.push_back( *current );
                ++current;
            }
#if !NEW_DIRECTORY_INTERFACE
            //-----------------------------------------------------------------
            // Apply the changes
            //-----------------------------------------------------------------
            mount_point_container_type& mp( mp_vlock.Var( ) );

            for ( mount_point_name_container_type::const_iterator
                      cur = deleted.begin( ),
                      last = deleted.end( );
                  cur != last;
                  ++cur )
            {
                mp.erase( *cur );
            }
            for ( mount_point_name_container_type::const_iterator
                      cur = added.begin( ),
                      last = added.end( );
                  cur != last;
                  ++cur )
            {
                mp[ *cur ] = root_type( new root_type::element_type( *cur ) );
            }
#else
            for ( mount_point_name_container_type::const_iterator
                      cur = deleted.begin( ),
                      last = deleted.end( );
                  cur != last;
                  ++cur )
            {
                m_directory_cache->RemoveDirectoryRecursively( *cur );
            }
#endif /* ! NEW_DIRECTORY_INTERFACE */
            //-----------------------------------------------------------------
            // Update the information returned to the user
            //-----------------------------------------------------------------
            Results.s_added.swap( added );
            {
                std::ostringstream msg;

                msg << "added: count: " << Results.s_added.size( );

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    30,
                    method_name,
                    "CXX" );
            }
            Results.s_deleted.swap( deleted );
            {
                std::ostringstream msg;

                msg << "deleted: count: " << Results.s_deleted.size( );

                GenericAPI::queueLogEntry(
                    msg.str( ),
                    GenericAPI::LogEntryGroup_type::MT_DEBUG,
                    30,
                    method_name,
                    "CXX" );
            }
            //-----------------------------------------------------------------
            // Update "indexes"
            //-----------------------------------------------------------------
            m_mount_points_dictionary_order.swap( dictionary_order );
            mpn_vlock.Var( ) = MountPoints;
        }
        {
            std::ostringstream msg;

            msg << "Exit: "
                << " mpn_vlock.size( ): " << mpn_vlock.Var( ).size( );
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }

    } // Method - Update

    //---------------------------------------------------------------------
    /// This does a deep scan of the requested mount point.
    //---------------------------------------------------------------------
    void
    MountPointManager::Scan( const std::string& MountPoint,
                             ScanResults&       Results )
    {
        static const char method_name[] = "diskCache::MountPointManager::Scan";

        {
            std::ostringstream msg;

            msg << "Starting the scan of mount point: " << MountPoint;
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        if ( excluded_mount_point_names.find( MountPoint ) ==
             excluded_mount_point_names.end( ) )
        {
            std::ostringstream msg;

            msg << "Ignoring mount point: " << MountPoint
                << " because it is part of the exclude ";
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_DEBUG,
                                       30,
                                       method_name,
                                       "CXX" );
        }
        else
        {
            Cache::DirectoryManager::ScanResults results;

            m_directory_cache->Scan( MountPoint, results );
            propigate propigator( Results );

            results( propigator );
        }
    } // Method - Scan

} // namespace diskCache
