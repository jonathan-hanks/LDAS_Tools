//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// GenericAPI Header Files
#include <queue>
#include "genericAPI/Logging.hh"

#include "diskcacheAPI/Cache/DirectoryManager.hh"
#include "diskcacheAPI/Cache/HotDirectory.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"

using GenericAPI::LogEntryGroup_type;
using GenericAPI::queueLogEntry;

using diskCache::Cache::HotDirectory;

namespace
{
    class queue_functor : public std::queue< std::string >
    {
    public:
        typedef std::queue< std::string > container_type;

        inline queue_functor( const container_type::value_type& Start )
        {
            push( Start );
        }

        inline void
        Parent( const std::string& P )
        {
            parent = P;
        }

        void
        operator( )( const container_type::value_type& Child )
        {
            std::ostringstream c;
            c << parent << "/" << Child;
            push( c.str( ) );
        }

    private:
        std::string parent;
    };

    class query_functor
    {
    public:
        typedef diskCache::Cache::Directory        Directory;
        typedef diskCache::Cache::DirectoryManager DirectoryManager;

        typedef diskCache::Cache::DirectoryManager::ScanResults ScanResults;
        typedef diskCache::Cache::QueryAnswer                   QueryAnswer;

        query_functor( const DirectoryManager& Manager, QueryAnswer& Answer );

        bool Done( ) const;

        void operator( )( Directory::dirref_type Node ) const;

        const DirectoryManager& m_directory_manager;
        QueryAnswer&            m_answer;
    };

    inline query_functor::query_functor( const DirectoryManager& Manager,
                                         QueryAnswer&            Answer )
        : m_directory_manager( Manager ), m_answer( Answer )
    {
    }

    inline bool
    query_functor::Done( ) const
    {
        return ( m_answer.IsCompleted( ) );
    }

    void
    query_functor::operator( )( Directory::dirref_type Node ) const
    {
        Node->Find( m_answer );
    }

    class scan_functor
    {
    public:
        typedef diskCache::Cache::Directory              Directory;
        typedef diskCache::Cache::Directory::dirref_type return_type;
        typedef diskCache::Cache::DirectoryManager       DirectoryManager;

        typedef diskCache::Cache::DirectoryManager::ScanResults ScanResults;

        scan_functor( DirectoryManager&  Manager,
                      ScanResults&       Results,
                      const std::string& Caller,
                      const std::string& JobInfo );

        return_type operator( )( Directory::dirref_type Node ) const;

        const std::string m_caller;
        const std::string m_job_info;
        DirectoryManager& m_directory_manager;
        ScanResults&      m_results;
    };

    inline scan_functor::scan_functor( DirectoryManager&  Manager,
                                       ScanResults&       Results,
                                       const std::string& Caller,
                                       const std::string& JobInfo )
        : m_caller( Caller ), m_job_info( JobInfo ),
          m_directory_manager( Manager ), m_results( Results )
    {
    }

    scan_functor::return_type
    scan_functor::operator( )( Directory::dirref_type Node ) const
    {
        static const char* caller = "scan_functor::operator()";
        QUEUE_LOG_MESSAGE(
            "Scanning: " << Node->Fullname( ), MT_DEBUG, 50, caller, "CXX" );
        Directory::dirref_type modified =
            Node->Scan( m_directory_manager, m_results, m_caller, m_job_info );

        if ( modified )
        {
            //-----------------------------------------------------------------
            /// \todo
            /// All of this needs to be part of the node scanning to ensure
            /// proper and minimal locking of critical sections.
            //-----------------------------------------------------------------
            /* m_directory_manager.OnUpdate( modified, m_results ); */
            Node = modified;
        }
        //-------------------------------------------------------------------
        // Check to see if the directory needs to be registered
        //-------------------------------------------------------------------
        HotDirectory::RegisterIfHot( Node, m_directory_manager, true );
        //-------------------------------------------------------------------
        // Record the scanning statistics once it is known
        //-------------------------------------------------------------------
        m_results.FileCountInc( Node->FileCount( ) );
        m_results.DirectoryCountInc( );
        //-------------------------------------------------------------------
        // Return to the caller
        //-------------------------------------------------------------------
        return Node;
    }
} // namespace

namespace diskCache
{
    namespace Cache
    {
        DirectoryManager::DirectoryManager( )
        {
        }

        void
        DirectoryManager::Find( const std::string& Root,
                                QueryAnswer&       Answer ) const
        {
            query_functor q( *this, Answer );

            Walk( q, Root );
        }

        void
        DirectoryManager::OnUpdate( directory_ref_type Dir,
                                    ScanResults&       Results )
        {
            //-----------------------------------------------------------------
            // Replace the previous directory information with the updated
            // information.
            //-----------------------------------------------------------------
            AddDirectory( Dir );
            //-----------------------------------------------------------------
            // Check to see if any child directories have been removed as
            //     nodes need to be removed.
            //-----------------------------------------------------------------
            try
            {
                const std::string basename( Dir->Fullname( ) );
                const ScanResults::filename_container_type& dirs(
                    Results.Removed( basename ) );

                for ( ScanResults::filename_container_type::const_iterator
                          cur = dirs.begin( ),
                          last = dirs.end( );
                      cur != last;
                      ++cur )
                {
                    std::string fullname( basename );
                    fullname += "/";
                    fullname += *cur;
                    // RemoveDirectory( fullname );
                    RemoveDirectoryRecursively( fullname );
                }
            }
            catch ( const std::range_error& Error )
            {
                //---------------------------------------------------------------
                // Nothing was deleted
                //---------------------------------------------------------------
            }
            //-----------------------------------------------------------------
            // Check to see if any child directories have been added as
            //     nodes need to be created.
            //-----------------------------------------------------------------
            try
            {
                const std::string basename( Dir->Fullname( ) );
                const ScanResults::filename_container_type& dirs(
                    Results.Added( basename ) );

                for ( ScanResults::filename_container_type::const_iterator
                          cur = dirs.begin( ),
                          last = dirs.end( );
                      cur != last;
                      ++cur )
                {
                    std::string fullname( basename );
                    fullname += "/";
                    fullname += *cur;
                    CreateDirectory( fullname, Dir->Root( ) );
                }
            }
            catch ( const std::range_error& Error )
            {
                //---------------------------------------------------------------
                // Nothing was added
                //---------------------------------------------------------------
            }
        }

        void
        DirectoryManager::RemoveDirectoryRecursively( const std::string& Name )
        {
            queue_functor q( Name );

            while ( q.empty( ) == false )
            {
                try
                {
                    directory_ref_type d( ReferenceDirectory( q.front( ) ) );

                    q.Parent( d->Fullname( ) );
                    if ( d )
                    {
                        d->Children( q );
                    }
                    RemoveDirectory( q.front( ) );
                }
                catch ( ... )
                {
                }
                q.pop( );
            }
        }

        void
        DirectoryManager::Scan( directory_ref_type Dir, ScanResults& Results )
        {
            static const char* caller = "DirectoryManager::Scan(Dir,Results)";

            scan_functor s( *this, Results, "SCAN", "HOT_DIR" );

            directory_container_type::mapped_type dir(
                ReferenceDirectory( Dir->Fullname( ) ) );

            if ( dir )
            {
                s( dir );
            }
            else
            {
                QUEUE_LOG_MESSAGE(
                    "No mapping for directory: " << Dir->Fullname( ),
                    MT_DEBUG,
                    40,
                    caller,
                    "CXX" );
            }
        }

        void
        DirectoryManager::Scan( const std::string& Root, ScanResults& Results )
        {
            static const char* caller = "DirectoryManager::Scan(Root, Results)";

            scan_functor s( *this, Results, "SCAN", "SCAN_MOUNTPT" );

            directory_container_type::mapped_type dir =
                ReferenceDirectory( Root );

            if ( !dir )
            {
                QUEUE_LOG_MESSAGE( "Creating new directory entry for: " << Root,
                                   MT_DEBUG,
                                   30,
                                   caller,
                                   "CXX" );
                dir.reset( new Directory( Root, Root ) );
                AddDirectory( dir );
            }

            Walk( s, Root );
        }
    } // namespace Cache
} // namespace diskCache
