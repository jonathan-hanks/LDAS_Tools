//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <sstream>

#include "ExcludedDirectories.hh"

namespace diskCache
{
    namespace Cache
    {
        ExcludedDirectories::ExcludedDirectories( )
        {
        } /* ExcludedDirectories::ExcludedDirectories */

        void
        ExcludedDirectories::Update(
            const directory_container_type& Directories )
        {
            //-----------------------------------------------------------------
            // Generate a new regular expression to quickly identify
            // directories that should be excluded
            //-----------------------------------------------------------------
            std::ostringstream r;

            r << "^(";
            for ( directory_container_type::const_iterator
                      start = Directories.begin( ),
                      cur = Directories.begin( ),
                      last = Directories.end( );
                  cur != last;
                  ++cur )
            {
                if ( cur != start )
                {
                    r << "|";
                }
                r << *cur;
            }
            r << ")$";

            //-----------------------------------------------------------------
            // Save the new pattern
            //-----------------------------------------------------------------
            pattern = Regex( r.str( ).c_str( ) );
            //-----------------------------------------------------------------
            // Check to see if anyone needs to be notified of the update
            //-----------------------------------------------------------------
            for ( callback_container_type::const_iterator
                      cur = callbacks.begin( ),
                      last = callbacks.end( );
                  cur != last;
                  ++cur )
            {
                ( *cur )( ); // Execute the callback
            }
        } /* ExcludedDirectories::Update */

    } // namespace Cache
} // namespace diskCache
