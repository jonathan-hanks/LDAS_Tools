//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <boost/shared_ptr.hpp>

#include "diskcacheAPI/Common/Logging.hh"

#include "diskcacheAPI/Streams/Binary.hh"

#include "diskcacheAPI/Cache/DirectoryManager.hh"

namespace diskCache
{
    namespace Cache
    {
        using Streams::IBinary;
        using Streams::OBinary;

        template <>
        IBinary&
        DirectoryManager::Read( IBinary& Stream )
        {
#if DEBUG_MOUNT_POINT_MANAGER
            static char const* caller =
                "diskCache::Cache::DirectoryManager::Read";
#endif /* DEBUG_MOUNT_POINT_MANAGER */

            {
                //---------------------------------------------------------------
                // Read the list of directories to exclude
                //---------------------------------------------------------------
                Directory::excluded_directories_type xdirs;

                Stream >> xdirs;
                Directory::ExcludedDirectories( xdirs );
            }
            try
            {
                //-------------------------------------------------------------
                // For the sake of speed, lock once
                //-------------------------------------------------------------
                directory_container_rw_type container(
                    directory_container_rw( ) );

                container.Var( ).clear( );
                while ( Stream.Readable( ) )
                {
                    boost::shared_ptr< Directory > dir( new Directory );

#if DEBUG_MOUNT_POINT_MANAGER
                    QUEUE_LOG_MESSAGE( "about to read directory",
                                       MT_DEBUG,
                                       DEBUG_MOUNT_POINT_MANAGER,
                                       caller,
                                       "CXX" );
#endif /* DEBUG_MOUNT_POINT_MANAGER */
                    dir->Read( Stream );
                    //-------------------------------------------------------------
                    // Record the directory
                    //-------------------------------------------------------------
                    if ( dir )
                    {
#if DEBUG_MOUNT_POINT_MANAGER
                        QUEUE_LOG_MESSAGE(

                            "managing directory: " << dir->Fullname( ),
                            MT_DEBUG,
                            DEBUG_MOUNT_POINT_MANAGER,
                            caller,
                            "CXX" );
#endif /* DEBUG_MOUNT_POINT_MANAGER */
                        container.Var( )[ dir->Fullname( ) ] = dir;
                    }
                }
            }
            catch ( const std::istream::failure& Exception )
            {
                if ( !Stream.eof( ) )
                {
                    //-------------------------------------------------------------
                    // Rethrow the exception since it is unexpected
                    //-------------------------------------------------------------
                    throw;
                }
                //---------------------------------------------------------------
                // Came to the end of the file. Nothing to concearn ourselves
                //   about.
                //---------------------------------------------------------------
            }
            return Stream;
        }

        template <>
        OBinary&
        DirectoryManager::Write( OBinary& Stream )
        {
            {
                //---------------------------------------------------------------
                // Write the list of directories to exclude
                //---------------------------------------------------------------
                Stream << Directory::ExcludedDirectories( ).Var( );
            }
            const directory_container_ro_type container(
                directory_container_ro( ) );

            for ( directory_container_type::const_iterator
                      cur = container.Var( ).begin( ),
                      last = container.Var( ).end( );
                  cur != last;
                  ++cur )
            {
                cur->second->Write( Stream );
            }
            return Stream;
        }

    } // namespace Cache
} // namespace diskCache
