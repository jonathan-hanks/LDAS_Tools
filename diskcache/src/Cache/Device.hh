//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__DEVICE_HH
#define DISKCACHE_API__CACHE__DEVICE_HH

#include <memory>
#include <string>

#include "ldastoolsal/ldas_types.h"
#include "ldastoolsal/Directory.hh"
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/Thread.hh"

namespace diskCache
{
    namespace Cache
    {
        //-------------------------------------------------------------------
        /// \brief Maintain information about a device
        //-------------------------------------------------------------------
        class Devices;

        class Device : public LDASTools::AL::Thread
        {
        public:
            typedef INT_8U id_type;
            enum state_type
            {
                OFFLINE,
                ONLINE
            };

            Device( );

            Device( const std::string& Path, id_type Id );

            Device( const Device& Source );

            ~Device( );

            const std::string& Path( ) const;

            id_type Id( ) const;

            //-----------------------------------------------------------------
            /// \brief Set the id associated with the device
            ///
            /// \note
            ///   This function should be used very sparingly.
            //-----------------------------------------------------------------
            void Id( id_type Source );

            bool IsOffline( ) const;

            void Offline( );

            state_type State( ) const;

            std::string StateStr( ) const;

            void Used( bool Value );

            bool Used( ) const;

        protected:
            void action( );

        private:
            friend class Devices;

            struct private_type;
            typedef std::unique_ptr< private_type > p_type;

            mutable LDASTools::AL::ReadWriteLock::baton_type baton;

            p_type pdata;

            state_type state;

            bool used;

            void path( const std::string& PathName );
        }; // class - Device

        inline bool
        Device::IsOffline( ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            return ( state != ONLINE );
        }

        inline Device::state_type
        Device::State( ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            return state;
        }

        inline std::string
        Device::StateStr( ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            std::string retval = "<unknown>";

            switch ( state )
            {
            case OFFLINE:
                retval = "OFFLINE";
                break;
            case ONLINE:
                retval = "ONLINE";
                break;
            }

            return retval;
        }

        inline void
        Device::Used( bool Value )
        {
            LDASTools::AL::ReadWriteLock lock(
                baton,
                LDASTools::AL::ReadWriteLock::WRITE,
                __FILE__,
                __LINE__ );

            used = Value;
        }

        inline bool
        Device::Used( ) const
        {
            LDASTools::AL::ReadWriteLock lock(
                baton, LDASTools::AL::ReadWriteLock::READ, __FILE__, __LINE__ );

            return used;
        }

    } // namespace Cache
} // namespace diskCache
#endif /* DISKCACHE_API__CACHE__DEVICE_HH */
