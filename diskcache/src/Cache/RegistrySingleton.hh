//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__CACHE__REGISTRY_SINGLETON_HH
#define DISKCACHE_API__CACHE__REGISTRY_SINGLETON_HH

#include <string>

#include "ldastoolsal/Singleton.hh"

#include "diskcacheAPI/Common/Registry.hh"

#include "diskcacheAPI/Cache/Registry.hh"
#include "diskcacheAPI/Cache/SearchInterface.hh"

namespace diskCache
{
    namespace Cache
    {
        class RegistrySingleton : private Registry
        {
        public:
            SINGLETON_TS_DECL( RegistrySingleton );

        public:
            typedef Registry::info_type           info_type;
            typedef Registry::id_type             id_type;
            typedef Registry::ascii_key_type      ascii_key_type;
            typedef Registry::Info                Info;
            typedef Registry::Info::indexing_type indexing_type;

            using Registry::KEY_NULL;

            static const info_type GetInfo( id_type Key );

            static id_type Id( const ascii_key_type& Key );

            static void OnDirectoryClose( DirectoryScanData& Data );

            static id_type Register( const Info& Key );

            static bool ScanForMatch( DirectoryScanData& Data );

            static void TranslateQuery( const QueryParams& Params,
                                        QueryAnswer&       Answer );

            static const id_type ID_NULL;

        private:
        }; // class RegistrySingleton

        inline const RegistrySingleton::info_type
        RegistrySingleton::GetInfo( id_type Key )
        {
            return Instance( ).Registry::GetInfo( Key );
        }

        inline RegistrySingleton::id_type
        RegistrySingleton::Id( const ascii_key_type& Key )
        {
            return Instance( ).Registry::Id( Key );
        }

        inline void
        RegistrySingleton::OnDirectoryClose( DirectoryScanData& Data )
        {
            Instance( ).Registry::OnDirectoryClose( Data );
        }

        inline RegistrySingleton::id_type
        RegistrySingleton::Register( const Info& Key )
        {
            return Instance( ).Registry::Register( Key );
        }

        inline bool
        RegistrySingleton::ScanForMatch( DirectoryScanData& Data )
        {
            return Instance( ).Registry::ScanForMatch( Data );
        }

        inline void
        RegistrySingleton::TranslateQuery( const QueryParams& Params,
                                           QueryAnswer&       Answer )
        {
            Instance( ).Registry::TranslateQuery( Params, Answer );
        }

    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE_API__CACHE__REGISTRY_SINGLETON_HH */
