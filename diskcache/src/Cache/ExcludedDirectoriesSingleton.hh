//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE__CACHE__EXCLUDED_DIRECTORIES_SINGLETON_HH
#define DISKCACHE__CACHE__EXCLUDED_DIRECTORIES_SINGLETON_HH

#include "ldastoolsal/ReadWriteSingleton.hh"

#include "genericAPI/Logging.hh"

#include "ExcludedDirectories.hh"

namespace diskCache
{
    namespace Cache
    {
        class ExcludedDirectoriesSingleton : private ExcludedDirectories
        {
        public:
            typedef ExcludedDirectories::directory_container_type
                                                       directory_container_type;
            typedef ExcludedDirectories::value_type    value_type;
            typedef ExcludedDirectories::callback_type callback_type;

            static bool IsExcluded( const value_type& Directory );

            static void OnUpdate( callback_type Callback );

            static void Update( const directory_container_type& Directories );

        private:
        public:
            DECLARE_READ_WRITE_SINGLETON( ExcludedDirectoriesSingleton );
        };

        inline bool
        ExcludedDirectoriesSingleton::IsExcluded( const value_type& Directory )
        {
            return Instance( ).ExcludedDirectories::IsExcluded( Directory );
        }

        inline void
        ExcludedDirectoriesSingleton::OnUpdate( callback_type Callback )
        {
            Instance( ).ExcludedDirectories::OnUpdate( Callback );
        }

        inline void
        ExcludedDirectoriesSingleton::Update(
            const directory_container_type& Directories )
        {
            Instance( ).ExcludedDirectories::Update( Directories );
        }

    } // namespace Cache
} // namespace diskCache

#endif /* DISKCACHE__CACHE__EXCLUDED_DIRECTORIES_SINGLETON_HH */
