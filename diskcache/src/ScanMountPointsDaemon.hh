//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef DISKCACHE_API__SCAN_MOUNT_POINTS_DAEMON_HH
#define DISKCACHE_API__SCAN_MOUNT_POINTS_DAEMON_HH

#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Task.hh"
#include "ldastoolsal/types.hh"

namespace diskCache
{
    //====================================================================================
    /// \brief Continuous scanning of the mount point lists
    //====================================================================================
    class ScanMountPointsDaemon : public LDASTools::AL::Task
    {
    public:
        typedef INT_4U interval_type;

        ScanMountPointsDaemon( std::ostream* Stream );

        ~ScanMountPointsDaemon( );

        static interval_type Interval( );

        static void Interval( interval_type Value );

        virtual void operator( )( );

    private:
        //-----------------------------------------------------------
        //-----------------------------------------------------------
        bool active;

        //-----------------------------------------------------------
        /// \brief Syncronize variable access
        //-----------------------------------------------------------
        static LDASTools::AL::MutexLock::baton_type m_variable_baton;

        //-----------------------------------------------------------
        /// \brief Delay between scans
        //-----------------------------------------------------------
        static interval_type m_interval;

        bool is_active( ) const;
    };

    inline ScanMountPointsDaemon::interval_type
    ScanMountPointsDaemon::Interval( )
    {
        LDASTools::AL::MutexLock l( m_variable_baton, __FILE__, __LINE__ );

        return m_interval;
    }

    inline void
    ScanMountPointsDaemon::Interval( interval_type Value )
    {
        LDASTools::AL::MutexLock l( m_variable_baton, __FILE__, __LINE__ );

        m_interval = Value;
    }

    inline bool
    ScanMountPointsDaemon::is_active( ) const
    {
        LDASTools::AL::MutexLock l( m_variable_baton, __FILE__, __LINE__ );

        return active;
    }
} // namespace diskCache
#endif /* DISKCACHE_API__SCAN_MOUNT_POINTS_DAEMON_HH */
