//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <boost/filesystem.hpp>

#include "genericAPI/Logging.hh"
#include "genericAPI/LDASplatform.hh"
#include "genericAPI/MonitorMemory.hh"
#include "genericAPI/Stat.hh"

#include "diskcacheAPI/Common/Variables.hh"
#include "diskcacheAPI/Cache/ExcludedDirectoriesSingleton.hh"
#include "diskcacheAPI/Cache/ExcludedPattern.hh"
#include "diskcacheAPI/Cache/HotDirectory.hh"
#include "diskcacheAPI/Variables.hh"

#include "ScanMountPointsDaemon.hh"

namespace
{
    struct format_hex_4_bytes
    {
    };
} // namespace

namespace std
{
    std::ostream&
    operator<<( std::ostream&             OutputStream,
                const format_hex_4_bytes& Formatter )
    {
        OutputStream << "0x";
        OutputStream.fill( '0' );
        OutputStream.width( 4 );
        OutputStream << std::uppercase << std::hex;
        return ( OutputStream );
    }
} // namespace std

namespace diskCache
{
    void
    Variables::ParseConfigurationFile( std::istream& Stream )
    {
        Config_ config_parser( *this );
        ParseConfigurationFile( Stream, config_parser );
    }

    void
    Variables::ParseConfigurationFile( std::istream& Stream,
                                       Config_&      ConfigParser )
    {
        ConfigParser.Parse( Stream );
    }

    //-------------------------------------------------------------------
    /// Setup the variables according to the requested configuration.
    /// Some of the variables can be reset by modifying the
    /// configuration file and then signaling the daemon process.
    //-------------------------------------------------------------------
    void
    Variables::Setup( int Mask ) const
    {
        using diskCache::Commands::updateFileExtList;
        using diskCache::Commands::updateMountPtList;

        if ( boost::any_cast< std::string >(
                 diskCache::Common::Variables::Get( VAR_NAME_LOG_DIRECTORY ) )
                 .length( ) > 0 )
        {
            boost::filesystem::create_directories(
                boost::any_cast< std::string >(
                    diskCache::Common::Variables::Get(
                        VAR_NAME_LOG_DIRECTORY ) ) );
        }
        QUEUE_LOG_MESSAGE( VAR_NAME_LOG
                               << " set to: "
                               << boost::any_cast< std::string >(
                                      diskCache::Common::Variables::Get(
                                          VAR_NAME_LOG ) ),
                           MT_NOTE,
                           0,
                           "setup_variables",
                           "CXX" );
        QUEUE_LOG_MESSAGE( VAR_NAME_LOG_DIRECTORY
                               << " set to: "
                               << boost::any_cast< std::string >(
                                      diskCache::Common::Variables::Get(
                                          VAR_NAME_LOG_DIRECTORY ) ),
                           MT_NOTE,
                           0,
                           "setup_variables",
                           "CXX" );
        QUEUE_LOG_MESSAGE( VAR_NAME_LOG_ARCHIVE_DIRECTORY
                               << " set to: "
                               << boost::any_cast< std::string >(
                                      diskCache::Common::Variables::Get(
                                          VAR_NAME_LOG_ARCHIVE_DIRECTORY ) ),
                           MT_NOTE,
                           0,
                           "setup_variables",
                           "CXX" );
        QUEUE_LOG_MESSAGE( VAR_NAME_LOG_FORMAT
                               << " set to: "
                               << boost::any_cast< std::string >(
                                      diskCache::Common::Variables::Get(
                                          VAR_NAME_LOG_FORMAT ) ),
                           MT_NOTE,
                           0,
                           "setup_variables",
                           "CXX" );
        QUEUE_LOG_MESSAGE(
            VAR_NAME_LOG_ROTATE_ENTRY_COUNT
                << " set to: "
                << boost::any_cast< int >( diskCache::Common::Variables::Get(
                       VAR_NAME_LOG_ROTATE_ENTRY_COUNT ) ),
            MT_NOTE,
            0,
            "setup_variables",
            "CXX" );
        QUEUE_LOG_MESSAGE(
            VAR_NAME_LOG_DEBUG_LEVEL
                << " set to: "
                << boost::any_cast< int >( diskCache::Common::Variables::Get(
                       VAR_NAME_LOG_DEBUG_LEVEL ) ),
            MT_NOTE,
            0,
            "setup_variables",
            "CXX" );
        if ( Mask & VAR_DIRECTORY_TIMEOUT )
        {
            //---------------------------------------------------------------
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set( VAR_NAME_DIRECTORY_TIMEOUT );
        }
        if ( Mask & VAR_EXCLUDED_DIRECTORIES )
        {
            //---------------------------------------------------------------
            // Update the list of directories to be excluded
            //---------------------------------------------------------------
            Cache::ExcludedDirectoriesSingleton::Update( excluded_directories );
        }
        if ( Mask & VAR_EXCLUDED_PATTERNS )
        {
            //---------------------------------------------------------------
            // Update the list of directories to be excluded
            //---------------------------------------------------------------
            Cache::ExcludedPattern::Update( excluded_patterns );
        }
        if ( ( Mask & VAR_EXTENSIONS ) && ( extensions.size( ) > 0 ) )
        {
            //---------------------------------------------------------------
            // Add the extensions
            //---------------------------------------------------------------
            {
                QUEUE_LOG_MESSAGE( "Adding extension: " << extensions.size( ),
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
            }
            updateFileExtList( extensions );
        }

        if ( ( Mask & VAR_MOUNT_POINTS ) && ( mount_points.size( ) > 0 ) )
        {
            //---------------------------------------------------------------
            // Add the mount points
            //---------------------------------------------------------------
            {
                QUEUE_LOG_MESSAGE(
                    "Adding mount points: " << mount_points.size( ),
                    MT_NOTE,
                    0,
                    "setup_variables",
                    "CXX" );
            }
            MountPointManagerSingleton::UpdateResults status;

            updateMountPtList( status, mount_points, false );
            {
                QUEUE_LOG_MESSAGE(
                    "Added mount points status:"
                        << " added: " << status.s_added.size( )
                        << " deleted: " << status.s_deleted.size( )
                        << " errors: " << status.s_errors.size( ),
                    MT_NOTE,
                    0,
                    "setup_variables",
                    "CXX" );
            }
        }

        if ( Mask & VAR_CONCURRENCY )
        {
            //---------------------------------------------------------------
            // Setup scanning parameters
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set( VAR_NAME_CONCURRENCY );
        }

        if ( Mask & VAR_HOT_DIRECTORY_AGE )
        {
            //---------------------------------------------------------------
            // Setup scanning parameters
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set( VAR_NAME_HOT_DIRECTORY_AGE );
        }

        if ( Mask & VAR_HOT_DIRECTORY_SCAN_INTERVAL )
        {
            //---------------------------------------------------------------
            // Setup scanning parameters
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set(
                VAR_NAME_HOT_DIRECTORY_SCAN_INTERVAL );
        }

        if ( Mask & VAR_RWLOCK_INTERVAL )
        {
            //---------------------------------------------------------------
            // Setup scanning parameters
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set( VAR_NAME_RWLOCK_INTERVAL );
        }

        if ( Mask & VAR_RWLOCK_TIMEOUT )
        {
            //---------------------------------------------------------------
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set( VAR_NAME_RWLOCK_TIMEOUT );
        }

        if ( Mask & VAR_SCAN_INTERVAL )
        {
            //---------------------------------------------------------------
            // Setup scanning parameters
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set( VAR_NAME_SCAN_INTERVAL );
        }

        if ( Mask & VAR_STAT_TIMEOUT )
        {
            //---------------------------------------------------------------
            // Setup stat timeout value
            //---------------------------------------------------------------
            diskCache::Common::Variables::Set( VAR_NAME_STAT_TIMEOUT );
        }

        //-----------------------------------------------------------------
        // Update the cache files
        //-----------------------------------------------------------------
        if ( Mask & VAR_CACHE_WRITE_DELAY )
        {
            QUEUE_LOG_MESSAGE(
                "Setting CACHE_WRITE_DELAY: " << boost::any_cast< std::string >(
                    diskCache::Common::Variables::Cache(
                        Variables::VAR_NAME_CACHE_WRITE_DELAY_SECS ) ),
                MT_NOTE,
                0,
                "setup_variables",
                "CXX" );
            diskCache::Common::Variables::Set(
                Variables::VAR_NAME_CACHE_WRITE_DELAY_SECS );
        }

        if ( Mask & VAR_OUTPUT_ASCII )
        {
            auto value = boost::any_cast< std::string >(
                diskCache::Common::Variables::Cache(
                    Variables::VAR_NAME_OUTPUT_ASCII ) );
            if ( !value.empty( ) )
            {
                QUEUE_LOG_MESSAGE( "Setting OUTPUT_ASCII: " << value,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                diskCache::Common::Variables::Set(
                    Variables::VAR_NAME_OUTPUT_ASCII );
            }
        }
        if ( Mask & VAR_OUTPUT_BINARY )
        {
            auto value = boost::any_cast< std::string >(
                diskCache::Common::Variables::Cache(
                    Variables::VAR_NAME_OUTPUT_BINARY ) );
            if ( !value.empty( ) )
            {
                QUEUE_LOG_MESSAGE( "Setting OUTPUT_BINARY: " << value,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                diskCache::Common::Variables::Set(
                    Variables::VAR_NAME_OUTPUT_BINARY );
            }
        }
        if ( Mask & VAR_VERSION_ASCII )
        {
            auto ascii_version =
                boost::any_cast< diskCache::DumpCacheDaemon::version_type >(
                    diskCache::Common::Variables::Get(
                        Variables::VAR_NAME_OUTPUT_ASCII_VERSION ) );
            if ( ascii_version != Streams::Interface::VERSION_NONE )
            {
                QUEUE_LOG_MESSAGE( "Setting " << VAR_NAME_OUTPUT_ASCII_VERSION
                                              << ": " << format_hex_4_bytes( )
                                              << ascii_version << std::dec,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                diskCache::Common::Variables::Set(
                    Variables::VAR_NAME_OUTPUT_ASCII_VERSION );
            }
        }
        if ( Mask & VAR_VERSION_BINARY )
        {
            auto version =
                boost::any_cast< diskCache::DumpCacheDaemon::version_type >(
                    diskCache::Common::Variables::Get(
                        Variables::VAR_NAME_OUTPUT_BINARY_VERSION ) );
            if ( version != Streams::Interface::VERSION_NONE )
            {
                QUEUE_LOG_MESSAGE( "Setting " << VAR_NAME_OUTPUT_BINARY_VERSION
                                              << ": " << format_hex_4_bytes( )
                                              << version << std::dec,
                                   MT_NOTE,
                                   0,
                                   "setup_variables",
                                   "CXX" );
                diskCache::Common::Variables::Set(
                    Variables::VAR_NAME_OUTPUT_BINARY_VERSION );
            }
        }
    }
    //===================================================================
    // Variables::Config_
    //===================================================================
    Variables::Config_::Config_( Variables& Storage )
        : storage( Storage ), state( NON_BLOCK )
    {
    }

    void
    Variables::Config_::Parse( std::istream& Stream )
    {
        base_type::Parse( Stream );
    }

    void
    Variables::Config_::ParseBlock( const std::string& Value )
    {
        if ( Value.compare( "DAEMON" ) == 0 )
        {
            state = BLOCK_DAEMON;
        }
        else if ( Value.compare( "EXTENSIONS" ) == 0 )
        {
            state = BLOCK_EXTENSIONS;
        }
        else if ( Value.compare( "EXCLUDED_DIRECTORIES" ) == 0 )
        {
            state = BLOCK_EXCLUDED_DIRECTORIES;
        }
        else if ( Value.compare( VAR_NAME_EXCLUDED_PATTERNS ) == 0 )
        {
            storage.reset_excluded_patterns( );
            state = BLOCK_EXCLUDED_PATTERNS;
        }
        else if ( Value.compare( "MOUNT_POINTS" ) == 0 )
        {
            state = BLOCK_MOUNT_POINTS;
        }
        else
        {
            //---------------------------------------------------------------
            // Tried everything we know. Flag as unknown block
            //---------------------------------------------------------------
            state = BLOCK_UNKNOWN;
        }
    }

    void
    Variables::Config_::ParseKeyValue( const std::string& Key,
                                       const std::string& Value )
    {
        static constexpr const char* method_name{
            "Variables::Config_::ParseKeyValue"
        };
        switch ( state )
        {
        case NON_BLOCK:
        case BLOCK_DAEMON:
            try
            {
                diskCache::Common::Variables::Cache( Key, Value );
                return;
            }
            catch ( const std::invalid_argument& Error )
            {
                //-------------------------------------------------------------
                // This is the case where the variable is NOT handled by
                //   the generic routine.
                //-------------------------------------------------------------
                QUEUE_LOG_MESSAGE( "Unable to process key: " << Key,
                                   MT_NOTE,
                                   0,
                                   method_name,
                                   "CXX" );
            }
            break;
        case BLOCK_EXCLUDED_DIRECTORIES:
        case BLOCK_EXCLUDED_PATTERNS:
        case BLOCK_EXTENSIONS:
        case BLOCK_MOUNT_POINTS:
            //---------------------------------------------------------------
            /// \todo
            ///     Need to produce an exception regarding parse error
            ///     in configuration file as a key/value pair was
            ///     encountered where a word value was expected.
            //---------------------------------------------------------------
            break;
        default:
            //---------------------------------------------------------------
            // Ignore all other cases
            //---------------------------------------------------------------
            break;
        }
    }

    void
    Variables::Config_::ParseWord( const std::string& Value )
    {
        if ( Value.size( ) <= 0 )
        {
            //---------------------------------------------------------------
            // Ignore empty words
            //---------------------------------------------------------------
            return;
        }
        //-----------------------------------------------------------------
        // Process the word according to the block it was found in
        //-----------------------------------------------------------------
        switch ( state )
        {
        case BLOCK_EXTENSIONS:
            storage.push_extension( Value );
            break;
        case BLOCK_EXCLUDED_DIRECTORIES:
            storage.push_excluded_directory( Value );
            break;
        case BLOCK_EXCLUDED_PATTERNS:
            storage.push_excluded_pattern( Value );
            break;
        case BLOCK_MOUNT_POINTS:
            storage.push_mount_point( Value );
            break;
        case BLOCK_DAEMON:
        case BLOCK_UNKNOWN:
        case NON_BLOCK:
            // This shouild never happen.
            /// \todo
            /// Have Variables::Config_::ParseWord throw exception
            /// if it reaches an unreachable state.
            break;
        }
    }

    diskCache::DumpCacheDaemon::version_type
    Variables::GetOutputASCIIVersion( ) const
    {
        return ( boost::any_cast< diskCache::DumpCacheDaemon::version_type >(
            diskCache::Common::Variables::Get(
                diskCache::Variables::VAR_NAME_OUTPUT_ASCII_VERSION ) ) );
    }

    diskCache::DumpCacheDaemon::version_type
    Variables::GetOutputBinaryVersion( ) const
    {
        return ( boost::any_cast< diskCache::DumpCacheDaemon::version_type >(
            diskCache::Common::Variables::Get(
                diskCache::Variables::VAR_NAME_OUTPUT_BINARY_VERSION ) ) );
    }

} // namespace diskCache
