//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

//-----------------------------------------------------------------------
///
/// \brief Write content of global frame data hash to ascii file.
///
/// \param[in] bfilename
///     Name of the file to write the binary frame hash to.
///     Default is NULL (C++ will use default file name).
///
/// \param[in] afilename
///     Name of the file to write the ascii frame hash to.
///     Default is NULL (C++ will use default file name).
///
//-----------------------------------------------------------------------
void
writeDirCacheFilesInParallel( const char* bfilename, const char* afilename )
{
    // Debug timer
    Timer tracker;
    tid*  binary_tid = writeDirCache_t( bfilename, (Tcl_Interp*)NULL, "" );
    tid*  ascii_tid = writeDirCacheAscii_t( afilename, (Tcl_Interp*)NULL, "" );

    binary_tid->Join( ); // Syncronize point
    ascii_tid->Join( ); // Syncronize point

    writeDirCache_r( binary_tid ); // Cleanup
    writeDirCacheAscii_r( ascii_tid ); // Cleanup

    tracker.getDelta( "writeDirCacheFilesInParallel()" );

    return;
}

#if defined( CREATE_THREADED1 )
CREATE_THREADED1( getDirEnt, std::string, const std::string )
#endif /* defined( CREATE_THREADED1 ) */

#if FOR_LDAS
CREATE_THREADED4( getIntervalsList,
                  std::string,
                  const char*,
                  const unsigned int,
                  const unsigned int,
                  const std::string& );
#endif /* ! FOR_PYTHON && ! FOR_TCL */

#if defined( CREATE_THREADED2V )
CREATE_THREADED2V( writeDirCache,
                   const char*,
                   diskCache::Streams::Interface::version_type )
#endif /* defined( CREATE_THREADED1V */

#if defined( CREATE_THREADED2V )
CREATE_THREADED2V( writeDirCacheAscii,
                   const char*,
                   diskCache::Streams::Interface::version_type )
#endif /* define( CREATE_THREADED1V ) */

#if defined( CREATE_THREADED2V )
CREATE_THREADED2V( writeDirCacheFiles, const char*, const char* )
#endif /* defined( CREATE_THREADED2V ) */

#if !FOR_PYTHON
CREATE_THREADED1V( CacheRead, const char* )
#endif /* ! FOR_PYTHON */

#if defined( CREATE_THREADED0V )
CREATE_THREADED0V( deleteDirCache )
#endif /* defined( CREATE_THREADED0V ) */
