//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <diskcache_config.h>

#include <fstream>
#include <iomanip>

#include "diskcacheAPI/Streams/ASCII.hh"
#include "diskcacheAPI/Streams/Binary.hh"
#include "diskcacheAPI/Streams/FStream.hh"

#include "IO.hh"
#include "MetaCommands.hh"

typedef diskCache::MetaCommand::ClientServerInterface::ServerInfo ServerInfo;

using diskCache::MetaCommand::transfer_helper;

namespace
{
    class transfer : public transfer_helper
    {
    public:
        //-------------------------------------------------------------------
        /// \brief Default constructor
        //-------------------------------------------------------------------
        transfer( );

        //-------------------------------------------------------------------
        /// \brief read the reponce from the stream
        ///
        /// \param[in] Stream
        ///     The output stream from which to read the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream from which the responce was read.
        //-------------------------------------------------------------------
        std::istream& read( std::istream& Stream );

        //-------------------------------------------------------------------
        /// \brief write the reponce onto the stream
        ///
        /// \param[in] Stream
        ///     The output stream on which to write the responce to the
        ///     request.
        ///
        /// \return
        ///     The stream on which the responce was written.
        //-------------------------------------------------------------------
        std::ostream& write( std::ostream& Stream );

        std::stringstream answer;
    };
} // namespace

namespace diskCache
{
    namespace MetaCommand
    {
        //===================================================================
        // Reconfigure
        //===================================================================
        OptionSet& Reconfigure::m_options( Reconfigure::init_options( ) );

        OptionSet&
        Reconfigure::init_options( )
        {
            static OptionSet   retval;
            std::ostringstream synopsis;
            std::ostringstream summary;

            synopsis << "Subcommand: "
                     << CommandTable::Lookup( CommandTable::CMD_RECONFIGURE );
            summary << "The "
                    << CommandTable::Lookup( CommandTable::CMD_RECONFIGURE )
                    << " sub command is intended to terminate the daemon.";

            retval.Synopsis( synopsis.str( ) );
            retval.Summary( summary.str( ) );

            return retval;
        }

        Reconfigure::Reconfigure(
            CommandLineOptions&                      Args,
            const ClientServerInterface::ServerInfo& Server )
            : ClientServerInterface( Server ), m_args( Args )
        {
            if ( m_args.empty( ) == false )
            {
                //---------------------------------------------------------------
                // Parse the commands
                //---------------------------------------------------------------
                std::string arg_name;
                std::string arg_value;
                bool        parsing( true );

                while ( parsing )
                {
                    switch ( m_args.Parse( m_options, arg_name, arg_value ) )
                    {
                    case CommandLineOptions::OPT_END_OF_OPTIONS:
                        parsing = false;
                        break;
                    default:
                        break;
                    }
                }
            }
        }

        void
        Reconfigure::File( const std::string& Filename )
        {
            filename = Filename;
        }

        const OptionSet&
        Reconfigure::Options( )
        {
            return m_options;
        }

        void
        Reconfigure::evalClient( )
        {
            static const char* caller =
                "diskCache::MetaCommand::Reconfigure::evalClient";

            std::ostringstream cmd;

            QUEUE_LOG_MESSAGE( "Entry", MT_NOTE, 0, caller, "CXX" );
            cmd << CommandTable::Lookup( CommandTable::CMD_RECONFIGURE )
                << std::endl;

            ServerRequest( cmd.str( ) );

            transfer responce;

            responce.read( *( serverRequestHandle( ) ) );

            std::cout << responce.answer.str( ) << std::endl;

            QUEUE_LOG_MESSAGE( "Exit", MT_DEBUG, 10, caller, "CXX" );
        }

        void
        Reconfigure::evalServer( )
        {
            static const char* caller =
                "diskCache::MetaCommand::Reconfigure::evalServer";

            transfer responce;

            QUEUE_LOG_MESSAGE( "Called", MT_DEBUG, 10, caller, "CXX" );

            responce.answer << "Rereading configuration file: " << filename;
            responce.write( *( clientHandle( ) ) );
        }

        void
        Reconfigure::evalStandalone( )
        {
            static const char* caller =
                "diskCache::MetaCommand::Reconfigure::evalStandalone";

            //-----------------------------------------------------------------
            // Standalone mode
            //-----------------------------------------------------------------
            QUEUE_LOG_MESSAGE( "Called", MT_DEBUG, 10, caller, "CXX" );
        }

    } // namespace MetaCommand
} // namespace diskCache

namespace
{
    //===================================================================
    // transfer
    //===================================================================
    transfer::transfer( )
    {
    }

    std::istream&
    transfer::read( std::istream& Stream )
    {
        bool available;

        Blob( Stream, available, answer );

        return Stream;
    }

    std::ostream&
    transfer::write( std::ostream& Stream )
    {
        Blob( Stream, true, answer );
        Stream.flush( );
        return Stream;
    }
} // namespace
