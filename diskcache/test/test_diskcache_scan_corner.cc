//
// LDASTools diskcache - Tools for querying a collection of files on disk
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools diskcache is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools diskcache is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <unistd.h>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "ldastoolsal/ldas_types.h"
#include "ldastoolsal/unittest.h"

#include "genericAPI/LDASplatform.hh"
#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

LDASTools::Testing::UnitTest Test;

static void
mkdir( const char* Dir )
{
    std::ostringstream cmd;
    cmd << "mkdir -p " << Dir;
    if ( system( cmd.str( ).c_str( ) ) )
    {
        std::cerr << "WARN: unable to " << cmd.str( ) << std::endl;
    }
}

void
touch( const char* Dir, const char* Desc, INT_8U Time, INT_4U Dt )
{
    std::ostringstream cmd;
    cmd << "touch " << Dir << "/Z-" << Desc << "-" << Time << "-" << Dt
        << ".gwf";
    if ( system( cmd.str( ).c_str( ) ) )
    {
        std::cerr << "WARN: unable to " << cmd.str( ) << std::endl;
    }
}

void
data_set_lead( )
{
    static const char* dir = "corner_data/lead";
    static const char* desc = "Lead";

    std::ostringstream cmd;

    mkdir( dir );

    touch( dir, desc, 1000000064, 64 );
    touch( dir, desc, 1000000128, 64 );
    touch( dir, desc, 1000000192, 32 );
    touch( dir, desc, 1000000256, 32 );
}

void
data_set_mid( )
{
    static const char* dir = "corner_data/mid";
    static const char* desc = "Mid";

    std::ostringstream cmd;

    mkdir( dir );

    touch( dir, desc, 1000000000, 64 );
    touch( dir, desc, 1000000064, 64 );
    touch( dir, desc, 1000000128, 32 );
    touch( dir, desc, 1000000224, 32 );
    touch( dir, desc, 1000000256, 64 );
    touch( dir, desc, 1000000320, 64 );
}

void
data_set_tail( )
{
    static const char* dir = "corner_data/tail";
    static const char* desc = "Tail";

    std::ostringstream cmd;

    mkdir( dir );

    touch( dir, desc, 1000000000, 32 );
    touch( dir, desc, 1000000096, 32 );
    touch( dir, desc, 1000000128, 64 );
    touch( dir, desc, 1000000192, 64 );
}

bool
result_parse( const char* Source )
{
    bool   retval = true;
    char   directory[ 1024 ];
    char   ifo;
    char   desc[ 1024 ];
    INT_4U one;
    INT_4U dt;
    INT_8U param_6;
    INT_4U file_count;
    ;
    std::string ranges;

    std::string buffer( Source );

    for ( std::string::iterator cur = buffer.begin( ), last = buffer.end( );
          cur != last;
          ++cur )
    {
        if ( *cur == ',' )
        {
            *cur = ' ';
        }
    }

    std::string::size_type range_start_pos = buffer.rfind( '{' );
    std::string::size_type range_stop_pos = buffer.rfind( '}' );

    ++range_start_pos;

    ranges = buffer.substr( range_start_pos, range_stop_pos - range_start_pos );

    sscanf( buffer.c_str( ),
            "%s %c %s %u %u %lu %u {",
            directory,
            &ifo,
            desc,
            &one,
            &dt,
            &param_6,
            &file_count );

    INT_8U             range_start, range_stop;
    std::istringstream ranges_stream( ranges );

    while ( ranges_stream >> range_start >> range_stop )
    {
        for ( INT_8U cur = range_start, last = range_stop; cur < last;
              cur += dt )
        {
            std::ostringstream filename;

            filename << directory << "/" << ifo << "-" << desc << "-" << cur
                     << "-" << dt << ".gwf";
            std::ifstream s( filename.str( ).c_str( ) );

            if ( s.is_open( ) == false )
            {
                Test.Message( 10 )
                    << "Failed to open file: " << filename.str( ) << std::endl;
                retval = false;
            }
        }
    }
    return retval;
}

void
validate_cache( )
{
    //---------------------------------------------------------------------
    // Run test and verify results
    //---------------------------------------------------------------------
    std::ostringstream cmd;
    static char        buffer[ 1024 ];
    bool               success = true;

    cmd << "../src/diskcache"
        << " scan"
        << " --log-directory -"
        << " --log-debug-level " << Test.Verbosity( ) << " --output-ascii -"
        << " --mount-points `pwd`/corner_data"
        << " --extensions gwf 2>/dev/null";
    FILE* stream = popen( cmd.str( ).c_str( ), "r" );
    while ( fgets( buffer, sizeof( buffer ), stream ) != (char*)NULL )
    {
        success = ( success && result_parse( buffer ) );
    }
    pclose( stream );
    Test.Check( success ) << "corner_data cache" << std::endl;
}

int
main( int ArgC, char** ArgV )
{
    //---------------------------------------------------------------------
    // Initialize the test structure
    //---------------------------------------------------------------------
    Test.Init( ArgC, ArgV );

    GenericAPI::LoggingInfo::LogDirectory( "-" );
    GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
    GenericAPI::LDASplatform::AppName( "test_createRDS_cpp" );

    if ( Test.Verbosity( ) > 0 )
    {
        GenericAPI::setLogDebugLevel( Test.Verbosity( ) );
    }

    //---------------------------------------------------------------------
    // Reset for the running of the test
    //---------------------------------------------------------------------
    if ( system( "rm -rf corner_data" ) )
    {
        std::cerr << "WARN: system rm failed" << std::endl;
    }

    //---------------------------------------------------------------------
    // Create some data sets
    //---------------------------------------------------------------------
    data_set_lead( );
    data_set_mid( );
    data_set_tail( );
    validate_cache( );

    //---------------------------------------------------------------------
    // Exit with the appropriate exit status
    //---------------------------------------------------------------------
    Test.Exit( );
    exit( 1 );
}
