# Release 1.1.1 - October 23, 2019
  - Modified cx_check_sizes() to not insert extra semicolon for 32-bit
    use of long long (Closes #83)
  - Added cx_ldas_prog_cxx() which checks some compiler characteristics.
  - Added ci_cache_list() to provide CACHE lists.
  - Renamed cm_cache() to ci_cache() and provided cm_cache() as alias
    for backwards compatibility

# Release 1.1.0 - Septempber 26, 2019
  - Moved the installation of .pc file from /usr/lib to /usr/share

# Release 1.0.8 - August 14, 2019
  - Added function cm_header_stdc
  - Added function cx_thread
  - Added function cx_lib_socket_nsl
  - Corrections to function cm_try_compile
  - Corrections to function cm_search_libs
  - Corrections to function cm_func_strerror_r
  - Corrections to function cx_check_pthread_read_write_lock
  - Corrected Portfile to have proper description (Closes #55)

# Release 1.0.7 - February  2, 2019
  - Corrected cm_init to provide Autotools defines as strings
  - Added cx_target_test
  - Moved most cm_ functions from Autotools/ArchiveX to Autotools

# Release 1.0.6 - January  8, 2019
  - Addeed EXTEND_SWIG_FLAGS to cx_swig_python_module

# Release 1.0.5 - December 6, 2018
  - Allow quieting of debug messages
  - For OSX shared libraries, use only the major number
  - Addressed packaging issues

# Release 1.0.4 - October 22, 2018
  - Added cx_target_pkgconfig

# Release 1.0.3 - August 27, 2018
  - Enhancements to further support for Pythyon 3

# Release 1.0.2 - June 22, 2018
  - Corrected Description syntax inside of debian/control (closes ticket #10)

# Release 1.0.2 - June 22, 2018
  - Corrected Description syntax inside of debian/control (closes ticket #10)

# Release 1.0.1 - June 19, 2018
  - Initial Release

