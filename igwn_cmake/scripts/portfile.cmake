#------------------------------------------------------------------------
# This command will take a Portfile template and do all substitutions
#
# Input Parameters:
#   DESCRIPTION         - Contains the project's short description
#   TEMPLATE            - Template Portfile
#   OUTPUT_DIR          - Where to store generated Portfile
#   SOURCE_DISTRIBUTION - Source package distribution
#------------------------------------------------------------------------

#------------------------------------------------------------------------
# Retrieve contents of the template
#------------------------------------------------------------------------
file( READ ${TEMPLATE} portfile_ )
#------------------------------------------------------------------------
# Calculate needed hashes
#------------------------------------------------------------------------
if ( portfile_ MATCHES ".*@SHA256@.*" )
  file( SHA256 ${SOURCE_DISTRIBUTION} SHA256 )
endif( )
if ( portfile_ MATCHES ".*@RMD160@.*" )
  find_program( PROG_OPENSSL openssl )
  execute_process(
    COMMAND ${PROG_OPENSSL} rmd160 ${SOURCE_DISTRIBUTION}
    OUTPUT_VARIABLE RMD160
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_QUIET
    )
  string( REGEX REPLACE "^[^=]*= " "" RMD160 "${RMD160}" )
endif( )
if ( portfile_ MATCHES ".*@SIZE@.*")
  find_program( PROG_WC wc )
  execute_process(
    COMMAND ${PROG_WC} -c ${SOURCE_DISTRIBUTION}
    OUTPUT_VARIABLE SIZE
    OUTPUT_STRIP_TRAILING_WHITESPACE
    ERROR_QUIET
    )
  string( REGEX REPLACE "^[ \t]*([0-9]+)[ \t].*$" "\\1" SIZE "${SIZE}" )
endif( )
#------------------------------------------------------------------------
# Tweek some values
#------------------------------------------------------------------------
set( spaces_ "long_description  " )
string( REGEX REPLACE "." " " spaces_ "${spaces_}" )
string( REPLACE "\n" ";"
  PROJECT_DESCRIPTION_LONG
  "${PROJECT_DESCRIPTION_LONG}"
  )
string( REPLACE ";" " \\\n${spaces_}"
  PROJECT_DESCRIPTION_LONG
  "${PROJECT_DESCRIPTION_LONG}"
  )
#------------------------------------------------------------------------
# Do substitutions
#------------------------------------------------------------------------
string( CONFIGURE "${portfile_}" portfile_ @ONLY )
#------------------------------------------------------------------------
# Create the Portfile
#------------------------------------------------------------------------
file( WRITE ${OUTPUT_DIR}/Portfile ${portfile_} )
