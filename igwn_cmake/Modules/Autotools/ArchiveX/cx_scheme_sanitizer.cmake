#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_target_abi_check( )
#
#------------------------------------------------------------------------
include( CMakeParseArguments )
include( CheckCXXCompilerFlag )

include( Autotools/cm_msg_error )
include( Autotools/ArchiveX/cx_msg_debug )
include( Autotools/ArchiveX/cx_msg_debug_variable )


function(cx_scheme_sanitizer)
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  get_property( languages GLOBAL PROPERTY ENABLED_LANGUAGES )
  list( FIND languages "CXX" cxx_offset)
  if ( cxx_offset EQUAL -1 )
    return( )
  endif( )
  if ( NOT DEFINED SANITIZER )
    set( SANITIZER "address" )
  endif( )
  cx_msg_debug( "Checking sanitizer: ${SANITIZER}" )
  if ( SANITIZER STREQUAL "none" )
    add_compile_options(
      "$<$<CONFIG:Debug>:-O0>"
      )
  else ( )
    set( CMAKE_REQUIRED_LIBRARIES -fsanitize=${SANITIZER} -fno-omit-frame-pointer )
    check_cxx_compiler_flag( -fsanitize=${SANITIZER} CXX_COMPILER_SUPPORTS_SANITIZER )
    cx_msg_debug_variable( CMAKE_GENERATOR )
    if ( APPLE )
      file( GLOB XCODE_CHAIN_DIRS
        "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/*/lib/darwin"
        )
      list( APPEND ASAN_LIB_HINTS
        ${XCODE_CHAIN_DIRS}
        )
    endif( )
    cx_msg_debug_variable( ASAN_LIB_HINTS )
    if ( CXX_COMPILER_SUPPORTS_SANITIZER )
      set( SANITIZER_ENABLED $<$<CONFIG:DEBUG>:True> CACHE INTERNAL "" )
      find_library( ASAN_LIB
        NAMES clang_rt.asan_osx_dynamic
        HINTS ${ASAN_LIB_HINTS}
        PATHS ${ASAN_LIB_HINTS}
        )
      cx_msg_debug_variable( ASAN_LIB )
      add_compile_options(
        "$<$<CONFIG:Debug>:-fsanitize=${SANITIZER}>"
        "$<$<CONFIG:Debug>:-fno-omit-frame-pointer>"
        "$<$<CONFIG:Debug>:-O0>"
        )
      link_libraries(
        "$<$<CONFIG:Debug>:-fsanitize=${SANITIZER}>"
        "$<$<CONFIG:Debug>:-fno-omit-frame-pointer>"
        )
      cx_msg_debug( "Setting compile option: ${CXX_COMPILER_SUPPORT_SANITIZER}" )
    else( )
      cx_msg_debug( "Option not accepted: ${CXX_COMPILER_SUPPORT_SANITIZER}" )
    endif( )
  endif ( )
endfunction( )
