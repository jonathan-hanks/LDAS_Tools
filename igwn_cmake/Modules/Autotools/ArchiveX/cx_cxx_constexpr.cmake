# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_CXX_CONSTEXPR
#     Check if C++ supports constexpr (2011 standard)
# ---------------------------------------------------------------------
include(Autotools/cm_define)
include(Autotools/cm_msg_checking)

function( CX_CXX_CONSTEXPR )
  list(FIND "${CMAKE_CXX_COMPILE_FEATURES}" "cxx_constexpr" FOUND)
  if( ${FOUND} EQUAL -1 )
    #--------------------------------------------------------------------
    # Not in the list means it is not supported
    #--------------------------------------------------------------------
    set(HAVE_CXX_CONSTEXPR 0)
    set( constexpr "/* constexpr */ " )
  else( ${FOUND} EQUAL -1 )
    #--------------------------------------------------------------------
    # In the list means it is supported
    #--------------------------------------------------------------------
    set(HAVE_CXX_CONSTEXPR 1)
    unset( constexpr )
  endif( ${FOUND} EQUAL -1 )
  cm_define(
    VARIABLE HAVE_CXX_CONSTEXPR
    DESCRIPTION "Defined if C++ does not support constexpr" )
  cm_define(
    VARIABLE constexpr
    DESCRIPTION "Defined if C++ does support constexpr" )
  cm_msg_checking(
    "if the C++ compiler supports constexpr"
    CONDITION HAVE_CXX_CONSTEXPR )
endfunction( CX_CXX_CONSTEXPR )
