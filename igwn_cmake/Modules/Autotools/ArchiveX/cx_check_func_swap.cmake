# -*- mode: cmake -*-
# ---------------------------------------------------------------------
#  CX_CHECK_FUNC_SWAP
# ---------------------------------------------------------------------
include( Autotools/cm_define )
include( Autotools/cm_try_compile )

function(CX_CHECK_FUNC_SWAP FUNC VAR TYPE)
  set(inc "
#include <stdlib.h>
#if HAVE_BYTESWAP_H
#include <byteswap.h>
#else /* HAVE_BYTESWAP_H */
#include <sys/types.h>
#if HAVE_SYS_BYTEORDER_H
#include <sys/byteorder.h>
#endif /* HAVE_SYS_BYTEORDER_H */
#if HAVE_MACHINE_BSWAP_H
#include <machine/bswap.h>
#endif /* HAVE_MACHINE_BSWAP_H */
#endif /* HAVE_BYTESWAP_H */
#if HAVE_LIBKERN_OSBYTEORDER_H
#include <libkern/OSByteOrder.h>
#endif /* HAVE_LIBKERN_OSBYTEORDER_H */
")
  set(body "
#if SIZEOF_SHORT == 2
#define int16 short
#endif
#if SIZEOF_INT == 4
#define int32 int
#endif
#if SIZEOF_LONG == 8
#define int64 long
#elif SIZEOF_LONG_LONG == 8
#define int64 long long
#endif

unsigned ${TYPE} buf;
(void)${FUNC}( buf );
")
  cm_try_compile("${inc}" "${body}" ${VAR}
    DEFINES
      -DHAVE_BYTESWAP_H=${HAVE_BYTESWAP_H}
      -DHAVE_SYS_BYTEORDER_H=${HAVE_SYS_BYTEORDER_H}
      -DHAVE_MACHINE_BSWAP_H=${HAVE_MACHINE_BSWAP_H}
      -DHAVE_LIBKERN_OSBYTEORDER_H=${HAVE_LIBKERN_OSBYTEORDER_H}
      -DSIZEOF_SHORT=${SIZEOF_SHORT}
      -DSIZEOF_INT=${SIZEOF_INT}
      -DSIZEOF_LONG=${SIZEOF_LONG}
)
if(${VAR})
  set(result "yes")
else(${VAR})
  set(result "no")
endif(${VAR})
message( STATUS "Checking for ${FUNC} ... ${result}" )
cm_define(
  VARIABLE ${VAR}
  DESCRIPTION "Defined if ${FUNC} is available" )
endfunction(CX_CHECK_FUNC_SWAP)
