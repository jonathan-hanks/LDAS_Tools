#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

function(cx_link_headers)
  set(options
    BUILT_HEADERS
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  list( GET ARG_UNPARSED_ARGUMENTS 0 dest )
  list( REMOVE_AT ARG_UNPARSED_ARGUMENTS 0 )

  if ( ARG_BUILT_HEADERS )
    set( source_dir "${CMAKE_CURRENT_BINARY_DIR}" )
  else( )
    set( source_dir "${CMAKE_CURRENT_SOURCE_DIR}" )
  endif( )
  set( dest_dir "${CMAKE_BINARY_DIR}/include/${dest}" )
  execute_process(COMMAND ${CMAKE_COMMAND} -E make_directory ${dest_dir})
  foreach(f ${ARG_UNPARSED_ARGUMENTS})
    # message("Creating link for ${f}")
    execute_process(
      COMMAND ${CMAKE_COMMAND} -E
        create_symlink "${source_dir}/${f}" ${dest_dir}/${f})
  endforeach( )
endfunction( )
