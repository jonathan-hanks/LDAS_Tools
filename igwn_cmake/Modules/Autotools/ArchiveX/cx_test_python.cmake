#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug_variable )

function( _cp_python_unset )
  foreach( lib ${ARGN} )
    unset( ${lib} CACHE )
  endforeach( )
endfunction( )

function(cx_test_python)
  set( options )
  set( oneValueArgs
    NAME
    )
  set( multiValueArgs
    COMMAND
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  # list(GET ARGN 0 cmd )
  # list(REMOVE_AT ARGN 0)

  if( WIN32 )
    set(TPATH ${NDS_LIBRARY_DIR})
  else( WIN32 )
    set(TPATH "${CMAKE_CURRENT_BINARY_DIR}:$ENV{PATH}")
  endif( WIN32 )

  #----------------------------------------------------------------------
  # Establish environment variables for Python testing
  #----------------------------------------------------------------------
  list( APPEND env_ "PYTHONPATH=${TPATH}" )
  list( APPEND env_ "PYTHON=${ARG_PYTHON_EXECUTABLE}" )
  cx_msg_debug_variable( ASAN_LIB )
  if ( ASAN_LIB )
    if ( APPLE )
      set( preload_ "DYLD_INSERT_LIBRARIES" )
    else( )
      set( preload_ "LD_PRELOAD" )
    endif( )
    list( APPEND env_ "${preload_}=${ASAN_LIB}" )
  endif( )
  cx_msg_debug_variable( env_ )
  #----------------------------------------------------------------------
  # Setup actual test
  #----------------------------------------------------------------------
  cx_msg_debug_variable( ARG_NAME )
  cx_msg_debug_variable( ARG_COMMAND )

  add_test( ${ARG_NAME} "${PYTHON_EXECUTABLE}" ${ARG_COMMAND} )
  cx_msg_debug_variable( ASAN_LIB )
  set_tests_properties( ${ARG_NAME}
    PROPERTIES
      SKIP_RETURN_CODE 77
      ENVIRONMENT "${env_}"      )

endfunction(cx_test_python)

