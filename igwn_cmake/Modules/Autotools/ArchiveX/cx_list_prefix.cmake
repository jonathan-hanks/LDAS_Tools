#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
function(cx_list_prefix LIST_VARNAME PREFIX)
  foreach(element ${${LIST_VARNAME}})
    list(APPEND new_list "${PREFIX}${element}")
  endforeach( )
  set( ${LIST_VARNAME} ${new_list} PARENT_SCOPE )
endfunction( )
