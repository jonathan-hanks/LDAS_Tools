#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_reconfigure( ) - Add reconfigure target to force reconfiguration
#
#------------------------------------------------------------------------
include( CMakeParseArguments )

function(cx_reconfigure)
  #----------------------------------------------------------------------
  # Parse arguments
  #----------------------------------------------------------------------
  set(options
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  foreach( argv RANGE ${CMAKE_ARGC} )
    # message( STATUS "CMAKE_ARGV${argv}: ${CMAKE_ARGV${argv}}" )
    list( APPEND COMMAND_LINE ${CMAKE_ARGV${argv}} )
  endforeach( argv RANGE ${CMAKE_ARGC} )
  # message( STATUS "COMMAND_LINE: ${COMMAND_LINE}" )

  add_custom_target( reconfigure
    VERBATIM
    COMMAND ${COMMAND_LINE}
    COMMENT "reconfiguring using: ${COMMAND_LINE}" )
endfunction(cx_reconfigure)
