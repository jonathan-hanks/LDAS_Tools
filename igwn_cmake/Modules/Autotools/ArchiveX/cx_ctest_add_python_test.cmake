# -*- conding: utf-8; cmake-tab-width: 4; indent-tabs-mode: nil; -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug )
include( Autotools/ArchiveX/cx_msg_debug_variable )


function( cx_ctest_add_python_test )
    set(options
        )
    set(oneValueArgs
        PYTHON_EXECUTABLE
        PYTHON_MODULE_DIR
        TARGET )
    set(multiValueArgs
        COMMAND
        DEPENDS
        PYTHON_PATH )
    cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if ( NOT ARG_PYTHON_EXECUTABLE )
      if ( ( SANITIZER_ENABLED ) AND ( DEFINED PYTHON_SANITIZER_EXECUTABLE ) )
        set( ARG_PYTHON_EXECUTABLE ${PYTHON_SANITIZER_EXECUTABLE} )
      else( )
        set( ARG_PYTHON_EXECUTABLE ${PYTHON_EXECUTABLE} )
      endif( )
    endif( )
    if( WIN32 )
        if ( ARG_PYTHON_PATH )
            set( TPATH ${ARG_PYTHON_PATH} )
        endif( )
        if ( ARG_PYTHON_MODULE_DIR )
            set( TPATH "${ARG_PYTHON_MODULE_DIR};${TPATH}" )
        endif( )
    else( WIN32 )
        set( TPATH "$ENV{PATH}" )
        if ( ARG_PYTHON_PATH )
            string( REPLACE ";" ":" ARG_PYTHON_PATH "${ARG_PYTHON_PATH}" )
            set( TPATH "${ARG_PYTHON_PATH}:${TPATH}" )
        endif( )
        if ( ARG_PYTHON_MODULE_DIR )
            set( TPATH "${ARG_PYTHON_MODULE_DIR}:${TPATH}" )
        endif( )
    endif( WIN32 )

    #--------------------------------------------------------------------
    # Establish environment variables for Python testing
    #--------------------------------------------------------------------
    list( APPEND env_ "PYTHONPATH=${TPATH}" )
    list( APPEND env_ "PYTHON=${ARG_PYTHON_EXECUTABLE}" )
    cx_msg_debug_variable( ASAN_LIB )
    if ( ASAN_LIB )
      if ( APPLE )
        set( preload_ "DYLD_INSERT_LIBRARIES" )
      else( )
        set( preload_ "LD_PRELOAD" )
      endif( )
      list( APPEND env_ "${preload_}=${ASAN_LIB}" )
    endif( )
    cx_msg_debug_variable( env_ )
    #--------------------------------------------------------------------
    # Setup actual test
    #--------------------------------------------------------------------
    add_test(
        ${ARG_TARGET}
        "${ARG_PYTHON_EXECUTABLE}"
        ${ARG_COMMAND}
        )
    set_tests_properties( ${ARG_TARGET}
        PROPERTIES
        ENVIRONMENT "${env_}"
        SKIP_RETURN_CODE 77
        )
    cx_msg_debug( "/usr/bin/env ${env_} ${ARG_COMMAND}" )
    if ( ARG_DEPENDS )
        set_tests_properties( ${ARG_TARGET}
            PROPERTIES
            DEPENDS ${ARG_DEPENDS}
            )
    endif( )
    cx_msg_debug_variable( ${ARG_PYTHON_EXECUTABLE} )
    get_test_property( ${ARG_TARGET} ENVIRONMENT ${ARG_TARGET}_ENVIRONMENT )
    cx_msg_debug_variable( ${ARG_TARGET}_ENVIRONMENT )
endfunction( )
