include( Autotools/cm_define )
include( Autotools/cm_check_lib )

function(CX_CHECK_LIB_FRAMEL)
  # ---------------------------------------------------------------------
  #  Start by finding the library
  # ---------------------------------------------------------------------
  cm_check_lib( "Frame" "FrVectCompress" )
  # ---------------------------------------------------------------------
  #  Determine if FR_LONG_LONG needs to be defined
  # ---------------------------------------------------------------------
  cm_define(
    VARIABLE DEFINE_FR_LONG_LONG
    DESCRIPTION "Define to 1 if FR_LONG_LONG needs to be defined" )
endfunction(CX_CHECK_LIB_FRAMEL)
