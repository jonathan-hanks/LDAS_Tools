include( Autotools/cm_try_run )
include( Autotools/cm_msg_checking )
include( Autotools/cm_define )

function( CX_STRUCT_PROCFS_PSINFO )
  cm_try_run(
    VARIABLE HAVE_PROCFS_PSINFO
    SOURCE
      "#include <unistd.h>"
      "#include <stdio.h>"
      " "
      "int"
      "main()"
      "{"
      "  int  retval[<sc>]"
      "  char filename[ 1024 ][<sc>]"
      " "
      "  #if SIZEOF_INT == SIZEOF_PID_T"
      "  sprintf( filename, \"/proc/%d/psinfo\", (int)(getpid( )) )[<sc>]"
      "  #else /* SIZEOF_INT == SIZEOF_PID_T */"
      "  sprintf( filename, \"/proc/%ld/psinfo\", getpid( ) )[<sc>]"
      "  #endif /* SIZEOF_INT == SIZEOF_PID_T */"
      " "
      "  retval = access( filename, R_OK )[<sc>]"
      "  "
      "  return retval[<sc>]"
      "}"
    DEFINES
      -DSIZEOF_INT=${SIZEOF_INT}
      -DSIZEOF_PID_T=${SIZEOF_PID_T}
    )
  cm_define(
    VARIABLE HAVE_PROCFS_PSINFO
    DESCRIPTION "Defined if /proc/<pid>/psinfo exists" )
  cm_msg_checking(
    "if process info is in /proc/<pid>/psinfo"
    CONDITION HAVE_PROCFS_PSINFO )
endfunction( CX_STRUCT_PROCFS_PSINFO )
