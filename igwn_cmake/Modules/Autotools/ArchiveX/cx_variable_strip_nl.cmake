function( cx_variable_strip_nl _VAR )
  string( REGEX REPLACE "\n$" "" "${_VAR}" "${${_VAR}}" )
  set( ${_VAR} ${${_VAR}} PARENT_SCOPE )
endfunction( cx_variable_strip_nl )
