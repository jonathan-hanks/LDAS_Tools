#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_swig_python_module( ) - Create python module using SWIG
#
# INTERFACE_FILE - 
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/Internal/ci_set_policy )
include( Autotools/cm_msg_error )

ci_set_policy(CMP0078 NEW)

function(cx_swig_python_module)
  set( DEFAULT_PYTHON_SWIG_FLAGS "-shadow" "-importall" "-Wall" "-threads" "-O" )
  if ( ${SWIG_VERSION} VERSION_LESS "4")
    list( INSERT DEFAULT_PYTHON_SWIG_FLAGS 0 "-classic" )
  endif( )
  #----------------------------------------------------------------------
  # Parse arguments
  #----------------------------------------------------------------------
  set(options
    EXTEND_SWIG_FLAGS
    CXX
    )
  set(oneValueArgs
    INTERFACE_FILE
    MODULE
    MODULE_DIR
    PREFIX
    )
  set(multiValueArgs
    LINK_LIBRARIES
    SWIG_FLAGS
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT ARG_PREFIX )
    set( ARG_PREFIX PYTHON )
  endif( )
  if ( ARG_MODULE_DIR )
    set( ARG_MODULE_DIR "/${ARG_MODULE_DIR}" )
  endif ( ARG_MODULE_DIR )
  if ( NOT ARG_SWIG_FLAGS )
    set( ARG_SWIG_FLAGS ${DEFAULT_PYTHON_SWIG_FLAGS} )
  else( )
    if ( ARG_EXTEND_SWIG_FLAGS )
      list( INSERT ARG_SWIG_FLAGS 0 ${DEFAULT_PYTHON_SWIG_FLAGS} )
    endif( )
  endif ( )

  #----------------------------------------------------------------------
  # Establish compiled characteristics
  #----------------------------------------------------------------------

  set(PYC_FILE_EXT ".pyc" )
  set(PYO_FILE_EXT ".pyo" )
  if (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 2)
    set(PYC_DIR "")
    set(PYC_FILE "${ARG_MODULE}${PYC_FILE_EXT}")
    set(PYO_FILE "${ARG_MODULE}${PYO_FILE_EXT}")
  elseif (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 3)
    if ( ${${ARG_PREFIX}_VERSION_MINOR} GREATER 4 )
      set( PYO_FILE_EXT ".opt-1.pyc" )
    endif ( ${${ARG_PREFIX}_VERSION_MINOR} GREATER 4 )
    set(PYC_DIR "/__pycache__")
    set(PYC_FILE "__pycache__/${ARG_MODULE}.cpython-${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}${PYC_FILE_EXT}")
    set(PYO_FILE "__pycache__/${ARG_MODULE}.cpython-${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}${PYO_FILE_EXT}")
  else (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 2)
    cm_msg_error( "Version ${${ARG_PREFIX}_VERSION_MAJOR}.x of python is not yet supported for use in builds of ${ARG_MODULE}" )
  endif (${${ARG_PREFIX}_VERSION_MAJOR} EQUAL 2)

  #----------------------------------------------------------------------
  # 
  #----------------------------------------------------------------------

  set(CMAKE_SWIG_FLAGS ${ARG_SWIG_FLAGS})
  set(CMAKE_SWIG_OUTDIR ${CMAKE_CURRENT_BINARY_DIR}${ARG_MODULE_DIR})
  set( target_name "${ARG_MODULE}${${ARG_PREFIX}_VERSION_MAJOR}" )
  if ( ARG_CXX )
    set_source_files_properties( ${ARG_INTERFACE_FILE}
      PROPERTIES
        CPLUSPLUS on )
    set_source_files_properties( ${ARG_MODULE}.cc PROPERTIES GENERATED true )
  else( )
    set_source_files_properties( ${ARG_MODULE}.c PROPERTIES GENERATED true )
  endif( )

  if ( COMMAND SWIG_ADD_LIBRARY )
    swig_add_library( ${target_name}
      LANGUAGE python
      SOURCES ${ARG_INTERFACE_FILE} )
  else ( COMMAND SWIG_ADD_LIBRARY )
    swig_add_module( ${target_name} python ${ARG_INTERFACE_FILE} )
  endif ( COMMAND SWIG_ADD_LIBRARY )
  set_target_properties(${SWIG_MODULE_${target_name}_REAL_NAME}
    PROPERTIES
      LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SWIG_OUTDIR}
      OUTPUT_NAME _${ARG_MODULE})
  if ( APPLE )
    set_target_properties( ${SWIG_MODULE_${target_name}_REAL_NAME}
      PROPERTIES
      INSTALL_RPATH "${CMAKE_INSTALL_FULL_LIBDIR}"
      BUILD_WITH_INSTALL_RPATH TRUE
      LINK_FLAGS "-undefined dynamic_lookup"
      )
  else( )
    target_link_libraries( ${SWIG_MODULE_${target_name}_REAL_NAME}
      ${${ARG_PREFIX}_LIBRARIES}
      )
  endif( )
  # cx_msg_debug_variable(${ARG_PREFIX}_LIBRARIES)
  if ( ARG_LINK_LIBRARIES )
    target_link_libraries( ${SWIG_MODULE_${target_name}_REAL_NAME}
      ${ARG_LINK_LIBRARIES}
      )
  endif( )
  file( WRITE ${CMAKE_SWIG_OUTDIR}/__init__.py "" )

  #----------------------------------------------------------------------
  # Compile .py files
  #----------------------------------------------------------------------

  add_custom_command(
    #--------------------------------------------------------------------
    # Compile python standard
    #--------------------------------------------------------------------
    OUTPUT
      ${CMAKE_CURRENT_BINARY_DIR}${ARG_MODULE_DIR}/${PYC_FILE}
    COMMAND
      ${${ARG_PREFIX}_EXECUTABLE} -m py_compile
        "${CMAKE_SWIG_OUTDIR}/${ARG_MODULE}.py"
    DEPENDS ${SWIG_MODULE_${target_name}_REAL_NAME}
    )
  add_custom_command(
    #--------------------------------------------------------------------
    # Compile python with optimization
    #--------------------------------------------------------------------
    OUTPUT
      ${CMAKE_CURRENT_BINARY_DIR}${ARG_MODULE_DIR}/${PYO_FILE}
    COMMAND
      ${${ARG_PREFIX}_EXECUTABLE} -O -m py_compile
        "${CMAKE_SWIG_OUTDIR}/${ARG_MODULE}.py"
    DEPENDS ${SWIG_MODULE_${ARG_MODULE}_REAL_NAME}
    )
  add_custom_target(
    ${target_name}_python_compile
    ALL
    DEPENDS
      ${CMAKE_CURRENT_BINARY_DIR}${ARG_MODULE_DIR}/${PYC_FILE}
      ${CMAKE_CURRENT_BINARY_DIR}${ARG_MODULE_DIR}/${PYO_FILE}
      )

  #----------------------------------------------------------------------
  # Installation rules
  #----------------------------------------------------------------------

  set(MODULE_REAL_NAME ${SWIG_MODULE_${target_name}_REAL_NAME})
  get_target_property( MODULE_OUTPUT_NAME ${MODULE_REAL_NAME} OUTPUT_NAME )
  get_target_property( MODULE_SUFFIX ${MODULE_REAL_NAME} SUFFIX )
  if ( NOT MODULE_SUFFIX )
    get_target_property( MODULE_SUFFIX ${MODULE_REAL_NAME} IMPORT_SUFFIX )
    if ( NOT MODULE_SUFFIX )
      set( MODULE_SUFFIX ${CMAKE_SHARED_MODULE_SUFFIX} )
    endif ( NOT MODULE_SUFFIX )
  endif ( NOT MODULE_SUFFIX )

  install(
    FILES
      ${CMAKE_SWIG_OUTDIR}/${ARG_MODULE}.py
      ${CMAKE_SWIG_OUTDIR}/${MODULE_OUTPUT_NAME}${MODULE_SUFFIX}
    DESTINATION
      "${PYTHON${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}_MODULE_INSTALL_DIR}${ARG_MODULE_DIR}"
    COMPONENT Python
    )
  install(
    FILES
      ${CMAKE_CURRENT_BINARY_DIR}${ARG_MODULE_DIR}/${PYC_FILE}
      ${CMAKE_CURRENT_BINARY_DIR}${ARG_MODULE_DIR}/${PYO_FILE}
    DESTINATION
      "${PYTHON${${ARG_PREFIX}_VERSION_MAJOR}${${ARG_PREFIX}_VERSION_MINOR}_MODULE_INSTALL_DIR}${ARG_MODULE_DIR}/${PYC_DIR}"
    COMPONENT Python
    )
endfunction(cx_swig_python_module)
