#========================================================================
# -*- mode: cmake -*-
#
# Options:
#    NO_INSTALL
#
# Single Value:
#    INTSTALL_DIR
#    OUTPUT_NAME
#    TARGET
#
# List Options:
#    PKG_CONFIG_PACKAGES
#    SOURCES
#    TARGET_LIBRARIES
#------------------------------------------------------------------------
include( CMakeParseArguments )

function( cx_create_executable )
  set(options NO_INSTALL)
  set(oneValueArgs TARGET OUTPUT_NAME INSTALL_DIR)
  set(multiValueArgs
    PKG_CONFIG_PACKAGES
    SOURCES
    TARGET_LIBRARIES
    )

  cmake_parse_arguments(
    ARG
      "${options}"
      "${oneValueArgs}"
      "${multiValueArgs}"
      ${ARGN}
      )

  #----------------------------------------------------------------------

  if ( ARG_PKG_CONFIG_PACKAGES )
    pkg_check_modules(
      EXTPKGS_${ARG_TARGET}
      REQUIRED ${ARG_PKG_CONFIG_PACKAGES}
      )
  endif ( ARG_PKG_CONFIG_PACKAGES )

  if ( EXTPKGS_${ARG_TARGET}_LIBRARY_DIRS )
    link_directories( ${EXTPKGS_${ARG_TARGET}_LIBRARY_DIRS} )
  endif ( EXTPKGS_${ARG_TARGET}_LIBRARY_DIRS )

  add_executable( ${ARG_TARGET} ${ARG_SOURCES} )
  if ( ARG_OUTPUT_NAME )
    set_target_properties( ${ARG_TARGET} PROPERTIES OUTPUT_NAME "${ARG_OUTPUT_NAME}" )
  endif ( ARG_OUTPUT_NAME )

  target_link_libraries(
    ${ARG_TARGET}
    ${EXTPKGS_${ARG_TARGET}_LIBRARIES}
    ${ARG_TARGET_LIBRARIES}
  )
  if ( EXTPKGS_${ARG_TARGET}_INCLUDE_DIRS )
    target_include_directories(
      ${ARG_TARGET}
      PUBLIC ${EXTPKGS_${ARG_TARGET}_INCLUDE_DIRS}
      )
  endif ( EXTPKGS_${ARG_TARGET}_INCLUDE_DIRS )

  if ( EXTPKGS_${ARG_TARGET}_CFLAGS_OTHER )
    target_compile_options(
      ${ARG_TARGET}
      PUBLIC ${EXTPKGS_${ARG_TARGET}_CFLAGS_OTHER}
      )
  endif ( EXTPKGS_${ARG_TARGET}_CFLAGS_OTHER )

  if ( NOT ARG_NO_INSTALL )
    if ( NOT ARG_INSTALL_DIR )
      set( ARG_INSTALL_DIR "${CMAKE_INSTALL_FULL_BINDIR}")
    endif ( NOT ARG_INSTALL_DIR )
    install(
      TARGETS ${ARG_TARGET}
      RUNTIME DESTINATION ${ARG_INSTALL_DIR}
      )
  endif ( NOT ARG_NO_INSTALL )
endfunction( cx_create_executable )
