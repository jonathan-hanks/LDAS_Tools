#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_system_daemon
#------------------------------------------------------------------------
include( CMakeParseArguments )

#------------------------------------------------------------------------
# Establish CACHE variables
#------------------------------------------------------------------------
set(
  SERVICES_DIR "${DEFAULT_SERVICES_DIR}"
  CACHE FILEPATH "Directory specifying where to install system services support files"
  )

function(_cxp_set_services_dir DIRECTORY)
  if ( NOT SERVICES_DIR )
    set( SERVICES_DIR "${DIRECTORY}" PARENT_SCOPE )
  endif ( NOT SERVICES_DIR )
endfunction(_cxp_set_services_dir)

function(_cx_system_daemon IN_FILE DIRECTORY OUT_FILE)
  if ( NOT OUT_FILE )
    set( OUT_FILE "${IN_FILE}" )
  endif ( NOT OUT_FILE )
  install(
    FILES       ${IN_FILE}
    DESTINATION "${DIRECTORY}"
    RENAME      ${OUT_FILE}
    )
endfunction(_cx_system_daemon)

#------------------------------------------------------------------------
# DEFINE: cx_system_daemon
#------------------------------------------------------------------------
function(cx_system_daemon)
  set(options)
  set(oneValueArgs
    DEBIAN_SOURCE  DEBIAN_DESTINATION
    RED_HAT_SOURCE RED_HAT_DESTINATION
    OSX_SOURCE     OSX_DESTINATION
    SOLARIS_SOURCE SOLARIS_DESTINATION
    SYSTEMD
    )
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( NOT CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR )
    execute_process(
      COMMAND ${PKG_CONFIG_EXECUTABLE} --variable=systemdsystemunitdir systemd
      OUTPUT_VARIABLE CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR
      OUTPUT_STRIP_TRAILING_WHITESPACE
      )
  endif ( NOT CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR )
  if ( CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR )
    set( CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR "${CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR}" PARENT_SCOPE )
    set( CMAKE_INSTALL_FULL_SYSTEMDSYSTEMUNITDIR "${CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR}" )
    set( CMAKE_INSTALL_FULL_SYSTEMDSYSTEMUNITDIR "${CMAKE_INSTALL_FULL_SYSTEMDSYSTEMUNITDIR}" PARENT_SCOPE )
    set( HAVE_SYSTEMD TRUE )
  endif ( CMAKE_INSTALL_SYSTEMDSYSTEMUNITDIR )
  if ( EXISTS "/etc/redhat-release" )
    set( RED_HAT_INITD TRUE )
    message( STATUS "Have RedHat system" )
  endif ( EXISTS "/etc/redhat-release" )
  if ( EXISTS "/etc/debian_version" )
    set( DEBIAN_INITD TRUE PARENT_SCOPE )
    message( STATUS "Have Debian system" )
  endif ( EXISTS "/etc/debian_version" )
  if ( EXISTS "/Library/LaunchDaemons" )
    set( OSX_INITD TRUE PARENT_SCOPE )
    message( STATUS "Have OSX system" )
  endif ( EXISTS "/Library/LaunchDaemons" )
  if ( EXISTS "/usr/sbin/svccfg" )
    set( SOLARIS_INITD TRUE PARENT_SCOPE )
    message( STATUS "Have Solaris system" )
  endif ( EXISTS "/usr/sbin/svccfg" )
  if ( HAVE_SYSTEMD )
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    # Generic systemd
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    message( STATUS "Setup systemd installation dir: ${CMAKE_INSTALL_FULL_SYSTEMDSYSTEMUNITDIR}" )
    message( STATUS "Setup systemd installation files: ${ARG_SYSTEMD}" )
    _cxp_set_services_dir( "${CMAKE_INSTALL_FULL_SYSTEMDSYSTEMUNITDIR}" )
    install(
      FILES       ${ARG_SYSTEMD}
      DESTINATION "${SERVICES_DIR}"
      )
  else ( HAVE_SYSTEMD )
    if ( DEBIAN_INITD )
      ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      ## Debian - services
      ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      _cxp_set_services_dir( "${CMAKE_INSTALL_FULL_SYSCONFDIR}/initd.d" )
      install(
	FILES       ${ARG_DEBIAN_SOURCE}
	DESTINATION "${SERVICES_DIR}"
	RENAME      ${ARG_DEBIAN_DESTINATION}
	)
    endif ( DEBIAN_INITD )
    if ( OSX_INITD )
      ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      ## OSX - services
      ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      _cxp_set_services_dir("/Library/LaunchDaemons" )
      install(
        FILES       ${ARG_OSX_SOURCE}
        DESTINATION "${SERVICES_DIR}"
        RENAME      ${ARG_OSX_DESTINATION}
        )
    endif ( OSX_INITD )
    if ( RED_HAT_INITD AND ARG_RED_HAT_SOURCE AND ARG_RED_HAT_DESTINATION )
      ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      ## RedHat without systemd
      ##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      _cxp_set_services_dir( "${CMAKE_INSTALL_FULL_SYSCONFDIR}/initd.d" )
      install(
	FILES       ${ARG_RED_HAT_SOURCE}
	DESTINATION "${SERVICES_DIR}"
	RENAME      ${ARG_RED_HAT_DESTINATION}
	)
    endif ( RED_HAT_INITD AND ARG_RED_HAT_SOURCE AND ARG_RED_HAT_DESTINATION )
    if ( SOLARIS_INITD )
      #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      # Solaris - services via svccfg
      #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
      #CLEAN_LOCAL += diskcache-xml-solaris-clean-local
      #
      #diskcache-xml-solaris-clean-local:
      #	rm -f diskcache.xml.solaris
      #
      #diskcache.xml.solaris: Makefile diskcache.xml.solaris.in
      #	$(AM_V_GEN) rm -f $@ $@.tmp; \
      #	srcdir=''; \
      #	  test -f ./$@.in || srcdir=$(srcdir)/; \
      #	  $(edit) $${srcdir}$@.in >$@.tmp \
      #	chmod +x $@.tmp; \
      #	chmod a-w $@.tmp; \
      #	mv $@.tmp $@
      #
      _cxp_set_services_dir( "${CMAKE_INSTALL_FULL_DATADIR}" )
      install(
	FILES       ${ARG_SOLARIS_SOURCE}
	DESTINATION ${CMAKE_INSTALL_FULL_DATADIR}
	RENAME      ${ARG_SOLARIS_DESTINATION}
	)
    endif ( SOLARIS_INITD )
  endif ( HAVE_SYSTEMD )


endfunction(cx_system_daemon)
