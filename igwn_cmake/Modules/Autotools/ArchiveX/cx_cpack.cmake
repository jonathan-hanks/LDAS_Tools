#========================================================================
# -*- mode: cmake -*-
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_packaging )

function( cx_cpack )
  set(options)
  set(oneValueArgs
    SOURCE_GENERATOR
    )
  set(multiValueArgs)

  cmake_parse_arguments(
    ARG
      "${options}"
      "${oneValueArgs}"
      "${multiValueArgs}"
      ${ARGN}
      )

  #======================================================================
  # CPack setup
  #----------------------------------------------------------------------

  #----------------------------------------------------------------------
  # Establish default values
  #----------------------------------------------------------------------

  if ( NOT ARG_SOURCE_GENERATOR )
    #set( ARG_SOURCE_GENERATOR "TBZ2" )
    set( ARG_SOURCE_GENERATOR "TGZ" )
  endif ( NOT ARG_SOURCE_GENERATOR )
  if ( NOT ARG_SOURCE_IGNORE_FILES )
    set( ARG_SOURCE_IGNORE_FILES
      "/build/;/.bzr/;~$;${CPACK_SOURCE_IGNORE_FILES}" )
  endif ( NOT ARG_SOURCE_IGNORE_FILES )

  #----------------------------------------------------------------------
  # Establish CPack configuration
  #----------------------------------------------------------------------

  set( CPACK_SOURCE_GENERATOR ${ARG_SOURCE_GENERATOR} )
  set( CPACK_SOURCE_IGNORE_FILES
    "${ARG_SOURCE_IGNORE_FILES}")
  set( CPACK_SOURCE_PACKAGE_FILE_NAME
    "${PROJECT_NAME}-${PROJECT_VERSION}")
  if ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TBZ2")
    set( CPACK_SOURCE_PACKAGE_FILE_EXTENSION
      ".tar.bz2")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "-j")
  elseif ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TGZ")
    set( CPACK_SOURCE_PACKAGE_FILE_EXTENSION
      ".tar.gz")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "-z")
  elseif ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TXZ")
    set( CPACK_SOURCE_PACKAGE_FILE_EXTENSION
      ".tar.xz")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "-J")
  else ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TBZ2")
    set( CPACK_SOURCE_EXTENSION ".tar")
    set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
      "")
  endif ( ${CPACK_SOURCE_GENERATOR} STREQUAL "TBZ2")
  set( CPACK_SOURCE_PACKAGE_FILE_NAME_FULL
    "${PROJECT_NAME}-${PROJECT_VERSION}${CPACK_SOURCE_PACKAGE_FILE_EXTENSION}")
  #----------------------------------------------------------------------
  # Export variables
  #----------------------------------------------------------------------
  set( CPACK_SOURCE_EXTENSION
    ${CPACK_SOURCE_EXTENSION}
    PARENT_SCOPE )
  set( CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION
    ${CPACK_SOURCE_PACKAGE_COMPRESSION_OPTION}
    PARENT_SCOPE )
  set( CPACK_SOURCE_PACKAGE_FILE_NAME_FULL
    ${CPACK_SOURCE_PACKAGE_FILE_NAME_FULL}
    PARENT_SCOPE )

  #----------------------------------------------------------------------
  # Invoke CPack
  #----------------------------------------------------------------------

  include(CPack)

  #----------------------------------------------------------------------
  # Allow for testing of local packaging systems
  #----------------------------------------------------------------------
  cx_packaging( ${ARGN} )

endfunction( cx_cpack )
