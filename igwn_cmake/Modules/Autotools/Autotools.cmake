#------------------------------------------------------------------------
# Collection of macros that have been developed based on Autoconf
# Macro
#
#   https://www.gnu.org/software/autoconf/autoconf.html
#
#------------------------------------------------------------------------

include(GNUInstallDirs)

include(Autotools/ch_bottom)
include(Autotools/ch_template)
include(Autotools/ch_top)

include(Autotools/cm_init)
include(Autotools/cm_c_bigendian)
include(Autotools/cm_check_funcs)
include(Autotools/cm_check_headers)
include(Autotools/cm_check_headers_default_autotools)
include(Autotools/cm_check_lib)
include(Autotools/cm_check_progs)
include(Autotools/cm_check_sizeof)
include(Autotools/cm_conditional)
include(Autotools/cm_config_header)
include(Autotools/cm_config_headers)
include(Autotools/cm_define)
include(Autotools/cm_func_strerror_r)
include(Autotools/cm_header_stdc)
include(Autotools/cm_msg_checking)
include(Autotools/cm_msg_notice)
include(Autotools/cm_msg_result)
include(Autotools/cm_prereq)
include(Autotools/cm_pkg_check_modules)
include(Autotools/cm_search_libs)
include(Autotools/cm_subst)
include(Autotools/cm_try_compile)
include(Autotools/cm_try_run)
