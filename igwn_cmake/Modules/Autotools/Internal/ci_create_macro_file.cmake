#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
#
# cm_deinfe( VARIABLE <variable> VALUE <value> [DESCRIPTION <description>] )
#
#   or
#
# cm_deinfe( <variable> <value> [<ddescription>] )
#------------------------------------------------------------------------
include( CMakeParseArguments )

function( ci_create_macro_file )
  set( options )
  set( oneValueArgs TAG INSTALL_DIR MODULE_DIR )
  set( multiValueArgs MODULES NO_TEST_MODULES )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set(FILENAME "${CMAKE_CURRENT_BINARY_DIR}/Find${ARG_TAG}.cmake")
  file( WRITE ${FILENAME} "" )
  foreach( module ${ARG_MODULES} )
    install(
      FILES "${CMAKE_CURRENT_SOURCE_DIR}/${module}"
      DESTINATION "${ARG_INSTALL_DIR}/${ARG_MODULE_DIR}"
      )
    set( TEST_IT YES )
    if ( ARG_NO_TEST_MODULES )
      list( FIND ARG_NO_TEST_MODULES ${module} TEST_IT )
      if ( NOT TEST_IT EQUAL -1 )
	      set( TEST_IT NO )
      endif()
    endif()
    if ( TEST_IT )
      add_test(
	      NAME ${module}
	      COMMAND
	      ${CMAKE_COMMAND}
	      "-DCMAKE_MODULE_PATH=${CMAKE_MODULE_PATH}"
	      -P ${CMAKE_CURRENT_SOURCE_DIR}/${module} )
    endif()
    string( REGEX REPLACE "[.]cmake$" "" module "${module}" )
    file( APPEND ${FILENAME} "include( ${ARG_MODULE_DIR}/${module} )\n" )
  endforeach()
  install(
    FILES ${FILENAME}
    DESTINATION "${ARG_INSTALL_DIR}"
    )
endfunction()
