# -*- mode: cmake -*-
#------------------------------------------------------------------------
# Simulate the CM_TRY_COMPILE macro
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug_variable )

function(cm_try_compile ARG_INCLUDE ARG_BODY ARG_VARIABLE )
  set(options)
  set(oneValueArgs)
  set(multiValueArgs DEFINES LIBRARIES )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  set(fn "${CMAKE_CURRENT_BINARY_DIR}/${ARG_VARIABLE}.cc")
  file(REMOVE ${fn})
  file(APPEND ${fn} "${ARG_INCLUDE}")
  file(APPEND ${fn} "int main() {\n")
  file(APPEND ${fn} "${ARG_BODY}")
  file(APPEND ${fn} "return 0;\n")
  file(APPEND ${fn} "}\n")
  try_compile(${ARG_VARIABLE} ${CMAKE_CURRENT_BINARY_DIR}
	      ${fn}
	      COMPILE_DEFINITIONS ${ARG_DEFINES}
        LINK_LIBRARIES ${ARG_LIBRARIES}
        OUTPUT_VARIABLE output_
        )
  if(${ARG_VARIABLE})
    set(${ARG_VARIABLE} 1)
  else( )
    set(${ARG_VARIABLE} 0)
  endif( )
  set(${ARG_VARIABLE} ${${ARG_VARIABLE}} PARENT_SCOPE)
  cx_msg_debug_variable(${ARG_VARIABLE})
  cx_msg_debug_variable(output_)
endfunction( )
