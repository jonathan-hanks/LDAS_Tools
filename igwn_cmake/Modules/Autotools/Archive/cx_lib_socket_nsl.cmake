# -*- mode: cmake -*-
# ---------------------------------------------------------------------
include( CMakeParseArguments )
include( Autotools/cm_check_lib )
include( Autotools/cm_search_libs )

function( cx_lib_socket_nsl )
  set(options)
  set(oneValueArgs
    VARIABLE )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_search_libs(
    FUNCTION gethostbyname
    SEARCH_LIBS nsl
    )
  cm_search_libs(
    FUNCTION socket
    SEARCH_LIBS socket
    VARIABLE have_lib_socket_
    )
  if ( NOT have_lib_socket_ )
    cm_check_lib(nsl gethostbyname)
    cm_check_lib(socket socket)
  endif( )

endfunction( )
