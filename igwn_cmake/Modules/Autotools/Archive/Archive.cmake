#------------------------------------------------------------------------
# Collection of macros that have been developed based on Autoconf
# Macro Archive
#
#   https://www.gnu.org/software/autoconf-archive/
#
#------------------------------------------------------------------------

include(Autotools/Archive/cx_lib_socket_nsl)
include(Autotools/Archive/cx_pkg_swig)
include(Autotools/Archive/cx_python)
include(Autotools/Archive/cx_thread)
