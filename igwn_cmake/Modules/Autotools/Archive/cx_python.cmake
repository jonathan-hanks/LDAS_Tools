#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cx_python( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

include( Autotools/ArchiveX/cx_msg_debug_variable )
include( Autotools/cm_define )

function( _cp_python_unset )
  foreach( var ${ARGN} )
    unset( ${var} CACHE )
    unset( ${var} PARENT_SCOPE )
  endforeach( )
endfunction( )

function(_cx_python_have_include _INCLUDE _VALUE)
  string(TOUPPER ${_INCLUDE} _INCLUDE_UPPER)
  string(REGEX REPLACE "[^A-Za-z0-9]" "_" _INCLUDE_UPPER ${_INCLUDE_UPPER} )
  if ( _VALUE )
    set( _VALUE 1 )
  else()
    set( _VALUE 0 )
  endif()
  cm_define(
    VARIABLE HAVE_${_INCLUDE_UPPER}
    VALUE ${_VALUE}
    DESCRIPTION
      "Define to 1 if you have the <${_INCLUDE}> header file."
      )

endfunction(_cx_python_have_include)

function( cx_python )
  cx_msg_debug_variable( PYTHON2_EXECUTABLE )
  cx_msg_debug_variable( PYTHON3_EXECUTABLE )
  #----------------------------------------------------------------------
  # PythonInterp
  #----------------------------------------------------------------------
  set( p_pythoninterp_vars_
    INTERP_FOUND
    _EXECUTABLE
    _SANITIZER_EXECUTABLE
    _VERSION_STRING
    _VERSION_MAJOR
    _VERSION_MINOR
    _VERSION_PATCH
    )
  #----------------------------------------------------------------------
  # PythonLibs
  #----------------------------------------------------------------------
  set( p_pythonlibs_vars_
    LIBS_FOUND
    _PREFIX
    _LIBRARY
    _LIBRARIES
    _LIBPL
    _DEBUG_LIBRARIES
    _INCLUDE_DIRS
    _SITE_PACKAGES
    _IS_DEBUG
    _INCLUDE_PATH
    )

  set( PYTHON_CACHE_VARIABLES_ "" )
  foreach( base_
      ${p_pythoninterp_vars_}
      ${p_pythonlibs_vars_}
      _MODULE_INSTALL_DIR
      )
    list( APPEND PYTHON_CACHE_VARIABLES_ PYTHON${base_})
  endforeach( )

  set(options
    CLEAR
    )
  set(oneValueArgs
    VERSION
    INTERP
    PREFIX
    )
  set(multiValueArgs
    )

  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cx_msg_debug_variable( ARG_CLEAR )
  cx_msg_debug_variable( ARG_VERSION )
  cx_msg_debug_variable( ARG_INTERP )
  cx_msg_debug_variable( ARG_PREFIX )
  cx_msg_debug_variable( ${ARG_PREFIX}_EXECUTABLE )

  if ( NOT ARG_PREFIX )
    set( ARG_PREFIX PYTHON )
  endif ( )
  cx_msg_debug_variable( ARG_PREFIX )
  if ( NOT ARG_VERSION OR "${ARG_VERSION}" STREQUAL "" )
    if ( ${ARG_PREFIX}_EXECUTABLE )
      string( REGEX REPLACE "^.*python" "" ARG_VERSION "${${ARG_PREFIX}_EXECUTABLE}" )
      string( REGEX REPLACE "([0-9])[^0-9]*$" "\\1" ARG_VERSION "${ARG_VERSION}" )
      if ( NOT ${ARG_PREFIX}_VERSION )
        set( ${ARG_PREFIX}_VERSION ${ARG_VERSION} CACHE INTERNAL "" )
      endif ( )
    elseif( ${ARG_PREFIX}_VERSION )
      set( ARG_VERSION ${${ARG_PREFIX}_VERSION} )
    endif( )
  endif ( )
  cx_msg_debug_variable( ARG_VERSION )
  if ( NOT ${ARG_PREFIX}_VERSION )
    set( ${ARG_PREFIX}_VERSION ${ARG_VERSION} )
  endif( )

  cx_msg_debug_variable( ${ARG_PREFIX}_EXECUTABLE )
  # cx_msg_debug_variable(ARG_VERSION)
  if ( NOT ARG_INTERP )
    if ( ${ARG_PREFIX}_EXECUTABLE )
      set( ARG_INTERP ${${ARG_PREFIX}_EXECUTABLE} )
    else( )
      string(REPLACE "." ";" pv ${${ARG_PREFIX}_VERSION})
      unset( ver )
      foreach(v ${pv})
        if ( ver )
          set( ver "${ver}.${v}" )
        else( )
          set( ver ${v} )
        endif( )
        list( INSERT ARG_INTERP 0 python${ver} )
      endforeach( )
    endif( )
  endif ( NOT ARG_INTERP )

  _cp_python_unset( ${PYTHON_CACHE_VARIABLES_} )
  if ( ARG_CLEAR )
    return()
  endif( ARG_CLEAR )

  find_program( PYTHON_EXECUTABLE NAMES ${ARG_INTERP} python )
  cx_msg_debug_variable(ARG_INTERP)
  cx_msg_debug_variable(PYTHON_EXECUTABLE)


  set(
    ENABLE_PYTHON "yes"
    CACHE BOOL "use python "
    )
  if ( ${ENABLE_PYTHON} )
    set(Python_ADDITIONAL_VERSIONS ${ARG_VERSION})

    find_package( PythonInterp ${ARG_VERSION} )
    if ( PYTHONINTERP_FOUND )
      find_package(PythonLibsNew ${PYTHON_VERSION_STRING})
      # -----------------------------------------------------------------
      #  Check for python interpreter to be used with sanitizer
      # -----------------------------------------------------------------
      cx_msg_debug_variable( PYTHON_PREFIX )
      find_program(
        PYTHON_SANITIZER_EXECUTABLE
        NAMES Python python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR} python
        HINTS
        "${PYTHON_PREFIX}/Resources/Python.app/Contents/MacOS"
        "${PYTHON_PREFIX}/bin"
        NO_CMAKE_PATH
        NO_CMAKE_ENVIRONMENT_PATH
        )
      cx_msg_debug_variable( PYTHON_EXECUTABLE )
    endif ( )
  endif ( )
  _cx_python_have_include( "Python.h" PYTHON_INCLUDE_DIRS )
  if ( PYTHONINTERP_FOUND )
    #--------------------------------------------------------------------
    # Determine where packages should be installed
    #--------------------------------------------------------------------
    set( pythondir_script "from distutils.sysconfig import get_python_lib; print( get_python_lib(1,0,\"${CMAKE_INSTALL_PREFIX}\") );" )
    set( pyexecdir_script "from distutils.sysconfig import get_python_lib; print( get_python_lib(1,0,\"${CMAKE_INSTALL_PREFIX}\") );" )

    execute_process(
      COMMAND ${PYTHON_EXECUTABLE} -c "${pythondir_script}"
      OUTPUT_VARIABLE pythondir_full
      OUTPUT_STRIP_TRAILING_WHITESPACE
      )
    set( pythondir_full ${pythondir_full} PARENT_SCOPE )
    execute_process(
      COMMAND ${PYTHON_EXECUTABLE} -c "${pyexecdir_script}"
      OUTPUT_VARIABLE pyexecdir_full
      OUTPUT_STRIP_TRAILING_WHITESPACE
      )
    set( pyexecdir_full ${pyexecdir_full} PARENT_SCOPE )
  endif ( )
  if( NOT PYTHON_MODULE_INSTALL_DIR )
    set( PYTHON_MODULE_INSTALL_DIR ${pythondir_full}
      CACHE PATH "Installation directory for Python modules" )
    # cx_msg_debug_variable(PYTHON_MODULE_INSTALL_DIR)
  endif( NOT PYTHON_MODULE_INSTALL_DIR )
  if( NOT PYTHON_EXTMODULE_INSTALL_DIR )
    set( PYTHON_EXTMODULE_INSTALL_DIR ${pyexecdir_full}
      CACHE PATH "Installation directory for Python extension modules" )
    # cx_msg_debug_variable(PYTHON_EXTMODULE_INSTALL_DIR)
  endif( NOT PYTHON_EXTMODULE_INSTALL_DIR )

  cx_msg_debug_variable( PYTHON_EXECUTABLE )
  cx_msg_debug_variable( PYTHON_SANITIZER_EXECUTABLE )
  cx_msg_debug_variable( PYTHON_PREFIX )
  cx_msg_debug_variable( PYTHON_VERSION_MAJOR )
  cx_msg_debug_variable( PYTHON_VERSION_MINOR )
  cx_msg_debug_variable( PYTHON_VERSION_PATCH )
  cx_msg_debug_variable( PYTHON_LIBRARIES )
  cx_msg_debug_variable( PYTHONLIBS_VERSION_STRING )

  if ( PYTHONINTERP_FOUND)
    foreach ( var_
        ${p_pythoninterp_vars_}
        )
      set( ${ARG_PREFIX}${var_} ${PYTHON${var_}} CACHE INTERNAL "" )
    endforeach( )
  endif( )
  if ( PYTHONLIBS_FOUND)
    foreach ( var_
        ${p_pythonlibs_vars_}
        )
      set( ${ARG_PREFIX}${var_} ${PYTHON${var_}} CACHE INTERNAL "" )
    endforeach( )
  endif( )

  if ( NOT ${ARG_PREFIX}_VERSION_NODOTS )
    set( ${ARG_PREFIX}_VERSION_NODOTS ${${ARG_PREFIX}_VERSION} )
    string( REPLACE "." "" ${ARG_PREFIX}_VERSION_NODOTS "${${ARG_PREFIX}_VERSION_NODOTS}" )
    set( ${ARG_PREFIX}_VERSION_NODOTS ${${ARG_PREFIX}_VERSION_NODOTS} CACHE INTERNAL "" )
  endif( )
  set( PYTHON${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}_MODULE_INSTALL_DIR ${PYTHON_MODULE_INSTALL_DIR} CACHE INTERNAL "" )
  set( PYTHON${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}_EXTMODULE_INSTALL_DIR ${PYTHON_EXTMODULE_INSTALL_DIR} CACHE INTERNAL "" )

  cx_msg_debug_variable( ${ARG_PREFIX}_VERSION )
  cx_msg_debug_variable( ${ARG_PREFIX}_VERSION_NODOTS )

  if ( ARG_PREFIX AND NOT "${ARG_PREFIX}" STREQUAL "PYTHON" )
    _cp_python_unset( ${PYTHON_CACHE_VARIABLES_} )
  endif( )
endfunction(cx_python)
