include( CMakeParseArguments )
include( Autotools/cm_msg_notice )

## /**
## @igwn_group_PrintingMessages
## @igwn_group_begin
## */
## /**
## @fn cm_msg_result( MSG )
## @brief Display result message
## @details
## Display a message informing the user of the availability of the feature that was being checked.
## This command should be immediately preceded by \ref cm_msg_checking
##
## @param MSG
## Message to display
##
## @sa cm_msg_checking
##
## @code{.cmake}
## cm_msg_result( "yes" )
## @endcode
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_msg_result( MSG );
function(CM_MSG_RESULT MSG)
  set(options)
  set(oneValueArgs )
  set(multiValueArgs)
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  cm_msg_notice( "${MSG}" NO_PREFIX )
endfunction(CM_MSG_RESULT)
