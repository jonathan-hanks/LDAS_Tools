#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_msg_notice( )
#------------------------------------------------------------------------
include( CMakeParseArguments )

## /**
## @igwn_group_PrintingMessages
## @igwn_group_begin
## */
## /**
## @fn cm_msg_notice( MSG )
## @brief Display notification message
## @details
## Display a message informing the user of the group of features being checked.
##
## @param MSG
## Message to display
##
## @sa cm_msg_checking cm_msg_result
##
## @code{.cmake}
## cm_msg_notice( "checking for feature group" )
## @endcode
## @author Edward Maros
## @date   2019-2020
## @igwn_copyright
## */
## /** @igwn_group_end */
## cm_msg_notice( MSG );
function(cm_msg_notice txt)
  set(prefix "-- ")
  set(echo_cmd "echo")
  set(options
    NO_PREFIX
    NO_NL
    )
  set(oneValueArgs
    )
  set(multiValueArgs
    )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if (${ARG_NO_PREFIX})
    unset(prefix)
  endif (${ARG_NO_PREFIX})
  if (${ARG_NO_NL})
    set(echo_cmd "echo_append")
  endif (${ARG_NO_NL})

  execute_process( COMMAND ${CMAKE_COMMAND} -E ${echo_cmd} "${prefix}${txt}" )
endfunction(cm_msg_notice)
