# analogue of AC_HEADER_STDC automake macro.
# refactored a bit from https://github.com/LuaDist/libgd/blob/master/cmake/modules/AC_HEADER_STDC.cmake
include( CheckPrototypeDefinition )
include( Autotools/cm_check_headers )

function( cm_header_stdc )
  check_prototype_definition(free "void free(void*)" "NULL" stdlib.h freeExists)
  if(freeExists)
    cm_check_headers(
      dlfcn.h
      stdint.h
      stddef.h
      inttypes.h
      stdlib.h
      strings.h
      string.h
      float.h
      )
  endif(freeExists)
  set( _state 0 )
  if ( HAVE_DLFCN_H
      AND HAVE_STDINT_H
      AND HAVE_STDDEF_H
      AND INTTYPES_H
      AND HAVE_STDLIB_H
      AND HAVE_STRINGS_H
      AND HAVE_STRING_H
      AND HAVE_FLOAT_H
      )
    set( _state 1 )
  endif( )
  cm_define(
    VARIABLE STDC_HEADERS
    VALUE ${_state}
    DESCRIPTION "System has ANSI C header files"
    )
  cm_msg_checking( "ANSI C header files" )
  if ( STDC_HEADERS )
    cm_msg_result( "found" )
  else( )
    cm_msg_result( "not found" )
  endif( )

  cm_check_headers(
    unistd.h
    sys/stat.h
    sys/types.h
    dirent.h
    )

  cm_define(
    VARIABLE HAVE_LIBM
    VALUE ${_state}
    DESCRIPTION "Set to 1 if have libm, 0 otherwise"
    )
endfunction( )
