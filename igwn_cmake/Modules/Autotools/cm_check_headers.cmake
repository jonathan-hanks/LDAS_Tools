#------------------------------------------------------------------------
# -*- mode: cmake -*-
#------------------------------------------------------------------------
# cm_check_headers( <file> [ <file> ...] )
#------------------------------------------------------------------------
include( CheckIncludeFile )
include( CheckIncludeFileCXX )
include( CMakeParseArguments )

include( Autotools/cm_msg_checking )
include( Autotools/cm_msg_result )
include( Autotools/cm_define )

function(_CM_CHECK_HEADER_VARIABLE_NAME _FILE _VAR)
  set(options)
  set(oneValueArgs PREFIX )
  set(multiValueArgs )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_PREFIX )
    set( ARG_PREFIX "${ARG_PREFIX}_" )
  endif( )
  string(TOUPPER "HAVE_${ARG_PREFIX}${_FILE}" ${_VAR})
  string(REGEX REPLACE "[^A-Za-z0-9]" "_" ${_VAR} ${${_VAR}} )
  set( ${_VAR} ${${_VAR}} PARENT_SCOPE )
endfunction(_CM_CHECK_HEADER_VARIABLE_NAME)

function(CM_CHECK_HEADERS )
  set(options)
  set(oneValueArgs PREFIX )
  set(multiValueArgs HEADERS )
  cmake_parse_arguments(ARG "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  if ( ARG_HEADERS )
    if ( ARG_UNPARSED_ARGUMENTS )
      set( ARG_HEADERS ${ARG_HEADERS} ${ARG_UNPARSED_ARGUMENTS} )
    endif( ARG_UNPARSED_ARGUMENTS )
  else( ARG_HEADERS )
    if ( ARG_UNPARSED_ARGUMENTS )
      set( ARG_HEADERS ${ARG_UNPARSED_ARGUMENTS} )
    else( ARG_UNPARSED_ARGUMENTS )
      set( ARG_HEADERS "" )
    endif( ARG_UNPARSED_ARGUMENTS )
  endif( ARG_HEADERS )
  if ( ARG_PREFIX )
    set( PREFIX_OPT_ "PREFIX" ${ARG_PREFIX})
  endif( )


  foreach( hdr ${ARG_HEADERS} )
    _cm_check_header_variable_name( ${hdr} _HEADER_UPPER ${PREFIX_OPT_} )
    # cx_msg_debug( "${hdr} varible name ${_HEADER_UPPER}" )
    #--------------------------------------------------------------------
    # Erase any previous knowledge of this variable
    #--------------------------------------------------------------------
    unset( ${_HEADER_UPPER} CACHE )
    if ( ${hdr} MATCHES "[.]h$" )
      # cx_msg_debug( "C checking of ${hdr}" )
      check_include_file( "${hdr}" ${_HEADER_UPPER} )
    else ( ${hdr} MATCHES "[.]h$" )
      # cx_msg_debug( "C++ checking of ${hdr}" )
      check_include_file_cxx( "${hdr}" ${_HEADER_UPPER} )
    endif ( ${hdr} MATCHES "[.]h$" )
    # cx_msg_debug( "${hdr} varible name ${_HEADER_UPPER} - ${${_HEADER_UPPER}}" )
    cm_define(
      VARIABLE ${_HEADER_UPPER}
      DESCRIPTION "Define to 1 if you have the <${hdr}> header file."
      )
    if( ${_HEADER_UPPER} )
      set( _result "yes" )
    else( ${_HEADER_UPPER} )
      set( _result "no" )
    endif( ${_HEADER_UPPER} )
    # cx_msg_debug( "${hdr} varible name ${_HEADER_UPPER} - ${${_HEADER_UPPER}}" )
    cm_msg_checking( "for header file <${hdr}> ... " )
    cm_msg_result( "${_result}" )
  endforeach( hdr ${ARG_HEADERS} )

endfunction(CM_CHECK_HEADERS)
