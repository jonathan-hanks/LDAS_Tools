//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <algorithm>
#include <set>
#include <sstream>
#include <stdexcept>
#include <vector>

#include "framecpp/FrameCPP.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#include "frameAPI/createRDS.hh"
#include "frameAPI/CalibrationVector.hh"

#include "frame_generator.hh"

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/filesystem.hpp>

#define SKIP_WORKING 0
#define SKIP_BROKEN 1
#define TEST_DEBUG_LEVEL 0

#if !SKIP_WORKING
#define TEST_REDUCE_FRAME 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_MULTI 1
#define TEST_REDUCE_FRAME_CHANNEL_ANALYSIS_READY 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0000 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0011 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0101 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0110 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0111 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1001 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1010 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1100 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1110 1
#define TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1111 1

#if FRAME_SPEC_CURRENT <= 8
#define TEST_LLCACHE_DATA_V1 1
#define TEST_LLCACHE_DATA_V1_1 1
#define TEST_LLCACHE_DATA_V2 0 // Requires more than 300 seconds to process
#define TEST_LLCACHE_DATA_V2_1 0 // Requires more than 300 seconds to process
#define TEST_LLCACHE_DATA_V3 1
#define TEST_LLCACHE_DATA_V4 1
#define TEST_LLCACHE_DATA_V5 1
#define TEST_LLCACHE_DATA_V6 1
#define TEST_LLCACHE_DATA_V7 1

#define TEST_HOFT_C00_DATA_10101 1
#endif /* FRAME_SPEC_CURRENT <= 8 */
#endif /* SKIP_WORKING */

#if !SKIP_BROKEN
#endif /* !SKIP_BROKEN */

struct SkipException : public std::runtime_error
{
    inline SkipException( ) : std::runtime_error( "Unit test being skipped" )
    {
    }
};

struct rds_global_fixture
{
    rds_global_fixture( )
    {
        //-------------------------------------------------------------------
        // Setup logging as is needed by query class
        //-------------------------------------------------------------------
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        auto cwd = boost::filesystem::current_path( ).string( );
        GenericAPI::Log::stream_file_type fs( new GenericAPI::Log::StreamFile );
        LDASTools::AL::Log::stream_type   s;

        s = fs;
        fs->FilenameExtension( GenericAPI::LogFormatter( )->FileExtension( ) );
        GenericAPI::LogFormatter( )->Stream( s );
        GenericAPI::setLogTag( "test_query" );
        GenericAPI::LoggingInfo::LogDirectory( cwd );
        GenericAPI::setLogDebugLevel( TEST_DEBUG_LEVEL );
    }

    ~rds_global_fixture( )
    {
    }
};

BOOST_GLOBAL_FIXTURE( rds_global_fixture );

namespace testing
{
    struct live_data_cache
    {
        FrameCPP::Time start_time;
        INT_4U         duration;
        std::string    description;

        boost::filesystem::path source_path;
        std::string             frame_file_ifo;
        std::string             frame_file_description;
        INT_4U                  frame_file_deltat;

        live_data_cache( const std::string& Description )
            : description( Description )
        {
        }

        void
        cleanup( )
        {
        }

        RDSFrame::frame_file_container_type
        setup( )
        {
            if ( !boost::filesystem::exists( source_path ) )
            {
                //-------------------------------------------------------
                // The directory containing the source files does not
                //   exist, so need to skip the unit test
                //-------------------------------------------------------
                throw SkipException( );
            }
            RDSFrame::frame_file_container_type files;
            files.reserve( ( duration / frame_file_deltat ) + 3 );

            for ( auto start_second = start_time.GetSeconds( ),
                       current_duration = frame_file_deltat;
                  current_duration <= duration;
                  current_duration += frame_file_deltat,
                       start_second += frame_file_deltat )
            {
                std::ostringstream filename;
                filename //
                    << frame_file_ifo //
                    << "-" << frame_file_description //
                    << "-" << start_second //
                    << "-" << frame_file_deltat //
                    << ".gwf";
                auto full_path = source_path / filename.str( );
                if ( boost::filesystem::exists( full_path ) )
                {
                    files.emplace_back( full_path.string( ) );
                }
            }

            return ( files );
        }
    };
} // namespace testing

template < typename FileCacheType = testing::file_cache_type >
struct test_rds
{
    std::string                test_name;
    FrameAPI::RDS::FileOptions options;
    std::string                channel_names = "H1:DataVector";
    FileCacheType              file_cache;
    boost::filesystem::path    output_directory;
    FrameAPI::start_type       rds_start_time = 0;
    INT_4U                     rds_duration = 0;

    test_rds( const std::string& TestName )
        : test_name( TestName ), file_cache( TestName )
    {
        using namespace testing;

        options.AnalysisReadyChannelName( "" );
        options.AnalysisReadyMask( 0x00 );
    }

    void
    operator( )( )
    {
        try
        {
            using namespace FrameAPI;

            //---------------------------------------------------------------------
            // Cache setup
            //---------------------------------------------------------------------
            auto frame_files = file_cache.setup( );
            //---------------------------------------------------------------------
            //
            //---------------------------------------------------------------------
            start_type start =
                ( ( rds_start_time != 0 )
                      ? ( rds_start_time )
                      : ( file_cache.start_time.GetSeconds( ) ) );
            end_type end = start +
                ( ( rds_duration != 0 ) ? ( rds_duration )
                                        : ( file_cache.duration ) );
            channel_container_type channels;

            channels.ParseChannelNames( channel_names );
            //---------------------------------------------------------------------
            // Create RDS frames
            //---------------------------------------------------------------------
            output_directory = boost::filesystem::current_path( );
            output_directory /= ( test_name + "_output" );
            boost::filesystem::remove_all( output_directory );
            boost::filesystem::create_directories( output_directory );

            options.OutputTimeStart( start );
            options.OutputTimeEnd( end );
            options.DirectoryOutputFrames( output_directory.string( ) );
            options.DirectoryOutputMD5Sum( output_directory.string( ) );
            options.OutputType( test_name );
            options.VerifyFilenameMetadata( false );

            //-------------------------------------------------------------------
            // Attempt to create a set of RDS frames from the input set
            //-------------------------------------------------------------------
            FrameAPI::createRDSSet( frame_files, channels, options );

            //---------------------------------------------------------------------
            // Cache cleanup
            //---------------------------------------------------------------------
            file_cache.cleanup( );
            BOOST_CHECK_MESSAGE( true, "No checking" );
        }
        catch ( const SkipException& Exception )
        {
            BOOST_WARN_MESSAGE( true, "SKIPPED" );
        }
        catch ( const std::exception& Exception )
        {
            BOOST_CHECK_MESSAGE( false,
                                 "Caught exception: " << Exception.what( ) );
        }
    }
};

struct start_time_delta_t : public std::pair< INT_4U, INT_4U >
{
public:
    typedef INT_4U                                     start_time_type;
    typedef INT_4U                                     delta_t_type;
    typedef std::pair< start_time_type, delta_t_type > base_type;

    using base_type::base_type;

    inline start_time_type
    start_time( )
    {
        return first;
    };
    inline delta_t_type
    delta_t( )
    {
        return second;
    };

private:
    using base_type::first;
    using base_type::second;
};

struct file_times_and_delta_t //
    : public std::set< start_time_delta_t >
{
    typedef std::set< start_time_delta_t > base_type;
    using base_type::base_type;
};

template < typename FilenameCollectionType >
static void
verify_filenames_exist( FilenameCollectionType const& Filenames )
{
    for ( auto filename : Filenames )
    {
        BOOST_CHECK_MESSAGE( //
            boost::filesystem::exists( filename ), //
            "Missing file " << filename.string( ) //
        );
    }
}

template < typename FilenameCollectionType,
           typename BaseFilenameCollectionType >
static void
generate_filename_collection( boost::filesystem::path const&    RootDir,
                              BaseFilenameCollectionType const& BaseFilenames,
                              FilenameCollectionType&           Filenames )
{
    for ( auto basefilename : BaseFilenames )
    {
        auto filename = RootDir;
        filename /= basefilename;

        Filenames.emplace_back( filename );
    }
}

template < typename FilenameType, typename BaseFilenameCollectionType >
static void
generate_filename_collection( boost::filesystem::path const&    RootDir,
                              BaseFilenameCollectionType const& BaseFilenames,
                              std::set< FilenameType >&         Filenames )
{
    for ( auto basefilename : BaseFilenames )
    {
        auto filename = RootDir;
        filename /= basefilename;

        Filenames.insert( filename );
    }
}

template < typename BaseFilenameCollectionType >
static void
verify_filename_collection( boost::filesystem::path const&    RootDir,
                            BaseFilenameCollectionType const& BaseFilenames )
{
    using namespace boost::filesystem;

    if ( is_directory( RootDir ) )
    {
        std::set< path > directory_entries;
        for ( auto& directory_entry : directory_iterator( RootDir ) )
        {
            directory_entries.insert( directory_entry.path( ) );
        }

        std::set< path > filenames;
        generate_filename_collection( //
            RootDir, //
            BaseFilenames, //
            filenames //
        );

        std::vector< path > path_differences;

        path_differences.clear( );
        std::set_difference( //
            filenames.begin( ), //
            filenames.end( ), //
            directory_entries.begin( ), //
            directory_entries.end( ), //
            std::inserter( path_differences, path_differences.begin( ) ) //
        );
        for ( auto missing_file : path_differences )
        {
            BOOST_CHECK_MESSAGE( //
                false, //
                "Missing file " << ( missing_file.string( ) ) //
            );
        }

        path_differences.clear( );
        std::set_difference( //
            directory_entries.begin( ), //
            directory_entries.end( ), //
            filenames.begin( ), //
            filenames.end( ), //
            std::inserter( path_differences, path_differences.begin( ) ) //
        );
        for ( auto unexpected_file : path_differences )
        {
            BOOST_CHECK_MESSAGE( //
                false, //
                "Found unexpected  file " << ( unexpected_file.string( ) ) //
            );
        }
    }
}

struct live_data_configuration_type
{
    std::string            source_path = "";
    INT_4U                 start_time = 0;
    INT_4U                 duration = 4096;
    INT_4U                 frames_per_output_file = 1;
    INT_4U                 seconds_per_output_frame = 4096;
    file_times_and_delta_t expected_file_patterns;
    INT_4U                 frame_file_start_time = 0;
    std::string            frame_file_ifo = "H";
    std::string            frame_file_description = "H1_llhoft";
    INT_4U                 frame_file_deltat = 1;
    FrameAPI::start_type   rds_start_time = 0;
    INT_4U                 rds_duration = 0;
};

void
test_live_data( const live_data_configuration_type& Configuration )
{
    using namespace FrameAPI::LIGO::O4;

    auto rds_validator = //
        test_rds< testing::live_data_cache >(
            boost::unit_test::framework::current_test_case( ).p_name );

    std::ostringstream channel_names_stream;

    channel_names_stream //
        << "H1:" << CHANNEL_NAME //
        << "="
        << "H1:" << CHANNEL_NAME << "_AR" //
        << ","
        << "H1:DMT-DQ_VECTOR=H1:DMT-DQ_VECTOR_AR" //
        << ","
        << "H1:GDS-CALIB_STRAIN=H1:GDS-CALIB_STRAIN_AR" //
        << ","
        << "H1:GDS-CALIB_STRAIN_CLEAN=H1:GDS-CALIB_STRAIN_CLEAN_AR" //
        << ","
        << "H1:GDS-CALIB_STRAIN_NOLINES=H1:GDS-CALIB_STRAIN_NOLINES_AR" //
        ;
    rds_validator.channel_names = channel_names_stream.str( );

    std::string analysis_ready_channel_name( "H1:" );
    analysis_ready_channel_name += FrameAPI::LIGO::O4::CHANNEL_NAME;

    rds_validator.options.AnalysisReadyChannelName(
        analysis_ready_channel_name );
    rds_validator.options.AnalysisReadyMask( StateMask::OBS_INTENT |
                                             StateMask::OBS_READY );
    //-------------------------------------------------------------------
    // output options
    //-------------------------------------------------------------------
    rds_validator.options.SecondsPerFrame(
        Configuration.seconds_per_output_frame );
    rds_validator.options.FramesPerFile( Configuration.frames_per_output_file );
    //-------------------------------------------------------------------
    // configure source generations
    //-------------------------------------------------------------------
    rds_validator.file_cache.source_path = Configuration.source_path;
    rds_validator.file_cache.frame_file_ifo = Configuration.frame_file_ifo;
    rds_validator.file_cache.frame_file_description =
        Configuration.frame_file_description;
    rds_validator.file_cache.start_time =
        ( ( Configuration.frame_file_start_time != 0 )
              ? FrameCPP::Time( Configuration.frame_file_start_time, 0 )
              : FrameCPP::Time( Configuration.start_time, 0 ) );
    rds_validator.rds_start_time = Configuration.rds_start_time;
    rds_validator.file_cache.frame_file_deltat =
        Configuration.frame_file_deltat;
    rds_validator.file_cache.duration = Configuration.duration;
    rds_validator.rds_duration = Configuration.rds_duration;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------

    std::set< boost::filesystem::path > expected_files;
    if ( Configuration.expected_file_patterns.size( ) != 0 )
    {
        expected_files.insert( rds_validator.test_name + "_output.md5" );
        for ( auto time_info : Configuration.expected_file_patterns )
        {
            std::ostringstream filename;
            filename << //
                "H-" //
                     << rds_validator.test_name //
                     << "-" << time_info.start_time( ) //
                     << "-" << time_info.delta_t( ) //
                     << ".gwf", //
                expected_files.insert( filename.str( ) );
        }
    }
    verify_filename_collection( //
        rds_validator.output_directory, //
        expected_files //
    );
}

void
test_live_data( const std::string&            SourcePath,
                INT_4U                        StartTime,
                const file_times_and_delta_t& ExpectedFilePatterns )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path = SourcePath;
    live_data_configuration.start_time = StartTime;
    live_data_configuration.expected_file_patterns = ExpectedFilePatterns;

    test_live_data( live_data_configuration );
}

#if TEST_REDUCE_FRAME
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );

    rds_validator.channel_names = //
        "H1:DataVector" //
        ",H1:analysis_ready" //
        ;

    rds_validator( );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;

    rds_validator( );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_MULTI
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_multi )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_v1_AR" //
        ",H1:DataVector=H1:DataVector_v2_AR" //
        ;

    rds_validator( );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_ANALYSIS_READY
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_analysis_ready )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:analysis_ready" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_ALL;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_ALL;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000000-64.gwf", //
            "H-" + rds_validator.test_name + "-600000064-64.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1111
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_1111 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        //",H1:DataVector=H1:DataVector_AR1" //
        //",H1:analysis_ready" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_ALL;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_ALL;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000000-64.gwf", //
            "H-" + rds_validator.test_name + "-600000064-64.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0000
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_0000 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_NONE;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_NONE;

    rds_validator( );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0011
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_0011 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR"
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_NONE;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_ALL;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000064-64.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1100
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_1100 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_ALL;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_NONE;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000000-64.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1110
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_1110 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_ALL;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_START;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000000-64.gwf", //
            "H-" + rds_validator.test_name + "-600000064-32.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0110
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_0110 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_END;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_START;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000032-64.gwf", //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0111
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_0111 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_END;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_ALL;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000032-64.gwf", //
            "H-" + rds_validator.test_name + "-600000096-32.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_0101
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_0101 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_END;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_END;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000032-32.gwf", //
            "H-" + rds_validator.test_name + "-600000096-32.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1001
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_1001 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_START;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_END;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000000-32.gwf", //
            "H-" + rds_validator.test_name + "-600000096-32.gwf" //
        } //
    );
}
#endif

#if TEST_REDUCE_FRAME_CHANNEL_RENAME_AR_1010
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_channel_rename_ar_1010 )
{
    auto rds_validator =
        test_rds<>( boost::unit_test::framework::current_test_case( ).p_name );
    auto& analysis_ready_filler_scheme(
        rds_validator.file_cache.analysis_ready_data_filler
            .analysis_ready_filler_scheme );

    rds_validator.channel_names = //
        "H1:DataVector=H1:DataVector_AR" //
        ",H1:mundane_channel=H1:mundane_channel_AR" //
        ;
    rds_validator.options.AnalysisReadyChannelName( "H1:analysis_ready" );
    rds_validator.options.AnalysisReadyMask( 0x03 );

    rds_validator.file_cache.analysis_ready_data_filler
        .resize_analysis_ready_filler_scheme( 2 );
    analysis_ready_filler_scheme[ 0 ] = testing::AR_READY_10;
    analysis_ready_filler_scheme[ 1 ] = testing::AR_READY_10;

    rds_validator( );

    //---------------------------------------------------------------------
    // Verify that the correct files were generated
    //---------------------------------------------------------------------
    verify_filename_collection( //
        rds_validator.output_directory, //
        std::set< boost::filesystem::path >{
            rds_validator.test_name + "_output.md5", //
            "H-" + rds_validator.test_name + "-600000000-32.gwf", //
            "H-" + rds_validator.test_name + "-600000064-32.gwf" //
        } //
    );
}
#endif

#if TEST_LLCACHE_DATA_V1
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_v1 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137138";
    live_data_configuration.start_time = 1371388626;
    live_data_configuration.expected_file_patterns = {
        { 1371388626, 95 } //
    };

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V1_1
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_v1_1 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137138";
    live_data_configuration.start_time = 1371388626;
    live_data_configuration.expected_file_patterns = {
        { 1371388626, 64 }, //
        { 1371388690, 31 } //
    };
    live_data_configuration.seconds_per_output_frame = 64;

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V2
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_v2 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137138";
    live_data_configuration.start_time = 1371385554;
    live_data_configuration.expected_file_patterns = {
        { 1371385785, 2936 } //
    };

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V2_1
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_v2_1 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137138";
    live_data_configuration.start_time = 1371385554;
    live_data_configuration.duration = 8192;
    live_data_configuration.expected_file_patterns = {
        { 1371385785, 2936 } //
    };

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V3
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_v3 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137075";
    live_data_configuration.start_time = 1370755507;
    live_data_configuration.expected_file_patterns = {
        { 1370758509, 1094 } //
    };

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V4
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_v4 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137023";
    live_data_configuration.start_time = 1370238976;
    live_data_configuration.expected_file_patterns = {};

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V5
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_v5 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137024";
    live_data_configuration.start_time = 1370248000;
    live_data_configuration.duration = 1000;
    live_data_configuration.expected_file_patterns = { { 1370248000, 1000 } };
    live_data_configuration.frames_per_output_file = 64;
    live_data_configuration.seconds_per_output_frame = 64;

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V6
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_010 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137193";
    live_data_configuration.start_time = 1371935500;
    live_data_configuration.duration = 1000;
    live_data_configuration.expected_file_patterns = { { 1371935774, 506 } };
    live_data_configuration.frames_per_output_file = 64;
    live_data_configuration.seconds_per_output_frame = 64;

    test_live_data( live_data_configuration );
}
#endif

#if TEST_LLCACHE_DATA_V7
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_llcache_data_101 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/llcache/kafka/H1/H-H1_llhoft-137193";
    live_data_configuration.start_time = 1371935700;
    live_data_configuration.duration = 2000;
    live_data_configuration.expected_file_patterns = { { 1371935774, 506 },
                                                       { 1371936990, 710 } };
    live_data_configuration.frames_per_output_file = 64;
    live_data_configuration.seconds_per_output_frame = 64;

    test_live_data( live_data_configuration );
}
#endif

#if TEST_HOFT_C00_DATA_10101
BOOST_AUTO_TEST_CASE( createRDS_reduce_frame_ar_hoft_c00_data_10101 )
{
    live_data_configuration_type live_data_configuration;

    live_data_configuration.source_path =
        "/ifocache/frames/O4/hoft_C00/H1/H-H1_HOFT_C00-136";
    live_data_configuration.frame_file_ifo = "H";
    live_data_configuration.frame_file_description = "H1_HOFT_C00";
    live_data_configuration.frame_file_deltat = 4096;
    live_data_configuration.frame_file_start_time = 1369944064;
    live_data_configuration.start_time = 1369945581;
    live_data_configuration.rds_start_time = 1369945581;
    live_data_configuration.duration = 12288;
    live_data_configuration.rds_duration = 9982;
    live_data_configuration.expected_file_patterns = { { 1369945581, 128 },
                                                       { 1369945733, 1536 },
                                                       { 1369951467, 4096 } };
    live_data_configuration.frames_per_output_file = 64;
    live_data_configuration.seconds_per_output_frame = 64;

    test_live_data( live_data_configuration );
}
#endif /* TEST_HOFT_C00_DATA_10101 */
