//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "../src/config.h"

extern "C" {
#include <unistd.h>
}

#include <cstdlib>

#include <map>
#include <fstream>
#include <sstream>

#include "createFrameGroupCommon.hh"
#include "TestConfig.h"

using std::getenv;

namespace
{
    typedef std::map< int, std::list< channel_info_type > > channel_level_map;
    typedef std::map< run_type, channel_level_map >         channel_run_map;
    typedef std::map< ifo_type, channel_run_map >           channel_ifo_map;

    struct dir_cache_info
    {
        const std::string s_directory;
        const INT_4U      s_start_time;
        const INT_4U      s_end_time;
        const INT_4U      s_dt;

        inline dir_cache_info( const std::string& Directory,
                               INT_4U             StartTime,
                               INT_4U             EndTime,
                               INT_4U             Dt )
            : s_directory( Directory ), s_start_time( StartTime ),
              s_end_time( EndTime ), s_dt( Dt )
        {
        }
    };

    typedef std::map< int, std::list< dir_cache_info > > dir_cache_level_map;
    typedef std::map< run_type, dir_cache_level_map >    dir_cache_run_map;
    typedef std::map< ifo_type, dir_cache_run_map >      dir_cache_ifo_map;

    bool initialize( );
    void load_channel_list( std::string AdcDecimateDir,
                            ifo_type    IFO,
                            run_type    Run,
                            int         Level );

    static channel_ifo_map   channel_info;
    static dir_cache_ifo_map dir_cache;

    static bool initialized = initialize( );
} // namespace

std::list< test_data_type > test_data;

namespace
{
    struct FrameSet
    {
        INT_4U      s_start;
        INT_4U      s_dt;
        INT_4U      s_number_of_files;
        std::string s_ifo;
        std::string s_type;

        FrameSet( INT_4U             Start,
                  INT_4U             DeltaTime,
                  INT_4U             NumberOfFiles,
                  const std::string& IFO,
                  const std::string& Type );
        std::string GetFilename( INT_4U Offset );
    };

    class Data
    {
    public:
        Data( );

        inline std::string
        GetFilename( const INT_4U FrameSet, const ifo_type IFO, INT_4U Offset )
        {
            return m_ifo_frame_sets[ IFO ][ FrameSet ].GetFilename( Offset );
        }

        INT_4U GetReduceStartTime( const INT_4U       FrameSet,
                                   const ifo_type     IFO,
                                   const unsigned int Frame );
        INT_4U GetReduceEndTime( const INT_4U       FrameSet,
                                 const ifo_type     IFO,
                                 const unsigned int Frame );
        INT_4U GetResampleStartTime( const INT_4U       FrameSet,
                                     const ifo_type     IFO,
                                     const unsigned int Frame );
        INT_4U GetResampleEndTime( const INT_4U       FrameSet,
                                   const ifo_type     IFO,
                                   const unsigned int Frame );

    private:
        typedef std::vector< FrameSet >          frame_sets;
        typedef std::map< ifo_type, frame_sets > ifo_frame_sets;

        ifo_frame_sets m_ifo_frame_sets;

        void init( );

        inline void
        add_frame_set( INT_4U             Start,
                       INT_4U             DeltaTime,
                       INT_4U             NumberOfFiles,
                       const std::string& Type )
        {
            m_ifo_frame_sets[ IFO_LHO ].push_back(
                FrameSet( Start, DeltaTime, NumberOfFiles, "H", Type ) );
            m_ifo_frame_sets[ IFO_LLO ].push_back(
                FrameSet( Start, DeltaTime, NumberOfFiles, "L", Type ) );
        }

    } DataInfo;

    Data::Data( )
    {
        init( );
    }

    void
    Data::init( )
    {
        // E7 data
        add_frame_set( 751791824, 16, 4, "R" );
        // E9 data
        add_frame_set( 727551008, 16, 4, "R" );
        // S3 data
        add_frame_set( 751791824, 16, 4, "R" );
    }

} // namespace

static std::string LHO_DATA_DIR( "/imports/data/frames/S3/LHO/frames/" );
static std::string LLO_DATA_DIR( "/imports/data/frames/S3/LLO/frames/" );
static std::string SFT_DIR( "/scratch/test/frames/S1_sfts_H2TEST_111902/" );

static const char* lho_dirs[] = {
    "/data/frames/E7/LHO/frames/",     "/imports/data/E7/LHO/frames/",
    "/imports/E7/LHO/frames/",         "/imports/data/frames/E9/LHO/",
    "/imports/data/frames/S3/LHO/L0/", "/scratch/mirror/frames/S3/LHO/L0/"
};

static const char* llo_dirs[] = {
    "/data/frames/E7/LLO/frames/",     "/imports/data/E7/LLO/frames/",
    "/imports/E7/LLO/frames/",         "/imports/data/frames/E9/LLO/",
    "/imports/data/frames/S3/LLO/L0/", "/scratch/mirror/frames/S3/LLO/L0/"
};

static const char* DATA_DIR[ IFO_MAX ];

int dataset_values[] = {
    0, // 0 - E7 - 1
    0, // 1 - E7 - 2
    0, // 2 - E7 - 3
    1, // 3 - S9
    2, // 4 - S3
    2, // 5 - S3
};

int dataset = -1;

const unsigned int framecnt( 4 );

static void
init( )
{
    static bool needs_initialization( true );
    if ( needs_initialization )
    {
        INT_4U x = sizeof( lho_dirs ) / sizeof( *lho_dirs );
        while ( x != 0 )
        {
            --x;
            if ( ( access( lho_dirs[ x ], R_OK ) == 0 ) &&
                 ( access( llo_dirs[ x ], R_OK ) == 0 ) )
            {
                DATA_DIR[ IFO_LHO ] = lho_dirs[ x ];
                DATA_DIR[ IFO_LLO ] = llo_dirs[ x ];
                LHO_DATA_DIR = lho_dirs[ x ];
                LLO_DATA_DIR = llo_dirs[ x ];
                dataset = dataset_values[ x ];
                break;
            }
        }
        needs_initialization = false;
    }
}

INT_4U
GetReduceStartTime( const ifo_type IFO, const unsigned int Frame )
{
    init( );

    if ( ( dataset == -1 ) || ( Frame >= framecnt ) )
    {
        throw std::runtime_error( "Unkown frame" );
    }
    return DataInfo.GetReduceStartTime( dataset_values[ dataset ], IFO, Frame );
}

INT_4U
GetReduceEndTime( const ifo_type IFO, const unsigned int Frame )
{
    init( );

    if ( ( dataset == -1 ) || ( Frame >= framecnt ) )
    {
        throw std::runtime_error( "Unkown frame" );
    }
    return DataInfo.GetReduceEndTime( dataset_values[ dataset ], IFO, Frame );
}

INT_4U
GetResampleStartTime( const ifo_type IFO, const unsigned int Frame )
{
    init( );

    if ( ( dataset == -1 ) || ( Frame >= framecnt ) )
    {
        throw std::runtime_error( "Unkown frame" );
    }
    return DataInfo.GetResampleStartTime(
        dataset_values[ dataset ], IFO, Frame );
}

INT_4U
GetResampleEndTime( const ifo_type IFO, const unsigned int Frame )
{
    init( );

    if ( ( dataset == -1 ) || ( Frame >= framecnt ) )
    {
        throw std::runtime_error( "Unkown frame" );
    }
    return DataInfo.GetResampleEndTime( dataset_values[ dataset ], IFO, Frame );
}

std::string
GetFrameName( const ifo_type IFO, const unsigned int Frame )
{
    init( );

    if ( ( dataset == -1 ) || ( Frame >= framecnt ) )
    {
        throw std::runtime_error( "Unkown frame" );
    }
    std::string retval( DATA_DIR[ IFO ] );
    retval += DataInfo.GetFilename( dataset_values[ dataset ], IFO, Frame );
    return retval;
}

void
GetFrameNames( const bool                Resample,
               const INT_4U              StartTime,
               const INT_4U              EndTime,
               const ifo_type            IFO,
               const run_type            Run,
               const int                 Level,
               std::list< std::string >& Filenames )
{
    if ( StartTime > EndTime )
    {
        return;
    }
    for ( std::list< dir_cache_info >::const_iterator
              cur = dir_cache[ IFO ][ Run ][ Level ].begin( ),
              last = dir_cache[ IFO ][ Run ][ Level ].end( );
          cur != last;
          ++cur )
    {
        if ( ( StartTime < cur->s_start_time ) ||
             ( EndTime >= cur->s_end_time ) )
        {
            // Not within range
            continue;
        }
        std::list< std::string > new_filenames;
        const INT_4U             dt = cur->s_dt;
        INT_4U start = StartTime - ( StartTime - cur->s_start_time ) % dt;
        INT_4U end = EndTime;

        if ( Resample )
        {
            start -= cur->s_dt;
            end += cur->s_dt;
        }

        for ( INT_4U s = start, e = end; s < e; s += dt )
        {
            std::ostringstream f;
            f << cur->s_directory << s << "-" << dt << ".gwf";

            new_filenames.push_back( f.str( ) );
        }

        bool all_readable = true;
        for ( std::list< std::string >::const_iterator
                  fcur = new_filenames.begin( ),
                  flast = new_filenames.end( );
              fcur != flast;
              ++fcur )
        {
            if ( access( fcur->c_str( ), R_OK ) != 0 )
            {
                all_readable = false;
                break;
            }
        }
        if ( ( new_filenames.size( ) > 0 ) && ( all_readable == true ) )
        {
            // Have files of interest.
            Filenames.splice( Filenames.end( ), new_filenames );
            break;
        }
    }
}

void
GetChannelList( const ifo_type IFO,
                const run_type Run,
                int            Level,
                channel_list&  Channels )
{
    for ( channel_list::const_iterator
              cur = channel_info[ IFO ][ Run ][ Level ].begin( ),
              last = channel_info[ IFO ][ Run ][ Level ].end( );
          cur != last;
          ++cur )
    {
        Channels.push_back( *cur );
    }
}

void
init_tests( )
{
    init( );

    if ( dataset == -1 )
    {
        return;
    }

    //---------------------------------------------------------------------
    // Test whole frame
    //---------------------------------------------------------------------
    test_data_type                    new_elem;
    ConditionData::channel_input_type new_input_type;

    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 0;
    test_data.back( ).s_channels.back( ).m_delta = 16;
    //---------------------------------------------------------------------
    // Test continuous span of data
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 15;
    test_data.back( ).s_channels.back( ).m_delta = 1;
    //---------------------------------------------------------------------
    // Test continuous span of data
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 2 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 0;
    test_data.back( ).s_channels.back( ).m_delta = 1;
    //---------------------------------------------------------------------
    // Test continuous span of data
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 1 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 0;
    test_data.back( ).s_channels.back( ).m_delta = 32;
    //---------------------------------------------------------------------
    // Test continuous span of data starting from non zero
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 1 ) );
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 2 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 37;
    //---------------------------------------------------------------------
    // Test continuous span of data trimming some data off the end
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 1 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 0;
    test_data.back( ).s_channels.back( ).m_delta = 27;
    //---------------------------------------------------------------------
    // Test continuous span of data trimming some data off both ends
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 1 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 20;
    //---------------------------------------------------------------------
    // Test with multiple channels
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 1 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 27;
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_I";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 17;
    test_data.back( ).s_channels.back( ).m_delta = 15;
    //---------------------------------------------------------------------
    // Test continuous span of data
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 0;
    test_data.back( ).s_channels.back( ).m_delta = 1;
    //---------------------------------------------------------------------
    // Test continuous span of data
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 0 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 8;
    test_data.back( ).s_channels.back( ).m_delta = 1;
    //*********************************************************************
    // Testing with resampling
    //*********************************************************************
    //---------------------------------------------------------------------
    // Test continuous span of data trimming some data off both ends
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 1 ) );
    test_data.back( ).s_files.push_back( GetFrameName( IFO_LHO, 2 ) );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
    test_data.back( ).s_channels.back( ).s_q = 16;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 20;
    //---------------------------------------------------------------------
    // Test having large data sets
    //---------------------------------------------------------------------
    if ( dataset == 0 )
    {
        test_data.push_back( new_elem ); // Make space for new record
        test_data.back( ).s_channels.push_back( new_input_type );
        {
            INT_4U       cnt = 2032;
            const int    inc( 16 );
            const INT_4U span( 64 );
            const INT_4U end( cnt + span );

            for ( INT_4U x( cnt ); x < end; x += inc )
            {
                std::ostringstream oss;
                oss << LHO_DATA_DIR << "H-R-69396" << x << "-16.gwf";

                test_data.back( ).s_files.push_back( oss.str( ) );
            }

            test_data.back( ).s_channels.back( ).m_delta = span;
        }
        test_data.back( ).s_channels.back( ).s_mode =
            ConditionData::channel_input_type::NAME;
        test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
        test_data.back( ).s_channels.back( ).s_q = 1;
        test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
        test_data.back( ).s_channels.back( ).m_offset = 5;
    }
    //---------------------------------------------------------------------
    // Test having large data sets with resampling
    //---------------------------------------------------------------------
    if ( dataset == 0 )
    {
        test_data.push_back( new_elem ); // Make space for new record
        test_data.back( ).s_channels.push_back( new_input_type );
        {
            INT_4U       cnt = 2032;
            const int    inc( 16 );
            const INT_4U span( 128 );
            const INT_4U end( cnt + span );

            for ( INT_4U x( cnt ); x < end; x += inc )
            {
                std::ostringstream oss;
                oss << LHO_DATA_DIR << "H-R-69396" << x << "-16.gwf";

                test_data.back( ).s_files.push_back( oss.str( ) );
            }

            test_data.back( ).s_channels.back( ).m_delta = span;
        }
        test_data.back( ).s_channels.back( ).s_mode =
            ConditionData::channel_input_type::NAME;
        test_data.back( ).s_channels.back( ).s_name = "H2:LSC-AS_Q";
        test_data.back( ).s_channels.back( ).s_q = 8;
        test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
        test_data.back( ).s_channels.back( ).m_offset = 5;
    }
    //*********************************************************************
    // Testing with gaps
    //*********************************************************************
    //---------------------------------------------------------------------
    // Test continuous span of data starting from non zero
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714150032-2048.gwf" );
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714152080-2048.gwf" );
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714154128-2048.gwf" );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::INDEX;
    test_data.back( ).s_channels.back( ).s_index = 0;
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::PROC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 1;
    //---------------------------------------------------------------------
    // Test continuous span of data starting from non zero
    //---------------------------------------------------------------------
    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714150032-2048.gwf" );
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714152080-2048.gwf" );
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714154128-2048.gwf" );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "all";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::PROC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 1;

    test_data.push_back( new_elem ); // Make space for new record
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714150032-2048.gwf" );
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714152080-2048.gwf" );
    test_data.back( ).s_files.push_back(
        SFT_DIR + "H-P_LDAS_KPD_SFT_TEST050_H2-714154128-2048.gwf" );
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "all";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::ADC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 1;
    test_data.back( ).s_channels.push_back( new_input_type );
    test_data.back( ).s_channels.back( ).s_mode =
        ConditionData::channel_input_type::NAME;
    test_data.back( ).s_channels.back( ).s_name = "all";
    test_data.back( ).s_channels.back( ).s_q = 1;
    test_data.back( ).s_channels.back( ).s_data_type = ConditionData::PROC;
    test_data.back( ).s_channels.back( ).m_offset = 5;
    test_data.back( ).s_channels.back( ).m_delta = 1;
}

namespace
{
    FrameSet::FrameSet( INT_4U             Start,
                        INT_4U             DeltaTime,
                        INT_4U             NumberOfFiles,
                        const std::string& IFO,
                        const std::string& Type )
        : s_start( Start ), s_dt( DeltaTime ),
          s_number_of_files( NumberOfFiles ), s_ifo( IFO ), s_type( Type )
    {
    }

    std::string
    FrameSet::GetFilename( INT_4U Offset )
    {
        if ( Offset >= s_number_of_files )
        {
            std::ostringstream msg;
            msg << "Offset (" << Offset << ")is beyond the number of files"
                << " (" << s_number_of_files << ")" << std::endl;
            throw std::range_error( msg.str( ) );
        }
        std::ostringstream filename;
        filename << s_ifo << "-" << s_type << "-" << s_start + ( Offset * s_dt )
                 << "-" << s_dt << ".gwf";
        return filename.str( );
    }

    INT_4U
    Data::GetReduceStartTime( const INT_4U       FrameGroup,
                              const ifo_type     IFO,
                              const unsigned int Frame )
    {
        const FrameSet& fs( m_ifo_frame_sets[ IFO ][ FrameGroup ] );
        return fs.s_start + ( fs.s_dt * Frame );
    }

    INT_4U
    Data::GetReduceEndTime( const INT_4U       FrameGroup,
                            const ifo_type     IFO,
                            const unsigned int Frame )
    {
        const FrameSet& fs( m_ifo_frame_sets[ IFO ][ FrameGroup ] );
        return fs.s_start + ( ( Frame + 1 ) * fs.s_dt );
    }

    INT_4U
    Data::GetResampleStartTime( const INT_4U       FrameGroup,
                                const ifo_type     IFO,
                                const unsigned int Frame )
    {
        const FrameSet& fs( m_ifo_frame_sets[ IFO ][ FrameGroup ] );
        return fs.s_start + ( ( Frame + 1 ) * fs.s_dt );
    }

    INT_4U
    Data::GetResampleEndTime( const INT_4U       FrameGroup,
                              const ifo_type     IFO,
                              const unsigned int Frame )
    {
        const FrameSet& fs( m_ifo_frame_sets[ IFO ][ FrameGroup ] );
        return fs.s_start + ( Frame * fs.s_dt );
    }
} // namespace

//-----------------------------------------------------------------------
// Channel Information
//-----------------------------------------------------------------------

namespace
{
    bool
    initialize( )
    {
        //===================================================================
        // Establish channels for various levels and runs of RDSs
        //===================================================================
        //-------------------------------------------------------------------
        // Get the directory where the channel sets are kept
        //-------------------------------------------------------------------
        std::string adcdecimate_dir( SRCDIR );
        adcdecimate_dir += "/../../../../api/test/System";
        //===================================================================
        // Science Run #3
        //===================================================================
        //-------------------------------------------------------------------
        // Channel lists for RDS generation
        //-------------------------------------------------------------------
        load_channel_list( adcdecimate_dir, IFO_LHO, RUN_S3, 1 );
        load_channel_list( adcdecimate_dir, IFO_LHO, RUN_S3, 2 );
        load_channel_list( adcdecimate_dir, IFO_LHO, RUN_S3, 3 );
        load_channel_list( adcdecimate_dir, IFO_LLO, RUN_S3, 1 );
        load_channel_list( adcdecimate_dir, IFO_LLO, RUN_S3, 2 );
        load_channel_list( adcdecimate_dir, IFO_LLO, RUN_S3, 3 );
        //-------------------------------------------------------------------
        // Frame file caches
        //-------------------------------------------------------------------
        dir_cache[ IFO_LHO ][ RUN_S3 ][ 0 ].push_back( dir_cache_info(
            "/scratch/test/frames/S3/LHO/H-R-", 751800000, 751803616, 16 ) );
        dir_cache[ IFO_LLO ][ RUN_S3 ][ 0 ].push_back( dir_cache_info(
            "/scratch/test/frames/S3/LLO/L-R-", 751800000, 751803616, 16 ) );
        //===================================================================
        // Science Run #4
        //===================================================================
        //-------------------------------------------------------------------
        // Channel lists for RDS generation
        //-------------------------------------------------------------------
        load_channel_list( adcdecimate_dir, IFO_LHO, RUN_S4, 1 );
        load_channel_list( adcdecimate_dir, IFO_LHO, RUN_S4, 3 );
        load_channel_list( adcdecimate_dir, IFO_LLO, RUN_S4, 1 );
        load_channel_list( adcdecimate_dir, IFO_LLO, RUN_S4, 3 );
        //-------------------------------------------------------------------
        // :TODO: Frame file caches
        //-------------------------------------------------------------------

        return true;
    }

    void
    load_channel_list( std::string AdcDecimateDir,
                       ifo_type    IFO,
                       run_type    Run,
                       int         Level )
    {
        std::ostringstream filename;
        //-------------------------------------------------------------------
        // Calculate IFO
        //-------------------------------------------------------------------
        filename << "adcdecimate_";
        switch ( IFO )
        {
        case IFO_LHO:
            filename << "H";
            break;
        case IFO_LLO:
            filename << "L";
            break;
        default:
            throw std::runtime_error( "Unknown IFO type" );
        }
        filename << "-RDS_R_L" << Level << "-";
        //-------------------------------------------------------------------
        // Calculate the Run type
        //-------------------------------------------------------------------
        switch ( Run )
        {
        case RUN_S3:
            filename << "S3";
            break;
        case RUN_S4:
            filename << "S4";
            break;
        }
        //-------------------------------------------------------------------
        // File suffix
        //-------------------------------------------------------------------
        filename << ".txt";

        //-------------------------------------------------------------------
        // Open the file
        //-------------------------------------------------------------------
        AdcDecimateDir += "/" + filename.str( );
        std::ifstream f( AdcDecimateDir.c_str( ) );
        //-------------------------------------------------------------------
        // Read the data
        //-------------------------------------------------------------------
        while ( true )
        {
            std::string channel_name;
            int         decimation;

            f >> channel_name >> decimation;
            if ( f.good( ) )
            {
                channel_info[ IFO ][ Run ][ Level ].push_back(
                    channel_info_type( channel_name, decimation ) );
            }
            else
            {
                break;
            }
        }
        //-------------------------------------------------------------------
        // Close the file
        //-------------------------------------------------------------------
        f.close( );
    }
} // namespace
