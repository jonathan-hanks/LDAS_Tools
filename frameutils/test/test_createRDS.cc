//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#include "frameAPI/createRDS.hh"

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/counting_range.hpp>

struct rds_global_fixture
{
    rds_global_fixture( )
    {
        //-------------------------------------------------------------------
        // Setup logging as is needed by query class
        //-------------------------------------------------------------------
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        static char cwd_buffer[ 2048 ];
        if ( getcwd( cwd_buffer,
                     sizeof( cwd_buffer ) / sizeof( *cwd_buffer ) ) ==
             (const char*)NULL )
        {
            exit( 1 );
        }
        std::string                       cwd( cwd_buffer );
        GenericAPI::Log::stream_file_type fs( new GenericAPI::Log::StreamFile );
        LDASTools::AL::Log::stream_type   s;

        s = fs;
        fs->FilenameExtension( GenericAPI::LogFormatter( )->FileExtension( ) );
        GenericAPI::LogFormatter( )->Stream( s );
        GenericAPI::setLogTag( "test_query" );
        GenericAPI::LoggingInfo::LogDirectory( cwd );

        std::cout << "global setup" << std::endl;
    }

    ~rds_global_fixture( )
    {
        std::cout << "global teardown" << std::endl;
    }
};

namespace testing
{
    struct channel_name
    {
        std::string original_channel_name;
        int         resampling;
        std::string renamed_channel_name;
    };

    void
    construct_channel_file(
        boost::filesystem::path&          Filename,
        std::vector< std::string > const& ChannelDescriptors )
    {
        boost::filesystem::ofstream channel_stream{ Filename };
        for ( auto channel_descriptor : ChannelDescriptors )
        {
            channel_stream << channel_descriptor << std::endl;
        }
        channel_stream.close( );
    }

    void
    validate( FrameAPI::channel_container_type const&     GeneratedChannels,
              std::vector< testing::channel_name > const& ExpectedChannels )
    {
        auto const size_expected = ExpectedChannels.size( );
        BOOST_CHECK_MESSAGE( GeneratedChannels.names.size( ) == size_expected,
                             "GeneratedChannels.names.size( ) is '"
                                 << GeneratedChannels.names.size( )
                                 << "' instead of '" << size_expected << "'" );
        BOOST_CHECK_MESSAGE( GeneratedChannels.resampling.size( ) ==
                                 size_expected,
                             "GeneratedChannels.resampling.size( ) is '"
                                 << GeneratedChannels.resampling.size( )
                                 << "' instead of '" << size_expected << "'" );
        if ( ( GeneratedChannels.names.size( ) != size_expected ) ||
             ( GeneratedChannels.resampling.size( ) != size_expected ) )
        {
            return;
        }
        for ( auto channel_index :
              boost::counting_range( size_t( 0 ), size_t( size_expected ) ) )
        {
            BOOST_CHECK_MESSAGE(
                GeneratedChannels.names[ channel_index ].old_channel_name ==
                    ExpectedChannels[ channel_index ].original_channel_name,
                "GeneratedChannels.names[ "
                    << channel_index << " ].old_channel_name is '"
                    << GeneratedChannels.names[ channel_index ].old_channel_name
                    << "' instead of '"
                    << ExpectedChannels[ channel_index ].original_channel_name
                    << "'" );
            BOOST_CHECK_MESSAGE(
                GeneratedChannels.names[ channel_index ].new_channel_name ==
                    ExpectedChannels[ channel_index ].renamed_channel_name,
                "GeneratedChannels.names[ "
                    << channel_index << " ].new_channel_name is '"
                    << GeneratedChannels.names[ channel_index ].new_channel_name
                    << "' instead of '"
                    << ExpectedChannels[ channel_index ].renamed_channel_name
                    << "'" );
            BOOST_CHECK_MESSAGE(
                GeneratedChannels.resampling[ channel_index ] ==
                    ExpectedChannels[ channel_index ].resampling,
                "GeneratedChannels.resampling[ "
                    << channel_index << " ] is '"
                    << GeneratedChannels.resampling[ channel_index ]
                    << "' instead of '"
                    << ExpectedChannels[ channel_index ].resampling << "'" );
        }
    }
} // namespace testing

BOOST_GLOBAL_FIXTURE( rds_global_fixture );

BOOST_AUTO_TEST_CASE( channel_simple )
{
    static const int         size_expected = 1;
    static const std::string test_channel_string( "H1:channel1" // channel name
    );
    static const std::string name_expected( "H1:channel1" );
    static const int         resample_expected = 1;
    static const std::string rename_expected( "H1:channel1" );

    FrameAPI::channel_container_type channels;
    channels.ParseChannelName( test_channel_string );

    BOOST_CHECK_MESSAGE( channels.names.size( ) == size_expected,
                         "channels.names.size( ) is '"
                             << channels.names.size( ) << "' instead of '"
                             << size_expected << "'" );
    BOOST_CHECK_MESSAGE( channels.resampling.size( ) == size_expected,
                         "channels.resampling.size( ) is '"
                             << channels.resampling.size( ) << "' instead of '"
                             << size_expected << "'" );
    if ( ( channels.names.size( ) == 1 ) &&
         ( channels.resampling.size( ) == 1 ) )
    {
        BOOST_CHECK_MESSAGE( channels.names.front( ).old_channel_name ==
                                 name_expected,
                             "channels.names.front( ).old_channel_name is '"
                                 << channels.names.front( ).old_channel_name
                                 << "' instead of '" << name_expected << "'" );
        BOOST_CHECK_MESSAGE(
            channels.names.front( ).new_channel_name == rename_expected,
            "channels.names.front( ).new_channel_name is '"
                << channels.names.front( ).new_channel_name << "' instead of '"
                << rename_expected << "'" );
        BOOST_CHECK_MESSAGE( channels.resampling.front( ) == resample_expected,
                             "channels.resampling.front( ) is '"
                                 << channels.resampling.front( )
                                 << "' instead of '" << resample_expected
                                 << "'" );
    }
}

BOOST_AUTO_TEST_CASE( channel_resample )
{
    static const int         size_expected = 1;
    static const std::string test_channel_string(
        "H1:channel1!4" // channel name
    );
    static const std::string name_expected( "H1:channel1" );
    static const int         resample_expected = 4;
    static const std::string rename_expected( "H1:channel1" );

    FrameAPI::channel_container_type channels;
    channels.ParseChannelName( test_channel_string );

    BOOST_CHECK_MESSAGE( channels.names.size( ) == size_expected,
                         "channels.names.size( ) is '"
                             << channels.names.size( ) << "' instead of '"
                             << size_expected << "'" );
    BOOST_CHECK_MESSAGE( channels.resampling.size( ) == size_expected,
                         "channels.resampling.size( ) is '"
                             << channels.resampling.size( ) << "' instead of '"
                             << size_expected << "'" );
    if ( ( channels.names.size( ) == 1 ) &&
         ( channels.resampling.size( ) == 1 ) )
    {
        BOOST_CHECK_MESSAGE( channels.names.front( ).old_channel_name ==
                                 name_expected,
                             "channels.names.front( ).old_channel_name is '"
                                 << channels.names.front( ).old_channel_name
                                 << "' instead of '" << name_expected << "'" );
        BOOST_CHECK_MESSAGE(
            channels.names.front( ).new_channel_name == rename_expected,
            "channels.names.front( ).new_channel_name is '"
                << channels.names.front( ).new_channel_name << "' instead of '"
                << rename_expected << "'" );
        BOOST_CHECK_MESSAGE( channels.resampling.front( ) == resample_expected,
                             "channels.resampling.front( ) is '"
                                 << channels.resampling.front( )
                                 << "' instead of '" << resample_expected
                                 << "'" );
    }
}

BOOST_AUTO_TEST_CASE( channel_rename )
{
    static const int         size_expected = 1;
    static const std::string test_channel_string(
        "H1:channel1=H1:channel1_AR" // channel name
    );
    static const std::string name_expected( "H1:channel1" );
    static const int         resample_expected = 1;
    static const std::string rename_expected( "H1:channel1_AR" );

    FrameAPI::channel_container_type channels;
    channels.ParseChannelName( test_channel_string );

    BOOST_CHECK_MESSAGE( channels.names.size( ) == size_expected,
                         "channels.names.size( ) is '"
                             << channels.names.size( ) << "' instead of '"
                             << size_expected << "'" );
    BOOST_CHECK_MESSAGE( channels.resampling.size( ) == size_expected,
                         "channels.resampling.size( ) is '"
                             << channels.resampling.size( ) << "' instead of '"
                             << size_expected << "'" );
    if ( ( channels.names.size( ) == 1 ) &&
         ( channels.resampling.size( ) == 1 ) )
    {
        BOOST_CHECK_MESSAGE( channels.names.front( ).old_channel_name ==
                                 name_expected,
                             "channels.names.front( ).old_channel_name is '"
                                 << channels.names.front( ).old_channel_name
                                 << "' instead of '" << name_expected << "'" );
        BOOST_CHECK_MESSAGE(
            channels.names.front( ).new_channel_name == rename_expected,
            "channels.names.front( ).new_channel_name is '"
                << channels.names.front( ).new_channel_name << "' instead of '"
                << rename_expected << "'" );
        BOOST_CHECK_MESSAGE( channels.resampling.front( ) == resample_expected,
                             "channels.resampling.front( ) is '"
                                 << channels.resampling.front( )
                                 << "' instead of '" << resample_expected
                                 << "'" );
    }
}

BOOST_AUTO_TEST_CASE( channel_resample_and_rename )
{
    static const int         size_expected = 1;
    static const std::string test_channel_string(
        "H1:channel1!4=H1:channel1_AR" // channel name
    );
    static const std::string name_expected( "H1:channel1" );
    static const int         resample_expected = 4;
    static const std::string rename_expected( "H1:channel1_AR" );

    FrameAPI::channel_container_type channels;
    channels.ParseChannelName( test_channel_string );

    BOOST_CHECK_MESSAGE( channels.names.size( ) == size_expected,
                         "channels.names.size( ) is '"
                             << channels.names.size( ) << "' instead of '"
                             << size_expected << "'" );
    BOOST_CHECK_MESSAGE( channels.resampling.size( ) == size_expected,
                         "channels.resampling.size( ) is '"
                             << channels.resampling.size( ) << "' instead of '"
                             << size_expected << "'" );
    if ( ( channels.names.size( ) == 1 ) &&
         ( channels.resampling.size( ) == 1 ) )
    {
        BOOST_CHECK_MESSAGE( channels.names.front( ).old_channel_name ==
                                 name_expected,
                             "channels.names.front( ).old_channel_name is '"
                                 << channels.names.front( ).old_channel_name
                                 << "' instead of '" << name_expected << "'" );
        BOOST_CHECK_MESSAGE(
            channels.names.front( ).new_channel_name == rename_expected,
            "channels.names.front( ).new_channel_name is '"
                << channels.names.front( ).new_channel_name << "' instead of '"
                << rename_expected << "'" );
        BOOST_CHECK_MESSAGE( channels.resampling.front( ) == resample_expected,
                             "channels.resampling.front( ) is '"
                                 << channels.resampling.front( )
                                 << "' instead of '" << resample_expected
                                 << "'" );
    }
}

BOOST_AUTO_TEST_CASE( channel_filename )
{
    static const std::vector< std::string > test_channel_strings = {
        "H1:channel1!4=H1:channel1_AR", //
        "H1:channel2=H1:channel2_AR", //
        "H1:channel3" //
    };
    static const std::vector< testing::channel_name > expected_channels = {
        { "H1:channel1", 4, "H1:channel1_AR" },
        { "H1:channel2", 1, "H1:channel2_AR" },
        { "H1:channel3", 1, "H1:channel3" }
    };

    boost::filesystem::path channel_text_file(
        boost::filesystem::current_path( ) );
    channel_text_file /= "channel_filename_channels.txt";

    testing::construct_channel_file( channel_text_file, test_channel_strings );

    FrameAPI::channel_container_type channels;
    channels.ParseChannelName( channel_text_file.string( ) );

    validate( channels, expected_channels );
}

BOOST_AUTO_TEST_CASE( channel_from_filename_and_inline )
{
    static std::string const channel_descriptor{
        "H1:MyChannel=H1:MyChannel_AR"
    };
    static std::vector< std::string > const test_filename_channel_strings = {
        "H2:channel1!4=H2:channel1_AR", //
        "H2:channel2=H2:channel2_AR", //
        "H2:channel3" //
    };
    static std::vector< testing::channel_name > const expected_channels = {
        { "H1:MyChannel", 1, "H1:MyChannel_AR" },
        { "H2:channel1", 4, "H2:channel1_AR" },
        { "H2:channel2", 1, "H2:channel2_AR" },
        { "H2:channel3", 1, "H2:channel3" }
    };

    boost::filesystem::path channel_text_file(
        boost::filesystem::current_path( ) );
    channel_text_file /= "channel_from_filename_and_inline_channels.txt";

    testing::construct_channel_file( channel_text_file,
                                     test_filename_channel_strings );

    FrameAPI::channel_container_type channels;
    channels.ParseChannelName( channel_descriptor );
    channels.ParseChannelName( channel_text_file.string( ) );

    validate( channels, expected_channels );
}
