//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "../src/config.h"

#include <unistd.h>

#include <list>
#include <string>

#include "ldastoolsal/ldasexception.hh"
#include "ldastoolsal/unittest.h"

#include "ilwd/ldascontainer.hh"

#include "CreateFrameGroup.hh"
#include "createFrameGroupCommon.hh"

int main( int ArgC, char** ArgV );

using namespace FrameAPI;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

LDASTools::AL::UnitTest Test;

static INT_4U NumberOfCycles( 1 );

int
main( int ArgC, char** ArgV ) try
{
    Test.Init( ArgC, ArgV );

    AT( );
    init_tests( );
    for ( INT_4U k = 0; k < NumberOfCycles; k++ )
    {
        int test_number( 0 );
        AT( );
        for ( std::list< test_data_type >::const_iterator t(
                  test_data.begin( ) );
              t != test_data.end( );
              t++, test_number++ )
        {
            bool bad_file = false;
            AT( );
            Test.Message( )
                << "Doing test number: " << test_number << std::endl;
            for ( ConditionData::frame_files_type::const_iterator f(
                      ( *t ).s_files.begin( ) );
                  ( bad_file == false ) && ( f != ( *t ).s_files.end( ) );
                  f++ )
            {
                if ( access( ( *f ).c_str( ), R_OK ) != 0 )
                {
                    Test.Message( ) << "Warning: Could not access file: " << *f
                                    << ". Test skipped" << std::endl;
                    bad_file = true;
                    break;
                }
            }
            if ( bad_file == true )
            {
                continue;
            }

            CreateFrameGroup fg( ( *t ).s_files, ( *t ).s_channels );

            AT( );
            ILwd::LdasContainer* c(
                dynamic_cast< ILwd::LdasContainer* >( fg.Eval( ) ) );
            AT( );
            Test.Check( c != (ILwd::LdasContainer*)NULL )
                << "Generated ILwd" << std::endl;
            if ( c && Test.IsVerbose( 20 ) )
            {
                AT( );
                Test.Message( ) << "Generated: " << std::endl;
                c->write( 2, 2, Test.Message( false ), ILwd::ASCII );
                Test.Message( false ) << std::endl;
            }
            delete c;
            AT( );
        }
    }
    AT( );
    Test.Exit( );
}
catch ( const std::exception& e )
{
    Test.Check( false ) << "Caught exception: " << e.what( ) << std::endl;
    Test.Exit( );
}
catch ( ... )
{
    Test.Check( false ) << "Caught unknown exception" << std::endl;
    Test.Exit( );
}
