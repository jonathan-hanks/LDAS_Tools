//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

//-----------------------------------------------------------------------
// Test the tcl interface to createFrameGroup command
//-----------------------------------------------------------------------
#ifdef HAVE_CONFIG_H
#include "../src/config.h"
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#include <sstream>

#include <string>

#include <tcl.h>

#include "ldastoolsal/unittest.h"

#include "createFrameGroupCommon.hh"

LDASTools::AL::UnitTest Test;

int
main( int ArgC, char** ArgV )
{
    //---------------------------------------------------------------------
    // Make sure that some environment variables exist.
    //---------------------------------------------------------------------

    assert( getenv( "LDAS_TOP_BUILD_DIR" ) );

    std::string ldas_top_build_dir( getenv( "LDAS_TOP_BUILD_DIR" ) );

    std::string generic_api( ldas_top_build_dir +
                             "/api/genericAPI/so/src/.libs/libgenericAPI.so" );
    std::string frame_api( ldas_top_build_dir +
                           "/api/frameAPI/so/src/.libs/libframeAPI.so" );

    //---------------------------------------------------------------------
    // Allocate the interpreter
    //---------------------------------------------------------------------

    Tcl_Interp* interp( Tcl_CreateInterp( ) );

    std::string tcl_cmd;

    //---------------------------------------------------------------------
    // Load some basic modules
    //---------------------------------------------------------------------

    tcl_cmd = "load ";
    tcl_cmd += generic_api;
    tcl_cmd += "\n";
    if ( Tcl_GlobalEval( interp, const_cast< char* >( tcl_cmd.c_str( ) ) ) ==
         TCL_ERROR )
    {
        std::cerr << "Unable to load " << generic_api << " into TCL interpreter"
                  << std::endl;
        exit( 1 );
    }
    tcl_cmd = "load ";
    tcl_cmd += frame_api;
    tcl_cmd += "\n";
    if ( Tcl_GlobalEval( interp, const_cast< char* >( tcl_cmd.c_str( ) ) ) ==
         TCL_ERROR )
    {
        std::cerr << "Unable to load " << frame_api << " into TCL interpreter"
                  << std::endl;
        exit( 1 );
    }

    //---------------------------------------------------------------------
    // evaluate some commands
    //---------------------------------------------------------------------

    bool threaded( false );

    for ( int x = 1; x < ArgC; x++ )
    {
        if ( *ArgV[ x ] == '-' )
        {
            if ( strcmp( "-t", ArgV[ x ] ) == 0 )
            {
                threaded = true;
                continue;
            }
        }

        std::ostringstream cmd;

        if ( threaded )
        {
            cmd << "set tid [ createFrameGroup_t " << ArgV[ x ] << " ]"
                << std::endl
                << "set ilwd [ createFrameGroup_r $tid ]" << std::endl
                << "puts \"Created(In Thread): $ilwd\"" << std::endl;
        }
        else
        {
            cmd << "set ilwd [ createFrameGroup " << ArgV[ x ] << " ]"
                << std::endl
                << "puts \"Created (In main process): $ilwd\"" << std::endl;
        }
        cmd << "puts [ getElement $ilwd ]" << std::endl;

        if ( Tcl_GlobalEval( interp,
                             const_cast< char* >( cmd.str( ).c_str( ) ) ) ==
             TCL_ERROR )
        {
            std::cerr << "Unable to evaluate: " << cmd.str( ) << std::endl
                      << "\t" << Tcl_GetStringResult( interp ) << std::endl;
        }
    }
    if ( ArgC == 1 )
    {
        init_tests( );

        std::ostringstream cmd;

        cmd << "set tids [ list ]" << std::endl;

        for ( std::list< test_data_type >::const_iterator t(
                  test_data.begin( ) );
              t != test_data.end( );
              t++ )
        {
            bool               bad_file = false;
            std::ostringstream file_list;

            for ( ConditionData::frame_files_type::const_iterator f(
                      ( *t ).s_files.begin( ) );
                  ( bad_file == false ) && ( f != ( *t ).s_files.end( ) );
                  f++ )
            {
                if ( access( ( *f ).c_str( ), R_OK ) != 0 )
                {
                    Test.Message( ) << "Warning: Could not access file: " << *f
                                    << ". Test skipped" << std::endl;
                    bad_file = true;
                    break;
                }
                file_list << " " << *f;
            }
            if ( bad_file )
            {
                continue;
            }
#if MULTI_THREAD_DEFAULT
            cmd << "lappend tids [ createFrameGroup_t {" << file_list.str( )
                << " }";
#else /* MULTI_THREAD_DEFAULT */
            cmd << "createFrameGroup {" << file_list.str( ) << " }";
#endif /* MULTI_THREAD_DEFAULT */

            cmd << " {";
            for ( std::list< ConditionData::channel_input_type >::const_iterator
                      c( ( *t ).s_channels.begin( ) );
                  c != ( *t ).s_channels.end( );
                  c++ )
            {
                cmd << " {"
                    << " " << ( *c ).s_name << " " << ( *c ).s_q << " "
                    << ( *c ).s_data_type << " " << ( *c ).m_offset << " "
                    << ( *c ).m_delta << " " << ( *c ).m_slice_specified
                    << " }";
            }
            cmd << " }";

            cmd
#if MULTI_THREAD_DEFAULT
                << " ]"
#endif /* MULTI_THREAD_DEFAULT */
                << std::endl;
        }

#if MULTI_THREAD_DEFAULT
        cmd << "foreach tid $tids {" << std::endl
            << "  puts [ createFrameGroup_r $tid ]" << std::endl
            << "}" << std::endl;
#endif /* MULTI_THREAD_DEFAULT */

        std::cerr << "DEBUG: cmd: " << cmd.str( ) << std::endl;
        if ( Tcl_GlobalEval( interp,
                             const_cast< char* >( cmd.str( ).c_str( ) ) ) ==
             TCL_ERROR )
        {
            std::cerr << "Unable to evaluate: " << cmd.str( ) << std::endl
                      << "\t" << Tcl_GetStringResult( interp ) << std::endl;
        }
    }

    //---------------------------------------------------------------------
    // Delete the interpreter
    //---------------------------------------------------------------------

    Tcl_DeleteInterp( interp );
}
