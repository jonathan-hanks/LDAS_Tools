//
// LDASTools Generic API - A library of general use algorithms for LDASTools
// Suite
//
// Copyright (C) 2023 California Institute of Technology
//
// LDASTools Generic API is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Generic API is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <algorithm>
#include <set>
#include <sstream>
#include <vector>

#include "genericAPI/Logging.hh"
#include "genericAPI/LogText.hh"

#include "frameAPI/Intervals.hh"

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/filesystem.hpp>

#define SKIP_WORKING 1
#define SKIP_BROKEN 0

#if !SKIP_WORKING
#endif /* SKIP_WORKING */

#if !SKIP_BROKEN
#define TEST_INTERVAL_INITIALIZERS 1
#endif /* !SKIP_BROKEN */

struct rds_global_fixture
{
    rds_global_fixture( )
    {
        //-------------------------------------------------------------------
        // Setup logging as is needed by query class
        //-------------------------------------------------------------------
        GenericAPI::SetLogFormatter( new GenericAPI::Log::Text( "" ) );
        auto cwd = boost::filesystem::current_path( ).string( );
        GenericAPI::Log::stream_file_type fs( new GenericAPI::Log::StreamFile );
        LDASTools::AL::Log::stream_type   s;

        s = fs;
        fs->FilenameExtension( GenericAPI::LogFormatter( )->FileExtension( ) );
        GenericAPI::LogFormatter( )->Stream( s );
        GenericAPI::setLogTag( "test_query" );
        GenericAPI::LoggingInfo::LogDirectory( cwd );
        GenericAPI::setLogDebugLevel( 0 );
    }

    ~rds_global_fixture( )
    {
    }
};

BOOST_GLOBAL_FIXTURE( rds_global_fixture );

namespace testing
{
    void
    initialize( FrameAPI::interval_type& interval, int start, int stop )
    {
        interval.start = start;
        interval.stop = stop;
    }

    void
    verify( FrameAPI::interval_type const& Interval, int Start, int Stop )
    {
        BOOST_CHECK_MESSAGE(
            ( ( Interval.start == Start ) && ( Interval.stop == Stop ) ),
            "Expected: [ " << Start << ", " << Stop << " )" //
                           << " Received: [ ]" << Interval.start << ", "
                           << Interval.stop << " )" );
    }

    void
    initialize( FrameAPI::interval_container_type& IntervalContainer,
                FrameAPI::interval_type&           Interval )
    {
        std::cerr << "initialize: IntervalContainer: before: "
                  << IntervalContainer;
        IntervalContainer += Interval;
        std::cerr << "initialize: IntervalContainer: after: "
                  << IntervalContainer;
    }

    void
    verify( FrameAPI::interval_container_type const& IntervalContainer,
            FrameAPI::interval_container_type const& IntervalContainerExpected )
    {
        std::cerr << "Expected: " << IntervalContainerExpected << " =?="
                  << " Received: " << IntervalContainer << std::endl;
        BOOST_CHECK_MESSAGE( IntervalContainer == IntervalContainerExpected,
                             "Expected: " );
    }

    void
    initializer_test_interval_container_concat_2( )
    {
        static FrameAPI::interval_container_type const
                                          interval_container_expected = { { 10, 30 } };
        FrameAPI::interval_type           rhs_interval = { 10, 30 };
        FrameAPI::interval_container_type           rhs_interval_container;
        FrameAPI::interval_container_type interval_container;

        rhs_interval_container += rhs_interval;
        interval_container += rhs_interval_container;

        verify( interval_container, interval_container_expected );
    }

  void
  initializer_test_interval_container_1( )
  {
    static FrameAPI::interval_container_type const
      interval_container_expected = { { 10, 30 } };
    FrameAPI::interval_type           interval = { 10, 30 };
    FrameAPI::interval_container_type interval_container;

    initialize( interval_container, interval );
    verify( interval_container, interval_container_expected );
  }

} // namespace testing

#if TEST_INTERVAL_INITIALIZERS
BOOST_AUTO_TEST_CASE( interval_initializers )
{
    FrameAPI::interval_type simple_interval = { 5, 6 };

    BOOST_CHECK_MESSAGE( simple_interval.start == 5,
                         "simple_interval.start:  " << simple_interval.start
                                                    << " =?= 5" );
    BOOST_CHECK_MESSAGE( simple_interval.stop == 6,
                         "simple_interval.stop:  " << simple_interval.start
                                                   << " =?= 6" );

    FrameAPI::interval_type interval_7_10 = { 0, 0 };

    testing::initialize( interval_7_10, 7, 10 );
    testing::verify( interval_7_10, 7, 10 );

    testing::initializer_test_interval_container_1( );
}
#endif
