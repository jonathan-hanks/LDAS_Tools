//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

// System Header Files
#include <frameutils_config.h>

#include <cassert>
#include <cstring>
#include <unistd.h>
#include <cerrno>

#include <algorithm>
#include <cstdlib>
#include <sstream>
#include <cmath>
#include <memory>

#include <boost/shared_ptr.hpp>

// General Header Files
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/types.hh"

#include "framecpp/Common/CheckSum.hh"
#include "framecpp/Common/MD5Sum.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrHistory.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrVect.hh"
#include "framecpp/STRING.hh"

// Filters Header Files
#include "filters/Resample.hh"

#include "genericAPI/Logging.hh"

#include "filereader.hh"
#include "ResampleCmd.hh"
#include "rdsresample.hh"
#include "RDSStream.hh"
#include "rdsutil.hh"
#include "util.hh"

using namespace std;
using namespace FrameCPP;

using FrameAPI::LogMD5Sum;
using FrameCPP::Common::CheckSum;
using FrameCPP::Common::MD5Sum;
using LDASTools::AL::GPSTime;

namespace
{
    typedef ::ResampleRawFrame::resample_container_type resample_container_type;

    static void adjustData( FrameCPP::FrVect* data,
                            FrameCPP::FrVect* next_data,
                            const INT_4U      delay );

    template < class T >
    static void adjustVector( FrameCPP::FrVect* vec,
                              FrameCPP::FrVect* next_vec,
                              const INT_4U      delay );

    inline void
    validate_resample_factor( INT_4U Value )
    {
        switch ( Value )
        {
        case 1:
        case 2:
        case 4:
        case 8:
        case 16:
            //--------------------------------------------------------------
            // Take no action for all valid values
            //--------------------------------------------------------------
            break;
        default:
            //--------------------------------------------------------------
            // Throw exception for non-valid values
            //--------------------------------------------------------------
            std::ostringstream err;
            err << "Invalid resample factor: " << Value
                << ". Allowed resample factor: 1, 2, 4, 8, 16.";
            throw SWIGEXCEPTION( err.str( ) );
            break;
        } // switch
    } // validate_resample_factor

    class seed_resample_factors
    {
    public:
        seed_resample_factors( const std::string&       ResamplingStr,
                               resample_container_type& Resampling )
            : parser( ResamplingStr.c_str( ) ), resampling( Resampling )
        {
            resample_vec = parser.getTokenList( );
            rv_cur = resample_vec.begin( );
        }

        void
        operator( )( const std::string& ChannelName )
        {
            typedef resample_container_type::mapped_type m_type;

            if ( rv_cur != resample_vec.end( ) )
            {
                INT_4U tmp;

                // Convert to int:
                char* endptr( 0 );

                tmp = strtoul( ( *rv_cur ).c_str( ), &endptr, 0 );
                if ( *endptr != '\0' )
                {
                    std::string msg( "Resample factor must be numeric: " );
                    msg += *rv_cur;
                    throw SWIGEXCEPTION( msg );
                }
                validate_resample_factor( tmp );
                resampling[ ChannelName ] =
                    m_type( tmp, m_type::second_type( ) );
                ++rv_cur;
            }
        }

    private:
        ListParser                                 parser;
        std::vector< std::string >                 resample_vec;
        std::vector< std::string >::const_iterator rv_cur;
        resample_container_type&                   resampling;
    };

    class resample_history
    {
    public:
        resample_history( const resample_container_type& Resamples,
                          ostringstream&                 Record,
                          const std::string&             HistoryName,
                          const LDASTools::AL::GPSTime&  HistoryTime )
            : resamples( Resamples ), record( Record ),
              history_name( HistoryName ), gps_time( HistoryTime )
        {
            len = record.str( ).length( );
        }

        void
        operator( )( const std::string& ChannelName )
        {
            resample_container_type::const_iterator res_cur =
                resamples.find( ChannelName );
            if ( res_cur == resamples.end( ) )
            {
                throw FrameAPI::RDS::MissingChannel( ChannelName );
            }

            ostringstream r;

            r << res_cur->second.first;
            if ( ( r.str( ).length( ) + len + 2 ) >=
                 FrameCPP::Version::STRING::MAX_STRING_LENGTH )
            {
                //-----------------------------------------------------------
                // Need to output the record since it is full
                //-----------------------------------------------------------
                record << ")";

                FrameCPP::FrameH::history_type::value_type h( new FrHistory(
                    history_name, gps_time.GetSeconds( ), record.str( ) ) );
                history.append( h );
                record.str( "frameAPI: RDS (resample factors (cont):" );
                len = record.str( ).length( );
            }
            record << " " << r.str( );
            len += r.str( ).length( ) + 1;
        }

    private:
        const resample_container_type& resamples;
        ostringstream&                 record;
        size_t                         len;
        FrameCPP::FrameH::history_type history;
        const std::string              history_name;
        LDASTools::AL::GPSTime         gps_time;
    };

} // namespace

ResampleRawFrame::ResampleRawFrame( const char*              frame_files,
                                    const char*              channels,
                                    const char*              Resampling,
                                    const RDSFrame::Options& UserOptions )
    : RDSFrame( frame_files, channels, UserOptions ), mCurrentDt( 0.0f ),
      m_should_write( false )
{
    static const char* caller = "ResampleRawFrame::ResampleRawFrame";

    m_remaining_to_write_to_file = m_options.FramesPerFile( );
    if ( Resampling == 0 || strlen( Resampling ) == 0 )
    {
        throw SWIGEXCEPTION( "Resample factor must be specified." );
    }

    // At least three files must be specified
    if ( ( GetNumberOfFrameFiles( ) < 3 ) && ( UserOptions.Padding( ) == 0 ) )
    {
        throw SWIGEXCEPTION( "At least three frame files must be specified." );
    }

    mResampleRecord = Resampling;
    initResampleFactor( );

    // Make sure there're as many resample factors as channels.
    if ( GetNumberOfChannels( ) != resampling.size( ) )
    {
        throw SWIGEXCEPTION(
            "Inconsistent number of channels and resample factors." );
    }
    QUEUE_LOG_MESSAGE( "ResampleRawFrame:"
                           << " frame_files: " << frame_files << " channels: "
                           << channels << " Resampling: " << Resampling,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );
}

ResampleRawFrame::ResampleRawFrame(
    const frame_file_container_type& frame_files,
    const channel_container_type&    channels,
    const resample_container_type&   Resampling,
    const RDSFrame::Options&         UserOptions )
    : RDSFrame( frame_files, channels, UserOptions ), mCurrentDt( 0.0f ),
      m_should_write( false )
{
    static const char* caller = "ResampleRawFrame::ResampleRawFrame";

    m_remaining_to_write_to_file = m_options.FramesPerFile( );
    //--------------------------------------------------------------------
    // At least three files must be specified
    //--------------------------------------------------------------------
    if ( ( GetNumberOfFrameFiles( ) < 3 ) && ( UserOptions.Padding( ) == 0 ) )
    {
        throw SWIGEXCEPTION( "At least three frame files must be specified." );
    }

    {
        bool                              first = true;
        std::ostringstream                msg;
        resample_container_type::iterator cur_rf;

        resampling = Resampling;
        msg << "{ ";
        for ( channel_container_type::const_iterator cur = channels.begin( ),
                                                     last = channels.end( );
              cur != last;
              ++cur )
        {
            // Locate the resampling information using the source channel name
            resample_container_type::const_iterator r_cur =
                resampling.find( cur->old_channel_name );
            if ( r_cur == resampling.end( ) )
            {
                std::ostringstream msg;

                msg << "No resampling information supplied for channel: "
                    << cur->old_channel_name;
                throw std::runtime_error( msg.str( ) );
            }
            validate_resample_factor( r_cur->second.first );
            if ( first )
            {
                first = false;
            }
            else
            {
                msg << ",";
            }
            msg << r_cur->second.first;
        }
        msg << " }";
        mResampleRecord = msg.str( );
    }

    // Make sure there're as many resample factors as channels.
    if ( GetNumberOfChannels( ) != resampling.size( ) )
    {
        throw SWIGEXCEPTION(
            "Inconsistent number of channels and resample factors." );
    }
#if WIP
    QUEUE_LOG_MESSAGE( "ResampleRawFrame:"
                           << " frame_files: " << frame_files << " channels: "
                           << channels << " Resampling: " << Resampling,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );
#else /* WIP */
    QUEUE_LOG_MESSAGE( "ResampleRawFrame:"
                           << "...",
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );
#endif /* WIP */
}

//-----------------------------------------------------------------------------
//
//: Destructor.
//
ResampleRawFrame::~ResampleRawFrame( )
{
}

ResampleRawFrame::resample_container_type
ResampleRawFrame::Resample( const channel_container_type& Channels,
                            const std::vector< INT_2U >&  Resampling )
{
    typedef resample_container_type::mapped_type m_type;

    ResampleRawFrame::resample_container_type retval;
    std::vector< INT_2U >::const_iterator     res_cur = Resampling.begin( );
    std::vector< INT_2U >::const_iterator     res_last = Resampling.end( );
    INT_2U                                    val;

    for ( channel_container_type::const_iterator cur = Channels.begin( ),
                                                 last = Channels.end( );
          cur != last;
          ++cur )
    {
        if ( res_cur != res_last )
        {
            val = *res_cur;
            ++res_cur;
        }
        else
        {
            val = 1;
        }
        retval[ cur->old_channel_name ] = m_type( val, m_type::second_type( ) );
    }

    return retval;
}

void
ResampleRawFrame::processChannel( fr_adc_data_type Adc )
{
    static const char* caller = "ResampleRawFrame::processChannel";

    //--------------------------------------------------------------------
    // initialize frequently accessed quantities
    //--------------------------------------------------------------------
    resample_container_type::iterator r( resampling.find( Adc->GetName( ) ) );

    if ( r == resampling.end( ) )
    {
        throw FrameAPI::RDS::MissingChannel( Adc->GetName( ) );
    }
    INT_4U resample_factor( r->second.first );
    //--------------------------------------------------------------------
    // Determine if things need to be initialized or not
    //--------------------------------------------------------------------
    if ( ( r->second.first > 1 ) && ( !r->second.second ) )
    {
        //-----------------------------------------------------------------
        // Seed the state
        //-----------------------------------------------------------------
        r->second.second.reset(
            createResampleState( resample_factor, Adc.get( ) ) );
    }
    //--------------------------------------------------------------------
    // Resample the channel
    //--------------------------------------------------------------------
    Filters::ResampleBase* state( r->second.second.get( ) );

    QUEUE_LOG_MESSAGE( "ADC: "
                           << " state: " << (void*)state,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );

    if ( state )
    {
        boost::shared_ptr< FrProcData > oproc;

        //-----------------------------------------------------------------
        // Perform resampling on the data
        //-----------------------------------------------------------------
        oproc.reset( resampleAdcData( Adc.get( ), state ) );
        //-----------------------------------------------------------------
        // Unregister result and put into result frame
        //-----------------------------------------------------------------
#if OLD
        unregisterProcData( oproc.get( ) );
#endif /* 0 */
        if ( mResultFrame )
        {
            getResultProcData( )->append( oproc );
        }
    }
    else
    {
        if ( mResultFrame.get( ) )
        {
            //--------------------------------------------------------------
            // Append origional channel: no resampling required
            //--------------------------------------------------------------
            mResultFrame->GetRawData( )->RefFirstAdc( ).append( Adc );
        }
    }
}

void
ResampleRawFrame::processChannel( fr_proc_data_type Proc )
{
    static const char* caller = "ResampleRawFrame::processChannel";

    //--------------------------------------------------------------------
    // initialize frequently accessed quantities
    //--------------------------------------------------------------------
    resample_container_type::iterator r( resampling.find( Proc->GetName( ) ) );

    if ( r == resampling.end( ) )
    {
        throw FrameAPI::RDS::MissingChannel( Proc->GetName( ) );
    }
    INT_4U resample_factor( r->second.first );
    //--------------------------------------------------------------------
    // Determine if things need to be initialized or not
    //--------------------------------------------------------------------
    if ( ( r->second.first > 1 ) && ( !r->second.second ) )
    {
        //-----------------------------------------------------------------
        // Seed the state
        //-----------------------------------------------------------------
        r->second.second.reset(
            createResampleState( resample_factor, Proc.get( ) ) );
    }
    Filters::ResampleBase* state( r->second.second.get( ) );

    QUEUE_LOG_MESSAGE( "PROC: "
                           << " state: " << (void*)state,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );

    if ( mResultFrame.get( ) )
    {
        if ( state )
        {
            boost::shared_ptr< FrProcData > oproc;

            //--------------------------------------------------------------
            // Perform resampling on the data
            //--------------------------------------------------------------
            oproc.reset( resampleProcData( Proc.get( ), state ) );
            //--------------------------------------------------------------
            // Put into result frame. This object was not registered
            //--------------------------------------------------------------
            getResultProcData( )->append( oproc );
        }
        else
        {
            //--------------------------------------------------------------
            // Append origional channel: no resampling required
            //--------------------------------------------------------------
            getResultProcData( )->append( Proc );
        }
    }

    //--------------------------------------------------------------------
    // Resample the channel
    //--------------------------------------------------------------------
    if ( state )
    {
    }
    else
    {
    }
}

ResampleRawFrame::stop_request_type
ResampleRawFrame::stopRequest( ) const
{
    return STOP_DATA;
}

//-----------------------------------------------------------------------------
//
//: Create history record.
//
void
ResampleRawFrame::createHistory( )
{
    if ( ( mResultFrame.get( ) == (FrameH*)NULL ) ||
         ( m_options.HistoryRecord( ) == false ) )
    {
        return;
    }

    RDSFrame::createHistory( );

    GPSTime gps_time;
    gps_time.Now( );

    ostringstream record;

    record << "frameAPI: resampled RDS (resample factors: ";

    resample_history rh( resampling, record, getHistoryName( ), gps_time );

    foreachChannel( rh );

    //--------------------------------------------------------------------
    // Write the final record of resample factors information
    //--------------------------------------------------------------------
    record << ")";
    FrameCPP::FrameH::history_type::value_type h( new FrHistory(
        getHistoryName( ), gps_time.GetSeconds( ), record.str( ) ) );
    mResultFrame->RefHistory( ).append( h );

    return;
}

//-----------------------------------------------------------------------------
//
//: Parse out numeric values of resample factors.
//
//! return: const char* resample - A list of resample factors.
//
void
ResampleRawFrame::initResampleFactor( )
{
    seed_resample_factors factors( mResampleRecord, resampling );

    foreachChannel( factors );

    return;
}

//-----------------------------------------------------------------------------
//
//: Adjust data for the current result frame.
//
// Current frame object is represented by mCurrentFrame, frame object
// used for data source is represented by mResultFrame.
//
// In a case if the channel was resampled, this method will do the
// following data adjustment:
// 1. Trim first data points at the beginning of the channel
// 2. Append missing data points at the end of the channel using data
//    from the next frame (mResultFrame)
//
void
ResampleRawFrame::adjustChannelData( )
{
    // Rehash container to create channel map
    mCurrentFrame->RefProcData( ).rehash( );

    // Rehash container to create channel map
    getResultProcData( )->rehash( );

    // Step through all channels in result frame:
#if 0
   INT_4U	offset( 0 );

   for ( state_const_iterator
	    state_iter( mResampleBase.begin( ) ),
	    end_state_iter( mResampleBase.end( ) );
	 state_iter != end_state_iter;
	 state_iter++, offset++ )
   {
      adjustResultChannel( getChannelName( offset ), *state_iter );
   }
#else /* 0 */
    for ( resample_container_type::const_iterator cur = resampling.begin( ),
                                                  last = resampling.end( );
          cur != last;
          ++cur )
    {
        adjustResultChannel( cur->first, cur->second.second.get( ) );
    }
#endif /* 0 */

    return;
}

//-----------------------------------------------------------------------------
//
//: Adjust data of resampled channel.
//
// Caller must call 'rehash' on all passed FrAdcData containers to
// guarantee existence of channel map for the frame.
//
void
ResampleRawFrame::adjustResultChannel( const std::string&           name,
                                       const Filters::ResampleBase* state )
{
    static const char* caller = "ResampleRawFrame::adjustResutChannel";
    if ( state == 0 )
    {
        // There's no resampling for the channel ===> nothing needs to be done
        return;
    }

    // Get state delay
    REAL_8 state_delay( state->getDelay( ) );
    if ( ceil( state_delay ) != state_delay )
    {
        ostringstream msg;
        msg << "Resample delay must be an integer: ";
        msg << state_delay;

        throw SWIGEXCEPTION( msg.str( ) );
    }

    const INT_4U delay( static_cast< INT_4U >( state_delay ) );
    if ( delay == 0 )
    {
        // There're no samples to adjust for the channel
        return;
    }

    QUEUE_LOG_MESSAGE( "Adjusting data for channel: " << name,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );
    // If channel was resampled, it's FrProc
    FrProcData* proc(
        getProcChannel( name, &( mCurrentFrame->RefProcData( ) ) ) );

    QUEUE_LOG_MESSAGE( "Got current proc channel: " << name,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );

    FrProcData* next_proc( getProcChannel( name, getResultProcData( ) ) );

    QUEUE_LOG_MESSAGE(
        "Got next channel: " << name, MT_DEBUG, 30, caller, "RDSResample" );

    // Channel data
    boost::shared_ptr< FrameCPP::FrVect > vect( proc->RefData( )[ 0 ] );
    if ( !vect )
    {
        string msg( "FrProcData of resampled frame is missing data: " );
        msg += name;
        throw SWIGEXCEPTION( msg );
    }

    boost::shared_ptr< FrameCPP::FrVect > next_vect(
        next_proc->RefData( )[ 0 ] );
    if ( !next_vect )
    {
        string msg( "FrProcData of next resampled frame is missing data: " );
        msg += name;
        throw SWIGEXCEPTION( msg );
    }

    if ( vect->GetNData( ) < delay || next_vect->GetNData( ) < delay )
    {
        ostringstream msg;
        msg << "Number of points for shift (currentVector=" << vect->GetNData( )
            << ", nextVector=" << next_vect->GetNData( )
            << ") is less than ResampleBase::delay (" << delay << ")";
        throw SWIGEXCEPTION( msg.str( ) );
    }

    // Get rid of 'delay' samples at the beginning of the data,
    // and append 'delay' samples from next frame channel at the end
    adjustData( vect.get( ), next_vect.get( ), delay );

    return;
}

#if 0
void ResampleRawFrame::
openFrameFile( const std::string& Filename )
{
#if 1
   assert( 0 );
#else
   RDSFrame::openFrameFile( Filename );
#endif /* 0 */
}
#endif /* 0 */

void
ResampleRawFrame::rangeOptimizer( const time_type& UserStart,
                                  const time_type& UserStop,
                                  time_type&       DataStart,
                                  time_type&       DataStop ) const
{
    return;
}

void
ResampleRawFrame::writeFrameToStream( )
{
    static const char* caller = "ResampleRawFrame::writeFrameToStream";

    QUEUE_LOG_MESSAGE( "ENTRY", MT_DEBUG, 30, caller, "RDSResample" );
    QUEUE_LOG_MESSAGE(
        "m_should_write: "
            << m_should_write << " mCurrentFrame: "
            << (void*)( mCurrentFrame.get( ) ) << " mCurrentFrame->GetGTime(): "
            << ( ( mCurrentFrame.get( ) ) ? mCurrentFrame->GetGTime( )
                                          : GPSTime( 0, 0 ) )
            << " mCurrentFrame->GetDt(): "
            << ( ( mCurrentFrame.get( ) ) ? mCurrentFrame->GetDt( ) : 0.0 )
            << " Should Start: " << GPSTime( m_options.OutputTimeStart( ), 0 ),
        MT_DEBUG,
        30,
        caller,
        "RDSResample" );
    if ( ( m_should_write == false ) && ( mCurrentFrame.get( ) ) &&
         ( mCurrentFrame->GetGTime( ) >=
           GPSTime( m_options.OutputTimeStart( ), 0 ) ) )
    {
        QUEUE_LOG_MESSAGE( "Flagging as should write" << __LINE__,
                           MT_DEBUG,
                           40,
                           caller,
                           "RDSResample" );

        m_should_write = true;
        m_remaining_to_write_to_file = m_options.FramesPerFile( );
    }
    QUEUE_LOG_MESSAGE( "mCurrentFrame: " << (void*)( mCurrentFrame.get( ) )
                                         << " m_should_write: "
                                         << m_should_write
                                         << " m_remaining_to_write_to_file: "
                                         << m_remaining_to_write_to_file,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );

    if ( m_should_write && mCurrentFrame.get( ) )
    {
        //-----------------------------------------------------------------
        // Adjust the current frame data
        //-----------------------------------------------------------------

        QUEUE_LOG_MESSAGE(
            "__LINE__: " << __LINE__, MT_DEBUG, 40, caller, "RDSResample" );

        adjustChannelData( );

        QUEUE_LOG_MESSAGE(
            "__LINE__: " << __LINE__, MT_DEBUG, 40, caller, "RDSResample" );

        mCurrentFrame->SetDt( mCurrentDt );

        //-----------------------------------------------------------------
        // Write Frame to the stream
        //-----------------------------------------------------------------
        QUEUE_LOG_MESSAGE(
            "__LINE__: " << __LINE__, MT_DEBUG, 40, caller, "RDSResample" );

        m_stream->Write( mCurrentFrame,
                         m_options.CompressionMethod( ),
                         m_options.CompressionLevel( ),
                         ( ( m_options.CreateChecksumPerFrame( ) )
                               ? CheckSum::CRC
                               : CheckSum::NONE ) );
    }
    //--------------------------------------------------------------------
    // Setup for the next frame
    //--------------------------------------------------------------------
    QUEUE_LOG_MESSAGE(
        "__LINE__: " << __LINE__, MT_DEBUG, 40, caller, "RDSResample" );

    mCurrentFrame = mResultFrame;
    mCurrentDt = mCurrentFrame->GetDt( );

    QUEUE_LOG_MESSAGE( "__LINE__: " << __LINE__ << " ResultFrame: "
                                    << mResultFrame->GetGTime( ) << ":"
                                    << mResultFrame->GetDt( )
                                    << " mCurrentDt: " << mCurrentDt,
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );

    QUEUE_LOG_MESSAGE( "EXIT", MT_DEBUG, 30, caller, "RDSResample" );
}

namespace FrameAPI
{
    namespace RDS
    {
        class CollectNames : public FrameCPP::Common::FrTOC::FunctionString,
                             public std::vector< std::string >
        {
        public:
            virtual ~CollectNames( )
            {
            }

            virtual void
            operator( )( const std::string& ChannelName )
            {
                push_back( ChannelName );
            }
        };

        void
        ExpandChannelList( channel_container_type&      Channels,
                           resample_arg_container_type& ResampleRates,
                           const std::string&           SampleFrame )
        {
            static const char* caller = "FrameAPI::RDS::ExpandChannelList";

#if OLD
            typedef resample_container_type::mapped_type m_type;
#endif /* OLD */

            channel_container_type retval_chan;
            CollectNames           sample_frame_channel_names;
#if OLD
            resample_container_type                 retval_rates;
            resample_container_type::const_iterator cur_rate;
            resample_container_type::const_iterator last_rate;
#else
            resample_arg_container_type                 retval_rates;
            resample_arg_container_type::const_iterator cur_rate;
            resample_arg_container_type::const_iterator last_rate;
#endif

            cur_rate = ResampleRates.begin( );
            last_rate = ResampleRates.end( );
            for ( channel_container_type::const_iterator
                      cur_chan = Channels.begin( ),
                      last_chan = Channels.end( );
                  cur_chan != last_chan;
                  ++cur_chan )
            {
                size_t pos( cur_chan->old_channel_name.find( '/' ) );
                size_t posend( cur_chan->old_channel_name.rfind( '/' ) );

                if ( ( posend > pos ) && ( ( posend - pos ) > 1 ) )
                {
                    //--------------------------------------------------------
                    // This is the case were a regular expression has been
                    //  listed as a channel and it needs to be expanded
                    //--------------------------------------------------------
                    if ( sample_frame_channel_names.size( ) <= 0 )
                    {
                        //-----------------------------------------------------
                        // Load the list of channel names from the specified
                        //   sample file.
                        // This is done only once for efficency purposes.
                        //-----------------------------------------------------
                        using FrameCPP::Common::FrameBuffer;
                        using FrameCPP::Common::FrTOC;
                        using FrameCPP::Common::IFrameStream;

                        FrameBuffer< LDASTools::AL::filebuf > ifb(
                            std::ios::in );

                        ifb.open( SampleFrame.c_str( ), std::ios::in );

                        IFrameStream ifs( false, &ifb );
                        const FrTOC* toc( ifs.GetTOC( ) );
                        CollectNames n;

                        if ( toc == (FrTOC*)NULL )
                        {
                            /// \todo Throw exception for wild cards are only
                            /// supported for frame sets with table of contents
                        }
                        toc->ForEach(
                            FrameCPP::Common::FrTOC::TOC_CHANNEL_NAMES, n );
                        sample_frame_channel_names.swap( n );
                    }
                    //--------------------------------------------------------
                    // Need to handle matching of channels using regex.
                    //--------------------------------------------------------
                    size_t      l( ( posend > ( pos + 1 ) )
                                  ? ( ( posend - pos ) - 2 )
                                  : 0 );
                    std::string suffix;

                    Regex pattern(
                        cur_chan->old_channel_name.substr( pos + 1, l ) );
                    RegexMatch matches;

                    if ( posend < ( cur_chan->old_channel_name.length( ) - 1 ) )
                    {
                        suffix =
                            cur_chan->old_channel_name.substr( ( posend + 1 ) );
                        QUEUE_LOG_MESSAGE(
                            "Extra text beyond expression is being ignored: "
                                << suffix,
                            MT_DEBUG,
                            30,
                            caller,
                            "RDSResample" );
                    }

                    //--------------------------------------------------------
                    // loop over channels
                    //--------------------------------------------------------
                    for ( auto cur = sample_frame_channel_names.begin( ),
                               last = sample_frame_channel_names.end( );
                          cur != last;
                          ++cur )
                    {
                        if ( matches.match( pattern, cur->c_str( ) ) )
                        {
                            //--------------------------------------------------
                            // add to list of channels
                            //--------------------------------------------------
                            retval_chan.emplace_back( *cur, *cur );
#if 1
                            retval_rates.push_back(
                                ( cur_rate != last_rate ) ? *cur_rate : 1 );
#else
                            retval_rates[ *cur ] =
                                m_type( ( ( cur_rate != last_rate )
                                              ? cur_rate->second.first
                                              : 1 ),
                                        m_type::second_type( ) );

#endif
                        }
                    }
                }
                else if ( pos != std::string::npos )
                {
                    //--------------------------------------------------------
                    // Case where an expression has been specified, but is
                    // malformed
                    //--------------------------------------------------------
                }
                else
                {
                    //--------------------------------------------------------
                    // Case where the channel name is to be placed in the
                    // output
                    //--------------------------------------------------------
                    retval_chan.push_back( *cur_chan );
#if 1
                    retval_rates.push_back(
                        ( cur_rate != last_rate ) ? *cur_rate : 1 );
#else /* 0 */
                    retval_rates[ *cur_chan ] = m_type(
                        ( ( cur_rate != last_rate ) ? cur_rate->second.first
                                                    : 1 ),
                        m_type::second_type( ) );
#endif /* 0 */
                }
                if ( cur_rate != last_rate )
                {
                    ++cur_rate;
                }
            }
            Channels.swap( retval_chan );
            ResampleRates.swap( retval_rates );
        } // Function - ExpandChannelList
    } // namespace RDS
} // namespace FrameAPI

namespace
{
    //--------------------------------------------------------------------
    //
    //: Do actual data adjustment on resampled channel.
    //
    void
    adjustData( FrameCPP::FrVect* data,
                FrameCPP::FrVect* next_data,
                const INT_4U      delay )
    {
        FrVect::data_types_type id =
            FrVect::data_types_type( data->GetType( ) );

        switch ( id )
        {
        case ( FrVect::FR_VECT_2S ):
            return adjustVector< INT_2S >( data, next_data, delay );
        case ( FrVect::FR_VECT_2U ):
            return adjustVector< INT_2U >( data, next_data, delay );
        case ( FrVect::FR_VECT_4S ):
            return adjustVector< INT_4S >( data, next_data, delay );
        case ( FrVect::FR_VECT_4U ):
            return adjustVector< INT_4U >( data, next_data, delay );
        case ( FrVect::FR_VECT_8S ):
            return adjustVector< INT_8S >( data, next_data, delay );
        case ( FrVect::FR_VECT_8U ):
            return adjustVector< INT_8U >( data, next_data, delay );
        case ( FrVect::FR_VECT_8C ):
            return adjustVector< COMPLEX_8 >( data, next_data, delay );
        case ( FrVect::FR_VECT_16C ):
            return adjustVector< COMPLEX_16 >( data, next_data, delay );
        case ( FrVect::FR_VECT_4R ):
            return adjustVector< REAL_4 >( data, next_data, delay );
        case ( FrVect::FR_VECT_8R ):
            return adjustVector< REAL_8 >( data, next_data, delay );
        case ( FrVect::FR_VECT_C ):
            return adjustVector< CHAR >( data, next_data, delay );
        case ( FrVect::FR_VECT_1U ):
            return adjustVector< CHAR_U >( data, next_data, delay );
        default:
            throw SWIGEXCEPTION( "Unsupported vector type." );
        }

        return;
    }

    template < class T >
    void
    adjustVector( FrameCPP::FrVect* v,
                  FrameCPP::FrVect* next_v,
                  const INT_4U      delay )
    {
        FrameCPP::FrVect::data_type data_src( v->GetDataUncompressed( ) );
        FrameCPP::FrVect::data_type next_data_src(
            next_v->GetDataUncompressed( ) );

        T*       data( reinterpret_cast< T* >( data_src.get( ) ) );
        const T* next_data(
            reinterpret_cast< const T* >( next_data_src.get( ) ) );

        if ( data == 0 || next_data == 0 )
        {
            throw SWIGEXCEPTION( "Channel has no data." );
        }

        // Get rid of first 'delay' samples
        const INT_4U numData( v->GetNData( ) );
        copy( data + delay, data + numData, data );

        // Append 'delay' samples at the end
        copy( next_data, next_data + delay, data + numData - delay );

        return;
    }

    // Instantiate templates
    template void adjustVector< CHAR >( FrameCPP::FrVect* v,
                                        FrameCPP::FrVect* next_v,
                                        const INT_4U      delay );
    template void adjustVector< CHAR_U >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< INT_2S >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< INT_2U >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< INT_4S >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< INT_4U >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< INT_8S >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< INT_8U >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< REAL_4 >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< REAL_8 >( FrameCPP::FrVect* v,
                                          FrameCPP::FrVect* next_v,
                                          const INT_4U      delay );
    template void adjustVector< COMPLEX_8 >( FrameCPP::FrVect* v,
                                             FrameCPP::FrVect* next_v,
                                             const INT_4U      delay );
    template void adjustVector< COMPLEX_16 >( FrameCPP::FrVect* v,
                                              FrameCPP::FrVect* next_v,
                                              const INT_4U      delay );
} // namespace
