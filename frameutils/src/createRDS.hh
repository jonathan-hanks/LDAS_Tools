//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__CREATE_RDS_HH
#define FRAME_API__CREATE_RDS_HH

#include "ldastoolsal/types.hh"
#if !defined( SWIGIMPORTED )
#include "boost/filesystem.hpp"
#include "boost/filesystem/fstream.hpp"
#include "boost/filesystem/path.hpp"

#include "frameAPI/rdsframe.hh"
#include "frameAPI/rdsresample.hh"
#include "frameAPI/Frame.hh"
#endif /* !defined( SWIGIMPORTED ) */

#define CREATERDS_START_TYPE INT_4U
#define CREATERDS_END_TYPE INT_4U

#if !defined( SWIGIMPORTED )
namespace FrameAPI
{
    typedef ::RDSFrame::frame_file_container_type frame_file_container_type;
    struct channel_container_type
    {
        typedef ::RDSFrame::channel_container_type channel_type;
        typedef INT_2U                             resample_type;
        typedef std::vector< resample_type >       resampling_type;
        typedef std::list< std::string > unparsed_channel_container_type;

        channel_type                    names;
        resampling_type                 resampling;
        unparsed_channel_container_type input_channels;

        void ParseChannelName( const std::string& ChannelNamesString );

        void ParseChannelNames( const std::string& ChannelNamesString );

        void operator( )( std::string const& OldName,
                          std::string const& Resampling,
                          std::string const& NewName );
    };

    struct channel_naming_conventions_type
    {
        inline static std::string
        Canonical( std::string const& OldName,
                   std::string const& Resampling = "",
                   std::string const& NewName = "" )
        {
            std::string retval( OldName );

            if ( ( Resampling.size( ) > 0 ) && ( Resampling != "1" ) )
            {
                retval += "!" + Resampling;
            }
            if ( ( !NewName.empty( ) ) && ( NewName != OldName ) )
            {
                retval += "=" + NewName;
            }
            return ( retval );
        }

        inline static std::string
        Canonical( std::string const& OldName,
                   INT_2U             Resampling,
                   std::string const& NewName = "" )
        {
            std::ostringstream resampling_stream;

            resampling_stream << Resampling;
            return ( Canonical( OldName, resampling_stream.str( ), NewName ) );
        }

        //---------------------------------------------------------------
        /// @brief Split channel name into components
        ///
        /// @param[in] Source
        ///    String containing the channel information
        /// @param[out] OldName
        ///    Name of channel as it is in the source frame
        /// @param[out] Resampling
        ///    Any resampling that should happen to the channel
        /// @param[out] NewName
        ///    Name of channel as it should appear in the output frame
        ///
        //---------------------------------------------------------------
        inline static void
        Parse( std::string const& Source,
               std::string&       OldName,
               std::string&       Resampling,
               std::string&       NewName )
        {
        }
    };

    //-------------------------------------------------------------------
    /// @brief Read channels from an input source
    ///
    /// \code{.unparsed}
    ///   # Original syntax
    ///   # <channel_name>[ <resampling>[ <new_channel_name>]]
    ///   H1:CHANNEL_1
    ///   H1:CHANNEL_2 4
    ///   H1:CHANNEL_3 4 H1:CHANNEL_3_AR
    ///   # New syntax
    ///   # <channel_name>[!<resampling>[=<new_channel_name>]]
    ///   H1:CHANNEL_4!16
    ///   H1:CHANNEL_5!4=H1:CHANNEL_5_AR
    /// \endcode
    //-------------------------------------------------------------------
    template < typename ChannelFunctor_Type >
    struct channel_name_file_type
    {
        inline channel_name_file_type( ChannelFunctor_Type& ChannelProcessor )
            : processor( ChannelProcessor )
        {
        }

        //---------------------------------------------------------------
        /// @brief Read channels from a stream
        ///
        /// @param[in] ChannelStream
        ///   Input stream containing channel namesm
        //---------------------------------------------------------------
        inline void
        Read( std::istream& ChannelStream )
        {
            std::string channel_name_descriptor;

            while ( std::getline( ChannelStream, channel_name_descriptor ) )
            {
                std::vector< std::string > tokens;

                if ( channel_name_descriptor.at( 0 ) == '#' )
                {
                    continue;
                }
                boost::algorithm::split( tokens,
                                         channel_name_descriptor,
                                         boost::is_any_of( "\t " ),
                                         boost::token_compress_on );
                std::string old_name;
                std::string new_name;
                std::string resampling;

                switch ( tokens.size( ) )
                {
                case 1:
                {
                    std::vector< std::string > old_new_name_tokens;
                    std::vector< std::string > name_resample_tokens;

                    boost::algorithm::split( old_new_name_tokens,
                                             tokens[ 0 ],
                                             boost::is_any_of( "=" ),
                                             boost::token_compress_on );
                    boost::algorithm::split( name_resample_tokens,
                                             old_new_name_tokens[ 0 ],
                                             boost::is_any_of( "!" ),
                                             boost::token_compress_on );
                    old_name = name_resample_tokens[ 0 ];
                    if ( name_resample_tokens.size( ) == 2 )
                    {
                        resampling = name_resample_tokens[ 1 ];
                    }
                    else
                    {
                        resampling = "1";
                    }
                    if ( old_new_name_tokens.size( ) == 2 )
                    {
                        new_name = old_new_name_tokens[ 1 ];
                    }
                    else
                    {
                        new_name = old_name;
                    }
                    break;
                }
                case 2:
                    // Case: <channel> <resample>
                    new_name = old_name = tokens[ 0 ];
                    resampling = tokens[ 1 ];
                    break;
                case 3:
                    // Case: <channel> <resample> <renamed_channel>
                    // Case: <channel> <resample>
                    old_name = tokens[ 0 ];
                    resampling = tokens[ 1 ];
                    new_name = tokens[ 1 ];
                    break;
                default:
                    continue;
                }
                processor( old_name, resampling, new_name );
            }
        }

        //---------------------------------------------------------------
        /// @brief Read channels from file
        ///
        /// @param[in] Filename
        ///   Name of file containing channel names
        //---------------------------------------------------------------
        void
        Read( boost::filesystem::path const& Filename )
        {
            boost::filesystem::ifstream channel_name_stream( Filename );
            Read( channel_name_stream );
        }

        void
        Read( std::string const& Filename )
        {
            boost::filesystem::path filename( Filename );
            Read( filename );
        }

    private:
        ChannelFunctor_Type& processor;
    }; // namespace FrameAPI

    typedef CREATERDS_START_TYPE                  start_type;
    typedef CREATERDS_END_TYPE                    end_type;
    typedef RDS::Options::compression_level_type  compression_level_type;
    typedef RDS::Options::compression_method_type compression_method_type;
    typedef RDS::Options::frames_per_file_type    frames_per_file_type;
    typedef RDS::Options::seconds_per_frame_type  seconds_per_frame_type;

    Frame createRDSFrame( const frame_file_container_type& FrameFiles,
                          start_type                       Start,
                          end_type                         End,
                          const channel_container_type&    Channels );

    void createRDSSet( const frame_file_container_type&  FrameFiles,
                       const channel_container_type&     Channels,
                       const FrameAPI::RDS::FileOptions& Opts );
} // namespace FrameAPI
#endif /* !defined( SWIGIMPORTED ) */

#endif /* FRAME_API__CREATE_RDS_HH */
