//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

extern "C" {
#include <sys/stat.h>
#include <unistd.h>
} // extern "C"

#include <cerrno>
#include <cstdio>
#include <cstring>

#include "ldastoolsal/FileType.hh"
#include "ldastoolsal/util.hh"

#include "framecpp/Common/FrameFilename.hh"
#include "framecpp/FrTOC.hh"

#include "genericAPI/Logging.hh"

#include "filereader.hh"
#include "util.hh"

using LDASTools::AL::FileType;
using LDASTools::AL::Rethrow;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

#if LM_DEBUG
#define INFO( a ) std::cerr << a << std::endl
#else
#define INFO( a )
#endif

void
FrameFile::check_if_dir( const std::string& Filename ) const
{
    //-------------------------------------------------------------------
    // Ensure the request is not to open a directory
    //-------------------------------------------------------------------
    struct stat         file_status;
    static unsigned int delays[] = { 0, 10 };

    for ( int cur = 0, last = ( sizeof( delays ) / sizeof( *delays ) ) + 1;
          cur != last;
          ++cur )
    {
        memset( &file_status, 0, sizeof( file_status ) );
        if ( stat( Filename.c_str( ), &file_status ) == 0 )
        {
            if ( S_ISDIR( file_status.st_mode ) )
            {
                if ( ( cur + 1 ) == last )
                {
                    //---------------------------------------------------
                    // It continues to persist as a directory. Report
                    //   back to the caller.
                    //---------------------------------------------------
                    std::ostringstream msg;

                    msg << Filename << ": "
                        << "permission_denied: the path refers to a directory."
                        << "( dev: " << file_status.st_dev
                        << " inode: " << file_status.st_ino
                        << " inode_size: " << sizeof( file_status.st_ino )
                        << " )";
                    throw SWIGEXCEPTION( msg.str( ) );
                }
                if ( delays[ cur ] )
                {
                    sleep( delays[ cur ] );
                }
                //-------------------------------------------------------
                // It was a directory, so need to do another iteration
                //-------------------------------------------------------
                continue;
            }
        }
        // Either it is not a directory or stat failed
        break;
    }
}

void
FrameFile::error( ) const
{
    int error = access( m_filename.c_str( ), R_OK );
    if ( error != 0 )
    {
        std::ostringstream oss;
        oss << m_filename << ": ";
        // convert error codes to SWIG exceptions.
        switch ( errno )
        {
        case EACCES:
            oss << "permission_denied: (EACCES) requested access to the "
                   "file or directory was denied.";
            break;
        case EROFS:
            oss << "permission_denied: (EROFS) file system is "
                   "read-only.";
            break;
        case EFAULT:
            oss << "file_not_found: (EFAULT) pathname points outside the "
                   "accessible address space.";
            break;
        case ENOENT:
            oss << "file_not_found: (ENOENT) A directory component does "
                   "not exist.";
            break;
        case ENOTDIR:
            oss << "file_not_found: (ENOTDIR) A component used as "
                   "a directory is not a directory.";
            break;
        case ENAMETOOLONG:
            oss << "file_not_found: (ENAMETOOLONG) pathname too long.";
            break;
        case ENOMEM:
            oss << "bad_alloc: (ENOMEM) Insufficient kernel memory "
                   "available.";
            break;
        case ELOOP:
            oss << "file_not_found: (ELOOP) Too many symbolic links "
                   "encountered.";
            break;
        case EIO:
            oss << "io_error: (EIO) An I/O error occurred.";
            break;
        default:
            oss << "system error: " << errno;
            break;
        }
        throw SWIGEXCEPTION( oss.str( ) );
    }
}

//-----------------------------------------------------------------------------
///
// \brief Constructor
///
/// \param[in] filename
///     The name of the frame file.
/// \param[in] size
///     Size of the buffer to use. If the value
///	is zero, then the default size will be used and the
///	buffer parameter will be ignored
/// \param[in] buffer
///     Optional buffer for i/o. If the value is
///	(char*)NULL, then the class will allocate and manage
///	a buffer based on size.
/// \param[in] EnableMemoryMappedIO
///     True if memory mapped io is to be used
/// \param[in] VerifyFilename
///     True if the filename is to be verified against
///	the internal data.
///
//-----------------------------------------------------------------------------
FileReader::FileReader( const char*  filename,
                        unsigned int size,
                        char*        buffer,
                        bool         EnableMemoryMappedIO,
                        bool         VerifyFilename ) try
    : FrameFile( filename, size, buffer, EnableMemoryMappedIO, std::ios::in ),
      FrameCPP
    ::IFrameStream( m_frame_buf )
    {
        using FrameCPP::Common::FrameFilename;

        if ( ( VerifyFilename ) && ( GetTOC( ) != (FrameCPP::FrTOC*)NULL ) )
        {
            FrameFilename                  ffn( GetFilename( ) );
            const FrameCPP::Common::FrTOC& toc( *( GetTOC( ) ) );

            LDASTools::AL::GPSTime ffn_gps_start( ffn.G( ), 0 );
            LDASTools::AL::GPSTime ffn_gps_end( ffn_gps_start + ffn.T( ) );

            LDASTools::AL::GPSTime gps_start( toc.GTimeS( )[ 0 ],
                                              toc.GTimeN( )[ 0 ] );
            LDASTools::AL::GPSTime gps_end(
                toc.GTimeS( )[ toc.nFrame( ) - 1 ],
                toc.GTimeN( )[ toc.nFrame( ) - 1 ] );
            gps_end += toc.dt( )[ toc.nFrame( ) - 1 ];
            if ( ( gps_start < ffn_gps_start ) || ( gps_end > ffn_gps_end ) )
            {
                std::ostringstream msg;

                msg << "Data inconsistency detected between filename and "
                       "internal data";
                throw std::logic_error( msg.str( ) );
            }
        }
    }
catch ( const std::exception& e )
{
    std::ostringstream msg;
    FileType           ft( filename );

    msg << filename << " (file type: " << ft.GetFileType( ) << ")";
    Rethrow( msg.str( ), e );
}

//-----------------------------------------------------------------------------
//
//: Constructor
//
//! param: const char* filename - The name of the frame file.
//! param: char* buffer - optional buffer for i/o. If the value is
//+		(char*)NULL, then the class will allocate and manage
//+		a buffer based on <i>size</i>.
//! param: unsigned int size - size of the buffer to use. If the value
//+		is zero, then the default size will be used and the
//+		<i>buffer</i> parameter will be ignored
//! param: const char* tmp_filename - The name of the temporary file. NOTE:
//+	This file must be on the same device as filename.
//
//! exc: SwigException - this is one of: <ul type=disc>
//+     <li>permission_denied - Permission to access the file or a
//+         directory component was denied.</li>
//+     <li>file_not_found - The file or a directory component was not
//+         found.</li>
//+     <li>bad_alloc - Insufficient kernel memory to open the file.</li>
//+     <li>io_error - An unknown I/O error occurred.</li></ul>
//+     <li>file_creation_failed - The file could not be created.</li></ul>
//
FileWriter::FileWriter( const char* filename, const char* tmp_filename ) try
    : FrameFile( filename,
                 0,
                 (char*)NULL,
                 false,
                 std::ios::out | std::ios::trunc,
                 tmp_filename ),
      FrameCPP
    ::OFrameStream( m_frame_buf ), file_start_time( 0 ), file_dt( 0 ),
        mTmpFilename( ( tmp_filename ? tmp_filename : "" ) )
    {
        INFO( "DEBUG: FileWriter::FileWriter: filename: "
              << filename << " tmp_filename: " << mTmpFilename );
    }
catch ( const std::exception& e )
{
    AT( );
    std::ostringstream err;

    err << filename << ": " << e.what( );
    throw std::runtime_error( err.str( ) );
}

//-----------------------------------------------------------------------------
//
//: Constructor
//
//! param: const char* filename - The name of the frame file.
//! param: char* buffer - optional buffer for i/o. If the value is
//+		(char*)NULL, then the class will allocate and manage
//+		a buffer based on <i>size</i>.
//! param: unsigned int size - size of the buffer to use. If the value
//+		is zero, then the default size will be used and the
//+		<i>buffer</i> parameter will be ignored
//! param: const char* tmp_filename - The name of the temporary file. NOTE:
//+	This file must be on the same device as filename.
//
//! exc: SwigException - this is one of: <ul type=disc>
//+     <li>permission_denied - Permission to access the file or a
//+         directory component was denied.</li>
//+     <li>file_not_found - The file or a directory component was not
//+         found.</li>
//+     <li>bad_alloc - Insufficient kernel memory to open the file.</li>
//+     <li>io_error - An unknown I/O error occurred.</li></ul>
//+     <li>file_creation_failed - The file could not be created.</li></ul>
//
FileWriter::FileWriter( const char*  filename,
                        unsigned int size,
                        char*        buffer,
                        bool         EnableMemoryMappedIO,
                        const char*  tmp_filename )
    : FrameFile( filename,
                 size,
                 buffer,
                 EnableMemoryMappedIO,
                 std::ios::out | std::ios::trunc,
                 tmp_filename ),
      FrameCPP::OFrameStream( m_frame_buf ), file_start_time( 0 ), file_dt( 0 ),
      file_frame_count( 0 ),
      mTmpFilename( ( tmp_filename ? tmp_filename : "" ) )
{
    INFO( "DEBUG: FileWriter::FileWriter: filename: "
          << filename << " tmp_filename: " << mTmpFilename );
}

//-----------------------------------------------------------------------------
//: Destructor
FileWriter::~FileWriter( )
{
    INFO( "DEBUG: FileWriter::~FileWriter: mFilename: "
          << GetFilename( ) << " mTmpFilename: " << mTmpFilename );
    FrameCPP::OFrameStream::Close( );
    finish( );
    Rename( );
}

//-----------------------------------------------------------------------------
//: Performs any file renaming that is to be done
//-----------------------------------------------------------------------------
void
FileWriter::Rename( )
{
    INFO( "DEBUG: FileWriter::Rename: mFilename: "
          << GetFilename( ) << " mTmpFilename: " << mTmpFilename );
    std::string tmp_name( mTmpFilename );
    int         status = rename_file( );
    if ( status != 0 )
    {
        AT( );
        std::ostringstream msg;

        msg << "Failed to rename: " << tmp_name << " to: " << GetFilename( )
            << " because: " << strerror( errno );
        throw std::runtime_error( msg.str( ) );
    }
}

void
FileWriter::WriteFrame( object_type FrameObject, chkSum_type FrameChecksumType )
{
    static const char* caller = "FileWriter::WriteFrame([2args])";

    update( FrameObject );
    FrameCPP::OFrameStream::WriteFrame( FrameObject, FrameChecksumType );
    ++file_frame_count;
    QUEUE_LOG_MESSAGE( "After writing framme"
                           << " start_time: " << StartTime( )
                           << " dt: " << Dt( ),
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );
}

void
FileWriter::WriteFrame( object_type FrameObject,
                        INT_2U      CompressionScheme,
                        INT_2U      CompressionLevel,
                        chkSum_type FrameChecksumType )
{
    static const char* caller = "FileWriter::WriteFrame([4args])";

    update( FrameObject );
    FrameCPP::OFrameStream::WriteFrame(
        FrameObject, CompressionScheme, CompressionLevel, FrameChecksumType );
    ++file_frame_count;
    QUEUE_LOG_MESSAGE( "After writing frame"
                           << " start_time: " << StartTime( )
                           << " dt: " << Dt( ),
                       MT_DEBUG,
                       30,
                       caller,
                       "RDSResample" );
}

int
FileWriter::rename_file( )
{
    int retval = 0;

    AT( );
    if ( mTmpFilename.length( ) > 0 )
    {
        AT( );
        retval = rename( mTmpFilename.c_str( ), GetFilename( ).c_str( ) );
        mTmpFilename = "";
    }
    return retval;
}

inline void
FileWriter::update( object_type FrameObject )
{
    try
    {
        boost::shared_ptr< FrameCPP::Common::FrameH > fr =
            boost::dynamic_pointer_cast< FrameCPP::Common::FrameH >(
                FrameObject );

        if ( !file_start_time )
        {
            file_start_time = fr->normalizedStartTime( ).GetSeconds( );
        }
        file_dt += fr->normalizedDeltaT( );
    }
    catch ( ... )
    {
    }
}
