//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "frameutils_config.h"

#if !HAVE_MNTENT_H && !HAVE_SYS_MNTTAB_H
#define HAVE_GETMNTINFO 1
#endif /* ! HAVE_MNTENT_H && ! HAVE_SYS_MNTTAB_H */

#include <algorithm>
#include <iostream>
#include <list>
#include <memory>
#include <stdexcept>

#include <sys/stat.h>
#if HAVE_MNTENT_H
#include <mntent.h>
typedef struct mntent mount_entry_type;
#define mount_entry_buffer_type char[ 4096 ]
#define MM_MNT_FSNAME( a ) ( a.mnt_fsname )
#define MM_MNT_DIR( a ) ( a.mnt_dir )
#define MM_MNT_TYPE( a ) ( a.mnt_type )
#define MM_MNT_OPTS( a ) ( a.mnt_opts )
#define mount_entry_info( a, b ) a, b
#endif /* HAVE_MNTENT_H */
#include <stdio.h>
#include <unistd.h>
#if HAVE_SYS_MNTTAB_H
#include <sys/mnttab.h>
typedef struct mnttab mount_entry_type;
#define MM_MNT_FSNAME( a ) ( a.mnt_special )
#define MM_MNT_DIR( a ) ( a.mnt_mountp )
#define MM_MNT_TYPE( a ) ( a.mnt_fstype )
#define MM_MNT_OPTS( a ) ( a.mnt_mntopts )
#define mount_entry_info( a, b ) a
#endif /* HAVE_SYS_MNTTAB_H */

#if HAVE_GETMNTINFO
#include <sys/param.h>
#include <sys/ucred.h>
#include <sys/mount.h>
typedef struct statfs mount_entry_type;
#define MM_MNT_FSNAME( a ) ( a.f_mntfromname )
#define MM_MNT_DIR( a ) ( a.f_mntonname )
#define MM_MNT_TYPE( a ) ( a.f_fstypename )
#define MM_MNT_OPTS( a ) ( MountTableHandle::MountOpts( a.f_flags ) )
#define mount_entry_info( a, b ) a
#endif /* HAVE_GETMNTINFO */

#include <sstream>
#include <stdexcept>

#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/System.hh"
#include "ldastoolsal/Thread.hh"

#include "genericAPI/Logging.hh"
#include "genericAPI/Stat.hh"

#include "MountMgr.hh"

using namespace FrameAPI;

using LDASTools::AL::MutexLock;
using LDASTools::AL::ReadWriteLock;
using LDASTools::AL::Thread;

#if HAVE_MNTENT_H || HAVE_SYS_MNTTAB_H
#define USE_MOUNTTAB_FILE 1
#endif /* HAVE_MNTENT_H || HAVE_SYS_MNTTAB_H */
namespace
{
    class MountTableHandle
    {
    public:
        MountTableHandle( );

        bool IsModified( time_t LastModifiedTime ) const;

        time_t ModifiedTime( );

        void Open( );

        void Close( );

        bool Query( mount_entry_type& Entry );

#if HAVE_GETMNTINFO
        static std::string
        MountOpts( int Flags )
        {
            std::ostringstream retval;

            return retval.str( );
        }
#endif /* HAVE_GETMNTINFO */
    private:
#if USE_MOUNTTAB_FILE
        const char* mtab;
        struct stat statbuf;
        int         stat_status;
        FILE*       fmtab;

        FILE* fd( );
#endif /* USE_MOUNTTAB_FILE */
#if HAVE_GETMNTINFO
        int cur_entry;
        int max_entry;

        mount_entry_type* entries;
#endif /* HAVE_GETMNTINFO */
    };

#if USE_MOUNTTAB_FILE
    inline FILE*
    MountTableHandle::fd( )
    {
        return fmtab;
    }
#endif /* USE_MOUNTTAB_FILE */

#if UNUSED
    class SyncHelper : public Thread
    {
    public:
        SyncHelper( MountMgr::mnt_info_type&       MntInfo,
                    MountMgr::active_fstypes_type& ActiveFSTypes );

        virtual ~SyncHelper( );

        bool Query( MountTableHandle& MountTable );

#if UNUSED
        static void
        QueryMountTable( MountTableHandle&              MountTable,
                         MountMgr::mnt_info_type&       MntInfo,
                         MountMgr::active_fstypes_type& ActiveFSTypes );
#endif /* UNUSED  */

    protected:
        virtual void action( );

    private:
        typedef std::list< SyncHelper* > pending_container_type;

        static MutexLock::baton_type m_baton;

        MountMgr::mnt_info_type&       m_mnt_info;
        MountMgr::active_fstypes_type& m_active_fstypes;

        mount_entry_type              m_mount_entry;
        struct stat                   m_stat_buffer;
        static pending_container_type m_pending;
    };
#endif /* UNUSED */
} // anonymous namespace

MountMgr::MountMgr( ) : m_last_mtime( (time_t)0 )
{
}

MountMgr::~MountMgr( )
{
}

void
MountMgr::IsKnownFSType( const std::string& FSType )
{
    sync( );

    ReadWriteLock lock(
        m_update_lock, ReadWriteLock::READ, __FILE__, __LINE__ );

    const active_fstypes_type::const_iterator fstype_loc =
        m_active_fstypes.find( FSType );
    if ( fstype_loc == m_active_fstypes.end( ) )
    {
        std::ostringstream msg;

        msg << "Unknown fstype: " << FSType
            << " (Known types are: " << m_active_fstypes_msg.str( ) << ")";

        throw std::range_error( msg.str( ) );
    }
}

#if 0
const MountMgr::MountPoint MountMgr::
Lookup( const std::string& FSType )
{
  sync( );

  ReadWriteLock
    lock( m_update_lock, ReadWriteLock::READ,
	  __FILE__, __LINE__ );

  for ( mnt_info_type::const_iterator
	  cur = m_mnt_info.begin( ),
	  end = m_mnt_info.end( );
	cur != end;
	++cur )
  {
    if ( (*cur).second.s_type.compare( FSType )  == 0 )
    {
      return (*cur).second;
    }
  }

  const active_fstypes_type::const_iterator
    fstype_loc = m_active_fstypes.find( FSType );
  if ( fstype_loc != m_active_fstypes.end( ) )
  {
    return fstype_loc->second;
  }

  std::ostringstream	msg;

  msg << "Unknown fstype " << FSType;
  throw std::runtime_error( msg.str( ) );
}
#endif /* 0 */

const MountMgr::MountPoint
MountMgr::Lookup( dev_t Device )
{
    sync( );

    ReadWriteLock lock(
        m_update_lock, ReadWriteLock::READ, __FILE__, __LINE__ );

    mnt_info_type::const_iterator cur( m_mnt_info.find( Device ) );
    if ( cur != m_mnt_info.end( ) )
    {
        return ( *cur ).second;
    }

    std::ostringstream msg;

    msg << "Unable to determine the File System type for device " << Device;
    throw std::runtime_error( msg.str( ) );
}

void
MountMgr::sync( )
{
    MountTableHandle mtab_handle;
    time_t           new_modified_time( 0 );

    if ( mtab_handle.IsModified( m_last_mtime ) )
    {
        ReadWriteLock lock(
            m_update_lock, ReadWriteLock::WRITE, __FILE__, __LINE__ );

        try
        {
            new_modified_time = mtab_handle.ModifiedTime( );
            if ( m_last_mtime == new_modified_time )
            {
                //---------------------------------------------------------------
                // Someone else did it while we were waiting
                //---------------------------------------------------------------
                return;
            }
        }
        catch ( const std::exception& Exception )
        {
            //-----------------------------------------------------------------
            // Something went wrong, go with what was previously known
            //-----------------------------------------------------------------
            return;
        }
    reread_mnt_table:
        m_last_mtime = new_modified_time;
        // Clear out the old cache and put in the new data
        m_mnt_info.erase( m_mnt_info.begin( ), m_mnt_info.end( ) );
        m_active_fstypes.erase( m_active_fstypes.begin( ),
                                m_active_fstypes.end( ) );
        m_active_fstypes_msg.str( "" );

        mtab_handle.Open( );
#if old
        //-------------------------------------------------------------------
        // Start threads scanning the mount table entries
        //-------------------------------------------------------------------
        SyncHelper::QueryMountTable(
            mtab_handle, m_mnt_info, m_active_fstypes );
#else /* old */
        {
            mount_entry_type buffer;
            dev_t            mnt_dev;
            //-----------------------------------------------------------------
            // Read the entry
            //-----------------------------------------------------------------
            while ( mtab_handle.Query( buffer ) )
            {
                //---------------------------------------------------------------
                // Store the entry
                //---------------------------------------------------------------
                std::string mnt_fsname( MM_MNT_FSNAME( buffer ) );
                std::string mnt_dir( MM_MNT_DIR( buffer ) );
                std::string mnt_type( MM_MNT_TYPE( buffer ) );
                std::string mnt_opts( MM_MNT_OPTS( buffer ) );
                std::string mnt_fstype( MM_MNT_TYPE( buffer ) );

                try
                {
                    struct stat stat_buffer;

                    GenericAPI::Stat( mnt_fsname, stat_buffer );
                    mnt_dev = stat_buffer.st_dev;
                    m_mnt_info[ mnt_dev ] = MountPoint(
                        mnt_fsname, mnt_dir, mnt_type, mnt_opts, mnt_dev );

                    m_active_fstypes.insert( mnt_fstype );
                }
                catch ( ... )
                {
                }
            }
        }
#endif /* old */

        mtab_handle.Close( );

        //-------------------------------------------------------------------
        // Check to see if anything has changed since reading the information
        //-------------------------------------------------------------------
        try
        {
            new_modified_time = mtab_handle.ModifiedTime( );

            if ( m_last_mtime < new_modified_time )
            {
                goto reread_mnt_table;
            }
        }
        catch ( const std::exception& Exception )
        {
            goto reread_mnt_table;
        }

#if old
        m_active_fstypes.sort( );
        m_active_fstypes.unique( );
#endif /* old */

        bool first = true;
        for ( active_fstypes_type::const_iterator
                  cur = m_active_fstypes.begin( ),
                  last = m_active_fstypes.end( );
              cur != last;
              ++cur )
        {
            if ( !first )
            {
                m_active_fstypes_msg << ", ";
            }
            else
            {
                first = false;
            }
            m_active_fstypes_msg << *cur;
        }
    } // if -
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
MountMgr::MountPoint::MountPoint( )
{
}

MountMgr::MountPoint::MountPoint( const std::string& FSName,
                                  const std::string& Dir,
                                  const std::string& Type,
                                  const std::string& Options,
                                  dev_t              Device )
    : s_fsname( FSName ), s_dir( Dir ), s_type( Type ), s_options( Options ),
      s_device( Device )
{
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
namespace
{
    MountTableHandle::MountTableHandle( )
#if USE_MOUNTTAB_FILE
        : mtab( (const char*)NULL )
#endif /* USE_MOUNTTAB_FILE */
    {
#if USE_MOUNTTAB_FILE
        //-------------------------------------------------------------------
        // These are the possible names of the mount point file
        //-------------------------------------------------------------------
        static const char* mnttab_files[] = {
            "/etc/mnttab", // Solaris
            "/etc/mtab" // Linux
        };

        // Loop over all known files that would contain mount point information
        for ( unsigned int x = 0; ( mtab == (const char*)NULL ) &&
              ( x < ( sizeof( mnttab_files ) / sizeof( *mnttab_files ) ) );
              ++x )
        {
            if ( access( mnttab_files[ x ], R_OK ) == 0 )
            {
                mtab = mnttab_files[ x ];
                break;
            }
        }
        if ( mtab == (const char*)NULL )
        {
            // Bad news. Cannot find the mount table file for which we have
            // read access
            throw std::runtime_error(
                "Unable to locate readable mount table file" );
        }
        stat_status = GenericAPI::Stat( mtab, statbuf );
#endif /* USE_MOUNTTAB_FILE */
    }

    bool
    MountTableHandle::IsModified( time_t LastModifiedTime ) const
    {
#if USE_MOUNTTAB_FILE
        return ( ( stat_status == 0 ) &&
                 ( LastModifiedTime < statbuf.st_mtime ) );
#elif HAVE_GETMNTINFO
        return true;
#else /* USE_MOUNTTAB_FILE */
#error Cannot determine if the mount table has been modified.
#endif /* USE_MOUNTTAB_FILE */
    }

    time_t
    MountTableHandle::ModifiedTime( )
    {
#if USE_MOUNTTAB_FILE
        if ( ( stat_status = GenericAPI::Stat( mtab, statbuf ) ) != 0 )
        {
            // Cannot stat the mtab file so just use the previous info
            throw std::runtime_error( "Stat of mount table file failed" );
        }
        return statbuf.st_mtime;
#elif HAVE_GETMNTINFO
        return 0;
#else /* USE_MOUNTTAB_FILE */
#error Cannot determine if the mount table has been modified.
#endif /* USE_MOUNTTAB_FILE */
    }

    void
    MountTableHandle::Open( )
    {
#if USE_MOUNTTAB_FILE
        fmtab = fopen( mtab, "r" );
#if HAVE_SYS_MNTTAB_H
        resetmnttab( fmtab );
#endif /* HAVE_SYS_MNTTAB_H */
#endif /* USE_MOUNTTAB_FILE */
#if HAVE_GETMNTINFO
        max_entry = getmntinfo( &entries, 0 );
#endif /* HAVE_GETMNTINFO */
    }

    void
    MountTableHandle::Close( )
    {
#if USE_MOUNTTAB_FILE
        fclose( fmtab );
        fmtab = (FILE*)NULL;
#endif /* USE_MOUNTTAB_FILE */
    }

    bool
    MountTableHandle::Query( mount_entry_type& Entry )
    {
#if HAVE_MNTENT_H
        char entry_buffer[ 2048 ];
#endif /* HAVE_MNTENT_H */

        bool retval = false;
#if HAVE_MNTENT_H
        retval = ( (mount_entry_type*)NULL ==
                   getmntent_r(
                       fd( ), &Entry, entry_buffer, sizeof( entry_buffer ) ) );
#elif HAVE_SYS_MNTTAB_H
        retval = ( 0 == getmntent( fd( ), &Entry ) );
#elif HAVE_GETMNTINFO
        retval = ( cur_entry < max_entry );
        Entry = entries[ cur_entry++ ];
#else
#error Do not have a method of getting mount info
#endif
        return retval;
    }

#if UNUSED
    MutexLock::baton_type SyncHelper::m_baton( __FILE__, __LINE__ );

    SyncHelper::pending_container_type SyncHelper::m_pending;

    SyncHelper::SyncHelper( MountMgr::mnt_info_type&       MntInfo,
                            MountMgr::active_fstypes_type& ActiveFSTypes )
        : m_mnt_info( MntInfo ), m_active_fstypes( ActiveFSTypes )
    {
    }

    SyncHelper::~SyncHelper( )
    {
        //-------------------------------------------------------------------
        // Make sure we are not still registered
        //-------------------------------------------------------------------
        MutexLock lock( m_baton, __FILE__, __LINE__ );

        m_pending.remove( this );
    }

#if UNUSED
    bool
    SyncHelper::Query( MountTableHandle& MountTable )
    {
        return MountTable.Query( m_mount_entry );
    }
#endif /* UNUSED */

#if UNUSED
    void
    SyncHelper::QueryMountTable( MountTableHandle&              MountTable,
                                 MountMgr::mnt_info_type&       MntInfo,
                                 MountMgr::active_fstypes_type& ActiveFSTypes )
    {
        //-------------------------------------------------------------------
        // Create a thread for each entry in the mount table to scan
        // the type of file system.
        //-------------------------------------------------------------------
        MutexLock lock( m_baton, __FILE__, __LINE__ );
        bool      scanning = true;

        while ( scanning )
        {
            std::unique_ptr< SyncHelper > helper(
                new SyncHelper( MntInfo, ActiveFSTypes ) );

            try
            {
                scanning = helper->Query( MountTable );
                if ( scanning )
                {
                    m_pending.push_back( helper.release( ) );
                }
            }
            catch ( ... )
            {
                scanning = false;
            }
        }
        //-------------------------------------------------------------------
        // Done with needing exclusive access to the data
        //-------------------------------------------------------------------
        lock.Release( __FILE__, __LINE__ );
        //-------------------------------------------------------------------
        // Loop over all requests and wait for them to complete
        //-------------------------------------------------------------------
        while ( m_pending.size( ) > 0 )
        {
            int                    cnt = 50;
            pending_container_type work;
            while ( cnt-- && ( m_pending.empty( ) == false ) )
            {
                work.push_back( m_pending.front( ) );
                work.back( )->Spawn( );
                m_pending.pop_front( );
            }

            for ( pending_container_type::const_iterator cur = work.begin( ),
                                                         last = work.end( );
                  cur != last;
                  ++cur )
            {
                std::unique_ptr< SyncHelper > helper( *cur );

                try
                {
                    helper->Join( ); // Wait for its completion
                }
                catch ( ... )
                {
                    helper->Detach( );
                }
            }
        }
    }
#endif /* UNUSED */

    void
    SyncHelper::action( )
    {
        static const char method_name[] = "FrameAPI::SyncHelper::action";

        //-------------------------------------------------------------------
        // Gain information about the mount table entry
        //-------------------------------------------------------------------
        std::string dir( MM_MNT_DIR( m_mount_entry ) );

        if ( dir[ 0 ] != '/' )
        {
            dir = "/";
            dir += MM_MNT_DIR( m_mount_entry );
        }
        if ( GenericAPI::Stat( dir, m_stat_buffer ) != 0 )
        {
            //-----------------------------------------------------------------
            // Do nothing since there was some error in getting the mount
            // table entry
            //-----------------------------------------------------------------
            std::ostringstream msg;

            msg << "Unable to get stat information for: " << dir << " ("
                << LDASTools::System::ErrnoMessage( ) << ")";
            GenericAPI::queueLogEntry(
                msg.str( ),
                GenericAPI::LogEntryGroup_type::MT_ORANGE,
                0,
                method_name,
                "CXX" );

            return;
        }
        //-------------------------------------------------------------------
        // Add to the list of known mount points using m_baton to ensure
        // only a single thread is updating the information at a time.
        //-------------------------------------------------------------------
        {
            std::ostringstream msg;

            msg << "Unable to get stat information for: " << dir;
            GenericAPI::queueLogEntry( msg.str( ),
                                       GenericAPI::LogEntryGroup_type::MT_OK,
                                       0,
                                       method_name,
                                       "CXX" );
        }
        {
            MutexLock lock( m_baton, __FILE__, __LINE__ );

            m_mnt_info[ m_stat_buffer.st_dev ] =
                MountMgr::MountPoint( MM_MNT_FSNAME( m_mount_entry ),
                                      MM_MNT_DIR( m_mount_entry ),
                                      MM_MNT_TYPE( m_mount_entry ),
                                      MM_MNT_OPTS( m_mount_entry ),
                                      m_stat_buffer.st_dev );

#if old
            m_active_fstypes.push_back( MM_MNT_TYPE( m_mount_entry ) );
#else
            m_active_fstypes.insert( MM_MNT_TYPE( m_mount_entry ) );
#endif /* old */

            lock.Release( __FILE__, __LINE__ );
        }
    }
#endif /* UNUSED */

} // namespace
