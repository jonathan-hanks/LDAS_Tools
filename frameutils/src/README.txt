//-----------------------------------------------------------------------
/*! \mainpage The Frame Utility Library
 *
 * @anchor LDASToolsFrameUtility
 *
 * \section abstract_sec Abstract
 *
 * The purpose of the Frame Utility Library is to provide a collection
 * of useful algorithms for manipulating frame data as described by
 * the document titled "Specification of a Common Data Frame Format
 * for Interferometric Gravitational Wave Detectors (IGWD)".
 * The document can be read in full from the DCC as LIGO-T970130.
 *
 * \section user_sec Users
 *
 * This package does provides no command line tools.
 *
 * \section devel_sec Developers
 *
 * \subsection devel_cplusplus C++
 *
 * For C++ developers, there is a rich extension to the FrameCPP library.
 * With this library, creation of RDS (Reduced Data Set) frame files
 * becomes a nearly trivial exercise.
 *
 * \subsection devel_c_sec C
 * 
 * This library has not yet been made available for C developers.
 *
 * \subsection devel_python_sec Python
 *
 * Many of the classes that are exposed for the C++ developer are also
 * available for Python developers.
 * The core module is ????
 */
