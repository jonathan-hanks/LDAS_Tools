//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__STAGE__HH
#define FRAME_API__STAGE__HH

#include <string>

#include "DeviceIOConfiguration.hh"
#include "SAMFSLoader.hh"

namespace FrameAPI
{
    static SAMFSLoader samfs;

    inline void
    Stage( const std::string& Filename )
    {
        DeviceIOConfiguration::size_type size;
        bool                             memory_mapped_io;
        std::string                      fstype;

        DeviceIOConfiguration::GetConfiguration(
            Filename.c_str( ), size, memory_mapped_io, fstype );
        if ( fstype == "samfs" )
        {
            //---------------------------------------------------------------
            // Stage for immediate loading
            //---------------------------------------------------------------
            samfs.Stage( Filename, "i" );
        }
    } // function Stage

} // namespace FrameAPI

#endif /* FRAME_API__STAGE__HH */
