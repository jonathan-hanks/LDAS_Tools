//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <sstream>

#include "framecpp/FrRawData.hh"

#include "frameAPI/Channel.hh"
#include "frameAPI/Frame.hh"

namespace
{
    class AdcChannel : public ::FrameAPI::Channel
    {
    public:
        typedef ::FrameCPP::FrRawData::firstAdc_type::value_type channel_type;

        AdcChannel( channel_type Channel );

        virtual data_type& RefData( ) const;

    private:
        channel_type m_channel;
    };

    class ProcChannel : public ::FrameAPI::Channel
    {
    public:
        typedef ::FrameCPP::FrameH::procData_type::value_type channel_type;

        ProcChannel( channel_type Channel );

        virtual data_type& RefData( ) const;

    private:
        channel_type m_channel;
    };
} // namespace

namespace FrameAPI
{

    //=====================================================================

    Frame::NoChannelFound::NoChannelFound( const std::string& ChannelName )
        : std::runtime_error( message( ChannelName ) )
    {
    }

    std::string
    Frame::NoChannelFound::message( const std::string& ChannelName )
    {
        std::ostringstream msg;

        msg << "Unable to find channel: " << ChannelName;
        return msg.str( );
    }

    //=====================================================================

    Frame::frameh_type
    Frame::operator=( frameh_type RHS )
    {
        return frameh_type::operator=( RHS );
    }

    Frame::channel_type
    Frame::GetChannel( const std::string& ChannelName ) const
    {
        channel_type retval;
        //-------------------------------------------------------------------
        // First check if the channel has already been "cached"
        //-------------------------------------------------------------------
        {
            channel_container_type::const_iterator cur =
                m_channels.find( ChannelName );
            if ( cur != m_channels.end( ) )
            {
                retval = cur->second;
            }
        }
        if ( ( !retval ) && ( *this ) )
        {
            //-----------------------------------------------------------------
            // Look in the Adc list
            //-----------------------------------------------------------------
            FrameCPP::FrameH::rawData_type rd( ( *this )->GetRawData( ) );
            if ( rd )
            {
                FrameCPP::FrRawData::firstAdc_type& adc( rd->RefFirstAdc( ) );
                FrameCPP::FrRawData::firstAdc_type::const_iterator cur(
                    adc.find( ChannelName ) );

                if ( cur != adc.end( ) )
                {
                    retval.reset( new AdcChannel( *cur ) );
                }
            }
            if ( !retval )
            {
                //---------------------------------------------------------------
                // Look in the Proc list
                //---------------------------------------------------------------
                FrameCPP::FrameH::procData_type::const_iterator cur(
                    ( *this )->RefProcData( ).find( ChannelName ) );

                if ( cur != ( *this )->RefProcData( ).end( ) )
                {
                    retval.reset( new ProcChannel( *cur ) );
                }
            }
            if ( !retval )
            {
                //---------------------------------------------------------------
                // Look in the SimData list
                //---------------------------------------------------------------
            }
            if ( retval )
            {
                //---------------------------------------------------------------
                // Cache the found channel
                //---------------------------------------------------------------
                m_channels[ ChannelName ] = retval;
            }
            else
            {
                throw NoChannelFound( ChannelName );
            }
        }
        return retval;
    }
} // namespace FrameAPI

namespace
{

    //=====================================================================
    AdcChannel::AdcChannel( channel_type Channel ) : m_channel( Channel )
    {
    }

#if 0
  const AdcChannel::frvect_type& AdcChannel::
  getDataVector( ) const
  {
    return m_channel->RefData( );
  }
#else
    AdcChannel::data_type&
    AdcChannel::RefData( ) const
    {
        return m_channel->RefData( );
    }
#endif /* 0 */

    //=====================================================================
    ProcChannel::ProcChannel( channel_type Channel ) : m_channel( Channel )
    {
    }

#if 0
  const ProcChannel::frvect_type& ProcChannel::
  getDataVector( ) const
  {
    return m_channel->RefData( );
  }
#else
    ProcChannel::data_type&
    ProcChannel::RefData( ) const
    {
        return m_channel->RefData( );
    }
#endif /* 0 */

} // namespace
