//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

#include <cmath>
#include <iomanip>
#include <sstream>
#include <string>

#include "ldastoolsal/types.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/util.hh"

#include "framecpp/Common/FrameFilename.hh"
#include "framecpp/Common/MD5Sum.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrTable.hh"
#include "framecpp/FrVect.hh"

#include "genericAPI/FileLock.hh"
#include "genericAPI/swigexception.hh"

#include "DeviceIOConfiguration.hh"

#include "util.hh"

using LDASTools::AL::slower;

using namespace FrameAPI;
using std::string;

using FrameCPP::Common::FrameFilename;
using GenericAPI::FileLock;

int FrameAPI::EnableMemoryMappedIO = false;
int FrameAPI::StreamBufferSize =
    int( FrameCPP::Common::FrameBufferInterface::M_BUFFER_SIZE_DEFAULT );

bool
FrameAPI::Continuous( const LDASTools::AL::GPSTime& T1,
                      const LDASTools::AL::GPSTime& T2 )
try
{
    const REAL_8 t1( T1.GetSeconds( ) +
                     ( T1.GetNanoseconds( ) * NANO_SECOND ) );
    const REAL_8 t2( T2.GetSeconds( ) +
                     ( T2.GetNanoseconds( ) * NANO_SECOND ) );

    return ( std::abs( t1 - t2 ) <= TIME_TOLERANCE );
}
catch ( ... )
{
    return false;
}

void
FrameAPI::LogMD5Sum( const std::string&              FFilename,
                     const FrameCPP::Common::MD5Sum& MD5,
                     const std::string&              OutputDir )
{
    const FrameFilename    ffname( FFilename );
    std::ostringstream     md5_string;
    std::string::size_type p = ffname.Dir( ).rfind( '/' );

    if ( p == std::string::npos )
    {
        return;
    }
    std::string filename( ( OutputDir.length( ) > 0 ) ? OutputDir
                                                      : ffname.Dir( ) );
    filename += "/";
    filename += ffname.Dir( ).substr( ++p );
    filename += ".md5";

    FileLock md5_file( filename );
    md5_file.Buffer( ) << MD5 << "  " << ffname.Base( ) << std::endl;
}

FrameCPP::FrVect::compression_scheme_type
FrameAPI::StrToCompressionScheme( const char* Method )
{
    //-------------------------------------------------------------------
    // Sanity checks on compression method parameter
    //-------------------------------------------------------------------
    std::string                               method( slower( Method ) );
    FrameCPP::FrVect::compression_scheme_type retval = FrameCPP::FrVect::RAW;
    if ( method.length( ) )
    {
        using namespace FrameCPP;

        if ( method == "raw" )
        {
            retval = FrVect::RAW;
        }
        else if ( method == "gzip" )
        {
            retval = FrVect::GZIP;
        }
        else if ( method == "diff_gzip" )
        {
            retval = FrVect::DIFF_GZIP;
        }
#if FRAME_SPEC_CURRENT >= 9
        else if ( method == "zero_suppress" )
        {
            retval = FrVect::ZERO_SUPPRESS;
        }
#else /* <= 8 */
        else if ( method == "zero_suppress_short" )
        {
            retval = FrVect::ZERO_SUPPRESS_SHORT;
        }
        else if ( method == "zero_suppress_int_float" )
        {
            retval = FrVect::ZERO_SUPPRESS_INT_FLOAT;
        }
#endif /* 0 */
        else if ( method == "zero_suppress_otherwise_gzip" )
        {
            retval = FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP;
        }
        else
        {
            std::ostringstream oss;

            oss << "Bad compression method: " << Method;
            throw SWIGEXCEPTION( oss.str( ) );
        }
    }
    return retval;
}
