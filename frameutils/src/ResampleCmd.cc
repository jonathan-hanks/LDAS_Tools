//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/objectregistry.hh"
#include "ldastoolsal/Singleton.hh"

#include "framecpp/Dimension.hh"

#include "filters/Resample.hh"
#include "filters/basic_array.hh"

/* #include "framecmd.hh" */
#include "ResampleCmd.hh"

#if 0
#include "fradcdata.hh"
#include "frprocdata.hh"
#endif /* 0 */

using FrameCPP::FrAdcData;
using FrameCPP::FrProcData;
using FrameCPP::FrVect;
using std::string;

typedef boost::shared_ptr< FrameCPP::FrVect > fr_vect_data_type;
typedef REAL_8                                sample_rate_type;

//-----------------------------------------------------------------------
/// \brief Registry of Filter::resampleBase objects
//-----------------------------------------------------------------------

namespace
{
    template < class TIn >
    inline void
    FrVectResample( fr_vect_data_type         Source,
                    Filters::Resample< TIn >& r,
                    sample_rate_type&         NewSampleRate,
                    fr_vect_data_type&        Dest )
    {
        typedef FrameCPP::FrVect::data_type::element_type        element_type;
        typedef typename Filters::ResampleTraits< TIn >::OutType TOut;

        const FrameCPP::FrVect::type_type fr_vect_data_type =
            FrameCPP::FrVect::GetDataType< TOut >( );
        const int p = r.getP( );
        const int q = r.getQ( );

        const INT_4U                nData = Source->GetNData( );
        FrameCPP::FrVect::data_type expanded;
        const TIn* const in = (TIn*)( Source->GetDataUncompressed( expanded ) );

        const INT_4U nDataOut( ( p * nData ) / q );
        const INT_4U nBytesOut(
            nDataOut * FrameCPP::FrVect::GetTypeSize( fr_vect_data_type ) );

        const FrameCPP::Dimension& dims = Source->GetDim( 0 );
        const FrameCPP::Dimension  newDims( nDataOut,
                                           ( dims.GetDx( ) * q ) / p,
                                           dims.GetUnitX( ),
                                           dims.GetStartX( ) );

        NewSampleRate = dims.GetDx( );
        if ( NewSampleRate <= sample_rate_type( 0.0 ) )
        {
            string msg( "Negative or null Dimension(0)::dx for FrVect (" );
            msg += Source->GetName( );
            msg += ")";
            throw SWIGEXCEPTION( msg );
        }
        NewSampleRate = ( sample_rate_type( 1.0 ) / NewSampleRate );
        NewSampleRate = ( p * NewSampleRate ) / q;

        // Create a C-style-array-like object
        Filters::basic_array< TOut > out( nDataOut );

        // Apply resampling. Note that the basic_array class takes care of
        // deleting memory in case of an exception thrown by apply()
        r.apply( out, in, nData );

        FrameCPP::FrVect::data_type out_data(
            reinterpret_cast< element_type* >( out.release( ) ) );

        Dest.reset( new FrameCPP::FrVect( Source->GetName( ),
                                          FrameCPP::FrVect::RAW,
                                          fr_vect_data_type,
                                          1,
                                          &newDims,
                                          nDataOut,
                                          nBytesOut,
                                          out_data,
                                          Source->GetUnitY( ) ) );
    }

    class resampleBaseRegistry
        : public LDASTools::AL::Singleton< resampleBaseRegistry >
    {
    public:
        typedef Filters::ResampleBase element_type;

        resampleBaseRegistry( );

        ~resampleBaseRegistry( );

        static bool IsRegistered( const element_type* const Element );

        static void RegisterObject( element_type* Element );

        static void RemoveObject( element_type* Element );

    private:
        typedef ObjectRegistry< element_type > registry_type;

        std::unique_ptr< registry_type > registry;
    };
} // namespace

namespace LDASTools
{
    namespace AL
    {
    } // namespace AL
} // namespace LDASTools

// using LDASTools::AL::resampleBaseRegistry;

//! exc: SwigException
Filters::ResampleBase*
createResampleState( const int q, const FrAdcData* const adc )
{
    if ( !adc )
    {
        throw SWIGEXCEPTION( "bad adc data pointer" );
    }

    if ( !adc->RefData( )[ 0 ] )
    {
        throw SWIGEXCEPTION( "adc has no data" );
    }

    if ( !( ( q == 2 ) || ( q == 4 ) || ( q == 8 ) || ( q == 16 ) ) )
    {
        std::ostringstream err;

        err << "q (" << q << ") must be one of 2, 4, 8, or 16";
        throw SWIGEXCEPTION( err.str( ) );
    }

    const int p = 1;

    Filters::ResampleBase* r = 0;
    switch ( adc->RefData( )[ 0 ]->GetType( ) )
    {
    case FrVect::FR_VECT_2S:
        r = new Filters::Resample< INT_2S >( p, q );
        break;
    case FrVect::FR_VECT_4S:
        r = new Filters::Resample< INT_4S >( p, q );
        break;
    case FrVect::FR_VECT_4R:
        r = new Filters::Resample< REAL_4 >( p, q );
        break;
    case FrVect::FR_VECT_8R:
        r = new Filters::Resample< REAL_8 >( p, q );
        break;
    case FrVect::FR_VECT_8C:
        r = new Filters::Resample< COMPLEX_8 >( p, q );
        break;
    case FrVect::FR_VECT_16C:
        r = new Filters::Resample< COMPLEX_16 >( p, q );
        break;
    default:
        throw SWIGEXCEPTION( "unsupported data type" );
    }

    resampleBaseRegistry::RegisterObject( r );

    return r;
}

//! exc: SwigException
Filters::ResampleBase*
createResampleState( const int q, const FrProcData* const proc )
{
    if ( !proc )
    {
        throw SWIGEXCEPTION( "bad FrProc data pointer" );
    }

    if ( !proc->RefData( )[ 0 ] )
    {
        throw SWIGEXCEPTION( "FrProc has no data" );
    }

    if ( !( ( q == 2 ) || ( q == 4 ) || ( q == 8 ) || ( q == 16 ) ) )
    {
        throw SWIGEXCEPTION( "q must be one of 2, 4, 8, 16" );
    }

    const int p = 1;

    Filters::ResampleBase* r = 0;
    switch ( proc->RefData( )[ 0 ]->GetType( ) )
    {
    case FrVect::FR_VECT_2S:
        r = new Filters::Resample< INT_2S >( p, q );
        break;
    case FrVect::FR_VECT_4S:
        r = new Filters::Resample< INT_4S >( p, q );
        break;
    case FrVect::FR_VECT_4R:
        r = new Filters::Resample< REAL_4 >( p, q );
        break;
    case FrVect::FR_VECT_8R:
        r = new Filters::Resample< REAL_8 >( p, q );
        break;
    case FrVect::FR_VECT_8C:
        r = new Filters::Resample< COMPLEX_8 >( p, q );
        break;
    case FrVect::FR_VECT_16C:
        r = new Filters::Resample< COMPLEX_16 >( p, q );
        break;
    default:
        throw SWIGEXCEPTION( "unsupported data type" );
    }

    resampleBaseRegistry::RegisterObject( r );

    return r;
}

//! exc: SwigException
void
destructResampleState( Filters::ResampleBase* const r )
{
    if ( !resampleBaseRegistry::IsRegistered( r ) )
    {
        throw SWIGEXCEPTION( "invalid_resample_state" );
    }

    resampleBaseRegistry::RemoveObject( r );

    delete r;
}

//! exc: std::invalid_argument
//! exc: std::range_error
//! exc: std::bad_alloc
template < class TIn >
FrameCPP::FrProcData*
resampleAdcData( const FrameCPP::FrAdcData& adc, Filters::Resample< TIn >& r )
{
    sample_rate_type newSampleRate( 0 );

    FrameCPP::FrAdcData::data_type::value_type  source( adc.RefData( )[ 0 ] );
    FrameCPP::FrProcData::data_type::value_type dest;

    FrVectResample( source, r, newSampleRate, dest );

    FrProcData* const newProc(
        new FrProcData( adc.GetName( ),
                        adc.GetComment( ),
                        FrProcData::TIME_SERIES,
                        FrProcData::UNKNOWN_SUB_TYPE,
                        adc.GetTimeOffset( ),
                        dest->GetNData( ) / newSampleRate, /* TRange */
                        adc.GetFShift( ),
                        adc.GetPhase( ),
                        0, /* FRange */
                        0 /* BW */ ) );

    newProc->RefData( ).append( dest );

    std::ostringstream oss;

    // Used to be a name
    oss << "resample(" << adc.GetName( ) << "," << r.getP( ) << "," << r.getQ( )
        << ")";

    // oss.str("");
    // oss << "Resampled by " << p << "/" << q << " in the frameAPI";
    newProc->AppendComment( oss.str( ) );

    // Append aux vectors
    FrAdcData::const_iterator                   iter = adc.RefAux( ).begin( );
    FrameCPP::FrProcData::data_type::value_type data;
    while ( iter != adc.RefAux( ).end( ) )
    {
        data.reset( new FrameCPP::FrVect( **iter ) );
        newProc->RefAux( ).append( data );
        ++iter;
    }

    return newProc;
}

//! exc: SwigException
FrameCPP::FrProcData*
resampleAdcData( const FrAdcData* const adc, Filters::ResampleBase* const r )
{
    if ( !resampleBaseRegistry::IsRegistered( r ) )
    {
        throw SWIGEXCEPTION( "invalid_resample_state" );
    }

    if ( adc == 0 )
    {
        throw SWIGEXCEPTION( "bad adc data pointer" );
    }

    if ( !adc->RefData( )[ 0 ] )
    {
        throw SWIGEXCEPTION( "adc has no data" );
    }

    if ( adc->RefData( ).size( ) > 1 )
    {
        throw SWIGEXCEPTION( "can't resample multi-dimensional adc data" );
    }

    switch ( adc->RefData( )[ 0 ]->GetType( ) )
    {
    case FrVect::FR_VECT_2S:
        return resampleAdcData< INT_2S >(
            *adc, dynamic_cast< Filters::Resample< INT_2S >& >( *r ) );
        break;
    case FrVect::FR_VECT_4S:
        return resampleAdcData< INT_4S >(
            *adc, dynamic_cast< Filters::Resample< INT_4S >& >( *r ) );
        break;
    case FrVect::FR_VECT_4R:
        return resampleAdcData< REAL_4 >(
            *adc, dynamic_cast< Filters::Resample< REAL_4 >& >( *r ) );
        break;
    case FrVect::FR_VECT_8R:
        return resampleAdcData< REAL_8 >(
            *adc, dynamic_cast< Filters::Resample< REAL_8 >& >( *r ) );
        break;
    case FrVect::FR_VECT_8C:
        return resampleAdcData< COMPLEX_8 >(
            *adc, dynamic_cast< Filters::Resample< COMPLEX_8 >& >( *r ) );
        break;
    case FrVect::FR_VECT_16C:
        return resampleAdcData< COMPLEX_16 >(
            *adc, dynamic_cast< Filters::Resample< COMPLEX_16 >& >( *r ) );
        break;
    default:
        throw SWIGEXCEPTION( "unsupported data type" );
        break;
    }
}

//! exc: std::invalid_argument
//! exc: std::range_error
//! exc: std::bad_alloc
template < class TIn >
FrameCPP::FrProcData*
resampleProcData( const FrameCPP::FrProcData& proc,
                  Filters::Resample< TIn >&   r )
{
    sample_rate_type newSampleRate( 0 );

    FrameCPP::FrProcData::data_type::value_type source( proc.RefData( )[ 0 ] );
    FrameCPP::FrProcData::data_type::value_type dest;

    FrVectResample( source, r, newSampleRate, dest );

    std::unique_ptr< FrProcData > newProc(
        new FrProcData( proc.GetName( ),
                        proc.GetComment( ),
                        FrProcData::TIME_SERIES,
                        FrProcData::UNKNOWN_SUB_TYPE,
                        proc.GetTimeOffset( ),
                        dest->GetNData( ) / newSampleRate, /* TRange */
                        proc.GetFShift( ),
                        proc.GetPhase( ),
                        proc.GetFRange( ),
                        proc.GetBW( ) ) );

    newProc->RefData( ).append( dest );

    std::ostringstream oss;

    // Used to be a name
    oss << "resample(" << proc.GetName( ) << "," << r.getP( ) << ","
        << r.getQ( ) << ")";

    // oss.str("");
    // oss << "Resampled by " << p << "/" << q << " in the frameAPI";
    newProc->AppendComment( oss.str( ) );

    // Append aux vectors
    FrameCPP::FrProcData::aux_type::value_type aux_data;
    for ( FrProcData::const_iterator iter = proc.RefAux( ).begin( ),
                                     end_iter = proc.RefAux( ).end( );
          iter != end_iter;
          ++iter )
    {
        aux_data.reset( new FrameCPP::FrVect( **iter ) );
        newProc->RefAux( ).append( aux_data );
    }

    return newProc.release( );
}

//! exc: SwigException
FrameCPP::FrProcData*
resampleProcData( const FrProcData* const proc, Filters::ResampleBase* const r )
{
    if ( !resampleBaseRegistry::IsRegistered( r ) )
    {
        throw SWIGEXCEPTION( "invalid_resample_state" );
    }

    if ( proc == 0 )
    {
        throw SWIGEXCEPTION( "bad FrProc data pointer" );
    }

    if ( !proc->RefData( )[ 0 ] )
    {
        throw SWIGEXCEPTION( "FrProc has no data" );
    }

    if ( proc->RefData( ).size( ) > 1 )
    {
        throw SWIGEXCEPTION( "can't resample multi-dimensional FrProc data" );
    }

    switch ( proc->RefData( )[ 0 ]->GetType( ) )
    {
    case FrVect::FR_VECT_2S:
        return resampleProcData< INT_2S >(
            *proc, dynamic_cast< Filters::Resample< INT_2S >& >( *r ) );
        break;
    case FrVect::FR_VECT_4S:
        return resampleProcData< INT_4S >(
            *proc, dynamic_cast< Filters::Resample< INT_4S >& >( *r ) );
        break;
    case FrVect::FR_VECT_4R:
        return resampleProcData< REAL_4 >(
            *proc, dynamic_cast< Filters::Resample< REAL_4 >& >( *r ) );
        break;
    case FrVect::FR_VECT_8R:
        return resampleProcData< REAL_8 >(
            *proc, dynamic_cast< Filters::Resample< REAL_8 >& >( *r ) );
        break;
    case FrVect::FR_VECT_8C:
        return resampleProcData< COMPLEX_8 >(
            *proc, dynamic_cast< Filters::Resample< COMPLEX_8 >& >( *r ) );
        break;
    case FrVect::FR_VECT_16C:
        return resampleProcData< COMPLEX_16 >(
            *proc, dynamic_cast< Filters::Resample< COMPLEX_16 >& >( *r ) );
        break;
    default:
        throw SWIGEXCEPTION( "unsupported data type" );
        break;
    }
}

//! exc: SwigException
REAL_8
resampleDelay( const FrameCPP::FrProcData* const  proc,
               const Filters::ResampleBase* const r )
{
    if ( !proc )
    {
        throw SWIGEXCEPTION( "bad proc data pointer" );
    }

    if ( !resampleBaseRegistry::IsRegistered( r ) )
    {
        throw SWIGEXCEPTION( "invalid_resample_state" );
    }

    return r->getDelay( ) / FrameAPI::SampleRate( *proc );
}

SINGLETON_INSTANCE_DEFINITION( SingletonHolder< ::resampleBaseRegistry > )

namespace
{
    resampleBaseRegistry::resampleBaseRegistry( )
        : registry( new registry_type )
    {
    }

    resampleBaseRegistry::~resampleBaseRegistry( )
    {
    }

    bool
    resampleBaseRegistry::IsRegistered( const element_type* const Element )
    {
        return Instance( ).registry->isRegistered( Element );
    }

    void
    resampleBaseRegistry::RegisterObject( element_type* Element )
    {
        Instance( ).registry->registerObject( Element );
    }

    void
    resampleBaseRegistry::RemoveObject( element_type* Element )
    {
        Instance( ).registry->removeObject( Element );
    }
} // namespace
