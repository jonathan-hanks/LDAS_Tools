//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <boost/filesystem.hpp>

#include "framecpp/Common/MD5Sum.hh"

#include "genericAPI/Logging.hh"

#include "DeviceIOConfiguration.hh"
#include "RDSStreamFile.hh"
#include "util.hh"

using FrameAPI::DeviceIOConfiguration;
using FrameAPI::LogMD5Sum;
using FrameCPP::Common::MD5Sum;

RDSStreamFile::RDSStreamFile( const std::string&         OutputDirectory,
                              const std::string&         OutputType,
                              const std::string&         MD5SumOutputDirectory,
                              const frames_per_file_type FramesPerFile )
    : m_md5sum_output_directory( MD5SumOutputDirectory ),
      frames_per_file( FramesPerFile ), m_output_directory( OutputDirectory ),
      m_output_type( OutputType ), m_rds_level( 0 )
{
    static const char* caller = "RDSStreamFile::RDSStreamFile";

    QUEUE_LOG_MESSAGE( "ENTRY:"
                           << " OutputDirectory: " << OutputDirectory
                           << " OutputType: " << OutputType
                           << " MD5SumOutputDirectory: "
                           << MD5SumOutputDirectory,
                       MT_DEBUG,
                       50,
                       caller,
                       "RDS" );
    QUEUE_LOG_MESSAGE( "EXIT", MT_DEBUG, 50, caller, "RDS" );
}

RDSStreamFile::~RDSStreamFile( )
{
    Abandon( );
}

void
RDSStreamFile::Abandon( )
{
    std::string tmpfile( ( fileWriter( ) ) ? fileWriter( )->GetFilename( )
                                           : std::string( "<no file>" ) );

    fileWriter( (file_writer_type::element_type*)NULL ); // Free up resourses
    boost::filesystem::remove( tmpfile ); // Remove file
}

void
RDSStreamFile::Close( bool Final )
{
    static const char* caller = "RDSStreamFile::Close";

    QUEUE_LOG_MESSAGE( "ENTRY:"
                           << " Final: " << Final,
                       MT_DEBUG,
                       50,
                       caller,
                       "RDS" );
    closeFrameFile( ( fileWriter( )
                          ? generateOutputName( fileWriter( )->StartTime( ),
                                                fileWriter( )->Dt( ) )
                          : "" ),
                    Final );
    QUEUE_LOG_MESSAGE( "EXIT", MT_DEBUG, 50, caller, "RDS" );
}

bool
RDSStreamFile::Next( const LDASTools::AL::GPSTime& FrameStart,
                     const REAL_8                  Dt,
                     INT_4U&                       FramesPerStream )
{
    static const char* caller = "RDSStreamFile::Next";
    bool               retval = false;

    QUEUE_LOG_MESSAGE( "ENTRY"
                           << " FrameStart: " << FrameStart
                           << " [ m_user_start: " << m_user_start
                           << " m_user_stop: " << m_user_stop << " ]"
                           << " Dt: " << Dt,
                       MT_DEBUG,
                       50,
                       caller,
                       "RDS" );

    if ( ( !fileWriter( ) ) && ( FrameStart >= m_user_start ) &&
         ( ( FrameStart + Dt ) <= m_user_stop ) )
    {
        openFrameFile( generateTmpOutputName( FrameStart.GetSeconds( ) ) );
        const INT_4U max( INT_4U(
            ( m_user_stop.GetSeconds( ) - FrameStart.GetSeconds( ) ) / Dt ) );
        if ( FramesPerStream > max )
        {
            FramesPerStream = max;
        }
        retval = true;
    }
    else
    {
        FramesPerStream = 1;
    }
    QUEUE_LOG_MESSAGE( "EXIT", MT_DEBUG, 50, caller, "RDS" );
    return retval;
}

void
RDSStreamFile::Write( frame_h_type            Frame,
                      compression_scheme_type CompressionScheme,
                      compression_level_type  CompressionLevel,
                      chkSum_type             CheckSum )
{
    static const char* caller = "RDSStreamFile::Write";

    QUEUE_LOG_MESSAGE(
        "ENTRY"
        " Frame::GTime: "
            << ( Frame ? Frame->GetGTime( ) : LDASTools::AL::GPSTime( ) )
            << " m_user_start: " << m_user_start
            << " m_user_stop: " << m_user_stop,
        MT_DEBUG,
        50,
        caller,
        "RDS" );
    if ( Frame )
    {
        if ( ( Frame->GetGTime( ) >= m_user_start ) &&
             ( Frame->GetGTime( ) < m_user_stop ) )
        {
            if ( !fileWriter( ) )
            {
                openFrameFile(
                    generateTmpOutputName( Frame->GetGTime( ).GetSeconds( ) ) );
                QUEUE_LOG_MESSAGE(
                    "Opened frame file", MT_DEBUG, 50, caller, "RDS" );
            }
        }
    }

    if ( fileWriter( ) )
    {
        //-----------------------------------------------------------------
        // Write Frame to the frame file
        //-----------------------------------------------------------------
        fileWriter( )->WriteFrame(
            Frame, CompressionScheme, CompressionLevel, CheckSum );
        //-----------------------------------------------------------------
        // Check to see if it was the last frame to be written to the file.
        //-----------------------------------------------------------------
        QUEUE_LOG_MESSAGE( "Frame: " << fileWriter( )->FrameCount( )
                                     << " of: " << frames_per_file,
                           MT_DEBUG,
                           50,
                           caller,
                           "RDS" );
        if ( fileWriter( )->FrameCount( ) == frames_per_file )
        {
            Close( false );
        }
    }
    QUEUE_LOG_MESSAGE( "EXIT", MT_DEBUG, 50, caller, "RDS" );
}

void
RDSStreamFile::appendOutputFrameFilename( const std::string& Filename )
{
    m_output_frame_names.push_back( Filename );
}

void
RDSStreamFile::closeFrameFile( const std::string& Filename, bool Final )
{
    static const char* caller = "RDSStreamFile::closeFrameFile";

    QUEUE_LOG_MESSAGE( //
        "ENTRY:" //
            << " Filename: " << Filename //
            << " Final: " << Final //
        ,
        MT_DEBUG,
        50,
        caller,
        "RDS" );
    closeFrameWriter( fileWriter( ), Filename );
    fileWriter( (file_writer_type::element_type*)NULL ); // Free up resourses
    QUEUE_LOG_MESSAGE( "EXIT:", MT_DEBUG, 50, caller, "RDS" );
}

void
RDSStreamFile::closeFrameWriter( file_writer_type   Writer,
                                 const std::string& Filename )
{
    static const char* caller = "RDSStreamFile::closeFrameWriter";

    QUEUE_LOG_MESSAGE( "ENTRY:"
                           << " Writer: " << Writer
                           << " Filename: " << Filename,
                       MT_DEBUG,
                       50,
                       caller,
                       "RDS" );
    if ( Writer )
    {
        std::string filename(
            generateOutputName( Writer->StartTime( ), Writer->Dt( ) ) );

        QUEUE_LOG_MESSAGE( "Writer is open", MT_DEBUG, 50, caller, "RDS" );
        //-------------------------------------------------------------------
        // Close output frame file, renaming it in accordance to
        //   user's request
        //-------------------------------------------------------------------
        Writer->Close( );

        std::string tmpfile( Writer->GetFilename( ) );
        bool        calculating_md5_sum = true;
        MD5Sum      md5;

        try
        {
            md5 = Writer->GetMD5Sum( );
            md5.Finalize( );
        }
        catch ( ... )
        {
            calculating_md5_sum = false;
        }

        try
        {
            ensureNoSuchFile( filename );
        }
        catch ( ... )
        {
            boost::filesystem::remove( tmpfile ); // Remove the tempory file
            throw;
        }
        QUEUE_LOG_MESSAGE( "About to rename"
                               << " tmpfile: " << tmpfile
                               << " filename: " << filename,
                           MT_DEBUG,
                           30,
                           caller,
                           "RDS" );
        try
        {
            boost::filesystem::rename( tmpfile, filename );
        }
        catch ( boost::filesystem::filesystem_error& Error )
        {
            //----------------------------------------------------------------
            // Cannot rename the file. Must remove the temporary file
            //   and inform the call about the error.
            //----------------------------------------------------------------
            boost::filesystem::remove( tmpfile ); // Remove the tempory file
            std::ostringstream oss;

            oss << "Unable to create: " << filename << " src: " << tmpfile
                << " error: " << Error.what( );
            throw std::runtime_error( oss.str( ) );
        }
        if ( !filename.empty( ) &&
             ( ( fileWriter( )->StartTime( ) == 0 ) ||
               ( fileWriter( )->Dt( ) == 0 ) ) )
        {
            //---------------------------------------------------------------
            // Remove empty files
            //---------------------------------------------------------------
            QUEUE_LOG_MESSAGE( //
                "Removing empty file:" //
                    << " Filename: " << Filename //
                ,
                MT_DEBUG,
                50,
                caller,
                "RDS" );
            boost::filesystem::remove( filename );
        }
        else
        {
            if ( calculating_md5_sum )
            {
                LogMD5Sum( filename, md5, m_md5sum_output_directory );
            }
            appendOutputFrameFilename( filename );
        }
    } // if ( Writer )
    else
    {
        QUEUE_LOG_MESSAGE( "Writer is NOT open", MT_DEBUG, 50, caller, "RDS" );
    }
    QUEUE_LOG_MESSAGE( "EXIT", MT_DEBUG, 50, caller, "RDS" );
}

void
RDSStreamFile::ensureNoSuchFile( const std::string& Filename ) const
{
    std::ifstream test_file;

    try
    {
        test_file.open( Filename.c_str( ) );
    }
    catch ( ... )
    {
        // File does not exist == continue
        return;
    }
    if ( test_file.is_open( ) )
    {
        test_file.close( );
    }
    else
    {
        return;
    }
    std::ostringstream err;
    err << "Frame file already exists: " << Filename;
    throw SWIGEXCEPTION( err.str( ) );
}

std::string
RDSStreamFile::generateOutputName( INT_4U FileStart, REAL_8 FileDeltaT ) const
{
    //--------------------------------------------------------------------
    // Generate an output file
    //--------------------------------------------------------------------
    std::ostringstream oss;
    oss << m_output_directory << "/" << IFOList( ) << "-" << m_output_type
        << "-" << FileStart << "-" << FileDeltaT << ".gwf";
    return oss.str( );
}

std::string
RDSStreamFile::generateTmpOutputName( const INT_4U FrameStart ) const
{
    //--------------------------------------------------------------------
    // Generate the tmp output file name
    //--------------------------------------------------------------------
    std::ostringstream oss;
    oss << m_output_directory << "/rds" << FrameStart << "_" << getpid( ) << "_"
        << pthread_self( ) << ".gwf.tmp";
    return oss.str( );
}

void
RDSStreamFile::openFrameFile( const std::string& Filename )
{
    static const char* caller = "RDSStreamFile::openFrameFile";
    unsigned int       buffer_size;
    bool               enable_memory_mapped_io;

    QUEUE_LOG_MESSAGE( "ENTRY:"
                           << " Filename: " << Filename,
                       MT_DEBUG,
                       50,
                       caller,
                       "RDS" );
    DeviceIOConfiguration::GetConfiguration(
        Filename.c_str( ), buffer_size, enable_memory_mapped_io );

    fileWriter( new FileWriter( Filename.c_str( ),
                                buffer_size,
                                (char*)NULL,
                                enable_memory_mapped_io ) );
    QUEUE_LOG_MESSAGE(
        "Allocated FileWrite: " << ( ( fileWriter( ) ) ? "Yes" : "No" ),
        MT_DEBUG,
        50,
        caller,
        "RDS" );
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    const std::string& dir( m_md5sum_output_directory );

    if ( ::access( dir.c_str( ), R_OK ) == 0 )
    {
        //------------------------------------------------------------------
        // Setup to calculate the MD5Sum
        //------------------------------------------------------------------
        fileWriter( )->ResetMD5Sum( true );
    }
    else
    {
        fileWriter( )->ResetMD5Sum( false );
    }
    QUEUE_LOG_MESSAGE( "EXIT", MT_DEBUG, 50, caller, "RDS" );
}
