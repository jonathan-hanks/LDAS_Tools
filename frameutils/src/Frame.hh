//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__FRAME_HH
#define FRAME_API__FRAME_HH

#include <stdexcept>

#include <boost/shared_ptr.hpp>

#include "framecpp/OFrameStream.hh"

namespace FrameAPI
{
    class Channel;

    class Frame : public ::FrameCPP::OFrameFStream::frameh_type
    {
    public:
        //===================================================================

        class NoChannelFound : public std::runtime_error
        {
        public:
            NoChannelFound( const std::string& ChannelName );

        private:
            static std::string message( const std::string& ChannelName );
        };

        //===================================================================

        typedef ::FrameCPP::OFrameFStream::frameh_type frameh_type;
        typedef boost::shared_ptr< Channel >           channel_type;

        frameh_type operator=( frameh_type RHS );

        channel_type GetChannel( const std::string& ChannelName ) const;

        ::FrameCPP::OFrameFStream::frameh_type AsFrameH( );

    protected:
    private:
        typedef LDASTools::AL::unordered_map< std::string, channel_type >
            channel_container_type;

        mutable channel_container_type m_channels;
    };

    inline ::FrameCPP::OFrameFStream::frameh_type
    Frame::AsFrameH( )
    {
        return *this;
    }
} // namespace FrameAPI

#endif /* FRAME_API__FRAME_HH */
