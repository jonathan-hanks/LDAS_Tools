//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

#include <sstream>

#include "SAMFSLoader.hh"

using LDASTools::AL::SOLoader;

static const char* path_libsam = "/opt/SUNWsamfs/lib";
static const char* name_libsam = "libsam.so";

namespace FrameAPI
{
    //---------------------------------------------------------------------
    // Definition of class methods
    //---------------------------------------------------------------------
    SAMFSLoader::SAMFSLoader( )
        : SOLoader( path_libsam, name_libsam, SOLoader::SOLOADER_OPTIONAL )
    {
        //-------------------------------------------------------------------
        // Load functions that need to be called from the frameAPI.
        //-------------------------------------------------------------------
        m_func_stage =
            reinterpret_cast< stage_func_type >( GetFunction( "sam_stage" ) );
    }
} // namespace FrameAPI
