//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

// System Header Files
#include <algorithm>
#include <cmath>
#include <sstream>

#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/unordered_map.hh"

#include "framecpp/FrameCPP.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrDetector.hh"
#include "framecpp/FrHistory.hh"

// Local Header Files
#include "FrameH.hh"
#include "FrRawData.hh"
#include "util.hh"

using LDASTools::AL::unordered_map;

using LDASTools::AL::GPSTime;

using FrameCPP::FrameH;
using FrameCPP::FrDetector;
using FrameCPP::FrHistory;

using namespace std;
//! ignore_begin:

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

namespace
{
    //-------------------------------------------------------------------
    // Local constant values
    //-------------------------------------------------------------------
    static const INT_4S MAX_RUN = 0x7FFFFFFF;
    //:TODO: DETECTOR_PREFIX_LENGTH needs to be part of FrDetector
    static const INT_2U DETECTOR_PREFIX_LENGTH = 2;

    //-------------------------------------------------------------------
    // Routine to add the run number into the history record
    //-------------------------------------------------------------------
    void
    add_run( FrameCPP::FrameH& Primary, const FrameCPP::FrameH& Secondary )
    {
        if ( Primary.GetRun( ) != MAX_RUN )
        {
            //-----------------------------------------------------------
            // Add record for primary run number
            //-----------------------------------------------------------
            for ( FrameH::detectProc_type::const_iterator
                      cur = Primary.RefDetectProc( ).begin( ),
                      last = Primary.RefDetectProc( ).end( );
                  cur != last;
                  ++cur )
            {
                std::ostringstream comment;
                const char*        prefix( ( *cur )->GetPrefix( ) );

                for ( INT_2U x = 0; x < DETECTOR_PREFIX_LENGTH; ++x, ++prefix )
                {
                    if ( *prefix )
                    {
                        comment << (char)( *prefix );
                    }
                }
                comment << ":" << Primary.GetRun( );

                FrameCPP::FrameH::history_type::value_type history(
                    new FrHistory( "run",
                                   GPSTime::NowGPSTime( ).GetSeconds( ),
                                   comment.str( ) ) );
                Primary.RefHistory( ).append( history );
            }
            Primary.SetRun( MAX_RUN );
        }
        if ( Secondary.GetRun( ) == MAX_RUN )
        {
            // Merging will happen when the history records are merged together
            return;
        }
        //---------------------------------------------------------------
        // Create a hash of known run info
        //---------------------------------------------------------------
        typedef LDASTools::AL::unordered_map< std::string, INT_4S >
                      run_info_type;
        run_info_type run_info;

        for ( FrameH::history_type::const_iterator
                  cur = Primary.RefHistory( ).begin( ),
                  last = Primary.RefHistory( ).end( );
              cur != last;
              ++cur )
        {
            if ( ( *cur )->GetName( ) == "run" )
            {
                INT_4S                 run;
                std::string            ifo;
                std::string::size_type colon_pos =
                    ( *cur )->GetComment( ).find_first_of( ":" );

                if ( colon_pos == std::string::npos )
                {
                    //---------------------------------------------------
                    // Malformed run number
                    //---------------------------------------------------
                    continue;
                }
                std::istringstream ifo_str(
                    ( *cur )->GetComment( ).substr( 0, colon_pos ) );
                ifo_str >> ifo;

                std::istringstream run_str(
                    ( *cur )->GetComment( ).substr( colon_pos + 1 ) );
                run_str >> run;
                run_info[ ifo ] = run;
            }
        }
        //---------------------------------------------------------------
        // Loop over list of detectors
        //---------------------------------------------------------------
        for ( FrameH::detectProc_type::const_iterator
                  dcur = Secondary.RefDetectProc( ).begin( ),
                  dlast = Secondary.RefDetectProc( ).end( );
              dcur != dlast;
              ++dcur )
        {
            const char*        prefix( ( *dcur )->GetPrefix( ) );
            std::ostringstream p;
            for ( INT_2U x = 0; x < DETECTOR_PREFIX_LENGTH; ++x, ++prefix )
            {
                if ( *prefix )
                {
                    p << (char)( *prefix );
                }
            }

            std::ostringstream            comment;
            run_info_type::const_iterator pos( run_info.find( p.str( ) ) );

            if ( pos == run_info.end( ) )
            {
                comment << p.str( ) << ":" << Secondary.GetRun( );

                FrameCPP::FrameH::history_type::value_type history(
                    new FrHistory( "run",
                                   GPSTime::NowGPSTime( ).GetSeconds( ),
                                   comment.str( ) ) );
                Primary.RefHistory( ).append( history );
                run_info[ p.str( ) ] = Secondary.GetRun( );
            }
            else
            {
                if ( Secondary.GetRun( ) != pos->second )
                {
                    std::ostringstream msg;

                    msg << "Run numbers differ for ifo: " << pos->first << " "
                        << Secondary.GetRun( ) << " vs. " << pos->second;
                    throw std::runtime_error( msg.str( ) );
                }
            }
        }
    }

} // namespace

namespace FrameAPI
{
    namespace FrameH
    {
        void
        merge( FrameCPP::FrameH& Primary, const FrameCPP::FrameH& Secondary )
        {
#if 0 /* This is currently commented out because it breaks backwards           \
         compatability */
	    //---------------------------------------------------------------
	    // Sanity Checks
	    //   Only allow merging like FrameH data
	    //---------------------------------------------------------------
	    if ( ( Primary.GetGTime( ) != Secondary.GetGTime( ) )
		 || ( Primary.GetDt( ) != Secondary.GetDt( ) ) )
	    {
		std::ostringstream	msg;

		msg << "Cannot merge FrameH structures of different times"
		    << " ( Primary: GTime: " << Primary.GetGTime( )
		    << " Dt: " << Primary.GetDt( )
		    << " vs. Secondary: GTime: "
		    << Secondary.GetGTime( ) << " )"
		    << " Dt: " << Secondary.GetDt( )
		    << " )";
		throw SWIGEXCEPTION( msg.str( ) );
	    }
#endif /* 0 */
            //---------------------------------------------------------------
            // Merge run number
            //---------------------------------------------------------------
            if ( Primary.GetRun( ) != Secondary.GetRun( ) )
            {
                add_run( Primary, Secondary );
            }
            //---------------------------------------------------------------
            // Merge DataQuality info
            //---------------------------------------------------------------
            Primary.SetDataQuality( Primary.GetDataQuality( ) |
                                    Secondary.GetDataQuality( ) );
            //---------------------------------------------------------------
            // Merge FrRawData info
            //---------------------------------------------------------------
            FrameCPP::FrameH::rawData_type::element_type* rd(
                FrameAPI::FrRawData::merge( Primary.GetRawData( ).get( ),
                                            Secondary.GetRawData( ).get( ) ) );
            if ( rd != Primary.GetRawData( ).get( ) )
            {
                FrameCPP::FrameH::rawData_type d( rd );
                Primary.SetRawData( d );
            }
            //---------------------------------------------------------------
            // Merge auxData - High resolution of dataQuality
            //---------------------------------------------------------------
            Primary.RefAuxData( ).Merge( Secondary.RefAuxData( ) );
            //---------------------------------------------------------------
            // Merge auxTable
            //---------------------------------------------------------------
            Primary.RefAuxTable( ).Merge( Secondary.RefAuxTable( ) );
            //---------------------------------------------------------------
            // Sim Detectors
            //---------------------------------------------------------------
            for ( FrameCPP::FrameH::detectSim_type::const_iterator
                      current( Secondary.RefDetectSim( ).begin( ) ),
                  end( Secondary.RefDetectSim( ).end( ) );
                  current != end;
                  current++ )
            {
                bool duplicate( false );

                for ( FrameCPP::FrameH::detectSim_type::const_iterator
                          pcurrent( Primary.RefDetectSim( ).begin( ) ),
                      pend( Primary.RefDetectSim( ).end( ) );
                      pcurrent != pend;
                      pcurrent++ )
                {
                    if ( ( *current )->GetName( ) == ( *pcurrent )->GetName( ) )
                    {
                        duplicate = true;
                        break;
                    }
                }
                if ( !duplicate )
                {
                    FrameCPP::FrameH::detectSim_type::value_type detector(
                        new FrDetector( **current ) );
                    Primary.RefDetectSim( ).append( detector );
                }
            }
            //---------------------------------------------------------------
            // Detectors
            //---------------------------------------------------------------
            for ( FrameCPP::FrameH::detectProc_type::const_iterator
                      current( Secondary.RefDetectProc( ).begin( ) ),
                  end( Secondary.RefDetectProc( ).end( ) );
                  current != end;
                  current++ )
            {
                bool duplicate( false );

                for ( FrameCPP::FrameH::detectProc_type::const_iterator
                          pcurrent( Primary.RefDetectProc( ).begin( ) ),
                      pend( Primary.RefDetectProc( ).end( ) );
                      pcurrent != pend;
                      pcurrent++ )
                {
                    if ( ( *current )
                             ->GetName( )
                             .compare( ( *pcurrent )->GetName( ) ) == 0 )
                    {
                        duplicate = true;
                        break;
                    }
                }
                if ( !duplicate )
                {
                    FrameCPP::FrameH::detectProc_type::value_type detector(
                        new FrDetector( **current ) );
                    Primary.RefDetectProc( ).append( detector );
                }
            }
            //---------------------------------------------------------------
            // History
            //---------------------------------------------------------------
            for ( FrameCPP::FrameH::history_type::const_iterator
                      current( Secondary.RefHistory( ).begin( ) ),
                  end( Secondary.RefHistory( ).end( ) );
                  current != end;
                  current++ )
            {
                FrameCPP::FrameH::history_type::value_type history(
                    new FrHistory( **current ) );

                Primary.RefHistory( ).append( history );
            }
        }
    } // namespace FrameH
} // namespace FrameAPI
