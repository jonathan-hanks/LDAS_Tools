//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef RDS_REDUCE_FRAME_HH
#define RDS_REDUCE_FRAME_HH

#include "frameAPI/rdsframe.hh"

//-----------------------------------------------------------------------------
//
//: Functional to create RDS frames.
//
class ReduceRawFrame : private RDSFrame
{
public:
    //---------------------------------------------------------------------------
    /// \brief Constructor.
    ///
    /// \param[in] frame_files
    ///     A list of frame file names.
    /// \param[in] channels
    ///     A list of channels to extract from  original frames
    ///     (only channel names are allowed).
    /// \param[in] UserOptions
    ///     User specified options.
    ///
    /// \return
    ///     New instance of this object.
    //---------------------------------------------------------------------------
    ReduceRawFrame( const char*              frame_files,
                    const char*              channels,
                    const RDSFrame::Options& UserOptions );

    //---------------------------------------------------------------------------
    /// \brief Constructor.
    ///
    /// \param[in] frame_files
    ///     A list of frame file names.
    /// \param[in] channels
    ///     A list of channels to extract from  original frames
    ///     (only channel names are allowed).
    /// \param[in] UserOptions
    ///     User specified options.
    ///
    /// \return
    ///     New instance of this object.
    //---------------------------------------------------------------------------
    ReduceRawFrame( const frame_file_container_type& frame_files,
                    const channel_container_type&    channels,
                    const RDSFrame::Options&         UserOptions );

    //---------------------------------------------------------------------------
    /// \brief Destructor
    //---------------------------------------------------------------------------
    virtual ~ReduceRawFrame( );

    virtual void writeFrameToStream( );

    // Expose the main hook for processing information
    using RDSFrame::ProcessRequest;

protected:
    virtual void processChannel( fr_adc_data_type Adc );

    virtual void processChannel( fr_proc_data_type Proc );

    virtual void rangeOptimizer( const time_type& UserStart,
                                 const time_type& UserStop,
                                 time_type&       DataStart,
                                 time_type&       DataStop ) const;

    virtual stop_request_type stopRequest( ) const;
};

#endif
