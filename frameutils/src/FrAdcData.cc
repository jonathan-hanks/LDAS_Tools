//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

// System Header Files
#include <cmath>
#include <sstream>

// Local Header Files
#include "FrAdcData.hh"
#include "FrVect.hh"
#include "util.hh"

using FrameCPP::FrAdcData;
using FrameCPP::FrVect;
using FrameCPP::Time;

using namespace std;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

using namespace FrameAPI::FrAdcData;

void
FrameAPI::FrAdcData::appendAuxData( FrameCPP::FrAdcData&       Dest,
                                    const FrameCPP::FrAdcData& Source )
{
    for ( FrameCPP::FrAdcData::aux_type::const_iterator a(
              Source.RefAux( ).begin( ) );
          a != Source.RefAux( ).end( );
          a++ )
    {
        //:TODO: At some point, may need to put in an 'empty' container to
        //:TODO:   preserve the fact that a frame had no Auxiliary info.
        //:TODO: If the above action is taken, then all segments must be
        //:TODO:   checked for aux info. If no adc segments have aux info
        //:TODO:   then don't write any 'empty' containers.
        FrameCPP::FrAdcData::aux_type::iterator dref = Dest.RefAux( ).begin( ),
                                                last = Dest.RefAux( ).end( );

        while ( ( dref != last ) &&
                ( ( *dref )->GetName( ).compare( ( *a )->GetName( ) ) != 0 ) )
        {
            if ( ( ( *dref )->GetName( ).length( ) == 0 ) &&
                 ( ( *a )->GetName( ).length( ) == 0 ) )
            {
                break;
            }
            dref++;
        }
        if ( dref != last )
        {
            FrameAPI::FrVect::FrVectList  vect_list;
            FrameCPP::FrAdcData::aux_type a_aux;
            FrameCPP::FrAdcData::aux_type dref_aux;

            a_aux.append( *a );
            dref_aux.append( *dref );

            vect_list.push_back( &dref_aux );
            vect_list.push_back( &a_aux );

            FrameCPP::FrAdcData::aux_type::value_type result(
                FrameAPI::FrVect::concat( vect_list ) );
            Dest.RefAux( ).erase( dref, dref + 1 );
            Dest.RefAux( ).append( result );
        }
        else
        {
            Dest.RefAux( ).append( *a );
        }
    }
}

FrameCPP::FrAdcData*
FrameAPI::FrAdcData::cloneHeader( const FrameCPP::FrAdcData& Source )
{
    return new FrameCPP::FrAdcData( Source.GetName( ),
                                    Source.GetChannelGroup( ),
                                    Source.GetChannelNumber( ),
                                    Source.GetNBits( ),
                                    Source.GetSampleRate( ),
                                    Source.GetBias( ),
                                    Source.GetSlope( ),
                                    Source.GetUnits( ),
                                    Source.GetFShift( ),
                                    Source.GetTimeOffset( ),
                                    Source.GetDataValid( ),
                                    Source.GetPhase( ) );
}

FrameCPP::FrAdcData*
FrameAPI::FrAdcData::concat( const std::list< FrameCPP::Object* >& Segment )
{
    //:TODO: Need to ad support for aux data
    //-------------------------------------------------------------------
    // Loop over the parts getting the statistics
    //-------------------------------------------------------------------

    AT( );

    INT_4U samples( 0 );
    INT_2U data_valid( 0 );

    AT( );
    for ( std::list< FrameCPP::Object* >::const_iterator s( Segment.begin( ) );
          s != Segment.end( );
          s++ )
    {
        AT( );
        FrameCPP::FrAdcData* src( dynamic_cast< FrameCPP::FrAdcData* >( *s ) );

        samples += FrameAPI::FrVect::getSamples( src->RefData( ).begin( ),
                                                 src->RefData( ).end( ) );
        if ( ( data_valid == 0 ) || ( src->GetDataValid( ) != 0 ) )
        {
            AT( );
            data_valid = src->GetDataValid( );
        }
    }

    //-------------------------------------------------------------------
    // Allocate a single channel to hold the data
    //-------------------------------------------------------------------
    AT( );
    std::unique_ptr< FrameCPP::FrAdcData > c( FrameAPI::FrAdcData::cloneHeader(
        *dynamic_cast< FrameCPP::FrAdcData* >( Segment.front( ) ) ) );
    AT( );
    c->SetDataValid( data_valid );

    //-------------------------------------------------------------------
    // Fill in the data
    //-------------------------------------------------------------------

    AT( );
    FrameAPI::FrVect::FrVectList vector_list;

    AT( );
    for ( std::list< FrameCPP::Object* >::const_iterator s( Segment.begin( ) );
          s != Segment.end( );
          s++ )
    {
        AT( );
        FrameCPP::FrAdcData* src( dynamic_cast< FrameCPP::FrAdcData* >( *s ) );

        // Append all comments
        AT( );
        c->AppendComment( src->GetComment( ) );
        // Append auxiliary information
        AT( );
        FrameAPI::FrAdcData::appendAuxData( *c, *src );
        // Build a list of vectors to concat
        vector_list.push_back( &( src->RefData( ) ) );
    }

    //-------------------------------------------------------------------
    // Create the new vector and attach it to the channel
    //-------------------------------------------------------------------

    AT( );
    FrameCPP::FrAdcData::data_type::value_type data(
        FrameAPI::FrVect::concat( vector_list ) );
    c->RefData( ).append( data );
#if LM_DEBUG
    std::cerr << "DEBUG: Number of Points: "
              << c->RefData( ).back( )->GetDim( 0 ).GetNx( ) << std::endl;
#endif /* LM_DEBUG */

    //-------------------------------------------------------------------
    // Return the results
    //-------------------------------------------------------------------

    AT( );
    return c.release( );
}

bool
areAdcsCompatable( const FrAdcData& Adc1, const FrAdcData& Adc2 )
{
    const string& adc1_name( Adc1.GetName( ) );
    const string& adc2_name( Adc2.GetName( ) );

    if ( strcasecmp( adc1_name.c_str( ), adc2_name.c_str( ) ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_channels" );
    }

    // Do simple comparison of simple datatypes within the class
#define ADC_COMPARE( LM_ATTR, LM_EXCEPTION )                                   \
    if ( Adc1.LM_ATTR( ) != Adc2.LM_ATTR( ) )                                  \
    {                                                                          \
        std::ostringstream msg;                                                \
        msg << LM_EXCEPTION << " (" << Adc1.LM_ATTR( )                         \
            << " != " << Adc2.LM_ATTR( )                                       \
            << ") - Channel Name: " << Adc1.GetName( );                        \
        throw SWIGEXCEPTION( msg.str( ) );                                     \
    }

    ADC_COMPARE( GetChannelGroup, "incompatible_adcdata: channelgroup" );
    ADC_COMPARE( GetChannelNumber, "incompatible_adcdata: channelnumber" );
    ADC_COMPARE( GetBias, "incompatible_adcdata: bias" );
    ADC_COMPARE( GetSlope, "incompatible_adcdata: slope" );
    ADC_COMPARE( GetUnits, "incompatible_adcdata: units" );
    ADC_COMPARE( GetSampleRate, "incompatible_adcdata: samplerate" );
    ADC_COMPARE( GetFShift, "incompatible_adcdata: fshift" );
    ADC_COMPARE( GetPhase, "incompatible_adcdata: phase" );

#undef ADC_COMPARE

    return true;
}

namespace FrameAPI
{
    template <>
    REAL_8
    SampleRate< FrameCPP::FrAdcData >( const FrameCPP::FrAdcData& C )
    {
        return C.GetSampleRate( );
    }
} // namespace FrameAPI
