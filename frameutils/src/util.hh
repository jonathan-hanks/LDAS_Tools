//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAMEAPI__UTIL_HH
#define FRAMEAPI__UTIL_HH

#include <string.h>

#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/gpstime.hh"

#include "framecpp/FrVect.hh"

//! ignore_begin:

//-----------------------------------------------------------------------------
//
//: Case-Sensitive Compare Operator.
//
struct eqstr
{
    //: Overloaded operator().
    //
    //! param: const char* s1 - First string.
    //! param: const char* s2 - Second string.
    //
    //! return: bool - True if strings are equal, false otherwise.
    //
    bool
    operator( )( const char* s1, const char* s2 ) const
    {
        return strcmp( s1, s2 ) == 0;
    }
};

typedef LDASTools::AL::unordered_map< std::string, int > QueryHash;

namespace LDASTools
{
    namespace AL
    {
        class GPSTime;
    }
} // namespace LDASTools

namespace FrameCPP
{
    namespace Common
    {
        class MD5Sum;
    }
} // namespace FrameCPP

#if HAVE_LDAS_PACKAGE_ILWD
namespace ILwd
{
    class LdasContainer;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

namespace FrameAPI
{
    const REAL_8 NANO_SECOND = 1.0e-9;
    const REAL_8 TIME_TOLERANCE = ( 1000 * NANO_SECOND );

    extern int EnableMemoryMappedIO;
    extern int StreamBufferSize;

#if HAVE_LDAS_PACKAGE_ILWD
    bool Continuous( const ILwd::LdasContainer& C1,
                     const ILwd::LdasContainer& C2 );
#endif /* HAVE_LDAS_PACKAGE_ILWD */
    bool Continuous( const LDASTools::AL::GPSTime& T1,
                     const LDASTools::AL::GPSTime& T2 );

#if HAVE_LDAS_PACKAGE_ILWD
    LDASTools::AL::GPSTime GetEndTime( const ILwd::LdasContainer& C );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
    LDASTools::AL::GPSTime GetStartTime( const ILwd::LdasContainer& C );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

    FrameCPP::FrVect::compression_scheme_type
    StrToCompressionScheme( const char* Method );

    void LogMD5Sum( const std::string&              FrameFilename,
                    const FrameCPP::Common::MD5Sum& MD5,
                    const std::string&              OutputDir );

    template < typename Channel >
    REAL_8 SampleRate( const Channel& C );
} // namespace FrameAPI

//! ignore_end:
#endif /* FRAMEAPI__UTIL_HH */
