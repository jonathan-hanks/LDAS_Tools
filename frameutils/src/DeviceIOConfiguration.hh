//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_API__DEVICE_IO_CONFIGURATION_HH
#define FRAME_API__DEVICE_IO_CONFIGURATION_HH

#include <map>
#include <string>
#include <vector>

#include "ldastoolsal/types.hh"
#include "ldastoolsal/unordered_map.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/ReadWriteLock.hh"
#include "ldastoolsal/Singleton.hh"

#include "framecpp/Common/IOStream.hh"

#include "MountMgr.hh"

namespace FrameAPI
{
    //---------------------------------------------------------------------
    /// \brief Singleton to keep track of how to support io on a device
    //---------------------------------------------------------------------
    class DeviceIOConfiguration
        : LDASTools::AL::Singleton< DeviceIOConfiguration >
    {
    public:
        typedef INT_8U dev_type;
        typedef FrameCPP::Common::FrameBufferInterface::buffer_size_type
                     size_type;
        typedef bool use_mmap_io_type;

        typedef struct
        {
            //-----------------------------------------------------------------
            /// \brief  Size of cache to use. 0 for default, positive for use
            /// specified
            //-----------------------------------------------------------------
            size_type s_buffer_size;
            //-----------------------------------------------------------------
            /// \brief Boolean for determining to use mmap functionallity in the
            /// file buffer
            //-----------------------------------------------------------------
            use_mmap_io_type s_use_mmap_io;
        } Characteristics;

        typedef struct
        {
            std::string     s_name;
            Characteristics s_characteristics;
        } Description;

        typedef std::vector< Description > descriptions_type;

        //-------------------------------------------------------------------
        /// \brief Constructor
        //-------------------------------------------------------------------
        DeviceIOConfiguration( );

        //-------------------------------------------------------------------
        /// \brief Destructor
        //-------------------------------------------------------------------
        ~DeviceIOConfiguration( );

        //-------------------------------------------------------------------
        /// \brief Fetch the options for the device associated with the file
        ///
        /// \param[in] Filename
        ///     The name of the file on the device being queried.
        ///
        /// \param[out] BufferSize
        ///     The optimal size of the buffer for the device.
        ///
        /// \param[out] UseMemoryMappedIO
        ///     True if the device performance is enhanced by using memory
        ///     mapped I/O.
        ///
        //-------------------------------------------------------------------
        static void GetConfiguration( const char* Filename,
                                      size_type&  BufferSize,
                                      bool&       UseMemoryMappedIO );

        //-------------------------------------------------------------------
        /// \brief Fetch the options for the device associated with the file
        ///
        /// \param[in] Filename
        ///     The name of the file on the device being queried.
        ///
        /// \param[out] BufferSize
        ///     The optimal size of the buffer for the device.
        ///
        /// \param[out] UseMemoryMappedIO
        ///     True if the device performance is enhanced by using memory
        ///     mapped I/O.
        ///
        /// \param[out] FSType
        ///     The description of the file type for the device being
        ///     queried.
        ///
        //-------------------------------------------------------------------
        static void GetConfiguration( const char*  Filename,
                                      size_type&   BufferSize,
                                      bool&        UseMemoryMappedIO,
                                      std::string& FSType );

        //-------------------------------------------------------------------
        /// \brief Resets the cache to be empty
        ///
        /// \param[out] Descriptions
        ///     List of names or file system types that are to be reset.
        ///
        /// \return
        ///     List of warning messages generated while trying to reset.
        //-------------------------------------------------------------------
        static std::string Reset( const descriptions_type& Descriptions );

    private:
        typedef std::map< dev_type, Characteristics > cache_type;
        typedef LDASTools::AL::unordered_map< std::string, Characteristics >
            fstype_cache_type;

        LDASTools::AL::ReadWriteLock::baton_type m_cache_lock;
        cache_type                               m_cache;
        fstype_cache_type                        m_fstype_cache;
        MountMgr                                 m_mount_mgr;

        //-------------------------------------------------------------------
        /// \brief Fetch the options for the device associated with the file
        ///
        /// \param[in] Filename
        ///     The name of the file on the device being queried.
        ///
        /// \param[out] BufferSize
        ///     The optimal size of the buffer for the device.
        ///
        /// \param[out] UseMemoryMappedIO
        ///     True if the device performance is enhanced by using memory
        ///     mapped I/O.
        ///
        /// \param[out] FSType
        ///     The description of the file type for the device being
        ///     queried.
        ///
        //-------------------------------------------------------------------
        void get_configuration( const char*  Filename,
                                size_type&   BufferSize,
                                bool&        UseMemoryMappedIO,
                                std::string& FSType );

        //-------------------------------------------------------------------
        /// \brief Resets the cache to be empty
        ///
        /// \param[out] Descriptions
        ///     List of names or file system types that are to be reset.
        ///
        /// \return
        ///     List of warning messages generated while trying to reset.
        //-------------------------------------------------------------------
        std::string reset( const descriptions_type& Descriptions );

    }; // class - DeviceIOConfiguration

    inline void
    DeviceIOConfiguration::GetConfiguration( const char* Filename,
                                             size_type&  BufferSize,
                                             bool&       UseMemoryMappedIO )
    {
        std::string fs_type = "__ignore__";

        GetConfiguration( Filename, BufferSize, UseMemoryMappedIO, fs_type );
    }

} // namespace FrameAPI

#endif /* FRAME_API__DEVICE_IO_CONFIGURATION_HH */
