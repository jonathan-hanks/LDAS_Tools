//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <frameutils_config.h>

// System Header Files
#include <sstream>

#include "framecpp/Time.hh"

// Local Header Files
#include "FrRawData.hh"
#include "util.hh"

using FrameCPP::Common::SearchContainer;

using namespace std;

namespace FrameAPI
{
    namespace FrRawData
    {
        //---------------------------------------------------------------
        //! param: FrameCPP::FrRawDaa* Primary - This will containe the
        //+		source plusthe infomation from Secondary
        //! param: FrameCPP::FrRawData* Secondary - Additional information to be
        //+		merged with Primary
        //! return: FrameCPP::FrRawData* - The union of Primary and Secondary

        FrameCPP::FrRawData*
        merge( FrameCPP::FrRawData*       Primary,
               const FrameCPP::FrRawData* Secondary )
        {
            //-----------------------------------------------------------
            // If BOTH the primary and the secondary are null, just
            //   return a null pointer
            //-----------------------------------------------------------
            if ( ( Primary == (FrameCPP::FrRawData*)NULL ) &&
                 ( Secondary == (FrameCPP::FrRawData*)NULL ) )
            {
                return (FrameCPP::FrRawData*)NULL;
            }
            //-----------------------------------------------------------
            // If the primary is null and the secondary is non-null,
            //   return a duplicate of the second
            //-----------------------------------------------------------
            if ( Primary == (FrameCPP::FrRawData*)NULL )
            {
                return new FrameCPP::FrRawData( *Secondary );
            }
            //-----------------------------------------------------------
            // If the primary is non-null and the secondary is null,
            //   return the primary.
            //-----------------------------------------------------------
            if ( Secondary == (FrameCPP::FrRawData*)NULL )
            {
                return Primary;
            }
            //-----------------------------------------------------------
            // :TODO: Merging of data fields
            //-----------------------------------------------------------
            return Primary;
        }
    } // namespace FrRawData
} // namespace FrameAPI
