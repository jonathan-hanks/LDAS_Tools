//
// LDASTools Frame Utilities - A library to extend manipulation of LIGO/Virgo
// Frame Files
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools Frame Utilities is free software; you may redistribute it and/or
// modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools Frame Utilities is distributed in the hope that it will be useful,
// but without any warranty or even the implied warranty of merchantability or
// fitness for a particular purpose. See the GNU General Public License (GPLv2)
// for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameApiVectHH
#define FrameApiVectHH

#include <algorithm>
#include <deque>
#include <memory>

#include "framecpp/Common/Container.hh"
#include "framecpp/FrVect.hh"

#include "genericAPI/swigexception.hh"

namespace FrameAPI
{
    namespace FrVect
    {
        typedef FrameCPP::Common::Container< FrameCPP::FrVect > Container;
        typedef std::list< const Container* >                   FrVectList;

        //-------------------------------------------------------------------
        /// \brief Append elements from another FrVect
        ///
        /// \param[in,out] Dest
        ///     The recipient of the appended container.
        ///
        /// \param[in] Source
        ///     Another container whose value is appended.
        //-------------------------------------------------------------------
        void appendStructures( Container& Dest, const Container& Source );

        //-------------------------------------------------------------------
        /// \brief Concatinate multiple FrVect objects into a single object
        ///
        /// \param[in] List
        ///     Container of ordered FrVect objects to be concatinated.
        ///
        /// \return
        ///     New FrVect object containing the concatination results.
        //-------------------------------------------------------------------
        FrameCPP::FrVect* concat( const FrVectList& List );

        //-------------------------------------------------------------------
        /// \brief Copy all data from a list of FrVect objects.
        ///
        /// \param[in] Source
        ///     A collection of FrVect objects containing data to be copied.
        ///
        /// \param[out] Dest
        ///     A preallocated continuous segment of memory that is large
        ///     enough to receive the data from each FrVect object.
        //-------------------------------------------------------------------
        void copy( void* Dest, const Container& Source );

        //-------------------------------------------------------------------
        /// \brief Create storage space appropriate for the FrVect data element.
        ///
        /// \param[in] Type
        ///     The data type for the data storage element of the FrVect
        ///
        /// \param[in] Size
        ///     The size for the data storage element of the FrVect
        ///
        /// \return
        ///     A new array that is appropriate to use as teh data element
        ///	    of an FrVect object.
        //-------------------------------------------------------------------
        void* createVector( INT_2U Type, INT_4U Size );

        //-------------------------------------------------------------------
        /// \brief Get the number of data points over the specified range.
        ///
        /// \param[in] Start
        ///     The first element of the range.
        ///
        /// \param[in] Stop
        ///     The last element of the range.
        ///
        /// \return
        ///     The number of samples contained within the specified range.
        //-------------------------------------------------------------------
        INT_4U getSamples( Container::const_iterator Start,
                           Container::const_iterator Stop );
    } // namespace FrVect
} // namespace FrameAPI

#endif // FrameApiVectHH
