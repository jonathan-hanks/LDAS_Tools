variables:
  PROJECT_DIR: .
  PROJECT_PACKAGE_NAME: ${CI_PROJECT_NAME}
  IGWN_CMAKE_PREFIX: /usr
  IGWN_CMAKE_PACKAGE_NAME: igwn-cmake-macros
  IGWN_CMAKE_PACKAGE_DIR: igwn-cmake
  IGWN_CMAKE_MODULES_DIRS: ${IGWN_CMAKE_PREFIX}/share/${IGWN_CMAKE_PACKAGE_DIR}/cmake/Modules
  DEPENDENCY_SCRIPT: ${IGWN_CMAKE_PREFIX}/share/${IGWN_CMAKE_PACKAGE_DIR}/cmake/scripts/dependencies.cmake
  PROJECT_HAS_USER_DOCS: "yes"
  PROJECT_HAS_DEVELOPER_DOCS: "yes"
  PROJECT_HAS_ADMIN_DOCS: "yes"
  #-------------------------------------------
  # Needed version info for mk_build_deps
  #-------------------------------------------
  BOOST_MINIMUM_VERSION: "1.67"
  IGWN_CMAKE_VERSION: "1.4.0"
  LDAS_TOOLS_AL_SWIG_VERSION: "2.6.10"
  LDAS_TOOLS_AL_VERSION: "2.6.7"
  LDAS_TOOLS_CMAKE_VERSION: "1.2.3"
  LDAS_TOOLS_DISKCACHEAPI_SWIG_VERSION: "2.6.12"
  LDAS_TOOLS_DISKCACHEAPI_VERSION: "2.7.2"
  LDAS_TOOLS_FILTERS_VERSION: "2.6.6"
  LDAS_TOOLS_FRAMEAPI_SWIG_VERSION: "2.6.12"
  LDAS_TOOLS_FRAMEAPI_VERSION: "2.6.6"
  LDAS_TOOLS_FRAMECPP_SWIG_VERSION: "2.6.14"
  LDAS_TOOLS_FRAMECPP_VERSION: "3.0.1"
  LDAS_TOOLS_LDASGEN_SWIG_VERSION: "2.6.10"
  LDAS_TOOLS_LDASGEN_VERSION: "2.7.3"
  LDAS_TOOLS_UTILITIES_VERSION: "2.6.7"
  LDAS_TOOLS_SWIG_VERSION: "3.0.7"

stages:
  - level0
  - level1
  - level2
  - level3
  - level4
  - level5
  - pages

#========================================================================
# A N C H O R S
#========================================================================
#------------------------------------------------------------------------
# Only
#------------------------------------------------------------------------
.only-manual: &only-manual
  only:
    refs:
      - manual

.only-ci: &only-ci
  only:
    refs:
      - pushes
      - schedules
      - web

.only-disable: &only-manual
  only:
    refs:
      - manual

#------------------------------------------------------------------------
# Stages
#------------------------------------------------------------------------
.stage-ldas-tools-suite: &stage-ldas-tools-suite
  stage: level1

.stage-igwn-cmake: &stage-igwn-cmake
  stage: level0

.stage-ldastools-cmake: &stage-ldastools-cmake
  stage: level1

.stage-ldastoolsal: &stage-ldastoolsal
  stage: level2

.stage-ldastoolsal-swig: &stage-ldastoolsal-swig
  stage: level3

.stage-filters: &stage-filters
  stage: level3

.stage-framecpp: &stage-framecpp
  stage: level3

.stage-framecpp-swig: &stage-framecpp-swig
  stage: level4

.stage-ldasgen: &stage-ldasgen
  stage: level3

.stage-ldasgen-swig: &stage-ldasgen-swig
  stage: level4

.stage-diskcache: &stage-diskcache
  stage: level4

.stage-diskcache-swig: &stage-diskcache-swig
  stage: level5

.stage-frameutils: &stage-frameutils
  stage: level4

.stage-frameutils-swig: &stage-frameutils-swig
  stage: level5

.stage-utilities: &stage-utilities
  stage: level5

#------------------------------------------------------------------------
# Package Setup
#------------------------------------------------------------------------

.setup-igwn-cmake:
  extends:
    - .stage-igwn-cmake

.setup-ldastools-cmake:
  extends:
    - .stage-ldastools-cmake
    - .var-ldastools-cmake

.setup-ldastoolsal:
  extends:
    - .stage-ldastoolsal
    - .var-ldastoolsal

.setup-ldastoolsal-swig:
  extends:
    - .stage-ldastoolsal-swig
    - .var-ldastoolsal-swig

.setup-filters:
  extends:
    - .stage-filters
    - .var-filters

.setup-framecpp:
  extends:
    - .stage-framecpp
    - .var-framecpp

.setup-framecpp-swig:
  extends:
    - .stage-framecpp-swig
    - .var-framecpp-swig

.setup-ldasgen:
  extends:
    - .stage-ldasgen
    - .var-ldasgen

.setup-ldasgen-swig:
  extends:
    - .stage-ldasgen-swig
    - .var-ldasgen-swig

.setup-diskcache:
  extends:
    - .stage-diskcache
    - .var-diskcache

.setup-diskcache-swig:
  extends:
    - .stage-diskcache-swig
    - .var-diskcache-swig

.setup-frameutils:
  extends:
    - .stage-frameutils
    - .var-frameutils

.setup-frameutils-swig:
  extends:
    - .stage-frameutils-swig
    - .var-frameutils-swig

.setup-utilities:
  extends:
    - .stage-utilities
    - .var-utilities

.setup-ldas-tools-suite:
  extends:
    - .stage-ldas-tools-suite

#------------------------------------------------------------------------
# Variable Sets
#------------------------------------------------------------------------
.var-diskcache: &var-diskcache
  variables:
    PROJECT_DIR:          diskcache
    PROJECT_PACKAGE_NAME: ldas-tools-diskcacheAPI

.var-diskcache-swig: &var-diskcache-swig
  variables:
    PROJECT_DIR:          diskcache-swig
    PROJECT_PACKAGE_NAME: ldas-tools-diskcacheAPI-swig
    PROJECT_PACKAGE_BASE_NAME: ldas-tools-diskcacheAPI

.var-filters: &var-filters
  variables:
    PROJECT_DIR:          filters
    PROJECT_PACKAGE_NAME: ldas-tools-filters

.var-framecpp: &var-framecpp
  variables:
    PROJECT_DIR:          framecpp
    PROJECT_PACKAGE_NAME: ldas-tools-framecpp

.var-framecpp-swig: &var-framecpp-swig
  variables:
    PROJECT_DIR:          framecpp-swig
    PROJECT_PACKAGE_NAME: ldas-tools-framecpp-swig
    PROJECT_PACKAGE_BASE_NAME: ldas-tools-framecpp

.var-frameutils: &var-frameutils
  variables:
    PROJECT_DIR:          frameutils
    PROJECT_PACKAGE_NAME: ldas-tools-frameAPI

.var-frameutils-swig: &var-frameutils-swig
  variables:
    PROJECT_DIR:          frameutils-swig
    PROJECT_PACKAGE_NAME: ldas-tools-frameAPI-swig
    PROJECT_PACKAGE_BASE_NAME: ldas-tools-frameAPI

.var-ldasgen: &var-ldasgen
  variables:
    PROJECT_DIR:          ldasgen
    PROJECT_PACKAGE_NAME: ldas-tools-ldasgen

.var-ldasgen-swig: &var-ldasgen-swig
  variables:
    PROJECT_DIR:          ldasgen-swig
    PROJECT_PACKAGE_NAME: ldas-tools-ldasgen-swig
    PROJECT_PACKAGE_BASE_NAME: ldas-tools-ldasgen

.var-ldastools-cmake: &var-ldastools-cmake
  variables:
    PROJECT_DIR:          ldastools_cmake
    PROJECT_PACKAGE_NAME: ldas-tools-cmake

.var-ldastoolsal: &var-ldastoolsal
  variables:
    PROJECT_DIR:          ldastoolsal
    PROJECT_PACKAGE_NAME: ldas-tools-al

.var-ldastoolsal-swig: &var-ldastoolsal-swig
  variables:
    PROJECT_DIR:          ldastoolsal-swig
    PROJECT_PACKAGE_NAME: ldas-tools-al-swig
    PROJECT_PACKAGE_BASE_NAME: ldas-tools-al

.var-utilities: &var-utilities
  variables:
    PROJECT_DIR:          utilities
    PROJECT_PACKAGE_NAME: ldas-tools-utilities

.var-swig-no: &var-swig-no
  BUILD_SWIG_EXTENSIONS: "-DBUILD_SWIG_EXTENSIONS=NO"

