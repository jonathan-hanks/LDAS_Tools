#!/bin/bash

FIND="gfind"
if which clang-format-3.8 > /dev/null; then
    CFORMAT=`which clang-format-3.8`
elif which clang-format-mp-3.8 > /dev/null; then
    CFORMAT=`which clang-format-mp-3.8`
elif which clang-format > /dev/null; then
    CFORMAT=`which clang-format`
else
    echo "Could not find clang format"
    exit 1
fi

SOURCE_DIRS="ldastoolsal ldastoolsal-swig framecpp filters ldasgen framecpp-swig ldasgen-swig frameutils diskcache frameutils-swig diskcache-swig utilities"
REFORMAT="${CFORMAT} -i -style=file -fallback-style=none"
# REFORMAT="echo"

for dir in ${SOURCE_DIRS}; do
    echo "Reformatting in ${dir}"
    ${FIND} ${dir} -type f -iregex ".*\.\(c\|cc\|cxx\|cpp\|h\|hh\|hpp\|hxx\)\(\.in\|\)" \! -iname \*_config.h.in -exec ${REFORMAT} {} \;
done
