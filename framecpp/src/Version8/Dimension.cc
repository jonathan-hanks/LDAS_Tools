//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version8/Dimension.hh"

namespace FrameCPP
{
    namespace Version_8
    {
        //------------------------------------------------------------------
        // Constructors
        //------------------------------------------------------------------

        //------------------------------------------------------------------
        /// \brief Default Constructor.
        ///
        /// This creates a dimension object with the following values:
        ///
        ///   Dimension Length (Nx) - 1
        ///   Scale Factor (Dx) - 1.0
        ///   Units (UnitX) - ""
        Dimension::Dimension( )
            : mNx( 1 ), mDx( 1.0 ), mUnitX( "" ), mStartX( .0 )
        {
        }

        //------------------------------------------------------------------
        /// \brief Copy constructor.
        ///
        /// \param[in] dim
        ///   The Dimension to copy from.
        ///
        /// \exception std::bad_alloc
        ///   Memory allocation failed.
        Dimension::Dimension( const Dimension& dim )
            : mNx( dim.mNx ), mDx( dim.mDx ), mUnitX( dim.mUnitX ),
              mStartX( dim.mStartX )
        {
        }

        //------------------------------------------------------------------
        /// \brief Constructor
        ///
        /// This creates a Dimension object with the specified attributes.
        ///
        /// \param[in] nx
        ///     The dimension length.
        /// \param[in] dx
        ///     The Scale Factor.
        /// \param[in] unitX
        ///     The units (unit per step size).
        /// \param[in] startx
        ///     The origin of the data set.
        ///
        /// \exception std::bad_alloc
        ///     Memory allocation failed.
        //------------------------------------------------------------------
        Dimension::Dimension( nx_type            nx,
                              dx_type            dx,
                              const std::string& unitX,
                              startX_type        startx )
            : mNx( nx ), mDx( dx ), mUnitX( unitX ), mStartX( startx )
        {
        }
    } // namespace Version_8
} // namespace FrameCPP
