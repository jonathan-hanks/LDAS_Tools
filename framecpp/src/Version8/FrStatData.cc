//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <algorithm>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrStatData.hh"
#include "framecpp/Version8/FrDetector.hh"
#include "framecpp/Version8/FrSE.hh"
#include "framecpp/Version8/FrSH.hh"
#include "framecpp/Version8/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//-----------------------------------------------------------------------

FR_OBJECT_META_DATA_DEFINE( FrStatDataImpl,
                            FSI_FR_STAT_DATA,
                            "FrStatData",
                            "Statistical Data Structure" )

namespace FrameCPP
{
    namespace Version_8
    {
        //===============================================================
        // FrStatData
        //===============================================================

        FrStatData::FrStatData( const FrStatData& Source )
        {
            Data::name = Source.GetName( );
            Data::comment = Source.GetComment( );
            Data::representation = Source.GetRepresentation( );
            Data::timeStart = Source.GetTimeStart( );
            Data::timeEnd = Source.GetTimeEnd( );
            Data::version = Source.GetVersion( );

            //-------------------------------------------------------------------
            // Copy references
            //-------------------------------------------------------------------
            SetDetector( Source.Data::GetDetector( ) );
            RefData( ) = Source.RefData( );
            RefTable( ) = Source.RefTable( );
        }

        FrStatData::FrStatData( const name_type&           Name,
                                const comment_type&        Comment,
                                const representation_type& Representation,
                                const timeStart_type       TimeStart,
                                const timeEnd_type         TimeEnd,
                                const version_type         Version )
        {
            Data::name = Name;
            Data::comment = Comment;
            Data::representation = Representation;
            Data::timeStart = TimeStart;
            Data::timeEnd = TimeEnd;
            Data::version = Version;
        }

        FrStatData::FrStatData( const Previous::FrStatData& Source,
                                istream_type*               Stream )
          : ClassicIO< FrStatData >( Source, Stream )
        {
        }

#if 0
        FrStatData*
        FrStatData::Clone( ) const
        {
            return new FrStatData( *this );
        }
#endif /* 0 */

        FrStatData&
        FrStatData::Merge( const FrStatData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrStatData::operator==( const Common::FrameSpec::Object& RHS ) const
        {
            return Common::Compare( *this, RHS );
        }

    } // namespace Version_8
} // namespace FrameCPP
