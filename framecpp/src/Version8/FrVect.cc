//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream>
#include <algorithm>
#include <stdexcept>

#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Compression.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrVect.hh"
#include "framecpp/Version8/FrSE.hh"
#include "framecpp/Version8/FrSH.hh"

#include "framecpp/Version8/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_8;

using namespace FrameCPP::Version_8;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

using namespace FrameCPP::Compression;

#define LM_DEBUG 0
#define LM_INFO 0

#if LM_DEBUG
#define AT( ) std::cerr << "INFO: " << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

#if HAVE_TEMPLATE_MOVE
#define MOVE_RET( a ) ( std::move( a ) )
#else /* HAVE_TEMPLATE_MOVE */
#define MOVE_RET( a ) ( a )
#endif /* HAVE_TEMPLATE_MOVE */

//=======================================================================
// Static
//=======================================================================

FR_OBJECT_META_DATA_DEFINE( FrVectImpl,
                            FSI_FR_VECT,
                            "FrVect",
                            "Vector Data Structure" )

namespace FrameCPP
{
    namespace Common
    {
    } // namespace Common
} // namespace FrameCPP

#define TRACE_MEMORY 0
#if TRACE_MEMORY
#define MEM_ALLOCATE( )                                                        \
    std::cerr << "MEMORY: Allocate: " << FrVect::getStaticName( ) << " "       \
              << (void*)this << " Line: " << __LINE__ << std::endl;
#define MEM_DELETE( )                                                          \
    std::cerr << "MEMORY: Delete: " << FrVect::getStaticName( ) << " "         \
              << (void*)this << " Line: " << __LINE__ << std::endl;
#else
#define MEM_ALLOCATE( )
#define MEM_DELETE( )
#endif

//=======================================================================
// CLASS - FrVect
//=======================================================================
FrVect::FrVect( )
{
    MEM_ALLOCATE( );
}

FrVect::FrVect( const FrVect& Source )
{
    MEM_ALLOCATE( );

    m_data.copy_core( Source.m_data );
    data_copy( Source.GetDataRaw( ).get( ), Source.GetNBytes( ) );
}

/** \cond ignore_no_uniquely */
FrVect::FrVect( const std::string&    name,
                type_type             type,
                nDim_type             nDim,
                const Dimension*      dims,
                const byte_order_type byte_order,
                const void*           data,
                const std::string&    unitY )
{
    MEM_ALLOCATE( );
    m_data.set( name, type, byte_order, nDim, dims, unitY );
    data_copy( reinterpret_cast< data_const_pointer_type >( data ),
               m_data.nBytes );
}
/** \endcond */

#if 0
FrVect::
FrVect( const std::string& name,
	INT_2U type,
	nDim_type nDim,
	const Dimension* dims,
	const byte_order_type byte_order,
	void* data,
	const std::string& unitY )
{
  MEM_ALLOCATE( );
  m_data.set( name, type, byte_order, nDim, dims, unitY );
  data_copy( static_cast< CHAR_U* >( data ), m_data.nBytes );
}
#endif /* 0 */

FrVect::FrVect( const Previous::FrVect& Source, Common::IStream* Stream )
{
    m_data.name = Source.GetName( );
    m_data.compress = Source.GetCompress( );
    m_data.type = Source.GetType( );
    m_data.nData = Source.GetNData( );
    m_data.nBytes = Source.GetNBytes( );
    m_data.data = Source.GetDataRaw( );
    for ( nDim_type cur = 0, last = Source.GetNDim( ); cur != last; ++cur )
    {
        m_data.dimension.push_back( Dimension( Source.GetDim( cur ) ) );
    }
    m_data.unitY = Source.GetUnitY( );
}

/** \cond ignore_no_uniquely */
FrVect::FrVect( const std::string& name,
                compress_type      Compress,
                type_type          type,
                nDim_type          nDim,
                const Dimension*   dims,
                nData_type         NData,
                nBytes_type        NBytes,
                data_type          Data,
                const std::string& unitY )
{
    m_data.set( name, type, BYTE_ORDER_HOST, nDim, dims, unitY );
    m_data.compress = Compress;
    m_data.nData = NData;
    m_data.nBytes = NBytes;
    m_data.data = Data;
}
/** \endcond */

#if 0
FrVect::
FrVect( const std::string& name,
	compress_type Compress,
	type_type type,
	nDim_type nDim,
	const Dimension* dims,
	nData_type NData,
	nBytes_type NBytes,
	data_type Data,
	const std::string& unitY )
{
  m_data.set( name, type, BYTE_ORDER_HOST, nDim, dims, unitY );
  m_data.compress = Compress;
  m_data.nData = NData;
  m_data.data = Data;
}
#endif /* 0 */

FrVect::FrVect( const std::string& Name,
                type_type          Type,
                nDim_type          NDim,
                const Dimension*   Dims,
                const std::string& UnitY )
{
    m_data.set( Name, Type, BYTE_ORDER_HOST, NDim, Dims, UnitY );
    SetNData( Dimension::CalcNData( NDim, Dims ) );
    nBytes_type nbytes = GetTypeSize( ) * GetNData( );
    m_data.data.reset( new CHAR_U[ nbytes ] );
}

FrVect::~FrVect( )
{
    MEM_DELETE( );
}

bool
FrVect::operator!=( const FrVect& RHS ) const
{
    return !( *this == RHS );
}

bool
FrVect::operator==( const FrVect& RHS ) const
{
    return ( this->m_data == RHS.m_data );
}

FrVect&
FrVect::operator+=( const FrVect& RHS )
{
    //---------------------------------------------------------------------
    // Perform sanity checks
    //---------------------------------------------------------------------
    if ( ( this->GetName( ) != RHS.GetName( ) ) ||
         ( this->GetType( ) != RHS.GetType( ) ) ||
         ( this->GetNDim( ) != RHS.GetNDim( ) ) ||
         ( this->GetUnitY( ) != RHS.GetUnitY( ) ) )
    {
        std::ostringstream msg;
        bool               comma;

        msg << "Unable to concat the FrVect structures because: ";
        comma = false;
        if ( this->GetName( ) != RHS.GetName( ) )
        {
            msg << "name of objects differ (" << this->GetName( ) << " vs. "
                << RHS.GetName( ) << ")";
            comma = true;
        }
        if ( this->GetType( ) != RHS.GetType( ) )
        {
            if ( comma )
            {
                msg << ", ";
            }
            msg << "type of objects differ (" << this->GetType( ) << " vs. "
                << RHS.GetType( ) << ")";
            comma = true;
        }
        if ( this->GetNDim( ) != RHS.GetNDim( ) )
        {
            if ( comma )
            {
                msg << ", ";
            }
            msg << "number of dimensions differ (" << this->GetNDim( )
                << " vs. " << RHS.GetNDim( ) << ")";
            comma = true;
        }
        if ( this->GetUnitY( ) != RHS.GetUnitY( ) )
        {
            if ( comma )
            {
                msg << ", ";
            }
            msg << "y units differ (" << this->GetUnitY( ) << " vs. "
                << RHS.GetUnitY( ) << ")";
            comma = true;
        }
        throw std::domain_error( msg.str( ) );
    }
    for ( nDim_type cur = 0, last = this->GetNDim( ); cur != last; ++cur )
    {
        const Dimension& ld( this->GetDim( cur ) );
        const Dimension& rd( RHS.GetDim( cur ) );

        if ( cur > 0 )
        {
            if ( ld != rd )
            {
                std::ostringstream msg;

                msg << "Unable to concat the FrVect structures";
                /// \todo
                ///   Need to have a list of reasons
                throw std::domain_error( msg.str( ) );
            }
        }
        else
        {
            if ( ( ld.GetNx( ) != rd.GetNx( ) ) ||
                 ( ld.GetStartX( ) != rd.GetStartX( ) ) ||
                 ( ld.GetUnitX( ) != rd.GetUnitX( ) ) )
            {
                std::ostringstream msg;

                msg << "Unable to concat the FrVect structures";
                /// \todo
                ///   Need to have a list of reasons
                throw std::domain_error( msg.str( ) );
            }
        }
    }
    //---------------------------------------------------------------------
    // Calculate how much space is required
    //---------------------------------------------------------------------
    const INT_2U type_size( GetTypeSize( GetType( ) ) );
    this->GetDim( 0 ).SetNx( this->GetDim( 0 ).GetNx( ) +
                             RHS.GetDim( 0 ).GetNx( ) );
    const INT_8U s = GetNData( ) + RHS.GetNData( );

    //---------------------------------------------------------------------
    // Allocate the space, fill it with the existing data and then store
    //   it.
    //---------------------------------------------------------------------
    data_const_pointer_type rhs_data( RHS.GetDataRaw( ).get( ) );
    data_type               rhs_data_expanded;
    nBytes_type             rhs_data_nbytes;

    expandToBuffer( rhs_data_expanded, rhs_data_nbytes );
    if ( rhs_data_nbytes )
    {
        rhs_data = rhs_data_expanded.get( );
    }

    data_type               data( new CHAR_U[ s ] );
    data_const_pointer_type source( GetDataUncompressed( ).get( ) );

    std::copy( source, source + ( GetNData( ) * type_size ), data.get( ) );
    std::copy( rhs_data,
               rhs_data + ( RHS.GetNData( ) * type_size ),
               data.get( ) + ( GetNData( ) * type_size ) );
    m_data.data = data;
    //---------------------------------------------------------------------
    // Return
    //---------------------------------------------------------------------
    return *this;
}

namespace FrameCPP
{
    namespace Version_8
    {
        namespace FrVectImpl
        {
            template <>
            Data::type_type
            Data::GetDataType< CHAR >( )
            {
                return FR_VECT_C;
            }

            template <>
            Data::type_type
            Data::GetDataType< CHAR_U >( )
            {
                return FR_VECT_1U;
            }

            template <>
            Data::type_type
            Data::GetDataType< INT_2S >( )
            {
                return FR_VECT_2S;
            }

            template <>
            Data::type_type
            Data::GetDataType< INT_2U >( )
            {
                return FR_VECT_2U;
            }

            template <>
            Data::type_type
            Data::GetDataType< INT_4S >( )
            {
                return FR_VECT_4S;
            }

            template <>
            Data::type_type
            Data::GetDataType< INT_4U >( )
            {
                return FR_VECT_4U;
            }

            template <>
            Data::type_type
            Data::GetDataType< INT_8S >( )
            {
                return FR_VECT_8S;
            }

            template <>
            Data::type_type
            Data::GetDataType< INT_8U >( )
            {
                return FR_VECT_8U;
            }

            template <>
            Data::type_type
            Data::GetDataType< REAL_4 >( )
            {
                return FR_VECT_4R;
            }

            template <>
            Data::type_type
            Data::GetDataType< REAL_8 >( )
            {
                return FR_VECT_8R;
            }

            template <>
            Data::type_type
            Data::GetDataType< COMPLEX_8 >( )
            {
                return FR_VECT_8C;
            }

            template <>
            Data::type_type
            Data::GetDataType< COMPLEX_16 >( )
            {
                return FR_VECT_16C;
            }

            template <>
            Data::type_type
            Data::GetDataType< std::string >( )
            {
                return FR_VECT_STRING;
            }

            template <>
            bool
            ClassicIO< FrameCPP::Version_8::FrVect >::
            operator==( const Common::FrameSpec::Object& Obj ) const
            {
                return Common::Compare( *this, Obj );
            }

            template <>
            template <>
            FrameCPP::cmn_streamsize_type
            ClassicIO< FrameCPP::Version_8::FrVect >::Bytes<
                std::vector< Dimension > >(
                const std::vector< Dimension >& Dims )
            {
                INT_8U ret( sizeof( nDim_type ) ); // nDim
                for ( std::vector< Dimension >::const_iterator d(
                          Dims.begin( ) );
                      d != Dims.end( );
                      d++ )
                {
                    ret += sizeof( INT_8U ) + // nx
                        sizeof( REAL_8 ) + // dx
                        sizeof( REAL_8 ) + // startX
                        ( *d ).GetUnitX( ).Bytes( );
                }
                return ret;
            } // Bytes
        } // namespace FrVectImpl
    } // namespace Version_8
} // namespace FrameCPP

const FrVect::name_type&
FrVect::GetName( ) const
{
    return ( Data::GetName( ) );
}

FrVect&
FrVect::Merge( const FrVect& RHS )
{
    /// \todo
    ///   Need to implement Merge routine
    std::string msg( "Merge currently not implemented for " );
    msg += StructName( );

    throw std::domain_error( msg );
    return *this;
}

FrVect::subfrvect_type
FrVect::SubFrVect( INT_4U Start, INT_4U Stop ) const
{
    //---------------------------------------------------------------------
    // Create new FrVect
    //---------------------------------------------------------------------
    subfrvect_type retval( new FrVect );
    //---------------------------------------------------------------------
    // Calculate the block size
    //---------------------------------------------------------------------
    INT_4U          block_size = GetTypeSize( GetType( ) );
    const nDim_type ndim = GetNDim( );

    for ( nDim_type x = 1; x < ndim; ++x )
    {
        block_size *= GetDim( x ).GetNx( );
    }
    //---------------------------------------------------------------------
    // Copy the core of the information
    //---------------------------------------------------------------------
    retval->m_data.copy_core( this->m_data );
    //---------------------------------------------------------------------
    // Set the Dimension information for the sink
    //---------------------------------------------------------------------
    retval->GetDim( 0 ).SetNx( Stop - Start );
    retval->SetNData( Stop - Start );
    //---------------------------------------------------------------------
    // Allocate space in the sink
    //---------------------------------------------------------------------
    retval->data_alloc( block_size * ( Stop - Start ) );
    retval->m_data.compress = RAW;
    //---------------------------------------------------------------------
    // Copy the data
    //---------------------------------------------------------------------
    data_pointer_type data_source( GetDataRaw( ).get( ) );
    nBytes_type       nBytes( 0 );
    data_type         expanded_buffer;

    expandToBuffer( expanded_buffer, nBytes );
    if ( nBytes )
    {
        data_source = expanded_buffer.get( );
    }
    data_pointer_type data_sink( retval->GetDataRaw( ).get( ) );

    for ( nBytes_type x = Start; x < Stop; ++x )
    {
        //-------------------------------------------------------------------
        // Copy 1 block's worth of data
        //-------------------------------------------------------------------
        std::copy( data_source + ( x * block_size ),
                   data_source + ( ( x + 1 ) * block_size ),
                   data_sink + ( ( x - Start ) * block_size ) );
    }
    //---------------------------------------------------------------------
    // Return the newly constructed FrVect
    //---------------------------------------------------------------------
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpessimizing-move"
#endif /* __clang__ */
    return MOVE_RET( retval );
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic pop
#endif /* __clang__ */
}

//=======================================================================
// Local functions
//=======================================================================
#if !defined( __SUNPRO_CC ) || ( __SUNPRO_CC > 0x550 )
#include "framecpp/Version8/FrVect.tcc"

#define INSTANTIATE( LM_TYPE )                                                 \
    template FrVect::FrVect( const std::string& name,                          \
                             nDim_type          nDim,                          \
                             const Dimension*   dims,                          \
                             const LM_TYPE*     data,                          \
                             const std::string& unitY );                       \
                                                                               \
    template FrVect::FrVect( const std::string& name,                          \
                             nDim_type          nDim,                          \
                             const Dimension*   dims,                          \
                             LM_TYPE*           data,                          \
                             const std::string& unitY )

#include "TypeInstantiation.tcc"

#undef INSTANTIATE

#endif /* */
