//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrEvent.hh"
#include "framecpp/Version8/FrSE.hh"
#include "framecpp/Version8/FrSH.hh"

#include "framecpp/Version8/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

//=======================================================================
// Static
//=======================================================================

FR_OBJECT_META_DATA_DEFINE( FrEventImpl,
                            FSI_FR_EVENT,
                            "FrEvent",
                            "Event Data Structure" )

namespace FrameCPP
{
    namespace Version_8
    {
        //=======================================================================
        // FrEvent
        //=======================================================================
        FrEvent::FrEvent( )
        {
        }

        FrEvent::FrEvent( const name_type&       name,
                          const comment_type&    comment,
                          const inputs_type&     inputs,
                          const time_type&       time,
                          const timeBefore_type  timeBefore,
                          const timeAfter_type   timeAfter,
                          const eventStatus_type eventStatus,
                          const amplitude_type   amplitude,
                          const probability_type probability,
                          const statistics_type& statistics,
                          const ParamList_type&  parameters )
        {
            Data::name = name;
            Data::comment = comment;
            Data::inputs = inputs;
            Data::GTime = time;
            Data::timeBefore = timeBefore;
            Data::timeAfter = timeAfter;
            Data::eventStatus = eventStatus;
            Data::amplitude = amplitude;
            Data::probability = probability;
            Data::statistics = statistics;
            Data::Params = parameters;
        }

        FrEvent::FrEvent( const FrEvent& Source )
        {
            name = Source.name;
            comment = Source.comment;
            inputs = Source.inputs;
            GTime = Source.GTime;
            timeBefore = Source.timeBefore;
            timeAfter = Source.timeAfter;
            eventStatus = Source.eventStatus;
            amplitude = Source.amplitude;
            probability = Source.probability;
            statistics = Source.statistics;
            Params = Source.Params;
        }

        FrEvent::FrEvent( const Previous::FrEvent& Source,
                          istream_type*            Stream )
            : FrEventImpl::ClassicIO< FrEvent >( Source, Stream )
        {
        }

        const std::string&
        FrEvent::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrEvent&
        FrEvent::Merge( const FrEvent& RHS )
        {
            throw Unimplemented(
                "FrEvent& FrEvent::Merge( const FrEvent& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        bool
        FrEvent::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

    } // namespace Version_8
} // namespace FrameCPP
