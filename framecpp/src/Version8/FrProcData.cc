//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Singleton.hh"
#include "ldastoolsal/gpstime.hh"

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version8/FrProcData.hh"
#include "framecpp/Version8/FrSE.hh"
#include "framecpp/Version8/FrSH.hh"

#include "framecpp/Version8/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using std::ceil;
using std::floor;

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------

FR_OBJECT_META_DATA_DEFINE( FrProcDataImpl,
                            FSI_FR_PROC_DATA,
                            "FrProcData",
                            "Post-Processed Data Structure" )

//=======================================================================
// Anonymous
//=======================================================================
namespace
{
    typedef FrameCPP::Version_8::FrProcData::type_type type_type;

    class IDTypeToStringMapping
    {
        SINGLETON_TS_DECL( IDTypeToStringMapping );

    public:
        const std::string& GetString( type_type Id ) const;

    private:
        std::vector< std::string > m_types;
    };

    class IDSubTypeToStringMappingFrequency
    {
        SINGLETON_TS_DECL( IDSubTypeToStringMappingFrequency );

    public:
        const std::string& GetString( type_type Id ) const;

    private:
        std::vector< std::string > m_types;
    };

} // namespace

namespace FrameCPP
{
    namespace Version_8
    {

        namespace FrProcDataImpl
        {
            const std::string&
            Data::IDTypeToString( Data::type_type Type )
            {
                return IDTypeToStringMapping::Instance( ).GetString( Type );
            }

            const std::string&
            Data::IDSubTypeToString( Data::type_type    Type,
                                     Data::subType_type SubType )
            {
                static const std::string empty( "" );
                switch ( Type )
                {
                case FREQUENCY_SERIES:
                    return IDSubTypeToStringMappingFrequency::Instance( )
                        .GetString( Type );
                    break;
                default:
                    return empty;
                    break;
                }
            }

            const Data::name_type&
            Data::GetNameSlow( ) const
            {
                return GetName( );
            }
        } // namespace FrProcDataImpl

        //=======================================================================
        // FrProcData
        //=======================================================================
        FrProcData::FrProcData( const name_type&    Name,
                                const comment_type& Comment,
                                type_type           Type,
                                subType_type        SubType,
                                timeOffset_type     TimeOffset,
                                tRange_type         TRange,
                                fShift_type         FShift,
                                phase_type          Phase,
                                fRange_type         FRange,
                                BW_type             BW )
        {
            Data::name = Name;
            Data::comment = Comment;
            Data::type = Type;
            Data::subType = SubType;
            Data::timeOffset = TimeOffset;
            Data::tRange = TRange;
            Data::fShift = FShift;
            Data::phase = Phase;
            Data::fRange = FRange;
            Data::BW = BW;
        }

        FrProcData::FrProcData( Previous::FrProcData& Source,
                                istream_type*         Stream )
            : FrProcDataImpl::ClassicIO< FrProcData >( Source, Stream )
        {
        }

        FrProcData::~FrProcData( )
        {
        }

        FrProcData&
        FrProcData::Merge( const FrProcData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        FrProcData::subset_ret_type
        FrProcData::Subset( REAL_8 Offset, REAL_8 Dt ) const
        {
            subset_ret_type retval;

            //---------------------------------------------------------------------
            // Process the types that are understood
            //---------------------------------------------------------------------
            switch ( GetType( ) )
            {
            case TIME_SERIES:
                retval = sub_time_series( Offset, Dt );
                break;
            default:
            {
                //-----------------------------------------------------------------
                // This type is not supported so throw an exception
                //-----------------------------------------------------------------
                std::ostringstream msg;
                msg << "Currently cannot take a subsection of type: "
                    << IDTypeToString( GetType( ) );
                const std::string& subtype(
                    IDSubTypeToString( GetType( ), GetSubType( ) ) );
                if ( subtype.length( ) > 0 )
                {
                    msg << " subType: " << subtype;
                }
                throw std::runtime_error( msg.str( ) );
            }
            break;
            }

            return retval;
        }

        bool
        FrProcData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        // Routine to take a subsection when the data type is TimeSeries data
        FrProcData::subset_ret_type
        FrProcData::sub_time_series( REAL_8 Offset, REAL_8 Dt ) const
        {
            //---------------------------------------------------------------------
            // Sanity checks
            //---------------------------------------------------------------------
            // Verify the existance of only a single FrVect component
            //---------------------------------------------------------------------
            if ( RefData( ).size( ) != 1 )
            {
                std::ostringstream msg;

                msg << "METHOD: frameAPI::FrProcData::SubFrProcData: "
                    << "data vectors of length " << RefData( ).size( )
                    << " currently are not supported";
                throw std::logic_error( msg.str( ) );
            }
            INT_4U num_elements = RefData( )[ 0 ]->GetDim( 0 ).GetNx( );
            //---------------------------------------------------------------------
            /// \todo
            ///   Ensure the dimension of the FrVect is 1
            //---------------------------------------------------------------------
            //---------------------------------------------------------------------
            /// \todo
            ///   Ensure the requested range is valid
            //---------------------------------------------------------------------
            REAL_8 sample_rate = 1.0 / RefData( )[ 0 ]->GetDim( 0 ).GetDx( );
            if ( Offset >
                 ( GetTimeOffset( ) + ( num_elements / sample_rate ) ) )
            {
                std::ostringstream msg;
                msg << "METHOD: frameAPI::FrProcData::SubFrProcData: "
                    << "The value for the parameter Offset (" << Offset
                    << ") is out of range ([0-"
                    << ( GetTimeOffset( ) + ( num_elements / sample_rate ) )
                    << "))";
                throw std::range_error( msg.str( ) );
            }
            //---------------------------------------------------------------------
            // Start moving of data
            //---------------------------------------------------------------------
            subset_ret_type retval( new FrProcData );
            // Copy in the header information
            retval->copy_core( *this );

            /// \todo
            ///   Get the slice of FrVect
            REAL_8 startSampleReal = ( Offset * sample_rate ) -
                ( retval->GetTimeOffset( ) * sample_rate );

            INT_4U endSample =
                INT_4U( ceil( startSampleReal + ( Dt * sample_rate ) ) );
            INT_4U startSample = ( startSampleReal > REAL_8( 0 ) )
                ? INT_4U( floor( startSampleReal ) )
                : INT_4U( 0 );

            {
                boost::shared_ptr< FrVect > subvect(
                    RefData( )[ 0 ]
                        ->SubFrVect( startSample, endSample )
                        .release( ) );
                retval->RefData( ).append( subvect );
            }

            //---------------------------------------------------------------------
            // Shift the offset
            //---------------------------------------------------------------------
            if ( startSampleReal > 0 )
            {
                // There is no gap
                retval->SetTimeOffset( REAL_8( 0 ) );
            }
            else
            {
                retval->timeOffset += Offset;
            }
            //---------------------------------------------------------------------
            // :TODO: Recalculate tRange
            //---------------------------------------------------------------------
            retval->SetTRange( retval->RefData( )[ 0 ]->GetDim( 0 ).GetNx( ) *
                               retval->RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) );
            //---------------------------------------------------------------------
            // Return the new FrProcData
            //---------------------------------------------------------------------
            return retval;
        }

    } // namespace Version_8
} // namespace FrameCPP

//=======================================================================
// Anonymous
//=======================================================================
namespace
{
    //---------------------------------------------------------------------
    // Initialization of singleton
    //---------------------------------------------------------------------
    SINGLETON_TS_INST( IDTypeToStringMapping );
    //---------------------------------------------------------------------
    // Default constructor
    //---------------------------------------------------------------------
    IDTypeToStringMapping::IDTypeToStringMapping( )
    {
        LDASTools::AL::MemChecker::Append(
            singleton_suicide,
            "<anonymous>::IDTypeToStringMapping::singleton_suicide",
            5 );
        m_types.push_back( "Unknown/user defined" );
        m_types.push_back( "Time Series" );
        m_types.push_back( "Frequency Series" );
        m_types.push_back( "Other 1D Series data" );
        m_types.push_back( "Time-Frequency" );
        m_types.push_back( "Wavelets" );
        m_types.push_back( "Multi-dimensional" );
    }

    const std::string&
    IDTypeToStringMapping::GetString( type_type Type ) const
    {
        if ( Type >= m_types.size( ) )
        {
            std::ostringstream msg;
            msg << "Cannot map FrProcData type value of " << Type
                << " to string value because it is out of range";
            throw std::range_error( msg.str( ) );
        }
        return m_types[ Type ];
    }

    //---------------------------------------------------------------------
    // Initialization of singleton
    //---------------------------------------------------------------------
    SINGLETON_TS_INST( IDSubTypeToStringMappingFrequency );
    //---------------------------------------------------------------------
    // Default constructor
    //---------------------------------------------------------------------
    IDSubTypeToStringMappingFrequency::IDSubTypeToStringMappingFrequency( )
    {
        LDASTools::AL::MemChecker::Append(
            singleton_suicide,
            "<anonymous>::IDTypeToStringMappingFrequency::singleton_suicide",
            5 );
        m_types.push_back( "Unknown/user defined" );
        m_types.push_back( "Amplitude Spectral Density" );
        m_types.push_back( "Power Spectral Density" );
        m_types.push_back( "Cross Spectral Density" );
        m_types.push_back( "Coherence" );
        m_types.push_back( "Transfer Function" );
    }

    const std::string&
    IDSubTypeToStringMappingFrequency::GetString( INT_2U Type ) const
    {
        if ( Type >= m_types.size( ) )
        {
            std::ostringstream msg;
            msg << "Cannot map FrProcData type value of " << Type
                << " to string value because it is out of range";
            throw std::range_error( msg.str( ) );
        }
        return m_types[ Type ];
    }
} // namespace

namespace FrameCPP
{
    namespace Version_8
    {
    } // namespace Version_8
} // namespace FrameCPP
