//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP__VERSION_9__StreamRef_HH
#define FrameCPP__VERSION_9__StreamRef_HH

#include "ldastoolsal/types.hh"

#include "framecpp/Version9/FrameSpec.hh"

#include "framecpp/Common/StreamRef.hh"

#undef FR_CE_LENGTH_TYPE
#undef FR_CE_CHKTYPE_TYPE
#undef FR_CE_CLASS_TYPE
#undef FR_CE_INSTANCE_TYPE

#define FR_CE_LENGTH_TYPE INT_8U
#define FR_CE_CHKTYPE_TYPE CHAR_U
#define FR_CE_CLASS_TYPE CHAR_U
#define FR_CE_INSTANCE_TYPE INT_4U

namespace FrameCPP
{
    namespace Version_9
    {
        typedef FR_CE_LENGTH_TYPE   fr_ce_length_type;
        typedef FR_CE_CHKTYPE_TYPE  fr_ce_chkType_type;
        typedef FR_CE_CLASS_TYPE    fr_ce_class_type;
        typedef FR_CE_INSTANCE_TYPE fr_ce_instance_type;

        //-------------------------------------------------------------------
        /// \brief Common Elements of all frame structures.
        ///
        /// This type is to provide the common components of all frame
        /// structures written to a stream.
        /// This implements table 6 in section 4.3.2 of the
        /// frame specification document.
        //-------------------------------------------------------------------
        typedef Common::StreamRef2< fr_ce_length_type /* length */,
                                    fr_ce_chkType_type /* chkType */,
                                    fr_ce_class_type /* class */,
                                    fr_ce_instance_type /* instance */ >
            StreamRef;
    } // namespace Version_9

} // namespace FrameCPP

#endif /* FrameCPP__VERSION_9__StreamRef_HH */
