//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_9__IMPL__FrTOCClassicIO_HH
#define FrameCPP_VERSION_9__IMPL__FrTOCClassicIO_HH

#include "framecpp/Version9/impl/FrTOCData.hh"

#if defined( __cplusplus ) && !defined( SWIG )
#include <map>
#include <vector>

#include "framecpp/Common/CheckSum.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameSpec.hh"
#include "framecpp/Common/FrTOC.hh"

#include "framecpp/Version8/impl/FrTOCClassicIO.hh"

#include "framecpp/Version9/FrameSpec.hh"
#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/FrSE.hh"
#endif /* defined( __cplusplus ) && !defined( SWIG ) */

#if defined( __cplusplus )
#include "framecpp/Version9/impl/FrTOCAdcDataClassicIO.hh"
#include "framecpp/Version9/impl/FrTOCDataClassicIO.hh"
#include "framecpp/Version9/impl/FrTOCEventClassicIO.hh"
#include "framecpp/Version9/impl/FrTOCProcDataClassicIO.hh"
#include "framecpp/Version9/impl/FrTOCSerDataClassicIO.hh"
#include "framecpp/Version9/impl/FrTOCSimDataClassicIO.hh"
#include "framecpp/Version9/impl/FrTOCSimEventClassicIO.hh"
#include "framecpp/Version9/impl/FrTOCSummaryClassicIO.hh"

#endif /* defined( __cplusplus ) */

#if defined( __cplusplus ) && !defined( SWIG )

namespace
{
    //-------------------------------------------------------------------
    /// @brief Encode string checksum
    ///
    /// @param[in] String
    ///   The source string
    /// @param[in,out] Checksum
    ///   The checksum buffer
    //-------------------------------------------------------------------
    inline void
    string_checksum( const std::string&             String,
                     FrameCPP::Common::CheckSumCRC& Checksum )
    {
        INT_2U size = String.size( );

        Checksum.calc( &size, sizeof( size ) );
        Checksum.calc( String.c_str( ), size + 1 );
    }
} // namespace

namespace FrameCPP
{
    namespace Version_9
    {

        class FrTOC;

        namespace FrTOCImpl
        {
            template < typename T >
            class ClassicIO : public virtual Data,
                              public ObjectWithChecksum< Common::FrTOC >,
                              public FrTOCDataClassicIO,
                              public FrTOCAdcDataClassicIO,
                              public FrTOCProcDataClassicIO,
                              public FrTOCSimDataClassicIO,
                              public FrTOCSerDataClassicIO,
                              public FrTOCSummaryClassicIO,
                              public FrTOCEventClassicIO,
                              public FrTOCSimEventClassicIO
            {
            public:
                //-----------------------------------------------------------------
                ///
                //-----------------------------------------------------------------
                ClassicIO( )
                    : ObjectWithChecksum< Common::FrTOC >(
                          StructDescription( ) )
                {
                }

                //---------------------------------------------------------------
                /// @brief Return TOC identifying checksum
                ///
                /// The CRC checksum that characterizes this Table of Contents.
                ///
                /// @return
                ///   The checksum that characterizes this Table of Contents
                ///   structure
                //---------------------------------------------------------------
                virtual INT_4U
                GetTOCChecksum( )
                {
                    Common::CheckSumCRC checksum;

                    // nFrame, dt
                    {
                        auto nFrame = GetNFrame( );

                        checksum.calc( &nFrame, sizeof( nFrame ) );

                        if ( nFrame > 0 )
                        {
                            auto dt = GetDt( );
                            checksum.calc( &dt[ 0 ],
                                           sizeof( dt_type ) * dt.size( ) );
                        }
                    }
                    if ( GetNFrame( ) > 0 )
                    {
                        // nADC, name,
                        {
                            auto      names = FrTOCAdcDataClassicIO::GetKeys( );
                            nadc_type nADC = names.size( );

                            checksum.calc( &nADC, sizeof( nADC ) );
                            for ( auto name : names )
                            {
                                string_checksum( name, checksum );
                            }
                        }
                        // nProc, nameProc,
                        {
                            auto nameProcs = FrTOCProcDataClassicIO::GetKeys( );
                            nproc_type nProc = nameProcs.size( );

                            checksum.calc( &nProc, sizeof( nProc ) );
                            for ( auto nameProc : nameProcs )
                            {
                                string_checksum( nameProc, checksum );
                            }
                        }
                        // nSim, nameSim,
                        {
                            auto      sims = FrTOCSimDataClassicIO::GetSim( );
                            nsim_type nSim = sims.size( );

                            checksum.calc( &nSim, sizeof( nSim ) );
                            for ( auto sim : sims )
                            {
                                string_checksum( sim.first, checksum );
                            }
                        }
                        // nSer, nameSer,
                        {
                            auto sers = FrTOCSerDataClassicIO::GetSer( );
                            FrTOCSerDataClassicIO::n_type nSer = sers.size( );

                            checksum.calc( &nSer, sizeof( nSer ) );
                            for ( auto ser : sers )
                            {
                                string_checksum( ser.first, checksum );
                            }
                        }
                        // nSummary, nameSum,
                        {
                            auto summarys =
                                FrTOCSummaryClassicIO::GetSummary( );
                            FrTOCSummaryClassicIO::n_type nSummary =
                                summarys.size( );

                            checksum.calc( &nSummary, sizeof( nSummary ) );
                            for ( auto summary : summarys )
                            {
                                string_checksum( summary.first, checksum );
                            }
                        }
                        // nEventType, nameEvent, nEvent, nTotalEvent,
                        {
                            FrTOCEventClassicIO::nevent_type nEventType =
                                FrTOCEventClassicIO::m_info.size( );
                            FrTOCEventClassicIO::nTotalEvent_type nTotalEvent =
                                0;

                            // nEventType
                            checksum.calc( &nEventType, sizeof( nEventType ) );
                            if ( nEventType > 0 )
                            {
                                // nameEvent
                                for ( auto event : FrTOCEventClassicIO::m_info )
                                {
                                    string_checksum( event.first, checksum );
                                }
                                // nEvent
                                for ( auto event : FrTOCEventClassicIO::m_info )
                                {
                                    const FrTOCEventClassicIO::nevent_type
                                        nEvent = event.second.size( );

                                    nTotalEvent += nEvent;
                                    checksum.calc( &nEvent, sizeof( nEvent ) );
                                }
                            }
                            // nTotalEvent
                            checksum.calc( &nTotalEvent,
                                           sizeof( nTotalEvent ) );
                        }
                        // nSimEventType, nameSimEvent, nSimEvent, nTotalSEven
                        {
                            FrTOCSimEventClassicIO::nevent_type nSimEventType =
                                FrTOCSimEventClassicIO::m_info.size( );
                            FrTOCSimEventClassicIO::nTotalSEvent_type
                                nTotalSEvent = 0;

                            // nSimEventType
                            checksum.calc( &nSimEventType,
                                           sizeof( nSimEventType ) );
                            if ( nSimEventType > 0 )
                            {
                                // nameSimEvent
                                for ( auto event :
                                      FrTOCSimEventClassicIO::m_info )
                                {
                                    string_checksum( event.first, checksum );
                                }
                                // nSimEvent
                                for ( auto event :
                                      FrTOCSimEventClassicIO::m_info )
                                {
                                    const FrTOCSimEventClassicIO::nevent_type
                                        nSimEvent = event.second.size( );

                                    nTotalSEvent += nSimEvent;
                                    checksum.calc( &nSimEvent,
                                                   sizeof( nSimEvent ) );
                                }
                            }
                            // nTotalSEvent
                            checksum.calc( &nTotalSEvent,
                                           sizeof( nTotalSEvent ) );
                        }
                    }
                    return ( checksum.value( ) );
                }

                //-----------------------------------------------------------------
                /// \brief The description of structure
                ///
                /// \return
                ///     A Description object which describes this structure as
                ///     specified by the frame specification.
                //-----------------------------------------------------------------
                static const Common::Description*
                StructDescription( )
                {
                    using Common::Description;

                    static Description ret;

                    if ( ret.size( ) == 0 )
                    {
                        //-----------------------------------------------
                        ret( FrSH( Data::StructName( ),
                                   Data::FrameSpecId( ),
                                   Data::FrameSpecDescription( ) ) );

                        FrTOCDataClassicIO::Description< FrSE >( ret );
                        FrTOCAdcDataClassicIO::Description< FrSE >( ret );
                        FrTOCProcDataClassicIO::Description< FrSE >( ret );
                        FrTOCSimDataClassicIO::Description< FrSE >( ret );
                        FrTOCSerDataClassicIO::Description< FrSE >( ret );
                        FrTOCSummaryClassicIO::Description< FrSE >( ret );
                        FrTOCEventClassicIO::Description< FrSE >( ret );
                        FrTOCSimEventClassicIO::Description< FrSE >( ret );

                        ret( FrSE( "chkSum",
                                   CheckSumDataClass( ),
                                   CheckSumDataComment( ) ) );
                    }

                    return &ret;
                }

            protected:
                //-----------------------------------------------------------------
                /// \brief Demotes object to previous version of the frame spec
                ///
                /// \param[in] Target
                ///     The version of the frame specification to demote too.
                /// \param[in] Obj
                ///     The version of the object to demote.
                /// \param[in] Stream
                ///     The input stream from which the original object was
                ///     read.
                ///
                /// \return
                ///     An object of the previous generation.
                //-----------------------------------------------------------------
                virtual demote_ret_type
                demote( INT_2U           Target,
                        demote_arg_type  Obj,
                        Common::IStream* Stream ) const
                {
                    throw Unimplemented(
                        "Object* FrTOC::demote( Object* Obj ) const",
                        DATA_FORMAT_VERSION,
                        __FILE__,
                        __LINE__ );
                }

                //-----------------------------------------------------------------
                /// \brief Promotes object to another version of the frame spec
                ///
                /// \param[in] Target
                ///     The version of the promoted frame specification.
                /// \param[in] Obj
                ///     The object to be promoted.
                /// \param[in] Stream
                ///     The input stream from which the original object was
                ///     read.
                ///
                /// \return
                ///     An object promoted to the next generation.
                //-----------------------------------------------------------------
                virtual promote_ret_type
                promote( INT_2U           Target,
                         promote_arg_type Obj,
                         Common::IStream* Stream ) const
                {
                    throw Unimplemented(
                        "Object* FrTOC::promote( Object* Obj ) const",
                        DATA_FORMAT_VERSION,
                        __FILE__,
                        __LINE__ );
                }

                //-----------------------------------------------------------------
                /// \brief Number of bytes needed to write this structure
                ///
                /// \param[in] Stream
                ///     The stream from which to the object is being read or
                ///     written.
                ///
                /// \return
                ///     The number of bytes need to read or write this object.
                //-----------------------------------------------------------------
                virtual Common::FrameSpec::size_type
                pBytes( const Common::StreamBase& Stream ) const
                {
                    return FrTOCDataClassicIO::Bytes( Stream ) +
                        FrTOCAdcDataClassicIO::Bytes( Stream ) +
                        FrTOCProcDataClassicIO::Bytes( Stream ) +
                        FrTOCSimDataClassicIO::Bytes( Stream ) +
                        FrTOCSerDataClassicIO::Bytes( Stream ) +
                        FrTOCSummaryClassicIO::Bytes( Stream ) +
                        FrTOCEventClassicIO::Bytes( Stream ) +
                        FrTOCSimEventClassicIO::Bytes( Stream );
                }

                //-----------------------------------------------------------------
                /// \brief Virtual constructor
                ///
                /// \param[in] Stream
                ///     The input stream from where the object is being read.
                //-----------------------------------------------------------------
                virtual self_type*
                pCreate( istream_type& Stream ) const
                {
                    T* obj( new T( ) );

                    obj->FrTOCDataClassicIO::Load( Stream );
                    obj->FrTOCAdcDataClassicIO::Load( Stream,
                                                      obj->GetNFrame( ) );
                    obj->FrTOCProcDataClassicIO::Load( Stream,
                                                       obj->GetNFrame( ) );
                    obj->FrTOCSimDataClassicIO::Load( Stream,
                                                      obj->GetNFrame( ) );
                    obj->FrTOCSerDataClassicIO::Load( Stream,
                                                      obj->GetNFrame( ) );
                    obj->FrTOCSummaryClassicIO::Load( Stream,
                                                      obj->GetNFrame( ) );
                    obj->FrTOCEventClassicIO::Load( Stream );
                    obj->FrTOCSimEventClassicIO::Load( Stream );

                    return ( obj );
                }

                //-----------------------------------------------------------------
                /// \brief Write the structure to the stream
                ///
                /// \param[in] Stream
                ///     The output stream where the object is to be written.
                //-----------------------------------------------------------------
                virtual void
                pWrite( Common::OStream& Stream ) const
                {
                    FrTOCDataClassicIO::write( Stream );
                    FrTOCAdcDataClassicIO::write( Stream );
                    FrTOCProcDataClassicIO::write( Stream );
                    FrTOCSimDataClassicIO::write( Stream );
                    FrTOCSerDataClassicIO::write( Stream );
                    FrTOCSummaryClassicIO::write( Stream );
                    FrTOCEventClassicIO::write( Stream );
                    FrTOCSimEventClassicIO::write( Stream );
                }

            }; // namespace FrTOCImpl
        } // namespace FrTOCImpl
    } // namespace Version_9
} // namespace FrameCPP

#endif /* defined( __cplusplus ) && !defined( SWIG ) */

#endif /* FrameCPP_VERSION_9__IMPL__FrTOCClassicIO_HH */
