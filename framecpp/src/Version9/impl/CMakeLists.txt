#
# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2019 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#

set( HDR_DIR framecpp/Version${FRAME_SPEC}/impl )
set( hhdir  ${CMAKE_INSTALL_INCLUDEDIR}/${HDR_DIR} )

#=========================================================================
# Frame Source and Headers
#=========================================================================

set( src_hdrs
  DimensionSerialIO.hh
  HelperSerialIO.hh
  FrObjectMetaData.hh

  FrameHClassicIO.hh
  FrameHData.hh
  FrAdcDataClassicIO.hh
  FrAdcDataData.hh
  FrDetectorClassicIO.hh
  FrDetectorData.hh
  FrDetectorSerialIO.hh
  FrEndOfFileClassicIO.hh
  FrEndOfFileData.hh
  FrEndOfFileSerialIO.hh
  FrEndOfFrameClassicIO.hh
  FrEndOfFrameData.hh
  FrEndOfFrameSerialIO.hh
  FrEventClassicIO.hh
  FrEventData.hh
  FrEventSerialIO.hh
  FrHistoryClassicIO.hh
  FrHistoryData.hh
  FrHistorySerialIO.hh
  FrMsgClassicIO.hh
  FrMsgData.hh
  FrMsgSerialIO.hh
  FrProcDataClassicIO.hh
  FrProcDataData.hh
  FrRawDataClassicIO.hh
  FrRawDataData.hh
  FrSEClassicIO.hh
  FrSESerialIO.hh
  FrSEData.hh
  FrSerDataClassicIO.hh
  FrSerDataData.hh
  FrSHClassicIO.hh
  FrSHSerialIO.hh
  FrSHData.hh
  FrSimDataClassicIO.hh
  FrSimDataData.hh
  FrSimEventClassicIO.hh
  FrSimEventData.hh
  FrSummaryClassicIO.hh
  FrSummaryData.hh
  FrTableClassicIO.hh
  FrTableSerialIO.hh
  FrTableData.hh
  FrTOCClassicIO.hh
  FrTOCData.hh
  FrTOCAdcDataClassicIO.hh
  FrTOCConstants.hh
  FrTOCDataClassicIO.hh
  FrTOCEventClassicIO.hh
  FrTOCProcDataClassicIO.hh
  FrTOCSerDataClassicIO.hh
  FrTOCSimDataClassicIO.hh
  FrTOCSimEventClassicIO.hh
  FrTOCSummaryClassicIO.hh
  FrVectClassicIO.hh
  FrVectData.hh
  FrVectSerialIO.hh
  )

set( hh_HEADERS ${src_hdrs} )


cx_link_headers( ${HDR_DIR} ${hh_HEADERS} )
# cx_link_headers( ${HDR_DIR} ${nodist_hh_HEADERS} BUILT_HEADERS )

#------------------------------------------------------------------------

cx_install_header(
  FILES ${hh_HEADERS}
  DESTINATION "${hhdir}"
  )
