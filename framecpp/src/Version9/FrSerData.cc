//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018-2020 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version9/FrSerData.hh"
#if 0
#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version9/FrSE.hh"
#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/PTR_STRUCT.hh"
#endif /* 0 */

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------

FR_OBJECT_META_DATA_DEFINE_9( FrSerDataImpl,
                            FSI_FR_SER_DATA,
                            "FrSerData",
                            "Serial Data Structure" )

//=======================================================================
// FrSerData
//=======================================================================

namespace FrameCPP
{
    namespace Version_9
    {
        const FrSerData::name_type&
        FrSerData::GetNameSlow( ) const
        {
            return GetName( );
        }

        namespace FrSerDataImpl
        {
        } // namespace FrSerDataImpl

        FrSerData::FrSerData( Previous::FrSerData& Source,
                              Common::IStream*     Stream )
            : FrSerDataImpl::ClassicIO< FrSerData >( Source, Stream )
        {
        }

        FrSerData::FrSerData( const Data::name_type&      Name,
                              const Data::time_type&      Time,
                              const Data::sampleRate_type SampleRate )
        {
            Data::name = Name;
            Data::time = Time;
            Data::sampleRate = SampleRate;
        }

        FrSerData&
        FrSerData::Merge( const FrSerData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrSerData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

    } // namespace Version_9
} // namespace FrameCPP
