//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018-2020 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version9/FrSimData.hh"

#if 0
#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version9/FrSimData.hh"
#include "framecpp/Version9/FrSE.hh"
#include "framecpp/Version9/FrSH.hh"

#include "framecpp/Version9/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_9;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::IStream;
using FrameCPP::Common::OStream;
#endif /* 0 */

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------

FR_OBJECT_META_DATA_DEFINE_9( FrSimDataImpl,
                              FSI_FR_SIM_DATA,
                              "FrSimData",
                              "Simulated Data Structure" )

namespace FrameCPP
{
    namespace Version_9
    {
        //=======================================================================
        // FrSimData
        //=======================================================================
        FrSimData::FrSimData( Previous::FrSimData& Source,
                              istream_type*        Stream )
            : FrSimDataImpl::ClassicIO< FrSimData >( Source, Stream )
        {
        }

        FrSimData::FrSimData( const name_type&      Name,
                              const comment_type&   Comment,
                              const sampleRate_type SampleRate,
                              const fShift_type     FShift,
                              const phase_type      Phase,
                              const timeOffset_type TimeOffset )
        {
            Data::name = Name;
            Data::comment = Comment;
            Data::sampleRate = SampleRate;
            Data::fShift = FShift;
            Data::phase = Phase;
            Data::timeOffset = TimeOffset;
        }

        const std::string&
        FrSimData::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrSimData&
        FrSimData::Merge( const FrSimData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrSimData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

    } // namespace Version_9
} // namespace FrameCPP
