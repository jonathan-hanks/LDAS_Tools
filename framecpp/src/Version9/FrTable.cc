//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version9/FrameSpec.hh"
#include "framecpp/Version9/FrTable.hh"

using namespace FrameCPP::Version_9;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::IStream;
using FrameCPP::Common::OStream;

//=======================================================================
// Static
//=======================================================================

FR_OBJECT_META_DATA_DEFINE_9( FrTableImpl,
                              FSI_FR_TABLE,
                              "FrTable",
                              "Table Data Structure" )

//=======================================================================
//=======================================================================
FrTable::FrTable( )
{
}

FrTable::FrTable( const FrTable& Source )
{
    setName( Source.GetName( ) );
    AppendComment( Source.GetComment( ) );
    RefColumn( ) = Source.RefColumn( );
}

#if 0
FrTable::FrTable( const FrTableNPS& Source )
    : FrTableNPS( Source )
{
}
#endif /* 0 */

FrTable::FrTable( const name_type& name, nRow_type nrows )
{
    setName( name );
}

FrTable::FrTable( Previous::FrTable& Source, Common::IStream* Stream )
{
    setName( Source.GetName( ) );
    AppendComment( Source.GetComment( ) );

    if ( Stream )
    {
        //-------------------------------------------------------------------
        // Fix references
        //-------------------------------------------------------------------
        Stream->ReplaceRef( Source.RefColumn( ), RefColumn( ), m_refs.MAX_REF );
    }
}

FrTable::~FrTable( )
{
}

FrTable&
FrTable::Merge( const FrTable& RHS )
{
    throw Unimplemented( "FrTable& FrTable::Merge( const FrTable& RHS )",
                         DATA_FORMAT_VERSION,
                         __FILE__,
                         __LINE__ );
    return *this;
}

const std::string&
FrTable::GetNameSlow( ) const
{
    return GetName( );
}

bool
FrTable::operator==( const Common::FrameSpec::Object& RHS ) const
{
    return compare( *this, RHS );
}

FrTable&
FrTable::operator+=( const FrTable& RHS )
{
    throw Unimplemented( "FrTable& FrTable::operator+=( const FrTable& RHS )",
                         DATA_FORMAT_VERSION,
                         __FILE__,
                         __LINE__ );
}
