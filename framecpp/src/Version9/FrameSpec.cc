//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Version9/FrameSpec.hh"

#include "framecpp/Version9/PTR_STRUCT.hh"
#include "framecpp/Version9/StreamRef.hh"

#include "framecpp/Version9/FrAdcData.hh"
#include "framecpp/Version9/FrameH.hh"
#include "framecpp/Version9/FrDetector.hh"
#include "framecpp/Version9/FrEndOfFile.hh"
#include "framecpp/Version9/FrEndOfFrame.hh"
#include "framecpp/Version9/FrEvent.hh"
#include "framecpp/Version9/FrHeader.hh"
#include "framecpp/Version9/FrHistory.hh"
#include "framecpp/Version9/FrMsg.hh"
#include "framecpp/Version9/FrProcData.hh"
#include "framecpp/Version9/FrRawData.hh"
#include "framecpp/Version9/FrSE.hh"
#include "framecpp/Version9/FrSerData.hh"
#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/FrSimData.hh"
#include "framecpp/Version9/FrSimEvent.hh"
#include "framecpp/Version9/FrSummary.hh"
#include "framecpp/Version9/FrTable.hh"
#include "framecpp/Version9/FrTOC.hh"
#include "framecpp/Version9/FrVect.hh"

namespace
{
    const FrameCPP::Common::DetectorNames&
    dnt_init( )
    {
        static FrameCPP::Common::DetectorNames retval;

        //-----------------------------------------------------------------
        // Setup the DetectorNameTable from the appendix
        //-----------------------------------------------------------------
        typedef std::pair< int, int > dqb;

        retval( "TAMA_300", "TAMA 300", "T1:", dqb( 0, 1 ) );
        retval( "Virgo", "Virgo 3 km", "V1:", dqb( 4, 5 ) );
        retval( "GEO_600", "GEO 600", "G1:", dqb( 6, 7 ) );
        retval( "LHO_4k", "LIGO LHO 4 km", "H1:", dqb( 10, 11 ) );
        retval( "LLO_4k", "LIGO LLO 4 km", "L1:", dqb( 12, 13 ) );
        retval( "CIT_40", "Caltech 40 meters", "P1:", dqb( 14, 15 ) );
        retval.Freeze( );

        return retval;
    }
} // namespace

namespace FrameCPP
{
    namespace Version_9
    {
        static const bool Initialized = init_frame_spec( );

        const Common::DetectorNames& DetectorNameTable = dnt_init( );

        bool
        init_frame_spec( )
        {
            using Common::FrameSpec;

            static bool initialized = false;

            (void)Initialized; // Suppress unused warning message

            if ( !initialized )
            {
                //---------------------------------------------------------------
                // Local storage describing implementation of the frame spec
                //---------------------------------------------------------------
                std::unique_ptr< Common::FrameSpec::Info > info(
                    new Common::FrameSpec::Info( DATA_FORMAT_VERSION,
                                                 LIBRARY_MINOR_VERSION
                                                 /* , Verify :TODO: */ ) );

                info->FrameObject( new PTR_STRUCT );
                info->FrameObject( new StreamRef );

                info->FrameObject( new FrAdcData );
                info->FrameObject( new FrameH );
                info->FrameObject( new FrDetector );
                info->FrameObject( new FrEndOfFile );
                info->FrameObject( new FrEndOfFrame );
                info->FrameObject( new FrEvent );
                info->FrameObject( new FrHeader );
                info->FrameObject( new FrHistory );
                info->FrameObject( new FrMsg );
                info->FrameObject( new FrProcData );
                info->FrameObject( new FrRawData );
                info->FrameObject( new FrSE );
                info->FrameObject( new FrSerData );
                info->FrameObject( new FrSH );
                info->FrameObject( new FrSimData );
                info->FrameObject( new FrSimEvent );
                info->FrameObject( new FrSummary );
                info->FrameObject( new FrTable );
                info->FrameObject( new FrTOC );
                info->FrameObject( new FrVect );

                //---------------------------------------------------------------
                // Register with the stream manipulator
                //---------------------------------------------------------------
                FrameSpec::SpecInfo(
                    FrameSpec::version_type( DATA_FORMAT_VERSION ),
                    info.release( ) );
                initialized = true;
            }

            //-----------------------------------------------------------------
            // Initialization has been completed.
            //-----------------------------------------------------------------
            return true;
        }

    } // namespace Version_9
} // namespace FrameCPP
