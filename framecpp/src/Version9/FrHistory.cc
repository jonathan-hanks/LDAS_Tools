//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version9/FrameSpec.hh"
#include "framecpp/Version9/FrHistory.hh"
#include "framecpp/Version9/FrSE.hh"
#include "framecpp/Version9/FrSH.hh"
#include "framecpp/Version9/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
FR_OBJECT_META_DATA_DEFINE_9( FrHistoryImpl,
                              FSI_FR_HISTORY,
                              "FrHistory",
                              "History Structure" )

namespace FrameCPP
{
    namespace Version_9
    {
        //=======================================================================
        // FrHistory::fr_history_data_type
        //=======================================================================

        constexpr FrHistory::time_type DEFAULT_TIME = 0;

        //=======================================================================
        // FrHistory
        /// The constuction of the default FrHistory.
        //=======================================================================
        FrHistory::FrHistory( )
        {
            time = DEFAULT_TIME;
        }

        //-----------------------------------------------------------------------
        /// Constructs a new FrHistory based on a previously initialized
        /// FrHistory instance.
        //-----------------------------------------------------------------------
        FrHistory::FrHistory( const FrHistory& Source )
        {
            name = Source.name;
            time = Source.time;
            comment = Source.comment;
        }

        //-----------------------------------------------------------------------
        /// Construct a new instance with values for all important
        //-----------------------------------------------------------------------
        FrHistory::FrHistory( const std::string& Name,
                              time_type          Time,
                              const std::string& Comment )
        {
            name = Name;
            time = Time;
            comment = Comment;
        }

        //---------------------------------------------------------------
        /// Construct a new instance based upon reading a stream which
        /// implements an earlier version of the fame specification.
        //---------------------------------------------------------------
        FrHistory::FrHistory( Previous::FrHistory& Source,
                              istream_type*        Stream )
        {
            name = Source.GetName( );
            time = Source.GetTime( );
            comment = Source.GetComment( );
        }

        std::string
        FrHistory::ErrorInfo( ) const
        {
            std::ostringstream result;

            result << "FrameCPP::Version9::FrHistory: {Name: " << name
                   << " Time: " << time << "} ";
            return result.str( );
        }

        //-----------------------------------------------------------------
        /// \brief Return the name associate with the FrHistory
        /// structure.
        ///
        /// \return
        ///     The name associated with the FrHistory structure
        //-----------------------------------------------------------------
        const FrHistory::name_type&
        FrHistory::GetName( ) const
        {
            return Data::GetName( );
        }

        FrHistory&
        FrHistory::Merge( const FrHistory& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        const char*
        FrHistory::ObjectStructName( ) const
        {
            return StructName( );
        }

        namespace FrHistoryImpl
        {
            template <>
            bool
            ClassicIO< FrHistory >::
            operator==( const Common::FrameSpec::Object& RHS ) const
            {
                return Common::Compare( *this, RHS );
            }
        } // namespace FrHistoryImpl

    } // namespace Version_9
} // namespace FrameCPP
