//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/CheckSumFilter.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/Verify.hh"
#include "framecpp/Common/FrameSpec.tcc"

#include "framecpp/Version9/FrEndOfFile.hh"
#include "framecpp/Version9/FrHeader.hh"
#include "framecpp/Version9/FrTOC.hh"
#include "framecpp/Version9/FrSE.hh"
#include "framecpp/Version9/FrSH.hh"

#include "framecpp/Version9/FrameSpec.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::CheckSum;
using FrameCPP::Common::CheckSumFilter;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::OFrameStream;
using FrameCPP::Common::VerifyException;

namespace FrameCPP
{
    namespace Version_9
    {

        FrEndOfFile::FrEndOfFile( )
        {
        }

        FrEndOfFile::FrEndOfFile( istream_type& Stream )
        {
            //---------------------------------------------------------------------
            // Read data stream
            //---------------------------------------------------------------------
            Stream >> nFrames >> nBytes >> seekTOC >> chkSumTOC >>
                chkSumFrHeader;
            //---------------------------------------------------------------------
            // FrHeader checksum
            //---------------------------------------------------------------------
            try
            {
                using FrameCPP::Common::IFrameStream;

#if LM_DEBUG_INPUT
                std::cerr << "DEBUG:"
                          << " [ " << __LINE__ << ": " __FILE__ << " ]"
                          << std::endl;
#endif /* LM_DEBUG_INPUT */
                const FrHeader& hdr(
                    dynamic_cast< const FrHeader& >( Stream.GetFrHeader( ) ) );

                Common::CheckSumFilter checksum( CheckSum::CRC );
#if LM_DEBUG_INPUT
                std::cerr << "DEBUG:"
                          << " [ " << __LINE__ << ": " __FILE__ << " ]"
                          << std::endl;
#endif /* LM_DEBUG_INPUT */
                hdr.Filter( checksum );

#if LM_DEBUG_INPUT
                std::cerr << "DEBUG:"
                          << " [ " << __LINE__ << ": " __FILE__ << " ]"
                          << std::endl;
#endif /* LM_DEBUG_INPUT */
                chkSumFrHeader_type calc_chkSumFrHeader( checksum.Value( ) );

#if LM_DEBUG_INPUT
                std::cerr << "DEBUG:"
                          << " chkSumFrHeader: " << chkSumFrHeader
                          << " calc_chkSumFrHeader: " << calc_chkSumFrHeader
                          << " [ " << __LINE__ << ": " __FILE__ << " ]"
                          << std::endl;
#endif /* LM_DEBUG_INPUT */
                if ( chkSumFrHeader && calc_chkSumFrHeader &&
                     ( chkSumFrHeader != calc_chkSumFrHeader ) )
                {
                    std::ostringstream msg;

                    msg << "FrHeader (via FrEndOfFile): "
                        << CheckSum::FormatError( chkSumFrHeader,
                                                  calc_chkSumFrHeader );

                    throw VerifyException( VerifyException::CHECKSUM_ERROR,
                                           msg.str( ) );
                }
            }
            catch ( const VerifyException& Exception )
            {
                throw;
            }
            catch ( ... )
            {
#if LM_DEBUG_INPUT
                std::cerr << "DEBUG:"
                          << " exception ..."
                          << " [ " << __LINE__ << ": " __FILE__ << " ]"
                          << std::endl;
#endif /* LM_DEBUG_INPUT */
            }
            //---------------------------------------------------------------------
            // Structure checksum
            //---------------------------------------------------------------------
            {
                chkSum_type checksum = 0;

                if ( Stream.GetCheckSumObject( ) &&
                     Stream.GetCheckSumObject( )->GetChecksum( ) )
                {
                    checksum =
                        Stream.GetCheckSumObject( )->GetChecksum( )->value( );
                }

                Stream >> chkSum;
#if LM_DEBUG_INPUT
                std::cerr << "DEBUG:"
                          << " chkSum: " << chkSum << " checksum: " << checksum
                          << " [ " << __LINE__ << ": " __FILE__ << " ]"
                          << std::endl;
#endif /* LM_DEBUG_INPUT */
                if ( chkSum && checksum && ( chkSum != checksum ) )
                {
                    std::ostringstream msg;

                    msg << "FrEndOfFile Object: "
                        << CheckSum::FormatError( chkSum, checksum );

                    throw VerifyException( VerifyException::CHECKSUM_ERROR,
                                           msg.str( ) );
                }
            }
            //---------------------------------------------------------------------
            // File checksum
            //---------------------------------------------------------------------
            {
                chkSumFile_type checksum = 0;

                CheckSumFilter* csf( Stream.GetCheckSumFile( ) );
                if ( csf )
                {
                    //-----------------------------------------------------------------
                    // Calculate the value to end checksum calculation.
                    //-----------------------------------------------------------------
                    checksum = csf->Value( );
                }
                Stream >> chkSumFile;
#if LM_DEBUG_INPUT
                std::cerr << "DEBUG:"
                          << " chkSumFile: " << chkSumFile
                          << " checksum: " << checksum << " [ " << __LINE__
                          << ": " __FILE__ << " ]" << std::endl;
#endif /* LM_DEBUG_INPUT */
                if ( checksum && chkSumFile && ( checksum != chkSumFile ) )
                {
                    std::ostringstream msg;

                    msg << "File: "
                        << CheckSum::FormatError( chkSumFile, checksum );

                    throw VerifyException( VerifyException::CHECKSUM_ERROR,
                                           msg.str( ) );
                }
            }
        }

        FrEndOfFile::~FrEndOfFile( )
        {
        }

        FrEndOfFile::chkSum_cmn_type
        FrEndOfFile::Filter( const istream_type& Stream,
                             Common::CheckSum&   Filt,
                             chkType_cmn_type&   Type,
                             void*               Buffer,
                             INT_8U              Size ) const
        {
            chkSum_type value;

            if ( Bytes( Stream ) != Size )
            {
                std::ostringstream msg;
                msg << "FrEndOfFile::Filter: "
                    << " Expected a buffer of size: " << Bytes( Stream )
                    << " but received a buffer of size: " << Size;
                throw std::length_error( msg.str( ) );
            }
            const size_t chkSumOffset( Size - sizeof( chkSumFile_type ) );

            Filt.calc( Buffer, chkSumOffset );

            std::copy( (const char*)Buffer + chkSumOffset,
                       (const char*)Buffer + chkSumOffset + sizeof( value ),
                       (char*)&value );

            if ( Stream.ByteSwapping( ) )
            {
                reverse< sizeof( value ) >( &value, 1 );
            }

            Type = FrameCPP::Common::CheckSum::UNSET;

            return value;
        }

        FrEndOfFile::nBytes_cmn_type
        FrEndOfFile::NBytes( ) const
        {
            return nBytes;
        }

        FrEndOfFile::nFrames_cmn_type
        FrEndOfFile::NFrames( ) const
        {
            return nFrames;
        }

        void
        FrEndOfFile::VerifyObject( Common::Verify&       Verifier,
                                   Common::IFrameStream& Stream ) const
        {
            Verifier.SeenFrEndOfFile( true );
            Verifier.ExamineFrHeaderChecksum( chkSumFrHeader );
            Verifier.ExamineFrEndOfFileChecksum(
                Stream, Verifier.ChecksumScheme( ), chkSumFile );
        }

        bool
        FrEndOfFile::operator==( const FrEndOfFile& RHS ) const
        {
            return (
                ( nFrames == RHS.nFrames ) && ( nBytes == RHS.nBytes ) &&
                ( seekTOC == RHS.seekTOC ) && ( chkSumTOC == RHS.chkSumTOC ) &&
                ( chkSumFrHeader == RHS.chkSumFrHeader ) &&
                ( chkSum == RHS.chkSum ) && ( chkSumFile == RHS.chkSumFile ) );
        }

        bool
        FrEndOfFile::operator==( const Common::FrameSpec::Object& RHS ) const
        {
            return Common::Compare( *this, RHS );
        }

        FrEndOfFile::chkSumFrHeader_type
        FrEndOfFile::getFrHeaderChecksum( ostream_type& Stream ) const
        {
            using namespace FrameCPP::Version_9;

            chkSumFrHeader_type retval = 0;

            const FrHeader* hdr = Stream.GetFrHeader< FrHeader >( );

            if ( hdr )
            {
                retval = hdr->Checksum( );
            }

            return retval;
        }
    } // namespace Version_9
} // namespace FrameCPP
