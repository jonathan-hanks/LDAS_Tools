//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/FrHistory.hh"

#include "framecppc/FrameH.h"
#include "framecppc/FrHistory.h"

#include "FrameCInternal.hh"
#include "FrHistoryInternal.hh"

using FrameC::Set;

namespace FrameC
{
    template <>
    const char*
    Pointers::Name< POINTER_FR_HISTORY >( )
    {
        return "FrHistory";
    }

} // namespace FrameC

fr_history_t*
FrameCFrHistoryAlloc( FrameCError**     Error,
                      const char*       Name,
                      fr_history_time_t Time,
                      const char*       Comment )
{
    Set( Error );

    FrameC::FrHistory*                   retval = (FrameC::FrHistory*)NULL;
    std::unique_ptr< FrameC::FrHistory > tmp;

    try
    {
        tmp.reset( new FrameC::FrHistory( Name, Time, Comment ) );
    }
    catch ( const std::exception& Exception )
    {
        Set( Error, FRAMEC_ERRNO_FR_HISTORY_ALLOC_ERROR, Exception.what( ) );
        tmp.reset( (FrameC::FrHistory*)NULL );
    }

    if ( tmp.get( ) )
    {
        FrameC::Handle::Deposit( tmp.get( ) );
        retval = tmp.release( );
    }
    return ( reinterpret_cast< fr_history_t* >( retval ) );
}

int
FrameCFrHistoryFree( FrameCError** Error, fr_history_t* History )
{
    int retval = true;

    Set( Error );

    FrameC::Handle::Free( Error,
                          History,
                          FrameC::POINTER_FR_HISTORY,
                          FRAMEC_ERRNO_FR_HISTORY_FREE_ERROR );

    if ( *Error )
    {
        retval = false;
    }

    return retval;
}

namespace FrameC
{
    FrHistory::FrHistory( ) : Handle( POINTER_FR_HISTORY )
    {
    }

    FrHistory::FrHistory( const char* Name, time_t Time, const char* Comment )
        : Handle( POINTER_FR_HISTORY )
    {
        m_data.reset( new FrameCPP::FrHistory( Name, Time, Comment ) );
    }
} // namespace FrameC
