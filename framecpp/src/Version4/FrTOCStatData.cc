//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version4/FrameSpec.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"
#include "framecpp/Version4/FrTOC.hh"

#include "framecpp/Version4/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::TOCInfo;

namespace FrameCPP
{
    namespace Version_4
    {
        //===================================================================
        //===================================================================
        FrTOCStatData::FrTOCStatData( )
        {
        }

        FrTOCStatData::FrTOCStatData( Common::IStream& Stream )
        {
            nstat_type nstat;
            Stream >> nstat;
            if ( nstat && ( nstat != ~nstat_type( 0 ) ) )
            {
                for ( nstat_type n = 0; n != nstat; ++n )
                {
                    //-------------------------------------------------------------
                    // Read in the information
                    //-------------------------------------------------------------
                    name_type           name;
                    detector_type       detector;
                    stat_type           stat;
                    nstat_instance_type nstat_instance;

                    Stream >> name >> detector >> nstat_instance;

                    if ( nstat_instance )
                    {
                        std::vector< tstart_type >  tstart( nstat_instance );
                        std::vector< tend_type >    tend( nstat_instance );
                        std::vector< version_type > version( nstat_instance );
                        std::vector< positionStat_type > positionStat(
                            nstat_instance );

                        Stream >> tstart >> tend >> version >> positionStat;

                        //-----------------------------------------------------------
                        // Move into structure.
                        //-----------------------------------------------------------
                        stat.stat_instances.resize( nstat_instance );
                        stat.detector = detector;
                        for ( nstat_instance_type cur = 0;
                              cur != nstat_instance;
                              ++cur )
                        {
                            stat.stat_instances[ cur ].tStart = tstart[ cur ];
                            stat.stat_instances[ cur ].tEnd = tend[ cur ];
                            stat.stat_instances[ cur ].version = version[ cur ];
                            stat.stat_instances[ cur ].positionStat =
                                positionStat[ cur ];
                        }
                    } // if ( nstat_instance )

                    m_info[ name ] = stat;
                } // if ( nstat_instance )
            }
        }

        void
        FrTOCStatData::Description( Common::Description& Desc )
        {
            Desc( FrSE( "nStatType",
                        "INT_4U",
                        "Number of static data block types in the file." ) );
            Desc( FrSE( "nameStat", "STRING", "FrStatData name" ) );
            Desc( FrSE( "detector", "STRING", "Detector name" ) );
            Desc( FrSE( "nStatInstance",
                        "INT_4U",
                        "Number of instance for this FrStatData" ) );
            Desc( FrSE( "tStart",
                        "*INT_4U",
                        "Array of GPS integer start times,"
                        " in seconds (size of nStatInstance)" ) );
            Desc( FrSE( "tEnd",
                        "*INT_4U",
                        "Array of GPS integer end times,"
                        " in seconds (size of nStatInstance)" ) );
            Desc( FrSE( "version",
                        "*INT_4U",
                        "Array of version time"
                        " (size of nStatInstance)" ) );
            Desc( FrSE( "postionStat",
                        "*INT_8U",
                        "Array of FrStatData positions from beginning of file"
                        " (size of nStatInstance)" ) );
        }

        void
        FrTOCStatData::QueryStatData( const Common::TOCInfo& Info,
                                      INT_4U                 FrameOffset,
                                      INT_8U                 Position )
        {
            STRING name;
            STRING detector;
            INT_4U start;
            INT_4U end;
            INT_4U version;

            Info.TOCQuery( TOCInfo::IC_NAME,
                           TOCInfo::DataType( name ),
                           &name,
                           TOCInfo::IC_DETECTOR,
                           TOCInfo::DataType( detector ),
                           &detector,
                           TOCInfo::IC_START,
                           TOCInfo::DataType( start ),
                           &start,
                           TOCInfo::IC_END,
                           TOCInfo::DataType( end ),
                           &end,
                           TOCInfo::IC_VERSION,
                           TOCInfo::DataType( version ),
                           &version,
                           TOCInfo::IC_EOQ );

            stat_instance_type si;

            si.tStart = start;
            si.tEnd = end;
            si.version = version;
            si.positionStat = Position;

            stat_type& i( m_info[ name ] );
            if ( ( i.detector.length( ) ) && ( i.detector != detector ) )
            {
                std::ostringstream msg;

                msg << "FrStatData with the name '" << name
                    << "' is associated with detector '" << i.detector
                    << "' and detector '" << detector << "'";
                throw std::runtime_error( msg.str( ) );
            }
            i.detector = detector;
            i.stat_instances.push_back( si );
        }

        void
        FrTOCStatData::write( Common::OStream& Stream ) const
        {
            if ( m_info.size( ) > 0 )
            {
                Stream << nstat_type( m_info.size( ) );
                for ( stat_container_type::const_iterator cur = m_info.begin( ),
                                                          last = m_info.end( );
                      cur != last;
                      ++cur )
                {
                    const nstat_type c( cur->second.stat_instances.size( ) );

                    Stream << cur->first << cur->second.detector << c;
                    if ( c )
                    {
                        //-----------------------------------------------------------
                        // Flatten the data
                        //-----------------------------------------------------------
                        std::vector< tstart_type >       tstart( c );
                        std::vector< tend_type >         tend( c );
                        std::vector< version_type >      version( c );
                        std::vector< positionStat_type > positionStat( c );
                        nstat_type                       x( 0 );

                        for ( stat_instance_container_type::const_iterator
                                  cur_si = cur->second.stat_instances.begin( ),
                                  last_si = cur->second.stat_instances.end( );
                              cur_si != last_si;
                              ++cur_si, ++x )
                        {
                            tstart[ x ] = cur_si->tStart;
                            tend[ x ] = cur_si->tEnd;
                            version[ x ] = cur_si->version;
                            positionStat[ x ] = cur_si->positionStat;
                        }

                        Stream << tstart << tend << version << positionStat;
                    }
                }
            }
            else
            {
                Stream << nstat_type( FrTOC::NO_DATA_AVAILABLE );
            }
        }

    } // namespace Version_4
} // namespace FrameCPP
