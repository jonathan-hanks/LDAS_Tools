//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version4/FrSimData.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"

#include "framecpp/Version4/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_SIM_DATA;

namespace FrameCPP
{
    namespace Version_4
    {
        //=======================================================================
        // FrSimData
        //=======================================================================
        FrSimData::FrSimData( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
        }

        FrSimData::FrSimData( const FrSimData& Source )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrSimDataStorage( Source ),
              FrSimDataRefs( Source ), Common::TOCInfo( Source )
        {
        }

        FrSimData::FrSimData( const std::string& Name,
                              const std::string& Comment,
                              REAL_4             SampleRate )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrSimDataStorage( Name, Comment, SampleRate )
        {
        }

        FrSimData::FrSimData( Previous::FrSimData& Source,
                              istream_type*        Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.name = Source.GetName( );
            m_data.comment = Source.GetComment( );
            m_data.sampleRate = Source.GetSampleRate( );

            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                Stream->ReplaceRef( RefData( ),
                                    Source.RefData( ),
                                    Previous::FrSimData::MAX_REF );
                Stream->ReplaceRef( RefInput( ),
                                    Source.RefInput( ),
                                    Previous::FrSimData::MAX_REF );
            }
        }

        FrSimData::FrSimData( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data( Stream );
            m_refs( Stream );
            Stream.Next( this );
        }

        FrameCPP::cmn_streamsize_type
        FrSimData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.Bytes( Stream ) + m_refs.Bytes( Stream ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        FrSimData*
        FrSimData::Create( istream_type& Stream ) const
        {
            return new FrSimData( Stream );
        }

        const char*
        FrSimData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrSimData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrSimData::StructName( ),
                           s_object_id,
                           "Simulated Data Structure" ) );

                ret( FrSE( "name", "STRING" ) );
                ret( FrSE( "comment", "STRING" ) );
                ret( FrSE( "sampleRate", "REAL_4" ) );

                ret(
                    FrSE( "data", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "input",
                           PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "table",
                           PTR_STRUCT::Desc( FrTable::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrSimData::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrSimData::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
            WriteNext( Stream );
        }

        const std::string&
        FrSimData::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrSimData&
        FrSimData::Merge( const FrSimData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        void
        FrSimData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        bool
        FrSimData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrSimData::demote_ret_type
        FrSimData::demote( INT_2U          Target,
                           demote_arg_type Obj,
                           istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                demote_arg_type retval( new Previous::FrSimData(
                    GetName( ), GetComment( ), GetSampleRate( ) ) );
                if ( Stream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
#if WORKING
                    Stream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    Stream->ReplaceRef(
                        retval->RefInput( ), RefInput( ), MAX_REF );
                    Stream->RemoveResolver( &( RefTable( ) ), MAX_REF );
#else
                    assert( 0 );
#endif
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrSimData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrSimData::promote_ret_type
        FrSimData::promote( INT_2U           Target,
                            promote_arg_type Obj,
                            istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_4
} // namespace FrameCPP
