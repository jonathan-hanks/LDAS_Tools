//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version4/FrSimEvent.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"

#include "framecpp/Version4/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_SIM_EVENT;

namespace FrameCPP
{
    namespace Version_4
    {
        //=======================================================================
        // FrSimEventNPS::storage_type
        //=======================================================================
        FrameCPP::cmn_streamsize_type
        FrSimEventNPS::storage_type::Bytes( ) const
        {
            FrameCPP::cmn_streamsize_type retval = name.Bytes( ) +
                comment.Bytes( ) + inputs.Bytes( ) + Common::Bytes( GTime ) +
                sizeof( timeBefore ) + sizeof( timeAfter ) +
                sizeof( amplitude );

            return retval;
        }

        void
        FrSimEventNPS::storage_type::operator( )( Common::IStream& Stream )
        {
            Stream >> name >> comment >> inputs >> GTime >> timeBefore >>
                timeAfter >> amplitude;
        }

        void
        FrSimEventNPS::storage_type::
        operator( )( Common::OStream& Stream ) const
        {
            Stream << name << comment << inputs << GTime << timeBefore
                   << timeAfter << amplitude;
        }

        //=======================================================================
        // FrSimEvent
        //=======================================================================
        FrSimEvent::FrSimEvent( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
        }

        FrSimEvent::FrSimEvent( const FrSimEvent& Source )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrSimEventNPS( Source ),
              FrSimEventPS( Source ), Common::TOCInfo( Source )
        {
        }

        FrSimEvent::FrSimEvent( const std::string& name,
                                const std::string& comment,
                                const std::string& inputs,
                                const GPSTime&     time,
                                const REAL_4       timeBefore,
                                const REAL_4       timeAfter,
                                const REAL_4       amplitude )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrSimEventNPS( name,
                             comment,
                             inputs,
                             time,
                             timeBefore,
                             timeAfter,
                             amplitude )

        {
        }

        FrSimEvent::FrSimEvent( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrSimEvent*
        FrSimEvent::Create( istream_type& Stream ) const
        {
            return new FrSimEvent( Stream );
        }

        const std::string&
        FrSimEvent::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrSimEvent&
        FrSimEvent::Merge( const FrSimEvent& RHS )
        {
            throw Unimplemented(
                "FrSimEvent& FrSimEvent::Merge( const FrSimEvent& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        const char*
        FrSimEvent::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrSimEvent::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrSimEvent::StructName( ),
                           s_object_id,
                           "Event Data Structure" ) );

                FrSimEventNPS::storage_type::Describe< FrSE >( ret );
                refs_type::Describe< FrSE >( ret );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrSimEvent::StructName( ) ),
                           "" ) );
            }

            return &ret;
        }

        void
        FrSimEvent::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_GTIME_S:
                        *data = GetGTime( ).GetSeconds( );
                        break;
                    case TOCInfo::IC_GTIME_N:
                        *data = GetGTime( ).GetNanoseconds( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_4:
                {
                    REAL_4* data = va_arg( vl, REAL_4* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_8:
                {
                    REAL_8* data = va_arg( vl, REAL_8* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        void
        FrSimEvent::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
            WriteNext( Stream );
        }

        bool
        FrSimEvent::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrSimEvent::demote_ret_type
        FrSimEvent::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            else if ( Target < DATA_FORMAT_VERSION )
            {
                //-------------------------------------------------------------------
                // Object does not exist prier to Version 4
                //-------------------------------------------------------------------
                return demote_ret_type( );
            }
            std::ostringstream msg;
            msg << "Object* FrSimEvent::demote( " << Target
                << ", Object* Obj ) const";
            throw Unimplemented(
                msg.str( ), DATA_FORMAT_VERSION, __FILE__, __LINE__ );
        }

        FrSimEvent::promote_ret_type
        FrSimEvent::promote( INT_2U           Target,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_4
} // namespace FrameCPP
