//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version4/FrAdcData.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"

#include "framecpp/Version4/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

using std::ceil;
using std::floor;

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_ADC_DATA;

namespace FrameCPP
{
    namespace Version_4
    {
        //-----------------------------------------------------------------------
        //-----------------------------------------------------------------------

        const FrAdcDataNPS::bias_type FrAdcDataNPS::DEFAULT_BIAS =
            FR_ADC_DATA_DEFAULT_BIAS;
        const FrAdcDataNPS::slope_type FrAdcDataNPS::DEFAULT_SLOPE =
            FR_ADC_DATA_DEFAULT_SLOPE;
        const FrAdcDataNPS::fShift_type FrAdcDataNPS::DEFAULT_FSHIFT =
            FR_ADC_DATA_DEFAULT_FSHIFT;
        const FrAdcDataNPS::timeOffsetS_type
            FrAdcDataNPS::DEFAULT_TIME_OFFSET_S =
                FR_ADC_DATA_DEFAULT_TIME_OFFSET_S;
        const FrAdcDataNPS::timeOffsetN_type
            FrAdcDataNPS::DEFAULT_TIME_OFFSET_N =
                FR_ADC_DATA_DEFAULT_TIME_OFFSET_N;
        const FrAdcDataNPS::dataValid_type FrAdcDataNPS::DEFAULT_DATA_VALID =
            FR_ADC_DATA_DEFAULT_DATA_VALID;

        FrAdcDataNPS::FrAdcDataNPS( )
            : bias( DEFAULT_BIAS ), slope( DEFAULT_SLOPE ),
              units( FR_ADC_DATA_DEFAULT_UNITS( ) ),
              timeOffsetS( DEFAULT_TIME_OFFSET_S ),
              timeOffsetN( DEFAULT_TIME_OFFSET_N ), fShift( DEFAULT_FSHIFT ),
              dataValid( DEFAULT_DATA_VALID )
        {
        }

        FrAdcDataNPS::FrAdcDataNPS( Common::IStream& Stream )
        {
            Stream >> name >> comment >> channelGroup >> channelNumber >>
                nBits >> bias >> slope >> units >> sampleRate >> timeOffsetS >>
                timeOffsetN >> fShift >> dataValid;
        }

        FrAdcDataNPS::FrAdcDataNPS( const Previous::FrAdcData& Source,
                                    Common::IStream*           Stream )
            : name( Source.GetName( ) ), comment( Source.GetComment( ) ),
              channelGroup( Source.GetCrate( ) ),
              channelNumber( Source.GetChannel( ) ),
              nBits( Source.GetNBits( ) ), bias( Source.GetBias( ) ),
              slope( Source.GetSlope( ) ), units( Source.GetUnits( ) ),
              sampleRate( Source.GetSampleRate( ) ),
              timeOffsetS( Source.GetTimeOffsetS( ) ),
              timeOffsetN( Source.GetTimeOffsetN( ) ),
              fShift( Source.GetFShift( ) ), dataValid( Source.GetOverRange( ) )
        {
        }

        const std::string&
        FrAdcDataNPS::GetName( ) const
        {
            return name;
        }

        //-----------------------------------------------------------------------
        //
        //-----------------------------------------------------------------------

        FrAdcData::FrAdcData( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
        }

        FrAdcData::FrAdcData( const FrAdcData& Source )
            : nps_type( Source ),
              ps_type( Source ), FrameSpec::Object( s_object_id,
                                                    StructDescription( ) ),
              Common::TOCInfo( Source )
        {
        }

        FrAdcData::FrAdcData( const std::string& Name,
                              channelGroup_type  Group,
                              channelNumber_type Channel,
                              nBits_type         NBits,
                              sampleRate_type    SampleRate,
                              bias_type          Bias,
                              slope_type         Slope,
                              const std::string& Units,
                              fShift_type        FShift,
                              timeOffsetS_type   TimeOffsetS,
                              timeOffsetN_type   TimeOffsetN,
                              dataValid_type     DataValid )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            name = Name;
            channelGroup = Group;
            channelNumber = Channel;
            nBits = NBits;
            sampleRate = SampleRate;
            bias = Bias;
            slope = Slope;
            units = Units;
            fShift = FShift;
            timeOffsetS = TimeOffsetS;
            timeOffsetN = TimeOffsetN;
            dataValid = DataValid;
        }

        FrAdcData::FrAdcData( Previous::FrAdcData& Source,
                              istream_type*        Stream )
            : nps_type( Source, Stream ),
              ps_type( Source, Stream ), FrameSpec::Object(
                                             s_object_id, StructDescription( ) )
        {
        }

        FrAdcData::FrAdcData( istream_type& Stream )
            : nps_type( Stream ),
              ps_type( Stream ), FrameSpec::Object( s_object_id,
                                                    StructDescription( ) )
        {
            Stream.Next( this );
        }

        FrAdcData::~FrAdcData( )
        {
        }

        FrameCPP::cmn_streamsize_type
        FrAdcData::Bytes( const Common::StreamBase& Stream ) const
        {
            return nps_type::bytes( Stream ) + ps_type::Bytes( Stream ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        FrAdcData*
        FrAdcData::Create( istream_type& Stream ) const
        {
            return new FrAdcData( Stream );
        }

        const std::string&
        FrAdcData::GetName( ) const
        {
            return name;
        }

        FrAdcData&
        FrAdcData::Merge( const FrAdcData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        const char*
        FrAdcData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrAdcData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrAdcData::StructName( ),
                           s_object_id,
                           "AdcData  Data Structure" ) );

                nps_type::structDescription< Description, FrSE >( ret );
                ps_type::structDescription< Description, FrSE >(
                    ret, PTR_STRUCT::Desc( FrVect::StructName( ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrAdcData::StructName( ) ),
                           "" ) );
            }

            return &ret;
        }

        void
        FrAdcData::Write( ostream_type& Stream ) const
        {
            try
            {
                Stream << name << comment << channelGroup << channelNumber
                       << nBits << bias << slope << units << sampleRate
                       << timeOffsetS << timeOffsetN << fShift << dataValid
                       << data << aux;
                WriteNext( Stream );
            }
            catch ( const std::exception& Exception )
            {
                std::cerr << "DEBUG: Error Writing FrAdcData: " << name
                          << " exception: " << Exception.what( ) << std::endl;
                throw; // rethrow
            }
        }

        void
        FrAdcData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /* WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_2U:
                {
                    INT_2U* data = va_arg( vl, INT_2U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_DATA_VALID:
                        *data = GetDataValid( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_CHANNEL_ID:
                        *data = GetChannelNumber( );
                        break;
                    case TOCInfo::IC_GROUP_ID:
                        *data = GetChannelGroup( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /* WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrAdcData::demote_ret_type
        FrAdcData::demote( INT_2U          Target,
                           demote_arg_type Obj,
                           istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------

                // Do actual down conversion
                demote_ret_type retval(
                    new Previous::FrAdcData( GetName( ),
                                             GetChannelGroup( ),
                                             GetChannelNumber( ),
                                             GetNBits( ),
                                             GetSampleRate( ),
                                             GetBias( ),
                                             GetSlope( ),
                                             GetUnits( ),
                                             GetFShift( ),
                                             GetTimeOffsetS( ),
                                             GetTimeOffsetN( ),
                                             GetDataValid( ) ) );
                reinterpret_cast< Previous::FrAdcData* >( retval.get( ) )
                    ->AppendComment( GetComment( ) );
                if ( Stream )
                {
#if WORKING
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    Stream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    Stream->ReplaceRef( retval->RefAux( ), RefAux( ), MAX_REF );
#else
                    assert( 0 );
#endif
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrAdcData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrAdcData::promote_ret_type
        FrAdcData::promote( INT_2U           Target,
                            promote_arg_type Obj,
                            istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

        bool
        FrAdcData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

    } // namespace Version_4
} // namespace FrameCPP
