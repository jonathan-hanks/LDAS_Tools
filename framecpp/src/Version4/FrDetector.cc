//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version4/FrameSpec.hh"
#include "framecpp/Version4/FrDetector.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"

#include "framecpp/Version4/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

inline void
correct( INT_2S& LongitudeD,
         INT_2S& LongitudeM,
         REAL_4& LongitudeS,
         INT_2S& LatitudeD,
         INT_2S& LatitudeM,
         REAL_4& LatitudeS )
{
    if ( ( LatitudeD == -119 ) && ( LongitudeD == 46 ) )
    {
        // Hanford
        LongitudeD = -119;
        LongitudeM = 24;
        LongitudeS = 27.565704;

        LatitudeD = 46;
        LatitudeM = 27;
        LatitudeS = 18.52812;
    }
    else if ( ( LatitudeD == -90 ) && ( LongitudeD == 30 ) )
    {
        // Livingston
        LongitudeD = -90;
        LongitudeM = 46;
        LongitudeS = 27.265404;

        LatitudeD = 30;
        LatitudeM = 33;
        LatitudeS = 46.419588;
    }
}

//=======================================================================
// FrDetector::fr_detector_data_type
//=======================================================================

static const INT_2U MAX_REF = 2;

namespace FrameCPP
{
    namespace Version_4
    {
        bool
        FrDetector::fr_detector_data_type::
        operator==( const fr_detector_data_type& RHS ) const
        {
#define CMP__( X ) ( X == RHS.X )

            return ( ( &RHS == this ) ||
                     ( CMP__( name ) && CMP__( longitudeD ) &&
                       CMP__( longitudeM ) && CMP__( longitudeS ) &&
                       CMP__( latitudeD ) && CMP__( latitudeM ) &&
                       CMP__( latitudeS ) && CMP__( elevation ) &&
                       CMP__( armXazimuth ) && CMP__( armYazimuth ) &&
                       CMP__( more ) && CMP__( moreTable ) ) );

#undef CMP__
        }

        //=======================================================================
        // FrDetector
        //=======================================================================
        FrDetector::FrDetector( ) : Common::FrDetector( StructDescription( ) )
        {
            m_data.longitudeD = 0;
            m_data.longitudeM = 0;
            m_data.longitudeS = 0.0;
            m_data.latitudeD = 0;
            m_data.latitudeM = 0;
            m_data.latitudeS = 0.0;
            m_data.elevation = 0.0;
            m_data.armXazimuth = 0.0;
            m_data.armYazimuth = 0.0;
        }

        FrDetector::FrDetector( const std::string& Name,
                                const INT_2S       LongitudeD,
                                const INT_2S       LongitudeM,
                                const REAL_4       LongitudeS,
                                const INT_2S       LatitudeD,
                                const INT_2S       LatitudeM,
                                const REAL_4       LatitudeS,
                                const REAL_4       Elevation,
                                const REAL_4       ArmXazimuth,
                                const REAL_4       ArmYazimuth )
            : Common::FrDetector( StructDescription( ) )
        {
            m_data.name = Name;
            m_data.longitudeD = LongitudeD;
            m_data.longitudeM = LongitudeM;
            m_data.longitudeS = LongitudeS;
            m_data.latitudeD = LatitudeD;
            m_data.latitudeM = LatitudeM;
            m_data.latitudeS = LatitudeS;
            m_data.elevation = Elevation;
            m_data.armXazimuth = ArmXazimuth;
            m_data.armYazimuth = ArmYazimuth;
        }

        FrDetector::FrDetector( const FrDetector& detector )
            : Common::FrDetector( StructDescription( ) ), Common::TOCInfo(
                                                              detector )
        {
            AT( );
            m_data.name = detector.m_data.name;
            AT( );
            m_data.longitudeD = detector.m_data.longitudeD;
            AT( );
            m_data.longitudeM = detector.m_data.longitudeM;
            AT( );
            m_data.longitudeS = detector.m_data.longitudeS;
            AT( );
            m_data.latitudeD = detector.m_data.latitudeD;
            AT( );
            m_data.latitudeM = detector.m_data.latitudeM;
            AT( );
            m_data.latitudeS = detector.m_data.latitudeS;
            AT( );
            m_data.elevation = detector.m_data.elevation;
            AT( );
            m_data.armXazimuth = detector.m_data.armXazimuth;
            AT( );
            m_data.armYazimuth = detector.m_data.armYazimuth;
            AT( );
            m_data.more = detector.m_data.more;
            AT( );
            m_data.moreTable = detector.m_data.moreTable;
            AT( );
        }

        FrDetector::FrDetector( Previous::FrDetector& Source,
                                istream_type*         Stream )
            : Common::FrDetector( StructDescription( ) )
        {
            m_data.name = Source.GetName( );
            m_data.longitudeD = Source.GetLongitudeD( );
            m_data.longitudeM = Source.GetLongitudeM( );
            m_data.longitudeS = Source.GetLongitudeS( );
            m_data.latitudeD = Source.GetLatitudeD( );
            m_data.latitudeM = Source.GetLatitudeM( );
            m_data.latitudeS = Source.GetLatitudeS( );
            m_data.elevation = Source.GetElevation( );
            m_data.armXazimuth = Source.GetArmXazimuth( );
            m_data.armYazimuth = Source.GetArmYazimuth( );

            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                Stream->ReplaceRef( RefMore( ), Source.RefMore( ), MAX_REF );
            }
        }

        FrDetector::FrDetector( istream_type& Stream )
            : Common::FrDetector( StructDescription( ) )
        {
            Stream >> m_data.name >> m_data.longitudeD >> m_data.longitudeM >>
                m_data.longitudeS >> m_data.latitudeD >> m_data.latitudeM >>
                m_data.latitudeS >> m_data.elevation >> m_data.armXazimuth >>
                m_data.armYazimuth >> m_data.more >> m_data.moreTable;

            correct( m_data.longitudeD,
                     m_data.longitudeM,
                     m_data.longitudeS,
                     m_data.latitudeD,
                     m_data.latitudeM,
                     m_data.latitudeS );
        }

        const std::string&
        FrDetector::GetName( ) const
        {
            return m_data.name;
        }

        FrameCPP::cmn_streamsize_type
        FrDetector::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( m_data.longitudeD ) +
                sizeof( m_data.longitudeM ) + sizeof( m_data.longitudeS ) +
                sizeof( m_data.latitudeD ) + sizeof( m_data.latitudeM ) +
                sizeof( m_data.latitudeS ) + sizeof( m_data.elevation ) +
                sizeof( m_data.armXazimuth ) + sizeof( m_data.armYazimuth ) +
                Stream.PtrStructBytes( ) // more
                + Stream.PtrStructBytes( ) // moreTable
                ;
        }

        FrDetector*
        FrDetector::Create( istream_type& Stream ) const
        {
            return new FrDetector( Stream );
        }

        const char*
        FrDetector::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrDetector::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrDetector::StructName( ),
                           FrDetector::STRUCT_ID,
                           "Detector Data Structure" ) );
                ret( FrSE( "name", "STRING" ) );
                ret( FrSE( "longitudeD", "INT_2S" ) );
                ret( FrSE( "longitudeM", "INT_2S" ) );
                ret( FrSE( "longitudeS", "REAL_4" ) );
                ret( FrSE( "latitudeD", "INT_2S" ) );
                ret( FrSE( "latitudeM", "INT_2S" ) );
                ret( FrSE( "latitudeS", "REAL_4" ) );
                ret( FrSE( "elevation", "REAL_4" ) );
                ret( FrSE( "armXazimuth", "REAL_4" ) );
                ret( FrSE( "armYazimuth", "REAL_4" ) );

                ret(
                    FrSE( "more", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "moreTable",
                           PTR_STRUCT::Desc( FrTable::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrDetector::Write( ostream_type& Stream ) const
        {
            Stream << m_data.name << m_data.longitudeD << m_data.longitudeM
                   << m_data.longitudeS << m_data.latitudeD << m_data.latitudeM
                   << m_data.latitudeS << m_data.elevation << m_data.armXazimuth
                   << m_data.armYazimuth << m_data.more << m_data.moreTable;
        }

        FrDetector&
        FrDetector::Merge( const FrDetector& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrDetector::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        void
        FrDetector::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrDetector::demote_ret_type
        FrDetector::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------

                demote_ret_type retval(
                    new Previous::FrDetector( GetName( ),
                                              GetLongitudeD( ),
                                              GetLongitudeM( ),
                                              GetLongitudeS( ),
                                              GetLatitudeD( ),
                                              GetLatitudeM( ),
                                              GetLatitudeS( ),
                                              GetElevation( ),
                                              GetArmXazimuth( ),
                                              GetArmYazimuth( ),
                                              0 ) );
                if ( Stream )
                {
#if WORKING
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    Stream->ReplaceRef(
                        retval->RefMore( ), RefMore( ), MAX_REF );
                    Stream->RemoveResolver( &RefMoreTable( ), MAX_REF );
#else
                    assert( 0 );
#endif
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrDetector::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrDetector::promote_ret_type
        FrDetector::promote( INT_2U           Source,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            return PromoteObject< Previous::FrDetector, FrDetector >(
                DATA_FORMAT_VERSION, Source, Obj, Stream );
        }
    } // namespace Version_4
} // namespace FrameCPP
