//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream>

#include "framecpp/Common/CheckSumFilter.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Version4/FrEndOfFile.hh"
#include "framecpp/Version4/FrSE.hh"
#include "framecpp/Version4/FrSH.hh"

#include "framecpp/Version4/FrameSpec.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::CheckSum;
using FrameCPP::Common::CheckSumFilter;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_4
    {
        FrEndOfFile::FrEndOfFile( )
            : Common::FrEndOfFile( StructDescription( ) ), m_nFrames( 0 ),
              m_nBytes( 0 ), m_chkFlag( 0 ), m_chkSum( 0 ), m_seekTOC( 0 )
        {
        }

        FrEndOfFile::FrEndOfFile( istream_type& Stream )
            : Common::FrEndOfFile( StructDescription( ) )
        {
            //---------------------------------------------------------------------
            // :TODO: Need to handle calculation of checksums
            //---------------------------------------------------------------------
            Stream >> m_nFrames >> m_nBytes;
            //---------------------------------------------------------------------
            // Check for checksum calculations
            //---------------------------------------------------------------------
            CheckSumFilter* crc = Stream.GetCheckSumFile( );

            if ( crc )
            {
                Stream.rdbuf( )->FilterRemove( crc );
            }
            Stream >> m_chkFlag >> m_chkSum;
            if ( crc )
            {
                Stream.rdbuf( )->FilterAdd( crc );
            }
            Stream >> m_seekTOC;
            if ( crc )
            {
                crc->Value( ); // Stop processing
            }
        }

        FrEndOfFile::~FrEndOfFile( )
        {
        }

        FrameSpec::size_type
        FrEndOfFile::Bytes( const Common::StreamBase& Stream ) const
        {
            return sizeof( m_nFrames ) + sizeof( m_nBytes ) +
                sizeof( m_chkFlag ) + sizeof( m_chkSum ) + sizeof( m_seekTOC );
        }

        FrEndOfFile*
        FrEndOfFile::Clone( ) const
        {
            return new FrEndOfFile( *this );
        }

        FrEndOfFile*
        FrEndOfFile::Create( istream_type& Stream ) const
        {
            return new FrEndOfFile( Stream );
        }

        FrEndOfFile::chkSum_cmn_type
        FrEndOfFile::Filter( const istream_type& Stream,
                             Common::CheckSum&   Filt,
                             chkType_cmn_type&   Type,
                             void*               Buffer,
                             INT_8U              Size ) const
        {
            chkSum_type value;

            if ( Bytes( Stream ) != Size )
            {
                std::ostringstream msg;
                msg << "FrEndOfFile::Filter: "
                    << " Expected a buffer of size: " << Bytes( Stream )
                    << " but received a buffer of size: " << Size;
                throw std::length_error( msg.str( ) );
            }
            Filt.calc( Buffer, sizeof( nFrames_type ) + sizeof( nBytes_type ) );

            Filt.calc( (const char*)( Buffer ) + Size - sizeof( seekTOC_type ),
                       sizeof( seekTOC_type ) );

            {
                chkFlag_type flag;

                INT_8U offset( Size - sizeof( seekTOC_type ) - sizeof( value ) -
                               sizeof( flag ) );

                std::copy( (const char*)Buffer + offset,
                           (const char*)Buffer + offset + sizeof( flag ),
                           (char*)&flag );
                offset += sizeof( flag );
                std::copy( (const char*)Buffer + offset,
                           (const char*)Buffer + offset + sizeof( value ),
                           (char*)&value );

                if ( Stream.ByteSwapping( ) )
                {
                    reverse< sizeof( flag ) >( &flag, 1 );
                    reverse< sizeof( value ) >( &value, 1 );
                }
                Type = flag;
            }

            return value;
        }

        FrEndOfFile::nBytes_cmn_type
        FrEndOfFile::NBytes( ) const
        {
            return m_nBytes;
        }

        FrEndOfFile::nFrames_cmn_type
        FrEndOfFile::NFrames( ) const
        {
            return m_nFrames;
        }

        const char*
        FrEndOfFile::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrEndOfFile::StructDescription( )
        {
            static Description ret;
            if ( ret.size( ) == 0 )
            {
                ret( FrSH( "FrEndOfFile",
                           FrEndOfFile::s_object_id,
                           "End of File Data Structure" ) );
                ret( FrSE(
                    "nFrames", "INT_4U", "Number of frames in this file" ) );
                ret( FrSE( "nBytes",
                           "INT_4U",
                           "Total number of bytes in this file"
                           " ( 0 if NOT computed )" ) );
                ret( FrSE(
                    "chkFlag",
                    "INT_4U",
                    "Checksum schemes: 0=none, 1=CRC, >2 presently unsued" ) );
                ret( FrSE( "chkSum", "INT_4U", "File checksum." ) );
                ret( FrSE( "seekTOC",
                           "INT_4U",
                           "Byes to back up to the beginning of the table of"
                           " contents structure. If seekTOC == 0, then there"
                           " is no TOC for this file." ) );
            }

            return &ret;
        }

        void
        FrEndOfFile::VerifyObject( Common::Verify&       Verifier,
                                   Common::IFrameStream& Stream ) const
        {
            Verifier.SeenFrEndOfFile( true );
            Verifier.ExamineFrEndOfFileChecksum( Stream, m_chkFlag, m_chkSum );
        }

        void
        FrEndOfFile::Write( ostream_type& Stream ) const
        {
            //---------------------------------------------------------------------
            // Get local information
            //---------------------------------------------------------------------

            const nFrames_type nFrames( Stream.GetNumberOfFrames( ) );
            const nBytes_type  nBytes( Stream.tellp( ) +
                                      std::streampos( Bytes( Stream ) ) );
            const seekTOC_type seekTOC( ( Stream.GetTOCOffset( ) > 0 )
                                            ? nBytes - Stream.GetTOCOffset( )
                                            : 0 );

            chkFlag_type chkFlag = CheckSum::NONE;
            chkSum_type  chkSum = 0;

            //---------------------------------------------------------------------
            // Calculate checksum
            //---------------------------------------------------------------------
            CheckSumFilter* crc = Stream.GetCheckSumFile( );

            if ( crc )
            {
                chkFlag = crc->Type( );

                crc->Filter( &nFrames, sizeof( nFrames ) );
                crc->Filter( &nBytes, sizeof( nBytes ) );
                crc->Filter( &seekTOC, sizeof( seekTOC ) );

                chkSum = crc->Value( );

                Stream.SetCheckSumFile( CheckSum::NONE );
            }

            //---------------------------------------------------------------------
            // Write out to the stream
            //---------------------------------------------------------------------
            Stream << nFrames << nBytes << chkFlag << chkSum << seekTOC;
        }

        bool
        FrEndOfFile::operator==( const FrEndOfFile& RHS ) const
        {
            return ( ( m_nFrames == RHS.m_nFrames ) &&
                     ( m_nBytes == RHS.m_nBytes ) &&
                     ( m_chkFlag == RHS.m_chkFlag ) &&
                     ( m_chkSum == RHS.m_chkSum ) &&
                     ( m_seekTOC == RHS.m_seekTOC ) );
        }

        bool
        FrEndOfFile::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        void
        FrEndOfFile::assign( assign_stream_type& Stream )
        {
            //-----------------------------------------------------------------
            // Read the data from the stream
            //-----------------------------------------------------------------

            Stream >> m_nFrames >> m_nBytes >> m_chkFlag >> m_chkSum >>
                m_seekTOC;
        }

        FrEndOfFile::demote_ret_type
        FrEndOfFile::demote( INT_2U          Target,
                             demote_arg_type Obj,
                             istream_type*   Stream ) const
        {
            throw Unimplemented(
                "Object* FrEndOfFile::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrEndOfFile::promote_ret_type
        FrEndOfFile::promote( INT_2U           Target,
                              promote_arg_type Obj,
                              istream_type*    Stream ) const
        {
            throw Unimplemented(
                "Object* FrEndOfFile::promote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }
    } // namespace Version_4
} // namespace FrameCPP
