//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version7/FrSimEvent.hh"
#include "framecpp/Version7/FrSE.hh"
#include "framecpp/Version7/FrSH.hh"

#include "framecpp/Version7/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

namespace
{
    using FrameCPP::Common::IStream;
    using FrameCPP::Common::OStream;

    using FrameCPP::Version_7::FrSimEvent;

    //---------------------------------------------------------------------
    /// \brief Read in the parameter list for the FrSimEvent
    ///
    /// \param[in,out] Stream
    ///     The source of the parameter list.
    /// \param[out] Data
    ///     The parameters parsed from Stream.
    ///
    /// \return
    ///     The input stream.
    //---------------------------------------------------------------------
    IStream& operator>>( IStream& Stream, FrSimEvent::ParamList_type& Data );

    //---------------------------------------------------------------------
    /// \brief Write the parameter list for the FrSimEvent
    ///
    /// \param[in,out] Stream
    ///     The destination of the parameter list.
    /// \param[out] Data
    ///     The parameter list to be written.
    ///
    /// \return
    ///     The output stream.
    //---------------------------------------------------------------------
    OStream& operator<<( OStream&                          Stream,
                         const FrSimEvent::ParamList_type& Data );
} // namespace
//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_SIM_EVENT;

namespace FrameCPP
{
    namespace Version_7
    {
        //=======================================================================
        // FrSimEventNPS::storage_type
        //=======================================================================
        FrameCPP::cmn_streamsize_type
        FrSimEventNPS::storage_type::Bytes( ) const
        {
            FrameCPP::cmn_streamsize_type retval = name.Bytes( ) +
                comment.Bytes( ) + inputs.Bytes( ) + Common::Bytes( GTime ) +
                sizeof( timeBefore ) + sizeof( timeAfter ) +
                sizeof( amplitude ) + sizeof( INT_2U ); // nParam
            ;
            for ( ParamList_type::const_iterator i( Params.begin( ) );
                  i != ( Params.end( ) );
                  i++ )
            {
                retval +=
                    sizeof( Param_type::second_type ) + ( *i ).first.Bytes( );
            }

            return retval;
        }

        void
        FrSimEventNPS::storage_type::operator( )( Common::IStream& Stream )
        {
            Stream >> name >> comment >> inputs >> GTime >> timeBefore >>
                timeAfter >> amplitude >> Params;
        }

        void
        FrSimEventNPS::storage_type::
        operator( )( Common::OStream& Stream ) const
        {
            Stream << name << comment << inputs << GTime << timeBefore
                   << timeAfter << amplitude << Params;
        }

        //=======================================================================
        // FrSimEvent
        //=======================================================================
        FrSimEvent::FrSimEvent( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
        }

        FrSimEvent::FrSimEvent( const FrSimEvent& Source )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrSimEventNPS( Source ),
              FrSimEventPS( Source ), Common::TOCInfo( Source )
        {
        }

        FrSimEvent::FrSimEvent( const std::string&    name,
                                const std::string&    comment,
                                const std::string&    inputs,
                                const GPSTime&        time,
                                const REAL_4          timeBefore,
                                const REAL_4          timeAfter,
                                const REAL_4          amplitude,
                                const ParamList_type& parameters )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrSimEventNPS( name,
                             comment,
                             inputs,
                             time,
                             timeBefore,
                             timeAfter,
                             amplitude,
                             parameters )
        {
        }

        FrSimEvent::FrSimEvent( const Previous::FrSimEvent& Source,
                                istream_type*               Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrSimEventNPS( Source )
        {
            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                Stream->ReplaceRef( RefData( ),
                                    Source.RefData( ),
                                    Previous::FrSimEvent::MAX_REF );
                Stream->ReplaceRef( RefTable( ),
                                    Source.RefTable( ),
                                    Previous::FrSimEvent::MAX_REF );
            }
        }

        FrSimEvent::FrSimEvent( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrSimEvent*
        FrSimEvent::Create( istream_type& Stream ) const
        {
            return new FrSimEvent( Stream );
        }

        const std::string&
        FrSimEvent::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrSimEvent&
        FrSimEvent::Merge( const FrSimEvent& RHS )
        {
            throw Unimplemented(
                "FrSimEvent& FrSimEvent::Merge( const FrSimEvent& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        const char*
        FrSimEvent::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrSimEvent::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrSimEvent::StructName( ),
                           s_object_id,
                           "Event Data Structure" ) );

                FrSimEventNPS::storage_type::Describe< FrSE >( ret );
                refs_type::Describe< FrSE >( ret );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrSimEvent::StructName( ) ),
                           "" ) );
            }

            return &ret;
        }

        void
        FrSimEvent::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_GTIME_S:
                        *data = GetGTime( ).GetSeconds( );
                        break;
                    case TOCInfo::IC_GTIME_N:
                        *data = GetGTime( ).GetNanoseconds( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_4:
                {
                    REAL_4* data = va_arg( vl, REAL_4* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_REAL_8:
                {
                    REAL_8* data = va_arg( vl, REAL_8* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_AMPLITUDE:
                        *data = GetAmplitude( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        void
        FrSimEvent::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
            WriteNext( Stream );
        }

        bool
        FrSimEvent::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrSimEvent::demote_ret_type
        FrSimEvent::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                Previous::FrSimEvent::ParamList_type params;
                for ( ParamList_type::const_iterator cur = GetParam( ).begin( ),
                                                     last = GetParam( ).end( );
                      cur != last;
                      ++cur )
                {
                    params.push_back( Previous::FrSimEvent::Param_type(
                        cur->first, cur->second ) );
                }
                // Do actual down conversion
                boost::shared_ptr< Previous::FrSimEvent > retval(
                    new Previous::FrSimEvent( GetName( ),
                                              GetComment( ),
                                              GetInputs( ),
                                              GetGTime( ),
                                              GetTimeBefore( ),
                                              GetTimeAfter( ),
                                              GetAmplitude( ),
                                              params ) );
                if ( Stream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    Stream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    Stream->ReplaceRef(
                        retval->RefTable( ), RefTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrSimEvent::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrSimEvent::promote_ret_type
        FrSimEvent::promote( INT_2U           Target,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

    } // namespace Version_7
} // namespace FrameCPP

//=======================================================================
// Local functions
//=======================================================================

namespace
{
    using FrameCPP::Common::IStream;
    using FrameCPP::Common::OStream;

    using FrameCPP::Version_7::FrSimEvent;

    //---------------------------------------------------------------------
    /// This reads a parameter list from the input stream and
    /// associates it with the FrSimEvent.
    //---------------------------------------------------------------------
    IStream&
    operator>>( IStream& Stream, FrSimEvent::ParamList_type& Data )
    {
        FrSimEvent::nParam_type nParam = Data.size( );

        Stream >> nParam;

        Data.resize( nParam );
        for ( FrSimEvent::nParam_type cur = 0; cur != nParam; ++cur )
        {
            Stream >> Data[ cur ].second;
        }
        for ( FrSimEvent::nParam_type cur = 0; cur != nParam; ++cur )
        {
            Stream >> Data[ cur ].first;
        }
        return Stream;
    }

    //---------------------------------------------------------------------
    /// This writes the parameter list of the FrSimEvent to the
    /// output stream.
    //---------------------------------------------------------------------
    OStream&
    operator<<( OStream& Stream, const FrSimEvent::ParamList_type& Data )
    {
        FrSimEvent::nParam_type nParam = Data.size( );

        Stream << nParam;

        for ( FrSimEvent::nParam_type cur = 0; cur != nParam; ++cur )
        {
            Stream << Data[ cur ].second;
        }
        for ( FrSimEvent::nParam_type cur = 0; cur != nParam; ++cur )
        {
            Stream << Data[ cur ].first;
        }
        return Stream;
    }
} // namespace
