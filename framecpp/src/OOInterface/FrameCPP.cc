//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <sstream>

#include "framecpp_config.h"

#include "framecpp/FrameCPP.hh"

#if FRAME_SPEC_CURRENT == 3
#include "framecpp/Version3/FrameSpec.hh"
#elif FRAME_SPEC_CURRENT == 4
#include "framecpp/Version4/FrameSpec.hh"
#elif FRAME_SPEC_CURRENT == 6
#include "framecpp/Version6/FrameSpec.hh"
#elif FRAME_SPEC_CURRENT == 7
#include "framecpp/Version7/FrameSpec.hh"
#elif FRAME_SPEC_CURRENT == 8
#include "framecpp/Version8/FrameSpec.hh"
#elif FRAME_SPEC_CURRENT == 9
#include "framecpp/Version9/FrameSpec.hh"
#else
#error "Unsupported frame spec: FRAME_SPEC_CURRENT"
#endif

namespace FrameCPP
{
    namespace Version_3
    {
        extern bool init_frame_spec( );
    }
    namespace Version_4
    {
        extern bool init_frame_spec( );
    }
    namespace Version_6
    {
        extern bool init_frame_spec( );
    }
    namespace Version_7
    {
        extern bool init_frame_spec( );
    }
    namespace Version_8
    {
        extern bool init_frame_spec( );
    }
    namespace Version_9
    {
      extern bool init_frame_spec( );
    }
} // namespace FrameCPP

//-----------------------------------------------------------------------------
// The time information that is returned by this function is based on when the
// top level configure.in ( ldas/configure.in) file was committed to CVS.
// The time value is in UTC time as that is what CVS uses internally.
//
//! return: std::string - The string representation of the cvs date
std::string
FrameCPP::GetCVSDate( )
{
    return "Source is under GIT";
}

//-----------------------------------------------------------------------------
//! return: std::string - The string representation of when the library was
//+ built
std::string
FrameCPP::GetBuildDate( )
{
    return FRAMECPP_BUILD_DATE;
}

//! return: INT_2U - current minor version for the frame library
INT_2U
FrameCPP::GetFrameLibraryMinorVersion( )
{
    return FrameCPP::Version::LIBRARY_MINOR_VERSION;
}

void
FrameCPP::Initialize( )
{
    Version_3::init_frame_spec( );
    Version_4::init_frame_spec( );
    Version_6::init_frame_spec( );
    Version_7::init_frame_spec( );
    Version_8::init_frame_spec( );
    Version_9::init_frame_spec( );
}
