//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2020 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "gtest/gtest.h"

#include "framecpp/FrProcData.hh"

using FrameCPP::FrProcData;

TEST( DataStructures, FrProcData_MIME )
{
    {
        //-------------------------------------------------------------------
        // Testing the
        //-------------------------------------------------------------------
        FrProcData data( "FrProcData", // Name
                         "Construct FrProcData for MIME information", // Comment
                         FrProcData::MIME, // Type
                         0, // SubType
                         0, // TimeOffset
                         1, // TRange
                         0, // FShift
                         0, // Phase
                         0, // FRange,
                         0 // BW
        );
        EXPECT_EQ( FrProcData::MIME, data.GetType( ) );
        EXPECT_EQ( 0, data.GetSubType( ) );
    }
    {
        //-------------------------------------------------------------------
        // Testing the default constructor and then adding
        //-------------------------------------------------------------------
        unsigned char mime_content[] = "contents text";
        std::string   mime_type_string( "text/plain" );
        FrProcData    data;

        std::string mime_name_1( "MIME_Entry_1" );
        data.AppendMIME( mime_type_string,
                         mime_content,
                         sizeof( mime_content ),
                         mime_name_1 );

        EXPECT_STREQ( "", data.GetName( ).c_str( ) );
        EXPECT_EQ( FrProcData::MIME, data.GetType( ) );
        EXPECT_EQ( 0, data.GetSubType( ) );
        EXPECT_EQ( 1, data.RefData( ).size( ) );
        EXPECT_STREQ( mime_name_1.c_str( ),
                      data.RefData( )[ 0 ]->GetName( ).c_str( ) );

        std::string mime_name_2( "MIME_Entry_2" );
        data.AppendMIME( mime_type_string,
                         mime_content,
                         sizeof( mime_content ),
                         mime_name_2 );

        EXPECT_STREQ( "", data.GetName( ).c_str( ) );
        EXPECT_EQ( FrProcData::MIME, data.GetType( ) );
        EXPECT_EQ( 0, data.GetSubType( ) );
        EXPECT_EQ( 2, data.RefData( ).size( ) );
        EXPECT_STREQ( mime_name_2.c_str( ),
                      data.RefData( )[ 1 ]->GetName( ).c_str( ) );
    }
}

int
main( int argc, char** argv )
try
{
    testing::InitGoogleTest( &argc, argv );
    return RUN_ALL_TESTS( );
}
catch ( ... )
{
}
