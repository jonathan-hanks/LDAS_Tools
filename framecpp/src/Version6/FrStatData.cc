//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <algorithm>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"

#include "framecpp/Version6/FrStatData.hh"
#include "framecpp/Version6/FrDetector.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        // FrStatData
        //===================================================================

        FrStatData::FrStatData( ) : Common::FrStatData( StructDescription( ) )
        {
        }

        FrStatData::FrStatData( const FrStatData& Source )
            : Common::FrStatData( StructDescription( ) ),
              FrStatDataNPS( Source ),
              FrStatDataPS( Source ), Common::TOCInfo( Source )
        {
        }

        FrStatData::FrStatData( const std::string& name,
                                const std::string& comment,
                                const std::string& representation,
                                const INT_4U       timeStart,
                                const INT_4U       timeEnd,
                                const INT_4U       version )
            : Common::FrStatData( StructDescription( ) ),
              FrStatDataNPS(
                  name, comment, representation, timeStart, timeEnd, version )
        {
        }

        FrStatData::FrStatData( const Previous::FrStatData& Source,
                                istream_type*               Stream )
            : Common::FrStatData( StructDescription( ) )
        {
            m_data( Source );
            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-----------------------------------------------------------------
                const INT_2U max_ref = Previous::FrStatData::MAX_REF;
#if WORKING
                Stream->ReplacePtr(
                    (FrameSpec::Object**)( AddressOfDetector( ) ),
                    (const FrameSpec::Object**)( Source.AddressOfDetector( ) ),
                    max_ref );
#endif /* WORKING */
                Stream->ReplaceRef( RefData( ), Source.RefData( ), max_ref );
                Stream->ReplaceRef( RefTable( ), Source.RefTable( ), max_ref );
            }
        }

        FrStatData::FrStatData( istream_type& Stream )
            : Common::FrStatData( StructDescription( ) )
        {
            m_data( Stream );
            m_refs( Stream );
        }

        FrameCPP::cmn_streamsize_type
        FrStatData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.Bytes( ) + m_refs.Bytes( Stream );
        }

        FrStatData*
        FrStatData::Clone( ) const
        {
            return new FrStatData( *this );
        }

        FrStatData*
        FrStatData::Create( istream_type& Stream ) const
        {
            return new FrStatData( Stream );
        }

        const char*
        FrStatData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrStatData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrStatData::StructName( ),
                           FrStatData::STRUCT_ID,
                           "Static Data Structure" ) );

                FrStatDataNPS::storage_type::Describe< FrSE >( ret );
                ref_type::Describe< FrSE >( ret );
            }

            return &ret;
        }

        void
        FrStatData::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
        }

        void
        FrStatData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    case TOCInfo::IC_DETECTOR:
                        if ( GetDetector( ) )
                        {
                            boost::shared_ptr< FrDetector > detector(
                                boost::dynamic_pointer_cast< FrDetector >(
                                    GetDetector( ) ) );
                            *data = detector->GetName( );
                        }
                        else
                        {
                            *data = "";
                        }
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                case TOCInfo::DT_INT_4U:
                {
                    INT_4U* data = va_arg( vl, INT_4U* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_START:
                        *data = GetTimeStart( );
                        break;
                    case TOCInfo::IC_END:
                        *data = GetTimeEnd( );
                        break;
                    case TOCInfo::IC_VERSION:
                        *data = GetVersion( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrStatData&
        FrStatData::Merge( const FrStatData& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrStatData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrStatData::demote_ret_type
        FrStatData::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                boost::shared_ptr< Previous::FrStatData > retval(
                    new Previous::FrStatData( GetName( ),
                                              GetComment( ),
                                              GetRepresentation( ),
                                              GetTimeStart( ),
                                              GetTimeEnd( ),
                                              GetVersion( ) ) );
                if ( Stream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
#if WORKING
                    Stream->ReplacePtr(
                        (FrameSpec::Object**)( retval->AddressOfDetector( ) ),
                        (const FrameSpec::Object**)( AddressOfDetector( ) ),
                        MAX_REF );
#endif /* WORKING */
                    Stream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    Stream->ReplaceRef(
                        retval->RefTable( ), RefTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrStatData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrStatData::promote_ret_type
        FrStatData::promote( INT_2U           Target,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
