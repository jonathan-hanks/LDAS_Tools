//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <memory>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/STRING.hh"

#include "framecpp/Version6/FrameSpec.hh"

#include "framecpp/Version6/FrSH.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        FrSH::FrSH( )
        {
        }

        FrSH::FrSH( const std::string& Name,
                    INT_2U             ClassId,
                    const std::string& Comment )
        {
            m_data.name = Name;
            m_data.classId = ClassId;
            m_data.comment = Comment;
        }

        FrSH::FrSH( istream_type& Stream )
        {
            Stream >> m_data.name >> m_data.classId >> m_data.comment;
        }

        FrSH::~FrSH( )
        {
        }

        INT_8U
        FrSH::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( m_data.classId ) +
                m_data.comment.Bytes( );
        }

        FrSH*
        FrSH::Create( istream_type& Stream ) const
        {
            return new FrSH( Stream );
        }

        FrSH*
        FrSH::Clone( ) const
        {
            return new FrSH( *this );
        }

        FrameSpec::Object*
        FrSH::Demote( Object* Obj, istream_type* Stream ) const
        {
            if ( dynamic_cast< FrSH* >( Obj ) != (FrSH*)NULL )
            {
                return Obj;
            }
            throw Unimplemented( "Object* FrSH::Demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrameSpec::Object*
        FrSH::Promote( Object* Obj, istream_type* Stream ) const
        {
            if ( dynamic_cast< FrSH* >( Obj ) != (FrSH*)NULL )
            {
                return Obj;
            }
            throw Unimplemented( "Object* FrSH::Promote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        const char* const
        FrSH::ObjectStructName( ) const
        {
            return StructName( );
        }

        void
        FrSH::Write( ostream_type& Stream ) const
        {
            Stream << m_data.name << m_data.classId << m_data.comment;
        }

        bool
        FrSH::operator==( const FrSH& RHS ) const
        {
            return ( ( m_data.name == RHS.m_data.name ) &&
                     ( m_data.classId == RHS.m_data.classId ) &&
                     ( m_data.comment == RHS.m_data.comment ) );
        }

        bool
        FrSH::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        void
        FrSH::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    Common::STRING< INT_2U >* data =
                        va_arg( vl, Common::STRING< INT_2U >* );
                    *data = GetName( );
                }
                break;
                case TOCInfo::DT_INT_2U:
                {
                    INT_2U* data = va_arg( vl, INT_2U* );
                    *data = GetClass( );
                }
                break;
                default:
                    // Stop processing
                    InfoClass = TOCInfo::IC_EOQ;
                    continue;
                }
                InfoClass = va_arg( vl, int );
            }
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        const std::string&
        FrSH::name( ) const
        {
            return GetName( );
        }

        const INT_2U
        FrSH::classId( ) const
        {
            return GetClass( );
        }

    } // namespace Version_6
} // namespace FrameCPP
