//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Singleton.hh"
#include "ldastoolsal/gpstime.hh"

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameStream.hh"

#include "framecpp/Version6/FrProcData.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using std::ceil;
using std::floor;

using namespace FrameCPP::Version_6;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::IStream;

#if HAVE_TEMPLATE_MOVE
#define MOVE_RET( a ) ( std::move( a ) )
#else /* HAVE_TEMPLATE_MOVE */
#define MOVE_RET( a ) ( a )
#endif /* HAVE_TEMPLATE_MOVE */

//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_PROC_DATA;

//=======================================================================
// Anonymous
//=======================================================================
namespace
{
    typedef FrameCPP::Version_6::FrProcData::type_type type_type;

    class IDTypeToStringMapping
    {
        SINGLETON_TS_DECL( IDTypeToStringMapping );

    public:
        const std::string& GetString( type_type Id ) const;

    private:
        std::vector< std::string > m_types;
    };

    class IDSubTypeToStringMappingFrequency
    {
        SINGLETON_TS_DECL( IDSubTypeToStringMappingFrequency );

    public:
        const std::string& GetString( type_type Id ) const;

    private:
        std::vector< std::string > m_types;
    };

} // namespace

namespace FrameCPP
{
} // namespace FrameCPP

namespace FrameCPP
{
    namespace Version_6
    {
        //=======================================================================
        //=======================================================================

        template <>
        template <>
        FrProcDataPST< FrVect, FrVect, FrTable, FrHistory >::FrProcDataPST(
            const Version_4::FrProcData& Source, IStream* Stream )
        {
            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                Stream->ReplaceRef( RefData( ), Source.RefData( ), MAX_REF );
                Stream->ReplaceRef( RefAux( ), Source.RefAux( ), MAX_REF );
                Stream->ReplaceRef( RefTable( ), Source.RefTable( ), MAX_REF );
            }
        }

        //=======================================================================
        // FrProcDataNPS
        //=======================================================================

        const FrProcDataNPS::phase_type  FrProcDataNPS::PHASE_UNKNOWN = 0.0;
        const FrProcDataNPS::fRange_type FrProcDataNPS::FRANGE_UNKNOWN = 0.0;
        const FrProcDataNPS::BW_type     FrProcDataNPS::BW_UNKNOWN = 0;

        const FrProcDataNPS::type_type FrProcDataNPS::DEFAULT_VALUE_TYPE =
            FrProcDataNPS::UNKNOWN_TYPE;
        const FrProcDataNPS::subType_type
            FrProcDataNPS::DEFAULT_VALUE_SUB_TYPE =
                FrProcDataNPS::UNKNOWN_SUB_TYPE;
        const FrProcDataNPS::tRange_type FrProcDataNPS::DEFAULT_VALUE_TRANGE =
            FR_PROC_DATA_DEFAULT_TRANGE;
        const FrProcDataNPS::fShift_type FrProcDataNPS::DEFAULT_VALUE_FSHIFT =
            FR_PROC_DATA_DEFAULT_FSHIFT;
        const FrProcDataNPS::phase_type FrProcDataNPS::DEFAULT_VALUE_PHASE =
            FR_PROC_DATA_DEFAULT_PHASE;
        const FrProcDataNPS::fRange_type FrProcDataNPS::DEFAULT_VALUE_FRANGE =
            FR_PROC_DATA_DEFAULT_FRANGE;
        const FrProcDataNPS::BW_type FrProcDataNPS::DEFAULT_VALUE_BW =
            FR_PROC_DATA_DEFAULT_BW;

        //=======================================================================
        // FrProcData
        //=======================================================================
        FrProcData::FrProcData( )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              m_synced_with_vector( false )
        {
        }

        FrProcData::FrProcData( const FrProcData& ProcData )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              Common::TOCInfo( ProcData ), FrProcDataNPS( ProcData ),
              FrProcDataPS( ProcData ), m_synced_with_vector( false )
        {
        }

        FrProcData::FrProcData( const name_type&    Name,
                                const comment_type& Comment,
                                type_type           Type,
                                subType_type        SubType,
                                timeOffset_type     TimeOffset,
                                tRange_type         TRange,
                                fShift_type         FShift,
                                phase_type          Phase,
                                fRange_type         FRange,
                                BW_type             BW )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrProcDataNPS( Name,
                             Comment,
                             Type,
                             SubType,
                             TimeOffset,
                             TRange,
                             FShift,
                             Phase,
                             FRange,
                             BW ),
              m_synced_with_vector( false )
        {
        }

#if 0
    FrProcData::
    FrProcData( const std::string& name,
		REAL_8 sampleRate, const GPSTime& timeOffset,
		REAL_8 fShift, REAL_4 phase )
      : FrameSpec::Object( s_object_id, StructDescription( ) ),
	m_synced_with_vector( false )

    {
      m_data.name = name;
      m_data.comment = std::string( "" );
      m_data.type = 0;
      m_data.subType = 0;
      m_data.timeOffset = timeOffset - GPSTime( 0, 0 );
      m_data.tRange = 0;
      m_data.fShift = fShift;
      m_data.phase = phase;
      m_data.fRange = 0;
      m_data.BW = 0;
    }
#endif /* 0 */

        FrProcData::FrProcData( Previous::FrProcData& Source,
                                istream_type*         Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrProcDataNPS( Source ), FrProcDataPS( Source, Stream ),
              m_synced_with_vector( false )
        {
        }

        FrProcData::FrProcData( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrProcDataNPS( Stream ), FrProcDataPS( Stream ),
              m_synced_with_vector( false )
        {
            Stream.Next( this );
        }

        FrProcData::~FrProcData( )
        {
        }

        FrameCPP::cmn_streamsize_type
        FrProcData::Bytes( const Common::StreamBase& Stream ) const
        {
            return FrProcDataNPS::bytes( ) + FrProcDataPS::bytes( Stream ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        const std::string&
        FrProcData::IDTypeToString( type_type Type )
        {
            return IDTypeToStringMapping::Instance( ).GetString( Type );
        }

        FrProcData*
        FrProcData::Create( istream_type& Stream ) const
        {
            return new FrProcData( Stream );
        }

        const char*
        FrProcData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const std::string&
        FrProcData::GetNameSlow( ) const
        {
            return GetName( );
        }

        const Description*
        FrProcData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrProcData::StructName( ),
                           s_object_id,
                           "Post-processed Data Structure" ) );

                ret( FrSE( "name", "STRING", "" ) );
                ret( FrSE( "comment", "STRING", "" ) );
                ret( FrSE( "type", "INT_2U", "" ) );
                ret( FrSE( "subType", "INT_2U", "" ) );
                ret( FrSE( "timeOffset", "REAL_8", "" ) );
                ret( FrSE( "tRange", "REAL_8", "" ) );
                ret( FrSE( "fShift", "REAL_8", "" ) );
                ret( FrSE( "phase", "REAL_4", "" ) );
                ret( FrSE( "fRange", "REAL_8", "" ) );
                ret( FrSE( "BW", "REAL_8", "" ) );
                ret( FrSE( "nAuxParam", "INT_2U", "" ) );
                ret( FrSE( "auxParam", "REAL_8[nAuxParam]", "" ) );
                ret( FrSE( "auxParamNames", "STRING[nAuxParam]", "" ) );

                ret(
                    FrSE( "data", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "aux", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "table",
                           PTR_STRUCT::Desc( FrTable::StructName( ) ) ) );
                ret( FrSE( "history",
                           PTR_STRUCT::Desc( FrHistory::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrProcData::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrProcData::Write( ostream_type& Stream ) const
        {
            FrProcDataNPS::write( Stream );
            FrProcDataPS::write( Stream );
            WriteNext( Stream );
        }

        const std::string&
        FrProcData::IDSubTypeToString( type_type Type, subType_type SubType )
        {
            static const std::string empty( "" );
            switch ( Type )
            {
            case FREQUENCY_SERIES:
                return IDSubTypeToStringMappingFrequency::Instance( ).GetString(
                    Type );
                break;
            default:
                return empty;
                break;
            }
        }

        FrProcData&
        FrProcData::Merge( const FrProcData& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        FrProcData::subset_ret_type
        FrProcData::SubFrProcData( REAL_8 Offset, REAL_8 Dt ) const
        {
            subset_ret_type retval;

            //---------------------------------------------------------------------
            // Process the types that are understood
            //---------------------------------------------------------------------
            switch ( GetType( ) )
            {
            case TIME_SERIES:
                retval = sub_time_series( Offset, Dt );
                break;
            default:
            {
                //-----------------------------------------------------------------
                // This type is not supported so throw an exception
                //-----------------------------------------------------------------
                std::ostringstream msg;
                msg << "Currently cannot take a subsection of type: "
                    << IDTypeToString( GetType( ) );
                const std::string& subtype(
                    IDSubTypeToString( GetType( ), GetSubType( ) ) );
                if ( subtype.length( ) > 0 )
                {
                    msg << " subType: " << subtype;
                }
                throw std::runtime_error( msg.str( ) );
            }
            break;
            }

#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpessimizing-move"
#endif /* __clang__ */
            return MOVE_RET( retval );
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic pop
#endif /* __clang__ */
        }

        bool
        FrProcData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        void
        FrProcData::copy_core( const FrProcData& Source )
        {
            FrProcDataNPS::operator=( Source );
            FrProcDataPS::copy_core( Source );
        }

        FrProcData::demote_ret_type
        FrProcData::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                /// \todo See if the actual sample rate can be computed
                Previous::GPSTime t;
                t += GetTimeOffset( );
                boost::shared_ptr< Previous::FrProcData > retval(
                    new Previous::FrProcData(
                        GetName( ), GetComment( ), 0.0, t, GetFShift( ) ) );
                if ( Stream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    Stream->ReplaceRef(
                        retval->RefData( ), RefData( ), MAX_REF );
                    Stream->ReplaceRef( retval->RefAux( ), RefAux( ), MAX_REF );
                    Stream->ReplaceRef(
                        retval->RefTable( ), RefTable( ), MAX_REF );
                    Stream->RemoveResolver( &( RefHistory( ) ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrProcData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrProcData::promote_ret_type
        FrProcData::promote( INT_2U           Target,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

        void
        FrProcData::sync_with_vector( )
        {
            if ( ( m_synced_with_vector == false ) &&
                 ( RefData( ).size( ) > 0 ) )
            {
                switch ( GetType( ) )
                {
                case TIME_SERIES:
                    SetTRange( ( RefData( )[ 0 ]->GetDim( 0 ).GetNx( ) *
                                 RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) ) );
                    break;
                case FREQUENCY_SERIES:
                    SetFRange( ( RefData( )[ 0 ]->GetDim( 0 ).GetNx( ) *
                                 RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) ) );
                    break;
                }
                m_synced_with_vector = true;
            }
        }

        // Routine to take a subsection when the data type is TimeSeries data
        FrProcData::subset_ret_type
        FrProcData::sub_time_series( REAL_8 Offset, REAL_8 Dt ) const
        {
            //---------------------------------------------------------------------
            // Sanity checks
            //---------------------------------------------------------------------
            // Verify the existance of only a single FrVect component
            //---------------------------------------------------------------------
            if ( RefData( ).size( ) != 1 )
            {
                std::ostringstream msg;

                msg << "METHOD: frameAPI::FrProcData::SubFrProcData: "
                    << "data vectors of length " << RefData( ).size( )
                    << " currently are not supported";
                throw std::logic_error( msg.str( ) );
            }
            INT_4U num_elements = RefData( )[ 0 ]->GetDim( 0 ).GetNx( );
            //---------------------------------------------------------------------
            //:TODO: Ensure the dimension of the FrVect is 1
            //---------------------------------------------------------------------
            //---------------------------------------------------------------------
            //:TODO: Ensure the requested range is valid
            //---------------------------------------------------------------------
            REAL_8 sample_rate = 1.0 / RefData( )[ 0 ]->GetDim( 0 ).GetDx( );
            if ( Offset >
                 ( GetTimeOffset( ) + ( num_elements / sample_rate ) ) )
            {
                std::ostringstream msg;
                msg << "METHOD: frameAPI::FrProcData::SubFrProcData: "
                    << "The value for the parameter Offset (" << Offset
                    << ") is out of range ([0-"
                    << ( GetTimeOffset( ) + ( num_elements / sample_rate ) )
                    << "))";
                throw std::range_error( msg.str( ) );
            }
            //---------------------------------------------------------------------
            // Start moving of data
            //---------------------------------------------------------------------
            subset_ret_type retval( new FrProcData );
            // Copy in the header information
            retval->copy_core( *this );

            //:TODO: Get the slice of FrVect
            REAL_8 startSampleReal = ( Offset * sample_rate ) -
                ( retval->GetTimeOffset( ) * sample_rate );

            INT_4U endSample =
                INT_4U( ceil( startSampleReal + ( Dt * sample_rate ) ) );
            INT_4U startSample = ( startSampleReal > REAL_8( 0 ) )
                ? INT_4U( floor( startSampleReal ) )
                : INT_4U( 0 );

            {
                boost::shared_ptr< FrVect > subvect(
                    RefData( )[ 0 ]
                        ->SubFrVect( startSample, endSample )
                        .release( ) );
                retval->RefData( ).append( subvect );
            }

            //---------------------------------------------------------------------
            // Shift the offset
            //---------------------------------------------------------------------
            if ( startSampleReal > 0 )
            {
                // There is no gap
                retval->timeOffset = timeOffset_type( 0 );
            }
            else
            {
                retval->timeOffset += Offset;
            }
            //---------------------------------------------------------------------
            // :TODO: Recalculate tRange
            //---------------------------------------------------------------------
            retval->SetTRange( retval->RefData( )[ 0 ]->GetDim( 0 ).GetNx( ) *
                               retval->RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) );
            //---------------------------------------------------------------------
            // Return the new FrProcData
            //---------------------------------------------------------------------
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpessimizing-move"
#endif /* __clang__ */
            return MOVE_RET( retval );
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic pop
#endif /* __clang__ */
        }
    } // namespace Version_6
} // namespace FrameCPP

//=======================================================================
// Anonymous
//=======================================================================
namespace
{
    //---------------------------------------------------------------------
    // Initialization of singleton
    //---------------------------------------------------------------------
    SINGLETON_TS_INST( IDTypeToStringMapping );
    //---------------------------------------------------------------------
    // Default constructor
    //---------------------------------------------------------------------
    IDTypeToStringMapping::IDTypeToStringMapping( )
    {
        LDASTools::AL::MemChecker::Append(
            singleton_suicide,
            "<anonymous>::IDTypeToStringMapping::singleton_suicide",
            5 );
        m_types.push_back( "Unknown/user defined" );
        m_types.push_back( "Time Series" );
        m_types.push_back( "Frequency Series" );
        m_types.push_back( "Other 1D Series data" );
        m_types.push_back( "Time-Frequency" );
        m_types.push_back( "Wavelets" );
        m_types.push_back( "Multi-dimensional" );
    }

    const std::string&
    IDTypeToStringMapping::GetString( type_type Type ) const
    {
        if ( Type >= m_types.size( ) )
        {
            std::ostringstream msg;
            msg << "Cannot map FrProcData type value of " << Type
                << " to string value because it is out of range";
            throw std::range_error( msg.str( ) );
        }
        return m_types[ Type ];
    }

    //---------------------------------------------------------------------
    // Initialization of singleton
    //---------------------------------------------------------------------
    SINGLETON_TS_INST( IDSubTypeToStringMappingFrequency );
    //---------------------------------------------------------------------
    // Default constructor
    //---------------------------------------------------------------------
    IDSubTypeToStringMappingFrequency::IDSubTypeToStringMappingFrequency( )
    {
        LDASTools::AL::MemChecker::Append(
            singleton_suicide,
            "<anonymous>::IDTypeToStringMappingFrequency::singleton_suicide",
            5 );
        m_types.push_back( "Unknown/user defined" );
        m_types.push_back( "Amplitude Spectral Density" );
        m_types.push_back( "Power Spectral Density" );
        m_types.push_back( "Cross Spectral Density" );
        m_types.push_back( "Coherence" );
        m_types.push_back( "Transfer Function" );
    }

    const std::string&
    IDSubTypeToStringMappingFrequency::GetString( INT_2U Type ) const
    {
        if ( Type >= m_types.size( ) )
        {
            std::ostringstream msg;
            msg << "Cannot map FrProcData type value of " << Type
                << " to string value because it is out of range";
            throw std::range_error( msg.str( ) );
        }
        return m_types[ Type ];
    }
} // namespace

namespace FrameCPP
{
    namespace Version_6
    {
        void
        FrProcData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

    } // namespace Version_6
} // namespace FrameCPP
