//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrTable.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        //===================================================================
        FrTable::FrTable( ) : Object( CLASS_ID, StructDescription( ) )
        {
        }

        FrTable::FrTable( const FrTable& Table )
            : Object( CLASS_ID, StructDescription( ) ), FrTableNPS( Table ),
              FrTablePS( Table )
        {
        }

        FrTable::FrTable( const std::string& name, INT_4U nrows )
            : Object( CLASS_ID, StructDescription( ) )
        {
            setName( name );
        }

        FrTable::FrTable( const std::string&         Name,
                          const std::string&         Comment,
                          nRow_type                  NRows,
                          column_name_container_type ColumnNames )
            : Object( CLASS_ID, StructDescription( ) )
        {
            setName( Name );
            AppendComment( Comment );
        }

        FrTable::FrTable( const FrTableNPS& Source )
            : Object( CLASS_ID, StructDescription( ) ), FrTableNPS( Source )
        {
        }

        FrTable::FrTable( istream_type& Stream )
            : Object( CLASS_ID, StructDescription( ) )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrTable::FrTable( Previous::FrTable& Source, istream_type* Stream )
            : Object( CLASS_ID, StructDescription( ) ), FrTableNPS( Source )
        {
            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Fix references
                //-------------------------------------------------------------------
                Stream->ReplaceRef(
                    Source.RefColumn( ), RefColumn( ), m_refs.MAX_REF );
            }
        }

        FrTable::~FrTable( )
        {
        }

        FrameCPP::cmn_streamsize_type
        FrTable::Bytes( const Common::StreamBase& Stream ) const
        {
            cmn_streamsize_type retval = m_data.Bytes( Stream ) +
                m_refs.Bytes( Stream ) + Stream.PtrStructBytes( ) // next
                ;
            return retval;
        }

        FrTable*
        FrTable::Create( istream_type& Stream ) const
        {
            return new FrTable( Stream );
        }

        FrTable&
        FrTable::Merge( const FrTable& RHS )
        {
            throw Unimplemented(
                "FrTable& FrTable::Merge( const FrTable& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        const char*
        FrTable::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrTable::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( StructName( ), CLASS_ID, "Table Data Structure" ) );

                data_type::Describe< FrSE >( ret );
                refs_type::Describe< FrSE >( ret );

                ret( FrSE( "next", ptr_struct_type::Desc( StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrTable::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );

            WriteNext( Stream );
        }

        const std::string&
        FrTable::GetNameSlow( ) const
        {
            return GetName( );
        }

        bool
        FrTable::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return compare( *this, Obj );
        }

        FrTable&
        FrTable::operator+=( const FrTable& RHS )
        {
            throw Unimplemented(
                "FrTable& FrTable::operator+=( const FrTable& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrTable::demote_ret_type
        FrTable::demote( INT_2U          Target,
                         demote_arg_type Obj,
                         istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                //-------------------------------------------------------------------
                // Same version
                //-------------------------------------------------------------------
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                boost::shared_ptr< Previous::FrTable > retval(
                    new Previous::FrTable( GetName( ), GetNRow( ) ) );
                if ( retval )
                {
                    retval->AppendComment( GetComment( ) );
                }
                if ( Stream )
                {
                    Stream->ReplaceRef(
                        retval->RefColumn( ), RefColumn( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented( "Object* FrTable::Demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrTable::promote_ret_type
        FrTable::promote( INT_2U           Source,
                          promote_arg_type Obj,
                          istream_type*    Stream ) const
        {
            return PromoteObject< Previous::FrTable, FrTable >(
                DATA_FORMAT_VERSION, Source, Obj, Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
