//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrTOC.hh"

#include "framecpp/Version6/STRING.hh"

#include "../Common/FrTOCPrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        //===================================================================
        FrTOCProcData::FrTOCProcData( )
        {
        }

        FrTOCProcData::FrTOCProcData( Common::IStream& Stream,
                                      INT_4U           FrameCount )
        {
            nproc_type nproc;
            Stream >> nproc;
            if ( nproc && ( nproc != FrTOC::NO_DATA_AVAILABLE ) )
            {
                //---------------------------------------------------------------
                // Read in the information
                //---------------------------------------------------------------
                key_container_type::size_type s( nproc );
                std::vector< position_type >  positions( s * FrameCount );

                m_keys.resize( s );

                for ( auto& key : m_keys )
                  {
                    Common::FrTOC::string_stream_type::Read( Stream, key );
                  }
                Stream >> positions;
                //---------------------------------------------------------------
                // Move into structure.
                //---------------------------------------------------------------
                typedef FrameCPP::Common::FrTOCPositionInputFunctor<
                    FrTOCProcData::MapProc_type,
                    FrTOCProcData::name_type,
                    FrTOCProcData::position_type >
                    functor;

                functor f( m_info, positions.begin( ), FrameCount );

                std::for_each( m_keys.begin( ), m_keys.end( ), f );
            }
        }

        FrTOCProcData::MapProc_type::const_iterator
        FrTOCProcData::GetProc( const std::string& Channel ) const
        {
            return GetProc( ).find( Channel );
        }

        FrTOCProcData::MapProc_type::const_iterator
        FrTOCProcData::GetProc( INT_4U Channel ) const
        {
            if ( Channel >= GetProc( ).size( ) )
            {
                return GetProc( ).end( );
            }
            return GetProc( ).find( m_keys[ Channel ] );
        }

        void
        FrTOCProcData::QueryProc( const Common::TOCInfo& Info,
                                  INT_4U                 FrameOffset,
                                  INT_8U                 Position )
        {
            STRING name;

            Info.TOCQuery( Common::TOCInfo::IC_NAME,
                           Common::TOCInfo::DT_STRING_2,
                           &name,
                           Common::TOCInfo::IC_EOQ );

            proc_info_type& i( m_info[ name ] );
            i.resize( FrameOffset + 1 );
            i[ FrameOffset ] = Position;
        }

        //-------------------------------------------------------------------
        /// This method allows for iterting over each element of information
        /// and allows the caller to gather information about each element.
        //-------------------------------------------------------------------
        void
        FrTOCProcData::forEach( Common::FrTOC::query_info_type Info,
                                Common::FrTOC::FunctionBase&   Action ) const
        {
            switch ( Info )
            {
            case Common::FrTOC::TOC_CHANNEL_NAMES:
            {
                try
                {
                    Common::FrTOC::FunctionString& action(
                        dynamic_cast< Common::FrTOC::FunctionString& >(
                            Action ) );

                    for ( MapProc_type::const_iterator cur = m_info.begin( ),
                                                       last = m_info.end( );
                          cur != last;
                          ++cur )
                    {
                        action( cur->first );
                    }
                }
                catch ( ... )
                {
                    // Does not understand Action
                }
            }
            break;
            default:
                //---------------------------------------------------------------
                // ignore all other requests
                //---------------------------------------------------------------
                break;
            }
        }
    } // namespace Version_6
} // namespace FrameCPP
