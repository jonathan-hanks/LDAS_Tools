//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_6_FrameStreamBase_HH
#define FrameCPP_VERSION_6_FrameStreamBase_HH

#include <iostream>
#include <stdexcept>

#include "ldastoolsal/types.hh"

#include "framecpp/Version6/Dictionary.hh"
#include "framecpp/Version6/FrHeader.hh"

namespace FrameCPP
{
    namespace Version_6
    {
        class FrameStreamBase
        {
        public:
            FrameStreamBase(
                const std::vector< Dictionary::io_calls_type >& LibraryDefs,
                INT_4U NumberOfClassTypes );

            virtual ~FrameStreamBase( );

            //: Add a callback handler
            Dictionary::callback_key_type
            AddCallback( INT_2U ClassId, Dictionary::Callback& Callback );

            //: Retrieve the buffer holding the endian bytes for an 8 byte
            // quantity
            const unsigned char* const Get8ByteEndianness( ) const;

            //: Retrieve the byte order of the frame
            INT_2U GetByteOrder( ) const;

            //: Retrieve the version of the frame spec that generated the stream
            INT_2U GetLibraryRevision( ) const;

            //: Retrieve the version of the frame spec that generated the stream
            INT_2U GetVersion( ) const;

            void RemoveCallback( Dictionary::callback_key_type Key );

        protected:
            // Header structure for the stream
            FrHeader m_header;

            // Dictionary for the stream
            Dictionary m_dictionary;

        }; // class - FrameStreamBase

        inline Dictionary::callback_key_type
        FrameStreamBase::AddCallback( INT_2U                ClassId,
                                      Dictionary::Callback& Callback )
        {
            return m_dictionary.AddCallback( ClassId, Callback );
        }

        inline const unsigned char* const
        FrameStreamBase::Get8ByteEndianness( ) const
        {
            return m_header.Get8ByteEndianness( );
        }

        inline INT_2U
        FrameStreamBase::GetByteOrder( ) const
        {
            return m_header.GetByteOrder( );
        }

        inline INT_2U
        FrameStreamBase::GetLibraryRevision( ) const
        {
            return m_header.GetLibraryRevision( );
        }

        inline INT_2U
        FrameStreamBase::GetVersion( ) const
        {
            return m_header.GetVersion( );
        }

        inline void
        FrameStreamBase::RemoveCallback( Dictionary::callback_key_type Key )
        {
            return m_dictionary.RemoveCallback( Key );
        }

    } // namespace Version_6
} // namespace FrameCPP

#endif /* FrameCPP_VERSION_6_FrameStreamBase_HH */
