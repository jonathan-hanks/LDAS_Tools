//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_6_StreamRefData_HH
#define FrameCPP_VERSION_6_StreamRefData_HH

#include "framecpp/Common/FrDataObject.hh"

// #include "framecpp/Version6/FrCommon.hh"

namespace FrameCPP
{
    namespace Version_6
    {
        class StreamRefData : public Common::FrDataObject
        {
        public:
            typedef INT_8U length_type;
            typedef INT_2U class_id_type;
            typedef INT_4U instance_type;

            StreamRefData( length_type   Length,
                           class_id_type ClassId,
                           instance_type Instance );

            StreamRefData( const StreamRefData& Source );

            virtual ~StreamRefData( );

            /// \brief number of bytes needed to write this structure.
            virtual INT_8U Bytes( ) const;

            /// \brief number of bytes needed to write specific version.
            static INT_8U BytesForVersion( INT_2U Version );

            class_id_type GetClassId( ) const;

            instance_type GetInstance( ) const;

            length_type GetLength( ) const;

            void SetClassId( class_id_type ClassId );

            void SetInstance( instance_type Instance );

            void SetLength( );

            void SetLength( length_type Length );

            const StreamRefData& operator=( const StreamRefData& Source );

        protected:
            length_type   length;
            class_id_type class_id;
            instance_type instance;
        }; // class StreamRefData

        inline StreamRefData::StreamRefData( length_type   Length,
                                             class_id_type ClassId,
                                             instance_type Instance )
            : length( Length ), class_id( ClassId ), instance( Instance )
        {
        }

        inline StreamRefData::StreamRefData( const StreamRefData& Source )
            : length( Source.length ), class_id( Source.class_id ),
              instance( Source.instance )
        {
        }

        inline StreamRefData::~StreamRefData( )
        {
        }

        inline INT_8U
        StreamRefData::Bytes( ) const
        {
            return sizeof( length_type ) // length;
                + sizeof( class_id_type ) // class_id;
                + sizeof( instance_type ); // instance;
        }

        inline StreamRefData::class_id_type
        StreamRefData::GetClassId( ) const
        {
            return class_id;
        }

        inline StreamRefData::instance_type
        StreamRefData::GetInstance( ) const
        {
            return instance;
        }

        inline StreamRefData::length_type
        StreamRefData::GetLength( ) const
        {
            return length;
        }

        inline void
        StreamRefData::SetClassId( class_id_type ClassId )
        {
            class_id = ClassId;
        }

        inline void
        StreamRefData::SetInstance( instance_type Instance )
        {
            instance = Instance;
        }

        inline void
        StreamRefData::SetLength( )
        {
            length = Bytes( );
        }

        inline void
        StreamRefData::SetLength( length_type Length )
        {
            length = Length;
        }

        inline const StreamRefData&
        StreamRefData::operator=( const StreamRefData& Source )
        {
            length = Source.length;
            class_id = Source.class_id;
            instance = Source.instance;
            return *this;
        }
    } // namespace Version_6
} // namespace FrameCPP

#endif /* FrameCPP_VERSION_6_StreamRefData_HH */
