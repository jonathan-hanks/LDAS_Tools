//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <cmath>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrDetector.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

static inline REAL_8
DMSToRadians( INT_2S Degrees, INT_2S Minutes, REAL_4 Seconds )
{
    REAL_8 retval = 0.0;

    if ( Degrees >= 0 )
    {
        retval =
            (REAL_8)Degrees + (REAL_8)Minutes / 60. + (REAL_8)Seconds / 3600.;
    }
    else
    {
        retval =
            (REAL_8)Degrees - (REAL_8)Minutes / 60. - (REAL_8)Seconds / 3600.;
    }
    retval = ( retval * M_PI ) / 180.;

    return retval;
}

static inline void
RadiansToDMS( REAL_8  Radians,
              INT_2S& Degrees,
              INT_2S& Minutes,
              REAL_4& Seconds )
{
    Radians = ( Radians * 180 ) / M_PI;
    Degrees = INT_2S( Radians );
    Radians -= Degrees;
    if ( Degrees < 0 )
    {
        // Convert to positive
        Radians *= -1;
    }
    Radians *= 60;
    Minutes = INT_2S( Radians );
    Radians -= Minutes;
    Radians *= 60;
    Seconds = INT_2S( Radians );
}

static const INT_2U MAX_REF = 2;

namespace FrameCPP
{
    namespace Version_6
    {
        //=======================================================================
        // FrDetector::fr_detector_data_type
        //=======================================================================

        bool
        FrDetector::fr_detector_data_type::
        operator==( const fr_detector_data_type& RHS ) const
        {
#define CMP__( X ) ( X == RHS.X )

            return ( ( &RHS == this ) ||
                     ( CMP__( name ) && CMP__( prefix[ 0 ] ) &&
                       CMP__( prefix[ 1 ] ) && CMP__( longitude ) &&
                       CMP__( latitude ) && CMP__( elevation ) &&
                       CMP__( armXazimuth ) && CMP__( armYazimuth ) &&
                       CMP__( armXaltitude ) && CMP__( armYaltitude ) &&
                       CMP__( armXmidpoint ) && CMP__( armYmidpoint ) &&
                       CMP__( localTime ) && CMP__( aux ) && CMP__( table ) ) );

#undef CMP__
        }

        //=======================================================================
        // FrDetector
        //=======================================================================
        FrDetector::FrDetector( ) : Common::FrDetector( StructDescription( ) )
        {
            m_data.prefix[ 0 ] = ' ';
            m_data.prefix[ 1 ] = ' ';
            m_data.longitude = 0.0;
            m_data.latitude = 0.0;
            m_data.elevation = 0.0;
            m_data.armXazimuth = 0.0;
            m_data.armYazimuth = 0.0;
            m_data.armXaltitude = 0.0;
            m_data.armYaltitude = 0.0;
            m_data.armXmidpoint = 0.0;
            m_data.armYmidpoint = 0.0;
            m_data.localTime = 0;
        }

        FrDetector::FrDetector( const std::string& Name,
                                const char*        Prefix,
                                const REAL_8       Longitude,
                                const REAL_8       Latitude,
                                const REAL_4       Elevation,
                                const REAL_4       ArmXazimuth,
                                const REAL_4       ArmYazimuth,
                                const REAL_4       ArmXaltitude,
                                const REAL_4       ArmYaltitude,
                                const REAL_4       ArmXmidpoint,
                                const REAL_4       ArmYmidpoint,
                                const INT_4S       LocalTime )
            : Common::FrDetector( StructDescription( ) )
        {
            m_data.name = Name;
            m_data.prefix[ 0 ] = Prefix[ 0 ];
            m_data.prefix[ 1 ] = Prefix[ 1 ];
            m_data.longitude = Longitude;
            m_data.latitude = Latitude;
            m_data.elevation = Elevation;
            m_data.armXazimuth = ArmXazimuth;
            m_data.armYazimuth = ArmYazimuth;
            m_data.armXaltitude = ArmXaltitude;
            m_data.armYaltitude = ArmYaltitude;
            m_data.armXmidpoint = ArmXmidpoint;
            m_data.armYmidpoint = ArmYmidpoint;
            m_data.localTime = LocalTime;
        }

        FrDetector::FrDetector( const FrDetector& detector )
            : Common::FrDetector( StructDescription( ) ), Common::TOCInfo(
                                                              detector )
        {
            AT( );
            m_data.name = detector.m_data.name;
            AT( );
            m_data.prefix[ 0 ] = detector.m_data.prefix[ 0 ];
            AT( );
            m_data.prefix[ 1 ] = detector.m_data.prefix[ 1 ];
            AT( );
            m_data.longitude = detector.m_data.longitude;
            AT( );
            m_data.latitude = detector.m_data.latitude;
            AT( );
            m_data.elevation = detector.m_data.elevation;
            AT( );
            m_data.armXazimuth = detector.m_data.armXazimuth;
            AT( );
            m_data.armYazimuth = detector.m_data.armYazimuth;
            AT( );
            m_data.armXaltitude = detector.m_data.armXaltitude;
            AT( );
            m_data.armYaltitude = detector.m_data.armYaltitude;
            AT( );
            m_data.armXmidpoint = detector.m_data.armXmidpoint;
            AT( );
            m_data.armYmidpoint = detector.m_data.armYmidpoint;
            AT( );
            m_data.localTime = detector.m_data.localTime;
            AT( );
            m_data.aux = detector.m_data.aux;
            AT( );
            m_data.table = detector.m_data.table;
            AT( );
        }

        FrDetector::FrDetector( Previous::FrDetector& Source,
                                istream_type*         Stream )
            : Common::FrDetector( StructDescription( ) )
        {
            m_data.name = Source.GetName( );
            /// \todo need to do prefix lookup
            m_data.longitude = DMSToRadians( Source.GetLongitudeD( ),
                                             Source.GetLongitudeM( ),
                                             Source.GetLongitudeS( ) );
            m_data.latitude = DMSToRadians( Source.GetLatitudeD( ),
                                            Source.GetLatitudeM( ),
                                            Source.GetLatitudeS( ) );
            m_data.elevation = Source.GetElevation( );
            m_data.armXazimuth = Source.GetArmXazimuth( );
            m_data.armYazimuth = Source.GetArmYazimuth( );

            // Convert from North of East to East of North
            m_data.armXazimuth = M_PI / 2 - m_data.armXazimuth;
            m_data.armYazimuth = M_PI / 2 - m_data.armYazimuth;

            m_data.armXaltitude = 0.0;
            m_data.armYaltitude = 0.0;
            m_data.armXmidpoint = 0.0;
            m_data.armYmidpoint = 0.0;
            m_data.localTime = 0;

            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                Stream->ReplaceRef( RefAux( ), Source.RefMore( ), MAX_REF );
                Stream->ReplaceRef(
                    RefTable( ), Source.RefMoreTable( ), MAX_REF );
            }
        }

        FrDetector::FrDetector( istream_type& Stream )
            : Common::FrDetector( StructDescription( ) )
        {
            Stream >> m_data.name;
            Stream.read( m_data.prefix, sizeof( m_data.prefix ) );
            Stream >> m_data.longitude >> m_data.latitude >> m_data.elevation >>
                m_data.armXazimuth >> m_data.armYazimuth >>
                m_data.armXaltitude >> m_data.armYaltitude >>
                m_data.armXmidpoint >> m_data.armYmidpoint >>
                m_data.localTime >> m_data.aux >> m_data.table;
            Stream.Next( this );
        }

        const std::string&
        FrDetector::GetName( ) const
        {
            return m_data.name;
        }

        FrameCPP::cmn_streamsize_type
        FrDetector::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( m_data.prefix ) +
                sizeof( m_data.longitude ) + sizeof( m_data.latitude ) +
                sizeof( m_data.elevation ) + sizeof( m_data.armXazimuth ) +
                sizeof( m_data.armYazimuth ) + sizeof( m_data.armXaltitude ) +
                sizeof( m_data.armYaltitude ) + sizeof( m_data.armXmidpoint ) +
                sizeof( m_data.armYmidpoint ) + sizeof( m_data.localTime ) +
                Stream.PtrStructBytes( ) // aux
                + Stream.PtrStructBytes( ) // table
                + Stream.PtrStructBytes( ) // next
                ;
        }

        FrDetector*
        FrDetector::Create( istream_type& Stream ) const
        {
            return new FrDetector( Stream );
        }

        const char*
        FrDetector::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrDetector::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrDetector::StructName( ),
                           FrDetector::STRUCT_ID,
                           "Detector Data Structure" ) );
                ret( FrSE( "name", "STRING" ) );
                ret( FrSE( "prefix",
                           "CHAR[2]"
                           "" ) );
                ret( FrSE( "longitude", "REAL_8" ) );
                ret( FrSE( "latitude", "REAL_8" ) );
                ret( FrSE( "elevation", "REAL_4" ) );
                ret( FrSE( "armXazimuth", "REAL_4" ) );
                ret( FrSE( "armYazimuth", "REAL_4" ) );
                ret( FrSE( "armXaltitude", "REAL_4" ) );
                ret( FrSE( "armYaltitude", "REAL_4" ) );
                ret( FrSE( "armXmidpoint", "REAL_4" ) );
                ret( FrSE( "armYmidpoint", "REAL_4" ) );
                ret( FrSE( "localTime", "INT_4S" ) );

                ret( FrSE( "aux", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "table",
                           PTR_STRUCT::Desc( FrTable::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrDetector::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrDetector::Write( ostream_type& Stream ) const
        {
            Stream << m_data.name;
            Stream.write( m_data.prefix, sizeof( m_data.prefix ) );
            Stream << m_data.longitude << m_data.latitude << m_data.elevation
                   << m_data.armXazimuth << m_data.armYazimuth
                   << m_data.armXaltitude << m_data.armYaltitude
                   << m_data.armXmidpoint << m_data.armYmidpoint
                   << m_data.localTime << m_data.aux << m_data.table;
            WriteNext( Stream );
        }

        FrDetector&
        FrDetector::Merge( const FrDetector& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        bool
        FrDetector::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        void
        FrDetector::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    case TOCInfo::IC_DETECTOR_PREFIX:
                    {
                        const char* p = GetPrefix( );
                        if ( p[ 0 ] )
                        {
                            if ( p[ 1 ] )
                            {
                                data->assign( p, 2 );
                            }
                            else
                            {
                                data->assign( p, 1 );
                            }
                        }
                    }
                    break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrDetector::demote_ret_type
        FrDetector::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Convert from Radians to degrees, minutes, seconds

                INT_2S lod, lom;
                REAL_4 los;
                INT_2S lad, lam;
                REAL_4 las;

                RadiansToDMS( GetLongitude( ), lod, lom, los );
                RadiansToDMS( GetLatitude( ), lad, lam, las );

                // Convert from East of North to North of East

                REAL_8 armXazimuth = GetArmXazimuth( );
                REAL_8 armYazimuth = GetArmYazimuth( );

                armXazimuth += M_PI / 2;
                armYazimuth += M_PI / 2;

                // Do actual down conversion
                boost::shared_ptr< Previous::FrDetector > retval(
                    new Previous::FrDetector( GetName( ),
                                              lod,
                                              lom,
                                              los,
                                              lad,
                                              lam,
                                              las,
                                              GetElevation( ),
                                              armXazimuth,
                                              armYazimuth ) );
                if ( Stream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    Stream->ReplaceRef(
                        retval->RefMore( ), RefAux( ), MAX_REF );
                    Stream->ReplaceRef(
                        retval->RefMoreTable( ), RefTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrDetector::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrDetector::promote_ret_type
        FrDetector::promote( INT_2U           Target,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
