//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version6/FrSerData.hh"

#include "framecpp/Version6/FrSerData.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_SER_DATA;

namespace FrameCPP
{
    namespace Version_6
    {
        //=======================================================================
        // FrSerData::fr_ser_data_data_type
        //=======================================================================

        bool
        FrSerData::fr_ser_data_data_type::
        operator==( const fr_ser_data_data_type& RHS ) const
        {
#define CMP__( X ) ( X == RHS.X )

            return ( ( &RHS == this ) ||
                     ( CMP__( name ) && CMP__( time ) && CMP__( sampleRate ) &&
                       CMP__( data ) && CMP__( serial ) && CMP__( table ) ) );
#undef CMP__
        }

    } // namespace Version_6
} // namespace FrameCPP
static const int MAX_REF = 2;

namespace FrameCPP
{
    namespace Version_6
    {
        //=======================================================================
        // FrSerData
        //=======================================================================

        FrSerData::FrSerData( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
        }

        FrSerData::FrSerData( const FrSerData& SerData )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              Common::TOCInfo( SerData )
        {
            m_data.name = SerData.m_data.name;
            m_data.time = SerData.m_data.time;
            m_data.sampleRate = SerData.m_data.sampleRate;
            m_data.data = SerData.m_data.data;
            m_data.serial = SerData.m_data.serial;
            m_data.table = SerData.m_data.table;
        }

        FrSerData::FrSerData( const std::string& name,
                              const GPSTime&     time,
                              REAL_4             sampleRate )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.name = name;
            m_data.time = time;
            m_data.sampleRate = sampleRate;
        }

        FrSerData::FrSerData( Previous::FrSerData& Source,
                              istream_type*        Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.name = Source.GetName( );
            m_data.time = Source.GetTime( );
            m_data.sampleRate = Source.GetSampleRate( );
            m_data.data = Source.GetData( );
            if ( Stream )
            {
                //-------------------------------------------------------------------
                // Modify references
                //-------------------------------------------------------------------
                Stream->ReplaceRef(
                    RefSerial( ), Source.RefSerial( ), MAX_REF );
                Stream->ReplaceRef( RefTable( ), Source.RefTable( ), MAX_REF );
            }
        }

        FrSerData::FrSerData( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            Stream >> m_data.name >> m_data.time >> m_data.sampleRate >>
                m_data.data >> m_data.serial >> m_data.table;
            Stream.Next( this );
        }

        FrameCPP::cmn_streamsize_type
        FrSerData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( INT_4U ) // timeSec
                + sizeof( INT_4U ) // timeNsec
                + sizeof( m_data.sampleRate ) + m_data.data.Bytes( ) +
                Stream.PtrStructBytes( ) // serial
                + Stream.PtrStructBytes( ) // table
                + Stream.PtrStructBytes( ) // next
                ;
        }

        FrSerData*
        FrSerData::Create( istream_type& Stream ) const
        {
            return new FrSerData( Stream );
        }

        const char*
        FrSerData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrSerData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrSerData::StructName( ),
                           s_object_id,
                           "Serial Data Structure" ) );

                ret( FrSE( "name", "STRING" ) );
                ret( FrSE( "timeSec", "INT_4U" ) );
                ret( FrSE( "timeNsec", "INT_4U" ) );
                ret( FrSE( "sampleRate", "REAL_4" ) );
                ret( FrSE( "data", "STRING" ) );

                ret( FrSE( "serial",
                           PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "table",
                           PTR_STRUCT::Desc( FrTable::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrSerData::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrSerData::Write( ostream_type& Stream ) const
        {
            Stream << m_data.name << m_data.time << m_data.sampleRate
                   << m_data.data << m_data.serial << m_data.table;
            WriteNext( Stream );
        }

        bool
        FrSerData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        const std::string&
        FrSerData::GetName( ) const
        {
            return m_data.name;
        }

        FrSerData&
        FrSerData::Merge( const FrSerData& RHS )
        {
            //:TODO: Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        void
        FrSerData::
#if WORKING_VIRTUAL_TOCQUERY
            TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
            vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
        {
            using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
            va_list vl;
            va_start( vl, InfoClass );
#endif /*  WORKING_VIRTUAL_TOCQUERY */

            while ( InfoClass != TOCInfo::IC_EOQ )
            {
                int data_type = va_arg( vl, int );
                switch ( data_type )
                {
                case TOCInfo::DT_STRING_2:
                {
                    STRING* data = va_arg( vl, STRING* );
                    switch ( InfoClass )
                    {
                    case TOCInfo::IC_NAME:
                        *data = GetName( );
                        break;
                    default:
                        goto cleanup;
                        break;
                    }
                }
                break;
                default:
                    // Stop processing
                    goto cleanup;
                }
                InfoClass = va_arg( vl, int );
            }
        cleanup:
#if WORKING_VIRTUAL_TOCQUERY
            va_end( vl )
#endif /*  WORKING_VIRTUAL_TOCQUERY */
                ;
        }

        FrSerData::demote_ret_type
        FrSerData::demote( INT_2U          Target,
                           demote_arg_type Obj,
                           istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            try
            {
                //-------------------------------------------------------------------
                // Copy non-reference information
                //-------------------------------------------------------------------
                // Do actual down conversion
                boost::shared_ptr< Previous::FrSerData > retval(
                    new Previous::FrSerData(
                        GetName( ), GetTime( ), GetSampleRate( ) ) );
                retval->SetData( GetData( ) );
                if ( Stream )
                {
                    //-----------------------------------------------------------------
                    // Modify references
                    //-----------------------------------------------------------------
                    Stream->ReplaceRef(
                        retval->RefSerial( ), RefSerial( ), MAX_REF );
                    Stream->ReplaceRef(
                        retval->RefTable( ), RefTable( ), MAX_REF );
                }
                //-------------------------------------------------------------------
                // Return demoted object
                //-------------------------------------------------------------------
                return retval;
            }
            catch ( ... )
            {
            }
            throw Unimplemented(
                "Object* FrSerData::Demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrSerData::promote_ret_type
        FrSerData::promote( INT_2U           Target,
                            promote_arg_type Obj,
                            istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
