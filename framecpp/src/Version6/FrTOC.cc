//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_ptr.hpp>

#include "ldastoolsal/regex.hh"
#include "ldastoolsal/regexmatch.hh"

#include "framecpp/Common/Description.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrTOC.hh"

using namespace FrameCPP::Version_6;

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        // FrTOC
        //===================================================================

        const INT_4U FrTOC::NO_DATA_AVAILABLE = 0xFFFFFFFF;

        FrTOC::FrTOC( ) : Common::FrTOC( StructDescription( ) )
        {
        }

        FrTOC::FrTOC( istream_type& Stream )
            : Common::FrTOC( StructDescription( ) ), FrTOCData( Stream ),
              FrTOCStatData( Stream ), FrTOCAdcData( Stream, GetNFrame( ) ),
              FrTOCProcData( Stream, GetNFrame( ) ),
              FrTOCSimData( Stream, GetNFrame( ) ),
              FrTOCSerData( Stream, GetNFrame( ) ),
              FrTOCSummary( Stream, GetNFrame( ) ), FrTOCEvent( Stream ),
              FrTOCSimEvent( Stream )
        {
        }

        FrTOC::FrTOC( const FrameCPP::Common::FrTOC* Source )
            : Common::FrTOC( StructDescription( ) )
        {
            const FrTOC* toc( dynamic_cast< const FrTOC* >( Source ) );

            if ( toc )
            {
                *this = *toc;
                return;
            }

            // \todo Support version 4 table of contents translation
        }

        //-----------------------------------------------------------------------
        /// Loop over members of the table of contents to gather information
        /// requested by the caller.
        //-----------------------------------------------------------------------
        void
        FrTOC::ForEach( query_info_type Info, FunctionBase& Action ) const
        {
            switch ( Info )
            {
            case TOC_CHANNEL_NAMES:
                //-------------------------------------------------------------------
                /// \todo Need to finish the list of channel names
                //-------------------------------------------------------------------
                FrTOCAdcData::forEach( Info, Action );
                FrTOCProcData::forEach( Info, Action );
                FrTOCSimData::forEach( Info, Action );
                break;
            case TOC_DETECTOR:
                FrTOCData::forEach( Info, Action );
                break;
            case TOC_FR_STRUCTS:
                FrTOCData::forEach( Info, Action );
                break;
            }
        }

        void
        FrTOC::FrStatDataQuery( const std::string&            NamePattern,
                                const LDASTools::AL::GPSTime& StartTime,
                                const LDASTools::AL::GPSTime& EndTime,
                                const INT_4U                  Version,
                                Common::FrStatData::Query&    Result ) const
        {
            //---------------------------------------------------------------------
            // Establish regex patterns
            //---------------------------------------------------------------------
            Regex                      name_regex( NamePattern );
            const stat_container_type& frStatData( GetStat( ) );

            RegexMatch name_match;

            //-----------------------------------------------------------------
            // Loop over all FrStatData structures looking for matches
            //-----------------------------------------------------------------
            for ( stat_container_type::const_iterator cur = frStatData.begin( ),
                                                      last = frStatData.end( );
                  cur != last;
                  ++cur )
            {
                //---------------------------------------------------------------
                // Verify name match
                //---------------------------------------------------------------
                if ( name_match.match( name_regex, ( *cur ).first.c_str( ) ) )
                {
                    //-------------------------------------------------------------
                    // Loop over each of the FrStatData instances for the given
                    //   FrStatData in the TOC
                    //-------------------------------------------------------------
                    INT_4U start;
                    INT_4U end;

                    Common::FrStatData::Query::Range(
                        StartTime, EndTime, start, end );

                    for ( INT_4U x = 0,
                                 last = cur->second.stat_instances.size( );
                          x < last;
                          ++x )
                    {
                        //-----------------------------------------------------------
                        // Add the FrStatData to the results along with any
                        // relevant
                        //   detector information.
                        //-----------------------------------------------------------
                        Result.Add(
                            start,
                            end,
                            Version,
                            cur->first,
                            cur->second.stat_instances[ x ].tStart,
                            cur->second.stat_instances[ x ].tEnd,
                            cur->second.stat_instances[ x ].version,
                            cur->second.stat_instances[ x ].positionStat,
                            cur->second.detector );
                    }
                }
            }
        }

        void
        FrTOC::IndexObject( object_type Object, std::streampos Position )
        {
            using Common::TOCInfo;

            if ( !Object )
            {
                //---------------------------------------------------------------
                // Object holds nothing of interest
                //---------------------------------------------------------------
                return;
            }
            const INT_8U position( Position );

            //-----------------------------------------------------------------
            // Identify object to look for specific information
            //-----------------------------------------------------------------
            try
            {
                //---------------------------------------------------------------
                // See if the object supports TOC querying.
                //---------------------------------------------------------------
                boost::shared_ptr< Common::TOCInfo > info(
                    boost::dynamic_pointer_cast< Common::TOCInfo >( Object ) );
                INT_4U frame_offset = ( m_dataQuality.size( ) <= 0 )
                    ? 0
                    : m_dataQuality.size( ) - 1;
                //---------------------------------------------------------------
                // Create object specific query
                //---------------------------------------------------------------
                switch ( Object->GetClass( ) )
                {
                case Common::FrameSpec::Info::FSI_FRAME_H:
                {
                    if ( m_dataQuality.size( ) == 0 )
                    {
                        info->TOCQuery( TOCInfo::IC_ULEAP_S,
                                        TOCInfo::DT_INT_2S,
                                        &m_ULeapS,
                                        TOCInfo::IC_EOQ );
                    }

                    INT_4U dq;
                    INT_4U gtimes, gtimen;
                    REAL_8 dt;
                    INT_4S run;
                    INT_4U frame;

                    info->TOCQuery( TOCInfo::IC_DATA_QUALITY,
                                    TOCInfo::DT_INT_4U,
                                    &dq,
                                    TOCInfo::IC_GTIME_S,
                                    TOCInfo::DT_INT_4U,
                                    &gtimes,
                                    TOCInfo::IC_GTIME_N,
                                    TOCInfo::DT_INT_4U,
                                    &gtimen,
                                    TOCInfo::IC_DT,
                                    TOCInfo::DT_REAL_8,
                                    &dt,
                                    TOCInfo::IC_RUN,
                                    TOCInfo::DT_INT_4S,
                                    &run,
                                    TOCInfo::IC_FRAME,
                                    TOCInfo::DT_INT_4U,
                                    &frame,
                                    TOCInfo::IC_EOQ );

                    m_dataQuality.push_back( dq );
                    m_GTimeS.push_back( gtimes );
                    m_GTimeN.push_back( gtimen );
                    m_dt.push_back( dt );
                    m_runs.push_back( run );
                    m_frame.push_back( frame );
                    m_positionH.push_back( Position );
                    m_nFirstADC.push_back( 0 );
                    m_nFirstSer.push_back( 0 );
                    m_nFirstTable.push_back( 0 );
                    m_nFirstMsg.push_back( 0 );
                }
                break;
                case Common::FrameSpec::Info::FSI_FR_DETECTOR:
                {
                    namedetector_type name;

                    info->TOCQuery( TOCInfo::IC_NAME,
                                    TOCInfo::DataType( name ),
                                    &name,
                                    TOCInfo::IC_EOQ );
                    namedetector_container_type::const_iterator cur_detector =
                        m_nameDetector.begin( );
                    positiondetector_container_type::iterator cur_position =
                        m_positionDetector.begin( );
                    while ( cur_detector != m_nameDetector.end( ) )
                    {
                        if ( name == *cur_detector )
                        {
                            break;
                        }
                        ++cur_detector;
                        ++cur_position;
                    }
                    if ( cur_detector == m_nameDetector.end( ) )
                    {
                        m_nameDetector.push_back( name );
                        m_positionDetector.push_back( position );
                    }
                    else
                    {
                        if ( Position < std::streampos( *cur_position ) )
                        {
                            *cur_position = position;
                        }
                    }
                }
                break;
                case Common::FrameSpec::Info::FSI_FR_ADC_DATA:
                    QueryAdc( *info, frame_offset, Position );
                    if ( ( m_nFirstADC.back( ) == 0 ) ||
                         ( position < m_nFirstADC.back( ) ) )
                    {
                        m_nFirstADC.back( ) = position;
                    }
                    break;
                case Common::FrameSpec::Info::FSI_FR_EVENT:
                    QueryEvent( *info, frame_offset, Position );
                    break;
                case Common::FrameSpec::Info::FSI_FR_MSG:
                    if ( ( m_nFirstMsg.back( ) == 0 ) ||
                         ( position < m_nFirstMsg.back( ) ) )
                    {
                        m_nFirstMsg.back( ) = position;
                    }
                    break;
                case Common::FrameSpec::Info::FSI_FR_PROC_DATA:
                    QueryProc( *info, frame_offset, Position );
                    break;
                case Common::FrameSpec::Info::FSI_FR_SER_DATA:
                    QuerySer( *info, frame_offset, Position );
                    if ( ( m_nFirstSer.back( ) == 0 ) ||
                         ( position < m_nFirstSer.back( ) ) )
                    {
                        m_nFirstSer.back( ) = position;
                    }
                    break;
                case Common::FrameSpec::Info::FSI_FR_SIM_DATA:
                    QuerySim( *info, frame_offset, Position );
                    break;
                case Common::FrameSpec::Info::FSI_FR_SIM_EVENT:
                    QuerySimEvent( *info, frame_offset, Position );
                    break;
                case Common::FrameSpec::Info::FSI_FR_STAT_DATA:
                    QueryStatData( *info, frame_offset, Position );
                    break;
                case Common::FrameSpec::Info::FSI_FR_SUMMARY:
                    QuerySum( *info, frame_offset, Position );
                    break;
                case Common::FrameSpec::Info::FSI_FR_SH:
                {
                    shname_type name;
                    shid_type   id;

                    info->TOCQuery( TOCInfo::IC_NAME,
                                    TOCInfo::DataType( name ),
                                    &name,

                                    TOCInfo::IC_ID,
                                    TOCInfo::DataType( id ),
                                    &id,
                                    TOCInfo::IC_EOQ );

                    m_SHid.push_back( id );
                    m_SHname.push_back( name );
                }
                break;
                case Common::FrameSpec::Info::FSI_FR_TABLE:
                    if ( ( m_nFirstTable.back( ) == 0 ) ||
                         ( position < m_nFirstTable.back( ) ) )
                    {
                        m_nFirstTable.back( ) = position;
                    }
                    break;
                default:
                    //-------------------------------------------------------------
                    // Ignore all other objects
                    //-------------------------------------------------------------
                    break;
                }
            }
            catch ( ... )
            {
                //---------------------------------------------------------------
                // Quietly ignore any object that throws exception while
                //   trying to index
                //---------------------------------------------------------------
            }
        }

        Common::FrameSpec::size_type
        FrTOC::Bytes( const Common::StreamBase& Stream ) const
        {
            return FrTOCData::Bytes( Stream ) + FrTOCStatData::Bytes( Stream ) +
                FrTOCAdcData::Bytes( Stream ) + FrTOCProcData::Bytes( Stream ) +
                FrTOCSimData::Bytes( Stream ) + FrTOCSerData::Bytes( Stream ) +
                FrTOCSummary::Bytes( Stream ) + FrTOCEvent::Bytes( Stream ) +
                FrTOCSimEvent::Bytes( Stream );
        }

        FrTOC*
        FrTOC::Create( ) const
        {
            return new FrTOC;
        }

        FrTOC*
        FrTOC::Create( istream_type& Stream ) const
        {
            return new FrTOC( Stream );
        }

        const char*
        FrTOC::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Common::Description*
        FrTOC::StructDescription( )
        {
            static Common::Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrTOC::StructName( ),
                           FrTOC::s_object_id,
                           "Table of Contents Structure" ) );

                FrTOCData::Description< FrSE >( ret );
                FrTOCStatData::Description< FrSE >( ret );
                FrTOCAdcData::Description< FrSE >( ret );
                FrTOCProcData::Description< FrSE >( ret );
                FrTOCSimData::Description< FrSE >( ret );
                FrTOCSerData::Description< FrSE >( ret );
                FrTOCSummary::Description< FrSE >( ret );
                FrTOCEvent::Description< FrSE >( ret );
                FrTOCSimEvent::Description< FrSE >( ret );
            }

            return &ret;
        }

        void
        FrTOC::Write( ostream_type& Stream ) const
        {
#if 0
      std::streampos pos = Stream.tellp( );

#define CHECK_POS( CLASS )                                                     \
    std::cerr << "DEBUG: " #CLASS ": wrote: " << ( Stream.tellp( ) - pos )     \
              << " of bytes: " << CLASS::Bytes( Stream ) << std::endl;         \
    pos = Stream.tellp( )
#else
#define CHECK_POS( CLASS )
#endif

            FrTOCData::write( Stream );
            CHECK_POS( FrTOCData );
            FrTOCStatData::write( Stream );
            CHECK_POS( FrTOCStatData );
            FrTOCAdcData::write( Stream );
            CHECK_POS( FrTOCAdcData );
            FrTOCProcData::write( Stream );
            CHECK_POS( FrTOCProcData );
            FrTOCSimData::write( Stream );
            CHECK_POS( FrTOCSimData );
            FrTOCSerData::write( Stream );
            CHECK_POS( FrTOCSerData );
            FrTOCSummary::write( Stream );
            CHECK_POS( FrTOCSummary );
            FrTOCEvent::write( Stream );
            CHECK_POS( FrTOCEvent );
            FrTOCSimEvent::write( Stream );
            CHECK_POS( FrTOCSimEvent );

#undef CHECK_POS
        }

        bool
        FrTOC::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            //:TODO: actually implement comparision operator
            throw std::runtime_error(
                "Comparison operator not supported for FrTOC" );
        }

        FrTOC::demote_ret_type
        FrTOC::demote( INT_2U          Target,
                       demote_arg_type Obj,
                       istream_type*   Stream ) const
        {
            throw Unimplemented( "Object* FrTOC::demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrTOC::promote_ret_type
        FrTOC::promote( INT_2U           Target,
                        promote_arg_type Obj,
                        istream_type*    Stream ) const
        {
            throw Unimplemented( "Object* FrTOC::promote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        //-------------------------------------------------------------------
        // Interface routines
        //-------------------------------------------------------------------
        INT_4U
        FrTOC::nFrame( ) const
        {
            return GetNFrame( );
        }

        const FrTOC::cmn_dt_container_type&
        FrTOC::dt( ) const
        {
            return GetDt( );
        }

        const FrTOC::cmn_GTimeS_container_type&
        FrTOC::GTimeS( ) const
        {
            return GetGTimeS( );
        }

        const FrTOC::cmn_GTimeN_container_type&
        FrTOC::GTimeN( ) const
        {
            return GetGTimeN( );
        }

        FrTOC::cmn_runs_container_type const&
        FrTOC::runs( ) const
        {
            return GetRuns( );
        }

        void
        FrTOC::loadHeader( Common::IStream& Stream )
        {
            FrTOCData::load( Stream );
        }

        FrTOC::cmn_position_type
        FrTOC::positionDetector( const std::string& Name ) const
        {
            namedetector_container_type     nd( GetNameDetector( ) );
            positiondetector_container_type pd( GetPositionDetector( ) );

            namedetector_container_type::const_iterator     ind( nd.begin( ) );
            positiondetector_container_type::const_iterator ipd( pd.begin( ) );

            while ( ind != nd.end( ) )
            {
                if ( ind->compare( Name ) == 0 )
                {
                    return *ipd;
                    break;
                }
                ++ind;
                ++ipd;
            }
            std::ostringstream msg;

            msg << "Unable to locate any detector name " << Name
                << " within the table of contents for the stream";
            throw std::range_error( msg.str( ) );
        }

        FrTOC::cmn_position_type
        FrTOC::positionH( INT_4U FrameIndex ) const
        {
            const FrTOCData::positionh_container_type& positionh =
                GetPositionH( );

            if ( FrameIndex >= positionh.size( ) )
            {
                std::ostringstream msg;

                msg << "Request for frame " << FrameIndex
                    << " exceeds the range of 0 through "
                    << ( positionh.size( ) - 1 );
                throw std::out_of_range( msg.str( ) );
            }
            return positionh[ FrameIndex ];
        }

        const FrTOC::cmn_name_container_type&
        FrTOC::nameADC( ) const
        {
            return FrTOCAdcData::m_keys;
        }

        FrTOC::cmn_position_type
        FrTOC::positionADC( INT_4U             FrameIndex,
                            const std::string& Channel ) const
        {
            return position_adc( FrameIndex, Channel );
        }

        FrTOC::cmn_position_type
        FrTOC::positionADC( INT_4U FrameIndex, INT_4U Channel ) const
        {
            return position_adc( FrameIndex, Channel );
        }

        FrTOC::cmn_position_type
        FrTOC::positionEvent( INT_4U             FrameIndex,
                              const std::string& Event ) const
        {
            return FrTOCEvent::positionEvent( FrameIndex, Event );
        }

        FrTOC::cmn_position_type
        FrTOC::positionEvent( const std::string& EventType, INT_4U Index ) const
        {
            return FrTOCEvent::positionEvent( EventType, Index );
        }

        const FrTOC::cmn_name_container_type&
        FrTOC::nameProc( ) const
        {
            return FrTOCProcData::m_keys;
        }

        FrTOC::cmn_position_type
        FrTOC::positionProc( INT_4U             FrameIndex,
                             const std::string& Channel ) const
        {
            return position_proc( FrameIndex, Channel );
        }

        FrTOC::cmn_position_type
        FrTOC::positionProc( INT_4U FrameIndex, INT_4U Channel ) const
        {
            return position_proc( FrameIndex, Channel );
        }

        const FrTOC::cmn_name_container_type&
        FrTOC::nameSer( ) const
        {
            return FrTOCSerData::m_keys;
        }

        FrTOC::cmn_position_type
        FrTOC::positionSer( INT_4U             FrameIndex,
                            const std::string& Channel ) const
        {
            //-----------------------------------------------------------------
            // Ensure the FrameIndex is within range
            //-----------------------------------------------------------------
            if ( FrameIndex >= GetNFrame( ) )
            {
                std::ostringstream msg;

                msg << "Request for frame " << FrameIndex
                    << " exceeds the range of 0 through "
                    << ( GetNFrame( ) - 1 );
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Try to find the channel
            //-----------------------------------------------------------------
            MapSer_type::const_iterator cur = GetSer( Channel );
            if ( cur == FrTOCSerData::m_info.end( ) )
            {
                //---------------------------------------------------------------
                // Channel name does not exist.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "No FrSerData structures with the name " << Channel;
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Return the offset
            //-----------------------------------------------------------------
            return cur->second[ FrameIndex ];
        }

        const FrTOC::cmn_name_container_type&
        FrTOC::nameSim( ) const
        {
            return FrTOCSimData::m_keys;
        }

        FrTOC::cmn_position_type
        FrTOC::positionSim( INT_4U             FrameIndex,
                            const std::string& Channel ) const
        {
            //-----------------------------------------------------------------
            // Ensure the FrameIndex is within range
            //-----------------------------------------------------------------
            if ( FrameIndex >= GetNFrame( ) )
            {
                std::ostringstream msg;

                msg << "Request for frame " << FrameIndex
                    << " exceeds the range of 0 through "
                    << ( GetNFrame( ) - 1 );
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Try to find the channel
            //-----------------------------------------------------------------
            MapSim_type::const_iterator cur = GetSim( Channel );
            if ( cur == FrTOCSimData::m_info.end( ) )
            {
                //---------------------------------------------------------------
                // Channel name does not exist.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "No FrSimData structures with the name " << Channel;
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Return the offset
            //-----------------------------------------------------------------
            return cur->second[ FrameIndex ];
        }

        FrTOC::cmn_position_type
        FrTOC::positionSimEvent( INT_4U             FrameIndex,
                                 const std::string& SimEvent ) const
        {
            return FrTOCSimEvent::positionSimEvent( FrameIndex, SimEvent );
        }

        FrTOC::cmn_position_type
        FrTOC::positionSimEvent( const std::string& EventType,
                                 INT_4U             Index ) const
        {
            return FrTOCSimEvent::positionSimEvent( EventType, Index );
        }

        INT_4U
        FrTOC::nSH( ) const
        {
            return m_SHid.size( );
        }

        INT_2U
        FrTOC::SHid( INT_4U Offset ) const
        {
            if ( Offset < m_SHid.size( ) )
            {
                return m_SHid[ Offset ];
            }
            std::ostringstream msg;
            msg << "The value of " << Offset << " exceeds the size ( "
                << m_SHid.size( ) << " ) of the SHid.";
            throw std::range_error( msg.str( ) );
        }

        const std::string&
        FrTOC::SHname( INT_4U Offset ) const
        {
            if ( Offset < m_SHname.size( ) )
            {
                return m_SHname[ Offset ];
            }
            std::ostringstream msg;
            msg << "The value of " << Offset << " exceeds the size ( "
                << m_SHname.size( ) << " ) of the SHname.";
            throw std::range_error( msg.str( ) );
        }

        template < typename ChannelType >
        FrTOC::cmn_position_type
        FrTOC::position_adc( INT_4U FrameIndex, ChannelType Channel ) const
        {
            //-----------------------------------------------------------------
            // Ensure the FrameIndex is within range
            //-----------------------------------------------------------------
            if ( FrameIndex >= GetNFrame( ) )
            {
                std::ostringstream msg;

                msg << "Request for frame " << FrameIndex
                    << " exceeds the range of 0 through "
                    << ( GetNFrame( ) - 1 );
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Try to find the channel
            //-----------------------------------------------------------------
            MapADC_type::const_iterator cur = GetADC( Channel );
            if ( cur == FrTOCAdcData::m_info.end( ) )
            {
                //---------------------------------------------------------------
                // Channel name does not exist.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "No FrAdcData structures with the name " << Channel;
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Return the offset
            //-----------------------------------------------------------------
            return cur->second.m_positionADC[ FrameIndex ];
        }

        template < typename ChannelType >
        FrTOC::cmn_position_type
        FrTOC::position_proc( INT_4U FrameIndex, ChannelType Channel ) const
        {
            //-----------------------------------------------------------------
            // Ensure the FrameIndex is within range
            //-----------------------------------------------------------------
            if ( FrameIndex >= GetNFrame( ) )
            {
                std::ostringstream msg;

                msg << "Request for frame " << FrameIndex
                    << " exceeds the range of 0 through "
                    << ( GetNFrame( ) - 1 );
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Try to find the channel
            //-----------------------------------------------------------------
            MapProc_type::const_iterator cur = GetProc( Channel );
            if ( cur == FrTOCProcData::m_info.end( ) )
            {
                //---------------------------------------------------------------
                // Channel name does not exist.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "No FrProcData structures with the name " << Channel;
                throw std::out_of_range( msg.str( ) );
            }
            //-----------------------------------------------------------------
            // Return the offset
            //-----------------------------------------------------------------
            return cur->second[ FrameIndex ];
        }

        void
        FrTOC::cacheAdcDataPositions( istream_type& Stream )
        {
            FrTOCAdcData::cachePositions( Stream );
        }

        void
        FrTOC::seekAdcDataPositions( istream_type&                  Stream,
                                     Common::FrTOC::channel_id_type Channel )
        {
            FrTOCAdcData::seekPositions( Stream, Channel );
        }

        void
        FrTOC::seekAdcDataPositions( istream_type&      Stream,
                                     const std::string& Channel )
        {
            FrTOCAdcData::seekPositions( Stream, Channel );
        }

        void
        FrTOC::procDataCachePositions( istream_type& Stream )
        {
            FrTOCProcData::cachePositions( Stream );
        }
    } // namespace Version_6
} // namespace FrameCPP
