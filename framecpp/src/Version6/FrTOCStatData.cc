//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/TOCInfo.hh"

#include "framecpp/Version6/FrameSpec.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"
#include "framecpp/Version6/FrTOC.hh"

#include "framecpp/Version6/STRING.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

namespace FrameCPP
{
    namespace Version_6
    {
        //===================================================================
        //===================================================================
        FrTOCStatData::FrTOCStatData( )
        {
        }

        FrTOCStatData::FrTOCStatData( Common::IStream& Stream )
        {
            nstat_type nstat;
            Stream >> nstat;
            if ( nstat && ( nstat != FrTOC::NO_DATA_AVAILABLE ) )
            {
                for ( nstat_type n = 0; n != nstat; ++n )
                {
                    //-------------------------------------------------------------
                    // Read in the information
                    //-------------------------------------------------------------
                    name_type           name;
                    detector_type       detector;
                    stat_type           stat;
                    nstat_instance_type nstat_instance;

                    Stream >> name >> detector >> nstat_instance;

                    if ( nstat_instance )
                    {
                        std::vector< tstart_type >  tstart( nstat_instance );
                        std::vector< tend_type >    tend( nstat_instance );
                        std::vector< version_type > version( nstat_instance );
                        std::vector< positionStat_type > positionStat(
                            nstat_instance );

                        Stream >> tstart >> tend >> version >> positionStat;

                        //-----------------------------------------------------------
                        // Move into structure.
                        //-----------------------------------------------------------
                        stat.stat_instances.resize( nstat_instance );
                        stat.detector = detector;
                        for ( nstat_instance_type cur = 0;
                              cur != nstat_instance;
                              ++cur )
                        {
                            stat.stat_instances[ cur ].tStart = tstart[ cur ];
                            stat.stat_instances[ cur ].tEnd = tend[ cur ];
                            stat.stat_instances[ cur ].version = version[ cur ];
                            stat.stat_instances[ cur ].positionStat =
                                positionStat[ cur ];
                        }
                    } // if ( nstat_instance )

                    m_info[ name ] = stat;
                }
            }
        }

        void
        FrTOCStatData::QueryStatData( const Common::TOCInfo& Info,
                                      INT_4U                 FrameOffset,
                                      INT_8U                 Position )
        {
            STRING name;
            STRING detector;
            INT_4U start;
            INT_4U end;
            INT_4U version;

            Info.TOCQuery( Common::TOCInfo::IC_NAME,
                           Common::TOCInfo::DataType( name ),
                           &name,
                           Common::TOCInfo::IC_DETECTOR,
                           Common::TOCInfo::DataType( detector ),
                           &detector,
                           Common::TOCInfo::IC_START,
                           Common::TOCInfo::DataType( start ),
                           &start,
                           Common::TOCInfo::IC_END,
                           Common::TOCInfo::DataType( end ),
                           &end,
                           Common::TOCInfo::IC_VERSION,
                           Common::TOCInfo::DataType( version ),
                           &version,
                           Common::TOCInfo::IC_EOQ );

            stat_instance_type si;

            si.tStart = start;
            si.tEnd = end;
            si.version = version;
            si.positionStat = Position;

            stat_type& i( m_info[ name ] );
            if ( ( i.detector.length( ) ) && ( i.detector != detector ) )
            {
                std::ostringstream msg;

                msg << "FrStatData with the name '" << name
                    << "' is associated with detector '" << i.detector
                    << "' and detector '" << detector << "'";
                throw std::runtime_error( msg.str( ) );
            }
            i.detector = detector;
            i.stat_instances.push_back( si );
        }

        void
        FrTOCStatData::write( Common::OStream& Stream ) const
        {
            if ( m_info.size( ) > 0 )
            {
                Stream << nstat_type( m_info.size( ) );
                for ( stat_container_type::const_iterator cur = m_info.begin( ),
                                                          last = m_info.end( );
                      cur != last;
                      ++cur )
                {
                    const nstat_type c( cur->second.stat_instances.size( ) );

                    Stream << cur->first << cur->second.detector << c;
                    if ( c )
                    {
                        //-----------------------------------------------------------
                        // Flatten the data
                        //-----------------------------------------------------------
                        std::vector< tstart_type >       tstart( c );
                        std::vector< tend_type >         tend( c );
                        std::vector< version_type >      version( c );
                        std::vector< positionStat_type > positionStat( c );
                        nstat_type                       x( 0 );

                        for ( stat_instance_container_type::const_iterator
                                  cur_si = cur->second.stat_instances.begin( ),
                                  last_si = cur->second.stat_instances.end( );
                              cur_si != last_si;
                              ++cur_si, ++x )
                        {
                            tstart[ x ] = cur_si->tStart;
                            tend[ x ] = cur_si->tEnd;
                            version[ x ] = cur_si->version;
                            positionStat[ x ] = cur_si->positionStat;
                        }

                        Stream << tstart << tend << version << positionStat;
                    }
                }
            }
            else
            {
#if WORKING
                Stream << nstat_type( FrTOC::NO_DATA_AVAILABLE );
#else /* WORKING */
                Stream << nstat_type( 0 );
#endif
            }
        }

    } // namespace Version_6
} // namespace FrameCPP
