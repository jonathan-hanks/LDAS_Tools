//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/CheckSumFilter.hh"
#include "framecpp/Common/Description.hh"

#include "framecpp/Version6/FrEndOfFrame.hh"
#include "framecpp/Version6/FrSE.hh"
#include "framecpp/Version6/FrSH.hh"

#include "framecpp/Common/Promotion.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::CheckSum;
using FrameCPP::Common::CheckSumFilter;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::OFrameStream;

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_END_OF_FRAME;

namespace FrameCPP
{
    namespace Version_6
    {
        FrEndOfFrame::FrEndOfFrame( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.chkType = 0;
            m_data.chkSum = 0;
        }

        FrEndOfFrame::FrEndOfFrame( INT_4S Run, INT_4U Frame )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.run = Run;
            m_data.frame = Frame;
        }

        FrEndOfFrame::FrEndOfFrame( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            Stream >> m_data.run >> m_data.frame >> m_data.chkType;
            //---------------------------------------------------------------------
            // Check for checksum calculations
            //---------------------------------------------------------------------
            CheckSumFilter* crc = Stream.GetCheckSumFrame( );

            if ( crc )
            {
                static const chkSum_type chkSum = 0;

                crc->Filter( &chkSum, sizeof( chkSum ) ); // Use 0 value
                crc->Value( ); // Stop processing
            }
            Stream >> m_data.chkSum;
        }

        FrEndOfFrame::~FrEndOfFrame( )
        {
        }

        FrameCPP::cmn_streamsize_type
        FrEndOfFrame::Bytes( const Common::StreamBase& Stream ) const
        {
            return sizeof( m_data.run ) + sizeof( m_data.frame ) +
                sizeof( m_data.chkType ) + sizeof( m_data.chkSum );
        }

        FrEndOfFrame*
        FrEndOfFrame::Create( istream_type& Stream ) const
        {
            return new FrEndOfFrame( Stream );
        }

        const char*
        FrEndOfFrame::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrEndOfFrame::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( "FrEndOfFrame",
                           s_object_id,
                           "End of Frame Data Structure" ) );
                ret( FrSE(
                    "run",
                    "INT_4S",
                    "Run number; same as in FrameHeader run number datum." ) );
                ret( FrSE(
                    "frame",
                    "INT_4U",
                    "Frame number, monotonically increasing until end of run;"
                    " same as in Frame Header run number datum" ) );
                ret( FrSE( "chkType",
                           "INT_4U",
                           "Same definition as in FrEndOfFile" ) );
                ret( FrSE( "chkSum", "INT_4U", "File checksum." ) );
            }

            return &ret;
        }

        void
        FrEndOfFrame::Write( ostream_type& Stream ) const
        {
            chkType_type chkType = CheckSum::NONE;
            chkSum_type  chkSum = 0;

            //---------------------------------------------------------------------
            // Calculate checksum
            //---------------------------------------------------------------------
            CheckSumFilter* crc = Stream.GetCheckSumFrame( );

            if ( crc )
            {
                chkType = crc->Type( );

                crc->Filter( &( m_data.run ), sizeof( m_data.run ) );
                crc->Filter( &( m_data.frame ), sizeof( m_data.frame ) );
                crc->Filter( &chkType, sizeof( chkType ) );
                crc->Filter( &chkSum, sizeof( chkSum ) );

                chkSum = crc->Value( );

                Stream.SetCheckSumFrame( CheckSum::NONE );
            }

            //---------------------------------------------------------------------
            // Write out to the stream
            //---------------------------------------------------------------------
            Stream << m_data.run << m_data.frame << chkType << chkSum;
        }

        FrEndOfFrame::demote_ret_type
        FrEndOfFrame::demote( INT_2U          Target,
                              demote_arg_type Obj,
                              istream_type*   Stream ) const
        {
            throw Unimplemented(
                "Object* FrEndOfFrame::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrEndOfFrame::promote_ret_type
        FrEndOfFrame::promote( INT_2U           Target,
                               promote_arg_type Obj,
                               istream_type*    Stream ) const
        {
            throw Unimplemented(
                "Object* FrEndOfFrame::promote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

#if WORKING
        void
        FrEndOfFrame::read( IFrameStream& Stream )
        {
            using namespace FrameCPP::Version_3_4_5;
            std::unique_ptr< EndOfFrame > eof5;
            switch ( Stream.GetVersion( ) )
            {
            case DATA_FORMAT_VERSION:
                Stream >> m_data.run >> m_data.frame >> m_data.chkType;
                //-------------------------------------------------------------------
                // Calculate the checksum for 0
                //-------------------------------------------------------------------
                m_data.chkSum = 0;
                if ( Stream.IsCalculatingChecksumFrame( ) )
                {
                    Stream.calcChecksumFrame( &( m_data.chkSum ),
                                              sizeof( m_data.chkSum ) );
                }
                Stream.read( &m_data.chkSum, sizeof( m_data.chkSum ), false );
                if ( Stream.IsCalculatingChecksumFile( ) )
                {
                    Stream.calcChecksumFile( &( m_data.chkSum ),
                                             sizeof( m_data.chkSum ) );
                }
                if ( Stream.IsCalculatingMD5Sum( ) )
                {
                    Stream.calcMD5Sum( &( m_data.chkSum ),
                                       sizeof( m_data.chkSum ) );
                }
                Stream.m_header.Reorder( m_data.chkSum );
                break;
            case 5:
                eof5.reset( EndOfFrame::read( Stream.GetFrame5Reader( ) ) );
                break;
            case 4:
                eof5.reset( EndOfFrame::read4( Stream.GetFrame5Reader( ) ) );
                break;
            case 3:
                eof5.reset( EndOfFrame::read3( Stream.GetFrame5Reader( ) ) );
                break;
            }
            if ( eof5.get( ) )
            {
                m_data.run = eof5->getRun( );
                m_data.frame = eof5->getFrame( );
                m_data.chkType = eof5->getChkType( );
                m_data.chkSum = eof5->getChkSum( );
            }
        }
#endif /* WORKING */

        bool
        FrEndOfFrame::operator==( const FrEndOfFrame& RHS ) const
        {
            return ( ( m_data.run == RHS.m_data.run ) &&
                     ( m_data.frame == RHS.m_data.frame ) &&
                     ( m_data.chkType == RHS.m_data.chkType ) &&
                     ( m_data.chkSum == RHS.m_data.chkSum ) );
        }

        bool
        FrEndOfFrame::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }
    } // namespace Version_6
} // namespace FrameCPP
