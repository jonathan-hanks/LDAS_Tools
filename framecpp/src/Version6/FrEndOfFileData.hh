//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP_VERSION_6_FrEndOfFileData_HH
#define FrameCPP_VERSION_6_FrEndOfFileData_HH

#include "framecpp/Common/FrDataObject.hh"

#include "framecpp/Version6/FrCommon.hh"

namespace FrameCPP
{
    namespace Version_6
    {
        class FrEndOfFileData : virtual public Common::FrDataObject
        {
        public:
            typedef INT_4U nFrames_type;
            typedef INT_8U nBytes_type;
            typedef INT_4U chkType_type;
            typedef INT_4U chkSum_type;
            typedef INT_8U seekTOC_type;

            FrEndOfFileData( );

            virtual ~FrEndOfFileData( );

            /// \brief number of bytes needed to write this structure.
            virtual INT_8U Bytes( ) const;

            /// \brief number of bytes needed to write specific version.
            static INT_8U BytesForVersion( INT_2U Version );

            //: Retrieve the number of frames in this file
            nFrames_type GetNFrames( ) const;

            //: Total number of bytes in this file (0 if NOT computed)
            nBytes_type GetNBytes( ) const;

            //: Checksum schemes
            chkType_type GetChkType( ) const;

            //: File checksum
            chkSum_type GetChkSum( ) const;

            //: Retrieve the start of the TOC, 0 if there is none.
            seekTOC_type GetSeekTOC( ) const;

            ///: Establish the number of bytes written
            void SetNBytes( nBytes_type NumberOfBytes );

            ///: Establish the number of frames written
            void SetNFrames( nFrames_type NumberOfFrames );

            ///: Establish where the toc is
            void SetSeekTOC( seekTOC_type Position );

        protected:
            nFrames_type         nFrames;
            nBytes_type          nBytes;
            mutable chkType_type chkType;
            mutable chkSum_type  chkSum;
            mutable seekTOC_type seekTOC;

            template < class IStream >
            IStream& readData( IStream& Stream );
        };

        inline FrEndOfFileData::~FrEndOfFileData( )
        {
        }

        inline INT_8U
        FrEndOfFileData::Bytes( ) const
        {
            return BytesForVersion( DATA_FORMAT_VERSION );
        }

        //: Total number of bytes in this file (0 if NOT computed)
        inline FrEndOfFileData::nBytes_type
        FrEndOfFileData::GetNBytes( ) const
        {
            return nBytes;
        }

        //: Total number of frames in this file (0 if NOT computed)
        inline FrEndOfFileData::nFrames_type
        FrEndOfFileData::GetNFrames( ) const
        {
            return nFrames;
        }

        //: Checksum schemes
        inline FrEndOfFileData::chkType_type
        FrEndOfFileData::GetChkType( ) const
        {
            return chkType;
        }

        //: File checksum
        inline FrEndOfFileData::chkSum_type
        FrEndOfFileData::GetChkSum( ) const
        {
            return chkSum;
        }

        inline FrEndOfFileData::seekTOC_type
        FrEndOfFileData::GetSeekTOC( ) const
        {
            return seekTOC;
        }

        inline void
        FrEndOfFileData::SetNBytes( nBytes_type NumberOfBytes )
        {
            nBytes = NumberOfBytes;
        }

        inline void
        FrEndOfFileData::SetNFrames( nFrames_type NumberOfFrames )
        {
            nFrames = NumberOfFrames;
        }

        inline void
        FrEndOfFileData::SetSeekTOC( seekTOC_type Position )
        {
            seekTOC = Position;
        }

    } // namespace Version_6
} // namespace FrameCPP

#endif /* FrameCPP_VERSION_6_FrEndOfFileData_HH */
