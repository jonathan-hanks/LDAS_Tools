#
# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#

add_subdirectory( v3 )
add_subdirectory( v4 )
add_subdirectory( v5 )
add_subdirectory( v8 )
add_subdirectory( v9 )

set( HDR_DIR framecpp/storage/data )
set( hhdir ${CMAKE_INSTALL_INCLUDEDIR}/${HDR_DIR} )

#=========================================================================
# Frame Source and Headers
#=========================================================================

set( src_hdrs
  FrameH.hh
  FrAdcData.hh
  FrObjectMetaData.hh
  FrSHData.hh
  FrSEData.hh
  FrMsg.hh
  )

set( hh_HEADERS ${src_hdrs} )


set( EXTRA_DIST ${SRCS} )

cx_link_headers( ${HDR_DIR} ${hh_HEADERS} )

#------------------------------------------------------------------------

cx_install_header(
  FILES ${hh_HEADERS}
  DESTINATION "${hhdir}"
  )

#------------------------------------------------------------------------

framecpp_target_gtest(
  test_data_types
  SOURCES
    data_types_test.cc
  INCLUDES
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}/src
    ${CMAKE_BINARY_DIR}/include
    ${CMAKE_BINARY_DIR}
    ${LDASTOOLS_INCLUDE_DIRS}
    ${Boost_INCLUDE_DIRS}
    ${GTEST_INCLUDE_DIRS}
  LDADD
    PUBLIC ${GTEST_LDFLAGS}
    PUBLIC ${GTEST_LIBRARIES}
    PUBLIC ${LDASTOOLS_LIBRARIES_FULL_PATH}
    PRIVATE Threads::Threads
    )
