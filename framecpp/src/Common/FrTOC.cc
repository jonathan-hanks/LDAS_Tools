//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/FrTOC.hh"

namespace FrameCPP
{
    namespace Common
    {
        FrTOC::FunctionBase::~FunctionBase( )
        {
        }

        FrTOC::FunctionC::~FunctionC( )
        {
        }

        FrTOC::FunctionString::~FunctionString( )
        {
        }

        FrTOC::FunctionSI::~FunctionSI( )
        {
        }

        FrTOC::FrTOC( const Description* Desc )
            : FrameSpec::Object( s_object_id, Desc )
        {
        }

        FrTOC::~FrTOC( )
        {
        }

        void
        FrTOC::ForEach( query_info_type Info, FunctionBase& Action ) const
        {
            //-----------------------------------------------------------------
            /// \todo
            ///     Need to decide if this functions should throw an
            ///     exception when called to indicate that it is not
            ///     implemented for the calling version of the frame
            ///     specification.
            //-----------------------------------------------------------------
        }

        INT_4U
        FrTOC::GetTOCChecksum( )
        {
            return ( 0 );
        }

    } // namespace Common
} // namespace FrameCPP
