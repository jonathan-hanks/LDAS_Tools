//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <algorithm>
#include <sstream>
#include <stdexcept>

#include "framecpp/Common/FrHeader.hh"
#include "framecpp/Common/IOStream.hh"

namespace FrameCPP
{
    namespace Common
    {
        static const char* DEFAULT_ORIGINATOR = "IGWD";

        FrHeader::FrHeader( IStream& Stream )
        {
            if ( Stream.tellg( ) )
            {
                std::ostringstream msg;
                msg << "Common::FrHeader cannot be initialized"
                    << " from non-zero stream position"
                    << " (pos: " << Stream.tellg( ) << ")";
                throw std::runtime_error( msg.str( ) );
            }
#if 1
            initialize( Stream );
#else
            Stream.read(
                reinterpret_cast< IStream::char_type* >( &( m_data.raw ) ),
                sizeof( m_data.raw ) );
            size_t here( Stream.tellg( ) );
            if ( here != sizeof( m_data.raw ) )
            {
                std::ostringstream msg;

                msg << "Failed to read the core of the FrHeader structure.";
                msg << " (" << here << " != " << sizeof( m_data.raw ) << ")";

                throw std::runtime_error( msg.str( ) );
            }
#endif
        }

        FrHeader::FrHeader( std::istringstream& Source )
        {
            if ( Source.tellg( ) )
            {
                std::ostringstream msg;

                msg << "Common::FrHeader cannot be initialized"
                    << " from non-zero stream position"
                    << " (pos: " << Source.tellg( ) << ")";
                throw std::runtime_error( msg.str( ) );
            }
            initialize( Source );
        }

        bool
        FrHeader::ByteSwapping( ) const
        {
            return false;
        }

        bool
        FrHeader::IsValid( ) const
        {
            return (
                ( ::strcmp( GetOriginator( ), DEFAULT_ORIGINATOR ) == 0 ) );
        }

        void
        FrHeader::SetOriginator( const std::string& Originator )
        {
            const int length( sizeof( m_data.raw.originator ) - 1 );

            strncpy( m_data.raw.originator, Originator.c_str( ), length );
            m_data.raw.originator[ length ] = '\0';
        }

        void
        FrHeader::Write( OStream& Stream ) const
        {
            Stream.write( reinterpret_cast< const Common::OStream::char_type* >(
                              &( m_data.raw ) ),
                          std::streamsize( sizeof( m_data.raw ) ) );
        }

        void
        FrHeader::complete( )
        {
        }

        //-------------------------------------------------------------------
        ///
        //-------------------------------------------------------------------
        void
        FrHeader::initialize( std::istream& Source )
        {
            const size_t start( Source.tellg( ) );

            Source.read( reinterpret_cast< std::istringstream::char_type* >(
                             &( m_data.raw ) ),
                         std::streamsize( sizeof( m_data.raw ) ) );
            const size_t stop( Source.tellg( ) );

            if ( ( stop - start ) != sizeof( m_data.raw ) )
            {
                std::ostringstream msg;

                msg << "Failed to read the core of the FrHeader structure.";
                msg << " (" << ( stop - start ) << "[" << start << ":" << stop
                    << "]"
                    << " != " << sizeof( m_data.raw ) << ")";

                throw std::runtime_error( msg.str( ) );
            }
        }
    } // namespace Common
} // namespace FrameCPP
