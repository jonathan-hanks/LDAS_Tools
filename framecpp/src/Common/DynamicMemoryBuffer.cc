//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include "framecpp/Common/StringStream.hh"
#include "framecpp/Common/DynamicMemoryBuffer.hh"

namespace FrameCPP
{
    namespace Common
    {
        //-------------------------------------------------------------------
        /// The default constructor will initialize all the parts used
        /// in the general case.
        //-------------------------------------------------------------------
        template < typename BT >
        DynamicMemoryBufferT< BT >::DynamicMemoryBufferT(
            bool ParentAutoDelete )
            : MemoryBufferT< BT >( std::ios::in, ParentAutoDelete )
        {
        }

        //-------------------------------------------------------------------
        /// Be careful to release all the resources that were created
        /// for this object.
        //-------------------------------------------------------------------
        template < typename BT >
        DynamicMemoryBufferT< BT >::~DynamicMemoryBufferT( )
        {
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        template < typename BT >
        void
        DynamicMemoryBufferT< BT >::NextBlock( const char* Buffer,
                                               size_type   Size )
        {
            scanner_type::NextBlock( Buffer, Size );
            //-----------------------------------------------------------------
            // Copy the bytes to the temporary output buffer
            //-----------------------------------------------------------------
            accumulated_buffer.write( Buffer, Size );
            if ( Ready( ) )
            {
                this->str( accumulated_buffer.str( ) );
            }
        }

        //-------------------------------------------------------------------
        //-------------------------------------------------------------------
        template < typename BT >
        void
        DynamicMemoryBufferT< BT >::Reset( )
        {
            //-----------------------------------------------------------------
            // Seek to the beginning of the buffer;
            //-----------------------------------------------------------------
            accumulated_buffer.seekp( std::streampos( 0 ) );
            accumulated_buffer.clear( );
            scanner_type::Reset( );
        }

        template class DynamicMemoryBufferT<>;
    } // namespace Common

} // namespace FrameCPP
