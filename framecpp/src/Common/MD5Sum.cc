//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <cstring>

#include <iomanip>
#include <iostream>

#include "framecpp/Common/MD5Sum.hh"

using std::memcpy;

#if HAVE_MD5_H && HAVE_LIBMD5
extern "C" {
#define PROTOTYPES 1
#if HAVE_MD5GLOBAL_H
#if !defined( SWIGIMPORTED )
#include <md5global.h>
#endif /* ! defined(SWIGIMPORTED) */
#endif /* HAVE_MD5GLOBAL_H */
#include <md5.h>
} /* extern "C" */

#define MD5_Final MD5Final
#define MD5_Init MD5Init
#define MD5_Update MD5Update

typedef unsigned char* md5_data_type;

#elif HAVE_COMMONCRYPTO_COMMONDIGEST_H
#if !defined( SWIGIMPORTED )
#include <CommonCrypto/CommonDigest.h>

#define MD5_CTX CC_MD5_CTX
#define MD5_Init CC_MD5_Init
#define MD5_Final CC_MD5_Final
#define MD5_Update CC_MD5_Update
#define MD5_LBLOCK CC_MD5_BLOCK_LONG

typedef void* md5_data_type;
#endif /* ! defined(SWIGIMPORTED) */

#elif HAVE_OPENSSL_MD5_H && HAVE_MD5_IN_CRYPTO
extern "C" {
#if !defined( SWIGIMPORTED )
#include <openssl/md5.h>
#endif /* ! defined(SWIGIMPORTED) */
} /* extern "C" */
typedef void* md5_data_type;

#endif /* OPENSSL_MD5_H */

#ifndef MD5_DIGEST_LENGTH
#define MD5_DIGEST_LENGTH 16
#endif /* MD5_DIGEST_LENGTH */

namespace
{
#ifdef MD5_LBLOCK
    bool
    operator==( const MD5_CTX& LHS, const MD5_CTX& RHS )
    {
#define MD5_CTX_CMP( a ) ( LHS.a == RHS.a )

        bool retval =
            ( MD5_CTX_CMP( A ) && MD5_CTX_CMP( B ) && MD5_CTX_CMP( C ) &&
              MD5_CTX_CMP( D ) && MD5_CTX_CMP( Nl ) && MD5_CTX_CMP( Nh ) &&
              std::equal( LHS.data, LHS.data + MD5_LBLOCK, RHS.data ) &&
              MD5_CTX_CMP( num ) );
#undef MD5_CTX_CMP
        return retval;
    }
#else /* MD5_LBLOCK */
    bool
    operator==( const MD5_CTX& LHS, const MD5_CTX& RHS )
    {
        bool retval =
            std::equal( LHS.state,
                        LHS.state +
                            ( sizeof( LHS.state ) / sizeof( *LHS.state ) ),
                        RHS.state ) &&
            std::equal( LHS.count,
                        LHS.count +
                            ( sizeof( LHS.count ) / sizeof( *LHS.count ) ),
                        RHS.state ) &&
            std::equal(
                LHS.buf_un.buf8,
                LHS.buf_un.buf8 +
                    ( sizeof( LHS.buf_un.buf8 ) / sizeof( *LHS.buf_un.buf8 ) ),
                RHS.buf_un.buf8 );
        return retval;
    }
#endif /* MD5_LBLOCK */
} // namespace

namespace FrameCPP
{
    namespace Common
    {
        struct MD5Sum::_state_type
        {
            bool    final;
            MD5_CTX context;
            CHAR_U  message_digest[ MD5_DIGEST_LENGTH ];

            inline _state_type( ) : final( false )
            {
                MD5_Init( &context );
            }

            inline _state_type( const _state_type& Source )
                : final( Source.final )
            {
                memcpy( &context, &Source.context, sizeof( context ) );
                memcpy( message_digest,
                        Source.message_digest,
                        sizeof( message_digest ) );
            }

            inline _state_type&
            operator=( const _state_type& Source )
            {
                final = Source.final;
                memcpy( &context, &Source.context, sizeof( context ) );
                memcpy( message_digest,
                        Source.message_digest,
                        sizeof( message_digest ) );
                return *this;
            }
        };

        MD5Sum::MD5Sum( ) : state( new _state_type )
        {
        }

        void
        MD5Sum::Finalize( ) const
        {
            if ( state->final == false )
            {
                MD5_Final( state->message_digest, &( state->context ) );
                state->final = true;
            }
        }

        inline bool
        MD5Sum::Finalized( ) const
        {
            return state->final;
        }

        void
        MD5Sum::Reset( )
        {
            MD5_Init( &( state->context ) );
        }

        void
        MD5Sum::Update( const data_type Data, size_type Length )
        {
            if ( state->final )
            {
                throw std::runtime_error( "MD5 error: Update called after "
                                          "value had been  finalized" );
            }
            MD5_Update( &( state->context ), Data, Length );
        }

        MD5Sum::md_type
        MD5Sum::Value( ) const
        {
            Finalize( );
            return static_cast< md_type >( state->message_digest );
        }

    } // namespace Common
} // namespace FrameCPP

FrameCPP::Common::MD5Sum::MD5Sum( const MD5Sum& Source )
    : state( new _state_type )
{
    *state = *( Source.state );
}

std::ostream&
FrameCPP::Common::MD5Sum::DumpIntermediate( std::ostream& Stream ) const
{
    //---------------------------------------------------------------------
    // Preserve stream formatting rules
    //---------------------------------------------------------------------

    std::ios_base::fmtflags sflags( Stream.flags( ) );
    Stream.setf( std::ios_base::hex, std::ios_base::basefield );
    Stream.unsetf( std::ios_base::uppercase );

    std::streamsize swidth( Stream.width( 8 ) );
    int             sfill( Stream.fill( '0' ) );

    //---------------------------------------------------------------------
    // Output our data
    //---------------------------------------------------------------------

#if 0
  Stream << "MD5Sum: A: " << state->context.A
	 << " B: " << state->context.B
	 << " C: " << state->context.C
	 << " D: " << state->context.D
	 << " Nl: " << state->context.Nl
	 << " Nh: " << state->context.Nh
	 << " num: " << state->context.num
    ;
#endif /* 0 */

    //---------------------------------------------------------------------
    // Restore the formatting rules
    //---------------------------------------------------------------------

    Stream.width( swidth );
    Stream.fill( sfill );
    Stream.flags( sflags );

    return Stream;
}

FrameCPP::Common::MD5Sum&
FrameCPP::Common::MD5Sum::operator=( const MD5Sum& Source )
{
    *state = *( Source.state );
    return *this;
}

bool
FrameCPP::Common::MD5Sum::operator==( const MD5Sum& RHS ) const
{
    bool retval = false;
    if ( &RHS == this )
    {
        retval = true;
    }
    else
    {
        if ( Finalized( ) == RHS.Finalized( ) )
        {
            if ( Finalized( ) )
            {
                retval = std::equal( state->message_digest,
                                     state->message_digest + MD5_DIGEST_LENGTH,
                                     RHS.state->message_digest );
            }
            else
            {
                retval = ( state->context == RHS.state->context );
            }
        }
    }
    return retval;
}

std::ostream&
std::operator<<( std::ostream& Stream, const FrameCPP::Common::MD5Sum& Data )
{
    Data.Finalize( ); // Do any finization
    //---------------------------------------------------------------------
    // Preserve stream formatting rules
    //---------------------------------------------------------------------

    std::ios_base::fmtflags sflags( Stream.flags( ) );
    Stream.setf( std::ios_base::hex, std::ios_base::basefield );
    Stream.unsetf( std::ios_base::uppercase );

    std::streamsize swidth( Stream.width( 2 ) );
    int             sfill( Stream.fill( '0' ) );

    //---------------------------------------------------------------------
    // Output our data
    //---------------------------------------------------------------------

    for ( INT_2U i = 0; i < sizeof( Data.state->message_digest ); ++i )
    {
        Stream << std::hex << std::setw( 2 ) << std::setfill( '0' )
               << (int)Data.state->message_digest[ i ];
    }

    //---------------------------------------------------------------------
    // Restore the formatting rules
    //---------------------------------------------------------------------

    Stream.width( swidth );
    Stream.fill( sfill );
    Stream.flags( sflags );

    return Stream;
}
