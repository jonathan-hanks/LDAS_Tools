//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream>
#include <sstream>

#include "framecpp/Common/FrameSpec.hh"
#include "framecpp/Common/FrameFilename.hh"

namespace
{
    inline void
    read_till_hyphen( std::istringstream& Stream, std::string& Buffer )
    {
        char c = 0;
        while ( Stream.good( ) && ( c != '-' ) )
        {
            Stream >> c;
            if ( ( !Stream.good( ) ) || ( c == '-' ) )
            {
                break;
            }
            Buffer += c;
        }
    }

} // namespace

const std::string
    FrameCPP::Common::FrameFilename::m_spec( "LIGO-T010150-00-E" );

FrameCPP::Common::FrameFilename::InvalidFrameFilename::InvalidFrameFilename(
    const std::string& Filename,
    const std::string& Spec,
    const char*        File,
    size_t             Line )
    : LdasException( Library::FRAMECPP,
                     FrameCPP::BAD_FRAME_FILE,
                     format( Filename, Spec ),
                     "",
                     File,
                     Line )
{
}

std::string
FrameCPP::Common::FrameFilename::InvalidFrameFilename::format(
    const std::string& Filename, const std::string& Spec )
{
    std::ostringstream msg;
    msg << "The frame file " << Filename
        << " does not conform to the naming convention as specified by "
           "document "
        << Spec;
    return msg.str( );
}

FrameCPP::Common::FrameFilename::FrameFilename( const std::string& Filename )
    : m_filename( Filename ), m_good( false )
{
    std::string::size_type p = m_filename.rfind( '/' );
    if ( p == std::string::npos )
    {
        m_directory = ".";
    }
    else
    {
        m_directory = Filename.substr( 0, p );
    }

    m_base = ( p == std::string::npos ) ? Filename : Filename.substr( ++p );
    std::istringstream ss( m_base );

    char c;

    // Read S
    read_till_hyphen( ss, m_s );
    // Read D
    read_till_hyphen( ss, m_d );
    if ( ss.good( ) )
    {
        // Read G
        ss >> m_g >> c;
        if ( ( c != '-' ) || ( !ss.good( ) ) )
        {
            return;
        }
        ss >> m_tt >> m_ext;
        if ( m_ext[ 0 ] != '.' )
        {
            return;
        }
        m_ext = m_ext.substr( 1 );
    }
    m_good = true;
}
