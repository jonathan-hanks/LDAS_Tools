//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <sstream>
#include <stdexcept>

#include "framecpp/Common/DetectorNames.hh"

namespace FrameCPP
{
    namespace Common
    {
        //-------------------------------------------------------------------
        /// The default constructor is used to ensure proper initialization
        /// of the data elements.
        //-------------------------------------------------------------------
        DetectorNames::DetectorNames( ) : m_frozen( false )
        {
        }

        //-------------------------------------------------------------------
        /// This method retrieves information about a detector.
        //-------------------------------------------------------------------
        const DetectorNames::info_type&
        DetectorNames::Detector( const std::string& Name ) const
        {
            detector_info_container_type::const_iterator pos =
                m_info.find( Name );

            if ( pos == m_info.end( ) )
            {
                //---------------------------------------------------------------
                /// \exception std::range_error
                ///     This exception is thrown if the Name parameter does
                ///     not reference an entry in the set of detectors.
                //---------------------------------------------------------------
                std::ostringstream msg;

                msg << "Unable to locate the detector named " << Name;
                throw std::range_error( msg.str( ) );
            }
            return pos->second;
        }

        //-------------------------------------------------------------------
        /// This adds detector infomation into the set of known detectors.
        /// This routine is intended to be used by the version specific
        /// initialization routine.
        //-------------------------------------------------------------------
        void
        DetectorNames::
        operator( )( const std::string&          DetectorName,
                     const std::string&          Detector,
                     const std::string&          Prefix,
                     const std::pair< int, int > DataQualityBits )
        {
            if ( m_frozen == false )
            {
                info_type& d( m_info[ DetectorName ] );

                d.s_detector = Detector;
                d.s_prefix = Prefix;
                d.s_data_quality_bits = DataQualityBits;
            }
        }
    } // namespace Common
} // namespace FrameCPP
