//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/FrEndOfFile.hh"

namespace FrameCPP
{
    namespace Common
    {
        FrEndOfFile::FrEndOfFile( const Description* Desc )
            : FrameSpec::Object( s_object_id, Desc )
        {
        }

        FrEndOfFile::~FrEndOfFile( )
        {
        }

        //-------------------------------------------------------------------
        /// Returns the file position of the start of the table of contents.
        /// If the stream has no table of contents, the value of zero
        /// is returned.
        //-------------------------------------------------------------------
        FrEndOfFile::seekTOC_cmn_type
        FrEndOfFile::SeekTOC( ) const
        {
            //-----------------------------------------------------------------
            // Default value is that there is no table of contents
            //-----------------------------------------------------------------
            return 0;
        }

    } // namespace Common
} // namespace FrameCPP
