//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FrameCPP__Common__FrTOCPrivate_HH
#define FrameCPP__Common__FrTOCPrivate_HH

namespace FrameCPP
{
    namespace Common
    {
        template < typename dest_type_,
                   typename name_type_,
                   typename position_type_ >
        class FrTOCAdcDataInputFunctor9
        {
        public:
            typedef INT_4U           frame_count_type;
            typedef dest_type_       dest_type;
            typedef name_type_       name_type;
            typedef position_type_   position_type;

            typedef typename std::vector< position_type >::const_iterator
                position_const_iterator;

            FrTOCAdcDataInputFunctor9( dest_type&                Destination,
                                      position_const_iterator   Position,
                                      frame_count_type          FrameCount )
                : destination( Destination ), cur_position( Position ),
                  frame_count( FrameCount ), index( INT_4U( 0 ) ),
                  it( Destination.begin( ) )
            {
            }

            void
            operator( )( const typename dest_type::key_type& Name )
            {
                typedef typename dest_type::value_type  value_type;
                typedef typename dest_type::mapped_type mapped_type;

                it = destination.insert(
                    it,
                    value_type( Name,
                                mapped_type( cur_position,
                                             cur_position + frame_count,
                                             index ) ) );
                cur_position += frame_count;
                ++index;
            }

        private:
            dest_type&                   destination;
            position_const_iterator      cur_position;
            frame_count_type             frame_count;
            INT_4U                       index;
            typename dest_type::iterator it;
        };

        template < typename dest_type_,
                   typename name_type_,
                   typename channel_id_type_,
                   typename group_id_type_,
                   typename position_type_ >
        class FrTOCAdcDataInputFunctor
        {
        public:
            typedef INT_4U           frame_count_type;
            typedef dest_type_       dest_type;
            typedef name_type_       name_type;
            typedef channel_id_type_ channel_id_type;
            typedef group_id_type_   group_id_type;
            typedef position_type_   position_type;

            typedef typename std::vector< channel_id_type >::const_iterator
                channel_id_const_iterator;
            typedef typename std::vector< group_id_type >::const_iterator
                group_id_const_iterator;
            typedef typename std::vector< position_type >::const_iterator
                position_const_iterator;

            FrTOCAdcDataInputFunctor( dest_type&                Destination,
                                      channel_id_const_iterator ChannelId,
                                      group_id_const_iterator   GroupId,
                                      position_const_iterator   Position,
                                      frame_count_type          FrameCount )
                : destination( Destination ), cur_channel_id( ChannelId ),
                  cur_group_id( GroupId ), cur_position( Position ),
                  frame_count( FrameCount ), index( INT_4U( 0 ) ),
                  it( Destination.begin( ) )
            {
            }

            void
            operator( )( const typename dest_type::key_type& Name )
            {
                typedef typename dest_type::value_type  value_type;
                typedef typename dest_type::mapped_type mapped_type;

                it = destination.insert(
                    it,
                    value_type( Name,
                                mapped_type( *cur_channel_id,
                                             *cur_group_id,
                                             cur_position,
                                             cur_position + frame_count,
                                             index ) ) );
                ++cur_channel_id;
                ++cur_group_id;
                cur_position += frame_count;
                ++index;
            }

        private:
            dest_type&                   destination;
            channel_id_const_iterator    cur_channel_id;
            group_id_const_iterator      cur_group_id;
            position_const_iterator      cur_position;
            frame_count_type             frame_count;
            INT_4U                       index;
            typename dest_type::iterator it;
        };

        template < typename dest_type_,
                   typename name_type_,
                   typename position_type_ >
        class FrTOCPositionInputFunctor
        {
        public:
            typedef INT_4U         frame_count_type;
            typedef dest_type_     dest_type;
            typedef name_type_     name_type;
            typedef position_type_ position_type;

            typedef typename std::vector< position_type >::const_iterator
                position_const_iterator;

            FrTOCPositionInputFunctor( dest_type&              Destination,
                                       position_const_iterator Position,
                                       frame_count_type        FrameCount )
                : destination( Destination ), cur_position( Position ),
                  frame_count( FrameCount ), it( Destination.begin( ) )
            {
            }

            void
            operator( )( const typename dest_type::key_type& Name )
            {
                typedef typename dest_type::value_type  value_type;
                typedef typename dest_type::mapped_type mapped_type;

                it = destination.insert(
                    it,
                    value_type( Name,
                                mapped_type( cur_position,
                                             cur_position + frame_count ) ) );
                cur_position += frame_count;
            }

        private:
            dest_type&                   destination;
            position_const_iterator      cur_position;
            frame_count_type             frame_count;
            typename dest_type::iterator it;
        };

    } // namespace Common
} // namespace FrameCPP

#endif /* FrameCPP__Common__FrTOCPrivate_HH */
