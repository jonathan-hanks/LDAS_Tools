//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

#include "ldastoolsal/types.hh"

#include "framecpp/Common/Compression.hh"

namespace FrameCPP
{
    namespace Compression
    {
        //-------------------------------------------------------------------
        /// \brief Routines related to the differential compression algorithm
        //-------------------------------------------------------------------
        namespace Differential
        {
            template < class T >
            void
            Encode( CHAR_U* Data, INT_4U NData )
            {
                if ( NData > 1 )
                {
                    T* cur( reinterpret_cast< T* >( Data ) );
                    T* pre( reinterpret_cast< T* >( Data ) );

                    //---------------------------------------------------------------
                    // Go backwards to eliminate the necisity of temporaries
                    //---------------------------------------------------------------
                    cur = &( cur[ NData - 1 ] );
                    pre = &( pre[ NData - 2 ] );

                    while ( cur != (T*)Data )
                    {
                        *cur -= *pre;
                        --cur;
                        --pre;
                    }
                }
            }

            template < class T >
            void
            Encode( const CHAR_U* Data, INT_4U NData, output_type& Dest )
            {
                Dest.reset( new CHAR_U[ sizeof( T ) * NData ] );

                T* out = reinterpret_cast< T* >( Dest.get( ) );

                if ( NData > 1 )
                {
                    const T* cur( reinterpret_cast< const T* >( Data ) );
                    const T* pre( reinterpret_cast< const T* >( Data ) );

                    //---------------------------------------------------------------
                    // Preserve the zeroth element
                    //---------------------------------------------------------------
                    *out = *cur;
                    //---------------------------------------------------------------
                    // Go backwards to eliminate the necisity of temporaries
                    //---------------------------------------------------------------
                    cur = &( cur[ NData - 1 ] );
                    pre = &( pre[ NData - 2 ] );
                    out = &( out[ NData - 1 ] );

                    while ( --NData )
                    {
                        *out = *cur - *pre;
                        --cur;
                        --pre;
                        --out;
                    }
                }
            }

            template < class T >
            void
            Decode( CHAR_U* Data, INT_4U NData )
            {
                if ( NData > 1 )
                {
                    T* pre( reinterpret_cast< T* >( Data ) );
                    T* cur( &( pre[ 1 ] ) );

                    while ( --NData > 0 )
                    {
                        *cur += *pre;
                        ++cur;
                        ++pre;
                    }
                }
            }

            template < class T >
            void
            Decode( const CHAR_U* Data, INT_4U NData, output_type& Out )
            {
                Out.reset( new CHAR_U[ sizeof( T ) * NData ] );

                T* out( reinterpret_cast< T* >( Out.get( ) ) );

                const T* cur( reinterpret_cast< const T* >( Data ) );

                *out = *cur;

                if ( NData > 1 )
                {
                    //---------------------------------------------------------------
                    // Go forward for the decoding process
                    //---------------------------------------------------------------
                    const T* pre( reinterpret_cast< const T* >( Data ) );

                    out++;
                    cur++;

                    while ( --NData > 0 )
                    {
                        *out = *cur + *pre;
                        ++cur;
                        ++pre;
                        ++out;
                    }
                }
            }

#define INSTANTIATE( LM_TYPE )                                                 \
    template void Decode< LM_TYPE >( CHAR_U*, INT_4U );                        \
    template void Decode< LM_TYPE >( const CHAR_U*, INT_4U, output_type& );    \
    template void Encode< LM_TYPE >( CHAR_U*, INT_4U );                        \
    template void Encode< LM_TYPE >( const CHAR_U*, INT_4U, output_type& )

            INSTANTIATE( CHAR_U );
            INSTANTIATE( CHAR );
            INSTANTIATE( INT_2U );
            INSTANTIATE( INT_2S );
            INSTANTIATE( INT_4U );
            INSTANTIATE( INT_4S );
            INSTANTIATE( INT_8U );
            INSTANTIATE( INT_8S );
            INSTANTIATE( REAL_4 );
            INSTANTIATE( REAL_8 );

#undef INSTANTIATE
        } // namespace Differential

    } // namespace Compression

} // namespace FrameCPP
