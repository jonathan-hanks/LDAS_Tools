//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#if HAVE_CONFIG_H
#include <framecpp_config.h>
#endif /* HAVE_CONFIG_H */

#include <algorithm>
#include <iostream>
#include <set>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#define BOOST_TEST_NO_MAIN

#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>
#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/CommandLineOptions.hh"
#include "ldastoolsal/fstream.hh"
#include "ldastoolsal/gpstime.hh"
#include "ldastoolsal/types.hh"

#include "framecpp/Common/FrameSpec.hh"
#include "framecpp/Common/CheckSum.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameBuffer.hh"
#include "framecpp/Common/FrameStream.hh"

#include "framecpp/FrameCPP.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrVect.hh"

#include "framecpp/Dimension.hh"

#include "FrSample.hh"
#include "FrSample3.tcc"
#include "FrSample4.tcc"
#include "FrSample6.tcc"
#include "FrSample7.tcc"
#include "FrSample8.tcc"
#include "FrSample9.tcc"

namespace po = boost::program_options;

using LDASTools::AL::filebuf;
using LDASTools::AL::MemChecker;

typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
typedef CommandLineOptions::Option        Option;
typedef CommandLineOptions::OptionSet     OptionSet;
using FrameCPP::Common::CheckSum;
using FrameCPP::Common::FrameBuffer;
using FrameCPP::Common::OFrameStream;
using FrameCPP::Common::OStream;

using LDASTools::AL::GPSTime;
using std::ostringstream;
using std::string;

using FrameCPP::Dimension;

using FrameCPP::FrAdcData;
using FrameCPP::FrameH;
using FrameCPP::FrRawData;
using FrameCPP::FrVect;

// The name of the program executable
typedef std::set< std::string > channel_set_type;

typedef boost::shared_ptr< FrameH >    frame_h_type;
typedef boost::shared_ptr< FrAdcData > fr_adc_data_type;
typedef boost::shared_ptr< FrRawData > fr_raw_data_type;
typedef boost::shared_ptr< FrVect >    fr_vect_type;

namespace std
{
    std::istream&
    operator>>( std::istream& InputStream, LDASTools::AL::GPSTime& StartTime )
    {
        INT_4U sec;

        InputStream >> sec;
        StartTime = GPSTime( sec, 0 );
        return InputStream;
    }

    std::istream&
    operator>>( std::istream& InputStream, channel_set_type& ChannelSet )
    {
        channel_set_type::value_type channel_name;

        InputStream >> channel_name;

        boost::to_upper( channel_name );
        ChannelSet.insert( channel_name );

        return InputStream;
    }
} // namespace std

//=======================================================================
// Forward declaration of local functions
//=======================================================================

//-----------------------------------------------------------------------
static void all_type_frame( const string&           Type,
                            const channel_set_type& ChannelSet,
                            const GPSTime&          Start,
                            const INT_4U            DeltaT,
                            const INT_4U            Iterations );

//-----------------------------------------------------------------------

static void all_type_iteration( const INT_4U            DeltaT,
                                const channel_set_type& ListOfChannels,
                                frame_h_type            Frame,
                                INT_4U*                 Channel,
                                INT_4U                  Iteration );

//-----------------------------------------------------------------------

template < class T >
static std::string
data_type( )
{
    return "";
}

//-----------------------------------------------------------------------

template < class T >
static fr_adc_data_type ramped_channel( INT_4U  DeltaT,
                                        INT_4U* Channel,
                                        INT_4U  Iteration,
                                        T       Start,
                                        T       Stop,
                                        T       Inc,
                                        INT_4U  SampleRate );

//-----------------------------------------------------------------------

template < class T >
static boost::shared_array< T >
ramp_data_create( INT_4U Samples, T Start, T Stop, T Inc );

//-----------------------------------------------------------------------

template < class T >
static string ramped_name( INT_4U Iteration );

//-----------------------------------------------------------------------

template < class T >
static fr_vect_type ramped_vector( INT_4U DeltaT,
                                   INT_4U SampleRate,
                                   INT_4U Iteration,
                                   T      Start,
                                   T      Stop,
                                   T      Inc );

//-----------------------------------------------------------------------

void
test_frame( int Version )
{
    using FrameCPP::Common::FrameSpec;

    //-------------------------------------------------------------------
    // Create the frame
    //-------------------------------------------------------------------
    stat_data_container_type stat_data;
    make_frame_ret_type      frame;

    switch ( Version )
    {
    case 3:
        frame = makeFrame< 3 >( stat_data );
        break;
    case 4:
        frame = makeFrame< 4 >( stat_data );
        break;
    case 6:
        frame = makeFrame< 6 >( stat_data );
        break;
    case 7:
        frame = makeFrame< 7 >( stat_data );
        break;
    case 8:
        frame = makeFrame< 8 >( stat_data );
        break;
    case 9:
      frame = makeFrame< 9 >( stat_data );
      break;
    default:
    {
        std::ostringstream msg;

        msg << "name makeFrame case for version: " << Version;
        throw std::runtime_error( msg.str( ) );
    }
    break;
    }
    //-------------------------------------------------------------------
    // Calculate the filename
    //-------------------------------------------------------------------
    std::ostringstream filename;

    filename << "Z-R_std_test_frame_ver" << Version << "-" << 600000000 << "-"
             << 1 << ".gwf";

    if ( frame.get( ) == (FrameSpec::Object*)NULL )
    {
        throw std::runtime_error( "Failed to create frame" );
    }
    //-------------------------------------------------------------------
    // Creation of io buffer.
    //-------------------------------------------------------------------
    FrameBuffer< filebuf >* obuf( new FrameBuffer< filebuf >( std::ios::out ) );
    try
    {
        //-----------------------------------------------------------------
        // Open the buffer and the stream
        //-----------------------------------------------------------------
        obuf->open( filename.str( ).c_str( ),
                    std::ios::out | std::ios::binary );

        OFrameStream ofs( obuf, Version );

        ofs.SetCheckSumFile( CheckSum::CRC );

        //-----------------------------------------------------------------
        // Write the FrStatData data to the stream
        //-----------------------------------------------------------------
        for ( stat_data_container_type::iterator cur = stat_data.begin( ),
                                                 last = stat_data.end( );
              cur != last;
              ++cur )
        {
            ofs.WriteFrStatData( *cur );
        }
        //-----------------------------------------------------------------
        // Write the frame to the file
        //-----------------------------------------------------------------
        ofs.WriteFrame( frame, CheckSum::CRC );
        //-----------------------------------------------------------------
        // Close the stream and file
        //-----------------------------------------------------------------
        ofs.Close( );
        obuf->close( );
    }
    catch ( ... )
    {
        obuf->close( );
        // unlink( filename.str( ).c_str( ) );
        throw;
    }

    //-------------------------------------------------------------------
    // Release resources associated with the FrStatData
    //-------------------------------------------------------------------
    stat_data.clear( );
}

//-----------------------------------------------------------------------
// Common tasks to be performed when exiting.
//-----------------------------------------------------------------------
inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

//-----------------------------------------------------------------------
/// \brief Class to handle command line options for this application
//-----------------------------------------------------------------------
class CommandLine
{
public:
    CommandLine( int ArgC, char** ArgV );

    inline const channel_set_type&
    ChannelType( ) const
    {
        return m_channel_set;
    }

    inline void
    ChannelType( const std::string& Value )
    {
        m_channel_set.insert( Value );
    }

    inline const std::string&
    Description( ) const
    {
        return m_description;
    }

    inline void
    Description( const std::string& Value )
    {
        m_description = Value;
    }

    inline INT_4U
    Duration( ) const
    {
        return m_duration;
    }

    inline void
    Duration( INT_4U Value )
    {
        m_duration = Value;
    }

    inline INT_4U
    Iterations( ) const
    {
        return m_iterations;
    }

    inline void
    Iterations( INT_4U Value )
    {
        m_iterations = Value;
    }

    inline const std::string&
    ProgramName( ) const
    {
        return m_program_name;
    }

    inline const GPSTime&
    StartTime( ) const
    {
        return m_start_time;
    }

    inline void
    StartTime( const GPSTime& Value )
    {
        m_start_time = Value;
    }

    inline bool
    TestFrameMode( ) const
    {
        return m_test_frame_mode;
    }

    inline void
    TestFrameMode( bool Value )
    {
        m_test_frame_mode = Value;
    }

    inline INT_4U
    Version( ) const
    {
        return m_version;
    }

    inline void
    Version( INT_4U Value )
    {
        m_version = Value;
    }

    inline void
    Usage( int ExitValue ) const
    {
        std::cerr << "Usage: " << ProgramName( ) << std::endl
                  << m_options << std::endl;
        depart( ExitValue );
    }

private:
    enum option_types
    {
        OPT_CHANNEL_TYPE,
        OPT_DESCRIPTION,
        OPT_DURATION,
        OPT_HELP,
        OPT_ITERATIONS,
        OPT_START_TIME,
        OPT_TEST_FRAME_MODE,
        OPT_VERSION
    };

    po::options_description m_options;
    channel_set_type        m_channel_set;
    std::string             m_description;
    INT_4U                  m_duration;
    INT_4U                  m_iterations;
    std::string             m_program_name;
    GPSTime                 m_start_time;
    bool                    m_test_frame_mode;
    INT_4U                  m_version;
};

CommandLine::CommandLine( int ArgC, char** ArgV )
    : m_description( "ilwd_test_frame" ), m_duration( 1 ), m_iterations( 1 ),
      m_start_time( GPSTime( 600000000, 0 ) ), m_test_frame_mode( false ),
      m_version( FRAME_SPEC_CURRENT )
{
    po::options_description desc( "Options" );

    m_program_name.assign( ArgV[ 0 ] );
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    m_options.add_options( )( "help,h", "Print help messages" )(
        "description",
        po::value< std::string >( &m_description )
            ->default_value( "ilwd_test_frame" ),
        "Specify the contents of the discription portion"
        " of the frame filename." )(
        "channel-type",
        po::value< channel_set_type >( &m_channel_set ),
        "Add a channel type to the output frame."
        " This option may appear multiple times on the command line to"
        " specify multiple channel types." )(
        "duration",
        po::value< INT_4U >( &m_duration )->default_value( 1 ),
        "The length of the frame in seconds." )(
        "iterations",
        po::value< INT_4U >( &m_iterations )->default_value( 1 ),
        "Number of sets of channels to produce."
        " This can be used to inflate the size of a frame." )(
        "start-time",
        po::value< GPSTime >( &m_start_time )
            ->default_value( GPSTime( 600000000, 0 ) ),
        "Number of sets of channels to produce."
        " This can be used to inflate the size of a frame." )(
        "test-frame-mode",
        po::value< bool >( &m_test_frame_mode )
            ->default_value( false )
            ->implicit_value( true ),
        "Number of sets of channels to produce."
        "Enable test frame mode" )(
        "version",
        po::value< INT_4U >( &m_version )->default_value( FRAME_SPEC_CURRENT ),
        "Verion of the frame specification to use for the output frame." );

    //---------------------------------------------------------------------
    // Parse the options specified on the command line
    //---------------------------------------------------------------------

    po::variables_map vm;
    po::store(
        po::command_line_parser( ArgC, ArgV ).options( m_options ).run( ), vm );
    po::notify( vm );

    if ( vm.count( "help" ) )
    {
        Usage( 0 );
    }
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
int
main( int ArgC, char* ArgV[] ) try
{
    MemChecker::Trigger gc_trigger( true );
    CommandLine         cl( ArgC, ArgV );

#if 0 || 1
#if 0
    if ( ( cl.empty( ) == false ) || ( cl.BadOption( ) ) )
    {
        cl.Usage( 1 );
    }
#endif /* 0 */

    FrameCPP::Initialize( );

    if ( cl.TestFrameMode( ) )
    {
        test_frame( cl.Version( ) );
    }
    else
    {
        if ( cl.ChannelType( ).size( ) == 0 )
        {
            cl.ChannelType( "int_2u" );
            cl.ChannelType( "int_2s" );
            cl.ChannelType( "int_4u" );
            cl.ChannelType( "int_4s" );
            cl.ChannelType( "int_8u" );
            cl.ChannelType( "int_8s" );
            cl.ChannelType( "real_4" );
            cl.ChannelType( "real_8" );
            cl.ChannelType( "complex_8" );
            cl.ChannelType( "complex_16" );
        }
        all_type_frame( cl.Description( ),
                        cl.ChannelType( ),
                        cl.StartTime( ),
                        cl.Duration( ),
                        cl.Iterations( ) );
    }
#endif

    depart( 0 );
}
catch ( const std::exception& e )
{
    std::cerr << "FATAL: Caught exception: " << e.what( ) << std::endl;
    depart( 1 );
}
catch ( ... )
{
    std::cerr << "FATAL: Caught unknown exception: " << std::endl;
    depart( 1 );
}

//=======================================================================
// Local functions
//=======================================================================

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------
static void
all_type_frame( const string&           Type,
                const channel_set_type& ChannelSet,
                const GPSTime&          Start,
                const INT_4U            DeltaT,
                const INT_4U            Iterations )
{
    //-------------------------------------------------------------------
    // Create filename
    //-------------------------------------------------------------------
    std::ostringstream filename;

    filename << "Z-" << Type << "-" << Start.GetSeconds( ) << "-" << DeltaT
             << ".gwf";

    //-------------------------------------------------------------------
    // Open the buffer and the stream
    //-------------------------------------------------------------------
    FrameBuffer< filebuf >* obuf( new FrameBuffer< filebuf >( std::ios::out ) );
    obuf->open( filename.str( ).c_str( ), std::ios::out | std::ios::binary );

    OFrameStream ofs( obuf );

    ofs.SetCheckSumFile( CheckSum::CRC );

    //-------------------------------------------------------------------
    // Generation of the frame
    //-------------------------------------------------------------------

    frame_h_type frame( new FrameH(
        Type, -1, 0, Start, Start.GetLeapSeconds( ), REAL_8( DeltaT ) ) );

    {
        FrameH::rawData_type rawData(
            new FrameH::rawData_type::element_type( "RawData" ) );
        frame->SetRawData( rawData );
    }

    //-------------------------------------------------------------------
    // Loop over the number of iterations for creation of the data sets
    //-------------------------------------------------------------------

    for ( INT_4U i = 1; i <= Iterations; ++i )
    {
        INT_4U channel = 0;
        all_type_iteration( DeltaT, ChannelSet, frame, &channel, i );
    }

    //-------------------------------------------------------------------
    // Writing of the frame
    //-------------------------------------------------------------------

    ofs.WriteFrame( frame );

    //-------------------------------------------------------------------
    // Close the frame and file stream
    //-------------------------------------------------------------------
    ofs.Close( );
    obuf->close( );
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

static void
all_type_iteration( const INT_4U            DeltaT,
                    const channel_set_type& ListOfChannels,
                    frame_h_type            Frame,
                    INT_4U*                 Channel,
                    INT_4U                  Iteration )
{
    fr_raw_data_type rd( Frame->GetRawData( ) );

    //-------------------------------------------------------------------
    // Make sure that we do not dereference a null pointer
    //-------------------------------------------------------------------
    if ( !rd )
    {
        return;
    }
    //-------------------------------------------------------------------
    // Safe to use
    //-------------------------------------------------------------------
    FrRawData::firstAdc_type& adclist( rd->RefFirstAdc( ) );
    //-------------------------------------------------------------------
    // Add the ramped channels
    //-------------------------------------------------------------------
    channel_set_type::const_iterator last_channel( ListOfChannels.end( ) );

    fr_adc_data_type rc;

    if ( ListOfChannels.find( "int_2u" ) != last_channel )
    {
        rc = ramped_channel< INT_2U >(
            DeltaT, Channel, Iteration, 0, 512, 1, 512 );
        adclist.append( rc );
    }
    if ( ListOfChannels.find( "int_2s" ) != last_channel )
    {
        rc = ramped_channel< INT_2S >(
            DeltaT, Channel, Iteration, -512, 512, 1, 512 );

        adclist.append( rc );
    }
    if ( ListOfChannels.find( "int_4u" ) != last_channel )
    {
        rc = ramped_channel< INT_4U >(
            DeltaT, Channel, Iteration, 0, 1024, 1, 1024 );
        adclist.append( rc );
    }
    if ( ListOfChannels.find( "int_4s" ) != last_channel )
    {
        rc = ramped_channel< INT_4S >(
            DeltaT, Channel, Iteration, -1024, 1024, 1, 1024 );
        adclist.append( rc );
    }
    if ( ListOfChannels.find( "int_8u" ) != last_channel )
    {
        rc = ramped_channel< INT_8U >(
            DeltaT, Channel, Iteration, 0, 2048, 1, 1024 );
        adclist.append( rc );
    }
    if ( ListOfChannels.find( "int_8s" ) != last_channel )
    {
        rc = ramped_channel< INT_8S >(
            DeltaT, Channel, Iteration, -2048, 2048, 1, 1024 );
        adclist.append( rc );
    }

    if ( ListOfChannels.find( "real_4" ) != last_channel )
    {
        rc = ramped_channel< REAL_4 >(
            DeltaT, Channel, Iteration, -1024, 1024, 0.5, 1024 );
        adclist.append( rc );
    }
    if ( ListOfChannels.find( "real_8" ) != last_channel )
    {
        rc = ramped_channel< REAL_8 >(
            DeltaT, Channel, Iteration, -1024, 1024, 0.5, 1024 );
        adclist.append( rc );
    }

    if ( ListOfChannels.find( "complex_8" ) != last_channel )
    {
        rc = ramped_channel< COMPLEX_8 >( DeltaT,
                                          Channel,
                                          Iteration,
                                          COMPLEX_8( -1024, 0 ),
                                          COMPLEX_8( 1024, 0 ),
                                          COMPLEX_8( 1, 0 ),
                                          1024 );
        adclist.append( rc );
    }
    if ( ListOfChannels.find( "complex_16" ) != last_channel )
    {
        rc = ramped_channel< COMPLEX_16 >( DeltaT,
                                           Channel,
                                           Iteration,
                                           COMPLEX_16( -1024, 0 ),
                                           COMPLEX_16( 1024, 0 ),
                                           COMPLEX_16( 1, 0 ),
                                           1024 );
        adclist.append( rc );
    }
}

#define DATA_TYPE( A )                                                         \
    template <>                                                                \
    std::string data_type< A >( )                                              \
    {                                                                          \
        return #A;                                                             \
    }

#if 0
//--------------------------
// Currently not being used
//--------------------------
DATA_TYPE(CHAR_U)
DATA_TYPE(CHAR)
#endif /* 0 */
DATA_TYPE( INT_2U )
DATA_TYPE( INT_2S )
DATA_TYPE( INT_4U )
DATA_TYPE( INT_4S )
DATA_TYPE( INT_8U )
DATA_TYPE( INT_8S )
DATA_TYPE( REAL_4 )
DATA_TYPE( REAL_8 )
DATA_TYPE( COMPLEX_8 )
DATA_TYPE( COMPLEX_16 )

#undef DATA_TYPE

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T >
static fr_adc_data_type
ramped_channel( INT_4U  DeltaT,
                INT_4U* Channel,
                INT_4U  Iteration,
                T       Start,
                T       Stop,
                T       Inc,
                INT_4U  SampleRate )
{
    //-------------------------------------------------------------------
    // Create the FrAdcData structure to hold the Adc metadata
    //-------------------------------------------------------------------
    fr_adc_data_type adc( new FrAdcData( ramped_name< T >( Iteration ),
                                         0, /* Group */
                                         *Channel, /* Channel Number */
                                         32, /* NBits */
                                         SampleRate, /* sampleRate */
                                         0.0, /* bias */
                                         1.0, /* slope */
                                         "counts", /* units */
                                         0.0, /* fShift */
                                         0.0, /* timeOffset */
                                         0, /* dataValid */
                                         0.0 /* phase */ ) );
    //-------------------------------------------------------------------
    // Link the data to the metadata
    //-------------------------------------------------------------------
    {
        fr_vect_type vec( ramped_vector< T >(
            DeltaT, SampleRate, Iteration, Start, Stop, Inc ) );

        adc->RefData( ).append( vec );
    }
    //-------------------------------------------------------------------
    // Incriment the channel number
    //-------------------------------------------------------------------
    ( *Channel )++;
    return adc;
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T >
static boost::shared_array< T >
ramp_data_create( INT_4U Samples, T Start, T Stop, T Inc )
{
    boost::shared_array< T > data( new T[ Samples ] );
    data[ 0 ] = Start;
    for ( INT_4U x = 1; x < Samples; x++ )
    {
        if ( data[ x - 1 ] == Stop )
        {
            data[ x ] = Start;
        }
        else
        {
            data[ x ] = data[ x - 1 ] + Inc;
        }
    }
    return data;
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T >
static string
ramped_name( INT_4U Iteration )
{
    ostringstream name;

    name << "Z0:RAMPED_" << data_type< T >( ) << "_" << Iteration;
    return name.str( );
}

//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

template < class T >
static fr_vect_type
ramped_vector(
    INT_4U DeltaT, INT_4U SampleRate, INT_4U Iteration, T Start, T Stop, T Inc )
{
    const INT_4U samples( DeltaT * SampleRate );

    Dimension                dim( samples );
    boost::shared_array< T > data(
        ramp_data_create< T >( samples, Start, Stop, Inc ) );

    fr_vect_type vect( new FrVect(
        ramped_name< T >( Iteration ), 1, &dim, data.get( ), "" /* unitY */ ) );
    return vect;
}
