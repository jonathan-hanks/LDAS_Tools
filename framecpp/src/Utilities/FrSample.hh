//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAME_CPP__TEST__FR_SAMPLE_HH
#define FRAME_CPP__TEST__FR_SAMPLE_HH

#include <list>
#include <sstream>
#include <stdexcept>

#include <boost/test/included/unit_test.hpp>
#include <boost/shared_ptr.hpp>

#include "framecpp/Common/FrameSpec.hh"

using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::STRING;

typedef boost::shared_ptr< FrameSpec::Object > stat_data_type;
typedef std::list< stat_data_type >            stat_data_container_type;
typedef boost::shared_ptr< FrameSpec::Object > make_frame_ret_type;

static const REAL_4        R4 = 1.0;
static const REAL_8        R8 = 2.0;
static const std::string   S = "TesT";
static const INT_2U        I2U = 1;
static const INT_4U        I4U = 2;
static const INT_8U        I8U = 3;
static const INT_2S        I2S = -1;
static const INT_4S        I4S = -2;
static const INT_8S        I8S = -3;
static const char          C = 'a';
static const unsigned char UC = 'b';
static const COMPLEX_8     C8( 3.0, 4.0 );
static const COMPLEX_16    C16( 5.0, 6.0 );

template < typename T >
T Default( );

template <>
inline REAL_4
Default( )
{
    return R4;
}
template <>
inline REAL_8
Default( )
{
    return R8;
}
template <>
inline INT_2U
Default( )
{
    return I2U;
}
template <>
inline INT_2S
Default( )
{
    return I2S;
}
template <>
inline INT_4U
Default( )
{
    return I4U;
}
template <>
inline INT_4S
Default( )
{
    return I4S;
}
template <>
inline INT_8U
Default( )
{
    return I8U;
}
template <>
inline INT_8S
Default( )
{
    return I8S;
}
template <>
inline CHAR
Default( )
{
    return C;
}
template <>
inline CHAR_U
Default( )
{
    return UC;
}
template <>
inline COMPLEX_8
Default( )
{
    return C8;
}
template <>
inline COMPLEX_16
Default( )
{
    return C16;
}
template <>
inline std::string
Default( )
{
    return S;
}
template <>
inline STRING< INT_2U >
Default( )
{
    return S;
}

template < int V >
make_frame_ret_type makeFrame( stat_data_container_type& StatData );

template < int V >
void verify_downconvert( FrameSpec::Object* FrameObj,
                         const std::string& Leader );
template < int V >
void verify_upconvert( FrameSpec::Object* FrameObj, const std::string& Leader );

namespace
{
    template < class C, class T >
    inline void
    append( C& Container, const T& Data, INT_2S Cnt )
    {
        if ( Cnt < 0 )
        {
            Cnt = Cnt * -1;
        }
        for ( ; Cnt > 0; --Cnt )
        {
            Container.push_back( Data );
        }
    }
} // namespace

#include "FrSample6.tcc"
#include "FrSample7.tcc"
#include "FrSample8.tcc"
#include "FrSample9.tcc"

namespace
{
#if USING_MAKE_FRAME_VERSION
    inline make_frame_ret_type
    makeFrameVersion( INT_2U Version, stat_data_container_type& Stat )
    {
        boost::shared_ptr< FrameSpec::Object > retval;

        switch ( Version )
        {
        case 6:
            retval = makeFrame< 6 >( Stat );
            break;
        case 7:
            retval = makeFrame< 7 >( Stat );
            break;
        case 8:
            retval = makeFrame< 8 >( Stat );
            break;
        case 9:
          retval = makeFrame< 9 >( Stat );
          break;
        default:
        {
            std::ostringstream msg;

            msg << "makeFrameVersion cannot create a frame for version: "
                << Version;
            throw std::range_error( msg.str( ) );
            break;
        } // default
        } // switch
        return retval;
    }
#endif /* USING_MAKE_FRAME_VERSION */

#if USING_VERIFY_DOWN_CONVERT
    inline void
    VerifyDownconvert( INT_2U             Version,
                       FrameSpec::Object* Obj,
                       const std::string& Leader )
    {
        //-------------------------------------------------------------------
        // Verification code is always in the version beyond Version
        //-------------------------------------------------------------------
        switch ( Version )
        {
        case 6:
            verify_downconvert< 7 >( Obj, Leader );
            break;
        default:
        {
            std::ostringstream msg;

            msg << "verify_upconvert cannot verify a frame for version: "
                << Version;
            throw std::range_error( msg.str( ) );
            break;
        } // default
        } // switch
    }
#endif /* USING_VERIFY_DOWN_CONVERT */

#if USING_VERIFY_UP_CONVERT
    inline void
    VerifyUpconvert( INT_2U             Version,
                     FrameSpec::Object* Obj,
                     const std::string& Leader )
    {
        //-------------------------------------------------------------------
        // Verification code is always in the version beyond Version
        //-------------------------------------------------------------------
        switch ( Version )
        {
        case 6:
            verify_upconvert< 7 >( Obj, Leader );
            break;
        default:
        {
            std::ostringstream msg;

            msg << "verify_upconvert cannot verify a frame for version: "
                << Version;
            throw std::range_error( msg.str( ) );
            break;
        } // default
        } // switch
    }
#endif /* USING_VERIFY_UP_CONVERT */

#if 0
  inline void
  VerifyDownConvert( INT_2U Version, FrameSpec::Object* Obj )
  {
    //-------------------------------------------------------------------
    // Verification code is always in the version beyond Version
    //-------------------------------------------------------------------
    switch( Version )
    {
    case 7:
      verify_downconvert< 7 >( Obj, Leader );
      break;
    default:
      {
	std::ostringstream	msg;
      
	msg << "verify_upconvert cannot verify a frame for version: "
	    << Version
	  ;
	throw std::range_error( msg.str( ) );
	break;
      } // default
    } // switch
  }
#endif /* 0 */
} // namespace

#endif /* FRAME_CPP__TEST__FR_SAMPLE_HH */
