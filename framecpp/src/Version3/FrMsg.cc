//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <memory>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"

#include "framecpp/Version3/FrMsg.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"

#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_3;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_MSG;

//=======================================================================
// FrMsg::fr_event_data_type
//=======================================================================

bool
FrMsg::fr_msg_data_type::operator==( const fr_msg_data_type& RHS ) const
{
#define CMP__( X ) ( X == RHS.X )

    return ( ( &RHS == this ) ||
             ( CMP__( alarm ) && CMP__( message ) && CMP__( severity ) ) );
#undef CMP__
}

//=======================================================================
// FrMsg
//=======================================================================
FrMsg::FrMsg( ) : FrameSpec::Object( s_object_id, StructDescription( ) )
{
  m_data.alarm = "";
  m_data.message = "";
  m_data.severity = DEFAULT_SEVERITY;
}

FrMsg::FrMsg( const FrMsg& Source )
    : FrameSpec::Object( s_object_id, StructDescription( ) ), Common::TOCInfo(
                                                                  Source )
{
    m_data.alarm = Source.m_data.alarm;
    m_data.message = Source.m_data.message;
    m_data.severity = Source.m_data.severity;
}

FrMsg::FrMsg( const std::string& Alarm,
              const std::string& Message,
              INT_4U             Severity )
    : FrameSpec::Object( s_object_id, StructDescription( ) )
{
    m_data.alarm = Alarm;
    m_data.message = Message;
    m_data.severity = Severity;
}

FrMsg::FrMsg( istream_type& Stream )
    : FrameSpec::Object( s_object_id, StructDescription( ) )
{
    Stream >> m_data.alarm >> m_data.message >> m_data.severity;
    Stream.Next( this );
}

const std::string&
FrMsg::GetAlarm( ) const
{
    return m_data.alarm;
}

FrameCPP::cmn_streamsize_type
FrMsg::Bytes( const Common::StreamBase& Stream ) const
{
    return m_data.alarm.Bytes( ) + m_data.message.Bytes( ) +
        sizeof( m_data.severity ) + Stream.PtrStructBytes( ) // next
        ;
}

FrMsg*
FrMsg::Create( istream_type& Stream ) const
{
    return new FrMsg( Stream );
}

FrMsg::promote_ret_type
FrMsg::Promote( INT_2U Source, promote_arg_type Obj, istream_type* Stream )
{
    return Common::PromoteObject( DATA_FORMAT_VERSION, Source, Obj );
}

const char*
FrMsg::ObjectStructName( ) const
{
    return StructName( );
}

const Description*
FrMsg::StructDescription( )
{
    static Description ret;

    if ( ret.size( ) == 0 )
    {

        ret( FrSH( FrMsg::StructName( ), s_object_id, "Msg Data Structure" ) );

        ret( FrSE( "alarm", "STRING", "" ) );
        ret( FrSE( "message", "STRING", "" ) );
        ret( FrSE( "severity", "INT_4U", "" ) );

        ret( FrSE( "next", PTR_STRUCT::Desc( FrMsg::StructName( ) ), "" ) );
    }

    return &ret;
}

void
FrMsg::
#if WORKING_VIRTUAL_TOCQUERY
    TOCQuery( int InfoClass, ... ) const
#else /*  WORKING_VIRTUAL_TOCQUERY */
    vTOCQuery( int InfoClass, va_list vl ) const
#endif /*  WORKING_VIRTUAL_TOCQUERY */
{
    using Common::TOCInfo;

#if WORKING_VIRTUAL_TOCQUERY
    va_list vl;
    va_start( vl, InfoClass );
#endif /* WORKING_VIRTUAL_TOCQUERY */
#if WORKING_VIRTUAL_TOCQUERY
    va_end( vl );
#endif /* WORKING_VIRTUAL_TOCQUERY */
}

void
FrMsg::Write( ostream_type& Stream ) const
{
    Stream << m_data.alarm << m_data.message << m_data.severity;
    WriteNext( Stream );
}

FrMsg&
FrMsg::Merge( const FrMsg& RHS )
{
    /// \todo
    ///   Need to implement Merge routine
    std::string msg( "Merge currently not implemented for " );
    msg += StructName( );

    throw std::domain_error( msg );
    return *this;
}

bool
FrMsg::operator==( const Common::FrameSpec::Object& Obj ) const
{
    return Common::Compare( *this, Obj );
}

FrMsg::demote_ret_type
FrMsg::demote( INT_2U Target, demote_arg_type Obj, istream_type* Stream ) const
{
    if ( Target >= DATA_FORMAT_VERSION )
    {
        return Obj;
    }
    throw Unimplemented(
        "Object* FrMsg::demote( INT_2U Target, Object* Obj ) const",
        DATA_FORMAT_VERSION,
        __FILE__,
        __LINE__ );
}

FrMsg::promote_ret_type
FrMsg::promote( INT_2U           Source,
                promote_arg_type Obj,
                istream_type*    Stream ) const
{
    promote_ret_type retval;

    if ( Source >= DATA_FORMAT_VERSION )
    {
        retval = Obj;
        return retval;
    }
    throw Unimplemented(
        "Object* FrMsg::promote( INT_2U Source, Object* Obj ) const",
        DATA_FORMAT_VERSION,
        __FILE__,
        __LINE__ );
}
