//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameStream.hh"

#include "framecpp/Version3/FrameSpec.hh"
#include "framecpp/Version3/FrHistory.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"
#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_3;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================
static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_HISTORY;

namespace FrameCPP
{
    namespace Version_3
    {
      constexpr INT_4U DEFAULT_TIME = 0;

        //===================================================================
        // FrEvent::fr_event_data_type
        //===================================================================

        bool
        FrHistory::fr_history_data_type::
        operator==( const fr_history_data_type& RHS ) const
        {
#define CMP__( X ) ( X == RHS.X )

            return ( ( &RHS == this ) ||
                     ( CMP__( name ) && CMP__( time ) && CMP__( comment ) ) );
#undef CMP__
        }

        //=======================================================================
        // FrEvent
        //=======================================================================
        FrHistory::FrHistory( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
          m_data.time = DEFAULT_TIME;
        }

        FrHistory::FrHistory( const FrHistory& Source )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.name = Source.m_data.name;
            m_data.time = Source.m_data.time;
            m_data.comment = Source.m_data.comment;
        }

        FrHistory::FrHistory( const std::string& Name,
                              INT_4U             Time,
                              const std::string& Comment )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.name = Name;
            m_data.time = Time;
            m_data.comment = Comment;
        }

        FrHistory::FrHistory( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            Stream >> m_data.name >> m_data.time >> m_data.comment;
            Stream.Next( this );
        }

        std::string
        FrHistory::ErrorInfo( ) const
        {
            std::ostringstream result;

            result << "FrameCPP::Version3::FrHistory: {Name: " << m_data.name
                   << " Time: " << m_data.time << "} ";
            return result.str( );
        }

        const std::string&
        FrHistory::GetName( ) const
        {
            return m_data.name;
        }

        FrameCPP::cmn_streamsize_type
        FrHistory::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( m_data.time ) +
                m_data.comment.Bytes( ) + Stream.PtrStructBytes( ) // next
                ;
        }

        FrHistory*
        FrHistory::Create( istream_type& Stream ) const
        {
            return new FrHistory( Stream );
        }

        FrHistory&
        FrHistory::Merge( const FrHistory& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        const char*
        FrHistory::ObjectStructName( ) const
        {
            return StructName( );
        }

        FrHistory::promote_ret_type
        FrHistory::Promote( INT_2U           Source,
                            promote_arg_type Obj,
                            istream_type*    Stream )
        {
            return Common::PromoteObject( DATA_FORMAT_VERSION, Source, Obj );
        }

        const Description*
        FrHistory::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrHistory::StructName( ),
                           s_object_id,
                           "History Structure" ) );
                ret( FrSE( "name", "STRING", "Name of history record" ) );
                ret( FrSE( "time", "INT_4U", "Time of post-processing." ) );
                ret( FrSE( "comment",
                           "STRING",
                           "Program name and relevant comments needed to"
                           " define post-processing" ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrHistory::StructName( ) ),
                           "Identifier for next history structure in the "
                           "linked list" ) );
            }

            return &ret;
        }

        void
        FrHistory::Write( ostream_type& Stream ) const
        {
            try
            {
                Stream << m_data.name << m_data.time << m_data.comment;
                WriteNext( Stream );
            }
            catch ( const std::exception& Exception )
            {
                std::cerr << "DEBUG: Error Writing FrHistory: " << m_data.name
                          << " exception: " << Exception.what( ) << std::endl;
                throw;
            }
        }

        bool
        FrHistory::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrHistory::demote_ret_type
        FrHistory::demote( INT_2U          Target,
                           demote_arg_type Obj,
                           istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            throw Unimplemented(
                "Object* FrHistory::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrHistory::promote_ret_type
        FrHistory::promote( INT_2U           Source,
                            promote_arg_type Obj,
                            istream_type*    Stream ) const
        {
            promote_ret_type retval;

            if ( Source >= DATA_FORMAT_VERSION )
            {
                retval = Obj;
                return retval;
            }
            throw Unimplemented(
                "Object* FrHistory::promote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }
    } // namespace Version_3
} // namespace FrameCPP
