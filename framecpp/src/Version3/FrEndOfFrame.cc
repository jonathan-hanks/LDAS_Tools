//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/FrameStream.hh"
#include "framecpp/Common/Description.hh"

#include "framecpp/Version3/FrEndOfFrame.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"

#include "framecpp/Common/Promotion.hh"

#include "Common/ComparePrivate.hh"

using namespace FrameCPP::Version_3;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;
using FrameCPP::Common::OFrameStream;

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_END_OF_FRAME;

FrEndOfFrame::FrEndOfFrame( )
    : FrameSpec::Object( s_object_id, StructDescription( ) )
{
}

FrEndOfFrame::FrEndOfFrame( run_type Run, frame_type Frame )
    : FrameSpec::Object( s_object_id, StructDescription( ) )
{
    m_data.run = Run;
    m_data.frame = Frame;
}

FrEndOfFrame::FrEndOfFrame( istream_type& Stream )
    : FrameSpec::Object( s_object_id, StructDescription( ) )
{
    Stream >> m_data.run >> m_data.frame;
}

FrEndOfFrame::~FrEndOfFrame( )
{
}

FrameCPP::cmn_streamsize_type
FrEndOfFrame::Bytes( const Common::StreamBase& Stream ) const
{
    return sizeof( m_data.run ) + sizeof( m_data.frame );
}

FrEndOfFrame*
FrEndOfFrame::Create( istream_type& Stream ) const
{
    return new FrEndOfFrame( Stream );
}

const char*
FrEndOfFrame::ObjectStructName( ) const
{
    return StructName( );
}

const Description*
FrEndOfFrame::StructDescription( )
{
    static Description ret;

    if ( ret.size( ) == 0 )
    {
        ret( FrSH(
            "FrEndOfFrame", s_object_id, "End of Frame Data Structure" ) );
        ret( FrSE( "run",
                   "INT_4U",
                   "Run number; same as in FrameHeader run number datum." ) );
        ret( FrSE( "frame",
                   "INT_4U",
                   "Frame number, monotonically increasing until end of run;"
                   " same as in Frame Header run number datum" ) );
    }

    return &ret;
}

void
FrEndOfFrame::Write( ostream_type& Stream ) const
{
    //---------------------------------------------------------------------
    // Write out to the stream
    //---------------------------------------------------------------------
    Stream << m_data.run << m_data.frame;
}

FrEndOfFrame::demote_ret_type
FrEndOfFrame::demote( INT_2U          Target,
                      demote_arg_type Obj,
                      istream_type*   Stream ) const
{
    throw Unimplemented( "Object* FrEndOfFrame::demote( Object* Obj ) const",
                         DATA_FORMAT_VERSION,
                         __FILE__,
                         __LINE__ );
}

FrEndOfFrame::promote_ret_type
FrEndOfFrame::promote( INT_2U           Target,
                       promote_arg_type Obj,
                       istream_type*    Stream ) const
{
    throw Unimplemented( "Object* FrEndOfFrame::promote( Object* Obj ) const",
                         DATA_FORMAT_VERSION,
                         __FILE__,
                         __LINE__ );
}

bool
FrEndOfFrame::operator==( const FrEndOfFrame& RHS ) const
{
    return ( ( this == &RHS ) ||
             ( ( m_data.run == RHS.m_data.run ) &&
               ( m_data.frame == RHS.m_data.frame ) ) );
}

bool
FrEndOfFrame::operator==( const Common::FrameSpec::Object& Obj ) const
{
    return Common::Compare( *this, Obj );
}
