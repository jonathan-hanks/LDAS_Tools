//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <memory>

#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version3/FrameSpec.hh"

#include "framecpp/Version3/FrSE.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::FrameSpec;

using FrameCPP::Common::Description;

static const Description* s_description = (Description*)NULL;

namespace FrameCPP
{
    namespace Version_3
    {

        FrSE::FrSE( ) : FrameSpec::Object( s_object_id, s_description )
        {
            m_data.name = "";
            m_data.classId = "";
            m_data.comment = "";
        }

        FrSE::FrSE( const std::string& Name,
                    const std::string& ClassId,
                    const std::string& Comment )
            : FrameSpec::Object( s_object_id, s_description )
        {
            m_data.name = Name;
            m_data.classId = ClassId;
            m_data.comment = Comment;
        }

        FrSE::FrSE( istream_type& Stream )
            : FrameSpec::Object( s_object_id, s_description )
        {
            Stream >> m_data.name >> m_data.classId >> m_data.comment;
        }

        FrSE::~FrSE( )
        {
        }

        const char*
        FrSE::ObjectStructName( ) const
        {
            return StructName( );
        }

        std::string
        FrSE::GetName( ) const
        {
            return m_data.name;
        }

        INT_8U
        FrSE::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + m_data.classId.Bytes( ) +
                m_data.comment.Bytes( );
        }

        FrSE*
        FrSE::Create( istream_type& Stream ) const
        {
            return new FrSE( Stream );
        }

        FrSE*
        FrSE::Clone( ) const
        {
            return new FrSE( *this );
        }

        void
        FrSE::Write( ostream_type& Stream ) const
        {
            Stream << m_data.name << m_data.classId << m_data.comment;
        }

        bool
        FrSE::operator==( const FrSE& RHS ) const
        {
            return ( ( m_data.name == RHS.m_data.name ) &&
                     ( m_data.classId == RHS.m_data.classId ) &&
                     ( m_data.comment == RHS.m_data.comment ) );
        }

        bool
        FrSE::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrSE::demote_ret_type
        FrSE::demote( INT_2U          Target,
                      demote_arg_type Obj,
                      istream_type*   Stream ) const
        {
            throw Unimplemented( "Object* FrSE::Demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrSE::promote_ret_type
        FrSE::promote( INT_2U           Target,
                       promote_arg_type Obj,
                       istream_type*    Stream ) const
        {
            throw Unimplemented( "Object* FrSE::Promote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }
    } // namespace Version_3
} // namespace FrameCPP
