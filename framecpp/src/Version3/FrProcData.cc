//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "ldastoolsal/mutexlock.hh"
#include "ldastoolsal/Singleton.hh"
#include "ldastoolsal/gpstime.hh"

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/FrameStream.hh"

#include "framecpp/Version3/FrProcData.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"

#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using std::ceil;
using std::floor;

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_PROC_DATA;

namespace FrameCPP
{
    namespace Version_3
    {
        //===================================================================
        // FrProcDataData::fr_proc_data_data_type
        //===================================================================

        bool
        FrProcDataData::fr_proc_data_data_type::
        operator==( const fr_proc_data_data_type& RHS ) const
        {
#define CMP__( X ) ( X == RHS.X )

            return ( ( &RHS == this ) ||
                     ( CMP__( name ) && CMP__( comment ) &&
                       CMP__( sampleRate ) && CMP__( timeOffset ) &&
                       CMP__( fShift ) ) );
#undef CMP__
        }

        FrProcDataData::fr_proc_data_data_type::fr_proc_data_data_type( )
        {
        }

        FrProcDataData::fr_proc_data_data_type::fr_proc_data_data_type(
            const fr_proc_data_data_type& Source )
            : name( Source.name ), comment( Source.comment ),
              sampleRate( Source.sampleRate ), timeOffset( Source.timeOffset ),
              fShift( Source.fShift )
        {
        }

        FrameCPP::cmn_streamsize_type
        FrProcDataData::fr_proc_data_data_type::Bytes(
            const Common::StreamBase& Stream ) const
        {
            return name.Bytes( ) + comment.Bytes( ) + sizeof( sampleRate ) +
                sizeof( INT_4U ) // timeOffsetS
                + sizeof( INT_4U ) // timeOffsetN
                + sizeof( fShift );
        }

        void
        FrProcDataData::fr_proc_data_data_type::Write(
            Common::OStream& Stream ) const
        {
            INT_4U timeOffsetS = INT_4U( timeOffset.GetSeconds( ) );
            INT_4U timeOffsetN = INT_4U( timeOffset.GetNanoseconds( ) );
            Stream << name << comment << sampleRate << timeOffsetS
                   << timeOffsetN << fShift;
        }

        const FrProcData::fr_proc_data_data_type&
        FrProcDataData::fr_proc_data_data_type::
        operator=( const fr_proc_data_data_type& Source )
        {
            name = Source.name;
            comment = Source.comment;
            sampleRate = Source.sampleRate;
            timeOffset = Source.timeOffset;
            fShift = Source.fShift;
            return *this;
        }

        //=======================================================================
        // FrProcDataData
        //=======================================================================

        FrProcDataData::FrProcDataData( )
        {
        }

        FrProcDataData::FrProcDataData( const std::string& Name,
                                        const std::string& Comment,
                                        const REAL_8       SampleRate,
                                        const GPSTime&     TimeOffset,
                                        const REAL_8       FShift )
        {
            m_data.name = Name;
            m_data.comment = Comment;
            m_data.sampleRate = SampleRate;
            m_data.timeOffset = TimeOffset;
            m_data.fShift = FShift;
        }

        FrProcDataData::FrProcDataData( Common::IStream& Stream )
        {
            INT_4U timeOffsetS;
            INT_4U timeOffsetN;

            Stream >> m_data.name >> m_data.comment >> m_data.sampleRate >>
                timeOffsetS >> timeOffsetN >> m_data.fShift;

            m_data.timeOffset = GPSTime( timeOffsetS, timeOffsetN );
        }

        //=======================================================================
        // FrProcData::fr_proc_data_ref_type
        //=======================================================================
        Common::IStream&
        FrProcData::fr_proc_data_ref_type::
        operator( )( Common::IStream& Stream )
        {
            Stream >> data >> aux;
            return Stream;
        }

        FrameCPP::cmn_streamsize_type
        FrProcData::fr_proc_data_ref_type::Bytes(
            const Common::StreamBase& Stream ) const
        {
            return Stream.PtrStructBytes( ) // data
                + Stream.PtrStructBytes( ) // aux
                ;
        }

        void
        FrProcData::fr_proc_data_ref_type::Write( ostream_type& Stream ) const
        {
            Stream << data << aux;
        }

        //=======================================================================
        // FrProcData
        //=======================================================================
        FrProcData::FrProcData( )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              m_synced_with_vector( false )
        {
        }

        FrProcData::FrProcData( const FrProcData& ProcData )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrProcDataData( ProcData ), m_refs( ProcData.m_refs ),
              m_synced_with_vector( false )
        {
        }

        FrProcData::FrProcData( const std::string& Name,
                                const std::string& Comment,
                                const REAL_8       SampleRate,
                                const GPSTime&     TimeOffset,
                                const REAL_8       FShift )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrProcDataData( Name, Comment, SampleRate, TimeOffset, FShift ),
              m_synced_with_vector( false )
        {
        }

        FrProcData::FrProcData( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrProcDataData( Stream ), m_synced_with_vector( false )
        {
            m_refs( Stream );
            Stream.Next( this );
        }

        FrProcData::~FrProcData( )
        {
        }

        FrameCPP::cmn_streamsize_type
        FrProcData::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.Bytes( Stream ) + m_refs.Bytes( Stream ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        FrProcData*
        FrProcData::Create( istream_type& Stream ) const
        {
            return new FrProcData( Stream );
        }

        const char*
        FrProcData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const std::string&
        FrProcData::GetNameSlow( ) const
        {
            return GetName( );
        }

        const Description*
        FrProcData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrProcData::StructName( ),
                           s_object_id,
                           "Post-processed Data Structure" ) );

                ret( FrSE( "name", "STRING", "" ) );
                ret( FrSE( "comment", "STRING", "" ) );
                ret( FrSE( "sampleRate", "REAL_8", "" ) );
                ret( FrSE( "timeOffsetS", "INT_4U", "" ) );
                ret( FrSE( "timeOffsetN", "INT_4U", "" ) );
                ret( FrSE( "fShift", "REAL_8", "" ) );

                ret(
                    FrSE( "data", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );
                ret( FrSE( "aux", PTR_STRUCT::Desc( FrVect::StructName( ) ) ) );

                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrProcData::StructName( ) ) ) );
            }

            return &ret;
        }

        void
        FrProcData::Write( ostream_type& Stream ) const
        {
            m_data.Write( Stream );
            m_refs.Write( Stream );

            WriteNext( Stream );
        }

        FrProcData&
        FrProcData::Merge( const FrProcData& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

#if WORKING
        std::unique_ptr< FrProcData >
        FrProcData::SubFrProcData( REAL_8 Offset, REAL_8 Dt ) const
        {
#if WORKING || 1
            std::unique_ptr< FrProcData > retval;

            //---------------------------------------------------------------------
            // Process the types that are understood
            //---------------------------------------------------------------------
            switch ( GetType( ) )
            {
            case TIME_SERIES:
                retval = sub_time_series( Offset, Dt );
                break;
            default:
            {
                //-----------------------------------------------------------------
                // This type is not supported so throw an exception
                //-----------------------------------------------------------------
                std::ostringstream msg;
                msg << "Currently cannot take a subsection of type: "
                    << IDTypeToString( GetType( ) );
                const std::string& subtype(
                    IDSubTypeToString( GetType( ), GetSubType( ) ) );
                if ( subtype.length( ) > 0 )
                {
                    msg << " subType: " << subtype;
                }
                throw std::runtime_error( msg.str( ) );
            }
            break;
            }

            return retval;
#else /* WORKING */
            throw Unimplemented(
                "FrProcData::SubFrAdcData( READL_8 Offset, REAL_8 Dt ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );

#endif /* WORKING */
        }
#endif /* 0 */

        bool
        FrProcData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        void
        FrProcData::copy_core( const FrProcData& Source )
        {
            static_cast< FrProcDataData& >( *this ) = Source;
            m_refs.copy_core( Source.m_refs );
        }

        FrProcData::demote_ret_type
        FrProcData::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            throw Unimplemented(
                "Object* FrProcData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrProcData::promote_ret_type
        FrProcData::promote( INT_2U           Target,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

#if WORKING
        // Routine to take a subsection when the data type is TimeSeries data
        std::unique_ptr< FrProcData >
        FrProcData::sub_time_series( REAL_8 Offset, REAL_8 Dt ) const
        {
            //---------------------------------------------------------------------
            // Sanity checks
            //---------------------------------------------------------------------
            // Verify the existance of only a single FrVect component
            //---------------------------------------------------------------------
            if ( m_refs.data.size( ) != 1 )
            {
                std::ostringstream msg;

                msg << "METHOD: frameAPI::FrProcData::SubFrProcData: "
                    << "data vectors of length " << m_refs.data.size( )
                    << " currently are not supported";
                throw std::logic_error( msg.str( ) );
            }
            INT_4U num_elements = m_refs.data[ 0 ]->GetDim( 0 ).GetNx( );
            //---------------------------------------------------------------------
            /// \todo
            ///   Ensure the dimension of the FrVect is 1
            //---------------------------------------------------------------------
            //---------------------------------------------------------------------
            /// \todo
            ///   Ensure the requested range is valid
            //---------------------------------------------------------------------
            REAL_8 sample_rate = 1.0 / m_refs.data[ 0 ]->GetDim( 0 ).GetDx( );
            if ( Offset >
                 ( m_data.timeOffset + ( num_elements / sample_rate ) ) )
            {
                std::ostringstream msg;
                msg << "METHOD: frameAPI::FrProcData::SubFrProcData: "
                    << "The value for the parameter Offset (" << Offset
                    << ") is out of range ([0-"
                    << ( m_data.timeOffset + ( num_elements / sample_rate ) )
                    << "))";
                throw std::range_error( msg.str( ) );
            }
            //---------------------------------------------------------------------
            // Start moving of data
            //---------------------------------------------------------------------
            std::unique_ptr< FrProcData > retval( new FrProcData );
            // Copy in the header information
            retval->copy_core( *this );

            /// \todo
            ///   Get the slice of FrVect
            REAL_8 startSampleReal = ( Offset * sample_rate ) -
                ( retval->m_data.timeOffset * sample_rate );

            INT_4U endSample =
                INT_4U( ceil( startSampleReal + ( Dt * sample_rate ) ) );
            INT_4U startSample = ( startSampleReal > REAL_8( 0 ) )
                ? INT_4U( floor( startSampleReal ) )
                : INT_4U( 0 );

            retval->m_refs.data.append(
                this->m_refs.data[ 0 ]
                    ->SubFrVect( startSample, endSample )
                    .release( ),
                false, /* allocate */
                true /* own */ );
            //---------------------------------------------------------------------
            // Shift the offset
            //---------------------------------------------------------------------
            if ( startSampleReal > 0 )
            {
                // There is no gap
                retval->m_data.timeOffset = REAL_8( 0 );
            }
            else
            {
                retval->m_data.timeOffset += Offset;
            }
            //---------------------------------------------------------------------
            /// \todo
            ///   Recalculate tRange
            //---------------------------------------------------------------------
            retval->SetTRange( retval->RefData( )[ 0 ]->GetDim( 0 ).GetNx( ) *
                               retval->RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) );
            //---------------------------------------------------------------------
            // Return the new FrProcData
            //---------------------------------------------------------------------
            return retval;
        }
#endif /* WORKING */
    } // namespace Version_3
} // namespace FrameCPP
