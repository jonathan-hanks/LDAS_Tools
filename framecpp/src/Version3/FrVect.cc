//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream>
#include <algorithm>
#include <stdexcept>

#include <boost/shared_array.hpp>

#include "framecpp/Common/Compression.hh"
#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/Version3/FrVect.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"

#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

using namespace FrameCPP::Compression;

#define LM_DEBUG 0

#if LM_DEBUG
#define AT( ) std::cerr << "INFO: " << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT( )
#endif

#if HAVE_TEMPLATE_MOVE
#define MOVE_RET( a ) ( std::move( a ) )
#else /* HAVE_TEMPLATE_MOVE */
#define MOVE_RET( a ) ( a )
#endif /* HAVE_TEMPLATE_MOVE */

//-----------------------------------------------------------------------
// Local functions and variables
//-----------------------------------------------------------------------

namespace
{
    using namespace FrameCPP::Version_3;
    //=====================================================================
    // Local enumerations
    //=====================================================================
    enum
    {
        MODE_RAW = FrVect::RAW,
        MODE_GZIP = FrVect::GZIP,
        MODE_DIFF = FrVect::DIFF,
        MODE_DIFF_GZIP = FrVect::DIFF_GZIP
    };

    enum
    {
#if WORDS_BIGENDIAN
        MODE_BIGENDIAN = FrVect::RAW,
        MODE_LITTLEENDIAN = FrVect::BYTE_SWAP,
#else
        MODE_BIGENDIAN = FrVect::BYTE_SWAP,
        MODE_LITTLEENDIAN = FrVect::RAW,
#endif
        MODE_HOSTENDIAN = FrVect::RAW
    };

    //=====================================================================
    // Local classes
    //=====================================================================
    class DimBuffer
    {
    public:
        typedef Dimension::nx_type nx_type;
        typedef Dimension::dx_type dx_type;

        DimBuffer( )
        {
        }

        DimBuffer( const std::vector< Dimension >& Dims );

        void copy( std::vector< Dimension >& Dims );

        static INT_8U GetMinimumBytes( );

    private:
        friend class FrameCPP::Common::IStream;
        friend class FrameCPP::Common::OStream;

        std::vector< nx_type > m_nx;
        std::vector< dx_type > m_dx;
        std::vector< STRING >  m_unitX;
    };

    inline INT_8U
    DimBuffer::GetMinimumBytes( )
    {
        return sizeof( nx_type ) + sizeof( dx_type ) + STRING::Bytes( "" );
    }

    //=====================================================================
    // Local function prototypes
    //=====================================================================
    FrameCPP::Compression::data_type_mapping&     data_type_map_init( );
    FrameCPP::Compression::compress_type_mapping& compress_type_map_init( );
    FrameCPP::Compression::compress_type_reverse_mapping&
    compress_type_reverse_map_init( );

    //=====================================================================
    // Local variables
    //=====================================================================
    static const FrameCPP::Compression::data_type_mapping& data_type_map =
        data_type_map_init( );

    static const FrameCPP::Compression::compress_type_mapping&
        compress_type_map = compress_type_map_init( );

    static const FrameCPP::Compression::compress_type_reverse_mapping&
        compress_type_reverse_map = compress_type_reverse_map_init( );

    //=====================================================================
    // Local functions
    //=====================================================================
    INT_8U
    calc_nData( const std::vector< Dimension >& Dimensions )
    {
        INT_8U ret( 0 );
        if ( Dimensions.size( ) > 0 )
        {
            ret = 1;
            for ( std::vector< Dimension >::const_iterator i(
                      Dimensions.begin( ) );
                  i != Dimensions.end( );
                  i++ )
            {
                ret *= ( *i ).GetNx( );
            }
        }
        return ret;
    }

    FrameCPP::Compression::compress_type_mapping&
    compress_type_map_init( )
    {
        static FrameCPP::Compression::compress_type_mapping m;

#define INIT( x, y ) m[ FrVect::x ] = FrameCPP::Compression::y

        INIT( RAW, MODE_RAW );
        INIT( GZIP, MODE_GZIP );
        INIT( DIFF_GZIP, MODE_DIFF_GZIP );

#undef INIT

        return m;
    }

    FrameCPP::Compression::compress_type_reverse_mapping&
    compress_type_reverse_map_init( )
    {
        static FrameCPP::Compression::compress_type_reverse_mapping m;

#define INIT( x, y ) m[ FrameCPP::Compression::x ] = FrVect::y

        INIT( MODE_RAW, RAW );
        INIT( MODE_GZIP, GZIP );
        INIT( MODE_DIFF_GZIP, DIFF_GZIP );

#undef INIT

        return m;
    }

    FrameCPP::Compression::data_type_mapping&
    data_type_map_init( )
    {
        static FrameCPP::Compression::data_type_mapping m;

#define INIT( x ) m[ FrameCPP::Version_3::FrVect::x ] = FrameCPP::Compression::x

        INIT( FR_VECT_C );
        INIT( FR_VECT_2S );
        INIT( FR_VECT_8R );
        INIT( FR_VECT_4R );
        INIT( FR_VECT_4S );
        INIT( FR_VECT_8S );
        INIT( FR_VECT_8C );
        INIT( FR_VECT_16C );
        INIT( FR_VECT_STRING );
        INIT( FR_VECT_2U );
        INIT( FR_VECT_4U );
        INIT( FR_VECT_8U );
        INIT( FR_VECT_1U );

#undef INIT
        return m;
    }

} // namespace

namespace
{
    INT_8U
    Bytes( const std::vector< Dimension >& Dims )
    {
        INT_8U ret( sizeof( INT_4U ) ); // nDim
        for ( std::vector< Dimension >::const_iterator d( Dims.begin( ) );
              d != Dims.end( );
              d++ )
        {
            ret += d->Bytes( );
        }
        return ret;
    } // Bytes

    DimBuffer::DimBuffer( const std::vector< Dimension >& Dims )
    {
        INT_4U nDim( Dims.size( ) );

        m_nx.resize( nDim );
        m_dx.resize( nDim );
        m_unitX.resize( nDim );

        for ( INT_4U x = 0; x < nDim; x++ )
        {
            m_nx[ x ] = Dims[ x ].GetNx( );
            m_dx[ x ] = Dims[ x ].GetDx( );
            m_unitX[ x ] = Dims[ x ].GetUnitX( );
        }
    }

    void
    DimBuffer::copy( std::vector< Dimension >& Dims )
    {
        INT_4U nDim( m_nx.size( ) );

        Dims.resize( nDim );
        for ( INT_4U x = 0; x < nDim; x++ )
        {
            Dims[ x ] = Dimension( m_nx[ x ], m_dx[ x ], m_unitX[ x ] );
        }
    }

} // namespace

namespace FrameCPP
{
    namespace Common
    {
        /// \cond ignore
        template <>
        OStream&
        OStream::operator<<( const DimBuffer& Dims )
        {
            INT_4U nDim( Dims.m_nx.size( ) );

            *this << nDim << Dims.m_nx << Dims.m_dx << Dims.m_unitX;
            return *this;
        }
        /// \endcond ignore

        /// \cond ignore
        template <>
        IStream&
        IStream::operator>>( DimBuffer& Dims )
        {
            INT_4U nDim( 0 );
            *this >> nDim;

            Dims.m_nx.resize( nDim );
            Dims.m_dx.resize( nDim );
            Dims.m_unitX.resize( nDim );

            *this >> Dims.m_nx >> Dims.m_dx >> Dims.m_unitX;

            return *this;
        }
        /// \endcond ignore
    } // namespace Common
} // namespace FrameCPP

#define TRACE_MEMORY 0
#if TRACE_MEMORY
#define MEM_ALLOCATE( )                                                        \
    std::cerr << "MEMORY: Allocate: " << FrVect::getStaticName( ) << " "       \
              << (void*)this << " Line: " << __LINE__ << std::endl;
#define MEM_DELETE( )                                                          \
    std::cerr << "MEMORY: Delete: " << FrVect::getStaticName( ) << " "         \
              << (void*)this << " Line: " << __LINE__ << std::endl;
#else
#define MEM_ALLOCATE( )
#define MEM_DELETE( )
#endif

namespace FrameCPP
{
    namespace Version_3
    {
        //===================================================================
        // CLASS - FrVect::data_container_type
        //===================================================================

        FrVect::data_container_type::data_container_type( )
            : name( "" ), type( FR_VECT_C ), nData( 0 ), nBytes( 0 ),
              dimension( ), unitY( "" )
        {
        }

        FrVect::data_container_type::data_container_type(
            const std::string&    n,
            type_type             t,
            const byte_order_type byte_order,
            INT_4U                ndim,
            const Dimension*      dims,
            const std::string&    unit )
            : name( n ), type( t ), dimension( ), unitY( unit )
        {
            compress = RAW;

            for ( INT_4U x = 0; x < ndim; ++x, ++dims )
            {
                dimension.push_back( *dims );
            }

            nData = calc_nData( dimension );
            nBytes = GetTypeSize( type ) * nData;
        }

        /** \cond ignore_no_uniquely */
        FrVect::data_container_type::data_container_type(
            const std::string& Name,
            compress_type      Compress,
            type_type          Type,
            nData_type         NData,
            nBytes_type        NBytes,
            void*              Data,
            nDim_type          NDim,
            const Dimension*   Dims,
            const std::string& UnitY,
            bool               Allocate )
            : name( Name ), compress( Compress ), type( Type ), nData( NData ),
              nBytes( NBytes ), data( reinterpret_cast< CHAR_U* >( Data ) ),
              unitY( UnitY )
        {
            if ( Allocate )
            {
                CHAR_U* cur = reinterpret_cast< CHAR_U* >( Data );
                data.reset( new CHAR_U[ NBytes ] );
                std::copy( cur, cur + NBytes, data.get( ) );
            }
            for ( INT_4U x = 0; x < NDim; ++x, ++Dims )
            {
                dimension.push_back( *Dims );
            }
        }
        /** \endcond */

        /** \cond ignore_no_uniquely */
        FrVect::data_container_type::data_container_type(
            const std::string& Name,
            compress_type      Compress,
            type_type          Type,
            nData_type         NData,
            nBytes_type        NBytes,
            data_type          Data,
            nDim_type          NDim,
            const Dimension*   Dims,
            const std::string& UnitY )
            : name( Name ), compress( Compress ), type( Type ), nData( NData ),
              nBytes( NBytes ), data( Data ), unitY( UnitY )
        {
            for ( INT_4U x = 0; x < NDim; ++x, ++Dims )
            {
                dimension.push_back( *Dims );
            }
        }
        /** \endcond */

        bool
        FrVect::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        void
        FrVect::data_container_type::copy_core(
            const data_container_type& Source )
        {
            name = Source.name;
            compress = Source.compress;
            type = Source.type;
            nData = Source.nData;
            nBytes = Source.nBytes;

            for ( std::vector< Dimension >::const_iterator
                      d( Source.dimension.begin( ) ),
                  d_end( Source.dimension.end( ) );
                  d != d_end;
                  ++d )
            {
                dimension.push_back( *d );
            }

            unitY = Source.unitY;
        }

        bool
        FrVect::data_container_type::
        operator==( const data_container_type& RHS ) const
        {
#if LM_INFO
            if ( name != RHS.name )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( compress != RHS.compress )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( type != RHS.type )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( nData != RHS.nData )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( nBytes != RHS.nBytes )
            {
                std::cerr << "DEBUG: " << __LINE__ << " nBytes: " << nBytes
                          << " RHS.nBytes: " << RHS.nBytes << std::endl;
            }
            if ( dimension != RHS.dimension )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( unitY != RHS.unitY )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            if ( memcmp( data, RHS.data, nBytes ) != 0 )
            {
                std::cerr << "DEBUG: " << __LINE__ << std::endl;
            }
            std::cerr << "DEBUG: nData: " << nData << std::endl;
#endif
            return ( ( this == &RHS ) ||
                     ( ( name == RHS.name ) && ( compress == RHS.compress ) &&
                       ( type == RHS.type ) && ( nData == RHS.nData ) &&
                       ( nBytes == RHS.nBytes ) &&
                       ( dimension == RHS.dimension ) &&
                       ( unitY == RHS.unitY ) &&
                       ( ( data == RHS.data ) ||
                         ( std::equal( data.get( ),
                                       data.get( ) + nBytes,
                                       RHS.data.get( ) ) ) ) ) );
        }

        //===================================================================
        // CLASS - FrVect
        //===================================================================
        const int FrVect::DEFAULT_GZIP_LEVEL( 6 );

        FrVect::FrVect( )
            : FrameSpec::Object( s_object_id, StructDescription( ) ), m_data( )
        {
            MEM_ALLOCATE( );
        }

        FrVect::FrVect( const FrVect& Source )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              Common::FrVect( Source ), m_data( )
        {
            MEM_ALLOCATE( );

            m_data.copy_core( Source.m_data );
            copy_data( Source.m_data.data.get( ), m_data.nBytes );
        }

        FrVect::FrVect( const std::string&    name,
                        type_type             type,
                        nDim_type             nDim,
                        const Dimension*      dims,
                        const byte_order_type byte_order,
                        const void*           data,
                        const std::string&    unitY )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              m_data( name, type, byte_order, nDim, dims, unitY )
        {
            MEM_ALLOCATE( );
            copy_data(
                const_cast< CHAR_U* >( static_cast< const CHAR_U* >( data ) ),
                m_data.nBytes );
        }

        FrVect::FrVect( const std::string&    name,
                        type_type             type,
                        nDim_type             nDim,
                        const Dimension*      dims,
                        const byte_order_type byte_order,
                        void*                 data,
                        const std::string&    unitY,
                        bool                  allocate,
                        bool                  owns )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              m_data( name, type, byte_order, nDim, dims, unitY )
        {
            MEM_ALLOCATE( );
            copy_data( static_cast< CHAR_U* >( data ), m_data.nBytes );
        }

        /** \cond ignore_no_uniquely */
        FrVect::FrVect( const std::string& name,
                        compress_type      Compress,
                        type_type          type,
                        nDim_type          nDim,
                        const Dimension*   dims,
                        nData_type         NData,
                        nBytes_type        NBytes,
                        void*              data,
                        const std::string& unitY,
                        bool               allocate,
                        bool               owns )
            : Common::FrameSpec::Object( s_object_id, StructDescription( ) ),
              m_data( name,
                      Compress,
                      type,
                      NData,
                      NBytes,
                      data,
                      nDim,
                      dims,
                      unitY,
                      allocate )
        {
        }
        /** \endcond */

        /** \cond ignore_no_uniquely */
        FrVect::FrVect( const std::string& name,
                        compress_type      Compress,
                        type_type          type,
                        nDim_type          nDim,
                        const Dimension*   dims,
                        nData_type         NData,
                        nBytes_type        NBytes,
                        data_type          data,
                        const std::string& unitY )
            : Common::FrameSpec::Object( s_object_id, StructDescription( ) ),
              m_data(
                  name, Compress, type, NData, NBytes, data, nDim, dims, unitY )
        {
        }
        /** \endcond */

        FrVect::FrVect( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            //---------------------------------------------------------------------
            // Read information from the stream
            //---------------------------------------------------------------------
            DimBuffer dims;

            Stream >> m_data.name >> m_data.compress >> m_data.type >>
                m_data.nData >> m_data.nBytes;
            m_data.data.reset( new CHAR_U[ m_data.nBytes ] );
            Stream.read( reinterpret_cast< istream_type::char_type* >(
                             m_data.data.get( ) ),
                         m_data.nBytes );
            Stream >> dims >> m_data.unitY;
            Stream.Next( this );
            //---------------------------------------------------------------------
            // Reorder dimension data
            //---------------------------------------------------------------------
            dims.copy( m_data.dimension );
        }

        FrVect::~FrVect( )
        {
            MEM_DELETE( );
        }

        FrVect*
        FrVect::Create( istream_type& Stream ) const
        {
            return new FrVect( Stream );
        }

        const char*
        FrVect::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrVect::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH(
                    StructName( ), s_object_id, "Vector Data Structure" ) );

                ret( FrSE( "name", "STRING", "" ) );
                ret( FrSE( "compress", "INT_2U", "" ) );
                ret( FrSE( "type", "INT_2U", "" ) );
                ret( FrSE( "nData", "INT_4U", "" ) );
                ret( FrSE( "nBytes", "INT_4U", "" ) );
                ret( FrSE( "data", "CHAR[nBytes]", "" ) );
                ret( FrSE( "nDim", "INT_4U", "" ) );
                ret( FrSE( "nx", "INT_4U[nDim]", "" ) );
                ret( FrSE( "dx", "REAL_8[nDim]", "" ) );
                ret( FrSE( "unitX", "STRING[nDim]", "" ) );
                ret( FrSE( "unitY", "STRING", "" ) );

                ret( FrSE( "next", PTR_STRUCT::Desc( StructName( ) ) ) );
            }

            return &ret;
        }

        const std::string&
        FrVect::GetName( ) const
        {
            return m_data.name;
        }

        void
        FrVect::Compress( compression_scheme_type Scheme, int Level )
        {
            AT( );
            if ( Scheme == GetCompress( ) )
            {
                // Nothing to do. Just return
                AT( );
                return;
            }

            //---------------------------------------------------------------------
            // Compress the data
            //---------------------------------------------------------------------

            INT_4U                        compress = ( Scheme & 0xFF );
            const INT_8U                  nData( m_data.nData );
            const INT_8U                  nBytes( m_data.nBytes );
            boost::shared_array< CHAR_U > data_out;
            INT_8U                        data_out_len( 0 );

            FrameCPP::Compression::Compress( compress,
                                             Level,
                                             compress_type_map,
                                             compress_type_reverse_map,
                                             GetType( ),
                                             data_type_map,
                                             m_data.data.get( ),
                                             nData,
                                             nBytes,
                                             data_out,
                                             data_out_len );

            if ( data_out && data_out_len )
            {
                //-------------------------------------------------------------------
                // Save the compressed data
                //-------------------------------------------------------------------
                m_data.nBytes = nBytes_type( data_out_len );
                m_data.data = data_out;
                //-------------------------------------------------------------------
                // Save the compression mode
                //-------------------------------------------------------------------
                m_data.compress = ( MODE_HOSTENDIAN | compress );
            }
        }

        void
        FrVect::CompressData( cmn_compression_scheme_type Scheme,
                              cmn_compression_level_type  GZipLevel )
        {
            Compress( compression_scheme_type( Scheme ), GZipLevel );
        }

        FrameCPP::Common::FrameSpec::Object*
        FrVect::CloneCompressed( cmn_compression_scheme_type Scheme,
                                 cmn_compression_level_type  Level ) const
        {
            Common::FrameSpec::ObjectInterface::unique_object_type retval;
            boost::shared_array< CHAR_U > compressed_data;
            INT_8U                        nbytes( 0 );

            AT( );
            if ( Scheme == GetCompress( ) )
            {
                // Nothing to do. Just return
                AT( );
                return retval.release( );
            }

            compressToBuffer( compress_type_map,
                              compress_type_reverse_map,
                              data_type_map,
                              GetType( ),
                              GetNData( ),
                              GetData( false ).get( ),
                              GetNBytes( ),
                              GetCompress( ),
                              compressed_data,
                              nbytes,
                              Scheme,
                              Level );

            if ( nbytes && compressed_data.get( ) )
            {
                //-------------------------------------------------------------------
                // Save the compressed data
                //-------------------------------------------------------------------
                retval.reset( new FrVect( GetName( ),
                                          ( MODE_HOSTENDIAN | Scheme ),
                                          GetType( ),
                                          GetNDim( ),
                                          &( m_data.dimension[ 0 ] ),
                                          GetNData( ),
                                          nbytes,
                                          compressed_data,
                                          GetUnitY( ) ) );
            }
            //---------------------------------------------------------------------
            // Return the results
            //---------------------------------------------------------------------
            return retval.release( );
        }

        //-------------------------------------------------------------------
        /// In general, uncompressing follows three steps:
        ///
        /// <ol>
        ///   <li>Gunzip the data.</li>
        ///   <li>Fix byte-ordering.</li>
        ///   <li>Integrate.</li>
        /// </ol>
        ///
        /// Depending upon the compression type and byte-ordering differences,
        /// not all of these steps are needed.
        //-------------------------------------------------------------------
        void
        FrVect::Uncompress( )
        {
            AT( );
            //---------------------------------------------------------------------
            // Check if there is anything that needs to be done
            //---------------------------------------------------------------------
            if ( GetCompress( ) == RAW )
            {
                // Nothing to do. The buffer is already uncompressed
                AT( );
                return;
            }

            //---------------------------------------------------------------------
            // There is something to be done
            //---------------------------------------------------------------------
            INT_8U                        nBytes( m_data.nBytes );
            boost::shared_array< CHAR_U > expanded_buffer;

            expandToBuffer( expanded_buffer, nBytes );

            //---------------------------------------------------------------------
            // Now record what has been done
            //---------------------------------------------------------------------
            m_data.data = expanded_buffer;
            m_data.nBytes = nBytes_type( nBytes );
            m_data.compress = RAW;
        }

        bool
        FrVect::operator!=( const FrVect& RHS ) const
        {
            return !( *this == RHS );
        }

        bool
        FrVect::operator==( const FrVect& RHS ) const
        {
            return ( this->m_data == RHS.m_data );
        }

        FrVect&
        FrVect::operator+=( const FrVect& RHS )
        {
            //---------------------------------------------------------------------
            // Perform sanity checks
            //---------------------------------------------------------------------
            if ( ( this->GetName( ) != RHS.GetName( ) ) ||
                 ( this->GetType( ) != RHS.GetType( ) ) ||
                 ( this->GetNDim( ) != RHS.GetNDim( ) ) ||
                 ( this->GetUnitY( ) != RHS.GetUnitY( ) ) )
            {
                std::ostringstream msg;
                bool               comma;

                msg << "Unable to concat the FrVect structures because: ";
                comma = false;
                if ( this->GetName( ) != RHS.GetName( ) )
                {
                    msg << "name of objects differ (" << this->GetName( )
                        << " vs. " << RHS.GetName( ) << ")";
                    comma = true;
                }
                if ( this->GetType( ) != RHS.GetType( ) )
                {
                    if ( comma )
                    {
                        msg << ", ";
                    }
                    msg << "type of objects differ (" << this->GetType( )
                        << " vs. " << RHS.GetType( ) << ")";
                    comma = true;
                }
                if ( this->GetNDim( ) != RHS.GetNDim( ) )
                {
                    if ( comma )
                    {
                        msg << ", ";
                    }
                    msg << "number of dimensions differ (" << this->GetNDim( )
                        << " vs. " << RHS.GetNDim( ) << ")";
                    comma = true;
                }
                if ( this->GetUnitY( ) != RHS.GetUnitY( ) )
                {
                    if ( comma )
                    {
                        msg << ", ";
                    }
                    msg << "y units differ (" << this->GetUnitY( ) << " vs. "
                        << RHS.GetUnitY( ) << ")";
                    comma = true;
                }
                throw std::domain_error( msg.str( ) );
            }
            for ( INT_4U cur = 0, last = this->GetNDim( ); cur != last; ++cur )
            {
                const Dimension& ld( this->GetDim( cur ) );
                const Dimension& rd( RHS.GetDim( cur ) );

                if ( cur > 0 )
                {
                    if ( ld != rd )
                    {
                        std::ostringstream msg;

                        msg << "Unable to concat the FrVect structures";
                        /// \todo
                        ///   Need to have a list of reasons
                        throw std::domain_error( msg.str( ) );
                    }
                }
                else
                {
                    if ( ( ld.GetNx( ) != rd.GetNx( ) ) ||
                         ( ld.GetUnitX( ) != rd.GetUnitX( ) ) )
                    {
                        std::ostringstream msg;

                        msg << "Unable to concat the FrVect structures";
                        /// \todo
                        ///   Need to have a list of reasons
                        throw std::domain_error( msg.str( ) );
                    }
                }
            }
            //---------------------------------------------------------------------
            // Calculate how much space is required
            //---------------------------------------------------------------------
            const INT_2U type_size( GetTypeSize( GetType( ) ) );
            this->GetDim( 0 ).SetNx( this->GetDim( 0 ).GetNx( ) +
                                     RHS.GetDim( 0 ).GetNx( ) );
            const INT_8U s = GetNData( ) + RHS.GetNData( );

            //---------------------------------------------------------------------
            // Allocate the space, fill it with the existing data and then store
            //   it.
            //---------------------------------------------------------------------
            boost::shared_array< CHAR_U > data( new CHAR_U[ s ] );
            std::copy( GetData( ).get( ),
                       GetData( ).get( ) + ( GetNData( ) * type_size ),
                       data.get( ) );
            std::copy( RHS.GetData( ).get( ),
                       RHS.GetData( ).get( ) + ( RHS.GetNData( ) * type_size ),
                       data.get( ) + ( GetNData( ) * type_size ) );
            copy_data( data.get( ), s );
            //---------------------------------------------------------------------
            // Return
            //---------------------------------------------------------------------
            return *this;
        }

        FrameCPP::cmn_streamsize_type
        FrVect::Bytes( const Common::StreamBase& Stream ) const
        {
            return m_data.name.Bytes( ) + sizeof( m_data.compress ) +
                sizeof( m_data.type ) + sizeof( m_data.nData ) +
                sizeof( m_data.nBytes ) + m_data.nBytes // data
                + ::Bytes( m_data.dimension ) + m_data.unitY.Bytes( ) +
                Stream.PtrStructBytes( ) // next
                ;
        }

        FrVect::data_type
        FrVect::GetData( bool Decompress ) const
        {
            if ( Decompress )
            {
                const_cast< FrVect* >( this )->Uncompress( );
            }
            return m_data.data;
        }

        FrVect::data_type
        FrVect::GetData( bool Decompress )
        {
            if ( Decompress )
            {
                Uncompress( );
            }

            return m_data.data;
        }

        template <>
        INT_2U
        FrVect::GetDataType< CHAR >( )
        {
            return FR_VECT_C;
        }

        template <>
        INT_2U
        FrVect::GetDataType< CHAR_U >( )
        {
            return FR_VECT_1U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_2S >( )
        {
            return FR_VECT_2S;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_2U >( )
        {
            return FR_VECT_2U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_4S >( )
        {
            return FR_VECT_4S;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_4U >( )
        {
            return FR_VECT_4U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_8S >( )
        {
            return FR_VECT_8S;
        }

        template <>
        INT_2U
        FrVect::GetDataType< INT_8U >( )
        {
            return FR_VECT_8U;
        }

        template <>
        INT_2U
        FrVect::GetDataType< REAL_4 >( )
        {
            return FR_VECT_4R;
        }

        template <>
        INT_2U
        FrVect::GetDataType< REAL_8 >( )
        {
            return FR_VECT_8R;
        }

        template <>
        INT_2U
        FrVect::GetDataType< COMPLEX_8 >( )
        {
            return FR_VECT_8C;
        }

        template <>
        INT_2U
        FrVect::GetDataType< COMPLEX_16 >( )
        {
            return FR_VECT_16C;
        }

        template <>
        INT_2U
        FrVect::GetDataType< std::string >( )
        {
            return FR_VECT_STRING;
        }

        FrVect::nData_type
        FrVect::GetNData( ) const
        {
            return m_data.nData;
        }

        //-----------------------------------------------------------------------------
        //-----------------------------------------------------------------------------
        size_t
        FrVect::GetTypeSize( type_type type )
        {
            switch ( type )
            {
            case FR_VECT_C:
                return sizeof( CHAR );
            case FR_VECT_2S:
                return sizeof( INT_2S );
            case FR_VECT_8R:
                return sizeof( REAL_8 );
            case FR_VECT_4R:
                return sizeof( REAL_4 );
            case FR_VECT_4S:
                return sizeof( INT_4S );
            case FR_VECT_8S:
                return sizeof( INT_8S );
            case FR_VECT_8C:
                return ( sizeof( REAL_4 ) * 2 );
            case FR_VECT_16C:
                return ( sizeof( REAL_8 ) * 2 );
                /*
                  ????
                  case FR_VECT_STR:
                  return sizeof( std::std::string );
                  ????
                */
            case FR_VECT_2U:
                return sizeof( INT_2U );
            case FR_VECT_4U:
                return sizeof( INT_4U );
            case FR_VECT_8U:
                return sizeof( INT_8U );
            case FR_VECT_1U:
                return sizeof( CHAR_U );
            }
            // None of above: Perhaps add type for throw?
            return 0;
        }

        FrVect&
        FrVect::Merge( const FrVect& RHS )
        {
            /// \todo
            ///   Need to implement Merge routine
            std::string msg( "Merge currently not implemented for " );
            msg += StructName( );

            throw std::domain_error( msg );
            return *this;
        }

        FrVect::subfrvect_type
        FrVect::SubFrVect( INT_4U Start, INT_4U Stop ) const
        {
            //---------------------------------------------------------------------
            // Create new FrVect
            //---------------------------------------------------------------------
            subfrvect_type retval( new FrVect );
            //---------------------------------------------------------------------
            // Calculate the block size
            //---------------------------------------------------------------------
            INT_4U       block_size = GetTypeSize( GetType( ) );
            const INT_4U ndim = GetNDim( );

            for ( INT_4U x = 1; x < ndim; ++x )
            {
                block_size *= GetDim( x ).GetNx( );
            }
            //---------------------------------------------------------------------
            // Copy the core of the information
            //---------------------------------------------------------------------
            retval->m_data.copy_core( this->m_data );
            //---------------------------------------------------------------------
            // Set the Dimension information for the sink
            //---------------------------------------------------------------------
            retval->GetDim( 0 ).SetNx( Stop - Start );
            retval->SetNData( Stop - Start );
            //---------------------------------------------------------------------
            // Allocate space in the sink
            //---------------------------------------------------------------------
            retval->copy_data( new CHAR_U[ block_size * ( Stop - Start ) ],
                               block_size * ( Stop - Start ) );
            retval->m_data.compress = RAW;
            //---------------------------------------------------------------------
            // Copy the data
            //---------------------------------------------------------------------
            const CHAR_U*                 data_source( GetDataRaw( ).get( ) );
            INT_8U                        nBytes( 0 );
            boost::shared_array< CHAR_U > expanded_buffer;

            expandToBuffer( expanded_buffer, nBytes );
            if ( nBytes )
            {
                data_source = expanded_buffer.get( );
            }
            CHAR_U* data_sink( retval->GetDataRaw( ).get( ) );

            for ( INT_8U x = Start; x < Stop; ++x )
            {
                //-------------------------------------------------------------------
                // Copy 1 block's worth of data
                //-------------------------------------------------------------------
                std::copy( data_source + ( x * block_size ),
                           data_source + ( ( x + 1 ) * block_size ),
                           data_sink + ( ( x - Start ) * block_size ) );
            }
            //---------------------------------------------------------------------
            // Return the newly constructed FrVect
            //---------------------------------------------------------------------
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wpessimizing-move"
#endif /* __clang__ */
            return MOVE_RET( retval );
#if __clang__ &&                                                               \
    ( ( __clang_major__ > 7 ) ||                                               \
      ( ( __clang_major__ == 7 ) && ( __clang_minor__ >= 3 ) ) )
#pragma clang diagnostic pop
#endif /* __clang__ */
        }

        //-----------------------------------------------------------------------
        /// This routine is called to verify the integrety of
        /// this FrVect object.
        //-----------------------------------------------------------------------
        void
        FrVect::VerifyObject( Common::Verify&       Verifier,
                              Common::IFrameStream& Stream ) const
        {
            if ( Verifier.Expandability( ) )
            {
                //-------------------------------------------------------------------
                // Verify that the buffer can be decompressed
                //-------------------------------------------------------------------
                INT_8U                        nBytes( 0 );
                boost::shared_array< CHAR_U > expanded_buffer;

                expandToBuffer( expanded_buffer, nBytes );
            }
        }

        void
        FrVect::Write( ostream_type& Stream ) const
        {
            //---------------------------------------------------------------------
            // Write data to the stream.
            //---------------------------------------------------------------------
            DimBuffer dims( m_data.dimension );
            Stream << m_data.name << m_data.compress << m_data.type
                   << m_data.nData << m_data.nBytes;
            Stream.write( reinterpret_cast< Common::OStream::char_type* >(
                              m_data.data.get( ) ),
                          m_data.nBytes );
            Stream << dims << m_data.unitY;
            WriteNext( Stream );
        }

        FrVect::demote_ret_type
        FrVect::demote( INT_2U          Target,
                        demote_arg_type Obj,
                        istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            throw Unimplemented( "Object* FrVect::demote( Object* Obj ) const",
                                 DATA_FORMAT_VERSION,
                                 __FILE__,
                                 __LINE__ );
        }

        FrVect::promote_ret_type
        FrVect::promote( INT_2U           Target,
                         promote_arg_type Obj,
                         istream_type*    Stream ) const
        {
            return Promote( Target, Obj, Stream );
        }

        void
        FrVect::expandToBuffer( boost::shared_array< CHAR_U >& Dest,
                                INT_8U& DestNBytes ) const
        {
            /// \todo The use of BYTE_ORDER_HOST here is flawed. Need to capture
            ///       the byte order information when the object is read from
            ///       disk. When initialially created, it will be in
            ///       BYTE_ORDER_HOST order.

            Common::FrVect::expandToBuffer( compress_type_map,
                                            data_type_map,
                                            GetType( ),
                                            GetNData( ),
                                            GetDataRaw( ).get( ),
                                            GetNBytes( ),
                                            GetCompress( ),
                                            ( ( GetCompress( ) & 0x100 )
                                                  ? BYTE_ORDER_LITTLE_ENDIAN
                                                  : BYTE_ORDER_BIG_ENDIAN ),
                                            Dest,
                                            DestNBytes );
        }

        //=======================================================================
        // Local functions
        //=======================================================================

        void
        FrVect::copy_data( CHAR_U* Data, INT_8U NBytes )
        {
            m_data.data.reset( new CHAR_U[ NBytes ] );
            if ( Data )
            {
                std::copy( Data, Data + NBytes, m_data.data.get( ) );
            }
        }
    } // namespace Version_3
} // namespace FrameCPP

using FrameCPP::Version_3::FrVect;

#if !defined( __SUNPRO_CC ) || ( __SUNPRO_CC > 0x550 )
#include "framecpp/Version3/FrVect.tcc"

#define INSTANTIATE( LM_TYPE )                                                 \
    template FrVect::FrVect( const std::string& name,                          \
                             nDim_type          nDim,                          \
                             const Dimension*   dims,                          \
                             const LM_TYPE*     data,                          \
                             const std::string& unitY );                       \
                                                                               \
    template FrVect::FrVect( const std::string& name,                          \
                             nDim_type          nDim,                          \
                             const Dimension*   dims,                          \
                             LM_TYPE*           data,                          \
                             const std::string& unitY,                         \
                             bool               allocate,                      \
                             bool               owns )

#include "TypeInstantiation.tcc"

#undef INSTANTIATE
#endif /* */
