//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/Description.hh"
#include "framecpp/Common/IOStream.hh"

#include "framecpp/Version3/FrRawData.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"
#include "framecpp/Version3/PTR_STRUCT.hh"

using namespace FrameCPP::Version_3;
using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define TRACE_MEMORY 0
#if TRACE_MEMORY
#define MEM_ALLOCATE( )                                                        \
    std::cerr << "MEMORY: Allocate: " << FrRawData::getStaticName( ) << " "    \
              << (void*)this << std::endl;
#define MEM_DELETE( )                                                          \
    std::cerr << "MEMORY: Delete: " << FrRawData::getStaticName( ) << " "      \
              << (void*)this << std::endl;
#else
#define MEM_ALLOCATE( )
#define MEM_DELETE( )
#endif

static const int MAX_REF = 4;

FrRawData::FrRawData( ) : Object( CLASS_ID, StructDescription( ) )
{
    MEM_ALLOCATE( );
}

FrRawData::FrRawData( const FrRawData& rawData )
    : Object( CLASS_ID, StructDescription( ) )
{
    MEM_ALLOCATE( );
    m_data.name = rawData.m_data.name;
    m_data.firstSer = rawData.m_data.firstSer;
    m_data.firstAdc = rawData.m_data.firstAdc;
    m_data.logMsg = rawData.m_data.logMsg;
    m_data.more = rawData.m_data.more;
}

FrRawData::FrRawData( const std::string& name )
    : Object( CLASS_ID, StructDescription( ) )
{
    MEM_ALLOCATE( );
    m_data.name = name;
}

FrRawData::FrRawData( istream_type& Stream )
    : Object( CLASS_ID, StructDescription( ) )
{
    Stream >> m_data.name >> m_data.firstSer >> m_data.firstAdc >>
        m_data.logMsg >> m_data.more;
}

FrRawData::~FrRawData( )
{
    MEM_DELETE( );
}

const std::string&
FrRawData::GetName( ) const
{
    return m_data.name;
}

FrameCPP::cmn_streamsize_type
FrRawData::Bytes( const Common::StreamBase& Stream ) const
{
    cmn_streamsize_type retval = m_data.name.Bytes( ) +
        +Stream.PtrStructBytes( ) // firstSer
        + Stream.PtrStructBytes( ) // firstAdc
        + Stream.PtrStructBytes( ) // logMsg
        + Stream.PtrStructBytes( ) // more
        ;
    return retval;
}

FrRawData*
FrRawData::Create( istream_type& Stream ) const
{
    return new FrRawData( Stream );
}

const char*
FrRawData::ObjectStructName( ) const
{
    return StructName( );
}

const Description*
FrRawData::StructDescription( )
{
    static Description ret;

    if ( ret.size( ) == 0 )
    {
        ret( FrSH( FrRawData::StructName( ),
                   FrRawData::CLASS_ID,
                   "Raw Data Structure" ) );

        ret( FrSE( "name", "STRING", "" ) );
        ret( FrSE(
            "firstSer", PTR_STRUCT::Desc( FrSerData::StructName( ) ), "" ) );
        ret( FrSE(
            "firstAdc", PTR_STRUCT::Desc( FrAdcData::StructName( ) ), "" ) );
        ret( FrSE( "logMsg", PTR_STRUCT::Desc( FrMsg::StructName( ) ), "" ) );
        ret( FrSE( "more", PTR_STRUCT::Desc( FrVect::StructName( ) ), "" ) );
    }
    return &ret;
}

void
FrRawData::Write( ostream_type& Stream ) const
{
    Stream << m_data.name << m_data.firstSer << m_data.firstAdc << m_data.logMsg
           << m_data.more;
}

bool
FrRawData::operator==( const Common::FrameSpec::Object& Obj ) const
{
    return compare( *this, Obj );
}

FrRawData::demote_ret_type
FrRawData::demote( INT_2U          Target,
                   demote_arg_type Obj,
                   istream_type*   Stream ) const
{
    if ( Target >= DATA_FORMAT_VERSION )
    {
        return Obj;
    }
    throw Unimplemented( "Object* FrRawData::Demote( Object* Obj ) const",
                         DATA_FORMAT_VERSION,
                         __FILE__,
                         __LINE__ );
}

FrRawData::promote_ret_type
FrRawData::promote( INT_2U           Target,
                    promote_arg_type Obj,
                    istream_type*    Stream ) const
{
    return Promote( Target, Obj, Stream );
}

FrRawData::fr_raw_data_type::fr_raw_data_type( )
    : firstSer( false ), firstAdc( false )
{
}
