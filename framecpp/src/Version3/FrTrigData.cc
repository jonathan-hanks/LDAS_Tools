//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include "framecpp/Common/IOStream.hh"
#include "framecpp/Common/Description.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "framecpp/Version3/FrTrigData.hh"
#include "framecpp/Version3/FrSE.hh"
#include "framecpp/Version3/FrSH.hh"

#include "framecpp/Version3/PTR_STRUCT.hh"

#include "Common/ComparePrivate.hh"

using FrameCPP::Common::Description;
using FrameCPP::Common::FrameSpec;

#define LM_DEBUG 0

//=======================================================================
// Static
//=======================================================================

static const FrameSpec::Info::frame_object_types s_object_id =
    FrameSpec::Info::FSI_FR_EVENT;

namespace FrameCPP
{
    namespace Version_3
    {
        //===================================================================
        // FrTrigDataStorage::data_type
        //===================================================================
        FrameCPP::cmn_streamsize_type
        FrTrigDataStorage::data_type::Bytes( ) const
        {
            FrameCPP::cmn_streamsize_type retval = name.Bytes( ) +
                comment.Bytes( ) + inputs.Bytes( ) + Common::Bytes( GTime ) +
                sizeof( bvalue ) + sizeof( rvalue ) + sizeof( probability ) +
                statistics.Bytes( );

            return retval;
        }

        void
        FrTrigDataStorage::data_type::operator( )( Common::IStream& Stream )
        {
            Stream >> name >> comment >> inputs >> GTime >> bvalue >> rvalue >>
                probability >> statistics;
        }

        void
        FrTrigDataStorage::data_type::
        operator( )( Common::OStream& Stream ) const
        {
            Stream << name << comment << inputs << GTime << bvalue << rvalue
                   << probability << statistics;
        }

        //=======================================================================
        // FrTrigData
        //=======================================================================
        FrTrigData::FrTrigData( )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
        }

        FrTrigData::FrTrigData( const FrTrigData& Source )
            : FrameSpec::Object( s_object_id, StructDescription( ) ),
              FrTrigDataStorage( Source ), FrTrigDataRefs( Source )
        {
        }

        FrTrigData::FrTrigData( const std::string& Name,
                                const std::string& Comment,
                                const std::string& Inputs,
                                const GPSTime&     GTime,
                                INT_4U             Bvalue,
                                REAL_4             Rvalue,
                                REAL_4             Probability,
                                const std::string& Statistics )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data.name = Name;
            m_data.comment = Comment;
            m_data.inputs = Inputs;
            m_data.GTime = GTime;
            m_data.bvalue = Bvalue;
            m_data.rvalue = Rvalue;
            m_data.probability = Probability;
            m_data.statistics = Statistics;
        }

        FrTrigData::FrTrigData( istream_type& Stream )
            : FrameSpec::Object( s_object_id, StructDescription( ) )
        {
            m_data( Stream );
            m_refs( Stream );

            Stream.Next( this );
        }

        FrTrigData*
        FrTrigData::Create( istream_type& Stream ) const
        {
            return new FrTrigData( Stream );
        }

        const std::string&
        FrTrigData::GetNameSlow( ) const
        {
            return GetName( );
        }

        FrTrigData&
        FrTrigData::Merge( const FrTrigData& RHS )
        {
            throw Unimplemented(
                "FrTrigData& FrTrigData::Merge( const FrTrigData& RHS )",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
            return *this;
        }

        const char*
        FrTrigData::ObjectStructName( ) const
        {
            return StructName( );
        }

        const Description*
        FrTrigData::StructDescription( )
        {
            static Description ret;

            if ( ret.size( ) == 0 )
            {
                ret( FrSH( FrTrigData::StructName( ),
                           s_object_id,
                           "Event Data Structure" ) );

                ret( FrSE( "name", "STRING", "Name of event." ) );
                ret( FrSE( "comment", "STRING", "Descriptor of event" ) );
                ret( FrSE( "inputs",
                           "STRING",
                           "Input channels and filter parameters to event "
                           "process." ) );
                ret( FrSE( "GTimeS",
                           "INT_4U",
                           "GPS time in seconds corresponding to reference "
                           "vale of event,"
                           " as defined by the search algorigthm." ) );
                ret( FrSE(
                    "GTimeN",
                    "INT_4U",
                    "GPS time in residual nanoseconds relative to GTimeS" ) );
                ret( FrSE( "bvalue",
                           "INT_4U",
                           "Boolean (T=1, F=0) value returned by trigger" ) );
                ret( FrSE( "rvalue",
                           "REAL_4",
                           "Continuouis output amplitude returned by event" ) );
                ret( FrSE( "probability",
                           "REAL_4",
                           "Likelihood estimate of event, if available"
                           " (probability = -1 if cannot be estimated)" ) );
                ret( FrSE( "statistics",
                           "STRING",
                           "Statistical description of event, if relevant or "
                           "available" ) );
                ret( FrSE(
                    "data", PTR_STRUCT::Desc( FrVect::StructName( ) ), "" ) );
                ret( FrSE( "next",
                           PTR_STRUCT::Desc( FrTrigData::StructName( ) ),
                           "" ) );
            }

            return &ret;
        }

        void
        FrTrigData::Write( ostream_type& Stream ) const
        {
            m_data( Stream );
            m_refs( Stream );
            WriteNext( Stream );
        }

        bool
        FrTrigData::operator==( const Common::FrameSpec::Object& Obj ) const
        {
            return Common::Compare( *this, Obj );
        }

        FrTrigData::demote_ret_type
        FrTrigData::demote( INT_2U          Target,
                            demote_arg_type Obj,
                            istream_type*   Stream ) const
        {
            if ( Target >= DATA_FORMAT_VERSION )
            {
                return Obj;
            }
            throw Unimplemented(
                "Object* FrTrigData::demote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }

        FrTrigData::promote_ret_type
        FrTrigData::promote( INT_2U           Target,
                             promote_arg_type Obj,
                             istream_type*    Stream ) const
        {
            throw Unimplemented(
                "Object* FrTrigData::promote( Object* Obj ) const",
                DATA_FORMAT_VERSION,
                __FILE__,
                __LINE__ );
        }
    } // namespace Version_3
} // namespace FrameCPP
