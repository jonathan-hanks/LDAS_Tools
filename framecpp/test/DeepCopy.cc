//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#if HAVE_CONFIG_H
#include <framecpp_config.h>
#endif /* HAVE_CONFIG_H */

// #include<malloc.h>

#include <iostream>
#include <fstream>
#include <vector>

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>

#include "ldastoolsal/gpstime.hh"

#include "framecpp/FrameCPP.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrHistory.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrDetector.hh"
#include "framecpp/FrStatData.hh"
#include "framecpp/Dimension.hh"

#include "test.hh"
#include "FrSample.hh"

using namespace FrameCPP::Version;
using namespace std;

// The name of the program executable
char* programname = 0;

void
error_watch( const string& msg )
{
    cerr << msg << endl;
}

void
usage( char* f )
{
    if ( !strncasecmp( f, "-h", 2 ) || !strncasecmp( f, "--h", 3 ) )
    {
        exit( 0 );
    }
}

BOOST_AUTO_TEST_CASE( FrVect )
{
    //---------------------------------------------------------------------
    // FrVect
    //---------------------------------------------------------------------
    {
        CHAR      data[ 4 ] = { 'a', 'a', 'a', 'a' };
        Dimension datadims[ 1 ] = { Dimension( 2, 2.00, "TesT", 2.00 ) };
        FrVect    s( "TesT", 1, datadims, data, "TesT" );
        FrVect    d( s );

        BOOST_CHECK( s.GetDataRaw( ) != d.GetDataRaw( ) );
    }
}

BOOST_AUTO_TEST_CASE( FrameH )
{
    //---------------------------------------------------------------------
    // FrameH
    //---------------------------------------------------------------------

    {
        stat_data_container_type sd;
        make_frame_ret_type      obj =
            ( makeFrameVersion( FRAME_SPEC_CURRENT, sd ) );

        FrameH*    frame = dynamic_cast< FrameH* >( obj.get( ) );
        const long adc_count(
            frame->GetRawData( )->RefFirstAdc( )[ 0 ].use_count( ) );
        const long proc_count( frame->RefProcData( )[ 0 ].use_count( ) );

        FrameH frame2( *frame );

        BOOST_CHECK( frame->GetRawData( ) != frame2.GetRawData( ) );
        BOOST_CHECK( &( frame->GetRawData( )->RefFirstAdc( ) ) !=
                     &( frame2.GetRawData( )->RefFirstAdc( ) ) );
        BOOST_CHECK( frame2.GetRawData( )->RefFirstAdc( )[ 0 ].use_count( ) >
                     adc_count );
        BOOST_CHECK( frame2.RefProcData( )[ 0 ].use_count( ) > proc_count );
    }
}
