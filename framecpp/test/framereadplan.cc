//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <framecpp_config.h>

#include <iostream.h>
#include <fstream.h>
#include <vector>

#include "../src/framereadplan.hh"
#include "../src/dump.hh"

void
error_watch( const string& msg )
{
    cerr << msg << endl;
}

// M1:DAQ-EDCU_TEST_000.rms
string names[] = { "M0:DAQ-ADCU4k2_chanCount.min", "M1:DAQ-EDCU_TEST_000.rms" };

int
main( int argc, char* argv[] ) try
{
    ifstream                tin( argv[ 1 ] );
    FrameCPP::FrameReadPlan fr( tin ); // Create the maps

    vector< string > adcNames;
    for ( unsigned i = 0; i < 2; i++ )
        adcNames.push_back( names[ i ] );

    fr.daqTriggerADC( adcNames ); // Specify what I want

    // Update using the maps and dump the contents
    for ( int i = 2; i < argc; i++ )
    {
        ifstream               tin1( argv[ i ] );
        const FrameCPP::Frame& frame = fr.readFrame( tin1, 0 );
        dump( cout, const_cast< FrameCPP::Frame& >( frame ) );
    }
}
catch ( exception& ex )
{
    cerr << "failed " << ex.what( ) << endl;
}
