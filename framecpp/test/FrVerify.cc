//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#include <iostream>
#include <memory>
#include <stdexcept>

#include "ldastoolsal/MemChecker.hh"
#include "ldastoolsal/CommandLineOptions.hh"

#include "framecpp/Common/FrameSpec.hh"
#include "framecpp/Common/Verify.hh"

#include "framecpp/FrameCPP.hh"

#include "StandardOptions.hh"

#define DEFAULT_BUFFER_SIZE 256

using LDASTools::AL::MemChecker;

typedef LDASTools::AL::CommandLineOptions CommandLineOptions;
typedef CommandLineOptions::Option        Option;
typedef CommandLineOptions::OptionSet     OptionSet;
using FrameCPP::Common::Verify;
using FrameCPP::Common::VerifyException;

namespace
{
    enum error_type
    {
        MIN_ERROR = VerifyException::MAX_ERROR + 1,
        USAGE = MIN_ERROR,
        UNHANDLED_EXCEPTION,
        MAX_ERROR = UNHANDLED_EXCEPTION
    };

    static const char* error_strings[] = {
        "usage error",
        "an exception was thown and not handled within the control loop "
    };

} // namespace

//-----------------------------------------------------------------------
// Common tasks to be performed when exiting.
//-----------------------------------------------------------------------
inline void
depart( int ExitCode )
{
    exit( ExitCode );
}

//-----------------------------------------------------------------------
/// \brief Class to handle command line options for this application
//-----------------------------------------------------------------------
class CommandLine : protected CommandLineOptions
{
public:
    using CommandLineOptions::empty;
    using CommandLineOptions::Pop;
    typedef CommandLineOptions::option_type option_type;

    CommandLine( int ArgC, char** ArgV );

    inline void
    Usage( int ExitValue ) const
    {
        std::cerr << "Usage: " << ProgramName( ) << m_options << std::endl;
        depart( ExitValue );
    }

    inline bool
    BadOption( ) const
    {
        bool retval = false;

        for ( const_iterator cur = begin( ), last = end( ); cur != last; ++cur )
        {
            if ( ( *cur )[ 0 ] == '-' )
            {
                std::cerr << "ABORT: Bad option: " << *cur << std::endl;
                retval = true;
            }
        }
        return retval;
    }

    inline size_t
    BlockSize( )
    {
        return m_block_size;
    }

    inline void
    BlockSize( size_t Value )
    {
        m_block_size = Value;
    }

    inline bool
    ChecksumFileOnly( )
    {
        return m_checksum_file_only;
    }

    inline void
    ChecksumFileOnly( bool Value )
    {
        m_checksum_file_only = Value;
    }

    inline bool
    ChecksumFrames( )
    {
        return m_checksum_frames;
    }

    inline void
    ChecksumFrames( bool Value )
    {
        m_checksum_frames = Value;
    }

    inline bool
    CollectAllErrors( ) const
    {
        return collect_all_errors;
    }

    inline void
    CollectAllErrors( bool Value )
    {
        collect_all_errors = Value;
    }

    inline bool
    DataValid( )
    {
        return m_data_valid;
    }

    inline void
    DataValid( bool Value )
    {
        m_data_valid = Value;
    }

    inline bool
    DataValidAll( )
    {
        return m_data_valid_all;
    }

    inline void
    DataValidAll( bool Value )
    {
        m_data_valid_all = Value;
    }

    inline bool
    DuplicateChannelNames( )
    {
        return duplicate_channel_names;
    }

    inline void
    DuplicateChannelNames( bool Value )
    {
        duplicate_channel_names = Value;
    }

    inline bool
    Force( )
    {
        return m_force;
    }

    inline void
    Force( bool Value )
    {
        m_force = Value;
    }

    inline bool
    MetadataCheck( )
    {
        return m_metadata_check;
    }

    inline void
    MetadataCheck( bool Value )
    {
        m_metadata_check = Value;
    }

    inline bool
    Relax( )
    {
        return m_relax;
    }

    inline void
    Relax( bool Value )
    {
        m_relax = Value;
    }

    inline bool
    RelaxEOFChecksum( )
    {
        return m_relax_eof_checksum;
    }

    inline void
    RelaxEOFChecksum( bool Value )
    {
        m_relax_eof_checksum = Value;
    }

    inline bool
    UseMemoryMappedIO( )
    {
        return m_use_memory_mapped_io;
    }

    inline void
    UseMemoryMappedIO( bool Value )
    {
        m_use_memory_mapped_io = Value;
    }

    inline size_t
    Verbose( )
    {
        return m_verbose;
    }

    inline void
    VerboseInc( )
    {
        ++m_verbose;
    }

private:
    enum option_types
    {
        OPT_BLOCK_SIZE,
        OPT_CHECKSUM_FILE_ONLY,
        OPT_CHECKSUM_FRAMES,
        OPT_COLLECT_ALL_ERRORS,
        OPT_DATA_VALID,
        OPT_DATA_VALID_ALL,
        OPT_DUPLICATE_CHANNEL_NAMES,
        OPT_FORCE,
        OPT_HELP,
        OPT_METADATA_CHECK,
        OPT_METADATA_CHECK_NO,
        OPT_RELAX,
        OPT_RELAX_EOF_CHECKSUM,
        OPT_USE_MEMORY_MAPPED_IO,
        OPT_VERBOSE
    };

    OptionSet m_options;
    size_t    m_block_size;
    bool      m_checksum_file_only;
    bool      m_checksum_frames;
    bool      collect_all_errors;
    bool      m_data_valid;
    bool      m_data_valid_all;
    bool      duplicate_channel_names;
    bool      m_force;
    bool      m_metadata_check;
    bool      m_relax;
    bool      m_relax_eof_checksum;
    bool      m_use_memory_mapped_io;
    size_t    m_verbose;
};

CommandLine::CommandLine( int ArgC, char** ArgV )
    : CommandLineOptions( ArgC, ArgV ), m_block_size( DEFAULT_BUFFER_SIZE ),
      m_checksum_file_only( false ), m_checksum_frames( false ),
      collect_all_errors( false ), m_data_valid( false ),
      m_data_valid_all( false ), duplicate_channel_names( true ),
      m_force( false ), m_relax( false ), m_relax_eof_checksum( false ),
      m_use_memory_mapped_io( false ), m_verbose( 0 )
{
    //---------------------------------------------------------------------
    // Setup the options that will be recognized.
    //---------------------------------------------------------------------
    std::ostringstream desc;

    m_options.Synopsis( "[options] <file> [<file> ...]" );

    m_options.Summary(
        "This verifies the contents of each file on the command line." );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    StandardOption< STANDARD_OPTION_HELP >( m_options, OPT_HELP );
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Verify each of the files instead of exiting on first exit."
            " (Default: "
         << ( ( Force( ) ) ? "true" : "false" ) << " )";
    m_options.Add(
        Option( OPT_FORCE, "force", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Use a relaxed interpretation of the frame specification"
            " (Default: "
         << ( ( Relax( ) ) ? "true" : "false" ) << " )";
    m_options.Add(
        Option( OPT_RELAX, "relax", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Accepts file that lack an end of file checksum."
            " (Default: "
         << ( ( RelaxEOFChecksum( ) ) ? "enabled" : "disabled" ) << " )";
    m_options.Add( Option( OPT_RELAX_EOF_CHECKSUM,
                           "relax-eof-checksum",
                           Option::ARG_NONE,
                           desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Collect all errors associated with the stream"
            " (Default: "
         << ( ( CollectAllErrors( ) ) ? "All" : "first" ) << " )";
    m_options.Add( Option( OPT_COLLECT_ALL_ERRORS,
                           "collect-all-errors",
                           Option::ARG_NONE,
                           desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc
        << "Check the dataValid bit in FrAdcData structures for non-zero values"
           " (Default: "
        << ( ( DataValid( ) ) ? "check" : "no check" ) << " )";
    m_options.Add(
        Option( OPT_DATA_VALID, "data-valid", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Check the dataValid bit in FrAdcData structures for non-zero "
            "values. This reports all channels with non-zero dataValid values."
            " (Default: "
         << ( ( DataValidAll( ) ) ? "all" : "first only" ) << " )";
    m_options.Add( Option(
        OPT_DATA_VALID_ALL, "data-valid-all", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Check for duplicate channel names within a frame"
            " (Default: "
         << ( ( DuplicateChannelNames( ) ) ? "check" : "no check" ) << " )";
    m_options.Add( Option( OPT_DUPLICATE_CHANNEL_NAMES,
                           "check-for-duplicate-channel-names",
                           Option::ARG_OPTIONAL,
                           desc.str( ),
                           "<bool>",
                           new CommandLineOptions::ValidateBool( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Enable the checking of the frame's meta data."
            " (Default: "
         << ( ( MetadataCheck( ) ) ? "check" : "no check" ) << " )";
    m_options.Add( Option(
        OPT_METADATA_CHECK, "metadata-check", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Disable the checking of the frame's meta data."
            " (Default: "
         << ( ( MetadataCheck( ) ) ? "check" : "no check" ) << " )";
    m_options.Add( Option( OPT_METADATA_CHECK_NO,
                           "no-metadata-check",
                           Option::ARG_NONE,
                           desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Display file name and exit status of each file processed."
            " If it appears multiple times on the command line,"
            " the level of verbosity will increase."
            " (Default: "
         << ( ( Verbose( ) ) ? "verbose" : "silent" ) << " )";
    m_options.Add(
        Option( OPT_VERBOSE, "verbose", Option::ARG_NONE, desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Only validate the file checksum."
            " (Default: "
         << ( ( ChecksumFileOnly( ) ) ? "yes" : "no" ) << " )";
    m_options.Add( Option( OPT_CHECKSUM_FILE_ONLY,
                           "checksum-file-only",
                           Option::ARG_NONE,
                           desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    desc.str( "" );
    desc << "Run checksums on individual frames."
            " (Default: "
         << ( ( ChecksumFrames( ) ) ? "yes" : "no" ) << " )";
    m_options.Add( Option( OPT_CHECKSUM_FRAMES,
                           "checksum-frames",
                           Option::ARG_NONE,
                           desc.str( ) ) );

    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    StandardOption< STANDARD_OPTION_USE_MMAP >(
        m_options, OPT_USE_MEMORY_MAPPED_IO, UseMemoryMappedIO( ) );
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    StandardOption< STANDARD_OPTION_BLOCK_SIZE >(
        m_options, OPT_BLOCK_SIZE, BlockSize( ) );
    //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    static OptionSet exit_group[ 3 ];
    static OptionSet exit_help[ MAX_ERROR + 1 ];

    static OptionSet* exit_group_ptr =
        &exit_group[ ( sizeof( exit_group ) / sizeof( *exit_group ) ) - 1 ];

    exit_group[ 0 ].Summary( " " );
    exit_group[ 1 ].Summary( "Exit Code Summary:" );
    exit_group[ 2 ].Summary( " " );
    for ( unsigned int e = 0;
          e < ( sizeof( exit_group ) / sizeof( *exit_group ) );
          ++e )
    {
        m_options.Add( exit_group[ e ] );
    }

    for ( unsigned int e = VerifyException::NO_ERROR;
          e <= VerifyException::MAX_ERROR;
          ++e )
    {
        std::ostringstream msg;

        msg << e << " - "
            << VerifyException::StrError( (VerifyException::error_type)e );
        exit_help[ e ].Summary( msg.str( ) );
        exit_group_ptr->Add( exit_help[ e ] );
    }
    for ( unsigned int e = MIN_ERROR; e <= MAX_ERROR; ++e )
    {
        std::ostringstream msg;

        msg << e << " - " << error_strings[ e - MIN_ERROR ];
        exit_help[ e ].Summary( msg.str( ) );
        exit_group_ptr->Add( exit_help[ e ] );
    }

    //---------------------------------------------------------------------
    // Parse the options specified on the command line
    //---------------------------------------------------------------------

    try
    {
        std::string arg_name;
        std::string arg_value;
        bool        parsing = true;

        while ( parsing )
        {
            const int cmd_id( Parse( m_options, arg_name, arg_value ) );

            switch ( cmd_id )
            {
            case OPT_HELP:
            {
                Usage( 0 );
            }
            break;
            case CommandLineOptions::OPT_END_OF_OPTIONS:
                parsing = false;
                break;
            case OPT_BLOCK_SIZE:
            {
                std::istringstream bs_buffer( arg_value );
                size_t             bs;

                bs_buffer >> bs;
                BlockSize( bs );
            }
            break;
            case OPT_CHECKSUM_FILE_ONLY:
                ChecksumFileOnly( true );
                break;
            case OPT_CHECKSUM_FRAMES:
                ChecksumFrames( true );
                break;
            case OPT_COLLECT_ALL_ERRORS:
                CollectAllErrors( true );
                break;
            case OPT_DATA_VALID:
                DataValid( true );
                break;
            case OPT_DATA_VALID_ALL:
                DataValid( true );
                DataValidAll( true );
                break;
            case OPT_DUPLICATE_CHANNEL_NAMES:
                if ( arg_value.size( ) > 0 )
                {
                    DuplicateChannelNames( InterpretBoolean( arg_value ) );
                }
                else
                {
                    DuplicateChannelNames( true );
                }
                break;
            case OPT_FORCE:
                Force( true );
                break;
            case OPT_METADATA_CHECK:
                MetadataCheck( true );
                break;
            case OPT_METADATA_CHECK_NO:
                MetadataCheck( false );
                break;
            case OPT_RELAX:
                Relax( true );
                break;
            case OPT_RELAX_EOF_CHECKSUM:
                RelaxEOFChecksum( true );
                break;
            case OPT_USE_MEMORY_MAPPED_IO:
                UseMemoryMappedIO( true );
                break;
            case OPT_VERBOSE:
                VerboseInc( );
                break;
            }
        }
    }
    catch ( ... )
    {
    }
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------
int
main( int ArgC, char** ArgV ) try
{
    MemChecker::Trigger gc_trigger( true );
    CommandLine         cl( ArgC, ArgV );

    Verify verifier;
    INT_4U exit_status = VerifyException::NO_ERROR;

    FrameCPP::Initialize( );

    //---------------------------------------------------------------------
    // Disable memory mapped I/O so failed reads can return without
    //   causing a signal.
    //---------------------------------------------------------------------
    verifier.BufferSize( cl.BlockSize( ) * 1024 );

    verifier.CollectAllErrors( cl.CollectAllErrors( ) );
    verifier.CheckFrameChecksum( cl.ChecksumFrames( ) );
    verifier.CheckDataValid( cl.DataValid( ) );
    verifier.CheckDataValidAll( cl.DataValidAll( ) );
    verifier.CheckForDuplicateChannelNames( cl.DuplicateChannelNames( ) );
    verifier.MustHaveEOFChecksum( cl.RelaxEOFChecksum( ) == false );
    verifier.Strict( cl.Relax( ) == false );
    verifier.UseMemoryMappedIO( cl.UseMemoryMappedIO( ) );
    verifier.ValidateMetadata( cl.MetadataCheck( ) );

    if ( cl.empty( ) || cl.BadOption( ) )
    {
        cl.Usage( USAGE );
    }

    while ( cl.empty( ) == false )
    {
        CommandLine::option_type filename = cl.Pop( );
        std::string              error_msg;

        if ( cl.Verbose( ) )
        {
            std::cout << filename << std::flush;
        }
        INT_4U new_exit_status = VerifyException::NO_ERROR;
        try
        {
            verifier( filename );
        }
        catch ( const VerifyException& Exception )
        {
            new_exit_status = Exception.ErrorCode( );
            error_msg = Exception.what( );
        }
        catch ( const std::exception& Exception )
        {
            error_msg = Exception.what( );
            if ( cl.Verbose( ) )
            {
                std::cout << "ABORT: Caught exception: " << Exception.what( );
            }
            std::cout << std::endl;
            new_exit_status = UNHANDLED_EXCEPTION;
            continue;
        }
        catch ( ... )
        {
            if ( cl.Verbose( ) )
            {
                std::cout << "ABORT: Caught unknown exception: ";
            }
            std::cout << std::endl;
            new_exit_status = UNHANDLED_EXCEPTION;
            continue;
        }

        if ( cl.Verbose( ) )
        {
            Verify::error_info_container_type errors(
                verifier.ErrorInfoQueue( ) );

            std::cout << " " << new_exit_status;
            if ( !errors.empty( ) )
            {
                for ( Verify::error_info_container_type::const_iterator
                          cur = errors.begin( ),
                          last = errors.end( );
                      cur != last;
                      ++cur )
                {
                    std::cout << std::endl << '\t' << ( *cur )->what( );
                }
            }
            if ( cl.Verbose( ) > 1 )
            {
                if ( ( errors.empty( ) ) && ( !error_msg.empty( ) ) )
                {
                    std::cout << std::endl << '\t' << error_msg;
                }
                std::cout << std::endl
                          << '\t'
                          << VerifyException::StrError(
                                 VerifyException::error_type(
                                     new_exit_status ) );
            }
            std::cout << std::endl << std::flush;
        }
        //------------------------------------------------------------------
        // Check to see if processing should continue
        //------------------------------------------------------------------
        if ( ( cl.Force( ) == false ) &&
             ( new_exit_status != VerifyException::NO_ERROR ) )
        {
            //----------------------------------------------------------------
            // No, so just exit with the exit status
            //----------------------------------------------------------------
            depart( new_exit_status );
        }
        if ( exit_status < new_exit_status )
        {
            exit_status = new_exit_status;
        }
    }
    depart( exit_status );
}
catch ( std::exception& e )
{
    std::cerr << std::endl
              << "ABORT: Caught exception: " << e.what( ) << std::endl;
    depart( UNHANDLED_EXCEPTION );
}
catch ( ... )
{
    std::cerr << std::endl << "ABORT: Caught unknown exception: " << std::endl;
    depart( UNHANDLED_EXCEPTION );
}
