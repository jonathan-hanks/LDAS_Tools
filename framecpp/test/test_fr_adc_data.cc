//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>

#include "framecpp/FrAdcData.hh"

BOOST_AUTO_TEST_CASE( test_fr_adc_data_defaults )
{
  using namespace FrameCPP;

  auto adc = FrAdcData( );

  BOOST_CHECK( adc.GetSlope( ) == FrAdcData::DEFAULT_SLOPE );
  BOOST_CHECK( adc.GetBias( ) == FrAdcData::DEFAULT_BIAS );
  BOOST_CHECK( adc.GetFShift( ) == FrAdcData::DEFAULT_FSHIFT );
  BOOST_CHECK( adc.GetTimeOffset( ) == FrAdcData::DEFAULT_TIME_OFFSET );
  BOOST_CHECK( adc.GetDataValid( ) == FrAdcData::DEFAULT_DATA_VALID );
  BOOST_CHECK( adc.GetPhase( ) == FrAdcData::DEFAULT_PHASE );
}

BOOST_AUTO_TEST_CASE( test_fr_adc_data_setting )
{
  using namespace FrameCPP;

  namespace tt = boost::test_tools;

  constexpr auto EXPECTED_SLOPE = FrAdcData::slope_type{4.0};
  constexpr auto EXPECTED_BIAS = FrAdcData::bias_type{3.4};
  constexpr auto EXPECTED_TIME_OFFSET = FrAdcData::timeOffset_type{7.3};
  constexpr auto EXPECTED_DATA_VALID = FrAdcData::dataValid_type{1};

  auto adc = FrAdcData( );

  adc.SetSlope( EXPECTED_SLOPE );
  BOOST_TEST( adc.GetSlope( ) == EXPECTED_SLOPE, tt::tolerance( 0.0001 ) );

  adc.SetBias( EXPECTED_BIAS );
  BOOST_TEST( adc.GetBias( ) == EXPECTED_BIAS, tt::tolerance( 0.0001 )  );

  adc.SetTimeOffset( EXPECTED_TIME_OFFSET );
  BOOST_TEST( adc.GetTimeOffset( ) == EXPECTED_TIME_OFFSET, tt::tolerance( 0.0001 )  );

  adc.SetDataValid( EXPECTED_DATA_VALID );
  BOOST_TEST( adc.GetDataValid( ) == EXPECTED_DATA_VALID );
}

BOOST_AUTO_TEST_CASE( test_fr_adc_data_const_with_defaults )
{
  using namespace FrameCPP;

  namespace tt = boost::test_tools;

  auto EXPECTED_NAME = FrAdcData::name_type{ "Z1:ChanneName" };
  constexpr auto EXPECTED_CHANNEL_GROUP = FrAdcData::channelGroup_type{ 3 };
  constexpr auto EXPECTED_CHANNEL_NUMBER = FrAdcData::channelNumber_type{ 9328 };
  constexpr auto EXPECTED_N_BITS = FrAdcData::nBits_type{ 12 };
  constexpr auto EXPECTED_SAMPLE_RATE = FrAdcData::sampleRate_type{ 289.14 };

  using namespace FrameCPP;

  auto adc = FrAdcData( EXPECTED_NAME,
                        EXPECTED_CHANNEL_GROUP,
                        EXPECTED_CHANNEL_NUMBER,
                        EXPECTED_N_BITS,
                        EXPECTED_SAMPLE_RATE );

  BOOST_CHECK( adc.GetSlope( ) == FrAdcData::DEFAULT_SLOPE );
  BOOST_CHECK( adc.GetBias( ) == FrAdcData::DEFAULT_BIAS );
  BOOST_CHECK( adc.GetFShift( ) == FrAdcData::DEFAULT_FSHIFT );
  BOOST_CHECK( adc.GetTimeOffset( ) == FrAdcData::DEFAULT_TIME_OFFSET );
  BOOST_CHECK( adc.GetDataValid( ) == FrAdcData::DEFAULT_DATA_VALID );
  BOOST_CHECK( adc.GetPhase( ) == FrAdcData::DEFAULT_PHASE );
}
