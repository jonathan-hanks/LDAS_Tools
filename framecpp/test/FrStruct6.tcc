//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__FR_STRUCT_6_TCC
#define FRAME_CPP__TEST__FR_STRUCT_6_TCC

#include "framecpp/Version6/FrameH.hh"
#include "framecpp/Version6/FrAdcData.hh"
#include "framecpp/Version6/FrDetector.hh"
#include "framecpp/Version6/FrEvent.hh"
#include "framecpp/Version6/FrHistory.hh"
#include "framecpp/Version6/FrMsg.hh"
#include "framecpp/Version6/FrProcData.hh"
#include "framecpp/Version6/FrRawData.hh"
#include "framecpp/Version6/FrSerData.hh"
#include "framecpp/Version6/FrSimData.hh"
#include "framecpp/Version6/FrSimEvent.hh"
#include "framecpp/Version6/FrStatData.hh"
#include "framecpp/Version6/FrSummary.hh"
#include "framecpp/Version6/FrTable.hh"
#include "framecpp/Version6/FrVect.hh"

#define TEMPLATE_SPEC 6
#define NAMESPACE FrameCPP::Version_6
#define PREVIOUS_TEMPLATE_SPEC 4
#define PREVIOUS_NAMESPACE FrameCPP::Version_4
#define USING( ) using namespace NAMESPACE

template <>
mk_frame_object_ret_type
mk_frame_object< TEMPLATE_SPEC >( FrameObjectTypes Type )
{
    USING( );

    using FrameCPP::Common::FrameSpec;

    mk_frame_object_ret_type retval;

    switch ( Type )
    {
    case FrameSpec::Info::FSI_FRAME_H:
        retval.reset(
            new FrameH( "frame_h", 1, 8, GPSTime( 10, 20 ), 1, 3.0, 2 ) );
        break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        retval.reset( new FrAdcData( "fr_adc_data",
                                     3,
                                     2,
                                     8,
                                     1024,
                                     2.0,
                                     1.0,
                                     "meters",
                                     30.,
                                     10.1,
                                     4,
                                     40.0 ) );
        reinterpret_cast< FrAdcData* >( retval.get( ) )
            ->AppendComment( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
    {
        static const char pre[ 2 ] = { 'a', 'b' };
        retval.reset( new FrDetector( "fr_detector_name",
                                      pre,
                                      1.1,
                                      2.2,
                                      200.3,
                                      3.3,
                                      4.4,
                                      5.5,
                                      6.6,
                                      7.7,
                                      8.8,
                                      1800 ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            FrEvent::ParamList_type params;

            params.push_back( FrEvent::Param_type( "param1", 64.0 ) );

            retval.reset( new FrEvent( "fr_event",
                                       "fr_event_comment",
                                       "fr_event_inputs",
                                       GPSTime( 10, 20000000 ),
                                       1024.0,
                                       2048.0,
                                       1,
                                       16.0,
                                       32.0,
                                       "fr_event_statistics",
                                       params ) );
        }
        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
    {
        std::unique_ptr< FrHistory > fr_history(
            new FrHistory( "fr_history_name", 10, "fr_hsitory_comment" ) );
        retval.reset( fr_history.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_MSG:
    {
        std::unique_ptr< FrMsg > fr_msg(
            new FrMsg( "alarm", "message", 10, GPSTime::NowGPSTime( ) ) );
        retval.reset( fr_msg.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        retval.reset( new FrProcData( "fr_proc_data",
                                      "fr_proc_data_comment",
                                      1,
                                      0,
                                      16.0,
                                      2048.,
                                      1024.0,
                                      4.0,
                                      4096.0,
                                      0.0 ) );

        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        retval.reset( new FrRawData( "fr_raw_data" ) );
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        retval.reset(
            new FrSerData( "fr_ser_data", GPSTime( 10, 20 ), 1024. ) );

        reinterpret_cast< FrSerData* >( retval.get( ) )->SetData( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        retval.reset( new FrSimData(
            "fr_sim_data", "fr_sim_data_comment", 1024., 2048., 4096. ) );
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        {
            FrSimEvent::ParamList_type params;

            params.push_back( FrSimEvent::Param_type( "param1", 64.0 ) );

            retval.reset( new FrSimEvent( "fr_sim_event",
                                          "fr_sim_event_comment",
                                          "fr_sim_event_inputs",
                                          GPSTime( 10, 20000000 ),
                                          1024.0,
                                          2048.0,
                                          16.0,
                                          params ) );
        }
        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        retval.reset( new FrStatData( "fr_stat_data",
                                      "fr_stat_data_comment",
                                      "fr_stat_data_representation",
                                      1,
                                      2,
                                      4 ) );
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        retval.reset( new FrSummary( "fr_summary",
                                     "fr_summary_comment",
                                     "fr_summary_test",
                                     GPSTime( 10, 20 ) ) );

        break;
    case FrameSpec::Info::FSI_FR_TABLE:
    {
        std::unique_ptr< FrTable > fr_table( new FrTable( "testTable", 0 ) );
        fr_table->AppendComment( "hello world" );
        retval.reset( fr_table.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_VECT:
    {
        typedef REAL_8 vect_data_type;

        static const int            SAMPLES = 4;
        static const vect_data_type START = 16.0;
        static const vect_data_type INC = 0.5;

        Dimension      dim( SAMPLES );
        vect_data_type data[ SAMPLES ];

        vect_data_type cur_val = START;

        for ( int cur = 0; cur != SAMPLES; ++cur )
        {
            data[ cur ] = cur_val;
            cur_val += INC;
        }

        retval.reset( new FrVect( "fr_vect", 1, &dim, data, "fr_vect_unitY" ) );
    }
    break;
    default:
    {
        std::ostringstream msg;

        msg << "mk_frame_object<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << Type;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
    return retval;
}

//=======================================================================

template <>
void
verify_downconvert< TEMPLATE_SPEC >( frame_object_type  FrameObj,
                                     const std::string& Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }

    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );
    std::unique_ptr< FrObject > demoted_obj;

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
        //-------------------------------------------------------------------
        // FrameH
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrameH );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetRun, run );
                CHECK_NUMBER( previous, current, GetFrame, frame );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER( previous, current, GetULeapS, ULeapS );
                CHECK_NUMBER_CONST( previous, GetLocalTime, 0, localTime );
                CHECK_NUMBER( previous, current, GetDt, dt );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        //-------------------------------------------------------------------
        // FrAdcData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrAdcData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER(
                    previous, current, GetChannelGroup, channelGroup );
                CHECK_NUMBER(
                    previous, current, GetChannelNumber, channelNumber );
                CHECK_NUMBER( previous, current, GetNBits, nBits );
                CHECK_NUMBER( previous, current, GetBias, bias );
                CHECK_NUMBER( previous, current, GetSlope, slope );
                CHECK_STRING( previous, current, GetUnits, units );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_NUMBER( previous, current, GetFShift, fShift );
                CHECK_NUMBER( previous, current, GetDataValid, dataValid );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
        //-------------------------------------------------------------------
        // FrDetector
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrDetector );

            if ( previous && current )
            {
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS_DIFF( FrEvent, FrTrigData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING( previous, current, GetInputs, inputs );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER( previous, current, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, current, GetTimeAfter, timeAfter );
                CHECK_NUMBER_4( previous,
                                GetTriggerStatus,
                                current,
                                GetEventStatus,
                                triggerStatus );
                CHECK_NUMBER( previous, current, GetAmplitude, amplitude );
                CHECK_NUMBER( previous, current, GetProbability, probability );
                CHECK_STRING( previous, current, GetStatistics, statistics );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
        //-------------------------------------------------------------------
        // FrMsg
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_SAME( FrHistory );
        }
        break;
    case FrameSpec::Info::FSI_FR_MSG:
        //-------------------------------------------------------------------
        // FrMsg
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrMsg );

            if ( previous && current )
            {
                //---------------------------------------------------------------
                // Check elements
                //---------------------------------------------------------------
                BOOST_TEST_MESSAGE( Leader << "field: alarm" );
                BOOST_CHECK(
                    previous->GetAlarm( ).compare( current->GetAlarm( ) ) == 0 )

                    ;
                BOOST_TEST_MESSAGE( Leader << "field: message" );
                BOOST_CHECK( previous->GetMessage( ).compare(
                                 current->GetMessage( ) ) == 0 )

                    ;
                BOOST_TEST_MESSAGE( Leader << "field: severity" );
                BOOST_CHECK( previous->GetSeverity( ) ==
                             current->GetSeverity( ) )

                    ;
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrProcData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetFShift, fShift );

                BOOST_TEST_MESSAGE( Leader << "field: timeOffset" );
                BOOST_CHECK( previous->GetTimeOffset( ).GetTime( ) ==
                             current->GetTimeOffset( ) )

                    ;

                CHECK_NUMBER_CONST( previous, GetSampleRate, 0.0, fShift );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        //-------------------------------------------------------------------
        // FrRawData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrRawData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSerData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetTime, time );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_STRING( previous, current, GetData, data );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSimData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSimEvent );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING( previous, current, GetInputs, inputs );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER( previous, current, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, current, GetTimeAfter, timeAfter );
                CHECK_NUMBER( previous, current, GetAmplitude, amplitude );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrStatData );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING(
                    previous, current, GetRepresentation, representation );
                CHECK_NUMBER( previous, current, GetTimeStart, timeStart );
                CHECK_NUMBER( previous, current, GetTimeEnd, timeEnd );
                CHECK_NUMBER( previous, current, GetVersion, version );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSummary );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING( previous, current, GetTest, test );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_TABLE:
    {
        DEMOTE_TO_PREVIOUS( FrTable );

        //-----------------------------------------------------------------
        // Verify the elements
        //-----------------------------------------------------------------
        if ( previous )
        {
            CHECK_STRING( previous, current, GetName, name );
            CHECK_STRING( previous, current, GetComment, comment );
            CHECK_NUMBER( previous, current, GetNColumn, nColumn );
            CHECK_NUMBER( previous, current, GetNRow, nRow );
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_VECT:
        //-------------------------------------------------------------------
        // FrVect
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrVect );

            if ( previous && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetCompress, compress );
                CHECK_NUMBER( previous, current, GetType, type );
                CHECK_NUMBER( previous, current, GetNData, nData );
                CHECK_NUMBER( previous, current, GetNBytes, nBytes );
                CHECK_NUMBER( previous, current, GetNDim, nDim );
                CHECK_NUMBER( previous, current, GetDim( 0 ).GetNx, nx );
                CHECK_NUMBER( previous, current, GetDim( 0 ).GetDx, dx );
                CHECK_NUMBER(
                    previous, current, GetDim( 0 ).GetStartX, startX );
                CHECK_STRING( previous, current, GetDim( 0 ).GetUnitX, unitX );
                CHECK_STRING( previous, current, GetUnitY, unitY );
            }
        }
        break;
    default:
    {
        std::ostringstream msg;

        msg << "verify_downconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
}

template <>
void
verify_upconvert< TEMPLATE_SPEC >( frame_object_type  FrameObj,
                                   const std::string& Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }

    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
    {
        PROMOTE_FROM_PREVIOUS( FrameH );

        if ( promoted && previous )
        {
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_NUMBER( (INT_4S)previous, promoted, GetRun, run );
            CHECK_NUMBER( previous, promoted, GetFrame, frame );
            CHECK_NUMBER( previous, promoted, GetDataQuality, dataQuality );
            CHECK_NUMBER( previous, promoted, GetGTime, GTime );
            CHECK_NUMBER( previous, promoted, GetULeapS, ULeapS );
            CHECK_NUMBER( previous, promoted, GetDt, dt );
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
    {
        PROMOTE_FROM_PREVIOUS( FrAdcData );

        if ( promoted && previous )
        {
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_NUMBER( previous, promoted, GetChannelGroup, channelGroup );
            CHECK_NUMBER( previous, promoted, GetChannelNumber, channelNumber );
            CHECK_NUMBER( previous, promoted, GetNBits, nBits );
            CHECK_NUMBER( previous, promoted, GetBias, bias );
            CHECK_NUMBER( previous, promoted, GetSlope, slope );
            CHECK_STRING( previous, promoted, GetUnits, units );
            CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
#if 0
	CHECK_NUMBER( previous, promoted, GetTimeOffsetS, timeOffsetS );
	CHECK_NUMBER( previous, promoted, GetTimeOffsetN, timeOffsetN );
#endif /* 0 */
            CHECK_NUMBER( previous, promoted, GetFShift, fShift );
            CHECK_NUMBER( previous, promoted, GetDataValid, dataValid );
            //---------------------------------------------------------------
            // Fields that are new
            //---------------------------------------------------------------
            BOOST_TEST_MESSAGE( Leader << "field: phase" );
            BOOST_CHECK( promoted->GetPhase( ) == FR_ADC_DATA_DEFAULT_PHASE )

                ;
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
    {
        PROMOTE_FROM_PREVIOUS( FrDetector );

        if ( promoted && previous )
        {
            BOOST_TEST_MESSAGE( Leader << "field: name" );
            BOOST_CHECK( promoted->GetName( ).compare( previous->GetName( ) ) ==
                         0 )

                ;
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS_DIFF( FrEvent, FrTrigData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING( previous, promoted, GetInputs, inputs );
                CHECK_NUMBER( previous, promoted, GetGTime, GTime );
                CHECK_NUMBER( previous, promoted, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, promoted, GetTimeAfter, timeAfter );
                CHECK_NUMBER_4( previous,
                                GetTriggerStatus,
                                promoted,
                                GetEventStatus,
                                eventStatus );
                CHECK_NUMBER( previous, promoted, GetAmplitude, amplitude );
                CHECK_NUMBER( previous, promoted, GetProbability, probability );
                CHECK_STRING( previous, promoted, GetStatistics, statistics );
                /// \todo Check number of parameters to be zero
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
        //-------------------------------------------------------------------
        // FrHistory
        //-------------------------------------------------------------------
        PROMOTE_TO_SAME( FrHistory );
        break;
    case FrameSpec::Info::FSI_FR_MSG:
    {
        PROMOTE_FROM_PREVIOUS( FrMsg );

        if ( promoted && previous )
        {
            BOOST_TEST_MESSAGE( Leader << "field: alarm" );
            BOOST_CHECK(
                promoted->GetAlarm( ).compare( previous->GetAlarm( ) ) == 0 )

                ;
            BOOST_TEST_MESSAGE( Leader << "field: message" );
            BOOST_CHECK( promoted->GetMessage( ).compare(
                             previous->GetMessage( ) ) == 0 )

                ;
            BOOST_TEST_MESSAGE( Leader << "field: nColumn" );
            BOOST_CHECK( promoted->GetSeverity( ) == previous->GetSeverity( ) );
            BOOST_TEST_MESSAGE( Leader << "field: GTime" );
            BOOST_CHECK( promoted->GetGTime( ) == GPSTime( ) )

                ;
        }
        else
        {
            BOOST_TEST_MESSAGE( Leader << "Validation of field elements" );
            BOOST_CHECK( false )

                ;
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrProcData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER_CONST(
                    promoted, GetType, FrProcData::TIME_SERIES, type );
                CHECK_NUMBER_CONST( promoted,
                                    GetSubType,
                                    FrProcData::UNKNOWN_SUB_TYPE,
                                    subType );
                // :TODO: Need to correct the test to allow for rounding errors
                // CHECK_NUMBER_CONST( promoted, GetTimeOffset,
                // previous->GetTimeOffset( ).GetTime( ), timeOffset );
                CHECK_NUMBER_CONST(
                    promoted, GetTRange, FR_PROC_DATA_DEFAULT_TRANGE, tRange );
                CHECK_NUMBER( previous, promoted, GetFShift, fShift );
                CHECK_NUMBER_CONST(
                    promoted, GetPhase, FR_PROC_DATA_DEFAULT_PHASE, phase );
                CHECK_NUMBER_CONST(
                    promoted, GetFRange, FR_PROC_DATA_DEFAULT_FRANGE, fRange );
                CHECK_NUMBER_CONST(
                    promoted, GetBW, FR_PROC_DATA_DEFAULT_BW, BW );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        //-------------------------------------------------------------------
        // FrRawData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrRawData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSerData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_NUMBER( previous, promoted, GetTime, time );
                CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
                CHECK_STRING( previous, promoted, GetData, data );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSimData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
                CHECK_NUMBER_CONST( promoted,
                                    GetTimeOffset,
                                    FrSimData::timeoffset_type(
                                        FrSimData::DEFAULT_TIME_OFFSET ),
                                    timeOffset );
                CHECK_NUMBER_CONST(
                    promoted,
                    GetFShift,
                    FrSimData::fshift_type( FrSimData::DEFAULT_FSHIFT ),
                    fShift );
                CHECK_NUMBER_CONST(
                    promoted,
                    GetPhase,
                    FrSimData::phase_type( FrSimData::DEFAULT_PHASE ),
                    phase );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSimEvent );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING( previous, promoted, GetInputs, inputs );
                CHECK_NUMBER( previous, promoted, GetGTime, GTime );
                CHECK_NUMBER( previous, promoted, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, promoted, GetTimeAfter, timeAfter );
                CHECK_NUMBER( previous, promoted, GetAmplitude, amplitude );
                CHECK_NUMBER_CONST( promoted, GetParam( ).size, 0, nParam );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrStatData );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING(
                    previous, promoted, GetRepresentation, representation );
                CHECK_NUMBER( previous, promoted, GetTimeStart, timeStart );
                CHECK_NUMBER( previous, promoted, GetTimeEnd, timeEnd );
                CHECK_NUMBER( previous, promoted, GetVersion, version );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSummary );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING( previous, promoted, GetTest, test );
                BOOST_TEST_MESSAGE( Leader << "field: GTime" );
                BOOST_CHECK( promoted->GetGTime( ) == GPSTime( ) )

                    ;
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_TABLE:
        //-------------------------------------------------------------------
        // FrTable
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrTable );

            //---------------------------------------------------------------
            // Verify the elements
            //---------------------------------------------------------------
            if ( promoted && previous )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER( previous, promoted, GetNColumn, nColumn );
                CHECK_NUMBER( previous, promoted, GetNRow, nRow );
            }
            else
            {
                BOOST_TEST_MESSAGE( Leader << "Validation of field elements" );
                BOOST_CHECK( false );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_VECT:
        //-------------------------------------------------------------------
        // FrVect
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrVect );

            if ( previous && promoted )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_NUMBER( previous, promoted, GetCompress, compress );
                CHECK_NUMBER( previous, promoted, GetType, type );
                CHECK_NUMBER( previous, promoted, GetNData, nData );
                CHECK_NUMBER( previous, promoted, GetNBytes, nBytes );
                CHECK_NUMBER( previous, promoted, GetNDim, nDim );
                CHECK_NUMBER( previous, promoted, GetDim( 0 ).GetNx, nx );
                CHECK_NUMBER( previous, promoted, GetDim( 0 ).GetDx, dx );
                CHECK_NUMBER(
                    previous, promoted, GetDim( 0 ).GetStartX, startX );
                CHECK_STRING( previous, promoted, GetDim( 0 ).GetUnitX, unitX );
                CHECK_STRING( previous, promoted, GetUnitY, unitY );
            }
        }
        break;
    default:
    {
        std::ostringstream msg;

        msg << "verify_upconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
}

//=======================================================================

#undef USING
#undef PREVIOUS_TEMPLATE_SPEC
#undef PREVIOUS_NAMESPACE
#undef NAMESPACE
#undef TEMPLATE_SPEC

#endif /* FRAME_CPP__TEST__FR_STRUCT_6_TCC */
