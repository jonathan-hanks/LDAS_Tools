//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__TOC_4_TCC
#define FRAME_CPP__TEST__TOC_4_TCC

#include "framecpp/Version4/FrTOC.hh"

namespace toc_4
{
  using FrameCPP::Version_4::FrTOC;

  //---------------------------------------------------------------------
  // Helpers for Static data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCStatData::name_type,
		     FrTOC::FrTOCStatData::stat_type > stat_data_type;

  inline void
  stat_name( std::ostream& Stream, const stat_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline void
  stat_detector( std::ostream& Stream, const stat_data_type& Data )
  {
    Stream << Data.second.detector;
  }
  
  inline void
  stat_n_stat_instance( std::ostream& Stream, const stat_data_type& Data )
  {
    Stream << Data.second.stat_instances.size( );
  }
  
  inline void
  stat_t_start( std::ostream& Stream, const stat_data_type& Data )
  {
    for ( FrTOC::stat_instance_container_type::const_iterator
	    first = Data.second.stat_instances.begin( ),
	    cur = first,
	    last = Data.second.stat_instances.end( );
	  cur != last;
	  ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->tStart;
    }
  }
  
  inline void
  stat_t_end( std::ostream& Stream, const stat_data_type& Data )
  {
    for ( FrTOC::stat_instance_container_type::const_iterator
	    first = Data.second.stat_instances.begin( ),
	    cur = first,
	    last = Data.second.stat_instances.end( );
	  cur != last;
	  ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->tEnd;
    }
  }
  
  inline void
  stat_version( std::ostream& Stream, const stat_data_type& Data )
  {
    for ( FrTOC::stat_instance_container_type::const_iterator
	    first = Data.second.stat_instances.begin( ),
	    cur = first,
	    last = Data.second.stat_instances.end( );
	  cur != last;
	  ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->version;
    }
  }
  
  inline void
  stat_position( std::ostream& Stream, const stat_data_type& Data )
  {
    for ( FrTOC::stat_instance_container_type::const_iterator
	    first = Data.second.stat_instances.begin( ),
	    cur = first,
	    last = Data.second.stat_instances.end( );
	  cur != last;
	  ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << cur->positionStat;
    }
  }
  
  //---------------------------------------------------------------------
  // Helpers for ADC data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCAdcData::name_type,
		     FrTOC::FrTOCAdcData::adc_info_type > adc_data_type;

  inline
  void
  adc_name( std::ostream& Stream, const adc_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline
  void
  adc_channelID( std::ostream& Stream, const adc_data_type& Data )
  {
    Stream << Data.second.channelID;
  }
  
  inline
  void
  adc_groupID( std::ostream& Stream, const adc_data_type& Data )
  {
    Stream << Data.second.groupID;
  }  

  //---------------------------------------------------------------------
  // Helpers for Proc data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCProcData::name_type,
		     FrTOC::FrTOCProcData::proc_info_type > proc_data_type;

  inline
  void
  proc_name( std::ostream& Stream, const proc_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline
  void proc_position( std::ostream& Stream, const proc_data_type& Data )
  {
    for( std::vector< FrTOC::FrTOCAdcData::position_type >::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << *cur;
    }
  }
  
  //---------------------------------------------------------------------
  // Helpers for Sim data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCSimData::name_type,
		     FrTOC::FrTOCSimData::sim_info_type > sim_data_type;

  inline
  void
  sim_name( std::ostream& Stream, const sim_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline
  void sim_position( std::ostream& Stream, const sim_data_type& Data )
  {
    for( std::vector< FrTOC::FrTOCAdcData::position_type >::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << *cur;
    }
  }
  
  //---------------------------------------------------------------------
  // Helpers for Ser data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCSerData::name_type,
		     FrTOC::FrTOCSerData::ser_info_type > ser_data_type;

  inline
  void
  ser_name( std::ostream& Stream, const ser_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline
  void ser_position( std::ostream& Stream, const ser_data_type& Data )
  {
    for( std::vector< FrTOC::FrTOCAdcData::position_type >::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << *cur;
    }
  }
  
  //---------------------------------------------------------------------
  // Helpers for Summary data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCSummary::name_type,
		     FrTOC::FrTOCSummary::summary_info_type >
  summary_data_type;

  inline
  void
  summary_name( std::ostream& Stream, const summary_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline
  void summary_position( std::ostream& Stream, const summary_data_type& Data )
  {
    for( std::vector< FrTOC::FrTOCAdcData::position_type >::const_iterator
	   first = Data.second.begin( ),
	   cur = first,
	   last = Data.second.end( );
	 cur != last;
	 ++cur )
    {
      if ( cur != first )
      {
	Stream << " ";
      }
      Stream << *cur;
    }
  }
  
  //---------------------------------------------------------------------
  // Helpers for Event data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCTrigData::name_type,
		     FrTOC::FrTOCTrigData::trig_data_type >
  event_data_type;

  inline void
  event_name( std::ostream& Stream, const event_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline void
  event_gtime_s( std::ostream& Stream, const event_data_type& Data )
  {
    Stream << Data.second.GTime.GetSeconds( );
  }

  inline void
  event_gtime_n( std::ostream& Stream, const event_data_type& Data )
  {
    Stream << Data.second.GTime.GetNanoseconds( );
  }

  inline void
  event_position( std::ostream& Stream, const event_data_type& Data )
  {
    Stream << Data.second.positionTrigData;
  }

#if 1
  //---------------------------------------------------------------------
  // Helpers for SimEvent data
  //---------------------------------------------------------------------
  typedef std::pair< const FrTOC::FrTOCSimEvent::name_type,
		     FrTOC::FrTOCSimEvent::trig_data_type >
  sim_event_data_type;

  inline void
  sim_event_name( std::ostream& Stream, const sim_event_data_type& Data )
  {
    Stream << Data.first;
  }
  
  inline void
  sim_event_gtime_s( std::ostream& Stream, const sim_event_data_type& Data )
  {
    Stream << Data.second.GTime.GetSeconds( );
  }

  inline void
  sim_event_gtime_n( std::ostream& Stream, const sim_event_data_type& Data )
  {
    Stream << Data.second.GTime.GetNanoseconds( );
  }

  inline void
  sim_event_position( std::ostream& Stream, const sim_event_data_type& Data )
  {
    Stream << Data.second.positionSimEvent;
  }
#endif /* 0 */
}

inline void
dump( const FrameCPP::Version_4::FrTOC* TOC )
{
  //---------------------------------------------------------------------
  // Cut down on typing
  //---------------------------------------------------------------------
  using FrameCPP::Version_4::FrTOC;
  using namespace toc_4;

  //---------------------------------------------------------------------
  // Print the information in a human readable form
  //---------------------------------------------------------------------
  std::cout << "ULeapS: " << TOC->GetULeapS( ) << std::endl
	    << "localTime: " << TOC->GetLocalTime( ) << std::endl
	    << "nFrame: " << TOC->GetNFrame( ) << std::endl
    ;
  dump_toc_array( "GTimeS", TOC->GetGTimeS( ) );
  dump_toc_array( "GTimeN", TOC->GetGTimeN( ) );
  dump_toc_array( "dt", TOC->GetDt( ) );
  dump_toc_array( "runs", TOC->GetRuns( ) );
  dump_toc_array( "frame", TOC->GetFrame( ) );
  dump_toc_array( "positionH", TOC->GetPositionH( ) );
  dump_toc_array( "nFirstADC", TOC->GetNFirstADC( ) );
  dump_toc_array( "nFirstSer", TOC->GetNFirstSer( ) );
  dump_toc_array( "nFirstTable", TOC->GetNFirstTable( ) );
  dump_toc_array( "nFirstMsg", TOC->GetNFirstMsg( ) );
  //---------------------------------------------------------------------
  // Sh data
  //---------------------------------------------------------------------
  std::cout << "nSH: " << TOC->GetSHid( ).size( ) << std::endl;
  dump_toc_array( "SHid", TOC->GetSHid( ) );
  dump_toc_array( "SHname", TOC->GetSHname( ) );
  //---------------------------------------------------------------------
  // FrStatData
  //---------------------------------------------------------------------
  {
    const FrTOC::stat_container_type& stat( TOC->GetStat( ) );
    
    std::cout << "nStatType: " << stat.size( ) << std::endl;
    dump_toc_internal_data( "nameStat", stat, &stat_name );
    dump_toc_internal_data( "detector", stat, &stat_detector );
    dump_toc_internal_data( "nStatInstance", stat, &stat_n_stat_instance );
    dump_toc_internal_data( "tStart", stat, &stat_t_start );
    dump_toc_internal_data( "tEnd", stat, &stat_t_end );
    dump_toc_internal_data( "version", stat, &stat_version );
    dump_toc_internal_data( "positionStat", stat, &stat_position );
  }
  //---------------------------------------------------------------------
  // FrAdcData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapADC_type& adc( TOC->GetADC( ) );
    std::cout << "nADC: " << adc.size( ) << std::endl;
    dump_toc_internal_data( "name", adc, &adc_name );
    dump_toc_internal_data( "channelId", adc, &adc_channelID );
    dump_toc_internal_data( "groupId", adc, &adc_groupID );
  }
  //---------------------------------------------------------------------
  // FrProcData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapProc_type& data( TOC->GetProc( ) );

    std::cout << "nProc: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameProc", data, &proc_name );
    dump_toc_internal_data( "positionProc", data, &proc_position );
  }
  //---------------------------------------------------------------------
  // FrSimData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapSim_type& data( TOC->GetSim( ) );

    std::cout << "nSim: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSim", data, &sim_name );
    dump_toc_internal_data( "positionSim", data, &sim_position );
  }
  //---------------------------------------------------------------------
  // FrSerData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapSer_type& data( TOC->GetSer( ) );

    std::cout << "nSer: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSer", data, &ser_name );
    dump_toc_internal_data( "positionSer", data, &ser_position );
  }
  //---------------------------------------------------------------------
  // FrSummary
  //---------------------------------------------------------------------
  {
    const FrTOC::MapSummary_type& data( TOC->GetSummary( ) );

    std::cout << "nSummary: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSum", data, &summary_name );
    dump_toc_internal_data( "positionSum", data, &summary_position );
  }
  //---------------------------------------------------------------------
  // FrTrigData
  //---------------------------------------------------------------------
  {
    const FrTOC::MapTrig_type& data( TOC->GetTrigData( ) );

    std::cout << "nTrig: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameTrig", data, &event_name );
    dump_toc_internal_data( "GTimeSTrig", data, &event_gtime_s );
    dump_toc_internal_data( "GTimeNTrig", data, &event_gtime_n );
    dump_toc_internal_data( "positionTrig", data, &event_position );
  }
#if 1
  //---------------------------------------------------------------------
  // FrSimEvent
  //---------------------------------------------------------------------
  {
    const FrTOC::MapSimEvt_type& data( TOC->GetSimEvent( ) );

    std::cout << "nSimEventType: " << data.size( ) << std::endl;
    dump_toc_internal_data( "nameSimEvent", data, &sim_event_name );
    dump_toc_internal_data( "GTimeSSimEvent", data, &sim_event_gtime_s );
    dump_toc_internal_data( "GTimeNSimEvent", data, &sim_event_gtime_n );
    dump_toc_internal_data( "positionSimEvent", data, &sim_event_position );
  }
#endif /* 0 */
}

#endif /* FRAME_CPP__TEST__TOC_4_TCC */
