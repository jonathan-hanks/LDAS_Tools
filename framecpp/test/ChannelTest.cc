#define BOOST_TEST_MAIN
#define BOOST_TEST_MODULE ChannelTest

#include <boost/test/included/unit_test.hpp>

#include <random>
#include <string>

#include "framecpp/Common/STRING.hh"

static std::string
random_string( )
{
    std::string str(
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz" );

    std::random_device rd;
    std::mt19937       generator( rd( ) );

    std::shuffle( str.begin( ), str.end( ), generator );

    return ( str );
}

BOOST_AUTO_TEST_CASE( channel_concat_comment_limit_size_128 )
{
    std::string                         comment;
    const FrameCPP::cmn_streamsize_type max_comment_size{ 128 };

    for ( int counter = 0; counter < 5; ++counter )
    {
        FrameCPP::Common::AppendComment(
            comment, random_string( ), max_comment_size );
    }

    BOOST_CHECK( comment.size( ) == ( max_comment_size - 1 ) );
}

BOOST_AUTO_TEST_CASE( no_duplicate_comment )
{
    const std::string source{ "This is a comment not to be duplicated" };
    std::string       comment{ source };
    const FrameCPP::cmn_streamsize_type max_comment_size{ source.size( ) };

    FrameCPP::Common::AppendComment( comment, source );

    BOOST_CHECK( comment.size( ) == ( max_comment_size ) );

    FrameCPP::Common::AppendComment( comment, "New info" );

    BOOST_CHECK( comment.size( ) > ( max_comment_size ) );
}
