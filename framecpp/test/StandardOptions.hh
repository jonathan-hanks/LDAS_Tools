//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#ifndef FRAMECPP__TEST__STANDARD_OPTIONS_HH
#define FRAMECPP__TEST__STANDARD_OPTIONS_HH

#include <sstream>

#include "ldastoolsal/CommandLineOptions.hh"

namespace
{
    enum
    {
        STANDARD_OPTION_BLOCK_SIZE,
        STANDARD_OPTION_HELP,
        STANDARD_OPTION_SILENT_DATA,
        STANDARD_OPTION_TOC_OPT,
        STANDARD_OPTION_USE_MMAP
    };

    template < int >
    void StandardOption( LDASTools::AL::CommandLineOptions::OptionSet& Options,
                         int                                           Id );

    template < int >
    void StandardOption( LDASTools::AL::CommandLineOptions::OptionSet& Options,
                         int                                           Id,
                         size_t Default );

    template < int >
    void StandardOption( LDASTools::AL::CommandLineOptions::OptionSet& Options,
                         int                                           Id,
                         bool Default );
} // namespace

namespace
{
    typedef LDASTools::AL::CommandLineOptions::OptionSet OptionSet;

    template <>
    inline void
    StandardOption< STANDARD_OPTION_HELP >( OptionSet& Options, int Id )
    {
        Options.Add( OptionSet::element_type( Id,
                                              "help",
                                              OptionSet::element_type::ARG_NONE,
                                              "Display this message" ) );
    }

    template <>
    inline void
    StandardOption< STANDARD_OPTION_BLOCK_SIZE >( OptionSet& Options,
                                                  int        Id,
                                                  size_t     Default )
    {
        std::ostringstream desc;

        desc << "Number of 1k blocks to use for io."
                " (Default: "
             << Default << " )";
        Options.Add(
            OptionSet::element_type( Id,
                                     "bs",
                                     OptionSet::element_type::ARG_REQUIRED,
                                     desc.str( ),
                                     "<block_size>" ) );
    }

    template <>
    inline void
    StandardOption< STANDARD_OPTION_SILENT_DATA >( OptionSet& Options,
                                                   int        Id,
                                                   bool       Default )
    {
        std::ostringstream desc;

        desc
            << "Supress the output of the data element of the FrVect structure."
               " (Default: "
            << ( ( Default ) ? "silent" : "verbose" ) << " )";

        Options.Add( OptionSet::element_type( Id,
                                              "silent-data",
                                              OptionSet::element_type::ARG_NONE,
                                              desc.str( ) ) );
    }

    template <>
    inline void
    StandardOption< STANDARD_OPTION_TOC_OPT >( OptionSet& Options,
                                               int        Id,
                                               bool       Default )
    {
        std::ostringstream desc;

        desc << "Use table of contents optimization."
                " (Default: "
             << ( ( Default ) ? "enabled" : "disabled" ) << " )";
        Options.Add( OptionSet::element_type(
            Id, "toc-opt", OptionSet::element_type::ARG_NONE, desc.str( ) ) );
    }

    template <>
    inline void
    StandardOption< STANDARD_OPTION_USE_MMAP >( OptionSet& Options,
                                                int        Id,
                                                bool       Default )
    {
        std::ostringstream desc;

        desc << "Use memory mapped io."
                " (Default: "
             << ( ( Default ) ? "enabled" : "disabled" ) << " )";

        Options.Add( OptionSet::element_type(
            Id, "use-mmap", OptionSet::element_type::ARG_NONE, desc.str( ) ) );
    }
} // namespace

#endif /* FRAMECPP__TEST__STANDARD_OPTIONS_HH */
