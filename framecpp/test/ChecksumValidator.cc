//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <unistd.h>

#include <sstream>

#define BOOST_TEST_MAIN
#include <boost/test/included/unit_test.hpp>

bool
test_helper( const char*  TestDir,
             const char** Files,
             const int    FileCount,
             bool         Expected )
{
    bool retval = true;
    if ( access( TestDir, X_OK ) != 0 )
    {
        /* Skip the test */
        return retval;
    }

    const char** cur_file;
    const char** last_file;

    for ( cur_file = Files, last_file = &( Files[ FileCount ] );
          cur_file != last_file;
          ++cur_file )
    {
        {
            std::ostringstream cmd_base;
            std::ostringstream cmd;

            cmd_base << "./framecpp_checksum"
                     << " --checksum-frame"
                     << " --checksum-file"
                     << " --checksum-structure"
                     << " " << TestDir << "/" << *cur_file;
            cmd << cmd_base.str( ) << " > /dev/null 2>&1"
                << " < /dev/null";

            int ret = system( cmd.str( ).c_str( ) );
            BOOST_CHECK( ( ret == 0 ) == Expected );
        }
        {
            std::ostringstream cmd_base;
            std::ostringstream cmd;

            cmd_base << "./framecpp_verify"
                     << " --verbose"
                     << " " << TestDir << "/" << *cur_file;
            cmd << cmd_base.str( ) << " > /dev/null 2>&1"
                << " < /dev/null";

            int ret = system( cmd.str( ).c_str( ) );
            BOOST_CHECK( ( ret == 0 ) == Expected );
        }
    }
    return retval;
}

void
test_bad_crc( )
{
    const char* files[] = { "L-R-991431920-16.gwf.bad",
                            "L-R-991432112-16.gwf.bad",
                            "L-R-991434112-16.gwf.bad" };
    BOOST_CHECK( test_helper( "/devscratch/test/frames/bad",
                              files,
                              sizeof( files ) / sizeof( *files ),
                              false ) );
}

void
test_S6( )
{
    const char* files[] = { "H-R-971621600-32.gwf",
                            "H-R-971621632-32.gwf",
                            "H-R-971621664-32.gwf",
                            "H-R-971621696-32.gwf" };
    BOOST_CHECK( test_helper( "/devscratch/frames/Samples/S6/L0/LHO/H-R-9716",
                              files,
                              sizeof( files ) / sizeof( *files ),
                              true ) );
}

BOOST_AUTO_TEST_CASE( CRC )
{
    test_bad_crc( );
}

BOOST_AUTO_TEST_CASE( S6 )
{
    test_S6( );
}
