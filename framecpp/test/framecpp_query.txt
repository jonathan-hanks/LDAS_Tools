/**
  \page CommandFramecppQuery framecpp_query
  <p>
  <b>Usage:</b> framecpp_query [options] <em>file</em> [<em>file</em> ...]
  <p>
    This command will dump the table of contents for each file specified on the command line.

  <TABLE>
    <TR>
      <TH><CENTER>Option</CENTER></TH>
      <TH><CENTER>Description</CENTER></TH>
    </TR>
    <TR>
      <TD>--silent-data</TD>
      <TD>
        Supress the output of the data element of the FrVect structure. (Default: verbose )
      </TD>
    </TR>
    <TR>
      <TD>--scientific-notation</TD>
      <TD>
        Display real and complex elements in scientific notation. (Default: disabled )
      </TD>
    </TR>
    <TR>
      <TD>--proc</TD>
      <TD>
        Display the specified FrProc channel.
      </TD>
    </TR>
    <TR>
      <TD>--list</TD>
      <TD>
        Display thhe channel names of the specified type.
      </TD>
    </TR>
    <TR>
      <TD>--help</TD>
      <TD>
        Display this message
      </TD>
    </TR>
    <TR>
      <TD>--header</TD>
      <TD>
        Display the frame header information.
      </TD>
    </TR>
    <TR>
      <TD>--frame-range</TD>
      <TD>
        Specify the range of frames to use in queries.
      </TD>
    </TR>
    <TR>
      <TD>--dump</TD>
      <TD>
        Dump the contents of the specified channel.
      </TD>
    </TR>
    <TR>
      <TD>--adc</TD>
      <TD>
        Display the specified FrADC channel.
      </TD>
    </TR>
  </TABLE>
*/
