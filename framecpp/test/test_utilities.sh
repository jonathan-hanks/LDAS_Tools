#! /bin/sh
#
# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#
#------------------------------------------------------------------------
# This script tests the utilities
#------------------------------------------------------------------------

#------------------------------------------------------------------------
# setup
#------------------------------------------------------------------------

status=0

#------------------------------------------------------------------------

test_conditional_print()
{
    case "${TEST_VERBOSE_MODE}" in
    ""|"no"|"false")
        ;;
    *)
	echo "$*"
        ;;
    esac
}

test_check()
{
    stat="$1"
    shift
    msg="$*"
    tag=""
    case "${stat}" in
    "0")
        tag="PASS"
        ;;
    *)
        tag="FAIL"
	if test ${status} -eq 0
        then
	    status=1
        fi
        ;;
    esac
    test_conditional_print "-- ${tag}: ${msg}"
}

#------------------------------------------------------------------------

test_framecpp_dump_objects()
{
    cmd=framecpp_dump_objects

    #--------------------------------------------------------------------
    # PR3439
    #--------------------------------------------------------------------
    ./$cmd --silent-data --verbose ${cmd} > /dev/null 2>&1
    test $? != 0
    test_check "$?" "${cmd}: PR3439 - processing non-frame files"
    #--------------------------------------------------------------------
    # Ensure the program completes within a reasonable amount of time
    #--------------------------------------------------------------------
    for gwf in *.gwf
    do
        ./${cmd} --silent-data ${gwf} > /dev/null 2>&1
        test_check "$?" "${cmd}: completes within alotted time"
      break
    done
}

#------------------------------------------------------------------------

test_time_trial()
{
    gwf="Z-R_std_test_frame_ver8-600000000-1.gwf"
    time ./framecpp_verify \
	--checksum-file-only \
	${gwf}
    time ./framecpp_verify_checksums \
	--no-checksum-structure \
	--no-checksum-frame \
	${gwf}
    # test_check "$?" "${cmd}: completes within alotted time"
}

#------------------------------------------------------------------------

echo ${BIN_PROGRAMS}
for pgm in ${BIN_PROGRAMS}
do
  case $pgm in
  framecpp_dump_objects)
      test_${pgm}
      ;;
  esac
done

test_time_trial

#------------------------------------------------------------------------
# Report the result of testing
#------------------------------------------------------------------------
exit ${status}