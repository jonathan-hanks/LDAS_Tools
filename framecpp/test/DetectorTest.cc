#define BOOST_TEST_MAIN
#define BOOST_TEST_MODULE DetectorTest

#include <boost/test/included/unit_test.hpp>

#include "framecpp/FrDetector.hh"

BOOST_AUTO_TEST_CASE( detector_instantiation )
{
    using namespace FrameCPP;

    // --------------------------------------------------------------------
    /// Ensure that each detector can be instantiated and is unique
    // --------------------------------------------------------------------
    BOOST_CHECK(
        ( FrDetector::GetDetector( FrDetector::DQO_GEO_600 )->GetName( ) ==
          "GEO_600" ) );
    BOOST_CHECK(
        ( FrDetector::GetDetector( FrDetector::DQO_LHO_4K )->GetName( ) ==
          "LHO_4k" ) );
    BOOST_CHECK(
        ( FrDetector::GetDetector( FrDetector::DQO_KAGRA )->GetName( ) ==
          "KAGRA" ) );
    BOOST_CHECK(
        ( FrDetector::GetDetector( FrDetector::DQO_LLO_4K )->GetName( ) ==
          "LLO_4k" ) );
    BOOST_CHECK(
        ( FrDetector::GetDetector( FrDetector::DQO_TAMA_300 )->GetName( ) ==
          "TAMA_300" ) );
    BOOST_CHECK(
        ( FrDetector::GetDetector( FrDetector::DQO_VIRGO )->GetName( ) ==
          "Virgo" ) );
}

BOOST_AUTO_TEST_CASE( detector_constructor )
{
    using namespace FrameCPP;

    const std::string                   name( "Test" );
    const std::string                   prefix( "Z1" );
    const FrDetector::longitude_type    longitude = 1.2;
    const FrDetector::latitude_type     latitude = 1.4;
    const FrDetector::elevation_type    elevation = 1.6;
    const FrDetector::armXazimuth_type  armXazimuth = 2.2;
    const FrDetector::armYazimuth_type  armYazimuth = 2.4;
    const FrDetector::armXaltitude_type armXaltitude = 3.2;
    const FrDetector::armYaltitude_type armYaltitude = 3.4;
    const FrDetector::armXmidpoint_type armXmidpoint = 4.2;
    const FrDetector::armYmidpoint_type armYmidpoint = 4.4;

    FrDetector d( name,
                  prefix.c_str( ),
                  longitude,
                  latitude,
                  elevation,
                  armXazimuth,
                  armYazimuth,
                  armXaltitude,
                  armYaltitude,
                  armXmidpoint,
                  armYmidpoint );

    BOOST_CHECK( d.GetName( ) == name );
    BOOST_CHECK( ( ( d.GetPrefix( )[ 0 ] == prefix[ 0 ] ) &&
                   ( d.GetPrefix( )[ 1 ] == prefix[ 1 ] ) ) );
    BOOST_CHECK( d.GetLongitude( ) == longitude );
    BOOST_CHECK( d.GetLatitude( ) == latitude );
    BOOST_CHECK( d.GetElevation( ) == elevation );
    BOOST_CHECK( d.GetArmXazimuth( ) == armXazimuth );
    BOOST_CHECK( d.GetArmYazimuth( ) == armYazimuth );
    BOOST_CHECK( d.GetArmXaltitude( ) == armXaltitude );
    BOOST_CHECK( d.GetArmYaltitude( ) == armYaltitude );
    BOOST_CHECK( d.GetArmXmidpoint( ) == armXmidpoint );
    BOOST_CHECK( d.GetArmYmidpoint( ) == armYmidpoint );
}
