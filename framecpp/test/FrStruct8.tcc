// -*- mode: c++; -*-
//
// LDASTools frameCPP - A library implementing the LIGO/Virgo frame
// specification
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools frameCPP is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools frameCPP is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* -*- mode: C++; c-basic-offset: 2; -*- */
#ifndef FRAME_CPP__TEST__FR_STRUCT_8_TCC
#define FRAME_CPP__TEST__FR_STRUCT_8_TCC

#include "framecpp/Version8/FrameH.hh"
#include "framecpp/Version8/FrAdcData.hh"
#include "framecpp/Version8/FrDetector.hh"
#include "framecpp/Version8/FrHistory.hh"
#include "framecpp/Version8/FrMsg.hh"
#include "framecpp/Version8/FrProcData.hh"
#include "framecpp/Version8/FrRawData.hh"
#include "framecpp/Version8/FrSerData.hh"
#include "framecpp/Version8/FrSimData.hh"
#include "framecpp/Version8/FrSimEvent.hh"
#include "framecpp/Version8/FrStatData.hh"
#include "framecpp/Version8/FrSummary.hh"
#include "framecpp/Version8/FrTable.hh"
#include "framecpp/Version8/FrVect.hh"

#define TEMPLATE_SPEC 8
#define NAMESPACE FrameCPP::Version_8
#define PREVIOUS_TEMPLATE_SPEC 7
#define PREVIOUS_NAMESPACE FrameCPP::Version_7
#define USING( ) using namespace NAMESPACE

template <>
mk_frame_object_ret_type
mk_frame_object< TEMPLATE_SPEC >( FrameObjectTypes Type )
{
    USING( );

    using FrameCPP::Common::FrameSpec;

    mk_frame_object_ret_type retval;

    switch ( Type )
    {
    case FrameSpec::Info::FSI_FRAME_H:
        retval.reset(
            new FrameH( "frame_h", 1, 8, GPSTime( 10, 20 ), 1, 3.0, 2 ) );
        break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        retval.reset( new FrAdcData( "fr_adc_data",
                                     3,
                                     2,
                                     8,
                                     1024,
                                     2.0,
                                     1.0,
                                     "meters",
                                     30.,
                                     10.1,
                                     4,
                                     40.0 ) );
        static_cast< FrAdcData* >( retval.get( ) )
            ->AppendComment( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
    {
        static const char pre[ 2 ] = { 'a', 'b' };
        retval.reset( new FrDetector( "fr_detector_name",
                                      pre,
                                      1.1,
                                      2.2,
                                      200.3,
                                      3.3,
                                      4.4,
                                      5.5,
                                      6.6,
                                      7.7,
                                      8.8,
                                      1800 ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            FrEvent::ParamList_type params;

            params.push_back( FrEvent::Param_type( "param1", 64.0 ) );

            retval.reset( new FrEvent( "fr_event",
                                       "fr_event_comment",
                                       "fr_event_inputs",
                                       GPSTime( 10, 20000000 ),
                                       1024.0,
                                       2048.0,
                                       1,
                                       16.0,
                                       32.0,
                                       "fr_event_statistics",
                                       params ) );
        }

        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
    {
        std::unique_ptr< FrHistory > fr_history(
            new FrHistory( "fr_history_name", 10, "fr_hsitory_comment" ) );
        retval.reset( fr_history.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_MSG:
    {
        std::unique_ptr< FrMsg > fr_msg(
            new FrMsg( "alarm", "message", 10, GPSTime::NowGPSTime( ) ) );
        retval.reset( fr_msg.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        retval.reset( new FrProcData( "fr_proc_data",
                                      "fr_proc_data_comment",
                                      1,
                                      0,
                                      16.0,
                                      2048.,
                                      1024.0,
                                      4.0,
                                      4096.0,
                                      0 ) );

        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        retval.reset( new FrRawData( "fr_raw_data" ) );
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        retval.reset(
            new FrSerData( "fr_ser_data", GPSTime( 10, 20 ), 1024. ) );

        static_cast< FrSerData* >( retval.get( ) )->SetData( "test data" );
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        retval.reset( new FrSimData(
            "fr_sim_data", "fr_sim_data_comment", 1024., 2048., 4096. ) );
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            FrSimEvent::ParamList_type params;

            params.push_back( FrSimEvent::Param_type( "param1", 64.0 ) );

            retval.reset( new FrSimEvent( "fr_event",
                                          "fr_event_comment",
                                          "fr_event_inputs",
                                          GPSTime( 10, 20000000 ),
                                          1024.0,
                                          2048.0,
                                          16.0,
                                          params ) );
        }

        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        retval.reset( new FrStatData( "fr_stat_data",
                                      "fr_stat_data_comment",
                                      "fr_stat_data_representation",
                                      1,
                                      2,
                                      4 ) );
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        retval.reset( new FrSummary( "fr_summary",
                                     "fr_summary_comment",
                                     "fr_summary_test",
                                     GPSTime( 10, 20 ) ) );

        break;
    case FrameSpec::Info::FSI_FR_TABLE:
    {
        std::unique_ptr< FrTable > fr_table( new FrTable( "testTable", 0 ) );
        fr_table->AppendComment( "hello world" );
        retval.reset( fr_table.release( ) );
    }
    break;
    case FrameSpec::Info::FSI_FR_VECT:
    {
        typedef INT_4U vect_data_type;

        static const vect_data_type START = 0;
        static const vect_data_type STOP = 512;
        static const vect_data_type INC = 2;
        static const int            SAMPLES = ( ( STOP - START ) / INC );

        testing::Ramp::GenSawToothFalling< vect_data_type > data_generator(
                                                                           START, STOP, INC );

        Dimension      dim( SAMPLES );
        vect_data_type data[ SAMPLES ];

        std::generate( &( data[ 0 ] ), &( data[ SAMPLES ] ), data_generator );

        retval.reset( new FrVect( "fr_vect", 1, &dim, data, "fr_vect_unitY" ) );
        auto frvect = boost::dynamic_pointer_cast< FrVect >( retval );
        frvect->CompressData( FrVect::DIFF_GZIP, 3 );
    }
    break;
    default:
    {
        std::ostringstream msg;

        msg << "mk_frame_obj<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << Type;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
    return retval;
}

//=======================================================================

template <>
void
verify_downconvert< TEMPLATE_SPEC >( frame_object_type  FrameObj,
                                     const std::string& Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }

    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
        //-------------------------------------------------------------------
        // FrameH
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrameH );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetRun, run );
                CHECK_NUMBER( previous, current, GetFrame, frame );
                CHECK_NUMBER( previous, current, GetDataQuality, dataQuality );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER( previous, current, GetULeapS, ULeapS );
                CHECK_NUMBER( previous, current, GetDt, dt );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        //-------------------------------------------------------------------
        // FrAdcData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrAdcData );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER(
                    previous, current, GetChannelGroup, channelGroup );
                CHECK_NUMBER(
                    previous, current, GetChannelNumber, channelNumber );
                CHECK_NUMBER( previous, current, GetNBits, nBits );
                CHECK_NUMBER( previous, current, GetBias, bias );
                CHECK_NUMBER( previous, current, GetSlope, slope );
                CHECK_STRING( previous, current, GetUnits, units );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_NUMBER( previous, current, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, current, GetFShift, fShift );
                CHECK_NUMBER( previous, current, GetPhase, phase );
                CHECK_NUMBER( previous, current, GetDataValid, dataValid );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
        //-------------------------------------------------------------------
        // FrDetector
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrDetector );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                // CHECK_STRING( previous, current, GetPrefix, prefix );
                CHECK_NUMBER( previous, current, GetLongitude, longitude );
                CHECK_NUMBER( previous, current, GetLatitude, latitude );
                CHECK_NUMBER( previous, current, GetElevation, elevation );
                CHECK_NUMBER( previous, current, GetArmXazimuth, armXazimuth );
                CHECK_NUMBER( previous, current, GetArmYazimuth, armYazimuth );
                CHECK_NUMBER(
                    previous, current, GetArmXaltitude, armXaltitude );
                CHECK_NUMBER(
                    previous, current, GetArmYaltitude, armYaltitude );
                CHECK_NUMBER(
                    previous, current, GetArmXmidpoint, armXmidpoint );
                CHECK_NUMBER(
                    previous, current, GetArmYmidpoint, armYmidpoint );
                CHECK_NUMBER( previous, current, GetLocalTime, localTime );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrEvent );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING( previous, current, GetInputs, inputs );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER( previous, current, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, current, GetTimeAfter, timeAfter );
                CHECK_NUMBER( previous, current, GetEventStatus, eventStatus );
                CHECK_NUMBER( previous, current, GetAmplitude, amplitude );
                CHECK_NUMBER( previous, current, GetProbability, probability );
                CHECK_STRING( previous, current, GetStatistics, statistics );

                bool status = ( previous->GetParam( ).size( ) ==
                                current->GetParam( ).size( ) );
                if ( status )
                {
                    Previous::FrEvent::ParamList_type::const_iterator pre_cur =
                        previous->GetParam( ).begin( );
                    FrEvent::ParamList_type::const_iterator
                        pro_cur = current->GetParam( ).begin( ),
                        pro_last = current->GetParam( ).end( );

                    while ( pro_cur != pro_last )
                    {
                        if ( ( pro_cur->first != pre_cur->first ) ||
                             ( pro_cur->second != pre_cur->second ) )
                        {
                            status = false;
                            break;
                        }
                        ++pre_cur;
                        ++pro_cur;
                    }
                }
                BOOST_TEST_MESSAGE( Leader << "field: param" );
                BOOST_CHECK( status );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
        //-------------------------------------------------------------------
        // FrHistory
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrHistory );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetTime, time );
                CHECK_STRING( previous, current, GetComment, comment );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_MSG:
        //-------------------------------------------------------------------
        // FrMsg
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrMsg );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetAlarm, alarm );
                CHECK_STRING( previous, current, GetMessage, message );
                CHECK_NUMBER( previous, current, GetSeverity, severity );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrProcData );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetType, type );
                CHECK_NUMBER( previous, current, GetSubType, subType );
                CHECK_NUMBER( previous, current, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, current, GetTRange, tRange );
                CHECK_NUMBER( previous, current, GetFShift, fShift );
                CHECK_NUMBER( previous, current, GetPhase, phase );
                CHECK_NUMBER( previous, current, GetFRange, fRange );
                CHECK_NUMBER( previous, current, GetBW, BW );

                bool status = ( previous->GetAuxParam( ).size( ) ==
                                current->GetAuxParam( ).size( ) );
                if ( status )
                {
                    Previous::FrProcData::AuxParamList_type::const_iterator
                        pre_cur = previous->GetAuxParam( ).begin( );
                    FrProcData::AuxParamList_type::const_iterator
                        pro_cur = current->GetAuxParam( ).begin( ),
                        pro_last = current->GetAuxParam( ).end( );

                    while ( pro_cur != pro_last )
                    {
                        if ( ( pro_cur->auxParam != pre_cur->auxParam ) ||
                             ( pro_cur->auxParamName !=
                               pre_cur->auxParamName ) )
                        {
                            status = false;
                            break;
                        }
                        ++pre_cur;
                        ++pro_cur;
                    }
                }
                BOOST_TEST_MESSAGE( Leader << "field: param" );
                BOOST_CHECK( status );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        //-------------------------------------------------------------------
        // FrRawData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrRawData );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSerData );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetTime, time );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_STRING( previous, current, GetData, data );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSimData
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSimData );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetSampleRate, sampleRate );
                CHECK_NUMBER( previous, current, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, current, GetFShift, fShift );
                CHECK_NUMBER( previous, current, GetPhase, phase );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSimEvent );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING( previous, current, GetInputs, inputs );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
                CHECK_NUMBER( previous, current, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, current, GetTimeAfter, timeAfter );
                CHECK_NUMBER( previous, current, GetAmplitude, amplitude );

                bool status = ( previous->GetParam( ).size( ) ==
                                current->GetParam( ).size( ) );
                if ( status )
                {
                    Previous::FrSimEvent::ParamList_type::const_iterator
                        pre_cur = previous->GetParam( ).begin( );
                    FrSimEvent::ParamList_type::const_iterator
                        pro_cur = current->GetParam( ).begin( ),
                        pro_last = current->GetParam( ).end( );

                    while ( pro_cur != pro_last )
                    {
                        if ( ( pro_cur->first != pre_cur->first ) ||
                             ( pro_cur->second != pre_cur->second ) )
                        {
                            status = false;
                            break;
                        }
                        ++pre_cur;
                        ++pro_cur;
                    }
                }
                BOOST_TEST_MESSAGE( Leader << "field: param" );
                BOOST_CHECK( status );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrMsg
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrStatData );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING(
                    previous, current, GetRepresentation, representation );
                CHECK_NUMBER( previous, current, GetTimeStart, timeStart );
                CHECK_NUMBER( previous, current, GetTimeEnd, timeEnd );
                CHECK_NUMBER( previous, current, GetVersion, version );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrSummary
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrSummary );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_STRING( previous, current, GetTest, test );
                CHECK_NUMBER( previous, current, GetGTime, GTime );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_TABLE:
        //-------------------------------------------------------------------
        // FrTable
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrTable );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_STRING( previous, current, GetComment, comment );
                CHECK_NUMBER( previous, current, GetNColumn, nColumn );
                CHECK_NUMBER( previous, current, GetNRow, nRow );

                //---------------------------------------------------------------
                // Do not check columnName field since it is derived from the
                // FrVects of this object.
                //---------------------------------------------------------------
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_VECT:
        //-------------------------------------------------------------------
        // FrVect
        //-------------------------------------------------------------------
        {
            DEMOTE_TO_PREVIOUS( FrVect );

            if ( previous.get( ) && current )
            {
                CHECK_STRING( previous, current, GetName, name );
                CHECK_NUMBER( previous, current, GetCompress, compress );
                CHECK_NUMBER( previous, current, GetType, type );
                CHECK_NUMBER( previous, current, GetNData, nData );
                CHECK_NUMBER( previous, current, GetNBytes, nBytes );
                CHECK_NUMBER( previous, current, GetNDim, nDim );
                CHECK_STRING( previous, current, GetUnitY, unitY );

                //---------------------------------------------------------------
                // Validate the data
                //---------------------------------------------------------------
                FrVect::data_type pdata( previous->GetDataRaw( ) );

                BOOST_TEST_MESSAGE( Leader << "field: data" );
                BOOST_CHECK( std::equal( pdata.get( ),
                                         pdata.get( ) + previous->GetNData( ),
                                         current->GetDataRaw( ).get( ) ) );

                //---------------------------------------------------------------
                // Validate the dim info
                //---------------------------------------------------------------

                bool status( previous->GetNDim( ) == current->GetNDim( ) );

                if ( status )
                {
                    for ( size_t x = current->GetNDim( ); x > 0; )
                    {
                        --x;
                        if ( !( ( previous->GetDim( x ).GetNx( ) ==
                                  current->GetDim( x ).GetNx( ) ) &&
                                ( previous->GetDim( x ).GetDx( ) ==
                                  current->GetDim( x ).GetDx( ) ) &&
                                ( previous->GetDim( x ).GetStartX( ) ==
                                  current->GetDim( x ).GetStartX( ) ) &&
                                ( previous->GetDim( x ).GetUnitX( ) ==
                                  current->GetDim( x ).GetUnitX( ) ) ) )
                        {
                            status = false;
                            break;
                        }
                    }
                }

                BOOST_TEST_MESSAGE( Leader << "field: nx, dx, startX, unitX" );
                BOOST_CHECK( status );
            }
        }
        break;
    default:
    {
        std::ostringstream msg;

        msg << "verify_downconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
}

template <>
void
verify_upconvert< TEMPLATE_SPEC >( frame_object_type             FrameObj,
                                   const std::string&            Leader )
{
    USING( );
    using FrameCPP::Common::FrameSpec;

    if ( !FrameObj )
    {
        throw std::runtime_error( "NULL frame object" );
    }

    const FrameObjectTypes object_id =
        FrameObjectTypes( FrameObj->GetClass( ) );

    switch ( object_id )
    {
    case FrameSpec::Info::FSI_FRAME_H:
    {
        PROMOTE_FROM_PREVIOUS( FrameH );

        if ( promoted.get( ) && previous.get( ) )
        {
            CHECK_STRING( previous, promoted, GetName, name );
            CHECK_NUMBER( (INT_4S)previous, promoted, GetRun, run );
            CHECK_NUMBER( previous, promoted, GetFrame, frame );
            CHECK_NUMBER( previous, promoted, GetDataQuality, dataQuality );
            CHECK_NUMBER( previous, promoted, GetGTime, GTime );
            CHECK_NUMBER( previous, promoted, GetULeapS, ULeapS );
            CHECK_NUMBER( previous, promoted, GetDt, dt );
        }
    }
    break;
    case FrameSpec::Info::FSI_FR_ADC_DATA:
        //-------------------------------------------------------------------
        // FrAdcData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrAdcData );

            if ( promoted.get( ) && previous.get( ) )
            {
                CHECK_STRING( previous, previous, GetName, name );
                CHECK_STRING( previous, previous, GetComment, comment );
                CHECK_NUMBER(
                    previous, previous, GetChannelGroup, channelGroup );
                CHECK_NUMBER(
                    previous, previous, GetChannelNumber, channelNumber );
                CHECK_NUMBER( previous, previous, GetNBits, nBits );
                CHECK_NUMBER( previous, previous, GetBias, bias );
                CHECK_NUMBER( previous, previous, GetSlope, slope );
                CHECK_STRING( previous, previous, GetUnits, units );
                CHECK_NUMBER( previous, previous, GetSampleRate, sampleRate );
                CHECK_NUMBER( previous, previous, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, previous, GetFShift, fShift );
                CHECK_NUMBER( previous, previous, GetPhase, phase );
                CHECK_NUMBER( previous, previous, GetDataValid, dataValid );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_DETECTOR:
        //-------------------------------------------------------------------
        // FrDetector
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrDetector );

            if ( promoted.get( ) && previous.get( ) )
            {
                CHECK_STRING( previous, previous, GetName, name );
                // CHECK_STRING( previous, previous, GetPrefix, prefix );
                CHECK_NUMBER( previous, previous, GetLongitude, longitude );
                CHECK_NUMBER( previous, previous, GetLatitude, latitude );
                CHECK_NUMBER( previous, previous, GetElevation, elevation );
                CHECK_NUMBER( previous, previous, GetArmXazimuth, armXazimuth );
                CHECK_NUMBER( previous, previous, GetArmYazimuth, armYazimuth );
                CHECK_NUMBER(
                    previous, previous, GetArmXaltitude, armXaltitude );
                CHECK_NUMBER(
                    previous, previous, GetArmYaltitude, armYaltitude );
                CHECK_NUMBER(
                    previous, previous, GetArmXmidpoint, armXmidpoint );
                CHECK_NUMBER(
                    previous, previous, GetArmYmidpoint, armYmidpoint );
                CHECK_NUMBER( previous, previous, GetLocalTime, localTime );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_EVENT:
        //-------------------------------------------------------------------
        // FrEvent
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrEvent );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING( previous, promoted, GetInputs, inputs );
                CHECK_NUMBER( previous, promoted, GetGTime, GTime );
                CHECK_NUMBER( previous, promoted, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, promoted, GetTimeAfter, timeAfter );
                CHECK_NUMBER( previous, promoted, GetEventStatus, eventStatus );
                CHECK_NUMBER( previous, promoted, GetAmplitude, amplitude );
                CHECK_NUMBER( previous, promoted, GetProbability, probability );
                CHECK_STRING( previous, promoted, GetStatistics, statistics );

                bool status = ( previous->GetParam( ).size( ) ==
                                promoted->GetParam( ).size( ) );
                if ( status )
                {
                    Previous::FrEvent::ParamList_type::const_iterator pre_cur =
                        previous->GetParam( ).begin( );
                    FrEvent::ParamList_type::const_iterator
                        pro_cur = promoted->GetParam( ).begin( ),
                        pro_last = promoted->GetParam( ).end( );

                    while ( pro_cur != pro_last )
                    {
                        if ( ( pro_cur->first != pre_cur->first ) ||
                             ( pro_cur->second != pre_cur->second ) )
                        {
                            status = false;
                            break;
                        }
                        ++pre_cur;
                        ++pro_cur;
                    }
                }
                BOOST_TEST_MESSAGE( Leader << "field: param" );
                BOOST_CHECK( status );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_HISTORY:
        //-------------------------------------------------------------------
        // FrHistory
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrHistory );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_NUMBER( previous, promoted, GetTime, time );
                CHECK_STRING( previous, promoted, GetComment, comment );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_MSG:
        //-------------------------------------------------------------------
        // FrMsg
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrMsg );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetAlarm, alarm );
                CHECK_STRING( previous, promoted, GetMessage, message );
                CHECK_NUMBER( previous, promoted, GetSeverity, severity );
                CHECK_NUMBER( previous, promoted, GetGTime, GTime );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_PROC_DATA:
        //-------------------------------------------------------------------
        // FrProcData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrProcData );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER( previous, promoted, GetType, type );
                CHECK_NUMBER( previous, promoted, GetSubType, subType );
                CHECK_NUMBER( previous, promoted, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, promoted, GetTRange, tRange );
                CHECK_NUMBER( previous, promoted, GetFShift, fShift );
                CHECK_NUMBER( previous, promoted, GetPhase, phase );
                CHECK_NUMBER( previous, promoted, GetFRange, fRange );
                CHECK_NUMBER( previous, promoted, GetBW, BW );

                bool status = ( previous->GetAuxParam( ).size( ) ==
                                promoted->GetAuxParam( ).size( ) );
                if ( status )
                {
                    Previous::FrProcData::AuxParamList_type::const_iterator
                        pre_cur = previous->GetAuxParam( ).begin( );
                    FrProcData::AuxParamList_type::const_iterator
                        pro_cur = promoted->GetAuxParam( ).begin( ),
                        pro_last = promoted->GetAuxParam( ).end( );

                    while ( pro_cur != pro_last )
                    {
                        if ( ( pro_cur->auxParam != pre_cur->auxParam ) ||
                             ( pro_cur->auxParamName !=
                               pre_cur->auxParamName ) )
                        {
                            status = false;
                            break;
                        }
                        ++pre_cur;
                        ++pro_cur;
                    }
                }
                BOOST_TEST_MESSAGE( Leader << "field: param" );
                BOOST_CHECK( status );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_RAW_DATA:
        //-------------------------------------------------------------------
        // FrRawData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrRawData );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SER_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSerData );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_NUMBER( previous, promoted, GetTime, time );
                CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
                CHECK_STRING( previous, promoted, GetData, data );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_DATA:
        //-------------------------------------------------------------------
        // FrSerData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSimData );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER( previous, promoted, GetSampleRate, sampleRate );
                CHECK_NUMBER( previous, promoted, GetTimeOffset, timeOffset );
                CHECK_NUMBER( previous, promoted, GetFShift, fShift );
                CHECK_NUMBER( previous, promoted, GetPhase, phase );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SIM_EVENT:
        //-------------------------------------------------------------------
        // FrSimEvent
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSimEvent );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING( previous, promoted, GetInputs, inputs );
                CHECK_NUMBER( previous, promoted, GetGTime, GTime );
                CHECK_NUMBER( previous, promoted, GetTimeBefore, timeBefore );
                CHECK_NUMBER( previous, promoted, GetTimeAfter, timeAfter );
                CHECK_NUMBER( previous, promoted, GetAmplitude, amplitude );
                bool status = ( previous->GetParam( ).size( ) ==
                                promoted->GetParam( ).size( ) );
                if ( status )
                {
                    Previous::FrSimEvent::ParamList_type::const_iterator
                        pre_cur = previous->GetParam( ).begin( );
                    FrSimEvent::ParamList_type::const_iterator
                        pro_cur = promoted->GetParam( ).begin( ),
                        pro_last = promoted->GetParam( ).end( );

                    while ( pro_cur != pro_last )
                    {
                        if ( ( pro_cur->first != pre_cur->first ) ||
                             ( pro_cur->second != pre_cur->second ) )
                        {
                            status = false;
                            break;
                        }
                        ++pre_cur;
                        ++pro_cur;
                    }
                }
                BOOST_TEST_MESSAGE( Leader << "field: param" );
                BOOST_CHECK( status );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_STAT_DATA:
        //-------------------------------------------------------------------
        // FrStatData
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrStatData );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING(
                    previous, promoted, GetRepresentation, representation );
                CHECK_NUMBER( previous, promoted, GetTimeStart, timeStart );
                CHECK_NUMBER( previous, promoted, GetTimeEnd, timeEnd );
                CHECK_NUMBER( previous, promoted, GetVersion, version );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_SUMMARY:
        //-------------------------------------------------------------------
        // FrTable
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrSummary );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_STRING( previous, promoted, GetTest, test );
                CHECK_NUMBER( previous, promoted, GetGTime, GTime );
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_TABLE:
        //-------------------------------------------------------------------
        // FrTable
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrTable );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_STRING( previous, promoted, GetComment, comment );
                CHECK_NUMBER( previous, promoted, GetNColumn, nColumn );
                CHECK_NUMBER( previous, promoted, GetNRow, nRow );

                //---------------------------------------------------------------
                // Do not check columnName field since it is derived from the
                // FrVects of this object.
                //---------------------------------------------------------------
            }
        }
        break;
    case FrameSpec::Info::FSI_FR_VECT:
        //-------------------------------------------------------------------
        // FrVect
        //-------------------------------------------------------------------
        {
            PROMOTE_FROM_PREVIOUS( FrVect );

            if ( previous.get( ) && promoted.get( ) )
            {
                CHECK_STRING( previous, promoted, GetName, name );
                CHECK_NUMBER( previous, promoted, GetCompress, compress );
                CHECK_NUMBER( previous, promoted, GetType, type );
                CHECK_NUMBER( previous, promoted, GetNData, nData );
                CHECK_NUMBER( previous, promoted, GetNBytes, nBytes );
                CHECK_NUMBER( previous, promoted, GetNDim, nDim );
                CHECK_STRING( previous, promoted, GetUnitY, unitY );

                //---------------------------------------------------------------
                // Validate the data
                //---------------------------------------------------------------

                FrVect::data_type pdata( previous->GetDataRaw( ) );

                BOOST_TEST_MESSAGE( Leader << "field: data" );
                BOOST_CHECK( std::equal( pdata.get( ),
                                         pdata.get( ) + previous->GetNData( ),
                                         promoted->GetDataRaw( ).get( ) ) );

                //---------------------------------------------------------------
                // Validate the dim info
                //---------------------------------------------------------------
                bool status( previous->GetNDim( ) == promoted->GetNDim( ) );

                if ( status )
                {
                    for ( size_t x = promoted->GetNDim( ); x > 0; )
                    {
                        --x;
                        if ( !( ( previous->GetDim( x ).GetNx( ) ==
                                  promoted->GetDim( x ).GetNx( ) ) &&
                                ( previous->GetDim( x ).GetDx( ) ==
                                  promoted->GetDim( x ).GetDx( ) ) &&
                                ( previous->GetDim( x ).GetStartX( ) ==
                                  promoted->GetDim( x ).GetStartX( ) ) &&
                                ( previous->GetDim( x ).GetUnitX( ) ==
                                  promoted->GetDim( x ).GetUnitX( ) ) ) )
                        {
                            status = false;
                            break;
                        }
                    }
                }

                BOOST_TEST_MESSAGE( Leader << "field: nx, dx, startX, unitX" );
                BOOST_CHECK( status );
            }
        }
        break;
    default:
    {
        std::ostringstream msg;

        msg << "verify_upconvert<" << TEMPLATE_SPEC
            << ">: Unsupported type: " << object_id;
        throw FrameCPP::Unimplemented(
            msg.str( ), TEMPLATE_SPEC, __FILE__, __LINE__ );
    }
    break;
    }
}

//=======================================================================

#undef USING
#undef PREVIOUS_TEMPLATE_SPEC
#undef PREVIOUS_NAMESPACE
#undef NAMESPACE
#undef TEMPLATE_SPEC

#endif /* FRAME_CPP__TEST__FR_STRUCT_8_TCC */
