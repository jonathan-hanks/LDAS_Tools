# Release NEXT -
    - Corrected initialization order
    - Restored CRC effecientcy (closes #164)
    - Restore framecpp_compare (closes #165)
    - Corrected handling of composite compression modes
    - Corrected translation of compression mode flags going from version 8
      frame specification to version 9 frame specification

# Release 3.0.2 - Tue January 17, 2023
    - Corrected handling of ZERO_SUPPRESS_OTHERWISE_GZIP (closes computing/ldastools/LDAS_Tools#159)
    - Added GetNDataValid function and other functions to access/manipulate dataValid vector to FrVect (closes computing/ldastools/LDAS_Tools#160)
    - Modified function signature of FrameCPP::Version9::FrVect::SetDataValid to accept const CHAR_U* as first parameter (closes computing/ldastools/LDAS_Tools#161)
    - Updated library versions:
      - data format version: 9.2
      - libframecpp9 2:0:2
      - libfraemcpp 17:0:1

# Release 3.0.1 - Wed January 04, 2023
    - Corrected framecpp/Types.hh for version 9 frame specification (closes computing/ldastools/LDAS_Tools#158)
    - Build using C++17 standard
    - Improved framecpp_fracfg utility to exit with non-zero exit status for unsupported frame spec versions
    - Updated library versions
      - data format version: 9.1
      - libfraemcpp 16:1:0
      - libframecpp9 1:1:1

# Release 3.0.0 - December 22, 2022
    - Corrected default slope value for version 8 FrAdcData (closes computing/ldastools/LDAS_Tools#114)
    - Removed FrStatData (closes computing/ldastools/LDAS_Tools#127)
    - Remove ULeapS from FrameH structure (closes computing/ldastools/LDAS_Tools#128)
    - Removed the following elements from the FrTOC (closes computing/ldastools/LDAS_Tools#131)
      - ULeapS
      - runs
      - frame
      - nFirstADC
      - nFirstSer
      - nFirstTable
      - nFirstMsg
      - nStatType
      - nameStat
      - detector
      - nStatInstance
      - nTotalStat
      - tStart
      - tEnd
      - version
      - positionStat
    - Have FrTOC::FrStatQuery throw runtime error as this function is depricated (closes computing/ldastools/LDAS_Tools#131) 
    - Remove ULeapS from FrameH structure (closes computing/ldastools/LDAS_Tools#128)
    - Added fileBaseName to FrTOC (closes computing/ldastools/LDAS_Tools#134)
    - Added MIME support to FrProcData (closes computing/ldastools/LDAS_Tools#133)
    - Added Zstd compression modes (closes computing/ldastools/LDAS_Tools#136)
    - Added dataValid to FrVect (closes computing/ldastools/LDAS_Tools#130)
    - Added chkSumTOC to FrEndOfFile (closes computing/ldastools/LDAS_Tools#132)
    - Updated library versions
      - libframecpp 16:0:0
      - libframecppc 4:0:0

# Release 2.9.1 - July 11, 2022
    - Corrected issue with reading Param information for FrEvent, FrSimEvent and FrProcData (closes computing/ldastools/LDAS_Tools#148)
    - Minor changes to the build system to allow for cross compilation (closes computing/ldastools/LDAS_Tools#144 computing/ldastools/LDAS_Tools#145)
    - Updated library versions
      - libframecpp8 11:1:0

# Release 2.9.0 - June 27, 2022
    - Corrected library versioning numbers to indicate API/API incompatibility
      - libframecpp8 11:0:0
      - libframecpp 15:0:0

# Release 2.8.2 - June 14, 2022
    - Corrected default slope value for version 8 FrAdcData (closes #114)
    - Code cleanup for SWIG compilation
    - Corrected type for FrSimEvent::parameters (REAL_4 => REAL_8)
    - Modified FrSimEvent time type to be GPSTime (consitent with FrEvent)
    - Modified FrameCPP::Common::AppendComment to prevent STRING overflow (closes #138)
    - Modified FrameCPP::Common::AppendComment to prevent copying of duplicates to STRING (closes #140)
    - Fixed issue with calculating cksums for files that were larger than 4G (closes #137)

# Release 2.8.1 - September 29, 2021
    - Many corrections to support building on RHEL 8
    - Minor version number bumped to reflect backwards
      incompatibility introduced with previous release.

# Release 2.7.3 - September 21, 2021
  - Removed excessive debugging statements (closes #109)
  - Updated library version numbers
    - libframecmn 12:0:0
    - libframecpp3 8:0:0
    - libframecpp4 10:0:0
    - libframecpp6 10:0:0
    - libframecpp7 6:0:0
    - libframecpp8 9:0:0
    - libframecpp 13:0:0

# Release 2.7.2 - August 13, 2021
  - Corrected macro FRAMECPP_VERSION_NUMBER (closes #87)
  - Corrected Debian package resolution order for boost libaries (closes #107)
  - Addressed type conversion issue that manifested in the Python SWIG bindings (closes #108)

# Release 2.7.1 - March 9, 2021
  - Continuation with Serialization infistructure
  - Updated library version
    - libframecppcmn 11:1:0
    - libframecpp3 7:2:1
    - libframecpp4 9:2:1
    - libframecpp6 9:1:1
    - libframecpp7 5:1:1
    - libframecpp8 8:1:1
    - libframecpp 12:1:0
 
# Release 2.7.0 - August 14, 2019
  - Converted to CMake (closes #33)
  - Converted framecpp_sample to use boost::program_options
  - Converted unit tests to use Boost::Test
  - Added over arching directive to build only FrameCPP library and support packages (closes #63).
  - Reformatted comments for Doxygen (Closes #66)
  - Corrected Portfile to have proper description (Closes #55)
  - Converted FrSimData::data_type from Common::SearchContainer to Common::Container (Closes #71)
  - Updated library version
    - libframecppcmn 11:0:0
    - libframecpp3 7:1:1
    - libframecpp4 9:1:1
    - libframecpp6 9:1:1
    - libframecpp7 5:1:1
    - libframecpp8 8:1:1
    - libframecpp 12:1:0

# Release 2.6.5 - January  8, 2019
  - Have IFrameStream::GetTOC() return shared_ptr to FrTOC to provide better memory protection symantics

# Release 2.6.4 - December 6, 2018
  - Addressed packaging issues

# Release 2.6.3 - November 27, 2018
  - Added requirement of C++ 2011 standard
  - Store in repository a static version of utility generated
    documentation (fixes #11)
  - Standardize source code format by using clang-format
  - Eliminated some warning messages noticed when compiling on Buster
  - Changed FrAdcData.GetUnits to return const STRING& (Fixes #36)
  - Addressed Debian Buster warning messages (Fixes #38)
  - Added checking of run number of FrameStreamPlan (Fixes #14)
  - Corrected problem with C-Interface compression (closes #44)
  - Added FrHistory to FrVect for Sample frames
  - When compressing buffers, only returned compressed buffer
    iff it is greater than zero and less than the input buffer. (closes #48)
  - Updated library version
    - libframecppcmn 10:0:1
    - libframecpp3 7:0:1
    - libframecpp4 9:0:1
    - libframecpp6 9:0:1
    - libframecpp7 5:0:1
    - libframecpp8 8:0:1
    - libframecpp 11:0:1
    - libframecppc 3:5:1

# Release 2.6.1 - June 22, 2018
  - Updated packaging rules to have build time dependency on specific
    versions of LDAS Tools packages

# Release 2.6.0 - June 19, 2018
  - Removed hand rolled smart pointers in favor of boost smart pointers
  - Changed version of framecppcmn library to 9:0:0
  - Changed version of framecpp3 library to 6:0:0
  - Changed version of framecpp4 library to 8:0:0
  - Changed version of framecpp6 library to 8:0:0
  - Changed version of framecpp7 library to 4:0:0
  - Changed version of framecpp8 library to 7:0:0
  - Changed version of framecpp library to 10:0:0
  - Changed version of framecppc library to 3:4:0
  - Added detector definitions for GEO, KAGRA, and TAMA

# Release 2.5.8 - March 21, 2018
  - Extended Python interface to support FrSimEvent

# Release 2.5.7 - August 25, 2017
  - Extended Python interface to read a subset of elements of the FrameH structure.
  - Extended Python interface to read type, user, detectSim, detectProc, history,
	auxData, and auxTable values from FrameH structure.

# Release 2.5.6 - February 13, 2017
  - Needed to incriment library version number to reflect binary incompatibility change

# Release 2.5.5 - January 27, 2017
  - Corrected Verify class's file checksum optimization (#4456)
  - Corrected problem with C-Interface comparession (#4490)

# Release 2.5.5 - January 27, 2017
  - Corrected support for reading FrEvent and FrSimEvent structures from
    frame stream (#4954)

# Release 2.5.4 - October 22, 2016
  - Refactored code to support Python subport for Macports

# Release 2.5.3 - October 10, 2016
  - Updated to support Debian Sketch

# Release 2.5.2 - September 9, 2016
  - Added --disable-warnings-as-errors to allow compilation on systems
    where warning messages have not yet been addressed
  - Added FRAMECPP_VERSION to Common/FrameSpec.hh file (#4279)
  - Added conditional setting of DESTDIR in python.mk to prevent install
    issues.
  - Reformatted extended DATA_INVALID error message to make it more readable.
  - Added 'none' as synonym for 'raw' to framecpp_compressor (#4327)
  - Removed verbose error message.

# Release 2.5.1 - April 21, 2016

The following problems have been resolved in this release:
  - Addressed potential infinite recursive call

# Release 2.5.0 - April 7, 2016
  - Split LDASTools into separate source packages

# Release 2.4.99.1 - March 11, 2016
  - Corrections to spec files.

# Release 2.4.99.0 - March 3, 2016
  - Separated code into independant source distribution
