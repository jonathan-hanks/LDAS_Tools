# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4

PortSystem    1.0
PortGroup     compiler_blacklist_versions 1.0
PortGroup     cmake 1.0

name          @PROJECT_NAME@
version       @PROJECT_VERSION@
categories    science
license       GPL-2+
platforms     darwin
maintainers   {@emaros ligo.org:ed.maros} openmaintainer

description      @PROJECT_DESCRIPTION@
long_description @PROJECT_DESCRIPTION_LONG@

homepage      @PACKAGE_URL@
master_sites  @LDAS_TOOLS_SOURCE_URL@

checksums \
    rmd160 @RMD160@ \
    sha256 @SHA256@ \
    size   @SIZE@

depends_build \
    port:cmake \
    port:pkgconfig \
    port:ldas-tools-cmake \
    port:gtest

depends_lib \
    port:ldas-tools-al \
    port:boost \
    port:openssl \
    port:zlib \
    port:bzip2

configure.args

# requires clang from Xcode5 or higher to build
compiler.blacklist-append {clang < 500.2.75} llvm-gcc-4.2 gcc-4.2

use_parallel_build yes

pre-fetch {
    if {${os.platform} eq "darwin" && ${os.major} < 11} {
        ui_error "${name} only runs on Mac OS X 10.7 or greater."
        return -code error "incompatible Mac OS X version"
    }
}

#------------------------------------------------------------------------

livecheck.type   regex
livecheck.url    ${master_sites}
livecheck.regex  {@PROJECT_NAME@-(\d+(?:\.\d+)*).tar.gz}
