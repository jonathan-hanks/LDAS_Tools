#! /bin/sh
#
# LDASTools frameCPP - A library implementing the LIGO/Virgo frame specification
#
# Copyright (C) 2018 California Institute of Technology
#
# LDASTools frameCPP is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 (GPLv2) of the
# License or at your discretion, any later version.
#
# LDASTools frameCPP is distributed in the hope that it will be useful, but
# without any warranty or even the implied warranty of merchantability
# or fitness for a particular purpose. See the GNU General Public
# License (GPLv2) for more details.
#
# Neither the names of the California Institute of Technology (Caltech),
# The Massachusetts Institute of Technology (M.I.T), The Laser
# Interferometer Gravitational-Wave Observatory (LIGO), nor the names
# of its contributors may be used to endorse or promote products derived
# from this software without specific prior written permission.
#
# You should have received a copy of the licensing terms for this
# software included in the file LICENSE located in the top-level
# directory of this package. If you did not, you can view a copy at
# http://dcc.ligo.org/M1500244/LICENSE
#

#------------------------------------------------------------------------
# Global Variables
#------------------------------------------------------------------------
ldas_repository_dir_="${HOME}/Sources"
ldas_prefix_="${HOME}/`uname`-`uname -m`"
ldas_ospace_dir_="${HOME}/`uname`-`uname -m`/stow_pkgs/ospace-head"
ldas_tcl_config_="/usr/lib64/tclConfig.sh"
ldas_optimization_="high"
ldas_ticket_prefix_="ldas-ticket-"
ldas_env_=""
ldas_with_ldcg_inclusion="no"
ldas_with_metadata_api_="no"
ldas_inlining_="yes"
ldas_with_doxygen_="yes"
ldas_extra_options_=""
ldas_enable_64bit_="yes"
#------------------------------------------------------------------------
# Read user's customization
#------------------------------------------------------------------------
ldas_configure_resource_file_="${HOME}/.ldas-configure.rc"
if test -f ${ldas_configure_resource_file_}
then
  . ${ldas_configure_resource_file_}
fi
#------------------------------------------------------------------------
# Nothing needs modification below this line.
#------------------------------------------------------------------------
case $# in
2)
    SOURCEDIR=$1
    TICKET=$2
    ;;
*)
    echo "USAGE: $0 <source-dir> <ticket-number>"
    exit 1
    ;;
esac
#------------------------------------------------------------------------
# Setup the options
#------------------------------------------------------------------------
#opts_="${opts_} --without-stlport"
#case "x${ldas_with_ldcg_inclusion}" in
#"xyes")
#  opts_="${opts_} --enable-auto-ldcg-inclusion"
#  ;;
#*)
#  opts_="${opts_} --disable-auto-ldcg-inclusion"
#  ;;
#esac
#case "x${ldas_with_metadata_api_}" in
#"xyes")
#  opts_="${opts_} --enable-metadata-api"
#  ;;
#*)
#  opts_="${opts_} --disable-metadata-api"
#  ;;
#esac
#opts_="${opts_} --with-ilwd"
#opts_="${opts_} --with-threads"
#opts_="${opts_} --with-ospace=${ldas_ospace_dir_}"
opts_="${opts_} --with-tcl-config=${ldas_tcl_config_}"
opts_="${opts_} --with-doxygen=${ldas_with_doxygen_}"
opts_="${opts_} --without-dot"
opts_="${opts_} --with-optimization=${ldas_optimization_}"
opts_="${opts_} --enable-64bit=${ldas_enable_64bit_}"
#case "x${ldas_inlining_}" in
#"xno")
#  opts_="${opts_} --disable-inlining"
#  ;;
#*)
#  opts_="${opts_} --enable-inlining"
#  ;;
#esac
opts_="${opts_} ${ldas_extra_options_}"
#------------------------------------------------------------------------
#------------------------------------------------------------------------
eval ${ldas_repository_dir_}/${SOURCEDIR}/ldas-tools/configure ${ldas_env_} \
    --prefix=${ldas_prefix_}/stow_pkgs/${ldas_ticket_prefix_}${TICKET} \
    --docdir='\${datarootdir}/doc/\${PACKAGE_NAME}-\${PACKAGE_VERSION}' \
    --cache-file=`pwd`/ldas.cache \
    ${opts_}
