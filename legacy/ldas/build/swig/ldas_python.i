#ifndef BUILD__SWIG__LDAS_PYTHON_I
#define BUILD__SWIG__LDAS_PYTHON_I 1

#define LDAS_DOC_PROTO_WITHOUT_TYPES "0"
#define LDAS_DOC_PROTO_WITH_TYPES "1"

/*
 */
%define Documentation(Proto,Brief,Full)
  %feature("autodoc",Proto);
  %feature("docstring") %{
    Brief

    Full
  %}
%enddef

/*
 */
%define LDASPythonDocStringStart
#if ! defined(_LDASDocStringStarted)
#define _LDASDocStringStarted 1
%feature("docstring") %{
#endif /* ! defined(_LDASDocStringStarted) */
%enddef /* LDASDocStartDocString */

/*
 */
%define LDASPythonDocStringStop
#if defined(_LDASDocStringStarted)
%}
#undef _LDASDocStringStarted
#endif /* _LDASDocStringStarted */
%enddef /* LDASPythonDocStringStop */

/*
 */
%define LDASDocBegin
#undef _LDAS_DOCSTRING
%enddef /* LDASDocBegin */

/*
 */
%define LDASDocEnd
%feature("docstring") %{
#if defined(LDASDocVarBrief)
LDASDocVarBrief
#undef LDASDocVarBrief
#endif /* LDASDocVarBrief */
#if defined(LDASDocVarDescription)
LDASDocVarDescription
#undef LDASDocVarDescription
#endif /* LDASDocVarBrief */
%}
%enddef /* LDASDocEnd */

/*
 */
%define LDASDocProto(Proto)
  %feature("autodoc",Proto)
%enddef

/*
 */
%define LDASDocBrief(Brief)
%#define LDASDocVarBrief Brief
%enddef /* LDASDocBrief(Brief) */

/*
 */
%define LDASDocDesc(Description)
%#define LDASDocVarDescription Description
%enddef

/*
 */
#define LDASDocParam(Name,Description) \
   Name: Description

%define LDASDoc(Function,Proto,Brief,Long,Params)
%feature("autodoc",Proto) Function;
%feature("docstring") Function %{
Brief

Long

Parameters:
Params
%};
%enddef /* LDASDoc */

%define LDASDocZeroParams(Function,Proto,Brief,Long)
%feature("autodoc",Proto) Function;
%feature("docstring") Function %{
Brief

Long
%};
%enddef /* LDASDoc */

#endif /* BUILD__SWIG__LDAS_PYTHON_I */
