#=========================================================================
# Rules for building the tcl index file
#=========================================================================
createPkgIndex: $(ldas_top_srcdir)/build/make-macros/tcl.mk
	echo '#!/bin/sh' > $@
	echo '# the next line restarts using tclsh'\\ >> $@
	echo 'exec tclsh "$$0" "$$@"' >> $@
	echo '#------------------------------------------------------------------------' >> $@
	echo '# This creates the pkgIndex file needed by tcl scripts' >> $@
	echo '#------------------------------------------------------------------------' >> $@
	echo 'eval pkg_mkIndex $$argv' >> $@
	chmod 0755 $@

#=========================================================================
# Rules needed when building TCL extensions
#=========================================================================

.SECONDEXPANSION:
%_tcl.stamp: $$($$*_SWIG) Makefile $($$*_SWIG_INCLUDES)
	@rm -f $*_tcl.stamp
	@touch $*_tcl.stamp.tmp
	$(AM_V_GEN)$(SWIG) $(SWIGFLAGS) \
		$(TCL_INCLUDE_SPEC) -I$(srcdir) \
		-I$(ldas_top_srcdir)/build/swig \
		-I$(ldas_top_builddir)/include $(AM_CPPFLAGS) \
		-tcl8 -c++ -pkgversion $(LDAS_PACKAGE_VERSION) \
		-o $*_tcl_wrap.cc $<
	@mv $*_tcl.stamp.tmp $*_tcl.stamp

#........................................................................
# This handles parallel cleanup safely
#........................................................................
%_tcl_wrap.cc: $$($$*_SWIG) Makefile $($$*_SWIG_INCLUDES)
	@if test -f $@ ; then :; else \
	  trap 'rm -rf $*_tcl.lock $*_tcl.stamp' 1 2 13 15; \
	  if mkdir $*_tcl.lock 2>/dev/null ; then \
	    rm -f $*_tcl.stamp; \
	    $(MAKE) $(AM_MAKEFLAGS) $*_tcl.stamp ; \
	    result=$$?; rm -rf $*_tcl.lock ; exit $$result; \
	  else \
	    while test -d $*_tcl.lock; do sleep 1; done; \
	    test -f $*_tcl.stamp ; \
	  fi; \
	fi
