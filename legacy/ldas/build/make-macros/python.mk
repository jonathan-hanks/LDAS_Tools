#=========================================================================
# Rules needed when building Python extensions
#=========================================================================

.SECONDEXPANSION:
%_python.stamp: $$($$*_SWIG) Makefile $$($$*_SWIG_INCLUDES) $$($$*_SWIG_AUXDEPS)
	@rm -f $*_python.stamp
	@touch $*_python.stamp.tmp
	$(AM_V_GEN)$(SWIG) $(SWIGFLAGS) \
		-shadow -c++ -importall -Wall \
		-I$(srcdir) \
		-I$(ldas_top_srcdir)/build/swig $(PYTHON_INCLUDES) \
		 $(AM_CPPFLAGS) \
		-c++ \
		-python -classic \
		-o $*_python_wrap.cc $< \
	&& echo "#include <wchar.h>" > $*_python_wrap.cc.new \
	&& cat $*_python_wrap.cc >> $*_python_wrap.cc.new \
	&& mv $*_python_wrap.cc.new $*_python_wrap.cc
	@mv $*_python.stamp.tmp $*_python.stamp

#........................................................................
# This handles parallel cleanup safely
#........................................................................
.SECONDEXPANSION:
%_python_wrap.cc: $$($$*_SWIG) Makefile $$($$*_SWIG_INCLUDES) $$($$*_SWIG_AUXDEPS)
	@if test -f $@ ; then :; else \
	  trap 'rm -rf $*_python.lock $*_python.stamp' 1 2 13 15; \
	  if mkdir $*_python.lock 2>/dev/null ; then \
	    $(MAKE) $(AM_MAKEFLAGS) $*_python.stamp ; \
	    result=$$?; rm -rf $*_python.lock ; exit $$result; \
	  else \
	    while test -d $*_python.lock; do sleep 1; done; \
	    test -f $*_python.stamp ; \
	  fi; \
	fi
