#========================================================================
# Allow for some system testing using valgrin if available
#========================================================================
VALGRIND_TESTS_ENVIRONMENT ?= $(TESTS_ENVIRONMENT)
VALGRIND_TESTS ?= $(TESTS)
VALGRIND_ALL_OPTS=
VALGRIND_ALL_OPTS+=--verbose
ifdef VALGRIND_GEN_SUPPRESSION
VALGRIND_ALL_OPTS+=--demangle=no
VALGRIND_ALL_OPTS+=--gen-suppressions=all
else
VALGRIND_ALL_OPTS+=--demangle=yes
VALGRIND_ALL_OPTS+=--track-origins=yes
endif
VALGRIND_ALL_OPTS+=--suppressions=$(ldas_top_srcdir)/build/data/ldas.supp
VALGRIND_ALL_OPTS+=--suppressions=$(ldas_top_srcdir)/build/data/ospace.supp
VALGRIND_ALL_OPTS+=--suppressions=$(ldas_top_srcdir)/build/data/gcc.supp
VALGRIND_ALL_OPTS+=--num-callers=40
# VALGRIND_ALL_OPTS+=--read-var-info=yes
VALGRIND_ALL_OPTS+=$(VALGRIND_TOOL_OPTS)
VALGRIND_ALL_OPTS+=$(VALGRIND_OPTS)

VALGRIND_TOOL_SUITE=

CALLGRIND_ALL_OPTS=
CALLGRIND_ALL_OPTS+=--verbose
CALLGRIND_ALL_OPTS+=--dump-every-bb=10000000

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# CALLGRIND
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=callgrind
CALLGRIND_TOOL_OPTS="--callgrind-out-file=%q{cmd}.profile.%p"
CALLGRIND_EGREP_PATTERN=''

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# DRD
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=drd
DRD_TOOL_OPTS=""
DRD_EGREP_PATTERN='(ERROR SUMMARY:)'

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# HELGRIND
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=helgrind
HELGRIND_TOOL_OPTS="--track-lockorders=no"
HELGRIND_EGREP_PATTERN='(ERROR SUMMARY:)'

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# MEMCHECK
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
VALGRIND_TOOL_SUITE+=memcheck
MEMCHECK_TOOL_OPTS="--leak-check=yes --show-reachable=yes"
MEMCHECK_EGREP_PATTERN='(definitely lost:|possibly lost:|still reachable:|ERROR)'

#========================================================================
# Rules section
#========================================================================

ifdef TESTS
valgrind-gen: check-TESTS
endif

valgrind-clean:
	@if test -d .libs; \
	 then               \
           ( cd .libs; rm -f $(patsubst %,*.%,$(VALGRIND_TOOL_SUITE) ) ); \
	 fi

memcheck: valgrind-memcheck
mtsafe: valgrind-drd

valgrind-memcheck:
	@$(MAKE) \
	  VALGRIND_TOOL="memcheck" \
	  VALGRIND_TOOL_OPTS=$(MEMCHECK_TOOL_OPTS) \
	  VALGRIND_TOOL_EGREP_PATTERN=$(MEMCHECK_EGREP_PATTERN) \
	  valgrind-gen

valgrind-memcheck-gen-supp:
	@$(MAKE) \
	  VALGRIND_GEN_SUPPRESSION=yes \
	  valgrind-memcheck

valgrind-callgrind:
	$(MAKE) \
	  VALGRIND_TOOL="callgrind" \
	  VALGRIND_TOOL_OPTS=$(CALLGRIND_TOOL_OPTS) \
	  VALGRIND_TOOL_EGREP_PATTERN=$(CALLGRIND_EGREP_PATTERN) \
	  valgrind-gen

valgrind-drd:
	$(MAKE) \
	  VALGRIND_TOOL="drd" \
	  VALGRIND_TOOL_OPTS=$(DRD_TOOL_OPTS) \
	  VALGRIND_TOOL_EGREP_PATTERN=$(DRD_EGREP_PATTERN) \
	  valgrind-gen

valgrind-drd-gen-supp:
	$(MAKE) \
	  VALGRIND_GEN_SUPPRESSION=yes \
	  valgrind-drd

valgrind-helgrind:
	$(MAKE) \
	  VALGRIND_TOOL="helgrind" \
	  VALGRIND_TOOL_OPTS=$(HELGRIND_TOOL_OPTS) \
	  VALGRIND_TOOL_EGREP_PATTERN=$(HELGRIND_EGREP_PATTERN) \
	  valgrind-gen

valgrind-helgrind-gen-supp:
	$(MAKE) \
	  VALGRIND_GEN_SUPPRESSION=yes \
	  valgrind-helgrind

valgrind-gen: $(VALGRIND_TESTS)
	@for x in $(VALGRIND_TESTS);				\
	do							\
	  if test -x .libs/lt-$$x;				\
	  then							\
	    cmd=".libs/lt-$$x";					\
	  elif test -x .libs/$x;				\
	  then							\
	    cmd=".libs/$$x";					\
	  else							\
	    cmd=".libs/$$x";					\
	  fi;							\
	  export cmd; 						\
	  $(VALGRIND_TESTS_ENVIRONMENT)				\
	  env LD_LIBRARY_PATH="$(VALGRIND_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH}" \
	    GLIBCXX_FORCE_NEW=yes				\
	    $(VALGRIND) --tool=$(VALGRIND_TOOL)			\
	    $(VALGRIND_ALL_OPTS)				\
            $$cmd $(COMMAND_OPTIONS) > $$cmd.$(VALGRIND_TOOL) 2>&1;		\
	done
	@case 'x$(VALGRIND_TOOL_EGREP_PATTERN)' in		\
	  x)							\
	     ;;							\
	  *) find ./ -name '*.$(VALGRIND_TOOL)'			\
	     -exec egrep '$(VALGRIND_TOOL_EGREP_PATTERN)'	\
	     {} /dev/null \;					\
	     ;;							\
	esac

callgrind: $(VALLGRIND_TESTS)
	echo $(VALGRIND_LD_LIBRARY_PATH)
	for x in $(VALGRIND_TESTS);				\
	do							\
	  if test -x .libs/lt-$$x;				\
	  then							\
	    cmd=".libs/lt-$$x";					\
	  elif test -x .libs/$x;				\
	  then							\
	    cmd=".libs/$$x";					\
	  else							\
	    cmd=".libs/$$x";					\
	  fi;							\
	  $(VALGRIND_TESTS_ENVIRONMENT)				\
	  env LD_LIBRARY_PATH="$(VALGRIND_LD_LIBRARY_PATH):$${LD_LIBRARY_PATH}" \
	  callgrind $(CALLGRIND_ALL_OPTS) --base=$$cmd $$cmd;	\
	done

clean: valgrind-clean
