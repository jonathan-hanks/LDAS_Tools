#include <stdexcept>

#include "FlatTeXSType.hh"
#include "FlatTeXDriver.hh"
#include "FlatTeXParser.hh"

//-----------------------------------------------------------------------
// CLASS: FlatTeXDriver
//-----------------------------------------------------------------------
FlatTeXDriver::
FlatTeXDriver( FlatTeXDriver* Parent )
{
  init( Parent );
}

FlatTeXDriver::
FlatTeXDriver( const char* Filename, FlatTeXDriver* Parent )
{
  init( Parent );
}

FlatTeXDriver::
~FlatTeXDriver( )
{
}

int FlatTeXDriver::
EvalCondition( const std::string& Expression )
{
  std::istringstream	in( Expression );
  std::ostringstream	out;
  FlatTeXDriver		driver( this );

  driver.Parse( &in, &out );

#if WORKING
  if ( parser.yyparse( ) != 0 )
  {
    throw( std::runtime_error( "Parsing error" ) );
  }
#endif /* WORKING */

  return driver.Expression( );
}

void FlatTeXDriver::
Evaluate( const std::string& Buffer, std::ostream* Out )
{
  std::istringstream	in( Buffer );
  FlatTeXDriver		driver( this );

  if ( Out == (std::ostream*)NULL )
  {
    Out = &(this->output( ));
  }
  driver.Parse( &in, Out );
#ifdef WORKING
  if ( parser.yyparse( ) != 0 )
  {
    throw( std::runtime_error( "Parsing error" ) );
  }
#endif /* WORKING */
}

FlatTeXDriver::token_type FlatTeXDriver::
Lex( yy::FlatTeXParser::semantic_type* YYLval,
     yy::FlatTeXParser::location_type* YYLloc )
{
  YYLval->reset( );
  m_scanner->YYLval( YYLval );
  m_scanner->YYLloc( YYLloc );

  token_type  retval = token_type( m_scanner->yylex( ) );
#if 0
  std::cerr << "DEBUG: YYLval: " << *YYLval
	    << " Type: " << YYLval->Type( )
	    << " token: " << retval
	    << std::endl;
#endif /* 0 */

  return retval;
}

void FlatTeXDriver::
Parse( std::istream* In, std::ostream* Out )
{
  yy::FlatTeXParser	parser( *this );
  FlatTeXLexer*		lexer = ( m_parent ) ? m_parent->get_lexer( ) : NULL;

  m_scanner.reset( new FlatTeXLexer( In, Out, lexer ) );
  parser.parse( );
}

void FlatTeXDriver::
init( FlatTeXDriver* Parent )
{
  m_mode = MODE_TTH;
  m_parent = Parent;
  if ( Parent )
  {
    m_booleans = Parent->m_booleans;
  }
  else
  {
    m_booleans = &m_booleans_local;
  }
  m_expression = EXPRESSION_UNKNOWN;
}

std::ostream& FlatTeXDriver::
output( )
{
  return m_scanner->YYOut( );
}

void FlatTeXDriver::
output( const char* Text )
{
  m_scanner->YYOut( ) << Text << std::flush;
}

void FlatTeXDriver::
output( const std::string& Text )
{
  m_scanner->YYOut( ) << Text << std::flush;
}

void FlatTeXDriver::
output( const SType& Info )
{
  m_scanner->YYOut( ) << Info << std::flush;
}
