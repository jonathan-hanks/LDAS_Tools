#include <ctype.h>
#include <stdarg.h>

#include <list>
#include <map>
#include <sstream>
#include <vector>

#define WITHOUT_AT_EXIT_CLASS
#include "general/unittest.h"

#include "FlatTeXDriver.hh"

namespace
{
  int	ParserMode = FlatTeXDriver::MODE_TTH;

  typedef std::map< std::string, bool >	boolean_table_type;
  //---------------------------------------------------------------------
  // Global variables local to this file
  //---------------------------------------------------------------------
  General::UnitTest 	Test;
  boolean_table_type	BooleanTable;

  bool
  get_boolean( const std::string& Variable )
  {
    boolean_table_type::const_iterator cur = BooleanTable.find( Variable );
    if ( cur == BooleanTable.end( ) )
    {
      return false;
    }
    return (*cur).second;
  }

  inline void
  set_boolean( const std::string& Variable, bool Value )
  {
    BooleanTable[ Variable ] = Value;
  }

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class base
  {
  public:
    typedef enum {
      IN,
      OUT
    } MODE;

    virtual ~base( )
    {
      for ( std::list< base* >::iterator
	      cur = m_free_list.begin( ),
	      last = m_free_list.end( );
	    cur != last;
	    ++cur )
      {
	delete *cur;
	*cur = NULL;
      }
    }
    virtual std::string operator()( MODE Mode ) const = 0;

    static std::string
    strip_comments( const std::string& Text )
    {
      if ( FlatTeXDriver::MODE_TTH != ParserMode )
      {
	std::string	msg;

	for ( std::string::const_iterator
		cur = Text.begin( ),
		last = Text.end( );
	      cur != last;
	      ++cur )
	{
	  if ( *cur == '\\' )
	  {
	    msg += *cur;
	    ++cur;
	    msg += *cur;
	  } else if ( *cur == '%' ) {
	    while( ( cur != last ) && ( *cur != '\n' ) )
	    {
	      ++cur;
	    }
	    if ( cur == last )
	    {
	      --cur;
	    }
	  } else {
	    msg += *cur;
	  }
	}
	return msg;
      }
      else
      {
	return Text;
      }
    }

    base*
    reg( base* Base )
    {
      m_free_list.push_back( Base );
      return Base;
    }
  private:
    std::list< base* >	m_free_list;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class block
    : public base
  {
  public:
    std::string
    operator()( base::MODE Mode ) const
    {
      std::ostringstream	block;
      for( std::list< base* >::const_iterator
	     cur = m_stmts.begin( ),
	     last = m_stmts.end( );
	   cur != last;
	   ++cur )
      {
	if ( (*cur) )
	{
	  block << (*(*cur))( Mode );
	}
      }
      return block.str( );
    }

    void append( base* Base, bool Register = true )
    {
      if ( Register )
      {
	reg( Base );
      }
      m_stmts.push_back( Base );
    }

  private:
    std::list< base* > m_stmts;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class boolean_assign
    : public base
  {
  public:
    boolean_assign( const std::string& Variable, bool Value )
      : m_variable( Variable ),
	m_value( Value )
    {
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      std::ostringstream	doc;
      if ( Mode == base::IN )
      {
	doc << "\\" << m_variable << ( (m_value ) ? "true" : "false" )
	    << std::endl;
      }
      else
      {
	if ( ParserMode != FlatTeXDriver::MODE_TTH )
	{
	  doc << "\\" << m_variable << ( (m_value ) ? "true" : "false" )
	      << std::endl;
	}
	set_boolean( m_variable, m_value );
      }
      return doc.str( );
    }

  private:
    std::string	m_variable;
    bool	m_value;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class def
    : public base
  {
  public:
    def( const std::string& Cmd, base* Definition )
      : m_name( Cmd ),
	m_def( Definition ),
	m_param_count( 0 )
    {
      reg( m_def );
    }

    def( const std::string& Cmd,
	 base* Definition,
	 unsigned int ParamCount )
      : m_name( Cmd ),
	m_def( Definition ),
	m_param_count( ParamCount )
    {
      reg( m_def );
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      if ( Mode == base::IN )
      {
	std::ostringstream	retval;
	retval << "\\def\\" << m_name;
	if ( m_param_count )
	{
	  retval << "#" << m_param_count;
	}
	retval << "{" << (*m_def)( base::IN ) << "}" << std::endl;
	return retval.str( );
      }
      return( "\n" );
    }
  private:
    std::string		m_name;
    base*		m_def;
    unsigned int	m_param_count;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class document
    : public base
  {
  public:
    document( )
      : m_block( NULL )
    {
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      std::ostringstream	doc;
      doc << "\\documentclass{article}" << std::endl
	  << "\\begin{document}" << std::endl
	;
      if ( m_block )
      {
	doc << (*m_block)( Mode );
      }
      doc << "\\end{document}" << std::endl
	;
      return doc.str( );
    }
  private:
    block*	m_block;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class if_else_fi
    : public base
  {
  public:
    if_else_fi( const std::string& BooleanVar,
		base* TrueBlock,
		base* FalseBlock )
      : m_var( BooleanVar ),
	m_true( TrueBlock ),
	m_false( FalseBlock )
    {
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      std::ostringstream	doc;

      if ( Mode == base::IN )
      {
	doc << "\\if" << m_var << std::endl;
	if ( m_true )
	{
	  doc << (*m_true)( Mode );
	}
	doc << "\\else" << std::endl;
	if ( m_false )
	{
	  doc << (*m_false)( Mode );
	}
	doc << "\\fi" << std::endl;
      }
      else
      {
	doc << std::endl; // if
	doc << std::endl; // else
	if ( get_boolean( m_var ) )
	{
	  if ( m_true )
	  {
	    doc << (*m_true)( Mode );
	  }
	}
	doc << std::endl; // fi
	if ( ! get_boolean( m_var ) )
	{
	  if ( m_false )
	  {
	    doc << (*m_false)( Mode );
	  }
	}
      }
      return doc.str( );
    }
  private:
    std::string	m_var;
    base*	m_true;
    base*	m_false;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class ifthenelse
    : public base
  {
  public:
    ifthenelse( const std::string& BooleanVar,
		base* TrueBlock,
		base* FalseBlock )
      : m_var( BooleanVar ),
	m_true( TrueBlock ),
	m_false( FalseBlock )
    {
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      std::ostringstream	doc;

      if ( Mode == base::IN )
      {
	doc << "\\ifthenelse{\\boolean{" << m_var << "}}{";
	if ( m_true )
	{
	  doc << (*m_true)( Mode );
	}
	doc << "}{" ;
	if ( m_false )
	{
	  doc << (*m_false)( Mode );
	}
	doc << "}";
      }
      else
      {
	if ( get_boolean( m_var ) )
	{
	  if ( m_true )
	  {
	    doc << (*m_true)( Mode );
	  }
	}
	else
	{
	  if ( m_false )
	  {
	    doc << (*m_false)( Mode );
	  }
	}
      }
      return doc.str( );
    }
  private:
    std::string	m_var;
    base*	m_true;
    base*	m_false;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class newboolean
    : public base
  {
  public:
    newboolean( const std::string& Name )
      : m_name( Name )
    {
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      std::ostringstream	retval;
      if ( ! ( ( FlatTeXDriver::MODE_TTH == ParserMode ) &&
	       ( base::OUT == Mode ) ) )
      {
	retval << "\\newboolean{" << m_name << "}";
      }
      return retval.str( );
    }

  private:
    std::string	m_name;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class newcommand
    : public base
  {
  public:
    newcommand( const std::string& Cmd, base* Definition )
      : m_name( Cmd ),
	m_def( Definition ),
	m_param_count( 0 )
    {
      reg( m_def );
    }

    newcommand( const std::string& Cmd,
		base* Definition,
		unsigned int ParamCount )
      : m_name( Cmd ),
	m_def( Definition ),
	m_param_count( ParamCount )
    {
      reg( m_def );
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      if ( Mode == base::IN )
      {
	std::ostringstream	retval;
	retval << "\\newcommand{\\"
	       << m_name << "}";
	if ( m_param_count )
	{
	  retval << "[" << m_param_count << "]";
	}
	retval << "{" << (*m_def)( base::IN ) << "}" << std::endl;
	return retval.str( );
      }
      return( "\n" );
    }
  private:
    std::string		m_name;
    base*		m_def;
    unsigned int	m_param_count;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class setboolean
    : public base
  {
  public:
    setboolean( const std::string& Name, bool Value )
      : m_name( Name ),
	m_value( Value )
    {
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      std::ostringstream	retval;
      if ( ! ( ( FlatTeXDriver::MODE_TTH == ParserMode ) &&
	       ( base::OUT == Mode ) ) )
      {
	retval << "\\setboolean{" << m_name << "}{"
	       << ( ( m_value ) ? "true" : "false" ) << "}";
      }
      if ( Mode == base::OUT )
      {
	set_boolean( m_name , m_value );
      }
      return retval.str( );
    }

  private:
    std::string	m_name;
    bool	m_value;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class substitute
    : public base
  {
  public:
    substitute( const std::string& Name, base* Definition )
      : m_name( Name ),
	m_def( Definition ),
	m_param_count( 0 )
    {
      // :TRICKY: Do not register Definition for deletion since it is
      // :TRICKY:   the responsibility of the caller to do so
    }

    substitute( const std::string& Name, base* Definition,
		unsigned int ParamCount, ... )
      : m_name( Name ),
	m_def( Definition ),
	m_param_count( ParamCount )
    {
      // :TRICKY: Do not register Definition for deletion since it is
      // :TRICKY:   the responsibility of the caller to do so
      base*	arg;

      va_list	ap;
      va_start( ap, ParamCount );
      for ( unsigned int x = 0; x < m_param_count; x++ )
      {
	arg = va_arg( ap, base* );
	m_args.push_back( reg( arg ) );
      }
      va_end( ap );
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      if ( Mode == base::IN )
      {
	std::ostringstream	retval;

	retval << "\\" << m_name;
	for ( std::vector< base* >::const_iterator
		cur = m_args.begin( ),
		last = m_args.end( );
	      cur != last;
	      ++cur )
	{
	  retval << "{" << (*(*cur))( Mode ) << "}";
	}
	return retval.str( );
      }
      std::string	def( (*m_def)( base::OUT ) );
      std::ostringstream	retval;

      for ( std::string::const_iterator
	      cur = def.begin( ),
	      last = def.end( );
	    cur != last;
	    ++cur )
      {
	if ( *cur == '\\' )
	{
	  retval << *(cur++) << (*cur);
	} else if ( *cur == '#' ) {
	  ++cur;
	  std::string	num;
	  while( isdigit( *cur ) )
	  {
	    num += *cur;
	    ++cur;
	  }
	  --cur;
	  retval << (*(m_args[ atoi( num.c_str( ) ) - 1 ]))( base::OUT );
	} else {
	  retval << (*cur);
	}
      }
      return retval.str( );
    }

  private:
    std::string			m_name;
    base*			m_def;
    unsigned int		m_param_count;
    std::vector< base* >	m_args;
  };

  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  class text
    : public base
  {
  public:
    text( const std::string& Desc )
      : m_text( Desc )
    {
    }

    std::string
    operator()( base::MODE Mode ) const
    {
      if ( Mode == base::IN )
      {
	return m_text;
      }
      return( strip_comments( m_text ) );
    }

  private:
    std::string	m_text;
  };

} // namespace - anonymous


//=======================================================================
// Various tests
//=======================================================================
bool
lexit( const base& Test )
{
  //:TODO: Need to create a parser shell around this to make it work
  std::istringstream in( Test( base::IN ) );
  std::ostringstream out;

  FlatTeXLexer	lexer( &in, &out );

  std::cerr << Test( base::OUT ) << std::endl
	    << "=?="
	    << out.str( ) << std::endl;
  return ( Test( base::OUT ).compare( out.str( ) ) == 0 );
}

bool
parseit( const base& Test )
{
  BooleanTable.erase( BooleanTable.begin( ),
		      BooleanTable.end( ) );	// erase any old values
  ::Test.Message( 50 ) << Test( base::IN ) << std::endl;

  std::istringstream in( Test( base::IN ) );
  std::ostringstream out;

  FlatTeXDriver	driver;

  driver.Parse( &in, &out );

  bool	retval = ( Test( base::OUT ).compare( out.str( ) ) == 0 );
  if ( ( retval == false ) || ::Test.IsVerbose( 30 ) )
  {
    ::Test.Message( 20 )
      << Test( base::OUT ) << " =?= " << out.str( ) << std::endl;
  }
  return retval;
}

//-----------------------------------------------------------------------
// Validate the trivial of latex documents
//-----------------------------------------------------------------------
void
trivial_test( )
{
  document	d;

  // Test.Check( lexit( d ) ) << "trivial - lexer analysis" << std::endl;
  Test.Check( parseit( d ) ) << "trivial - parser analysis" << std::endl;
}

void
comment_test( )
{
  block	b;
  b.append( new text( "%This is a comment" ) );

  Test.Check( parseit( b ) ) << "comment - parser analysis" << std::endl;
}

void
def_1_test( )
{
  block	b;
  base* hw = new text( "hello world" );
  b.append( new def( "hw", hw ) );
  b.append( new substitute( "hw", hw ) );

  Test.Check( parseit( b ) )
	    << "def_1_test - parser analysis"
	    << std::endl;
}

void
newcommand_test( )
{
  block	b;
  b.append( new newcommand( "hw",
			    new text( "hello world" ) ) );

  Test.Check( parseit( b ) ) << "newcommand - parser analysis" << std::endl;
}

void
newcommand_2_test( )
{
  block	b;
  b.append( new newcommand( "hw",
			    new text( "hello #1" ), 1 ) );

  Test.Message( 40 ) << "INFO: newcommand_2_test: "
		     << b( base::IN ) << std::endl;
  Test.Check( parseit( b ) )
    << "newcommand_2_test - parser analysis"
    << std::endl;
}

void
substitution_1_test( )
{
  block	b;
  base* hw = new text( "hello world" );
  b.append( new newcommand( "hw", hw ) );
  b.append( new substitute( "hw", hw ) );

  Test.Check( parseit( b ) )
	    << "substitution_1_test - parser analysis"
	    << std::endl;
}

void
substitution_2_test( )
{
  block	b;
  base* hw = new text( "hello world" );
  base* hwcall = new substitute( "hw", hw );
  block* dhw = new block( );
  dhw->append( hwcall);
  dhw->append( new text( " big blue " ) );
  dhw->append( hwcall, false );
  b.append( new newcommand( "hw", hw ) );
  b.append( new newcommand( "dhw", dhw) );
  b.append( new substitute( "dhw", dhw ) );

  Test.Check( parseit( b ) )
	    << "substitution_2_test - parser analysis"
	    << std::endl;
}

void
substitution_3_test( )
{
  block	b;
  base* hw = new text( "hello #1" );
  base* world = new text( "world" );
  b.append( new newcommand( "hw", hw, 1 ) );
  b.append( new substitute( "hw", hw, 1, world ) );

  Test.Check( parseit( b ) )
    << "substitution_3_test - parser analysis"
    << std::endl;
}

void
substitution_4_test( )
{
  block	b;
  base* arg = new text( "{\\rm\\it #1}" );
  base* func = new text( "{\\bf #1}" );
  base* datacondcaller = new
    text ( "int\n"
	   "DatacondCaller( const char* \\arg{Algorithm},\n"
	   "                const char* \\arg{Aliases},\n"
	   "                datacond_symbol_type* \\arg{Symbols},\n"
	   "                int	\\arg{NumberOfSymbols},\n"
	   "                char** \\arg{ErrorMessage} );\n" );
  b.append( new newcommand( "arg", arg, 1 ) );
  b.append( new newcommand( "func", func, 1 ) );
  b.append( new substitute( "func", func, 1, datacondcaller ) );

  Test.Check( parseit( b ) )
    << "substitution_4_test - parser analysis"
    << std::endl;
}

void
if_else_fi_1_test( )
{
  block b;

  b.append( new text("\\newif\\ifgreeting\n" ) );
  b.append( new boolean_assign( "greeting", true ) );
  b.append( new if_else_fi( "greeting",
			    new text( "hello\n" ),
			    new text( "goodbye\n" ) ) );
  b.append( new text( "world\n" ) );
  Test.Check( parseit( b ) )
    << "if_else_fi_1_test - parser analysis"
    << std::endl;
}

void
if_else_fi_2_test( )
{
  block b;
  b.append( new text("\\newif\\ifgreeting\n" ) );
  b.append( new text("\\newif\\ifpos\n" ) );
  b.append( new boolean_assign( "greeting", true ) );
  b.append( new boolean_assign( "pos", true ) );
  b.append( new if_else_fi( "greeting",
			    new if_else_fi( "pos" ,
					    new text( "very happy hello\n" ),
					    new text( "not so happy hello\n" )
					    ),
			    new text( "goodbye\n" ) ) );
  b.append( new text( "world\n" ) );
  Test.Check( parseit( b ) )
    << "if_else_fi_2_test - parser analysis"
    << std::endl;
}

void
ifthenelse_1_test( )
{
  block b;

  b.append( new newboolean("greeting" ) );
  b.append( new text( "\n" ) );
  b.append( new setboolean( "greeting", true ) );
  b.append( new text( "\n" ) );
  b.append( new ifthenelse( "greeting",
			    new text( "hello" ),
			    new text( "goodbye" ) ) );
  b.append( new text( "world\n" ) );
  Test.Check( parseit( b ) )
    << "ifthenelse_1_test - parser analysis"
    << std::endl;
}

//-----------------------------------------------------------------------
// MAIN
//-----------------------------------------------------------------------

int
main(int ArgC, char** ArgV)
{
  //---------------------------------------------------------------------
  // Initialize the test structure
  //---------------------------------------------------------------------
  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // :TODO: Run through a series of tests
  //---------------------------------------------------------------------

  trivial_test( );
  comment_test( );

  newcommand_test( );
  newcommand_2_test( );

  substitution_1_test( );
  substitution_2_test( );
  substitution_3_test( );
#if 0
  //:TODO: Need to see about getting this one to work with producing
  //:TODO:   the correct comparison string. The string from the
  //:TODO:   bison parser is correct.
  substitution_4_test( );
#endif /* 0 */

  if_else_fi_1_test( );
  if_else_fi_2_test( );

  def_1_test( );

  ifthenelse_1_test( );

  //---------------------------------------------------------------------
  // Exit with the appropriate exit status
  //---------------------------------------------------------------------
  Test.Exit();
  return 1;
}
