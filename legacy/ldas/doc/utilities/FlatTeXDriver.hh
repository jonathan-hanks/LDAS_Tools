#ifndef FLATTEX_DRIVER_HH
#define	FLATTEX_DRIVER_HH

#include <iostream>
#include <list>
#include <map>
#include <memory>
#include <string>
#include <sstream>
#include <vector>

#include "general/unordered_map.hh"

// Forward declaration
class FlatTeXDriver;
class SType;

#include "FlatTeXLexer.hh"
#include "FlatTeXParser.hh"

//: Handling of symantic data type
std::ostream& operator<<( std::ostream& Stream, const SType& Info );

//: Handling of syntatical input of TeX sources
class FlatTeXDriver
{
public:
  typedef yy::FlatTeXParser::token_type token_type;

  enum {
    EXPRESSION_UNKNOWN,
    EXPRESSION_TRUE,
    EXPRESSION_FALSE,
    EXPRESSION_STRING
  };

  enum {
    MODE_TTH,
    MODE_LATEX
  };

  FlatTeXDriver( FlatTeXDriver* Parent = (FlatTeXDriver*)NULL );
  FlatTeXDriver( const char* Filename,
		 FlatTeXDriver* Parent = (FlatTeXDriver*)NULL );
  ~FlatTeXDriver( );

  inline bool
  Boolean( const std::string& Name ) const
  {
    booleans_map_type::const_iterator	cur = m_booleans->find( Name );

    if ( cur != m_booleans->end( ) ) {
      return (*cur).second;
    }
    return false;
  }

  inline void
  Boolean( const std::string& Name, const bool Value )
  {
    (*m_booleans)[ Name ] = Value;
  }

  int EvalCondition( const std::string& Expression );

  int Expression( ) const;

  inline void
  Expression( const bool Value )
  {
    m_expression = ( Value )
      ? FlatTeXDriver::EXPRESSION_TRUE
      : FlatTeXDriver::EXPRESSION_FALSE;
  }

  inline void
  Expression( const int Value )
  {
    m_expression = Value;
  }

  void Evaluate( const std::string& Buffer,
		 std::ostream* Out = (std::ostream*)NULL );

  inline bool
  HaveBoolean( const std::string& Name ) const
  {
    return ( m_booleans->find( Name ) != m_booleans->end( ) );
  }

  token_type Lex( yy::FlatTeXParser::semantic_type* YYLval,
		  yy::FlatTeXParser::location_type* YYLloc );

  inline int
  Mode( ) const
  {
    return m_mode;
  }

  void Parse( std::istream* In = (std::istream*)NULL,
	      std::ostream* Out = (std::ostream*)NULL );

  std::ostream& output( );

  void output( const char* Text );

  void output( const std::string& Text );

  void output( const SType& Info );

  void yyerror( std::string Message ) const;

  bool GetCommandInfo( const std::string& Name,
		       unsigned int& NumOfParams,
		       std::string& Definition	) const;
  void NewCommand( const std::string& Name,
		   const int ParameterCount,
		   const std::string& Definition );
  void RenewCommand( const std::string& Name,
		     const int ParameterCount,
		     const std::string& Definition );
private:
  typedef std::map< std::string, bool >	booleans_map_type;

  booleans_map_type*	m_booleans;
  booleans_map_type	m_booleans_local;

  int			m_expression;
  int			m_mode;

  std::auto_ptr< FlatTeXLexer >		m_scanner;
  FlatTeXDriver*			m_parent;

  void init( FlatTeXDriver* Driver );

  inline FlatTeXLexer* get_lexer( )
  {
    return m_scanner.get( );
  }

};

inline int FlatTeXDriver::
Expression( ) const
{
  return m_expression;
}

inline bool FlatTeXDriver::
GetCommandInfo( const std::string& Name,
		unsigned int& NumOfParams,
		std::string& Definition	) const
{
  return m_scanner->getCommandInfo( Name, NumOfParams, Definition );

}

inline void FlatTeXDriver::
NewCommand( const std::string& Name,
	    const int ParameterCount,
	    const std::string& Definition )
{
  m_scanner->newCommand( Name, ParameterCount, Definition );
}

inline void FlatTeXDriver::
RenewCommand( const std::string& Name,
	      const int ParameterCount,
	      const std::string& Definition )
{
  m_scanner->renewCommand( Name, ParameterCount, Definition );
}
#endif	/* FLATTEX_HH */
