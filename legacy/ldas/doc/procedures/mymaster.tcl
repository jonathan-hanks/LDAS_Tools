#!/ldcg/bin/tclsh

proc runScripts {files} {
    foreach file $files {
        catch {file attributes $file -permissions u+x}
        set errfile [file join /tmp [file rootname $file].[pid]]

        catch {exec ./$file 2>$errfile} msg

        ;## get any errors
        set err {}
        catch {
            set fd [open $errfile "r"]
            set err [read -nonewline $fd]
            close $fd
            file delete -- $errfile
        }

        if {[string length $err]} {
            puts stderr "Errors for $file:\n$err\n"
        }

        puts stdout "${file}:"; flush stdout
        ;## get jobids
        set idx 0
        while {1} {
            if {![regexp -indices -start $idx -- {([A-Za-z\-\_]+\d+)} $msg -> indices]} {
                break
            }
            set jobid [eval string range \$msg $indices]
            set idx [expr {[lindex $indices 1] + 1}]
            puts stdout " ${jobid}"; flush stdout
        }

        ;## sleep for a bit
        after 10000 
    }
}

;## MAIN ##
set exceptionList {
    arithapi01a.tcl
    coherenceapi01a.tcl
    coherenceapi01b.tcl
    coherenceapi01c.tcl
    coherenceapi01d.tcl
    complexapi01a.tcl
    complexapi01b.tcl
    complexapi01c.tcl
    fftapi01a.tcl
    fftapi01b.tcl
    linfiltapi01a.tcl
    linfiltapi01b.tcl
    linfiltapi01c.tcl
    mixerapi01a.tcl
    mixerapi01b.tcl
    psdapi01a.tcl
    psdapi01b.tcl
    psdapi01c.tcl
    psdapi01d.tcl
    resampleapi01a.tcl
    resampleapi01b.tcl
    resampleapi01c.tcl
    resampleapi01d.tcl
    shiftapi01a.tcl
    signumapi01a.tcl
    signumapi01b.tcl
    signumapi01c.tcl
    signumapi01d.tcl
    statapi01.tcl
    tseriesapi01a.tcl
    waveletapi01a.tcl
    waveletapi01b.tcl
    waveletapi01c.tcl
    waveletapi01d.tcl
    waveletapi01e.tcl
    waveletapi01f.tcl
    waveletapi01g.tcl
    waveletapi01h.tcl
    waveletapi01i.tcl
    waveletapi01k.tcl
}

set successList {
    arithapi02a.tcl
    coherenceapi02a.tcl
    coherenceapi02b.tcl
    coherenceapi02c.tcl
    complexapi02a.tcl
    fftapi02a.tcl
    fftapi02b.tcl
    fftapi02c.tcl
    fftapi02d.tcl
    fftapi02e.tcl
    interpolateapi02a.tcl
    linfiltapi02a.tcl
    linfiltapi02b.tcl
    mixerapi02a.tcl
    mixerapi02b.tcl
    pipe01.tcl
    psdapi02a.tcl
    psdapi02b.tcl
    psdapi02c.tcl
    psdapi02d.tcl
    resampleapi02a.tcl
    resampleapi02b.tcl
    resampleapi02c.tcl
    resampleapi02d.tcl
    resampleapi02e.tcl
    realimagabsapi01.tcl
    shiftapi02a.tcl
    shiftapi02b.tcl
    siggenapi02a.tcl
    signumapi02a.tcl
    statapi02.tcl
    tseriesapi02a.tcl
    waveletapi02a.tcl
}

set testList [concat $exceptionList $successList]

for {set idx 0} {$idx < $::argc} {incr idx} {
    set opt [lindex $::argv $idx]
    switch -glob -- $opt {
        -e {set testList [join $exceptionList]}
        -s {set testList [join $successList]}
        -t {set testList [lindex $::argv [incr idx]]}
        -* {
            puts stderr "Error: Invalid option: $opt"
            exit 1
        }
        default {}
    }
}

runScripts $testList

