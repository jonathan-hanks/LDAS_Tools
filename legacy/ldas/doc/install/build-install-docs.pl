#!/usr/bin/env perl
$start = 0;
$depth = 1;
$in_quote = 0;
$quote="blockquote";
$backslash_text = "__text_back_slash__";
$bad_tag = "__no_such_tag__";
$tag = $bad_tag;
$contents[++$#contents] = "<ul>";

my $SOURCE = "INSTALL";
my $PROJECT;

foreach $arg ( @ARGV )
{
    if ($arg =~ /^--source=(.*)/)
    {
	$SOURCE = $1;
	# $SOURCE =~ s/--source=//g;
    }
    else
    {
	$PROJECT = $arg;
    }
}

open INSTALL_LYX, "< $SOURCE.lyx";
while(<INSTALL_LYX>)
{
    if ($_ =~ /^Project\}\{\{(.*)\}\}$/)
    {
	$PROJECT = $1;
    }
}
close INSTALL_LYX;
mkdir "tth", 0700;
mkdir "tth/lib", 0700;
foreach $file (glob "*.tex")
{
    system("sed -e 's/\\textbackslash\{\}/$backslash_text/g'  $file > tth/$file");
}
foreach $file (glob "../lib/*.tex")
{
    my($f);

    $f = `basename $file`;
    system("sed -e 's/\\textbackslash\{\}/$backslash_text/g'  $file > tth/lib/$f");
}
chdir "tth";
open TTH, "cat  $SOURCE.tex | tth -plib |";
while(<TTH>)
{
    chop;
    s/\s*\\Project~/$PROJECT /g;
    s/\s*\\Project\\/$PROJECT/g;
    if (! $start)
    {
	if (/^<html>$/)
	{
	    $start = 1;
	}
	else
	{
	    next;
	}
    }
    s/-&nbsp;-/--/g;
    # Create reference
    s/((http|telnet|gopher|file|wais|ftp):\/\/([\w\/\#~:.?+=&%@!\-]+?)(?=[.:?\-]*[^\w\/\#~:.?+=&%@!\-]|$))/<a href=$1>$1<\/a>/og &&
       s/<(\/)?tt>//og;
    # Translate block quotes to Plain Text Format
    s/<(\/)?BLOCKQUOTE>/<$1$quote>/ig;
    if ($in_quote == 1)
    {
	s/^<p>$/<br>/i;
	/<\/$quote>/i && { $in_quote = 0 };
    }
    else
    {
	/<$quote>/i && { $in_quote = 1};
    }
    if (/^.tar.gz/)
    {
	$lines[$#lines] =~ s/((http|telnet|gopher|file|wais|ftp):\/\/([\w\/\#~:.?+=&%@!\-]+?)(?=[.:?\-]*[^\w\/\#~:.?+=&%@!\-]|$))/$1.tar.gz/og;
    }
    else
    {
	$lines[++$#lines] = $_;
    }
    # Fix html references that end with a punctuation mark.
    $lines[$#lines] =~ s/<a href=(.*)([.?!])>.*<\/a>/<a href=$1>$1<\/a>$2/og;
    /^<title>.*<\/title>$/ && { $title_line = $. };
    if ($label eq "")
    {
	if (/<h([1234])><a name="([^\"]*)">/o)
	{
	    $header=$1;
	    $label=$2;
	}
    }
    else
    {
	if ($header > $depth)
	{
	    while ($depth < $header)
	    {
		$depth++;
		$contents[++$#contents] = "<ul>";
	    }
	}
	if ($header < $depth)
	{
	    while ($depth > $header)
	    {
		$depth--;
		$contents[++$#contents] = "</ul>";
	    }
	}
	if (/^<\/a>($tag.*)<\/h$header>/)
	{
	    $contents[$#contents] .= "$1</a>";
	    $tag = $bad_tag;
	    $label = "";
	}
	elsif (/(\d+(((\.\d+)?\.\d+)?\.\d+)?)<\/a>&nbsp;&nbsp;(.*)<\/h$header>/)
	{
	    $contents[++$#contents] = "<li><a href=\"$SOURCE.html#$label\">$1&nbsp;$5</a>";
	    $label = "";
	}
	elsif (/(\d+(((\.\d+)?\.\d+)?\.\d+)?)<\/a>&nbsp;&nbsp;<a name="(.*)">/)
	{
	    $contents[++$#contents] = "<li><a href=\"$SOURCE.html#$label\">$1&nbsp;";
	    $tag = $5;
	}
    }
}
while ($depth > 1)
{
    $contents[++$#contents] = "</ul>";
    $depth--;
}
close TTH;
chdir "..";
#------------------------------------------------------------------------
# Need to put back in back slash
#------------------------------------------------------------------------

$line_continue_state = 0;

for ($i = $#lines; $i >= 1; $i--)
{
    $lines[$i] =~ s/$backslash_text/\\/g;
}

#------------------------------------------------------------------------
# Write out the text and html versions of the documentation.
#------------------------------------------------------------------------

$line_continue_state = 0;
open HTML, "> $SOURCE.html";
open LYNX_HTML, "> $SOURCE.lynx.html";
$title_line++;
for ($i = 1; $i <= $title_line; $i++)
{
    print HTML "$lines[$i]\n";
    $lines[$i] =~ s/href=\"[^\"]*\"//g;
    $lines[$i] =~ s/href=\W*//g;
    print LYNX_HTML "$lines[$i]\n";
}
print HTML "<h2>Contents</h2>\n";
print LYNX_HTML "<h2>Contents</h2>\n";
for ($i = 1; $i <= $#contents; $i++)
{
    #--------------------------------------------------------------------
    # Fix table of contents to have tags relative to the current file
    #--------------------------------------------------------------------
    $contents[$i] =~ s/href=\"$SOURCE.html\#/href=\"\#/g;
    print HTML "$contents[$i]\n";
    #--------------------------------------------------------------------
    # Remove hreferences for text version of file
    #--------------------------------------------------------------------
    $contents[$i] =~ s/href=\"[^\"]*\"//g;
    $contents[$i] =~ s/href=\W*//g;
    print LYNX_HTML "$contents[$i]\n";
}
$title_line++;
$line_continue_pattern = "\\s*\\\\\\s*\$";
for ($i = $title_line; $i <= $#lines; $i++)
{
    #--------------------------------------------------------------------
    # For HTML documentation, splice together the multilines
    #--------------------------------------------------------------------
    $html_out = $lines[$i];
    $lynx_out = $lines[$i];
    if ($line_continue_state == 0)
    {
	$html_out =~ /$line_continue_pattern/ && { $line_continue_state = 1 };
	$html_out =~ s/$line_continue_pattern/ /;
    }
    elsif ($line_continue_state == 1)
    {
	$html_out =~ s/^\s*$//;
	$html_out =~ s/^<(p|br)>$//i;
	$html_out =~ s/^&nbsp;&nbsp;//i;
	$html_out !~ /$line_continue_pattern/ &&
	    $html_out !~ /^$/ &&
	    { $line_continue_state = 0 };
	$html_out =~ s/$line_continue_pattern/ /;
    }
    print HTML "$html_out";
    if ($line_continue_state == 0)
    {
	print HTML "\n";
    }
    $lynx_out =~ s/href=\"[^\"]*\"//g;
    $lynx_out =~ s/href=\W*//g;
    print LYNX_HTML "$lynx_out\n";
}
close LYNX_HTML;
close HTML;
system("lynx -hiddenlinks=ignore -image_links -dump $SOURCE.lynx.html > $SOURCE.txt");
unlink "$SOURCE.lynx.html";
system("rm -rf tth");
