#!/ldcg/bin/tclsh
#
## This is the server script for the mock LDAS interface.
## At this point it successfully returns the contents of
## a file to the client which the client sources for it's
## proc's.

## last modified 110398
;#barecode
source genericAPI.tcl

## get resources file with preferences
## or die ignominiously...

if { [ catch { sourceFile serverAPI.rsc } mess ] } {
          return -code error $mess
          }

set operator [ openListenSock $localhost operator safe ]
set emergency [ openListenSock $localhost emergency ]

## create an alias to allow client access to files.
## ********************************************************
interp alias $operator open {} \
safe_open $operator $rootPath

##
## ********************************************************
interp alias $operator fconfigure {} \
safe_fconfigure $operator

## create an alias to allow limited exec'ing
## of server commands
## ********************************************************
interp alias $operator exec {} \
safe_exec $operator

## create an alias to allow the slave to cd "home"
## ********************************************************
interp alias $operator cd {} \
safe_cd $operator $rootPath

## create an alias to allow safe globbing
## ********************************************************
interp alias $operator glob {} \
safe_glob $operator *

## create an alias to allow safe file querying
## ********************************************************
interp alias $operator file {} \
safe_file $operator $rootPath

## procedures to be invoked in the master interpreter when
## the slave needs access to "unsafe" commands.
## ********************************************************
proc safe_open { parser path name { mode r } } {
     interp invokehidden $parser \
     open [ file join $path [ file tail $name ] ] $mode
}

proc safe_fconfigure { parser fid opt val } {
     interp invokehidden $parser \
     fconfigure $fid $opt $val
}     

proc safe_exec { parser command } {
     if { [ string match ls $command ] } {
        interp invokehidden $parser \
        exec $command
        }
}        

proc safe_glob { parser pattern } {
     interp invokehidden $parser \
     glob -nocomplain $pattern
}

proc safe_cd { parser path } {
     interp invokehidden $parser \
     cd $path
}     

## look out! syntax is eg: "file foo exists"
proc safe_file { parser path name { action exists } } {
     interp invokehidden $parser \
     file $action [ file join $path [ file tail $name ] ]
}

## the main loop for the slave interpreter
## ********************************************************
;#barecode
$operator eval {

    proc parse_cmd { { args "" } } {
         return [ eval [ lindex  $args 1 ] ]
         }

## get the contents of a file         
    proc get_data { filename } {
         if { [ file $filename exists ] } {
            set fid [ open $filename r ]
            set data [ read $fid ]
            close $fid
            return $data
            }
         return {}   
    }

## get the remote directory listing         
    proc get_ls {}  {
         cd;
         set FileList [ glob ]
         return $FileList
    }
}

 vwait enter-mainloop

