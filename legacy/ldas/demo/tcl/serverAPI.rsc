## root path of files to make accessible
set rootPath $LDAS 

## machine name
set localhost $env(HOST)
set hostname $localhost

## name of a remote host we want to talk to
set remotehost "maasim"

variable REMOTE_HOSTS { maasim }

## listening ports aliased on this machine,
## port #'s are {}'ed to stress that they are lists.
array set $localhost { operator  { 9001 } }
array set $localhost { emergency { 9002 } }

