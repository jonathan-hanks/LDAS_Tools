#!/usr/ldas/ldas/bin/tclsh
##
## This is the client side script for the mock LDAS
## interface.  It sets up a socket connection and
## requests code from the server which it sources.
## The client has it's own local help and message
## service, so if it can't connect it can talk about
## it and ask for help.  
## All other client services are loaded at runtime
## from the server, so the client does not need to
## be updated.
## some facility may be added for automatically
## copying new help extensions on start-up.

## we're dynamically loading Tk now
## note the "vwait" at the end of the script
## which instantiates the event loop.  This is
## required when not running "WISH" directly.

package require http 1.0
package require Tk

## BLT is not currently used
## but this is the syntax to get it
#package require BLT
#namespace import blt::*
#bltdebug 1

## settings for the app window
## ********************************************************
wm minsize . 500 500
wm geometry . +80+50
wm title . "LIGO LDAS User Interface"
set screen_W [ winfo screenwidth  . ]
set screen_H [ winfo screenheight . ]
## ********************************************************


## The Main Menu
## ********************************************************
proc build_menu {} {
upvar #0 operator operator
. configure -menu .mbar -borderwidth 2 \
                        -relief raised
menu .mbar
     ## not a real menu item
     .mbar add cascade -label "Setup" \
                       -menu .mbar.setup
      menu .mbar.setup
           .mbar.setup add command -label "exit" \
           -command exit
     ## not sure what this SHOULD do!
     .mbar add cascade -label "Data" \
                       -menu .mbar.data
      menu .mbar.data  
           .mbar.data add command -label "hanford" \
           -command { popMsg "unimplemented" }
           .mbar.data add separator
           .mbar.data add command -label "livingston" \
           -command { popMsg "unimplemented" }    
     ## local and remote file browsing
     .mbar add cascade  -label "Browse" \
                        -menu .mbar.browse
     menu .mbar.browse
          .mbar.browse add command -label "local" \
          -command {
                    set filename [ fileDialog . open "" "" ]
                    if { [ string length $filename ] } {
                       set txt [ dumpFile $filename ]
                       textBox $txt $filename
                       }
                    }
          .mbar.browse add separator
          .mbar.browse add command -label "remote" \
          -command { 
                   if { [ string match "unreachable" $operator ] } {
                       popMsg "server not responding"
                       } else { 
                       remote_browse
                       }
                   }
     ## simpleminded http based FTP widget
     .mbar add cascade  -label "GetURL" \
                        -menu .mbar.http
     menu .mbar.http
          .mbar.http add command -label "download" \
          -command {
           if { [ string match "unreachable" $operator ] } {
              popMsg "server not responding"
              } else { download }
              }
          ## interpreters. Tcl/Tk, Perl and SQL 
          .mbar add cascade -label "Interp" \
                        -menu .mbar.interp
     menu .mbar.interp
          .mbar.interp add command -label "tcl" \
          -command {
           if { [ string match "unreachable" $operator ] } {
              popMsg "launching dumb TkConsole"
              source [ file join $tcl_library tkcon.tcl ]
              } else { interp_InABox tcl }
              }
           .mbar.interp add separator
           .mbar.interp add command -label "mSQL" \
           -command {
            if { [ string match "unreachable" $operator ] } {
               popMsg "server not responding"
               } else { popMsg "unimplemented" }
               }
           .mbar.interp add separator
           .mbar.interp add command -label "perl" \
           -command {
            if { ! [ isExec perl ] } {
               popMsg "Perl interpeter not found"
               return
               }
            if { [ string match "unreachable" $operator ] } {
               popMsg "server not responding"
               return
               }
            interp_InABox perl;
            }
      .mbar add cascade -label "preferences" \
                        -menu .mbar.pref
      menu .mbar.pref
           .mbar.pref add command -label "colors" \
           -command {
            set color [ tk_chooseColor ]
            if { ! [ string match "" $color ] } {
               tk_setPalette $color
               }
            }
           .mbar.pref add command -label "edit rcfile" \
           -command {
            upvar #0 RSC_FILE file
            if [ file exists $file ] {
               if [ info exists env(EDITOR) ] {
                  eval exec $env(EDITOR) $file &
                  } else { exec vi $file & }
               sourceFile $file
               } else { popMsg "client.rc file not found" }
            }               
     magicHelpMenu .mbar                     
}
## end menu bar
## ********************************************************

## ********************************************************
## interpreters in a box!
proc interp_InABox { { interp "" } } {
     global _interp; set _interp $interp;
     if { $interp == "" } {
        popMsg "interp_InABox called with no argument."
        return
        }
     if { [ winfo exists .$interp ] } { return }
     toplevel   .$interp
     wm title   .$interp "$interp In A Box!"
     wm minsize .$interp 400 150
     ;## setWinPos 50 150 .$interp .
     ;## dialog_safeguard .$interp

     frame .$interp.prompt       
     label .$interp.prompt.enter -foreground blue \
                             -text "enter expression (Esc to quit):"

     frame .$interp.eqn          
     entry .$interp.eqn.input    -width 50 \
                             -relief sunken
      bind .$interp.eqn.input <Enter> {
                              set _interp [ lindex [ split %W . ] 1 ]
                              }
      bind .$interp.eqn.input <Return> {
                              set cmd [ .$_interp.eqn.input get ]
                              set result [ interpret $_interp $cmd ]
                              if { [ string match "exit" $result ] } {
                                 destroy .$_interp
                                 } else {
                                 .$_interp.eqn.input delete 0 end
                                 .$_interp.log.txt configure -state normal
                                 .$_interp.log.txt insert end $result
                                 .$_interp.log.txt see end
                                 .$_interp.log.txt configure -state disabled
                                 }
                              }
      bind .$interp.eqn.input <Escape> "destroy .$_interp"
     focus .$interp.eqn.input
      
     frame .$interp.log
     label .$interp.log.session -foreground red \
                                -text "session log:"
     text .$interp.log.txt -insertofftime 0 \
                           -width 60 \
                           -height 10 \
                           -relief sunken \
                           -wrap word \
                           -yscrollcommand { .$_interp.log.scroll set }        
     scrollbar .$interp.log.scroll -width 15 \
                                   -command { .$_interp.log.txt yview }
     grid .$interp.log.session -sticky w
     grid .$interp.log.txt \
          .$interp.log.scroll  -sticky ns

     ## build the InABox widget
      grid .$interp.prompt.enter
      grid .$interp.eqn.input
      grid .$interp.prompt -sticky w
      grid .$interp.eqn -padx 4 -pady 4
      grid .$interp.log -padx 4 -pady 8

     .$interp.log.txt configure -state disabled
}
## ******************************************************** 

## ******************************************************** 
## the interpreter itself.
## allows Tcl hacks at the running program!
proc interpret { { interp "" } { cmd "" } } {
     global ${interp}expr_ok
     if { $interp == "" } {
        unset expr_ok
        popMsg "interpret called with no argument."
        return
        }
     if { [ string match "exit" $cmd ] } {
        catch {
              unset result
              unset ${interp}expr_ok
              } 
        return $cmd
        }
     if { [ string match "" $cmd ] } {
        return 
        } else {
        append cmd ";"
        }
     catch { set expr_test [ set ${interp}expr_ok ] }
     append expr_test $cmd;
  
     switch -exact -- $interp {
            tcl {          
                if { [ catch { eval $expr_test } result ] } {
                   append result " excising $cmd";
                   set cmd "";
                   }
                }
           perl {
                if { [ catch { eval { exec  perl -e $expr_test } } result ] } {
                   append result " excising $cmd";
                   set cmd "";
                   }
                if { [ regexp -nocase {.*print[ f]+} $expr_test ] } {
                   append cmd " $result"
                   set result $cmd
                   set cmd "";
                   }     
                }
            }
     append ${interp}expr_ok $cmd
     return "$cmd $result\n"
}
## ********************************************************

## ********************************************************
## widget with buttons & inputs
## for file download via http_get
## gets file from url, puts it in file.

proc download { { url "" } } {
     if { [ winfo exists .download ] } { return }
     toplevel .download
     wm title .download "Download File"
     wm minsize .download 450 150
     setWinPos 50 300 .download .
     label  .download.l1   -text "Get file (url):"
     entry  .download.e1   -width 30 \
                           -relief sunken     
     label  .download.l2   -text "save as:"
     entry  .download.e2   -width 30 \
                           -relief sunken
     label  .download.prog -foreground blue \
                           -textvariable $url
     label  .download.done -foreground red \
                           -text "DONE!"
     button .download.oops -text "Never Mind!" \
                           -command { destroy .download }
     button .download.remote -text "http browse"
     bind  .download.e2 <Return> {          
                                 set url  [ .download.e1 get ]
                                 set file [ .download.e2 get ]
                                 grid forget .download.oops  
                                 grid .download.prog -pady 6
                                 get_file $url $file                         
                                 grid .download.done -pady 6
                                 after 3000 destroy .download
                                 }
     grid .download.l1 .download.e1       -pady 8
     grid .download.l2 .download.e2       -pady 8
     grid .download.oops .download.remote -pady 8
}
## ******************************************************** 

## ******************************************************** 
## wrapper for HTTP_GET

proc get_file { url { file "xxx" } } {
     upvar #0 LDAS LDAS
     set fid [ open [ file join $LDAS $file ] w ]
     set token [ http_get $url -progress ftpGage \
                               -headers { pragma no-cache } \
                               -channel $fid ]
     close $fid;
}
## ******************************************************** 

## ******************************************************** 
## keep the luser busy while the file trickles in

proc ftpGage { token total current } {
     #global pie
     #set eighths [ expr int($current / ${total}.0 / 0.125) ]
     #display pie($eighths); 
     #;#update;
}

## ******************************************************** 
## build the animated pie image stack

proc buildPieAnim { { subdir help } } {
     global pie
     set n 0
     set fileroot [ file join $LDAS $subdir pie ]
     while { $n <= 8 } {
           set pie($n) [ image create photo -file ${fileroot}${n}.gif ]
           incr n
           }
}
## ********************************************************

## ******************************************************** 
## remote LIGO server file browser

proc remote_browse { } {
     upvar #0 remotehost remotehost
     if { [ winfo exists .browse ] } { return }
     toplevel .browse
     label .browse.label -fg blue -text "Files on hostname"

     frame .browse.button
     button .browse.button.view -text "View" -command {
            set index     [ .browse.lbox.lst curselection ]
            set from_file [ .browse.lbox.lst get $index ]
            set sid [ openSock $remotehost operator ]
            set stuff     [ retrieveFile $sid $from_file ]
            textBox $stuff $from_file
            }
     button .browse.button.get  -text "Get"
     button .browse.quit -text "Quit" \
                         -command { destroy .browse }
     pack .browse.button.view -side left -padx 8 -pady 4
     pack .browse.button.get  -side right -padx 8 -pady 4

     frame   .browse.lbox
     listbox .browse.lbox.lst -width 30 -height 12 \
                              -background LemonChiffon \
                              -yscrollcommand { .browse.lbox.sbar set }
     scrollbar .browse.lbox.sbar -orient vertical \
                                 -command { .browse.lbox.lst yview }
     pack .browse.lbox.lst -side left  -expand yes -fill both
     pack .browse.lbox.sbar -side right -fill y
     pack .browse.label -pady 8
     pack .browse.lbox -expand yes -fill both
     pack .browse.button
     pack .browse.quit -pady 4
     update

     set sid [ openSock $remotehost operator ]
     puts $sid get_ls
     set filename ""
         foreach filename [ split [ gets $sid ] ] {
                 .browse.lbox.lst insert end $filename
                 }
     closeSock $sid            
     .browse.lbox.lst selection set 0
}
## ******************************************************** 

## ******************************************************** 
## Home grown plotting package for LDAS.
## will recognize data frames and will plot them
## when they are chosen in the remote browser

proc title { c txt { win . } } {
           set x [ expr [ winfo width  $win ] * 0.50 ]
           set y [ expr [ winfo height $win ] * 0.05 ]
           set gr_title [ $c create text $x $y -anchor s \
                        -fill black \
                        -font {times 14 italic} \
                        -text $txt \
                        -tags txt ]
}

proc xLabel { c txt { win . } } {
           set x [ expr [ winfo width  $win ] * 0.50 ]
           set y [ expr [ winfo height $win ] * 1.00 ]
           set x_label [ $c create text $x $y -anchor n \
                      -fill black \
                      -font {times 14 italic} \
                      -text $txt \
                      -tags txt ]
}

proc yLabel { c txt { win . } } {
           set x [ expr [ winfo width  $win ] * 0.100 ]
           set y [ expr [ winfo height $win ] * 0.065 ]
           set y_label [ $c create text $x $y -anchor center \
                      -fill black \
                      -font {times 14 italic} \
                      -text $txt \
                      -tags txt ]
}

proc abscissa { c  { win . } } {
             set x1 [ expr [ winfo width  $win ] * 0.10 ]
             set y  [ expr [ winfo height $win ] * 0.95 ]
             set x2 [ expr [ winfo width  $win ] * 0.95 ]
             set Abcissa [ $c create line $x1 $y $x2 $y \
                         -fill blue \
                         -width 2 \
                         -tags axes ]
             set tic $x1
             while { $tic < $x2 } {
                   set tic [ expr $tic + ($x2-$x1)/10 ]
                   $c create line $tic $y $tic [ expr $y-$y*0.02 ] \
                            -fill blue \
                            -width 1
                   $c create text $tic [ expr $y*1.02 ] -anchor center \
                            -fill black \
                            -text [ expr $tic-$x1 ] \
                            -tags tics
                   }
}

proc ordinate { c { win . } } {
             set x  [ expr [ winfo width  $win ] * 0.10 ]
             set y1 [ expr [ winfo height $win ] * 0.95 ]
             set y2 [ expr [ winfo height $win ] * 0.10 ]
             set Ordinate [ $c create line $x $y1 $x $y2 \
                         -fill blue \
                         -width 2 \
                         -tags axes ]
             set tic $y1
             while { $tic >= $y2 } {
             set tic [ expr $tic - ($y1-$y2)/10 ]
             $c create line $x $tic [ expr $x+$x*0.15 ] $tic \
                            -fill blue \
                            -width 1
             $c create text [ expr $x*0.95 ] $tic -anchor e \
                            -fill black \
                            -text [ expr $y1-$tic ] \
                            -tags tics
                   }            
}

## ******************************************************** 
## blit a bitmap to the center of a canvas.
## good for setting a background image.

proc bitblt { canv img { win . } { subdir "art" } { fg "grey80" } } {
     upvar #0 LDAS LDAS
     global msg
     set bitmap [ file join $LDAS $subdir $img ]
     if { ! [ file exists $bitmap ] } {
        set msg "file not found:\n$bitmap"
        popMsg $msg
        return
        }
     set x [ expr [ winfo width  $win ] /2 ]
     set y [ expr [ winfo height $win ] /2 ]
     $canv create bitmap $x $y -anchor center \
                               -bitmap "@$bitmap" \
                               -foreground $fg \
                               -tag bgimg                          
}
## ********************************************************

## ******************************************************** 
## where data is a list created by appending...
## which looks like "x1 y1 x2 y2 x3 y3 etc"

proc plot_data { c data { win . } } {
     set xcorr 0.10
     set ycorr 0.95
     set tada ""
     set xcorr [ expr [ winfo width  $win ] * $xcorr ]
     set ycorr [ expr [ winfo height $win ] * $ycorr ]
     foreach { x1 y1 x2 y2 } $data {
             if { $y2 != "" } {
                set x1 [ expr $xcorr + $x1 ]
                set x2 [ expr $xcorr + $x2 ]
                set y1 [ expr $ycorr - $y1 ]
                set y2 [ expr $ycorr - $y2 ]
                append tada "$x1 $y1  $x2 $y2  "
                }
             }
     eval { $c create line } $tada { -fill red \
                                     -width 1 \
                                     -tags data }
}
## ******************************************************** 

## ******************************************************** 
## A large scrolling canvas.
## Usage: Scrolled_Canvas .c -width nnn -height nnn \
##                           -scrollregion { 0 0 1000 400 }

proc Scroll_Zoom_Canvas { c args } {
	frame $c
	eval {canvas $c.canvas \
		-xscrollcommand [ list $c.xscroll set ] \
		-yscrollcommand [ list $c.yscroll set ] \
		-highlightthickness 0 \
		-borderwidth 0 } $args
	scrollbar $c.xscroll -orient horizontal \
		-command [ list $c.canvas xview ]
	scrollbar $c.yscroll -orient vertical \
		-command [ list $c.canvas yview ]
	button $c.zm_in  -text "zoom in" \
                      -command "zoom $c.canvas 1.25"
     button $c.zm_out -text "zoom out" \
                      -command "zoom $c.canvas 0.80"
     grid $c.canvas $c.yscroll -columnspan 5 -sticky news
	grid $c.xscroll           -columnspan 5 -sticky ew
	grid rowconfigure    $c 0 -weight 1
	grid columnconfigure $c 0 -weight 1
	grid $c.zm_in  -column 0 -row 2 -sticky w -pady 4 -padx 8
     grid $c.zm_out -column 4 -row 2 -sticky e -pady 4 -padx 8
     return $c.canvas
}
## ********************************************************

## ********************************************************
## zoomer

proc zoom { canv factor } {
     $canv scale all 0 0 $factor $factor
     $canv configure -scrollregion [ $canv bbox all ]
}
## ********************************************************

## ******************************************************** 
## identify canvas items as movable:
##   $can bind movable <Button-1>  { CanvasMark %x %y %W }
##   $can bind movable <B1-Motion> { CanvasDrag %x %y %W }
##   $can create ... -tag movable

proc CanvasMark { x y can} {
	global canvas
	# Map from view coordinates to canvas coordinates
	set x [$can canvasx $x]
	set y [$can canvasy $y]
	# Remember the object and its location
	set canvas($can,obj) [$can find closest $x $y]
	set canvas($can,x) $x
	set canvas($can,y) $y
}
proc CanvasDrag { x y can} {
	global canvas
	# Map from view coordinates to canvas coordinates
	set x [$can canvasx $x]
	set y [$can canvasy $y]
	# Move the current object
	set dx [expr $x - $canvas($can,x)]
	set dy [expr $y - $canvas($can,y)]
	$can move $canvas($can,obj) $dx $dy
	set canvas($can,x) $x
	set canvas($can,y) $y 
}
## ********************************************************

## ******************************************************** 
#  Example using these functions:

proc demoGraphing {} {
     upvar #0 LDAS LDAS
     Scroll_Zoom_Canvas .f -width 500 -height 500 \
                           -background PapayaWhip \
                           -scrollregion { -500 -500 1000 1000 }

     pack forget .f
     bitblt   .f.canvas LDAS.xbm
     title    .f.canvas "This is a Graph"
     xLabel   .f.canvas abscissa
     yLabel   .f.canvas ordinate
     abscissa .f.canvas
     ordinate .f.canvas
     set data [ dumpFile [ file join $LDAS data ] ]
     plot_data .f.canvas $data
}
## ******************************************************** 

source ../genericAPI/genericAPI.tcl

sourceFile clientAPI.rsc

## where we need to go for stuff
if { [
   catch { set operator [ openSock $remotehost operator ] } err
   ] } {
   puts stderr $err
   set operator "unreachable"
}

## load up the menu
build_menu
## flash the "Tcl-Powered" logo
powerSplash

demoGraphing 

catch { pack .f }
after 125 { update }

## initialize the event loop
vwait forever

##
## ********************************************************
