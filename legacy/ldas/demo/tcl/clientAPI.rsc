## For the download widget
package require http 1.0

## For the "option" entries
package require Tk 8.0

## This file:
set RSC_FILE [ file join $LDAS clientAPI.rsc ]

## The LIGO LDAS root directory, set as an environment
## variable "LDASDIR".  Or optionally as the working directory.
## set LDAS env(LDASDIR)
set LDAS [ pwd ]

set LOCAL_LOG_FILE [ file join $LDAS log LDASclient.log ]

## for html support
source [file join $LDAS html.tcl]

## UNIX users can use this
set localhost $env(HOST)

## Others may need to do this
## set localhost  "maasim"

## The remotehost would not normally be set by the user
## set remotehost "m27"

## For local testing
set remotehost "maasim"

variable REMOTE_HOSTS { maasim }

## the ports on the given remote machine we want
## to be able to connect to.  The port # is in a
## list so that the socket i.d. can be lappended
## to it
array set $remotehost { operator  { 9001 } }
array set $remotehost { emergency { 9002 } }


## Some colors and things you can mess with.
## These can be changed - usually for the worse,
## from the preferences menu in the GUI.
option add *background PapayaWhip user
option add *foreground Black user
option add *activeBackground Bisque user
option add *activeForeground Black user
option add *selectColor #b03060 user
option add *selectBackground #c3c3c3 user
option add *troughColor #c3c3c3 user
option add *disabledForeground #a3a3a3 user
option add *Menu*background Lavender user
option add *Menu*foreground Blue user
option add *button.background PapayaWhip user
option add *Menu.tearOff 0 user
option add *Entry.background LemonChiffon user
option add *Mess*background SkyBlue user
option add *Mess*foreground Red user
option add *borderWidth 2 user
