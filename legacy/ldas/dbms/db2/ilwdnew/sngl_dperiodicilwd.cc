#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sngl_dperiodic( "sngl_dperiodicgroup:sngl_dperiodic:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "sngl_dperiodicgroup:sngl_dperiodic:process_id" ); 
	LdasContainer filter_id_cont ( "sngl_dperiodicgroup:sngl_dperiodic:filter_id" ); 
	LdasContainer ifo_cont ( "sngl_dperiodicgroup:sngl_dperiodic:ifo" ); 
	LdasContainer search_cont ( "sngl_dperiodicgroup:sngl_dperiodic:search" ); 
	LdasContainer channel_cont ( "sngl_dperiodicgroup:sngl_dperiodic:channel" ); 
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
	REAL_4	duration[ARRAYSIZE];
	LdasContainer target_name_cont ( "sngl_dperiodicgroup:sngl_dperiodic:target_name" ); 
	REAL_8	sky_ra[ARRAYSIZE];
	REAL_8	sky_dec[ARRAYSIZE];
	REAL_8	frequency[ARRAYSIZE];
	REAL_4	amplitude[ARRAYSIZE];
	REAL_4	phase[ARRAYSIZE];
	REAL_4	snr[ARRAYSIZE];
	REAL_4	confidence[ARRAYSIZE];
	LdasContainer stat_name_cont ( "sngl_dperiodicgroup:sngl_dperiodic:stat_name" ); 
	REAL_4	stat_value[ARRAYSIZE];
	REAL_4	stat_prob[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"filter.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"filter_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sngl_dperiodicgroup:sngl_dperiodic:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sngl_dperiodicgroup:sngl_dperiodic:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_filter_id () {

// fill in filter_id array
	char filter_id_str[30] ;
	CHAR_U filter_id[13];
	CHAR filter_idattr[200];

	int i,j;
// convert filter_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id_str, filter[ i % filter_len	].FILTER_ID);
		for ( int x = 0, j = 0; x < strlen(filter_id_str); x=x+2, j++ ) {
			filter_id[j] = ((filter_id_str[x]-'0' ) << 4) + (filter_id_str[x+1]-'0');
		}
		sprintf(filter_idattr, "filter_id:%s", filter_id_str);
		LdasArray< CHAR_U > filter_id_vector ( filter_id, 13,
		"sngl_dperiodicgroup:sngl_dperiodic:filter_id" );
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );
	}
}

static void setup_filter_id_symref () {
// fill in filter_id array
// NOT NULL
	CHAR filter_id[30] ;
	CHAR filter_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id, filter[ i % filter_len ].FILTER_ID );
		sprintf( filter_idattr, "filter_id:%s", filter_id );
		LdasArray< CHAR > filter_id_vector ( filter_id, strlen(filter_id),
		"sngl_dperiodicgroup:sngl_dperiodic:filter_id");
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "sngl_dperiodic:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"sngl_dperiodicgroup:sngl_dperiodic:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_search () {

// fill in search array
// NOT NULL
	CHAR search[24];
	CHAR searchattr[224];

	for (int i=0; i < numElements; i++) {
		snprintf(search, (size_t) 24, "search_%d", i + thistime + 1 );
		sprintf(searchattr, "sngl_dperiodic:%s", search);
		LdasArray< CHAR > search_vector ( search, strlen(search),
			"sngl_dperiodicgroup:sngl_dperiodic:search" );
		search_vector.setComment( searchattr );
		search_cont.push_back( search_vector );
	}
}

static void setup_channel () {

// fill in channel array
	CHAR channel[64];
	CHAR channelattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(channel, (size_t) 64, "channel_%d", i + thistime + 1 );
		sprintf(channelattr, "sngl_dperiodic:%s", channel);
		LdasArray< CHAR > channel_vector ( channel, strlen(channel),
			"sngl_dperiodicgroup:sngl_dperiodic:channel" );
		channel_vector.setComment( channelattr );
		channel_cont.push_back( channel_vector );
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] = (INT_4S) ( i + gps_secs );
}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

static void setup_duration () {

// fill in duration array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		duration[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_target_name () {

// fill in target_name array
	CHAR target_name[32];
	CHAR target_nameattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(target_name, (size_t) 32, "target_name_%d", i + thistime + 1 );
		sprintf(target_nameattr, "sngl_dperiodic:%s", target_name);
		LdasArray< CHAR > target_name_vector ( target_name, strlen(target_name),
			"sngl_dperiodicgroup:sngl_dperiodic:target_name" );
		target_name_vector.setComment( target_nameattr );
		target_name_cont.push_back( target_name_vector );
	}
}

static void setup_sky_ra () {

// fill in sky_ra array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		sky_ra[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_sky_dec () {

// fill in sky_dec array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		sky_dec[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_frequency () {

// fill in frequency array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		frequency[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_amplitude () {

// fill in amplitude array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		amplitude[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_phase () {

// fill in phase array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		phase[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_snr () {

// fill in snr array
	for (int i=0; i < numElements; i++) {
		snr[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_confidence () {

// fill in confidence array
	for (int i=0; i < numElements; i++) {
		confidence[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_stat_name () {

// fill in stat_name array
	CHAR stat_name[32];
	CHAR stat_nameattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(stat_name, (size_t) 32, "stat_name_%d", i + thistime + 1 );
		sprintf(stat_nameattr, "sngl_dperiodic:%s", stat_name);
		LdasArray< CHAR > stat_name_vector ( stat_name, strlen(stat_name),
			"sngl_dperiodicgroup:sngl_dperiodic:stat_name" );
		stat_name_vector.setComment( stat_nameattr );
		stat_name_cont.push_back( stat_name_vector );
	}
}

static void setup_stat_value () {

// fill in stat_value array
	for (int i=0; i < numElements; i++) {
		stat_value[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_stat_prob () {

// fill in stat_prob array
	for (int i=0; i < numElements; i++) {
		stat_prob[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
// event_id is uniqueId, generated by metadataAPI

}

void setup_sngl_dperiodic ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sngl_dperiodic.push_back( process_id_cont ) ;
	setup_filter_id();
	ilwd3sngl_dperiodic.push_back( filter_id_cont ) ;
	setup_ifo();
	ilwd3sngl_dperiodic.push_back(  ifo_cont ) ;
	setup_search();
	ilwd3sngl_dperiodic.push_back(  search_cont ) ;
	setup_channel();
	ilwd3sngl_dperiodic.push_back(  channel_cont ) ;
	setup_start_time();
	ilwd3sngl_dperiodic.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:start_time" ) );
	setup_start_time_ns();
	ilwd3sngl_dperiodic.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:start_time_ns" ) );
	setup_end_time();
	ilwd3sngl_dperiodic.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:end_time" ) );
	setup_end_time_ns();
	ilwd3sngl_dperiodic.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:end_time_ns" ) );
	setup_duration();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_4 >( duration, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:duration" ) );
	setup_target_name();
	ilwd3sngl_dperiodic.push_back(  target_name_cont ) ;
	setup_sky_ra();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_8 >( sky_ra, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:sky_ra" ) );
	setup_sky_dec();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_8 >( sky_dec, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:sky_dec" ) );
	setup_frequency();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_8 >( frequency, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:frequency" ) );
	setup_amplitude();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_4 >( amplitude, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:amplitude" ) );
	setup_phase();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_4 >( phase, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:phase" ) );
	setup_snr();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_4 >( snr, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:snr" ) );
	setup_confidence();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_4 >( confidence, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:confidence" ) );
	setup_stat_name();
	ilwd3sngl_dperiodic.push_back(  stat_name_cont ) ;
	setup_stat_value();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_4 >( stat_value, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:stat_value" ) );
	setup_stat_prob();
	ilwd3sngl_dperiodic.push_back(  LdasArray< REAL_4 >( stat_prob, numElements,
	"sngl_dperiodicgroup:sngl_dperiodic:stat_prob" ) );
	setup_event_id();
}

#ifdef TESTMAIN 
 void setup_sngl_dperiodic( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sngl_dperiodic( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sngl_dperiodic );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
