#! /ldcg/bin/tclsh

## ******************************************************** 
## putValidate.tcl
##
#! /ldcg/bin/tclsh

## generates the ilwdValidate.sh shell script to validate
## ilwd input and select query output. 
##
## Release Date: Not Available
##
##  
## ******************************************************** 

;#barecode

;## make sure manager is started fresh so job number
;## can begin at TEST1.

set sqlfile "/usr1/ldas/mlei/LDAS/examples_put.sql"
puts "using sql data file $sqlfile"
set fd [ open $sqlfile "r" ]
set cmddata [ read $fd ]
close $fd
set cmddata [ split $cmddata \n ]

set fout [ open "ilwdValidate.sh" "w" ]
puts $fout "#! /bin/sh"
puts $fout "dims=\$\{1\}"

set mode "TEST"
;## set mode "NORMAL"

set index 1
set jobbase 3065
foreach cmd $cmddata {
	if	{  [ regexp {^;##} $cmd ] } {
		continue
	}
	if  {  [ regexp {select \* from ([a-z_]+)} $cmd match table dims ] } {
		puts $fout "putValidate.tcl ${table}\$\{dims\}.ilwd /ldas_outgoing/jobs/NORMAL$jobbase/getMeta[ expr $index ].ilwd"
		incr index 1
		incr jobbase 1
	}
}

close $fout		
puts "created ilwdValidate.sh"
