#! /ldcg/bin/tclsh

;## parses output of sql script to build .h files for events.
;## 

proc    endtable {} {
    uplevel #0 {
        puts $fout "\};"
        puts $fout "#define ${oldtable}_len $numRows"
        close $fout  
    }
}

;## this `
set reserved { VERSION } 

 set fd [ open [ lindex $argv 0 ] "r" ]
 puts "file=[lindex $argv 0 ]"
 
 set xrefs [ read $fd ]
 set xrefs [ split $xrefs \n ]
 
 set tables {}
 
 set state "NEWTABLE"
 set numRows 0
 
 foreach line $xrefs {
    regsub -all {[ \t]+} $line " " line
    
    ;## skip blank lines 
    if  { [ regexp -nocase {^[-DB2]+} $line ] } {
        continue
    }
    if  { [ regexp -nocase {table (\S+)} $line match table ] } {
        set state "NEWTABLE"
        if  { [ info exist fout ] } {
            endtable
        }
        set fout [ open "${table}.h" "w" ]
        set oldtable $table              
        continue
    }
    if  {  ! [ string compare $state "NEWTABLE" ] } {
        set linelist [ split $line ]
        if  { [ llength $linelist ] == 0 } {
            continue
        }
        puts $fout "struct ${table}_type \{"
        foreach field $linelist {
            if  { [ lsearch -exact $reserved $field ] > -1 } {
                set field "${field}_local"
            }
            ;##puts "field=$field"
            if  { [ string length $field ] } {
                puts $fout "\tchar *${field};" 
            }       
        }
        puts $fout "\};"
        set state "NEWFIELDS"
        continue
    }
    if  { [ regexp -nocase {(\d+) record} $line match numRows ] } {
            puts $fout "// $table has $numRows rows" 
            continue
    }   
  
    if  {  ! [ string compare $state "NEWFIELDS" ] || ! [ string compare $state "NEWDATA" ] } {
        set linelist [ split $line ]
        if  { [ llength $linelist ] > 0 } {
            if  { ! [ string compare $state "NEWFIELDS" ] } {
                puts $fout "${table}_type ${table}\[\] = \{"
                set state "NEWDATA" 
            }
            puts -nonewline $fout "\t\{ " 
            foreach data $line {
                if  { [ regexp -nocase {[x']*([A-Za-z0-9_.]+)[']*} $data match data1 ] } {
                    ;##puts "data=$data, data1=$data1"
                    set data $data1
                }
                puts -nonewline $fout " \"${data}\", "       
            }
            puts $fout "\},"
        }
        continue
    }
}

endtable    


