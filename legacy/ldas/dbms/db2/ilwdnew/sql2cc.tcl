#! /ldcg/bin/tclsh

foreach dbtable {\
process.sql \
runlist.sql \
calib_info.sql \
exttrig_search \
process_params.sql \
frameset_chanlist.sql \
frameset_writer.sql \
frameset.sql \
frameset_loc.sql \
segment_definer.sql \
segment.sql \
summ_value.sql \
summ_statistics.sql \
summ_spectrum.sql \
summ_csd.sql \
summ_comment.sql \
summ_mime.sql \
filter.sql \
filter_params.sql \
sim_inst.sql \
sim_inst_params.sql \
sim_type.sql \ 
sim_type_params.sql \
gds_trigger.sql \
sngl_inspiral.sql \
sngl_burst.sql \
sngl_ringdown.sql \
sngl_unmodeled.sql \
sngl_dperiodic.sql \
sngl_datasource.sql \
sngl_transdata.sql \
sngl_mime.sql \
sngl_unmodeled_v.sql \
multi_inspiral.sql \
multi_burst.sql \
coinc_sngl.sql \
search_summary.sql \
search_summvars.sql \
waveburst.sql \
waveburst_mime.sql } {
	catch { exec parseSql.tcl $dbtable } result
	puts $result
	append data "$result\n"
}

set data [ split $data \n ]

foreach line $data {
	if	{ [ regexp {parent\s+([^,]+),\s+foreign_fields\s+(.+)} $line -> table fields ] } {
		regsub {\s+} $fields { } fields
		set fields [ split $fields , ]
		foreach field $fields {
			set field [ string trim $field ]
			if	{ ! [ info exist tab_$table ] } {
				set tab_$table [ list ]
			}
			if	{ [ lsearch -exact [ set tab_$table ] $field ] == -1 } {
				if	{ ! [ string equal creator_db $field ] } {
					lappend tab_${table} $field
				}
			}
		}
	}
}

set fd [ open "get_refs.sql" w ]
puts $fd "--"
puts $fd "-- select X-referenced data from tables to generate"
puts $fd "-- .h files for ilwds"
puts [info vars tab* ]
foreach table [ lsort [ info vars tab_* ] ] {
	regexp {tab_(.+)} $table -> tabname
	puts $fd "echo table $tabname;"
	set fields [ join [ set ${table} ] , ] 	
	puts $fd "select $fields from $tabname;"
}
close $fd
puts "get_refs.sql generated"

set tables [ list ]
foreach line $data {
	if	{ [ regexp {(\S+)\s+has event_id as primary key} $line -> table ] } {
		lappend tables $table
	}
}

set fd [ open "get_event_id.sql" w ]
puts $fd "--"
puts $fd "-- select event_id from tables to provide data for" 
puts $fd "-- X-reference tables like sngl_datasource, coinc_sngl."
puts $fd "--"

foreach table $tables {
	puts $fd "echo table $table;"
	puts $fd "select event_id from $table;"
}

close $fd
puts "get_event_id.sql generated"
catch { exec ls -lt get_refs.sql get_event_id.sql } err
puts $err
