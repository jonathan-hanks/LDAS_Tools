#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sim_inst( "sim_instgroup:sim_inst:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer simulation_id_cont ( "sim_instgroup:sim_inst:simulation_id" ); 
	LdasContainer process_id_cont ( "sim_instgroup:sim_inst:process_id" ); 
	INT_4S	this_sim_type[ARRAYSIZE];
	INT_4S	s_burst_count[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"sim_type.h"
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"sim_type_symref.h"
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sim_instgroup:sim_inst:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sim_instgroup:sim_inst:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_sim_type () {

// fill in sim_type array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		this_sim_type[i] = atol( sim_type[ i % sim_type_len ].SIM_TYPE);
	}
}

static void setup_s_burst_count () {

// fill in s_burst_count array
	for (int i=0; i < numElements; i++) {
		s_burst_count[i] = (INT_4S) i + thistime + 1 ;
	}
}

void setup_sim_inst ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sim_inst.push_back( process_id_cont ) ;
	setup_sim_type();
	ilwd3sim_inst.push_back(  LdasArray< INT_4S >(this_sim_type, numElements,
	"sim_instgroup:sim_inst:sim_type" ) );
	setup_s_burst_count();
	ilwd3sim_inst.push_back(  LdasArray< INT_4S >(s_burst_count, numElements,
	"sim_instgroup:sim_inst:s_burst_count" ) );
}

#ifdef TESTMAIN 
 void setup_sim_inst( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sim_inst( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sim_inst );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
