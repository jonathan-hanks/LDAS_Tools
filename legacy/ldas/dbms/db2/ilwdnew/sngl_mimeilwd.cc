#include <cstring>
#include <algorithm>
#include <iostream>
#include <climits>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sngl_mime( "sngl_mimegroup:sngl_mime:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "sngl_mimegroup:sngl_mime:process_id" ); 
	LdasContainer event_table_cont ( "sngl_mimegroup:sngl_mime:event_table" ); 
	LdasContainer event_id_cont ( "sngl_mimegroup:sngl_mime:event_id" ); 
	LdasContainer ifo_cont ( "sngl_mimegroup:sngl_mime:ifo" ); 
	LdasContainer origin_cont ( "sngl_mimegroup:sngl_mime:origin" ); 
	LdasContainer filename_cont ( "sngl_mimegroup:sngl_mime:filename" ); 
	LdasContainer submitter_cont ( "sngl_mimegroup:sngl_mime:submitter" ); 
	LdasContainer mimedata_cont ( "sngl_mimegroup:sngl_mime:mimedata" ); 
	INT_4S	mimedata_length[ARRAYSIZE];
	LdasContainer mimetype_cont ( "sngl_mimegroup:sngl_mime:mimetype" ); 
	LdasContainer descrip_cont ( "sngl_mimegroup:sngl_mime:descrip" ); 
	LdasContainer comment_cont ( "sngl_mimegroup:sngl_mime:comment" ); 
} // end namespace

#ifdef TESTMAIN
#include	"events.h"
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"events_symref.h"
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sngl_mimegroup:sngl_mime:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sngl_mimegroup:sngl_mime:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_event_table () {

// fill in event_table array
// NOT NULL
// fill in event_table array
// NOT NULL
	CHAR event_table[18];
	CHAR event_tableattr[218];
	for (int i=0; i < numElements; i++) {
		sprintf(event_table, "%s", events[i].TABLE);
		sprintf(event_tableattr, "event_table:%s", event_table);
		LdasArray< CHAR > event_table_vector ( event_table, strlen(event_table),
		"sngl_mimegroup:sngl_mime:event_table" );
	event_table_vector.setComment( event_tableattr );
	event_table_cont.push_back( event_table_vector );}
}

static void setup_event_id () {

// fill in event_id array
// primary key
// NOT NULL
	char event_id_str[30] ;
	CHAR_U event_id[13];
	CHAR event_idattr[200];

	int i,j;
// convert event_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(event_id_str, events[ i % events_len	].EVENT_ID);
		for ( int x = 0, j = 0; x < strlen(event_id_str); x=x+2, j++ ) {
			event_id[j] = ((event_id_str[x]-'0' ) << 4) + (event_id_str[x+1]-'0');
		}
		sprintf(event_idattr, "event_id:%s", event_id_str);
		LdasArray< CHAR_U > event_id_vector ( event_id, 13,
		"sngl_mimegroup:sngl_mime:event_id" );
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );
	}
}

static void setup_event_id_symref () {
// fill in event_id array
// NOT NULL
	CHAR event_id[30] ;
	CHAR event_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(event_id, events[ i % events_len ].EVENT_ID );
		sprintf( event_idattr, "event_id:%s", event_id );
		LdasArray< CHAR > event_id_vector ( event_id, strlen(event_id),
		"sngl_mimegroup:sngl_mime:event_id");
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "sngl_mime:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"sngl_mimegroup:sngl_mime:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_origin () {

// fill in origin array
	CHAR origin[64];
	CHAR originattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(origin, (size_t) 64, "origin_%d", i + thistime + 1 );
		sprintf(originattr, "sngl_mime:%s", origin);
		LdasArray< CHAR > origin_vector ( origin, strlen(origin),
			"sngl_mimegroup:sngl_mime:origin" );
		origin_vector.setComment( originattr );
		origin_cont.push_back( origin_vector );
	}
}

static void setup_filename () {

// fill in filename array
	CHAR filename[64];
	CHAR filenameattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(filename, (size_t) 64, "filename_%d", i + thistime + 1 );
		sprintf(filenameattr, "sngl_mime:%s", filename);
		LdasArray< CHAR > filename_vector ( filename, strlen(filename),
			"sngl_mimegroup:sngl_mime:filename" );
		filename_vector.setComment( filenameattr );
		filename_cont.push_back( filename_vector );
	}
}

static void setup_submitter () {

// fill in submitter array
	CHAR submitter[48];
	CHAR submitterattr[248];

	for (int i=0; i < numElements; i++) {
		snprintf(submitter, (size_t) 48, "submitter_%d", i + thistime + 1 );
		sprintf(submitterattr, "sngl_mime:%s", submitter);
		LdasArray< CHAR > submitter_vector ( submitter, strlen(submitter),
			"sngl_mimegroup:sngl_mime:submitter" );
		submitter_vector.setComment( submitterattr );
		submitter_cont.push_back( submitter_vector );
	}
}

static void setup_mimedata () {

// fill in mimedata array
// NOT NULL
// fill in mimedata array
	if	( numElements > 1000 ) {
		BLOBSIZE=1 ;
	}
	mimedata_cont.setComment("mimedata blob arrays");

	char name[100];
	char units[]="blob_units";

	for ( int i = 0; i < numElements ; i++ ) {
		sprintf (name, "%s", "mimedatagroup:sngl_mime:mimedata");
		size_t size( ( i % 100 + 1 )  * BLOBSIZE );
		
		const CHAR_U fill_char( i%UCHAR_MAX );
		CHAR_U buffer[ size ];		
       	fill( buffer, buffer + size, fill_char );
		
        try
		{
  		   LdasArray< CHAR_U >* char_u_vector = new 
		   		LdasArray< CHAR_U >( buffer, size, name, units );

           // create variable size blobs
		   mimedata_cont.push_back( char_u_vector, 
               	LdasContainer::NO_ALLOCATE,
                LdasContainer::OWN );
		}
          	catch( const std::exception& exc )
		{
		   	cout << "Exception: " << exc.what() << endl;
               	   exit( 1 );
	  	}
           	catch( const FormatException& exc )
		{
           	const size_t exc_size( exc.getSize() );
           	cout << "Format exception: ";
           	for( size_t i = 0; i < exc_size; ++i )
		  	{
		    	cout << exc[ i ].getMessage() << " ";
		  	}
            cout << endl;
           	exit( 1 );
		}
	}
	mimedata_cont.setWriteFormat(ILwd::BASE64);
	mimedata_cont.setWriteCompression( ILwd::GZIP0);
}

static void setup_mimedata_length () {

// fill in mimedata_length array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		mimedata_length[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_mimetype () {

// fill in mimetype array
// NOT NULL
	CHAR mimetype[64];
	CHAR mimetypeattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(mimetype, (size_t) 64, "mimetype_%d", i + thistime + 1 );
		sprintf(mimetypeattr, "sngl_mime:%s", mimetype);
		LdasArray< CHAR > mimetype_vector ( mimetype, strlen(mimetype),
			"sngl_mimegroup:sngl_mime:mimetype" );
		mimetype_vector.setComment( mimetypeattr );
		mimetype_cont.push_back( mimetype_vector );
	}
}

static void setup_descrip () {

// fill in descrip array
	CHAR descrip[64];
	CHAR descripattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(descrip, (size_t) 64, "descrip_%d", i + thistime + 1 );
		sprintf(descripattr, "sngl_mime:%s", descrip);
		LdasArray< CHAR > descrip_vector ( descrip, strlen(descrip),
			"sngl_mimegroup:sngl_mime:descrip" );
		descrip_vector.setComment( descripattr );
		descrip_cont.push_back( descrip_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[240];
	CHAR commentattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 240, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "sngl_mime:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"sngl_mimegroup:sngl_mime:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

static void setup_sngl_mime_id () {

// fill in sngl_mime_id array
// NOT NULL
// sngl_mime_id is uniqueId, generated by metadataAPI

}

void setup_sngl_mime ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sngl_mime.push_back( process_id_cont ) ;
	setup_event_table();
	ilwd3sngl_mime.push_back(  event_table_cont ) ;
	setup_event_id();
	ilwd3sngl_mime.push_back( event_id_cont ) ;
	setup_ifo();
	ilwd3sngl_mime.push_back(  ifo_cont ) ;
	setup_origin();
	ilwd3sngl_mime.push_back(  origin_cont ) ;
	setup_filename();
	ilwd3sngl_mime.push_back(  filename_cont ) ;
	setup_submitter();
	ilwd3sngl_mime.push_back(  submitter_cont ) ;
	setup_mimedata();
	ilwd3sngl_mime.push_back( mimedata_cont ) ;
	setup_mimedata_length();
	ilwd3sngl_mime.push_back(  LdasArray< INT_4S >(mimedata_length, numElements,
	"sngl_mimegroup:sngl_mime:mimedata_length" ) );
	setup_mimetype();
	ilwd3sngl_mime.push_back(  mimetype_cont ) ;
	setup_descrip();
	ilwd3sngl_mime.push_back(  descrip_cont ) ;
	setup_comment();
	ilwd3sngl_mime.push_back(  comment_cont ) ;
	setup_sngl_mime_id();
}

#ifdef TESTMAIN 
 void setup_sngl_mime( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sngl_mime( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sngl_mime );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
