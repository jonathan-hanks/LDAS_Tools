#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3process_params( "process_paramsgroup:process_params:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer program_cont ( "process_paramsgroup:process_params:program" ); 
	LdasContainer process_id_cont ( "process_paramsgroup:process_params:process_id" ); 
	LdasContainer param_cont ( "process_paramsgroup:process_params:param" ); 
	LdasContainer type_cont ( "process_paramsgroup:process_params:type" ); 
	LdasContainer value_cont ( "process_paramsgroup:process_params:value" ); 
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#endif

using namespace local;

static void setup_program () {

// fill in program array
// NOT NULL
	CHAR program[16];
	CHAR programattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(program, (size_t) 16, "program_%d", i + thistime + 1 );
		sprintf(programattr, "process_params:%s", program);
		LdasArray< CHAR > program_vector ( program, strlen(program),
			"process_paramsgroup:process_params:program" );
		program_vector.setComment( programattr );
		program_cont.push_back( program_vector );
	}
}

static void setup_process_id () {

// fill in process_id array
// primary key
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"process_paramsgroup:process_params:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"process_paramsgroup:process_params:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_param () {

// fill in param array
// primary key
// NOT NULL
	CHAR param[32];
	CHAR paramattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(param, (size_t) 32, "param_%d", i + thistime + 1 );
		sprintf(paramattr, "process_params:%s", param);
		LdasArray< CHAR > param_vector ( param, strlen(param),
			"process_paramsgroup:process_params:param" );
		param_vector.setComment( paramattr );
		param_cont.push_back( param_vector );
	}
}

static void setup_type () {

// fill in type array
// NOT NULL
	CHAR type[16];
	CHAR typeattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(type, (size_t) 16, "type_%d", i + thistime + 1 );
		sprintf(typeattr, "process_params:%s", type);
		LdasArray< CHAR > type_vector ( type, strlen(type),
			"process_paramsgroup:process_params:type" );
		type_vector.setComment( typeattr );
		type_cont.push_back( type_vector );
	}
}

static void setup_value () {

// fill in value array
// NOT NULL
	CHAR value[1024];
	CHAR valueattr[1224];

	for (int i=0; i < numElements; i++) {
		snprintf(value, (size_t) 1024, "value_%d", i + thistime + 1 );
		sprintf(valueattr, "process_params:%s", value);
		LdasArray< CHAR > value_vector ( value, strlen(value),
			"process_paramsgroup:process_params:value" );
		value_vector.setComment( valueattr );
		value_cont.push_back( value_vector );
	}
}

void setup_process_params ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_program();
	ilwd3process_params.push_back(  program_cont ) ;
	setup_process_id();
	ilwd3process_params.push_back( process_id_cont ) ;
	setup_param();
	ilwd3process_params.push_back(  param_cont ) ;
	setup_type();
	ilwd3process_params.push_back(  type_cont ) ;
	setup_value();
	ilwd3process_params.push_back(  value_cont ) ;
}

#ifdef TESTMAIN 
 void setup_process_params( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_process_params( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3process_params );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
