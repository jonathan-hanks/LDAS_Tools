#! /ldcg/bin/tclsh

proc    endtable {} {
    uplevel #0 {
        puts $fout1 "\};"
        puts $fout1 "#define events_len $totRows"        
        close $fout1 
        puts $fout2 "\};"     
        close $fout2   
    }
}

;## parses output of sql script to build .h files for events.
;## 
 set fd [ open [ lindex $argv 0 ] "r" ]
 puts "event id file=[lindex $argv 0 ]"
 
 set output1 "events.h"
 set output2 "events_by_table.h"

 set eventids [ read $fd ]
 set eventids [ split $eventids \n ]
 
 set tables {}
 
 set fout1 [ open "events.h" "w" ]
 set fout2 [ open "events_by_table.h" "w" ]
 
 puts $fout1 "struct event_type \{\n\tchar *TABLE;\n\tchar *EVENT_ID;\n\};"
 puts $fout2 "struct event_type \{\n\tchar *TABLE;\n\tchar *EVENT_ID;\n\};"
 
 set init 1 
 set totRows 0
 
 foreach line $eventids {
    regsub -all {[ \t]+} $line " " line
    
    ;## skip blank lines 
    if  { [ regexp -nocase {^[-]+} $line ] } {
        continue
    }
    if  { [ regexp -nocase {table (coinc_sngl|multi_burst|sngl_ringdown|multi_inspiral|sngl_transdata|filter_params|process_params|sngl_unmodeled_v|filter|process|sngl_unmodeled|frameset_chanlist|segment_definer|summ_comment|frameset_loc|segment|summ_spectrum|frameset_writer|sngl_burst|summ_statistics|frameset|sngl_datasource|summ_value|gds_trigger|sngl_dperiodic|sngl_inspiral)} \
$line match table ] } {
        if  { $init } {
            puts $fout1 "event_type events\[\] = \{"
            set init 0
        } else {
            puts $fout2 "\};"
        }
        puts $fout2 "event_type ${table}\[\] = \{"
        continue
    }
    if  { [ regexp -nocase {^x\'([0-9]+)\'} $line match eventId ] } {
        puts $fout1 "\t\{ \"$table\", \"$eventId\" \}," 
        puts $fout2 "\t\{ \"$table\", \"$eventId\" \}," 
    }
    if  { [ regexp -nocase {([0-9]+) record} $line match numRows ] } {
        puts $fout1 "// $table has $numRows rows" 
        incr totRows $numRows 
        puts $fout2 "// $table has $numRows rows"  
        puts $fout2 "#define ${table}_len $numRows"    
    }   
}

endtable       
