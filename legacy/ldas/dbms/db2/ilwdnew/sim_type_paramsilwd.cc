#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sim_type_params( "sim_type_paramsgroup:sim_type_params:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	INT_4S	this_sim_type[ARRAYSIZE];
	LdasContainer name_cont ( "sim_type_paramsgroup:sim_type_params:name" ); 
	LdasContainer comment_cont ( "sim_type_paramsgroup:sim_type_params:comment" ); 
	REAL_8	value[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"sim_type.h"
#endif

#ifdef  TESTSYMREF
#include	"sim_type_symref.h"
#endif

using namespace local;

static void setup_sim_type () {

// fill in sim_type array
// primary key
// NOT NULL
	for (int i=0; i < numElements; i++) {
		this_sim_type[i] = atol( sim_type[ i % sim_type_len ].SIM_TYPE);
	}
}

static void setup_name () {

// fill in name array
// primary key
// NOT NULL
	CHAR name[24];
	CHAR nameattr[224];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 24, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "sim_type_params:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"sim_type_paramsgroup:sim_type_params:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[64];
	CHAR commentattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 64, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "sim_type_params:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"sim_type_paramsgroup:sim_type_params:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

static void setup_value () {

// fill in value array
	for (int i=0; i < numElements; i++) {
		value[i] = (REAL_8) i + thistime + 1 ;
	}
}

void setup_sim_type_params ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_sim_type();
	ilwd3sim_type_params.push_back(  LdasArray< INT_4S >(this_sim_type, numElements,
	"sim_type_paramsgroup:sim_type_params:sim_type" ) );
	setup_name();
	ilwd3sim_type_params.push_back(  name_cont ) ;
	setup_comment();
	ilwd3sim_type_params.push_back(  comment_cont ) ;
	setup_value();
	ilwd3sim_type_params.push_back(  LdasArray< REAL_8 >( value, numElements,
	"sim_type_paramsgroup:sim_type_params:value" ) );
}

#ifdef TESTMAIN 
 void setup_sim_type_params( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sim_type_params( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sim_type_params );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
