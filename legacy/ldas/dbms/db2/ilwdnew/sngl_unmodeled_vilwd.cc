#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sngl_unmodeled_v( "sngl_unmodeled_vgroup:sngl_unmodeled_v:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "sngl_unmodeled_vgroup:sngl_unmodeled_v:process_id" ); 
	LdasContainer event_id_cont ( "sngl_unmodeled_vgroup:sngl_unmodeled_v:event_id" ); 
	LdasContainer ifo_cont ( "sngl_unmodeled_vgroup:sngl_unmodeled_v:ifo" ); 
	LdasContainer name_cont ( "sngl_unmodeled_vgroup:sngl_unmodeled_v:name" ); 
	REAL_4	value[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"sngl_unmodeled.h"
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"sngl_unmodeled_symref.h"
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sngl_unmodeled_vgroup:sngl_unmodeled_v:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sngl_unmodeled_vgroup:sngl_unmodeled_v:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
	char event_id_str[30] ;
	CHAR_U event_id[13];
	CHAR event_idattr[200];

	int i,j;
// convert event_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(event_id_str, sngl_unmodeled[ i % sngl_unmodeled_len	].EVENT_ID);
		for ( int x = 0, j = 0; x < strlen(event_id_str); x=x+2, j++ ) {
			event_id[j] = ((event_id_str[x]-'0' ) << 4) + (event_id_str[x+1]-'0');
		}
		sprintf(event_idattr, "event_id:%s", event_id_str);
		LdasArray< CHAR_U > event_id_vector ( event_id, 13,
		"sngl_unmodeled_vgroup:sngl_unmodeled_v:event_id" );
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );
	}
}

static void setup_event_id_symref () {
// fill in event_id array
// NOT NULL
	CHAR event_id[30] ;
	CHAR event_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(event_id, sngl_unmodeled[ i % sngl_unmodeled_len ].EVENT_ID );
		sprintf( event_idattr, "event_id:%s", event_id );
		LdasArray< CHAR > event_id_vector ( event_id, strlen(event_id),
		"sngl_unmodeled_vgroup:sngl_unmodeled_v:event_id");
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "sngl_unmodeled_v:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"sngl_unmodeled_vgroup:sngl_unmodeled_v:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_name () {

// fill in name array
// primary key
// NOT NULL
	CHAR name[32];
	CHAR nameattr[232];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 32, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "sngl_unmodeled_v:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"sngl_unmodeled_vgroup:sngl_unmodeled_v:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_value () {

// fill in value array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		value[i] = (REAL_4) i + thistime + 1 ;
	}
}

void setup_sngl_unmodeled_v ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sngl_unmodeled_v.push_back( process_id_cont ) ;
	setup_event_id();
	ilwd3sngl_unmodeled_v.push_back( event_id_cont ) ;
	setup_ifo();
	ilwd3sngl_unmodeled_v.push_back(  ifo_cont ) ;
	setup_name();
	ilwd3sngl_unmodeled_v.push_back(  name_cont ) ;
	setup_value();
	ilwd3sngl_unmodeled_v.push_back(  LdasArray< REAL_4 >( value, numElements,
	"sngl_unmodeled_vgroup:sngl_unmodeled_v:value" ) );
}

#ifdef TESTMAIN 
 void setup_sngl_unmodeled_v( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sngl_unmodeled_v( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sngl_unmodeled_v );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
