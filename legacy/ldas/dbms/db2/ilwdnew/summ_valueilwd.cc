#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3summ_value( "summ_valuegroup:summ_value:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer program_cont ( "summ_valuegroup:summ_value:program" ); 
	LdasContainer process_id_cont ( "summ_valuegroup:summ_value:process_id" ); 
	LdasContainer frameset_group_cont ( "summ_valuegroup:summ_value:frameset_group" ); 
	LdasContainer segment_group_cont ( "summ_valuegroup:summ_value:segment_group" ); 
	INT_4S	version[ARRAYSIZE];
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
	LdasContainer ifo_cont ( "summ_valuegroup:summ_value:ifo" ); 
	LdasContainer name_cont ( "summ_valuegroup:summ_value:name" ); 
	REAL_4	value[ARRAYSIZE];
	REAL_4	error[ARRAYSIZE];
	INT_4S	intvalue[ARRAYSIZE];
	LdasContainer comment_cont ( "summ_valuegroup:summ_value:comment" ); 
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"segment.h"
#include	"frameset.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"segment_symref.h"
#include	"frameset_symref.h"
#endif

using namespace local;

static void setup_program () {

// fill in program array
// NOT NULL
	CHAR program[16];
	CHAR programattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(program, (size_t) 16, "program_%d", i + thistime + 1 );
		sprintf(programattr, "summ_value:%s", program);
		LdasArray< CHAR > program_vector ( program, strlen(program),
			"summ_valuegroup:summ_value:program" );
		program_vector.setComment( programattr );
		program_cont.push_back( program_vector );
	}
}

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"summ_valuegroup:summ_value:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"summ_valuegroup:summ_value:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_frameset_group () {

// fill in frameset_group array
	CHAR frameset_group[48];
	CHAR frameset_groupattr[248];

	for (int i=0; i < numElements; i++) {
		strcpy(frameset_group, frameset[ i % frameset_len ].FRAMESET_GROUP);
		sprintf(frameset_groupattr, "summ_value:%s", frameset_group);
		LdasArray< CHAR > frameset_group_vector ( frameset_group, strlen(frameset_group),
			"summ_valuegroup:summ_value:frameset_group" );
		frameset_group_vector.setComment( frameset_groupattr );
		frameset_group_cont.push_back( frameset_group_vector );
	}
}

static void setup_segment_group () {

// fill in segment_group array
	CHAR segment_group[64];
	CHAR segment_groupattr[264];

	for (int i=0; i < numElements; i++) {
		strcpy(segment_group, segment[ i % segment_len ].SEGMENT_GROUP);
		sprintf(segment_groupattr, "summ_value:%s", segment_group);
		LdasArray< CHAR > segment_group_vector ( segment_group, strlen(segment_group),
			"summ_valuegroup:summ_value:segment_group" );
		segment_group_vector.setComment( segment_groupattr );
		segment_group_cont.push_back( segment_group_vector );
	}
}

static void setup_version () {

// fill in version array
	for (int i=0; i < numElements; i++) {
		local::version[i] = atol( segment[ i % segment_len ].VERSION_local);
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] =  (INT_4S) atol (segment[ i % segment_len ].START_TIME);
	}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] =  (INT_4S) atol(segment[ i % segment_len ].END_TIME);
	}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "summ_value:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"summ_valuegroup:summ_value:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_name () {

// fill in name array
// NOT NULL
	CHAR name[128];
	CHAR nameattr[328];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 128, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "summ_value:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"summ_valuegroup:summ_value:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_value () {

// fill in value array
	for (int i=0; i < numElements; i++) {
		value[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_error () {

// fill in error array
	for (int i=0; i < numElements; i++) {
		error[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_intvalue () {

// fill in intvalue array
	for (int i=0; i < numElements; i++) {
		intvalue[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[80];
	CHAR commentattr[280];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 80, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "summ_value:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"summ_valuegroup:summ_value:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

void setup_summ_value ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_program();
	ilwd3summ_value.push_back(  program_cont ) ;
	setup_process_id();
	ilwd3summ_value.push_back( process_id_cont ) ;
	setup_frameset_group();
//	ilwd3summ_value.push_back(  frameset_group_cont ) ;
	setup_segment_group();
	ilwd3summ_value.push_back(  segment_group_cont ) ;
	setup_version();
	ilwd3summ_value.push_back(  LdasArray< INT_4S >(local::version, numElements,
	"summ_valuegroup:summ_value:version" ) );
	setup_start_time();
	ilwd3summ_value.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"summ_valuegroup:summ_value:start_time" ) );
	setup_start_time_ns();
	ilwd3summ_value.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"summ_valuegroup:summ_value:start_time_ns" ) );
	setup_end_time();
	ilwd3summ_value.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"summ_valuegroup:summ_value:end_time" ) );
	setup_end_time_ns();
	ilwd3summ_value.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"summ_valuegroup:summ_value:end_time_ns" ) );
	setup_ifo();
	ilwd3summ_value.push_back(  ifo_cont ) ;
	setup_name();
	ilwd3summ_value.push_back(  name_cont ) ;
	setup_value();
	ilwd3summ_value.push_back(  LdasArray< REAL_4 >( value, numElements,
	"summ_valuegroup:summ_value:value" ) );
	setup_error();
	ilwd3summ_value.push_back(  LdasArray< REAL_4 >( error, numElements,
	"summ_valuegroup:summ_value:error" ) );
	setup_intvalue();
	ilwd3summ_value.push_back(  LdasArray< INT_4S >(intvalue, numElements,
	"summ_valuegroup:summ_value:intvalue" ) );
	setup_comment();
	ilwd3summ_value.push_back(  comment_cont ) ;
}

#ifdef TESTMAIN 
 void setup_summ_value( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_summ_value( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3summ_value );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
