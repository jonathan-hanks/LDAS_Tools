#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3segment( "segmentgroup:segment:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "segmentgroup:segment:process_id" ); 
	LdasContainer segment_group_cont ( "segmentgroup:segment:segment_group" ); 
	INT_4S	version[ARRAYSIZE];
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"segmentgroup:segment:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"segmentgroup:segment:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_segment_group () {

// fill in segment_group array
// primary key
// NOT NULL
	CHAR segment_group[64];
	CHAR segment_groupattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(segment_group, (size_t) 64, "segment_group_%d", i + thistime + 1 );
		sprintf(segment_groupattr, "segment:%s", segment_group);
		LdasArray< CHAR > segment_group_vector ( segment_group, strlen(segment_group),
			"segmentgroup:segment:segment_group" );
		segment_group_vector.setComment( segment_groupattr );
		segment_group_cont.push_back( segment_group_vector );
	}
}

static void setup_version () {

// fill in version array
// primary key
// NOT NULL
	for (int i=0; i < numElements; i++) {
		local::version[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_start_time () {

// fill in start_time array
// primary key
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// primary key
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] = (INT_4S) ( i + gps_secs );
}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

void setup_segment ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3segment.push_back( process_id_cont ) ;
	setup_segment_group();
	ilwd3segment.push_back(  segment_group_cont ) ;
	setup_version();
	ilwd3segment.push_back(  LdasArray< INT_4S >(local::version, numElements,
	"segmentgroup:segment:version" ) );
	setup_start_time();
	ilwd3segment.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"segmentgroup:segment:start_time" ) );
	setup_start_time_ns();
	ilwd3segment.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"segmentgroup:segment:start_time_ns" ) );
	setup_end_time();
	ilwd3segment.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"segmentgroup:segment:end_time" ) );
	setup_end_time_ns();
	ilwd3segment.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"segmentgroup:segment:end_time_ns" ) );
}

#ifdef TESTMAIN 
 void setup_segment( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_segment( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3segment );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
