#include <cstring>
#include <algorithm>
#include <iostream>
#include <climits>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3waveburst_mime( "waveburst_mimegroup:waveburst_mime:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "waveburst_mimegroup:waveburst_mime:process_id" ); 
	LdasContainer event_id_cont ( "waveburst_mimegroup:waveburst_mime:event_id" ); 
	LdasContainer mimetype_cont ( "waveburst_mimegroup:waveburst_mime:mimetype" ); 
	INT_4S	t_size[ARRAYSIZE];
	INT_4S	f_size[ARRAYSIZE];
	LdasContainer cluster_o_cont ( "waveburst_mimegroup:waveburst_mime:cluster_o" ); 
	LdasContainer cluster_p_cont ( "waveburst_mimegroup:waveburst_mime:cluster_p" ); 
} // end namespace

#ifdef TESTMAIN
#include	"waveburst.h"
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"waveburst_symref.h"
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"waveburst_mimegroup:waveburst_mime:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"waveburst_mimegroup:waveburst_mime:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
	char event_id_str[30] ;
	CHAR_U event_id[13];
	CHAR event_idattr[200];

	int i,j;
// convert event_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(event_id_str, waveburst[ i % waveburst_len	].EVENT_ID);
		for ( int x = 0, j = 0; x < strlen(event_id_str); x=x+2, j++ ) {
			event_id[j] = ((event_id_str[x]-'0' ) << 4) + (event_id_str[x+1]-'0');
		}
		sprintf(event_idattr, "event_id:%s", event_id_str);
		LdasArray< CHAR_U > event_id_vector ( event_id, 13,
		"waveburst_mimegroup:waveburst_mime:event_id" );
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );
	}
}

static void setup_event_id_symref () {
// fill in event_id array
// NOT NULL
	CHAR event_id[30] ;
	CHAR event_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(event_id, waveburst[ i % waveburst_len ].EVENT_ID );
		sprintf( event_idattr, "event_id:%s", event_id );
		LdasArray< CHAR > event_id_vector ( event_id, strlen(event_id),
		"waveburst_mimegroup:waveburst_mime:event_id");
		event_id_vector.setComment(event_idattr);
		event_id_cont.push_back( event_id_vector );}
}

static void setup_mimetype () {

// fill in mimetype array
	CHAR mimetype[64];
	CHAR mimetypeattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(mimetype, (size_t) 64, "mimetype_%d", i + thistime + 1 );
		sprintf(mimetypeattr, "waveburst_mime:%s", mimetype);
		LdasArray< CHAR > mimetype_vector ( mimetype, strlen(mimetype),
			"waveburst_mimegroup:waveburst_mime:mimetype" );
		mimetype_vector.setComment( mimetypeattr );
		mimetype_cont.push_back( mimetype_vector );
	}
}

static void setup_t_size () {

// fill in t_size array
	for (int i=0; i < numElements; i++) {
		t_size[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_f_size () {

// fill in f_size array
	for (int i=0; i < numElements; i++) {
		f_size[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_cluster_o () {

// fill in cluster_o array
// fill in cluster_o array
// NOT NULL
	if	( numElements > 1000 ) {
		BLOBSIZE=1 ;
	}

	cluster_o_cont.setComment("cluster_o blob arrays");

	char name[100];
	char units[]="blob_units";

	for ( int i = 0; i < numElements ; i++ ) {
		sprintf (name, "%s", "cluster_ogroup:waveburst_mime:cluster_o");
		size_t size( ( i %100 + 1 )  * BLOBSIZE );		
       	const CHAR_U fill_char( i%UCHAR_MAX );
        CHAR_U buffer[ size ];
        fill( buffer, buffer + size, fill_char );

		try 
		{
			LdasArray< CHAR_U >* char_u_vector = new 
		   		LdasArray< CHAR_U >( buffer, size, name, units );
			// create variable size blobs
		    cluster_o_cont.push_back( char_u_vector, 
           		LdasContainer::NO_ALLOCATE,
                LdasContainer::OWN );
		}
			catch( const std::exception& exc )
		{
		  	cout << "Exception: " << exc.what() << endl;
           	exit( 1 );
		}
           	catch( const FormatException& exc )
		{
           	const size_t exc_size( exc.getSize() );
           	cout << "Format exception: ";
           	for( size_t i = 0; i < exc_size; ++i )
		  	{
		    	cout << exc[ i ].getMessage() << " ";
		  	}
            cout << endl;
           	exit( 1 );
		} 
	}
	cluster_o_cont.setWriteFormat(ILwd::BASE64);
	cluster_o_cont.setWriteCompression( ILwd::GZIP0);
}

static void setup_cluster_p () {

// fill in cluster_p array
// fill in cluster_p array
// NOT NULL
	if	( numElements > 1000 ) {
		BLOBSIZE=1 ;
	}
	cluster_p_cont.setComment("cluster_p blob arrays");

	char name[100];
	char units[]="blob_units";

	for ( int i = 0; i < numElements ; i++ ) {	
		sprintf (name, "%s", "cluster_pgroup:waveburst_mime:cluster_p");
		size_t size( ( i % 100 + 1 )  * BLOBSIZE );
        const CHAR_U fill_char( i%UCHAR_MAX );
        CHAR_U buffer[ size ];
        fill( buffer, buffer + size, fill_char );

        try
		{
  		   LdasArray< CHAR_U >* char_u_vector = new 
		   		LdasArray< CHAR_U >( buffer, size, name, units );

           // create variable size blobs
		   cluster_p_cont.push_back( char_u_vector, 
           		LdasContainer::NO_ALLOCATE,
                LdasContainer::OWN );
		}
          	catch( const std::exception& exc )
		{
		   	cout << "Exception: " << exc.what() << endl;
               	   exit( 1 );
	  	}
           	catch( const FormatException& exc )
		{
           	const size_t exc_size( exc.getSize() );
           	cout << "Format exception: ";
           	for( size_t i = 0; i < exc_size; ++i )
		  	{
		    	cout << exc[ i ].getMessage() << " ";
		  	}
            cout << endl;
           	exit( 1 );
		}
	}
	cluster_p_cont.setWriteFormat(ILwd::BASE64);
	cluster_p_cont.setWriteCompression( ILwd::GZIP0);
}

void setup_waveburst_mime ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3waveburst_mime.push_back( process_id_cont ) ;
	setup_event_id();
	ilwd3waveburst_mime.push_back( event_id_cont ) ;
	setup_mimetype();
	ilwd3waveburst_mime.push_back(  mimetype_cont ) ;
	setup_t_size();
	ilwd3waveburst_mime.push_back(  LdasArray< INT_4S >(t_size, numElements,
	"waveburst_mimegroup:waveburst_mime:t_size" ) );
	setup_f_size();
	ilwd3waveburst_mime.push_back(  LdasArray< INT_4S >(f_size, numElements,
	"waveburst_mimegroup:waveburst_mime:f_size" ) );
	setup_cluster_o();
	ilwd3waveburst_mime.push_back( cluster_o_cont ) ;
	setup_cluster_p();
	ilwd3waveburst_mime.push_back( cluster_p_cont ) ;
}

#ifdef TESTMAIN 
 void setup_waveburst_mime( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_waveburst_mime( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3waveburst_mime );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
