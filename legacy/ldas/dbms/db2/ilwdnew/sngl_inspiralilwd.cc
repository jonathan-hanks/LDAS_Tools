#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3sngl_inspiral( "sngl_inspiralgroup:sngl_inspiral:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "sngl_inspiralgroup:sngl_inspiral:process_id" ); 
	LdasContainer filter_id_cont ( "sngl_inspiralgroup:sngl_inspiral:filter_id" ); 
	LdasContainer ifo_cont ( "sngl_inspiralgroup:sngl_inspiral:ifo" ); 
	LdasContainer search_cont ( "sngl_inspiralgroup:sngl_inspiral:search" ); 
	LdasContainer channel_cont ( "sngl_inspiralgroup:sngl_inspiral:channel" ); 
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
	REAL_8	end_time_gmst[ARRAYSIZE];
	INT_4S	impulse_time[ARRAYSIZE];
	INT_4S	impulse_time_ns[ARRAYSIZE];
	REAL_8	template_duration[ARRAYSIZE];
	REAL_8	event_duration[ARRAYSIZE];
	REAL_4	amplitude[ARRAYSIZE];
	REAL_4	eff_distance[ARRAYSIZE];
	REAL_4	coa_phase[ARRAYSIZE];
	REAL_4	mass1[ARRAYSIZE];
	REAL_4	mass2[ARRAYSIZE];
	REAL_4	mchirp[ARRAYSIZE];
	REAL_4	eta[ARRAYSIZE];
	REAL_4	tau0[ARRAYSIZE];
	REAL_4	tau2[ARRAYSIZE];
	REAL_4	tau3[ARRAYSIZE];
	REAL_4	tau4[ARRAYSIZE];
	REAL_4	tau5[ARRAYSIZE];
	REAL_4	ttotal[ARRAYSIZE];
	REAL_4	psi0[ARRAYSIZE];
	REAL_4	psi3[ARRAYSIZE];
	REAL_4	f_final[ARRAYSIZE];
	REAL_4	snr[ARRAYSIZE];
	REAL_4	chisq[ARRAYSIZE];
	INT_4S	chisq_dof[ARRAYSIZE];
	REAL_8	sigmasq[ARRAYSIZE];
	REAL_4	alpha[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#include	"filter.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#include	"filter_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"sngl_inspiralgroup:sngl_inspiral:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"sngl_inspiralgroup:sngl_inspiral:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_filter_id () {

// fill in filter_id array
	char filter_id_str[30] ;
	CHAR_U filter_id[13];
	CHAR filter_idattr[200];

	int i,j;
// convert filter_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id_str, filter[ i % filter_len	].FILTER_ID);
		for ( int x = 0, j = 0; x < strlen(filter_id_str); x=x+2, j++ ) {
			filter_id[j] = ((filter_id_str[x]-'0' ) << 4) + (filter_id_str[x+1]-'0');
		}
		sprintf(filter_idattr, "filter_id:%s", filter_id_str);
		LdasArray< CHAR_U > filter_id_vector ( filter_id, 13,
		"sngl_inspiralgroup:sngl_inspiral:filter_id" );
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );
	}
}

static void setup_filter_id_symref () {
// fill in filter_id array
// NOT NULL
	CHAR filter_id[30] ;
	CHAR filter_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(filter_id, filter[ i % filter_len ].FILTER_ID );
		sprintf( filter_idattr, "filter_id:%s", filter_id );
		LdasArray< CHAR > filter_id_vector ( filter_id, strlen(filter_id),
		"sngl_inspiralgroup:sngl_inspiral:filter_id");
		filter_id_vector.setComment(filter_idattr);
		filter_id_cont.push_back( filter_id_vector );}
}

static void setup_ifo () {

// fill in ifo array
// NOT NULL
// fill in ifo array
// NOT NULL
CHAR ifo[2];
CHAR ifoattr[202];
for (int i=0; i < numElements; i++) {
	snprintf(ifo, (size_t) 2, "%s", "C1");
	sprintf(ifoattr, "sngl_inspiral:%s", ifo);
		LdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),
		"sngl_inspiralgroup:sngl_inspiral:ifo" );
		ifo_vector.setComment( ifoattr );
		ifo_cont.push_back( ifo_vector );
}
}

static void setup_search () {

// fill in search array
// NOT NULL
	CHAR search[24];
	CHAR searchattr[224];

	for (int i=0; i < numElements; i++) {
		snprintf(search, (size_t) 24, "search_%d", i + thistime + 1 );
		sprintf(searchattr, "sngl_inspiral:%s", search);
		LdasArray< CHAR > search_vector ( search, strlen(search),
			"sngl_inspiralgroup:sngl_inspiral:search" );
		search_vector.setComment( searchattr );
		search_cont.push_back( search_vector );
	}
}

static void setup_channel () {

// fill in channel array
	CHAR channel[64];
	CHAR channelattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(channel, (size_t) 64, "channel_%d", i + thistime + 1 );
		sprintf(channelattr, "sngl_inspiral:%s", channel);
		LdasArray< CHAR > channel_vector ( channel, strlen(channel),
			"sngl_inspiralgroup:sngl_inspiral:channel" );
		channel_vector.setComment( channelattr );
		channel_cont.push_back( channel_vector );
	}
}

static void setup_end_time () {

// fill in end_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] = (INT_4S) ( i + gps_secs );
}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

static void setup_end_time_gmst () {

// fill in end_time_gmst array
	for (int i=0; i < numElements; i++) {
		end_time_gmst[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_impulse_time () {

// fill in impulse_time array
	for (int i=0; i < numElements; i++) {
		impulse_time[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_impulse_time_ns () {

// fill in impulse_time_ns array
	for (int i=0; i < numElements; i++) {
		impulse_time_ns[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_template_duration () {

// fill in template_duration array
	for (int i=0; i < numElements; i++) {
		template_duration[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_event_duration () {

// fill in event_duration array
	for (int i=0; i < numElements; i++) {
		event_duration[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_amplitude () {

// fill in amplitude array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		amplitude[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_eff_distance () {

// fill in eff_distance array
	for (int i=0; i < numElements; i++) {
		eff_distance[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_coa_phase () {

// fill in coa_phase array
	for (int i=0; i < numElements; i++) {
		coa_phase[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_mass1 () {

// fill in mass1 array
	for (int i=0; i < numElements; i++) {
		mass1[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_mass2 () {

// fill in mass2 array
	for (int i=0; i < numElements; i++) {
		mass2[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_mchirp () {

// fill in mchirp array
	for (int i=0; i < numElements; i++) {
		mchirp[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_eta () {

// fill in eta array
	for (int i=0; i < numElements; i++) {
		eta[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_tau0 () {

// fill in tau0 array
	for (int i=0; i < numElements; i++) {
		tau0[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_tau2 () {

// fill in tau2 array
	for (int i=0; i < numElements; i++) {
		tau2[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_tau3 () {

// fill in tau3 array
	for (int i=0; i < numElements; i++) {
		tau3[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_tau4 () {

// fill in tau4 array
	for (int i=0; i < numElements; i++) {
		tau4[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_tau5 () {

// fill in tau5 array
	for (int i=0; i < numElements; i++) {
		tau5[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_ttotal () {

// fill in ttotal array
	for (int i=0; i < numElements; i++) {
		ttotal[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_psi0 () {

// fill in psi0 array
	for (int i=0; i < numElements; i++) {
		psi0[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_psi3 () {

// fill in psi3 array
	for (int i=0; i < numElements; i++) {
		psi3[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_f_final () {

// fill in f_final array
	for (int i=0; i < numElements; i++) {
		f_final[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_snr () {

// fill in snr array
	for (int i=0; i < numElements; i++) {
		snr[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_chisq () {

// fill in chisq array
	for (int i=0; i < numElements; i++) {
		chisq[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_chisq_dof () {

// fill in chisq_dof array
	for (int i=0; i < numElements; i++) {
		chisq_dof[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_sigmasq () {

// fill in sigmasq array
	for (int i=0; i < numElements; i++) {
		sigmasq[i] = (REAL_8) i + thistime + 1 ;
	}
}

static void setup_alpha () {

// fill in alpha array
	for (int i=0; i < numElements; i++) {
		alpha[i] = (REAL_4) i + thistime + 1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
// NOT NULL
// event_id is uniqueId, generated by metadataAPI

}

void setup_sngl_inspiral ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3sngl_inspiral.push_back( process_id_cont ) ;
	setup_filter_id();
	ilwd3sngl_inspiral.push_back( filter_id_cont ) ;
	setup_ifo();
	ilwd3sngl_inspiral.push_back(  ifo_cont ) ;
	setup_search();
	ilwd3sngl_inspiral.push_back(  search_cont ) ;
	setup_channel();
	ilwd3sngl_inspiral.push_back(  channel_cont ) ;
	setup_end_time();
	ilwd3sngl_inspiral.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"sngl_inspiralgroup:sngl_inspiral:end_time" ) );
	setup_end_time_ns();
	ilwd3sngl_inspiral.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"sngl_inspiralgroup:sngl_inspiral:end_time_ns" ) );
	setup_end_time_gmst();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_8 >( end_time_gmst, numElements,
	"sngl_inspiralgroup:sngl_inspiral:end_time_gmst" ) );
	setup_impulse_time();
	ilwd3sngl_inspiral.push_back(  LdasArray< INT_4S >(impulse_time, numElements,
	"sngl_inspiralgroup:sngl_inspiral:impulse_time" ) );
	setup_impulse_time_ns();
	ilwd3sngl_inspiral.push_back(  LdasArray< INT_4S >(impulse_time_ns, numElements,
	"sngl_inspiralgroup:sngl_inspiral:impulse_time_ns" ) );
	setup_template_duration();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_8 >( template_duration, numElements,
	"sngl_inspiralgroup:sngl_inspiral:template_duration" ) );
	setup_event_duration();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_8 >( event_duration, numElements,
	"sngl_inspiralgroup:sngl_inspiral:event_duration" ) );
	setup_amplitude();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( amplitude, numElements,
	"sngl_inspiralgroup:sngl_inspiral:amplitude" ) );
	setup_eff_distance();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( eff_distance, numElements,
	"sngl_inspiralgroup:sngl_inspiral:eff_distance" ) );
	setup_coa_phase();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( coa_phase, numElements,
	"sngl_inspiralgroup:sngl_inspiral:coa_phase" ) );
	setup_mass1();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( mass1, numElements,
	"sngl_inspiralgroup:sngl_inspiral:mass1" ) );
	setup_mass2();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( mass2, numElements,
	"sngl_inspiralgroup:sngl_inspiral:mass2" ) );
	setup_mchirp();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( mchirp, numElements,
	"sngl_inspiralgroup:sngl_inspiral:mchirp" ) );
	setup_eta();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( eta, numElements,
	"sngl_inspiralgroup:sngl_inspiral:eta" ) );
	setup_tau0();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( tau0, numElements,
	"sngl_inspiralgroup:sngl_inspiral:tau0" ) );
	setup_tau2();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( tau2, numElements,
	"sngl_inspiralgroup:sngl_inspiral:tau2" ) );
	setup_tau3();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( tau3, numElements,
	"sngl_inspiralgroup:sngl_inspiral:tau3" ) );
	setup_tau4();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( tau4, numElements,
	"sngl_inspiralgroup:sngl_inspiral:tau4" ) );
	setup_tau5();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( tau5, numElements,
	"sngl_inspiralgroup:sngl_inspiral:tau5" ) );
	setup_ttotal();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( ttotal, numElements,
	"sngl_inspiralgroup:sngl_inspiral:ttotal" ) );
	setup_psi0();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( psi0, numElements,
	"sngl_inspiralgroup:sngl_inspiral:psi0" ) );
	setup_psi3();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( psi3, numElements,
	"sngl_inspiralgroup:sngl_inspiral:psi3" ) );
	setup_f_final();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( f_final, numElements,
	"sngl_inspiralgroup:sngl_inspiral:f_final" ) );
	setup_snr();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( snr, numElements,
	"sngl_inspiralgroup:sngl_inspiral:snr" ) );
	setup_chisq();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( chisq, numElements,
	"sngl_inspiralgroup:sngl_inspiral:chisq" ) );
	setup_chisq_dof();
	ilwd3sngl_inspiral.push_back(  LdasArray< INT_4S >(chisq_dof, numElements,
	"sngl_inspiralgroup:sngl_inspiral:chisq_dof" ) );
	setup_sigmasq();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_8 >( sigmasq, numElements,
	"sngl_inspiralgroup:sngl_inspiral:sigmasq" ) );
	setup_alpha();
	ilwd3sngl_inspiral.push_back(  LdasArray< REAL_4 >( alpha, numElements,
	"sngl_inspiralgroup:sngl_inspiral:alpha" ) );
	setup_event_id();
}

#ifdef TESTMAIN 
 void setup_sngl_inspiral( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_sngl_inspiral( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3sngl_inspiral );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
