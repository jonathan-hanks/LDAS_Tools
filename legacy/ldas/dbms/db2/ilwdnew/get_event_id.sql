--
-- select event_id from tables to provide data for
-- X-reference tables like sngl_datasource, coinc_sngl.
--
echo table gds_trigger;
select event_id from gds_trigger;
echo table sngl_inspiral;
select event_id from sngl_inspiral;
echo table sngl_burst;
select event_id from sngl_burst;
echo table sngl_ringdown;
select event_id from sngl_ringdown;
echo table sngl_unmodeled;
select event_id from sngl_unmodeled;
echo table sngl_dperiodic;
select event_id from sngl_dperiodic;
echo table sngl_unmodeled_v;
select event_id from sngl_unmodeled_v;
echo table multi_inspiral;
select event_id from multi_inspiral;
echo table multi_burst;
select event_id from multi_burst;
echo table waveburst;
select event_id from waveburst;
