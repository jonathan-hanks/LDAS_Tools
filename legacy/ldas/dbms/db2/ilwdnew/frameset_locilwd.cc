#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3frameset_loc( "frameset_locgroup:frameset_loc:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer name_cont ( "frameset_locgroup:frameset_loc:name" ); 
	LdasContainer media_type_cont ( "frameset_locgroup:frameset_loc:media_type" ); 
	LdasContainer node_cont ( "frameset_locgroup:frameset_loc:node" ); 
	LdasContainer media_loc_cont ( "frameset_locgroup:frameset_loc:media_loc" ); 
	LdasContainer fullname_cont ( "frameset_locgroup:frameset_loc:fullname" ); 
	INT_4S	filenum[ARRAYSIZE];
	LdasContainer decompress_cmd_cont ( "frameset_locgroup:frameset_loc:decompress_cmd" ); 
} // end namespace

#ifdef TESTMAIN
#include	"frameset.h"
#endif

#ifdef  TESTSYMREF
#include	"frameset_symref.h"
#endif

using namespace local;

static void setup_name () {

// fill in name array
// NOT NULL
	CHAR name[80];
	CHAR nameattr[280];

	for (int i=0; i < numElements; i++) {
		strcpy(name, frameset[ i % frameset_len ].NAME);
		sprintf(nameattr, "frameset_loc:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"frameset_locgroup:frameset_loc:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_media_type () {

// fill in media_type array
// NOT NULL
	CHAR media_type[16];
	CHAR media_typeattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(media_type, (size_t) 16, "media_type_%d", i + thistime + 1 );
		sprintf(media_typeattr, "frameset_loc:%s", media_type);
		LdasArray< CHAR > media_type_vector ( media_type, strlen(media_type),
			"frameset_locgroup:frameset_loc:media_type" );
		media_type_vector.setComment( media_typeattr );
		media_type_cont.push_back( media_type_vector );
	}
}

static void setup_node () {

// fill in node array
// primary key
// NOT NULL
	CHAR node[48];
	CHAR nodeattr[248];

	for (int i=0; i < numElements; i++) {
		snprintf(node, (size_t) 48, "node_%d", i + thistime + 1 );
		sprintf(nodeattr, "frameset_loc:%s", node);
		LdasArray< CHAR > node_vector ( node, strlen(node),
			"frameset_locgroup:frameset_loc:node" );
		node_vector.setComment( nodeattr );
		node_cont.push_back( node_vector );
	}
}

static void setup_media_loc () {

// fill in media_loc array
	CHAR media_loc[48];
	CHAR media_locattr[248];

	for (int i=0; i < numElements; i++) {
		snprintf(media_loc, (size_t) 48, "media_loc_%d", i + thistime + 1 );
		sprintf(media_locattr, "frameset_loc:%s", media_loc);
		LdasArray< CHAR > media_loc_vector ( media_loc, strlen(media_loc),
			"frameset_locgroup:frameset_loc:media_loc" );
		media_loc_vector.setComment( media_locattr );
		media_loc_cont.push_back( media_loc_vector );
	}
}

static void setup_fullname () {

// fill in fullname array
// primary key
// NOT NULL
	CHAR fullname[128];
	CHAR fullnameattr[328];

	for (int i=0; i < numElements; i++) {
		snprintf(fullname, (size_t) 128, "fullname_%d", i + thistime + 1 );
		sprintf(fullnameattr, "frameset_loc:%s", fullname);
		LdasArray< CHAR > fullname_vector ( fullname, strlen(fullname),
			"frameset_locgroup:frameset_loc:fullname" );
		fullname_vector.setComment( fullnameattr );
		fullname_cont.push_back( fullname_vector );
	}
}

static void setup_filenum () {

// fill in filenum array
	for (int i=0; i < numElements; i++) {
		filenum[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_decompress_cmd () {

// fill in decompress_cmd array
	CHAR decompress_cmd[128];
	CHAR decompress_cmdattr[328];

	for (int i=0; i < numElements; i++) {
		snprintf(decompress_cmd, (size_t) 128, "decompress_cmd_%d", i + thistime + 1 );
		sprintf(decompress_cmdattr, "frameset_loc:%s", decompress_cmd);
		LdasArray< CHAR > decompress_cmd_vector ( decompress_cmd, strlen(decompress_cmd),
			"frameset_locgroup:frameset_loc:decompress_cmd" );
		decompress_cmd_vector.setComment( decompress_cmdattr );
		decompress_cmd_cont.push_back( decompress_cmd_vector );
	}
}

void setup_frameset_loc ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_name();
	ilwd3frameset_loc.push_back(  name_cont ) ;
	setup_media_type();
	ilwd3frameset_loc.push_back(  media_type_cont ) ;
	setup_node();
	ilwd3frameset_loc.push_back(  node_cont ) ;
	setup_media_loc();
	ilwd3frameset_loc.push_back(  media_loc_cont ) ;
	setup_fullname();
	ilwd3frameset_loc.push_back(  fullname_cont ) ;
	setup_filenum();
	ilwd3frameset_loc.push_back(  LdasArray< INT_4S >(filenum, numElements,
	"frameset_locgroup:frameset_loc:filenum" ) );
	setup_decompress_cmd();
	ilwd3frameset_loc.push_back(  decompress_cmd_cont ) ;
}

#ifdef TESTMAIN 
 void setup_frameset_loc( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_frameset_loc( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3frameset_loc );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
