#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

//extern	LdasContainer ilwd3process;
//extern	LdasContainer ilwd3process_params;
//extern	LdasContainer ilwd3frameset_chanlist;
//extern  LdasContainer ilwd3frameset_writer;
//extern  LdasContainer ilwd3segment_definer;
//extern  LdasContainer ilwd3segment;
//extern  LdasContainer ilwd3summ_value;
//extern  LdasContainer ilwd3summ_comment;
//extern  LdasContainer ilwd3summ_statistics;
//extern  LdasContainer ilwd3summ_spectrum;
//extern  LdasContainer ilwd3frameset;
//extern  LdasContainer ilwd3frameset_loc;
//extern  LdasContainer ilwd3filter;
//extern  LdasContainer ilwd3filter_params;
//extern  LdasContainer ilwd3gds_trigger;
//extern  LdasContainer ilwd3sngl_inspiral;
//extern  LdasContainer ilwd3sngl_burst;
//extern  LdasContainer ilwd3sngl_ringdown;
//extern  LdasContainer ilwd3sngl_unmodeled;
//extern  LdasContainer ilwd3sngl_unmodeled_v;
//extern  LdasContainer ilwd3sngl_dperiodic;
//extern  LdasContainer ilwd3sngl_datasource;
//extern  LdasContainer ilwd3sngl_transdata;
//extern  LdasContainer ilwd3multi_inspiral;
//extern  LdasContainer ilwd3multi_burst;
extern  LdasContainer ilwd3coinc_sngl;

//extern void setup_process();
//extern void setup_process_params();
//extern void setup_frameset_chanlist();
//extern void setup_frameset_writer();
//extern void setup_segment_definer();
//extern void setup_segment();
//extern void setup_summ_value();
//extern void setup_summ_comment();
//extern void setup_summ_statistics();
//extern void setup_summ_spectrum();
//extern void setup_frameset();
//extern void setup_frameset_loc();
//extern void setup_filter();
//extern void setup_filter_params();
//extern void setup_gds_trigger();
//extern void setup_sngl_inspiral();
//extern void setup_sngl_burst();
//extern void setup_sngl_ringdown();
//extern void setup_sngl_unmodeled();
//extern void setup_sngl_unmodeled_v();
//extern void setup_sngl_dperiodic();
//extern void setup_sngl_datasource();
//extern void setup_sngl_transdata();
//extern void setup_multi_inspiral();
//extern void setup_multi_burst();
extern void setup_coinc_sngl();

int main(int argc, char **argv) {

	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:process:file" );
    
	//setup_process();
	//ilwd1.push_back( ilwd3process );

	//setup_process_params();
	//ilwd1.push_back( ilwd3process_params );

    //setup_frameset_chanlist();
    //ilwd1.push_back( ilwd3frameset_chanlist );
    
    //setup_frameset_writer();
    //ilwd1.push_back( ilwd3frameset_writer );
    
    //setup_segment_definer();
    //ilwd1.push_back( ilwd3segment_definer );
    
    //setup_segment();
    //ilwd1.push_back( ilwd3segment );
    
    //setup_summ_value();
    //ilwd1.push_back( ilwd3summ_value );
    
    //setup_summ_comment();
    //ilwd1.push_back( ilwd3summ_comment );
    
    //setup_summ_statistics();
    //ilwd1.push_back( ilwd3summ_statistics );
    
    //setup_summ_spectrum();
    //ilwd1.push_back( ilwd3summ_spectrum );
    
    //setup_frameset();
    //ilwd1.push_back( ilwd3frameset );
    
    //setup_frameset_loc();
    //ilwd1.push_back( ilwd3frameset_loc );
    
    //setup_filter();
    //ilwd1.push_back( ilwd3filter );

    //setup_filter_params();
    //ilwd1.push_back( ilwd3filter_params );   
    
    //setup_gds_trigger();
    //ilwd1.push_back( ilwd3gds_trigger );
    
    //setup_sngl_inspiral();
    //ilwd1.push_back( ilwd3sngl_inspiral );
    
    //setup_sngl_burst();
    //ilwd1.push_back( ilwd3sngl_burst );
    
    //setup_sngl_ringdown();
    //ilwd1.push_back( ilwd3sngl_ringdown );
    
    //setup_sngl_unmodeled();
    //ilwd1.push_back( ilwd3sngl_unmodeled );
    
    //setup_sngl_unmodeled_v();
    //ilwd1.push_back( ilwd3sngl_unmodeled_v ); 
    
    //setup_sngl_dperiodic();
    //ilwd1.push_back( ilwd3sngl_dperiodic ); 
    
    //setup_sngl_datasource();
    //ilwd1.push_back( ilwd3sngl_datasource ); 
    
    //setup_sngl_transdata();
    //ilwd1.push_back( ilwd3sngl_transdata ); 
    
    //setup_multi_inspiral();
    //ilwd1.push_back( ilwd3multi_inspiral ); 
    
    //setup_multi_burst();
    //ilwd1.push_back( ilwd3multi_burst ); 
    
    setup_coinc_sngl();
    ilwd1.push_back( ilwd3coinc_sngl );
    
	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
