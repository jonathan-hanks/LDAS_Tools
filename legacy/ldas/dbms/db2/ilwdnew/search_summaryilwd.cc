#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3search_summary( "search_summarygroup:search_summary:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "search_summarygroup:search_summary:process_id" ); 
	LdasContainer shared_object_cont ( "search_summarygroup:search_summary:shared_object" ); 
	LdasContainer lalwrapper_cvs_tag_cont ( "search_summarygroup:search_summary:lalwrapper_cvs_tag" ); 
	LdasContainer lal_cvs_tag_cont ( "search_summarygroup:search_summary:lal_cvs_tag" ); 
	LdasContainer comment_cont ( "search_summarygroup:search_summary:comment" ); 
	INT_4S	in_start_time[ARRAYSIZE];
	INT_4S	in_start_time_ns[ARRAYSIZE];
	INT_4S	in_end_time[ARRAYSIZE];
	INT_4S	in_end_time_ns[ARRAYSIZE];
	INT_4S	out_start_time[ARRAYSIZE];
	INT_4S	out_start_time_ns[ARRAYSIZE];
	INT_4S	out_end_time[ARRAYSIZE];
	INT_4S	out_end_time_ns[ARRAYSIZE];
	INT_4S	nevents[ARRAYSIZE];
	INT_4S	nnodes[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// primary key
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"search_summarygroup:search_summary:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"search_summarygroup:search_summary:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_shared_object () {

// fill in shared_object array
// NOT NULL
	CHAR shared_object[64];
	CHAR shared_objectattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(shared_object, (size_t) 64, "shared_object_%d", i + thistime + 1 );
		sprintf(shared_objectattr, "search_summary:%s", shared_object);
		LdasArray< CHAR > shared_object_vector ( shared_object, strlen(shared_object),
			"search_summarygroup:search_summary:shared_object" );
		shared_object_vector.setComment( shared_objectattr );
		shared_object_cont.push_back( shared_object_vector );
	}
}

static void setup_lalwrapper_cvs_tag () {

// fill in lalwrapper_cvs_tag array
// NOT NULL
	CHAR lalwrapper_cvs_tag[128];
	CHAR lalwrapper_cvs_tagattr[328];

	for (int i=0; i < numElements; i++) {
		snprintf(lalwrapper_cvs_tag, (size_t) 128, "lalwrapper_cvs_tag_%d", i + thistime + 1 );
		sprintf(lalwrapper_cvs_tagattr, "search_summary:%s", lalwrapper_cvs_tag);
		LdasArray< CHAR > lalwrapper_cvs_tag_vector ( lalwrapper_cvs_tag, strlen(lalwrapper_cvs_tag),
			"search_summarygroup:search_summary:lalwrapper_cvs_tag" );
		lalwrapper_cvs_tag_vector.setComment( lalwrapper_cvs_tagattr );
		lalwrapper_cvs_tag_cont.push_back( lalwrapper_cvs_tag_vector );
	}
}

static void setup_lal_cvs_tag () {

// fill in lal_cvs_tag array
// NOT NULL
	CHAR lal_cvs_tag[128];
	CHAR lal_cvs_tagattr[328];

	for (int i=0; i < numElements; i++) {
		snprintf(lal_cvs_tag, (size_t) 128, "lal_cvs_tag_%d", i + thistime + 1 );
		sprintf(lal_cvs_tagattr, "search_summary:%s", lal_cvs_tag);
		LdasArray< CHAR > lal_cvs_tag_vector ( lal_cvs_tag, strlen(lal_cvs_tag),
			"search_summarygroup:search_summary:lal_cvs_tag" );
		lal_cvs_tag_vector.setComment( lal_cvs_tagattr );
		lal_cvs_tag_cont.push_back( lal_cvs_tag_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[240];
	CHAR commentattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 240, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "search_summary:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"search_summarygroup:search_summary:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

static void setup_in_start_time () {

// fill in in_start_time array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		in_start_time[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_in_start_time_ns () {

// fill in in_start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		in_start_time_ns[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_in_end_time () {

// fill in in_end_time array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		in_end_time[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_in_end_time_ns () {

// fill in in_end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		in_end_time_ns[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_out_start_time () {

// fill in out_start_time array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		out_start_time[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_out_start_time_ns () {

// fill in out_start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		out_start_time_ns[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_out_end_time () {

// fill in out_end_time array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		out_end_time[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_out_end_time_ns () {

// fill in out_end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		out_end_time_ns[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_nevents () {

// fill in nevents array
	for (int i=0; i < numElements; i++) {
		nevents[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_nnodes () {

// fill in nnodes array
	for (int i=0; i < numElements; i++) {
		nnodes[i] = (INT_4S) i + thistime + 1 ;
	}
}

void setup_search_summary ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3search_summary.push_back( process_id_cont ) ;
	setup_shared_object();
	ilwd3search_summary.push_back(  shared_object_cont ) ;
	setup_lalwrapper_cvs_tag();
	ilwd3search_summary.push_back(  lalwrapper_cvs_tag_cont ) ;
	setup_lal_cvs_tag();
	ilwd3search_summary.push_back(  lal_cvs_tag_cont ) ;
	setup_comment();
	ilwd3search_summary.push_back(  comment_cont ) ;
	setup_in_start_time();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(in_start_time, numElements,
	"search_summarygroup:search_summary:in_start_time" ) );
	setup_in_start_time_ns();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(in_start_time_ns, numElements,
	"search_summarygroup:search_summary:in_start_time_ns" ) );
	setup_in_end_time();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(in_end_time, numElements,
	"search_summarygroup:search_summary:in_end_time" ) );
	setup_in_end_time_ns();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(in_end_time_ns, numElements,
	"search_summarygroup:search_summary:in_end_time_ns" ) );
	setup_out_start_time();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(out_start_time, numElements,
	"search_summarygroup:search_summary:out_start_time" ) );
	setup_out_start_time_ns();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(out_start_time_ns, numElements,
	"search_summarygroup:search_summary:out_start_time_ns" ) );
	setup_out_end_time();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(out_end_time, numElements,
	"search_summarygroup:search_summary:out_end_time" ) );
	setup_out_end_time_ns();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(out_end_time_ns, numElements,
	"search_summarygroup:search_summary:out_end_time_ns" ) );
	setup_nevents();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(nevents, numElements,
	"search_summarygroup:search_summary:nevents" ) );
	setup_nnodes();
	ilwd3search_summary.push_back(  LdasArray< INT_4S >(nnodes, numElements,
	"search_summarygroup:search_summary:nnodes" ) );
}

#ifdef TESTMAIN 
 void setup_search_summary( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_search_summary( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3search_summary );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
