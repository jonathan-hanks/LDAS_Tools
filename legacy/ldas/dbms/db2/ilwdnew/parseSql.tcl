#! /ldcg/bin/tclsh

## ilwd idiocrasies
## 1. version conflicts with math.h so in source code
## it uses local::version, and in sql generated .h
## it uses VERSION_local.
## 2. chanlist_cb must be all 1 to match with creator_db
## Fix: also select creator_db if it is a foreign key
## 3. The summ_* tables references both segment and frameset.
## start_time, end_time; currently only the segment link
## is set up in the source code and frameset_group
## is commented out.
## 4. an events.h is used for sngl_transdata,sngl_mime and
## sngl_datasource ilwds.
## 5. coinc_sngl uses all the header files for parent records
## instead of events.h
## 
##
## set the parent table of the fields for coinc_sngl table
## fix: include clob handling
proc setparent {} {
	uplevel #0 {
		;## coinc_sngl table use all the .h files 
		if	{ [ string equal coinc_sngl $table ] } {
			switch -regexp -- $dname {
			{inspiral_id} { array set parentTable [ list $dname sngl_inspiral ] }
			{burst_id}	 { array set parentTable [ list $dname sngl_burst ] }
			{ringdown_id} { array set parentTable [ list $dname sngl_ringdown ] }
			{unmodeled_id} { array set parentTable [ list $dname sngl_unmodeled ] }
			{gdstrig_id}  { array set parentTable [ list $dname gds_trigger ] }
			{process_id}  { array set parentTable [ list $dname process ] }
			}
		} 
	}
}

##
## output src code for unique Ids to get data from header file
##			
proc handle_uniqueId {} {
	uplevel #0 {
		set dbfield [ string toupper $dname ]
		if	{ [ string equal coinc_sngl $table ] } {
			if	{ [ regexp {inspiral|burst|ringdown|unmodeled|gdstrig_id} $dname ] } {
				set dbfield EVENT_ID
			}
		} 
		puts "dname=$dname,dbfield=$dbfield,parent=$parent"
		puts $fout "\tchar ${dname}_str\[30\] ;"
        puts $fout "\tCHAR_U ${dname}\[13\];"
        puts $fout "\tCHAR ${dname}attr\[200\];\n"    
        puts $fout "\tint i,j;" 
        puts $fout "// convert $dname string to 13 bit BCD"
    	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
        puts $fout "\t\tstrcpy(${dname}_str, ${parent}\[ i % ${parent}_len	\].$dbfield);"
	    puts $fout "\t\tfor ( int x = 0, j = 0; x < strlen(${dname}_str); x=x+2, j++ ) \{"
		puts $fout "\t\t\t${dname}\[j\] = ((${dname}_str\[x\]-'0' ) << 4) + (${dname}_str\[x+1\]-'0');\n\t\t\}"
		puts $fout "\t\tsprintf(${dname}attr, \"$dname:%s\", ${dname}_str);"
		puts $fout "\t\tLdasArray< CHAR_U > ${dname}_vector ( $dname, 13,"
        puts $fout "\t\t\"${table}group:${table}:${dname}\" );"
        puts $fout "\t\t${dname}_vector.setComment(${dname}attr);"
		puts $fout "\t\t${dname}_cont.push_back( ${dname}_vector );"
        puts $fout "\t\}\n\}\n"   
		puts $fout "static void setup_${dname}_symref () \{"
    	puts $fout "// fill in ${dname} array"
    	puts $fout "// NOT NULL"
		puts $fout "\tCHAR ${dname}\[30\] ;"
		puts $fout "\tCHAR ${dname}attr\[200\];"    
		puts $fout "\tfor (int i=0; i < numElements; i++) \{"
		puts $fout "\t\tstrcpy(${dname}, ${parent}\[ i % ${parent}_len \].$dbfield );"
		puts $fout "\t\tsprintf( ${dname}attr, \"${dname}:%s\", ${dname} );"
		puts $fout "\t\tLdasArray< CHAR > ${dname}_vector ( ${dname}, strlen(${dname}),"
        puts $fout "\t\t\"${table}group:${table}:${dname}\");"
        puts $fout "\t\t${dname}_vector.setComment(${dname}attr);"
		puts $fout "\t\t${dname}_cont.push_back( ${dname}_vector );\}"
	}		
}

##
## output src code for unique Ids to get data from header file
##	
proc handle_sngl_unmodeled_v_event_id {} {
	uplevel #0 {
		set parent $parentTable($dname)
	}
	handle_uniqueId 
}

##
## output special handling for ifo
##	
proc handle_ifo {} {
	uplevel #0 {
	puts $fout "// fill in ifo array"
	puts $fout "// NOT NULL"
	puts $fout "CHAR ifo\[2\];"
	puts $fout "CHAR ifoattr\[202\];"

	puts $fout "for (int i=0; i < numElements; i++) \{"
	puts $fout "\tsnprintf(ifo, (size_t) 2, \"%s\", \"[ set ifo-site($domain) ]\");"
	puts $fout "\tsprintf(ifoattr, \"$table:%s\", ifo);"
	puts $fout "\t\tLdasArray< CHAR > ifo_vector ( ifo, strlen(ifo),"
	puts $fout "\t\t\"${table}group:$table:ifo\" );"
	puts $fout "\t\tifo_vector.setComment( ifoattr );"
	puts $fout "\t\tifo_cont.push_back( ifo_vector );\n\}"
	}
}

proc handle_cdb {} {
	uplevel #0 {
	puts $fout "// NOT NULL"
	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
	puts $fout "\t\t${dname}\[i\] = (INT_4S) 1 ;\n\t\}"
	}
}

proc handle_event_table {} {
	uplevel #0 {
	
	puts $fout	"// fill in event_table array"
	puts $fout 	"// NOT NULL"
	puts $fout	"\tCHAR event_table\[$dsize\];"
	puts $fout	"\tCHAR event_tableattr\[[expr $dsize+200]\];"

	puts $fout	"\tfor (int i=0; i < numElements; i++) \{"
    puts $fout 	"\t\tsprintf(event_table, \"%s\", events\[i\].TABLE);"
	puts $fout	"\t\tsprintf(event_tableattr, \"event_table:%s\", event_table);"
	puts $fout	"\t\tLdasArray< CHAR > event_table_vector ( event_table, strlen(event_table)," 
    puts $fout  "\t\t\"${table}group:$table:event_table\" );"
    puts $fout	"\tevent_table_vector.setComment( event_tableattr );"
	puts $fout	"\tevent_table_cont.push_back( event_table_vector );\}"
	}
}

proc handle_start_time {} {
	uplevel #0 {
    puts $fout "\tget_gpstime( &gps_secs, &gps_nanosecs );"    
	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
	puts "in handle_start_time"
	if	{ [ info exist parentTable($dname) ] } {
		puts $fout "\t\tstart_time\[i\] = \
		([ set ${dname}(ilwdtype)]) atol (${parent}\[ i % ${parent}_len \].[string toupper $dname]);\n\t\}"
	} else {
		puts $fout "\t\tstart_time\[i\] = ([ set ${dname}(ilwdtype)]) ( i + gps_secs );\}"
	}
	}
}

proc handle_start_time_ns {} {
	uplevel #0 {
	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
	puts $fout "\t\tstart_time_ns\[i\] = (INT_4S) ( i + gps_nanosecs );\}"
	}
}

proc handle_end_time {} {
	uplevel #0 {
    puts $fout "\tget_gpstime( &gps_secs, &gps_nanosecs );"    
	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
	if	{ [ info exist parentTable($dname) ] } {
		puts $fout "\t\tend_time\[i\] = \
		([ set ${dname}(ilwdtype)]) atol(${parent}\[ i % ${parent}_len \].[string toupper $dname]);\n\t\}"
	} else {
		puts $fout "\t\tend_time\[i\] = ([ set ${dname}(ilwdtype)]) ( i + gps_secs );\n\}"
	}
	}
}

proc handle_end_time_ns {} {
	uplevel #0 {
	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
	puts $fout "\t\tend_time_ns\[i\] = (INT_4S) ( i + gps_nanosecs );\n\t\}"
	}
}

proc handle_spectrum_length {} {
	uplevel #0 {
	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
	puts $fout "\t\tspectrum_length\[i\] = (INT_4S) ( i + 1 ) * (BLOBSIZE/sizeof(BLOB_TYPE) );\n\t\}"
	}
}

##
## These tables use event id header file 
##
proc isTernary {table} {
	uplevel #0 {
	set is_summ 0
	set ternary 0
	if	{  [ regexp {sngl_transdata|sngl_datasource|sngl_mime} $table ] } {
		set ternary 1
	} else {
		if	{ [ regexp {summ_} $table ] } {
			set is_summ 1
		} 
	}
	}
}

##
## append a namespace to vars that could conflict 
##
proc resolve_conflict {} {
	uplevel #0 {
		if	{ [ lsearch -exact $conflicts $dname ] != -1 } {
			return "local::${dname}"
		} else {
			return $dname
		}
	}
}

##
## output blobdata generation 
##
proc blobdata {} {

    uplevel #0 {
	puts $fout "// fill in $dname array"
	if	{ [ set ${dname}(null) ] } {
		puts $fout "// NOT NULL"
	}
	puts $fout "\tBLOB_TYPE blob\[BLOBSIZE*1000\] ;"
    puts $fout "\tint i ;"
    puts $fout "\tfor \( i = 0; i < 1000*BLOBSIZE ; i++ \) \{"
    puts $fout "\t\tblob\[i\] = 42949672 + i ;\n\t\}" 
    puts $fout "\t${dname}_cont.setComment(\"${dname} blob arrays\");\n"   
    puts $fout "\tchar name\[100\];"
    puts $fout "\tchar units\[\]=\"blob_units\";"
    puts $fout "\tCHAR_U buffer\[1024*1000\];\n"	
    puts $fout "\tfor ( i = 0; i < numElements ; i++ ) \{"
    puts $fout "\t\tsprintf \(name, \"%s\", \"${dname}group:${table}:${dname}\");"
    puts $fout "\t\tsize_t size( ( i + 1 )  * BLOBSIZE );"
    puts $fout "\t\tmemcpy(buffer, blob, size);"
    puts $fout "\t\tLdasArray< CHAR_U > char_u_vector ( buffer, size ,name, units );"
    puts $fout "\t\t// create variable size blobs"
    puts $fout "\t\t${dname}_cont.push_back( char_u_vector );\n\t\}"
    puts $fout "\t${dname}_cont.setWriteFormat(ILwd::BASE64);"
    puts $fout "\t${dname}_cont.setWriteCompression( ILwd::GZIP0);"
	}
}

##
## push containers src code
##
proc out_name { name } {
    uplevel #0 {
		set var [ resolve_conflict ]
        switch -exact -- [set ${dname}(ilwdtype)] {
    
        CHAR      { ;## do not link frameset for now if summ_* tables 
					if	{ $is_summ && [ string equal frameset_group $dname ] } {
						puts -nonewline $fout "//"
					}
					puts $fout "\tilwd3${table}.push_back(  ${var}_cont ) ;" 
				  }
        REAL_8    { puts $fout "\tilwd3${table}.push_back(  LdasArray< REAL_8 >( ${var}, numElements,"
					puts $fout "\t\"${table}group:$table:$dname\" ) );"                    }
        REAL_4    { puts $fout "\tilwd3${table}.push_back(  LdasArray< REAL_4 >( ${var}, numElements,"
					puts $fout "\t\"${table}group:$table:$dname\" ) );"                    }              
        INT_4S    { puts $fout "\tilwd3${table}.push_back(  LdasArray< INT_4S >(${var}, numElements," 
					puts $fout "\t\"${table}group:$table:$dname\" ) );"                    }
        INT_2S    { puts $fout "\tilwd3${table}.push_back(  LdasArray< INT_2S >(${var}, numElements," 
					puts $fout "\t\"${table}group:$table:$dname\" ) );"                    }
        CHAR_U    { if { [ info exist ${dname}(uniqueId) ] && ! [ info exist parentTable($dname) ] } {
					} else {
						puts $fout "\tilwd3${table}.push_back( ${var}_cont ) ;"  
				  	}  
				  }                                       
        default   { puts "type = [set ${dname}(dtype)] " }
        }
    }
}


proc set_ilwd_type  {} {

    uplevel #0 {
        switch -regexp -- $dtype {
        
        {BLOB}    {  array set ${dname} { ilwdtype CHAR_U } } 
        {TIMESTAMP} {  array set ${dname} { ilwdtype CHAR } }
        {CHAR_BIT_DATA} {  array set ${dname} { ilwdtype CHAR_U } } 		
        {CHAR\(\d+\)} {  array set ${dname} { ilwdtype CHAR } 
                    array set ${dname} [ list dsize $dsize ] } 
        {double|DOUBLE}  {  array set ${dname} { ilwdtype REAL_8 } }
        {FLOAT}   - 
        {REAL}    {  array set ${dname} { ilwdtype REAL_4 } } 
        {INTEGER} {  array set ${dname} { ilwdtype INT_4S } }
        {SMALLINT} { array set ${dname} { ilwdtype INT_2S } }
        default { puts "unknown type = $dtype" }
        
        }
    }
}

## avoid NOT NULL restrictions by having i not 0
proc out_func  {} {
    uplevel #0 {
		if	{ [ info exist ${dname}(dsize) ] } {
			set dsize [ set ${dname}(dsize) ]
		} else {
			set dsize 30
		}
		if	{ [ string length [ info procs handle_${table}_${dname} ] ]  } {
			handle_${table}_${dname}
			return
		}
		puts "$dname, [ info procs handle_${dname} ] "
		if	{ [ string length [ info procs handle_${dname} ] ]  } {
			handle_${dname}
			return
		}
		if	{ [ regexp {_cdb} $dname ] } {
			handle_cdb
			return
		}
		set var [ resolve_conflict ]
        switch -regexp -- [ set ${dname}(dtype) ] {
            {string} { 	puts $fout  "\tchar buf\[100\];"
                     	puts $fout  "\tfor (int i=0; i < numElements; i++) \{"
                     	puts $fout  "\t    sprintf(buf, \"${dname}_%d\", i);" 
                     	puts $fout  "\t    ${var}\[i\] = string(buf);\n\t\}"                         
                  }
            {CHAR\(\d+\)} { 
						set dsize [ set ${dname}(dsize) ]
                    	puts $fout "\tCHAR ${dname}\[$dsize\];"
                    	puts $fout "\tCHAR ${dname}attr\[[expr $dsize+200]\];\n"
                    	puts $fout "\tfor (int i=0; i < numElements; i++) \{"
						if	{ [ info exist parentTable($dname) ] } {
							set parent $parentTable($dname)
							puts $fout "\t\tstrcpy(${dname}, ${parent}\[ i % ${parent}_len \].[string toupper $dname]);"
						} else {
	                		puts $fout "\t\tsnprintf($dname, (size_t) $dsize, \"${dname}_%d\", i + thistime + 1 );"
						}
                    	puts $fout "\t\tsprintf(${dname}attr, \"${table}:%s\", $dname);"
						puts $fout "\t\tLdasArray< CHAR > ${dname}_vector ( $dname, strlen($dname)," 
            			puts $fout "\t\t\t\"${table}group:${table}:$dname\" );"
						puts $fout "\t\t${dname}_vector.setComment( ${dname}attr );"
                    	puts $fout "\t\t${dname}_cont.push_back( ${dname}_vector );"
                    	puts $fout "\t\}"
	              }
            {CHAR_BIT_DATA} { 
					if	{ [ info exist ${dname}(uniqueId) ] && ! [ info exist parentTable($dname) ] } {
						puts $fout "// $dname is uniqueId, generated by metadataAPI\n"
						return
					}
					if	{ [ info exist parentTable(${dname}) ] } {
						set parent $parentTable($dname)
					 } else {
					 	if	{ [ regexp {(.+)_id$} $dname -> parent ] } {
						} else {
					 		set parent $dname
						}
					 } 
					 handle_uniqueId 
	              }
			{BLOB}  { blobdata }	 
            default { puts $fout "\tfor (int i=0; i < numElements; i++) \{"
					  if   	{ [ info exist parentTable($dname) ] } {
					  		set parent $parentTable($dname)
							set mdname [ string toupper $dname ]
							regsub {^VERSION$} $mdname {VERSION_local} mdname
					  	   	puts $fout "\t\t${var}\[i\] = atol( ${parent}\[ i % ${parent}_len \].$mdname);"
					  } else {
                      		puts $fout "\t\t${var}\[i\] = \([set ${dname}(ilwdtype)]\) i + thistime + 1 ;"
					  }
                      puts $fout "\t\}"
                  }

        }
    }
}


proc out_storage {} {
    uplevel #0 {
		;## if not default field 
        	switch -regexp -- [ set ${dname}(dtype) ] {
			{CHAR_BIT_DATA} { if { ![ info exist ${dname}(uniqueId) ] || [ info exist parentTable($dname) ] } {
					puts $fout "\tLdasContainer ${dname}_cont ( \"${table}group:${table}:${dname}\" ); " } 
				} 
			{BLOB} -
            {[VAR]*CHAR\(\d+\)} { puts $fout "\tLdasContainer ${dname}_cont ( \"${table}group:${table}:${dname}\" ); " }            
            default { puts $fout "\t[set ${dname}(ilwdtype)]\t$dname\[ARRAYSIZE\];"}
        	}
    }
}

proc out_foreign {} {
	uplevel #0 {
		puts $fout "#ifdef TESTMAIN"
		puts "parent=$parents"
		foreach parent $parents {
			puts $fout "#include\t\"${parent}.h\""
		}
		puts $fout "#endif\n"
		puts $fout "#ifdef  TESTSYMREF"
		foreach parent $parents {
			puts $fout "#include\t\"${parent}_symref.h\""
		}
		puts $fout "#endif\n"
	}
}

proc add_parent {} {
	uplevel #0 {
		if	{ [ lsearch -exact $parents $parent ] == -1 } {	
			lappend parents $parent
		}
	}
}

set parents [ list ]
 set fd [ open [ lindex $argv 0 ] "r" ]
 puts "SQL file=[lindex $argv 0 ]"
 set sql [ read $fd ]
 set sql [ split $sql \n ]

 set namelist {}
 set uniqueId 0
 
array set ifo-site { ldas-dev C1 }
array set ifo-site { ldas-wa H1 }
array set ifo-site { ldas-la L1 }
array set ifo-site { ldas-test C1 }
set fd [ open /etc/ldasname r ]
set domain [ read $fd ]
regexp {^([^\n]+)} $domain -> domain
close $fd
puts "domain $domain."

set defaults [ list ]
 foreach line $sql {
    regsub -all {[ \t]+} $line " " line
    
    ;## skip comments in sql
    if  { [ regexp -nocase {^[-]+} $line ] } {
		if	{ [ regexp -nocase {unique\s+identifier} $line ] } {
			set uniqueId 1
		} else {
			set uniqueId 0
		}
        continue
    }

    if  { [ regexp -nocase {REAL|DOUBLE|INTEGER|SMALLINT|CHAR\((\d+)\)|TIMESTAMP|BLOB} $line match dsize ] } {
		if  { [ info exist table ] } {
           	set dname [lindex $line 0 ]
			if	{ [ regexp -nocase {WITH\s+DEFAULT} $line ] } {
				lappend defaults $dname
				continue
			}
           	set dtype $match
           	lappend namelist $dname
			if	{ [ regexp -nocase {CHAR\(13\)\s+FOR\s+BIT\s+DATA} $line ] } {
					set dtype CHAR_BIT_DATA
					if	{ $uniqueId } {
						array set ${dname} { uniqueId 1 }
						puts "$table $dname is uniqueId"
					}
					setparent 
			}
			if	{ [ regexp -nocase {VARCHAR} $line ] } {
					set dtype "VARCHAR\($dsize\)"
			}
			puts "dname=$dname, dtype=$match, size=$dsize"
            set_ilwd_type
			array set ${dname} [ list dtype $dtype ] 
        	if  { [ regexp -nocase {NOT NULL} $line match ] } {
            		array set ${dname} { null 0 } 
        	} else {
            		array set ${dname} { null 1 }
        	}
		}
        continue
    }
	
    if { [ regexp -nocase {CREATE TABLE[ \t]+([A-Z_])+} $line match table ] } {
        set table [ lindex $match 2 ]
        set table [ string tolower $table ]
		isTernary $table
        puts "creating ${table}ilwd.cc"
        continue
    }
    if  { [ regexp {PRIMARY\s+KEY\s+\((.+)\)} $line match primary ] } {
		regsub -all {\s+} $primary {} primary
		set primary [ split $primary , ]
		array set ${dname} [list primary $primary ]
		foreach field $primary {
			set field [ string trim $field ]
			if	{ [ string equal event_id $field ] } {
				if	{ $ternary } {
					if	{ ! [ info exist parentTable(event_id) ] } { 
						array set parentTable [ list $field events ]
						set parent events 
						add_parent 
						puts "$table has event_id as foreign key,parent $parentTable($field)"
					}
				} else {
					puts "$table has event_id as primary key"
				}
			} 
		}		
        continue
    }
    if  { [ regexp {FOREIGN\s+KEY\s+\((.+)} $line match foreign ] } {
		regsub -all {\s+} $foreign {} foreign
		set foreign [ split $foreign , ]
		foreach field $foreign {
			;## the ternary tables use the events.h file for now
			if	{ [ string equal event_id $field ] && $ternary } {
				if	{ ! [ info exist ::parentTable(event_id) ] } { 
					array set parentTable [ list [string trim $field] events ]
					set parent events 
					add_parent
					puts "$table has event_id as foreign key in parent $parentTable($field)"
					break
				}
			}
		}
    }
	if  { [ regexp {REFERENCES\s+([^\(]+)\((.+)\)} $line match parent foreign_fields ] } {
		regsub -all {\s+} $foreign_fields {} foreign_fields
		add_parent 
        puts "$table, parent $parent, foreign_fields $foreign_fields"
		set foreign_fields [ split $foreign_fields , ]
		foreach field $foreign_fields {
			array set parentTable [ list [string trim $field] $parent ]
		}
        continue
    } 
}

#catch { puts "$table foreign table $parents" } 
;## names that clash with global names in include files
set conflicts { version }

;## remove unique Id from primary key field since generated by metadataAPI
if	{ [ info exist primary ] } {
	foreach field $primary {
		if	{ ! [ info exist ${field}(uniqueId) ] } {
			lappend temp $field
		}
	}
	set primary $temp
} else {
	set primary [ list ]
}

# output stub
set output "${table}ilwd.cc" 
if  { [ file exists $output ] } {
    catch { file rename $output  "${output}.old" }
}
set fout [ open $output "w" ]
set heading 	"#include <string.h>\n"
append heading 	"#include <stdio.h>\n"
append heading 	"#include <ilwd/ldascontainer.hh>\n"
append heading	"#include <ilwd/ldasarray.hh>\n"
append heading 	"#include <ilwd/ldasstring.hh>\n" 
append heading 	"#include<ilwd/util.hh>\n" 
append heading 	"using ILwd::LdasContainer;\n"
append heading	"using ILwd::LdasArray;\n"
append heading	"using ILwd::LdasString;\n" 
append heading	"using namespace std;\n" 
append heading	"typedef double BLOB_TYPE;\n" 
append heading  "extern void get_gpstime( double *, double *);\n"
append heading	"LdasContainer ilwd3${table}( \"${table}group:$table:table\" );\n"

set ending 	"\n\tilwd1.push_back( ilwd3 );\n"
append ending 	"\tILwd::writeHeader(cout);\n"
append ending 	"\tilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );\n"
append ending 	"\tcout << endl;\n\}"


puts $fout $heading

puts $fout "namespace local \{\n"
puts $fout "\tconst INT_2U ARRAYSIZE = 10000;"
puts $fout "\tint thistime = (int)time (0) ;"
puts $fout "\tint numElements;"
puts $fout "\tdouble gps_secs ;"
puts $fout "\tdouble gps_nanosecs ;"
puts $fout "\tINT_2U BLOBSIZE = 64;"

foreach dname $namelist {
    if  { [ lsearch -exact $defaults $dname ] == -1 } {
        out_storage 
    }
}

puts $fout "\} // end namespace\n"

if	{ [ llength $parents ] } {
	out_foreign
} 
puts $fout "using namespace local;"

foreach dname $namelist {
    
    if  { [ lsearch $defaults $dname ] } {
    ;## the function stubs for user to fill in array data
        puts $fout "\nstatic void setup_${dname} () \{\n"
        puts $fout "// fill in $dname array"
        if  { [ lsearch $primary $dname ] > -1 } {
            puts $fout "// primary key"
        }
        if  { ! [ set ${dname}(null) ] } {
            puts $fout "// NOT NULL"
        }
        out_func  
        puts $fout "\}"
    } else {
        puts $fout "\n// $dname done by database"
    }
}

puts $fout "\nvoid setup_${table} ( int dims ) \n\{"
puts $fout "\tnumElements = dims ;"
puts $fout "\tif	( dims == 1000 ) \{"
puts $fout "\t\tBLOBSIZE = 5 ;\n\t\}"

foreach dname $namelist {
	if	{ [ lsearch $defaults $dname ] == -1 } {
    	puts $fout "\tsetup_${dname}();"
    	out_name $dname
	}
}

puts $fout "\}"

;## put the main line code
puts $fout "\n\#ifdef TESTMAIN \n\
void setup_${table}( int dims ); \n\n\
int main(int argc, char **argv) \{\n\
\tLdasContainer ilwd1( \"ligo:ldas:file\" );\n\
\tsetup_${table}( atoi ( argv \[1\] ) );\n\
\tilwd1.push_back( ilwd3${table} );\n\
\tILwd::writeHeader(cout);\n\
\tilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );\n\
\t//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );\n\
\tcout << endl;\n\}"

puts $fout "#endif"
close $fout
puts "created ${table}ilwd.cc"
