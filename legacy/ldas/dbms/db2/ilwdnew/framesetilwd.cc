#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3frameset( "framesetgroup:frameset:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "framesetgroup:frameset:process_id" ); 
	LdasContainer frameset_group_cont ( "framesetgroup:frameset:frameset_group" ); 
	INT_4S	chanlist_cdb[ARRAYSIZE];
	LdasContainer chanlist_id_cont ( "framesetgroup:frameset:chanlist_id" ); 
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	start_time_ns[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	INT_4S	end_time_ns[ARRAYSIZE];
	INT_4S	n_frames[ARRAYSIZE];
	INT_4S	missing_frames[ARRAYSIZE];
	INT_4S	n_bytes[ARRAYSIZE];
	LdasContainer name_cont ( "framesetgroup:frameset:name" ); 
} // end namespace

#ifdef TESTMAIN
#include	"frameset_writer.h"
#include	"frameset_chanlist.h"
#endif

#ifdef  TESTSYMREF
#include	"frameset_writer_symref.h"
#include	"frameset_chanlist_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, frameset_writer[ i % frameset_writer_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"framesetgroup:frameset:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, frameset_writer[ i % frameset_writer_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"framesetgroup:frameset:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_frameset_group () {

// fill in frameset_group array
// primary key
// NOT NULL
	CHAR frameset_group[48];
	CHAR frameset_groupattr[248];

	for (int i=0; i < numElements; i++) {
		strcpy(frameset_group, frameset_writer[ i % frameset_writer_len ].FRAMESET_GROUP);
		sprintf(frameset_groupattr, "frameset:%s", frameset_group);
		LdasArray< CHAR > frameset_group_vector ( frameset_group, strlen(frameset_group),
			"framesetgroup:frameset:frameset_group" );
		frameset_group_vector.setComment( frameset_groupattr );
		frameset_group_cont.push_back( frameset_group_vector );
	}
}

static void setup_chanlist_cdb () {

// fill in chanlist_cdb array
// NOT NULL
// NOT NULL
	for (int i=0; i < numElements; i++) {
		chanlist_cdb[i] = (INT_4S) 1 ;
	}
}

static void setup_chanlist_id () {

// fill in chanlist_id array
	char chanlist_id_str[30] ;
	CHAR_U chanlist_id[13];
	CHAR chanlist_idattr[200];

	int i,j;
// convert chanlist_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(chanlist_id_str, frameset_chanlist[ i % frameset_chanlist_len	].CHANLIST_ID);
		for ( int x = 0, j = 0; x < strlen(chanlist_id_str); x=x+2, j++ ) {
			chanlist_id[j] = ((chanlist_id_str[x]-'0' ) << 4) + (chanlist_id_str[x+1]-'0');
		}
		sprintf(chanlist_idattr, "chanlist_id:%s", chanlist_id_str);
		LdasArray< CHAR_U > chanlist_id_vector ( chanlist_id, 13,
		"framesetgroup:frameset:chanlist_id" );
		chanlist_id_vector.setComment(chanlist_idattr);
		chanlist_id_cont.push_back( chanlist_id_vector );
	}
}

static void setup_chanlist_id_symref () {
// fill in chanlist_id array
// NOT NULL
	CHAR chanlist_id[30] ;
	CHAR chanlist_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(chanlist_id, frameset_chanlist[ i % frameset_chanlist_len ].CHANLIST_ID );
		sprintf( chanlist_idattr, "chanlist_id:%s", chanlist_id );
		LdasArray< CHAR > chanlist_id_vector ( chanlist_id, strlen(chanlist_id),
		"framesetgroup:frameset:chanlist_id");
		chanlist_id_vector.setComment(chanlist_idattr);
		chanlist_id_cont.push_back( chanlist_id_vector );}
}

static void setup_start_time () {

// fill in start_time array
// primary key
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_start_time_ns () {

// fill in start_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		start_time_ns[i] = (INT_4S) ( i + gps_nanosecs );}
}

static void setup_end_time () {

// fill in end_time array
// primary key
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] = (INT_4S) ( i + gps_secs );
}
}

static void setup_end_time_ns () {

// fill in end_time_ns array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		end_time_ns[i] = (INT_4S) ( i + gps_nanosecs );
	}
}

static void setup_n_frames () {

// fill in n_frames array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		n_frames[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_missing_frames () {

// fill in missing_frames array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		missing_frames[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_n_bytes () {

// fill in n_bytes array
// NOT NULL
	for (int i=0; i < numElements; i++) {
		n_bytes[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_name () {

// fill in name array
// NOT NULL
	CHAR name[80];
	CHAR nameattr[280];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 80, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "frameset:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"framesetgroup:frameset:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

void setup_frameset ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3frameset.push_back( process_id_cont ) ;
	setup_frameset_group();
	ilwd3frameset.push_back(  frameset_group_cont ) ;
	setup_chanlist_cdb();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(chanlist_cdb, numElements,
	"framesetgroup:frameset:chanlist_cdb" ) );
	setup_chanlist_id();
	ilwd3frameset.push_back( chanlist_id_cont ) ;
	setup_start_time();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"framesetgroup:frameset:start_time" ) );
	setup_start_time_ns();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(start_time_ns, numElements,
	"framesetgroup:frameset:start_time_ns" ) );
	setup_end_time();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"framesetgroup:frameset:end_time" ) );
	setup_end_time_ns();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(end_time_ns, numElements,
	"framesetgroup:frameset:end_time_ns" ) );
	setup_n_frames();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(n_frames, numElements,
	"framesetgroup:frameset:n_frames" ) );
	setup_missing_frames();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(missing_frames, numElements,
	"framesetgroup:frameset:missing_frames" ) );
	setup_n_bytes();
	ilwd3frameset.push_back(  LdasArray< INT_4S >(n_bytes, numElements,
	"framesetgroup:frameset:n_bytes" ) );
	setup_name();
	ilwd3frameset.push_back(  name_cont ) ;
}

#ifdef TESTMAIN 
 void setup_frameset( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_frameset( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3frameset );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
