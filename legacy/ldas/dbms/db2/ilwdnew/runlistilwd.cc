#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3runlist( "runlistgroup:runlist:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer site_cont ( "runlistgroup:runlist:site" ); 
	INT_4S	run[ARRAYSIZE];
	INT_4S	start_time[ARRAYSIZE];
	INT_4S	end_time[ARRAYSIZE];
	LdasContainer run_type_cont ( "runlistgroup:runlist:run_type" ); 
	LdasContainer comment_cont ( "runlistgroup:runlist:comment" ); 
} // end namespace

using namespace local;

static void setup_site () {

// fill in site array
// primary key
// NOT NULL
	CHAR site[4];
	CHAR siteattr[204];

	for (int i=0; i < numElements; i++) {
		snprintf(site, (size_t) 4, "site_%d", i + thistime + 1 );
		sprintf(siteattr, "runlist:%s", site);
		LdasArray< CHAR > site_vector ( site, strlen(site),
			"runlistgroup:runlist:site" );
		site_vector.setComment( siteattr );
		site_cont.push_back( site_vector );
	}
}

static void setup_run () {

// fill in run array
// primary key
// NOT NULL
	for (int i=0; i < numElements; i++) {
		run[i] = (INT_4S) i + thistime + 1 ;
	}
}

static void setup_start_time () {

// fill in start_time array
// NOT NULL
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		start_time[i] = (INT_4S) ( i + gps_secs );}
}

static void setup_end_time () {

// fill in end_time array
	get_gpstime( &gps_secs, &gps_nanosecs );
	for (int i=0; i < numElements; i++) {
		end_time[i] = (INT_4S) ( i + gps_secs );
}
}

static void setup_run_type () {

// fill in run_type array
	CHAR run_type[64];
	CHAR run_typeattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(run_type, (size_t) 64, "run_type_%d", i + thistime + 1 );
		sprintf(run_typeattr, "runlist:%s", run_type);
		LdasArray< CHAR > run_type_vector ( run_type, strlen(run_type),
			"runlistgroup:runlist:run_type" );
		run_type_vector.setComment( run_typeattr );
		run_type_cont.push_back( run_type_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[1024];
	CHAR commentattr[1224];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 1024, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "runlist:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"runlistgroup:runlist:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

void setup_runlist ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_site();
	ilwd3runlist.push_back(  site_cont ) ;
	setup_run();
	ilwd3runlist.push_back(  LdasArray< INT_4S >(run, numElements,
	"runlistgroup:runlist:run" ) );
	setup_start_time();
	ilwd3runlist.push_back(  LdasArray< INT_4S >(start_time, numElements,
	"runlistgroup:runlist:start_time" ) );
	setup_end_time();
	ilwd3runlist.push_back(  LdasArray< INT_4S >(end_time, numElements,
	"runlistgroup:runlist:end_time" ) );
	setup_run_type();
	ilwd3runlist.push_back(  run_type_cont ) ;
	setup_comment();
	ilwd3runlist.push_back(  comment_cont ) ;
}

#ifdef TESTMAIN 
 void setup_runlist( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_runlist( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3runlist );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
