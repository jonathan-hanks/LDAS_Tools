#! /ldcg/bin/tclsh

 set fd [ open [ lindex $argv 0 ] "r" ]
 puts "resource file=[lindex $argv 0 ]"
 set output "[ lindex [ split [file tail [lindex $argv 0 ] ] . ] 0 ]"
 set outputfile "${output}Main.cc"
 puts "output in $outputfile"
 set rsc [ read $fd ]
 set rsc [ split $rsc \n ]
 
 set tables {}
 
 foreach line $rsc {
    regsub -all {[ \t]+} $line " " line
    
    ;## skip comments in rsc
    if  { [ regexp -nocase {^[-]+} $line ] } {
        continue
    }
    if  { [ regexp -nocase {([0-9]+) (coinc_sngl|multi_burst|sngl_ringdown|multi_inspiral|sngl_transdata|filter_params|process_params|sngl_unmodeled_v|filter|process|sngl_unmodeled|frameset_chanlist|segment_definer|summ_comment|frameset_loc|segment|summ_spectrum|frameset_writer|sngl_burst|summ_statistics|frameset|sngl_datasource|summ_value|gds_trigger|sngl_dperiodic|sngl_inspiral)} \
$line match numElements table ] } {
        puts "match=$match"
        puts "numElements=$numElements, table=$table"
        set ${table}(numElements) $numElements
        lappend tables $table
    }
}


set fout [ open $outputfile "w" ]
set heading1 "#include <string.h>\n\#include <stdio.h>\n\#include <ilwd/ldascontainer.hh>\n\#include <ilwd/ldasarray.hh>\n\#include <ilwd/ldasstring.hh>\n\#include<ilwd/util.hh>" 
set heading2 "using ILwd::LdasContainer;\n\using ILwd::LdasArray;\n\using ILwd::LdasString;\n"

puts $fout "$heading1\n\n$heading2\n"

foreach table $tables {
set heading8 "extern\tLdasContainer ilwd3${table};"
set heading9 "extern void setup_${table}( int dims );"

puts $fout "$heading8\n$heading9\n"
}

puts $fout "int main(int argc, char **argv) \{"

puts $fout "\tLdasContainer ilwd1( \"ligo:process:file\" );"

foreach table $tables {
    puts $fout "\tsetup_${table}( [ set ${table}(numElements) ] );"
    puts $fout "\tilwd1.push_back( ilwd3${table} );"
}

puts $fout	"\tILwd::writeHeader(cout);"
puts $fout 	"\tilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );"
puts $fout 	"\tcout << endl;\n\}"

close $fout

;#3 create make script
set fmake [ open "make.${output}Main" "w" ]
puts "makefile is make.${output}Main"
foreach table $tables {
    puts $fmake "g++ -c -g -DTESTSYMREF -I./ -I/ldas/include -I/ldcg/include  -o ${table}ilwd.o ${table}ilwd.cc"
}
puts $fmake "gcc  -g -c  -o commonilwd.o commonilwd.cc"
puts $fmake "g++ -c -g -I./ -I/ldas/include -I/ldcg/include  -o ${output}Main.o ${outputfile}"
set tablenames "[ join $tables "ilwd.o " ]ilwd.o"  
puts $fmake "g++ -o ${output}Main -L/ldas/lib -lilwd -lsocket $tablenames \
commonilwd.o ${output}Main.o" 

close $fmake
catch { exec chmod +x "make.${output}Main" }
