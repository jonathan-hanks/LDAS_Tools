#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3search_summvars( "search_summvarsgroup:search_summvars:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer process_id_cont ( "search_summvarsgroup:search_summvars:process_id" ); 
	LdasContainer name_cont ( "search_summvarsgroup:search_summvars:name" ); 
	LdasContainer string_cont ( "search_summvarsgroup:search_summvars:string" ); 
	REAL_8	value[ARRAYSIZE];
} // end namespace

#ifdef TESTMAIN
#include	"search_summary.h"
#endif

#ifdef  TESTSYMREF
#include	"search_summary_symref.h"
#endif

using namespace local;

static void setup_process_id () {

// fill in process_id array
// primary key
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, search_summary[ i % search_summary_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"search_summvarsgroup:search_summvars:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, search_summary[ i % search_summary_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"search_summvarsgroup:search_summvars:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_name () {

// fill in name array
// primary key
// NOT NULL
	CHAR name[64];
	CHAR nameattr[264];

	for (int i=0; i < numElements; i++) {
		snprintf(name, (size_t) 64, "name_%d", i + thistime + 1 );
		sprintf(nameattr, "search_summvars:%s", name);
		LdasArray< CHAR > name_vector ( name, strlen(name),
			"search_summvarsgroup:search_summvars:name" );
		name_vector.setComment( nameattr );
		name_cont.push_back( name_vector );
	}
}

static void setup_string () {

// fill in string array
	CHAR string[256];
	CHAR stringattr[456];

	for (int i=0; i < numElements; i++) {
		snprintf(string, (size_t) 256, "string_%d", i + thistime + 1 );
		sprintf(stringattr, "search_summvars:%s", string);
		LdasArray< CHAR > string_vector ( string, strlen(string),
			"search_summvarsgroup:search_summvars:string" );
		string_vector.setComment( stringattr );
		string_cont.push_back( string_vector );
	}
}

static void setup_value () {

// fill in value array
	for (int i=0; i < numElements; i++) {
		value[i] = (REAL_8) i + thistime + 1 ;
	}
}

void setup_search_summvars ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_process_id();
	ilwd3search_summvars.push_back( process_id_cont ) ;
	setup_name();
	ilwd3search_summvars.push_back(  name_cont ) ;
	setup_string();
	ilwd3search_summvars.push_back(  string_cont ) ;
	setup_value();
	ilwd3search_summvars.push_back(  LdasArray< REAL_8 >( value, numElements,
	"search_summvarsgroup:search_summvars:value" ) );
}

#ifdef TESTMAIN 
 void setup_search_summvars( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_search_summvars( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3search_summvars );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
