#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
using namespace std;
typedef double BLOB_TYPE;
extern void get_gpstime( double *, double *);
LdasContainer ilwd3frameset_writer( "frameset_writergroup:frameset_writer:table" );

namespace local {

	const INT_2U ARRAYSIZE = 10000;
	int thistime = (int)time (0) ;
	int numElements;
	double gps_secs ;
	double gps_nanosecs ;
	INT_2U BLOBSIZE = 64;
	LdasContainer program_cont ( "frameset_writergroup:frameset_writer:program" ); 
	LdasContainer process_id_cont ( "frameset_writergroup:frameset_writer:process_id" ); 
	LdasContainer frameset_group_cont ( "frameset_writergroup:frameset_writer:frameset_group" ); 
	LdasContainer data_source_cont ( "frameset_writergroup:frameset_writer:data_source" ); 
	LdasContainer ifos_cont ( "frameset_writergroup:frameset_writer:ifos" ); 
	LdasContainer comment_cont ( "frameset_writergroup:frameset_writer:comment" ); 
} // end namespace

#ifdef TESTMAIN
#include	"process.h"
#endif

#ifdef  TESTSYMREF
#include	"process_symref.h"
#endif

using namespace local;

static void setup_program () {

// fill in program array
// NOT NULL
	CHAR program[16];
	CHAR programattr[216];

	for (int i=0; i < numElements; i++) {
		snprintf(program, (size_t) 16, "program_%d", i + thistime + 1 );
		sprintf(programattr, "frameset_writer:%s", program);
		LdasArray< CHAR > program_vector ( program, strlen(program),
			"frameset_writergroup:frameset_writer:program" );
		program_vector.setComment( programattr );
		program_cont.push_back( program_vector );
	}
}

static void setup_process_id () {

// fill in process_id array
// primary key
// NOT NULL
	char process_id_str[30] ;
	CHAR_U process_id[13];
	CHAR process_idattr[200];

	int i,j;
// convert process_id string to 13 bit BCD
	for (int i=0; i < numElements; i++) {
		strcpy(process_id_str, process[ i % process_len	].PROCESS_ID);
		for ( int x = 0, j = 0; x < strlen(process_id_str); x=x+2, j++ ) {
			process_id[j] = ((process_id_str[x]-'0' ) << 4) + (process_id_str[x+1]-'0');
		}
		sprintf(process_idattr, "process_id:%s", process_id_str);
		LdasArray< CHAR_U > process_id_vector ( process_id, 13,
		"frameset_writergroup:frameset_writer:process_id" );
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );
	}
}

static void setup_process_id_symref () {
// fill in process_id array
// NOT NULL
	CHAR process_id[30] ;
	CHAR process_idattr[200];
	for (int i=0; i < numElements; i++) {
		strcpy(process_id, process[ i % process_len ].PROCESS_ID );
		sprintf( process_idattr, "process_id:%s", process_id );
		LdasArray< CHAR > process_id_vector ( process_id, strlen(process_id),
		"frameset_writergroup:frameset_writer:process_id");
		process_id_vector.setComment(process_idattr);
		process_id_cont.push_back( process_id_vector );}
}

static void setup_frameset_group () {

// fill in frameset_group array
// primary key
// NOT NULL
	CHAR frameset_group[48];
	CHAR frameset_groupattr[248];

	for (int i=0; i < numElements; i++) {
		snprintf(frameset_group, (size_t) 48, "frameset_group_%d", i + thistime + 1 );
		sprintf(frameset_groupattr, "frameset_writer:%s", frameset_group);
		LdasArray< CHAR > frameset_group_vector ( frameset_group, strlen(frameset_group),
			"frameset_writergroup:frameset_writer:frameset_group" );
		frameset_group_vector.setComment( frameset_groupattr );
		frameset_group_cont.push_back( frameset_group_vector );
	}
}

static void setup_data_source () {

// fill in data_source array
// NOT NULL
	CHAR data_source[240];
	CHAR data_sourceattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(data_source, (size_t) 240, "data_source_%d", i + thistime + 1 );
		sprintf(data_sourceattr, "frameset_writer:%s", data_source);
		LdasArray< CHAR > data_source_vector ( data_source, strlen(data_source),
			"frameset_writergroup:frameset_writer:data_source" );
		data_source_vector.setComment( data_sourceattr );
		data_source_cont.push_back( data_source_vector );
	}
}

static void setup_ifos () {

// fill in ifos array
	CHAR ifos[12];
	CHAR ifosattr[212];

	for (int i=0; i < numElements; i++) {
		snprintf(ifos, (size_t) 12, "ifos_%d", i + thistime + 1 );
		sprintf(ifosattr, "frameset_writer:%s", ifos);
		LdasArray< CHAR > ifos_vector ( ifos, strlen(ifos),
			"frameset_writergroup:frameset_writer:ifos" );
		ifos_vector.setComment( ifosattr );
		ifos_cont.push_back( ifos_vector );
	}
}

static void setup_comment () {

// fill in comment array
	CHAR comment[240];
	CHAR commentattr[440];

	for (int i=0; i < numElements; i++) {
		snprintf(comment, (size_t) 240, "comment_%d", i + thistime + 1 );
		sprintf(commentattr, "frameset_writer:%s", comment);
		LdasArray< CHAR > comment_vector ( comment, strlen(comment),
			"frameset_writergroup:frameset_writer:comment" );
		comment_vector.setComment( commentattr );
		comment_cont.push_back( comment_vector );
	}
}

void setup_frameset_writer ( int dims ) 
{
	numElements = dims ;
	if	( dims == 1000 ) {
		BLOBSIZE = 5 ;
	}
	setup_program();
	ilwd3frameset_writer.push_back(  program_cont ) ;
	setup_process_id();
	ilwd3frameset_writer.push_back( process_id_cont ) ;
	setup_frameset_group();
	ilwd3frameset_writer.push_back(  frameset_group_cont ) ;
	setup_data_source();
	ilwd3frameset_writer.push_back(  data_source_cont ) ;
	setup_ifos();
	ilwd3frameset_writer.push_back(  ifos_cont ) ;
	setup_comment();
	ilwd3frameset_writer.push_back(  comment_cont ) ;
}

#ifdef TESTMAIN 
 void setup_frameset_writer( int dims ); 

 int main(int argc, char **argv) {
 	LdasContainer ilwd1( "ligo:ldas:file" );
 	setup_frameset_writer( atoi ( argv [1] ) );
 	ilwd1.push_back( ilwd3frameset_writer );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	//ilwd1.write( 0, 4, cout, ILwd::BINARY, ILwd::NO_COMPRESSION );
 	cout << endl;
}
#endif
