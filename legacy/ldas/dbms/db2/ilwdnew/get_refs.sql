--
-- select X-referenced data from tables to generate
-- .h files for ilwds
echo table filter;
select filter_id from filter;
echo table frameset;
select name,frameset_group,start_time,end_time from frameset;
echo table frameset_chanlist;
select chanlist_id from frameset_chanlist;
echo table frameset_writer;
select frameset_group,process_id from frameset_writer;
echo table gds_trigger;
select event_id from gds_trigger;
echo table process;
select process_id from process;
echo table search_summary;
select process_id from search_summary;
echo table segment;
select segment_group,version,start_time,end_time from segment;
echo table sim_inst;
select simulation_id from sim_inst;
echo table sim_type;
select sim_type from sim_type;
echo table sngl_burst;
select event_id from sngl_burst;
echo table sngl_inspiral;
select event_id from sngl_inspiral;
echo table sngl_ringdown;
select event_id from sngl_ringdown;
echo table sngl_unmodeled;
select event_id from sngl_unmodeled;
echo table waveburst;
select event_id from waveburst;
