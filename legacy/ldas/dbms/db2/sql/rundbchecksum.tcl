#! /ldcg/bin/tclsh

set statefile [ file join $::TOPDIR cntlmonAPI cntlmon.state ]
if      { [ file exist $statefile ] } {
        source $statefile
} else {
        set ::LDASLOG /ldas_outgoing/logs
        set ::LDASARC /ldas_outgoing/logs/archive
        set ::RUNCODE [string toupper [exec cat /etc/ldasname]]
        set ::LDASTMP $TOPDIR/tmp
        set ::PUBDIR  $TOPDIR/jobs
        set ::LDAS /ldas
}  
source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc 
source /ldas/lib/genericAPI/b64.tcl

proc execdb2cmd {} {
	uplevel {
		set cmd "exec $::db2 $cmd"
		# puts $cmd
		catch { eval $cmd } data
		# puts $data
	}
}

source /ldas_outgoing/metadataAPI/LDASdsnames.ini
set site [ exec cat /etc/ldasname ]
;## generate sql foreach database


proc generateSQL { db } {

    set cmd "list tables"
    execdb2cmd
    set tables [ split $data \n ]
    set fd [ open $db.sql w  ]
 
    foreach line $tables {
        if  { [ regexp {^(\S+)\s+LDASDB} $line -> table ] } {
            puts $fd "echo --$table;"
            puts $fd "select count(*) from $table;"
            set cmd "describe table $table" 
            execdb2cmd
            set columns [ split $data \n ]
            set integer [ list ]
            set real [ list ]
            set double [ list ]
            foreach line $columns {
                switch -regexp -- $line {
                INTEGER { lappend integer $line }
                REAL    { lappend real $line }
                DOUBLE  { lappend double $line } 
                }
            }
            foreach entry [ list integer real double ] {
                foreach entry1 [ set $entry ] { 
                regexp {^(\S+)\s+} $entry1 -> colname
                puts $fd "echo --[ string toupper $entry ]:$table:${colname};"
                if  { ![ string equal double $entry ] } {
                    puts $fd "select sum(DOUBLE($colname)),\'sum_${table}_${colname}\' from ${table};"
                    puts $fd "select avg(DOUBLE($colname)),\'avg_${table}_${colname}\' from ${table};"
                } else {
                    puts $fd "select sum($colname),\'sum_${table}_${colname}\' from ${table};"
                    puts $fd "select avg($colname),\'avg_${table}_${colname}\' from ${table};"
                }                    
                puts $fd "select max($colname),\'max_${table}_${colname}\' from ${table};"
                puts $fd "select min($colname),\'min_${table}_${colname}\' from ${table};"
                }
            }
        }
    }
    close $fd

}

if  { $argc } {
    set dblist [ lrange $argv 0 end ]
} else {
    set dblist $::site_dbnames($site)
}
foreach db $dblist {
    set cmd "connect to $db user ldasdb using [ decode64 $::dblock ]"
    execdb2cmd
	#if	{ [ regexp {_tst|test} $db ] } {
	#	continue
	#}
    puts "db=$db"
	generateSQL $db
	set cmd "-tf $db.sql > $db.out"
	puts "exec sql file $db.sql into $db.out"
	execdb2cmd
}

