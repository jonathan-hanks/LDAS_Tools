-- 
-- reset counters to initial values
--
-- to run, first connect to db2 via command line connect 
-- e.g. db2 connect to cit_1 user db2sol7s using <password>
--
echo resetting tables;

-- simulation tables
echo table sim_type;
ALTER TABLE sim_type ALTER COLUMN sim_type RESTART WITH 0;
