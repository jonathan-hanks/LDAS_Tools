#! /ldcg/bin/tclsh

proc execdb2cmd { cmd } {
	uplevel {
		set start [ clock seconds ]
		set cmd "exec $::db2 $cmd"
        if  { ! [ regexp -nocase {connect} $cmd ] } {
		    puts $cmd
        }
		catch { eval $cmd } data
		set end [ clock seconds ]
		puts $data
		puts "took [ expr $end-$start ] seconds"
	}
}

set TOPDIR /ldas_outgoing
set LDAS /ldas
if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
puts "db2=$db2"
set dsname [ lindex $argv 0 ]
source /ldas/doc/db2/doc/text/dbtables.tcl
set auto_path "$::LDAS/lib $auto_path"
source $::LDAS/lib/genericAPI/b64.tcl

set backupdir [ file join $::dbdir backup ]
if  { ! [ file exist $backupdir ] } {
    puts "Please create backup directory $backupdir"
    exit
}
set cmd "backup database $dsname to $backupdir"
execdb2cmd $cmd

set cmd "connect to $dsname user ldasdb using [decode64 $::dblock]"
execdb2cmd $cmd

catch { exec ls -lt $backupdir } data
puts "backup $data"

set tables sngl_burst
foreach table $tables {
	set cmd "alter table $table add column stop_time INTEGER ;"
    execdb2cmd $cmd
    set cmd "alter table $table add column stop_time_ns INTEGER;"
    execdb2cmd $cmd
    set cmd "alter table $table add column flow REAL;"
    execdb2cmd $cmd
    set cmd "alter table $table add column fhigh REAL;"
    execdb2cmd $cmd
    set cmd "alter table $table add column tfvolume REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column hrss REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column time_lag REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column peak_time INTEGER;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column peal_time_ns INTEGER;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column peak_frequency REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column peak_strain        REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column peak_time_error    REAL;"
    execdb2cmd $cmd       
    set cmd "alter table $table add column peak_frequency_error REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column peak_strain_error  REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column ms_start_time      INTEGER;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column ms_start_time_ns   INTEGER;"
    execdb2cmd $cmd       
    set cmd "alter table $table add column ms_stop_time       INTEGER;"
    execdb2cmd $cmd       
    set cmd "alter table $table add column ms_stop_time_ns    INTEGER;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column ms_duration        REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column ms_flow            REAL;"
    execdb2cmd $cmd       
    set cmd "alter table $table add column ms_fhigh           REAL;"
    execdb2cmd $cmd   
    set cmd "alter table $table add column ms_bandwidth       REAL;"
    execdb2cmd $cmd 
    set cmd "alter table $table add column ms_hrss 	         REAL;"
    execdb2cmd $cmd  
    set cmd "alter table $table add column ms_snr             REAL;"
    execdb2cmd $cmd  
    set cmd "alter table $table add column ms_confidence      REAL;"
    execdb2cmd $cmd  
    set cmd "alter table $table add column param_one_name	 VARCHAR(64);"
    execdb2cmd $cmd  
    set cmd "alter table $table add column param_one_value	 DOUBLE;"
    execdb2cmd $cmd  
    set cmd "alter table $table add column param_two_name	 VARCHAR(64);"
    execdb2cmd $cmd      
    set cmd "alter table $table add column param_two_value	 DOUBLE;"
    execdb2cmd $cmd      
    set cmd "alter table $table add column param_three_name	 VARCHAR(64) ;"
    execdb2cmd $cmd  
    set cmd "alter table $table add column param_three_value	 DOUBLE;"
    execdb2cmd $cmd  
	set cmd "describe table $table"
	execdb2cmd $cmd
}
;## do another backup again
set cmd "backup database $dsname to $backupdir"
execdb2cmd $cmd
catch { exec ls -lt $backupdir } data
puts "backup $data"
