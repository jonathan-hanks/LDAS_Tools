#! /ldcg/bin/tclsh

proc execdb2cmd { cmd } {
	uplevel {
		set start [ clock seconds ]
		set cmd "exec $::db2 $cmd"
		puts $cmd
		catch { eval $cmd } data
		set end [ clock seconds ]
		puts $data
		puts "took [ expr $end-$start ] seconds"
	}
}

set TOPDIR /ldas_outgoing
set LDAS /ldas
if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
puts "db2=$db2"

# source /ldas/doc/db2/doc/text/dbtables.tcl
set auto_path "$::LDAS/lib $auto_path"
source $::LDAS/lib/genericAPI/b64.tcl

set dsname [ lindex $argv 0 ]
set cmd "connect to $dsname user ldasdb using [decode64 $::dblock]"
execdb2cmd $cmd

set tables { calib_info summ_spectrum }
foreach table $tables {
	set cmd "drop table $table"
	execdb2cmd $cmd 
	set cmd "-tf $::DB2SCRIPTS/$table.sql"
	execdb2cmd $cmd
    set cmd "select insert_time from $table"
    execdb2cmd $cmd
    set cmd "select insertion_time from $table"
    execdb2cmd $cmd
}

set cmd "-tf grant.sql"
execdb2cmd $cmd
