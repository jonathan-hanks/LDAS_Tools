#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern int numElements;
LdasContainer ilwd3fpfft( "gdsgroup:fpfft" );

namespace {
 	const INT_2U ARRAYSIZE = 10000;
    
	static string	filterparam_id[ARRAYSIZE];
	static INT_4S	window_time[ARRAYSIZE];
} // namespace 

static void setup_filterparam_id () {

// filterparam_id is a uniqueId field, can be filled in by LDAS.
}

static void setup_window_time () {

// fill in window_time array
	for (int i=0; i < numElements; i++) {
	    window_time[i] = i+1 ;
	}
}

void setup_fpfft () {

	//filterparam_id can be filled in by LDAS
	setup_window_time();
	ilwd3fpfft.push_back(  LdasArray< INT_4S >(window_time, numElements,
	"gdsgroup:fpfft:window_time" ) );


}
