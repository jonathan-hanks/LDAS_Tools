#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern int numElements;
LdasContainer ilwd3transformeddata( "gdsgroup:transformeddata" );

namespace {
 	const INT_2U ARRAYSIZE = 10000;

    LdasContainer   x_units_cont ( "gdsgroup:transformeddata:x_units" );
    LdasContainer   y_units_cont ( "gdsgroup:transformeddata:y_units" );
    
	static INT_4S	num_bins[ARRAYSIZE];
	static REAL_8	first_bin[ARRAYSIZE];
	static REAL_8	bin_width[ARRAYSIZE];
	static string	x_units[ARRAYSIZE];
	static INT_4S	data_type[ARRAYSIZE];
	static string	y_units[ARRAYSIZE];
	static CHAR_U	data[ARRAYSIZE];
	static string	transformeddata_id[ARRAYSIZE];
} // namespace 

static void setup_num_bins () {

// fill in num_bins array
	for (int i=0; i < numElements; i++) {
	    num_bins[i] = i+1 ;
	}
}

static void setup_first_bin () {

// fill in first_bin array
	for (int i=0; i < numElements; i++) {
	    first_bin[i] = i+1.1 ;
	}
}

static void setup_bin_width () {

// fill in bin_width array
	for (int i=0; i < numElements; i++) {
	    bin_width[i] = i+1.1 ;
	}
}

static void setup_x_units () {

// fill in x_units array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x_units");
	    x_units[i] = string(buf);
	}
}

static void setup_x_units_new () {

// fill in name array
	CHAR x_units[100];
    CHAR x_unitsattr[200];
    
    x_units_cont.setComment("transformeddata x units");
    for ( int i = 0; i < numElements ; i++ ) {
        strcpy(x_units, "x_units");
        sprintf(x_unitsattr, "x_units:%s", x_units);
        LdasArray< CHAR > x_units_vector ( x_units, strlen(x_units), x_unitsattr);
    // add to name container  
        x_units_cont.push_back( x_units_vector );
    }
}

static void setup_data_type () {

// fill in data_type array
	for (int i=0; i < numElements; i++) {
	    data_type[i] = i+1 ;
	}
}

static void setup_y_units () {

// fill in y_units array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "y_units");
	    y_units[i] = string(buf);
	}
}

static void setup_y_units_new () {

// fill in name array
	CHAR y_units[100];
    CHAR y_unitsattr[200];
    
    y_units_cont.setComment("transformeddata y units");
    for ( int i = 0; i < numElements ; i++ ) {
        strcpy(y_units, "y_units" );
        sprintf(y_unitsattr, "y_units:%s", x_units);
        LdasArray< CHAR > y_units_vector ( y_units, strlen(y_units), y_unitsattr);
    // add to name container  
        y_units_cont.push_back( y_units_vector );
    }
}

static void setup_data () {

// fill in data array
	for (int i=0; i < numElements; i++) {
	    data[i] = i+1 ;
	}
}

static void setup_transformeddata_id () {

// transformeddata_id is a uniqueId field, can be filled in by LDAS.
}

void setup_transformeddata () {

	setup_num_bins();
	ilwd3transformeddata.push_back(  LdasArray< INT_4S >(num_bins, numElements,
	"gdsgroup:transformeddata:num_bins" ) );

	setup_first_bin();
	ilwd3transformeddata.push_back(  LdasArray< REAL_8 >( first_bin, numElements,
	"gdsgroup:transformeddata:first_bin" ) );

	setup_bin_width();
	ilwd3transformeddata.push_back(  LdasArray< REAL_8 >( bin_width, numElements,
	"gdsgroup:transformeddata:bin_width" ) );

	setup_x_units_new();
	//ilwd3transformeddata.push_back(  LdasString(x_units, (size_t) numElements,
	//string("gdsgroup:transformeddata:x_units"), string("") ) );
    ilwd3transformeddata.push_back(  x_units_cont );
    
	setup_data_type();
	ilwd3transformeddata.push_back(  LdasArray< INT_4S >(data_type, numElements,
	"gdsgroup:transformeddata:data_type" ) );

	setup_y_units_new();
	//ilwd3transformeddata.push_back(  LdasString(y_units, (size_t) numElements,
	//string("gdsgroup:transformeddata:y_units"), string("") ) );
    ilwd3transformeddata.push_back(  y_units_cont );

	//setup_data();
	// ilwd3transformeddata.push_back(  LdasArray< CHAR_U >(data, numElements,
	//"gdsgroup:transformeddata:data" ) );

	//transformeddata_id can be filled in by LDAS

}
