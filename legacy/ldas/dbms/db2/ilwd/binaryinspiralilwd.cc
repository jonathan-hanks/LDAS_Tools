#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace binaryinspiral {
LdasContainer   ifo_site_cont ( "binaryinspiralgroup:binaryinspiral:ifo_site" );
LdasContainer	program_id_cont ( "binaryinspiralgroup:binaryinspiral:program_id" );

REAL_8	coalescence_time[ARRAYSIZE];
REAL_8	impulse_time[ARRAYSIZE];
REAL_8	fout_peak_time[ARRAYSIZE];
string	ifo_site[ARRAYSIZE];
INT_4S	filter_duration[ARRAYSIZE];
REAL_4	effective_distance[ARRAYSIZE];
REAL_4	sky_ra[ARRAYSIZE];
REAL_4	sky_dec[ARRAYSIZE];
REAL_4	angle_of_arrival[ARRAYSIZE];
REAL_4	d_angle_of_arrival[ARRAYSIZE];
REAL_4	mass1[ARRAYSIZE];
REAL_4	mass2[ARRAYSIZE];
REAL_4	signal_to_noise[ARRAYSIZE];
REAL_4	confidence[ARRAYSIZE];
REAL_8	coalescence_phase[ARRAYSIZE];
INT_2S	num_frames[ARRAYSIZE];
string	event_id[ARRAYSIZE];
string	ringdown_id[ARRAYSIZE];
string	program_id[ARRAYSIZE];
} // end namespace

int thistime = (int)time (0) ;

static void setup_coalescence_time () {

// fill in coalescence_time array
// unique primary key
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::coalescence_time[i] = thistime + i ;
	}
}

static void setup_impulse_time () {

// fill in impulse_time array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::impulse_time[i] = 5449.98 + i ;
	}
}

static void setup_fout_peak_time () {

// fill in fout_peak_time array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::fout_peak_time[i] = i + 7648.123 ;
	}
}

static void setup_ifo_site () {

// fill in ifo_site array
// primary key
	char buf[100];    
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "Caltech_40m");
	    binaryinspiral::ifo_site[i] = string(buf);
	}
}

static void setup_ifo_site_new () {

// fill in ifo_site array
// primary key
    CHAR ifo_site[100];
    CHAR ifo_siteattr[200];
    
    binaryinspiral::ifo_site_cont.setComment("ifo_site");
      
	for (int i=0; i < numElements; i++) {
	    strcpy(ifo_site, "Handford H1");
        sprintf(ifo_siteattr, "binaryinspiral:ifo_site:%s",ifo_site);
        LdasArray< CHAR > ifo_site_vector ( ifo_site, strlen(ifo_site),
            ifo_siteattr );
        binaryinspiral::ifo_site_cont.push_back( ifo_site_vector );
	}
}

static void setup_filter_duration () {

// fill in filter_duration array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::filter_duration[i] = i+1 ;
	}
}

static void setup_effective_distance () {

// fill in effective_distance array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::effective_distance[i] = i+4 ;
	}
}

static void setup_sky_ra () {

// fill in sky_ra array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::sky_ra[i] = i % 360 ;
	}
}

static void setup_sky_dec () {

// fill in sky_dec array
	for (int i=0; i < numElements; i++) {
        if ( i % 2 != 0 ) {
	        binaryinspiral::sky_dec[i] = i % 90 ;
        } else {
            binaryinspiral::sky_dec[i] = ( i % 90 ) * (-1) ;
        }
	}
}

static void setup_angle_of_arrival () {

// fill in angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::angle_of_arrival[i] = i % 360 ;
	}
}

static void setup_d_angle_of_arrival () {

// fill in d_angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::d_angle_of_arrival[i] = i % 360 ;
	}
}

static void setup_mass1 () {

// fill in mass1 array
// primary key
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::mass1[i] = thistime +i ;
	}
}

static void setup_mass2 () {

// fill in mass2 array
// primary key
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::mass2[i] = thistime + i + 5 ;
	}
}

static void setup_signal_to_noise () {

// fill in signal_to_noise array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::signal_to_noise[i] = i + 0.98 ;
	}
}

static void setup_confidence () {

// fill in confidence array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::confidence[i] = i+1 ;
	}
}

static void setup_coalescence_phase () {

// fill in coalescence_phase array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::coalescence_phase[i] = i ;
	}
}

static void setup_num_frames () {

// fill in num_frames array
	for (int i=0; i < numElements; i++) {
	    binaryinspiral::num_frames[i] = i+1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "event_id_%d", i);
	    binaryinspiral::event_id[i] = string(buf);
	}
}

static void setup_ringdown_id () {

// fill in ringdown_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "ringdown_id_%d", i);
	    binaryinspiral::ringdown_id[i] = string(buf);
	}
}

static void setup_program_id () {

// fill in program_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x'19990927201223201858000000'");
	    binaryinspiral::program_id[i] = string(buf);
	}
}

static void setup_program_id_new () {

// fill in program_id array
    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "binaryinspiral:program_id:%s", program_id_str);
        LdasArray< CHAR_U > program_id_vector ( program_id, 13,
            program_idattr );
        binaryinspiral::program_id_cont.push_back( program_id_vector );        
	}
}

main(int argc, char **argv) { 	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:binaryinspiral:file" );
	LdasContainer ilwd3( "binaryinspiralgroup:binaryinspiral" );
	setup_coalescence_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( binaryinspiral::coalescence_time, numElements,
 	"binaryinspiralgroup:binaryinspiral:coalescence_time" ) );

	setup_impulse_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( binaryinspiral::impulse_time, numElements,
 	"binaryinspiralgroup:binaryinspiral:impulse_time" ) );

	setup_fout_peak_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( binaryinspiral::fout_peak_time, numElements,
 	"binaryinspiralgroup:binaryinspiral:fout_peak_time" ) );

	setup_ifo_site_new();
	//ilwd3.push_back(  LdasString(binaryinspiral::ifo_site, (size_t) numElements,
	//string("binaryinspiralgroup:binaryinspiral:ifo_site"), string("") ) );
    ilwd3.push_back( binaryinspiral::ifo_site_cont );

	setup_filter_duration();
	ilwd3.push_back(  LdasArray< INT_4S >(binaryinspiral::filter_duration, numElements,
 
	"binaryinspiralgroup:binaryinspiral:filter_duration" ) );

	setup_effective_distance();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::effective_distance, numElements,
 	"binaryinspiralgroup:binaryinspiral:effective_distance" ) );

	setup_sky_ra();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::sky_ra, numElements,
 	"binaryinspiralgroup:binaryinspiral:sky_ra" ) );

	setup_sky_dec();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::sky_dec, numElements,
 	"binaryinspiralgroup:binaryinspiral:sky_dec" ) );

	setup_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::angle_of_arrival, numElements,
 	"binaryinspiralgroup:binaryinspiral:angle_of_arrival" ) );

	setup_d_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::d_angle_of_arrival, numElements,
 	"binaryinspiralgroup:binaryinspiral:d_angle_of_arrival" ) );

	setup_mass1();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::mass1, numElements,
 	"binaryinspiralgroup:binaryinspiral:mass1" ) );

	setup_mass2();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::mass2, numElements,
 	"binaryinspiralgroup:binaryinspiral:mass2" ) );

	setup_signal_to_noise();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::signal_to_noise, numElements,
 	"binaryinspiralgroup:binaryinspiral:signal_to_noise" ) );

	setup_confidence();
	ilwd3.push_back(  LdasArray< REAL_4 >( binaryinspiral::confidence, numElements,
 	"binaryinspiralgroup:binaryinspiral:confidence" ) );

	setup_coalescence_phase();
	ilwd3.push_back(  LdasArray< REAL_8 >( binaryinspiral::coalescence_phase, numElements,
 	"binaryinspiralgroup:binaryinspiral:coalescence_phase" ) );

	setup_num_frames();
	ilwd3.push_back(  LdasArray< INT_2S >(binaryinspiral::num_frames, numElements, 
	"binaryinspiralgroup:binaryinspiral:num_frames" ) );

	//setup_event_id();
	//ilwd3.push_back(  LdasString(binaryinspiral::event_id, (size_t) numElements,
	//string("binaryinspiralgroup:binaryinspiral:event_id"), string("") ) );

	//setup_ringdown_id();
	//ilwd3.push_back(  LdasString(binaryinspiral::ringdown_id, (size_t) numElements,
	//string("binaryinspiralgroup:binaryinspiral:ringdown_id"), string("") ) );

	setup_program_id_new();
	//ilwd3.push_back(  LdasString(binaryinspiral::program_id, (size_t) numElements,
	//string("binaryinspiralgroup:binaryinspiral:program_id"), string("") ) );
    ilwd3.push_back(  binaryinspiral::program_id_cont );

 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
