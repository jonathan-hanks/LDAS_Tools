#! /ldcg/bin/tclsh


proc blobdata {} {

    uplevel #0 {
    puts $fout "\tdouble blob\[10\] = (double)time (0);"
    puts $fout "\tfor ( int i = 0; i < numElements; i++ )\n\
    \t\{";
    puts $fout "\tmemcpy($dname\[i\], blob, 80);"
    puts $fout "\t\}"
    }
}

proc out_name { name } {
    uplevel #0 {
        switch -- [set ${dname}(dtype)] {
    
        CHAR_U    -
        CHAR      { puts $fout "\tilwd3${table}.push_back(  ${dname}_cont ) ;" }    
        REAL_8    { puts $fout "\tilwd3${table}.push_back(  LdasArray< REAL_8 >( ${dname}, numElements,\n\
\t\"${table}group:$table:$dname\" ) );\n"                    }
        REAL_4    { puts $fout "\tilwd3${table}.push_back(  LdasArray< REAL_4 >( ${dname}, numElements,\n\
\t\"${table}group:$table:$dname\" ) );\n"                    }              
        INT_4S   { puts $fout "\tilwd3${table}.push_back(  LdasArray< INT_4S >(${dname}, numElements,\n\ 
\t\"${table}group:$table:$dname\" ) );\n"                    }
        INT_2S   { puts $fout "\tilwd3${table}.push_back(  LdasArray< INT_2S >(${dname}, numElements,\n\ 
\t\"${table}group:$table:$dname\" ) );\n"                    }
        CHAR_U  {  puts $fout "\tilwd3${table}.push_back(  LdasArray< CHAR_U >(${dname}, numElements,\n\ 
\t\"${table}group:$table:$dname\" ) );\n"  }                                        
        default { puts "type = [set ${dname}(dtype)] " }
        }
    }
}


proc set_ilwd_type  {} {

    uplevel #0 {
        switch -regexp -- $dtype {
        
        {BLOB}    {  array set ${dname} { dtype CHAR_U } } 
        {TIMESTAMP} -
        {CHAR\(13\)} {  array set ${dname} { dtype CHAR_U } } 
        {CHAR\([0-9]+\)} {  array set ${dname} { dtype CHAR } 
                    array set ${dname} [ list dsize $dsize ] } 
        {double|DOUBLE}  {  array set ${dname} { dtype REAL_8 } }
        {FLOAT}   - 
        {REAL}    {  array set ${dname} { dtype REAL_4 } } 
        {INTEGER} {  array set ${dname} { dtype INT_4S } }
        {SMALLINT} { array set ${dname} { dtype INT_2S } }
        default { puts "unknown type = $dtype" }
        
        }
    }
}

## avoid NOT NULL restrictions by having i not 0
proc out_func  {} {
    uplevel #0 {
        switch -exact -- [ set ${dname}(dtype) ] {
        
            string { puts $fout  "\tchar buf\[100\];"
                     puts $fout  "\tfor (int i=0; i < numElements; i++) \{"
                     puts $fout  "\t    sprintf(buf, \"${dname}_%d\", i);" 
                     puts $fout  "\t    ${dname}\[i\] = string(buf);\n\t\}"                         
                  }
            CHAR  { set dsize [ set ${dname}(dsize) ] 
                    puts "size=$dsize"
                    puts $fout "\tCHAR $dname\[$dsize\];"
                    puts $fout "\tCHAR ${dname}attr\[200\];\n"
                    puts $fout "\tfor (int i=0; i < numElements; i++) \{"
	                puts $fout "\t\tsprintf($dname, \"U_%d\", i + thistime );"
                    puts $fout "\t\tsprintf(${dname}attr, \"${table}:$dname:%s\", $dname);"
                    puts $fout "\t\tLdasArray< CHAR > ${dname}_vector ( $dname, strlen($dname), ${dname}attr);"
                    puts $fout "\t\t${dname}_cont.push_back( ${dname}_vector );"
                    puts $fout "\t\}"
	              }
            CHAR_U { puts $fout "\tchar ${dname}_str\[\] = \"19991203215155148381000000\";"
                     puts $fout "\tCHAR_U ${dname}\[13\];"
                     puts $fout "\tCHAR ${dname}attr\[200\];\n"    
                     puts $fout "\tint i,j;" 
                     puts $fout "// convert program string to 13 bit BCD"
                     puts $fout "\tfor ( i = 0, j = 0; i < strlen(${dname}_str); i=i+2, ++j ) \{"
                     puts $fout "\t\t${dname}\[j\] = ((${dname}_str\[i\]-'0' ) << 4) + (${dname}_str\[i+1\]-'0');"
                     puts $fout "\t\}" 
    
	                 puts $fout "\tfor (int i=0; i < numElements; i++) \{"
                     puts $fout "\t\tsprintf(${dname}attr, \"${table}:${dname}:%s\", ${dname}_str);"
                     puts $fout "\t\tLdasArray< CHAR_U > ${dname}_vector ( ${dname}, 13, ${dname}attr );"
                     puts $fout "\t\t${dname}_cont.push_back( ${dname}_vector );"  
                     puts $fout "\t\}"      
	              }
            default { puts $fout "\tfor (int i=0; i < numElements; i++) \{"
                      puts $fout "\t    ${dname}\[i\] = i + 1 ;"
                      puts $fout "\t\}"
                  }

        }
    }
}


proc out_storage {} {
    uplevel #0 {
    puts "dname=$dname,[ set ${dname}(dtype) ]"
        switch -- [ set ${dname}(dtype) ] {
            CHAR_U -
            CHAR { puts $fout "\tLdasContainer ${dname}_cont ( \"${table}group:${table}:${dname}\" ); " }            
            default { puts $fout "\t[set ${dname}(dtype)]\t$dname\[ARRAYSIZE\];"}
        }
    
    }
}


 set fd [ open [ lindex $argv 0 ] "r" ]
 puts "SQL file=[lindex $argv 0 ]"
 set sql [ read $fd ]
 set sql [ split $sql \n ]

 set namelist {}
 
 foreach line $sql {
    regsub -all {[ \t]+} $line " " line
    
    ;## skip comments in sql
    if  { [ regexp -nocase {^[-]+} $line ] } {
        continue
    }

    if  { [ regexp -nocase {REAL|DOUBLE|INTEGER|SMALLINT|CHAR\(13\)|CHAR\(([0-9]+)\)|TIMESTAMP|BLOB} $line match dsize ] } {
        if  { [ info exist table ] } {
            set dname [lindex $line 0 ]
            set dtype $match
            puts "dname=$dname, dtype=$match, size=$dsize"
            lappend namelist $dname
            set_ilwd_type 
        }
        if  { [ regexp -nocase {NOT NULL} $line match ] } {
            array set ${dname} { null 0 } 
        } else {
            array set ${dname} { null 1 }
        }
        continue
        puts "name=$dname, type=${dname}(dtype)"
    }
    if { [ regexp -nocase {CREATE TABLE[ \t]+([A-Z_])+} $line match table ] } {
        set table [ lindex $match 2 ]
        set table [ string tolower $table ]
        puts "creating ${table}ilwd.cc"
        continue
    }
    if  { [ regexp {PRIMARY KEY[ \t]+\(([a-z_, ]+)} $line match primary ] } {
        set primary [ join $primary ]
        continue
    }
    if  { [ regexp {FOREIGN KEY[ \t]+\(([a-z_, ]+)} $line match foreign ] } {
        regexp "$foreign" $primary match $primary
        puts "new primary=$primary"
        continue
    }    
}

set ${table}(primary) [ split $primary ,]
puts "[llength [ set ${table}(primary)] ]"
puts [ set ${table}(primary)]

# output stub
set output "${table}ilwd.cc" 
if  { [ file exists $output ] } {
    catch { file rename $output  "${output}.old" }
}
set fout [ open $output "w" ]
set heading1 "#include <string.h>\n\#include <stdio.h>\n\#include <ilwd/ldascontainer.hh>\n\#include <ilwd/ldasarray.hh>\n\#include <ilwd/ldasstring.hh>\n\#include<ilwd/util.hh>" 
set heading2 "using ILwd::LdasContainer;\n\using ILwd::LdasArray;\n\using ILwd::LdasString;\n"
set heading3 "extern void get_gpstime( double *, double *);"
set heading4 "extern int numElements;"
set heading5 "LdasContainer ilwd3${table}( \"${table}group:$table\" );"

set ending "\n\
\tilwd1.push_back( ilwd3 );\n\
\tILwd::writeHeader(cout);\n\
\tilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );\n\
\tcout << endl;\n\}"


puts $fout "$heading1\n\n$heading2\n\n$heading3\n$heading4\n$heading5\n"

puts $fout "namespace \{\n"
puts $fout "\tconst INT_2U ARRAYSIZE = 10000;"
puts $fout "\tint thistime = (int)time (0) ;"
puts $fout "\tdouble gps_secs ;"
puts $fout "\tdouble gps_nanosecs ;"

foreach dname $namelist {
    if  { [ string compare $dname "creator_db" ] } {
        out_storage 
    }
}
puts $fout "\} // end namespace"

foreach dname $namelist {
    
    if  { [ string compare $dname "creator_db" ] } {
    ;## the function stubs for user to fill in array data
        puts $fout "\nstatic void setup_${dname} () \{\n"
        puts $fout "// fill in $dname array"
        if  { [ lsearch [ set ${table}(primary) ] $dname ] > -1 } {
            puts $fout "// primary key"
        }
        if  { ! [ set ${dname}(null) ] } {
            puts $fout "// NOT NULL"
        }
        out_func  
        puts $fout "\}"
    } else {
        puts $fout "\n// creator_db done in database"
    }
}

puts $fout "\nvoid setup_${table} () \n\{"

puts $fout "\tLdasContainer ilwd1( \"ligo:$table:file\" );"
puts $fout "\tLdasContainer ilwd3( \"${table}group:$table\" );"

foreach dname $namelist {
    puts $fout "\tsetup_${dname}();"
    out_name $dname
}

puts $fout "\}"

close $fout 
puts "created ${table}ilwd.cc"
