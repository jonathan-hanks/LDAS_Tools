#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace burstevent {
LdasContainer	program_id_cont ( "binaryinspiralgroup:binaryinspiral:program_id" );

REAL_8	llo_4km_time[ARRAYSIZE];
REAL_8	lho_2km_time[ARRAYSIZE];
REAL_8	lho_4km_time[ARRAYSIZE];
INT_4S	num_frames[ARRAYSIZE];
REAL_4	distance[ARRAYSIZE];
REAL_4	sky_ra[ARRAYSIZE];
REAL_4	sky_dec[ARRAYSIZE];
REAL_4	angle_of_arrival[ARRAYSIZE];
REAL_4	d_angle_of_arrival[ARRAYSIZE];
REAL_4	signal_to_noise[ARRAYSIZE];
REAL_4	central_frequency[ARRAYSIZE];
REAL_4	bandwidth[ARRAYSIZE];
REAL_8	delta_time[ARRAYSIZE];
string	program_id[ARRAYSIZE];
string	event_id[ARRAYSIZE];
} // end namespace

int thistime = (int)time (0) ;

static void setup_llo_4km_time () {

// fill in llo_4km_time array
// unique primary key
	for (int i=0; i < numElements; i++) {
	    burstevent::llo_4km_time[i] = thistime + i + 4;
	}
}

static void setup_lho_2km_time () {

// fill in lho_2km_time array
// unique primary key
	for (int i=0; i < numElements; i++) {
	    burstevent::lho_2km_time[i] = thistime + i + 2 ;
	}
}

static void setup_lho_4km_time () {

// fill in lho_4km_time array
// unique primary key 
	for (int i=0; i < numElements; i++) {
	    burstevent::lho_4km_time[i] = thistime + i + 1;
	}
}

static void setup_num_frames () {

// fill in num_frames array
	for (int i=0; i < numElements; i++) {
	    burstevent::num_frames[i] = i ;
	}
}

static void setup_distance () {

// fill in distance array
	for (int i=0; i < numElements; i++) {
	    burstevent::distance[i] = 3+i ;
	}
}

static void setup_sky_ra () {

// fill in sky_ra array
	for (int i=0; i < numElements; i++) {
	    burstevent::sky_ra[i] = i % 360 ;
	}
}

static void setup_sky_dec () {

// fill in sky_dec array
	for (int i=0; i < numElements; i++) {
        if  ( i % 2 != 0 ) {
	        burstevent::sky_dec[i] = i % 90 ;
        } else {
            burstevent::sky_dec[i] = (i % 90)*(-1) ;
        }
	}
}

static void setup_angle_of_arrival () {

// fill in angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    burstevent::angle_of_arrival[i] = i % 360 ;
	}
}

static void setup_d_angle_of_arrival () {

// fill in d_angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    burstevent::d_angle_of_arrival[i] = i % 360 ;
	}
}

static void setup_signal_to_noise () {

// fill in signal_to_noise array
	for (int i=0; i < numElements; i++) {
	    burstevent::signal_to_noise[i] = 0.9 + i ;
	}
}

static void setup_central_frequency () {

// fill in central_frequency array
	for (int i=0; i < numElements; i++) {
	    burstevent::central_frequency[i] = i+1 ;
	}
}

static void setup_bandwidth () {

// fill in bandwidth array
	for (int i=0; i < numElements; i++) {
	    burstevent::bandwidth[i] = i+1 ;
	}
}

static void setup_delta_time () {

// fill in delta_time array
	for (int i=0; i < numElements; i++) {
	    burstevent::delta_time[i] = i+1 ;
	}
}

static void setup_program_id () {

// fill in program_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x\'19990927201223201858000000\'");
	    burstevent::program_id[i] = string(buf);
	}
}

static void setup_program_id_new () {

// fill in program_id array
    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "binaryinspiral:program_id:%s", program_id_str);
        LdasArray< CHAR_U > program_id_vector ( program_id, 13,
            program_idattr );
        burstevent::program_id_cont.push_back( program_id_vector );        
	}
}

static void setup_event_id () {

// fill in event_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "event_id_%d", i);
	    burstevent::event_id[i] = string(buf);
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:burstevent:file" );
	LdasContainer ilwd3( "bursteventgroup:burstevent" );
	setup_llo_4km_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( burstevent::llo_4km_time, numElements,
 	"bursteventgroup:burstevent:llo_4km_time" ) );

	setup_lho_2km_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( burstevent::lho_2km_time, numElements,
 	"bursteventgroup:burstevent:lho_2km_time" ) );

	setup_lho_4km_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( burstevent::lho_4km_time, numElements,
 	"bursteventgroup:burstevent:lho_4km_time" ) );

	setup_num_frames();
	ilwd3.push_back(  LdasArray< INT_4S >(burstevent::num_frames, numElements,
 
	"bursteventgroup:burstevent:num_frames" ) );

	setup_distance();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::distance, numElements,
 	"bursteventgroup:burstevent:distance" ) );

	setup_sky_ra();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::sky_ra, numElements,
 	"bursteventgroup:burstevent:sky_ra" ) );

	setup_sky_dec();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::sky_dec, numElements,
 	"bursteventgroup:burstevent:sky_dec" ) );

	setup_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::angle_of_arrival, numElements,
 	"bursteventgroup:burstevent:angle_of_arrival" ) );

	setup_d_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::d_angle_of_arrival, numElements,
 	"bursteventgroup:burstevent:d_angle_of_arrival" ) );

	setup_signal_to_noise();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::signal_to_noise, numElements,
 	"bursteventgroup:burstevent:signal_to_noise" ) );

	setup_central_frequency();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::central_frequency, numElements,
 	"bursteventgroup:burstevent:central_frequency" ) );

	setup_bandwidth();
	ilwd3.push_back(  LdasArray< REAL_4 >( burstevent::bandwidth, numElements,
 	"bursteventgroup:burstevent:bandwidth" ) );

	setup_delta_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( burstevent::delta_time, numElements,
 	"bursteventgroup:burstevent:delta_time" ) );

	setup_program_id_new();
	//ilwd3.push_back(  LdasString(burstevent::program_id, (size_t) numElements,
	//string("bursteventgroup:burstevent:program_id"), string("") ) );
    ilwd3.push_back(  burstevent::program_id_cont );

	//setup_event_id();
	//ilwd3.push_back(  LdasString(burstevent::event_id, (size_t) numElements,
	//string("bursteventgroup:burstevent:event_id"), string("") ) );


 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
