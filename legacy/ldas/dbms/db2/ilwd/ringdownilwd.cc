#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include <ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace ringdown {
LdasContainer	program_id_cont ( "ringdowngroup:ringdown:program_id" );
LdasContainer   ifo_site_cont ( "ringdowngroup:ringdown:ifo_site" );

REAL_8	start_time[ARRAYSIZE];
string	ifo_site[ARRAYSIZE];
REAL_8	delta_time[ARRAYSIZE];
INT_4S	num_frames[ARRAYSIZE];
REAL_4	distance[ARRAYSIZE];
REAL_4	sky_ra[ARRAYSIZE];
REAL_4	sky_dec[ARRAYSIZE];
REAL_4	angle_of_arrival[ARRAYSIZE];
REAL_4	d_angle_of_arrival[ARRAYSIZE];
REAL_4	q[ARRAYSIZE];
REAL_4	frequency[ARRAYSIZE];
REAL_4	mass[ARRAYSIZE];
REAL_4	signal_to_noise[ARRAYSIZE];
REAL_4	confidence[ARRAYSIZE];
string	program_id[ARRAYSIZE];
string	event_id[ARRAYSIZE];
string	binaryinspiral_id[ARRAYSIZE];
} // end namespace

int thistime = (int)time (0) ;

static void setup_start_time () {
    
// fill in start_time array 
// unique, primary key
	for (int i=0; i < numElements; i++) {
	    ringdown::start_time[i] = thistime + i ;
	}
}

static void setup_ifo_site () {

// fill in ifo_site array
// primary key 
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "Caltech_40m");
	    ringdown::ifo_site[i] = string(buf);
	}
}


static void setup_ifo_site_new () {

// fill in ifo_site array
// primary key
    CHAR ifo_site[100];
    CHAR ifo_siteattr[200];
    
    ringdown::ifo_site_cont.setComment("ifo_site");
      
	for (int i=0; i < numElements; i++) {
	    strcpy(ifo_site, "Handford H1");
        sprintf(ifo_siteattr, "ringdown:ifo_site:%s",ifo_site);
        LdasArray< CHAR > ifo_site_vector ( ifo_site, strlen(ifo_site),
            ifo_siteattr );
        ringdown::ifo_site_cont.push_back( ifo_site_vector );
	}
}

static void setup_delta_time () {

// fill in delta_time array
	for (int i=0; i < numElements; i++) {
	    ringdown::delta_time[i] = i+1 ;
	}
}

static void setup_num_frames () {

// fill in num_frames array
	for (int i=0; i < numElements; i++) {
	    ringdown::num_frames[i] = i+1 ;
	}
}

static void setup_distance () {

// fill in distance array
	for (int i=0; i < numElements; i++) {
	    ringdown::distance[i] = i+3 ;
	}
}

static void setup_sky_ra () {

// fill in sky_ra array between 0-360 
	for (int i=0; i < numElements; i++) {
	    ringdown::sky_ra[i] = i % 360  ;
	}
}

static void setup_sky_dec () {

// fill in sky_dec array betweem -90 and +90 
	for (int i=0; i < numElements; i++) {
        if  ( i % 2 != 0 ) {
	        ringdown::sky_dec[i] = i % 90 ;
        } else {
            ringdown::sky_dec[i] = (i % 90)*(-1) ;
        }
	}
}

static void setup_angle_of_arrival () {

// fill in angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    ringdown::angle_of_arrival[i] = i % 360 ;
	}
}

static void setup_d_angle_of_arrival () {

// fill in d_angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    ringdown::d_angle_of_arrival[i] = i % 360 ;
	}
}

static void setup_q () {

// fill in q array
	for (int i=0; i < numElements; i++) {
	    ringdown::q[i] = i+1 ;
	}
}

static void setup_frequency () {

// fill in frequency array
	for (int i=0; i < numElements; i++) {
	    ringdown::frequency[i] = i+1 ;
	}
}

static void setup_mass () {

// fill in mass array
	for (int i=0; i < numElements; i++) {
	    ringdown::mass[i] = i+100 ;
	}
}

static void setup_signal_to_noise () {

// fill in signal_to_noise array
	for (int i=0; i < numElements; i++) {
	    ringdown::signal_to_noise[i] = i+0.9 ;
	}
}

static void setup_confidence () {

// fill in confidence array
	for (int i=0; i < numElements; i++) {
	    ringdown::confidence[i] =  0.9 ;
	}
}

static void setup_program_id () {

// fill in program_id array
// foreign key

	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x\'19990927201223201858000000\'");
	    ringdown::program_id[i] = string(buf);
	}
}

static void setup_program_id_new () {

// fill in program_id array
    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "ringdown:program_id:%s", program_id_str);
        LdasArray< CHAR_U > program_id_vector ( program_id, 13,
            program_idattr );
        ringdown::program_id_cont.push_back( program_id_vector );        
	}
}


static void setup_event_id () {

// fill in event_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "event_id_%d", i);
	    ringdown::event_id[i] = string(buf);
	}
}

static void setup_binaryinspiral_id () {

// fill in binaryinspiral_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "binaryinspiral_id_%d", i);
	    ringdown::binaryinspiral_id[i] = string(buf);
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:ringdown:file" );
	LdasContainer ilwd3( "ringdowngroup:ringdown" );
	setup_start_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( ringdown::start_time, numElements,
 	"ringdowngroup:ringdown:start_time" ) );

	setup_ifo_site_new();
	//ilwd3.push_back(  LdasString(ringdown::ifo_site, (size_t) numElements,
	//string("ringdowngroup:ringdown:ifo_site"), string("") ) );
    ilwd3.push_back(  ringdown::ifo_site_cont );
    
	setup_delta_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( ringdown::delta_time, numElements,
 	"ringdowngroup:ringdown:delta_time" ) );

	setup_num_frames();
	ilwd3.push_back(  LdasArray< INT_4S >(ringdown::num_frames, numElements,
 
	"ringdowngroup:ringdown:num_frames" ) );

	setup_distance();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::distance, numElements,
 	"ringdowngroup:ringdown:distance" ) );

	setup_sky_ra();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::sky_ra, numElements,
 	"ringdowngroup:ringdown:sky_ra" ) );

	setup_sky_dec();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::sky_dec, numElements,
 	"ringdowngroup:ringdown:sky_dec" ) );

	setup_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::angle_of_arrival, numElements,
 	"ringdowngroup:ringdown:angle_of_arrival" ) );

	setup_d_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::d_angle_of_arrival, numElements,
 	"ringdowngroup:ringdown:d_angle_of_arrival" ) );

	setup_q();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::q, numElements,
 	"ringdowngroup:ringdown:q" ) );

	setup_frequency();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::frequency, numElements,
 	"ringdowngroup:ringdown:frequency" ) );

	setup_mass();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::mass, numElements,
 	"ringdowngroup:ringdown:mass" ) );

	setup_signal_to_noise();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::signal_to_noise, numElements,
 	"ringdowngroup:ringdown:signal_to_noise" ) );

	setup_confidence();
	ilwd3.push_back(  LdasArray< REAL_4 >( ringdown::confidence, numElements,
 	"ringdowngroup:ringdown:confidence" ) );

	setup_program_id_new();
	//ilwd3.push_back(  LdasString(ringdown::program_id, (size_t) numElements,
	//string("ringdowngroup:ringdown:program_id"), string("") ) );
    ilwd3.push_back(  ringdown::program_id_cont );
    
	//setup_event_id();
	//ilwd3.push_back(  LdasString(ringdown::event_id, (size_t) numElements,
	//string("ringdowngroup:ringdown:event_id"), string("") ) );

	//setup_binaryinspiral_id();
	//ilwd3.push_back(  LdasString(ringdown::binaryinspiral_id, (size_t) numElements,
	//string("ringdowngroup:ringdown:binaryinspiral_id"), string("") ) );


 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
