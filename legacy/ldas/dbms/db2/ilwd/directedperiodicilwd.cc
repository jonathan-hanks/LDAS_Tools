#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace directedperiodic {
LdasContainer	program_id_cont ( "directedperiodicgroup:directedperiodic:program_id" );
LdasContainer   target_name_cont ( "directedperiodicgroup:directedperiodic:target_name" );
LdasContainer   ifo_site_cont ( "directedperiodicgroup:directedperiodic:ifo_site" );

REAL_8	start_time[ARRAYSIZE];
string	ifo_site[ARRAYSIZE];
REAL_8	delta_time[ARRAYSIZE];
INT_4S	num_frames[ARRAYSIZE];
REAL_4	distance[ARRAYSIZE];
REAL_4	sky_ra[ARRAYSIZE];
REAL_4	sky_dec[ARRAYSIZE];
REAL_4	angle_of_arrival[ARRAYSIZE];
REAL_4	d_angle_of_arrival[ARRAYSIZE];
string	target_name[ARRAYSIZE];
REAL_4	signal_to_noise[ARRAYSIZE];
REAL_4	frequency[ARRAYSIZE];
string	event_id[ARRAYSIZE];
string	program_id[ARRAYSIZE];
} // end namespace

int thistime = (int)time (0) ;

static void setup_start_time () {

// fill in start_time array
// unique primary key

	for (int i=0; i < numElements; i++) {
	    directedperiodic::start_time[i] = thistime + i ;
	}
}

static void setup_ifo_site () {

// fill in ifo_site array
// unique primary key

	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "Hanford H1");
	    directedperiodic::ifo_site[i] = string(buf);
	}
}

static void setup_ifo_site_new () {

// fill in ifo_site array
// primary key
    CHAR ifo_site[100];
    CHAR ifo_siteattr[200];
    
    directedperiodic::ifo_site_cont.setComment("ifo_site");
      
	for (int i=0; i < numElements; i++) {
	    strcpy(ifo_site, "Handford H1");
        sprintf(ifo_siteattr, "directedperiodic:ifo_site:%s",ifo_site);
        LdasArray< CHAR > ifo_site_vector ( ifo_site, strlen(ifo_site),
            ifo_siteattr );
        directedperiodic::ifo_site_cont.push_back( ifo_site_vector );
	}
}

static void setup_delta_time () {

// fill in delta_time array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::delta_time[i] = i+1 ;
	}
}

static void setup_num_frames () {

// fill in num_frames array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::num_frames[i] = i+1 ;
	}
}

static void setup_distance () {

// fill in distance array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::distance[i] = 3+i;
	}
}

static void setup_sky_ra () {

// fill in sky_ra array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::sky_ra[i] = i % 360 ;
	}
}

static void setup_sky_dec () {

// fill in sky_dec array
	for (int i=0; i < numElements; i++) {
        if  ( i % 2 != 0 ) {
	        directedperiodic::sky_dec[i] = i % 90 ;
        } else {
            directedperiodic::sky_dec[i] = ( i % 90 ) * (-1) ;
        }
	}
}

static void setup_angle_of_arrival () {

// fill in angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::angle_of_arrival[i] = i % 360;
	}
}

static void setup_d_angle_of_arrival () {

// fill in d_angle_of_arrival array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::d_angle_of_arrival[i] = i % 360 ;
	}
}

static void setup_target_name () {

// fill in target_name array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "target_name_%d", i);
	    directedperiodic::target_name[i] = string(buf);
	}
}

static void setup_target_name_new () {

// fill in frame_id array
 	CHAR target_name[100];
    CHAR target_nameattr[200];
    
    directedperiodic::target_name_cont.setComment("target name"); 
    
	for (int i=0; i < numElements; i++) {
	    sprintf(target_name, "target_%d", thistime + i);
        sprintf(target_nameattr, "directedperiodic:target_name:%s", target_name);
        LdasArray< CHAR > target_name_vector ( target_name, strlen(target_name), 
            target_nameattr );
        directedperiodic::target_name_cont.push_back( target_name_vector );
	}
}

static void setup_signal_to_noise () {

// fill in signal_to_noise array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::signal_to_noise[i] = i+1 ;
	}
}

static void setup_frequency () {

// fill in frequency array
	for (int i=0; i < numElements; i++) {
	    directedperiodic::frequency[i] = i+1 ;
	}
}

static void setup_event_id () {

// fill in event_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "event_id_%d", i);
	    directedperiodic::event_id[i] = string(buf);
	}
}

static void setup_program_id () {

// fill in program_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    strcpy(buf, "x\'19990927201223201858000000\'");
	    directedperiodic::program_id[i] = string(buf);
	}
}

static void setup_program_id_new () {

// fill in program_id array
    char program_id_str[] = "19990927201223201858000000";
    CHAR_U program_id[13];
    CHAR program_idattr[200];
    
    int i,j;
 
    // convert program string to 13 bit BCD 
    for ( i = 0, j = 0; i < strlen(program_id_str); i=i+2, ++j ) {
        program_id[j] = ((program_id_str[i]-'0' ) << 4) + (program_id_str[i+1]-'0');
    } 
    
	for (int i=0; i < numElements; i++) {
        sprintf(program_idattr, "directedperiodic:program_id:%s", program_id_str);
        LdasArray< CHAR_U > program_id_vector ( program_id, 13,
            program_idattr );
        directedperiodic::program_id_cont.push_back( program_id_vector );        
	}
}

main(int argc, char **argv) {
	int var = 0;
  
	numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:directedperiodic:file" );
	LdasContainer ilwd3( "directedperiodicgroup:directedperiodic" );
	setup_start_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( directedperiodic::start_time, numElements,
 	"directedperiodicgroup:directedperiodic:start_time" ) );

	setup_ifo_site_new();
	//ilwd3.push_back(  LdasString(directedperiodic::ifo_site, (size_t) numElements,
	//string("directedperiodicgroup:directedperiodic:ifo_site"), string("") ) );
    ilwd3.push_back(  directedperiodic::ifo_site_cont );
    
	setup_delta_time();
	ilwd3.push_back(  LdasArray< REAL_8 >( directedperiodic::delta_time, numElements,
 	"directedperiodicgroup:directedperiodic:delta_time" ) );

	setup_num_frames();
	ilwd3.push_back(  LdasArray< INT_4S >(directedperiodic::num_frames, numElements, 
	"directedperiodicgroup:directedperiodic:num_frames" ) );

	setup_distance();
	ilwd3.push_back(  LdasArray< REAL_4 >( directedperiodic::distance, numElements,
 	"directedperiodicgroup:directedperiodic:distance" ) );

	setup_sky_ra();
	ilwd3.push_back(  LdasArray< REAL_4 >( directedperiodic::sky_ra, numElements,
 	"directedperiodicgroup:directedperiodic:sky_ra" ) );

	setup_sky_dec();
	ilwd3.push_back(  LdasArray< REAL_4 >( directedperiodic::sky_dec, numElements,
 	"directedperiodicgroup:directedperiodic:sky_dec" ) );

	setup_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( directedperiodic::angle_of_arrival, numElements,
 	"directedperiodicgroup:directedperiodic:angle_of_arrival" ) );

	setup_d_angle_of_arrival();
	ilwd3.push_back(  LdasArray< REAL_4 >( directedperiodic::d_angle_of_arrival, numElements,
 	"directedperiodicgroup:directedperiodic:d_angle_of_arrival" ) );

	setup_target_name_new();
	//ilwd3.push_back(  LdasString(directedperiodic::target_name, (size_t) numElements,
	//string("directedperiodicgroup:directedperiodic:target_name"), string("") ) );
    ilwd3.push_back(  directedperiodic::target_name_cont );

	setup_signal_to_noise();
	ilwd3.push_back(  LdasArray< REAL_4 >( directedperiodic::signal_to_noise, numElements,
 	"directedperiodicgroup:directedperiodic:signal_to_noise" ) );

	setup_frequency();
	ilwd3.push_back(  LdasArray< REAL_4 >( directedperiodic::frequency, numElements,
 	"directedperiodicgroup:directedperiodic:frequency" ) );

	//setup_event_id();
	//ilwd3.push_back(  LdasString(directedperiodic::event_id, (size_t) numElements,
	//string("directedperiodicgroup:directedperiodic:event_id"), string("") ) );

	setup_program_id_new();
	//ilwd3.push_back(  LdasString(directedperiodic::program_id, (size_t) numElements,
	//string("directedperiodicgroup:directedperiodic:program_id"), string("") ) );
    ilwd3.push_back(  directedperiodic::program_id_cont );

 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
