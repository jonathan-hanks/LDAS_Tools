#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern int numElements;
LdasContainer ilwd3filter( "gdsgroup:filter" );

namespace {
 	const INT_2U ARRAYSIZE = 10000;
    LdasContainer   type_cont ( "gdsgroup:filter:type" );
    LdasContainer   filterparam_id_cont ( "gdsgroup:filter:filterparam_id" );
    LdasContainer   filter_id_cont ( "gdsgroup:filter:filter_id" );    
    
	static string	type[ARRAYSIZE];
	static string	filterparam_id[ARRAYSIZE];
	static string	filter_id[ARRAYSIZE];
} // namespace 


static void setup_type () {

// fill in type array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type_%d", i);
	    type[i] = string(buf);
	}
}


static void setup_type_new () {

// fill in type array
    CHAR type[100];
    CHAR typeattr[200];
    
    type_cont.setComment("filter type");
	for (int i=0; i < numElements; i++) {
	    sprintf(type, "type_%d", i+1 );
        sprintf(typeattr, "filter:type:%s", type );
        LdasArray< CHAR > type_vector ( type, strlen(type),
            typeattr );
        type_cont.push_back( type_vector );
	}
}

static void setup_filterparam_id () {

// filterparam_id is a uniqueId field, can be filled in by LDAS.
}

static void setup_filter_id () {

// filter_id is a uniqueId field, can be filled in by LDAS.
}

void setup_filter () {

	setup_type_new();
	//ilwd3filter.push_back(  LdasString(type, (size_t) numElements,
	//string("gdsgroup:filter:type"), string("") ) );
    ilwd3filter.push_back( type_cont );

	//filterparam_id can be filled in by LDAS
	//filter_id can be filled in by LDAS

}
