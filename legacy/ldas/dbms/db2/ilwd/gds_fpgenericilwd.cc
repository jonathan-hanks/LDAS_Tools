#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;

extern int numElements;
LdasContainer ilwd3fpgeneric( "gdsgroup:fpgeneric" );

namespace {
 	const INT_2U ARRAYSIZE = 10000;

	static string	filterparam_id[ARRAYSIZE];
	static string	name1[ARRAYSIZE];
	static string	type1[ARRAYSIZE];
	static string	value1[ARRAYSIZE];
	static string	name2[ARRAYSIZE];
	static string	type2[ARRAYSIZE];
	static string	value2[ARRAYSIZE];
	static string	name3[ARRAYSIZE];
	static string	type3[ARRAYSIZE];
	static string	value3[ARRAYSIZE];
	static string	name4[ARRAYSIZE];
	static string	type4[ARRAYSIZE];
	static string	value4[ARRAYSIZE];
	static string	name5[ARRAYSIZE];
	static string	type5[ARRAYSIZE];
	static string	value5[ARRAYSIZE];
	static string	name6[ARRAYSIZE];
	static string	type6[ARRAYSIZE];
	static string	value6[ARRAYSIZE];
	static string	name7[ARRAYSIZE];
	static string	type7[ARRAYSIZE];
	static string	value7[ARRAYSIZE];
	static string	name8[ARRAYSIZE];
	static string	type8[ARRAYSIZE];
	static string	value8[ARRAYSIZE];
	static string	name9[ARRAYSIZE];
	static string	type9[ARRAYSIZE];
	static string	value9[ARRAYSIZE];
	static string	name10[ARRAYSIZE];
	static string	type10[ARRAYSIZE];
	static string	value10[ARRAYSIZE];
} // namespace 

static void setup_filterparam_id () {

// filterparam_id is a uniqueId field, can be filled in by LDAS.
}

static void setup_name1 () {

// fill in name1 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name1_%d", i);
	    name1[i] = string(buf);
	}
}

static void setup_type1 () {

// fill in type1 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type1_%d", i);
	    type1[i] = string(buf);
	}
}

static void setup_value1 () {

// fill in value1 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value1_%d", i);
	    value1[i] = string(buf);
	}
}

static void setup_name2 () {

// fill in name2 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name2_%d", i);
	    name2[i] = string(buf);
	}
}

static void setup_type2 () {

// fill in type2 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type2_%d", i);
	    type2[i] = string(buf);
	}
}

static void setup_value2 () {

// fill in value2 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value2_%d", i);
	    value2[i] = string(buf);
	}
}

static void setup_name3 () {

// fill in name3 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name3_%d", i);
	    name3[i] = string(buf);
	}
}

static void setup_type3 () {

// fill in type3 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type3_%d", i);
	    type3[i] = string(buf);
	}
}

static void setup_value3 () {

// fill in value3 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value3_%d", i);
	    value3[i] = string(buf);
	}
}

static void setup_name4 () {

// fill in name4 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name4_%d", i);
	    name4[i] = string(buf);
	}
}

static void setup_type4 () {

// fill in type4 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type4_%d", i);
	    type4[i] = string(buf);
	}
}

static void setup_value4 () {

// fill in value4 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value4_%d", i);
	    value4[i] = string(buf);
	}
}

static void setup_name5 () {

// fill in name5 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name5_%d", i);
	    name5[i] = string(buf);
	}
}

static void setup_type5 () {

// fill in type5 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type5_%d", i);
	    type5[i] = string(buf);
	}
}

static void setup_value5 () {

// fill in value5 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value5_%d", i);
	    value5[i] = string(buf);
	}
}

static void setup_name6 () {

// fill in name6 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name6_%d", i);
	    name6[i] = string(buf);
	}
}

static void setup_type6 () {

// fill in type6 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type6_%d", i);
	    type6[i] = string(buf);
	}
}

static void setup_value6 () {

// fill in value6 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value6_%d", i);
	    value6[i] = string(buf);
	}
}

static void setup_name7 () {

// fill in name7 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name7_%d", i);
	    name7[i] = string(buf);
	}
}

static void setup_type7 () {

// fill in type7 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type7_%d", i);
	    type7[i] = string(buf);
	}
}

static void setup_value7 () {

// fill in value7 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value7_%d", i);
	    value7[i] = string(buf);
	}
}

static void setup_name8 () {

// fill in name8 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name8_%d", i);
	    name8[i] = string(buf);
	}
}

static void setup_type8 () {

// fill in type8 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type8_%d", i);
	    type8[i] = string(buf);
	}
}

static void setup_value8 () {

// fill in value8 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value8_%d", i);
	    value8[i] = string(buf);
	}
}

static void setup_name9 () {

// fill in name9 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name9_%d", i);
	    name9[i] = string(buf);
	}
}

static void setup_type9 () {

// fill in type9 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type9_%d", i);
	    type9[i] = string(buf);
	}
}

static void setup_value9 () {

// fill in value9 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value9_%d", i);
	    value9[i] = string(buf);
	}
}

static void setup_name10 () {

// fill in name10 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name10_%d", i);
	    name10[i] = string(buf);
	}
}

static void setup_type10 () {

// fill in type10 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type10_%d", i);
	    type10[i] = string(buf);
	}
}

static void setup_value10 () {

// fill in value10 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value10_%d", i);
	    value10[i] = string(buf);
	}
}

void setup_fpgeneric () {

	//filterparam_id can be filled in by LDAS
	setup_name1();
	ilwd3fpgeneric.push_back(  LdasString(name1, (size_t) numElements,
	string("gdsgroup:fpgeneric:name1"), string("") ) );

	setup_type1();
	ilwd3fpgeneric.push_back(  LdasString(type1, (size_t) numElements,
	string("gdsgroup:fpgeneric:type1"), string("") ) );

	setup_value1();
	ilwd3fpgeneric.push_back(  LdasString(value1, (size_t) numElements,
	string("gdsgroup:fpgeneric:value1"), string("") ) );

	setup_name2();
	ilwd3fpgeneric.push_back(  LdasString(name2, (size_t) numElements,
	string("gdsgroup:fpgeneric:name2"), string("") ) );

	setup_type2();
	ilwd3fpgeneric.push_back(  LdasString(type2, (size_t) numElements,
	string("gdsgroup:fpgeneric:type2"), string("") ) );

	setup_value2();
	ilwd3fpgeneric.push_back(  LdasString(value2, (size_t) numElements,
	string("gdsgroup:fpgeneric:value2"), string("") ) );

	setup_name3();
	ilwd3fpgeneric.push_back(  LdasString(name3, (size_t) numElements,
	string("gdsgroup:fpgeneric:name3"), string("") ) );

	setup_type3();
	ilwd3fpgeneric.push_back(  LdasString(type3, (size_t) numElements,
	string("gdsgroup:fpgeneric:type3"), string("") ) );

	setup_value3();
	ilwd3fpgeneric.push_back(  LdasString(value3, (size_t) numElements,
	string("gdsgroup:fpgeneric:value3"), string("") ) );

	setup_name4();
	ilwd3fpgeneric.push_back(  LdasString(name4, (size_t) numElements,
	string("gdsgroup:fpgeneric:name4"), string("") ) );

	setup_type4();
	ilwd3fpgeneric.push_back(  LdasString(type4, (size_t) numElements,
	string("gdsgroup:fpgeneric:type4"), string("") ) );

	setup_value4();
	ilwd3fpgeneric.push_back(  LdasString(value4, (size_t) numElements,
	string("gdsgroup:fpgeneric:value4"), string("") ) );

	setup_name5();
	ilwd3fpgeneric.push_back(  LdasString(name5, (size_t) numElements,
	string("gdsgroup:fpgeneric:name5"), string("") ) );

	setup_type5();
	ilwd3fpgeneric.push_back(  LdasString(type5, (size_t) numElements,
	string("gdsgroup:fpgeneric:type5"), string("") ) );

	setup_value5();
	ilwd3fpgeneric.push_back(  LdasString(value5, (size_t) numElements,
	string("gdsgroup:fpgeneric:value5"), string("") ) );

	setup_name6();
	ilwd3fpgeneric.push_back(  LdasString(name6, (size_t) numElements,
	string("gdsgroup:fpgeneric:name6"), string("") ) );

	setup_type6();
	ilwd3fpgeneric.push_back(  LdasString(type6, (size_t) numElements,
	string("gdsgroup:fpgeneric:type6"), string("") ) );

	setup_value6();
	ilwd3fpgeneric.push_back(  LdasString(value6, (size_t) numElements,
	string("gdsgroup:fpgeneric:value6"), string("") ) );

	setup_name7();
	ilwd3fpgeneric.push_back(  LdasString(name7, (size_t) numElements,
	string("gdsgroup:fpgeneric:name7"), string("") ) );

	setup_type7();
	ilwd3fpgeneric.push_back(  LdasString(type7, (size_t) numElements,
	string("gdsgroup:fpgeneric:type7"), string("") ) );

	setup_value7();
	ilwd3fpgeneric.push_back(  LdasString(value7, (size_t) numElements,
	string("gdsgroup:fpgeneric:value7"), string("") ) );

	setup_name8();
	ilwd3fpgeneric.push_back(  LdasString(name8, (size_t) numElements,
	string("gdsgroup:fpgeneric:name8"), string("") ) );

	setup_type8();
	ilwd3fpgeneric.push_back(  LdasString(type8, (size_t) numElements,
	string("gdsgroup:fpgeneric:type8"), string("") ) );

	setup_value8();
	ilwd3fpgeneric.push_back(  LdasString(value8, (size_t) numElements,
	string("gdsgroup:fpgeneric:value8"), string("") ) );

	setup_name9();
	ilwd3fpgeneric.push_back(  LdasString(name9, (size_t) numElements,
	string("gdsgroup:fpgeneric:name9"), string("") ) );

	setup_type9();
	ilwd3fpgeneric.push_back(  LdasString(type9, (size_t) numElements,
	string("gdsgroup:fpgeneric:type9"), string("") ) );

	setup_value9();
	ilwd3fpgeneric.push_back(  LdasString(value9, (size_t) numElements,
	string("gdsgroup:fpgeneric:value9"), string("") ) );

	setup_name10();
	ilwd3fpgeneric.push_back(  LdasString(name10, (size_t) numElements,
	string("gdsgroup:fpgeneric:name10"), string("") ) );

	setup_type10();
	ilwd3fpgeneric.push_back(  LdasString(type10, (size_t) numElements,
	string("gdsgroup:fpgeneric:type10"), string("") ) );

	setup_value10();
	ilwd3fpgeneric.push_back(  LdasString(value10, (size_t) numElements,
	string("gdsgroup:fpgeneric:value10"), string("") ) );


}
