#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace ppgeneric {

string	programparam_id[ARRAYSIZE];
string	name1[ARRAYSIZE];
string	type1[ARRAYSIZE];
string	value1[ARRAYSIZE];
string	name2[ARRAYSIZE];
string	type2[ARRAYSIZE];
string	value2[ARRAYSIZE];
string	name3[ARRAYSIZE];
string	type3[ARRAYSIZE];
string	value3[ARRAYSIZE];
string	name4[ARRAYSIZE];
string	type4[ARRAYSIZE];
string	value4[ARRAYSIZE];
string	name5[ARRAYSIZE];
string	type5[ARRAYSIZE];
string	value5[ARRAYSIZE];
string	name6[ARRAYSIZE];
string	type6[ARRAYSIZE];
string	value6[ARRAYSIZE];
string	name7[ARRAYSIZE];
string	type7[ARRAYSIZE];
string	value7[ARRAYSIZE];
string	name8[ARRAYSIZE];
string	type8[ARRAYSIZE];
string	value8[ARRAYSIZE];
string	name9[ARRAYSIZE];
string	type9[ARRAYSIZE];
string	value9[ARRAYSIZE];
string	name10[ARRAYSIZE];
string	type10[ARRAYSIZE];
string	value10[ARRAYSIZE];
} // end namespace

static void setup_programparam_id () {

// fill in programparam_id array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "programparam_id_%d", i);
	    ppgeneric::programparam_id[i] = string(buf);
	}
}

static void setup_name1 () {

// fill in name1 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name1_%d", i);
	    ppgeneric::name1[i] = string(buf);
	}
}

static void setup_type1 () {

// fill in type1 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type1_%d", i);
	    ppgeneric::type1[i] = string(buf);
	}
}

static void setup_value1 () {

// fill in value1 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value1_%d", i);
	    ppgeneric::value1[i] = string(buf);
	}
}

static void setup_name2 () {

// fill in name2 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name2_%d", i);
	    ppgeneric::name2[i] = string(buf);
	}
}

static void setup_type2 () {

// fill in type2 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type2_%d", i);
	    ppgeneric::type2[i] = string(buf);
	}
}

static void setup_value2 () {

// fill in value2 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value2_%d", i);
	    ppgeneric::value2[i] = string(buf);
	}
}

static void setup_name3 () {

// fill in name3 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name3_%d", i);
	    ppgeneric::name3[i] = string(buf);
	}
}

static void setup_type3 () {

// fill in type3 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type3_%d", i);
	    ppgeneric::type3[i] = string(buf);
	}
}

static void setup_value3 () {

// fill in value3 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value3_%d", i);
	    ppgeneric::value3[i] = string(buf);
	}
}

static void setup_name4 () {

// fill in name4 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name4_%d", i);
	    ppgeneric::name4[i] = string(buf);
	}
}

static void setup_type4 () {

// fill in type4 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type4_%d", i);
	    ppgeneric::type4[i] = string(buf);
	}
}

static void setup_value4 () {

// fill in value4 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value4_%d", i);
	    ppgeneric::value4[i] = string(buf);
	}
}

static void setup_name5 () {

// fill in name5 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name5_%d", i);
	    ppgeneric::name5[i] = string(buf);
	}
}

static void setup_type5 () {

// fill in type5 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type5_%d", i);
	    ppgeneric::type5[i] = string(buf);
	}
}

static void setup_value5 () {

// fill in value5 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value5_%d", i);
	    ppgeneric::value5[i] = string(buf);
	}
}

static void setup_name6 () {

// fill in name6 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name6_%d", i);
	    ppgeneric::name6[i] = string(buf);
	}
}

static void setup_type6 () {

// fill in type6 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type6_%d", i);
	    ppgeneric::type6[i] = string(buf);
	}
}

static void setup_value6 () {

// fill in value6 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value6_%d", i);
	    ppgeneric::value6[i] = string(buf);
	}
}

static void setup_name7 () {

// fill in name7 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name7_%d", i);
	    ppgeneric::name7[i] = string(buf);
	}
}

static void setup_type7 () {

// fill in type7 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type7_%d", i);
	    ppgeneric::type7[i] = string(buf);
	}
}

static void setup_value7 () {

// fill in value7 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value7_%d", i);
	    ppgeneric::value7[i] = string(buf);
	}
}

static void setup_name8 () {

// fill in name8 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name8_%d", i);
	    ppgeneric::name8[i] = string(buf);
	}
}

static void setup_type8 () {

// fill in type8 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type8_%d", i);
	    ppgeneric::type8[i] = string(buf);
	}
}

static void setup_value8 () {

// fill in value8 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value8_%d", i);
	    ppgeneric::value8[i] = string(buf);
	}
}

static void setup_name9 () {

// fill in name9 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name9_%d", i);
	    ppgeneric::name9[i] = string(buf);
	}
}

static void setup_type9 () {

// fill in type9 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type9_%d", i);
	    ppgeneric::type9[i] = string(buf);
	}
}

static void setup_value9 () {

// fill in value9 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value9_%d", i);
	    ppgeneric::value9[i] = string(buf);
	}
}

static void setup_name10 () {

// fill in name10 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "name10_%d", i);
	    ppgeneric::name10[i] = string(buf);
	}
}

static void setup_type10 () {

// fill in type10 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "type10_%d", i);
	    ppgeneric::type10[i] = string(buf);
	}
}

static void setup_value10 () {

// fill in value10 array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "value10_%d", i);
	    ppgeneric::value10[i] = string(buf);
	}
}

main(int argc, char **argv) {
