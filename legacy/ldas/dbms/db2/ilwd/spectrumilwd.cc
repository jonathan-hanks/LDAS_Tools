#include <string.h>
#include <stdio.h>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include<ilwd/util.hh>

using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;


const INT_2U ARRAYSIZE = 10000;
int numElements;

namespace spectrum {

string	channel_id[ARRAYSIZE];
string	frameset_time[ARRAYSIZE];
INT_2S	spectrum_type[ARRAYSIZE];
LdasContainer blob_container ( "ligo:element_test:blob");
} // end namespace

int thistime = (int)time (0) ;

static void setup_channel_id () {

// fill in channel_id array
// primary key
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "C%d", i);
	    spectrum::channel_id[i] = string(buf);
	}
}

static void setup_frameset_time () {

// fill in frameset_time array
	char buf[100];
	for (int i=0; i < numElements; i++) {
	    sprintf(buf, "frameset_time_%d", i);
	    spectrum::frameset_time[i] = string(buf);
	}
}

static void setup_spectrum_type () {

// fill in spectrum_type array
	for (int i=0; i < numElements; i++) {
	    spectrum::spectrum_type[i] = i + 1 ;
	}
}

static void setup_spectrum () {

// fill in spectrum array
//  initialize the blob 
	double blob[10] ;
    int i ;
    for ( i = 0; i < 10 ; i++ ) {
        blob[i] = 42949672 + i ;
    }
    
    
    spectrum::blob_container.setComment("testing blob arrays");
    
    char name[100];
    char units[]="blob_units";
    
    for ( i = 0; i < numElements ; i++ ) {
        sprintf (name,  "spectrumgroup:spectrum:blob_%d", i);
        CHAR_U buffer[ i+1 ];
        size_t size( i+1 );
        memcpy(buffer, blob,  size);
        LdasArray< CHAR_U > char_u_vector ( buffer, i+1 ,
            name, units );
    // create variable size blobs 
        spectrum::blob_container.push_back( char_u_vector );
    }
    spectrum::blob_container.setWriteFormat(ILwd::BASE64);
    spectrum::blob_container.setWriteCompression( ILwd::GZIP0);
    //spectrum::blob_container.write( 0, 4, cout, ILwd::BASE64, ILwd::NO_COMPRESSION );
}

main(int argc, char **argv) { 
	
    numElements = atoi(argv[1]);
	LdasContainer ilwd1( "ligo:spectrum:file" );
	LdasContainer ilwd3( "spectrumgroup:spectrum" );
	setup_channel_id();
	ilwd3.push_back(  LdasString(spectrum::channel_id, (size_t) numElements,
	string("spectrumgroup:spectrum:channel_id"), string("") ) );

	setup_frameset_time();
	ilwd3.push_back(  LdasString(spectrum::frameset_time, (size_t) numElements,
	string("spectrumgroup:spectrum:frameset_time"), string("") ) );

	setup_spectrum_type();
	ilwd3.push_back(  LdasArray< INT_2S >(spectrum::spectrum_type, numElements, 
	"spectrumgroup:spectrum:spectrum_type" ) );

	setup_spectrum();
	ilwd3.push_back(  spectrum::blob_container );


 	ilwd1.push_back( ilwd3 );
 	ILwd::writeHeader(cout);
 	ilwd1.write( 0, 4, cout, ILwd::ASCII, ILwd::NO_COMPRESSION );
 	cout << endl;
}
