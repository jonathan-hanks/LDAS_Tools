#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sqlcli1.h>
#include "samputil.h" 

#define ID_LENGTH 28

extern SQLCHAR server[SQL_MAX_DSN_LENGTH + 1] ;
extern SQLCHAR uid[MAX_UID_LENGTH + 1] ;
extern SQLCHAR pwd[MAX_PWD_LENGTH + 1] ;


int main( int argc, char * argv[] ) 
{
  
  SQLHANDLE henv, hdbc, hstmt ;
  SQLRETURN rc ;

  SQLCHAR * stmt = ( SQLCHAR * ) "CALL uniqueids(  ?, ? )" ;
  SQLCHAR *uniqueid;

  SQLINTEGER n;
  SQLINTEGER  uniqueidsind  = 0;  
  SQLINTEGER sz;

  if(argc != 2)
    {
      fprintf(stderr, "Usage: %s number_of_ids\n");
      exit(1);
    }

  n = atoi(argv[1]);
  uniqueid = (SQLCHAR *) malloc(n*ID_LENGTH*2*sizeof(SQLCHAR)+1);
  
  strncpy( ( char * ) server, "cit_1", SQL_MAX_DSN_LENGTH ); 
  strncpy( ( char * ) uid, "db2sol7s", MAX_UID_LENGTH ) ; 
  strncpy( ( char * ) pwd, "db2ibm", MAX_PWD_LENGTH ) ; 

  rc = SQLAllocHandle( SQL_HANDLE_ENV, SQL_NULL_HANDLE, &henv ) ;
  if ( rc != SQL_SUCCESS ) return( terminate( henv, rc ) ) ;
  
  rc = DBconnect( henv, &hdbc ) ;
  if ( rc != SQL_SUCCESS ) return( terminate( henv, rc ) ) ;
  
  rc = SQLAllocHandle( SQL_HANDLE_STMT, hdbc, &hstmt ) ;
  CHECK_HANDLE( SQL_HANDLE_DBC, hdbc, rc ) ;
  
  uniqueidsind = SQL_NULL_DATA; 
  
  rc = SQLPrepare(hstmt, stmt, SQL_NTS);
  CHECK_HANDLE( SQL_HANDLE_STMT, hstmt, rc ) ;
  
  rc = SQLBindParameter(hstmt, 
			1, 
			SQL_PARAM_OUTPUT, 
			SQL_C_CHAR, SQL_LONGVARCHAR,
			n*ID_LENGTH, 
			0, 
			uniqueid, 
			ID_LENGTH*n*2, 
			&uniqueidsind);
  CHECK_HANDLE( SQL_HANDLE_STMT, hstmt, rc ) ;
  
  rc = SQLBindParameter(hstmt, 
			2, 
			SQL_PARAM_INPUT, 
			SQL_C_LONG, SQL_INTEGER,
			0, 
			0, 
			&n, 
			4, 
			NULL);
  CHECK_HANDLE( SQL_HANDLE_STMT, hstmt, rc ) ;

  SQLExecute(hstmt);
  if (rc != SQL_SUCCESS & rc != SQL_SUCCESS_WITH_INFO)
    CHECK_HANDLE( SQL_HANDLE_STMT, hstmt, rc ) ;
  
  if (uniqueidsind == SQL_NULL_DATA) 
    printf("Error !\n");
  else
    printf("%s", uniqueid );
  
  rc = SQLFreeHandle( SQL_HANDLE_STMT, hstmt ) ;
  CHECK_HANDLE( SQL_HANDLE_STMT, hstmt, rc ) ;
  
  rc = SQLEndTran( SQL_HANDLE_DBC, hdbc, SQL_COMMIT ) ;
  CHECK_HANDLE( SQL_HANDLE_DBC, hdbc, rc ) ;
  
  rc = SQLDisconnect( hdbc ) ;
  CHECK_HANDLE( SQL_HANDLE_DBC, hdbc, rc ) ;
  
  rc = SQLFreeHandle( SQL_HANDLE_DBC, hdbc ) ;
  CHECK_HANDLE( SQL_HANDLE_DBC, hdbc, rc ) ;
  
  rc = SQLFreeHandle( SQL_HANDLE_ENV,  henv ) ;
  if ( rc != SQL_SUCCESS ) 
    return( terminate( henv, rc ) ) ;
  
  return( SQL_SUCCESS ) ;
} 
