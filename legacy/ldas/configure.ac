dnl -*- m4 -*-

AC_REVISION($Revision: 1.3010 $)
AC_PREQ(2.59)

dnl ---------------------------------------------------------------------
dnl  Keep track of import version variables
dnl ---------------------------------------------------------------------

AC_INIT(ldas,1.19.39,http://www.ligo.caltech.edu,ldas)
AC_CONFIG_AUX_DIR([.])
AC_CONFIG_MACRO_DIR([build/ac-macros])
AC_CONFIG_SRCDIR(build-ldas)

AM_INIT_AUTOMAKE([1.11 foreign tar-ustar silent-rules])

LT_INIT

dnl ---------------------------------------------------------------------
dnl  save import values
dnl ---------------------------------------------------------------------

package_name="$PACKAGE_NAME"
package_version="$PACKAGE_VERSION"
package_version_cvs="`echo ${package_version} | sed -e 's/\./_/g'`"

LDAS_VERSION=$package_version
export LDAS_VERSION

export package_name
export package_version
export package_version_cvs

dnl ---------------------------------------------------------------------
dnl   Setup LDAS variables
dnl ---------------------------------------------------------------------

LDAS_PACKAGE_NAME=$package_name
LDAS_PACKAGE_VERSION=$package_version
LDAS_PACKAGE_VERSION_NUMERIC="`echo ${package_version} |  ${SED} -e 's/[~](alpha|beta).*$//'`"

export LDAS_PACKAGE_NAME
export LDAS_PACKAGE_VERSION
export LDAS_PACKAGE_VERSION_NUMERIC


AC_LDAS_INIT(top)

dnl =====================================================================
dnl common subroutines
dnl =====================================================================

get_tag( )
{
  cd $1
  if test -d CVS
  then
    #--------------------------------------------------------------------
    # Can do a cvs lookup
    #--------------------------------------------------------------------
    get_symbolic_name=0
    cvs log -rHEAD configure.in | ( while read line
    do
      if test "$get_symbolic_name" -eq 1
      then
        symbolic_name=`echo ${line} | sed -e s/:.*$//g`
        hard_version_name=`echo ${line} | sed -e 's/^.*:[ ]*//g'`
        get_symbolic_name=0
      fi
      case "$line" in
      "symbolic names:")
        get_symbolic_name=1
        ;;
      "revision "*)
        revision=`echo ${line} | sed -e 's/revision[ ]*//'`
        ;;
      esac
      if test -n "$hard_version_name" -a -n "$revision" -a "$hard_version_name" = "$revision"
      then
        echo $symbolic_name
	echo $symbolic_name > symbolic_tag
        hard_version_name=""
      fi
    done
    )
  else
    #--------------------------------------------------------------------
    # Cannot do a cvs lookup:
    #  Either this is a tarball distribution or a cvs exported version
    #--------------------------------------------------------------------
    if test -f ${srcdir}/symbolic_tag
    then
      cat ${srcdir}/symbolic_tag
    fi
  fi
}


package_tag="`get_tag ${srcdir}`"
dnl =====================================================================
dnl  Create the include directory
dnl =====================================================================

case ${srcdir} in
/*) ;;
*) ldas_builddir=../../ ;;
esac

build_date="`date`";

mkdir include 2>&1 > /dev/null
mkdir include/ldas 2>&1 > /dev/null

dnl =====================================================================
dnl  check for documentation directory
dnl =====================================================================

if test -d ${srcdir}/doc
then
rm -rf doc
mkdir doc 2>&1 > /dev/null
cat > doc/version.txt << __EOF__
${package_version}
`date`
__EOF__
case x${package_tag} in
x)
  ;;
*)
  echo ${package_tag} >> doc/version.txt
  ;;
esac
no_doc_dir=no
else
no_doc_dir=yes
fi

dnl =====================================================================
dnl  Determine if the path is absolute or relative
dnl =====================================================================

ldas_srcdir=$srcdir
case "$ldas_srcdir" in
/*) up1=""; up2="";;
*) up1="../"; up2="../../";;
esac

dnl =====================================================================
dnl  Check for key programs
dnl =====================================================================

AC_LDAS_PROG_CC
AC_LDAS_PROG_CXX
LDAS_PROG_BISON
LDAS_PROG_LEX
AC_LDAS_PROG_LIBTOOL
LDAS_PROG_PKGBUILDER
AC_CHECK_PROGS(EXPR,expr)

LDAS_PACKAGE_TCL

dnl =====================================================================
dnl Check for some documentation that may need to be installed
dnl =====================================================================

AM_CONDITIONAL(LDAS_HAVE_SRC_DIR_BUILD, test -d ${srcdir}/build)
AM_CONDITIONAL(LDAS_HAVE_SRC_DIR_DBMS, test -d ${srcdir}/dbms)
AM_CONDITIONAL(LDAS_HAVE_SRC_DIR_DOC, test -d ${srcdir}/doc)

AM_CONDITIONAL(LDAS_COND_HAVE_DB2_DIR, test -d ${srcdir}/dbms/db2)
AM_CONDITIONAL(LDAS_COND_HAVE_DOC_DIR, test -d ${srcdir}/doc)
AM_CONDITIONAL(LDAS_COND_HAVE_CNTLMONAPI_DIR, test -d ${srcdir}/api/cntlmonAPI)
LDAS_PROG_LATEX
AC_CHECK_PROG(LATEX2HTML,tth,perl \$(srcdir)/../utilities/post_tth.pl,)
AM_CONDITIONAL(HAVE_LATEX2HTML, test -n "$LATEX2HTML")
AC_CHECK_PROGS(PS2PDF,ps2pdf)
AM_CONDITIONAL(HAVE_PS2PDF,test -n "$PS2PDF")


dnl =====================================================================
dnl  Finish configuration
dnl =====================================================================

AC_CONFIG_SUBDIRS( lib api )

AC_LDAS_FINISH


dnl =====================================================================
dnl  Generate header file with configuration information.
dnl =====================================================================

ldas_version_numeric="`echo ${package_version} | sed -e 's/\./00000 + /' -e 's/\./000 + /'`"
ldas_version_numeric="`${EXPR} $ldas_version_numeric`"
cat > ${ldas_top}/include/ldas/ldasconfig.hh << __EOF__
#ifndef LDAS_CONFIG_HH
#define	LDAS_CONFIG_HH

#define LDAS_NAME "${package_name}"
#define LDAS_VERSION "${package_version}"
#define LDAS_VERSION_NUMBER ${ldas_version_numeric}
#define LDAS_CVS_TAG  "${package_name}-${package_version_cvs}"
#define LDAS_CVS_DATE "\$Date: 2010/02/25 22:48:37 $"

#define LDAS_BUILD_DATE "${build_date}"

#define LDAS_CONFIGURE_COMMAND_LINE_OPTIONS "${ac_configure_args}"

#define LDAS_CONFIGURE_CXX "${CXX}"
#define LDAS_CONFIGURE_CXXFLAGS "${CXXFLAGS}"

#define LDAS_SYMBOLIC_TAG "${package_tag}"

#endif	/* LDAS_CONFIG_HH */
__EOF__

dnl =====================================================================
dnl  Generate local output
dnl =====================================================================

AC_OUTPUT(
	Makefile
	ldas-tools.spec
	ldas-tools-user-env.csh
	ldas-tools-user-env.sh
	build/Makefile
	build/ac-macros/Makefile
	build/make-macros/Makefile
	build/scripts/Makefile
	build/data/Makefile
	build/swig/Makefile
	)

AS_IF([test -d ${srcdir}/doc],
       [AC_OUTPUT(
	dnl -------------------------------------------------------------
	dnl   Documentation
	dnl -------------------------------------------------------------
	doc/Makefile 
	doc/lib/Makefile
	doc/utilities/Makefile
	doc/developer/Makefile
	doc/install/Makefile
	doc/install_framecpp/Makefile
	doc/install_cmonClient/Makefile
	doc/datacond/Makefile
	doc/diskcache/Makefile
	doc/wrapper/Makefile
	doc/ldas-tools/Makefile
	doc/procedures/Makefile
	doc/procedures/standalone.html
	doc/testing/Makefile
	doc/problem_reports/Makefile
	doc/faq/Makefile
       )]
)

AS_IF([test -d ${srcdir}/dbms],
      [AC_OUTPUT(dnl
	dnl -------------------------------------------------------------
	dnl   dbms
	dnl -------------------------------------------------------------
	dbms/Makefile
	dbms/db2/Makefile
	dbms/db2/doc/Makefile
	dbms/db2/doc/html/Makefile
	dbms/db2/doc/pdf/Makefile
	dbms/db2/doc/text/Makefile
	dbms/db2/sql/Makefile
	)]
)
