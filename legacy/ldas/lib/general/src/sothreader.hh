#ifndef SOTHREADER_HH
#define SOTHREADER_HH

#include <cstdlib>

#include<dlfcn.h>
#include<pthread.h>
#include<stdio.h>
#include<string.h>
#include<vector>

#include <general/autoarray.hh>

//-----------------------------------------------------------------------------
/// \brief  SoThreader
///
/// Preload shared object library several times into different address space
/// to allow threads parallelism. This class is useful with libraries that has
/// static variables and thus are not re-entrant.
///
/// \todo put code with cloops into the compiled .cc file
///
/// \todo extend this class to handle shared object file replication
///       automatically. That code will need to manipulate LD_LIBRARY_PATH
///       to point to different directory, where it puts new copy of
///       the shared object library (or libraries as a shared object lib could
///       have dependencies on the other non-reentrant solibs).
///       Above extension could create problems on the systems where LDAS file
///       system is exported read only.
///
/// \verbatim
/// Usage example with the CLAPACK:
///
/// #include<sothreader.hh>
///
/// SoThreader lapack("libclapack%d.so", 4, false);
///
///  // Get a handle to the CLAPACK library instance
///  void *handle = lapack.getHandle();
///
///  //Find a pointer to the function to call
///  int (*dgesvx_) (char *fact, char *trans, integer *n, integer * nrhs, doublereal *a, integer *lda, doublereal *af, integer *ldaf, integer *ipiv, char *equed, doublereal *r__, doublereal *c__, doublereal *b, integer *ldb, doublereal *x, integer *ldx, doublereal * rcond, doublereal *ferr, doublereal *berr, doublereal *work, integer * iwork, integer *info)
///    = (int (*) (char *fact, char *trans, integer *n, integer * nrhs, doublereal *a, integer *lda, doublereal *af, integer *ldaf, integer *ipiv, char *equed, doublereal *r__, doublereal *c__, doublereal *b, integer *ldb, doublereal *x, integer *ldx, doublereal * rcond, doublereal *ferr, doublereal *berr, doublereal *work, integer * iwork, integer *info)) dlsym( handle, "dgesvx_" );
///
///  // Exit if symbol lookup failed
///  char *error;
///  if ((error = dlerror()) != NULL)  {
///    cerr << error << endl;
///    exit(1);
///  }
///
///  // Do the processing...
///  // CLAPACK library instance is held private to this thread.
///  for (int i = 0; i < 1000; i++) {
///    (*dgesvx_)(&fact, &trans, &n, &nrhs, a, &lda, af, &ldaf,
///	       ipiv, &equed, r__, c__, b, &ldb, x, &ldx, &rcond,
///	       ferr, berr, work, iwork, &info);
///    inf |= info;
///  }
///
///  // Release CLAPACK library handle back, which makes it
///  // available for the use in other threads.
///  lapack.giveHandle( handle );
/// \endverbatim
class SoThreader
{
public:

  /// \brief  Constructor.
  ///
  /// Preload shared object library N times. Prints error message and exits the
  /// program if shared object loading fails.
  ///
  /// \param prtstr Shared object file name template.
  ///+ This has to have a for of "libfoo%d.so", where "%d"
  ///+ will be replaced by the sprintf() function with the 
  ///+ decimal numbers in [0-N) range. These object files has to exist
  ///+ in the LD_LIBRARY_PATH for this to work.
  /// \param N Number of times to preload.
  /// \param loadNow  "false" means resolve undefined symbols as code from the 
  ///+ dynamic library is executed or "true" means resolve all undefined now.
  ///+ Calls exit(1) if this cannot be done.
  ///
  SoThreader(const char* prtstr, int N = 4, bool loadNow = true)
    : handles()
  {
    for (int i = 0; i < N; i++)
    {
      General::AutoArray< char > buf( new char[strlen(prtstr) + 16] );
      sprintf( buf.get(), prtstr, i);
      void *handle = dlopen( buf.get(), loadNow?RTLD_NOW:RTLD_LAZY );
      if (!handle) {
	fputs ( dlerror(), stderr );
	fputs( "\n", stderr );
	std::exit(1);
      }
      handles.push_back( handle );
    }
    pthread_mutex_init (&lock, NULL);
    pthread_cond_init (&notempty, NULL);
  }

  
  /// \brief  Destructor.
  ~SoThreader()
  {
    for (std::vector<void*>::iterator i = handles.begin();
	 i != handles.end();
	 i++) {
	dlclose(*i);
	*i = (void*)NULL;
    }
    handles.erase( handles.begin( ), handles.end( ) );
    pthread_cond_destroy (&notempty);
    pthread_mutex_destroy (&lock);
  }


  /// \brief  Get available shared object library handle handle or wait for it.
  /// \return void* - shared object library handle.
  void* getHandle()
  {
    pthread_mutex_lock (&lock);
    while ( handles.size() == 0)
      pthread_cond_wait ( &notempty, &lock );
    void* handle = handles.back();
    handles.pop_back();
    pthread_mutex_unlock (&lock);
    return handle;
  }


  /// \brief  Return handle back when done.
  /// \param handle shared object library handle.
  void giveHandle( void *handle )
  {
    pthread_mutex_lock (&lock);
    handles.push_back( handle );
    pthread_cond_broadcast (&notempty);
    pthread_mutex_unlock (&lock);
  }


private:
  /// \brief  Shared object handles storage.
  std::vector<void*> handles;

  /// \brief  Users will wait on this for the next available handle.
  pthread_cond_t notempty;

  /// \brief  Object instance lock.
  pthread_mutex_t lock;
};

#endif // SOTHREADER_HH
