#ifndef OSpaceIdHH
#define OSpaceIdHH

namespace Stream
{
    const int OS_START  = os_user_start_id;

    namespace FrameCPP
    {
        enum
        {
            BASE = OS_START,
            ADCDATA,
            DETECTOR,
            DIMENSION,
            FRAME,
            HISTORY,
            LOCATION,
            MSG,
            PROCDATA,
            RAWDATA,
            SERDATA,
            SIMDATA,
            STATDATA,
            SUMMARY,
            TIME,
            TRIGDATA,
            VECT,
            CONTAINERBASE,
            CONTAINER_VECT,
            CONTAINER_ADCDATA,
            CONTAINER_SERDATA,
            CONTAINER_SIMDATA,
            CONTAINER_PROCDATA,
            CONTAINER_STATDATA,
            CONTAINER_MSG,
            CONTAINER_HISTORY,
            CONTAINER_SUMMARY,
            CONTAINER_TRIGDATA,
            VECTOR_DIMENSION,
            FRAMECPP_END
        };
    }

    namespace GenericAPI
    {
        enum
        {
            LDASBINARY = Stream::FrameCPP::FRAMECPP_END,
            GENERICAPI_END
        };
    }

    const int OS_ILWD = GenericAPI::GENERICAPI_END;
}


#endif // OSpaceIdHH
