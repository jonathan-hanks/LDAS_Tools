#ifndef REFCOUNT_HH
#define REFCOUNT_HH

#include<pthread.h>


//-----------------------------------------------------------------------------
/// \brief  RefCount
///
/// Reference counting class suitable for implementing
/// delayed destruction call, which blocks waiting for no references.
///
class RefCount
{
public:

  /// \brief  Constructor.
  RefCount() : refnum(0)
  {
    pthread_mutex_init (&lock, NULL);
    pthread_cond_init (&noref, NULL);
  }
  
  /// \brief  Destructor.
  ~RefCount()
  {
    pthread_cond_destroy (&noref);
    pthread_mutex_destroy (&lock);
  }


  /// \brief  Increment reference count
  void refCountUp()
  {
    pthread_mutex_lock (&lock);
    refnum++;
    pthread_mutex_unlock (&lock);
  }

  /// \brief  Decrement reference count
  void refCountDown()
  {
    pthread_mutex_lock (&lock);
    refnum--;
    if ( refnum == 0 )
	pthread_cond_broadcast ( &noref );
    pthread_mutex_unlock (&lock);
  }

  /// \brief  Wait until reference counter goes to zero
  void waitForNoReferences()
  {
    pthread_mutex_lock (&lock);
    while ( refnum != 0 )
      pthread_cond_wait ( &noref, &lock );
    pthread_mutex_unlock (&lock);
  }

  /// \brief  Get reference count
  unsigned int getRefCount()
  {
	return refnum;
  }

private:
  /// \brief  Reference count
  int refnum;

  /// \brief  Broadcast when reference counter goes to zero
  pthread_cond_t noref;

  /// \brief  Object instance lock.
  pthread_mutex_t lock;
};

class RefCountGuard 
{
public:
  RefCountGuard( RefCount& pr ) :r(pr) { r.refCountUp(); }
  ~RefCountGuard() { r.refCountDown(); }
private:
  RefCount& r;
};

#endif // REFCOUNT_HH
