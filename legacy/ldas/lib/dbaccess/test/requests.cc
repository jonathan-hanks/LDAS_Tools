#ifdef HAVE_CONFIG_H
#include "config.h"
#endif	/* HAVE_CONFIG_H */


#include "general/unittest.h"
#include "general/types.hh"
#include "general/Memory.hh"

#include "ilwd/ldascontainer.hh"

#include "dbaccess/Table.hh"
#include "dbaccess/Transaction.hh"
#include "dbaccess/SummStatistics.hh"

static void request_generic(void);

General::UnitTest	Test;

using namespace std;   
   

int
main(int Argc, char** Argv)
{
  Test.Init(Argc, Argv);

  request_generic();

  Test.Exit();
}

static void request_generic(void)
try
{
  ILwd::LdasContainer*	c1;
  ILwd::LdasContainer*	c2;

  DB::Transaction	rg;
  DB::Table*		rg_process(DB::Table::CreateTable("process"));
  DB::Transaction	rg_ilwd;
  DB::Table*		rg_ilwd_process(DB::Table::CreateTable("process"));

  DB::Table*		ss(DB::Table::CreateTable("summ_statistics"));
  std::unique_ptr< DB::Table >
    ss_ilwd(DB::Table::CreateTable("summ_statistics"));
  DB::Table*		ss_ilwd_rg(DB::Table::CreateTable("summ_statistics"));

  const CHAR* program[] = { "p1", "p2" };
  const CHAR program_set[] = { "p1" "p2" };
  int program_length[] = {(int)(strlen(program[0])),
			  (int)(strlen(program[1]))};

  INT_4S start_sec[] = {1, 2};
  INT_4S start_nsec[] = {1, 2};
  INT_4S end_sec[] = {start_sec[0]+1, start_sec[1]+1};
  INT_4S end_nsec[] = {start_nsec[0]+1, start_nsec[1]+2};

  INT_4S samples[] = {100,200};
  
  const CHAR* channel[] = {"channel1", "channel2"};
  const CHAR channel_set[] = {"channel1" "channel2"};
  int channel_length[] = {(int)(strlen(channel[0])),
			     (int)(strlen(channel[1]))};
  REAL_8 stats[][2] = {{1.1, 1.2}, // Min
		       {2.1, 2.2}, // Max
		       {3.1, 3.2}, // Mean
		       {4.1, 4.2}, // RMS
		       {5.1, 5.2}, // Variance
		       {6.1, 6.2}, // Skewness
		       {7.1, 7.2}, // Kurtosis
  };

  unsigned int	rows(sizeof(start_sec)/sizeof(*start_sec));

  rg.AppendTable(ss, true);
  rg.AppendTable(rg_process, true);
  rg_ilwd.AppendTable(ss_ilwd_rg, true);
  rg_ilwd.AppendTable(rg_ilwd_process, true);

  rg_process->AppendColumnEntry("program", "request");
  rg_process->AppendColumnEntry("comment", "testing program");
  rg_process->AppendColumnEntry("version", "0.0");
  rg_process->AppendColumnEntry("cvs_repository", "cvs repository");
  rg_process->AppendColumnEntry("cvs_entry_time", 0);
  rg_process->AppendColumnEntry("start_time", 0);
  rg_process->AppendColumnEntry("unix_procid", -1);
  rg_process->AppendColumnEntry("node", "localhost");
  rg_process->AppendColumnEntry("username", "me");

  rg_ilwd_process->AppendColumnEntry("program", "request");
  rg_ilwd_process->AppendColumnEntry("comment", "testing program");
  rg_ilwd_process->AppendColumnEntry("version", "0.0");
  rg_ilwd_process->AppendColumnEntry("cvs_repository", "cvs repository");
  rg_ilwd_process->AppendColumnEntry("cvs_entry_time", 0);
  rg_ilwd_process->AppendColumnEntry("start_time", 0);
  rg_ilwd_process->AppendColumnEntry("unix_procid", -1);
  rg_ilwd_process->AppendColumnEntry("node", "localhost");
  rg_ilwd_process->AppendColumnEntry("username", "me");

  ss->AppendColumnEntries(DB::SummStatistics::PROGRAM, rows,
			  program_set, program_length);

  ss->AppendColumnEntries(DB::SummStatistics::START_TIME_SECONDS,
			  rows, start_sec);
  ss->AppendColumnEntries(DB::SummStatistics::START_TIME_NANOSECONDS,
			  rows, start_nsec);
  ss->AppendColumnEntries(DB::SummStatistics::END_TIME_SECONDS,
			  rows, end_sec);
  ss->AppendColumnEntries(DB::SummStatistics::END_TIME_NANOSECONDS,
			  rows, end_nsec);

  ss->AppendColumnEntries(DB::SummStatistics::SAMPLES, rows, samples);

  ss->AppendColumnEntries(DB::SummStatistics::CHANNEL,
			  rows, channel_set, channel_length);

  ss->AppendColumnEntries(DB::SummStatistics::MIN, rows, stats[0]);
  ss->AppendColumnEntries(DB::SummStatistics::MAX, rows, stats[1]);
  ss->AppendColumnEntries(DB::SummStatistics::MEAN, rows, stats[2]);
  ss->AppendColumnEntries(DB::SummStatistics::RMS, rows, stats[3]);
  ss->AppendColumnEntries(DB::SummStatistics::VARIANCE, rows, stats[4]);
  ss->AppendColumnEntries(DB::SummStatistics::SKEWNESS, rows, stats[5]);
  ss->AppendColumnEntries(DB::SummStatistics::KURTOSIS, rows, stats[6]);

  for (unsigned int x = 0; x < rows; x++)
  {
    ss->Associate(x, *rg_process, 0);
  }

  ss_ilwd->AppendColumnEntries(DB::SummStatistics::PROGRAM, rows,
			       program_set, program_length);

  ss_ilwd->AppendColumnEntries(DB::SummStatistics::START_TIME_SECONDS,
			       rows, start_sec);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::START_TIME_NANOSECONDS,
			       rows, start_nsec);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::END_TIME_SECONDS,
			       rows, end_sec);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::END_TIME_NANOSECONDS,
			       rows, end_nsec);

  ss_ilwd->AppendColumnEntries(DB::SummStatistics::SAMPLES, rows, samples);

  ss_ilwd->AppendColumnEntries(DB::SummStatistics::CHANNEL,
			       rows, channel_set, channel_length);

  ss_ilwd->AppendColumnEntries(DB::SummStatistics::MIN, rows, stats[0]);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::MAX, rows, stats[1]);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::MEAN, rows, stats[2]);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::RMS, rows, stats[3]);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::VARIANCE, rows, stats[4]);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::SKEWNESS, rows, stats[5]);
  ss_ilwd->AppendColumnEntries(DB::SummStatistics::KURTOSIS, rows, stats[6]);

  for (unsigned int x = 0; x < rows; x++)
  {
    ss_ilwd->Associate(x, *rg_ilwd_process, 0);
  }

  //---------------------------------------------------------------------
  // Generic request version
  //---------------------------------------------------------------------

  c1 = rg.GetILwd();	// Establish base

  //---------------------------------------------------------------------
  // Generic request version using an ILwd as the source of most info
  //---------------------------------------------------------------------

  c2 = ss_ilwd->GetILwd();

  ss_ilwd_rg->AppendILwd(*c2);
  delete c2;

  c2 = rg_ilwd.GetILwd();

  Test.Check(*c1 == *c2)
    << "Generic request same as generic ilwd request (line: "
    << __LINE__ << ")"
    << endl;

  delete c2;

  //---------------------------------------------------------------------
  // Release resources
  //---------------------------------------------------------------------

  delete c1;
}
catch(const std::exception& e)
{
  Test.Check(false) << "request_generic(): Caught exception: " << e.what()
		    << endl;
}
