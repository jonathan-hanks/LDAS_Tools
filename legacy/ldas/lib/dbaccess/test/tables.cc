#ifdef HAVE_CONFIG_H
#include "config.h"
#endif	/* HAVE_CONFIG_H */

#include "general/unittest.h"

#include "general/Memory.hh"

#include "dbaccess/Field.hh"
#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldBlob.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/FieldUniqueId.hh"
#include "dbaccess/Table.hh"
#include "dbaccess/Process.hh"
#include "dbaccess/ProcessParams.hh"
#include "dbaccess/SummStatistics.hh"

using namespace std;   
   
typedef struct{
  const char*		fd_name;
  DB::Field::field_type	fd_type;
} field_desc_type;


static void check_table(int LineNumber,
			const DB::Table& Table,
			const field_desc_type* Fields,
			unsigned int FieldCount);

static void generic_table( );

static void table_process(void);
static void table_process_params(void);
static void table_summ_statistics(void);

General::UnitTest	Test;

int
main(int Argc, char** Argv)
{
  Test.Init(Argc, Argv);

  table_process();
  table_process_params();
  table_summ_statistics();

  generic_table( );

  Test.Exit();
}

static void
check_table(int LineNumber, const DB::Table& Table,
	    const field_desc_type* Fields,
	    unsigned int FieldCount)
{
  std::map<std::string,int>	field_map;

  try {
#define	LEADER() LineNumber << " " << Table.GetName()
    Test.Check(Table.GetFieldCount())
      << LEADER() << ": Initialized (Line Number: " << LineNumber << ")"
      << endl;
    
    for (unsigned int x = 0; x < FieldCount; x++)
    {
      int field_index;
      
      try {
	field_index = Table.HasField(Fields[x].fd_name);
      }
      catch ( ... )
      {
	field_index = -1;
      }
      Test.Check(field_index >= 0)
	<< LEADER() << ": " << Fields[x].fd_name << ": Valid Field Name"
	<< endl;
      if (field_index >= 0)
      {
	Test.Check(Table.GetField(field_index).GetType() == Fields[x].fd_type)
	  << LEADER() << ": " << Fields[x].fd_name << ": Correct data type"
	  << endl;
      }
      if (field_map[Fields[x].fd_name] > 0)
      {
	Test.Check(false) << LEADER()
			  << ": Field appears multiple times in list: "
			  << Fields[x].fd_name
			  << endl;
      }
      field_map[Fields[x].fd_name]++;
    }
    Test.Check(Table.GetFieldCount() == FieldCount)
      << LEADER() << ": appropriate number of fields "
      << FieldCount << " (" << Table.GetFieldCount() << ")"
      << endl;
    if (Table.GetFieldCount() > FieldCount)
    {
      for (unsigned int x = 0; x < Table.GetFieldCount(); x++)
      {
	if (field_map[Table.GetField(x).GetName()] == 0)
	{
	  Test.Check(false)
	    << LEADER() << ": Unchecked field: "
	    << Table.GetField(x).GetName()
	    << endl;
	}
      }
    }
#undef LEADER
  }
  catch ( const std::exception& e )
  {
    Test.Check(false) << "Caught exception: " << e.what() << endl;
  }
  catch ( ... )
  {
    Test.Check(false) << "Caught unexpected exception" << endl;
  }
}

static void
generic_table( )
try
{
  std::string table_string =
"    <ilwd comment='SQL=SELECT * FROM PROCESS ORDER BY start_time FETCH FIRST 5 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_table:table' size='9'>"
"        <lstring dims='0' name='LSTRING' size='0'></lstring>"
"        <int_2s dims='0' name='INT_2S'></int_2s>"
"        <int_2u dims='0' name='INT_2U'></int_2u>"
"        <int_4s dims='0' name='INT_4S'></int_4s>"
"        <int_4u dims='0' name='INT_4U'></int_4u>"
"        <int_8s dims='0' name='INT_8S'></int_8s>"
"        <int_8u dims='0' name='INT_8U'></int_8u>"
"        <real_4 dims='0' name='REAL_4'></real_4>"
"        <real_8 dims='0' name='REAL_8'></real_8>"
"    </ilwd>"
;


  std::istringstream	is( table_string );
  ILwd::Reader		reader(is);
  std::unique_ptr< ILwd::LdasContainer >
    ilwd( dynamic_cast<ILwd::LdasContainer*>
    ( ILwd::LdasElement::createElement( reader ) ) );

  std::unique_ptr< DB::Table >
    table( DB::Table::MakeTableFromILwd( *ilwd, true ) );

  ilwd.reset( table->GetILwd( ) );

  Test.Message() << "Resulting ILwd";
  ilwd->write( 2, 2, Test.Message( false ), ILwd::ASCII );
  Test.Message( false ) << std::endl;

  Test.Check( true ) << "Generic Table" << std::endl;
}
catch( std::exception& e )
{
  Test.Check( false )
    << "generic table: Caught std::exception: " << e.what()
    << std::endl;
}
catch( ... )
{
  Test.Check( false )
    << "generic table: Caught unknown exception"
    << std::endl;
}

static void
table_process(void)
{
  std::unique_ptr< DB::Table >
    table(DB::Table::CreateTable(DB::Process::TABLE_NAME));
  field_desc_type	fields[] =
  {
#define x(a,b) {DB::Process::a, DB::Field::b}
    x(CREATOR_DB,		DB2_INTEGER),
    x(PROGRAM,			DB2_CHAR),
    x(PROGRAM_VERSION,		DB2_CHAR),
    x(REPOSITORY,		DB2_VARCHAR),
    x(REPOSITORY_ENTRY_TIME,	DB2_INTEGER),
    x(COMMENT,			DB2_VARCHAR),
    x(IS_ONLINE,		DB2_INTEGER),
    x(NODE,			DB2_VARCHAR),
    x(USERNAME,			DB2_CHAR),
    x(JOB_ID,			DB2_INTEGER),
    x(START_TIME,		DB2_INTEGER),
    x(END_TIME,			DB2_INTEGER),
    x(PROCESS_ID,		FIELD_UNIQUE_ID),
    x(PARAM_SET,		DB2_INTEGER),
    x(IFOS,			DB2_CHAR),
    x(OS_PROCESS_ID,		DB2_INTEGER),
    x(DOMAIN_NAME,		DB2_VARCHAR)
#undef x
  };
  
  check_table(__LINE__, *table, fields, sizeof(fields)/sizeof(*fields));
  
}

static void
table_process_params(void)
{
  std::unique_ptr< DB::Table >
    table(DB::Table::CreateTable(DB::ProcessParams::TABLE_NAME));
  field_desc_type		fields[] =
  {
#define x(a,b) {DB::ProcessParams::a, DB::Field::b}
    x(CREATOR_DB,	DB2_INTEGER),
    x(PROGRAM,		DB2_CHAR),
    x(PROCESS_ID,	FIELD_UNIQUE_ID),
    x(PARAM,		DB2_VARCHAR),
    x(TYPE,		DB2_VARCHAR),
    x(VALUE,		DB2_VARCHAR)
#undef x
  };
  
  check_table(__LINE__, *table, fields, sizeof(fields)/sizeof(*fields));
}

static void
table_summ_statistics(void)
{
  DB::Table*	table(DB::Table::CreateTable(DB::SummStatistics::TABLE_NAME));
  field_desc_type	fields[] =
  {
#define x(a,b) {DB::SummStatistics::a, DB::Field::b}
    x(CREATOR_DB,		DB2_INTEGER),
    x(PROGRAM,			DB2_CHAR),
    x(PROCESS_ID,		FIELD_UNIQUE_ID),
    x(FRAMESET_GROUP,		DB2_VARCHAR),
    x(SEGMENT_GROUP,		DB2_VARCHAR),
    x(SEGMENT_VERSION,		DB2_INTEGER),
    x(START_TIME_SECONDS,	DB2_INTEGER),
    x(START_TIME_NANOSECONDS,	DB2_INTEGER),
    x(END_TIME_SECONDS,		DB2_INTEGER),
    x(END_TIME_NANOSECONDS,	DB2_INTEGER),
    x(FRAMES_USED,		DB2_INTEGER),
    x(SAMPLES,			DB2_INTEGER),
    x(CHANNEL,			DB2_CHAR),
    x(MIN,			DB2_DOUBLE),
    x(MAX,			DB2_DOUBLE),
    x(MIN_DELTA,		DB2_DOUBLE),
    x(MAX_DELTA,		DB2_DOUBLE),
    x(MIN_DELTA_DELTA,		DB2_DOUBLE),
    x(MAX_DELTA_DELTA,		DB2_DOUBLE),
    x(MEAN,			DB2_DOUBLE),
    x(RMS,			DB2_DOUBLE),
    x(VARIANCE,			DB2_DOUBLE),
    x(SKEWNESS,			DB2_DOUBLE),
    x(KURTOSIS,			DB2_DOUBLE),
#undef x
  };
  
  check_table(__LINE__, *table, fields, sizeof(fields)/sizeof(*fields));
  delete table;
}
