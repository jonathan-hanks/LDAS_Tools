#ifndef DB_TABLE_HH
#define	DB_TABLE_HH

#include <algorithm>
#include <list>
#include <map>
#include <string>
#include <vector>

#include <stdexcept>
#include <string>
#include <utility>   

#include "general/mutexlock.hh"
#include "general/types.hh"
#include "general/Singleton.hh"

#include "dbaccess/Field.hh"

//!ignore_begin:
namespace ILwd {
  class LdasContainer;
}
//!ignore_end:

namespace DB {
  template < class Type > class FieldArray;
  class FieldBlob;
  class FieldString;
  class FieldUniqueId;
  class DB2SyntaxDriver;

  class Table
  {
  public:
    //: type for row offsets
    typedef unsigned int rowoffset_type;

    //: type for column offsets
    typedef unsigned int columnoffset_type;

    //: Exception
    //
    // This exception it thrown when an unknown table name is requested.
    //
    class BadTableName : public std::range_error
    {
    public:
      //: Constructor
      BadTableName(const std::string& TableName);
    };

    //: Exception
    //
    // Thrown when a foreign key cannot be found for the specified
    // ForeignTable.
    //
    class UnknownForeignKey : public std::runtime_error
    {
    public:
      //: Constructor
      //
      //!param: const std::string& Table - Name of source table
      //!param: const std::string& ForeignTable - Name of foreign table
      //
      UnknownForeignKey(const std::string& Table,
			const std::string& ForeignTable);
    };

    //: Information describing a field
    class FieldDesc {
    public:
      //: constructor
      //
      //!param: unsigned int Index - Field offset
      //!param: std::string Name - Name of field
      //!param: const Field* Type - Class used for storage allocation.
      //
      //!return: FieldDesc
      FieldDesc(unsigned int Index,
		std::string Name,
		const Field* Type,
		bool Required);

      //: Retrive the field's index
      unsigned int GetIndex(void) const;
 
     //: Retrieve the field's name
      std::string GetName(void) const;

      //: Retrieve the field's specific type
      const Field* GetType(void) const;

      //: Retrieve if the column is required
      bool IsRequired(void) const;

    private:
      //: Field index
      //
      // This value represents the column number of the field.
      unsigned int	m_index;

      //: Field name
      //
      // This value represents the name of the field
      std::string	m_name;

      //: Is field required
      //
      // This value is set to true if the field must be supplied for the
      // insertion to succeed, false otherwise. The rule for determining
      // if a field is required is based on several criteria. If the field
      // is NULL, then it is never considered required. If the field is
      // NOT NULL but has a default value, then the field is not considered
      // required since the database can supply a value. If the field is NOT
      // NULL and does not have a default value, but is a UNIQUE ID then if
      // the field is not part of a FOREIGN KEY it is not considered required
      // since the metadataAPI can supply an appropriate value.
      bool		m_required;

      //: Storage allocation method
      //
      const Field*	m_type;

    };

    //: Primary Key
    //  Description of the primary key for a table.
    class PrimaryKey
    {
    public:
      //: Constructor
      PrimaryKey(void);
      //: Constuctor
      PrimaryKey( const std::vector<std::string>& Fields );

      //: Returns true if Column is in the list of fields
      bool HasPrimaryField( const std::string& ColumnName ) const;

    protected:
      //: Name of fields that compose the key
      std::vector<std::string>	m_fields;
    };

    //: Foreign Key
    //  Description of the foreign key of a table.
    class ForeignKey : public PrimaryKey
    {
    public:
      //:Exception
      class ColumnMismatch : public std::logic_error
      {
      public:
	//: Constructor
	//
	//!param: std::string Table - Name of source table
	//!param: std::string Column - Name of source table column
	//!param: std::string ForeignTable - Name of foreign table
	//!param: std::string ForeignColumn - Name of column in foreign table
	ColumnMismatch(std::string Table,
		       std::string Column,
		       std::string ForeignTable,
		       std::string ForeignColumn);
      };

      //: Constructor
      ForeignKey(void);
      //: Constructor
      ForeignKey( const std::vector<std::string>& Fields,
		  std::string ForeignTable,
		  const std::vector<std::string>& ForeignFields );

      //: Get name of foreign table
      const std::string& GetForeignTable(void) const;

      //: Get the mapping pair
      void GetMapping(DB::Table::columnoffset_type Offset, std::pair<std::string, std::string>& Mapping) const;

      //: Get number of columns composing the key
      DB::Table::columnoffset_type Size(void) const;

    private:
      //: Names of foreign fields
      std::vector<std::string>	m_fields;

      //: Name of foreign table
      std::string		m_foreign_table;
    };

    //: Constructor
    //
    // Create a table with the given name
    //
    //!param: std::string Name - Name of the table as it appears in the
    //+                          database
    //
    //!return: New instance of a Table
    Table(const std::string& Name);

    //: Copy Constructor
    //
    //!return: New instance of a Table
    Table(const DB::Table& Table);

    //:Destructor
    virtual ~Table();

    //: Add a new field to the table
    //
    //!param: Field* Column - Storage allocation method
    //
    //!return: none
    void AddField(Field* Column);

    //: Add an ilwd to the table
    //
    //!param: const ILwd::LdasContainer& TableContainer - Element to be added
    //
    //!return: none
    void AppendILwd( const ILwd::LdasContainer& TableContainer,
		     bool GenericTable = false );

    //: Append N copies of an object
    //
    //!param: int ColumnId - Index of field where data is to be appended
    //!param: int N - Number of rows in data set
    //!param: cosnt Type* Data - Pointer to data
    //!param: int Size - number of bytes (only needed for BLOBs)
    //
    //!return: none
    //
    template < class Type >
    void AppendNCopies(int ColumnId, int N,
		       const Type* Data, int Size);

    //: Append N copies of an object
    //
    //!param: std::string ColumnName - name of the field where data is to
    //+                   be appended
    //!param: int N - Number of rows to generate
    //!param: cosnt Type* Data - Pointer to data
    //!param: int Size - number of bytes (only needed for BLOBs)
    //
    //!return: none
    //
    template < class Type >
    void AppendNCopies(std::string ColumnName, int N,
		       const Type* Data, int Size);

    //: Append a single element of data to the column
    //
    //!param: std::string ColumnName - name of the field where data is to
    //+                   be appended
    //!param: cosnt Type Data - Data to be appended
    //!param: int Length - Length of data. Only required when blob data
    //+       is being passed since there is no way to detect its length.
    //
    //!return: none
    //
    template < class Type >
    void AppendColumnEntry(std::string ColumnName, const Type Data,
			   const int Length = -1);

    //: Append a single element of data to the column
    //
    //!param: std::string ColumnName - name of the field where data is to
    //+                   be appended
    //!param: cosnt Type Data - Data to be appended
    //!param: int Length - Length of data. Only required when blob data
    //+       is being passed since there is no way to detect its length.
    //
    //!return: none
    //
    template < class Type >
    void AppendColumnEntry(std::string ColumnName, const Type* Data,
			   const int Length = -1);

    //: Append multiple elements of data to the column
    //
    //!param: int ColumnId - Index of field where data is to be appended
    //!param: int Rows - Number of rows in data set
    //!param: cosnt Type* Data - Pointer to data
    //!param: int* Lengths - Pointer to lengths. This is used when the
    //+            Data is either character or blob data.
    //
    //!return: none
    //
    template < class Type >
    void AppendColumnEntries(int ColumnId, int Rows,
			     const Type* Data,
			     const int* Lengths = (const int *)NULL);

    //: Append multiple elements of data to the column
    //
    //!param: std::string ColumnName - name of the field where data is to
    //+                   be appended
    //!param: int Rows - Number of rows in data set
    //!param: cosnt Type* Data - Pointer to data
    //!param: int* Lengths - Pointer to lengths. This is used when the
    //+            Data is either character or blob data.
    //
    //!return: none
    //
    template < class Type >
    void AppendColumnEntries(std::string ColumnName, int Rows,
			     const Type* Data,
			     const int* Lengths = (const int *)NULL);

    //: Append unique ids to the specified field
    //
    //!param: int ColumnId - Index of field where data is to be appended
    //!param: int Rows - Number of rows in data set
    //!param: std::string UniqueIdKey - Unique Id Identifier
    //
    //!return: none
    void AppendUniqueIds(unsigned int ColumnId, int Rows,
			 std::string UniqueIdKey);

    //: Append unique ids to the specified field
    //
    //!param: std::string ColumnName - name of the field where data is to
    //+                   be appended
    //!param: int Rows - Number of rows in data set
    //!param: std::string UniqueIdKey - Unique Id Identifier
    //
    //!return: none
    void AppendUniqueIds(std::string ColumnName, int Rows,
			 std::string UniqueIdKey);

    //: Associate data in this table with another table
    //
    // This makes use of the foreign keys to associate data in
    // the current table with data in another table.
    //
    void Associate(const DB::Table::rowoffset_type Row,
		   const DB::Table& ForeignTable,
		   const DB::Table::rowoffset_type ForeignRow);
    //: Virtual new
    //
    // This method gives the appearance of a virtual new
    virtual DB::Table* Create(void) const;

    //: Create a table based on string
    //
    static DB::Table* CreateTable(const std::string& Name);

    static void GetTableNames( std::list< std::string >& TableNames );

    //: Get the number of fields
    //
    //!return: unsigned int - number of fields
    unsigned int GetFieldCount(void) const;

    //: Retrieve the field for a column
    //
    //!param: std::string ColumnName - Name of the column
    //
    //!return: Field& - Reference to the field
    Field& GetField( std::string ColumnName ) const;

    //: Retrieve the field for a column
    //
    //!param: int Offset 
    //
    //!return: Field& - Reference to the field
    Field& GetField(int Offset) const;

    //: Retrieve a field as a FieldArray<Type>
    //
    //!param: int Offset
    template <class Type>
    FieldArray<Type>& GetFieldArray(int Offset) const;

    //: Retrieve a field as a FieldBlob
    //
    //!param: int Offset 
    FieldBlob& GetFieldBlob(int Offset) const;

    //: Retrieve a field as a FieldString
    //
    //!param: int Offset 
    FieldString& GetFieldString(int Offset) const;

    //: Retrieve a field as a FieldUniqueId
    //
    //!param: int Offset 
    FieldUniqueId& GetFieldUniqueId(int Offset) const;

    //: Retrieve list of foreign keys
    const std::vector< ForeignKey >& GetForeignKeys( ) const;

    //: Convert the table to ILWD format
    //!return: ILwd::LdasContainer* - pointer to the created ILWD.
    ILwd::LdasContainer* GetILwd(void) const;

    //: Retrieve the name of the table
    //
    //!return: std::string - name of the table.
    std::string GetName(void) const;

    //: Return the number of rows.
    //
    // This method returns the maximum number of rows that have been
    // allocated to any one of the columns.
    //
    //!ret: rowoffset_type - number of rows
    rowoffset_type GetRowCount(void) const;

    //: Determine if a column is part of a foreign key
    bool IsPartOfForeignKey( const std::string& ColumnName ) const;

    //: Find the field
    //!return: Column offset if the field exists.
    int HasField(std::string FieldName) const;


    //: return true if the Container has table information
    static DB::Table* MakeTableFromILwd( const ILwd::LdasContainer& Container,
					 const bool Generic = false );

    //: Modify a specific entry
    //
    //!param: int ColumnId - Index of field where data is to be appended
    //!param: int Row - Row number of the data set ( starting from zero ).
    //!param: cosnt Type* Data - Pointer to new value for field.
    //!param: int Length - Length of data. This is used when the
    //+            Data is either character or blob data.
    //
    //!return: none
    //
    template < class Type >
    void ModifyEntry( int ColumnId, const int Row,
		      const Type* Data,
		      const int Length = -1 );

    //: Modify a specific entry
    //
    //!param: std::string ColumnName - name of the field where data is to
    //+                   be modified
    //!param: int Row - Row number of the data set ( starting from zero ).
    //!param: cosnt Type Data - New data value.
    //!param: int Length - Length of data. This is used when the
    //+            Data is either character or blob data.
    //
    //!return: none
    //
    template < class Type >
    void ModifyEntry( std::string ColumnName, const int Row,
		      const Type* Data,
		      const int Length = -1 );

    //: Obtain a reference to a table
    //
    static const DB::Table* ReferenceTable( const std::string& Name );

  protected:
    //: Add a foreign key
    //!param: const DB::Table::ForeignKey& Key - Key to copy
    void appendForeignKey(const DB::Table::ForeignKey& Key);
    //: Add a foreign key
    //!param: const std::vector<string>& Fields - list of field in current
    //+       table.
    //!param:  const std::string& ForeignTableName - name of foreign table
    //!param: const std::vector<string>& Fields - list of field in the
    //+       foreign table.
    void appendForeignKey(const std::vector<std::string>& Fields,
			  const std::string& ForeignTableName,
			  const std::vector<std::string>& ForeignTableFields);

    //: Add a field to the table
    //!param: std::string Name - Name of the field
    //!param: const DB::Field& Type - Storage allocation method for field
    //!param: bool Required - Specify if the field is considered required
    void appendField(std::string Name, const DB::Field& Type, bool Required);

  private:
    friend class DB::DB2SyntaxDriver;

    //: Singleton to keep track of all known db tables.
    class KnownTables
    {
       // Declare public methods generic to Singleton class:
       // see "general" library
       SINGLETON_TS_DECL( KnownTables );

    public:
   
       ~KnownTables();
   
       //: Find table (and corresponding lock) by name
       DB::Table* cloneTable( const std::string& name );

       //: Find original table used for cloning
       const DB::Table* findTable( const std::string& name ) const;

      void GetTableNames( std::list< std::string >& Results ) const;
   
    private:

       void add_from_sql_definition( const std::string& SQLSourceName );   
       void cleanup();

       // Helpfull typedef's
       typedef std::pair< const DB::Table*, pthread_mutex_t > TablePair;      
       typedef std::map< const std::string, TablePair >::value_type
          TableValueType;
       typedef std::map< const std::string, TablePair >::iterator
          TableIterator;   
       typedef std::map< const std::string, TablePair >::const_iterator
          TableConstIterator;      
   
       //: Map holding mask table and corresponding locks
       std::map< const std::string, TablePair > mTableMap;
    };
         
   
    //: Name of the table
    //
    // Each table should have a unique name and the name must exist
    // in the database.
    std::string		m_table_name;

    //: Fields in the table
    //
    // This is a list of pointers to the data for each field
    std::vector<Field*>	m_fields;

    //: Primary Key
    PrimaryKey			m_primary_key;

    //: List of foreign keys
    std::vector<ForeignKey>	m_foreign_keys;

    //: Appends column to the table
    //!param: const DB::Field* Field - pointer to field type.
    //!param: std::string Name - Name of column
    //!param: bool Required - true if the field is required.
    void append_column(const DB::Field* Field,
		       std::string Name,
		       bool Required);

    //: Retrieve the foreign key
    //
    //!param: const std::string& ForeignTable - Name of foreign table
    //
    ForeignKey& get_foreign_key(const std::string& ForeignTable);

    //: Ensure that an index is within the range.
    bool range_check(int index) const;

  }; // class - Table

  inline unsigned int Table::FieldDesc::
  GetIndex(void) const
  {
    return m_index;
  }

  inline std::string Table::FieldDesc::
  GetName(void) const
  {
    return m_name;
  }

  inline const Field* Table::FieldDesc::
  GetType(void) const
  {
    return m_type;
  }

  inline bool Table::FieldDesc::
  IsRequired(void) const
  {
    return m_required;
  }

  inline bool Table::PrimaryKey::
  HasPrimaryField( const std::string& ColumnName ) const
  {
    return find( m_fields.begin( ), m_fields.end( ), ColumnName )
      != m_fields.end( );
  }

  inline const std::string& Table::ForeignKey::
  GetForeignTable(void) const
  {
    return m_foreign_table;
  }

  inline DB::Table::columnoffset_type Table::ForeignKey::
  Size(void) const
  {
    return m_fields.size();
  }

  inline void Table::
  AddField(Field* Column)
  {
    m_fields.push_back(Column);
  }

  template < class Type >
  inline void Table::
  AppendNCopies(std::string ColumnName, int N, const Type* Data, int Size)
  {
    AppendNCopies(HasField(ColumnName), N, Data, Size);
  }

  template < class Type >
  inline void Table::
  AppendColumnEntries(std::string ColumnName, int Rows,
	     const Type* Data, const int* Lengths)
  {
    AppendColumnEntries(HasField(ColumnName), Rows, Data, Lengths);
  }

  inline void Table::
  AppendUniqueIds(std::string ColumnName, int Rows, std::string UniqueIdKey)
  {
    AppendUniqueIds(HasField(ColumnName), Rows, UniqueIdKey);
  }

  inline Field& Table::
  GetField( std::string ColumnName ) const
  {
    return GetField( HasField( ColumnName ) );
  }

  inline unsigned int Table::
  GetFieldCount(void) const
  {
    return m_fields.size();
  }

  inline const std::vector< Table::ForeignKey >& Table::
  GetForeignKeys( ) const
  {
    return m_foreign_keys;
  }

  inline std::string Table::
  GetName(void) const
  {
    return m_table_name;
  }

  template < class Type >
  inline void Table::
  ModifyEntry( std::string ColumnName, const int Row,
	       const Type* Data,
	       const int Length )
  {
    ModifyEntry( HasField(ColumnName), Row, Data, Length );
  }

  inline void DB::Table::
  appendForeignKey(const DB::Table::ForeignKey& Key)
  {
    m_foreign_keys.push_back(Key);
  }
} // namespace - DB

#endif	/* DB_TABLE_HH */
