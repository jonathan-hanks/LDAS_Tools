#if HAVE_CONFIG_H
#include "dbaccess/config.h"
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>
#include <sys/stat.h>

#if HAVE_DIRENT_H
# include <dirent.h>
# define NAMELEN(dirent) strlen((dirent)->d_name)
#else
# define dirent direct
# define NAMELEN(dirent) (dirent)->d_namelen
# if HAVE_SYS_NDIR_H
#  include <sys/ndir.h>
# endif
# if HAVE_SYS_DIR_H
#  include <sys/dir.h>
# endif
# if HAVE_NDIR_H
#  include <ndir.h>
# endif
#endif

#include <fstream>
#include <memory>   
#include <sstream>
#include <typeinfo>

#include "general/AtExit.hh"
#include "general/unordered_map.hh"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "DB2SyntaxDriver.hh"
#include "DB2SyntaxParser.hh"
#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldBlob.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/FieldUniqueId.hh"
#include "dbaccess/Table.hh"

   
using namespace std;
   
   
//#define DUMP_TO_STDERR

#if 0
#define AT()  std::cerr << __FILE__ << " " << __LINE__ << std::endl
#else
#define	AT()
#endif

//!ignore_begin:

//=======================================================================
// Case insensitive comparison
//=======================================================================

static bool
compare_ignore_case( const std::string& a, const std::string& b )
{
  if ( a.size() == b.size() )
  {
    size_t x;

    for ( x = 0; ( x < a.size() ) && ( tolower(a[x]) == tolower(b[x]) ); x++ )
      ;
    if ( x == a.size() )
    {
      return true;
    }
  }
  return false;
}

//=======================================================================
// Utility function to do range checking
//=======================================================================

//!exc: std::range_error("Requested Field out of range") - Field offset is
//+     out of range (either below zero or greater than the number of columns
//+     for the table).
bool DB::Table::
range_check(int Index) const
{
  if ((Index >= 0) && (Index < (int)(m_fields.size())))
  {
    return true;
  }
  throw std::range_error("Requested Field out of range");
}
//!ignore_end:

   
///=======================================================================
/// Class definition - DB::Table::BadTableName
///=======================================================================

DB::Table::BadTableName::
BadTableName(const std::string& TableName)
  : range_error("unknown table requested: " + TableName)
{}

   
///=======================================================================
/// Class definition - DB::Table::UnknownForeignKey
///=======================================================================

DB::Table::UnknownForeignKey::
UnknownForeignKey(const std::string& Table, const std::string& ForeignTable)
  : runtime_error("Unkown foreign key (" + ForeignTable + ") requested for "
		  + Table)
{}

   
///=======================================================================
/// Class definition - DB::Table::FieldDesc
///=======================================================================

DB::Table::FieldDesc::
FieldDesc(unsigned int Index, std::string Name, const Field* Type, bool Required)
  : m_index(Index),
    m_name(Name),
    m_required(Required),
    m_type(Type)
{}

   
///=======================================================================
/// Class definition - DB::Table::PrimaryKey
///=======================================================================

DB::Table::PrimaryKey::
PrimaryKey( void )
{}

   
DB::Table::PrimaryKey::
PrimaryKey(const std::vector<std::string>& Fields)
  : m_fields(Fields)
{
  //:TODO: check that the fields exist in the table.
}

///=======================================================================
/// Class definition - DB::Table::ForeignKey::ColumnMismatch
///=======================================================================

DB::Table::ForeignKey::ColumnMismatch::
ColumnMismatch(std::string Table, std::string Column,
	       std::string ForeignTable, std::string ForeignColumn)
  : std::logic_error("Columns are of different types: "
		     + Table + "." + Column + " vs "
		     + ForeignTable + "." + ForeignColumn)
{}

   
///=======================================================================
/// Class definition - DB::Table::ForeignKey
///=======================================================================

DB::Table::ForeignKey::
ForeignKey( void )
{}

   
//!exc: std::logic_error("Fields mismatch") - thrown when the number
//+     of fields specified for the source table differs from the
//+     number of fields specified for the foreign table.
DB::Table::ForeignKey::
ForeignKey( const std::vector<std::string>& Fields,
	    std::string ForeignTable,
	    const std::vector<std::string>& ForeignFields )
  : PrimaryKey(ForeignFields),
    m_fields(Fields),
    m_foreign_table(ForeignTable)
{
  if (Fields.size() != ForeignFields.size())
  {
    throw std::logic_error("Fields mismatch");
  }
  //:TODO: Check that the foreign fields exist in the foreign table
}

   
// The first element is the field name in the primary table and the second
// element is the the field name in the foreign table.
//!exc: std::range_error("FForeignKey::GetMapping: Offset") - thrown
//+     when the requested offset is beyond the range of the key.
void DB::Table::ForeignKey::
GetMapping(DB::Table::columnoffset_type Offset, std::pair<std::string, std::string>& Mapping) const
{
  if ( Offset >= Size( ) )
  {
    throw std::range_error("ForeignKey::GetMapping: Offset");
  }
  Mapping.first = m_fields[Offset];
  Mapping.second = DB::Table::PrimaryKey::m_fields[Offset];
   
  return;
}


   
// Instantiate generic to Singleton class methods:   
// see "general" library   
SINGLETON_TS_INST( DB::Table::KnownTables );   
   
   
//-----------------------------------------------------------------------------
//   
//: Constructor   
//   
// This constructor will be called only once for the whole run of an API 
// that is using dbaccess library.
//   
DB::Table::KnownTables::KnownTables()
{
  General::AtExit::Append( singleton_suicide,
			   "DB::Table::KnownTables::singleton_suicide",
			   0 );
   try
   {
      static const char* env = "LDAS_TABLE_DEFINITION";
      string path;

      if( getenv( env ) )
      {
         path += getenv(env);
      }
      else
      {
	 path += PREFIX;
	 path += "/doc/db2/doc/text";
      }

   
      struct stat	stats;

      if ( stat( path.c_str(), &stats) == 0 )
      {
        if ( stats.st_mode & S_IFDIR )
  	{
  	  // Read through the list of sql files
  	  DIR* dir( opendir( path.c_str() ) );
  	  struct dirent* entry;

          try
          {    
             while( ( entry = readdir(dir) ) )
  	     {
   	        const std::string filename( path + "/" + entry->d_name );
    
                if( ( filename.substr(filename.length() - 4) != ".sql" ) &&
   		    ( filename.substr(filename.length() - 4) != ".SQL" ) )
                {
                   continue;
                }
   
   	        stat( filename.c_str(), &stats );
                if ( !( stats.st_mode & S_IFREG ) )
                {
   	           continue;
                }
   
                add_from_sql_definition( filename );
  	     }
          }
          catch( ... )
          {
             closedir(dir);   
             throw;
          }
   
  	  closedir(dir);
  	}
  	else 
  	{
           string msg( "DBAccess: unable to create map of known tables." );
           msg += " Specified path for sql definitions: ";
           msg += path;
           msg += " is not a directory.";
   
  	   throw runtime_error( msg.c_str() );
  	}
      }
      else
      {
         string msg( "DBAccess: unable to create map of known tables." );
         msg += " Error executing stat() for sql definitions directory: ";
         msg += path;
   
         throw runtime_error( msg.c_str() );         
      }
  }
  catch( ... )
  {
     AT( );
     cleanup();

     throw;
  }
}

   
//-----------------------------------------------------------------------------
//   
//: Destructor
//   
DB::Table::KnownTables::~KnownTables()
{
   cleanup();
}
   
   
//-----------------------------------------------------------------------------
//   
//: Clone table
//   
// This method creates a copy of already defined Table in the map.
//  
//!param: const std::string& name - Name of the table to clone.
//   
DB::Table* DB::Table::KnownTables::cloneTable( const std::string& name )
{
   TableIterator iter( mTableMap.find( name ) );
   
   if( iter == mTableMap.end() )
   {
      throw BadTableName( name );
   }
   
   MutexLock lock( iter->second.second );
   return ( iter->second ).first->Create();
}

//-----------------------------------------------------------------------------
//   
//: Find table
//   
// This method reveals an access to already defined Table in the map.
//  
//!param: const std::string& name - Name of the table.
//   
const DB::Table* DB::Table::KnownTables::findTable( const std::string& name ) const
{
   TableConstIterator iter( mTableMap.find( name ) );
   
   if( iter == mTableMap.end() )
   {
      throw BadTableName( name );
   }
   
   return ( iter->second ).first;
}
   

void DB::Table::KnownTables::
GetTableNames( std::list< std::string >& TableNames ) const
{
  TableNames.erase( TableNames.begin( ), TableNames.end( ) );
  for ( TableConstIterator
	  cur = mTableMap.begin( ),
	  last = mTableMap.end( );
	cur != last;
	++cur )
  {
    TableNames.push_back( cur->first );
  }
}

//-----------------------------------------------------------------------------
//   
//: Add table definition based on sql file.
//
// This method adds an entry into the map of known tables by parsing
// corresponding sql file.   
//   
//!param: const std::string& SQLSourceName - Name of sql file with table 
//+       definition.
//
//!return: Nothing.   
//   
//!exc: runtime_error - Failed to insert new table definition into the map.
//   
void DB::Table::KnownTables::
add_from_sql_definition( const std::string& SQLSourceName )
{
  DB::DB2SyntaxDriver	driver;

  driver.Parse( SQLSourceName );

  for ( DB::DB2SyntaxDriver::table_iterator
	  tbl = driver.BeginTable( ),
	  last = driver.EndTable( );
        tbl != last;
	++tbl )
  {
     auto_ptr< Table > new_table( (*tbl)->Create() );
     pthread_mutex_t   new_lock;
   
     pair< TableIterator, bool > result = mTableMap.insert( 
                                             TableValueType( (*tbl)->GetName(),
                                                             TablePair( new_table.release(), new_lock ) ) );
   
     if( result.second == false )
     {
        // Table with given name already exists
        string msg( "DBaccess::KnownTables::add_from_sql_definition() failed" );
        msg += " to insert new table: ";
        msg += ( *tbl )->GetName();
        msg += ". Table already exists.";
   
        throw runtime_error( msg.c_str() );
     }
   
   
     // New table got inserted ===> initialize it's mutex
     pthread_mutex_init( &( (result.first)->second.second ), NULL );
   
   
#ifdef DUMP_TO_STDERR
    std::ostringstream	oss;

    const Table* const inserted_table( ( *result ).second.first );
    oss << "Table Definition: " << (*tbl)->GetName( )	// Name
	<< " (" << static_cast< const void* >( inserted_table ) << ")" // Pointer
	<< " Fields: ";
    for ( unsigned int cur = 0, end = inserted_table->GetFieldCount();
	  cur < end; ++cur )
    {
      if ( cur != 0 )
      {
	oss << ", ";
      }
   
      oss << inserted_table->GetField( cur ).GetName( );
    }
    std::cerr << oss.str( ) << std::endl;
#endif /* DUMP_TO_STDERR */
  }
  return;
}

   
//-----------------------------------------------------------------------------
//   
//: Cleanup.
//
// This method frees all dynamically allocated memory for the map of known 
// tables.
//
//!return: Nothing.   
//   
void DB::Table::KnownTables::cleanup()
{
   for( TableIterator iter = mTableMap.begin(), end_iter = mTableMap.end();
        iter != end_iter; ++iter )   
   {
      {
         MutexLock( iter->second.second );
         delete iter->second.first;
         iter->second.first = 0;
      }
   
      pthread_mutex_destroy( &( iter->second.second ) );
   }
}


///=======================================================================
/// Class definition - DB::Table
///=======================================================================

DB::Table::
Table( const std::string& Name)
  : m_table_name(Name)
{
}

   
DB::Table::
Table(const DB::Table& Source)
  : m_table_name(Source.m_table_name),
    m_primary_key(Source.m_primary_key),
    m_foreign_keys(Source.m_foreign_keys)
{
  for (std::vector<Field*>::const_iterator f = Source.m_fields.begin();
       f != Source.m_fields.end(); ++f)
  {
    m_fields.push_back((*f)->NewField((*f)->GetName(),
				      (*f)->IsRequired(),
				      *this));
  }
}

   
DB::Table::~Table()
{
  for (std::vector<Field*>::iterator f = m_fields.begin();
       f != m_fields.end(); ++f )
  {
    delete *f;
    *f = (Field*)NULL;
  }
  m_fields.resize(0);
}
   

//!exc: runtime_error("Unable to ingest ILwd table info") - if TableConttainer
//+     is not recognized as having ilwd appropriate for table description.
//!exc: runtime_error("Unknown column name") - if the FieldName is unknown
//+     in the context of the table.
void DB::Table::
AppendILwd( const ILwd::LdasContainer& TableContainer,
	    bool GenericTable )
{
  const bool create_fields( m_fields.size() <= 0 );
#define AUGMENT(Type) \
      { \
	const ILwd::LdasArray<Type>* f(static_cast<const ILwd::LdasArray<Type>*>(*e)); \
	GetFieldArray<Type>(offset).Append(f->getData(), f->getDimension(0)); \
      }

  if ((TableContainer.getName(2) != "table") ||
      (TableContainer.getName(1) != m_table_name))
  {
    throw std::runtime_error("Unable to ingest ILwd table info");
  }
  for ( ILwd::LdasContainer::const_iterator
	  e = TableContainer.begin();
	e != TableContainer.end();
	++e )
  {
    std::string field_name( (*e)->getName( 0 ) );   
    if( (*e)->getNameSize() >= 3 )
    {
       field_name = (*e)->getName( 2 );
    }

    if ( create_fields )
    {
      AddField( DB::Field::Create( *(*e), *this, !GenericTable ) );
    }

    int offset(HasField( field_name ));
    //-------------------------------------------------------------------
    // Transfer null mask
    //-------------------------------------------------------------------

    GetField( offset ).SetNullMask( (*e)->getNullMask() );

    switch ( GetField( offset ).GetType() )
    {
    case DB::Field::FIELD_BLOB:
      {
	//---------------------------------------------------------------
	// Converting to BLOB field
	//---------------------------------------------------------------
	AT();
	DB::FieldBlob&		blob(GetFieldBlob(offset));
	ILwd::LdasContainer*	c =
	  dynamic_cast<ILwd::LdasContainer*>(*e);

	if (c)
	{
	  for (ILwd::LdasContainer::const_iterator e2 = c->begin();
	       e2 != c->end();
	       e2++)
	  {
	    try
	    {
	      ILwd::LdasArray<CHAR_U>*	id =
		static_cast<ILwd::LdasArray<CHAR_U>*>(*e2);
	      if ( id )
	      {
		blob.push_back(id->getData(), id->getDimension(0));
	      }
	      else
	      {
		throw std::bad_cast( );
	      }
	    }
	    catch(const std::bad_cast& c)
	    {
	      throw std::runtime_error("Unrecognized format for BLOB field");
	    }
	  }
	}
	break;
      }
    case DB::Field::FIELD_STRING:
      {
	//---------------------------------------------------------------
	// Converting to String field
	//---------------------------------------------------------------
	AT();
	DB::FieldString&	str(GetFieldString(offset));
	switch((*e)->getElementId())
	{
	case ILwd::ID_LSTRING:
	  {
	    AT();
	    ILwd::LdasString*
	      str_c = dynamic_cast<ILwd::LdasString*>(*e);

	    if (str_c)
	    {
	      for (ILwd::LdasString::const_iterator i = str_c->begin();
		   i != str_c->end();
		   i++)
		{
		  str.push_back(*i);
		}
	    }
	    else
	    {
	      throw std::bad_cast( );
	    }
	  }
	  break;
	case ILwd::ID_ILWD:
	  {
	    AT();
	    ILwd::LdasContainer*	c =
	      dynamic_cast<ILwd::LdasContainer*>(*e);

	    if (c)
	    {
	      for (ILwd::LdasContainer::const_iterator i = c->begin();
		   i != c->end();
		   i++)
	      {
		std::string	buffer;

		switch((*i)->getElementId())
		{
		case ILwd::ID_CHAR:
		  {
		    AT();

		    ILwd::LdasArray<CHAR>* i_buffer =
		      static_cast<ILwd::LdasArray<CHAR>*>(*i);
		    if ( i_buffer )
		    {
		      buffer.append(i_buffer->getData(),
				    i_buffer->getDimension(0));
		    }
		    else
		    {
		      throw std::bad_cast( );
		    }
		  }
		  break;
		default:
		  throw std::runtime_error("Unknown conversion for String field");
		}
		str.push_back(buffer);
	      }
	    }
	    break;
	  }
	default:
	  throw std::runtime_error("Unknown conversion for String field");
	}
	break;
      }
    case DB::Field::FIELD_UNIQUE_ID:
      {
	//---------------------------------------------------------------
	// Converting to UniqueId field
	//---------------------------------------------------------------
	AT();
	DB::FieldUniqueId&	uid(GetFieldUniqueId(offset));
	ILwd::LdasContainer*	c =
	  dynamic_cast<ILwd::LdasContainer*>(*e);

	if (c)
	{
	  for (ILwd::LdasContainer::const_iterator e2 = c->begin();
	       e2 != c->end();
	       e2++)
	  {
	    std::string	buffer("");

	    try {
	      ILwd::LdasArray<CHAR>*	id =
		static_cast<ILwd::LdasArray<CHAR>*>(*e2);
	      if ( id )
	      {
		buffer.append(id->getData(),
			      id->getDimension(0));
		uid.push_back(buffer.c_str());
	      }
	      else
	      {
		throw std::bad_cast( );
	      }
	    }
	    catch(const std::bad_cast& c)
	    {
	      throw std::runtime_error("Unrecognized format for UniqueId field");
	    }
	  }
	}
	break;
      }
    default:
      //-----------------------------------------------------------------
      // Converting to Array field
      //-----------------------------------------------------------------
      AT();
      switch ((*e)->getElementId())
      {
      case ILwd::ID_INT_2S:	AUGMENT(INT_2S);	break;
      case ILwd::ID_INT_2U:	AUGMENT(INT_2U);	break;
      case ILwd::ID_INT_4S:	AUGMENT(INT_4S);	break;
      case ILwd::ID_INT_4U:	AUGMENT(INT_4U);	break;
      case ILwd::ID_INT_8S:	AUGMENT(INT_8S);	break;
      case ILwd::ID_INT_8U:	AUGMENT(INT_8U);	break;
      case ILwd::ID_REAL_4:	AUGMENT(REAL_4);	break;
      case ILwd::ID_REAL_8:	AUGMENT(REAL_8);	break;
      case ILwd::ID_COMPLEX_8:	AUGMENT(COMPLEX_8);	break;
      case ILwd::ID_COMPLEX_16:	AUGMENT(COMPLEX_16);	break;
      case ILwd::ID_LSTRING:
	{ 
	  AT();
	  const ILwd::LdasString* s(dynamic_cast<ILwd::LdasString*>(*e));
	  if ( s )
	  {
	    FieldString&	field(GetFieldString(offset));
	    for (ILwd::LdasString::const_iterator i = s->begin();
		 i != s->end();
		 i++)
	    {
	      field.push_back(*i);
	    }
	  }
	  else
	  {
	    throw std::bad_cast( );
	  }
	}
	break;
      default:
	AT();
	std::string msg("Unable to convert ILwd Table element (");
	msg += m_table_name + "." + GetField(offset).GetName() + ")";
	throw std::runtime_error(msg);
      }
    }
  }
}

template< class Type >
void DB::Table::
AppendNCopies(int ColumnId, int N, const Type* Data, int Size)
{
  FieldArray<Type>& field(GetFieldArray<Type>(ColumnId));
  for (int x = 0; x < N; x++)
  {
    field.Append(Data, 1);
  }
}

namespace DB
{
  template <>
  void Table::
  AppendNCopies<CHAR>(int ColumnId, int N, const CHAR* Data, int Size)
  {
    try {
      FieldString&	field(GetFieldString(ColumnId));

      for (int x = 0; x < N; x++)
      {
	field.push_back(Data);
      }
      return;
    }
    catch ( const std::bad_cast& )
    {
      // :TRICKY: Catch bad cast so another type can be tried.
    }
    FieldUniqueId&	field(GetFieldUniqueId(ColumnId));

    for (int x = 0; x < N; x++)
    {
      if (Size > 0)
      {
	field.push_back(Data, Size);
      }
      else
      {
	field.push_back(Data);
      }
    }
  }

  template <>
  void Table::
  AppendNCopies<CHAR_U>(int ColumnId, int N, const CHAR_U* Data, int Size)
  {
    FieldBlob&	field(GetFieldBlob(ColumnId));

    for (int x = 0; x < N; x++)
    {
      field.push_back(Data, Size);
    }
  }

  template < class Type >
  void Table::
  AppendColumnEntry(std::string ColumnName, const Type Data,
		    const int Length )
  {
    if (Length == -1)
    {
      AppendColumnEntries(HasField(ColumnName), 1, &Data);
    }
    else
    {
      int sizes[] = { Length };
      AppendColumnEntries(HasField(ColumnName), 1, &Data, sizes);
    }
  }

  template< class Type >
  void Table::
  AppendColumnEntries(int ColumnId, int Rows,
		      const Type* Data, const int* Lengths)
  {
    GetFieldArray<Type>(ColumnId).Append(Data, Rows);
  }

  template <>
  void Table::
  AppendColumnEntries<CHAR>(int ColumnId, int Rows,
			    const CHAR* Data, const int* Lengths)
  {
    if (!Data)
    {
      throw std::bad_cast();
    }
    if (Lengths)
    {
      for (int x = 0; x < Rows; x++)
      {
	try {
	  GetFieldString(ColumnId).push_back(Data, Lengths[x]);
	}
	catch ( const std::bad_cast& )
	{
	  GetFieldUniqueId(ColumnId).push_back(Data, Lengths[x]);
	}
	Data += Lengths[x];
      }
    }
    else
    {
      GetFieldArray<CHAR>(ColumnId).Append(Data, Rows);
    }
  }

  template <>
  void Table::
  AppendColumnEntries<CHAR_U>(int ColumnId, int Rows,
			      const CHAR_U* Data, const int* Lengths)
  {
    if (Lengths)
    {
      for (int x = 0; x < Rows; Data += Lengths[x], x++)
      {
	GetFieldBlob(ColumnId).push_back(Data, Lengths[x]);
      }
    }
    else
    {
      GetFieldArray<CHAR_U>(ColumnId).Append(Data, Rows);
    }
  }

  template <>
  void Table::
  AppendColumnEntry< CHAR >(std::string ColumnName, const CHAR* Data,
			    const int Length )
  {
    int sizes[] = { Length };

    if (Length == -1)
    {
      sizes[0] = strlen(Data);
    }
    AppendColumnEntries(HasField(ColumnName), 1, Data, sizes);
  }

  template <>
  void Table::
  AppendColumnEntry<CHAR_U>(std::string ColumnName, const CHAR_U* Data,
			    const int Length )
  {
    if (Length == -1)
    {
      throw std::runtime_error("request to add BLOB without specifying the size");
    }
    else
    {
      int sizes[] = { Length };
      AppendColumnEntries(HasField(ColumnName), 1, Data, sizes);
    }
  }

  template <>
  void Table::
  AppendColumnEntry< std::string >(std::string ColumnName,
				   const std::string Data,
				   const int Length )
  {
    AppendColumnEntry< CHAR >(ColumnName, Data.c_str(), Length);
  }
} // namespace - DB

void DB::Table::
AppendUniqueIds(unsigned int ColumnId, int Rows, std::string UniqueIdKey)
{
  FieldUniqueId& unique_id_field(GetFieldUniqueId(ColumnId));

  for (int x = 0; x < Rows; x++)
  {
    unique_id_field.push_back(UniqueIdKey);
  }
}

//!exc: UnknownForeignKey - thrown when a foreign key cannot be found
//+     for the specified ForeignTable.
void DB::Table::
Associate(const DB::Table::rowoffset_type Row,
	  const DB::Table& ForeignTable,
	  const DB::Table::rowoffset_type ForeignRow)
{
  AT( );
  const ForeignKey& fkey = get_foreign_key(ForeignTable.GetName());

  std::pair<std::string, std::string>	mapping;

  for (DB::Table::columnoffset_type x = 0; x < fkey.Size(); x++)
  {
    AT( );
    fkey.GetMapping(x, mapping);
    DB::Table::columnoffset_type	offset(HasField(mapping.first));
    DB::Table::columnoffset_type	foffset(ForeignTable.HasField(mapping.second));

    if (GetField(offset).GetType() != ForeignTable.GetField(foffset).GetType())
    {
      AT( );
      throw DB::Table::ForeignKey::ColumnMismatch(GetName(),
						  mapping.first,
						  ForeignTable.GetName(),
						  mapping.second);
    }
    switch(GetField(offset).GetType())
    {
    case DB::Field::FIELD_BLOB:
      {
	AT( );
	//:TODO: Complete
	FieldBlob& sblob(GetFieldBlob(offset));
	FieldBlob& fblob(ForeignTable.GetFieldBlob(foffset));

	if (fblob.Size() < ForeignRow )
	{
	  for ( unsigned int r = fblob.Size(); r < ForeignRow; r++ )
	  {
	    
	  }
	}
	if ( sblob.Size() >= Row )
	{
	  sblob.Replace(Row,
			fblob[ForeignRow]->getData(),
			fblob[ForeignRow]->getDimension(0) );
	}
	else if ( sblob.Size() == (Row - 1) )
	{
	  sblob.push_back( fblob[ForeignRow]->getData(),
			   fblob[ForeignRow]->getDimension(0) );
	}
      }
      break;
    case DB::Field::FIELD_UNIQUE_ID:
      {
	//:TODO: Complete
	AT( );

	FieldUniqueId& sid(GetFieldUniqueId(offset));
	AT( );
	FieldUniqueId& fid(ForeignTable.GetFieldUniqueId(foffset));
	AT( );

	//: Create the keys
	if ( fid.Size() < ( ForeignRow + 1) )
	{
	  AT( );
	  for ( Field::size_t r = fid.Size();
		r < (ForeignRow + 1);
		++r )
	  {
	    AT( );
	    fid.push_back( fid.MakeUniqueId(ForeignTable, r) );
	  }
	}
	AT( );
	if ( sid.Size() >= (Row + 1) )
	{
	  AT( );
	  sid.Replace(Row,
			fid[ForeignRow]->getData(),
			fid[ForeignRow]->getDimension(0));
	}
	else if ( sid.Size() == Row )
	{
	  AT( );
	  std::string key;

	  key.append( fid[ForeignRow]->getData(),
		      fid[ForeignRow]->getDimension(0) );
	  sid.push_back( key );
	}
	else
	{
	  AT( );
	  if (GetField(offset).IsRequired())
	  {
	    throw std::range_error("gaps in foreign key field");
	  }
	}
      }
      break;
    case DB::Field::FIELD_STRING:
      {
	AT( );

	FieldString& local(GetFieldString(offset));
	FieldString& foreign(ForeignTable.GetFieldString(foffset));
	
	if ( local.Size() == Row )
	{
	  local.push_back( foreign[ForeignRow] );
	}
	else
	{
	  if (GetField(offset).IsRequired())
	  {
	    throw std::range_error("gaps in foreign key field");
	  }
	}
      }
      break;
    default:
      AT( );
      if (GetField(offset).IsRequired())
      {
	throw std::runtime_error("Unable to produce association due to unhandled column type");
      }
      break;
    }
  }
}

DB::Table* DB::Table::
Create(void) const
{
  return new Table(*this);
}

   
//!exc: BadTableName - Thrown if a table of the requestd name could not be
//+     created.
DB::Table* DB::Table::
CreateTable(const std::string& Name)
{
  AT();

  // The very first time "Instance()" gets called, it will initialize 
  // map of known tables. Each of those tables will be reused later on
  // during the lifetime of the library as a factory for new
  // thread local tables
  return KnownTables::Instance().cloneTable( Name );
}

   
//!exc: std::range_error("Requested Field out of range") - Field offset is
//+     out of range (either below zero or greater than the number of columns
//+     for the table).
DB::Field& DB::Table::
GetField(int Offset) const
{
  range_check(Offset);
  return *m_fields[Offset];
}

//!exc: std::range_error("Requested Field out of range") - Field offset is
//+     out of range (either below zero or greater than the number of columns
//+     for the table).
template < class Type >
DB::FieldArray< Type > & DB::Table::
GetFieldArray(int Offset) const
{
  range_check(Offset);
  return dynamic_cast<DB::FieldArray<Type>&>(*m_fields[Offset]);
}

//!exc: std::range_error("Requested Field out of range") - Field offset is
//+     out of range (either below zero or greater than the number of columns
//+     for the table).
DB::FieldBlob& DB::Table::
GetFieldBlob(int Offset) const
{
  range_check(Offset);
  return dynamic_cast<DB::FieldBlob&>(*m_fields[Offset]);
}

//!exc: std::range_error("Requested Field out of range") - Field offset is
//+     out of range (either below zero or greater than the number of columns
//+     for the table).
DB::FieldString& DB::Table::
GetFieldString(int Offset) const
{
  range_check(Offset);
  return dynamic_cast<DB::FieldString&>(*m_fields[Offset]);
}

//!exc: std::range_error("Requested Field out of range") - Field offset is
//+     out of range (either below zero or greater than the number of columns
//+     for the table).
DB::FieldUniqueId& DB::Table::
GetFieldUniqueId(int Offset) const
{
  range_check(Offset);
  return dynamic_cast<DB::FieldUniqueId&>(*m_fields[Offset]);
}

ILwd::LdasContainer* DB::Table::
GetILwd(void) const
{
  DB::Field::size_t	data_size(0);

  std::string		ilwd_name( m_table_name );
  ilwd_name += "group:";
  ilwd_name += m_table_name;
  ilwd_name += ":table";

  ILwd::LdasContainer*	c( new ILwd::LdasContainer( ilwd_name ) );

  try {
    for ( std::vector<DB::Field*>::const_iterator field = m_fields.begin();
	 field != m_fields.end();
	 field++)
    {
      if (((*field)->IsRequired()) && ((*field)->Size() == 0))
      {
	std::string	msg("Required field found to be empty: ");

	msg += m_table_name;
	msg += ".";
	msg += (*field)->GetName();
	throw std::logic_error(msg.c_str());
      }
      if ((*field)->Size() > 0)
      {
	if (data_size)
	{
	  if ((*field)->Size() != data_size)
	  {
	    std::string msg("Not all fields have same number of rows: ");
	    msg += m_table_name + "." + (*field)->GetName();
	    throw std::range_error(msg);
	  }
	}
	else
	{
	  data_size = (*field)->Size();
	}

	bool			need_to_copy;
	ILwd::LdasElement*	fe((*field)->GetILwd(need_to_copy));
	
	std::string		name_string( (*field)->GenerateFieldName( ) );
	fe->setNameString( name_string );
   
        ILwd::LdasContainer::allocate_type to_allocate( ILwd::LdasContainer::NO_ALLOCATE );
        if( need_to_copy )
        {
           to_allocate = ILwd::LdasContainer::ALLOCATE;
        }
   
	c->push_back( fe, to_allocate, ILwd::LdasContainer::OWN );
      }
    }
  }
  catch(...)
  {
    delete c;	// Release allocated resources
    throw;	// Rethrow the exception
  }
  return c;
}
DB::Table::rowoffset_type DB::Table::
GetRowCount(void) const
{
  size_t	row_size(0);

  for (std::vector<DB::Field*>::const_iterator field = m_fields.begin();
       field != m_fields.end();
       field++)
  {
    if ((*field)->Size() > row_size)
    {
      row_size = (*field)->Size();
    }
  }
  return (int)row_size;
}

void DB::Table::
GetTableNames( std::list< std::string >& TableNames )
{
  KnownTables::Instance().GetTableNames( TableNames );
}

// IsPartOfForeignKey searches the list of foreign keys to see if the
// specified ColumnName is used as the local column component of a foreign
// key specification.
//
//!param: const std::string& ColumnName - The column name being search for
//+	in the list of foreign keys.
//!return: true if the column is part of a foreign key, false otherwise
bool DB::Table::
IsPartOfForeignKey( const std::string& ColumnName ) const
{
  for ( std::vector< ForeignKey >::const_iterator
	  cur = m_foreign_keys.begin( ),
	  end = m_foreign_keys.end( );
	cur != end;
	++cur )
  {
    if ( (*cur).HasPrimaryField( ColumnName ) )
    {
      return true;
    }
  }
  return false;
}

//!exc: runtime_error("Unknown column name") - if the FieldName is unknown
//+     in the context of the table.
int DB::Table::
HasField(std::string FieldName) const
{
  int cnt = 0;
  for (std::vector<Field*>::const_iterator f = m_fields.begin();
       f != m_fields.end();
       f++, cnt++)
  {
    if ( compare_ignore_case( (*f)->GetName(), FieldName ) )
    {
      return cnt;
    }
  }
  std::string msg("Unknown column name: ");
  msg += m_table_name + "." + FieldName;
  msg += "(valid fields: ";
  for (std::vector<Field*>::const_iterator
	 begin = m_fields.begin( ),
	 f = m_fields.begin();
       f != m_fields.end();
       f++, cnt++)
  {
    if ( f != begin )
    {
      msg += ", ";
    }
    msg += (*f)->GetName( );
  }
  msg += " )";
#ifdef DUMP_TO_STDERR
  {
    std::ostringstream	oss;
    const Table* const tbl( ReferenceTable( GetName( ) ) );

    oss << "Table Definition: " 			// Name
	<< GetName( ) << "(" << (void*)tbl << ")"	// Pointer
	<< " Fields: ";
    for ( unsigned int cur = 0, end = tbl->GetFieldCount( );
	  cur < end; ++cur )
    {
      if ( cur != 0 )
      {
	oss << ", ";
      }
      oss << tbl->GetField( cur ).GetName( );
    }
    std::cerr << oss.str( ) << std::endl;
  }
  std::cerr << msg << std::endl;
#endif /* DUMP_TO_STDERR */
  throw std::runtime_error(msg);
}


DB::Table* DB::Table::
MakeTableFromILwd( const ILwd::LdasContainer& Container,
		   const bool Generic )
{
  AT();
  std::domain_error	e("ILwd is not a Table");

  AT();
  if ( ( Container.getNameSize() != 3 )
       || ( Container.getName( 2 ) != "table" ) )
  {
    AT();
    throw e;
  }

  AT();
  std::string	tblName( Container.getName(1) );
  AT();
  static const std::string group("group");
  AT();
  DB::Table*	table = (DB::Table*)NULL;

  AT();
  if ( Generic )
  {
#if WORKING || 1
    AT();
    if ( ( Container.getName(0).length() < group.length() ) ||
	 ( Container.getName(0).substr( Container.getName( 0 ).length()
				      - group.length() )
	   != group ) )
    {
      AT();
      throw e;
    }
#endif

    AT();
    table = new DB::Table( tblName );
  }
  else
  {
    AT();
    if ( ( Container.getName(0).substr( 0, tblName.length() ) != tblName )
	 || ( Container.getName(0).substr( tblName.length() ) != group ) )
    {
      AT();
      throw e;
    }
    AT();
    table = DB::Table::CreateTable( tblName );
  }

  AT();
  table->AppendILwd( Container, Generic );

  AT();
  return table;
}

template< class Type >
void DB::Table::
ModifyEntry( int ColumnId, const int Row,
	     const Type* Data, const int Length )
{
  GetFieldArray<Type>(ColumnId).ModifyEntry( Row, Data );
}

namespace DB
{
  template <>
  void Table::
  ModifyEntry<CHAR>( int ColumnId, const int Row,
		     const CHAR* Data, const int Length )
  {
    if (!Data)
    {
      throw std::bad_cast();
    }
    try {
      GetFieldString(ColumnId).ModifyEntry(Row, Data);
    }
    catch ( const std::bad_cast& )
    {
      GetFieldUniqueId(ColumnId).ModifyEntry( Row, Data );
    }
  }

  template <>
  void Table::
  ModifyEntry<CHAR_U>( int ColumnId, const int Row,
		       const CHAR_U* Data, const int Length )
  {
    if ( Length > -1 )
    {
      GetFieldBlob(ColumnId).ModifyEntry( Row, Data, Length );
    }
    else
    {
      GetFieldArray<CHAR_U>(ColumnId).ModifyEntry( Row, Data );
    }
  }
} // namespace - DB

//!exc: BadTableName - Thrown if a table of the requestd name could not be
//+  found.
const DB::Table* DB::Table::
ReferenceTable( const std::string& Name )
{
  return KnownTables::Instance().findTable( Name );   
}
   

void DB::Table::
appendField(std::string Name, const DB::Field& Type, bool Required)
{
  m_fields.push_back(Type.NewField(Name, Required, *this));
  return;
}

void DB::Table::
appendForeignKey(const std::vector<std::string>& Fields,
		 const std::string& ForeignTableName,
		 const std::vector<std::string>& ForeignTableFields)
{
  m_foreign_keys.push_back(DB::Table::ForeignKey(Fields,
						 ForeignTableName,
						 ForeignTableFields));
}


void DB::Table::
append_column(const DB::Field* Field, std::string Name, bool Required)
{
  m_fields.push_back(Field->NewField(Name, Required, *this));
}

//!exc: UnknownForeignKey - thrown when a foreign key cannot be found
//+     for the specified ForeignTable.
DB::Table::ForeignKey& DB::Table::
get_foreign_key(const std::string& ForeignTable)
{
  for (std::vector<ForeignKey>::iterator fk = m_foreign_keys.begin();
       fk != m_foreign_keys.end();
       fk++)
  {
    if ((*fk).GetForeignTable() == ForeignTable)
    {
      return *fk;
    }
  }
  throw UnknownForeignKey(m_table_name, ForeignTable);
}

#define	INSTANTIATE(Type) \
template void \
DB::Table::AppendColumnEntry< Type >(std::string ColumnName, \
			      const Type Data, const int Length); \
template void \
DB::Table::AppendColumnEntries< Type >(int ColumnId, int Rows, \
			      const Type* Data, const int* Lengths); \
template DB::FieldArray< Type >& \
DB::Table::GetFieldArray< Type >(int Offset) const; \
template void \
DB::Table::ModifyEntry< Type >(int ColumnId, const int Row, \
			       const Type* Data, const int Length)

INSTANTIATE(INT_2S);
INSTANTIATE(INT_2U);
INSTANTIATE(INT_4S);
INSTANTIATE(INT_4U);
INSTANTIATE(INT_8S);
INSTANTIATE(INT_8U);
INSTANTIATE(REAL_4);
INSTANTIATE(REAL_8);
INSTANTIATE(COMPLEX_8);
INSTANTIATE(COMPLEX_16);

#undef INSTANTIATE
