%skeleton "lalr1.cc"                          /*  -*- C++ -*- */ 
%require "2.1a"
 /* %debug */
%defines
%output="DB2SyntaxParser.cc"
%name-prefix="DB"
%define "parser_class_name" "DB2SyntaxParser"

%{
# include <string>
   namespace yy
   {
   }

   namespace DB
   {
     using namespace yy;

     class DB2SyntaxDriver;
   } // namespace - DB
%}

// The parsing context.
%parse-param { DB2SyntaxDriver& Driver }
%lex-param   { DB2SyntaxDriver& Driver }
%locations
//%initial-action
//{
  // Initialize the initial location.
//  @$.begin.filename = @$.end.filename = &driver.file;
//};
%debug
%error-verbose

// Symbols.
%union
{
  int		ival;
  std::string*	sval;
};
%{
# include "DB2SyntaxDriver.hh"

#define YY_DECL \
  static DB::DB2SyntaxParser::token_type  		\
  yylex( DB::DB2SyntaxParser::semantic_type* YYLval,	\
	 DB::DB2SyntaxParser::location_type* YYLloc,	\
	 DB::DB2SyntaxDriver& Driver )

// Declare it for the parser's sake.
YY_DECL;
%}

%token <ival> TKN_CONSTANT_INTEGER	"const_integer"
%token <sval> TKN_CONSTANT_STRING	"const_string"
%destructor { delete $$; }		"const_string"
%token <sval> TKN_IDENTIFIER		"identifier"
%destructor { delete $$; }		"identifier"

%token TKN_END 0

%token TKN_ACTION
%token TKN_ALL
%token TKN_ALWAYS
%token TKN_AND
%token TKN_AS
%token TKN_ASC
%token TKN_BIGINT
%token TKN_BIT
%token TKN_BLOB
%token TKN_BY
%token TKN_CACHE
%token TKN_CAPTURE
%token TKN_CASCADE
%token TKN_CHANGES
%token TKN_CHAR
%token TKN_CHARACTER
%token TKN_CHECK
%token TKN_CLOB
%token TKN_COMPACT
%token TKN_CONSTRAINT
%token TKN_CREATE
%token TKN_CURRENT
%token TKN_DATA
%token TKN_DBCLOB
%token TKN_DEFAULT
%token TKN_DELETE
%token TKN_DESC
%token TKN_DOUBLE
%token TKN_ECHO
%token TKN_FOR
%token TKN_FOREIGN
%token TKN_GE
%token TKN_GENERATED
%token TKN_GRANT
%token TKN_HASHING
%token TKN_IDENTITY
%token TKN_INCREMENT
%token TKN_INDEX
%token TKN_INITIALLY
%token TKN_INT
%token TKN_INTEGER
%token TKN_KEY
%token TKN_LE
%token TKN_LOGGED
%token TKN_LONG
%token TKN_NE
%token TKN_NO
%token TKN_NONE
%token TKN_NOT
%token TKN_NULL
%token TKN_ON
%token TKN_OR
%token TKN_PARTITIONING
%token TKN_PRECISION
%token TKN_PRIMARY
%token TKN_PUBLIC
%token TKN_REAL
%token TKN_REFERENCES
%token TKN_REPLICATED
%token TKN_RESTRICT
%token TKN_REVOKE
%token TKN_SELECT
%token TKN_SET
%token TKN_SMALLINT
%token TKN_START
%token TKN_SUMMARY
%token TKN_TABLE
%token TKN_TIMESTAMP
%token TKN_UNIQUE
%token TKN_UPDATE
%token TKN_USING
%token TKN_VARCHAR
%token TKN_VARYING
%token TKN_WITH

%type <sval> column_name 	"column_name"
%destructor { delete $$; }	"column_name"
%type <sval> table_name 	"table_name"
%destructor { delete $$; }	"table_name"

%%

%start sql_stmts;

table_name: TKN_IDENTIFIER { $$ = $1; }
	;
column_name: TKN_IDENTIFIER { $$ = $1; }
	;

sql_stmts: sql_stmt ';'
	| sql_stmts ';' sql_stmt ';'
	;

sql_stmt: create
	| admin_stmt
	| error ';'
	;

admin_stmt: TKN_GRANT ignore_clause
	| TKN_ECHO ignore_clause
	| TKN_REVOKE ignore_clause
	;

ignore_clause: error ';'

create: TKN_CREATE create_clause
	| TKN_CREATE error ';'
	;

create_clause: create_table
	;

/*
 * CREATE TABLE
 */
create_table:
	tbl_summary TKN_TABLE table_name
	{
	  Driver.reset_table_state( );
	  Driver.m_table_state.name = *$3;
	}
	create_table_clause
	;

create_table_clause:
	'(' element_list ')' tbl_sequences_1
	{
	  Driver.create_table_from_state_info( );
	}
	| error {
	  std::cerr << "Unable to create table: "
		    << Driver.m_table_state.name << std::endl;
	}
	;

tbl_summary: /* empty */
	| TKN_SUMMARY
	;

element_list: element_list_term
	| element_list ',' element_list_term
	;

element_list_term: /* empty */
	| column_definition
	| tbl_constraint
	;

column_definition:
	column_name
	{
	  Driver.reset_column_state( );
	  Driver.m_column_state.name = *$1;
	}
	data_type column_options
	{
	  Driver.create_column_from_state_info( );
	}
	;

data_type:
        TKN_SMALLINT { Driver.m_column_state.type = DB::SHORT; }
	| TKN_INTEGER { Driver.m_column_state.type = DB::INTEGER; }
	| TKN_INT { Driver.m_column_state.type = DB::INTEGER; }
	| TKN_BIGINT { Driver.m_column_state.type = DB::BIGINT; }
	| TKN_REAL { Driver.m_column_state.type = DB::REAL; }
	| TKN_TIMESTAMP { Driver.m_column_state.type = DB::UNSET; }
	| _dt_double { Driver.m_column_state.type = DB::DOUBLE; }
	| _dt_character_sequence
	| _dt_blob { Driver.m_column_state.type = DB::Blob; } '(' TKN_CONSTANT_INTEGER { Driver.m_column_state.size = $4; } _dt_blob_scale ')'
	| error { std::cerr << "Bad Column Type: "
              		    << std::string( Driver.Lexer( ).YYText( ),
					    Driver.Lexer( ).YYLeng( ) )
			    << " ("
			    << Driver.m_table_state.name
			    << "."
			    << Driver.m_column_state.name
			    << ")"
			    << std::endl; }
	;

_dt_blob: TKN_BLOB
	| TKN_CLOB
	| TKN_DBCLOB
	;

_dt_blob_scale:
	| TKN_IDENTIFIER
        { if ( $1->length( ) == 1)
	  {
	    switch( $1->operator[]( 0 ) )
	    {
	    case 'K': Driver.m_column_state.size *= 1024; break;
	    case 'M': Driver.m_column_state.size *= 1024 * 1024; break;
	    case 'G': Driver.m_column_state.size *= 1024 * 1024 * 1024; break;
	    }
	  }
	}
	;

_dt_character_sequence:
  _dt_character
  ;

_dt_character:
	_dt_varchar _dt_size_spec { Driver.m_column_state.type = VARCHAR; }
	| _dt_char _dt_size_spec { Driver.m_column_state.type = CHARACTER; }
	;

_dt_char:
  TKN_CHAR
  | TKN_CHARACTER
  ;

_dt_double: TKN_DOUBLE
	| TKN_DOUBLE TKN_PRECISION
	;

_dt_varchar:
  TKN_VARCHAR
  | TKN_CHAR TKN_VARYING
  | TKN_CHARACTER TKN_VARYING
  | TKN_LONG TKN_VARCHAR
  ;

_dt_size_spec: '(' TKN_CONSTANT_INTEGER { Driver.m_column_state.size = $2; } ')';


column_options: /* empty */
	| column_options_2
	;

column_options_2:
	column_options_2 column_option
	| column_option
	;

column_option:
	TKN_NOT TKN_NULL
	{ 
	  Driver.m_column_state.not_null = true;
	}
	| default_clause { Driver.m_column_state.default_exists = true; }
	| TKN_FOR TKN_BIT TKN_DATA { Driver.m_column_state.bit_data = true; }
	| lob_options
	| column_default_spec
	;

lob_options: TKN_NOT TKN_LOGGED
	| TKN_LOGGED
	| TKN_NOT TKN_COMPACT
	| TKN_COMPACT
	;

column_default_spec: TKN_GENERATED
	column_default_spec_2
	TKN_AS
	column_default_spec_3
	;

column_default_spec_2: TKN_ALWAYS
        {
	  Driver.m_column_state.generated_always = true;
        }
        | TKN_BY TKN_DEFAULT
	;

column_default_spec_3:
	identity_clause
	;

identity_clause:
	TKN_IDENTITY '(' identity_clause_2 ')'
	| TKN_IDENTITY
	;

identity_clause_2:
	identity_clause_3
	| identity_clause_2 ',' identity_clause_3
	;

identity_clause_3:
	TKN_START TKN_WITH numeric_constant
	| TKN_INCREMENT TKN_BY numeric_constant
	| TKN_CACHE TKN_CONSTANT_INTEGER
	| TKN_NO TKN_CACHE
	;

default_clause:
	TKN_WITH TKN_DEFAULT default_values
	| TKN_DEFAULT default_values
	;

default_values: /* empty */
	| TKN_CONSTANT_INTEGER
	| TKN_CONSTANT_STRING
	| TKN_CURRENT TKN_TIMESTAMP
	| error {
	  std::cerr << "Unsupported default value for field "
		    << Driver.m_table_state.name << "." << Driver.m_column_state.name
		    << std::endl;
	}
	;

tbl_sequences_1: /* empty */
	| tbl_sequence_1
	| tbl_sequences_1 ',' tbl_sequence_1
	;

tbl_sequence_1:
	data_capture
	| TKN_PARTITIONING TKN_KEY tbl_partitioning tbl_hashing
	| TKN_REPLICATED
	| TKN_NOT TKN_LOGGED TKN_INITIALLY
	;
 
tbl_partitioning: '(' column_name ')'
	| tbl_partitioning ',' '(' column_name ')'
	;

tbl_hashing: /* empty */
	| TKN_USING TKN_HASHING
	;

tbl_constraint: TKN_CONSTRAINT TKN_IDENTIFIER { Driver.m_constraint = Driver.UNKNOWN; } constraint_type;

constraint_type: TKN_UNIQUE { Driver.m_constraint = Driver.UNIQUE; } column_list_decl
	| TKN_PRIMARY TKN_KEY { Driver.m_constraint = Driver.PRIMARY; } column_list_decl
	| TKN_FOREIGN TKN_KEY { Driver.m_constraint = Driver.FOREIGN; Driver.reset_foreign_key_state( ); } column_list_decl { Driver.m_foreign_key_state.reference_clause = true; } reference_clause { Driver.m_foreign_keys.push_back(Driver.m_foreign_key_state); }
	| TKN_CHECK { Driver.m_constraint = Driver.CHECK; } '(' check_condition ')'
	| error { std::cerr << "constraint_type: "
			    << Driver.m_table_state.name
			    << " " << std::string( Driver.Lexer( ).YYText( ),
						   Driver.Lexer( ).YYLeng( ) )
			    << std::endl; }
	;

check_condition: TKN_NOT '(' check_condition ')' check_condition_2
	| '(' check_condition ')' check_condition_2
	| condition
	;

check_condition_2: /* empty */
	| condition_token check_condition
	;

condition: condition_binary_arg condition_binary_op condition_binary_arg
	;

condition_binary_arg: TKN_IDENTIFIER
	| TKN_CONSTANT_INTEGER
	| TKN_CONSTANT_STRING
	;

condition_binary_op: '>'
	| '<'
	| '='
	| TKN_LE
	| TKN_GE
	| TKN_NE
	;

condition_token: TKN_AND
	| TKN_OR
	;

column_list_opt_decl: /* empty */
	| column_list_decl
	;

column_list_decl: '(' column_list ')'
	;

column_list: column_name { Driver.append_column_to_constraint(*$1); }
	| column_list ',' column_name { Driver.append_column_to_constraint(*$3); }
	;

reference_clause: TKN_REFERENCES table_name { Driver.m_foreign_key_state.foreign_table_name = *$2; } column_list_opt_decl rule_clause;

rule_clause: /* empty */
	| rule_clause rule_delete_or_update
	;

rule_delete_or_update: TKN_ON TKN_DELETE rule_clause_delete
	| TKN_ON TKN_UPDATE rule_clause_update
	;

rule_clause_delete: TKN_NO TKN_ACTION
| TKN_RESTRICT
| TKN_CASCADE
| TKN_SET TKN_NULL
;

rule_clause_update: TKN_NO TKN_ACTION
| TKN_RESTRICT
;

data_capture: TKN_DATA TKN_CAPTURE TKN_CHANGES
	| TKN_DATA TKN_CAPTURE TKN_NONE
	;

numeric_constant:
	TKN_CONSTANT_INTEGER
	;

%%

void DB::DB2SyntaxParser::
error ( const DB::DB2SyntaxParser::location_type& l,
	const std::string& m )
{
  // Driver.Error (l, m);
}


DB::DB2SyntaxParser::token_type
yylex( DB::DB2SyntaxParser::semantic_type* YYLval,
       DB::DB2SyntaxParser::location_type* YYLloc,
       DB::DB2SyntaxDriver& Driver )
{
  return Driver.Lex( YYLval, YYLloc );
}
