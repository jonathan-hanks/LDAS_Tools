#include <fstream>

#include "DB2SyntaxDriver.hh"
#include "DB2SyntaxParser.hh"

namespace DB
{
  DB2SyntaxDriver::
  DB2SyntaxDriver( )
    : m_scanner( new DB2SyntaxLexer( ) )
  {
  }

  DB2SyntaxDriver::
  ~DB2SyntaxDriver( )
  {
  }

  void DB2SyntaxDriver::
  Parse( const std::string& Filename )
  {
    std::ifstream	source( Filename.c_str( ) );
    DB::DB2SyntaxParser parser( *this );

    m_scanner->yyrestart( &source );
    parser.parse( );

    source.close( );
  }

  DB2SyntaxDriver::token_type DB2SyntaxDriver::
  Lex( DB2SyntaxParser::semantic_type* YYLval,
       DB2SyntaxParser::location_type* YYLloc )
  {
    m_scanner->SetYYLval( YYLval );
    m_scanner->SetYYLloc( YYLloc );
    
    token_type	retval = token_type( m_scanner->yylex( ) );

    return retval;
  }

  void DB::DB2SyntaxDriver::
  append_column_to_constraint(std::string Name)
  {
    switch(m_constraint)
    {
    case FOREIGN:
      if (m_foreign_key_state.reference_clause)
      {
	m_foreign_key_state.foreign_table_fields.push_back(Name);
      }
      else
      {
	m_foreign_key_state.fields.push_back(Name);
      }
      break;
    default:
      break;
    }
  }

  void DB::DB2SyntaxDriver::
  create_column_from_state_info(void)
  {
    if (m_column_state.type != UNSET)
    {
      m_fields.push_back(m_column_state);
    }
  }

  void DB::DB2SyntaxDriver::
  create_table_from_state_info(void)
  {
    //---------------------------------------------------------------------
    // Create space for the table
    //---------------------------------------------------------------------
    DB::Table*	table;
    m_tables.push_back((table = new DB::Table(m_table_state.name)));
    //---------------------------------------------------------------------
    // Add columns to the table
    //---------------------------------------------------------------------
    for (std::vector<state_column_type>::const_iterator c = m_fields.begin();
	 c != m_fields.end();
	 c++)
    {
      bool		required(is_required(*c));
      DB::FieldType_type
	field_type = (is_unique_id(*c))
	? UniqueId : c->type;

      table->appendField( c->name, DB::GetFieldType(field_type), required );
    }
    //---------------------------------------------------------------------
    // Add foreign keys
    //---------------------------------------------------------------------
    for (std::list<state_foreign_key_type>::const_iterator
	   fk = m_foreign_keys.begin();
	 fk != m_foreign_keys.end();
	 fk++)
    {
      table->appendForeignKey(fk->fields,
			      fk->foreign_table_name,
			      fk->foreign_table_fields);
    }
  }

  bool DB::DB2SyntaxDriver::
  is_part_of_foreign_key(const state_column_type& ColumnInfo) const
  {
    for (std::list<state_foreign_key_type>::const_iterator
	   fk = m_foreign_keys.begin();
	 fk != m_foreign_keys.end();
	 fk++)
    {
      for (std::vector<std::string>::const_iterator f = fk->fields.begin();
	   f != fk->fields.end();
	   f++)
      {
	if (*f == ColumnInfo.name)
	{
	  return true;
	}
      }
    }
    return false;
  }

  bool DB::DB2SyntaxDriver::
  is_required(const state_column_type& ColumnInfo) const
  {
    bool required = false;

    if (ColumnInfo.not_null)
    {
      if ( !ColumnInfo.default_exists
	   && !ColumnInfo.generated_always )
      {
	// Required because the field is not null and no default value
	// has been specified.
	required = true;
      }
      if (required && is_unique_id(ColumnInfo))
      {
	if (!is_part_of_foreign_key(ColumnInfo))
	{
	  // If the unique id is not part of a foreign key, then it is
	  // valid to let the metadataAPI supply the value.
	  required = false;
	}
      }
    }

    return required;
  }

  bool DB::DB2SyntaxDriver::
  is_unique_id(const state_column_type& ColumnInfo) const
  {
    if ((ColumnInfo.bit_data)
	&& (ColumnInfo.type == CHARACTER)
	&& (ColumnInfo.size == 13))
    {
      return true;
    }
    return false;
  }

  void DB::DB2SyntaxDriver::
  reset_column_state(void)
  {
    m_column_state.name = "";
    m_column_state.type = UNSET;
    m_column_state.size = 0;
    m_column_state.not_null = false;
    m_column_state.generated_always = false;
    m_column_state.default_exists = false;
    m_column_state.default_type = UNSET;
    m_column_state.bit_data = false;
  }

  void DB::DB2SyntaxDriver::
  reset_foreign_key_state(void)
  {
    m_foreign_key_state.reference_clause = false;
    m_foreign_key_state.fields.resize(0);
    m_foreign_key_state.foreign_table_name = "";
    m_foreign_key_state.foreign_table_fields.resize(0);
  }

  void DB::DB2SyntaxDriver::
  reset_table_state(void)
  {
    m_fields.resize(0);
    m_table_state.name = "";
  }
} // namespace - DB
