#ifndef DB__DB2_SYNTAX_DRIVER_HH
#define DB__DB2_SYNTAX_DRIVER_HH

#include <list>
#include <memory>
#include <string>
#include <vector>

#include "general/unordered_map.hh"

#include "dbaccess/dbaccess.h"
#include "dbaccess/Table.hh"
#include "DB2SyntaxLexer.hh"
#include "DB2SyntaxParser.hh"

namespace DB
{
  class DB2SyntaxDriver;
  class DB2SyntaxLexer;
} // namespace - DB

namespace DB
{
  class DB2SyntaxDriver
  {
  public:
    typedef std::list<DB::Table*>::const_iterator table_iterator;
    typedef DB2SyntaxParser::token_type token_type;

    DB2SyntaxDriver( );
    virtual ~DB2SyntaxDriver( );
    DB2SyntaxLexer& Lexer( ) const;
    void Parse( const std::string& Filename );
    token_type Lex( DB::DB2SyntaxParser::semantic_type* YYLval,
		    DB::DB2SyntaxParser::location_type* YYLloc );

    table_iterator BeginTable(void) const;
    table_iterator EndTable(void) const;

  private:
    friend class DB2SyntaxParser;

    typedef struct {
      std::string	name;
    } state_table_type;

    typedef struct {
      typedef union {
	double	dbl;
	int	integer;
	float	flt;
	char*	str;
      } data_value_type;

      std::string		name;
      DB::FieldType_type	type;
      unsigned int		size;
      bool			not_null;
      bool			generated_always;
      bool			default_exists;
      DB::FieldType_type	default_type;
      data_value_type		default_value;
      bool			bit_data;
    } state_column_type;

    typedef struct {
      bool			reference_clause;
      std::vector<std::string>	fields;
      std::string		foreign_table_name;
      std::vector<std::string>	foreign_table_fields;
    } state_foreign_key_type;

    typedef enum {
      FOREIGN,
      PRIMARY,
      UNIQUE,
      CHECK,
      UNKNOWN
    } constraint_type;

    state_column_type			m_column_state;
    constraint_type			m_constraint;
    std::vector<state_column_type>	m_fields;
    std::list<state_foreign_key_type>	m_foreign_keys;
    state_foreign_key_type		m_foreign_key_state;
    std::list<DB::Table*>		m_tables;
    state_table_type			m_table_state;

    std::auto_ptr< DB2SyntaxLexer >	m_scanner;

    void append_column_to_constraint(std::string Name);
    void create_column_from_state_info( );
    void create_table_from_state_info(void);
    bool is_part_of_foreign_key(const state_column_type& ColumnInfo) const;
    bool is_required(const state_column_type& ColumnInfo) const;
    bool is_unique_id(const state_column_type& ColumnInfo) const;
    void reset_column_state( );
    void reset_foreign_key_state( );
    void reset_table_state( );

  };

  inline DB2SyntaxDriver::table_iterator DB2SyntaxDriver::
  BeginTable(void) const
  {
    return m_tables.begin();
  }

  inline DB2SyntaxDriver::table_iterator DB2SyntaxDriver::
  EndTable(void) const
  {
    return m_tables.end();
  }
  inline DB2SyntaxLexer& DB2SyntaxDriver::
  Lexer( ) const
  {
    return *(m_scanner.get( ) );
  }
} // namespace - DB

#endif /* DB__DB2_SYNTAX_DRIVER_HH */
