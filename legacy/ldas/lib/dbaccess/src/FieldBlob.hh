#ifndef DB_FIELD_BLOB_HH
#define	DB_FIELD_BLOB_HH

#include "general/types.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "dbaccess/Field.hh"

namespace DB {
  class Table;

  //: Blob field
  //
  // This class contains the definition and methods appropriate for
  // working with blob data. This data requires a special handler
  // since there is no deterministic order to the data that can
  // be infered by the database software.
  //
  class FieldBlob: public DB::Field
  {
  public:
    //: Constructor
    //
    //!param: std::string Name - Name of column to create
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldBlob(const std::string& Name, bool Required,
	      const DB::Table& Table);

    //: Get ILwd representation
    //
    // This method generates an
    // ilwd representing the data of the field.
    //
    //!return: ILwd::LdasElement* - ilwd representation
    ILwd::LdasContainer* GetILwd(bool& NeedToCopy) const;

    //: Modify an entry.
    //
    //!param: const int Row - Row offset of entry to modify.
    //!param: const CHAR_U* Data - Pointer to new value for field.
    //!param: const int Length - Length of Data
    //
    //!return: none
    void ModifyEntry( const int Row, const CHAR_U* Data, const int Length );

    //: Virtual constructor
    //
    //!param: std::string Name - Name for the column
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldBlob* NewField( const std::string& Name,
			 bool Required,
			 const DB::Table& Table ) const;

    //: Append data
    //
    // Add a single element to the list of data managed by this instance.
    //
    //!param: const CHAR_U* Data - Data to be appended.
    //!param: unsigned int Length - number of bytes composing Data.
    void push_back(const CHAR_U* Data, unsigned int Length);

    //: Replace data
    //
    // Replace data at the specified row with another value.
    //
    //!param: int Row - Offset for data to be replaced
    //!param: const CHAR_U* Data - New value
    //!param: unsigned int Length - number of bytes composing Data.
    void Replace(unsigned int Row, const CHAR_U* Data, unsigned int Length);

    //: Number of elements in data set.
    //
    // This method returns the number
    // of elements (rows) that are contained in the data set.
    //
    //!return: size_t - Number of elements in the data set.
    Field::size_t Size(void) const;

    //: Get Nth element
    //
    // Returns the data specified by Offset.
    //
    //!param: unsigned int Offset - Offset of requested data item.
    //!return: ILwd::LdasArray<CHAR_U>* - Pointer to data at offset.
    const ILwd::LdasArray<CHAR_U>* operator[](unsigned int Offset) const;

  private:
    //: Data storage
    ILwd::LdasContainer		m_data;
  }; // class - FieldBlob

} // namespace - DB
#endif	/* DB_FIELD_BLOB_HH */
