#include "dbaccess/config.h"

#include "dbaccess/Field.hh"
#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldBlob.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/Table.hh"

using DB::Field;
using DB::Table;

static DB::Field& create_null_ref( );

namespace {
  //: String Field
  //
  // This class contains the definition and methods appropriate for
  // working with string data.
  class FieldNull: public Field
  {
  public:
    //: Constructor
    FieldNull( );

    //: Get ILwd representation
    //
    // This method generates an
    // ilwd representing the data of the field.
    //
    //!return: ILwd::LdasString* - ilwd representation
    virtual ILwd::LdasString* GetILwd(bool& NeedToCopy) const;
    
    //: Virtual constructor
    //
    //!param: std::string Name - Name for the column
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldNull* NewField( const std::string& Name,
			 bool Required,
			 const DB::Table& Table ) const;
    //: Number of elements in data set.
    //
    // This method returns the number
    // of elements (rows) that are contained in the data set.
    //
    //!return: Field::size_t - Number of elements in the data set.
     /* virtual */ Field::size_t Size(void) const;
    
  private:
    //: Copy Constructor
    FieldNull( const FieldNull& );
    
    static const Table& get_table( );
    
  }; // class - FieldString

  Field::size_t FieldNull::Size(void) const
  {
    return 0;
  }
  //---------------------------------------------------------------------
  //                    P U B L I C   M E T H O D S
  //---------------------------------------------------------------------

  FieldNull::
  FieldNull( )
    : Field( "__null__", false, Field::FIELD_NULL, get_table( ) )
  {
  }

  ILwd::LdasString* FieldNull::
  GetILwd( bool& NeedToCopy ) const
  {
    throw std::runtime_error( "method GetILwd called on FieldNull" );
  }

  FieldNull* FieldNull::
  NewField( const std::string& Name, bool Required, const DB::Table& Table ) const
  {
    throw std::runtime_error( "method NewField called on FieldNull" );
  }

  //---------------------------------------------------------------------
  //                   P R I V A T E   M E T H O D S
  //---------------------------------------------------------------------

  const Table& FieldNull::
  get_table( )
  {
    static const Table	table( "__null_field_table__" );

    return table;
  }
} // anonymous

const DB::Field& DB::Field::NullRef = create_null_ref( );

DB::Field::
Field(const std::string& Name, bool Required, DB::Field::field_type Type,
      const DB::Table& Table)
  : m_name(Name),
    m_required(Required),
    m_table(Table),
    m_type(Type)
{
}

DB::Field::
~Field(void)
{
}

DB::Field* DB::Field::
Create( const ILwd::LdasElement& Element,
	const DB::Table& Table,
	bool Required )
{
  switch ( Element.getElementId() )
  {
  case ILwd::ID_LSTRING:
    return new DB::FieldString( Element.getNameString(), Required, Table );
#define A(T) case ILwd::ID_##T: return new DB::FieldArray<T>( Element.getNameString(), Required, Table )

    A( INT_2S );
    A( INT_2U );
    A( INT_4S );
    A( INT_4U );
    A( INT_8S );
    A( INT_8U );
    A( REAL_4 );
    A( REAL_8 );
    A( COMPLEX_8 );
    A( COMPLEX_16 );

#undef A

  case ILwd::ID_CHAR:
    return new DB::FieldString( Element.getNameString(),
				Required,
				Table );
    break;
  case ILwd::ID_CHAR_U:
    return new DB::FieldBlob( Element.getNameString(),
			      Required,
			      Table );
    break;
  case ILwd::ID_ILWD:
    {
      const ILwd::LdasContainer& container =
	dynamic_cast<const ILwd::LdasContainer&>( Element );

      if ( container.size() > 0 )
      {
	switch( container[0]->getElementId() )
	{
	case ILwd::ID_CHAR:
	  return new DB::FieldString( Element.getNameString(),
				      Required,
				      Table );
	case ILwd::ID_CHAR_U:
	  return new DB::FieldBlob( Element.getNameString(),
				    Required,
				    Table );
	case ILwd::ID_ILWD:
	case ILwd::ID_EXTERNAL:
	  break;
	default:
	  return Create( *( container[0] ), Table, Required );
	}
      }
      else
      {
	std::string
	  coltype( container.getMetadata<std::string>( "coltype" ) );
	
	if ( strcmp(coltype.c_str(), "char") )
	{
	  return new DB::FieldString( Element.getNameString(),
				      Required,
				      Table );
	}
	else if ( strcmp( coltype.c_str(), "char_u" ) )
	{
	  return new DB::FieldBlob( Element.getNameString(),
				    Required,
				    Table );
	}
	else
	{
	  throw std::runtime_error("Unable to create Field from 0 size LdasContainer");
	}
      }
    }
    break;
  default:
    break;
  }
  throw std::runtime_error("Unable to create Field from LdasElement");
}

std::string DB::Field::
GenerateFieldName(void) const
{
  std::string	result( m_table.GetName( ) );

  result += "group:";
  result += m_table.GetName();
  result += ':';
  result += m_name;
  return result;
}


// Constructs a new field based on the values of the current field. The
// created field with be of the same class as the current field.
Field* Field::NewField(void) const
{
   return NewField(m_name, m_required, m_table);
}   
   
   
bool DB::Field::
IsNull(size_t Offset) const
{
  if (m_null_mask.size() && (Offset < m_null_mask.size()))
  {
    return m_null_mask[Offset];
  }
  return false;
}

void DB::Field::
SetNull( size_t Offset, bool Value )
{
  if (Value)
  {
    if (m_null_mask.size() < (Offset + 1))
    {
      m_null_mask.resize(Offset+1);
    }
    m_null_mask[Offset] = Value;
  }
  else
  {
    bool	is_all_null = true;

    if (m_null_mask.size() > Offset)
    {
      if (m_null_mask.size() > 0) m_null_mask[Offset] = Value;
      for (General::bit_vector::const_iterator i = m_null_mask.begin();
	   i != m_null_mask.end();
	   i++)
      {
	if ((*i))
	{
	  is_all_null = false;
	  break;
	}
      }
      if (is_all_null)
      {
	m_null_mask.resize(0);
      }
    }
  }
}

void DB::Field::
SetNullMask( const General::bit_vector& NullMask )
{
  m_null_mask = NullMask;
}


static DB::Field&
create_null_ref( )
{
  static FieldNull ref;

  return ref;
}
