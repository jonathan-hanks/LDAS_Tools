#ifndef DB_SUMM_SPECTRUM_HH
#define	DB_SUMM_SPECTRUM_HH

namespace DB
{
  namespace SummSpectrum
  {
    extern const char TABLE_NAME[];

    extern const char CREATOR_DB[];

    extern const char PROGRAM[];
    extern const char PROCESS_ID[];

    extern const char FRAMESET_GROUP[];
    extern const char SEGMENT_GROUP[];
    extern const char SEGMENT_VERSION[];

    extern const char START_TIME_SECONDS[];
    extern const char START_TIME_NANOSECONDS[];
    extern const char END_TIME_SECONDS[];
    extern const char END_TIME_NANOSECONDS[];

    extern const char FRAMES_USED[];

    extern const char START_FREQUENCY[];
    extern const char DELTA_FREQUENCY[];
    extern const char MIMETYPE[];

    extern const char CHANNEL[];

    extern const char TYPE[];
    extern const char SPECTRUM[];
    extern const char LENGTH[];
  }
}

#endif	/* DB_SUMM_SPECTRUM_HH */
