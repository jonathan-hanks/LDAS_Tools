#include "dbaccess/SummStatistics.hh"

namespace DB
{
  namespace SummStatistics
  {
    const char TABLE_NAME[] = "summ_statistics";

    const char CREATOR_DB[] = "creator_db";

    const char PROGRAM[] = "program";
    const char PROCESS_ID[] = "process_id";

    const char FRAMESET_GROUP[] = "frameset_group";
    const char SEGMENT_GROUP[] = "segment_group";
    const char SEGMENT_VERSION[] = "version";

    const char START_TIME_SECONDS[] = "start_time";
    const char START_TIME_NANOSECONDS[] = "start_time_ns";
    const char END_TIME_SECONDS[] = "end_time";
    const char END_TIME_NANOSECONDS[] = "end_time_ns";

    const char FRAMES_USED[] = "frames_used";

    const char SAMPLES[] = "samples";

    const char CHANNEL[] = "channel";

    const char MIN[] = "min_value";
    const char MAX[] = "max_value";
    const char MIN_DELTA[] = "min_delta";
    const char MAX_DELTA[] = "max_delta";
    const char MIN_DELTA_DELTA[] = "min_deltadelta";
    const char MAX_DELTA_DELTA[] = "max_deltadelta";
    const char MEAN[] = "mean";
    const char VARIANCE[] = "variance";
    const char RMS[] = "rms";
    const char SKEWNESS[] = "skewness";
    const char KURTOSIS[] = "kurtosis";
  }
}
