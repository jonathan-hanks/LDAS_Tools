#ifndef DB_FIELD_UNIQUE_ID_HH
#define	DB_FIELD_UNIQUE_ID_HH

#include "general/types.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "dbaccess/Field.hh"

namespace DB {
  class Table;

  //: UniqueId Field
  //
  // This class contains the definition and methods appropriate for
  // working with a database's unique id type. This requires special
  // handling since each database my have a different way of representing
  // this data type and the metadataAPI has special processing for these
  // fields.
  class FieldUniqueId: public Field
  {
  public:
    //: Constructor
    //
    //!param: std::string Name - Name of column to create
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldUniqueId(std::string Name, bool Required, const DB::Table& Table);

    //: Get ILwd representation
    //
    // This method generates an
    // ilwd representing the data of the field.
    //
    //!return: ILwd::LdasElement* - ilwd representation
    ILwd::LdasContainer* GetILwd(bool& NeedToCopy) const;

    //: Get Cell value
    //
    // This method returns the value of the data stored at the row offset
    //
    //!param: unsigned int RowOffset - Row offset of the data
    //
    //!return: std::string - value of the data
    std::string GetDataAsString(unsigned int RowOffset);

    //: Generate unique id
    //
    // This method generates a unique id that is appropriate for sending
    // to the metadataAPI.
    //
    //!param: const DB::Table& Table - Table that is being referenced
    //!param: unsigned int Id - Offset of Unique Id.
    std::string MakeUniqueId(const DB::Table& Table, unsigned int Id) const;

    //: Modify an entry.
    //
    //!param: const int Row - Row offset of entry to modify.
    //!param: const std::string& Data - Pointer to new value for field.
    //
    //!return: none
    void ModifyEntry( const int Row, const std::string& Data );

    //: Modify an entry.
    //
    //!param: const int Row - Row offset of entry to modify.
    //!param: const CHAR* Data - Pointer to new value for field.
    //
    //!return: none
    void ModifyEntry( const int Row, const CHAR* Data );

    //: Virtual constructor
    //
    //!param: std::string Name - Name for the column
    //!param: bool Required - true if the column is required to be
    //+       supplied at time of submission, false otherwise.
    //!param: const DB::Table& Table - Reference to the table in which
    //+       the column exists.
    FieldUniqueId* NewField( const std::string& Name,
			     bool Required,
			     const DB::Table& Table ) const;
    //: Append data
    //
    // Add a single element to the list of data managed by this instance.
    //
    //!param: std::string Data - Data to be appended.
    void push_back( const std::string& Id);

    //: Append data
    //
    // Add a single element to the list of data managed by this instance.
    //
    //!param: const CHAR* Data - Data to be appended.
    //!param: unsigned int Length - number of bytes composing Data.
    void push_back(const CHAR* Id, int Length);

    //: Append raw unique ids
    //
    // Add a single element to the list of data managed by this instance.
    //
    //!param: const CHAR_U* Data - Data to be appended.
    //!param: unsigned int Length - number of bytes composing Data.
    void push_back(const CHAR_U* Id, int Length);

    //: Replace data
    //
    // Replace data at the specified row with another value.
    //
    //!param: int Row - Offset for data to be replaced
    //!param: const CHAR* Data - New value
    //!param: unsigned int Length - number of bytes composing Data.
    void Replace(unsigned int Row, const CHAR* Data, unsigned int Length);

    //: Number of elements in data set.
    //
    // This method returns the number
    // of elements (rows) that are contained in the data set.
    //
    //!return: Field::size_t - Number of elements in the data set.
    Field::size_t Size(void) const;

    //: Get Nth element
    //
    // Returns the data specified by Offset.
    //
    //!param: unsigned int Offset - Offset of requested data item.
    //!return: ILwd::LdasArray<CHAR>* - Pointer to data at offset.
    const ILwd::LdasArray<CHAR>* operator[](unsigned int Offset) const;

  private:
    //: Data storage
    ILwd::LdasContainer		m_data;
  }; // class - FieldUniqueId

} // namespace - DB
#endif	/* DB_FIELD_UNIQUE_ID_HH */
