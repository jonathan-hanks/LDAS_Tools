#ifndef DBACCESS__FIELD_ARRAY_CC
#define DBACCESS__FIELD_ARRAY_CC

#include "dbaccess/config.h"

#include "CachePointer.hh"
#include "ilwd/ldasarray.hh"

#include "dbaccess/FieldArray.hh"
#include "dbaccess/Table.hh"

template < class Type > DB::Field::field_type get_type(void);

#define	GET_TYPE(a) \
template < > \
inline DB::Field::field_type \
get_type< a >(void) { return DB::Field::FIELD_ARRAY_##a; }

GET_TYPE(CHAR)
GET_TYPE(CHAR_U)
GET_TYPE(INT_2S)
GET_TYPE(INT_2U)
GET_TYPE(INT_4S)
GET_TYPE(INT_4U)
GET_TYPE(INT_8S)
GET_TYPE(INT_8U)
GET_TYPE(REAL_4)
GET_TYPE(REAL_8)
GET_TYPE(COMPLEX_8)
GET_TYPE(COMPLEX_16)

#undef GET_TYPE

template < class Type >
DB::FieldArray< Type >::
FieldArray(std::string FieldName, bool Required, const DB::Table& Table)
  : Field(FieldName, Required, get_type< Type >(), Table)
{
}

template < class Type >
void DB::FieldArray< Type >::
Append(const Type* Data, int Rows)
{
  for (int x = 0; x < Rows; x++)
  {
    m_data.push_back(*Data);
    Data++;
  }
}

template < class Type >
ILwd::LdasElement* DB::FieldArray< Type >::
GetILwd(bool& NeedToCopy) const
{
  NeedToCopy = false;
   
  return new ILwd::LdasArray< Type >( m_data.Get(), m_data.Length() );
}

template < class Type >
void DB::FieldArray< Type >::
ModifyEntry( const int Row, const Type* Data )
{
  m_data.ModifyEntry( Row, Data );
}

template < class Type >
void DB::FieldArray< Type >::
push_back(Type Data)
{
  m_data.push_back(Data);
}


template< class Type >
DB::FieldArray< Type >* DB::FieldArray< Type >::
NewField( const std::string& Name, bool Required, const DB::Table& Table ) const
{
   return new DB::FieldArray< Type >(Name, Required, Table);
}  
   
   
template< class Type >
DB::Field::size_t DB::FieldArray< Type >::
Size(void) const
{
   return m_data.Length();
}
   

#define	INSTANTIATE(Type) \
template class DB::FieldArray< Type >

INSTANTIATE(CHAR);
INSTANTIATE(CHAR_U);
INSTANTIATE(INT_2S);
INSTANTIATE(INT_2U);
INSTANTIATE(INT_4S);
INSTANTIATE(INT_4U);
INSTANTIATE(INT_8S);
INSTANTIATE(INT_8U);
INSTANTIATE(REAL_4);
INSTANTIATE(REAL_8);
INSTANTIATE(COMPLEX_8);
INSTANTIATE(COMPLEX_16);

#undef INSTANTIATE

#endif /* DBACCESS__FIELD_ARRAY_CC */
