#include "dbaccess/config.h"

#include "dbaccess/FieldString.hh"
#include "dbaccess/Table.hh"

DB::FieldString::
FieldString(const std::string& FieldName, bool Required, const DB::Table& Table)
  : Field(FieldName, Required, DB::Field::FIELD_STRING, Table),
    m_data((const char**)NULL, 0)
{
}

ILwd::LdasString* DB::FieldString::
GetILwd(bool& NeedToCopy) const
{
  ILwd::LdasString* retval = const_cast<ILwd::LdasString*>(&m_data);

  NeedToCopy = true;
  return retval;
}

void DB::FieldString::
ModifyEntry( const int Row, const std::string& Data )
{
  if ( ( Row >= (int)m_data.size() ) || ( Row < 0 ) )
  {
    throw std::range_error("Row to modify is outside array bounds");
  }
  m_data[Row] = Data;
}

void DB::FieldString::
ModifyEntry( const int Row, const CHAR* Data )
{
  if ( ( Row >= (int)m_data.size() ) || ( Row < 0 ) )
  {
    throw std::range_error("Row to modify is outside array bounds");
  }
  m_data[Row] = std::string(Data, strlen(Data) );
}

DB::FieldString* DB::FieldString::
NewField( const std::string& Name, bool Required, const DB::Table& Table ) const
{
   return new DB::FieldString(Name, Required, Table);
}   
   
void DB::FieldString::
push_back(std::string Data)
{
  m_data.push_back(Data);
}

void DB::FieldString::
push_back(const CHAR* Data, int Length)
{
  m_data.push_back(std::string(Data, Length));
}

std::string DB::FieldString::
operator[](unsigned int Offset) const
{
  return m_data[Offset];
}

   
DB::Field::size_t DB::FieldString::Size(void) const
{
   return m_data.size();
}   
