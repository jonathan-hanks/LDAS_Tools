#include "dbaccess/config.h"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"

#include "dbaccess/FieldBlob.hh"
#include "dbaccess/Table.hh"

DB::FieldBlob::
FieldBlob(const std::string& Name, bool Required, const DB::Table& Table)
  : Field(Name, Required, FIELD_BLOB, Table)
{
}

ILwd::LdasContainer* DB::FieldBlob::
GetILwd(bool& NeedToCopy) const
{
  NeedToCopy = true;
  return const_cast<ILwd::LdasContainer*>(&m_data);
}

void DB::FieldBlob::
ModifyEntry( const int Row, const CHAR_U* Data, const int Length )
{
  if ( Length < 0 )
  {
    throw std::runtime_error("BLOB data supplied without a length");
  }
  if ( ( Row >= (int)m_data.size() ) || ( Row < 0 ) )
  {
    throw std::range_error("Row to modify is outside array bounds");
  }
  delete m_data[Row];
  m_data[Row] = new ILwd::LdasArray<CHAR_U>(Data, Length);
}

void DB::FieldBlob::
push_back(const CHAR_U* Data, unsigned int Length)
{
  m_data.push_back( new ILwd::LdasArray<CHAR_U>( Data, Length ),
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
}

void DB::FieldBlob::
Replace(unsigned int Row, const CHAR_U* Data, unsigned int Length)
{
  ILwd::LdasContainer::iterator pos( m_data.begin() );
  if( Length >= m_data.size() )
  {
     pos = m_data.end();
  }
  else
  {
     std::advance( pos, Length );
  }
   

  if (pos != m_data.end())
  {
     // Use iterator to the inserted element to erase old entry
     ILwd::LdasContainer::iterator iter( 
        m_data.insert( pos, new ILwd::LdasArray<CHAR_U>(Data, Length),
                       ILwd::LdasContainer::NO_ALLOCATE,
                       ILwd::LdasContainer::OWN ) );
   
     // Don't need to check for != end(): it's guaranteed
     ++iter;
   
     m_data.erase(iter);
  }
   
  return;
}


DB::Field::size_t DB::FieldBlob::Size(void) const
{
   return m_data.size();
}   
   
   
const ILwd::LdasArray<CHAR_U>* DB::FieldBlob::
operator[](unsigned int Offset) const
{
  return dynamic_cast<ILwd::LdasArray<CHAR_U>*>(m_data[Offset]);
}


DB::FieldBlob* DB::FieldBlob::
NewField( const std::string& Name, bool Required, const DB::Table& Table ) const
{
   return new DB::FieldBlob(Name, Required, Table);
}   
