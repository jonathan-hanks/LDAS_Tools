/* -*- mode: c++ -*- */
/*----------------------------------------------------------------------*\
 Scanner for DB2 Create Table command. The choice for the class
 interface was to allow the code to be reentrant and to support
 multiple scanners in a single executable.
\*----------------------------------------------------------------------*/
/* %option debug */
%option c++
%option yyclass="DB::DB2SyntaxLexer"
%option outfile="DB2SyntaxLexer.cc"
%option noyywrap
%option nounput
%option batch
%option never-interactive
%option nostdinit

%{
#include "config.h"

#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "general/unordered_map.hh"

#include "DB2SyntaxLexer.hh"
#include "DB2SyntaxDriver.hh"
#include "DB2SyntaxParser.hh"

   typedef DB::DB2SyntaxParser::token token;

#define yyterminate() return token::TKN_END

   using std::exit;
   using std::atoi;
   using std::malloc;
   using std::realloc;
   using std::free;
%}

whitespace [ \t]
letter [a-zA-Z]
number [0-9]
comment [-][-]

%%

[\n]			/* Eat end of line char */;
{whitespace}+		/* Eat white space */;
{comment}.*		/* Eat one line comments */;
"\\"$			/* Eat line continuation escapes */;
"<="			return token::TKN_LE;
">="			return token::TKN_GE;
"<>"			return token::TKN_NE;
"'"[^\']*"'"		m_yylval->sval = new std::string( YYText( ) ); return token::TKN_CONSTANT_STRING;
{number}+		m_yylval->ival = atoi( YYText( ) ); return token::TKN_CONSTANT_INTEGER;
[A-Za-z][A-Za-z_0-9]*	return KeywordLookup( YYText( ) );
[(),;*=<>]		return DB::DB2SyntaxParser::token_type( *(YYText( ) ) );
.			;

%%
namespace
{
  DB::DB2SyntaxLexer::keywords_type& gen_keywords( );
}

namespace DB
{
  DB2SyntaxLexer::keywords_type& DB2SyntaxLexer::m_keywords = gen_keywords();

  DB2SyntaxLexer::token_type DB2SyntaxLexer::
  KeywordLookup( const char* Word ) const
  {
    typedef DB::DB2SyntaxParser::token token;

    keywords_type::const_iterator	pos( m_keywords.find( Word ) );

    if ( pos != m_keywords.end( ) )
    {
      return (*pos).second;
    }
    m_yylval->sval = new std::string( Word );
    return token::TKN_IDENTIFIER;
  }

}

namespace
{
  DB::DB2SyntaxLexer::keywords_type&
  gen_keywords(void)
  {
    static DB::DB2SyntaxLexer::keywords_type	keywords_map;

#define MAPIT(a) keywords_map[#a] = token::TKN_##a
    MAPIT(ACTION);
    MAPIT(ALL);
    MAPIT(ALWAYS);
    MAPIT(AND);
    MAPIT(AS);
    MAPIT(ASC);
    MAPIT(BIGINT);
    MAPIT(BIT);
    MAPIT(BLOB);
    MAPIT(BY);
    MAPIT(CACHE);
    MAPIT(CAPTURE);
    MAPIT(CASCADE);
    MAPIT(CHANGES);
    MAPIT(CHAR);
    MAPIT(CHARACTER);
    MAPIT(CHECK);
    MAPIT(CLOB);
    MAPIT(COMPACT);
    MAPIT(CONSTRAINT);
    MAPIT(CREATE);
    MAPIT(CURRENT);
    MAPIT(DATA);
    MAPIT(DBCLOB);
    MAPIT(DEFAULT);
    MAPIT(DELETE);
    MAPIT(DESC);
    MAPIT(DOUBLE);
    MAPIT(ECHO);
    MAPIT(FOR);
    MAPIT(FOREIGN);
    MAPIT(GENERATED);
    MAPIT(GRANT);
    MAPIT(HASHING);
    MAPIT(IDENTITY);
    MAPIT(INCREMENT);
    MAPIT(INDEX);
    MAPIT(INITIALLY);
    MAPIT(INT);
    MAPIT(INTEGER);
    MAPIT(KEY);
    MAPIT(LOGGED);
    MAPIT(LONG);
    MAPIT(NO);
    MAPIT(NONE);
    MAPIT(NOT);
    MAPIT(NULL);
    MAPIT(ON);
    MAPIT(OR);
    MAPIT(PARTITIONING);
    MAPIT(PRECISION);
    MAPIT(PRIMARY);
    MAPIT(PUBLIC);
    MAPIT(REAL);
    MAPIT(REFERENCES);
    MAPIT(REPLICATED);
    MAPIT(RESTRICT);
    MAPIT(REVOKE);
    MAPIT(SELECT);
    MAPIT(SET);
    MAPIT(SMALLINT);
    MAPIT(START);
    MAPIT(SUMMARY);
    MAPIT(TABLE);
    MAPIT(TIMESTAMP);
    MAPIT(UNIQUE);
    MAPIT(UPDATE);
    MAPIT(USING);
    MAPIT(VARCHAR);
    MAPIT(VARYING);
    MAPIT(WITH);
#undef MAPIT

    //-------------------------------------------------------------------
    // Give the caller the newly created map
    //-------------------------------------------------------------------
    return keywords_map;
  }
} // namespace - anonymous
