#include "ilwd/config.h"

#include "../attributevalue.hh"
#include <general/test.hh>

using Lwd::AttributeValue;

bool test1()
{
    OUT << "default constructor... ";
    // test default constructor
    AttributeValue v;
    if ( v.getDelimiter() == '\'' )
    {
        if ( v.getString().size() == 0 )
        {
            return true;
        }
    }
    return false;
}

bool test2()
{
    OUT << "template constructor... ";
    // test template constructor
    AttributeValue v1( float(1.12345678901234567890) );
    AttributeValue v2( double(1.12345678901234567890), '\'' );
    AttributeValue v3( int(1025), '"' );
    AttributeValue v4( "\"c-string\"" );
    AttributeValue v5( string( "'string'" ), '"' );
    AttributeValue v6( 'x' );

    TEST( (v1.getDelimiter() == '\'') );
    TEST( (v2.getDelimiter() == '\'') );
    TEST( (v3.getDelimiter() == '"') );

    TEST( (v1.getString() == "1.123457" ) );
    TEST( (v2.getString() == "1.12345678901235" ) );
    TEST( (v3.getString() == "1025" ) );
    TEST( v4.getString() == "\"c-string\"" );
    TEST( v5.getString() == "'string'" );
    TEST( v6.getString() == "x" );

    try
    {
        AttributeValue v7( "illegal<character" );
        return false;
    }
    catch( FormatException& e )
    {
    }

    try
    {
        AttributeValue v7( "illegal&character" );
        return false;
    }
    catch( FormatException& e )
    {
    }

    try
    {
        AttributeValue v7( "illegal'character" );
        return false;
    }
    catch( FormatException& e )
    {
    }

    try
    {
        AttributeValue v7( "illegal\"character", '"' );
        return false;
    }
    catch( FormatException& e )
    {
    }

    
    return true;
}

bool test3()
{
    OUT << "setDelimiter/setString... ";

    AttributeValue v;

    v.setDelimiter( '"' );
    TEST( v.getDelimiter() == '"' );
    v.setDelimiter( '\'' );
    TEST( v.getDelimiter() == '\'' );

    try
    {
        v.setString( "a'b" );
        return false;
    }
    catch( FormatException& e )
    {
    }

    v.setString( "a\"b" );

    try
    {
        v.setDelimiter( '"' );
        return false;
    }
    catch( FormatException& e )
    {
    }

    try
    {
        v.setDelimiter( ':' );
        return false;
    }
    catch( FormatException& e )
    {
    }

    v.setString( "tmp" );
    v.setDelimiter( '"' );
    v.setString( "a'b" );

    try
    {
        v.setDelimiter( '\'' );
        return false;
    }
    catch( FormatException& e )
    {
    }

    try
    {
        setAttributeValue( v, "a\"b" );
        return false;
    }
    catch( FormatException& e )
    {
    }
    
    return true;
}

bool test4()
{
    OUT << "getValue... ";

    AttributeValue v1( "1.234" );
    TEST( v1.template getValue< float >() == 1.234f );
    AttributeValue v2( "1.2345678901" );
    TEST( v2.template getValue< double >() == 1.2345678901 );
    AttributeValue v3( "-1234" );
    TEST( v3.template getValue< int >() == -1234 );
    AttributeValue v4( string( "TEST" ) );
    
    try
    {
        v4.template getValue< unsigned int >();
        return false;
    }
    catch( FormatException& e )
    {
    }

    return true;
}

bool test5()
{
    OUT << "Stream Insertion/Extraction/Construction... ";
    OUT << "NOT IMPLEMENTED ";
    return true;
}

main()
{
    OUT << "Testing AttributeValue:\n";
    
    bool r = true;
    
    RUN( r, test1 );
    RUN( r, test2 );
    RUN( r, test3 );
    RUN( r, test4 );
    RUN( r, test5 );

    return !r;
}

    


    

    
