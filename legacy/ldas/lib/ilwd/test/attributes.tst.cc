#include "ilwd/config.h"

#include <sstream>
   
#include <general/test.hh>
#include "../attributes.hh"

using Lwd::Attributes;

bool test1()
try
{
    OUT << "Stream insertion/extraction... ";
    STARTTEST;
    
    // Check the write method
    {
        Attributes a;
        a.addAttribute( "a1", 100 );
        a.addAttribute( "a2", "test" );
        a.addAttribute( "a3", 3.45 );
        ostringstream s;
        a.write( s );
        
        ETEST( strcmp( s.str(), "a1='100' a2='test' a3='3.45'" ) == 0 );
    }

    // check the read method
    {
        Attributes a;

        // Add something to the attributes so we can make sure it isn't there
        // when we read the new stream into the object.
        a.addAttribute( "testvalue", 'v' );

        // Initialize a stream and read it
        stringstream s;
        s << "a1='100'      \t a2=\"test\" \n\n  a3='3.45'\ta4='final'";
        try
        {
            a.read( s );
        }
        catch( FormatException& e )
        {
            cout << e.getMessage() << endl;
        }
        
        // Make sure the old attribute isn't there
        ETEST( a.size() == 4 );
        ETEST( a.find( "testvalue" ) == a.end() );

        // check the new attributes
        ETEST( a.getAttribute( "a1" )->template getValue< int >() == 100 );
        ETEST( a.getAttribute( "a2" )->template getValue< string >()
               == "test" );
        ETEST( a.getAttribute( "a3" )->template getValue< float >() - 3.45
              < 1e-6 );
        ETEST( a.getAttribute( "a4" )->getString() == "final" );
        ETEST( a.getAttribute( "a1" )->getDelimiter() == '\'' );
        ETEST( a.getAttribute( "a2" )->getDelimiter() == '"' );
    }
    
    return true;
}
catch( int& t )
{
    OUT << "#" << t << " ";
    return false;
}

bool test2()
try
{
    OUT << "Assignment operator... ";
    STARTTEST;
    
    Attributes* a = new Attributes();
    a->addAttribute( "a1", 1.2 );
    a->addAttribute( "a2", "troff" );
    a->addAttribute( "a3", -1 );

    Attributes b;
    b = *a;

    delete a;

    ETEST( b.getAttribute( "a1" )->getString() == "1.2" );
    ETEST( b.getAttribute( "a2" )->getString() == "troff" );
    ETEST( b.getAttribute( "a3" )->getString() == "-1" );

    return true;
}
catch( int& t )
{
    cout << "#" << t << " ";
    return false;
}

    

main()
{
    OUT << "Testing Attributes:\n";

    bool r = true;
    RUN( r, test1 );
    RUN( r, test2 );
    
    return !r;
}

    

    
