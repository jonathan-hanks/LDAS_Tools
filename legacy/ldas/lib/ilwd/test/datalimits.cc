#include "ilwd/config.h"

#include <iostream>
#include <memory>
#include <sstream>   
   
// Test Header Files
#include "general/unittest.h"   

// ILWD Header Files   
#include "ldascontainer.hh"
#include "style.hh"
#include "util.hh"
   
// General Header Files   
#include <general/util.hh>   
   
using ILwd::LdasContainer;   
using ILwd::Reader;
   
using namespace std;   
   
General::UnitTest limitsTest;
   

static const char underflow_real4[] =
"<ilwd name='test0:cont0' size='1'>\n"
"    <real_4 dims='4' name='Column:XML'>1.0e-40 1036 1037 1038</real_4>\n"
"</ilwd>";
   
static const char overflow_real4[] =
"<ilwd name='test0:cont0' size='1'>\n"
"    <real_4 dims='4' name='Column:XML'>1.0e+40 1036 1037 1038</real_4>\n"
"</ilwd>";
   
static const char underflow_real8[] =
"<ilwd name='test0:cont0' size='1'>\n"
"    <real_8 dims='4' name='Column:XML'>1.0e-309 1036 1037 1038</real_8>\n"
"</ilwd>";   

static const char overflow_real8[] =
"<ilwd name='test0:cont0' size='1'>\n"
"    <real_8 dims='4' name='Column:XML'>1.0e+309 1036 1037 1038</real_8>\n"
"</ilwd>";   
   
   
int main( int argc, char** argv )
try   
{
   // Initialize test
   limitsTest.Init( argc, argv );
   
   std::auto_ptr< LdasContainer> c;
   std::auto_ptr< Reader > r;

   // Test real_4 underflow
   istringstream underflow4( underflow_real4 );   
   r.reset( new Reader( underflow4 ) );   
   try
   {
      c.reset( new LdasContainer( *r ) );
   }
   catch( LdasException& exc )
   {
      std::string msg( exc[ 0 ].getMessage() + ": " + exc[ 0 ].getInfo() ),
             expected_msg( "out_of_range: 1.0e-40" );
      
      limitsTest.Check( msg.substr( 0, expected_msg.size() ) == expected_msg )
	<< msg
	<< endl;
   }
   
   // Test real_4 overflow
   istringstream overflow4( overflow_real4 );   
   r.reset( new Reader( overflow4 ) );
   try
   {
      c.reset( new LdasContainer( *r ) );
   }
   catch( LdasException& exc )
   {
      std::string msg( exc[ 0 ].getMessage() + ": " + exc[ 0 ].getInfo() ),
             expected_msg( "out_of_range: 1.0e+40" );
      
      limitsTest.Check( msg.substr( 0, expected_msg.size() ) == expected_msg )
	<< msg
	<< endl;
   }
   
   // Test real_8 underflow
   istringstream underflow8( underflow_real8 );   
   r.reset( new Reader( underflow8 ) );
   try
   {
      c.reset( new LdasContainer( *r ) );
   }
   catch( LdasException& exc )
   {
      std::string msg( exc[ 0 ].getMessage() + ": " + exc[ 0 ].getInfo() ),
             expected_msg( "out_of_range: 1.0e-309" );
      
      limitsTest.Check( msg.substr( 0, expected_msg.size() ) == expected_msg )
	<< msg
	<< endl;
   }   
   
   // Test real_8 overflow
   istringstream overflow8( overflow_real8 );   
   r.reset( new Reader( overflow8 ) );
   try
   {
      c.reset( new LdasContainer( *r ) );
   }
   catch( LdasException& exc )
   {
      std::string msg( exc[ 0 ].getMessage() + ": " + exc[ 0 ].getInfo() ),
             expected_msg( "out_of_range: 1.0e+309" );
      
      limitsTest.Check( msg.substr( 0, expected_msg.size() ) == expected_msg )
	<< msg
	<< endl;
   }   
   r.reset( (Reader*)NULL );
   limitsTest.Exit();

}
catch( std::bad_alloc& )
{
   limitsTest.Check( false ) << "memory allocation failed"
                                << endl << flush;
   limitsTest.Exit();   
}
catch( ... )
{
   limitsTest.Check( false ) << "unknown exception"
                                << endl << flush;
   limitsTest.Exit();   
}

   
