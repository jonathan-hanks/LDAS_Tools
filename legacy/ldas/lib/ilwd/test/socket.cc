#include "ilwd/config.h"

#include <stdio.h>
#include <unistd.h>
#include <stream.h>

#include <general/util.hh>

#include "test.hh"

//=======================================================================
// Global variables
//=======================================================================

const char* Port;

//=======================================================================
// Forward declaration of functions
//=======================================================================

void client(void);
void server(void);
bool start_server(void);

int
main(int argc, const char** argv)
{
  bool is_server(false);

  for (int x = 1; x < argc; x++)
  {
    if (strcmp(argv[x], "--server") == 0)
    {
      is_server = true;
    }
    else if (strcmp(argv[x], "--port") == 0)
    {
      Port = argv[++x];
    }
  }
  if (is_server)
  {
    server();
  }
  else
  {
    client();
  }
}

void
client()
{
  TSTART();

  TEST(start_server());
  TMSG("start_server");

  TEXIT();
}

void
server()
{
}

bool
start_server()
{
  bool	ret(true);

  return ret;
}
