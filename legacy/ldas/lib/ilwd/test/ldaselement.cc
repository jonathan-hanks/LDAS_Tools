#include "ilwd/config.h"

#include <cstring>
#include <stdlib.h>
#include <unistd.h>

#include <iostream>
#include <memory>
#include <sstream>

#ifdef HAVE_LIBOSPACE
#include "general/undef.h"
#include <ospace/file/file.h>
#include <ospace/stream/bstream.h>
#endif // HAVE_LIBOSPACE

#include "general/LDASUnexpected.hh"
#include "general/Memory.hh"
#include "general/util.hh"
#include "general/unittest.h"

#include "ldaselement.hh"
#include "ldasformatelement.hh"
#include "ldasarray.hh"
#include "ldasstring.hh"
#include "ldascontainer.hh"
#include "ldasexternal.hh"
#include "reader.hh"

using namespace std;

using NAMESPACE_GENERAL_MEMORY :: unique_ptr;
   
#define	FULL 1
#define OSPACE_BINARY_FILE_TEST 1

General::UnitTest	Test;

std::string NULL_STRING;

namespace
{
  std::string
  FormatType( ILwd::Format F )
  {
    using namespace ILwd;

#define CASE( x ) case x: return #x 
    switch( F )
    {
      CASE(BINARY);
      CASE(BASE64);
      CASE(ASCII);
      CASE(USER_FORMAT);
    }
#undef CASE
    return "Unknown Format";
  } // FormatType

  std::string
  CompressionType( ILwd::Compression C )
  {
    using namespace ILwd;

#define CASE( x ) case x: return #x 
    switch( C )
    {
      CASE(NO_COMPRESSION);
      CASE(GZIP);
      CASE(GZIP0);
      CASE(GZIP1);
      CASE(GZIP2);
      CASE(GZIP3);
      CASE(GZIP4);
      CASE(GZIP5);
      CASE(GZIP6);
      CASE(GZIP7);
      CASE(GZIP8);
      CASE(GZIP9);
      CASE(USER_COMPRESSION);
    }
#undef CASE
    return "Unknown Compression";
  } // CompressionType
} // namespace - anonymous

template<class T>
void
native_test(const T* Input, ILwd::LdasArrayBase::size_type InputSize,
	    const char* Result, ILwd::LdasArrayBase::size_type ResultSize);

void
check(const std::string& TestName,
      const ILwd::LdasElement& Element,
      const char* Result, ILwd::LdasArrayBase::size_type ResultSize)
{
  ostringstream 	os;
  ostringstream		ros;

  unique_ptr< ILwd::LdasElement>	elem;
  try {
    Element.write(os, ILwd::ASCII);

    istringstream 			is( Result );
    ILwd::Reader			r( is );

    elem.reset( ILwd::LdasElement::createElement( r ) );
    elem->write( ros, ILwd::ASCII );
  }
  catch(const LdasException& e)
  {
    for (ILwd::LdasArrayBase::size_type x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << TestName << ": write error: "
			<< e[x].getMessage()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << TestName << ": Caught error on writing" << endl;
    return;
  }

#if 0
  Test.Check(bcmp(ros.str( ).c_str( ), os.str().c_str(), ResultSize) == 0)
    << TestName << ": " << ResultSize << " (" << os.str().size() << ") "
    << os.str() << " =?= " << ros.str( ) << endl;
#else
  Test.Check( *elem == Element )
    << TestName << ": " << ResultSize << " (" << os.str().size() << ") "
    << os.str() << " =?= " << ros.str( ) << endl;
#endif

  return;
}

template<class T>
void
char_test( const std::string& Input,
	   const std::string& Result = NULL_STRING )
{
  istringstream	is( Input );
  ILwd::Reader 	reader(is);
  T	       	st;
  const std::string	result( ( &Result == &NULL_STRING ) ? Input : Result );
  

  try {
    st.read(reader);
  }
  catch(const LdasException& e)
  {
    for (ILwd::LdasArrayBase::size_type x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << "char_test: read error: "
			<< e[x].getMessage() 
                        << " info: "
                        << e[x].getInfo()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << "char_test: Caught error on reading" << endl;
    return;
  }
  check( "char_test", st, result.c_str(), result.length() + 1 );
}

template<class T>
void
assignment_test( const std::string& Input,
	   const std::string& Result = NULL_STRING )
{
  istringstream	is( Input );
  ILwd::Reader 	reader(is);
  T	       	st;
  T		st2;
  const std::string	result( ( &Result == &NULL_STRING ) ? Input : Result );
  

  try {
    st.read(reader);
    st2 = st;
  }
  catch(const LdasException& e)
  {
    for (ILwd::LdasArrayBase::size_type x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << "assignment_test: read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << "assignment_test: Caught error on reading" << endl;
    return;
  }
  check( "assignment_test", st2, result.c_str(), result.length() + 1 );
   
  return;
}

template<class T>
void
copy_constructor_test( const std::string& Input,
	   const std::string& Result = NULL_STRING )
{
  istringstream	is( Input );
  ILwd::Reader 	reader(is);
  T	       	st;
  const std::string	result( ( &Result == &NULL_STRING ) ? Input : Result );
  

  try {
    st.read(reader);
  }
  catch(const LdasException& e)
  {
    for (ILwd::LdasArrayBase::size_type x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << "copy_constructor_test: read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << "copy_constructor_test: Caught error on reading" << endl;
    return;
  }
  T st2(st);
  check( "copy_constructor_test", st2, result.c_str(), result.length() + 1 );
   
  return;
}

template<class T>
void
os_char_test( const std::string& Input,
	      const std::string& Result = NULL_STRING )
{
#if HAVE_LIBOSPACE
#if OSPACE_BINARY_FILE_TEST
  istringstream	is( Input );
  ILwd::Reader 	reader(is);
  T	       	st;
  bool		active = true;
  const std::string	result( ( &Result == &NULL_STRING ) ? Input : Result );

  try {
    st.read(reader);
  }
  catch(const LdasException& e)
  {
    for (ILwd::LdasArrayBase::size_type x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << "os_char_test: read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< endl;
      continue;
    }
  }
  catch(...)
  {
    Test.Check(false) << "os_char_test: Caught error on reading"
		      << endl;
    return;
  }
  //---------------------------------------------------------------------
  // Write the contents out to a file using Object Space
  //---------------------------------------------------------------------
  static const char	*filename = "__f__";
  os_file	f;
  T		rd;
  try
  {
    f.open(filename, O_OPEN | O_CREAT, O_RDWR, 0666);
    if (f.ok())
    {
      os_bstream	stream( f );
      stream << st;
      f.seek(0, 0L );
      stream >> rd;
      f.close();
    }
  }
  catch( const std::exception& Exception )
  {
    Test.Check(false)
      << "os_char_test: Caught error on writing stream to file: "
      << Exception.what( )
      << endl;
    active = false;
  }
  catch( ... )
  {
    Test.Check(false) << "os_char_test: Caught error on writing stream to file."
		      << endl;
    active = false;
  }
  (void)unlink(filename);
  //---------------------------------------------------------------------
  // Read the contents back from the file using Object Space
  //---------------------------------------------------------------------
  if ( active )
  {
    check("os_char_test", rd, result.c_str(), result.length() + 1);
  }
   
  return;
#endif /* OSPACE_BINARY_FILE_TEST */
#endif /* HAVE_LIBOSPACE */
}

template<class InputType, class LdasType>
void
native_test(const InputType* Input, ILwd::LdasArrayBase::size_type InputSize,
	    const char* Result, ILwd::LdasArrayBase::size_type ResultSize)
{
  LdasType in(Input, InputSize, "native test");

  check("native_test", in, Result, ResultSize);
   
  return;
}

//-----------------------------------------------------------------------
// Test data conversion and sending data through Object Space sockets.
//-----------------------------------------------------------------------
template<class T>
void
all_char_test( const std::string& Input,
	       const std::string& Result = NULL_STRING )
{
  char_test<T>( Input, Result );
#if HAVE_LIBOSPACE
  os_char_test<T>( Input, Result );
#endif /* HAVE_LIBOSPACE */
  assignment_test<T>( Input, Result );
  copy_constructor_test<T>( Input, Result );
   
  return;
}

//-----------------------------------------------------------------------
// Test arrays of zero length
//-----------------------------------------------------------------------
void
array_zero()
{
  std::string	test_name;
  //---------------------------------------------------------------------
  // Test with ndims = 1, dims = 0
  //---------------------------------------------------------------------

  const std::string t1 =
    "<char ndim='1' dims='0' name='character array'></char>";

  const std::string c1 =
    "<char dims='0' name='character array'></char>";

  char_test< ILwd::LdasArray<CHAR> >( t1, c1 );

  //---------------------------------------------------------------------
  // Test with ndim = 0
  //---------------------------------------------------------------------
  const std::string t2 =
    "<char name='character array' ndim='0'></char>";

  char_test< ILwd::LdasArray<CHAR> >( t2 );

  //---------------------------------------------------------------------
  // Test with ndims = 1, dims=0 and data NULL - ok
  //---------------------------------------------------------------------

  ILwd::LdasArray<INT_2S> e3((INT_2S*)NULL, 0, (const ILwd::LdasArrayBase::size_type*)NULL,
			     "array_zero_e3");
  const char c3[] = "<int_2s name='array_zero_e3' ndim='0'></int_2s>";
  
  check("array_zero - 3", e3, c3, sizeof(c3));

  //---------------------------------------------------------------------
  // Test with dims=0 and data NULL - ok
  //---------------------------------------------------------------------

  ILwd::LdasArray<INT_2S> e4((INT_2S*)NULL, 0, "array_zero_e4");
  const char c4[] = "<int_2s dims='0' name='array_zero_e4'></int_2s>";
  
  check("array_zero - 4", e4, c4, sizeof(c4));

  //---------------------------------------------------------------------
  // Test with ndims = 0 and data non-NULL - exception
  //---------------------------------------------------------------------

  test_name = "array_zero_ex1";
  try {
    INT_4S data[] = { 2 };
    ILwd::LdasArrayBase::size_type sizes[] = { 1 };
    ILwd::LdasArray<INT_4S> e(data, 0, sizes, test_name);

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

  //---------------------------------------------------------------------
  // Test with dims = 0
  //---------------------------------------------------------------------

  test_name = "array_zero_ex2";
  try {
    INT_4S data[] = { 2 };
    ILwd::LdasArray<INT_4S> e(data, 0, test_name, "");

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(const FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

  //---------------------------------------------------------------------
  // Test with ndims = 1, dims=1 and data NULL - exception
  //---------------------------------------------------------------------

  test_name = "array_zero_ex3";
  try {
    ILwd::LdasArrayBase::size_type sizes[] = { 1 };
    ILwd::LdasArray<INT_4S> e((INT_4S*)NULL, 1, sizes, test_name);

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(const FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

  //---------------------------------------------------------------------
  // Test with ndim=1 dims=1 data NULL - exception
  //---------------------------------------------------------------------

  test_name = "array_zero_ex4";
  try {
    ILwd::LdasArray<INT_4S> e((INT_4S*)NULL, 1, test_name);

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(const FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

  //---------------------------------------------------------------------
  // Test with ndim=0 dims=non-NULL data NULL - exception
  //---------------------------------------------------------------------

  test_name = "array_zero_ex5";
  try {
    ILwd::LdasArrayBase::size_type sizes[] = { 1 };
    ILwd::LdasArray<INT_4S> e((INT_4S*)NULL, 0, sizes, test_name);

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(const FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

  //---------------------------------------------------------------------
  // Test with ndim=1 dims=1 data NULL - exception
  //---------------------------------------------------------------------

  test_name = "array_zero_ex6";
  try {
    ILwd::LdasArray<INT_4S> e((INT_4S*)NULL, 1, test_name);

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(const FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

  //---------------------------------------------------------------------
  // Test with ndim=1 dims=0 data non-NULL - exception
  //---------------------------------------------------------------------

  test_name = "array_zero_ex7";
  try {
    INT_4S data[] = { 4 };
    ILwd::LdasArray<INT_4S> e(data, 0, test_name);

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(const FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

  //---------------------------------------------------------------------
  // Test with ndim=1 dims=0 data non-NULL - exception
  //---------------------------------------------------------------------

  test_name = "array_zero_ex8";
  try {
    INT_4S data[] = { 4 };
    ILwd::LdasArrayBase::size_type sizes[] = { 0 };
    ILwd::LdasArray<INT_4S> e(data, 1, sizes, test_name);

    Test.Check(false) << test_name << ": Failed to generate exception" << endl;
  }
  catch(const FormatException& e)
  {
    Test.Check(true) << test_name << ": Caught exception" << endl;
  }
  catch(...)
  {
    Test.Check(false) << test_name << ": Caught wrong exception" << endl;
  }

}

void
array_char()
{
  istringstream	is("<char dims='13' name='character array'>\\074hello\\377world\\076</char>");
  istringstream	uis("<char_u dims='13' name='character array'>\\074hello\\377world\\076</char_u>");

  ILwd::Reader	reader(is);
  
  ILwd::LdasArray<CHAR> ca("help", 4, "character array");
   
  Test.Message() << "CHAR: ASCII:  " << ca << endl;
  ca.setWriteFormat(ILwd::BINARY);
  Test.Message() << "CHAR: BINARY: ";
  ca.write(Test.Message(false), ILwd::BINARY);
  Test.Message(false) << endl;

   
  try
  {
     ca.read(reader);
  }
  catch( std::exception& exc )
  {
     Test.Check(false) << "array_char: caught exception: "
                       << exc.what() << endl;
  }
   
   
  Test.Message() << "CHAR: from stringstream: ";
  ca.write(Test.Message(false), ILwd::ASCII);
  Test.Message(false) << endl;

  ILwd::Reader	ureader(uis);

  ILwd::LdasArray<CHAR_U> uca;   
   
  uca.read(ureader);
  Test.Message() << "CHAR_U: from stringstream: ";
  uca.write(Test.Message(false), ILwd::ASCII);
  Test.Message(false) << endl;
   
  return;
}

void
array_int_with_nulls()
{
  INT_4S data[4] = { -1, -2, -3, -4};
  unsigned int	size(sizeof(data)/sizeof(*data));

  ILwd::LdasArray<INT_4S> a(data, size, "4 byte int array");
  ILwd::LdasArray<INT_4S> b;

  a.setNull(2, true);
  Test.Message() << "INT_4S: 3rd elment is null" << a << endl;

  b = a;
  Test.Message() << "b =?= a: 3rd element is null" << b << endl;

  a.setNull(2, false);
  Test.Message() << "INT_4S: no nulls" << a << endl;

  b = a;
  Test.Message() << "b =?= a: no nulls" << b << endl;

  a.setNull("o");
  Test.Message() << "INT_4S: 1st element is null" << a << endl;
}

void
int_array_assign()
{
  INT_4S data1[] = {-4, -3, -2, -1};

  ILwd::LdasArray<INT_4S>	a1(data1, sizeof(data1)/sizeof(*data1), "a1");
  ILwd::LdasArray<INT_4S>	a2 = a1;

  Test.Check(a2 == a1) << "int_array_assign" << endl;
}

void
binary_lstring()
{
  std::string lstring_data = "<lstring format='binary' name='SEARCH' size='8'>\001\002\003\004\005\006\007\010</lstring>";

  std::string ilwd_data =
    "<ilwd name='ligo:ldas:file'>"
    "    <ilwd comment='SQL=SELECT * FROM SNGL_INSPIRAL WHERE ((process_id, creator_db) in (select process_id, creator_db from process where (jobid = 2015))) ORDER BY end_time FETCH FIRST 100 ROWS ONLY for read only' name='ldasgroup:result_table:table'>"
    "        <lstring format='binary' name='SEARCH' size='8'>\001\002\003\004\005\006\007\010</lstring>"
    "    </ilwd>"
    "</ilwd>";


  std::istringstream	is( lstring_data );
  ILwd::Reader 		reader(is);
  ILwd::LdasString	st;
  const std::string	test( "binary_lstring");
  

  try {
    st.read(reader);
  }
  catch( const FormatException& e )
  {
    Test.Check( true ) << test << ": Caught expected exception"
		       << std::endl;
    return;
  }
  catch(const LdasException& e)
  {
    for (ILwd::LdasArrayBase::size_type x = 0; x < e.getSize(); x++)
    {
      Test.Check(false) << test << ": read error: "
			<< e[x].getMessage()
			<< " " << e[x].getFile()
			<< " " << e[x].getLine()
			<< endl;
      continue;
    }
    return;
  }
  catch(...)
  {
    Test.Check(false) << test << ": Caught error on reading" << endl;
    return;
  }
  Test.Check(false) << test << ": Caught no exception" << std::endl;
}

void
check_formatting( const ILwd::LdasElement& Element,
		  ILwd::Format F, ILwd::Compression C )
{
  //---------------------------------------------------------------------
  // Local variables
  //---------------------------------------------------------------------
  bool pass = true;
  //---------------------------------------------------------------------
  // Write element to a stream.
  //---------------------------------------------------------------------
  std::ostringstream	out;

  try {
    Element.write( out, F, C );
  }
  catch( ... )
  {
    pass = false;
  }
  //---------------------------------------------------------------------
  // Check if everything is good so far
  //---------------------------------------------------------------------
  if ( pass )
  {
    //-------------------------------------------------------------------
    // Read the data back
    //-------------------------------------------------------------------
    std::istringstream	in( out.str( ) );
    ILwd::Reader r( in );

    try
    {
      //-----------------------------------------------------------------
      // Create a copy based on the ilwd stream representation
      //-----------------------------------------------------------------
      std::auto_ptr< ILwd::LdasElement >
	comp( ILwd::LdasElement::createElement( r ) );

      //-----------------------------------------------------------------
      // Compare the to source
      //-----------------------------------------------------------------
      pass = ( Element == *comp );
    }
    catch( ... )
    {
      pass = false;
    }
  }
    
  Test.Check( pass ) << " Format: " << FormatType( F )
		     << " Compression: " << CompressionType( C )
		     << " name: " << Element.getNameString( )
		     << std::endl;
} // check_formatting

void
formatting_test( )
{
  //---------------------------------------------------------------------
  // Initialize the data
  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  // LdasArray<T>
  //---------------------------------------------------------------------
  float data[ 10 ];
  const INT_4U data_size = sizeof( data ) / sizeof( *data );

  for ( INT_4U x = 0; x < data_size; ++x )
  {
    data[ x ] = (float)x;
  }
  ILwd::LdasArray< float >
    element( data, data_size, "LdasArray<float> reference" );

  //---------------------------------------------------------------------
  // LdasString
  //---------------------------------------------------------------------
  ILwd::LdasString
    string_element( "hello world!", "LdasString reference" );

  //---------------------------------------------------------------------
  // LdasString with two elements
  //---------------------------------------------------------------------
  std::vector< std::string >
    string2_data( 2 );

  string2_data[0] = "Hello World!";
  string2_data[1] = "Goodbye World!";
  ILwd::LdasString
    string2_element( string2_data, "LdasString[2] reference" );

  //---------------------------------------------------------------------
  // LdasExternal
  //---------------------------------------------------------------------
  const std::string external_data( "hello external world!" );
  ILwd::LdasExternal
    external_element( (void*)( external_data.c_str( ) ),
		      external_data.length( ) );
  external_element.setNameString( "LdasExternal reference" );

  //---------------------------------------------------------------------
  // LdasContainer
  //---------------------------------------------------------------------
  ILwd::LdasContainer	container_element( "LdasContainer reference" );

  container_element.push_back( &element,
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::NO_OWN );
  container_element.push_back( &string_element,
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::NO_OWN );
  container_element.push_back( &string2_element,
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::NO_OWN );
  container_element.push_back( &external_element,
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::NO_OWN );

  //---------------------------------------------------------------------
  // Run the compression tests
  //---------------------------------------------------------------------
  for ( INT_4U f = 0; f < ILwd::USER_FORMAT; ++f )
  {
    for ( INT_4U c = 0; c < ILwd::USER_COMPRESSION; ++c )
    {
      Test.Message(  )
	<< "##### "
	<< " Format: " << FormatType( (ILwd::Format)f )
	<< " Compression: " << CompressionType( (ILwd::Compression)c )
	<< " #####"
	<< std::endl;
      check_formatting( element, (ILwd::Format)f, (ILwd::Compression)c );
      check_formatting( external_element,
			(ILwd::Format)f, (ILwd::Compression)c );
      check_formatting( string_element,
			(ILwd::Format)f, (ILwd::Compression)c );
      check_formatting( string2_element,
			(ILwd::Format)f, (ILwd::Compression)c );
      check_formatting( container_element,
			(ILwd::Format)f, (ILwd::Compression)c );
    }
  }
} // formatting_test

int
main(int ArgC, char* ArgV[])
{
  Test.Init(ArgC, ArgV);

  General::LDASUnexpected::makeAbort( true );

  array_char();
  array_int_with_nulls();

  int_array_assign();
  array_zero();

  //---------------------------------------------------------------------
  // Go through a series of tests on native types
  //---------------------------------------------------------------------

  INT_4S n_int_1[] = { -1, -2, -3, -4};
  char c_int_1[] =
    "<int_4s dims='4' name='native test'>"
    "-1 -2 -3 -4"
    "</int_4s>";

  native_test<INT_4S, ILwd::LdasArray<INT_4S> >
    (n_int_1, sizeof(n_int_1)/sizeof(*n_int_1),
     c_int_1, sizeof(c_int_1));

  //---------------------------------------------------------------------
  // Go through a series of tests on characters
  //---------------------------------------------------------------------

  const std::string c1 = "<char dims='4' name='unprintable char array'>"
	       "\\177\\274\\375\\377"
	       "</char>";
  const std::string c2 = "<char dims='1028'>"
	    "41847AE140000000"	// 1
	    "41847AE148000000"	// 1
	    "41847AE150000000"	// 1
	    "41847AE158000000"	// 1
	    "41847AE160000000"	// 1
	    "41847AE168000000"	// 1
	    "41847AE170000000"	// 1
	    "41847AE178000000"	// 1
	    "41847AE180000000"	// 1
	    "41847AE188000000"	// 10
	    "41847AE190000000"	// 1
	    "41847AE198000000"	// 1
	    "41847AE1A0000000"	// 1
	    "41847AE1A8000000"	// 1
	    "41847AE1B0000000"	// 1
	    "41847AE1B8000000"	// 1
	    "41847AE1C0000000"	// 1
	    "41847AE1C8000000"	// 1
	    "41847AE1D0000000"	// 1
	    "41847AE1D8000000"	// 20
	    "41847AE1E0000000"	// 2
	    "41847AE1E8000000"	// 2
	    "41847AE1F0000000"	// 2
	    "41847AE1F8000000"	// 2
	    "41847AE200000000"	// 2
	    "41847AE208000000"	// 2
	    "41847AE210000000"	// 2
	    "41847AE218000000"	// 2
	    "41847AE220000000"	// 2
	    "41847AE228000000"	// 30
	    "41847AE230000000"	// 3
	    "41847AE238000000"	// 3
	    "41847AE240000000"	// 3
	    "41847AE248000000"	// 3
	    "41847AE250000000"	// 3
	    "41847AE258000000"	// 3
	    "41847AE260000000"	// 3
	    "41847AE268000000"	// 3
	    "41847AE270000000"	// 3
	    "41847AE278000000"	// 40
	    "41847AE280000000"	// 4
	    "41847AE288000000"	// 4
	    "41847AE290000000"	// 4
	    "41847AE298000000"	// 4
	    "41847AE2A0000000"	// 4
	    "41847AE2A8000000"	// 4
	    "41847AE2B0000000"	// 4
	    "41847AE2B8000000"	// 4
	    "41847AE2C0000000"	// 4
	    "41847AE2C8000000"	// 50
	    "41847AE2D0000000"	// 5
	    "41847AE2D8000000"	// 5
	    "41847AE2E0000000"	// 5
	    "41847AE2E8000000"	// 5
	    "41847AE2F0000000"	// 5
	    "41847AE2F8000000"	// 5
	    "41847AE300000000"	// 5
	    "41847AE308000000"	// 5
	    "41847AE310000000"	// 5
	    "41847AE318000000"	// 60
	    "41847AE320000000"	// 6
	    "41847AE328000000"	// 6
	    "41847AE330000000"	// 6
	    "41847AE338000000"	// 64
	    "\\000X\\274\\020"	// + 4
	    "</char>";
  const std::string c3 = "<char dims='4' name='unprintable char array'>"
	       "\177\274\375\377"
	       "</char>";
  all_char_test< ILwd::LdasArray<CHAR> >( c1 );
  all_char_test< ILwd::LdasArray<CHAR> >( c2 );
  all_char_test< ILwd::LdasArray<CHAR> >( c3, c1 );

  //---------------------------------------------------------------------
  // Go through a series of tests on lstrings
  //---------------------------------------------------------------------

  const std::string lstring_1 =
    "<lstring dims='0' name='lstring 0 element test'>"
    "</lstring>";
  const std::string lstring_2 =
    "<lstring name='lstring 1 element test'>"
    "</lstring>";
  const std::string lstring_3 =
    "<lstring dims='3' name='lstring 10 element test' size='16'>"
    "\\177\\,\\277\\,\\377"
    "</lstring>";
  const std::string lstring_4 =
    "<lstring dims='10' name='lstring 10 element test' size='18'>"
    "\\,\\,\\,\\,\\,\\,\\,\\,\\,"
    "</lstring>";
  const std::string lstring_5 =
    "<lstring dims='10' name='lstring 10 element test' size='28'>"
    "0\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\,9"
    "</lstring>";
  const std::string lstring_6 =
    "<lstring dims='10' name='lstring 10 element test' size='27'>"
    "\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\,9"
    "</lstring>";
  const std::string lstring_7 =
    "<lstring dims='10' name='lstring 10 element test' size='27'>"
    "0\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\,"
    "</lstring>";
  const std::string lstring_8 =
    "<lstring dims='9' name='lstring 9 element test' size='46'>"
    "0\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\\\,embedded backslash"
    "</lstring>";
  const std::string lstring_9 =
    "<lstring dims='9' name='lstring 9 element test' size='45'>"
    "0\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\\\embedded backslash"
    "</lstring>";
  const std::string lstring_10 =
    "<lstring dims='5' name='TYPE_CLOB' nullmask='0' size='83'>"
    "\\,\\,TrigMgr20001017041153000002010000.xml\n"
    "TrigMgr20001017041158000003010000.xml\\,\\,"
    "</lstring>";
   
  const std::string lstring_11 =
    "<lstring dims='10' name='lstring 10 element test' size='20'>"
    "\\\\\\,\\,\\,\\,\\,\\,\\,\\,\\,"
    "</lstring>";   
   
  all_char_test< ILwd::LdasString >( lstring_1 );
  all_char_test< ILwd::LdasString >( lstring_2 );
  all_char_test< ILwd::LdasString >( lstring_3 );
  all_char_test< ILwd::LdasString >( lstring_4 );
  all_char_test< ILwd::LdasString >( lstring_5 );
  all_char_test< ILwd::LdasString >( lstring_6 );
  all_char_test< ILwd::LdasString >( lstring_7 );
  all_char_test< ILwd::LdasString >( lstring_8 );
  all_char_test< ILwd::LdasString >( lstring_9 );
  all_char_test< ILwd::LdasString >( lstring_10 );
  all_char_test< ILwd::LdasString >( lstring_11 );  

  //---------------------------------------------------------------------
  // Go through a series of tests on containers.
  //---------------------------------------------------------------------

  const std::string container_1 =
    "<ilwd name='container'>"
    "<lstring dims='0' name='lstring 0 element test'>"
    "</lstring>"
    "</ilwd>";
  const std::string container_2 =
    "<ilwd name='container'>"
    "<lstring dims='0' name='lstring 0 element test'>"
    "</lstring>"
    "</ilwd>";
  const std::string container_3 =
    "<ilwd name='container'>"
    "<lstring name='lstring 1 element test'>"
    "</lstring>"
    "</ilwd>";
  const std::string container_4 =
    "<ilwd name='container'>"
    "<lstring dims='10' name='lstring 10 element test' size='18'>"
    "\\,\\,\\,\\,\\,\\,\\,\\,\\,"
    "</lstring>"
    "</ilwd>";
  const std::string container_5 =
    "<ilwd name='container'>"
    "<lstring dims='10' name='lstring 10 element test' size='28'>"
    "0\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\,9"
    "</lstring>"
    "</ilwd>";
  const std::string container_6 =
    "<ilwd name='container'>"
    "<lstring dims='10' name='lstring 10 element test' size='27'>"
    "\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\,9"
    "</lstring>"
    "</ilwd>";
  const std::string container_7 =
    "<ilwd name='container'>"
    "<lstring dims='10' name='lstring 10 element test' size='27'>"
    "0\\,1\\,2\\,3\\,4\\,5\\,6\\,7\\,8\\,"
    "</lstring>"
    "</ilwd>";
  const std::string container_8 =
    "<ilwd name='container'>"
    "<char dataValueUnit='foo' dims='10' dx='2' name='char 10 element test' startx='10'>"
    "\\00012345678\\000"
    "</char>"
    "</ilwd>";
  const std::string container_9 =
    "<ilwd jobid='4'>"
    "<ilwd name='junk'>"
    "<int_4u name='hello'>0</int_4u>"
    "</ilwd>"
    "</ilwd>";

  const std::string container_10 =
    "<ilwd jobid='4'>"
    "<ilwd name='junk' size='2'>"
    "<int_4s dims='0' name='int_4s'></int_4s>"
    "<int_4u dims='0' name='int_4u'></int_4u>"
    "</ilwd>"
    "</ilwd>";

  all_char_test< ILwd::LdasContainer >( container_1 );
  all_char_test< ILwd::LdasContainer >( container_2 );
  all_char_test< ILwd::LdasContainer >( container_3 );
  all_char_test< ILwd::LdasContainer >( container_4 );
  all_char_test< ILwd::LdasContainer >( container_5 );
  all_char_test< ILwd::LdasContainer >( container_6 );
  all_char_test< ILwd::LdasContainer >( container_7 );
  all_char_test< ILwd::LdasContainer >( container_8 );
  all_char_test< ILwd::LdasContainer >( container_9 );
  all_char_test< ILwd::LdasContainer >( container_10 );

  //---------------------------------------------------------------------
  // Go through a series of tests on externals
  //---------------------------------------------------------------------

  const std::string external_1 =
    "<external mime_type='audio/x-pn-realaudio' size='11'>"
    "hello world"
    "</external>";

  all_char_test< ILwd::LdasExternal >( external_1 );

  //---------------------------------------------------------------------
  // Go through a set of test with metadata attribute
  //---------------------------------------------------------------------

  const std::string metadata_1 =
    "<lstring dims='0' metadata='hello=4' name='lstring 0 element test'>"
    "</lstring>";

  const std::string metadata_2 =
    "<lstring dims='0' metadata='world=45:hello=4' name='lstring 0 element test'>"
    "</lstring>";

  const std::string metadata_3 =
    "<lstring dims='0' metadata='world=45\\:stuff:hello=4' name='lstring 0 element test'>"
    "</lstring>";

  const std::string metadata_4 =
    "<lstring dims='0' metadata='hello=4:h2\\:world=45 stuff' name='lstring 0 element test'>"
    "</lstring>";

  all_char_test< ILwd::LdasString >( metadata_1 );
  all_char_test< ILwd::LdasString >( metadata_2 );
  all_char_test< ILwd::LdasString >( metadata_3 );
  all_char_test< ILwd::LdasString >( metadata_4 );

  //---------------------------------------------------------------------
  // Test to see if binary data in lstring throws exception
  //---------------------------------------------------------------------

  binary_lstring();

  //---------------------------------------------------------------------
  // Testing of data can be create, written, read, written and read.
  //---------------------------------------------------------------------
  formatting_test( );

  //---------------------------------------------------------------------
  // Finished the tests; exit with the appropriate exit status.
  //---------------------------------------------------------------------

  Test.Exit( );
}
