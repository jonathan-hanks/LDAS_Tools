#include "ilwd/config.h"

#include <sys/stat.h>

#include <fcntl.h>
#include <memory>
#include <sstream>
#include <unistd.h>

#include "general/unittest.h"
#include "general/autoarray.hh"

#include "ilwd/reader.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/util.hh"

using ILwd::Reader;
using namespace std;   
   

#define	TEST_NAME "reader.test::"

General::UnitTest	Test;

void
pushback()
{
    ifstream ss( "in.dat" );
    Reader rd( ss );

    rd.pushback( ' ' );
    rd.pushback( "Hello" );
    Test.Check( rd.readString( "Hello world!" ) ) << "pushback: " << endl;
    try
    {
      int max(20);
      while(max)
      {
	max--;
      }
    }
    catch(...)
    {
    }
}

void
file_read()
{
    struct stat	stat_buf;
    stat("in.dat", &stat_buf);
    int	fd;

    General::AutoArray< char > buffer( new char[ stat_buf.st_size + 1 ] );
    fd = open("in.dat", O_RDONLY);
    if ( read(fd, buffer.get( ), stat_buf.st_size) != stat_buf.st_size )
    {
      Test.Check( false ) << "Reading in.dat" << std::endl;
      close( fd );
      return;
    }
    close(fd);
    buffer[stat_buf.st_size] = '\0';

    std::ifstream ss( "in.dat" );

    Test.Check( ss.good() ) << "Created file stream" << std::endl;
    Test.Check( ss.is_open() ) << "Stream is open" << std::endl;
    Test.Message() << "Peek: " << ss.peek() << std::endl;
    Reader rd( ss );
    Test.Check( rd.readString( buffer.get( ) ) ) << "file_read" << endl;
}

void
test1( )
{
  //=====================================================================
  // Generate data file
  //=====================================================================

  std::ofstream o("in.dat");
  o << "world!";
  o.close();

  //=====================================================================
  // Perform the tests
  //=====================================================================

  file_read();
  pushback();
}

void
test2( )
{
  static std::istringstream
    in( "<ilwd name='H2\\:LSC-AS_Q::AdcData:6000000:0:Frame' size='3'>\n"
	"    <lstring name='comment'></lstring>\n"
	"    <int_4u name='channelGroup'>5</int_4u>\n"
	"    <int_4u name='channelNumber'>297</int_4u>\n"
	"</ilwd>\n" );
  
  try {
      ILwd::Reader r( in );
      ILwd::readHeader( in );

      r.skipWhiteSpace();
      std::auto_ptr<ILwd::LdasElement>
	e(ILwd::LdasElement::createElement( r ) );
      Test.Check( e.get( ) != ( ILwd::LdasElement*)NULL )
	<< "Read ILwd from stream" << std::endl;
  }
  catch( const LdasException& Exception )
  {
    for ( size_t
	    i = 0,
	    last = Exception.getSize( );
	  i != last;
	  ++i )
    {
      Test.Check( false ) << "Caught LdasException: "
			  << Exception.getError( i ).getMessage( )
			  << " Info: "
			  << Exception.getError( i ).getInfo( )
			  << std::endl;
    }
  }
  catch( const std::exception& Exception )
  {
    Test.Check( false ) << "Caught std::exception: " << Exception.what( )
			<< endl;
  }
  catch( ... )
  {
    Test.Check( false ) << "Caught unknown exception" << endl;
  }
}

int
main(int ArgC, char** ArgV)
{
  //=====================================================================
  // Set things up for testing
  //=====================================================================

  Test.Init(ArgC, ArgV);

  //=====================================================================
  // Run though a set of tests
  //=====================================================================
  test1( );
  test2( );

  //=====================================================================
  // Exit according to test results
  //=====================================================================

  Test.Exit();
}
