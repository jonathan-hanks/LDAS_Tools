/* -*- mode: c++; c-basic-offset: 4; -*- */

#ifndef IlwdLdasExternalHH
#define IlwdLdasExternalHH

//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

// ObjectSpace Includes
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

// Local Includes
#include "ldasformatelement.hh"
#include "reader.hh"

/// \cond DOXYGEN_IGNORE
namespace ILwd
{
    class LdasExternal;
}
/// \endcond

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::LdasExternal& base );
void os_read( os_bstream& stream, ILwd::LdasExternal& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------------
//
//: ILWD External object.
//
class ILwd::LdasExternal : public ILwd::LdasFormatElement
{
private:
  static const std::string mIdent;
public:
  static const char* ATTR_STR_MIME_TYPE;
  static const char* ATTR_STR_SIZE;
  static const char* ATTR_STR_BYTES;

    /* Constructors / Destructor */
    LdasExternal();
    LdasExternal( void* buffer, size_t s, const std::string& MimeType = "" );
    LdasExternal( const LdasExternal& le );
   
    LdasExternal( Reader& r );
    LdasExternal( Reader& r, const Attributes& att );
   
    ~LdasExternal();

    /* Operator Overloads */
    const LdasExternal& operator=( const LdasExternal& le );
   
    //
    //: Concatenation Operator.
    //
    //!param: const LdasExternal& le - The object to concatenate from.
    //
    //!return: const LdasExternal& - A reference to this object.
    //
    //!exc: std::bad_alloc - Memory allocation failed.
    //   
    const LdasExternal& operator+=( const LdasExternal& le );
    bool operator==( const LdasExternal& le ) const;
    bool operator!=( const LdasExternal& le ) const;

    /* Accessors */
    const std::string& getIdentifier() const;
    static const std::string& getIdentifierStatic();
    void* getBuffer();
    const void* getBuffer() const;
    size_t getSize() const;
    std::string getMimeType() const;
    ElementId getElementId() const;
    
    /* Copy */
    LdasExternal* createCopy() const;
    
    /* I/O */
    virtual void read( Reader& r );

private:

    virtual void writeData( std::ostream& stream ) const;
    virtual void read( Reader& r, const Attributes& att );

    unsigned char* mBuffer;
    size_t mSize;
    std::string mMimeType;

#ifdef HAVE_LIBOSPACE
    //: Write to the stream.
    //
    //!param: os_bstream& - A reference to the stream to write to.
    //!param: const LdasExternal& - A reference to the object to write to  +
    //!param:    the stream.
    //     
    friend void ::os_write( os_bstream&, const LdasExternal& );
   
    //: Read from the stream.
    //
    //!param: os_bstream& - A reference to the stream to read from.
    //!param: const LdasExternal& - A reference to the object to read      + 
    //!param:    from the stream.
    //      
    friend void ::os_read( os_bstream&, LdasExternal& );
#endif // HAVE_LIBOSPACE
};


//-----------------------------------------------------------------------------
// ObjectSpace
//-----------------------------------------------------------------------------


#ifdef HAVE_LIBOSPACE
OS_POLY_CLASS( ILwd::LdasExternal )
OS_STREAM_OPERATORS( ILwd::LdasExternal )
#endif // HAVE_LIBOSPACE    
                

#endif // IlwdLdasExternalHH
