/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "ilwd/config.h"

#include <sstream>
#include <string>   

#include "general/regexmatch.hh"

#include "util.hh"

namespace {
    void unget( const std::stringbuf& Source, std::istream& Stream )
    {
	// Put back a group of characters. Have to do it in reverse order so
	// they will be read in the proper order
	std::string src( Source.str() );
	for ( std::string::reverse_iterator c = src.rbegin();
	      c != src.rend();
	      c++ )
	{
	    Stream.putback( *c );
	}
	// Reset the stream to be in a good state
        Stream.clear();        
   
        return;
    }
}

namespace ILwd {

    using namespace std;   

//-----------------------------------------------------------------------------
//
//: Write ILWD Header
//
// This method simply writes "<?ilwd?>" to the output stream.
//
//!param: ostream& os
//
void writeHeader( ostream& os )
{
    os << "<?ilwd?>" << std::endl;
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Read ILWD Header
//
// This function attempts to read the ILWD header from a stream.  The ILWD
// header is simply an XML PI with 'ilwd' as the target.  If the header is not
// found, then the method returns false.  In this case, the stream will have
// been modified in one of three ways:
//
// 1. If '<?' was not found next on the stream (ignoring whitespace),  then
//    everything up to the first non-whitespace character is extracted.
// 2. If a PI was found, but the target was not "ilwd", then everything up to
//    and including that PI will be extracted.
// 3. If only whitespace exists in the stream, then everything will be
//    extracted.
//
// This function uses a regular expression to match the header.  This regex
// matches something that looks like:
//     <?ilwd extra stuff ?>
//
// Detail (case insensitive match):
//
// ^              The beginning of the string
// [[:space:]]*   Optional whitespace before the start
// <\?ilwd        The start of the PI & target
// (              Grouping of optional contents
//   [[:space:]]    Required space after target
//   (              Grouping for OR
//     [^?]+          Anything that isn't a '?' (the start of '?>')
//     |              OR
//     \?[^>]         '?' followed by anything except '>'
//   )*             0 or more groups of these
// )?             This grouping was optional
// \?>            End delimiter for PI
//
//!param: istream& is - The stream to read the header from.
//
static const Regex re(
        "^[[:space:]]*<\\?ilwd([[:space:]]([^?]+|\\?[^>])*)?\\?>",
        REG_EXTENDED | REG_ICASE );

bool readHeader( istream& is )
{
    std::stringbuf sb;

    // Eat whitespace
    while( is.good() && isspace( is.peek() ) != 0 )
    {
        is.get();
    }
    
    // Get the starting '<'
    if ( !is.good() || is.peek() != '<' )
    {
        return false;
    }
    sb.sputc( is.get() );

    // Make sure that a '?' is next
    if ( !is.good() || is.peek() != '?' )
    {
        // It isn't, pushback the '<' and return false
	unget( sb, is );
        return false;
    }
    sb.sputc( is.get() );

    // Now get everything up through and including the closing '?>'
    char c( '\0' );
    while( c != '>' && is.good() )
    {
        if ( ! is.get( sb, '?' ) )
	{
	    break;
	}
        sb.sputc( is.get() );
        sb.sputc( ( c = is.get() ) );
    }
    
    // Make sure the stream is OK
    if ( !is.good() )
    {
	unget( sb, is );
        return false;
    }
    
    // Now try and match the string.
    RegexMatch	rm;
    const string& sb_string( sb.str() );
   
    bool	retval( rm.match( re, sb_string.c_str() ) );

    if ( !retval )
    {
	unget( sb, is );
    }
    return retval;
}

 
} // namespace
