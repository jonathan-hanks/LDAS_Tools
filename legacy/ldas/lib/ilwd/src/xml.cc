#if HAVE_CONFIG_H
#include "ilwd/config.h"
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>

#include "xml.hh"

namespace XML
{
    // Spaces are not significant in Regex comments

    /**
     * S
     *
     * Regex:
     *   a-zA-Z
     */
#   define XML_S "[[:space:]]+"
    const char* S( XML_S );

    /**
     * S_OPT
     *
     * Regex:
     *   a-zA-Z
     */
#   define XML_S_OPT "[[:space:]]*"
    const char* S_opt( XML_S_OPT );

    /**
     * Letter
     *
     * this is used as a character class.
     *
     * Regex:
     *   a-zA-Z
     */
#   define XML_LETTER "a-zA-Z"
    const char* Letter( XML_LETTER );

    /**
     * Digit
     *
     * This is used as a character class.
     *
     * Regex:
     *   0-9
     */
#   define XML_DIGIT "0-9"
    const char* Digit( XML_DIGIT );

    /**
     * NameChar
     *
     * This is used as a character class.
     *
     * Regex:
     *   Letter Digit .\-_
     */
#   define XML_NAMECHAR XML_LETTER XML_DIGIT "_:.-"
    const char* NameChar( XML_NAMECHAR );

    /*
     * Name
     *
     * Captures: NONE
     *
     * Regex:
     *   [a-zA-Z_:][a-zA-Z.\-_:]*
     */
#   define XML_NAME "[" XML_LETTER "_:][" XML_NAMECHAR "]*"
    const char* Name( XML_NAME );

    /*
     * Entity Reference
     *
     * Captures: NONE
     *
     * Regex:
     *   &Name;
     */
#   define XML_ENTITYREFERENCE "&" XML_NAME ";"
    const char* EntityReference( XML_ENTITYREFERENCE );

    /*
     * Reference
     *
     * Captures: NONE
     *
     * Regex:
     *   EntityReference
     */
#   define XML_REFERENCE XML_ENTITYREFERENCE
    const char* Reference( XML_REFERENCE );
    
    /*
     * AttValue
     *
     * Captures: 3
     *   1 - The attribute value, including the delimiter.
     *   2 - The attribute value, if the delimiter is: "
     *   3 - The attribute value, if the delimiter is: '
     *
     * Regex:
     *   (
     *     "([^"]*)"
     *     |
     *     '([^']*)'
     *   )
     */
#   define XML_ATTVALUE "(\"([^\"]*)\"|'([^']*)')"
    const char* AttValue( XML_ATTVALUE );
    
    /*
     * Eq
     *
     * Captures: None
     *
     * Regex:
     *   S?=S?
     */
#   define XML_EQ XML_S_OPT "=" XML_S_OPT    
    const char* Eq( XML_EQ );

    /*
     * Attribute
     *
     * Captures: 4
     *   1 - The attribute name
     *   2 - The attribute value, including the delimiter.
     *   3 - The attribute value, if the delimiter is: "
     *   4 - The attribute value, if the delimiter is: '
     *
     * Regex:
     *   S? (Name) Eq AttValue S?
     */
#   define XML_ATTRIBUTE \
        XML_S_OPT "(" XML_NAME ")" XML_EQ XML_ATTVALUE XML_S_OPT
    const char* Attribute( XML_ATTRIBUTE );
    
    /*
     * STag
     *
     * Captures: 2
     *   1 - The tag identifier
     *   2 - The collection of attributes
     *
     * Regex:
     *   <(Name)([^>]*)>
     */
#   define XML_STAG "<(" XML_NAME ")([^>]*)>"
    const char* STag( XML_STAG );
    
    /*
     * ETag
     *
     * Captures: 1
     *   1 - The end-tag identifier
     *
     * Regex:
     *   </(Name) S?>
     */
#   define XML_ETAG "</(" XML_NAME ")" XML_S_OPT ">"
    const char* ETag( XML_ETAG );
}

    
