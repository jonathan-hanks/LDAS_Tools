/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "ilwd/config.h"

#include "style.hh"
#include "ldasformatelement.hh"

using ILwd::LdasFormatElement;


/* Constructors / Destructor */

//-----------------------------------------------------------------------------
//   
//: Constructor.
// 
//!param: const std::string& name - A reference to the name string. Default is     +
//!param:    an empty string.
//!param: const std::string& comment - A reference to the comment string. Default  +
//!param:    is an empty string.
//   
LdasFormatElement::LdasFormatElement(
   const std::string& name, const std::string& comment )
   : LdasElement( name, comment ), Style()
{}


//-----------------------------------------------------------------------------
//   
//: Copy constructor.
// 
//!param: const LdasFormatElement& e - A reference to the object to copy.
//   
LdasFormatElement::LdasFormatElement( const LdasFormatElement& e )
   : LdasElement( e ), Style( e )
{}


//-----------------------------------------------------------------------------
//   
//: Destructor.
// 
LdasFormatElement::~LdasFormatElement()
{}


/* Operator Overloads */

   
//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const LdasFormatElement& e - The object to assign from.
//
//!return: const LdasFormatElement& - A reference to this object.
//
const LdasFormatElement& LdasFormatElement::operator=(
    const LdasFormatElement& e )
{
    if ( this != &e )
    {
        LdasElement::operator=( e );
        Style::operator=( e );
    }

    return *this;
}


//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another LdasFormatElement for equality.
//
//!param: const LdasFormatElement& e - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool LdasFormatElement::operator==( const LdasFormatElement& e ) const
{
    return ( LdasElement::operator==( e ) &&
	     Style::operator==( e ) );
}

   
/* Attribute manipulation */

//-----------------------------------------------------------------------------
//
//: Writes attributes.
//
// This method writes attributes of the object.
//
//!param: const Attributes& att - A reference to the object attributes to write.
//
void LdasFormatElement::writeAttributes( Attributes& att ) const
{
    LdasElement::writeAttributes( att );
    Style::writeAttributes( att );
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Reads attributes.
//
// This method reads attributes of the object.
//
//!param: const Attributes& att - A reference to the object attributes to read.
//   
void LdasFormatElement::readAttributes( const Attributes& att )
{
    LdasElement::readAttributes( att );
    Style::readAttributes( att );
   
    return;
}


/* I/O */
//-----------------------------------------------------------------------------
//
//: Writes object to the stream with specified format and compression.
//
//!param: ostream& stream - A reference to the stream to write to.
//!param: Format f - Format to use for writing.
//!param: Compression c - Compression to use for writing. 
//   
//!exc: std::exception - An unknown error occured.
//   
void LdasFormatElement::write( std::ostream& stream, Format f,
   Compression c ) const
{
    // Remember original format and compression
    const Format my_format( getWriteFormat() );
    const Compression my_compression( getWriteCompression() );
    
    if ( f != USER_FORMAT && f != my_format )
    {
        mFormat = f;
    }
   
    if ( c != USER_COMPRESSION && c != my_compression )
    {
        mCompression = c;
    }
    
    writeData( stream );

    // Restore original format and compression
    mFormat = my_format;
    mCompression = my_compression;   

    return;
}


//-----------------------------------------------------------------------------
// ObjectSpace
//-----------------------------------------------------------------------------

#if HAVE_LIBOSPACE
#include <ospace/source.h>
#include "ospaceid.hh"

void os_write( os_bstream& stream, const ILwd::LdasFormatElement& o )
{
    os_write( stream, (ILwd::LdasElement&)o );
    os_write( stream, (ILwd::Style&)o );
   
    return;
}


void os_read( os_bstream& stream, ILwd::LdasFormatElement& o )
{
    os_read( stream, (ILwd::LdasElement&)o );
    os_read( stream, (ILwd::Style&)o );
   
    return;
}

OS_NO_FACTORY_STREAMABLE_2( (ILwd::LdasFormatElement*),
                            Stream::ILwd::LDASFORMATELEMENT,
                            (ILwd::LdasElement*),
                            (ILwd::Style*) )

#endif // HAVE_LIBOSPACE
