#include "ilwd/config.h"

#include "errors.hh"

const char* ILwd::getErrorString( int err )
{
    switch( err )
    {
        case Errors::INVALID_DELIMITER:
            return "invalid_delimiter";
        case Errors::INVALID_NAME:
            return "invalid_name";
        case Errors::INVALID_ATTVALUE:
            return "invalid_attvalue";
        case Errors::UNKNOWN_REFERENCE:
            return "unknown_reference";
        case Errors::INVALID_ATTRIBUTE:
            return "invalid_attribute";
        case Errors::INVALID_START_TAG:
            return "invalid_start_tag";
        case Errors::INVALID_END_TAG:
            return "invalid_end_tag";
        case Errors::INVALID_CHAR:
            return "invalid_char";
        case Errors::INVALID_IDENTIFIER:
            return "invalid_identifier";
        case Errors::INVALID_CDATA:
            return "invalid_cdata";
        case Errors::INVALID_COMMENT:
            return "invalid_comment";
        case Errors::INVALID_PI:
            return "invalid_pi";
        case Errors::BAD_ATTRIBUTE:
            return "bad_attribute";
        case Errors::BAD_START_TAG:
            return "bad_start_tag";
        case Errors::BAD_END_TAG:
            return "bad_end_tag";
        case Errors::BAD_CONVERSION:
            return "bad_conversion";
        case Errors::MISSING_ATTRIBUTE:
            return "missing_attribute";
        case Errors::MISSING_DELIMITER:
            return "missing_delimiter";
        case Errors::MISSING_EQUAL:
            return "missing_equal";
        case Errors::DUPLICATE_ATTRIBUTE:
            return "duplicate_attribute";
        case Errors::UNKNOWN_IDENTIFIER:
            return "unknown_identifier";
        case Errors::UNKNOWN_OBJECT:
            return "unknown_object";
        case Errors::INVALID_FORMAT:
            return "invalid_format";
        case Errors::OUT_OF_RANGE:
	    return "out_of_range";
        case Errors::BAD_DATA_TYPE:
	    return "bad_data_type";
        case Errors::UNSIGNED_BELOW_ZERO:
	    return "unsigned_below_zero";
        case Errors::EXTRA_DATA:
	    return "extra_data";
        case Errors::DIM_ERROR:
	    return "dim_error";
        case Errors::CONVERSION:
	    return "conversion";
        default:
            return "unknown_error_code";
    }
}

