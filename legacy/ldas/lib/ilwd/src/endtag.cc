#include "ilwd/config.h"

#include <string>
#include <iostream>

#include "endtag.hh"
#include "errors.hh"
#include "xml.hh"

#include <general/types.hh>   
   
using ILwd::EndTag;
using namespace std;   
   

const Regex &EndTag::regex = Regex( std::string( "^[[:space:]]*" ) + XML::ETag );


//-----------------------------------------------------------------------------
//
//: Constructor.
//
//!param: const std::string& name -
//
ILwd::EndTag::EndTag( const std::string& name )
   : mName( slower( name ) )
{}
   

//-----------------------------------------------------------------------------
//
//: Destructor.
//
ILwd::EndTag::~EndTag()
{}
   
   
//-----------------------------------------------------------------------------
//
//: Creates an end-tag from a reader.
//
//!param: Reader& r -
//
//!except: StreamException -
//!except: FormatException -
//
EndTag::EndTag( Reader& r )
{
    read( r );
}


//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const EndTag& e - The object to assign from.
//
//!return: const EndTag& - A reference to this object.
//
//!except: bad_alloc - Memory allocation failed.
//
const EndTag& EndTag::operator=( const EndTag& e )
{
    if ( this != &e )
    {
        Base::operator=( e );
        mName = e.mName;
    }

    return *this;
}


//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another PI for equality.
//
//!param: const EndTag& e - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool EndTag::operator==( const EndTag& e ) const
{
    return ( Base::operator==( e ) && mName == e.mName );
}


//-----------------------------------------------------------------------------
//
//: Writes an end-tag to a stream.
//
//!param: ostream& stream -
//!param: const EndTag& end -
//
//!return: ostream& -
//
//!except: std::exception - An unknown exception was thrown.
//
std::ostream& std::operator<<( std::ostream& stream,
   const ILwd::EndTag& end )
{
    end.write( stream );
    return stream;
}


//-----------------------------------------------------------------------------
//
//: Get the class ID.
//
//!return: ClassType - The ID for this class: ID_ENDTAG.
//
ILwd::ClassType ILwd::EndTag::getClassId() const
{
    return ID_ENDTAG;
}
   
   
//-----------------------------------------------------------------------------
//
//: Writes an end-tag to a stream.
//
//!param: ostream& stream -
//
//!except: exception -
//
void EndTag::write( std::ostream& stream ) const
{
    stream << "</" << mName << ">";
    return;
}


//-----------------------------------------------------------------------------
//
//: Reads an end-tag from a reader.
// 
// Reads an EndTag from a stream.  This will skip whitespace preceding the
// end-tag.  The read procedure is:
//
// 1) Read the end-tag into a buffer 50 bytes at a time until the '>'
//    delimiter is reached.
// 2) Perform a Regex match to identify parts of the end-tag.
// 3) Store the name (converted to lowercase).
//
//!param: Reader& r -
//
//!except: StreamException - An error reading from the stream occurred.
//!except: FormatException - The format for the PI was invalid.
//
void EndTag::read( Reader& r )
try
{
    // const int bufferSize = 50;

    std::stringbuf	buffer;
    std::istream&	stream( r.getStream() );
    
    stream.get( buffer, '>' );
     if ( stream.gcount() > 0 )
     {
         buffer.sputc( stream.get() );
     }
    
    const std::string etag( buffer.str() );
    const CHAR* etag_string( etag.c_str() );
    
    RegexMatch rm( XML::MATCHES_ETAG );
    if ( !rm.match( regex, etag_string ) )
    {
        string msg( "The end-tag is not valid ILWD: " );
        msg += etag;
         
        throw ILWD_FORMATEXCEPTION_INFO( Errors::INVALID_END_TAG, msg );
    }

    // Get the tag name
    mName = slower( rm.getSubString( XML::M_ETAG_IDENTIFIER ) );
   
    return;
}
ADD_ILWD_FORMATEXCEPTION_INFO(
    Errors::INVALID_END_TAG, "The end-tag is not valid ILWD." )


// ObjectSpace    
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::EndTag*), Stream::ILwd::ENDTAG, (ILwd::Base*) )

void os_write( os_bstream& stream, const EndTag& o )
{
    os_write( stream, (ILwd::Base&)o );    
    stream << o.mName;
   
    return;
}

void os_read( os_bstream& stream, EndTag& o )
{
    os_read( stream, (ILwd::Base&)o );
    stream >> o.mName;
   
    return;
}

#endif // HAVE_LIBOSPACE
