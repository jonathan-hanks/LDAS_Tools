#include "ilwd/config.h"

#include <cstring>

#include "attributevalue.hh"

using ILwd::AttributeValue;
using namespace std;   


//-----------------------------------------------------------------------
/// \brief ( Default ) Constructor.
//-----------------------------------------------------------------------
AttributeValue::AttributeValue()
    : mValue()
{}  
   
   
//-----------------------------------------------------------------------
/// \brief Constructs this object with a given string.
///
/// \param[in] s
///     A reference to the string representing an attribute value.   
///
/// \exception std::bad_alloc
///     Memory allocation failed.
//-----------------------------------------------------------------------
AttributeValue::AttributeValue( const std::string& s )
        : mValue( s )
{}

   

//-----------------------------------------------------------------------
/// \brief Copy Constructor.
///
/// \param[in] av
///     A reference to the AttributeValue object to be copied.   
///
/// \exception std::bad_alloc
///     Memory allocation failed.
//-----------------------------------------------------------------------
AttributeValue::AttributeValue( const AttributeValue& av )
    : mValue( av.mValue )
{}
   
   
//-----------------------------------------------------------------------
//
/// \brief Assignment operator.
//
/// \param[in] av
///     The object to assign from.
///
/// \return
///     A reference to this object.
///
/// \exception std::bad_alloc
///     Memory allocation failed.
//-----------------------------------------------------------------------
const AttributeValue& AttributeValue::operator=( const AttributeValue& av )
{
    if ( this != &av )
    {
        mValue = av.mValue;
    }

    return *this;
}


//-----------------------------------------------------------------------
/// \brief Equal comparison operator.
///
/// This compares this object to another PI for equality.
///
/// \param[in] av
///     The object to compare with.
///
/// \return
///     True if the objects are equal.
//-----------------------------------------------------------------------
bool AttributeValue::operator==( const AttributeValue& av ) const
{
    return ( mValue == av.mValue );
}


//-----------------------------------------------------------------------
/// \brief Inequality operator.
///
/// \param[in] av
///     The object to compare to.
///
/// \return
///     true if the objects are not equal.  False otherwise.
//-----------------------------------------------------------------------
bool AttributeValue::operator!=( const AttributeValue& av ) const
{
    return !operator==( av );
}

   
//-----------------------------------------------------------------------
/// \brief Sets the value of the attribute to the given string.
///
/// \param[in] v
///
/// \exception std::bad_alloc
///     Memory allocation failed.
//-----------------------------------------------------------------------
void ILwd::AttributeValue::readString( const std::string& v )
    try
{ 
    mValue = v;
    replaceReferences();
   
    return;
} 
ADD_ILWD_FORMATEXCEPTION_INFO(
    Errors::INVALID_ATTVALUE, "The attribute value is not valid ILWD." )
   

//-----------------------------------------------------------------------
/// \brief Write the AttributeValue to a stream.
///
/// \param[out] out
///     The output stream.
//-----------------------------------------------------------------------
  void AttributeValue::write( std::ostream& out ) const
{
    std::string tmp( mValue );

    std::string::size_type pos = tmp.find_first_of( "<>&\'\"" );

    while( pos != std::string::npos )
    {
        switch( tmp.at( pos ) )
        {
            case '<':
                tmp.replace( pos, 1, "&lt;" );
                break;

            case '>':
                tmp.replace( pos, 1, "&gt;" );
                break;
                
            case '&':
                tmp.replace( pos, 1, "&amp;" );
                break;
                
            case '\'':
                tmp.replace( pos, 1, "&apos;" );
                break;
                
            case '"':
                tmp.replace( pos, 1, "&quot;" );
                break;
        }

        pos = tmp.find_first_of( "<>&'\"", pos + 1 );
    }

    out << tmp;
}


//-----------------------------------------------------------------------
/// \brief Replace XML references with the appropriate character.
//-----------------------------------------------------------------------
void AttributeValue::replaceReferences()
{
    std::string::size_type pos = mValue.find( '&' );
    while ( pos != std::string::npos )
    {
        if ( strncmp( mValue.c_str() + pos, "&lt;", 4 ) == 0 )
        {
            mValue.replace( pos, 4, "<" );
        }
        else if ( strncmp( mValue.c_str() + pos, "&gt;", 4 ) == 0 )
        {
            mValue.replace( pos, 4, ">" );
        }
        else if ( strncmp( mValue.c_str() + pos, "&amp;", 5 ) == 0 )
        {
            mValue.replace( pos, 5, "&" );
        }
        else if ( strncmp( mValue.c_str() + pos, "&apos;", 6 ) == 0 )
        {
            mValue.replace( pos, 6, "'" );
        }
        else if ( strncmp( mValue.c_str() + pos, "&quot;", 6 ) == 0 )
        {
            mValue.replace( pos, 6, "\"" );
        }
        else
        {
            string msg( "An unknown ILWD reference was found: " );
            msg += (mValue.c_str() + pos);
            
            throw ILWD_FORMATEXCEPTION_INFO( Errors::UNKNOWN_REFERENCE, msg );
        }

        pos = mValue.find( '&', pos );
    }
}


//-----------------------------------------------------------------------
/// Template Specialization of the setValue template for REAL_4.
//-----------------------------------------------------------------------
template<>
void ILwd::AttributeValue::setValue< REAL_4 >( const REAL_4& v )
{
    std::ostringstream s;

    s.precision( REAL_4_DIGITS + 1 );
    s << v;
    setString( s.str() );

    return;
}


//-----------------------------------------------------------------------
/// Template Specialization of the setValue template for REAL_8.
//-----------------------------------------------------------------------
template<>
void ILwd::AttributeValue::setValue< REAL_8 >( const REAL_8& v )
{
    std::ostringstream s;

    s.precision( REAL_8_DIGITS + 1 );
    s << v;
    setString( s.str() );

    return;
}
   

//-----------------------------------------------------------------------
/// \brief Insertion operator.
///
/// \param[out] stream
/// \param[in] e
///
/// \return
///     The stream which is being written
///
/// \exception exception - An unknown exception was thrown.
//-----------------------------------------------------------------------
namespace std
{
  std::ostream& operator<<( std::ostream& stream,
		                   const ILwd::AttributeValue& e )
  {
    e.write( stream );
    return stream;
  }
}
   
   
   
// ObjectSpace Block
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_0( (ILwd::AttributeValue*), Stream::ILwd::ATTRIBUTEVALUE )

void os_write( os_bstream& stream, const AttributeValue& o )
{
    stream << o.mValue;
    return;
}

void os_read( os_bstream& stream, AttributeValue& o )
{
    stream >> o.mValue;
    return;
}

#endif // HAVE_LIBOSPACE

