#ifndef AttributesHH
#define AttributesHH


#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

// System includes
#include <utility>
#include <string>
#include <iosfwd>   
#include <map>
#include <vector>

// LDAS includes
#include <general/types.hh>
#include <general/util.hh>
#include <general/regex.hh>

// Local includes
#include "attributevalue.hh"
#include "reader.hh"

// ObjectSpace
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif

namespace ILwd {
  class Attributes;
  struct noCaseLess;
}


//-----------------------------------------------------------------------------
//
//: Comparison function object for Attributes.
//
// This implements a case insensitive comparison for strings.
//
struct ILwd::noCaseLess
{
  //: Function operator.
  //
  // This is a binary function which returns true if the two strings provided
  // as parameters are equal (ignoring case).
  //
  //!param: const string& s1 - The 1st string.
  //!param: const string& s2 - The 2nd string.
  //
  //!return: bool - True if the strings are equal ignoring case.
  //
  bool operator()( const std::string& s1,
		   const std::string& s2 ) const
  {
    return ( General::cmp_nocase( s1, s2 ) < 0 );
  }
};


//-----------------------------------------------------------------------------
//
//: Implements a collection of attributes for a tag.
//
// This is simply a map with some additional methods to read and write the
// attributes.
//
// <p><b>Using the class</B><br>
//
// <p>The purpose of this class is to facilitate writing and reading of "ILWD"
// attribute lists.  Internally, the attributes are stored in an STL map which 
// maps an attribute name (a C++ string) to its value (an AttributeValue).
// The class consists of methods to manipulate these values and methods to
// read and write them in "ILWD" form.
//
// <p><b>Construction</b><br>
//
// <p>The primary method in which an Attributes object is created is by passing
// a c-string which contains a sequence of attributes in "ILWD" form.  For
// example:
//
// <p>char* atts = "att1=\"attvalue1\" att2='attvalue2'";
//
// <p>This string is matched sequentially by a regular expression which
// verifies the format and separatea the string into
// attribute/attributevalue pairs.  The string must not contain any additional
// whitespace at the beginning or end and the entire string must parse 
// successfully or the constructor will throw an exception.
//
// <p>Additionally, duplicate attributes will cause the constructor to throw an
// exception.
//
// <p>The Regular Expressions used for the matching are contained in xml.cc .
//
//!todo: DWF 990505 - Make this a part of StartTag instead of its own class.
//!todo: This class does not implement enough functionality to deserve being
//!todo: its own class.  Furthermore, it is only used by the StartTag class.
//
class ILwd::Attributes
  : public std::map< std::string, ILwd::AttributeValue, ILwd::noCaseLess >
{
public:

  /* typedef's */
  typedef std::map< std::string, AttributeValue, noCaseLess >::iterator iterator;
  typedef std::map< std::string, AttributeValue, noCaseLess >::
  const_iterator const_iterator;

  /* Constructors / Destructor */
  //---------------------------------------------------------------------
  /// \brief Default Constructor
  //---------------------------------------------------------------------
  Attributes();
  //---------------------------------------------------------------------
  /// \brief Constructor
  ///
  /// \param a
  ///     The string representing an attribute list in an ILWD format.
  ///
  /// \exception FormatException
  ///     Illegal format is specified.
  //---------------------------------------------------------------------
  explicit Attributes( const char* a );
  //---------------------------------------------------------------------
  /// \brief Copy constructor.
  /// \param att
  ///     The object to copy from.
  ///
  /// \exception std::bad_alloc
  ///     Memory allocation failed.
  //---------------------------------------------------------------------
  Attributes( const Attributes& att );
  //---------------------------------------------------------------------
  /// \brief Destructor.
  //---------------------------------------------------------------------
  virtual ~Attributes();

  /* Operator overloads */
  //---------------------------------------------------------------------
  /// \brief Assignment operator.
  ///
  /// \param att
  ///     The object to assign from.
  ///
  /// \return
  ///     A reference to this object.
  ///
  /// \exception std::bad_alloc
  ///     Memory allocation failed.
  //---------------------------------------------------------------------
  Attributes& operator=( const Attributes& att );
  //---------------------------------------------------------------------
  /// \brief Equal comparison operator.
  ///
  /// \param att
  ///     The object to compare with.
  ///
  /// \return
  ///     True if the objects are equal.
  //---------------------------------------------------------------------
  bool operator==( const Attributes& att ) const;
  //---------------------------------------------------------------------
  /// \brief Inequality operator.
  ///
  /// \param att
  ///     The object to compare to.
  ///
  /// \return
  ///     True if the objects are not equal, false otherwise.
  //---------------------------------------------------------------------
  bool operator!=( const Attributes& att ) const;

  /* Accessors/Mutators */
  //---------------------------------------------------------------------
  /// \brief Add an attribute to the collection.
  ///
  /// \param[in] name
  ///     The name of the attribute.
  /// \param[in] v
  ///     The value for the attribute.
  ///
  /// \exception std::bad_alloc
  ///     Memory allocation failed.
  //---------------------------------------------------------------------
  void addAttribute( const std::string& name, const AttributeValue& v );
  //---------------------------------------------------------------------
  /// \brief Get an attribute.
  /// 
  /// \param name
  ///     The name of the attribute.
  ///
  /// \return
  ///     AttributeValue - A pointer to the AttributeValue or NULL if the
  ///     attribute wasn't found.
  //---------------------------------------------------------------------
  AttributeValue* getAttribute( const std::string& name );

  /* I/O */
  //---------------------------------------------------------------------
  /// \brief Read
  ///
  /// \param a
  ///     A C string containing the attributes.  The attributes appear
  ///     in this string as they appear in the start-tag, minus the
  ///     '<', the identifier, and the '>'.
  ///
  /// \exception std::bad_alloc
  ///     Memory allocation failed.
  /// \exception FormatException
  ///     The attribute format was invalid.
  /// \cond EXCLUDE
  ///     <ul>
  ///   <li>INVALID_ATTRIBUTE: An attribute in the string is not valid
  ///     ILWD.  The unmatched attribute string, starting from the
  ///     offending attribute, is returned in the info message.<br>
  ///
  ///     <p>The attribute is not valid ILWD: name=bob comment="ok"<br></li>
  ///
  ///   <li>DUPLICATE_ATTRIBUTE: The same attribute appeared more than
  ///       once.  The duplicate attribute name is returned, e.g:<br>
  ///
  ///     <p>An attribute appeared more than once: name</li><ul>
  /// \endcond
  //---------------------------------------------------------------------
  void read( const char* a );
  //---------------------------------------------------------------------
  /// \brief Write the attributes to a stream.
  ///
  /// \param stream
  ///     The stream to write to.
  ///
  /// \exception std::exception
  ///     An unknown exception was thrown.
  //---------------------------------------------------------------------
  void write( std::ostream& stream ) const;

private:

  static const Regex &regex;

};


/*--------------------------------*/
/* External function declarations */
/*--------------------------------*/

//-----------------------------------------------------------------------
/// \brief operator<<
//
/// \param stream
///     The stream to write to.
/// \param att
///     The object to write.
///
/// \return
///     The stream.
///
/// \exception std::exception
///     An unknown exception occurred.
//-----------------------------------------------------------------------
std::ostream& operator<<( std::ostream& stream, const ILwd::Attributes& att );


#if HAVE_LIBOSPACE
OS_CLASS( ILwd::Attributes )
  OS_STREAM_OPERATORS( ILwd::Attributes )
#endif

#endif /* AttributesHH */
