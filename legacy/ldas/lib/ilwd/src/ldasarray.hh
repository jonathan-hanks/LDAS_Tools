/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef LdasArrayHH
#define LdasArrayHH


/// \author David Farnham

#include "ilwd/config.h"

/* ObjectSpace Includes */
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#include <ospace/stream/protocol.h>
#endif // HAVE_LIBOSPACE

   
/* System Header Files */
#include <string>   
   
/* LDAS Includes */
#include <general/types.hh>

/* Local Includes */
#include "ilwd/starttag.hh"
#include "ilwd/endtag.hh"
#include "ilwd/ldasarraybase.hh"
#include "ilwd/style.hh"
#include "ilwd/elementid.hh"

namespace ILwd {

    /* ObjectSpace forward class declarations */
#ifdef HAVE_LIBOSPACE
   
    template< class T > class LdasArray;
   
    template< class T >
    void ospaceWrite( os_bstream& stream, const LdasArray< T >& o );
    template< class T >
    void ospaceWriteHeader( os_bstream& stream, const LdasArray< T >& o );

    template< class T >
    void ospaceRead( os_bstream& stream, LdasArray< T >& o );
    template< class T >
    void ospaceReadHeader( os_bstream& stream, LdasArray< T >& o );
   
    // Template specializations
    template<>   
    void ospaceWrite( os_bstream& stream, const LdasArray< COMPLEX_8 >& o );   
    template<>
    void ospaceRead( os_bstream& stream, LdasArray< COMPLEX_8 >& o );
    template<>   
    void ospaceWrite( os_bstream& stream, const LdasArray< COMPLEX_16 >& o );
    template<>
    void ospaceRead( os_bstream& stream, LdasArray< COMPLEX_16 >& o );
   
   
#endif // HAVE_LIBOSPACE


    //-------------------------------------------------------------------
    /// \brief The ILWD array type.
    ///
    /// This class represents an ILWD array type.
    /// This stores a multi-dimensional
    /// array of a specific type (such as INT_2S).
    //-------------------------------------------------------------------
    template< class T >
    class LdasArray : public LdasArrayBase
    {
    public:
	typedef LdasArrayBase::size_type size_type;
	/* Constructors/Destructor */
	explicit LdasArray( T v = 0, const std::string& name = "", const std::string& units = "",
			    REAL_8 dx = 1., REAL_8 startx = .0, const std::string& dataValueUnit = "" );

	LdasArray( Reader& r );

	LdasArray( Reader& r, const Attributes& att );

	LdasArray( const LdasArray< T >& e );

	LdasArray( const T* v, size_type ndim, const size_type* dims,
		   const std::string& name = "", const std::string* units = 0,
		   const REAL_8* dx = 0, const REAL_8* startx = 0,
		   const std::string& dataValueUnit = "" );

	LdasArray( const T* v, size_type dims, const std::string& name = "",
		   const std::string& units = "" ,
		   REAL_8 dx = 1., REAL_8 startx = .0,
		   const std::string& dataValueUnit = "" );

	LdasArray( T* v, size_type ndim, const size_type* dims,
		   const std::string& name = "", const std::string* units = 0,
		   bool allocate = true, bool owns = true,
		   const REAL_8* dx = 0, const REAL_8* startx = 0,
		   const std::string& dataValueUnit = "" );

	LdasArray( T* v, size_type dims, const std::string& name = "",
		   const std::string& units = "", bool allocate = true,
		   bool owns = true,
		   const REAL_8 dx = 1., const REAL_8 startx = .0,
		   const std::string& dataValueUnit = "" );

	~LdasArray();

	/* Operator overloads */
	const LdasArray& operator=( const LdasArray< T >& e );
   
	///--------------------------------------------------------------
	/// \brief Overloaded += operator.
	/// 
	/// Concatinate two arrays into one.
	///
	/// \param[in] e
	///     The array to concatinate from.
	///
	/// \return
	///     A reference to this object.
	///
	/// \exception std::bad_alloc
	///     Memory allocation failed.
	///--------------------------------------------------------------
	const LdasArray& operator+=( const LdasArray< T >& e );

	bool operator==( const LdasArray< T >& a ) const;
	bool operator!=( const LdasArray< T >& a ) const;

	/* Accessors */
	static const std::string& getIdentifierStatic();
	const std::string& getIdentifier() const;
	size_type getNDim() const;
	size_type getDimension( size_type n ) const;
	std::string getUnits( size_type n ) const;
	std::string getUnitsString( std::string separator = ":" ) const;
	void setUnits( size_type pos, const std::string& s);
	REAL_8 getDx( size_type n ) const;
	REAL_8 getStartX( size_type n ) const;
	std::string getDataValueUnit() const;
	ArrayOrder getArrayOrder() const;
	T* getData();
	const T* getData() const;
   
	/// \brief Get element Id.
	//
	/// \return ElementId - Id of this element.
	ElementId getElementId() const;
	bool isDataOwned() const;

#ifdef HAVE_LIBOSPACE
	static os_protocol::type_type getObjectSpaceType();
#endif

	void setDataOwnership( bool owns );
	void setData( const T* v );
	void setData( T* v, bool allocate = true, bool owns = true );
    
	/* Copy */
	LdasArray* createCopy() const;
    
	/* I/O */
	void read( Reader& r );

    private:

	/* Private helper methods */
	void read( Reader& r, const Attributes& att );

	size_type* readDimAttribute( const Attributes& att, size_type nDim );

	void parseDim( size_type nDim, size_type* dims, const char* s );

	void checkByteOrder( const ByteOrder& byteOrder);
    
	/* I/O */
	ByteOrder readAttributes( const Attributes& att );

	void writeAttributes( Attributes& att ) const;
	virtual void writeData( std::ostream& stream ) const;

	/* Data members */
	size_type  mNDim;
	size_type* mDims;
	std::string* mUnits;
	ArrayOrder mArrayOrder;
	T* mData;

	REAL_8 *mDx; // Scale factors for each coordinate
	REAL_8 *mStartX; // Origin for each data set
	std::string mDataValueUnit;

	static const std::string mIdentifier;
	bool mOwnsData;
    
	/// \cond DOXYGEN_IGNORE
#   ifdef HAVE_LIBOSPACE
	friend void ospaceWrite<>( os_bstream&, const LdasArray< T >& );
	friend void ospaceWriteHeader<>( os_bstream&, const LdasArray< T >& );
	friend void ospaceRead<>( os_bstream&, LdasArray< T >& );
	friend void ospaceReadHeader<>( os_bstream&, LdasArray< T >& );
#   endif // HAVE_LIBOSPACE    
	/// \endcond
    };


    // template specializations    
    template<> ElementId
    LdasArray< CHAR >::getElementId() const;
    template<> ElementId
    LdasArray< CHAR_U >::getElementId() const;
    template<> ElementId
    LdasArray< INT_2S >::getElementId() const;
    template<> ElementId
    LdasArray< INT_2U >::getElementId() const;
    template<> ElementId
    LdasArray< INT_4S >::getElementId() const;
    template<> ElementId
    LdasArray< INT_4U >::getElementId() const;
    template<> ElementId
    LdasArray< INT_8S >::getElementId() const;
    template<> ElementId
    LdasArray< INT_8U >::getElementId() const;
    template<> ElementId
    LdasArray< REAL_4 >::getElementId() const;
    template<> ElementId
    LdasArray< REAL_8 >::getElementId() const;
    template<> ElementId
    LdasArray< COMPLEX_8 >::getElementId() const;
    template<> ElementId
    LdasArray< COMPLEX_16 >::getElementId() const;

    template<> void
    LdasArray< COMPLEX_8 >::checkByteOrder( const ByteOrder& byteOrder );

    template<> void
    LdasArray< COMPLEX_16 >::checkByteOrder( const ByteOrder& byteOrder );
   
   
} // namespace ILwd


namespace std
{
    template< class T >
    std::ostream& operator <<( std::ostream& stream,
			       ILwd::LdasArray< T >& tag );

    template< class T >
    ILwd::Reader& operator >>( ILwd::Reader& r,
                               ILwd::LdasArray< T >& tag );
}
   
    
/* ObjectSpace forward class declarations */
#ifdef HAVE_LIBOSPACE
template< class T >
void os_write( os_bstream& stream, const ILwd::LdasArray< T >& o );
template< class T >
void os_read( os_bstream& stream, ILwd::LdasArray< T >& o );

   
namespace ILwd
{
    template<> inline os_protocol::type_type
    LdasArray< CHAR >::getObjectSpaceType()
    {
	return os_protocol::char8;
    }

    template<> inline os_protocol::type_type
    LdasArray< CHAR_U >::getObjectSpaceType()
    {
	return os_protocol::byte8;
    }
    
    template<> inline os_protocol::type_type
    LdasArray< INT_2S >::getObjectSpaceType()
    {
	return os_protocol::int16;
    }

    template<> inline os_protocol::type_type
    LdasArray< INT_2U >::getObjectSpaceType()
    {
	return os_protocol::uint16;
    }

    template<> inline os_protocol::type_type
    LdasArray< INT_4S >::getObjectSpaceType()
    {
	return os_protocol::int32;
    }

    template<> inline os_protocol::type_type
    LdasArray< INT_4U >::getObjectSpaceType()
    {
	return os_protocol::uint32;
    }

    template<> inline os_protocol::type_type
    LdasArray< INT_8S >::getObjectSpaceType()
    {
	return os_protocol::int64;
    }

    template<> inline os_protocol::type_type
    LdasArray< INT_8U >::getObjectSpaceType()
    {
	return os_protocol::uint64;
    }

    template<> inline os_protocol::type_type
    LdasArray< REAL_4 >::getObjectSpaceType()
    {
	return os_protocol::float32;
    }
    
    template<> inline os_protocol::type_type
    LdasArray< REAL_8 >::getObjectSpaceType()
    {
	return os_protocol::float64;
    }
   
    template<> inline os_protocol::type_type
    LdasArray< COMPLEX_8 >::getObjectSpaceType()
    {
	return os_protocol::float32;
    }

    template<> inline os_protocol::type_type
    LdasArray< COMPLEX_16 >::getObjectSpaceType()
    {
	return os_protocol::float64;
    }
} // namespace - ILwd
   

#endif // HAVE_LIBOSPACE


//-----------------------------------------------------------------------
/// \brief Get the identifier.
///
/// This returns the identifier for this element.  This is the static version.
///
/// \return
///     The identifier.
//-----------------------------------------------------------------------
template< class T >
inline const std::string& ILwd::LdasArray< T >::getIdentifierStatic()
{
    return mIdentifier;
}


//-----------------------------------------------------------------------
/// \brief Get the identifier.
///
/// Returns the identifier for this element.
///
/// \return
///     The identifier.
//-----------------------------------------------------------------------
template< class T > const std::string& ILwd::LdasArray< T >::getIdentifier() 
    const
{
    return getIdentifierStatic();
}


//-----------------------------------------------------------------------
/// \brief Get the array data.
///
/// Returns a pointer to the data for this array.  The index ordering for the
/// data may be found via the getArrayOrder method.
///
/// \return
///     The data.
//-----------------------------------------------------------------------
template< class T >
inline T* ILwd::LdasArray< T >::getData()
{
    return mData;
}

//-----------------------------------------------------------------------
/// \brief Get the array data.
///
/// Returns a pointer to the data for this array.  The index ordering for the
/// data may be found via the getArrayOrder method.
///
/// \return
///     The data.
//-----------------------------------------------------------------------
template< class T >
inline const T* ILwd::LdasArray< T >::getData() const
{
    return mData;
}

//-----------------------------------------------------------------------
/// \brief Get the index ordering.
///
/// This returns the index ordering for the array.  This is only important if
/// this is a multidimensional array.  The possible types are 'C' ordering
/// (row-wise, the last subscript varies fastest) or 'F77' (column-wise).
///
/// \return ArrayOrder
///     The array order
//-----------------------------------------------------------------------
template< class T >
ILwd::ArrayOrder ILwd::LdasArray< T >::getArrayOrder() const
{
    return mArrayOrder;
}


//-----------------------------------------------------------------------
/// \brief Get the number of dimensions in the array.
///
/// \return
///     The number of dimensions.
//-----------------------------------------------------------------------
template< class T >
typename ILwd::LdasArray< T >::size_type ILwd::LdasArray< T >::getNDim() const
{
    return mNDim;
}


//-----------------------------------------------------------------------
/// \brief Get the data ownership.
///
/// \return
///     True is object owns the data. False otherwise.
//-----------------------------------------------------------------------
template< class T >
inline bool ILwd::LdasArray< T >::isDataOwned() const
{
    return mOwnsData;
}


//-----------------------------------------------------------------------
/// \brief Set the data ownership.
///
/// \param[in] owns
///     One of true | false. The value for ownership.   
//-----------------------------------------------------------------------
template< class T >
inline void ILwd::LdasArray< T >::setDataOwnership( bool owns )
{
    mOwnsData = owns;
    return;
}


#endif // LdasArrayHH
