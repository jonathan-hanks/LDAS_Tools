#if HAVE_CONFIG_H
#include "ilwd/config.h"
#endif /* HAVE_CONFIG_H */

#include <sys/types.h>

#include "ldasarraybase.hh"


using ILwd::LdasArrayBase;

   
//-----------------------------------------------------------------------------
//
//: (Default) Constructor.
//
// This creates an object with the specified name and comment.
//
//!param: const std::string& name - The name for the array.  Default: An empty
//!param:     string.
//!param: const std::string& comment - A comment.  Default: An empty string.
//
ILwd::LdasArrayBase::LdasArrayBase(
   const std::string& name, const std::string& comment )
   : LdasFormatElement( name, comment )
{}


//-----------------------------------------------------------------------------
//
//: Copy Constructor.
//
//!param: const LdasArrayBase& e - The object to copy.
//
ILwd::LdasArrayBase::LdasArrayBase( const LdasArrayBase& e )
   : LdasFormatElement( e )
{}

   

//-----------------------------------------------------------------------------
//
//: Calculates the number of items in the array.
//
//!return: size_type - The number of items in the array.
//
LdasArrayBase::size_type LdasArrayBase::getNData() const
{
    size_type ndim( getNDim() );
    if ( ndim == 0 )
    {
        return 0;
    }

    size_type size = 1;
    for ( size_type i = 0; i < ndim; ++i )
    {
        size *= getDimension( i );
    }

    return size;
}


//-----------------------------------------------------------------------------
//
//: Destructor.
//
// The destructor.
//
LdasArrayBase::~LdasArrayBase()
{}



#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include "ospaceid.hh"

OS_NO_FACTORY_STREAMABLE_1( (ILwd::LdasArrayBase*),
                            Stream::ILwd::LDASARRAYBASE,
                            (ILwd::LdasFormatElement*) )

void os_write( os_bstream& stream, const ILwd::LdasArrayBase& o )
{
    os_write( stream, (const ILwd::LdasFormatElement&)o );
    return;
}


void os_read( os_bstream& stream, ILwd::LdasArrayBase& o )
{
    os_read( stream, (ILwd::LdasFormatElement&)o );
    return;
}

#endif // HAVE_LIBOSPACE
