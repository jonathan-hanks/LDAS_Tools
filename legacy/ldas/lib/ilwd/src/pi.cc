#include "ilwd/config.h"

#include <sstream>
   
#include "pi.hh"



using ILwd::PI;
using ILwd::Reader;
using namespace std;   


//-----------------------------------------------------------------------------
//
//: Default constructor.
//
// Creates a PI with a target of "none" and no data.
//
PI::PI() 
   : mPITarget( "none" ), mData()
{}


//-----------------------------------------------------------------------------
//
//: Input constructor.
//
// Reads a PI from a reader.  The PI must be the next element on the reader
// or an exception will be thrown.
//
//!param: Reader& r - The reader to read from.
//
//!except: std::bad_alloc - Memory allocation failed.
//!except: StreamException - An error reading from the stream occurred.
//!except: FormatException - The format for the PI was invalid.
//
PI::PI( Reader& r )
{
    read( r );
}


//-----------------------------------------------------------------------------
//
//: Copy constructor.
//
//!param: const PI& o - The object to copy.
//
//!except: std::bad_alloc - Memory allocation failed.
//
PI::PI( const PI& o )
  : Base( o ),
    mPITarget( o.mPITarget ),
    mData( o.mData )
{}


//-----------------------------------------------------------------------------
//
//: Destructor.
//
PI::~PI()
{}


//-----------------------------------------------------------------------------
//   
//: Gets class Id.
//
//!return: ClassType - ID for this type of object. It's ID_PI.
//   
ILwd::ClassType PI::getClassId() const
{
   return ID_PI;
}
   
   
//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const PI& o - The object to assign from.
//
//!return: const PI& - A reference to this object.
//
//!except: std::bad_alloc - Memory allocation failed.
//
const PI& PI::operator=( const PI& o )
{
    if ( this != &o )
    {
        Base::operator=( o );
        mPITarget = o.mPITarget;
        mData = o.mData;
    }
    
    return *this;
}


//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another PI for equality.
//
//!param: const PI& o - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool PI::operator==( const PI& o ) const
{
    return ( Base::operator==( o ) &&
             mPITarget == o.mPITarget &&
             mData == o.mData );
}

   
//-----------------------------------------------------------------------------
//
//: Inequality operator.
//
//!param: const PI& o - The object to compare to.
//
//!return: bool - true if the objects are not equal.  False otherwise.
//
bool ILwd::PI::operator!=( const ILwd::PI& o ) const 
{   
    return !operator==( o );
}


//-----------------------------------------------------------------------------
//
//: PI insertion operator.
//
// Writes this XML processing instruction to a stream in XML format.
//
//!param: ostream& stream - The stream to write to.
//!param: const PI& o - The PI to write.
//
//!return: ostream& - The stream.
//
std::ostream& operator<<( std::ostream& stream, const ILwd::PI& o )
{
    o.write( stream );
    return stream;
}
   
   
   
//-----------------------------------------------------------------------------
//
//: Read.
//
//!param: Reader& r - The reader to read from.
//
//!except: std::bad_alloc - Memory allocation failed.
//!except: StreamException - An error reading from the stream occurred.
//!except: FormatException - The format for the PI was invalid.
//
void PI::read( Reader& r )
try
{
    if ( !r.readString( "<?" ) )
    {
        throw ILWD_FORMATEXCEPTION_INFO(
            Errors::MISSING_DELIMITER, "The opening delimiter for a PI was not "
            "found." );
    }

    std::string target;
    target = r.readName(); // May throw StreamException, FormatException

    // :TODO: should scan target for variants of xml

    ostringstream ss;
    bool finish = false;

    while ( !finish )
    {
        int c = r.get(); // May throw StreamException

        if ( c == '?' )
        {
            c = r.get(); // May throw StreamException

            if ( c == '>' )
            {
                finish = true;
            }
            else
            {
                ss << '?';
            }
        }
        else
        {
            ss << c;
        }
    }

    mData = ss.str();

    return;
}
ADD_ILWD_FORMATEXCEPTION_INFO(
    Errors::INVALID_PI, "The PI was not valid ILWD." )


//-----------------------------------------------------------------------------
//
//: Write the PI to a stream.
//
//!param: std::ostream& stream - The stream to write to.
//
//!except: An unknown exception was thrown.
//
void PI::write( std::ostream& stream ) const
{
    stream << "<?" << mPITarget << mData << "?>";
   
    return;
}

            
            
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::PI*), Stream::ILwd::PI, (ILwd::Base*) )

void os_write( os_bstream& stream, const PI& o )
{
    os_write( stream, (ILwd::Base&)o );    
    stream << o.mPITarget << o.mData;
   
    return;
}

void os_read( os_bstream& stream, PI& o )
{
    os_read( stream, (ILwd::Base&)o );
    stream >> o.mPITarget >> o.mData;
   
    return;
}

#endif // HAVE_LIBOSPACE
