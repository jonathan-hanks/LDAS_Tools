
namespace XML
{
    extern const char* S;

    extern const char* Letter;

    extern const char* Digit;

    extern const char* NameChar;

    extern const char* Name;
    enum
    {
        MATCHES_NAME = 1
    };

    extern const char* EntityReference;
    enum
    {
        MATCHES_ENTITYREFERENCE = 1
    };

    extern const char* Reference;
    enum
    {
        MATCHES_REFERENCE = 1
    };
    
    extern const char* AttValue;
    enum
    {
        M_ATTVALUE_PLUS_DELIMITER = 1,
        M_ATTVALUE_QUOTE          = 2,
        M_ATTVALUE_APOSTROPHE     = 3,
        MATCHES_ATTVALUE          = 4
    };
    
    extern const char* Eq;
    enum
    {
        MATCHES_EQ = 1
    };
    
    extern const char* Attribute;
    enum
    {
        M_ATTRIBUTE_NAME                 = 1,
        M_ATTRIBUTE_VALUE_PLUS_DELIMITER = 2,
        M_ATTRIBUTE_VALUE_QUOTE          = 3,
        M_ATTRIBUTE_VALUE_APOSTROPHE     = 4,
        MATCHES_ATTRIBUTE                = 5
    };

    extern const char* STag;
    enum
    {
        M_STAG_IDENTIFIER = 1,
        M_STAG_ATTRIBUTES = 2,
        MATCHES_STAG      = 3
    };
    
    extern const char* ETag;
    enum
    {
        M_ETAG_IDENTIFIER = 1,
        MATCHES_ETAG      = 2
    };

    const int M_ALL = 0;
}

    
