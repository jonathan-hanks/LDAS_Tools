#ifndef CDataHH
#define CDataHH

//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif


#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE


// System Includes
#include <iosfwd>


// Local Includes
#include "base.hh"
#include "reader.hh"


namespace ILwd
{
    class CData;
}


#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::CData& base );
void os_read( os_bstream& stream, ILwd::CData& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------------
//
//: XML CData.
//
class ILwd::CData : public ILwd::Base
{
public:

    /* Constructors / Destructor */
    CData();
    CData( Reader& r );
     CData( const CData& o );

    virtual ~CData();

    /* Operator Overloads */
    const CData& operator=( const CData& o );
    bool operator==( const CData& o ) const;
    bool operator!=( const CData& o ) const;

    /* I/O */
    virtual void read( Reader& r );
    virtual void write( std::ostream& stream ) const;

    //: Reads class ID.
    //
    //!return: ClassType - ID.
    //
    ClassType getClassId() const;

private:

    std::string mData;

#ifdef HAVE_LIBOSPACE
    friend void ::os_write( os_bstream&, const CData& );
    friend void ::os_read( os_bstream&, CData& );
#endif // HAVE_LIBOSPACE
};



//-----------------------------------------------------------------------------
//
//: Not equal comparison operator.
//
// This compares this object to another array for inequality.
//
//!param: const CData& o - The object to compare with.
//
//!return: bool - true if the arrays are not equal.
//
inline bool ILwd::CData::operator!=( const ILwd::CData& o ) const
{
    return !operator==( o );
}

 
//-----------------------------------------------------------------------------
//
//: CData Stream insertion.
//
// Writes the CData as XML to a stream.
//
//!param: ostream& stream - The stream to write to.
//!param: const CData& o - The object to write.
//
//!return: ostream& - The stream.
//
inline std::ostream& operator<<( std::ostream& stream, const ILwd::CData& o )
{
    o.write( stream );
    return stream;
}


#ifdef HAVE_LIBOSPACE

OS_POLY_CLASS( ILwd::CData )
OS_STREAM_OPERATORS( ILwd::CData )

#endif // HAVE_LIBOSPACE

#endif // CDataHH
