/* -*- mode: c++ -*- */

#ifndef LDAS_HISTORY_HH
#define	LDAS_HISTORY_HH

#include <stdexcept>
#include <string>
#include <vector>
#include <typeinfo>

namespace ILwd {
  class LdasContainer;
  class LdasElement;

  //: Maintains a single record of history
  //
  // This class represent a single record of history. A history record has
  // the description of the action and a list of ILwds which represet the
  // significant arguments passed to the action.
  //
  class LdasHistoryRecord {
  public:
    //: Constructor
    LdasHistoryRecord(const std::string& Description);
    //: Constructor
    LdasHistoryRecord(const std::string& Description,
		      const std::vector<const ILwd::LdasElement*>& Args);
    //: Constructor
    LdasHistoryRecord(const ILwd::LdasContainer* Record);
    //: Copy Constructor
    LdasHistoryRecord( const ILwd::LdasHistoryRecord& Record );

    //: 
    ~LdasHistoryRecord( );

    //: Retrieve the history record.
    ILwd::LdasContainer* GetHistoryRecordAsIlwd(bool RelinquishOwnership = false);

    //: Test if ilwd is a history record
    //
    //!param: ILwd::LdasElement* Element - ILwd element to test
    //
    //!return bool - true if the ilwd is a history record, false otherwise
    //
    static bool IsHistoryRecord(const ILwd::LdasElement* Element);

  private:
    // True if m_record owned by this object
    bool			m_owned;
    // Pointer to LDAS container containing a single record of history info
    const ILwd::LdasContainer*	m_record;
  };

  //: Wrapper for History ILwd data
  //
  // This class provides a wrapper around LDAS's Internal Light Weight Data
  // type which represents history information. This class contains a list
  // of History Records (ILwd::LdasHistoryRecord). Below is an example of a
  // History Container in ILwd format.
  // <br>
  // <samp>
  // &lt;ilwd name='LDAS_History' size='2'&gt;<br>
  // &nbsp;&nbsp;&lt;ilwd name='datacondAPI:resample' size='2'&gt;<br>
  // &nbsp;&nbsp;&nbsp;&nbsp;&lt;int_4 name='upsampling ratio'&gt;1&lt;/int_4&gt;<br>
  // &nbsp;&nbsp;&nbsp;&nbsp;&lt;int_4 name='downsampling ratio'&gt;2&lt;/int_4&gt;<br>
  // &nbsp;&nbsp;&lt;/ilwd&gt;<br>
  // &nbsp;&nbsp;&lt;ilwd name='methodII:line_removal' size='3'&gt;<br>
  // &nbsp;&nbsp;&nbsp;&nbsp;&lt;real_4 name='lines' dims='3' units='HZ'&gt;60.0 180.0 360.0&lt;/real_4&gt;<br>
  // &nbsp;&nbsp;&lt;/ilwd&gt;<br>
  // &lt;/ilwd&gt;<br>
  // </samp>
  //
  class LdasHistory {
  public:
    //: exception
    class NonHistoryContainer: public std::runtime_error
    {
    public:
      //: Constructor
      NonHistoryContainer();
    };

    //: Constructor
    LdasHistory();

    //: Constructor
    //
    // This constructor can be used when an ILwd already exists and the
    // programmer desires to have an easier way to add additional history
    // information.
    //
    //!param: LdasContainer* Container - Pointer from which to create history
    //        structure.
    //
    //!return: new instance of an ILwd::LdasHistory
    //
    //!exc: NonHistoryContainer - thrown if Container is not a container
    //+     suitable for history information.
    //
    LdasHistory(LdasContainer* Container);
    
    //: Constructor
    //
    // This constructor can be used when an ILwd already exists and the
    // programmer desires to have an easier way to add additional history
    // information.
    //
    //!param: const LdasContainer* Container - Pointer from which to create history
    //        structure.
    //
    //!return: new instance of an ILwd::LdasHistory
    //
    //!exc: NonHistoryContainer - thrown if Container is not a container
    //+     suitable for history information.
    //
    LdasHistory(const LdasContainer* Container);

    //: Constructor
    LdasHistory(const LdasHistory& History);
    
    //: Destructor
    ~LdasHistory();

    //: Retrieve a pointer to the history container
    //
    //!return: const ILwd::LdasContainer& - reference to ILwd containing
    //+        the complete history information.
    const ILwd::LdasContainer& GetHistoryAsILwd() const;

    //: Add the contents of another history record to this one.
    //
    //!param: const LdasHistory& History - source for data to be inserted.
    void Append( const LdasHistory& History );

    //: Add a history record
    //
    //!param: const std::string& Description - Description of action
    //!param: const ILwd::LdasElement* Element - Pointer to first element.
    //!param: ... - 0 or more ILwd::LdasElement pointers representing the
    //+       the arguments to the action.
    //
    //!return: none
    void AppendRecord(const std::string& Description,
		      const ILwd::LdasElement* Element, ... );

    //: Add a history record
    //
    //!param: const LdasHistoryRecord& Record - Formatted history record
    //
    //!return: none
    void AppendRecord( const LdasHistoryRecord& Record );

    //: Test if ilwd is a history container
    //
    //!param: ILwd::LdasElement* Element - ILwd element to test
    //
    //!return bool - true if the ilwd is a history container, false otherwise
    //
    static bool IsHistoryContainer(const ILwd::LdasElement* Element);

    //: Return the number of History records
    unsigned int size ( ) const;

    //: Assignment opperator
    ILwd::LdasHistory& operator=(const ILwd::LdasHistory& History);

  private:
    //: Name of history container
    static const std::string	TAG;

    //: ilwd container which contains the history information.
    LdasContainer*		m_history;

    typedef std::vector<LdasHistoryRecord>	history_record_list_type;
    //: list of history records
    history_record_list_type	m_history_records;

    //: True if the container was created by this class
    bool			m_own_history;

    //: Initialization
    //
    //!return: none
    void init();
  }; /* class LdasHistory */

  inline unsigned int LdasHistory::
  size( ) const
  {
    return m_history_records.size( );
  }
} /* namespace */


#endif	/* LDAS_HISTORY_HH */
