#include "ilwd/config.h"

#include <exception>
#include <stdexcept>

#include "general/toss.hh"

#include "ilwd/ilwdtoc.hh"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldasstring.hh"

#if 0
#define AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl
#else	/* 0 */
#define	AT()
#endif	/* 0 */
using namespace ILwd;

//=======================================================================
//=======================================================================

typedef General::unordered_map<TOC::key_type, ILwd::LdasElement*> mapping_type;

//=======================================================================
// local exceptions
//=======================================================================

class bad_key: public std::range_error
{
public:
  bad_key( const std::string& Key );
  
private:
  static std::string  format( const std::string& Key );
};

//=======================================================================
//=======================================================================

namespace ILwd
{
  struct TOC::private_data
  {
  public:
    private_data( const ILwd::LdasContainer& Source,
		  const TOC::Keys_type& Keys );

    private_data* Clone( );

    const ILwd::LdasContainer&	m_source;
    const TOC::Keys_type&		m_keys;
    mapping_type			m_mapping;
    const TOC::RKeys_type		m_reverse_lookup;

  private:
    void map_container( );
  };

  //=======================================================================
  //=======================================================================

  TOC::
  TOC( const ILwd::LdasContainer& Source, const Keys_type& Keys )
    : m_private( *(new private_data( Source, Keys ) ) )
  {
  }

  TOC::
  TOC( const ILwd::LdasElement& Source, const Keys_type& Keys )
    : m_private( *(new private_data
		   ( dynamic_cast<const ILwd::LdasContainer&>(Source),
		     Keys ) ) )
  {
  }

  TOC::
  TOC( const TOC& Source )
    : m_private( *( Source.m_private.Clone() ) )
  {
  }

  TOC::
  ~TOC( )
  {
    delete &m_private;
  }

  TOC::RKeys_type TOC::
  GenReverseLookupMap( const Keys_type& Keys )
  {
    RKeys_type	retval;
    for ( TOC::Keys_type::const_iterator kw( Keys.begin( ) );
	  kw != Keys.end( );
	  kw ++ )
    {
      retval[ (*kw).second ] = (*kw).first;
    }
    return retval;
  }

  const ILwd::LdasContainer& TOC::
  GetContainer( ) const
  {
    return m_private.m_source;
  }

  template<>
  std::string TOC::
  GetData<std::string>( key_type Key ) const
  {
    mapping_type::const_iterator	pos = m_private.m_mapping.find( Key );

    AT();
    if ( pos == m_private.m_mapping.end() )
    {
      AT();
      std::string key_string;
      try
      {
	AT();
	key_string = ReverseLookup( Key );
      }
      catch( ... )
      {
	AT();
	key_string = "<unknown>";
      }
      AT();
      throw bad_key( key_string );
    }
    const ILwd::LdasString* e
      ( dynamic_cast<const ILwd::LdasString*>( (*pos).second ) );
    AT();
    if ( e )
    {
      AT();
      return e->getString( );
    }
    AT();
    throw std::domain_error( "Cannot cast to requested type" );
  }

  template<>
  std::string ILwd::TOC::
  GetData<std::string>( key_type Key, const std::string& Default ) const
  {
    AT();
    mapping_type::const_iterator	pos = m_private.m_mapping.find( Key );

    AT();
    if ( pos == m_private.m_mapping.end() )
    {
      AT();
      return Default;
    }
    const ILwd::LdasString* e
      ( dynamic_cast<const ILwd::LdasString*>( (*pos).second ) );
    if ( e )
    {
      AT();
      return e->getString( );
    }
    AT();
    return Default;
  }

  template<class Type_>
  Type_ ILwd::TOC::
  GetData( key_type Key ) const
  {
    AT();

    mapping_type::const_iterator	pos = m_private.m_mapping.find( Key );

    AT();
    if ( pos == m_private.m_mapping.end() )
    {
      AT();
      throw bad_key( ReverseLookup( Key ) );
    }
    const ILwd::LdasArray< Type_ >* e
      ( dynamic_cast<const ILwd::LdasArray< Type_ >*>( (*pos).second ) );
    AT();
    if ( e )
    {
      AT();
      return e->getData()[0];
    }
    AT();
    throw std::domain_error( "Cannot cast to requested type" );
  }

  template<class Type_>
  Type_ ILwd::TOC::
  GetData( key_type Key, const Type_& Default ) const
  {
    mapping_type::const_iterator	pos = m_private.m_mapping.find( Key );

    if ( pos == m_private.m_mapping.end() )
    {
      return Default;
    }
    const ILwd::LdasArray< Type_ >* e
      ( dynamic_cast<const ILwd::LdasArray< Type_ >*>( (*pos).second ) );
    if ( e )
    {
      return e->getData()[0];
    }
    return Default;
  }

  template<class Type_>
  const Type_& TOC::
  GetILwd( key_type Key ) const
  {
    mapping_type::const_iterator	pos = m_private.m_mapping.find( Key );
    if ( pos == m_private.m_mapping.end() )
    {
      throw std::range_error( "Key not found in ilwd" );
    }
    return dynamic_cast<const Type_&>(*(*pos).second );
  }

  template<class Type_>
  Type_& TOC::
  GetILwd( key_type Key )
  {
    mapping_type::const_iterator	pos = m_private.m_mapping.find( Key );
    if ( pos == m_private.m_mapping.end() )
    {
      throw std::range_error( "Key not found in ilwd" );
    }
    return const_cast<Type_&>( dynamic_cast<const Type_&>(*(*pos).second ) );
  }

  template<class Type_>
  void TOC::
  SetData( key_type Key, const Type_& Value ) const
  {
    mapping_type::const_iterator	pos = m_private.m_mapping.find( Key );

    if ( pos == m_private.m_mapping.end() )
    {
      throw std::range_error( "Object index does not exist in Container" );
    }

    ILwd::LdasArray< Type_ >* e
      ( dynamic_cast<ILwd::LdasArray< Type_ >*>( (*pos).second ) );
    if ( e )
    {
      e->getData()[0] = Value;
      return;
    }
  }

  const std::string& TOC::
  ReverseLookup( key_type Key ) const
  {
    const TOC::RKeys_type::const_iterator
      pos( m_private.m_reverse_lookup.find( Key ) );

    if ( pos == m_private.m_reverse_lookup.end() )
    {
      General::toss<std::range_error>( "General::TOC::ReverseLookup",
				       __FILE__,
				       __LINE__,
				       "Key is out of range" );
    }
    return (*pos).second;
  }

#define	INSTANTIATE(Type_) \
template Type_ \
ILwd::TOC::GetData<Type_>( TOC::key_type ) const; \
template Type_ \
ILwd::TOC::GetData<Type_>( TOC::key_type, const Type_& )  const; \
template const ILwd::LdasArray<Type_> & \
ILwd::TOC::GetILwd<ILwd::LdasArray<Type_> >( TOC::key_type )  const; \
template ILwd::LdasArray<Type_> & \
ILwd::TOC::GetILwd<ILwd::LdasArray<Type_> >( TOC::key_type ); \
template void \
ILwd::TOC::SetData<Type_>( key_type Key, const Type_ & Value ) const

  /// \cond EXCLDUE
  INSTANTIATE(CHAR);
  INSTANTIATE(INT_2S);
  INSTANTIATE(INT_2U);
  INSTANTIATE(INT_4S);
  INSTANTIATE(INT_4U);
  INSTANTIATE(REAL_4);
  INSTANTIATE(REAL_8);
  /// \endcond

#undef INSTANTIATE

  template const ILwd::LdasContainer &
  TOC::GetILwd<ILwd::LdasContainer>( TOC::key_type )  const;
  template ILwd::LdasContainer &
  TOC::GetILwd<ILwd::LdasContainer >( TOC::key_type );

  //=======================================================================
  // struct TOC::private_data
  //=======================================================================

  TOC::private_data::
  private_data( const ILwd::LdasContainer& Source, const TOC::Keys_type& Keys )
    : m_source( Source ),
      m_keys( Keys ),
      m_reverse_lookup( TOC::GenReverseLookupMap( Keys ) )
  {
    map_container( );
  }

  TOC::private_data* TOC::private_data::
  Clone( )
  {
    private_data*	retval( new private_data( m_source, m_keys ) );
    return retval;
  }

  void TOC::private_data::
  map_container( )
  {
    const Keys_type::const_iterator kw_end( m_keys.end() );

    for( LdasContainer::const_iterator e = m_source.begin();
	 e != m_source.end();
	 e++ )
    {
      Keys_type::const_iterator kwi( m_keys.begin() );

      while( kwi != m_keys.end() )
      {
	if ( (*kwi).first.operator[](0) == ':' )
	{
	  std::string::size_type	p =
	    (*e)->getNameString().rfind( (*kwi).first );
	  if ( p == ( (*e)->getNameString().size() - (*kwi).first.size() ) )
	  {
	    break;
	  }
	}
	else
	{
	  if ( (*kwi).first == (*e)->getNameString() )
	  {
	    break;
	  }
	}
	kwi++;
      }

      if ( kwi != kw_end )
      {
	m_mapping[ (*kwi).second ] = *e;
      }
    }
  }
} // namespace - ILwd

//=======================================================================
// bad_key
//=======================================================================

bad_key::
bad_key( const std::string& Key )
  : range_error( format(Key) )
{
  AT();
}
    
std::string bad_key::
format( const std::string& Key )
{
  AT();
  std::string result( "Requested Key(" );
  result += Key;
  result += ") does not exist in ILwd::TOC";
  AT();
  return result;
}
