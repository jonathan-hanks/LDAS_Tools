#ifndef CharDataHH
#define CharDataHH


#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

/* System Includes */
#include <string>
#include <exception>

/* Local Includes */
#include "base.hh"
#include "reader.hh"


namespace ILwd
{
  class CharData;
}

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::CharData& base );
void os_read( os_bstream& stream, ILwd::CharData& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------
/// \brief XML CharData
//-----------------------------------------------------------------------
class ILwd::CharData : public ILwd::Base
{
public:

  //---------------------------------------------------------------------
  /// \brief ( Default ) Constructor.
  //
  // Constructs the object.
  //---------------------------------------------------------------------
  CharData() : mData() {}
  CharData( Reader& r );
  //---------------------------------------------------------------------
  /// \brief Destructor. 
  //
  // Destructs the object. 
  //---------------------------------------------------------------------
  virtual ~CharData();

  /* Operator Overloads */
  const CharData& operator=( const CharData& c );
  bool operator==( const CharData& c ) const;
  bool operator!=( const CharData& c ) const;
    
  ClassType getClassId() const;

  /* I/O */
  void write( std::ostream& stream ) const;
    
private:

  std::string mData;

#ifdef HAVE_LIBOSPACE
  friend void ::os_write( os_bstream&, const CharData& );
  friend void ::os_read( os_bstream&, CharData& );
#endif // HAVE_LIBOSPACE
};



//-----------------------------------------------------------------------
/* Inline Methods */
//-----------------------------------------------------------------------


//-----------------------------------------------------------------------
/// \brief Inequality operator.
///
/// \param[in] c
///     The object to compare to.
///
/// \return
///     true if the objects are not equal.  False otherwise.
//-----------------------------------------------------------------------
inline bool ILwd::CharData::operator!=( const CharData& c ) const
{
  return !operator==( c );
}


//-----------------------------------------------------------------------
/// \brief Writes character data to a stream.
///
/// Currently the char data is just
/// written out literally, it doesn't put it inside of a tag.
///
/// \param[out] stream
///     The output stream.
//-----------------------------------------------------------------------
inline void ILwd::CharData::write( std::ostream& stream ) const
{
  stream << mData;
  return;
}


//-----------------------------------------------------------------------
/// \brief Writes the character data to a stream.
///
/// \param[out] stream
///     The output stream.
/// \param[in] o
///     The object to write.
///
/// \return
///     The stream.
//-----------------------------------------------------------------------
inline std::ostream& operator<<( std::ostream& stream,
				 const ILwd::CharData& o )
{
  o.write( stream );
  return stream;
}


#ifdef HAVE_LIBOSPACE

OS_POLY_CLASS( ILwd::CharData )
  OS_STREAM_OPERATORS( ILwd::CharData )

#endif // HAVE_LIBOSPACE


#endif // CharDataHH
