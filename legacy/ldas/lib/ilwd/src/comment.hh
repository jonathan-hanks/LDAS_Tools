#ifndef CommentHH
#define CommentHH


#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

/* ObjectSpace Includes */
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

/* System Includes */
#include <exception>

/* LDAS Includes */
#include <general/formatexception.hh>

/* Local Includes */
#include "base.hh"
#include "reader.hh"
#include "errors.hh"


namespace ILwd
{
  class Comment;
}

#ifdef HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::Comment& base );
void os_read( os_bstream& stream, ILwd::Comment& base );
#endif // HAVE_LIBOSPACE

//-----------------------------------------------------------------------
/// \brief XML Comment.
///
/// This class represents an XML comment.
//-----------------------------------------------------------------------
class ILwd::Comment : public ILwd::Base
{
public:

  Comment( const std::string& comment="" );
  Comment( const Comment& c );
  Comment( Reader& r );
 
  virtual ~Comment();

  /* Operator overloads */
  const Comment& operator=( const Comment& c );
  bool operator==( const Comment& c ) const;
  bool operator!=( const Comment& c ) const;

  /* Accessors */
  const std::string& getComment() const;
  ClassType getClassId() const;

  /* Mutators */
  void setComment( const std::string& c );

  /* I/O */
  virtual void write( std::ostream& stream ) const;
  virtual void read( Reader& r );

private:
    
  static bool isComment( const std::string& s );

  std::string mComment;

#ifdef HAVE_LIBOSPACE
  friend void ::os_write( os_bstream&, const Comment& );
  friend void ::os_read( os_bstream&, Comment& );
#endif // HAVE_LIBOSPACE
};


//-----------------------------------------------------------------------
/// \brief Inequality operator.
///
/// \param[in] c
///     The object to compare to.
///
/// \return
///     true if the objects are not equal.  False otherwise.
//-----------------------------------------------------------------------
inline bool ILwd::Comment::
operator!=( const Comment& c ) const
{
  return !operator==( c );
}


//-----------------------------------------------------------------------
/// \brief Returns the comment.
///
/// \return
///     The comment.
//-----------------------------------------------------------------------
inline const std::string& ILwd::
Comment::getComment() const
{
  return mComment;
}


/* ObjectSpace */
#ifdef HAVE_LIBOSPACE

OS_POLY_CLASS( ILwd::Comment )
OS_STREAM_OPERATORS( ILwd::Comment )

#endif // HAVE_LIBOSPACE


#endif // CommentHH
