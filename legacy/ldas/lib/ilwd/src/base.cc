#include "ilwd/config.h"

#include <string>
   
// Local Includes
#include "base.hh"
#include "starttag.hh"
#include "endtag.hh"
#include "comment.hh"
#include "pi.hh"
#include "cdata.hh"
#include "chardata.hh"

using ILwd::Base;
using ILwd::Reader;   
using namespace std;   

   
Base::~Base()
{}

   
//-----------------------------------------------------------------------------
//
//: Reads a single ILWD object from a reader.
//
//!param: Reader& r - A reference to the reader.
//
//!return: Base* - A pointer to the read Base object.
// 
//!exc: bad_alloc - Memory allocation failed.   
//!exc: StreamException - An error reading from the stream occurred. 
//!exc: FormatException - The format for the PI was invalid.
//
Base* Base::createObject( Reader& r )
{
    // Check to see if a '<' is coming next.
    int p = r.peek();
    if ( p != '<' )
    {
        return new CharData( r );
    }

    p = r.get();
    p = r.peek();

    switch( p )
    {
        case '/':
            r.pushback( '<' );
            return new EndTag( r );
            break;

        case '!':

            p = r.get();
            p = r.peek();

            switch( p )
            {
                case '-':
                    r.pushback( "<!" );
                    return new Comment( r );

                case '[':
                    r.pushback( "<!" );
                    return new CData( r );

                default:

                    string msg( "I found an ILWD object starting with \"<!" );
                    msg += static_cast< char >( p );
                    msg += "\" but I don't know what it is.";
                    throw ILWD_FORMATEXCEPTION_INFO(
                        Errors::UNKNOWN_OBJECT, msg );
            }        

        case '?':
            r.pushback( '<' );
            return new PI( r );
            
        default:
            if ( Reader::isNameChar( p ) )
            {
                // It must be a start tag
                // What about empty element tag?
                r.pushback( '<' );
                return new StartTag( r );
            }
            else
            {
                // Maybe we should try some error recovery here
                string msg( "I found \"<" );
                msg += static_cast< char >( p );
                msg += "\" on the stream, but I don't know what it means.";
                throw ILWD_FORMATEXCEPTION_INFO( Errors::UNKNOWN_OBJECT, msg );
            }
            break;
    }
   
    return 0;
}

   
//-----------------------------------------------------------------------------
//
//: Read a fundamental XML object into a pointer.
//
// An object is read from the stream, memory allocated, and a pointer to the
// object returned through a parameter.
//
//!param: Reader& r
//!param: Base*&
//
//!return: Reader&
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error reading from the stream occurred.
//!exc: FormatException - The format for the PI was invalid.
//
Reader& operator>>( Reader& r, Base*& o )
{
    o = Base::createObject( r );
    return r;
}
   
   
//-----------------------------------------------------------------------------
//
// Writes a Base object to a stream.
//
//!param: std::ostream& stream
//!param: const Base& object
//
//!return: std::ostream&
//
//!exc: std::exception
//
std::ostream& operator<<( std::ostream& stream, const ILwd::Base& object )
{
    object.write( stream );
    return stream;
}
   
   
#ifdef HAVE_LIBOSPACE

#include <ospace/source.h>
#include "ospaceid.hh"

OS_NO_FACTORY_STREAMABLE_0( (ILwd::Base*), Stream::ILwd::BASE )

void os_write( os_bstream& stream, const ILwd::Base& base )
{
   return;
}


void os_read( os_bstream& stream, ILwd::Base& base )
{
   return;
}

#endif // HAVE_LIBOSPACE
