#ifndef StyleHH
#define StyleHH

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

// ObjectSpace Includes
#if HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif /* HAVE_LIBOSPACE */

#include "attributes.hh"

namespace ILwd {

// Storage Formats
    enum Format
    {
        BINARY,
        BASE64,
        ASCII,
        USER_FORMAT
    };

    enum Compression
    {
        NO_COMPRESSION,
        GZIP,
        GZIP0,
        GZIP1,
        GZIP2,
        GZIP3,
        GZIP4,
        GZIP5,
        GZIP6,
        GZIP7,
        GZIP8,
        GZIP9,
        USER_COMPRESSION
    };

    class Style;
} // namespace


//-----------------------------------------------------------------------------
//   
//: Represents LDAS style element.   
//   
class ILwd::Style
{
public:

  //--------------------------------------------------------------------------
  /// \brief Constructor.
  ///
  /// \param f
  ///     The format.
  /// \param c
  ///     The compression.   
  //--------------------------------------------------------------------------
  explicit Style( Format f = ASCII, Compression c = NO_COMPRESSION );

  //--------------------------------------------------------------------------
  /// \brief Copy constructor
  ///
  /// \param s
  ///     A reference to the object to copy.
  //--------------------------------------------------------------------------
  Style( const Style& s );

  //-------------------------------------------------------------------------
  /// \brief Assignment operator.
  ///
  /// \param s
  ///     The style to assign from.
  ///
  /// \return
  ///     A reference to this style
  //-------------------------------------------------------------------------
  const Style& operator=( const Style& s );

  //--------------------------------------------------------------------------
  /// \brief Equal comparison operator.
  ///
  /// \param s
  ///     The object to compare with.
  ///
  /// \return
  ///     true if the objects are equal.
  //--------------------------------------------------------------------------
  bool operator==( const Style& s ) const;

  //--------------------------------------------------------------------------
  /// \brief Not equal comparison operator.
  ///
  /// \param s
  ///     The object to compare with.
  ///
  /// \return
  ///     true if the objects are not equal.
  //--------------------------------------------------------------------------
  bool operator!=( const Style& s ) const;
    
  /* Accessors */
  //--------------------------------------------------------------------------
  /// \brief Gets format for writing.
  ///
  /// \return
  ///     Format - Format for writing.
  //--------------------------------------------------------------------------
  Format getWriteFormat() const;

  //--------------------------------------------------------------------------
  /// \brief Gets compression for writing.
  ///
  /// \return
  ///     Compression for writing.
  //--------------------------------------------------------------------------
  Compression getWriteCompression() const;

  /* Mutators */
  //--------------------------------------------------------------------------
  /// \brief Sets format for writing.
  ///
  /// \param f
  ///     Format for writing.
  //--------------------------------------------------------------------------
  void setWriteFormat( Format f );

  //--------------------------------------------------------------------------
  /// \brief Sets compression for writing.
  ///
  /// \param c
  ///     Compression for writing.
  //--------------------------------------------------------------------------
  void setWriteCompression( Compression c );

  /* Conversion */
  //---------------------------------------------------------------------
  /// \brief Converts internal data from specified format into binary.
  ///
  /// \param source
  ///     A null-terminated string containing the formated data to translate.
  /// \param size
  ///     The size of the buffer.   
  /// \param destSize
  ///     The number of elements to translate.
  ///
  /// \return
  ///     A pointer to the allocated memory containing the
  ///     uncompressed data. Caller is responsible for memory destruction.
  ///
  /// \exception bad_alloc Memory allocation failed.
  /// \exception FormatException
  /// \exception LdasException   
  //---------------------------------------------------------------------
  template< class T > T* convertFromFormat( const char* source,
					    size_t size,
					    size_t destSize ) const;

  //---------------------------------------------------------------------
  /// \brief Converts internal data from binary into specified format.
  ///
  /// \param source
  ///     The buffer containing the binary data.
  /// \param size
  ///     The size of the buffer.
  /// \param destSize
  ///     This parameter is used to store the number of bytes in the
  ///     compressed data (if size is not null)   
  ///
  /// \exception bad_alloc Memory allocation failed.
  //---------------------------------------------------------------------
  template< class T > char* convertToFormat( const T* source, 
					     size_t size,
					     size_t* destSize ) const;

  /* Attribute Manipulation */
  //--------------------------------------------------------------------------
  /// \brief Write attributes.
  ///
  /// \param att
  ///     The attributes object to be written.
  //--------------------------------------------------------------------------
  void writeAttributes( Attributes& att ) const;
  //--------------------------------------------------------------------------
  /// \brief Read & set the attributes for this object.
  ///
  /// \param att
  ///     The attributes object to fill.
  //--------------------------------------------------------------------------
  void readAttributes( const Attributes& att );
    
protected:

  /* Conversion */
  //--------------------------------------------------------------------------
  /// \brief Converts the internal data from binary to Base 64
  ///
  /// \param source
  ///     The internal data.
  /// \param sourceSize
  ///     The size of the data.
  ///
  /// \return
  ///     Base 64 data.
  //--------------------------------------------------------------------------
  char* binaryToBase64( const char* source, size_t sourceSize ) const;

  //--------------------------------------------------------------------------
  /// \brief Converts the internal data from Base64 to binary.
  /// 
  /// \param source
  ///     The internal data.
  /// \param destSize
  ///     A pointer to the destination size of the data.
  ///
  /// \return
  ///     Binary data.
  //--------------------------------------------------------------------------
  char* base64ToBinary( const char* source, size_t* destSize ) const;
   
  //--------------------------------------------------------------------------
  /// The data is returned as a null-terminated ASCII string.
  ///
  /// \param source
  ///     The source array for the data.  If this is null
  ///     the method returns false immediately.
  /// \param sourceSize
  ///     The number of elements to convert to ASCII.
  //--------------------------------------------------------------------------
  template< class T > const std::string dataToAscii( const T* source,
						     size_t sourceSize ) const;
   
  //---------------------------------------------------------------------
  /// \brief Converts the internal data from ascii to uncompressed binary
  ///        format.
  ///
  /// \param[in] source
  ///     A null-terminated string containing the ascii numbers to translate.
  /// \param[in] destSize
  ///     The number of elements to translate.  There must be at least this
  ///	  many numbers in the source string, otherwise an exception will be
  ///     thrown.
  /// \param[out] end_ptr
  ///     The end position of the decoded data from \a source.
  ///
  /// \exception FormatException
  /// \exception LdasException
  //---------------------------------------------------------------------
  template< class T > T* asciiToData( const char* source, 
				      size_t destSize,
				      const char** end_ptr ) const;

  //---------------------------------------------------------------------------
  /// \brief Converts the internal data from binary to Base 64.
  //---------------------------------------------------------------------------
  template< class T > char* dataToBase64( const T* source,
					  size_t sourceSize ) const;

  //---------------------------------------------------------------------
  /// \brief Converts the internal data from Base64 to binary.
  //---------------------------------------------------------------------
  template< class T > T* base64ToData( const char* source,
				       size_t destSize ) const;

  //---------------------------------------------------------------------
  /// \brief Compresses internal data in binary format.
  ///
  /// \param source
  ///     The buffer containing the compressed data.
  /// \param sourceSize
  ///     The size of the buffer.
  /// \param compression
  ///     The compression method to use
  /// \param destSize
  ///     This parameter is used to store the number of bytes in the
  ///     compressed data (if destSize is not null)
  ///
  /// \return
  ///     A pointer to the allocate memory containing the compressed data.
  ///
  /// \exception bad_alloc
  ///     Memory allocation failed.
  //---------------------------------------------------------------------
  template< class T > char* compress( const T* source,
				      size_t sourceSize,
				      Compression& compression,
				      size_t* destSize ) const;
  template< class T > T* uncompress( const char* source,
				     size_t sourceSize,
				     size_t destSize,
				     Compression compression ) const;

  mutable Format mFormat;
  mutable Compression mCompression;

};


inline ILwd::Format ILwd::Style::
getWriteFormat() const
{
    return mFormat;
}


inline ILwd::Compression ILwd::Style::
getWriteCompression() const
{
    return mCompression;
}


inline void ILwd::Style::
setWriteFormat( Format f )
{
    if ( f != USER_FORMAT )
    {
        mFormat = f;
    }
   
    return;
}


inline void ILwd::Style::
setWriteCompression( Compression c )
{
    if ( c != USER_COMPRESSION )
    {
        mCompression = c;
    }
   
    return;
}


#if HAVE_LIBOSPACE
void os_write( os_bstream& stream, const ILwd::Style& o );
void os_read( os_bstream& stream, ILwd::Style& o );

OS_CLASS( ILwd::Style )
OS_STREAM_OPERATORS( ILwd::Style )
#endif /* HAVE_LIBOSPACE */

#endif /* StyleHH */
