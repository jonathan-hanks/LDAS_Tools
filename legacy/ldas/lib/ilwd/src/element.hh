/* -*- c-basic-offset: 4; -*- */
#ifndef ElementHH
#define ElementHH


#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

/// \cond DOXYGEN_IGNORE
namespace ILwd
{
    class Element;
}
/// \endcond


/* System Includes */
#include <iosfwd>
#include <exception>

/* LDAS Includes */
#include <general/formatexception.hh>

// Local Includes
#include "starttag.hh"
#include "elementid.hh"


//-----------------------------------------------------------------------------
//
//: Represents an XML data element.
//
class ILwd::Element
{
public:
    /* Operator Overloads */
    virtual ~Element( );

    /* Operator Overloads */
    const Element& operator=( const Element& e );
    bool operator==( const Element& e ) const;
    bool operator!=( const Element& e ) const;

    //: Get Identifier.
    //
    // This pure virtual method is to introduce interface into the 
    // base class to be overloaded by all derived classes.
    virtual const std::string& getIdentifier() const = 0;
   
    //: Get Id.
    //
    // This pure virtual method is to introduce interface into the 
    // base class to be overloaded by all derived classes.   
    virtual ElementId getElementId() const = 0;
    
    //: I/O.
    //   
    // This pure virtual method is to introduce interface into the 
    // base class to be overloaded by all derived classes.       
    //!exc: std::exception
    //
    virtual void write( std::ostream& stream ) const = 0;

protected:

};



//-----------------------------------------------------------------------------
// Inline Methods
//-----------------------------------------------------------------------------
inline ILwd::Element::
~Element( )
{
}


//-----------------------------------------------------------------------------
//   
//: Assignment operator.
// 
//!param: const Element& e - The object to assign from.
//
//!return: const Element& - A reference to this object.
//
inline const ILwd::Element&
ILwd::Element::operator=( const Element& e )
{
    return *this;
}


//-----------------------------------------------------------------------
/// \brief Equal comparison operator.
///
/// This compares this object to another for equality.
///
/// \param e
///     The object to compare with.
///
/// \return
///     true if the objects are equal.
//-----------------------------------------------------------------------
inline bool ILwd::Element::operator==( const Element& e ) const
{
    return true;
}


//-----------------------------------------------------------------------------
//
//: Inequality operator.
//
//!param: const Element& e - The object to compare to.
//
//!return: bool - true if the objects are not equal.  False otherwise.
//   
inline bool ILwd::Element::operator!=( const Element& e ) const
{
    return false;
}


#ifdef HAVE_LIBOSPACE

void os_write( os_bstream&, const ILwd::Element& );
void os_read( os_bstream&, ILwd::Element& );

OS_POLY_CLASS( ILwd::Element )
OS_STREAM_OPERATORS( ILwd::Element )

#endif // HAVE_LIBOSPACE

#endif // ElementHH
