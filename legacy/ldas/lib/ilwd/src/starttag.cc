#include "ilwd/config.h"

#include <sstream>
   
#include "general/AtExit.hh"

#include "starttag.hh"
#include "xml.hh"

using ILwd::StartTag;
using ILwd::Reader;
   
using namespace std;   

const Regex &StartTag::regex = Regex( std::string( "^[[:space:]]*" ) + XML::STag );

   
//-----------------------------------------------------------------------------
//
//: Constructor.
//
//!param: const std::string& name - A reference to the name string.
//
ILwd::StartTag::StartTag( const std::string& name )
        : mName( slower( name ))
{}
   

//-----------------------------------------------------------------------------
//
//: Returns the Generic Identifier for this tag.
//
//!return: const std::string& - Identifier.
//
const std::string& ILwd::StartTag::getIdentifier() const
{
    return mName;
}
   
   
//-----------------------------------------------------------------------------
//
//: Constructor.
//
// Creates a start tag with the given identifier and attributes.
//
//!param: const std::string& name -
//!param: const Attributes& attributes -
//
//!exc: bad_alloc - Memory allocation failed.
//
StartTag::StartTag(
    const std::string& name, const Attributes& attributes )
    : mName( slower( name )), mAttributes( attributes )
{}
   

//-----------------------------------------------------------------------------
//
//: Copy Constructor.
//
//!param: const StartTag& tag - A reference to the object to copy.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
StartTag::StartTag( const StartTag& tag )
  : Base( tag ),
    mName( tag.mName ),
    mAttributes( tag.mAttributes )
{}


//-----------------------------------------------------------------------------
//
//: Input constructor.
//
//!param: Reader& r - - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error reading from the stream occurred.
//!exc: FormatException - The format for the StartTag was invalid.
//
StartTag::StartTag( Reader& r )
{
    read( r );
}

   
//-----------------------------------------------------------------------------
//
//: Destructor.
//
ILwd::StartTag::~StartTag()
{}
   

//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another for equality.
//
//!param: const StartTag& s - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool StartTag::operator==( const StartTag& s ) const 
{
    return ( Base::operator==( s ) &&
             mName == s.mName && mAttributes == s.mAttributes );
}


//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
//!param: const StartTag& tag - The object to assign from.
//
//!return: const StartTag& - A reference to this object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
ILwd::StartTag& ILwd::StartTag::operator=( const StartTag& tag )
{
    if ( this != &tag )
    {
        mName = tag.mName;
        mAttributes = tag.mAttributes;
    }
    
    return (*this);
}



//-----------------------------------------------------------------------------
//
//: Inequality operator.
//
//!param: const StartTag& s - The object to compare to.
//
//!return: bool - true if the objects are not equal.  False otherwise.
//
bool ILwd::StartTag::operator!=( const StartTag& s ) const 
{
    return !operator==( s );
}


//-----------------------------------------------------------------------------
//
//: Writes the start-tag to a stream.
//
//!param: ostream& stream - A reference to the stream to write to.
//!param: const StartTag& start - A reference to the StartTag object to write +
//!param:    to the stream.   
//
//!return: ostream& - A reference to the stream.
//
//!except: std::exception - An unknown error occured.
//
std::ostream& operator<<( std::ostream& stream, const ILwd::StartTag& start )
{
    start.write( stream );
    return stream;
}


//-----------------------------------------------------------------------------
//
//: Returns the ID for this class.
//
//!return: ClassType - ID. It is ID_STARTTAG. 
//
ILwd::ClassType ILwd::StartTag::getClassId() const
{
    return ID_STARTTAG;
}
   
   
//-----------------------------------------------------------------------------
//
//: Writes a StartTag to a stream.
//
//!param: std::ostream& stream - A reference to the stream to write to.
//
//!exc: std::exception - An unknown error occured.
//
void StartTag::write( std::ostream& stream ) const
{
    stream << "<" << getIdentifier();
    
    if ( mAttributes.size() > 0 )
    {
        stream << " " << mAttributes;
    }

    stream << ">";
   
    return;
}



//-----------------------------------------------------------------------------
//
//: Read
//
// Reads a StartTag from a stream.  This will skip whitespace preceding the
// start-tag.  The read procedure is:
//
// 1) Read the start-tag into a buffer 100 bytes at a time until the '>'
//    delimiter is reached.
// 2) Perform a Regex match to identify parts of the start-tag.
// 3) Store the name (converted to lowercase) and attributes.
//
//!param: Reader& reader - The stream to read the start-tag from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error reading from the stream occurred.
//!exc: FormatException - The format for the StartTag was invalid.
//
void StartTag::read( Reader& r )
{
    const int bufferSize( 100 );
    
    char buffer[ bufferSize ];
    istream& stream( r.getStream() );
    ostringstream ss;

    // What if the stream is already in a !good state?
    do
    {
        if ( stream.eof( ) )
	{
	    throw ILWD_STREAMEXCEPTION( stream );
	}
	stream.clear();
        stream.getline( buffer, bufferSize, '>' );
        ss << buffer;
    }
    while( stream.fail( ) );
    
    stream.clear();
    ss << '>';
   
   
    string ss_str( ss.str() );
   
    // Don't assume string::c_str() returns pointer to the internal buffer.
    // This CHAR pointer needs to stay in scope since RegexMatch is using
    // it as a shallow pointer
    const CHAR* ss_buffer( ss_str.c_str() );
    
    RegexMatch rm( XML::MATCHES_STAG );
    if ( !rm.match( regex, ss_buffer ) )
    {
       string msg( "The start-tag is not valid ILWD: " );
       msg += ss_str;
        
       throw ILWD_FORMATEXCEPTION_INFO( Errors::INVALID_START_TAG, 
                                        msg );
    }

    // Get the tag name
    mName = slower( rm.getSubString( XML::M_STAG_IDENTIFIER ) );
    
    // Read the attributes
    const string& atts_string( rm.getSubString( XML::M_STAG_ATTRIBUTES ) );
    mAttributes.read( atts_string.c_str() );
   
    return;
}


// ObjectSpace
#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::StartTag*), Stream::ILwd::STARTTAG, (ILwd::Base*) )

void os_write( os_bstream& stream, const StartTag& o )
{
    os_write( stream, (ILwd::Base&)o );    
    stream << o.mName << o.mAttributes;
   
    return;
}

void os_read( os_bstream& stream, StartTag& o )
{
    os_read( stream, (ILwd::Base&)o );
    stream >> o.mName >> o.mAttributes;
   
    return;
}

#endif // HAVE_LIBOSPACE
