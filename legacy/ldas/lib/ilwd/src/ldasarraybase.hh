/* -*- c-basic-offset: 4; -*- */
#ifndef LdasArrayBaseHH
#define LdasArrayBaseHH

//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif


// System Includes
#include <exception>

// ObjectSpace Includes
#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE

// LDAS Includes
#include <general/formatexception.hh>

// Local Includes
#include "ldasformatelement.hh"


namespace ILwd {
    class LdasArrayBase;

//: Array row-ordering formats
//
// This enumeration is used to describe the manner in which a multidimensional
// array is stored in an LdasArray container.  There are two different orders
// in which the data can be stored, the order used by the 'C' language and the
// order used by Fortran.
//
// <TABLE BORDER=2 CELLPADDING=4>\n
//   <TR><TD><FONT SIZE=2>Array Order</FONT></TD>
//       <TD><FONT SIZE=2>Description</FONT></TD></TR>\n
//   <TR><TD>C_ORDER</TD><TD>C style array orderering.  The elements of a
//       multidimensional array are stored row-wise, i.e. The last index
//       varies fastest.</TD></TR>
//   <TR><TD>F77_ORDER</TD><TD>Fortran style array orderering.  The elements of
//       a multidimensional array are stored column-wise, i.e. The first index
//       varies fastest.</TD></TR>
// </TABLE>
//
    enum ArrayOrder
    {
        C_ORDER,
        F77_ORDER
    };

//: Byte-order formats
//
// This describes how multi-byte data (data which takes up more than one byte
// to store, such as a 4-byte integer) is stored by the LdasArray.  There are
// only two formats supported: big-endian and little-endian.
//
// <TABLE BORDER=2 CELLPADDING=4>\n
//   <TR><TD><FONT SIZE=2>Byte Order</FONT></TD>
//       <TD><FONT SIZE=2>Description</FONT></TD></TR>\n
//   <TR><TD>BIG</TD><TD>The data is big-endian.  This format is used by
//       Solaris.  The most significant byte comes first.</TD></TR>
//   <TR><TD>LITTLE</TD><TD>The data is little-endian.  This is the format used
//       by intel and alpha cpu's.  The least significant byte is stored first.
//       </TD>/TR>
// </TABLE>
//
    enum ByteOrder
    {
        LITTLE,
        BIG
    };

    static const ByteOrder HostOrder(
#ifdef WORDS_BIGENDIAN
				     BIG
#else	/* WORDS_BIGENDIAN */
				     LITTLE
#endif	/* WORDS_BIGENDIAN */
				     );
}





//-----------------------------------------------------------------------------
//
//: The base class for ILWD array elements.
//
class ILwd::LdasArrayBase : public ILwd::LdasFormatElement
{
public:
    typedef INT_4U	size_type;

    /* Constructors / Destructor */
    explicit LdasArrayBase(
        const std::string& name = "", const std::string& comment = "" );
    LdasArrayBase( const LdasArrayBase& e );
    virtual ~LdasArrayBase();
    
    /* Accessors */
    size_type getNData() const;

    /************************/
    /* Pure Virtual Methods */
    /************************/

    /*************/
    /* Accessors */
    /*************/

    //: <font color=red>PURE VIRTUAL</font> Get 
    //: the identifier for this element.
    //
    // Returns the identifier for this element.
    //
    //!return: const std::string& - The identifier.
    //
    virtual const std::string& getIdentifier() const = 0;

    //: <font color=red>PURE VIRTUAL</font> 
    //: Get the index ordering.
    //
    // This returns the index ordering for the array.  This is only important
    // if this is a multidimensional array.  The possible types are 'C' 
    // ordering (row-wise) or 'F77' (column-wise).
    //
    //!return: ArrayOrder - The array order
    //
    virtual ArrayOrder getArrayOrder() const = 0;

    //: <font color=red>PURE VIRTUAL</font> 
    //: Get the number of dimensions in the array.
    //
    //!return: size_type - The number of dimensions.
    //
    virtual size_type getNDim() const = 0;

    //: <font color=red>PURE VIRTUAL</font> 
    //: Returns a dimension size.
    //
    //!param: size_type n - The dimension whose size is desired.
    //
    //!return: size_type - the size of the dimension.
    //
    //!except: range_error - The requested dimension did not exist.
    //
    virtual size_type getDimension( size_type n = 0 ) const = 0;

    //: <font color=red>PURE VIRTUAL</font>    
    //: Get scale factor for a dimension.
    //
    //!param: size_type n - The dimension whose scale is desired.
    //
    //!return: REAL_8 - scale factor.
    //
    //!except: range_error - The requested dimension did not exist.
    //   
    virtual REAL_8 getDx( size_type n ) const = 0;
   
    //: <font color=red>PURE VIRTUAL</font>    
    //: Get origin for a dimension.
    //
    //!param: size_type n - The dimension whose origin is desired.
    //
    //!return: REAL_8 - origin.
    //
    //!except: range_error - The requested dimension did not exist.
    //   
    virtual REAL_8 getStartX( size_type n ) const = 0;   
   
    //: <font color=red>PURE VIRTUAL</font> 
    //: Get the units for a dimension.
    //
    //!param: size_type n - The dimension whose size is desired.
    //
    //!return: const std::string& - the units for the dimension.
    //
    //!except: range_error - The requested dimension did not exist.
    //
    virtual std::string getUnits( size_type n = 0 ) const = 0;

    //: <font color=red>PURE VIRTUAL</font> 
    //: Get data units.
    //
    //!return: const std::string - The units for the data values.
    //
    virtual std::string getDataValueUnit() const = 0;   
   
    //: <font color=red>PURE VIRTUAL</font> 
    //: Create a copy of this element.
    //
    // This method allocates memory and creates a copy of this array object.
    // This is a virtual method which allows one to copy an LdasElement without
    // knowingwhat its exact type is.
    //
    //!return: LdasArrayBase* - A pointer to a newly allocated copy of this
    //!return:     object.
    //
    //!except: bad_alloc - Memory allocation failed.
    //
    virtual LdasArrayBase* createCopy() const = 0;

    //: <font color=red>PURE VIRTUAL</font> 
    //: Gets the element ID of this type of object.
    //
    // The element ID is used to determine what type an object is without
    // having to resort to RTTI or comparing identifiers.
    //
    //!return: ElementId - The element id (an enumeration).
    //
    virtual ElementId getElementId() const = 0;

private:
    
    /************/
    /* Mutators */
    /************/

    //: <font color=red>PURE VIRTUAL</font> 
    //: Writes this element to a stream.
    //
    // This writes the array to a stream as an XML object.
    //
    //!param: ostream& stream - The stream to write to.
    //
    //!except: exception - An unexpected exception.
    //
    virtual void writeData( std::ostream& stream ) const = 0;
};


#ifdef HAVE_LIBOSPACE

void os_write( os_bstream&, const ILwd::LdasArrayBase& );
void os_read( os_bstream&, ILwd::LdasArrayBase& );

OS_POLY_CLASS( ILwd::LdasArrayBase )
OS_STREAM_OPERATORS( ILwd::LdasArrayBase )

#endif // HAVE_LIBOSPACE


#endif // LdasArrayBaseHH
