#ifndef ILwdElementIdHH
#define ILwdElementIdHH

namespace ILwd
{
    enum ElementId
    {
        ID_CHAR,
        ID_CHAR_U,
        ID_INT_2S,
        ID_INT_2U,
        ID_INT_4S,
        ID_INT_4U,
        ID_INT_8S,
        ID_INT_8U,
        ID_REAL_4,
        ID_REAL_8,
        ID_COMPLEX_8,
        ID_COMPLEX_16,
        ID_ILWD,
        ID_LSTRING,
        ID_EXTERNAL
    };
}


#endif // ILwdElementIdHH
