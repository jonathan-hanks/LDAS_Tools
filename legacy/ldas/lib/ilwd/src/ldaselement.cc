#include "ilwd/config.h"

#include <sstream>
#include <cassert>
#include <cstdio>   
#include <algorithm>   
#include <iterator>   
   
#include "general/base64.hh"
#include "general/gpstime.hh"
#include "general/autoarray.hh"

#include "ilwdconst.h"
#include "ldaselement.hh"
#include "ldasarray.hh"
#include "ldascontainer.hh"
#include "ldasstring.hh"
#include "ldasexternal.hh"


using namespace std;     
   
   
// #define LDAS_DEBUG      
   
#ifdef LDAS_DEBUG   
#define PRINT_DEBUG(a) cerr << a << endl;
#else
#define PRINT_DEBUG(a)
#endif   
   

   
//-----------------------------------------------------------------------
/// \file
/// \author="David Farnham"
//-----------------------------------------------------------------------

using namespace ILwd;


enum
  {
    TAG_CHAR,
    TAG_CHAR_U,
    TAG_INT_2S,
    TAG_INT_2U,
    TAG_INT_4S,
    TAG_INT_4U,
    TAG_INT_8S,
    TAG_INT_8U,
    TAG_REAL_4,
    TAG_REAL_8,
    TAG_COMPLEX_8,
    TAG_COMPLEX_16,
    TAG_ILWD,
    TAG_LSTRING,
    TAG_EXTERNAL
  };


//-----------------------------------------------------------------------
/// \brief Case Insensitive Compare Operator.
//-----------------------------------------------------------------------
struct eqstr
{
  //---------------------------------------------------------------------
  /// \brief Function operator
  ///
  /// This is a binary function which returns true if the two null-
  /// terminated character arrays are equal
  ///
  /// \param[in] s1
  ///     The first string.
  /// \param[in] s2
  ///     The second string.
  ///
  /// \return
  ///     True if the strings are equal, false otherwise.
  //---------------------------------------------------------------------
  bool
  operator()( const char* s1, const char* s2 ) const
  {
    return strcmp(s1, s2) == 0;
  }
};


typedef General::unordered_map< std::string, int > QueryHash;


// This function gets called once per so lifetime.
QueryHash& initTagHash()
{
  static QueryHash h;

  h[ std::string( "char" )       ] = TAG_CHAR;
  h[ std::string( "char_u" )     ] = TAG_CHAR_U;
  h[ std::string( "int_2s" )     ] = TAG_INT_2S;
  h[ std::string( "int_2u" )     ] = TAG_INT_2U;
  h[ std::string( "int_4s" )     ] = TAG_INT_4S;
  h[ std::string( "int_4u" )     ] = TAG_INT_4U;
  h[ std::string( "int_8s" )     ] = TAG_INT_8S;
  h[ std::string( "int_8u" )     ] = TAG_INT_8U;
  h[ std::string( "real_4" )     ] = TAG_REAL_4;
  h[ std::string( "real_8" )     ] = TAG_REAL_8;
  h[ std::string( "complex_8" )  ] = TAG_COMPLEX_8;
  h[ std::string( "complex_16" ) ] = TAG_COMPLEX_16;   
  h[ std::string( "ilwd" )       ] = TAG_ILWD;
  h[ std::string( "lstring" )    ] = TAG_LSTRING;
  h[ std::string( "external" )   ] = TAG_EXTERNAL;
  return h;
}


static QueryHash& tagHash = initTagHash();

const char METADATA_SEPERATOR(':');

std::string
substr(std::string::const_iterator Start,
       const std::string::const_iterator End )
{
  std::string	retval;

  while( Start != End )
  {
    retval += *Start;
    Start++;
  }
   
  return retval;
}


//-----------------------------------------------------------------------
/// \brief Append "value" with escaped characters found in "escapeChars" 
///        to the "result" string.
///
/// This helper method will append value to the result by escaping any
/// characters in value if they are found in "escapeChars" sequence.
///
/// \param[in] value
///     String to append.
/// \param[in] escapeChars
///     Sequence of chars to escape.
/// \param[out] result
///     A reference to the result string.
///
/// \return
///     True if "value" had escaped characters, false otherwise.
//-----------------------------------------------------------------------
static bool
appendEscaped( const std::string& value,
	       const std::string& escapeChars,
	       std::string& result )
{
  std::string::size_type pos( 0 ), prev_pos( 0 );


  if( ( pos = value.find_first_of( escapeChars, pos ) ) != std::string::npos )
  {
    pos = 0;

    // There are characters to escape               
    while( ( pos = value.find_first_of( escapeChars, pos ) ) != std::string::npos )
    {
      // Don't use string::insert method: it messes up padding for STLPort debug string.
      result.append( value, prev_pos, pos - prev_pos );
 
      // Escape the char
      result += BACK_SLASH_CHAR;

      // Append char itself
      result += *( value.begin() + pos );
 
      // Skip over escaped character if it's not very last character
      if( ++pos != value.size() )
      {
	prev_pos = pos;
      }
      else
      {
	// It was the very last character in the string
	prev_pos = std::string::npos;
	break;  
      }
    }


    // Append the rest of the string after last escaped character
    if( prev_pos != std::string::npos )
    {
      result.append( value.begin() + prev_pos, value.end() );
    }  

    return true;
  }


  // No characters to escape found in value:
  result.append( value );

  return false;
}



// Static data initialization
const unsigned int LdasElement::DFLT_JOBID( 0 );

const char* const LdasElement::ATTR_STR_COMMENT( "comment" );
const char* const LdasElement::ATTR_STR_JOBID( "jobid" );
const char* const LdasElement::ATTR_STR_METADATA( "metadata" );
const char* const LdasElement::ATTR_STR_NAME( "name" );
const char* const LdasElement::ATTR_STR_NULLMASK( "nullmask" );


//-----------------------------------------------------------------------
/// This creates an LdasElement with the specified name and comment.
//-----------------------------------------------------------------------
LdasElement::
LdasElement( const std::string& name, const std::string& comment )
  : m_jobid( DFLT_JOBID ), mNameVector(), mComment( comment )
{	
  setNameString( name );
}

LdasElement::
LdasElement( const LdasElement& e )
  : Element( ),
    RefCount( ),
    m_null_mask( e.m_null_mask ),
    m_jobid( e.m_jobid ),
    mNameVector( e.mNameVector ),
    mComment( e.mComment ),
    m_metadata( e.m_metadata )
{
}


LdasElement::
~LdasElement()
{
#ifdef LDAS_DEBUG         
  ostringstream top_debug_str;
  top_debug_str << static_cast< void* >( this )
		<< ": entering ~LdasElement";
   
  if( m_container_list.size() )
  {
    top_debug_str << " parents: ";   
    copy( m_container_list.begin(), m_container_list.end(),
	  ostream_iterator< void* >( top_debug_str, " " ) );
  }
#endif   
   
  PRINT_DEBUG( top_debug_str.str() );
   
   
  try
  {
    std::list< ILwd::LdasContainer* >:: iterator iter( m_container_list.begin() );

    // This loop steps through all parent containers and      
    // unregisters itself with each of them by calling LdasContainer::erase()
    // method, which in turn will remove itself from the current parent list
    // of "this" element
    while( iter != m_container_list.end() )
    {
#ifdef LDAS_DEBUG               
      ostringstream debug_str;
#endif   
   
      if( *iter )
      {
#ifdef LDAS_DEBUG            
	debug_str << static_cast< void* >( this )
		  << ": ~LdasElement: searching parent: "
		  << static_cast< void* >( *iter );
#endif
   
	ILwd::LdasContainer::iterator	pos( (*iter)->find( this ) );
   
	// Parent doesn't own child ---> unregister child with parent
	if ( (*iter)->isOwned( pos ) == true )
	{
	  // Parent owns child, and since parent is still in the 
	  // m_container_list sequence: child is not being destructed
	  // through the parent (first thing parent does before destroying
	  // children is removing itself from their m_container_list )
	  (*iter)->unsetOwnership( pos );   
	}
   
   
	pos = (*iter)->erase( pos );   
	PRINT_DEBUG( debug_str.str() );
   
	// m_container_list has been changed, reset iterator to the
	// beginning of the list
	iter = m_container_list.begin();
      }
    }
  }
  catch( const std::exception& exc )
  {
    // Don't allow exception leave destructor
    const string exc_msg( exc.what() );
    assert( exc_msg.empty() == true );
  }
}


const LdasElement& LdasElement::
operator=( const LdasElement& e )
{
  if ( this != &e )
  {
    mNameVector = e.mNameVector;
    mComment = e.mComment;
    m_null_mask = e.m_null_mask;
    m_metadata = e.m_metadata;
    m_jobid = e.m_jobid;
  }

  return *this;
}

void LdasElement::
addContainerRef( ILwd::LdasContainer* Container )
{
#ifdef LDAS_DEBUG               
  ostringstream debug_str;
  debug_str << "+++" << static_cast< void* >( this )
	    << ": adding parent: " << static_cast< void* >( Container )
	    << " to list: ";
   
  copy( m_container_list.begin(), m_container_list.end(),
	ostream_iterator< void* >( debug_str, " " ) );   
#endif

  PRINT_DEBUG( debug_str.str() );
   
   
  m_container_list.push_back( Container );
   
   
#ifdef LDAS_DEBUG                  
  debug_str.str( "" );
  debug_str  << "+++" << static_cast< void* >( this )
	     << ": after adding parent: list now: "; 
  copy( m_container_list.begin(), m_container_list.end(),
	ostream_iterator< void* >( debug_str, " " ) );   
#endif   

  PRINT_DEBUG( debug_str.str() );
   
   
  return;
}
   

//-----------------------------------------------------------------------
/// This method removes a reference to the parent container if that
/// pointer has been registered with object before.
///   
/// If passed Container object is not in the object's registered
/// sequence, method won't do anything.   
//-----------------------------------------------------------------------
void LdasElement::
removeContainerRef( ILwd::LdasContainer* const Parent )
{
#ifdef LDAS_DEBUG                  
  ostringstream debug_str;
  debug_str << "---" << static_cast< void* >( this )
	    << ": removing parent: " << static_cast< void* >( Parent )
	    << " from list: ";
   
  copy( m_container_list.begin(), m_container_list.end(),
	ostream_iterator< void* >( debug_str, " " ) );   
#endif   

  PRINT_DEBUG( debug_str.str() );
   
   
  m_container_list.remove(Parent );
   
   
#ifdef LDAS_DEBUG                  
  debug_str.str( "" );
  debug_str  << "---" << static_cast< void* >( this )
	     << ": after removing parent: left in list: "; 
  copy( m_container_list.begin(), m_container_list.end(),
	ostream_iterator< void* >( debug_str, " " ) );   
#endif
   
  PRINT_DEBUG( debug_str.str() );


  return;
}   

void LdasElement::
clearMetadata( )
{
  m_metadata.clear();
}

void LdasElement::
appendName( const std::string& s )
{
  mNameVector.push_back( s );
}

void LdasElement::
insertName( size_t pos, const std::string& s )
{
  mNameVector.insert( mNameVector.begin() + pos, s );
}

void LdasElement::
setName( size_t pos,
	 const std::string& s,
	 bool FixedPosition )
{
  if ( pos >= mNameVector.size() )
  {
    mNameVector.resize(pos + 1);
  }
  // If just need to reset specified name position, then 
  // don't need this "if" statement to shift all elements.
  // Now setName method has almost the same effect as insertName method.
  if( ( !FixedPosition ) && ( mNameVector[ pos ].empty() == false ) )
  {
    mNameVector.push_back( "" );
    int prev( mNameVector.size() - 2 );
    for ( int i = mNameVector.size() - 1; ( i - pos ) != 0; --i )
    {
      mNameVector[ i ] = mNameVector[ prev ];
      --prev;
    }
  } 
  mNameVector[ pos ] = s;
   
  return;
}


void LdasElement::
deleteName( size_t pos )
{
  if ( pos >= mNameVector.size() )
  {
    return;
  }
    
  mNameVector.erase( mNameVector.begin() + pos );
   
  return;
}

bool LdasElement::
operator==( const LdasElement& e ) const
{
  if ( mComment != e.mComment )
  {
    return false;
  }
    
  if ( m_metadata.size() != e.m_metadata.size() )
  {
    return false;
  }

  for ( metadata_type::const_iterator
	  md = m_metadata.begin(),
	  end_md = m_metadata.end();
	md != end_md;
	++md )
  {
    metadata_type::const_iterator md_2( e.m_metadata.find( (*md).first ) );

    if ( ( md_2 == e.m_metadata.end() ) ||
	   
	 ( strcmp( (*md).second.c_str(), (*md_2).second.c_str() ) != 0 ) )
    {
      return false;
    }
  }
    
  return mNameVector == e.mNameVector;
}

   
template<>
std::string LdasElement::
getMetadata<std::string>( const std::string& Variable ) const
{
  metadata_type::const_iterator	v( m_metadata.find(Variable) );

  if ( v == m_metadata.end() )
  {
    throw std::range_error( "unable to retrieve metadata" );
  }
  return v->second;
}

template<>
General::GPSTime LdasElement::
getMetadata<General::GPSTime>( const std::string& Variable ) const
{
  using General::GPSTime;

  INT_4U sec = 0;
  INT_4U nanosec = 0;

  std::istringstream ss( getMetadata<std::string>(Variable) );

  bool have_left_brace = false;
  bool fail = false;
  char c = '\0';

  // Allowed formats are:    secs
  //                        {secs}
  //                        {secs nanosecs}

  // Consume the opening brace
  ss >> c;
  fail = fail || ss.fail();
  if (!fail)
  {
    if (c == '{')
    {
      have_left_brace = true;
    }
    else
    {
      ss.putback(c);
    }
  }

  // Read the first number (must be present!)
  ss >> sec;
  fail = fail || ss.fail();

  if (!fail && have_left_brace)
  {
    // Read the second number (if present, otherwise default to zero)
    // Note we DON'T need to check if the extraction is ok, since there
    // may be no second number inside the braces
    ss >> nanosec;
    ss.clear(); // Don't care if we fail here, so clear the state
    
    // Read the right brace for parse checking - this MUST be present
    ss >> c;
    fail = ss.fail() || (c != '}');
  }

  // Now there must be NO characters left in the string!
  fail = fail || (ss >> c);

  if (fail)
  {
    ostringstream oss;

    oss << "Unable to convert metadata variable "
	<< Variable
	<< " with value of \""
	<< getMetadata<std::string>(Variable)
	<< "\" to requested type";

    throw std::logic_error( oss.str() );
  }

  return GPSTime(sec, nanosec);
}


//-----------------------------------------------------------------------
/// Returns the entire name in the colon-delimited format.
//-----------------------------------------------------------------------
const std::string LdasElement::
getNameString() const
{
  static const string escape( "\\:" );
  std::string result_name;


  if( mNameVector.size() != 0 )
  {
    for( vector< string >::const_iterator iter = mNameVector.begin(),
	   begin_iter = mNameVector.begin(), end_iter = mNameVector.end();
	 iter != end_iter; ++iter )
    {
      if ( iter != begin_iter )
      {
	result_name += ':';
      }

      appendEscaped( *iter, escape, result_name );            
    }
  }


  return result_name;
}


//-----------------------------------------------------------------------
// This method writes the attributes for this object to an
// Attributes object.
//-----------------------------------------------------------------------
void LdasElement::
writeAttributes( Attributes& att ) const
{
  std::string nameString = getNameString();
  if ( nameString != "" )
  {
    att.addAttribute( ATTR_STR_NAME, nameString );
  }
    
  if ( mComment != "" )
  {
    att.addAttribute( ATTR_STR_COMMENT, mComment );
  }

  if ( m_null_mask.size() > 0)
  {
    att.addAttribute( ATTR_STR_NULLMASK, convert_nullmask_to_string() );
  }

  if ( m_jobid != DFLT_JOBID)
  {
    att.addAttribute( ATTR_STR_JOBID, m_jobid);
  }

  if ( m_metadata.size() > 0 )
  {
    att.addAttribute( ATTR_STR_METADATA, convert_metadata_to_string() );
  }
   
  return;
}


//-----------------------------------------------------------------------
/// This reads the attributes (from an Attributes object) for this
/// object and sets them.
//-----------------------------------------------------------------------
void LdasElement::
readAttributes( const Attributes& att )
{
  mNameVector.erase( mNameVector.begin(), mNameVector.end() );
    
  Attributes::const_iterator iter = att.find( ATTR_STR_NAME );
  if ( iter != att.end() )
  {
    setNameString(iter->second.getString());
  }

  // comment
  iter = att.find( ATTR_STR_COMMENT );
  if ( iter != att.end() )
  {
    mComment = iter->second.getString();
  }
  else
  {
    mComment = "";
  }

  // Handle the NULL mask
  iter = att.find( ATTR_STR_NULLMASK );
  if (iter != att.end() )
  {
    convert_string_to_nullmask(iter->second.getString());
  }

  // Handle the Job ID
  iter = att.find( ATTR_STR_JOBID );
  if (iter != att.end() )
  {
    const string& iter_str( iter->second.getString() );
    m_jobid = atoi(iter_str.c_str());
  }

  // Handle the METADATA
  iter = att.find( ATTR_STR_METADATA );
  if (iter != att.end() )
  {
    convert_string_to_metadata(iter->second.getString());
  }

  return;
}

    
bool LdasElement::
isNull(size_t Offset) const
{
  if (m_null_mask.size() && (Offset < m_null_mask.size()))
  {
    return m_null_mask[Offset];
  }
  return false;
}

// Sets metadata specified by Variable to Value
template <>
void LdasElement::
setMetadata< std::string >( const std::string& Variable,
			    const std::string& Value )
{
  m_metadata[ Variable ] = Value;
  return;
}

template<>
void LdasElement::
setMetadata<General::GPSTime>( const std::string& Variable,
			       const General::GPSTime& Value )
{
  using General::GPSTime;

  static const size_t size( 56 );
  CHAR buffer[ size ];
   
  INT_4U tmp( Value.GetSeconds() );
  sprintf( buffer, "%u", tmp );
  
  string value( "{" );
  value += buffer;
  value += ' ';
   
  tmp = Value.GetNanoseconds();
  sprintf( buffer, "%u", tmp );   
  
  value += buffer;
  value += '}';

  setMetadata( Variable, value );
  return;
}
   

/// Removes Variable from the list of metadata.
void LdasElement::
unsetMetadata( const std::string& Variable )
{
  metadata_type::iterator	i( m_metadata.find( Variable ) );
  if ( i != m_metadata.end() )
  {
    m_metadata.erase(i);
  }
   
  return;
}

void LdasElement::
setNull(size_t Offset, bool Value)
{
  if (Value)
  {
    if (m_null_mask.size() < (Offset + 1))
    {
      m_null_mask.resize(Offset+1);
    }
    m_null_mask[Offset] = Value;
  }
  else
  {
    bool	is_all_null = true;

    if (m_null_mask.size() > Offset)
    {
      if (m_null_mask.size() > 0) m_null_mask[Offset] = Value;
      for (General::bit_vector::const_iterator i = m_null_mask.begin();
	   i != m_null_mask.end();
	   i++)
      {
	if ((*i))
	{
	  is_all_null = false;
	  break;
	}
      }
      if (is_all_null)
      {
	m_null_mask.resize(0);
      }
    }
  }
   
  return;
}

   
void LdasElement::
setNull(std::string Mask64)
{
  convert_string_to_nullmask(Mask64);
  return;
}

   
LdasElement* LdasElement::
createElement( Reader& r, const StartTag& st )
{
  const string& st_ident( st.getIdentifier() );
  switch( tagHash[ st_ident.c_str() ] )
  {
  case TAG_CHAR:
    {
      return new LdasArray< CHAR >( r, st.getAttributes() );
    }
  case TAG_CHAR_U:
    {
      return new LdasArray< CHAR_U >( r, st.getAttributes() );
    }
  case TAG_INT_2S:
    {
      return new LdasArray< INT_2S >( r, st.getAttributes() );
    }
  case TAG_INT_2U:
    {
      return new LdasArray< INT_2U >( r, st.getAttributes() );
    }
  case TAG_INT_4S:
    {
      return new LdasArray< INT_4S >( r, st.getAttributes() );
    }
  case TAG_INT_4U:
    {
      return new LdasArray< INT_4U >( r, st.getAttributes() );
    }
  case TAG_INT_8S:
    {
      return new LdasArray< INT_8S >( r, st.getAttributes() );
    }
  case TAG_INT_8U:
    {
      return new LdasArray< INT_8U >( r, st.getAttributes() );
    }
  case TAG_REAL_4:
    {
      return new LdasArray< REAL_4 >( r, st.getAttributes() );
    }
  case TAG_REAL_8:
    {
      return new LdasArray< REAL_8 >( r, st.getAttributes() );
    }
  case TAG_COMPLEX_8:
    {
      return new LdasArray< COMPLEX_8 >( r, st.getAttributes() );
    }   
  case TAG_COMPLEX_16:
    {
      return new LdasArray< COMPLEX_16 >( r, st.getAttributes() );
    }      
  case TAG_ILWD:
    {
      return new LdasContainer( r, st.getAttributes() );
    }
  case TAG_LSTRING:
    {
      return new LdasString( r, st.getAttributes() );
    }
  case TAG_EXTERNAL:
    {
      return new LdasExternal( r, st.getAttributes() );
    }
  default:
    {
      string msg( "Unknown element type: <" );
      msg += st.getIdentifier();
      msg += ">.";
      throw ILWD_FORMATEXCEPTION_INFO( Errors::UNKNOWN_IDENTIFIER, msg );
   
      break;
    }
  }
  return 0;
}


LdasElement* LdasElement::
createElement( Reader& r )
{
  StartTag st( r );
  return createElement( r, st );
}


//-----------------------------------------------------------------------
/// Replace the value of mNameVector with the results of parsing
/// \a nameString.
//-----------------------------------------------------------------------
void LdasElement::
setNameString(const std::string& nameString)
{
  // Should already be empty.
  mNameVector.erase( mNameVector.begin(), mNameVector.end() );

    
  const CHAR* s = nameString.c_str();

  const size_t tmp_size( nameString.size() + 1 );
  General::AutoArray< CHAR > tmp( new CHAR[ tmp_size ] );


  tmp[ 0 ] = 0;

  CHAR *p( (char*)NULL );

  for( const char* i = s, *end = ( s + nameString.size() ); i < end; )
  {
    p = strpbrk( (char*)i, "\\:" );
    if (p==0)
    {
      strcat(tmp.get(),i);
      mNameVector.push_back(tmp.get());
      return;
    }

    strncat(tmp.get(),i,p-i);
    switch (*p)
    {
    case ':':
      {
	mNameVector.push_back(tmp.get());
	tmp[ 0 ] = 0;
	break;
      }
    case '\\':
      {
	strncat(tmp.get(),++p,1);
	break;
      }
    default:
      assert(0);  // Will never happen
    }

    i=p+1;
  }


  if ( p && ( *p == ':' ) )
  {
    // Case were name ends with a colon
    mNameVector.push_back( "" );
  }

  return;
}

void LdasElement::
write( std::ostream& stream ) const
{
  write( stream, USER_FORMAT, USER_COMPRESSION );
  return;
}


std::string LdasElement::
getName( size_t i ) const
{
  if ( i >= mNameVector.size() )
  {
    return "";
  }
    
  return mNameVector[ i ];
}

size_t LdasElement::
getNameSize() const
{
  return mNameVector.size();
}


void LdasElement::
convert_string_to_metadata( const std::string& Metadata )
{
  std::string		md( Metadata );
  std::string::iterator	start;
  std::string::iterator	end;

  std::string		var;
  std::string		value;

  bool	finish( ( Metadata.length() > 0 ) ? false : true );

  m_metadata.erase( m_metadata.begin( ), m_metadata.end( ) );
  end = md.begin();

  while( !finish )
  {
    start = end;
    //-------------------------------------------------------------------
    // Calculate var
    //-------------------------------------------------------------------
    while( ( end != md.end() ) &&
	   ( *end != '=' ) )
    {
      if (*end == '\\')
      {
	// Take the next character as a literal
	// std::string::erase() may invalidate iterator: reset to
	// the first element remaining beyond element removed:
	end = md.erase(end);
   
	if( end == md.end() )
	{
	  break;
	}
      }

      ++end;
    }

    if ( end == md.end() )
    {
      var = substr(start,end);
      m_metadata[var] = "";
      break;
    }
    var = substr(start,end);

   
    ++end;		// Go beyond '='
    start = end;	// Reset the start
    //-------------------------------------------------------------------
    // Calculate value
    //-------------------------------------------------------------------
    while( ( end != md.end() ) &&
	   ( *end != METADATA_SEPERATOR ) )
    {
      if (*end == '\\')
      {
	// Take the next character as a literal
	// std::string::erase() may invalidate iterator: reset to
	// the first element remaining beyond element removed:   
	end = md.erase(end);
    
	if( end == md.end() )
	{
	  break;
	}   
      }
   
      ++end;
    }
    if ( end == md.end() )
    {
      // :TODO: Copy between begin and end out to value
      value = substr(start,end);
      finish = true;
    }
    else
    {
      value = substr(start, end);
    }
   
   
    m_metadata[var] = value;

    // Increment iterator only if it's not end of string
    if( end != md.end() )
    {
      ++end;
    }
  }
   
  return;
}

   
std::string LdasElement::
convert_metadata_to_string( ) const
{
  static const string metadata_string( 1, METADATA_SEPERATOR );
  std::string	retval;
  bool		add_space(false);


  for ( metadata_type::const_iterator i = m_metadata.begin(), 
          i_end = m_metadata.end(); i != i_end; ++i )
  {
    if ( add_space )
    {
      retval += METADATA_SEPERATOR;
    }
    else
    {
      add_space = true;
    }

    
    appendEscaped( i->first, metadata_string, retval );

    retval += '=';

    appendEscaped( i->second, metadata_string, retval );
  }


  return retval;
}

void LdasElement::
convert_string_to_nullmask(const std::string& Mask)
{
  Base64::decode(m_null_mask, Mask);
  return;
}

std::string LdasElement::
convert_nullmask_to_string(void) const
{
  std::string	retval("");

  Base64::encode(retval, m_null_mask);
  return retval;
}

//-----------------------------------------------------------------------------
// ObjectSpace
//-----------------------------------------------------------------------------

#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include "ospaceid.hh"
#include <ospace/uss/std/vector.h>
#include <ospace/uss/std/string.h>

OS_NO_FACTORY_STREAMABLE_1( (ILwd::LdasElement*), Stream::ILwd::LDASELEMENT,
			    (ILwd::Element*) )
  OS_STREAMABLE_0( (std::vector< std::string >*),
		   Stream::ILwd::LDASELEMENTNAMEVECTOR )

  namespace ILwd {

    void ospaceWrite( os_bstream& stream, const LdasElement& o )
    {

      os_write( stream, static_cast<const Element&>( o ) );
      stream << o.mComment
	     << o.convert_nullmask_to_string()
	     << o.m_jobid
	     << o.convert_metadata_to_string();


      stream << o.mNameVector;
   
      return;   
    }


    void ospaceRead( os_bstream& stream, LdasElement& o )
    {
      std::string null_mask;
      std::string metadata;
      os_read( stream, static_cast<Element&>(o) );

      o.mNameVector.erase( o.mNameVector.begin(), o.mNameVector.end() );

      stream >> o.mComment
	     >> null_mask
	     >> o.m_jobid
	     >> metadata;
    
      stream >> o.mNameVector;
      o.convert_string_to_nullmask(null_mask);
      o.convert_string_to_metadata(metadata);
   
      return;   
    }
 
  } // namespace


void os_write( os_bstream& stream, const ILwd::LdasElement& o )
{
  PRINT_DEBUG( "DEBUG: " << __FILE__ << ": " << __LINE__ );
  ospaceWrite( stream, o );
  PRINT_DEBUG( "DEBUG: " << __FILE__ << ": " << __LINE__ );
  return;
}


void os_read( os_bstream& stream, ILwd::LdasElement& o )
{
  ospaceRead( stream, o );
  return;
}


#endif // HAVE_LIBOSPACE


#define	INSTANTIATE( LM_TYPE )					\
  template							\
  LM_TYPE ILwd::LdasElement::					\
  getMetadata<LM_TYPE>( const std::string& Variable ) const;	\
  template							\
  void ILwd::LdasElement::					\
  setMetadata<LM_TYPE>( const std::string& Variable,		\
			const LM_TYPE& Value )

//-----------------------------------------------------------------------
/// \cond EXCLUDE
//-----------------------------------------------------------------------

INSTANTIATE( INT_2S );
INSTANTIATE( INT_2U );
INSTANTIATE( INT_4S );
INSTANTIATE( INT_4U );
INSTANTIATE( REAL_4 );
INSTANTIATE( REAL_8 );

//-----------------------------------------------------------------------
/// \endcond
//-----------------------------------------------------------------------

#undef INSTANTIATE

