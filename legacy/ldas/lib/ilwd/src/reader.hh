#ifndef ILwdReaderHH
#define ILwdReaderHH


//! author="David Farnham"

#ifdef HAVE_CONFIG_H
#include "ilwd/config.h"
#endif

/* System includes */
#include <sstream>
#include <string>

/* LDAS general includes */
#include <general/formatexception.hh>
#include <general/streamexception.hh>


namespace ILwd {


//-----------------------------------------------------------------------------
//
//: ILwd Reader.
//
// This class reads internal light-weight data from an istream.
//
class Reader
{
public:

   // Constructors / Destructor
   Reader( std::istream& in );
   Reader( const Reader& r );
   virtual ~Reader();

   // Operator overloads
   const Reader& operator=( const Reader& r );

   // Base accessor
   std::istream& getStream();

   // XML read methods
   bool readEq();
   std::string readName();

   // Non-XML read methods
   bool readChar( char c );
   bool readChar( char* c, char* r );
   bool readCharNot( char* c, char* r );
   bool readString( const char* s );
   std::string skipWhiteSpace();
   int get();
   int peek();
   void read( void* buffer, size_t s );
   void skip( size_t s );

   // Stream status
   bool good() const;
   bool fail() const;
   bool bad() const;
   bool eof() const;

   // pushback
   void pushback( char c );
   void pushback( const char* s );
   void pushback( const void* s, size_t n );

   // XML check static methods
   static bool isLetter( char c );
   static bool isDigit( char c );
   static bool isNameChar( char c );

private:

   void checkAndThrow() const;

   std::istream* mStream;
   std::stringstream* mBuffer;
};


//----------------//
// Inline Methods //
//----------------//

//***********/
/* Accessors */
//***********/


//-----------------------------------------------------------------------------
//
//: Get the underlying istream object.
//
//!return: istream& - 
//
inline std::istream& Reader::getStream()
{
    return *mStream;
}
 
} // namespace ILwd

#endif // ILwdReaderHH
