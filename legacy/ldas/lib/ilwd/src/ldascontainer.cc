/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "ilwd/config.h"

#include <cassert>
#include <typeinfo>
#include <sstream>   
#include <algorithm>   
#include <iterator>      
#include <iostream>   
   
#include "ldasarray.hh"
#include "ldascontainer.hh"
#include "ldasexternal.hh"
#include "ldasstring.hh"
#include "endtag.hh"

   
   
//#define LDAS_DEBUG      
   
#ifdef LDAS_DEBUG   
#define PRINT_DEBUG(a) std::cout << a << endl;
#else
#define PRINT_DEBUG(a)
#endif   
   
   
// Throw exception if container.size()/container.mOwns.size() 
// mismatch is detected
#define ENABLE_SIZE_MISMATCH_EXCEPTION      
   
   
// Terminate process if container.size()/container.mOwns.size() 
// mismatch is detected
//#define ENABLE_SIZE_MISMATCH_ASSERT   
   
   
#ifdef ENABLE_SIZE_MISMATCH_ASSERT   
#define LDAS_ASSERT(a) assert(a)   
#else
#define LDAS_ASSERT(a)
#endif   

   
#ifdef ENABLE_SIZE_MISMATCH_EXCEPTION   
   
#define CHECK_SIZE_MISMATCH \
    if( size() != mOwns.size() ) \
    { \
        ostringstream msg; \
        msg << ": LdasContainer: inconsistent container size: " \
            << " size=" << size() << " mOwns.size=" << mOwns.size() \
            << " FILE: " << __FILE__ << " LINE: " << __LINE__; \
\
        const string msg_str( msg.str() ); \
        throw runtime_error( msg_str.c_str() ); \
    }
   
#define CHECK_OBJECT_SIZE_MISMATCH(OBJECT) \
    if( OBJECT.size() != OBJECT.mOwns.size() ) \
    { \
        ostringstream msg; \
        msg << "LdasContainer: inconsistent container size: " \
            << " size=" << OBJECT.size() \
            << " mOwns.size=" << OBJECT.mOwns.size() \
            << " FILE: " << __FILE__ << " LINE: " << __LINE__; \
   \
        const string msg_str( msg.str() ); \
        throw runtime_error( msg_str.c_str() ); \
    }   
#else
#define CHECK_SIZE_MISMATCH     
#define CHECK_OBJECT_SIZE_MISMATCH   
#endif

   
using ILwd::LdasContainer;
using namespace std;   

//: Static data member initialization   
const std::string ILwd::LdasContainer::mIdent = std::string("ilwd");
   

//-----------------------------------------------------------------------------
//
//: (Default) Constructor.
//
// Creates a container with the given name.
//
//!param: const std::string& name - The name for the container.  Default:
//!param:     An empty string.
//
LdasContainer::LdasContainer( const std::string& name )
        : LdasElement( name ), std::vector< LdasElement* >(), mIsHashing( false ),
          mNameHash(), mFormat( USER_FORMAT ),
    mCompression( USER_COMPRESSION ), mHashPosition( 0 ), mOwns()
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;
}


//-----------------------------------------------------------------------------
//
//: Copy Constructor.
//
// This performs a deep copy of the container.
//
//!param: const LdasContainer& c - The element to copy from.
//
//!exc: bad_alloc - Memory allocation failed.
//
LdasContainer::LdasContainer( const LdasContainer& c )
   : LdasElement( c ), std::vector< LdasElement* >(),
     mIsHashing( c.mIsHashing ), mNameHash(),
     mFormat( c.mFormat ), mCompression( c.mCompression ),
     mHashPosition( c.mHashPosition ), mOwns()
{
    // Allocate memory and copy the elements
    // NOTE: initialization list is not setting mOwns to the c.mOwns:
    // push_back() method will update mOwns with new entry 
    // for each inserted ILwd element
    const_ownership_iterator own_iter( c.mOwns.begin() );
   
    for ( const_iterator iter = c.begin(), end_iter = c.end(); 
          iter != end_iter; ++iter, ++own_iter )
    {
        const allocate_type allocate( *own_iter == OWN ? ALLOCATE : NO_ALLOCATE );
        push_back( *iter, allocate, *own_iter );     
    }

   
    // Build a hash table base upon the element names
    rehash();
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
}


//-----------------------------------------------------------------------------
//
//: Input constructor.
//
// Constructs this object from an ILWD reader.
//
//!param: Reader& r - The reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//
LdasContainer::LdasContainer( Reader& r )
        : LdasElement(), std::vector< LdasElement* >(), mIsHashing( false ),
    mNameHash(), mFormat( USER_FORMAT ), mCompression( USER_COMPRESSION ),
    mHashPosition( 0 ), mOwns()
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;  
   
    try
    {
	read( r );
    }
    catch( ... )
    {
	//---------------------------------------------------------------
	// Cleanup anything that was successfully read
	//---------------------------------------------------------------
	setHashing( false );
	erase( begin(), end() );
	//---------------------------------------------------------------
	// rethrow the exception
	//---------------------------------------------------------------
	throw;
    }
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;     
}


//-----------------------------------------------------------------------------
//
//: Constructs this element from a stream, given the attributes.
//
// This constructor is used when the start-tag has already been read.  For
// example, if we do not know what type of element is coming next then the
// start-tag is read from the stream, the type of object identified, and
// the Reader&/Attributes& constructor is called.
//
//!todo: - Have the Reader class ID objects and pushback the start-tag.  Then
//!todo: we wouldn't need this method.
//
//!param: Reader& r - The reader to read from.
//!param: const Attributes& att - The attributes for this object.
//
//!exc: std::bad_alloc - Memory Allocation failed.
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//
LdasContainer::LdasContainer( Reader& r, const Attributes& att )
        : LdasElement(), std::vector< LdasElement* >(), mIsHashing( false ),
    mNameHash(), mFormat( USER_FORMAT ), mCompression( USER_COMPRESSION ),
    mHashPosition( 0 ), mOwns()
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;        
   
    read( r, att );
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;           
}


//-----------------------------------------------------------------------------
//
//: Destructor
//
LdasContainer::~LdasContainer()
{
    // This calls the redefined erase method which deletes all of the elements
    // in the container.
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;              
   
#ifdef LDAS_DEBUG      
    ostringstream top_debug_str;
    top_debug_str << static_cast< void* >( this )
                  << ": entering ~LdasContainer";
   
    if( size() )
    {
        top_debug_str << " elements: ";
        copy( begin(), end(), ostream_iterator< void* >( top_debug_str, " " ) );
   
        top_debug_str << " owns: ";
        copy( mOwns.begin(), mOwns.end(), ostream_iterator< int >( top_debug_str, " " ) );        
    }   
#endif   
   
    PRINT_DEBUG( top_debug_str.str() );   
   

    setHashing( false );
    erase( begin(), end() );
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                 
}

   
//-----------------------------------------------------------------------------
//: Gets the element ID of this type of object.
//
// The element ID is used to determine what type an object is without
// having to resort to RTTI or comparing identifiers.
//
//!return: ElementId - The element id (an enumeration).
//
ILwd::ElementId ILwd::LdasContainer::getElementId() const
{
    return ID_ILWD;
}
  
   
//-----------------------------------------------------------------------------
//: Gets the ownership of the specified by index container element.
//
//!param: size_t index - Index to specify container element.
//
//!return: bool - True if container owns the element, false otherwise.
//   
bool ILwd::LdasContainer::isOwned( size_t index ) const
{
    if ( index >= size() )
    {
        return false;
    }
    
    return ( mOwns[ index ] == OWN );
}
   

ILwd::LdasContainer::iterator ILwd::LdasContainer::begin()
{
    return std::vector< LdasElement* >::begin();
}

ILwd::LdasContainer::const_iterator ILwd::LdasContainer::begin() const
{
    return std::vector< LdasElement* >::begin();
}

ILwd::LdasContainer::iterator ILwd::LdasContainer::end()
{
    return std::vector< LdasElement* >::end();
}

ILwd::LdasContainer::const_iterator ILwd::LdasContainer::end() const
{
    return std::vector< LdasElement* >::end();
}

ILwd::LdasContainer::reverse_iterator ILwd::LdasContainer::rbegin()
{
    return std::vector< LdasElement* >::rbegin();
}

ILwd::LdasContainer::const_reverse_iterator ILwd::LdasContainer::rbegin() const
{
    return std::vector< LdasElement* >::rbegin();
}

ILwd::LdasContainer::reverse_iterator ILwd::LdasContainer::rend()
{
    return std::vector< LdasElement* >::rend();
}

ILwd::LdasContainer::const_reverse_iterator ILwd::LdasContainer::rend() const
{
    return std::vector< LdasElement* >::rend();
}
   
   
   
//-----------------------------------------------------------------------------
//
//: Assignment operator.
//
// This assigns another container object to this.  A deep copy is performed of
// all contained elements.
//
//!param: const LdasContainer& c - The container to assign from.
//
//!return: const LdasContainer& - A reference to this container.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
const LdasContainer& LdasContainer::operator=( const LdasContainer& c )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;
   
    if ( this != &c )
    {
        // First, call the assignment operator for the base class
        LdasElement::operator=( c );

        // Erase everything
        erase( begin(), end() );
        mOwns.erase( mOwns.begin(), mOwns.end() );
   
        // Copy the hashing information since it will be used by the push_back
        // method.  Copy ownership information.
        mIsHashing = c.mIsHashing;
        mHashPosition = c.mHashPosition;

        // Copy the data
        const_ownership_iterator o_iter = c.mOwns.begin();
   
        for ( const_iterator iter = c.begin(), end_iter = c.end(); 
              iter != end_iter; ++iter )
        {
            if ( ( *o_iter == OWN ) && *iter != 0 )
            {
                push_back( **iter );
            }
            else
            {
                // Since this element was unowned by the source container,
                // it is OK to cast away constness.
                push_back( const_cast< LdasElement* >( *iter ), 
                           ( *o_iter == OWN ) ? ALLOCATE : NO_ALLOCATE,
			   *o_iter );
            }
            
            ++o_iter;
        }

        // Copy format & compression
        mFormat = c.mFormat;
        mCompression = c.mCompression;
    }

   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;
   
   
    return *this;
}


//-----------------------------------------------------------------------------
//
//: Concatenation operator.
//
// This appends another container object to this.  A deep copy is performed of
// all contained elements.
//
//!param: const LdasContainer& c - The container to append from.
//
//!return: const LdasContainer& - A reference to this container.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
const LdasContainer& LdasContainer::operator+=( const LdasContainer& c )
{
  LDAS_ASSERT( size() == mOwns.size() );
  CHECK_SIZE_MISMATCH;   

   
  // Copy the data
  const_ownership_iterator o_iter = c.mOwns.begin();
   
  for ( const_iterator iter = c.begin(), end_iter = c.end(); 
        iter != end_iter; ++iter )
  {
    if ( ( *o_iter == OWN ) && *iter != 0 )
    {
       push_back( **iter );
    }
    else
    {
      // Since this element was unowned by the source container,
      // it is OK to cast away constness.
      push_back( const_cast< LdasElement* >( *iter ),
		 ( *o_iter == OWN ) ? ALLOCATE : NO_ALLOCATE,
		 *o_iter );
    }
    ++o_iter;
  }
   
  LDAS_ASSERT( size() == mOwns.size() );
  CHECK_SIZE_MISMATCH;      
   
   
  return *this;
}

//-----------------------------------------------------------------------------
//
//: Equal comparison operator.
//
// This compares this object to another container for equality.  Ownership is not
// compared.
//
//!param: const LdasContainer &c - The object to compare with.
//
//!return: bool - true if the objects are equal.
//
bool LdasContainer::
operator==( const LdasContainer& c ) const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;         
   
   
    if ( this == &c )
    {
        return true;
    }

    if ( ( size() != c.size() ) || (!LdasElement::operator==( c )) )
    {
        return false;
    }

    const_iterator iter1 = begin();
    const_iterator iter2 = c.begin();
    
    while ( iter1 != end() )
    {
        if ( ( *iter1 == 0 && *iter2 != 0 ) ||
             ( *iter1 != 0 && *iter2 == 0 ) )
        {
            return false;
        }
   
        // Both elements are NULL's
	if ( !(*iter1) )
	{
            ++iter1;
            ++iter2;
	    continue;
	}
	if ((*iter1)->getElementId() != (*iter2)->getElementId())
	{
	    return false;
	}
	bool r = false;
#define	COMPARE(id,type) \
case id: \
	r = (*(dynamic_cast<const type *>(*iter1)) == \
	     *(dynamic_cast<const type *>(*iter2))); \
        break

#define COMPARE_A(d_type) COMPARE(ID_##d_type, LdasArray<d_type>)
	switch((*iter1)->getElementId())
	{
	    COMPARE_A(CHAR);
	    COMPARE_A(CHAR_U);
	    COMPARE_A(INT_2S);
	    COMPARE_A(INT_2U);
	    COMPARE_A(INT_4S);
	    COMPARE_A(INT_4U);
	    COMPARE_A(INT_8S);
	    COMPARE_A(INT_8U);
	    COMPARE_A(REAL_4);
	    COMPARE_A(REAL_8);
	    COMPARE_A(COMPLEX_8);
	    COMPARE_A(COMPLEX_16);
	    COMPARE(ID_ILWD,LdasContainer);
	    COMPARE(ID_LSTRING,LdasString);
	    COMPARE(ID_EXTERNAL,LdasExternal);
	default:
	    throw std::bad_cast();
	}
#undef COMPARE_A
#undef COMPARE
	if (!r)
	{
	    return false;
	}
   
	++iter1;
	++iter2;
    }

    // We don't need to check the hash
    return ( (mFormat == c.mFormat) && (mCompression == c.mCompression) );
}

   
//-----------------------------------------------------------------------------
//
//: Not equal comparison operator.
//
// This compares this object to another LdasContainer for inequality.
//
//!param: const LdasContainer& c - The object to compare with.
//
//!return: bool - true if the containers are not equal.
//
bool ILwd::LdasContainer::operator!=( const LdasContainer& c ) const
{
    return !operator==( c );
}
   
   
//-----------------------------------------------------------------------------
//
//: Get the identifier.
//
// This returns the identifier for this element.  This is the static version.
//
//!return: const std::string& - The identifier.
//
const std::string& LdasContainer::getIdentifierStatic()
{
    return mIdent;
}


//-----------------------------------------------------------------------------
//
//: Get the identifier.
//
// Returns the identifier for this element.
//
//!return: const std::string& - The identifier.
//
const std::string& LdasContainer::getIdentifier() const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;            

    return getIdentifierStatic();
}


//-----------------------------------------------------------------------------
//
//: Insert an element into the container.
//
// This copies an element into the container at the desired position.
//
//!param: iterator pos - The position at which the item should be inserted.
//!param:     No checking is performed on the iterator, so if it is not valid
//!param:     the results are uncertain.
//!param: const LdasElement& b - A reference to the item to insert.  The item
//     will be COPIED into the container.
//
//!return: iterator - An iterator pointing to the location of the stored
//!return:     object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
LdasContainer::iterator LdasContainer::insert(
    iterator pos, const LdasElement& b )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;               
   
   
    // Remember the new pointer so we can put it in the hash
    LdasElement* e = b.createCopy();

    if( e == 0 )
    {
        throw bad_alloc();
    }


    // Update ownership
    mOwns.insert( mOwns.begin() + ( pos - begin() ),
		  OWN );   

    e->addContainerRef( this );   
   
    // Add the new element
    iterator iter = std::vector< LdasElement* >::insert( pos, e );

   
    // Now add the name (changed to lowercase) to the hash
    if ( mIsHashing )
    {
        mNameHash.insert(
            NameHash::value_type( e->getName( mHashPosition ), e ) );
    }
    
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                  
   
   
    return iter;
}


//-----------------------------------------------------------------------------
//
//: Insert an element into the container.
//
// This copies an element into the container at the desired position.
//
//!param: iterator pos - The position at which the item should be inserted.
//!param:     No checking is performed on the iterator, so if it is not valid
//!param:     the results are uncertain.
//!param: const LdasElement* b - A pointer to the item to insert.  The item
//     will be COPIED into the container.
//
//!return: iterator - An iterator pointing to the location of the stored
//!return:     object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//   
LdasContainer::iterator LdasContainer::insert(
    iterator pos, const LdasElement* b )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;            

    // Remember the new pointer so we can put it in the hash
    LdasElement* e( 0 );
    if( b != 0 )
    {
       e = b->createCopy();
    }

   
    if( e == 0 )
    {
        throw runtime_error( "LdasContainer::insert(): cannot insert NULL element" );
    }

    // Update ownership
    mOwns.insert( mOwns.begin() + ( pos - begin() ),
		  OWN );   

    e->addContainerRef( this );
   
    // Add the new element
    iterator iter = std::vector< LdasElement* >::insert( pos, e );
   

    // Now add the name (changed to lowercase) to the hash
    if ( mIsHashing )
    {
        mNameHash.insert(
            NameHash::value_type( e->getName( mHashPosition ), e ) );
    }
    
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;               
   
   
    return iter;
}


//-----------------------------------------------------------------------------
//
//: Insert an element into the container.
//
// This copies an element into the container at the desired position.
//
//!param: iterator pos - The position at which the item should be inserted.
//!param:     No checking is performed on the iterator, so if it is not valid
//!param:     the results are uncertain.
//!param: const LdasElement* b - A pointer to the item to insert. 
//!param: allocate_ allocate - The item will be COPIED into the container
//+          if this parameter is set to ALLOCATE. Possible values are
//+          ALLOCATE and NO_ALLOCATE.
//!param: own_type owns - A flag to indicate if object owns the inserted element.
//+          Values are NO_OWN or OWN.
//
//!return: iterator - An iterator pointing to the location of the stored
//!return:     object.
//
//!exc: std::bad_alloc - Memory allocation failed.
//   
LdasContainer::iterator LdasContainer::insert(
    iterator pos, LdasElement* b, allocate_type allocate, own_type owns )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                  
   
   
    if ( b == ( const LdasElement*)NULL )
    {
	throw std::range_error( "LdasContainer::insert(): cannot insert NULL element" );
    }   
   
    // Remember the new pointer so we can put it in the hash
    LdasElement* e( 0 );

    if ( allocate == ALLOCATE )
        e = b->createCopy();
    else
        e = b;

   
    if( e == 0 )
    {
        throw bad_alloc();
    }
   

    // Update ownership
    mOwns.insert( mOwns.begin() + ( pos - begin() ), owns );

    e->addContainerRef( this );

   
    // Add the new element
    iterator iter = std::vector< LdasElement* >::insert( pos, e );

    // Now add the name (changed to lowercase) to the hash
    if ( mIsHashing )
    {
        mNameHash.insert( NameHash::value_type( e->getName( mHashPosition ), e ) );
    }
    
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                     
   
   
    return iter;
}


//-----------------------------------------------------------------------------
//
//: Rehashs the container.
//
// The LdasContainer class constructs a hash on its elements based upon the
// first name in each element.  this method instructs the container to rehash
// all of the elements.  The user may wish to run this command if the name of
// a ocntained element is changed, thereby rendering the hash out-of-date.
//
void LdasContainer::rehash() const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                        
   
   
    if ( !mIsHashing )
    {
        return;
    }
    
    mNameHash.erase( mNameHash.begin(), mNameHash.end() );
    const_iterator end_iter( end() );

    for ( const_iterator iter = begin(); iter != end_iter; ++iter )
    {
        mNameHash.insert( NameHash::value_type(
            ( *iter != 0 ? (*iter)->getName( mHashPosition ) : "" ),
            *iter ) );
    }
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Find an element in the hash.
//
// This private method attempts to find the given name in the hash. The name
// must match the first name of an element within the container.  
//
//!param: const std::string& name - The first name of the element to search for.
//
//!return: NameHash::iterator - An iterator pointing to the requested element
//!return:     in the hash, if found.  If the name was not found, the iterator
//!return:     will point to the end of the hash.
//
//!todo: Consider not having the container rehash when a find fails.  Instead
//!todo: make the user rehash.
//
LdasContainer::NameHash::iterator
LdasContainer::findHash( const std::string& name ) const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                           
   
   
    // First, we'll just look for the name in the hash
    namehash_iterator iter = mNameHash.find( name );
    if ( iter != mNameHash.end() )
    {
        // We will double check the name of this object to make sure it
        // is correct.  It is possible that the name was changed, and the
        // user is querying the old value which should fail.
        const string& hash_name( iter->second == 0 ? "" : 
                                                     iter->second->getName( mHashPosition ) );
        if ( iter->second == 0 ||
             strcasecmp( iter->first.c_str(), hash_name.c_str() ) == 0 )
        {
	    LDAS_ASSERT( size() == mOwns.size() );
            CHECK_SIZE_MISMATCH;                           
   
            // OK, everything's fine.
            return iter;
        }
        else
        {
	    LDAS_ASSERT( size() == mOwns.size() );
            CHECK_SIZE_MISMATCH;                           
   
            // Nope, the name for this object has changed.  Rehash and
            // try again.
            rehash();
            return mNameHash.find( name );
        }
    }

    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
    return iter;
}


//-----------------------------------------------------------------------------
//
//: Find the position of an element.
//
// 
LdasContainer::iterator LdasContainer::
find( const ILwd::LdasElement* Element )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
   
    if ( Element == (ILwd::LdasElement*)NULL )
    {
	return end( );
    }
    iterator iter( begin() );

    for ( iterator end_iter = end(); (iter != end_iter) && ( *iter != Element );
          ++iter )
    {}

   
    return iter;
}

//-----------------------------------------------------------------------------
//
//: Find an element.
//
// This method finds an element in the container.  Each LdasElement can have
// an arbitary number of names.  When the element is written out in its ILWD
// form, these names appear as an attribute:
//
// name="name0:name1:name2:name3"
//
// This method allows the user to search for an element matching the specified
// string in the specified position. So,
//
// find( "collapse", 2 )
//
// would search for an element which had a name2 value of "collapse".
//
// If the user searches on the 'name0' field, then the container will use a
// hash to try and find the element quickly.
//
//!param: const std::string& name - The string to search for.
//!param: size_t pos - an integer specifiying which name attribute should be
//!param:     searched.
//
//!return: const LdasElement* - A pointer to the element which was found.  If
//!return:     an element satisfying the request could not be found, then a
//!return:     null pointer is returned.
//
const ILwd::LdasElement* LdasContainer::find( const std::string& name, size_t pos )
    const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;      
   
   
    if ( mIsHashing && pos == mHashPosition )
    {
        namehash_iterator iter = findHash( name );
        if ( iter == mNameHash.end() )
        {
            return 0;
        }
        
        return iter->second;
    }

   
    const_iterator iter( begin() ), end_iter( end() );
    const CHAR* const name_str( name.c_str() );
   
    while( iter != end_iter )
    {
       if( *iter != 0 )
       {
          const string& iter_name( (*iter)->getName( pos ) );
   
          if( strcasecmp( iter_name.c_str(), name_str ) == 0 )
          {
             // Found the match
             break;
          }
       }
   
       // Do we really want to match a NULL element: it
       // doesn't mean it's matching an empty string 
       //else if( *iter == 0 && name.empty() ){ break; }
       ++iter;
    }

    if ( iter == end() )
    {
       return 0;
    }

    return *iter;
}


//-----------------------------------------------------------------------------
//
//: Find an element.
//
// This method finds an element in the container.  Each LdasElement can have
// an arbitary number of names.  When the element is written out in its ILWD
// form, these names appear as an attribute:
//
// name="name0:name1:name2:name3"
//
// This method allows the user to search for an element matching the specified
// string in the specified position. So,
//
// find( "collapse", 2 )
//
// would search for an element which had a name2 value of "collapse".
//
// If the user searches on the 'name0' field, then the container will use a
// hash to try and find the element quickly.
//
//!param: const std::string& name - The string to search for.
//!param: size_t pos - an integer specifiying which name attribute should be
//!param:     searched.
//
//!return: LdasElement* - A pointer to the element which was found.  If
//!return:     an element satisfying the request could not be found, then a
//!return:     null pointer is returned.
//
ILwd::LdasElement* LdasContainer::find( const std::string& name, size_t pos )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;         
   
   
    if ( mIsHashing && pos == mHashPosition )
    {
        namehash_iterator iter = findHash( name );
        if ( iter == mNameHash.end() )
        {
            return 0;
        }
        
        return iter->second;
    }

   
    const_iterator iter( begin() ), end_iter( end() );
    const CHAR* const name_str( name.c_str() );
   
    while( iter != end_iter )
    {
       if( *iter != 0 )
       {
          const string& iter_name( (*iter)->getName( pos ) );
     
          if( strcasecmp( iter_name.c_str(), name_str ) == 0 )
          {
             break;
          }
       }
       // Do we really want to match a NULL element to the empty name string?  
       // else if( *iter == 0 && name.empty() ){ break; }   

       ++iter;
    }

    if ( iter == end() )
    {
       return 0;
    }

    return *iter;
}


//-----------------------------------------------------------------------------
//
//: Remove an element.
//
// Remove the element being pointed to by the iterator.
//
//!param: iterator pos - The element to remove.
//
//!return: iterator - An iterator to the element immediately following the one
//!return:     removed.
//
LdasContainer::iterator LdasContainer::erase( iterator pos )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;            

#ifdef LDAS_DEBUG
    ostringstream top_debug_str;
    top_debug_str << static_cast< void* >( this )
                  << ": entering LdasContainer::erase(pos)";
   
    if( size() )
    {
        top_debug_str << " elements: ";
        copy( begin(), end(), ostream_iterator< void* >( top_debug_str, " " ) );
   
        top_debug_str << " owns: ";
        copy( mOwns.begin(), mOwns.end(), ostream_iterator< int >( top_debug_str, " " ) );        
    }      
#endif   
   
   
    // There's nothing at the end to erase
    if( pos == end() )
    {
        return end();
    }
   
    if ( mIsHashing )
    {
        std::pair< namehash_iterator, namehash_iterator > iter_range = 
           mNameHash.equal_range( *pos != 0 ?
                                  (*pos)->getName( mHashPosition ) : "" );
   
        bool not_found( true );
        namehash_iterator iter( iter_range.first );
   
        while( not_found && iter != iter_range.second )
        {
           // If the multimap entry that corresponds to the 
           // container element to be deleted was found,
           // delete it
           if( iter->second == *pos  )
           {
               mNameHash.erase( iter );
               not_found = false;
           }
           else
           {
              ++iter;
           }
        }
    }

   
#ifdef LDAS_DEBUG      
    ostringstream debug_str;
    debug_str << static_cast< void* >( this )
              << ": LdasContainer::erase(pos) removeContRef for "
              << static_cast< void* >( *pos );
#endif   
    PRINT_DEBUG( debug_str.str() );   
   
   
    // Remove container reference in case child doesn't 
    // belong to "this" container
    ( *pos )->removeContainerRef( this );
   
   
    // Now destruct the object and remove it from the array.
    ownership_iterator own_iter( mOwns.begin() );
   
    std::advance( own_iter, ( pos - begin() ) );
   
    if ( *own_iter == OWN )
    {
	delete *pos;
        *pos = 0;
    }


    mOwns.erase( own_iter );
   
    LdasContainer::iterator retval( std::vector< LdasElement* >::erase( pos ) );
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;               
   
    return retval;
}


//-----------------------------------------------------------------------------
//
//: Remove a range of elements.
//
// Removes a range of elements in the container.  The range removed is
// [first, last).
//
//!param iterator first - An iterator to the first element to remove.
//!param iterator last - An iterator indicating the last element to remove.
//!param     This element will NOT be removed, the element which appears before
//!param     this one will be the last element removed.
//
//!return: iterator - An iterator to the element immediately following the last
//!return:     element removed from the container.
//
LdasContainer::iterator LdasContainer::erase(
    iterator first, iterator last )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                  
   
#ifdef LDAS_DEBUG   
    ostringstream top_debug_str;
    top_debug_str << static_cast< void* >( this )
                  << ": entering LdasContainer::erase(first,last)";
   
    if( size() )
    {
        top_debug_str << " elements: ";
        copy( begin(), end(), ostream_iterator< void* >( top_debug_str, " " ) );
   
        top_debug_str << " owns: ";
        copy( mOwns.begin(), mOwns.end(), ostream_iterator< int >( top_debug_str, " " ) );        
    }   
#endif   
   
   
    // There's nothing at the end to erase
    if( first == end() )
    {
       return end();
    }
   
   
    const_ownership_iterator owns_iter( mOwns.begin() +
			   	        ( first - begin() ) );
   
    for ( iterator iter = first; iter != last; ++iter, ++owns_iter )
    {
	LDAS_ASSERT( size() == mOwns.size() );
        CHECK_SIZE_MISMATCH;                     
   
        if ( mIsHashing )
        {
           // Remove the item from the hash   
           std::pair< namehash_iterator, namehash_iterator > iter_range = 
              mNameHash.equal_range( *iter != 0 ?
                                     (*iter)->getName( mHashPosition ) : "" );
   
           bool not_found( true );
           namehash_iterator niter( iter_range.first );
   
           while( not_found && niter != iter_range.second )
           {
              // If the multimap entry that corresponds to the 
              // container element to be deleted was found,
              // delete it
              if( niter->second == *iter  )
              {
                  mNameHash.erase( niter );
                  not_found = false;
              }
              else
              {
                 ++niter;
              }
           }   
        }
        
	LDAS_ASSERT( size() == mOwns.size() );
        CHECK_SIZE_MISMATCH;    
   

#ifdef LDAS_DEBUG      
       ostringstream debug_str;
       debug_str << static_cast< void* >( this )
                 << ": LdasContainer::erase(first,last) removeContRef for "
                 << static_cast< void* >( *iter );
#endif   
       PRINT_DEBUG( debug_str.str() );   
   
   
        // Remove container reference in case child doesn't 
        // belong to "this" container
        ( *iter )->removeContainerRef( this );
   
   
        // Destruct the element
	if ( *( owns_iter ) == OWN )
	{
	    delete *iter;
            *iter = 0;
	}
    }

    // erase the range from the vector
    ownership_iterator own_first( mOwns.begin() ),
                       own_last( mOwns.begin() );
   
    std::advance( own_first, ( first - begin() ) );
    std::advance( own_last, ( last - begin() ) );
   
    mOwns.erase( own_first, own_last );
    
    LdasContainer::iterator retval( std::vector< LdasElement* >::erase( first, last ) );
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
    return retval;
}


//-----------------------------------------------------------------------------
//
//: Appends an element to the container.
//
// This pushes an element onto the end of the container.
//
//!param: const LdasElement& b - The element to add.  This element is COPIED
//!param:     into the container.
//
//!exc: std::bad_alloc - Memory allocation failed.
//
void LdasContainer::push_back( const LdasElement& b )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH; 
   
   
    // Create a copy
    LdasElement* e = b.createCopy();

    if( e == 0 )
    {
        throw bad_alloc();
    }

   
    // Add it to the hash
    if ( mIsHashing )
    {
        mNameHash.insert(
            NameHash::value_type( e->getName( mHashPosition ), e ) );
    }

    // Add parent reference to new element
    e->addContainerRef( this );      
   
    // Save the element
    mOwns.push_back( OWN );
    std::vector< LdasElement* >::push_back( e );

   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;    
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Appends an element to the container.
//
// This pushes an element onto the end of the container.
//
//!param: const LdasElement* b - The element to add.  This element is COPIED
//!param:     into the container.
//
//!exc: std::range_error - b was a NULL pointer
//!exc: std::bad_alloc - Memory allocation failed.
//   
void LdasContainer::push_back( const LdasElement* b )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;       
   
   
    if ( b == ( const LdasElement*)NULL )
    {
	throw std::range_error( "Cannot add NULL element" );
    }
   
    // Create a copy
    LdasElement* e( b->createCopy() );
   
    if( e == 0 )
    {
        throw bad_alloc();
    }


    // Add it to the hash
    if ( mIsHashing )
    {
        mNameHash.insert( NameHash::value_type( e->getName( mHashPosition ), e ) );
    }
    
    // Save the element
    mOwns.push_back( OWN );
   
    // Add parent reference to new element
    e->addContainerRef( this );      
   
    std::vector< LdasElement* >::push_back( e );
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Appends an element to the container.
//
// This pushes an element onto the end of the container.
//
//!param: const LdasElement*b - The element to add. 
//!param: allocate_ allocate - The item will be COPIED into the container
//+          if this parameter is set to ALLOCATE. Possible values are
//+          ALLOCATE and NO_ALLOCATE.
//!param: own_type owns - A flag to indicate if object owns the inserted element.
//+          Values are NO_OWN or OWN.
//
//!exc: std::range_error - b was a NULL pointer
//!exc: std::bad_alloc - Memory allocation failed.
//   
void LdasContainer::push_back( LdasElement* b,
			       allocate_type allocate, own_type owns )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;      
   
   
    if ( b == ( const LdasElement*)NULL )
    {
	throw std::range_error( "Cannot add NULL element" );
    }
   
    // Create a copy
    LdasElement* e( 0 );
    if ( allocate == ALLOCATE )
        e = b->createCopy();
    else
        e = b;
   
   
    if( e == 0 )
    {
        throw bad_alloc();
    }

   
    // Add it to the hash
    if ( mIsHashing )
    {
        mNameHash.insert( NameHash::value_type( e->getName( mHashPosition ), e ) );
    }
    
    // Save the element
    mOwns.push_back( owns );
   
    // Add parent reference to new element
    e->addContainerRef( this );      
   
    std::vector< LdasElement* >::push_back( e );
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;         
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Removes the last element from the container.
//
void LdasContainer::pop_back()
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
   
    if ( mIsHashing )
    {
       // Search for the element in the hash
       std::pair< namehash_iterator, namehash_iterator > iter_range = 
          mNameHash.equal_range( back() != 0 ?
                                 back()->getName( mHashPosition ) : "" );
   
       bool not_found( true );
       namehash_iterator niter( iter_range.first );
   
       while( not_found && niter != iter_range.second )
       {
          // If the multimap entry that corresponds to the 
          // container element to be deleted was found,
          // delete it
          if( niter->second == back()  )
          {
              mNameHash.erase( niter );
              not_found = false;
          }
          else
          {
             ++niter;
          }
       }
    }

   
#ifdef LDAS_DEBUG      
    ostringstream debug_str;
    debug_str << static_cast< void* >( this )
              << ": LdasContainer::pop_back removeContRef for "
              << static_cast< void* >( back() );
#endif   
    PRINT_DEBUG( debug_str.str() );   
   
   
    // Remove container reference in case child doesn't 
    // belong to "this" container   
    back()->removeContainerRef( this );
    
   
    // Delete element only if container owns it
    if( mOwns.back() == OWN )
    {
       delete back();
    }
   
    mOwns.pop_back();
    std::vector< LdasElement* >::pop_back();
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;      
   
    return;
}

    
//-----------------------------------------------------------------------------
//   
//: Reads the object of container type from Reader.
//
//!param: Reader& r - A reference to the reader to read from.
//
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: StreamException - Stream error occured.
//!exc: FormatException - Invalid format was specified.
//   
void LdasContainer::read( Reader& r )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;         
   
    StartTag st( r );
    
    if ( st.getIdentifier() != getIdentifier() )
    {
        string msg( "The start-tag for this object was not found, expecting <" );
        msg += getIdentifier();
        msg += ">, found <";
        msg += st.getIdentifier();
        msg += ">.";
   
        throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_START_TAG, msg );
    }

    read( r, st.getAttributes() );
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;            
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Constructs this element from a stream, given the attributes.
//
// This constructor is used when the start-tag has already been read.  For
// example, if we do not know what type of element is coming next then the
// start-tag is read from the stream, the type of object identified, and
// the Reader&/Attributes& constructor is called.
//
//!todo: - Have the Reader class ID objects and pushback the start-tag.  Then
//!todo: we wouldn't need this method.
//
//!param: Reader& r - The reader to read from.
//!param: const Attributes& att - The attributes for this object.
//
//!exc: StreamException - An error occurred while reading from the stream.
//!exc: FormatException - The format for the LdasString was invalid.
//
void LdasContainer::read( Reader& r, const Attributes& att )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;               

   
    erase( begin(), end() );
        
    readAttributes( att );
    size_t s;
    
    Attributes::const_iterator iter = att.find( "size" );
    if ( iter != att.end() )
    {
        s = iter->second.getValue< size_t >();
    }
    else
    {
        s = 1;
    }

    for ( size_t i = s; i != 0; --i )
    {
        r.skipWhiteSpace();
        
        push_back( LdasElement::createElement( r ), NO_ALLOCATE, OWN );
    }

    r.skipWhiteSpace();

    // Read & Check the end-tag.
    EndTag et( r );
    if ( et.getIdentifier() != getIdentifier() )
    {
        ostringstream ss;
        ss << "Expecting </ilwd>, found ";
        et.write( ss );
        ss << '.';
        throw ILWD_FORMATEXCEPTION_INFO( Errors::BAD_END_TAG, ss.str() );
    }

    rehash();
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                  
   
    return;
}

        
//-----------------------------------------------------------------------------
//
//: Writes the object to a stream.
//
// Writes the array to a stream with the given format and compression.
//
//!param: ostream& stream - The stream to write to.
//!param: Format format - The format to write in.  Default: ASCII.
//!param: Compression compression - The compression to use.  The default is
//!param:     NO_COMPRESSION.
//
//!exc: exception - An unknown error occurred.
//
void LdasContainer::write( std::ostream& stream, Format format,
                           Compression compression ) const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
    Format f = ( format == USER_FORMAT ? mFormat : format );
    Compression c =
        ( compression == USER_COMPRESSION ? mCompression : compression );
    
    Attributes att;
    writeAttributes( att );

    if ( size() != 1 )
    {
        att.addAttribute( "size", size() );
    }

    StartTag st( getIdentifier(), att );
    st.write( stream );

    for ( const_iterator iter = begin(); iter != end(); ++iter )
    {
        if( *iter == 0 )
        {
           throw ILWD_FORMATEXCEPTION_INFO( Errors::INVALID_FORMAT, 
                                            "null_container_element" );
        }
   
        (*iter)->write( stream, f, c );
    }

    EndTag et( getIdentifier() );
    et.write( stream );
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Writes the object to a stream with indentation.
//
// Writes the array to a stream with the given format and compression.
// Contained elements are indented based upon the passed parameters.
//
//!param: size_t indent - The number of spaces to indent the container.
//!param: size_t delta - The number of spaces to indent the elements in the
//!param:     container.  This is relative to the indentation of the container.
//!param: ostream& stream - The stream to write to.
//!param: Format format - The format to write in.  Default: ASCII.
//!param: Compression compression - The compression to use.  The default is
//!param:     NO_COMPRESSION.
//
//!exc: std::exception - An unknown error occurred.
//!exc: null_container_element - One of the container elements is NULL.     
//
void LdasContainer::write( size_t indent, size_t delta,
                           std::ostream& stream, Format format,
                           Compression compression ) const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;      
   
   
    Format f = ( format == USER_FORMAT ? mFormat : format );
    Compression c =
        ( compression == USER_COMPRESSION ? mCompression : compression );
    
    // Load the base (LdasElement) attributes for this container
    Attributes att;
    writeAttributes( att );

    // Set the size attribute if necessary
    if ( size() != 1 )
    {
        att.addAttribute( "size", size() );
    }


    // Create a buffer which holds the correct number of spaces to indent the
    // contained elements.
    static const CHAR indent_char( ' ' );
    size_t newIndent( indent + delta );
    const string buffer( newIndent, indent_char );

    // Write out the start-tag
    stream.write( buffer.c_str(), indent );
    StartTag st( getIdentifier(), att );
    st.write( stream );
    stream << endl;

    // Now iterate through the contained elements, writing them.
    for ( const_iterator iter = begin(); iter != end(); ++iter )
    {
        if( *iter == 0 )
        {
           throw ILWD_FORMATEXCEPTION_INFO( Errors::INVALID_FORMAT, 
                                            "null_container_element" );
        }
   
        if ( (*iter)->getElementId() == ID_ILWD )
        {
            // If the contained element is itself a container, then write it
            // out with indentation also.
            dynamic_cast< LdasContainer& >( **iter ).write(
                newIndent, delta, stream, f, c );
        }
        else
        {
            stream << buffer.c_str();
            (*iter)->write( stream, f, c );
        }
        stream << endl;
    }

    stream.write( buffer.c_str(), indent );
    EndTag et( getIdentifier() );
    et.write( stream );
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;         
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Create a copy of this element.
//
// This method allocates memory and creates a copy of this string object.  This
// is a virtual method which allows one to copy an LdasElement without knowing
// what its exact type is.
//
//!return: LdasContainer* - A pointer to a newly allocated copy of this object.
//
//!except: std::bad_alloc - Memory allocation failed.
//
LdasContainer* LdasContainer::createCopy() const
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;            
   
   
    return new LdasContainer( *this );
}


//-----------------------------------------------------------------------------
//
//: Sets hashing for the object.
//
//!param: bool hashing - A boolean value to trigger hashing.
//
void LdasContainer::setHashing( bool hashing )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;               
   
    if ( hashing == mIsHashing )
    {
        return;
    }

    mIsHashing = hashing;
    
    if ( hashing )
    {
        rehash();
    }
    else
    {
        mNameHash.erase( mNameHash.begin(), mNameHash.end() );
    }
   
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;                  
   
    return;
}


//-----------------------------------------------------------------------------
//
//: Sets hashing for the object.
//
//!param: bool hashing - A boolean value to trigger hashing.
//!param: size_t pos - Start position for hashing.   
//   
void LdasContainer::setHashing( bool hashing, size_t pos )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;
   
   
    if ( hashing == mIsHashing )
    {
        if ( pos != mHashPosition )
        {
            mHashPosition = pos;
            if ( mIsHashing )
            {
                rehash();
            }
        }
    }
    else
    {
        mIsHashing = hashing;
        
        if ( mIsHashing )
        {
            rehash();
        }
        else
        {
            mNameHash.erase( mNameHash.begin(), mNameHash.end() );
        }
    }
   
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;   
   
    return;
}

   
//-----------------------------------------------------------------------------
//
//: Unset ownership for specific child element.
//
// This method was introduced exclusively for use by LdasElement destructor.
// It handles proper memory management if container owns the child, and that
// child is destructed directly by the caller.
//   
//!param: const_iterator iter - Element's position which ownership should
//+       be unset.   
//   
//!return: Nothing.
//   
void LdasContainer::unsetOwnership( const_iterator iter )
{
    if( iter == end() )
    {
        return;
    }
   
    ownership_iterator own_iter( mOwns.begin() );    
    std::advance( own_iter, ( iter - begin() ) );
   
    *own_iter = NO_OWN;
    return;
}
   

//-----------------------------------------------------------------------------
//
//: Sets hashing position.
//
//!param: size_t pos - Hashing position.
//   
void LdasContainer::setHashPosition( size_t pos )
{
    LDAS_ASSERT( size() == mOwns.size() );
    CHECK_SIZE_MISMATCH;      
   
    if ( mHashPosition == pos )
    {
        return;
    }

    if ( mIsHashing )
    {
        rehash();
    }
   
    return;
}


//-----------------------------------------------------------------------------
// ObjectSpace
//-----------------------------------------------------------------------------


#ifdef HAVE_LIBOSPACE
#include <ospace/source.h>
#include <ospace/uss/std/string.h>
#include <ospace/uss/std/vector.h>
#include <ospace/io/device.h>
#include <ospace/io/iocontrl.h>

#include "ospaceid.hh"

OS_STREAMABLE_1( (ILwd::LdasContainer*), Stream::ILwd::LDASCONTAINER,
                 (ILwd::LdasElement*) )
OS_STREAMABLE_0( (std::vector< ILwd::LdasElement* >*),
                 Stream::ILwd::LDASCONTAINERVECT )

void os_write( os_bstream& stream, const LdasContainer& o ) {
    LDAS_ASSERT( o.size() == o.mOwns.size() );
    CHECK_OBJECT_SIZE_MISMATCH(o);   
   
    char	key;
    os_device*	dev = stream.adapter().device();

    dev->get_flush_lock(&key, 8 * 1024);
    try {
      os_write( stream, (ILwd::LdasElement&)( o ) );
      stream << (unsigned int)( o.size( ) );
      for ( std::vector< ILwd::LdasElement* >::const_iterator
		cur = o.begin( ),
		last = o.end( );
	    cur != last;
	    ++cur )
      {
	  stream << *cur;
      }
      
      stream << o.mIsHashing << int( o.mFormat ) << int( o.mCompression );
    }
    catch (...)
    {
      dev->rel_flush_lock(&key);	// Release the lock
      throw;				// Re-throw the exception
    }
    dev->rel_flush_lock(&key);
   
   
    LDAS_ASSERT( o.size() == o.mOwns.size() );
    CHECK_OBJECT_SIZE_MISMATCH(o);
   
    return;
}

void os_read( os_bstream& stream, LdasContainer& o )
{
    LDAS_ASSERT( o.size() == o.mOwns.size() );
    CHECK_OBJECT_SIZE_MISMATCH(o);
   
    char	key;
    os_device*	dev = stream.adapter().device();

    // Will clear mOwns vector as well
    o.erase( o.begin(), o.end() );
    
    dev->get_flush_lock(&key, 8 * 1024);

    try {
      os_read( stream, (ILwd::LdasElement&)o );
      os_read( stream, (std::vector< ILwd::LdasElement* >&)o );

      o.mOwns.resize( o.size(), LdasContainer::OWN );

      int format, compression;
    
      stream >> o.mIsHashing >> format >> compression;
      o.mFormat = ILwd::Format( format );
      o.mCompression = ILwd::Compression( compression );
      
      o.rehash();
      for ( LdasContainer::iterator e = o.begin();
	    e != o.end();
	    e++ )
      {
	  (*e)->addContainerRef( &o );
      }
    }
    catch (...)
    {
      dev->rel_flush_lock(&key);	// Release the lock
      o.mOwns.resize( o.size(), LdasContainer::OWN );
      throw; 				// Re-throw the exception
    }

    dev->rel_flush_lock(&key);
   
   
    LDAS_ASSERT( o.size() == o.mOwns.size() );
    CHECK_OBJECT_SIZE_MISMATCH(o); 
   
    return;
}

#endif // HAVE_LIBOSPACE
