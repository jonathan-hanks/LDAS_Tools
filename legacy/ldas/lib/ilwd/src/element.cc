#include "ilwd/config.h"

#include "element.hh"

using ILwd::Element;


//-----------------------------------------------------------------------------
// ObjectSpace
//-----------------------------------------------------------------------------

#ifdef HAVE_LIBOSPACE

#include <ospace/source.h>
#include "ospaceid.hh"

OS_NO_FACTORY_STREAMABLE_0( (ILwd::Element*), Stream::ILwd::ELEMENT )

void os_write( os_bstream& stream, const Element& o )
{}

void os_read( os_bstream& stream, Element& o )
{}

#endif // HAVE_LIBOSPACE
