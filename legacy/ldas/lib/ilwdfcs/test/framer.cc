/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "ilwdfcs/config.h"

#include <sstream>

#include "general/types.hh"
#include "general/unittest.h"

#include "ilwd/ldascontainer.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrDetector.hh"
#include "ilwdfcs/FrHistory.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"
#include "ilwdfcs/FrTable.hh"

General::UnitTest	Test;

using namespace ILwdFCS;
using namespace std;   
   

#if 0
#define AT() std::cerr << "DEBUG: AT: " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#endif

static void
test_frvect( )
{
  COMPLEX_8	data[3] =
  {
    COMPLEX_8(7.7956095e-03,0.0000000e+00),
    COMPLEX_8(1.1365998e-03,0.0000000e+00),
    COMPLEX_8(3.6299421e-04,0.0000000e+00)
  };

  ILwd::LdasArrayBase::size_type	ndims = 1;
  ILwd::LdasArrayBase::size_type	dims[] = { 3 };
  double	startX[] = { 80 };
  std::string	unitX[] = { std::string( "hz" ) };
  double	dx[] = { 64.0 };
  std::string	unitY( "s^2" );

  FrVect	v;

  v.SetData( data, ndims, dims, startX, unitX, dx, unitY );
}

static void
multi_frprocs( )
{
  const ILwd::LdasArrayBase::size_type n = 3;
  COMPLEX_8	data[n] =
  {
    COMPLEX_8(7.7956095e-03,0.0000000e+00),
    COMPLEX_8(1.1365998e-03,0.0000000e+00),
    COMPLEX_8(3.6299421e-04,0.0000000e+00)
  };

  ILwd::LdasArrayBase::size_type	ndims = 1;
  ILwd::LdasArrayBase::size_type	dims[] = { n };
  double	startX[] = { 80 };
  std::string	unitX[] = { std::string( "hz" ) };
  double	dx[] = { 64.0 };
  std::string	unitY( "s^2" );

  const INT_4U  nProcs = 3;
  FrProcData	frproc[nProcs];
  FrVect	frvect[nProcs];

  double	delta = 3.125e-02;
  const INT_4U  t0 = 48811610UL;
  GPSTime	start(t0, 0);

  FrameH	frame;

  frame.SetRun(1);
  frame.SetFrameNumber(0);
  frame.SetDataQuality(1);
  frame.SetStartTime(start);
  frame.SetGPSLeapSeconds(32);
  frame.SetDeltaTime(100.0);

  FrHistory fhist1("Frame history 1", "FrameHistory1", t0);
  FrHistory fhist2("Frame history 1", "FrameHistory1", t0);
  
  frame.AppendHistory(fhist1);
  frame.AppendHistory(fhist2);

  FrHistory hist1("Test history 1", "History1", t0);
  FrHistory hist2("Test history 2", "History2", t0 + 1);

  float	rowData[n] = { 0, 1, 2 };
  FrVect row;

  row.SetData( rowData, n, 0.0, "cols", 1.0, "values" );
  FrTable table;
  table.AppendRow(row);
  
  for ( unsigned int k = 0; k < nProcs; ++k )
  {
    frproc[k].SetName("TestProc");

    ostringstream oss;

    oss << "frproc[" << k << "]";
    frproc[k].SetComment(oss.str());
    
    frproc[k].SetType(1);
    frproc[k].SetSubType(0);

    frproc[k].SetTimeOffset( 0.0 );
    frproc[k].SetTRange(100);

    frproc[k].SetFShift( 0.0 );
    frproc[k].SetPhase( 0.0 );

    frproc[k].SetFRange(50);

    frproc[k].SetBW( 0.0 );

    frproc[k].AppendAuxParam("param0", 0.0);
    frproc[k].AppendAuxParam("param1", 1.0);
    frproc[k].AppendAuxParam("param2", 2.0);

    (frvect[k]).SetData( data, ndims, dims, startX, unitX, dx, unitY );

    frproc[k].AppendData( frvect[k] );

    frproc[k].AppendTable( table );

    frproc[k].AppendHistory( hist1 );
    frproc[k].AppendHistory( hist2 );

    // These are a bit strange, shouldn't they be in the frame?
    frproc[k].SetStartTime( start );
    frproc[k].SetStopTime( start + delta );

    start += delta;
    frame.AppendProcData( frproc[k] );
    AT( );
  }

  AT( );
  ILwd::LdasContainer*	ilwd = frame.ConvertToILwd( 8, "test", "1", true );

  AT( );
  Test.Message() << "multi_frprocs: ";
  AT( );
  ilwd->write( 2, 2, Test.Message( false ), ILwd::ASCII );
  AT( );
  Test.Message( false ) << std::endl;
  AT( );
  delete ilwd;
  AT( );
}

static void
test_frdetector( )
{
  FrameH	frame;

  FrDetector	frdetector[3];
  REAL_8	lat( 3.0 );

  for ( unsigned int x = 0; x < sizeof(frdetector)/sizeof(*frdetector); x++ )
  {
    frdetector[x].SetLongitude( 0.0 );
    frdetector[x].SetLatitude( lat );
    frdetector[x].SetLongitude( 0.0 );

    frame.AppendDetector( frdetector[x] );
    lat += 1.0;
  }

  ILwd::LdasContainer*	ilwd = frame.ConvertToILwd( 8, "test", "1", true );

  Test.Message() << "FrDetectors: ";
  ilwd->write( 2, 2, Test.Message( false ), ILwd::ASCII );
  Test.Message( false ) << std::endl;
  delete ilwd;
}

int
main( int ArgC, char **ArgV )
{
  AT( );
  Test.Init(ArgC, ArgV);

  using namespace ILwdFCS;

  AT( );
  FrameH		header;
  FrHistory		history_1("89 109", "NoHistoryPresent_0");
  FrHistory		history_2("some test data", "NoHistoryPresent_1");
  FrHistory		history_3("89 109", "NoHistoryPresent_0");
  FrHistory		history_4("some test data", "NoHistoryPresent_1");
  FrProcData		proc_data[3];
  FrVect		vect_data[3];
  AT( );
  COMPLEX_8	data[3][9] =
  {
    { COMPLEX_8(0.0000000e+00, 0.0000000e+00),
      COMPLEX_8(9.6219733e-02, 0.0000000e+00),
      COMPLEX_8(1.2027467e-02, 0.0000000e+00),
      COMPLEX_8(3.5636937e-03, 0.0000000e+00),
      COMPLEX_8(1.5034333e-03, 0.0000000e+00),
      COMPLEX_8(7.6975784e-04, 0.0000000e+00),
      COMPLEX_8(4.4546172e-04, 0.0000000e+00),
      COMPLEX_8(2.8052399e-04, 0.0000000e+00),
      COMPLEX_8(1.8792917e-04, 0.0000000e+00)
      },
      { COMPLEX_8(0.0000000e+00, 0.0000000e+00),
	COMPLEX_8(9.6219733e-02, 0.0000000e+00),
	COMPLEX_8(1.2027467e-02, 0.0000000e+00),
	COMPLEX_8(3.5636937e-03, 0.0000000e+00),
	COMPLEX_8(1.5034333e-03, 0.0000000e+00),
	COMPLEX_8(7.6975784e-04, 0.0000000e+00),
	COMPLEX_8(4.4546172e-04, 0.0000000e+00),
	COMPLEX_8(2.8052399e-04, 0.0000000e+00),
	COMPLEX_8(1.8792917e-04, 0.0000000e+00)
      },
      { COMPLEX_8(0.0000000e+00, 0.0000000e+00),
	COMPLEX_8(9.6219733e-02, 0.0000000e+00),
	COMPLEX_8(1.2027467e-02, 0.0000000e+00),
	COMPLEX_8(3.5636937e-03, 0.0000000e+00),
	COMPLEX_8(1.5034333e-03, 0.0000000e+00),
	COMPLEX_8(7.6975784e-04, 0.0000000e+00),
	COMPLEX_8(4.4546172e-04, 0.0000000e+00),
	COMPLEX_8(2.8052399e-04, 0.0000000e+00),
	COMPLEX_8(1.8792917e-04, 0.0000000e+00)
     }
  };
  INT_4U		time_offset[3][2] = {
    { 0UL, 0UL },
    { 0UL, 31250000UL },
    { 0UL, 62500000UL }
  };
  INT_4U		stop_times[3][2] = {
    { 488811610UL, 31250000UL },
    { 488811610UL, 62500000UL },
    { 488811610UL, 93750000UL }
  };
  
  std::istringstream	ilwd_text(
"<ilwd comment='mdd' metadata='outputname=file\\:/Z-P_LDAS_5_framer-488811610-1.gwf' name='::Frame' size='8'>"
"    <int_4s name='run'>0</int_4s>"
"    <int_4u name='frame'>0</int_4u>"
"    <int_4u name='dataquality'>0</int_4u>"
"    <int_2u name='ULeapS'>29</int_2u>"
"    <int_4u dims='2' name='GTime'>488811610 0</int_4u>"
"    <real_8 name='dt'>9.3750000000000000e-02</real_8>"
"    <ilwd name=':procData:Container(ProcData):Frame' size='3'>"
"      <ilwd name='TestProc_0::ProcData:488811610:0:Frame' size='4'>"
"        <lstring name='comment' size='24'>multiDimData to ProcData</lstring>"
"        <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"        <real_8 name='timeOffset'>0.0000000000000000e+00</real_8>"
"        <ilwd name=':data:Container(Vect):Frame'>"
"          <complex_8 dims='9' dx='0' units='dimensionless'>0.0000000e+00 0.0000000e+00 9.6219733e-02 0.0000000e+00 1.2027467e-02 0.0000000e+00 3.5636937e-03 0.0000000e+00 1.5034333e-03 0.0000000e+00 7.6975784e-04 0.0000000e+00 4.4546172e-04 0.0000000e+00 2.8052399e-04 0.0000000e+00 1.8792917e-04 0.0000000e+00</complex_8>"
"        </ilwd>"
"      </ilwd>"
"      <ilwd name='TestProc_1::ProcData:488811610:0:Frame' size='4'>"
"        <lstring name='comment' size='24'>multiDimData to ProcData</lstring>"
"        <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"        <real_8 name='timeOffset'>3.1250000000000000e-01</real_8>"
"        <ilwd name=':data:Container(Vect):Frame'>"
"          <complex_8 dims='9' dx='0' units='dimensionless'>0.0000000e+00 0.0000000e+00 9.6219733e-02 0.0000000e+00 1.2027467e-02 0.0000000e+00 3.5636937e-03 0.0000000e+00 1.5034333e-03 0.0000000e+00 7.6975784e-04 0.0000000e+00 4.4546172e-04 0.0000000e+00 2.8052399e-04 0.0000000e+00 1.8792917e-04 0.0000000e+00</complex_8>"
"        </ilwd>"
"      </ilwd>"
"      <ilwd name='TestProc_2::ProcData:488811610:0:Frame' size='4'>"
"        <lstring name='comment' size='24'>multiDimData to ProcData</lstring>"
"        <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"        <real_8 name='timeOffset'>6.2500000000000000e-01</real_8>"
"        <ilwd name=':data:Container(Vect):Frame'>"
"          <complex_8 dims='9' dx='0' units='dimensionless'>0.0000000e+00 0.0000000e+00 9.6219733e-02 0.0000000e+00 1.2027467e-02 0.0000000e+00 3.5636937e-03 0.0000000e+00 1.5034333e-03 0.0000000e+00 7.6975784e-04 0.0000000e+00 4.4546172e-04 0.0000000e+00 2.8052399e-04 0.0000000e+00 1.8792917e-04 0.0000000e+00</complex_8>"
"        </ilwd>"
"      </ilwd>"
"    </ilwd>"
"    <ilwd name=':history:Container(History):Frame' size='4'>"
"      <ilwd name='NoHistoryPresent_0::History:Frame' size='2'>"
"        <int_4u name='time'>0</int_4u>"
"        <lstring name='comment' size='6'>89 109</lstring>"
"      </ilwd>"
"      <ilwd name='NoHistoryPresent_1::History:Frame' size='2'>"
"        <int_4u name='time'>0</int_4u>"
"        <lstring name='comment' size='14'>some test data</lstring>"
"      </ilwd>"
"      <ilwd name='NoHistoryPresent_0::History:Frame' size='2'>"
"        <int_4u name='time'>0</int_4u>"
"        <lstring name='comment' size='6'>89 109</lstring>"
"      </ilwd>"
"      <ilwd name='NoHistoryPresent_1::History:Frame' size='2'>"
"        <int_4u name='time'>0</int_4u>"
"        <lstring name='comment' size='14'>some test data</lstring>"
"      </ilwd>"
"    </ilwd>"
"  </ilwd>"
 	);
 

  for ( unsigned int x = 0; x < sizeof(proc_data)/sizeof(*proc_data); x++ )
  {
    std::ostringstream name;

    name << "TestProc_" << x;
    vect_data[x].SetData(data[x], 9, 0.0, "dimensionless");

    proc_data[x].SetName( name.str( ) );
    proc_data[x].SetComment( "multiDimData to ProcData" );
    proc_data[x].SetFShift( 0.0 );
    proc_data[x].SetStartTime( General::GPSTime(488811610UL, 0UL) );
    proc_data[x].SetTimeOffset( time_offset[x][0] +
				time_offset[x][1] * 10E-9 );
    proc_data[x].SetStopTime( General::GPSTime( stop_times[x][0],
						stop_times[x][1] ) );
    proc_data[x].AppendData( vect_data[x] );

    header.AppendProcData( proc_data[x] );
  }
  AT( );
  header.AppendHistory( history_1 );
  header.AppendHistory( history_2 );
  header.AppendHistory( history_3 );
  header.AppendHistory( history_4 );
  AT( );
  // header.SetStartTime( General::GPSTime(488811610UL, 0UL) );
  
  // SetLocalTime deprecated in Frame 6
  // header.SetLocalTime( 1002047961UL );

  AT( );
  header.SetCommentAttribute( "mdd" );
  // header.SetDeltaTime( 0.09375 );

  ILwd::Reader		reader( ilwd_text );

  AT( );
  ILwd::LdasContainer	check;
  ILwd::LdasContainer*	ilwd( header.ConvertToILwd(5, "framer", "", true ) );

  try
  {
    bool ok;

    check.read( reader );

    Test.Check( ( ok = ( check == *ilwd ) ) )
      << " Convert Frame struct to ilwd" << std::endl;
    if ( !ok )
    {
      Test.Message() << "Frame ILwd: ";
      ilwd->write( 2, 2, Test.Message( false ), ILwd::ASCII );
      Test.Message( false ) << std::endl << std::flush;
    }
  }
  catch( const LdasException& e )
  {
    for ( ILwd::LdasArrayBase::size_type x = 0; x < e.getSize(); x++ )
    {
      Test.Check( false ) << "read error: "
			  << e[x].getMessage()
			  << " " << e[x].getFile()
			  << " " << e[x].getLine()
			  << endl << flush;
      continue;
    }
  }
  catch( ... )
  {
    Test.Check( false ) << "Caught exception" << std::endl;
  }
    
  AT( );
  delete ilwd;

  //---------------------------------------------------------------------
  // Test creation of FrVect structure.
  //---------------------------------------------------------------------

  AT( );
  test_frvect();

  //---------------------------------------------------------------------
  //
  //---------------------------------------------------------------------

  AT( );
  multi_frprocs( );

  //---------------------------------------------------------------------
  // Test creation of FrDetector structure.
  //---------------------------------------------------------------------

  AT( );
  test_frdetector();

  //---------------------------------------------------------------------
  // Exit reflecting status of testing
  //---------------------------------------------------------------------

  AT( );
  Test.Exit();

  //---------------------------------------------------------------------
  // Should never get here, but if we do, consider it a failure.
  //---------------------------------------------------------------------
  AT( );
  return 1;
}
