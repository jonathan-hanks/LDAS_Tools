#ifdef HAVE_CONFIG_H
#include "ilwdfcs/config.h"
#endif	/* HAVE_CONFIG_H */

#include <math.h>

#include <sys/time.h>
#include <cmath>
#include <cstdio>
#include <sstream>

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"

#include "FrameH.hh"
#include "FrDetector.hh"
#include "FrHistory.hh"
#include "FrProcData.hh"
#include "helper.hh"

using ILwdFCS::FrameH;
using std::string;   

namespace {

#define	CONTAINER(VAR,NAME,CNAME) \
  const std::string     TAG_##VAR##_SHORT(#NAME); \
  const std::string     TAG_##VAR(":" #NAME ":Container(" #CNAME "):Frame")

  CONTAINER(PROC_DATA,procData,ProcData);
  CONTAINER(HISTORY,history,History);
  CONTAINER(DETECTOR_OR_SIMULATION,detectProc,Detector);

#undef CONTAINER

  const std::string	TAG_PROC_DATA_2(":procdata:Container(ProcData)");
  const std::string	TAG_DATA_QUALITY("dataquality");
  const std::string	TAG_DETECTOR_PROC(TAG_DETECTOR_OR_SIMULATION);
  const std::string	TAG_DELTA_TIME("dt");
  const std::string	TAG_FRAME_NUMBER("frame");
  const std::string	TAG_GPS_LEAP_SECONDS("ULeapS");
  const std::string	TAG_GPS_TIME("GTime");
  const std::string	TAG_GPS_TIME_SECONDS("GTimeS");
  const std::string	TAG_GPS_TIME_NANOSECONDS("GTimeN");
  const std::string	TAG_RAW_DATA(":rawData:RawData:Frame");
  const std::string	TAG_RUN("run");

  static const ILwd::TOC::Keys_type& gen_toc_key( );

}

#if 0
#define AT() std::cerr << "DEBUG: AT: " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#endif

using namespace ILwdFCS;

const ILwd::TOC::Keys_type& FrameH::TOCKeys = gen_toc_key();

namespace ILwdFCS
{
  template<>
  FrameH::FrameContainer<FrDetector>::
  FrameContainer()
  {
    getContainer()->appendName( "" );
    getContainer()->appendName( TAG_DETECTOR_OR_SIMULATION_SHORT );
    getContainer()->appendName( "Container(Detector)" );
    getContainer()->appendName( "Frame" );
  }

  template<>
  FrameH::FrameContainer<FrProcData>::
  FrameContainer()
  {
    getContainer()->appendName( "" );
    getContainer()->appendName( TAG_PROC_DATA_SHORT );
    getContainer()->appendName( "Container(ProcData)" );
    getContainer()->appendName( "Frame" );
  }

  template<>
  FrameH::FrameContainer<FrHistory>::
  FrameContainer()
  {
    getContainer()->appendName( "" );
    getContainer()->appendName( TAG_HISTORY_SHORT );
    getContainer()->appendName( "Container(History)" );
    getContainer()->appendName( "Frame" );
  }
} // namespace - ILwdFCS

template < class DataType_ >
FrameH::FrameContainer< DataType_ >::
~FrameContainer( )
{
  for ( const_iterator i = m_data.begin(); i != m_data.end(); i++ )
  {
    delete *i;
  }
}

template < class DataType_ >
void FrameH::FrameContainer< DataType_ >::
Append( DataType_& Data )
{
  Container::Append( Data );
  m_data.push_back( new DataType_( Data ) );
}

//!exc: bad_alloc

FrameH::
FrameH()
  : Container()
{
  struct timeval	now;

  gettimeofday( &now, 0 );

  SetRun(0);
  SetFrameNumber(0);
  SetDataQuality(0);
  SetGPSLeapSeconds(0);
  SetStartTime( General::GPSTime(0UL, 0UL) );
  SetDeltaTime( 0.0 );

  getContainer()->appendName( "" );
  getContainer()->appendName( "" );
  getContainer()->appendName( "Frame" );
}

FrameH::
FrameH( const FrameH& Source )
  : Container( Source )
{
}

void FrameH::
AppendDetector( FrDetector& Detector, bool Duplicates )
{
  for ( FrameContainer<FrDetector>::const_iterator d =
	  m_detector_container.begin();
	d != m_detector_container.end();
	d++ )
  {
    if ( *((*d)->GetContainer()) == *(Detector.GetContainer()) )
    {
      return;
    }
  }
  FrDetector*	d( GetDetector( Detector.GetName() ) );
  if ( d && !Duplicates )
  {
    //:TODO: Check to see if they are the same
    //if ( *d != Detector )
    //{
    //General::toss<std::runtime_error>( "FrameH::AppendDetector",
    //					 __FILE__,
    //					 __LINE__,
    //					 "Differing Detector information" );
    //}
  }
  else
  {
    if ( m_detector_container.OwnsData() )
    {
      // Add to the container
      Append( m_detector_container );
    }
    m_detector_container.Append( Detector );
  }
}

void FrameH::
AppendProcData( FrProcData& ProcData )
{
  AT();
  if ( m_proc_data_container.size() == 0 )
  {
    AT();
    SetStartTime( ProcData.GetStartTime() );
    AT();
    SetDeltaTime( ProcData.GetStopTime() - ProcData.GetStartTime() );
  }
  else
  {
    AT();
    if ( ProcData.GetStartTime() < m_start )
    {
      AT();
      SetStartTime( ProcData.GetStartTime() );
    }
    else
    {
      ProcData.ShiftStartTime( m_start );
    }
    AT();
    if ( ( m_start + m_delta ) < ProcData.GetStopTime() )
    {
      AT();
      SetDeltaTime( ProcData.GetStopTime() - m_start );
    }
  }
  AT();
  // ProcData.SetNameAttribute( TAG_PROC_DATA );
  if ( m_proc_data_container.OwnsData() )
  {
    AT();
    Append( m_proc_data_container );
  }
  AT();
  m_proc_data_container.Append( ProcData );
}

void FrameH::
AppendHistory( FrHistory& History )
{
  if ( m_history_container.OwnsData() )
  {
    // Add to the container
    Append( m_history_container );
  }

  m_history_container.Append( History );
}

ILwd::LdasContainer* FrameH::
ConvertToILwd( INT_4U JobId, const std::string& API, const std::string& Name,
	       bool RelinquishOwnership )
{
  static const char	major_sep( '-' );
  static const char	desc_sep( '_' );
   
   
  string result( "file:/Z" );

  result += major_sep;
  result += "P";
  result += desc_sep;
  result += "LDAS";
  result += desc_sep;
   
  std::ostringstream	buffer;
  buffer << JobId;
   
  result += buffer.str( );
  result += desc_sep;
  result += API;
   
  if ( Name.length() > 0 )
  {
    result += desc_sep;
    result += Name;
  }
   
  result += major_sep;
   
  buffer.str( "" );
  buffer << m_start.GetSeconds( );
   
  result += buffer.str( );
  result += major_sep;
  
  INT_4U delta( static_cast< INT_4U >( ceil(m_delta) ) );

  buffer.str( "" );
  buffer << delta;
   
  result += buffer.str( );
  result += ".gwf";

  getContainer()->setMetadata( "outputname", result );

  setOwnsData( !RelinquishOwnership );
  
  return getContainer();
}

FrDetector* FrameH::
GetDetector( const std::string& DetectorName ) const
{
  return (FrDetector*)NULL;
}

void FrameH::
SetCompression( const std::string& Method, int Level )
{
  getContainer( )->setMetadata( "compression_method", Method );
  getContainer( )->setMetadata( "compression_level", Level );
}

void FrameH::
SetCommentAttribute( const std::string& Comment )
{
  getContainer()->setComment( Comment );
}

#define	SET_DATA(call,type,tag,count) \
void FrameH:: \
Set##call( type Value ) \
{ \
  ILwd::LdasArray<type>* data = \
    FindOrCreateArray<type>( *getContainer(), \
			     TAG_##tag, \
			     count); \
\
  (data->getData())[0] = Value; \
}


SET_DATA(DataQuality,INT_4U,DATA_QUALITY,1)
SET_DATA(FrameNumber,INT_4U,FRAME_NUMBER,1)
SET_DATA(GPSLeapSeconds,INT_2U,GPS_LEAP_SECONDS,1)
SET_DATA(Run,INT_4S,RUN,1)

#undef SET_DATA

void FrameH::
SetDeltaTime( double Value )
{
  ILwd::LdasArray<REAL_8>* data =
    FindOrCreateArray<REAL_8>( *getContainer(), TAG_DELTA_TIME, 1);

  m_delta = Value;
  (data->getData())[0] = m_delta;
}

void FrameH::
SetStartTime( const General::GPSTime& StartTime )
{
  AT();
  ILwd::LdasArray<INT_4U>* gps_time =
    FindOrCreateArray<INT_4U>( *getContainer(),
			       TAG_GPS_TIME,
			       2);
  AT();

  AT();
  (gps_time->getData())[0] = StartTime.GetSeconds();
  AT();
  (gps_time->getData())[1] = StartTime.GetNanoseconds();

  AT();
  for ( FrameContainer<FrProcData>::iterator i =
	  m_proc_data_container.begin();
	i != m_proc_data_container.end();
	i++ )
  {
    AT();
    (*i)->ShiftStartTime( StartTime );
  }
  AT();
  m_start = StartTime;
  SetGPSLeapSeconds( StartTime.GetLeapSeconds( ) );
}

namespace {
  static const ILwd::TOC::Keys_type& gen_toc_key( )
  {
    static ILwd::TOC::Keys_type key;

#define	ADD(x) key[ TAG_##x] = FrameH::x
    ADD( DATA_QUALITY );
    ADD( DELTA_TIME );
    ADD( DETECTOR_PROC );
    ADD( FRAME_NUMBER );
    ADD( GPS_LEAP_SECONDS );
    ADD( GPS_TIME );
    ADD( GPS_TIME_SECONDS );
    ADD( GPS_TIME_NANOSECONDS );
    ADD( HISTORY );
    ADD( DETECTOR_OR_SIMULATION );
    ADD( PROC_DATA );
    ADD( RAW_DATA );
    ADD( RUN );
#undef ADD
    key[ TAG_PROC_DATA_2 ] = FrameH::PROC_DATA;

    return key;
  }
}

