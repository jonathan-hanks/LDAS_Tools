#ifndef ILWDFCS__FRFRDETECTOR_HH
#define ILWDFCS__FRFRDETECTOR_HH

#include <string>
#include <vector>

#include "general/types.hh"
#include "general/gpstime.hh"
#include "general/unordered_map.hh"

#include "ilwd/ilwdtoc.hh"

#include "ilwdfcs/Container.hh"

namespace ILwd
{
  class LdasContainer;
}

namespace ILwdFCS
{
  class FrTable;
  class FrVect;

  using namespace ::General;
  class FrDetector: public Container
  {
  public:
    typedef General::unordered_map<std::string,std::string>
    name_map_type;

    enum {
      PREFIX,
      LONGITUDE,
      LONGITUDE_DEGREES,
      LONGITUDE_MINUTES,
      LONGITUDE_SECONDS,
      LATITUDE,
      LATITUDE_DEGREES,
      LATITUDE_MINUTES,
      LATITUDE_SECONDS,
      ELEVATION,
      ARM_X_AZIMUTH,
      ARM_Y_AZIMUTH,
      ARM_X_ALTITUDE,
      ARM_Y_ALTITUDE,
      ARM_X_MIDPOINT,
      ARM_Y_MIDPOINT,
      LOCAL_TIME,
      AUX,
      TABLE,
      MORE = AUX,
      MORE_TABLE = TABLE
    };

    static const ILwd::TOC::Keys_type& TOCKeys;

    static const REAL_4	DFLT_ARM_X_ALTITUDE;
    static const REAL_4	DFLT_ARM_Y_ALTITUDE;
    static const REAL_4	DFLT_ARM_X_MIDPOINT;
    static const REAL_4	DFLT_ARM_Y_MIDPOINT;
    static const REAL_4 DFLT_ELEVATION;

    FrDetector();

    FrDetector( const std::string& 	Name,
		const char*		Prefix,
		const REAL_8		Longitude,
		const REAL_8		Latitude,
		const REAL_4		Elevation,
		const REAL_4		ArmXazimuth,
		const REAL_4		ArmYazimuth,
		const REAL_4		ArmXaltitude,
		const REAL_4		ArmYaltitude,
		const REAL_4		ArmXmidpoint,
		const REAL_4		ArmYmidpoint,
		const INT_4S		localTime );

    FrDetector( const FrDetector& Source );

    //: Add user-provided structure
    //
    // Append identifier for user-provided (presently undefinhed) structure
    // for additional detector data.
    //
    //!param: FrVect& Data - User-provided structure
    void AppendMore( FrVect& Data );

    //: Add user-provided table structure
    //
    // Append identiier for user-provided (presently undefined)
    // table structure for additional detector data.
    //
    //!param: FrTable& Table - User-provided table
    void AppendMoreTable( FrTable& Table );

    //: Get name of the detector
    //
    //!ret: Name of the detector
    const std::string GetName( ) const;

    static const std::string& MapDetectorToPrefix( const std::string& Detector );

    static const std::string& MapPrefixToDetector( const std::string& Prefix );

    void SetPrefix( const char* Prefix );

    //: Set Detector vertex longitude radians
    //
    //!param: REAL_8 Longitude - of vertex, radians
    void SetLongitude( REAL_8 Longitude );

    //: Set Detector vertex longitude degree
    //
    // Detector vertex longitude, geographical coordinates
    // Integer degrees; DDD>0 => E of Greenwich
    //
    //!param: INT_2S D - Degrees where D > 0 => E of Greenwich
    void SetLongitudeD( INT_2S D );

    //: Set Detector vertex longitude minutes
    //
    // Detector vertex longitude, geographical coordinates
    // Decimal minutes; 60 > MM >= 0
    //
    //!param: INT_2S M - minutes where 50 > MM >= 0
    void SetLongitudeM( INT_2S M );

    //: Set Detector vertex longitude seconds
    //
    // Detector vertex longitude, geographical coordinates
    // Decimal seconds; 60.0 > SS >= 0.0
    //
    //!param: REAL_4 S - seconds wehre 60.0 > S >= 0.0
    void SetLongitudeS( REAL_4 S );

    //: Set Detector vertex latitude radians
    //
    //!param: REAL_8 Latitude - of vertex, radians
    void SetLatitude( REAL_8 Latitude );

    //: Set Detector vertex latitude degree
    //
    // Detector vertex latitude, geographical coordinates
    // Integer degrees; DDD>0 => E of Greenwich
    //
    //!param: INT_2S D - Degrees where D > 0 => N of Equator
    void SetLatitudeD( INT_2S D );

    //: Set Detector vertex latitude minutes
    //
    // Detector vertex latitude, geographical coordinates
    // Decimal minutes; 60 > MM >= 0
    //
    //!param: INT_2S M - minutes where 50 > MM >= 0
    void SetLatitudeM( INT_2S M );

    //: Set Detector vertex latitude seconds
    //
    // Detector vertex latitude, geographical coordinates
    // Decimal seconds; 60.0 > SS >= 0
    //
    //!param: REAL_4 S - seconds wehre 60.0 > S >= 0.0
    void SetLatitudeS( REAL_4 S );

    //: Set vertex elevation
    //
    // Units are meters relative to WGS84 ellipsoid.
    //
    //!param: REAL_4 Elevation - elevation in meters relative to
    //+		WGS84 ellipsoid.
    void SetElevation( REAL_4 Elevation );

    //: Set orientation of X arm
    //
    // Set orientation of X arm, measured in radians East of North
    //
    //!param: REAL_4 X - Orientation of X arm.
    void SetArmXazimuth( REAL_4 X );

    //: Set orientation of Y arm
    //
    // Set orientation of Y arm, measured in radians East of North
    //
    //!param: REAL_4 Y - Orientation of Y arm.
    void SetArmYazimuth( REAL_4 Y );

    //: Set Altitude (pitch) angle of X arm.
    //
    // Set Altitude (pitch angle of X arm, measured in decimals degrees above
    // horizon (local tanget to WGS84 ellipsoid)
    //
    //!param: REAL_4 - Altitude angle of X arm.
    void SetArmXaltitude( REAL_4 X );

    //: Set Altitude (pitch) angle of Y arm.
    //
    // Set Altitude (pitch angle of Y arm, measured in decimals degrees above
    // horizon (local tanget to WGS84 ellipsoid)
    //
    //!param: REAL_4 - Altitude angle of Y arm.
    void SetArmYaltitude( REAL_4 Y );

    //: Set midpoint of the X cavity
    //
    // Distance between the detector vertex and the middle of the X cavity
    // with respect to he detector vertex (meter) (should be zero for bars).
    //
    //!param: REAL_4 - X midpoint
    void SetArmXmidpoint( REAL_4 X );

    //: Set midpoint of the Y cavity
    //
    // Distance between the detector vertex and the middle of the Y cavity
    // with respect to he detector vertex (meter) (should be zero for bars).
    //
    //!param: REAL_4 - Y midpoint
    void SetArmYmidpoint( REAL_4 Y );

    //: Set local time
    //
    // Set local seasonal time - UTC in seconds
    //
    //!param: INT_4S Time - UTC in seconds
    void SetLocalTime( INT_4S Time );

    //: Set instrument name
    //
    // Set instrument name (e.g., VIRGO; GEO; TAMA; LIGO_1,_2_3; 4m; PNI;
    // simulated pseudo data - model version etc.)
    //
    //!param: const std::string& Name - Name of instrument
    void SetName( const std::string& Name );

  private:
    std::vector<FrVect*>	m_more;
    std::vector<FrTable*>	m_more_table;

    void init_container();

    // Map of detector names to channel prefixes
    static const name_map_type&	m_detector;

    // Map of channel prefixes to detector names
    static const name_map_type&	m_channel_prefix;
  };
}

#endif	/* ILWDFCS__FRFRDETECTOR_HH */
