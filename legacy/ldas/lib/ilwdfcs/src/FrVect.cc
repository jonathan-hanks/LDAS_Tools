#ifdef HAVE_CONFIG_H
#include "ilwdfcs/config.h"
#endif	/* HAVE_CONFIG_H */

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"

#include "ilwdfcs/FrVect.hh"

#include "helper.hh"

#if 0
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl
#else
#define	AT()
#endif

using namespace ILwdFCS;

FrVect::
FrVect( )
  : m_is_owned( ILwdFCS::OWN ),
    m_read_only( false ),
    m_data( (ILwd::LdasArrayBase*)NULL )
{
}

FrVect::
FrVect( ILwd::LdasArrayBase* Vect, ILwdFCS::own_type Ownership )
  : m_is_owned( Ownership ),
    m_read_only( false ),
    m_data( Vect )
{
}

FrVect::
FrVect( ILwd::LdasElement* Vect, ILwdFCS::own_type Ownership )
  : m_is_owned( Ownership ),
    m_read_only( false ),
    m_data( dynamic_cast< ILwd::LdasArrayBase* >( Vect ) )
{
}

FrVect::
FrVect( const ILwd::LdasArrayBase* Vect )
  : m_is_owned( ILwdFCS::NO_OWN ),
    m_read_only( true ),
    m_data( const_cast< ILwd::LdasArrayBase* >( Vect ) )
{
}

FrVect::
FrVect( const ILwd::LdasElement* Vect )
  : m_is_owned( ILwdFCS::NO_OWN ),
    m_read_only( true ),
    m_data( const_cast< ILwd::LdasArrayBase* >
	    ( dynamic_cast< const ILwd::LdasArrayBase* >( Vect ) ) )
{
}

FrVect::
FrVect( const FrVect& Source )
  : m_is_owned( ILwdFCS::NO_OWN ),
    m_read_only( Source.m_read_only ),
    m_data( Source.m_data )
{
}

FrVect::
~FrVect( )
{
  if ( m_is_owned )
  {
    delete m_data;
    m_data = (ILwd::LdasArrayBase*)NULL;
  }
}

REAL_8 FrVect::
GetDx( ILwd::LdasArrayBase::size_type Index ) const
{
  if ( m_data )
  {
    return m_data->getDx( Index );
  }
  throw std::runtime_error( "ILwdFCS::FrVect::GetDx( ): No data associated with FrVect" );
}

INT_8U FrVect::
GetNx( ILwd::LdasArrayBase::size_type Index ) const
{
  if ( m_data )
  {
    return m_data->getDimension( Index );
  }
  throw std::runtime_error( "ILwdFCS::FrVect::GetNx( ): No data associated with FrVect" );
}

REAL_8 FrVect::
GetStartX( ILwd::LdasArrayBase::size_type Index ) const
{
  if ( m_data )
  {
    return m_data->getStartX( Index );
  }
  throw std::runtime_error( "ILwdFCS::FrVect::GetStartX( ): No data associated with FrVect" );
}

std::string FrVect::
GetUnitX( ILwd::LdasArrayBase::size_type Index ) const
{
  if ( m_data )
  {
    return m_data->getUnits( Index );
  }
  throw std::runtime_error( "ILwdFCS::FrVect::GetUnits( ): No data associated with FrVect" );
}

template < class DataType_ >
void FrVect::
SetData( const DataType_* Data,
	 const ILwd::LdasArrayBase::size_type Dims,
	 const double StartX,
	 const std::string& Units,
	 const double DX,
	 const std::string& DataUnits )
{
  ILwd::LdasArray< DataType_ >*
    vect( dynamic_cast< ILwd::LdasArray< DataType_ >* >( m_data ) );
  if ( vect ==  (ILwd::LdasArray< DataType_ >*)NULL )
  {
    if ( m_is_owned )
    {
      delete m_data;
      m_data = (ILwd::LdasArrayBase*)NULL;
    }
  }
  if ( m_data == (ILwd::LdasArrayBase*)NULL )
  {
    DataType_*	data( (DataType_*)NULL );
    if ( Dims )
    {
      data = new DataType_[Dims];
      std::copy( Data, &Data[Dims], data );
    }
    vect =
      new ILwd::LdasArray<DataType_>
      ( data, Dims, "", Units,
	false, true,
	DX, StartX, DataUnits );
    m_data = vect;
  }
}


template <class DataType_>
void FrVect::
SetData( const DataType_* Data,
	 const ILwd::LdasArrayBase::size_type NDims,
	 const ILwd::LdasArrayBase::size_type* Dims,
	 const double* StartX,
	 const std::string* Units,
	 const double* DX,
	 const std::string& DataUnits)
{
  ILwd::LdasArray< DataType_ >*
    vect( dynamic_cast< ILwd::LdasArray< DataType_ >* >( m_data ) );
  if ( vect ==  (ILwd::LdasArray< DataType_ >*)NULL )
  {
    if ( m_is_owned )
    {
      delete m_data;
      m_data = (ILwd::LdasArrayBase*)NULL;
    }
  }
  if ( m_data == (ILwd::LdasArrayBase*)NULL )
  {
    ILwd::LdasArrayBase::size_type	count( 0 );

    if ( NDims > 0 )
    {
      count = Dims[0];
      for ( ILwd::LdasArrayBase::size_type x = 1; x < NDims; x++ )
      {
	count *= Dims[x];
      }
      AT();
    }
    AT();
    DataType_*	data = new DataType_[count];
    std::copy( Data, &Data[count], data );
    
    vect = 
      new ILwd::LdasArray<DataType_>
      ( data, NDims, Dims, "", Units,
	false, true,
	DX, StartX, DataUnits );
    m_data = vect;
  }
}

void FrVect::
SetName( const std::string& Name )
{
  if ( m_data )
  {
    m_data->setName( 0, Name, true );
  }
}

#define	INSTANTIATE(DataType_) \
template \
void FrVect:: \
SetData< DataType_ >( const DataType_* Data, const ILwd::LdasArrayBase::size_type DataSize, \
		       const double StartX, \
		       const std::string& Units, \
		       const double DX, \
		       const std::string& DataUnits ); \
template \
void FrVect:: \
SetData< DataType_ >( const DataType_* Data, \
		       const ILwd::LdasArrayBase::size_type NDims, \
		       const ILwd::LdasArrayBase::size_type* Dims, \
		       const double* StartX, \
		       const std::string* Units, \
		       const double* DX, \
		       const std::string& DataUnits)

  INSTANTIATE(CHAR);
  INSTANTIATE(CHAR_U);
  INSTANTIATE(INT_2S);
  INSTANTIATE(INT_2U);
  INSTANTIATE(INT_4S);
  INSTANTIATE(INT_4U);
  INSTANTIATE(INT_8S);
  INSTANTIATE(INT_8U);
  INSTANTIATE(REAL_4);
  INSTANTIATE(REAL_8);
  INSTANTIATE(COMPLEX_8);
  INSTANTIATE(COMPLEX_16);

#undef INSTANTIATE
