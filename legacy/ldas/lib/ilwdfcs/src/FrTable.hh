#ifndef ILWDFCS__FRTABLE_HH
#define	ILWDFCS__FRTABLE_HH

#include <list>
#include <string>

#include "ilwdfcs/Container.hh"

namespace ILwd
{
  class LdasElement;
}
namespace ILwdFCS
{
  class FrVect;

  class FrTable: public Container
  {
  public:
    FrTable();

    FrTable( const FrTable& Table );

    FrTable( ILwd::LdasElement* Element, ILwdFCS::own_type Ownership );

    FrTable( const ILwd::LdasElement* Element );

    void AppendRow( FrVect& Row );

    void SetComment( const std::string& Name );

    void SetName( const std::string& Name );

  private:
    static const std::string	TAG_COMMENT;
    static const std::string	TAG_NAME;
    static const std::string	TAG_NUMBER_OF_COLUMNS;
    static const std::string	TAG_NUMBER_OF_ROWS;

    std::list<FrVect*>		m_columns;
  };
}

#endif	/* ILWDFCS__FRTABLE_HH */
