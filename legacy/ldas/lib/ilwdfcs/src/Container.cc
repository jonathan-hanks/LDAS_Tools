#ifdef HAVE_CONFIG_H
#include "ilwdfcs/config.h"
#endif	/* HAVE_CONFIG_H */

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/Container.hh"
#include "ilwdfcs/FrVect.hh"

#include "helper.hh"

#if 0
#define AT() std::cerr << "DEBUG: AT: " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#endif

using namespace ILwdFCS;

Container::
Container()
  : m_is_owned( ILwdFCS::OWN ),
    m_read_only( false ),
    m_data( new ILwd::LdasContainer() )
{
  AT( );
  getContainer( )->setHashing( true );
  getContainer( )->rehash( );
}

Container::
Container( ILwd::LdasContainer* Container, ILwdFCS::own_type Ownership )
  : m_is_owned( Ownership ),
    m_read_only( false ),
    m_data( Container )
{
  AT( );
  getContainer( )->setHashing( true );
  getContainer( )->rehash( );
}

Container::
Container( const ILwd::LdasContainer* Container, ILwdFCS::own_type Ownership )
  : m_is_owned( Ownership ),
    m_read_only( true ),
    m_data( const_cast< ILwd::LdasContainer* >( Container ) )
{
  AT( );
  getContainer( )->setHashing( true );
  getContainer( )->rehash( );
}

Container::
Container( const Container& Source )
  : m_is_owned( ILwdFCS::NO_OWN ),
    m_read_only( Source.m_read_only ),
    m_data( Source.m_data )
{
}

Container::
~Container()
{
  AT( );
  if ( m_is_owned == ILwdFCS::OWN )
  {
    AT( );
    delete m_data;
    m_data = 0;
  }
  AT( );
}

void Container::
Append( Container& DataSet )
{
  if ( DataSet.OwnsData() )
  {
    DataSet.setOwnsData( false );
    m_data->push_back( DataSet.getContainer(),
		       ILwd::LdasContainer::NO_ALLOCATE,
		       ILwd::LdasContainer::OWN );
  }
  else
  {
    m_data->push_back( DataSet.getContainer(),
		       ILwd::LdasContainer::NO_ALLOCATE,
		       ILwd::LdasContainer::NO_OWN );
  }
}

void Container::
Append( FrVect& DataSet )
{
  if ( DataSet.OwnsData() )
  {
    DataSet.setOwnsData( false );
    m_data->push_back( DataSet.getArrayBase( ),
		       ILwd::LdasContainer::NO_ALLOCATE,
		       ILwd::LdasContainer::OWN );
  }
  else
  {
    m_data->push_back( DataSet.getArrayBase( ),
		       ILwd::LdasContainer::NO_ALLOCATE,
		       ILwd::LdasContainer::NO_OWN );
  }
}

namespace ILwdFCS
{
  template<>
  std::string Container::
  getValue< std::string >( const std::string& Element ) const
  {
    const ILwd::LdasString*  v =
      ILwdFCS::FindElement< ILwd::LdasString >
      ( *(this->getContainer() ), Element );

    if ( v == ( const ILwd::LdasString*)NULL )
    {
      std::string msg( "FrDetector::" );
      msg += Element;
      msg += " not specified";

      throw std::runtime_error( msg.c_str() );
    }
    return v->getString( );
  }

  template<>
  std::string Container::
  getValue< std::string >( const std::string& Element,
			   const std::string Default ) const
  {
    const ILwd::LdasString*  v =
      ILwdFCS::FindElement< ILwd::LdasString >
      ( *(this->getContainer() ), Element );

    if ( v == (const ILwd::LdasString*)NULL )
    {
      return Default;
    }
    return v->getString();
  }

  template<class Data_>
  Data_ Container::
  getValue( const std::string& Element ) const
  {
    const ILwd::LdasArray<Data_>*  v =
      ILwdFCS::FindArray<Data_>( *(this->getContainer() ), Element );

    if ( v == ( const ILwd::LdasArray<Data_>*)NULL )
    {
      std::string	msg( "Container::getValue: " );
      msg += Element;
      msg += " not specified";

      throw std::runtime_error( msg.c_str() );
    }
    return (v->getData())[0];
  }

  template<class Data_>
  Data_ Container::
  getValue( const std::string& Element, const Data_ Default ) const
  {
    const ILwd::LdasArray<Data_>*  v =
      ILwdFCS::FindArray<Data_>( *(this->getContainer() ), Element );

    if ( v == (ILwd::LdasArray<Data_>*)NULL )
    {
      return Default;
    }
    return (v->getData())[0];
  }

  template<>
  void Container::
  setValue<std::string>( const std::string& Element,
			 const std::string Value )
  {
    ILwd::LdasString*  v =
      ILwdFCS::FindOrCreateElement<ILwd::LdasString>( *(this->getContainer() ),
						      Element );

    v->setString( Value );
  }

  template<class Data_>
  void Container::
  setValue( const std::string& Element, const Data_ Value )
  {
    ILwd::LdasArray<Data_>*  v =
      ILwdFCS::FindOrCreateArray<Data_>( *(this->getContainer() ), Element, 1 );

    (v->getData())[0] = Value;
  }
} // namespace - ILwdFCS

template INT_2S Container::getValue<INT_2S>( const std::string& Element, const INT_2S Default ) const;
template REAL_4 Container::getValue<REAL_4>( const std::string& Element, const REAL_4 Default ) const;
template REAL_8 Container::getValue<REAL_8>( const std::string& Element, const REAL_8 Default ) const;

template void Container::setValue<INT_2S>( const std::string& Element, const INT_2S Value );
template void Container::setValue<INT_4S>( const std::string& Element, const INT_4S Value );
template void Container::setValue<INT_4U>( const std::string& Element, const INT_4U Value );
template void Container::setValue<REAL_4>( const std::string& Element, const REAL_4 Value );
template void Container::setValue<REAL_8>( const std::string& Element, const REAL_8 Value );
