/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef ILWDFCS__FRPROCDATA_HH
#define ILWDFCS__FRPROCDATA_HH

#include <string>
#include <utility>
#include <vector>

#include "general/gpstime.hh"

// #include "framecpp/FrProcData.hh"

#include "ilwdfcs/Container.hh"
#include "ilwdfcs/SubContainer.hh"

namespace ILwd
{
  class LdasArrayBase;
  class LdasContainer;
  class LdasHistory;
}

namespace ILwdFCS
{
  class FrHistory;
  class FrTable;
  class FrVect;

  using General::GPSTime;

  class FrProcData : public Container
  {
  public:
    enum type_type
    {
      UNKNOWN_TYPE = 0,
      TIME_SERIES = 1,
      FREQUENCY_SERIES = 2,
      OTHER_1D_SERIES_DATA = 3,
      TIME_FREQUENCY = 4,
      WAVELETS = 5,
      MULTI_DIMENSIONAL = 6
    };

    enum subType_type
    {
      UNKNOWN_SUB_TYPE = 0,
      //-----------------------------------------------------------------
      // Subtype for fSeries
      //-----------------------------------------------------------------
      DFT = 1,
      AMPLITUDE_SPECTRAL_DENSITY = 2,
      POWER_SPECTRAL_DENSITY = 3,
      CROSS_SPECTRAL_DENSITY = 4,
      COHERENCE = 5,
      TRANSFER_FUNCTION = 6
     };

    typedef SubContainer< FrVect >::list_type data_type;
    typedef SubContainer< FrVect >::list_type aux_type;
    typedef SubContainer< FrTable >::list_type table_type;
    typedef SubContainer< FrHistory >::list_type history_type;

    FrProcData();

    FrProcData( const ILwd::LdasContainer* Container );

    FrProcData( const FrProcData& Source );
    ~FrProcData( );

    // Accessor methods

    GPSTime GetFrameStartTime( ) const;

    std::string GetName( ) const;

    std::string GetComment( ) const;

    //: Calculate total "span" of the data contained in the FrProc
    //
    // This function returns the sum of the spans of each FrVect contained
    // in the FrPRoc. That is, it returns the sum of of dx[k]*nx[k] over k,
    // where k is the index of an FrVect structure and dx[k], nx[k] are its
    // dx and nx. In the special case of a time-series FrProc, the value
    // returned is the length of the time series in seconds.
    //
    // When attempting to find the length of a time-series, this method
    // should always be used in preference to GetTRange(), since the
    // tRange field in a frame is not required to be the same as the span
    // calculated from the sum of the spans of the FrVect's
    //
    REAL_8 GetSpan() const;

    INT_2U GetType( ) const;

    INT_2U GetSubType( ) const;

    REAL_8 GetTimeOffset( ) const;

    REAL_8 GetTRange( ) const;

    REAL_8 GetFShift( ) const;

    REAL_4 GetPhase( ) const;

    REAL_8 GetFRange( ) const;

    REAL_8 GetBW( ) const;

    REAL_8 GetDelta() const;

    REAL_8 GetSampleRate( ) const;

    const GPSTime& GetStartTime() const;

    GPSTime GetStopTime() const;

    // Pull out the references to the objects

    const data_type& RefData( ) const;
    const ILwd::LdasContainer& RefDataContainer( ) const;
    const aux_type& RefAux( ) const;
    const table_type& RefTable( ) const;
    const history_type& RefHistory( ) const;

    // Methods for appending data to the FrProc structure

    void AppendAuxParam( const std::string& name, const REAL_8 value );

    void AppendData( FrVect& Data );

    void AppendData( ILwd::LdasArrayBase* Vect, ILwdFCS::own_type Ownership );

    void AppendAux( FrVect& Aux );

    void AppendAux( ILwd::LdasArrayBase* Vect, ILwdFCS::own_type Ownership );

    void AppendTable( FrTable& Table );

    void AppendTable( ILwd::LdasContainer* Table, ILwdFCS::own_type Ownership );

    void AppendHistory( FrHistory& History );

    void AppendHistory( ILwd::LdasContainer* History,
			ILwdFCS::own_type Ownership );

    //: Append an LdasHistory object to the FrProc history
    //
    // Appends a list of history items contained in an LdasHistory
    // to the current list of FrHistory objects maintained by the FrProc.
    // Since the LdasHistory doesn't contain a time field, a GPS time must
    // be provided which is applied to the whole list.
    //
    // :NOTE: This function must be called AFTER the name of the FrProc
    // has been set.
    //
    //!param: history - an LDAS history list in ILWD form
    //!param: time - a timestamp which will be applied to each history
    void AppendHistory( const ILwd::LdasHistory& history,
                        const General::GPSTime& time );

    // Set methods

    void SetFrameStartTime( const GPSTime& FrameStartTime );

    void SetName( const std::string& Channel );

    void SetName( const std::string& Channel1, const std::string& Channel2 );

    void SetComment( const std::string& Comment );

    void SetType( const INT_2U type );

    void SetSubType( const INT_2U subType );

    void SetTimeOffset( REAL_8 Offset );

    void SetTRange( const REAL_8 tRange );

    void SetFShift( const REAL_8 fShift );

    void SetPhase( const REAL_4 Phase );

    void SetFRange( const REAL_8 fRange );

    void SetBW( const REAL_8 BW );

    void SetStartTime( const GPSTime& StartTime );

    void SetStopTime( const GPSTime& StopTime );

    void ShiftStartTime( const GPSTime& NewStartTime );

    void SetDelta( const double Delta );

  private:
    static const std::string	TAG_CONTAINER;
    static const std::string	TAG_NAME;
    static const std::string	TAG_COMMENT;
    static const std::string	TAG_TYPE;
    static const std::string	TAG_SUB_TYPE;
    static const std::string	TAG_TIME_OFFSET;
    static const std::string	TAG_TRANGE;
    static const std::string	TAG_FSHIFT;
    static const std::string	TAG_PHASE;
    static const std::string	TAG_FRANGE;
    static const std::string	TAG_BW;
    static const std::string	TAG_AUXPARAMS;
    static const std::string	TAG_AUXPARAM;
    static const std::string	TAG_AUXPARAMNAME;
    static const std::string	TAG_AUXPARAMVALUE;
    static const std::string	TAG_DATA;
    static const std::string	TAG_AUX;
    static const std::string	TAG_TABLE;
    static const std::string	TAG_HISTORY;

    SubContainer<FrVect>	m_data_container;
    SubContainer<FrVect>	m_aux_container;
    SubContainer<FrTable>	m_table_container;
    SubContainer<FrHistory>	m_history_container;

    GPSTime			m_start;
    REAL_8			m_offset;

    REAL_8			m_delta;

    void init_container();

    void init_from_container();
  };

  inline
  REAL_8 FrProcData::
  GetDelta( ) const
  {
    return m_delta;
  }

  inline
  const GPSTime& FrProcData::
  GetStartTime( ) const
  {
    return m_start;
  }

  inline const FrProcData::data_type& FrProcData::
  RefData( ) const
  {
    return m_data_container.Ref( );
  }

  inline const FrProcData::aux_type& FrProcData::
  RefAux( ) const
  {
    return m_aux_container.Ref( );
  }

  inline const FrProcData::table_type& FrProcData::
  RefTable( ) const
  {
    return m_table_container.Ref( );
  }

  inline const FrProcData::history_type& FrProcData::
  RefHistory( ) const
  {
    return m_history_container.Ref( );
  }
}

#endif // ILWDFCS__FRPROCDATA_HH
