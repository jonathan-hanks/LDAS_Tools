
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

#include "ldas/ldasconfig.hh"

static const char* DefaultOptions[] =
  {
    "-a",
    (const char*)NULL
  };

inline std::ostream&
out( std::ostream& Stream )
{
  static	bool out( false );
  if ( out )
  {
    Stream << " ";
  }
  out = true;
  return Stream;
}

template< class T >
inline bool
output( std::ostream& Stream,
	const bool All,
	const char* Current,
	const char* Option,
	const T& Data )
{
  if ( All || ( strcmp( Current, Option ) == 0 ) )
  {
    Stream << out << Data;
    return !All;
  }
  return false;
}

std::string
make_flags( )
{
  const char* flags = getenv( "MAKEFLAGS" );
  if ( flags == (const char*)NULL )
  {
    return "";
  }
  return flags;
}

int
main( int ArgC, char** ArgV )
{
  char**	cur = &( ArgV[ 1 ] );

  if ( *cur == (char*)NULL )
  {
    cur = const_cast< char** >( DefaultOptions );
  }
  for ( ; *cur != (char*)NULL; ++cur )
  {
    bool	all = ( strcmp( *cur, "-a" ) == 0 ) ? true : false;

    if ( strcmp( *cur, "-h" ) == 0 )
    {
      std::cerr << "Usage: " << ArgV[0] << " [OPTION]..." << std::endl
		<< std::endl
		<< "Prints certain ldas system information."
		<< " With no OPTION, same as -a."
		<< std::endl
		<< std::endl
		<< "-h\tthis help message" << std::endl
		<< "-a\tprint all information, in the following order" << std::endl
		<< "-n\tName of ldas (should be ldas)" << std::endl
		<< "-v\tVersion of ldas" << std::endl
		<< "-c\tCVS data for ldas version. (When ldas/configure.in was committed)" << std::endl
		<< "-b\tDate and time when version was built" << std::endl
		<< "-o\tConfiguration options that were specified" << std::endl
		<< "-k\tC++ Compiler used for compiling code" << std::endl
		<< "-f\tC++ Compiler flags used for compilation" << std::endl
		<< "-m\tValue of MAKEFLAGS at time of build" << std::endl
		<< "-s\tAny symbolic tag associated with this version" << std::endl
	;
      exit( 1 );
    }
    if ( output( std::cout, all, *cur, "-n", LDAS_NAME ) ||
	 output( std::cout, all, *cur, "-v", LDAS_VERSION ) ||
	 output( std::cout, all, *cur, "-c", LDAS_CVS_DATE ) ||
	 output( std::cout, all, *cur, "-b", LDAS_BUILD_DATE ) ||
	 output( std::cout, all, *cur, "-o", LDAS_CONFIGURE_COMMAND_LINE_OPTIONS ) ||
	 output( std::cout, all, *cur, "-k", LDAS_CONFIGURE_CXX ) ||
	 output( std::cout, all, *cur, "-f", LDAS_CONFIGURE_CXXFLAGS ) ||
	 output( std::cout, all, *cur, "-m", make_flags( ) ) ||
	 output( std::cout, all, *cur, "-s", LDAS_SYMBOLIC_TAG )
	 )
    {
      continue;
    }
  }
  std::cout << std::endl;
}
