<!-- -*- mode: HTML -*- -->
<HTML>
<HEAD>

<!-- Please preserve look n' feel! -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<TITLE>Sample putStandAlone meta macro</TITLE>

<META name="description" content="Description of the putStandAlone User Command">

<META name="keywords" content="macros, commands, user">

</HEAD>

<BODY BGCOLOR="#DDDDDD" TEXT="#000000">
<a name="top">
<table>
<td width=100%><td>
<IMG SRC="/doc/gifs/LIGO.gif" ALIGN=right ALT="LIGO.gif">
</table>

<h3> The <i>putStandAlone</i> User Command (Meta Macro) </h3>
<b>All user commands have the form:</b>
<p>
<tt><font color="red">
<b>ldasJob</b> { -name {} -password {} -email {} } { <b>userCmd</b> -opt1 {} ... }
</tt></font>
<p>
Which is in the format of a <a href=http://www.tcl.tk/software/tcltk/><b>Tcl</b></a> command, <b>ldasJob</b>, with two required
arguments:
<ol>
<li>
A Tcl list of <b>user information</b> consisting of username,
password, and e-mail address.  All fields must be filled or
the command will be rejected.<br>
Client software which would like to receive responses from the
LDAS system on a port instead of through email can provide a
<b>hostname:port</b> combination as the email argument.  A
further feature of this method is that the hostname can be replaced
with the magic key <b>!host!</b>, and the LDAS system will use
the IP address it finds by doing a peername lookup on the
connected socket.
<li>
A <b>user command</b> in the form of a Tcl list, for which there
exists a &quot;meta&quot; macro file which can be expanded into Tcl code
which can then be evaluated by the LDAS system.
<br>
An argument <b>must be provided to every option field</b> for
any given command or the command will be rejected.<br>
Some option fields will accept a &quot;null&quot; argument consisting of
a matching pair of braces <b>&quot;{}&quot;</b> with no interposed space.
<br>
Meta macros consist of a prototype declaration of the arguments
for the given user command, and a template describing the calling
order of API specific blocks of Tcl code which are concatenated
into a larger block comprising the complete request, which can
then be distributed by the assistant manager for interpretation
by the low level API's.
<br>
These API specific blocks are maintained as API specific macro
files consisting of immediately interpretable Tcl code.
</ol>
There is a ligolw API command to convert from LIGO_LW to ilwd format.
<ul>
<a href="#putStandAlone"><li><b><i>putStandAlone</i></b></a>
</ul>
<!-- bottom of page pointer section --> 
<!-- Here we put the stuff which will become the linked pages --> 
<hr>
<ul>
<li>
<a name="putStandAlone">
<b><i>putStandAlone</i></b>
<p>
The putStandAlone user command is the "back end" of the dataStandAlone
user command. Wrapper API output data from a dataStandAlone command
is read from disk and passed to the eventmon API for ingestion into the
database tables.
<p>
Calling convention (all on a single line):
<p>
<b>ldasJob</b> &nbsp;
<b>{</b>
-name &nbsp; <i>"username"</i> &nbsp;
-password &nbsp; <i>"********"</i> &nbsp; 
-email &nbsp; <i>"user@foobar.edu"</i><b>}</b> &nbsp; 
<b>{<i>putStandAlone</i></b> &nbsp; 

<a href=#wrapperdata>-wrapperdata</a>
&nbsp;<font color="red"> <i>URL</i></font>&nbsp;

<a href=#returnprotocol>-returnprotocol</a>
&nbsp;<font color="red"> <i>URL</i></font>&nbsp; 

<a href=#metadataapi>-metadataapi</a>
&nbsp;<font color="red"> <i>URL</i></font>&nbsp; 

<a href=#database>-database</a> &nbsp;
   <font color="red"> <i>dsname</i></font> &nbsp;

<a href=#outputformat>-outputformat</a>
&nbsp;<font color="red"> <i>{ilwd ascii | ilwd binary | LIGO_LW }</i></font>&nbsp;
<a href=#subject>-subject</a>
&nbsp;<font color="red"> <i>{freeform string}</i></font>&nbsp;
<b>}</b>
<p>
<font color="green">Option Descriptions:</font>
<ul>
<p>
<a name=wrapperdata>
<b>-wrapperdata:</b> &nbsp; &nbsp; 
                   <tt>
                   http ftp file 
                   </tt>
<p>
<ul>
<tt>
The argument of the <b>-wrapperdata</b> option URL format
is of the format similar to
that of <b>-returnprotocol</b>. The URL specifies the source of
of the data to be ingested in ilwd format: it can be files,
or directory containing wrapper files.
The URL must be valid for the command to be successfully executed.
<p>
<li>http://filename.ext<p> 
<li>ftp://filename.ext<p>
<li>file:/filename.ext<p>
<p>
<font color="red">
NOTES:
<ol>
<li>Embedded spaces in the argument to the -wrapperdata
option will cause the request to fail.
</ol>
</font>
</tt>
</ul>
<p>
<!--#include virtual="/subst.cgi?opt_returnprotocol.cgi"-->
<!--#include virtual="/subst.cgi?opt_outputformat.cgi"-->
<a name=metadataapi>
<b>-metadataapi:</b> &nbsp; &nbsp; <tt>api</tt>
<p>
<ul>
<li>Optional<br><p>
Default: <font color="red">metadata</font>
The metadata results will be inserted into the database.<p>
If 'ligolw' is specified e.g. <i>-metadataapi ligolw</i>, 
and the <i>-returnprotocol</i> specifies a url, the metadata results will be output in the form of xml
rather than being inserted into the database i.e.
running at a site that does not have a database.<p>
If 'tee' option is specified, e.g. <i>-metadataapi tee</i>, and the <i>-returnprotocol</i> specifies a url
the metadata results will
be output both as an xml and also be inserted
into the database.<p>
</ul>
<p>
<a name=database>
<b>-database:</b> &nbsp; &nbsp; <tt>dsname<i>string</i></tt>
<p>
<ul>
<tt>
The argument of -database option is a valid database dataset name for
the site that the command applies e.g. <i>dev_1</i> and <i>dev_test</i> are
dataset names for site <i>ldas-dev</i>, <i>lho_1</i> and <i>lho_test</i> are
dataset names for site <i>ldas-wa</i> etc. For dsnames of sites,
please visit the LDAS database link on each ldas site web page.
If this option is not specified, insertion is done from the default database.
</tt>
</ul>
<p>
<a name=subject>
<b>-subject:</b>&nbsp;&nbsp;<tt>{subject for the return e-mail}</tt>
<p>
<ul>
<font color=red>
Default: <i>"jobID completed"</i>
</font><br>   
<tt>
String to be used as the subject for the e-mail returned by the
system on completion of the job.
</tt>
</ul>   
</ul>
<a name=examples>
<p><font color="green">Examples of putStandAlone commands:</font>
<p>
Examples on how to use this command
<p>
This example presents a Tcl script which can be run from any
computer with a network connection to submit a user command
to the LDAS system:
<p>
<ul>
<pre>
#!/bin/sh
# use -*-Tcl-*- \
exec tclsh "$0" "$@"

# convenient formatted command definition
# database insertion from wrapper files
set cmd "ldasJob
   { -name foo -password **** -email foo@foobar.edu }
   {
      putStandAlone
         # subject to be used when e-mailing user
         -subject {test script for putStandAlone}
         
         # input url for converting 
         -wrappperdata file:/ldas_outgoing/jobs/globus_files/NORMAL1548
         
         # ilwd format for output ilwd (optional) 
         -outputformat {ilwd ascii}
         	
        # database if not default
        -database ldas_tst     
   }"

# strip comments and compact cmd into user command format
set cmd [ split $cmd "\n" ]
foreach line $cmd {
   set line [ string trim $line ]
   if { [ regexp {^#} $line ] } {
      continue
   }
   lappend tmp $line
}   
set cmd [ join $tmp "\n" ]
regsub -all -- {[\n\s]+} $cmd { } cmd

# connect, issue command, print reply from manager,
# and disconnect
set sid [ socket ldas-dev.ligo.caltech.edu 10001 ]
puts  $sid $cmd
flush $sid
puts [ read $sid ]
close $sid
</pre>
<b><i>Which submits this command to the system:</i></b>
<p>
<li>
<b>ldasJob</b>&nbsp;
   <b>{&nbsp;</b>
      -name
         &nbsp;<i>"foo"</i>&nbsp;
      -password
         &nbsp;<i>"****"</i>&nbsp; 
      -email
         &nbsp;<i>"foo@foobar.edu"</i><b>&nbsp;}</b>&nbsp; 
   <b>{&nbsp;<i>putStandAlone</i></b>&nbsp; 
      -subject
         &nbsp;<font color="red"><i>{test script for putStandAlone}</i></font>&nbsp;
      -wrappperdata
         &nbsp;<font color="red"><i></i>file:/ldas_outgoing/jobs/globus_files/NORMAL1548</font>&nbsp;
      -outputformat
         &nbsp;<font color="red"><i>{ilwd ascii}</i></font>&nbsp;
   <b>&nbsp;}</b>
<p>
<p>Example of xml output
<pre>
# xml output from wrapper files
set cmd "ldasJob
   { -name foo -password **** -email foo@foobar.edu }
   {
      putStandAlone
         # subject to be used when e-mailing user
         -subject {test script for putStandAlone}
         
         # input url for converting 
         -wrappperdata file:/ldas_outgoing/jobs/globus_files/NORMAL1548
                	
		 -returnprotocol http://results 
		 
		 -outputformat {LIGO_LW} 
		 
		 -metadataapi { ligolw } 
		  
   }"
</pre>

<i><b>Output will be created by ligolw API in job output directory:</b></i>
<pre>
Your results:
results.xml
can be found at:
http://131.215.115.248/ldas_outgoing/jobs/LDAS-DEV_1049/LDAS-DEV10494929
</pre>
<p>using the default option for -metadataapi, the response is database insertion:
<pre>
Subject: BOX-III98872 tfclu online running on H2:LSC-AS_Q Inserted 
1 rows into ldas_tst database table process Inserted 14 rows into ldas_tst database 
table process_params Inserted 1330 rows into ldas_tst database table sngl_burst 
Inserted 1 rows into ldas_tst database table search_summary
</pre>
<p>
If '-metadataapi tee' option is used, the response is both xml and database output:
<pre>
Subject: BOX-III98809 tfclu online running on H2:LSC-AS_Q Inserted 1 rows into ldas_tst database 
table process Inserted 14 rows into ldas_tst database table process_params 
Inserted 1330 rows into ldas_tst database table sngl_burst Inserted 1 rows into ldas_tst database 
table search_summary Your results: tfclusters_result.xml can be found 
at: http://131.215.114.23/ldas_outgoing/jobs/BOX-III_9/BOX-III98809
</pre>

<!-- bottom of page pointer section -->  
<!-- End of putStandAlone --> 
<p>
<center>
<h2>
<IMG SRC="/doc/gifs/arrow_up.gif" ALT="arrow_up.gif">
<a href="#top">Return to Top</a>
<IMG SRC="/doc/gifs/arrow_up.gif" ALT="arrow_up.gif">
</h2>
</center>

<!-- End of putStandAlone --> 

</ul>
</BODY>

<!-- VI POWERED! -->

</HTML>
