#!/ldcg/bin/perl -w
# -*- mode: perl; c-default-offset: 2; -*-

use CGI::Carp qw(fatalsToBrowser);
use CGI qw(:standard);

my($x);

my(@query) = split(/\&/,$ENV{"QUERY_STRING"});

for( $x = 0; $x <= $#query; $x++ )
{
    $query[$x] =~ /^(.*)=(.*)$/ and do {
	$$1 = $2;
	next;
    };
    if ( -f $query[$x] )
    {
	open HTML, $query[$x];
    }
    elsif ( -f "$ENV{SRCDIR}/$query[$x]" )
    {
	open HTML, "$ENV{SRCDIR}/$query[$x]";
    }
    else
    {
	next;
    }
    while(<HTML>)
    {
	chop;
	/^<!--(.*)=(.*)-->$/ and do {
	    if ( !defined($$1) )
	    {
		$$1=$2;
	    }
	    next;
	};
	s/\"/\\\"/g;
	$_ = "print \"$_\\n\"";
	eval $_;
    }
    close HTML;
}
