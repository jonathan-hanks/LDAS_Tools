<a name=returnprotocol>
<b>-returnprotocol:</b> &nbsp; &nbsp; 
                   <tt>
                   http ftp gridftp mailto file port
                   </tt>
<p>
<ul>
<font color=red>
<b>Optional</b>&nbsp;&nbsp;<p>
<b>Default Value:</b> <i><b>http://results.ilwd</b></i>
<p>
Note that in most cases this option should NOT be explicitly
set by the user, since it can have unexpected interactions
with the <b>output()</b> actions in the datacond API, for example.
</font>
<p>
The argument to <b>-returnprotocol</b> resembles the usual
browser conventions for URL's, and is used to control the
naming and disposition of output data from a user request.
<p>
The <b>http://</b>, <b>ftp://</b>, <b>gridftp:/</b>,
and <b>file:/</b> URI types are currently supported.
<p>
The specific format of the argument, and possibly the values
of other arguments will affect the way that the
<b>-returnprotocol</b> argument is applied by the LDAS system:
<tt>
<p>
<ul>
<li><b>-returnprotocol [file|ftp|http]</b>
<p>
Will cause the system to return the location of output
data as an ftp or http URL (the system default, in the
absence of a user supplied -returnprotocol option, is
to use http).<br>
Specifying "file" will return the local path to the file
as it is seen by all LDAS API's.<br>
Specifying <b>ftp</b> or <b>http</b> will return a URL
relative to the gateway machine of the local system (which
is the same machine to which user requests are made).<br>
The names used for the output files will be determined
by the system and are generally descriptive of the
content of the files.
<p>
<li><b>-returnprotocol [file|http|ftp]:filename.ext</b>
<p>
Will cause the system to return the location of output
data as an ftp or http URL (the system default, in the
absence of a user supplied -returnprotocol option, is
to use http). <b>And</b> any output object which is not
in frame format will be named according to the <i>
suggested</i> pattern.  When possible, indexes will be
appended to the filename to differentiate between multiple
output files, but sometimes this can not be managed and in
that case as many levels of .ba* files will be created as
required to avoid overwriting data.<br>
The "file" form is used to get the local name of the file
relative to the LDAS installation for use as input data
by subsequent jobs.<br>
This form is not advised when the job is one that is expected
to produce many output files!<br>
In some special cases the suggested filenamr and extension
may be ignored, as when a frame file is being produced, or
an exact ilwd representation of a frame file.
<p>
<li><b>-returnprotocol [http|ftp|file]://dirname/subdirname/</b>
<p>
When this form is used and <b>dirname</b> corresponds to an
existing directory in the anonymous ftp area of the LDAS
system, all output files will be copied into that directory.
In some cases a filename can be specified and will be used;
in this case .ba* files may be created to avoid overwriting
of data.<br>
Note that the trailing slash is absolutely required in order
for the URL to be interpreted as a local directory by the
LDAS system.<br>
The URL returned to the user will be of the type specified.
<p><font color=red><b>This option of -returnprotocol is currently is not supported. The output
e.g. frame or xml, is placed in the job output directory.
The user has to retrieve the output via http or ftp to
to his/her local system.</b></font>
<p>
<li><b>-returnprotocol ftp://foo.bar.edu/here/there/file.ext</b>
<p>
When this form is used the LDAS system will attempt to copy
the output data via anonymous ftp to the remote site.<br>
<b>The user will need to run tests to make certain that this
form works with the specified site.</b>
<p><font color=red><b>This option of -returnprotocol is currently is not supported. The output
e.g. frame or xml, is placed in the job output directory.
The user has to retrieve the output via http or ftp to
to his/her local system.</b></font>
<p>
<li><b>-returnprotocol gridftp://here/there/file.ext</b>
<p>
<font color=red>
<b>This is a special protocol used by the dataStandAlone user
   command for interaction with a GLOBUS/GRID system.
   <p>
   This returnprotocol is only interpreted by the datacond API
   for use on output data formatted for ingestion by a stand alone
   wrapper API.
</b>
</font>   
<p>
The file will be written to the local gridftp home directory
or a subdirectory of it which is writable by the user which
LDAS runs as.<br>
The LDAS writable subdirectory should be defined in the
<b>LDASapi.rsc</b> resource file as the
<b>::GRID_FTP_WRITABLE_SUBDIRECTORY</b>, which will be joined
to the grid home directory when calculating output filenames.
<p>
Several modified forms of the gridftp URL are available:
<p>
<ul>
<li>If the specified URL is a left-hand substring of the default
    location, then data will be written into job specific
    subdirectories under the default location.  This allows the
    use of:<p>
    <ul>
     <b>-returnprotocol gridftp</b>
    </ul>
    <p>
    as a useful shorthand notation for producing a directory
    hierarchy of dataStandAlone results which maps directly to
    the job directories normally produced by LDAS, allowing
    simple correlation of output data with other job resuts or
    messages from the LDAS system.
<p>
<li>If the specified URL refers to an existing or non-existant
    directory <b>under</b> the default area:<p>
    <ul>
     <b>-returnprotocol gridftp://here/there/mydir/mysubdir/</b>
    </ul>
    <p>
    Then the directory hierarchy will be created as required
    and unique job-specific default filenames will be assigned
    to the datacond API's output for the wrapper, i.e.:<p>
    <ul>
     <b>LDAS-TEST1234567_wrapperdata.ilwd</b>
    </ul>
    <p>
<li>If the specified URL does NOT end with a slash, the final
    element of the name will be used as the filename, with the
    extension <b>.ilwd</b> appended to the root of the final
    element.
</ul>
<p>
<font color="red">
NOTES:
<ol>
<li>Embedded spaces in the argument to the -returnprotocol
option will cause the request to fail.
</ol>
</font>
</tt>
</ul>
<p>
