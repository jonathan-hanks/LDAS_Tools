<a name=tarball>
<b>-tarball:</b> &nbsp; &nbsp; 
                   <tt>
                   http or ftp URL to .tar.gz or .tar.bz2 file
                   </tt>
<p>
<ul>
The purpose of this option is to limit the number of individual
<a href=http://curl.haxx.se>Curl</a> calls
made by the manager API to avoid making the manager very busy managing
hundreds (or thousands of remote file retrievals via http or ftp.
<p>
This option is intended to be made use of in <b><i>addition</i></b>
to any and all former options, and does not obsolete or obviate the
requirement for any other option.
<p>
The <b><tt>-tarball</tt></b> option accepts a single argument of
a URL referencing a <b><i>.tar.gz</i></b> or <b><i>.tar.bz2</i></b>
file containing <b>all of</b> the files which are otherwise
referenced via http and ftp url's in the user command.
<p>
The location and directory hierarchy within the tarball should be
consistent with those of the http and ftp references.
<p>
For example, if there is an option of this form:
<font color=brown>
<pre>
 -responsefiles &#92;
    http://www.foo.org/bar/baz/data1.ilwd
    http://www.foo.org/bar/baz/data2.ilwd
    http://www.foo.org/bar/baz/data3.ilwd
    http://www.foo.org/bar/baz/data4.ilwd
</pre>
</font>
then the tarball option could be any of:
<font color=brown>
<pre>
    -tarball http://www.foo.org/tarball.tar.gz
    
    -tarball http://www.foo.org/bar/tarball.tar.gz
    
    -tarballhttp://www.foo.org/bar/baz/tarball.tar.gz
</pre>
</font>
And as long as the internal structure of the tarball is such that
it would unpack and overwrite the files individually referenced
otherwise, the user command will succeed!
<p>
<font color=red>
<b>
NOTE:
</b>
</font>
Internally (in the LDAS manager API), the use of the
<b><tt>-tarball</tt></b> option turns off retrieval of
<b><i>ALL</i></b> other http and ftp URL's, so you <b><i>MUST</i></b>
put all of the files otherwise referenced by URL's in the user
command into the tarball!
</ul>
<p>
