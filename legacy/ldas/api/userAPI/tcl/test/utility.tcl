#------------------------------------------------------------------------
# Collection of utility functions for testing of the ldas system
#------------------------------------------------------------------------

proc get_latest {} {
    set sid [ socket $::host [ expr $::base_port + 5 ] ]
    puts $sid "foo \"puts \$cid \$::frame::cache(0)\""
    flush $sid
    set data [ read $sid ]
    close $sid
    return [ lindex $data 3 ]
}
