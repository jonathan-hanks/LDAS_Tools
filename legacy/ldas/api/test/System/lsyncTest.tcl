#!/bin/sh
#\
    . /ldas/libexec/setup_tclsh.sh
# \
    exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for createRDS user command 
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./createRDSTest_s3.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*createRDS_S3*"
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================
if { [info exists env(SRCDIR)] } {
    foreach path $env(SRCDIR) {
	lappend ::auto_path $path
    }
}

if { [info exists env(PREFIX)] } {
    lappend ::auto_path $env(PREFIX)/lib/genericAPI
} elseif { [file isdirectory /ldas/lib/genericAPI] } {
    lappend ::auto_path /ldas/lib
}

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set auto_path "[file dirname $argv0] $::auto_path"
}

if { [ file exist /ldas_outgoing/cntlmonAPI/cntlmon.state ] } {
    source /ldas_outgoing/cntlmonAPI/cntlmon.state
} else {
    set ::LDAS ./ldas
    set ::API frame
}
if { [ file exist /ldas_outgoing/LDASapi.rsc ] } {
    source /ldas_outgoing/LDASapi.rsc
} else {
    source /ldas/lib/LDASapi.rsc
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::lsyncTest::test {

    ;## initialize
    set APIDIR "/ldas_outgoing/diskcacheAPI"
    set RSCFILE "LDASdiskcache.rsc"
    set LSYNCRSC lsync.rsc
    set LSYNCRSC_BACKUP lsync.rsc.org
    set EXCLUDE_VARS_FROM_UPDATE [ list ::DISKCACHE_HASHFILE_NAME ]
    set limit 10
    set tarball {}
    set iterations 1
    set delay 10000
    ;## max time for this test is 2 hrs or 0 for no time limit
    set max_timecheck 0
    catch { exec ls -l /ldas/bin } data
    regexp {ldas-([\d\.]+)} $data -> ldas_version
    set tarball_path /ldas_outgoing/nightly_tarballs
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--ldas-version} {string} \
    {ldas version of lsync tarball} \
    { set ::QA::lsyncTest::test::ldas_version $::QA::Options::Value }
::QA::Options::Add {} {--iterations} {string} \
    {number of times to repeat this test} \
    { set ::QA::lsyncTest::test::iterations $::QA::Options::Value }
::QA::Options::Add {} {--limit} {string} \
    {number of scanned log lines to wait for stabilization } \
    { set ::QA::lsyncTest::test::limit $::QA::Options::Value }
::QA::Options::Add {} {--max-time-check} {string} \
    {max time for this test or 0 for unlimited} \
    { set ::QA::lsyncTest::test::max_timecheck $::QA::Options::Value }
::QA::Options::Add {} {--tarball_path} {string} \
    {path of tarball} \
    { set ::QA::lsyncTest::test::tarball_path $::QA::Options::Value }       
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

## ********************************************************
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::lsyncTest::test {
    
    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
    namespace import ::QA::Rexec
    
    if { [ file exist /ldcg/bin/diff ] } {
        set diffexec   /ldcg/bin/diff
    } else {
        set diffexec [ auto_execok diff ]
    }
    
    proc debugProgress { msg } {
	set name [ namespace current ]
	namespace import [ namespace parent $name ]::*
	
	Puts 1 $msg
	set fd [ set ::${name}::fd ]
	puts $fd $msg
	flush $fd 
    }
    
    ## ******************************************************** 
    ##
    ## kill lsync scripts 
    ## kill -SIGTERM sometimes results in error bad option and not killing
    ## ********************************************************
    proc killer { pattern { signal 9 } } {
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
	
        catch { exec ps -A -o "pid,args" | grep $pattern } data
        
        foreach line [ split $data \n ]  {
	    if { [ regexp {(\d+).+sh} $line -> pid ] } {
		catch { exec kill -$signal $pid } err
		debugProgress "kill -$signal pid $pid, line=$line,err=$err"
	    }
        }
    }
    
    ## ******************************************************** 
    ##
    ## update lsync resource values from diskcacheAPI rsc file 
    ##
    ## ******************************************************** 

    proc update_lsyncRsc {} {
	
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
        set lsyncrsc [ namespace eval $name set LSYNCRSC ]
        set lsyncrscbackup [ namespace eval $name set LSYNCRSC_BACKUP ]
        set exclude_vars_from_update [ namespace eval $name set EXCLUDE_VARS_FROM_UPDATE ]
        set newvars [ list ]
        
        if { [ catch {
            set rscfile [ file join [ namespace eval $name set APIDIR ] \
			      [ namespace eval $name set RSCFILE ] ] 
	    set diskcache_interp [ interp create ]
	    $diskcache_interp eval source $rscfile
	    set lsync_interp [ interp create ]
	    $lsync_interp eval source $lsyncrsc
	    set vars [ $diskcache_interp eval info vars ]
	    set lsyncvars [ list ]
	    foreach var $vars {
		if { ![ regexp {tcl|env|auto|error} $var ] } {
		    lappend lsyncvars ::$var
		}
	    }
	    foreach var $lsyncvars {
		if { [ lsearch $exclude_vars_from_update $var ] > -1  } {
		    continue
		}
                if { [ $lsync_interp eval info exist $var ] } {
                    if { [ $lsync_interp eval array exist $var] } {
                        set origvalue [ $lsync_interp eval array get $var ]
                    } else {
			set origvalue [ $lsync_interp eval set $var ]
                    }
                } else {
                    set origvalue NONE
                    lappend newvars $var
                }
                if { [ $diskcache_interp eval array exist $var ] } {
                    set newvalue [ $diskcache_interp eval array get $var ]
		    $lsync_interp eval array set $var [ list $newvalue ]
		    debugProgress "$var updated from $origvalue to $newvalue"
		} elseif { [ $diskcache_interp eval info exist $var ] } {
		    set newvalue [ $diskcache_interp eval set $var ]
		    $lsync_interp eval set $var [ list $newvalue ]
		    debugProgress "$var updated from $origvalue to $newvalue"
		}
	    }
	    ;## save a coopy of original and restore it if run repeatedly
	    if { ![ file exist $lsyncrscbackup] } {
		file copy -force $lsyncrsc $lsyncrscbackup
	    }
	    file copy -force $lsyncrscbackup $lsyncrsc

	    set fid [ open $lsyncrsc r ]
	    set data [ read -nonewline $fid ]
	    close $fid
	    
	    set filedata ""
	    foreach line [ split $data \n ] {
		if { [ regexp {set[\s\t]*(\S+)} $line -> varname ] } {
		    if { [ lsearch $lsyncvars $varname ] > -1 } {
                        if { [ $lsync_interp eval array exist $varname ] } {
                            set line "array set $varname \[ list [ $lsync_interp eval array get $varname ] \]\n"
                        } else {
                            set line "set $varname \[ list [ $lsync_interp eval set $varname ] \]\n"
                        }
		    }
                    append filedata $line\n
		} elseif { [ regexp {^[;\#\s]+} $line ] } {
				       append filedata $line\n
				   }
	        }
			     ;## now append the new vars from diskcache
			     if { [ string length $newvars ] } {
				 append filedata ";## *** update the following diskcache resources to lsync.rsc\n"
			     }
			     foreach var $newvars {
				 if { [ $lsync_interp eval array exist $var ] } {
				     set line "array set $var \[ list [ $lsync_interp eval array get $var ] \]\n"
				 } else {
				     set line "set $var \[ list [ $lsync_interp eval set $var ] \]\n"
				 }
				 append filedata $line\n
			     }
			     append filedata "set ::DEBUG_THREADS 1\n"
	    set fid [open $lsyncrsc w]
	    puts $fid [ string trim $filedata \n ]
	    close $fid
	} err ] } {
	    return -code error $err
	}
    }
    
    ## ******************************************************** 
    ##
    ## check if lsync test needs to be terminated or repeat check
    ##
    ## ******************************************************** 
    ## set a time limit to checking 
    
    proc lsyncMonitorExpireTestCheck {} {
	
	uplevel {
	    set duration [ set ::${name}::time_checked ]
	    set max_duration [ set ::${name}::max_timecheck ]
	    if { ! $max_duration || ( $duration < $max_duration ) } {
		incr ::${name}::time_checked [ set ::${name}::delay ]
		after [ set ::${name}::delay ] ${name}::lsyncMonitor                 
	    } else {
		error "Test exceeded limit: [ expr $duration/1000 ] secs \
                exceeded max [ expr $max_duration/1000 ] secs, terminated prematurely"
	    }
	}
    }
    
    ## ******************************************************** 
    ##
    ## monitor lsync.tcl to see if it is done generating the cache
    ## give it some extra time to complete
    ## on dev give it extra time 5 lines, tandems 2 is ok
    ##
    ## ******************************************************** 
    ## set a time limit to checking

    proc lsyncMonitor {} {
	set name [ namespace current ]
	namespace import [ namespace parent $name ]::*
	
	;## -----------------------------------------------------
	;##  Ensure that the program is still running
	;## -----------------------------------------------------
	if { [catch { exec kill -0 $::lsync_pid } err] } {
	    debugProgress "lsync process has terminated"
	    set ::${name}::done 1
	}
	;## -----------------------------------------------------
	;## test passes with 20 iterations,
	;##  5 results in differences sometimes
	;## -----------------------------------------------------
	if { [ catch {
	    set rc [ catch { exec tail -[ set ::${name}::limit ] lsync.out | grep -c scanned } data ]
	    if { !$rc } {
		if { $data == [ set ::${name}::limit ] || [ set ::${name}::time_checked ] > [ set ::${name}::max_timecheck ] } {
		    set ::${name}::done 1
		} else {
		    lsyncMonitorExpireTestCheck
		}
	    } else {
		;## see if we need to keep checking
		# Puts 5 "tail lsync.out found '$data'"
		lsyncMonitorExpireTestCheck
	    }
	} err ] } {
	    return -code error $err
	}
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## diff_analyzer - compares mount point subdirectories to see if
    ;## only the updated times diff
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc diff_analyzer { diff } {
	
	set error ""
	Puts 1 "diff '$diff' len [ string length $diff ]"
	if { ! [ string length [ string trim $diff ] ] } {
	    return
	}
	foreach line [ split $diff \n ] {
	    if { ![ regexp {^[<>]*\s+/} $line ] } {
		;## ignore comments
		continue
	    }
	    if { [ regexp {(<|>)\s+(/\S+)\s+(\d+)\s+(\d+)\s+\{.+\}} $line -> fileid subdir time1 time2 period ] } {
		Puts 5 "fileid $fileid subdir $subdir time1 $time1 time2 $time2 period $period"
		set ${fileid}($subdir) [ list $time1 $time2 $period ]
	    }
	}
	Puts 1 "< array [ array get < ]\n> array [ array get > ]"
	if { ! [ array size < ] && ! [ array size > ]} {
	    return
	}
	
	;## check if diskcache cache dirs match lsync except in update time
	foreach subdir [ array names < ] {
	    foreach { file1_time1 file1_time2 file1_period } [ set <($subdir) ] { break }
	    if { [ info exist >($subdir) ] } {
		foreach { file2_time1 file2_time2 file2_period } [ set >($subdir) ] { break }
		if { [ string equal $file1_period $file2_period ] } {
		    if { ! [ string equal $file1_time1 $file2_time1 ] || 
			  ! [ string equal $file1_time2 $file2_time2 ] } {
			Puts 1 "time1 $file1_time1 $file2_time1 or time2 $file1_time2 $file2_time2 differ only, ok"
		    } else {
			Puts 5 "time1 $file1_time1 $file2_time1 or time2 $file1_time2 $file2_time2 agree perfect"
		    }
		} else {
		    append error "period $file1_period $file2_period does not agree\n"
		}
	    } else {
		append error "$subdir is in diskcache but does not exist in lsync cache."
	    }
	}
	
	;## check if lsync cache has dirs missing in diskcache
	foreach subdir [ array names > ] {
	    if { ![ info exist <($subdir) ] } {
		append error "$subdir is in lsync.cache but does not exist in diskcache"
	    }
	}
	if { [ string length $error ] } {
	    return -code error $error
	}
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## main 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
    
    ;## if this is not running on frame host, use ssh to execute it on frame host
    
    if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
	source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    } elseif { [ file exist /ldas/lib/genericAPI/LDASapi.rsc ] } {
	source /ldas/lib/genericAPI/LDASapi.rsc 
    } else {
	set ::DISKCACHE_API_HOST opterondata-dev
    }
    set localhost [ exec uname -n ]

    
    if { ! [ regexp $::DISKCACHE_API_HOST $localhost ] && 
	  ! [ regexp {sunopteron|ldasbox1} $::DISKCACHE_API_HOST  ] } {
	Puts 0 "diskcache $::DISKCACHE_API_HOST me $localhost, run remotely"
	QA::ConnectToAgent
	set cmd { $::QA::LDAS_ROOT/testbin/lsyncTest.tcl --site $::SITE -debug 0 -verbose lpse \
		      --qa-debug-level $::QA::Debug::Level --ldas-version $ldas_version \
		      --iterations $iterations \
		      --qa-debug-auto-flush $::QA::Debug::AutoFlush } 
	
	set cmd [ subst $cmd ]
	catch {Rexec $::DISKCACHE_API_HOST ldas $cmd} result
	Puts 1 "$::DISKCACHE_API_HOST lsyncTest result:\n$result"
	
	cleanupTests
	return
    }
    
    ;## this part is executed on the diskcache host
    
    file copy -force ${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc \
	${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc.now
    
    ;## need to put in LDASdiskcache.rsc.lsync to reduce mount points
    if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc.lsync ] } {
	file copy -force ${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc.lsync \
	    ${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc
    } else {
	Puts 0 "No diskcache resource specific for lsync test, \
        use current ${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc"
    }
    
    set fd [ open $::TMP/lsyncTest[pid].log w ]
    debugProgress "fd $fd $::TMP/lsyncTest[pid].log"
    
    ;## on same host as diskcache, unzip the tarball
    ;## diskcacheAPI frame cache should be rebuild or in sync before this test
    ;## copy diskcache MOUNT_PT to lsync.rsc and create new lsync.rsc files

    if { ! [ info exist ldas_version ] } {
	return -code error "No -ldas_version <ldas-version-number> specified"
    }
    
    cd  $::TMP
    if { [ string match i86pc $::tcl_platform(machine) ] } {
	set workdir lsync-SunOS-i86pc-${ldas_version}
    } else {
	set workdir lsync-SunOS-sun4u-${ldas_version}
    }
    catch { file delete -force $workdir }
    if { ! [ regexp {\.tar.gz} $tarball_path ] } { 
	set tarball $tarball_path/${workdir}.tar.gz
    } else {
	set tarball $tarball_path
    }
    debugProgress "tarball $tarball"
    
    if { ![ file exist $tarball ] } {
	return -code error "$tarball does not exist"
    }
    
    for { set i 1 } { $i <= $iterations } { incr i } {
	test lsyncTest::diskcache::cacheCmp:$i {} -body {
	    catch { file delete -force $workdir } 
	    catch { exec /ldcg/bin/tar xzf $tarball } tarout
	    debugProgress "$i th round: unzip $tarball: $tarout"

	    cd $workdir
	    
	    killer lsync.tcl
	    
	    debugProgress "working in [ pwd ]"
	    update_lsyncRsc

	    # invoke lsync to build cache
	    catch { exec ./runner.sh lsync.tcl >& lsync.out & } pid
	    debugProgress  "running lsync.tcl $pid"
	    
	    if { [ regexp {^(\d+)$} $pid ] } {
		set ::lsync_pid $pid
	    } else {
		error "Error: $pid"
	    }

	    ;## wait for lsync to be done scanning mount points
	    set time_checked 0
	    catch unset ::QA::lsyncTest::test::done
	    after 10000 ::QA::lsyncTest::test::lsyncMonitor 
	    vwait ::QA::lsyncTest::test::done

	    after 5000

	    ;## kill lsync when it has finished
	    # killer lsync.tcl SIGTERM
	    killer lsync.tcl 15
	    
	    ;## wait a little for cache write
	    set duration 0
	    
	    while { $duration < 1000000 } {
		catch { exec /ldas/bin/cacheDump >& t1 } err
		set rc [ catch { exec grep -c /data/node t1 } count ]
		Puts 1 "rc $rc, count $count"
		if { $rc } {
		    break
		}
		incr duration 5000
		after 5000
	    }
	    Puts 1 "waited $duration msecs for diskcache to update"
	    ;## restore the lsync rsc file
	    # file copy -force $::LSYNCRSC_BACKUP $::LSYNCRSC

	    ;## compare cache ascii representation to that of diskcache

	    catch { exec /ldas/bin/cacheDump >& t1 } err
	    if { [ string length $err ] } {
		return -code error "diskcache cacheDump error: $err"
	    }
	    
	    catch { exec ./runner.sh cacheDump.py lsync.cache >& t2 } err
	    if { [ string length $err ] } {
		return -code error "lsync cacheDump.py error: $err"
	    }
	    
	    catch { exec /ldas/bin/cacheDump lsync.cache >& t3 } err
	    if { [ string length $err ] } {
		return -code error "lsync cacheDump errors $err"
	    }

	    catch { exec $diffexec -Ew t1 t2 } diff_python
	    catch { exec $diffexec -Ew t1 t3 } diff_tcl
	    
	    set fail ""  
	    foreach type [ list python tcl ]  {
		if { [ catch {
		    diff_analyzer [ set diff_${type} ]
		} err ] } {
		    append fail "$type diff error: '$err'\n[ set diff_$type ]\n"
		}
	    }

	    set fail [ string trim $fail \n ]

	    ;## see if there is a core dump
	    
	    set err "'$fail', cacheDump compares failed; maybe you need to rebuild frame cache \
	    (cmonClient Utilities -> \"rebuild frame cache\" and rerun this \
	    test again."
	    if { [ file exist *core* ] } {
		append err " dumped core: [ glob -nocomplain *core* ]"
	    }
	    if { [ string length $fail ] } {
		return -code error $err
	    }
	} -result {}
    } ;## end of for loop for iterations
    
    after 5000
    
    ;## *** must kill this for ssh to return to caller
    killer lsync.tcl 9
    
    catch { close $fd }
    
    #========================================================================
    # Testing is complete
    #========================================================================
    
    ;## retore the saved LDASdiskcache.rsc file
    file copy -force ${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc.now \
	${::QA::LDAS_OUTGOING_ROOT}/diskcacheAPI/LDASdiskcache.rsc
    cleanupTests
} ;## namespace - ::QA::lsyncTest::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::lsyncTest::test
exit
