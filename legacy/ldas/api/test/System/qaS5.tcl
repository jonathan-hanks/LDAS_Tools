package provide qaS5 1.0

namespace eval QA {
    set CurrentDataSet S5

    set DataSet(${CurrentDataSet},start) 855975136
    set DataSet(${CurrentDataSet},dt) 32
	set DataSet(${CurrentDataSet},frameLength) 32
	set ::frameLength 32
    foreach site [list H H-H1 H-H2 L] {
		foreach level [ list 1 3 4 ] {
			set file  "adcdecimate_${site}-RDS_R_L${level}-${CurrentDataSet}.txt"
            catch { set DataSet(${CurrentDataSet},$site,level$level) \
				[ ::QA::LoadChannelInfo $file ] }
		} ;# foreach
    } ;# foreach 
} ;# namespace - QA
