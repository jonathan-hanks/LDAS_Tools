#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

if {![info exists ::env(LIGOTOOLS)]} {
    return -code error "Can't find LIGOtools."
}

lappend ::auto_path $::env(LIGOTOOLS)/lib .
package require qalib
package require qatest
package require LDASJob

qaInit

set ::times 693960000-693960031 ;## E7 Frames

set ::testScript {
    catch {LJrun job -nowait -manager $::SITE $::cmd}
    if {$LJerror} {
        set errmsg $::job(error)
        LJdelete ::job
        error "Error from LJrun: $errmsg"
    }

    #::tcltest::DebugPuts 1 $::job(jobid)
    puts $::job(jobid)
    catch {LJwait ::job}
    if {$LJerror} {
        set errmsg [trimReply $::job(error)]
        LJdelete ::job
        error "Error from LJrun: $errmsg"
    }

    set urlList $::job(outputs)
    LJdelete ::job
    foreach url $urlList {
        #::tcltest::DebugPuts 1 $url
        puts $url
        checkURL $url
    }

    list
}

set ::cmd {
    $::command
       -returnprotocol http://results
       -outputformat {ilwd ascii}
       -framequery {R H {} $::times Adc(1!resample!2!)}
}

foreach ::command {getFrameElements getFrameData concatFrameData} {
    ::tcltest::test "$::command" {} $::testScript {}
}

