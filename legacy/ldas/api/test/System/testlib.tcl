#! /ldcg/bin/tclsh
# common procs used for testing

package provide testlib 1.0

proc chkfile { fname } {
	if	{ [ file exist $fname ] } {
		puts "$fname [ file size $fname ]"
        set ::${fname}(done) 1
        catch { unset ::${fname}(count) }
    } else {
        if { ! [ info exist ::${fname}(count) ] } {
            set ::${fname}(count) 0
        }
        if { [ set ::${fname}(count) ] < 1000 } {
            incr ::${fname}(count) 1
            after 2000 chkfile $fname
        } else {
            puts "check for $fname has expired"
            set ::${fname}(done) 1
        }
    }
}

proc bgerror {err} {
    set trace {}
    puts stderr "\nError: $err"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
    }

    exit 1
}\

proc debugPuts { args } {
	if	{ [ info exist ::DEBUG ] $$ $::DEBUG } {
		puts [ join $args ]
	}
}

proc serverOpen {{port 0}} {
    set lsid [socket -server serverCfg -myaddr [info hostname] $port]
    foreach {::LADDR ::LHOST ::LPORT} [fconfigure $lsid -sockname] {break}
    return lsid
}

proc serverCfg  {sid addr port} {
    fconfigure $sid -buffering line -blocking off
    fileevent $sid readable [list getResult $sid]
    return
}

proc serverClose {sid} {
    catch {close $sid}
    return
}
proc clearCmd {{when ""} args} {
    if {![string length $when]} {
        array set ::STAT {
            Send 0
            Info 0
            Recv 0
            ErrS 0
            ErrR 0
            SendList {}
            RecvList {}
            ErrList  {}
        }
        puts "STAT: Clear [ clock format [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]"

        return ""
    }

    if {[catch {getSecs $when} secs]} {
        return $secs
    }
    after $secs [myname]

    return ""
}

proc waitCmd {{when ""} args} {
    if {![string length $when]} {
        return $::WAIT
    }
    if {![string is integer $when]} {
        return -code error "bad argument \"$when\": must be a positive integer"
    }
    set when [expr {abs($when)}]
    if {$when < 1000} {
        set when [expr {$when * 1000}]
    }
    if {[catch {set ::WAIT $when} msg]} {
        return -code error $msg
    }

    puts "WAIT: $::WAIT"

    ;## reset wait mod counter
    set ::WAIT_MODS 0

    return ""
}

proc getResult {sid} {
    set i 0
    set data {}
    set datum {}

    while {1} {
        if {[catch {read $sid} datum]} {
            break
        }
        if {[string length $datum]} {
            append data $datum
            set i 10
            continue
        } else {
            incr i 10
            if {$i > $::TIMEOUT_RECV} {break}
        }
        if {[eof $sid]} {break}
        after $i
    }
    ;##regsub -all {\n} $data " " data
    set reply [string trim $data]
    fileevent $sid readable {}
    set msg {}
    set cmd {}
    set rest {}
    regexp -- {(\S+)\s*(.*)} $reply -> cmd rest
    if {![string length $cmd]} {
        catch {close $sid}
        return
    }
    if {![string length $rest]} {
        set rest {{}}
    }
    debugPuts "RECV: $reply"
	catch { validateResult $reply }
    if {[regexp -nocase -- "(${::RUNCODE}\\d+)" $reply -> jobid]} {
        incr ::STAT(Recv)

        set idx [lsearch $::STAT(SendList) $jobid]
        if {$idx == -1} {
            lappend ::STAT(RecvList) $jobid
        } else {
            set ::STAT(SendList) [lreplace $::STAT(SendList) $idx $idx]
        }

        if {[regexp -- {error!} $reply]} {
            incr ::STAT(ErrR)
            lappend ::STAT(ErrList) $jobid
        }
    }
	debugPuts "jobs with responses pending [ llength $::STAT(SendList) ]"
	if	{ ! [ llength $::STAT(SendList) ] } {
		set ::done 1
	}
    catch {cWAIT_MODSlose $sid}
    return
}

proc getJobid {sid aid { seq 1 } } {
    catch {after cancel $aid}

    if {[eof $sid]} {
        catch {close $sid}
        puts "INFO: Error: EOF on socket: broken connection"
        incr ::STAT(ErrS)
        return
    }

    set reply {}
    while {[gets $sid line] != -1} {
        append reply "$line "
    }
    catch {close $sid}

    if {[regexp -- "(${::RUNCODE}\\d+)" $reply -> jobid]} {
        puts "INFO: $jobid"
        incr ::STAT(Info)
		if	{ $seq } {
        	lappend ::STAT(SendList) $jobid
		}

        if {$::WAIT_MODS > 0} {
            incr ::WAIT -$::WAIT_INCR
            incr ::WAIT_MODS -1
            puts "WAIT: $::WAIT"
        }
    } else {
        puts "INFO: $reply"
        incr ::STAT(ErrS)
    }
	if	{ ! $seq } {
		set ::sent 1
	}
    return
}

proc killSock {sid} {
    catch {close $sid}
    puts "SEND: Error sending job: Timeout on confirmation."
    puts "WAIT: $::WAIT"
	set ::done 1
    return
}

proc ckLJrun {} {
    uplevel {
    if  {$LJerror} {
        if  {[info exists ::job(error)]} {
            set err $::job(error)
            puts $err
        }
        LJdelete job
		puts "exiting"
		exit
    }
	if 	{ [ regexp "(${::RUNCODE}\\d+)" $err -> jobid ] } {
			lappend ::STAT(SendList) $jobid
			puts "sent jobs $::STAT(SendList)"
	} 
	LJdelete job
    vwait ::done 
	}
}

set ::passed 0
set ::failed 0
