#!/bin/sh
#\
    . /ldas/libexec/setup_tclsh.sh
# \
    exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#
# example of running with -match; this can be done on ldas system or
# part of ldas system testing
#./regression.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 \
# --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1 --manager-reply-timeout 120000
#
# to only verify email in spool directory 
# I noticed I can only grep successfully if I copied the spool mail to
# another spot on my local disk
# regression.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 \
# --qa-debug-level 1 --tests email --jobListFile ./regression.jobs \
# --maildir /usr1/lcldsk/mlei/mail.spool > & regression.emails &
#
# to only verify email in filtered mail folder
# regression.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 \
# --qa-debug-level 1 --tests email --jobListFile ./regression.jobs \
#  > & regression.emails &
#
# To run all tests, leave out --tests option.
#
# writes a temporary file /ldas_outgoing/jobs/regression.jobs so email
# verification can be done on a local system.
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

set ::TOPDIR /ldas

#------------------------------------------------------------------------
# Namespace global v    set testscripts { ariables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::regression::test {

    set testList [ list ]
    set delay 5000
    set maxTestSecs 10800
    set testThrottle 5
    set excluded { pr2793ChannelList }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--tests} {string} \
    {A comma seperated list of regression scripts to execute. For example --tests pr2029.tcl,pr1993.tcl} \
    { set ::QA::regression::test::testList [ split $::QA::Options::Value , ] }
::QA::Options::Add {} {--testThrottle} {string} \
    {The maximum number of tests to run concurrently} \
    { set ::QA::regression::test::testThrottle [ split $::QA::Options::Value , ] }
::QA::Options::Add {} {--maxTestSecs} {string} \
    {testset} \
    { set ::QA::regression::test::maxTestTime [ split $::QA::Options::Value , ] }
::QA::Options::Add {} {--excluded} {string} \
    {A comma seperated list of regression scripts to exclude from execution.} \
    { set ::QA::regression::test::excluded [ split $::QA::Options::Value , ] }    

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::cmdcase1::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::regression::test {

	##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
    
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Periodic check for completion of each test.
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    

    proc checkDone {} {
        set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
        set testNameList [ array names ::${name}::testPid ]
        set count 0
        set testsInProgress [ list ]
        set maxTestSecs [ set ::${name}::maxTestSecs ]
        foreach testname $testNameList {
            set logfile [ set ::${name}::log($testname) ]
            catch { exec tail -1 $logfile } data
            # Puts 5 "test $testname, $logfile, $data, ::${name}::testPid($testname) [ info exist ::${name}::testPid($testname) ]"
            if  { [ regexp {Total.+Passed.+Skipped.+Failed.+} $data ] } {
                Puts 5 "$testname done or skipped"
                catch { unset ::${name}::testPid($testname) }
            } else {
                ;## check if pid is up
                if  { [ info exist ::${name}::testPid($testname) ] } {
                    set pid [ set ::${name}::testPid($testname) ]
                    set rc [ catch { exec ps -fu ldas | grep -E ldas.+$pid.+$testname } psdata ]
                    # Puts 5 "test $testname rc $rc pid $pid psdata $psdata"
                    set rc 0
                    if  { ! $rc } {
						foreach line [ split $psdata \n ] {
							;## skip grep line
							if	{ [ regexp {grep} $line ] } { 
								continue
							}				
                        	set mtime [ file mtime $logfile ]
                        	set now [ clock seconds ]
							set delta [ expr $now - $mtime ]
                        	if  { $delta < $maxTestSecs } {
                                # Puts 1 "in progress $testname"               
                            	lappend testsInProgress $testname
                        	} else {
								catch { exec kill -HUP $pid } err 
								Puts 0 "$testname.tcl has been not been updated for over $delta secs, killed $err"
                                catch { unset ::${name}::testPid($testname) }
							}
						}
                    } else {
                        catch { unset ::${name}::testPid($testname) }
                    }                        
                } 
            }
        }
        set numTestsInProgress [ llength $testsInProgress ]
        ;## if this is called to check on outstanding tests just returned 
        Puts 5 "numTestsInProgress $numTestsInProgress"
        if  { $numTestsInProgress } {
            set delay [ set ::${name}::delay ]
            set ::${name}::afterId  [ after $delay [ list ::${name}::checkDone ] ]
            set ::${name}::numTestsInProgress $numTestsInProgress
        } else {
            Puts 5 "#3 set done for vwait: no test in progress, ${name}::done"
            after 0 [ list set ::${name}::numTestsInProgress $numTestsInProgress ]
            after 0 [ list set ::${name}::done 1 ]
        }
    }
        
    set excluded { pr2793ChannelList }
    
    if  { ![ llength $testList ] } {
        set testList [ lsort -decreasing -dictionary [ glob -nocomplain $::QA::LDAS_ROOT/lib/test/regression/pr*.tcl ] ]
        foreach test $excluded {
            set index [ lsearch -glob $testList $::QA::LDAS_ROOT/lib/test/regression/${test}.tcl]
            if  { $index > -1 } {
                set testList [ lreplace $testList $index $index ]
            }
        }
    } 

    set numTests [ llength $testList ]
    Puts 1 "numTests $numTests"
    
    set testsRunning [ list ]
    set numTestsInProgress 0
    
    if  { ! [ info exist afterId ] } {
        set afterId [ after 5000 ::QA::regression::test::checkDone ]
        Puts 5 "afterId $afterId numTestsInProgress $numTestsInProgress"
    }        
    
    foreach test $testList {
        set testname [ file rootname [ file tail $test ] ]
        catch { unset to_exclude }
        foreach excluded_test $excluded {
            if  { [ string match ${testname} $excluded_test ] } {
                set to_exclude 1
                break
            }
        }
        ;## skip excluded files which are not executable scripts
        if  { [ info exist to_exclude  ] } {
            continue
        }
        set test [ file join $::QA::LDAS_ROOT/lib/test/regression/[ file tail $test ] ]
        set log($testname) $::TMP/$testname.log
        
        while { $numTestsInProgress >=  $testThrottle } {
            Puts 5 "$numTestsInProgress vs $testThrottle vwaiting for throttle"
            vwait ::QA::regression::test::numTestsInProgress
        }
        catch { eval exec $test $::argv --site $::SITE \
		--enable-globus $::USE_GLOBUS_CHANNEL --enable-gsi $::USE_GSI \
		--qa-debug-level $::QA::Debug::Level >& $log($testname) &  } err

        if  { [ regexp {^\d+$} $err ] } {
            set testPid($testname) $err
            Puts 5 "$testname pid $err" 
        } else {
            Puts 0 "No pid for $testname '$err'"
        }
        incr numTestsInProgress 1   
    }
    
    catch { unset ::QA::regression::test::done }
    after cancel $afterId
    ::QA::regression::test::checkDone
        
    vwait ::QA::regression::test::done
    
    ;## merge results into here
    set result ""

    foreach field [ list totalTests totalSubTests totalPassed totalFailed totalSkipped ] {
        set $field 0
    }
    
    foreach test [ lsort -decreasing [ array names log ] ] {
        if  { [ file exist $log($test) ] } {
            set fd [ open $log($test) r ]
            set data [ read -nonewline $fd ]
            close $fd 
            ;## file delete $testlog
            append result "$test\n$data\n"
            set lastline [ string range $data end-200 end ]
            if  { [ regexp {pr\S+\.tcl:\s+Total\s+(\d+)\s+Passed\s+(\d+)\s+Skipped\s+(\d+)\s+Failed\s+(\d+)} $lastline \
                -> totals passed skipped failed ] } {
                incr totalTests 1
                incr totalSubTests $totals
                incr totalPassed $passed
                incr totalFailed $failed
                incr totalSkipped $skipped 
                # Puts 1 "$test running totals $totalTests subtests $totalSubTests Passed $totalPassed Skipped $totalSkipped Failed $totalFailed"
            } else {
                Puts 0 "Cant find totals in $test '$lastline'"
            }
        } else {
            append result "$log($test) does not exist"
        }
    }
    Puts 0 [ string trim $result \n ]
    
    Puts 1 "\nTotals $totalTests subtests $totalSubTests Passed $totalPassed Skipped $totalSkipped Failed $totalFailed"
    test "Overall Result" {} -body {
        set errors ""
        if  { $totalTests != $numTests } {
            append errors "*** Expected $numTests test files but got $totalTests\n"
        }
        if  { $totalFailed } {
            append errors "*** Found $totalFailed failures\n"
        }
        
        if  { [ string length $errors ] } {
            Puts 0 $errors
            return -code error $errors
        }
        
	} -result {} 
           
    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
    
} ;## namespace - ::QA::regression::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::regression::test    
