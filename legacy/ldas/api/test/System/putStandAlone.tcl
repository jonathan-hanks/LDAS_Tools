#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::putStandAlone::test {
	set dbutilsrsc /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc 
	set TESTLIB /ldas_usr/ldas/test/database
	set TESTDATADIR /ldas_outgoing/jobs/LDASTest/putStandAlone/ilwddir
	;## series of exception tests in 3 entries per list <description> <cmd> <expected>
	set WRAPPERDIR http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/jobs/LDASTest/putStandAlone
	set WRAPPERDATA [ list $WRAPPERDIR/process_1.ilwd $WRAPPERDIR/output_1.ilwd $WRAPPERDIR/process_2.ilwd ]
	
	set BADWRAPPERDATA $WRAPPERDIR/BOX-III185203_wrapperdata.ilwd
	set runtimes 1
	
	;## cmdlist can be run without clearing database in between 
	set cmdlist {
	"no ilwd files in wrapper directory" 
	"putStandAlone 
    -subject {exception - non ilwd files in wrapper data directory} 
    -metadataapi {metadata} 
    -returnprotocol {http://result} \
    -wrapperdata $WRAPPERDIR
    -database $dbname"
	"Some non ilwd is specified by -wrapperdata option"
	
	"bad wrapper data 1" 
	"putStandAlone 
    -subject {exception - bad wrapper data among valid data - Malformed ILWD element} 
    -metadataapi {metadata} 
    -returnprotocol {http://result} 
    -wrapperdata [ list [ concat $BADWRAPPERDATA $WRAPPERDATA ] ]
    -database $dbname"
	"Malformed ILWD element"
	
	"bad wrapper data 2" 
	"putStandAlone 
    -subject {exception - bad wrapper data - Malformed ILWD element} 
    -metadataapi {metadata} 
    -returnprotocol {http://result} 
    -wrapperdata $BADWRAPPERDATA
    -database $dbname"
	"Malformed ILWD element"
	
	"2 process elements" 
	"putStandAlone 
    -subject {no products, extra process_1.ilwd- Job already has 2 process elements}
    -metadataapi {metadata} 
    -returnprotocol {http://result} 
    -wrapperdata [ list [ concat $WRAPPERDATA [ lindex $WRAPPERDATA 0 ] ] ]
    -database $dbname"
	"Job already has 2 process elements"
	
	"exceed maximum database insertion rate"
	"putStandAlone
    -subject {database flood}
    -metadataapi metadata
    -multidimdatatarget ligolw
    -returnprotocol http://dbflood
    -database $dbname
    -wrapperdata [ list [ list $WRAPPERDIR/dbflood/process_1.ilwd $WRAPPERDIR/dbflood/output_1.ilwd $WRAPPERDIR/dbflood/process_2.ilwd ] ]"
	{Number of database rows generated \(\d+\) exceeds the maximum of \d+}
	
	"data product number exceeded"
	"putStandAlone
    -subject {test exception: Number of received data products exceeded expected number for the job}
    -metadataapi {metadata}
    -returnprotocol {http://result}
    -wrapperdata [ list [ concat $WRAPPERDATA [ lindex $WRAPPERDATA 1 ] ] ]
    -database $dbname"
	"Number of received data products exceeded expected number for the job"	
	
	"#data products exceeded expected number for the job"
	"putStandAlone
    -subject {Number of received data products exceeded expected number for the job}
    -metadataapi metadata
    -multidimdatatarget ligolw
    -returnprotocol http://result
    -database $dbname
    -wrapperdata [ list [ concat $WRAPPERDATA [ lindex $WRAPPERDATA 1 ] ] ]"
	"Number of received data products exceeded expected number for the job"	
	
	"Insertion ok"
	"putStandAlone
    -subject {inserted into database}
    -metadataapi {metadata}
    -returnprotocol {http://result}
    -wrapperdata [ list $WRAPPERDATA ]
    -database $dbname"
	"Inserted 28 rows"		

	}
	
	;## cmdlist1 needs clearing of previous data from database
	set cmdlist1 {	
	"mdd data to frame"
	"putStandAlone 
    -subject {putStandAlone test from dev with mdd data to frame}
    -metadataapi {metadata} 
    -returnprotocol {http://result} 
    -wrapperdata [ list $WRAPPERDATA ]
    -multidimdatatarget {frame}       
    -database $dbname"
	{Inserted 28 rows.+\nYour results:.+gwf}
	}
	
	set cmdlist6 {
	"duplicate output in same directory"
	"putStandAlone 
    -subject {all files in a directory,duplicate output} 
    -metadataapi {metadata} 
    -returnprotocol {http://result} 
    -wrapperdata { file:$TESTDATADIR }
    -database $dbname"
	"Number of received data products exceeded expected number for the job"
	}
	
	set cmdlist7 {
	"ilwd directory"
	"putStandAlone 
    -subject {duplicate output removed} 
    -metadataapi {metadata} 
    -returnprotocol {http://result} 
    -wrapperdata { file:$TESTDATADIR }
    -database $dbname"
	"Inserted"
	}
	
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--database} {string} \
    {database name} \
    { set ::QA::putStandAlone::test::dbname $::QA::Options::Value }
	
::QA::Options::Add {} {--runtimes} {string} \
    {times runtimes} \
    { set ::QA::putStandAlone::test::runtimes $::QA::Options::Value }	  
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::putStandAlone::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	set user ldas
	
	if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
		source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    } elseif { [ file exist /ldas/lib/genericAPI/LDASapi.rsc ] } {
		source /ldas/lib/genericAPI/LDASapi.rsc 
	} else {
		set ::METADATA_API_HOST metaserver
	}
	QA::ConnectToAgent
	
	proc runTest {} {
		uplevel {
		foreach { desc cmd expected } $cmdset {
			set cmd [ subst $cmd ]
			Puts 1 "cmd $cmd"
			test putStandAlone:$desc {} -body {
				if 	{[catch {SubmitJob job $cmd} err]} {
					LJdelete job
					Puts 1 "err got $err\nexpected $expected"
	    			return $err
				}
				set reply $job(jobReply)
				LJdelete job
				Puts 1 "got $reply\nexpected $expected"
				return $reply
    		} -match regexp -result $expected
		}
		}
	}
	
	proc clearDB { { mydb "" } } {
		upvar user user
		if	{ ! [ string length $mydb ] } {
			upvar dbname dbname
			set mydb $dbname
		}
		set err [ Rexec $::METADATA_API_HOST $user "/usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
		DB2INSTANCE=ldasdb del_db_process.tcl $mydb version=LDASTest" ]
		Puts 1 "$mydb cleanup $err"
	}
	
	for { set i 0 } { $i < $::QA::putStandAlone::test::runtimes } { incr i } {	
		set cmdset $cmdlist
		clearDB
		runTest
	
		clearDB 
	
		set cmdset $cmdlist1
		runTest
		
		set desc "duplicate ilwd in same directory"
	
		test putStandAlone:$desc {} -body {
			file copy -force $TESTDATADIR/output_1.ilwd $TESTDATADIR/output_2.ilwd
			Puts 1 [ glob $TESTDATADIR/output* ]
			set  cmdset $cmdlist6
			runTest
			;## remove duplicate
			after 5000
			file delete -force $TESTDATADIR/output_2.ilwd
			clearDB
			set cmdset $cmdlist7
			runTest
			list
		} -match regexp -result {}

		flush [outputChannel]
    
	}
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests

} ;## namespace - ::QA::putStandAlone::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::putStandAlone::test	
	
