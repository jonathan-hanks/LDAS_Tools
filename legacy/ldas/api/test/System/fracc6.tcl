#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Establish basic search path
#------------------------------------------------------------------------
set ::auto_path ". /ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#------------------------------------------------------------------------
# Establish Namespace GLOBALS
#------------------------------------------------------------------------
namespace eval ::QA::fracc::test {
    variable Real 0
}
#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
package require qaoptions
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::QA::Options::Add {} {-fracc-frame-spec} {string} \
    {Specify version of frame spec to use} \
    { set ::QA::fracc::test::FrameSpec $::QA::Options::Value }
::QA::Options::Add {} {-fracc-real} {string} \
    {Only operate on real values} \
    { set ::QA::fracc::test::Real $::QA::Options::Value }
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
qaInit
#------------------------------------------------------------------------
# Load additional packages that will be needed.
#------------------------------------------------------------------------
package require QAFrameSpec

#------------------------------------------------------------------------
# Specialize some data for specific version of the frame specification.
#------------------------------------------------------------------------

namespace eval ::QA::FrameSpec {
    ;## :TODO: add tests for detectSim and strain
    array set expval {
        char       a
        char_u     b
        int_2u     1
        int_4u     2
        int_8u     3
        int_2s    -1
        int_4s    -2
        int_8s    -3
        real_4     1.0000000e+00
        real_8     2.0000000000000000e+00
        lstring    TesT
        complex_8  {3.0000000e+00 4.0000000e+00}
        complex_16 {5.0000000000000000e+00 6.0000000000000000e+00}
    }

    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## Version 6
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval Version6 {
        variable TYPE R_std_test_frame_ver6
        variable IFOS T
        variable TIMES 600000000-600000000

        array set alt_expval {
            Detector:prefix {a\000}
            ProcData:tRange  1024.0
            SimData:timeOffset 0.0
        }

        namespace eval Skip {
            ;##--------------------------------------------------------------
            ;## List of structures known not to work
            ;##--------------------------------------------------------------
            variable Structs [ list Msg StatData Summary]
            variable Elements [ list \
                                    Event:nParam \
                                    Event:parameterNames \
                                    Event:parameters \
                                    ProcData:auxParam \
                                    ProcData:auxParamNames \
                                    ProcData:nAuxParam \
                                    SimEvent:nParam \
                                    SimEvent:parameterNames \
                                    SimEvent:parameters \
                                       ]
            variable ElementQueries [ list \
                                          Detector:prefix \
                                          Event:eventStatus \
                                          Msg:alarm \
                                          ProcData:auxParamNames \
                                          SimData:timeOffset \
                                          StatData:name \
                                          StatData:representation \
                                          StatData:comment \
                                          StatData:timeEnd \
                                          StatData:timeStart \
                                          StatData:version \
                                         ]
        } ;# namespace - Skip

    }; # namespace - Version6
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ;## Version 5
    ;##++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    namespace eval Version5 {
        variable TYPE R_std_test_frame_ver6
        variable IFOS T
        variable TIMES 600000000-600000000

        array set alt_expval {
            Detector:prefix {a\000}
            ProcData:tRange  1024.0
            SimData:timeOffset 0.0
        }

        namespace eval Skip {
            ;##--------------------------------------------------------------
            ;## List of structures known not to work
            ;##--------------------------------------------------------------
            variable Structs [ list Msg StatData Summary]
            variable Elements [ list \
                                    Event:nParam \
                                    Event:parameterNames \
                                    Event:parameters \
                                    ProcData:auxParam \
                                    ProcData:auxParamNames \
                                    ProcData:nAuxParam \
                                    SimEvent:nParam \
                                    SimEvent:parameterNames \
                                    SimEvent:parameters \
                                       ]
            variable ElementQueries [ list \
                                          Detector:prefix \
                                          Event:eventStatus \
                                          Msg:alarm \
                                          ProcData:auxParamNames \
                                          SimData:timeOffset \
                                          StatData:name \
                                          StatData:representation \
                                          StatData:comment \
                                          StatData:timeEnd \
                                          StatData:timeStart \
                                          StatData:version \
                                         ]
        } ;# namespace - Skip

    }; # namespace - Version5
} ;# namespace - ::QA::FrameSpec


namespace eval ::QA::fracc::test {
    ;##------------------------------------------------------------------
    ;## Importing of external elements to simplify coding
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::Flush
    namespace import ::QA::Debug::Puts
    namespace import ::QA::TclTest::SubmitJob

    ;##------------------------------------------------------------------
    ;## Definition of test data
    ;##------------------------------------------------------------------

    variable Constraints
    variable Results
    variable FrameSpec

    variable urlList
    variable usercmd
    variable ilwd
    variable vect
    variable query
    variable expval
    namespace eval Skip {}

    if { ! [info exists FrameSpec] } {
        set FrameSpec $::QA::FrameSpec::CURRENT_SPEC
    }
    set FrameSpecImport [subst ::QA::FrameSpec::[subst $FrameSpec]::*]
    namespace import ::tcltest::*
    namespace import ::QA::TclTest::*
    namespace import [subst $FrameSpecImport]

    Init [namespace current]
    ::QA::ImportVariables \
        ::QA::FrameSpec::[subst $FrameSpec]::Skip \
        [namespace current]::Skip

    Puts 1 "Initializing...\n"

    proc sendjob {cmd} {
        catch {SubmitJob job $cmd} err
        set urlList $job(outputs)
        LJdelete job

        return $urlList
    }

    proc IsElementQueryValid { Struct Element } {
        variable Constraints

        if { [lsearch $Skip::ElementQueries "$Struct:$Element"] == -1 } {
            return 1
        }
        lappend Constraints knownBug
        return 0
    }

    proc IsElementValid { Struct Element } {
        variable Constraints

        if [IsStructValid $Struct] {
            if { [lsearch $Skip::Elements "$Struct:$Element"] == -1 } {
                return 1
            } else {
                lappend Constraints knownBug
            }
        }
        return 0
    }

    proc IsStructValid { Struct } {
        variable Constraints
        variable FrameSpec

        if { [lsearch $Skip::Structs $Struct] == -1  } {
            return 1
        } else {
            lappend Constraints knownBug
            return 0
        }
    }

    proc geturl {url} {
        set file "$::TMP/temp[clock seconds][clock clicks].ilwd"

        if {[catch {LJcopy $url $file}]} {
            catch {file delete $file}
            error "Error retrieving output file from $url."
        }

        if {[catch {open $file r} fid]} {
            error "Error opening output file $file: $fid"
        }
        set ilwd [read -nonewline $fid]
        catch {close $fid}
        catch {file delete $file}

        return $ilwd
    }

    set usercmd {
        getFrameElements
        -returnprotocol http://foo
        -outputformat {ilwd ascii}
        -framequery { {$TYPE} \
                          {$IFOS} \
                          {} \
                          {$TIMES} \
                          $query }
    }

    if {$Real} {
        ;## skip the tests for structures that don't yet exist
        ;## in production frames
        configure -skip {3:*}
    }

    ;###=================================================================
    ;## Test Set 1
    ;###=================================================================
    ;## Get full structure in ilwd
    ;## Extract the elements, types, and values
    ;## Compare with expected types and values
    ;##------------------------------------------------------------------
    Puts 1 "****************"
    Puts 1 "* TEST SUITE 1 *"
    Puts 1 "****************"
    foreach struct $structList {
        variable Constraints [list]

        ;##--------------------------------------------------------------
        ;## Query for the structure
        ;##--------------------------------------------------------------
        set ilwdx [list]
        IsStructValid $struct
        test 1:query:${struct} {} -constraints $Constraints -setup {
            set query $struct
            append query "(0)"

            set branch "1,0"
            if {[lsearch -exact $nested $struct] != -1} {
                set branch "2,0"
            }
            Puts 10 "INFO: query: $query"
        } -body {
            set urlList [sendjob [subst -nobackslashes $usercmd]]
            set ilwd [geturl [lindex $urlList 0]]

            Puts 10 $ilwd
            set ilwdx [extractILWD $ilwd $branch 1]
            Puts 10 $ilwdx
            regexp -line {^(.+)$} $ilwd -> head
            list $head
        } -result {<?ilwd?>}
        ;##--------------------------------------------------------------
        ;## Ensure that structure has the appropriate elements and values.
        ;##--------------------------------------------------------------
        foreach item $ilwdx {
            foreach {type att val} $item {break}
            array set attr $att
            set class($attr(name)) $type
            set value($attr(name)) $val
        }
        foreach name [lsort [array names $struct]] {
            ;## can't check name field because it is not
            ;## an individual element in the ilwd but rather
            ;## specified in the container header <ilwd name=....>
            if {[string equal "name" $name]} {continue}

            set desc ${struct}:${name}
            set type [set ${struct}($name)]

            ;##----------------------------------------------------------
            ;## Verify the existence of the stucture elements
            ;##----------------------------------------------------------
            variable Constraints [list]
            IsElementValid $struct $name
            test 1:elements:${desc} {} -constraints $Constraints -setup {
                set res {}
                if {[lsearch -exact $exceptions $desc] != -1} {
                    regexp {^(.+)([SN])} $name -> name res
                    switch -exact -- $res {
                        S {set value($name) [lindex $value($name) 0]}
                        N {set value($name) [lindex $value($name) 1]}
                        default {}
                    }
                }
            } -body {
                if {[lsearch [array names class] $name] == -1} {
                    error "Can't find '$name'"
                }
                if  { [ info exist alt_expval($desc) ] } {
                    if  { $value($name) == $alt_expval($desc) } {
                        Puts 5 "$name has alt value $alt_expval($desc) for $type"
                        set value($name) $expval($type)
                    }
                }
                concat $class($name) [expr {$Real?"":$value($name)}]
            } -result [concat $type [expr {$Real?"":$expval($type)}]]
            flush [outputChannel]
            after 1000
        }
        array unset class
        array unset attr
        array unset value
    }

    ;###=================================================================
    ;## Test Set 2
    ;###=================================================================
    ;## Query for individual elements in structure
    ;## Compare with expected name and type
    ;##------------------------------------------------------------------
    Puts 1 "****************"
    Puts 1 "* TEST SUITE 2 *"
    Puts 1 "****************"
    foreach struct $structList {
        foreach name [lsort [array names $struct]] {
            variable Constraints [list]
            set desc ${struct}:${name}

            set type [set ${struct}($name)]

            set res {}
            if {[lsearch -exact $exceptions $desc] != -1} {
                regexp {^(.+)([SN])} $name -> name res
            }
            set result [concat $name $type [expr {$Real?"":$expval($type)}]]

            IsElementQueryValid $struct $name
            tcltest::test 2:${desc} {} -constraints $Constraints -setup {
                set query ${struct}[string toupper $name 0 0]
                append query "(0)"
            } -body {
                set urlList [sendjob [subst -nobackslashes $usercmd]]
                set ilwd [geturl [lindex $urlList 0]]

                Puts 10 $ilwd
                set retval [extractILWD $ilwd]
                Puts 5 $retval

                foreach {class att val} [lindex $retval 0] {break}
                array set attr $att

                switch -exact -- $res {
                    S {set val [lindex $val 0]}
                    N {set val [lindex $val 1]}
                    default {}
                }
                if  { [ info exist alt_expval($desc) ] } {
                    if  { $val == $alt_expval($desc) } {
                        Puts 5 "INFO: $name has alt value $val for $type"
                        set val $expval($type)
                    }
                }

                concat $attr(name) $class [expr {$Real?"":$val}]
            } -result $result
            flush [outputChannel]

            array unset attr
        }
    }

    ;###=================================================================
    ;## Test Set 3
    ;###=================================================================
    ;## Subroutines needed for this test set
    ;##------------------------------------------------------------------
    proc ADCSetup { Type } {
        variable Constraints
        variable Results
        
        set Constraints [list]
        switch -exact -- $Type {
            lstring {
                lappend Constraints knownBug
            }
            default {
            }
        }
    }
    proc ADCVectorExec { } {
        variable urlList
        variable usercmd
        variable ilwd
        variable vect
        variable query
        variable TYPE
        variable IFOS
        variable TIMES

        set urlList [sendjob [subst -nobackslashes $usercmd]]
        set ilwd [geturl [lindex $urlList 0]]
            
        set vect [extractILWD $ilwd 2,13]
    }
    ;##------------------------------------------------------------------
    ;## Main tests
    ;##------------------------------------------------------------------
    Puts 1 "****************"
    Puts 1 "* TEST SUITE 3 *"
    Puts 1 "****************"
    set query "AdcData(0)"

    set passed 0
    test 3:AdcData:Vector {} {
        ADCVectorExec
        Puts 10 $ilwd
        Puts 10 $vect
        set passed [regexp -line {^<([^ ]+) } $ilwd -> tag]
        list $tag
    } {ilwd}

    if { ! [info exists vect] } {
        ADCVectorExec
        set passed [regexp -line {^<([^ ]+) } $ilwd -> tag]
    }

    ;## continue only if the test passed
    foreach item $vect {
        foreach {type att val} $item {break}
        switch -exact -- $type {
            char -
            char_u {
                set value($type) [string index [subst $val] 0]
            }
                
            complex_8 -
            complex_16 {
                set value($type) [lrange $val 0 1]
            }

            default {
                set value($type) [lindex $val 0]
            }
        }
    }
            
    ;## print list of types received
    Puts 10  "INFO: Found types:\n   [join [array names value] "\n   "]"
    foreach type [array names expval] {
        variable Constraints

        ADCSetup $type
        tcltest::test 3:DataClass:$type {} -constraints $Constraints -body {
            if {[lsearch [array names value] $type] == -1} {
                error "Can't find '$type' type"
            }
            if  { $value($type) != $expval($type) } {
                if  { [ info exist $alt_expval($type)] } {
                    if  { $value($type) == $alt_expval($type) } {
                        Puts 5 "INFO: $type matches alternate value $alt_expval($type)"
                        set value($type)  $expval($type)                     
                    }
                }
            }
            set value($type)
        } -result "$expval($type)"
        flush [outputChannel]

        after 1000
    }
    array unset value

    cleanupTests
}
namespace delete ::QA::fracc::test
