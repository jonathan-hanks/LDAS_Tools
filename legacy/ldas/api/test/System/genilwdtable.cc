#include "dbaccess/config.h"

#include <limits.h>

#include <fstream>
#include <iomanip>
#include <memory>
#include <sstream>

#include "general/autoarray.hh"
#include "general/gpstime.hh"
#include "general/unordered_map.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/util.hh"

#include "dbaccess/Field.hh"
#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldBlob.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/FieldUniqueId.hh"
#include "dbaccess/Table.hh"

typedef double BLOB_TYPE;
const int Now = (int)time( 0 );
const General::GPSTime GPSNow( General::GPSTime::NowGPSTime( ) );

using General::unordered_map;

namespace
{
  class Handler
  {
  public:
    virtual ~Handler( )
    {
    }

    virtual void operator()( const DB::Table& Table,
			     DB::Field& Field,
			     unsigned int Rows ) const = 0;
  };

  class CharacterHandler
    : public Handler
  {
  public:
    CharacterHandler( const std::string& Value );

    void operator()( const DB::Table& Table,
		     DB::Field& Field,
		     unsigned int Rows ) const;
  private:
    const std::string	m_value;
  };

  class IntegerHandler
    : public Handler
  {
  public:
    IntegerHandler( const INT_4U Value );

    void operator()( const DB::Table& Table,
		     DB::Field& Field,
		     unsigned int Rows ) const;
  private:
    const INT_4U	m_value;
  };

  class GeneratedAlwaysHandler
    : public Handler
  {
  public:
    void operator()( const DB::Table& Table,
		     DB::Field& Field,
		     unsigned int Rows ) const;
  };

  class GPSSecondHandler
    : public Handler
  {
  public:
    void operator()( const DB::Table& Table,
		     DB::Field& Field,
		     unsigned int Rows ) const;
  };

  class SkipHandler
    : public Handler
  {
  public:
    void operator()( const DB::Table& Table,
		     DB::Field& Field,
		     unsigned int Rows ) const;
  };

  class Handlers
  {
  public:
    const Handler& GetHandler( const DB::Table& Table,
			       const DB::Field& Field ) const;

    void SetHandler( const std::string& Table,
		     const std::string& Field,
		     Handler& Handler );

  private:
    typedef unordered_map< std::string, Handler* >	handler_list_type;
    handler_list_type	m_handlers;
  };

  template < class FieldType >
  void fill( const DB::Table& Table, FieldType& Field, unsigned int Rows );

  template<>
  inline
  void fill< DB::FieldBlob >( const DB::Table& Table, 
			      DB::FieldBlob& Field, unsigned int Rows )
  {
    int blob_size = 64;

    //-------------------------------------------------------------------
    // Calculate the number of bytes to allocate for the blob
    //-------------------------------------------------------------------
    if ( Rows >= 10000 )
    {
      blob_size = 1;
    }
    else if ( Rows >= 1000 )
    {
      blob_size = 5;
    }

    //-------------------------------------------------------------------
    // Create a buffer large enough to hold the maximum blob
    //-------------------------------------------------------------------
    General::AutoArray< CHAR_U > blob_buf( new CHAR_U[ Rows * blob_size ] );

    try
    {
      for ( unsigned int x = 0; x < Rows; ++x )
      {
	const CHAR_U fill_char( x % UCHAR_MAX );
	const size_t size( ( x % 100 + 1 ) * blob_size );

	std::fill( blob_buf.get( ), blob_buf.get( ) + size, fill_char );
	Field.push_back( blob_buf.get( ), size );
      }
    }
    catch( const std::exception& e )
    {
      std::cerr << "ERROR: fill< DB::FieldBlob >: Exception: "
		<< e.what( ) << std::endl;
      exit( 1 );
    }
  }

  template<>
  inline
  void fill< DB::FieldString >( const DB::Table& Table, 
				DB::FieldString& Field, unsigned int Rows )
  {
    for ( unsigned int x = 0; x < Rows; ++x )
    {
      std::ostringstream	data;

      data /* << Table.GetName( ) << ":"
	      << Field.GetName( ) << "_" */
	   << (int)( Now + x + 1 );
      Field.push_back( data.str( ) );
    }
  }

  template<>
  inline
  void fill< DB::FieldUniqueId >( const DB::Table& Table, 
				  DB::FieldUniqueId& Field,
				  unsigned int Rows )
  {
    // Ex: "20040424005905964369000000"
    //      YYYYMMDDHHMMSS123456789012
    int	magnitude = 1;
    unsigned int r = Rows;
    CHAR_U	process_id[ 13 ];

    //-------------------------------------------------------------------
    // Calculate the magnitude of the number of rows
    //-------------------------------------------------------------------
    while ( r > 10 )
    {
      magnitude++;
      r = r / 10;
    }

    try
    {
      for ( unsigned int x = 0; x < Rows; ++x )
      {
	std::ostringstream	uniqueid_stream;

	uniqueid_stream << "200404240059059"
			<< std::setfill( '0' ) << std::setw( magnitude ) << x
			<< std::setfill( '0' ) << std::setw( 11 - magnitude )
			<< 0;

	std::string	id( uniqueid_stream.str( ) );
	//---------------------------------------------------------------
	// convert to 13 bit BCD
	//---------------------------------------------------------------
	for ( unsigned int
		x = 0,
		j = 0,
		end = id.length( );
	      x < end;
	      x += 2, j++ )
	{
	  process_id[ j ] = ( ( ( id[ x ] - '0' ) << 4 ) +
			      (id[ x + 1 ] - '0' ) );
	}

	Field.push_back( process_id, sizeof( process_id ) );
      }
    }
    catch( const std::exception& e )
    {
      std::cerr << "ERROR: fill< DB::FieldUniqueId >: Exception: "
		<< e.what( ) << std::endl;
      exit( 1 );
    }
  }

  template < class ArrayType >
  inline
  void fill_field_array( DB::FieldArray< ArrayType >& Field,
			 unsigned int Rows )
  {
    for ( unsigned int x = 0; x < Rows; ++x )
    {
      Field.push_back( (ArrayType)( Now + x + 1) );
    }
  }

  template < class ArrayType >
  inline
  void fill_field_array( DB::FieldArray< ArrayType >& Field,
			 unsigned int Rows,
			 ArrayType Value,
			 const ArrayType Inc )
  {
    for ( unsigned int x = 0; x < Rows; ++x )
    {
      Field.push_back( (ArrayType)( Value ) );
      Value += Inc;
    }
  }

  inline void
  fill_field( const DB::Table& Table, DB::Field& Field, unsigned int Rows )
  {
#define HANDLE_ARRAY( TYPE ) \
    case DB::Field::FIELD_ARRAY_ ## TYPE: \
	fill_field_array( dynamic_cast< DB::FieldArray< TYPE >& > \
			  ( Field ), \
			  Rows ); \
	break;

    switch( Field.GetType( ) )
    {
    HANDLE_ARRAY( CHAR );
    HANDLE_ARRAY( CHAR_U );
    HANDLE_ARRAY( INT_2S );
    HANDLE_ARRAY( INT_2U );
    HANDLE_ARRAY( INT_4S );
    HANDLE_ARRAY( INT_4U );
    HANDLE_ARRAY( INT_8S );
    HANDLE_ARRAY( INT_8U );
    HANDLE_ARRAY( REAL_4 );
    HANDLE_ARRAY( REAL_8 );
    HANDLE_ARRAY( COMPLEX_8 );
    HANDLE_ARRAY( COMPLEX_16  );
    case DB::Field::FIELD_BLOB:
      fill( Table, dynamic_cast< DB::FieldBlob& >( Field ), Rows );
      break;
    case DB::Field::FIELD_STRING:
      fill( Table, dynamic_cast< DB::FieldString& >( Field ), Rows );
      break;
    case DB::Field::FIELD_UNIQUE_ID:
      fill( Table, dynamic_cast< DB::FieldUniqueId& >( Field ), Rows );
      break;
    case DB::Field::FIELD_NULL:
      break;
    }
  }

  Handlers	SpecialTableColumns;
}

static void
create_table( const char* TableName, unsigned int Rows )
{
  //---------------------------------------------------------------------
  // Create the table
  //---------------------------------------------------------------------

  std::auto_ptr< DB::Table > tbl( DB::Table::CreateTable( TableName ) );
  
  //---------------------------------------------------------------------
  // Initialize columns either by special handlers or default routines
  //---------------------------------------------------------------------
  for ( unsigned int x = 0,
	  end = tbl->GetFieldCount( );
	x < end;
	++x )
  {
    DB::Field&	f( tbl->GetField( x ) );

    try
    {
      const Handler&	h( SpecialTableColumns.GetHandler( *tbl, f ) );
      //-----------------------------------------------------------------
      // Use the special handler for this column
      //-----------------------------------------------------------------
      h( *tbl, f, Rows );
    }
    catch( ... )
    {
      //-----------------------------------------------------------------
      // Use default initializer
      //-----------------------------------------------------------------
      switch( f.GetType( ) )
      {
      case DB::Field::FIELD_NULL:
	break;
      default:
	fill_field( *tbl, f, Rows );
	break;
      }
    }
  }
	

  //---------------------------------------------------------------------
  // Wrap the container one more time
  //---------------------------------------------------------------------

  ILwd::LdasContainer ilwd_file( "ligo:ldas:file" );
  ilwd_file.push_back( tbl->GetILwd( ),
		       ILwd::LdasContainer::NO_ALLOCATE,
		       ILwd::LdasContainer::OWN );

  //---------------------------------------------------------------------
  // Write out the contents
  //---------------------------------------------------------------------

  std::ostringstream	filename;

  if ( ::getenv( "LDASTESTTMP" ) != (char*)NULL )
  {
    filename << ::getenv( "LDASTESTTMP" ) << "/";
  }
  else
  {
    filename  << "./";
  }
  filename << TableName << Rows << ".ilwd";

  std::ofstream	ofs( filename.str( ).c_str( ) );
  ILwd::writeHeader( ofs );
  ilwd_file.write( 0, 4, ofs, ILwd::ASCII, ILwd::NO_COMPRESSION );

  ofs << std::endl;

  ofs.close( );
}

int
main( int ArgC, char** ArgV )
try
{
  //---------------------------------------------------------------------
  // Setup exceptions
  //---------------------------------------------------------------------
  std::ostringstream	data;
  data << (int)( Now );

  GeneratedAlwaysHandler	gah;
  CharacterHandler		ifoh( "Z0" );
  SkipHandler			sh;
  CharacterHandler		media_status( "OK" );
  IntegerHandler		lenh( data.str( ).length( ) );
  CharacterHandler		type_clob( "type_clob" );
  CharacterHandler		chanlist( "c1;c2;c3;c4" );

  SpecialTableColumns.SetHandler( "frameset_chanlist", "chanlist", chanlist );
  SpecialTableColumns.SetHandler( "frameset_loc", "media_status",
				  media_status );
  SpecialTableColumns.SetHandler( "gds_trigger", "binarydata_length", lenh );
  SpecialTableColumns.SetHandler( "gds_trigger", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "ilwdtypes", "type_clob", type_clob );
  SpecialTableColumns.SetHandler( "runlist", "site", ifoh );
  SpecialTableColumns.SetHandler( "sim_inst", "sim_type", gah );
  SpecialTableColumns.SetHandler( "sim_type", "sim_type", sh );
  SpecialTableColumns.SetHandler( "sim_type_params", "sim_type", gah );
  SpecialTableColumns.SetHandler( "sngl_block", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_burst", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_datasource", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_dperiodic", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_inspiral", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_mime", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_ringdown", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_transdata", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_unmodeled", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "sngl_unmodeled_v", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "summ_comment", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "summ_value", "ifo", ifoh );
  SpecialTableColumns.SetHandler( "waveburst", "ifo_1", ifoh );
  SpecialTableColumns.SetHandler( "waveburst", "ifo_2", ifoh );
  SpecialTableColumns.SetHandler( "waveburst", "sim_type", gah );
  

  //---------------------------------------------------------------------
  // Figure out the command line options specified
  //---------------------------------------------------------------------

  unsigned int	rows = 0;

  if ( ArgC < 2 )
  {
    std::cerr << "USAGE: " << ArgV[ 0 ] << " <row count> <table> ..."
	      << std::endl;
    exit( 1 );
  }

  std::istringstream irows( ArgV[ 1 ] );
  irows >> rows;

  if ( ArgC == 2 )
  {
    std::list< std::string >	table_names;
    DB::Table::GetTableNames( table_names );

    for ( std::list< std::string >::const_iterator
	    cur = table_names.begin( ),
	    last = table_names.end( );
	  cur != last;
	  ++cur )
    {
      create_table( cur->c_str( ), rows );
    }
  }
  else
  {
    for ( int x = 2; x < ArgC; ++x )
    {
      create_table( ArgV[ x ], rows );
    }
  }
  //---------------------------------------------------------------------
  // Normal exit
  //---------------------------------------------------------------------
  exit( 0 );
}
catch( const std::exception& e )
{
  std::cerr << "ERROR: Caught exception: " << e.what( ) << std::endl;
  exit( 1 );
}
catch( ... )
{
  std::cerr << "ERROR: Caught an unknown exception" << std::endl;
  exit( 1 );
}

namespace
{
  CharacterHandler::
  CharacterHandler( const std::string& Value )
    : m_value( Value )
  {
  }

  void CharacterHandler::
  operator()( const DB::Table& Table,
	      DB::Field& Field,
	      unsigned int Rows ) const
  {
    switch( Field.GetType( ) )
    {
    case DB::Field::FIELD_STRING:
      {
	DB::FieldString& f = dynamic_cast< DB::FieldString& >( Field );

	for ( unsigned int x = 0; x < Rows; ++x )
	{
	  f.push_back( m_value );
	}
      }
      break;
    case DB::Field::FIELD_BLOB:
      {
	DB::FieldBlob& f = dynamic_cast< DB::FieldBlob& >( Field );

	for ( unsigned int x = 0; x < Rows; ++x )
	{
	  f.push_back( (const CHAR_U*)( m_value.c_str( ) ),
		       m_value.length( ) );
	}
      }
      break;
    default:
      {
	std::ostringstream msg;
	msg << "CharacterHandler called on unsupported column type: "
	    << Field.GetType( );
	throw std::runtime_error( msg.str( ) );
      }
      break;
    }
  }

  void GeneratedAlwaysHandler::
  operator()( const DB::Table& Table,
	      DB::Field& Field,
	      unsigned int Rows ) const
  {
    switch( Field.GetType( ) )
    {
    case DB::Field::FIELD_ARRAY_INT_4S:
      fill_field_array( dynamic_cast< DB::FieldArray< INT_4S >& >( Field ),
			Rows,
			(INT_4S)( 0 ),
			(INT_4S)( 1 ) );
      break;
    default:
      {
	std::ostringstream msg;
	msg << "GeneratedAlwaysHandler called on unsupported column type: "
	    << Field.GetType( );
	throw std::runtime_error( msg.str( ) );
      }
      break;
    }
  }

  void GPSSecondHandler::
  operator()( const DB::Table& Table,
	      DB::Field& Field,
	      unsigned int Rows ) const
  {
    switch( Field.GetType( ) )
    {
    case DB::Field::FIELD_ARRAY_INT_4S:
      fill_field_array( dynamic_cast< DB::FieldArray< INT_4S >& >( Field ),
			Rows,
			(INT_4S)( GPSNow.GetSeconds( ) + 1 ),
			(INT_4S)( 1 ) );
      break;
    default:
      {
	std::ostringstream msg;
	msg << "GPSSecondHandler called on unsupported column type: "
	    << Field.GetType( );
	throw std::runtime_error( msg.str( ) );
      }
      break;
    }
  }

  IntegerHandler::
  IntegerHandler( const INT_4U Value )
    : m_value( Value )
  {
  }

  void IntegerHandler::
  operator()( const DB::Table& Table,
	      DB::Field& Field,
	      unsigned int Rows ) const
  {
    switch( Field.GetType( ) )
    {
    case DB::Field::FIELD_ARRAY_INT_4S:
      fill_field_array( dynamic_cast< DB::FieldArray< INT_4S >& >(Field),
			Rows,
			(INT_4S)m_value,
			(INT_4S)0 );
      break;
    default:
      {
	std::ostringstream msg;
	msg << "IntegerHandler called on unsupported column type: "
	    << Field.GetType( );
	throw std::runtime_error( msg.str( ) );
      }
      break;
    }
  }

  void SkipHandler::
  operator()( const DB::Table& Table,
	      DB::Field& Field,
	      unsigned int Rows ) const
  {
    //-------------------------------------------------------------------
    // This is a do nothing handler for fields that should not be put in
    //-------------------------------------------------------------------
  }

  const Handler& Handlers::
  GetHandler( const DB::Table& Table,
	      const DB::Field& Field ) const
  {
    std::string	key( Table.GetName( ) );
    key += ":";
    key += Field.GetName( );
    
    handler_list_type::const_iterator
      cur( m_handlers.find( key ) );
    if ( cur != m_handlers.end( ) )
    {
      return *(cur->second);
    }
    std::ostringstream	msg;
    msg << "No special handler for: " << key;
    throw std::range_error( msg.str( ) );
  }

  void Handlers::
  SetHandler( const std::string& Table,
	      const std::string& Field,
	      Handler& H )
	      
  {
    std::string	key( Table );
    key += ":" + Field;
    
    m_handlers[ key ] = &H;
  }
}
