#! /ldcg/bin/tclsh

#set ::env(C_VERBOSE_MODE) 20
set ::env(TEST_VERBOSE_MODE) 20

set packages [ package names ]
if	{ [ regexp -nocase Tk $packages ] } {
	set ::WISH 1
} else {
	set ::WISH 0
}
set email_mode [ lindex $argv 0 ]

if	{ [ string match persistent $email_mode ] } {
	set email persistent_socket
} else {
	set email mlei@ligo.caltech.edu
} 
puts "email $email"

set ::host ldas-suntest3.ligo.caltech.edu
#set  ::host ldas-dev.ligo.caltech.edu
set ::port 10031
set ::contact_string ${::host}:$::port

set ::SHAREDDIR /ldas/share/ldas
set ::cmddir [ file join $::SHAREDDIR ldascmds ]
set ::SITE [ exec uname -n ]
;#end  


;## use grid-proxy-destroy to trigger use of grid-proxy-init

proc debugPuts { msg } {
	
	if	{ $::DEBUG } {	
		puts $msg
    } 
}

proc addLogEntry { args } {

	debugPuts [ file join $args ]
}

namespace eval RawGlobusClient {
	set gridprogs [ list grid-proxy-info grid-proxy-init grid-proxy-destroy ]
    set libs [ list globus_module \
		  globus_error \
		  globus_object \
		  globus_xio \
          gssapi ]
    set waitforbytes 1
    set buffersize 4096
    set info ".info"
}

;## list all cmd client cmds in /ldas
proc getClientCmds {} {

	set cmds [ list ]
    set ::LATERCMDS [ list ]
	foreach subdir [ lsort [ glob -nocomplain $::cmddir/* ]] {
	set usercmd [ file tail $subdir ]
	regsub {\.cmd} $usercmd {} usercmd
	
	if	{ [ string equal createRDS [ file tail $subdir ] ] } {
		switch -regexp -- $::SITE {
			mit { set cmds [ concat $cmds [ glob -nocomplain $subdir/createRDS_mit*.cmd ] ] }
			llo  { set cmds [ concat $cmds [ glob -nocomplain $subdir/createRDS_L_*_1.cmd ] ] 
				   set ::LATERCMDS [ concat $::LATERCMDS [ glob -nocomplain $subdir/createRDS_L_*_2.cmd ] ] }
			lho  { set cmds [ concat $cmds [ glob -nocomplain $subdir/createRDS_H_*_1.cmd ] ] 
				   set ::LATERCMDS [ concat $::LATERCMDS [ glob -nocomplain $subdir/createRDS_H_*_2.cmd ] ] }
			default  { set cmds [ concat [ glob -nocomplain $subdir/createRDS*_1.cmd ] ]
                    lappend cmds $subdir/createRDS_mit.cmd
				    set ::LATERCMDS [ concat $::LATERCMDS [ glob -nocomplain $subdir/createRDS*_2.cmd ] ] }
		}
	} else {
    	set cmds [ concat $cmds [ lsort [ glob -nocomplain $subdir/*.cmd ] ] ]
	}
    
    }
    set cmds [ concat $cmds $::LATERCMDS ]
    return $cmds

} 

## ******************************************************** 
##
## Name: bgerror 
##
## Description:
## Handles uncaught errors
##
## Usage:
##
## Comments:

proc bgerror { msg } {
     puts stderr "bgerror: $msg"
	 puts stderr $::errorInfo 

     set strlist [ split $msg ]
     set index [ lsearch $strlist "*sock*" ]
     if  { $index > -1 } {
         set sid [ lindex $strlist $index ]
         regsub -all  {[\"]} $sid {} sid 
         puts "bgerror sid = $sid."
    }
}
#------------------------------------------------------------------------
# From: Practical Programming in Tcl and Tk 4th Edition pp. 139
# Assign a set of variables from a alist of values.
# If there are more values than variables, they are returned.
# If there are fewer values than variables,
# the variables get the empty string
#------------------------------------------------------------------------
proc lassign {valueList args} {
    if { [llength $args] == 0} {
	error "wrong \# args: lassign list varname ?varname..?"
    }
    if { [llength $valueList] == 0} {
	# Ensure one trip through the foreach loop
	set valueList [list {}]
    }
    uplevel 1 [list foreach $args $valueList {break}]
    return [lrange $valueList [llength $args] end]
}

## ******************************************************** 
##
## Name: RawGlobusClient data structures
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:
## is there a limit on buffer so need to read again

;## tcpdriver - tcp driver from globus_xio_driver_load
;## filedriver - file driver from globus_xio_driver_load
;## driver stack - driver stack from globus_xio_stack_push_driver 
;## tcphandle - tcp handle   from globus_xio_handle_create 
;## filehandle - file handle from globus_xio_handle_create 
;## buffer - to hold data for read/write operations
;## buffersize - number of bytes to read 
;## nbytes - number of bytes to write
;## contact_string - data passed during globus_xio_open 


## ******************************************************** 
##
## Name: RawGlobusClient::init
##
## Description:
## initialize to use the RawGlobusClient toolkit
##
## Usage:
##
## Comments:

proc RawGlobusClient::init {} {

    set seqpt ""
  
	foreach lib $::RawGlobusClient::libs {
    	load [ eval file join $::TCLGLOBUS_DIR libtcl${lib}.so ]
	}

    if	{ [ info exist $::globus_i_xio_module ] } {
    	return -code error "No globus xio extensions are loaded"
    }
    
    if	{ [ catch {
    
    #----------------------------------------------------------------
	# Activate RawGlobusClient XIO module
	#----------------------------------------------------------------
		set seqpt "globus_module_activate"
		globus_module_activate $::globus_i_xio_module
	
	#----------------------------------------------------------------
	# Load transport driver
	#----------------------------------------------------------------
		set seqpt "globus_xio_driver_load"
		lassign [ globus_xio_driver_load tcp ] result ::RawGlobusClient::tcpdriver
        debugPuts "load tcp result $result"
    	lassign [ globus_xio_driver_load gsi ] result ::RawGlobusClient::gsidriver
        debugPuts "load gsi result $result"
        
	    lassign [ globus_xio_stack_init NULL ] result ::RawGlobusClient::client_tcpstack	    
	    debugPuts "client init globus_xio_stack_push_driver result $result"
        
	    lassign [ globus_xio_stack_push_driver $::RawGlobusClient::client_tcpstack $::RawGlobusClient::tcpdriver ] result
        debugPuts "push tcp driver result $result"
		lassign [ globus_xio_stack_push_driver $::RawGlobusClient::client_tcpstack $::RawGlobusClient::gsidriver ] result
        debugPuts "push gsi driver result $result"
    
    #----------------------------------------------------------------
	# Authenication -  Set GSI authorization mode
	#---------------------------------------------------------------- 
    
    # TCP & GSI driver specific attribute initialization
    
	    debugPuts "GSI driver specific attribute initialization"
	    lassign [ globus_xio_attr_init ] result ::RawGlobusClient::client_attr
        
        ;## do not use IPV6 protocol
        lassign [ globus_xio_attr_cntl \
			      $::RawGlobusClient::client_attr \
			      $::RawGlobusClient::tcpdriver \
			      $::GLOBUS_XIO_TCP_SET_NO_IPV6 \
			      $::GLOBUS_TRUE \
			     ] status
		debugPuts "GLOBUS_XIO_TCP_SET_NO_IPV6: status: $status"
        
    	# $gss_c_no_credential indicates a default credential 
	    lassign [ globus_xio_attr_cntl \
			  $::RawGlobusClient::client_attr \
			  $::RawGlobusClient::gsidriver \
			  $::GLOBUS_XIO_GSI_SET_CREDENTIAL \
			  $::gss_c_no_credential \
			 ] status
	    debugPuts "GLOBUS_XIO_GSI_SET_CREDENTIAL status: $status"

	    # Authentication part - Set GSI authorization mode
	    lassign [ globus_xio_attr_cntl \
			  $::RawGlobusClient::client_attr \
			  $::RawGlobusClient::gsidriver \
			  $::GLOBUS_XIO_GSI_SET_AUTHORIZATION_MODE \
			  $::GLOBUS_XIO_GSI_HOST_AUTHORIZATION \
			 ] status 
	    debugPuts "GLOBUS_XIO_GSI_SET_AUTHORIZATION_MODE status: $status"
	    debugPuts "GSI driver specific attribute initialization $::RawGlobusClient::client_attr"
    	
    } err ] } {
    	debugPuts $err
    	return -code error "$seqpt: $err"
    }

}

## ******************************************************** 
##
## Name: RawGlobusClient::close
##
## Description:
## initialize to use the RawGlobusClient toolkit
##
## Usage:
##
## Comments:

proc RawGlobusClient::close {  { user_parms NULL } } {
	
    if	{ [ info exist ::RawGlobusClient::networkhandle ] } {    
    	catch { unset ::RawGlobusClient::done }
		;## remove outstanding callback
        lassign [ globus_xio_handle_cancel_operations \
        	$::RawGlobusClient::networkhandle $::GLOBUS_XIO_CANCEL_READ ] result 
        debugPuts "cancel operation read result $result"
        
        lassign [ globus_xio_handle_cancel_operations \
        	$::RawGlobusClient::networkhandle $::GLOBUS_XIO_CANCEL_WRITE ] result 
        debugPuts "cancel operation write result $result"       
        
    	lassign [ globus_xio_register_close $::RawGlobusClient::networkhandle NULL \
    	[ list RawGlobusClient::closeCallback $user_parms ] ] result 
        vwait ::RawGlobusClient::done  
        unset ::RawGlobusClient::done  
    } 
    if	{ [ string equal exit $user_parms ] } {
    	RawGlobusClient::cleanup
    }
}

proc RawGlobusClient::closeCallback { user_parms handle result } {
	
    debugPuts "closeCallback user_parms $user_parms handle $handle result $result"
    unset ::RawGlobusClient::networkhandle
    set ::RawGlobusClient::done 1
}

## ******************************************************** 
##
## Name: RawGlobusClient::cleanup
##
## Description:
## initialize to use the RawGlobusClient toolkit
##
## Usage:
##
## Comments:

proc RawGlobusClient::cleanup {} {
    
    global globus_i_xio_module
	global tcpdriver
	global filedriver
	
    #----------------------------------------------------------------
	# destroy the client attribute
	#------------::RawGlobusClient::client_attr----------------------------------------------------
    
    if	{ [ info exist ::RawGlobusClient::client_attr ] } {
    	lassign [ globus_xio_attr_destroy $::RawGlobusClient::client_attr ] status
    	debugPuts "destroy attr status $status"
        unset ::RawGlobusClient::client_attr
    }
    
    if	{ [ info exist ::RawGlobusClient::client_tcpstack ] } {
    	lassign [ globus_xio_stack_destroy $::RawGlobusClient::client_tcpstack ] status
    	debugPuts "destroy stack status $status"
        unset ::RawGlobusClient::client_tcpstack
    }
    
	#----------------------------------------------------------------
	# Unload both drivers
	#----------------------------------------------------------------
    
    if	{ [ info exist ::RawGlobusClient::gsidriver ] } {
    	lassign [ globus_xio_driver_unload $::RawGlobusClient::gsidriver ] status
    	debugPuts "globus_xio_driver_unload gsi status $status"
        unset ::RawGlobusClient::gsidriver
    }
	
    if	{ [ info exist ::RawGlobusClient::tcpdriver ] } {
		lassign [ globus_xio_driver_unload $::RawGlobusClient::tcpdriver ] status
    	debugPuts "globus_xio_driver_unload tcp status $status"
        unset ::RawGlobusClient::tcpdriver
    }
 
	#----------------------------------------------------------------
	# Deactivate RawGlobusClient XIO module
	#----------------------------------------------------------------
    
    if	{ [ info exist ::globus_i_xio_module ] } {
		lassign [ globus_module_deactivate $::globus_i_xio_module ] status 
    	debugPuts "globus_module_deactivate status $status, ::globus_i_xio_module [ info exist ::globus_i_xio_module ]"
   	}
}

## ******************************************************** 
##
## Name: sendCmd
##
## Description:
## issue a command, generally to operator socket of cntlmonAPI
##
## Usage:
##
## Comments:
## later we can use remote file transfer for getting job
## files over cmonTree::getFileContents 

proc RawGlobusClient::sendCmd { cmd { register 1 } } {

    ;## write cmd to server
    if  { [ catch {
    	if	{ [ string length $cmd ] } {
        	lassign [ globus_xio_register_write $::RawGlobusClient::networkhandle \
		    $cmd \
		    $::RawGlobusClient::waitforbytes \
		    NULL \
		    [ list RawGlobusClient::sendCmdCallback $register ] ] result 

            debugPuts "sendCmd done result $result"
                        
        } 
    } err ] } {
        # cmonClient::state0 lostconnect
        debugPuts "err: $err"
		return -code error "failed to connect: $err"
    }  
}

proc RawGlobusClient::sendCmdCallback { user_parms handle result buffer data_desc } {
	debugPuts "sendCmd callback $user_parms handle $handle result $result buffer $buffer data_desc $data_desc"
    
    incr ::NUMSENT 1
    ;## get ready to read data from server
    if	{ ! $user_parms } {
    	return
    }
    ;## dont register if it has been done
    if	{ [ catch {
    	if	{ [ string length $result ] } {
    		error $err
        }
  		lassign [ globus_xio_register_read $handle $::RawGlobusClient::buffersize \
    	 	$::RawGlobusClient::waitforbytes NULL \
    		[ list RawGlobusClient::readCallback $user_parms ] ] result
   	} err ] } {
    	debugPuts "sendCmdCallback err $err"
    }
                   
}	

## ******************************************************** 
##
## Name: readDataCallback
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:
## a callback is registered each time to listen to server data
## may return "An end of file occurred" from result if there is nothing

proc RawGlobusClient::readCallback { user_parms handle result buffer data_desc } {

	if	{ [ catch {
		debugPuts "readCallback read callback $user_parms handle $handle result  \
			$result buffer [ string length $buffer ] bytes"
    
    	if	{ [ regexp -nocase {end of file|error!|not a valid} $result ] } {
        	incr ::NUMRECV 1
    		error "$handle eof or error in job" 
		}
 		;## close socket to manager
        debugPuts $buffer 
       	if	{ [ string length $result ] } {
        	error $result
       	}
       	lassign [ globus_xio_register_read $handle $::RawGlobusClient::buffersize \
    	 		$::RawGlobusClient::waitforbytes NULL \
    			[ list RawGlobusClient::readCallback $user_parms ] ] result
        
    } err ] } {
    	puts stderr "readCallback error $err"
        catch { RawGlobusClient::close }
        set ::RawGlobusClient::done 1
    }
    debugPuts "$::NUMTESTS tests, sent $::NUMSENT, received $::NUMRECV"
}

## ******************************************************** 
##
## Name: openCallback
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:

proc RawGlobusClient::openCallback { user_parms handle result } {

	if	{ [ catch {
    	debugPuts "open callback, user_parms $user_parms handle $handle result $result"
		if	{ ! [ string length $result ] } {
			set cmd $::ldasjob
    		RawGlobusClient::sendCmd $cmd
    	} else {
        	error $result
    	}  
   } err  ] } {
   		puts stderr "openCallback error $err"
        catch { RawGlobusClient::close }
        set ::RawGlobusClient::done 1
   }  
}

## ******************************************************** 
##
## Name: RawGlobusClient::state1
##
## Description:
## enter state1 by opening socket to server via RawGlobusClient XIO 
##
## Usage:
##
## Comments:

proc RawGlobusClient::connect {} {
    
		if	{ [ catch {	
        
           	lassign [ globus_xio_handle_create $::RawGlobusClient::client_tcpstack ] result \
                    	::RawGlobusClient::networkhandle
                    
           	debugPuts "handle create result $result $::RawGlobusClient::networkhandle result $result"
                    
          	;## use non blocking IO   
          	lassign [ globus_xio_register_open $::RawGlobusClient::networkhandle \
                    	$::contact_string $::RawGlobusClient::client_attr \
                    	[ list RawGlobusClient::openCallback NULL ] ] result
              
		} err ] } {
			puts stderr "connect $err"
            return $err
		}
}

set ::DEBUG 1
set ::USE_GLOBUS 1

set ::TCLGLOBUS_DIR /ldcg/lib
set ::TCLGLOBUS_PORT 10031
puts "pid [ pid ]"
debugPuts " tclglobus directory $::TCLGLOBUS_DIR"

set files [ getClientCmds ]
set files [ list loop/Lw2ILwd.cmd loop/getFrameCache.cmd ]

set ::NUMTESTS [ llength $files ]
set ::NUMSENT 0
set ::NUMRECV 0

RawGlobusClient::init

foreach file $files {

	if	{ [ catch {
		set fd [ open $file r ]
		set cmd [ read -nonewline $fd ]
    	close $fd 
    	set cmd [ split $cmd \n ]
    	set newcmd ""
    	foreach line $cmd {
       		if	{ ! [ regexp {^[\s\t]*#} $line ] } {
            	append newcmd "$line "
        	}
   		} 
		set ldasjob ldasJob
        ;## get the name from cert 
		lappend ldasjob "-name Mary_O_Lei -password x.509 -email $email"
		lappend ldasjob $newcmd

		puts "length of ldasjob $file [ llength $ldasjob ]"

		RawGlobusClient::connect
		vwait ::RawGlobusClient::done
    	catch { unset ::RawGlobusClient::done }
    } err ] } {
    	puts stderr $err
        catch { RawGlobusClient::close }
	}
}
# puts "$::NUMTESTS tests, sent $::NUMSENT, received $::NUMRECV, done"
RawGlobusClient::cleanup
puts "[ pid ] exiting"
exit
