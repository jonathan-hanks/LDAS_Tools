#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::dbperformance::test {
    set Dims [list 1 10 100 1000]
    set Database ldas_tst
    set PreviousResultDirectory ""
    set Modes [list test validate statistics]
    set UpperLimit(ilwdValidate) 1000
    set FileConversionSpec [list]
} ;## namespace ::QA::dbperformance::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

::QA::Options::Add {} {--dbperformance-dims} {string} \
    {Comma seperated list of dimensions} \
    {set ::QA::dbperformance::test::Dims [split $::QA::Options::Value ,]}

::QA::Options::Add {} {--dbperformance-database} {string} \
    {Name of database to use for testing} \
    {set ::QA::dbperformance::test::Database $::QA::Options::Value}

::QA::Options::Add {} {--dbperformance-previous-result-dir} {string} \
    {Directory containing the previous result set} \
    {set ::QA::dbperformance::test::PreviousResultDirectory $::QA::Options::Value}

::QA::Options::Add {} {--dbperformance-upper-limit-ilwdValidate} {string} \
    {Maximum number of rows for which to perform ilwd validation} \
    {set ::QA::dbperformance::test::UpperLimit(ilwdValidate) $::QA::Options::Value}

::QA::Options::Add {} {--dbperformance-modes} {string} \
    {comma seperated list of operations to perform: test, validate, statistics, convert} \
    {set ::QA::dbperformance::test::Modes [split $::QA::Options::Value ,]}

::QA::Options::Add {} {--dbperformance-convert-files} {string} \
    {File conversion specification} \
    {set ::QA::dbperformance::test::FileConversionSpec [split $::QA::Options::Value ,]; set ::QA::dbperformance::test::Modes [list convert]}
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

set ::auto_path "[file join $::LDAS lib] $::auto_path"
set API cntlmon

source [file join $::TOPDIR cntlmonAPI cntlmon.state]
source [file join $::TOPDIR LDASapi.rsc]

package require base64
package require generic

set ::env(RUNDIR) $::TOPDIR 

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::dbperformance::test {

    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------

    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob

    ;##------------------------------------------------------------------
    ;## Local helper functions
    ;##------------------------------------------------------------------

    namespace eval util {
	proc compare {Current Previous Tables Dims} {
	    ;##----------------------------------------------------------
	    ;## Things needed from the caller
	    ;##----------------------------------------------------------
	    upvar $Current current
	    upvar $Previous previous

	    set retval [list]

	    ::QA::Debug::Puts 10 [array get previous]
	    set cur_version \
		[lindex [split [lindex [array names current] 0] ,] 1]
	    set prev_version \
		[lindex [split [lindex [array names previous] 0] ,] 1]

	    ::QA::Debug::Puts 10 "cur_version: $cur_version prev_version: $prev_version"
	    if {[info exists previous($::SITE,$prev_version,.date)]} {
		set d $previous($::SITE,$prev_version,.date)
	    } else {
		set d {Date unknown}
	    }
	    foreach table $Tables {
		foreach dim $Dims {
		    if { ! [info exists current($::SITE,$cur_version,$table,$dim)] \
			     || ! [info exists previous($::SITE,$prev_version,$table,$dim)] } {
			;##----------------------------------------------
			;## Ensure comparing apples to apples by only
			;##   using results common to both sets.
			;##----------------------------------------------
			continue
		    }
		    set c $current($::SITE,$cur_version,$table,$dim)
		    set p $previous($::SITE,$prev_version,$table,$dim)
				   
		    ;##--------------------------------------------------
		    ;## Element appears in both lists so it is fair
		    ;## to compare.
		    ;##--------------------------------------------------
		    lappend cur_rate [lindex $c 0]
		    lappend prev_rate [lindex $p 0]
		    
		    lappend cur_wall_time [lindex $c 1]
		    lappend prev_wall_time [lindex $p 1]
		}
	    }
	    ;##----------------------------------------------------------
	    ;## Generate report information
	    ;##----------------------------------------------------------
	    set o {align="right"}
	    lappend result {<table border="1">}
	    lappend result "<caption>Compared to $prev_version ($d)</caption>"
	    lappend result "<tr>"
	    lappend result "<th><th>New Avg<th>Old Avg<th>Analysis"
	    set cm [::QA::Math::Mean $cur_rate]
	    set pm [::QA::Math::Mean $prev_rate]
	    set a [expr ($cm / $pm) * 100]
	    if {$a > 100.} {
		set av [expr $a - 100.] 
		set am Faster
	    } else {
		set av [expr 100. - $a] 
		set am Slower
	    }
	    set s [format {%6.2f%% %s} $a $am]
	    set cm [format {%.6f} $cm]
	    set pm [format {%.6f} $pm]
	    lappend result "<tr>"
	    lappend result "<th>Overall Rate (MB/s)<td $o>$cm<td $o>$pm<td $o>$s"
	    lappend result "</tr>"
	    set cm [::QA::Math::Mean $cur_wall_time]
	    set pm [::QA::Math::Mean $prev_wall_time]
	    set a [expr ($cm / $pm) * 100]
	    if {$a > 100.} {
		set av [expr $a - 100.] 
		set am Slower
	    } else {
		set av [expr 100. - $a] 
		set am Faster
	    }
	    set cm [format {%.6f} $cm]
	    set pm [format {%.6f} $pm]
	    set s [format {%.2f%% %s} $a $am]
	    lappend result "<tr>"
	    lappend result "<th>Overall Wall Time (s)<td $o>$cm<td $o>$pm<td $o>$s"
	    lappend result "</tr>"
	    lappend result {</table>}
	    ::QA::Debug::Puts 10 "Mean(current): [::QA::Math::Mean $cur_rate]"
	    ::QA::Debug::Puts 10 "Mean(previous): [::QA::Math::Mean $prev_rate]"
	    ::tcltest::test \
		":${::QA::PROGRAM}:analysis:walltime:$cur_version vs. $prev_version:" \
		{} -body {
		    if {[string compare $am Slower] == 0 && $av > 10.} {
			return FAIL
		    }
		    return PASS
		} -match exact -result PASS
	    return [join $result "\n"]
	} ;## proc - compare

	proc extract {Start End Version JobList StatTable} {
	    upvar $StatTable results
	    upvar $JobList job_list

	    set var "[namespace parent]::Database"
	    set pat "putMetaData inserted.+[set $var].* at the rate of"

	    ;##==========================================================
	    ;## Insertion Rates Computation
	    ;##   This greps the logs for the period of time the test was
	    ;##   running and then generates a table of values to be used
	    ;##   for comparison with earlier runs.
	    ;##==========================================================
	    set raw [ grepLogs ${Start}-${End} $pat {metadata} ]
	    foreach line [split $raw '\n'] {
		::QA::Debug::Puts 20 "LINE: $line"
		if { [regexp -- {.*<b><i>([[:digit:][:alpha:]-]+)</i></b>.*\s+inserted\s+(\d+)\s+row.*into\s+.*\s+database table\s+(\S+)\s+at the rate of\s+(\S+)\s+rows/sec, took walltime\s+(\S+)\s+secs.*} $line => jobid dim table rate walltime] && \
			 [info exists job_list($jobid) ] } {
		    ::QA::Debug::Puts 10 "jobid: $jobid table: $table rate: $rate walltime: $walltime"
		    set results($::SITE,$Version,$table,$dim) \
			[list $rate $walltime]
		}
	    }
	}
    } ;## namespace util

    ;##==================================================================
    ;## Establishment of additional namespace variables
    ;##==================================================================

    set GenILwdTable [file join $::QA::LDAS_ROOT lib test genilwdtable]
    set TableDependency [file join $::QA::LDAS_ROOT lib test tabledependency]
    set ILwdValidate [file join $::QA::LDAS_ROOT lib test ilwdValidate.tcl]
    ;##------------------------------------------------------------------
    ;## Generate list of table names
    ;##------------------------------------------------------------------
    if [catch {set Tables [split [exec ${TableDependency}] '\n']} err] {
	puts $err
    }
    ;##------------------------------------------------------------------
    ;## Remove tables for which ilwds cannot be generated
    ;##------------------------------------------------------------------
    foreach table [list dbmdctest external_trigger ] {
	set i [lsearch $Tables $table]
	if { $i >= 0 } {
	    set Tables [lreplace $Tables $i $i]
	}
    }

    ;##==================================================================
    ;## For DEBUGGING purposes only
    ;##   When testing generic logic of the script, only use the first
    ;##   table as a representative model.
    ;##==================================================================
    if { 0 } {
	set Tables [lreplace $Tables 1 end]
    }
    
    proc DumpTable {Table Filename} {
	upvar $Table table
	if { ! [catch {set data_fid \
			   [open "[file join $::DATA_OUTPUT_DIR $Filename]" w] \
		       } err] } {
	    
	    foreach {key value} [array get table] {
		puts $data_fid "$key [list $value]"
	    }
	    close $data_fid
	}
    }

    proc LoadTable {Table Filename} {
	upvar $Table table
	if { ! [catch {set data_fid \
			   [open "[file join $::DATA_OUTPUT_DIR $Filename]" r] \
		       } err] } {
	    
	    while {[gets $data_fid line] >= 0} {
		if {[regsub -- {^(\S+)(.*)$} $line {set table(\1) \2} cmd]} {
		    eval $cmd
		}
	    }
	    close $data_fid
	}
    }

    proc Insertion { } {
	;##==============================================================
	;## Get things from the namespace
	;##==============================================================
	variable Database
	variable Dims
	variable GenILwdTable
	variable TableDependency
	variable Tables
	
	
	;##==============================================================
	;## Allow us to run commands the machine hosting the database
	;##==============================================================

	::QA::DB::DB2::Init
	::QA::ConnectToAgent

	;##==============================================================
	;## Open a stream to talk with database
	;##==============================================================

	if {[catch {set dbstream [::QA::DB::DB2::Open $Database]} err]} {
	    puts $err
	}
    
	;##==============================================================
	;## Loop over each dimension
	;##==============================================================

	array set joblist {}
	set start_time [ gpsTime now ]
	foreach dim $Dims {
	    ;##==========================================================
	    ;## Preperation Phase
	    ;##==========================================================
	    ;##----------------------------------------------------------
	    ;## Remove files
	    ;##----------------------------------------------------------
	    foreach table $Tables {
		file delete -force $::TMP/${table}${dim}.ilwd
	    }
	    ;##----------------------------------------------------------
	    ;## Clear and reset the database
	    ;##----------------------------------------------------------
	    set tmpname "$::QA::LDAS_OUTGOING_ROOT/tmp/dbperformance[pid].sql"
	    set fileId [open $tmpname w 0644]
	    for {set i [expr [llength $Tables] - 1]} {$i >= 0} {decr i } {
		set table [lindex $Tables $i]
		puts $fileId "echo deleting contents of $table;"
		puts $fileId "delete from $table;"
		puts $fileId "select count(*) from $table;"
	    }
	    close $fileId
	    ::QA::DB::PutsCmdFile $dbstream $tmpname
	    ::QA::Remote::WaitForDone $dbstream
	    ::QA::DB::PutsCmdFile $dbstream "$::DB2SCRIPTS/resetall.sql"
	    ::QA::Remote::WaitForDone $dbstream
	    file delete -force $tmpname ;## Remove command file

	    ;##==========================================================
	    ;## Insertion Phase
	    ;##==========================================================
	    ;##----------------------------------------------------------
	    ;## Create ilwd files
	    ;##----------------------------------------------------------

	    if [catch {exec $GenILwdTable $dim} err] {
		puts $err
	    }

	    foreach table $Tables {
		;##------------------------------------------------------
		;## Put into database
		;##------------------------------------------------------
		set result ".*Inserted $dim rows into table $table.*"
		test :${::QA::PROGRAM}:putMetaData:$table:$dim: {} -body {
		    set cmd [concat "putMetaData " \
				 " -database $Database " \
				 " -ingestdata file:$::TMP/${table}${dim}.ilwd"]

		    ::QA::Debug::Puts 10 $cmd
		    if {[catch {SubmitJob job $cmd} err]} {
			::QA::Debug::Puts 10 "JobId: $job(jobid): $err"
			return $err
		    }
		    ::QA::Debug::Puts 10 "JobId: $job(jobid)"
		    if { ! [ info exists ldas_version ] } {
			set ldas_version $job(LDASVersion)
		    }
		    set joblist($job(jobid)) $dim
		    set reply $job(jobReply)
		    set reply
		} -match regexp -result $result
		;##------------------------------------------------------
		;## Extract from database
		;##------------------------------------------------------
		set result ".*Your results:.*${table}${dim}.xml.*"

		test :${::QA::PROGRAM}:getMetaData:$table:$dim: {} -body {
		    set cmd [concat "getMetaData" \
				 " -returnprotocol http:/${table}${dim}" \
				 " -outputformat {LIGO_LW}" \
				 " -database $Database" \
				 " -sqlquery {select * from $table }" ]

		    ::QA::Debug::Puts 10 $cmd
		    if {[catch {SubmitJob job $cmd} err]} {
			return $err
		    }
		    ;##--------------------------------------------------
		    ;## Bring to local system for validation
		    ;##--------------------------------------------------
		    set reply $job(jobReply)
		    LJcopy [lindex $job(outputs) 0] $::TMP
		    set reply
		} -match regexp -result $result

	    } ;## foreach table
	    
	} ;# foreach dim
	set end_time [ gpsTime now ]
	::QA::DB::DB2::Conclude
	close $dbstream

	;##==============================================================
	;## Record when the test was done
	;##==============================================================
	set TableInfo($::SITE,$ldas_version,.date) \
	    [clock format [clock seconds] -format "%Y.%m.%d"]

	[namespace current]::util::extract $start_time $end_time \
	    $ldas_version joblist TableInfo
	;##--------------------------------------------------------------
	;## Write out the raw numbers
	;##--------------------------------------------------------------
	set filename "${::QA::PROGRAM}_${::SITE}_${ldas_version}.dat"
	DumpTable TableInfo $filename
    }

    proc Validation { } {
	;##==============================================================
	;## Get things from the namespace
	;##==============================================================
	variable Database
	variable Dims
	variable Tables
	variable UpperLimit
	variable ILwdValidate

	;##==============================================================
	;## Loop over each dimension
	;##==============================================================

	foreach dim $Dims {
	    ;##==========================================================
	    ;## Query and ilwd validation phase
	    ;##==========================================================
	    foreach table $Tables {
		if { $dim <= $UpperLimit(ilwdValidate) } {
		    ;##--------------------------------------------------
		    ;## Check the query results with the data that was put
		    ;##   into the database.
		    ;##--------------------------------------------------
		    set ilwd [file join $::TMP ${table}${dim}.ilwd]
		    set url [file join $::TMP ${table}${dim}.xml]
		    set result ".*$ilwd has been validated against $url.*"

		    test :${::QA::PROGRAM}:ilwdValidate:$table:$dim: {} -body {
			catch {exec ${ILwdValidate} $ilwd $url} err
			set err
		    } -match regexp -result $result
		}
	    } ;## foreach table
	    
	} ;# foreach dim
    }

    proc Statistics { } {
	;##--------------------------------------------------------------
	;## Find files that are needed for doing statistical analysis
	;##--------------------------------------------------------------
	set pattern "${::QA::PROGRAM}_${::SITE}_(\[\[:digit:\].\]+)\[.\]dat"
	set files [glob -directory $::DATA_OUTPUT_DIR "${::QA::PROGRAM}_${::SITE}_*.dat"]
	::QA::Debug::Puts 10 "files: $files"
	foreach file $files {
	    set filebase [lindex [file split $file] end]
	    ::QA::Debug::Puts 10 "filebase: $filebase"
	    if [regexp -- $pattern $filebase => version] {
		::QA::Debug::Puts 10 "version: $version"
		lappend versions $version
	    }
	}
	set versions [lsort -unique -decreasing -command ::QA::VersionCompare $versions]
	if {[llength $versions] <= 0} {
	    return
	}
	set current [lindex $versions 0]
	set versions [lreplace $versions 0 0]
	LoadTable current_stats "${::QA::PROGRAM}_${::SITE}_$current.dat"
	::QA::Debug::Puts 10 [array get current_stats]
	;##--------------------------------------------------------------
	;## Get list of tables and a list of dimentions
	;##--------------------------------------------------------------
	set tables [array names current_stats {*,*,*,*}]
	set dims [list]
	for {set x 0} {$x < [llength $tables]} {incr x} {
	    regexp -- {[^,]*,[^,]*,([^,]*),([^,]*)} \
		[lindex $tables $x] => t d
	    lappend dims $d
	    lset tables $x $t
	}
	set dims [lsort -integer -unique $dims]
	set tables [lsort -dictionary -unique $tables]
	::QA::Debug::Puts 10 "dims: $dims"
	::QA::Debug::Puts 10 "tables: $tables"
	;##--------------------------------------------------------------
	;## Loop over previous runs finding versions to compare to
	;##--------------------------------------------------------------
	set rel_count 0
	set results [list]
	::QA::Debug::Puts 10 "versions: $versions"
	foreach prev $versions {
	    ::QA::Debug::Puts 10 "Concidering: $prev"
	    if { [regexp -- {[.]0$} $prev] \
		     && $rel_count < 3 } {
		;##------------------------------------------------------
		;## Matching against a release
		;##------------------------------------------------------
		::QA::Debug::Puts 10 "Matching: $current against $prev"
		LoadTable prev_stats "${::QA::PROGRAM}_${::SITE}_$prev.dat"
		set t [::QA::${::QA::PROGRAM}::test::util::compare \
			   current_stats prev_stats $tables $dims]
		::QA::Debug::Puts 10 $t
		lappend results $t
		unset prev_stats
		incr rel_count
	    }
	}
	;##--------------------------------------------------------------
	;## Write out the results into an html files that is ready
	;##   for inclusion into the Weekly Stats report.
	;##--------------------------------------------------------------
	set filename [file join $::tcltest::temporaryDirectory ${::QA::PROGRAM}_stats.html]
	if {[catch {set fid [open $filename w]} err]} {
	    ::QA::Debug::Puts 0 "ERROR: Cannot open $filename"
	} else {
	    puts $fid "<font size=\"-1\">"
	    foreach r $results {
		puts $fid $r
	    }
	    puts $fid "</font>"
	    close $fid
	}
	
    }

    proc Convert { } {
	variable FileConversionSpec
	set option_list [list date version site]
	foreach convert $FileConversionSpec {
	    if [info exists TableInfo] {
		unset TableInfo
	    }

	    foreach var $option_list {
		if [info exists $var] {
		    unset $var
		}
	    }

	    ::QA::Debug::Puts 15 $convert
	    set flags [split $convert :]
	    foreach opt $option_list {
		set i [lsearch -glob $flags "${opt}=*"]
		if { $i != -1 } {
		    ::QA::Debug::Puts 20 "Doing option: $opt: [lindex $flags $i]"
		    set pattern "${opt}="
		    regsub -- $pattern [lindex $flags $i] {} $opt
		    set flags [lreplace $flags $i $i]
		}
	    }

	    ;##----------------------------------------------------------
	    ;## Make sure all the information that is needed has been
	    ;##   supplied
	    ;##----------------------------------------------------------
	    set ready 1
	    foreach opt $option_list {
		if { ! [info exists $opt] } {
		    set ready 0
		    break;
		}
	    }
	    if { [llength $flags] != 1 } {
		set ready 0
	    }
	    if { ! $ready } {
		;##------------------------------------------------------
		;## Do not have enough information to do conversion
		;##------------------------------------------------------
		continue
	    }
	    set file [lindex $flags 0]
	    ::QA::Debug::Puts 10 "::${::QA::PROGRAM}::Convert: $file $date $version"
	    ;##----------------------------------------------------------
	    ;## Read the data file
	    ;##----------------------------------------------------------
	    set table_names [list]
	    set all_dims [list]

	    if { ! [catch {set in_fid [open "$file" r] } err] } {
		while {[gets $in_fid line] >= 0} {
		    set line [string trim $line]
		    ::QA::Debug::Puts 10 $line
		    if [regsub -- {The\s+(rate|walltime|cputime)\s+} $line {\1 } line] {
			::QA::Debug::Puts 10 "$line"
			set sline [split $line]
			set elements [llength $sline]
			set mode [lindex $sline 0]
			set dims [lreplace $sline 0 0]
			::QA::Debug::Puts 10 "mode: $mode dims: $dims elements: $elements"
			set all_dims [concat $all_dims $dims]
			continue
		    }
		    set sline [split $line]
		    ::QA::Debug::Puts 10 "llength: [llength $sline] sline: $sline"
		    if { [llength $sline] == $elements } {
			set table [lindex $sline 0]
			lappend table_names $table
			set values [lreplace $sline 0 0]
			::QA::Debug::Puts 10 "table: $table values: $values"
			for {set i 0} {$i < [llength $dims]} {incr i} {
			    set tmp_table($table,$mode,[lindex $dims $i]) \
				[lindex $values $i]
			}
		    }
		}
		close $in_fid
		::QA::Debug::Puts 20 [array get tmp_table]
		;##------------------------------------------------------
		;## Create the actual table to dump
		;##------------------------------------------------------
		set table_names [lsort -unique -ascii $table_names]
		set all_dims [lsort -unique -integer $all_dims]
		if {[info exists date]} {
		    set dump_table($site,$version,.date) $date
		}
		foreach table $table_names {
		    foreach dim $all_dims {
			if [info exists tmp_table($table,rate,$dim) ] {
			    set rate $tmp_table($table,rate,$dim)
			} else {
			    set rate [list]
			}
			if [info exists tmp_table($table,walltime,$dim) ] {
			    set walltime $tmp_table($table,walltime,$dim)
			} else {
			    set walltime [list]
			}
			set dump_table($site,$version,$table,$dim) \
			    [list $rate $walltime]
		    }
		}
		::QA::Debug::Puts 10 [array get dump_table]
	    } else {
		::QA::Debug::Puts 0 $err
	    }
			   
	    ;##----------------------------------------------------------
	    ;## Output the information
	    ;##----------------------------------------------------------
	    set filename "${::QA::PROGRAM}_${site}_${version}.dat"
	    DumpTable dump_table $filename
	}
    }

    array set TableInfo {}

    if { [lsearch $Modes convert] != -1 } {
	Convert
    }
    if { [lsearch $Modes test] != -1 } {
	Insertion
    }
    if { [lsearch $Modes validate] != -1 } {
	Validation
    }
    if { [lsearch $Modes statistics] != -1 } {
	Statistics
    }


    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================

    cleanupTests
    
} ;## namespace ::QA::dbperformance::test

namespace delete ::QA::dbperformance::test
