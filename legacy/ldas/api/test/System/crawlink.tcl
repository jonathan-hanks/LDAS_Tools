#!/bin/sh
# -*- mode: tcl -*- \
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
# crawlink.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*ndas*"
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::crawlink::test {
    array set status  {}
    set baseurl ""
    set verbose 0
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--baseurl} {string} \
    {top level url to start} \
    { set ::QA::crawlink::test::baseurl $::QA::Options::Value }

::QA::Options::Add {} {--crawlink-verbose} {string} \
    {output more messages} \
    { set ::QA::crawlink::test::verbose $::QA::Options::Value }	  
    
 ::QA::Options::Add {} {--resultdir} {string} \
    {directory of crawlink results to diff} \
    { set ::QA::crawlink::test::resultdir $::QA::Options::Value }	      
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::crawlink::test {	

    	##-------------------------------------------------------------------
    	## Bring commonly used items into the local namespace
    	##-------------------------------------------------------------------
    	namespace import ::tcltest::*
    	namespace import ::QA::Debug::*
        namespace import ::QA::Debug::Puts
    	namespace import ::QA::TclTest::SubmitJob
    	namespace import ::QA::URL::List
        
proc myName {args} {
    if {[info level] < 2} {return ::}

    set level -1
    set cmd [lindex [info level $level] 0]
    set nsp [uplevel 1 {namespace current}]

    return [string trimleft [namespace eval $nsp namespace origin $cmd] ":"]
}

proc vputs {msg1 {msg2 ""} args} {
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*
    
    if { [ set ${name}::verbose ] > 0} {
        Puts 0 $msg1
    } elseif {[string length $msg2]} {
        Puts 0 $msg2
    }

    return
}

proc compactPath {path} {
    set endslash ""
    regexp {(\/)$} $path -> endslash

    set parts [file split [string trim $path "/"]]
    set full [list]

    foreach part $parts {
        switch -exact -- $part {
            . {continue}
            .. {set full [lreplace $full end end]}
            default {lappend full $part }
        }
    }

    set full [join $full "/"]
    return "${full}${endslash}"
}

proc fullURL {link root} {
    ;## check for protocol
    if {! [regexp {^([^:]+):[^:]+} $link]} {
        foreach {protocol server path document} [splitURL $root] {break}

        ;## this is a link on the same server:
        if {[regexp {^\/} $link]} {
            ;## from root
            set link "${protocol}://${server}${link}"
        } else {
            ;## relative to the scanned page's dir
            set link "${protocol}://${server}/[compactPath ${path}/${link}]"
        }
    }

    return $link
}

proc splitURL {inurl} {
    set rx1 {^([^:]+):\/{1,2}([^\/]+)\/(.+)\/(.*)}
    set rx2 {^([^:]+):\/{1,2}([^\/]+)\/(.+)}
    set rx3 {^([^:]+):\/{1,2}(.+)}

    set protocol ""
    set server ""
    set path ""
    set document ""

    if {[regexp -- $rx1 $inurl -> protocol server path document]} {
        ;##
    } elseif {[regexp -- $rx2 $inurl -> protocol server document]} {
        ;##
    } elseif {[regexp -- $rx3 $inurl -> protocol server]} {
        ;##
    } else {
        return -code error "[myName]: Error parsing URL: $inurl"
    }

    foreach var {server path document} {
        set $var [eval string trim \$$var "/"]
    }

    return [list $protocol $server $path $document]
}

proc getPage_old {geturl} {
    set indoc ""
    set code 200
    set type "text/plain"
    set pagemoved 0

    if {[catch {open "|$::linkcheck $geturl" r} HEADGET]} {
        return -code error "[myName]: Error getting page $geturl: $HEADGET"
    }

    while {[gets $HEADGET line] != -1} {
        #puts stderr $line
        if {[regexp -- {^HTTP[^ ]+ (\d+)} $line -> code]} {
            if {[string index $code 0] == 3} {
                set pagemoved 1
            }
        } elseif {[regexp -- {^Content-Type: ([\/a-zA-Z]+)} $line -> type]} {
            ;##
        } elseif {$pagemoved && [regexp -- {^Location: (.*)$} $line -> geturl]} {
            set pagemoved 0
        }
    }
    catch {close $HEADGET}

    if {$pagemoved} {
        return -code error "[myName]: Page has moved to unknown location."
    }

    if {[string equal "text/html" $type]} {
        if {[catch {open "|[ set ::${name}$::htmlget ] $geturl" r} WEBGET]} {
            return -code error "[myName]: Error getting page $geturl: $WEBGET"
        }
        set indoc [read -nonewline $WEBGET]
        catch {close $WEBGET}

        ;## Look for redirect
        if {[regexp -nocase -- {<meta.+refresh.+url *= *([^\"\'>]+)} $indoc -> redir]} {
            set redir [fullURL $redir $geturl]
            return [getPage $redir]
        }
    }

    return [list $geturl $indoc $code $type]
}

proc getPage {url} {
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*
    
    if {[catch {eval exec [ set ::${name}::htmlget ] -w \"\n%{url_effective}\" \"$url\"} data]} {
        return -code error "[myName]: Error getting page $url: $data"
    }

    ;## Look for redirect
    #if {[regexp -nocase -- {<meta.+refresh.+url *= *([^\"\'>]+)} $data -> redir]} {
    #    return [getPage [fullURL $redir $url]]
    #}

    ;## get effective url, as curl follows redirects
    regexp {\n([^\n]+)$} $data -> url

    return [list $url $data]
}

proc linkWorks {link} {
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*
    
    regexp {^([^:]+):[^:]+} $link -> proto
    if {! [regexp -nocase -- {http|ftp} $proto]} {
        return "SKIP"
    }

    if {[catch {eval exec [ set ::${name}::linkcheck ] \"$link\"} data]} {
        return -code error "$data"
    }

    set result "BAD"
    set code "---"
    set type "text/plain"
    set pagemoved 0

    foreach line [split $data "\n"] {
        if {! [string length $line]} {
            break
        }

        if {[regexp -- {^HTTP[^ ]+ (\d\d\d)} $line -> code]} {
            if {$code >= 400} {
                break
            } elseif {$code >= 300} {
                set pagemoved 1
            } else {
                set result "GOOD"
            }
        } elseif {$pagemoved && [regexp -- {^Location: (.+)$} $line -> redir]} {
            return [linkWorks [fullURL $redir $link]]
        } elseif {[regexp -- {^Content-Type: (.+)$} $line -> type]} {
            ;##
        } elseif {[string equal -nocase "ftp" $proto] && [regexp -- {^Content-Length: \d+$} $line]} {
            set result "GOOD"
        }
    }

    return [list $result $code $type]
}

proc getLinks {indoc} {
    set start 0
    set result [list]
    array set found {}

    regsub -all -- {<!--.*?-->} $indoc {} indoc

    while {[regexp -indices -start $start -- {[^<]*(<[^>]+>)} $indoc -> tagi]} {
        set tag [string range $indoc [lindex $tagi 0] [lindex $tagi 1]]
        set start [expr {[lindex $tagi 1] + 1}]

        if {[regexp -- {<!--} $tag]} {
            # this is a comment tag, ignore it
            continue
        }

        if {[regexp -nocase -- {(src|href|background|archive|url) *= *([\"\'][^\"\']*[\"\']|[^\s\)>]*)} $tag -> attr url]} {
            # remove surrounding quotes
            set url [string trim $url "\"\'"]

            regsub {([^\#]*)\#.*} $url {\1} url
            #regsub {([^\?]*)\?.*} $url {\1} url

            if {! [string length $url]} {
                # link was nothing more than an anchor link (#)
                continue
            }

            if {[info exists found($url)]} {
                incr found($url)
                continue
            }

            set found($url) 1
            lappend result $url

            #if {[regexp -- {< *([^ ]+)} $tag -> type]} {
            #    set ::tagtype($url) $type
            #}
        }
    }

    return [array get found]
}

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## crawk each link 2 levels (default) down
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

proc crawl {url} {
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*
    
    set fd [ set ::${name}::outfd ]
    
    if {! [string length $url]} {
        return
    }
    if  { [ regexp {crawlink\d+.html} $url ] } {
        return
    }
    if {[catch {splitURL $url} err]} {
        vputs "\nERROR: $err"
        return
    }
    if {[catch {getPage $url} data]} {
        vputs "\nERROR: $data"
        return
    }
    foreach {url indoc} $data {break}
    set ::${name}::rooturl($url) 0
    vputs "\nROOT $url"

    set showroot 1
    set candidates [list]

    foreach {link found} [getLinks $indoc] {
        set full [fullURL $link $url]
        
        if {! [string equal $link $full] && [ set ::${name}::external ] } {
            ;## internal link
            continue
        }
        if {! [info exists ::status($full)]} {
            if {[catch {linkWorks $full} data]} {
                vputs "ERROR: $data"
                set data [list BAD err "text/plain"]
            }
            set ::status($full) $data
        }
        foreach {success code type} $::status($full) {break}

        if {[string equal "SKIP" $success]} {
            ;## Unsupported protocol
            vputs " SKIP $link"
            continue
        }
        vputs " $success $found $code $link"
        if { [string equal "BAD" $success] \
		&& ! [ regexp {ldas-sw.+tar.gz} $link ] } {
	    ;##----------------------------------------------------------
	    ;## Logging of bad links
	    ;##----------------------------------------------------------
            incr ::${name}::badlinks

            if  { ![ string length [ set ::${name}::Links ] ] } {
                append ::${name}::Links "<ul>\n"
            } 

            if  { ![ set ::${name}::verbose ] } {
                if {$showroot} {
                    puts $fd "\nROOT: $url"
                    flush $fd
                    if  { ![ string equal "<ul>\n" [ set ::${name}::Links ] ] } {
                        append ::${name}::Links "</li><br>\n"
                    }
                    append ::${name}::Links "<li>ROOT: <a href=\"$url\">$url</a><br>\n"
                    set showroot 0
                }
		puts $fd " BAD $found error $code $link"
		flush $fd
		append ::${name}::Links " BAD $found error $code <a href=\"$link\">$link</a><br>\n"
            }
            if { [ set ::${name}::linenumber ] } {
                set linenum 1
                regsub -all {[\+\*\.\?\{\}\(\)\[\]\:\^\$\-\\]} $link {\\&} link
                foreach line [split $indoc "\n"] {
                    set rx {(src|href|background|archive|url) *= *[\"\']?$link}
                    if {[regexp -nocase -- [subst -nobackslashes -nocommands $rx] $line]} {
                        puts $fd "  line $linenum"
                        flush $fd
                        append ::${name}::Links "  line $linenum"
                    }
                    incr linenum
                }
            }
            continue
        }

        if {! [regexp -nocase -- {text/html} $type]} {
            continue
        }
        if {[excluded $full]} {
            #vputs " SKIP $link"
            continue
        }

        lappend candidates $full
    }

    foreach root $candidates {
        if {[info exists ::${name}::rooturl($root)]} {
            continue
        }

        set ::${name}::rooturl($root) 0
        crawl $root
    }

    return
}

proc excluded {url} {
    set result 0
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*

    if {! [regexp "^[ set ::${name}::baseurl ]" $url]} {
        set result 1
    } else {
        foreach rx [ set ::${name}::excludeList ] {
            if {[regexp "$rx" $url]} {
                set result 2
                break
            }
        }
    }

    return $result
}

proc outputHtml {url} {
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*
    
    set hdr "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n\
    <html>\n\
  <head>\n\
    <title>Broken Link Report for $url</title>\n\
  </head>\n\
  <body>\n\
    <h1>Broken Link Report for $url</h1>"
  
    set fd [ open [ set ::${name}::outhtml ] w ]
    puts $fd $hdr
   ;## output stats as table
   
   puts $fd "<table>\n\
      <tbody>\n\
        <tr>\n\
          <td>Pages checked:</td>\n\
          <td align=right bgcolor='#ecea7c' width=10>[llength [array names ::${name}::rooturl]]</td>\n\
        </tr>\n\
        <tr>\n\
          <td>Links checked:</td>\n\
          <td align=right bgcolor='#ecea7c' width=10>[llength [array names ::${name}::status]]</td>\n\
        </tr>\n\
        <tr>\n\
          <td>Bad links:</td>\n\
          <td align=right bgcolor='#fe781a' width=10>[ set ::${name}::badlinks]</td>\n\
        </tr>\n\
      </tbody>\n\
    </table>"
  
    puts $fd "<p>[ set ::${name}::Links]\n</li></ul>\n"
    set tail "</body>\n\</html>"
    puts $fd $tail
    close $fd
}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## main
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

set date [ clock format [ clock seconds ] -format "%m%d" ]
set outhtml [ file join $::tcltest::temporaryDirectory crawlink$date.html ]
       
set thisoutfile [file join $::tcltest::temporaryDirectory crawlink.${date} ]
set outfd [ open $thisoutfile w ]
Puts 0 "output file $thisoutfile"

set now [clock format [clock seconds] -format {%a %b %e %T %Z %Y}]
         
set url ""
set linenumber 0
set external 0
set badlinks 0
set Links "Updated $now\n"

set excludeList {
    /ldas_outgoing/jobs
    /ldas_outgoing/logs
    /ldas_outgoing/nightly_testing
    /doc/testing/system_results
    xenu
}

;## relevant curl options
;## -s silent
;## -S show errors
;## -N no buffering
;## -m maxtime
;## -r range of bytes
;## -L follow redirects
;## -I header only
;## -i include header
;## -k insecure connection
;## --connect-timeout
;## --max-filesize

# ok to run curl on linux
#set curlbase [list /ldcg/bin/curl -L -s -S -N -m 60 --connect-timeout 30]
set curlbase [list [ auto_execok curl ] -L -s -S -N -m 60 --connect-timeout 30]

;## If curl version >= 7.10, add -k option
;## to turn off certificate checking
catch {exec curl -V} curlver
regexp {^curl (\d+)\.(\d+).(\d+)} $curlver -> maj min sub
if {"${maj}.${min}" >= 7.10} {
    lappend curlbase -k
}

set htmlget [concat $curlbase -r 0-1048576]
set linkcheck [concat $curlbase -I]
set linkcheckfull [concat $curlbase -i]

if {! [string length $baseurl]} {
   return -code error  "Error: Missing URL."
}

array set status  {}
set url $baseurl
set rooturl($url) 0
crawl $url

vputs ""
vputs "pages scanned  : [llength [array names ::rooturl]]"
vputs "links checked  : [llength [array names ::status]]"
vputs "bad links found: $badlinks"

outputHtml $url

;## optional diff
set outputfiles {}
   
;## expect output files in /ldas_outgoing/logs
;## add code to trim the list down to a max of 3 history

catch { set outputfiles [ lsort -decreasing -dictionary [ glob -nocomplain $resultdir/crawlink.* ] ] }
Puts 1 "outputfiles $outputfiles"

if  { [ llength $outputfiles ] } {
    set max_mtime 0
    foreach file $outputfiles {
   	    set mtime [ file mtime $file ]
        if  { $mtime > $max_mtime } {
    	    if  { ![ regexp {crawlink\.(\d+)} $file -> rundate ] } {
                set rundate [ clock format [ file mtime $file ] -format "%m%d" ]                
            }
            if	{ ! [ string equal $rundate $date ] } {
            	set max_mtime $mtime
       	    }
        }
    }

    set diffout [ file join $::tcltest::temporaryDirectory crawlink_diff.${rundate}_$date ]
    set diffout1 [ file join $::tcltest::temporaryDirectory crawlink_diff1.${rundate}_$date ]
    
    Puts 1 "diffing $file vs $thisoutfile, results in $diffout, $diffout1 (removed tarball diffs)"

    catch { exec diff $file $thisoutfile >& $diffout } err
    if  { [ file exist $diffout ] } {
        set fd [ open $diffout1 w ]
        puts $fd "$now diff $file vs $thisoutfile  (removed tarball diffs)"
        close $fd
        catch { exec grep -v tar $diffout >>& $diffout1 } 
    }
    file rename -force  $thisoutfile ${::QA::LDAS_OUTGOING_ROOT}/logs/crawlink.log
    file rename -force  $outhtml ${::QA::LDAS_OUTGOING_ROOT}/logs/crawlink.html
    file rename -force  $diffout1 ${::QA::LDAS_OUTGOING_ROOT}/logs/crawlink.diff
} else {
    Puts 0 "no previous results to diff"
    file rename -force  $thisoutfile ${::QA::LDAS_OUTGOING_ROOT}/logs/crawlink.log
    file rename -force  $outhtml ${::QA::LDAS_OUTGOING_ROOT}/logs/crawlink.html
}
    close $outfd 
	flush [outputChannel]
    
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::crawlink::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::crawlink::test
