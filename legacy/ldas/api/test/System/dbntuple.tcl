#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::dbntuple::test {}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::dbntuple::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	
	set TYPE DT_512
	set TIMES "610000000-610000511"

	set user ldas
	set index 0
	set dbname ldas_tst
	
	set cmdInit {
    dataPipeline
    -dynlib libldasinspiral.so
    -filterparams (262144,7,131072,40.0,0,69.0,8,0.9,1,5.0,20.0,0,1,0,1,1,(4.0,4.0))
    -datacondtarget wrapper
    -database $dbname
    -multidimdatatarget ligolw
    -responsefiles {
        file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
    }
    -aliases {gw=P2\\:LSC-AS_Q::AdcData;}
    -algorithms {
        rgw = resample(gw, 1, 8);
        output(rgw,_,_,,resampled gw timeseries);
        p = psd( rgw, 262144 );
        output(p,_,metadata,,spectrum data);
        output(p,_,_,,spectrum data);
    }
    -framequery {
        {$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
    }
	}
	
	set cmdstem {
	    dataPipeline
        -subject {$subject}
        -dynlib {$dynlib}
        -filterparams (0)
        -dbntuple { {$sql} $pushpass ntuple }
        -datacondtarget $dctarget
        -multidimdatatarget ligolw
        -database $dbname
        #-responsefiles {
        #    file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
        #}
        -aliases {gw=P2\\:LSC-AS_Q::AdcData;}
        -algorithms {$algo}
        -framequery {
            {$TYPE} {} {} {$TIMES} {Adc(P2:LSC-AS_Q)}
        }
	}
	
	if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/cntlmonAPI/cntlmon.state ]  } {
		source ${::QA::LDAS_OUTGOING_ROOT}/cntlmonAPI/cntlmon.state
    } else {
		catch { exec cat /etc/ldasname } RUNCODE
		set RUNCODE [ string toupper $RUNCODE ]
	}
		
	proc runTest {} {
		uplevel {
			set cmd [ subst $cmdstem ]
			Puts 1 "cmd $cmd"
			test dbntuple:dataPipeline:$index:$subject:frameV4  {} -body {
				if 	{[catch {SubmitJob job $cmd} err]} {
                    Puts 1 "Error: $err"
					LJdelete job
	    			return $err
				}
				set reply $job(jobReply)
				LJdelete job
				Puts 1 "got $reply\nexpected $expected"
				return $reply
    		} -match regexp -result $expected
		}
	}
      
    set index 0
    set jobid none
	if 	{ [catch {
		set cmd [ subst $cmdInit ]
        set subject {DBNTUPLE:INIT:wrapper}
        test dbntuple:dataPipeline:$index:$subject  {} -body {
            if  { [ catch {SubmitJob job $cmd} err]} {
                Puts 1 "Error $err"
			    LJdelete job
	    	    return $err
		    }		              
		    set jobid [string trim $job(jobid) ${RUNCODE}]
		    Puts 0 "$job(jobid) ($job(LDASVersion)) INIT$index"	
        }
	} err]} {
		LJdelete job
        set jobid none
		Puts 0 "Fail to initialize: $err, cannot run wrapper jobs"
	}
    Puts 0 "jobid $jobid"
    
    
	set dynlib libldastrivial.so
	set sql {select impulse_time, mchirp, sigmasq, search, ifo from sngl_inspiral fetch first 1 rows only}

	set index 1
	;## 1 - good types, push, wrapper
	set subject {DBNTUPLE:SIMPLE:PUSH:wrapper}
	set expected "produced no products"
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(ntuple,_,_,,ntuple query);
	}
	runTest

	;## 2 - good types, pass, wrapper
	set subject {DBNTUPLE:SIMPLE:PASS:wrapper}
	set expected "produced no products"
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	runTest 
	
	set dynlib {}
	
	;## 3 - good types, push, ligolw
	set subject {DBNTUPLE:SIMPLE:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
	set expected "No MPI processing will occur"

    set algo {
        output(ntuple,_,_,,ntuple query);
    }
	runTest
	
	;## 4 - good types, pass, ligolw
	set subject {DBNTUPLE:SIMPLE:PASS:LIGOLW}
	set expected "No MPI processing will occur"
    set pushpass pass
    set dctarget ligolw
	set dynlib {}
    set algo {}
	runTest

	set dynlib libldastrivial.so
	set sql {select impulse_time, mchirp, sigmasq, search, ifo from sngl_inspiral where search='_not_there_' fetch first 1 rows only}
	set index 2
	
	;## 5 - empty data, push, wrapper
	set subject {DBNTUPLE:EMPTY:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
	set expected "produced no products"
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(ntuple,_,_,,ntuple query);
    }

	;## 6 - empty data, pass, wrapper
	set subject {DBNTUPLE:EMPTY:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
	set expected "produced no products"
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	runTest 

	set dynlib {}
	;## 7 - empty data, push, ligolw
	set subject {DBNTUPLE:EMPTY:PUSH:LIGOLW}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(ntuple,_,_,,ntuple query);
    }
	set expected "can be found at:"
	runTest 

	;## 8 - empty data, pass, ligolw
	set subject {DBNTUPLE:EMPTY:PASS:LIGOLW}
    set pushpass pass
    set dctarget ligolw
	set expected "can be found at:"
    set algo {}
	runTest 
    
    set subject {DBNTUPLE:BINARY:PUSH:wrapper}
	set dynlib libldastrivial.so
    set sql "select * from summ_spectrum where ((process_id, creator_db) in (select process_id, creator_db from process where (jobid = $jobid and program = 'datacondAPI'))) fetch first 1 rows only"
	set index 3
	
	;## 9 - binary data, push, wrapper
    set pushpass push
    set dctarget wrapper
	set expected "table contains columns that are of non-simple types"
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(ntuple,_,_,,ntuple query);
    }   
    runTest

	;## 10 - binary data, pass, wrapper
	set subject {DBNTUPLE:BINARY:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "table contains columns that are of non-simple types"
	runTest
	
    ;## this test requires initialization with wrapper
	set dynlib {}
	;## 11 - binary data, push, ligolw
	set subject {DBNTUPLE:BINARY:PUSH:LIGOLW:wrapper}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(ntuple,_,_,,ntuple query);
    }
	set expected "table contains columns that are of non-simple types"
	runTest

	;## 12 - binary data, pass, ligolw
	set subject {DBNTUPLE:BINARY:PASS:LIGOLW:wrapper}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "table contains columns that are of non-simple types"
	runTest
    
	set dynlib libldastrivial.so
	set sql "select ifo, case end_time when 610000119 then null when 610000143 then null else end_time end as end_time2, impulse_time from sngl_inspiral where ((process_id, creator_db) in (select process_id, creator_db from process where (jobid = $jobid))) order by end_time fetch first 5 rows only"
	set index 4
	
	;## 13 - bad data, push, wrapper
	set subject {DBNTUPLE:BAD:PUSH:wrapper}
    set pushpass push
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
        output(ntuple,_,_,,ntuple query);
    }
	set expected "field was discovered to be null in a query result"
    runTest

	;## 14 - bad data, pass, wrapper
	set subject {DBNTUPLE:BAD:PASS:wrapper}
    set pushpass pass
    set dctarget wrapper
    set algo {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);
    }
	set expected "field was discovered to be null in a query result"
	runTest 
	
	set dynlib {}
	;## 15 - bad data, push, ligolw
	set subject {DBNTUPLE:BAD:PUSH:LIGOLW:wrapper}
    set pushpass push
    set dctarget ligolw
    set algo {
        output(ntuple,_,_,,ntuple query);
    }
	set expected "field was discovered to be null in a query result"
	runTest 

	;## 16 - bad data, pass, ligolw
	set subject {DBNTUPLE:BAD:PASS:LIGOLW:wrapper}
    set pushpass pass
    set dctarget ligolw
    set algo {}
	set expected "field was discovered to be null in a query result"
	runTest 
	
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests

} ;## namespace - ::QA::dbntuple::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::dbntuple::test	
