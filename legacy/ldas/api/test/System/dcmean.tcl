#!/bin/sh
# -*- mode: TCL; -*- \
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::dcmean::test {
} ;## namespace ::QA::dcmean::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::dcmean::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## Raw frame
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    set cmd {
	conditionData
	-returnprotocol { http://mean.ilwd }
	-outputformat { ilwd ascii }
	-framequery { R H {} 730524000-730524063 Adc(H1:LSC-AS_Q) }
	-aliases { asq = LSC-AS_Q; }
	-algorithms {
	    m = mean(asq);
	    output(m,_,_,m,mean of asq);
	}
    }

    test :[file tail $argv0]:conditionData:raw: {} -body {
	if {[catch {SubmitJob job $cmd} err]} {
	    return $err
	}
	set result [LJread [lindex $job(outputs) 0]]
	set result
    } -match glob -result {*>-2.2270608572370152e-03<*}

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## RDS frame
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    set cmd {
	conditionData
	-returnprotocol { http://mean.ilwd }
	-outputformat { ilwd ascii }
	-framequery { RDS_R_L1 H {} 730524000-730524063 Adc(H1:LSC-AS_Q) }
	-aliases { asq = LSC-AS_Q; }
	-algorithms {
	    m = mean(asq);
	    output(m,_,_,m,mean of asq);
	}
    }

    test :[file tail $argv0]:conditionData:RDS: {} -body {
	if {[catch {SubmitJob job $cmd} err]} {
	    return $err
	}
	set result [LJread [lindex $job(outputs) 0]]
	set result
    } -match glob -result {*>-2.2270608572370152e-03<*}

    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
    
} ;## namespace ::QA::dcmean::test

namespace delete ::QA::dcmean::test
