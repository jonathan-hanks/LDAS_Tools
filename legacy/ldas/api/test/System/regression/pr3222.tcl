#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr3222::test {
    set ValidateMode 1
} ;## namespace ::QA::pr3222::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--validateMode} {string} \
    {dataPipeline or datacond} \
    { if    { [ string equal datacond $::QA::Options::Value ] } { \
            set ::QA::pr3222::test::ValidateMode 2 \
      } else { \
            set ::QA::pr3222::test::ValidateMode 1 \
      } }

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr3222::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob

    #====================================================================
    # Execute a command 
    # specify md5sum substitutions
    #====================================================================
    		set gapOptions { 0 1 }
            set startgps 693960000
            set endgps 693960032
    		set cmd1 {cacheGetFilenames -times $startgps-$endgps -ifos H -types R_GAP_TEST -channels { Adc() } -allowgaps $gap }
            set cmd2 {cacheGetFilenames -framequery {R_GAP_TEST H {} {693960000-693960032} Adc() } -allowgaps $gap } 
            set styles { times framequery }
		    set resultOK \
            "/scratch/test/frames/gap_test16/H-R_GAP_TEST-$startgps-16.gwf.+/scratch/test/frames/gap_test16/H-R_GAP_TEST-$endgps-16.gwf"
    		set resultERR "$resultOK.+Gaps are detected: [ expr $startgps + 16 ] $endgps"
       
            foreach gap $gapOptions {
          	    if	{ $gap } {
                  	set expected $resultOK
                } else {
                   	set expected $resultERR
              	}
                set index 0
          	    foreach cmd [ list $cmd1 $cmd2 ] {
               	    set cmd [ subst $cmd ]
                    test regression:pr3222:cacheGetFilenames:[ lindex $styles $index ]:gap=$gap {} -body {
                        if 	{ [catch {
                            SubmitJob job $cmd
                        } err] } {
                            if  { [ info exist job(error) ] } {
                                set joberror $job(error)
                            }
				            LJdelete job
	    		            return -code error $err
                        }
                        if  { [ info exist job(jobReply) ] } {
                            set reply $job(jobReply)
                        } else {
                            set reply none
                        }
                        set jobid $job(jobid)
                        LJdelete job
                        return $reply
                    } -match regexp -result $expected
                    incr index 1
                }      
            }
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr3222::test

namespace delete ::QA::pr3222::test
