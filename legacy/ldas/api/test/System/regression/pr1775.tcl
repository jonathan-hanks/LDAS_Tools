#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1775::test {

#========================================================================
# The start time given below is for S2 data
#========================================================================
    set Start 730000048
    set Options [list -framechecksum 0 -filechecksum 0 -metadatacheck 0]

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
    set Dt 32
    set Finish [ expr $Start + $Dt - 1]

    set cmdstem { createRDS
    -subject { pr1775 - Instance counts not reset after FrEndFrame }
    -times { ${Start}-${Finish} }
    -type { R }
    -channels { H1:LSC-AS_Q }
    -compressiontype { raw }
    [join $Options]
    }
}   

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

namespace eval ::QA::pr1775::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    if  { [ catch {
        set cmd [ subst -nobackslashes $cmdstem ]
        set ldascmd [ lindex $cmd 0 ] 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        set expected "Your results:.+gwf"
        
        test regression:$thispr:$ldascmd:Start=$Start:DeltaT=$Dt:Finish=$Finish:submit {} -body {
            if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    set reply $Job(jobReply)
            } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
            }
            return $reply
        } -match regexp -result $expected

#========================================================================
# Read the output and make sur that it looks correct
#------------------------------------------------------------------------

        if  { [ info exist Job ] } {
            regexp {^([^:]*):(.*)$} $Job(manager) -> host port
            test regression:$thispr:$ldascmd:Start=$Start:DeltaT=$Dt:Finish=$Finish:validate {} -body {
                set errors ""
                foreach frame $Job(outputs) {
                    regexp {^.*/([^/]*)$} $frame -> base_name
                    if  { [ regexp {^/} $frame ] } {
	# Add http protocol so LJcopy will be able to
	#   retrieve the file
	                    set frame "http://${host}${frame}"
                    }
                    LJcopy $frame $base_name
                    set output [ exec $::FRDUMP -s $base_name ]
                    set object ""
                    foreach line [ split $output "\n" ] {
	                    if  { [ regexp {(FrEndOf\w*).*Instance:\s+(\d+)\s+} $line -> object instance ] } {
	                        if  { ! [ info exists instance ] } {
                                append errors "Object: $object Instance: unknown\n"
                            }
                        }
                    }
                }
            } -result {}
	    }
    } err ] } {
        Puts 0 $err
    }
    
    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1775::test
#========================================================================
