#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2790::test {
    
    set start_time 791500224
    set end_time 791500255
    set UserTag RDS_R_PR2790_3

    set cmdstem { createRDS
    -subject { pr2790 - Merging non-aligned frames }
	-times 751791600-751791855
	-type { RDS_R_PR2790_1 }
	-usertype { $UserTag }
	-usejobdirs 1
	-channels { H1:LSC-AS_Q H2:LSC-AS_Q H2:LSC-REFL_Q L1:LSC-AS_Q L1:LSC-REFL_Q }
	-compressiontype { gzip }
	-compressionlevel { 1 }
	-filechecksum { 1 }
	-frametimecheck { 1 }
	-framedatavalid { 0 }
	-framesperfile { 1 }
	-secperframe { 256 }
	-allowshortframes { 1 }
	-generatechecksum { 1 }
	-fillmissingdatavalid { 0 }  
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

namespace eval ::QA::pr2790::test {

    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    set cmd [ subst $cmdstem ]
    set ldascmd createRDS 
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    set subject [ string trim $subject ]
    set expected "Your results.+$UserTag.+.gwf"
    
    #--------------------------------------------------------------------
    # Generate an RDS in our working directory
    #--------------------------------------------------------------------  
  
    test regression:$thispr:$ldascmd:$subject:submit {} -body {
        if  { [ catch {
            upvar Job Job
            upvar jobDone jobDone
            SubmitJob Job $cmd
            set jobDone 1
            if  { [ info exist Job(jobReply) ] } {
                set reply $Job(jobReply)
            } else {
               set reply none
            }           
        } err] } {
            if  { [ info exist Job(error) ] } {                        
                set joberror $Job(error)
            }
            set jobDone -1
            return -code error $joberror
        }
        return $reply
    } -match regexp -result $expected
    
	#----------------------------------------------------------------
	# In some cases, recalculate the user tag
	#----------------------------------------------------------------
	if { [string length $UserTag] == 0 } {
	    regexp {^(.*[^0-9])([0-9]+)$} $Type match base level
	    set level [expr $level + 1]
	    set UserTag "$base$level"
	}
	if { [regexp {^.*[0-9]$} $UserTag match ] == 0 } {
	    regexp {^.*[^0-9]([0-9]+)$} $Type match level
	    set level [expr $level + 1]
	    set UserTag "$UserTag$level"
	}
	#----------------------------------------------------------------
	# Get a list of files that were generated
	#----------------------------------------------------------------
    
    test regression:$thispr:$ldascmd:$subject:validate {} -body {
            set success ""
            set failure ""
	        foreach remote_file $Job(outputs) {
	            if  { ! [regexp -- "HL-$UserTag-" $Job(outputs) match] } {
		            append failure "Resulting user tag is bad - $remote_file (Expected User tag: $UserTag)\n"
	            } else {
		            append success "Created - $remote_file\n"
	            }    
            }
            if  { [ string length $failure ] } {
                return -code error [ string trim $failure \n ]
            } 
            Puts 5 $success
    } -result {} 
        
    #--------------------------------------------------------------------
    # All finished with the check
    #--------------------------------------------------------------------
    LJdelete job
    flush [outputChannel]
   ;##==================================================================
   ;## Testing is complete
   ;##==================================================================
   cleanupTests
} ;## namespace ::QA::pr2790::test
