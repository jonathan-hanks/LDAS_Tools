#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2071::test {

    set Start 729363827

#------------------------------------------------------------------------
# These values should not be altered
#------------------------------------------------------------------------

    set fp_rx {([-+]?([0-9]*\.)?[0-9]+([eE][-+]?[0-9]+)?)}
    
    set cmdstem { conditionData
    -subject { pr2071 Incorrect filling of arrays for minute trend data }
	-responsefiles {  }
	-aliases {   h1oloopR8  = H1\\:CAL-OLOOP_FAC.mean;  }
	-outputformat { ilwd ascii }
	-algorithms {
	    h1oloopR4 = float(h1oloopR8);
	    clear(h1oloopR8);
	    h1oloop  = complex(h1oloopR4);
	    clear(h1oloopR4);
	    output(h1oloop,_,_,_,H1 open loop factor);
	} 
    -framequery {{ SenseMonitor_V02_H1_M H {} ${Start}-${finish}
	    adc(H1:CAL-OLOOP_FAC.mean) } } 
    }    
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2071
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2071::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

#========================================================================
# Execute a command 
#========================================================================

    proc do_test { Start DeltaT} {
        set name [ namespace current ]
        if  { [ catch {
            set finish [ expr $Start + $DeltaT - 1 ]
            
            set cmd [ subst [ set ::${name}::cmdstem ] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            set expected "Your results.+results.ilwd"
            
            test regression:[ set ::${name}::thispr ]:$ldascmd:Start=$Start:DeltaT=$DeltaT:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar outputs outputs               
                    set outputs $Job(outputs)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
            
            if  { [ info exist outputs ] } {
	            set url $outputs
	            set data [LJread $url]
                Puts 5 $data

	#----------------------------------------------------------------
	# Examine the time offsets
	#----------------------------------------------------------------
                set fp_rx [ set ::${name}::fp_rx ]
                test regression:[ set ::${name}::thispr ]:$ldascmd:Start=$Start:DeltaT=$DeltaT:validate {} -body {
	                regexp {<complex_8[^>]*>([^<]*)<} $data match complex
	                regexp "$fp_rx ${fp_rx}$" $complex -> real
                    Puts 5 "complex $complex, real $real"
                    	if { [ expr $real == 0.0 ] } {
	                       return -code error "$real is not 0.0"
	                    }
                } -result {}
            }
        } err ] } {
            return -code error $err
        }

    }

    foreach delta [ list 60 70 75 ] {
        if  { [ catch {
            do_test $Start $delta
        } err ] } {
            Puts 0 $err
        }
    }
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2071::test
