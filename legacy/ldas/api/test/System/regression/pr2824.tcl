#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

;## expected result from make install
;## -- INFO: frametimecheck: -frametimecheck 0 metadatacheck: -metadatacheck 0
;## -- INFO: frametimecheck: -frametimecheck 0 metadatacheck: -metadatacheck 0

;##-- PASS: PR2824: Generated output: http://gateway/ldas_outgoing/jobs/LDAS-DEV_9577/LDAS-DEV95774505//H-RDS_PR2824_1-793914400-32.gwf
;##-- INFO: frametimecheck: -frametimecheck 0 metadatacheck: -metadatacheck 1
;##-- PASS: PR2824: Generated correct error message: Subject:
;##LDAS-DEV95774526  error!
;##frame::callbackRDS:tid: _10dc350300000000_p_tid callback: ::reduceRawFrames_r error: _10dc350300000000_p_tid:Unable to open frame file: /scratch/test/frames/PRFrames/H-R_PR2824-793914432-32.gwf Because: Malformed file: /scratch/test/frames/PRFrames/H-R_PR2824-793914432-32.gwf Verification exit code: METADATA_INVALID Descriptive Message: One or more pieces of the filename meta data is invalid. Additional info:
;##VerifyException: METADATA_INVALID:   METADATA_VALID ERROR: Either the start time or the duration is invalid


;##-- INFO: frametimecheck: -frametimecheck 1 metadatacheck: -metadatacheck 0
;##-- PASS: PR2824: Generated correct error message: Subject:
;##LDAS-DEV95774534  error!
;##frame::callbackRDS:tid: _d0d9380300000000_p_tid callback: ::reduceRawFrames_r error: _d0d9380300000000_p_tid:Unable to open frame file: /scratch/test/frames/PRFrames/H-R_PR2824-793914432-32.gwf Because: /scratch/test/frames/PRFrames/H-R_PR2824-793914432-32.gwf (file type: binary frame): Data inconsistency detected between filename and internal data

;##-- INFO: frametimecheck: -frametimecheck 1 metadatacheck: -metadatacheck 1
;##-- PASS: PR2824: Generated correct error message: Subject:
;##LDAS-DEV95774558  error!
;##frame::callbackRDS:tid: _50c9380300000000_p_tid callback: ::reduceRawFrames_r error: _50c9380300000000_p_tid:Unable to open frame file: /scratch/test/frames/PRFrames/H-R_PR2824-793914432-32.gwf Because: Malformed file: /scratch/test/frames/PRFrames/H-R_PR2824-793914432-32.gwf Verification exit code: METADATA_INVALID Descriptive Message: One or more pieces of the filename meta data is invalid. Additional info:
;##VerifyException: METADATA_INVALID:   METADATA_VALID ERROR: Either the start time or the duration is invalid

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2824::test {
       
    set framelen 32
    set start 793914432
    set end [expr $start + $framelen - 1]
    set dstart [expr $start - $framelen]
    set dend [expr $end - $framelen ]
    set type G1_RDS_C01_L3
    set frame_filename "/scratch/test/frames/PRFrames/H-R_PR2824-${start}-${framelen}.gwf"
    set channels "H1\:LSC-AS_Q"
    set UserTag RDS_PR2824_1
    set metadatacheck_opts [ list 0 1 ]
    set frametimecheck_opts [ list 0 1 ]
    set Times $dstart-$dend
    
	set cmdstem { createRDS
    -subject { $thispr FrameTimeCheck=$frametimecheck MetadataCheck=$metadatacheck }
    -framequery { {}  {} { $frame_filename } {} Chan($channels) }
    -usertype { $UserTag }
    $frametime_check_flag
	$metadata_check_flag
    -filechecksum 1
	-times $Times
	-channels $channels
	-secperframe 32
	-allowshort 1
    }    

} ;## namespace ::QA::pr2824::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2824 - ---no-metadata-check option
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2824::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]


    #====================================================================
    # Execute a command 
    #====================================================================

    set expected "Your results:.+can be found at"
    
    foreach frametimecheck $frametimecheck_opts {
        if  { $frametimecheck ==  0 } {
            set frametime_check_flag "-frametimecheck 0"
        } else {
            set frametime_check_flag "-frametimecheck 1"
        }
        foreach metadatacheck $metadatacheck_opts {
            if  { $metadatacheck ==  0 } {
                set metadata_check_flag "-metadatacheck 0"
            } else {
                set metadata_check_flag "-metadatacheck 1"
            }
            set framefile [ subst $frame_filename ]        
            set cmd [ subst $cmdstem ]
            set ldascmd [ lindex $cmd 0 ]
            regexp -- {-subject \{([^\}]+)} $cmd -> subject

            if  { $frametimecheck == 1 } {
                ;## frametimecheck=1 metadatacheck=1
                if  { $metadatacheck == 1 } {
                    set expected "Verification exit code: METADATA_INVALID"
                } else {
                    ;## frametimecheck=0 metadatacheck=1
                    set expected "Data inconsistency detected between filename and internal data"
                }
	        } elseif { $metadatacheck == 1 } {
                ;## frametimecheck=0 metadatacheck=1
		        set expected "Verification exit code: METADATA_INVALID "
            } else {
                ;## frametimecheck=0 metadatacheck=0
                set expected "Generated output.+H-RDS_PR2824_1-793914400-32.gwf"
            }

           test regression:$thispr:$ldascmd:$subject {} -body {
                if 	{ [catch {
                    SubmitJob job $cmd
                    if { $frametimecheck || $metadatacheck } {
	                    set reply "$job(jobId) Generated no error message"
	                } elseif { ! $frametimecheck && ! $metadatacheck } {
	                    set reply "Generated output: $job(outputs)"
	                }
                    LJdelete job 
                    Flush 
                    return $reply                 
                } err] } {
                    set joberror $err
                    if  { [ info exist job(error) ] } {                        
                        set joberror $job(error)
                    }
                    LJdelete job
                    return -code error $joberror
                }
            } -match regexp -result $expected
        }
    }
    
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2824::test
