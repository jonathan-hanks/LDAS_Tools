#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2221::test {
	set cmdstem { getMetaData
    -subject { pr2221 - getElement returns invalid string for empty element array }
	-returnprotocol file:/foo
	-outputformat { ilwd $Mode}
	-database ldas_tst
	-sqlquery {select * from $Table fetch first 10 rows only}
    }
    set testArgs { { binary sngl_burst } { ascii sngl_burst } 
        { binary sngl_block } { ascii sngl_block } }

} ;## namespace ::QA::pr2221::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit
lappend auto_path /ldas/lib
package require genericAPI

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2221 - framequery for MDC frames
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2221::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    proc Check { Mode Table } {
        set name [ namespace current ]
    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------
	    if	{ [ string equal binary $Mode ] } {
		    set option "-binary 1"
	    } else {
		    set option "-binary 0"
	    }
        
        set cmd [ subst [ set ::${name}::cmdstem ] ]
        set ldascmd [ lindex $cmd 0 ]
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        Puts 5 "cmd $cmd"
        set expected .+
        test regression:[ set ::${name}::thispr ]:$ldascmd:$Mode:$Table:submit {} -body {
        
            if  { [ catch {
                upvar Job Job
                SubmitJob Job $cmd
                upvar outputs outputs
                set outputs  $Job(outputs)
                set reply $Job(jobReply)
                upvar host host
                if { ! [ regexp {^([^:]*):(.*)$} $Job(manager) -> host port ] } {
                    set host $Job(manager)
                }
                Puts 5 $reply
            } err] } {
                if  { [ info exist Job(error) ] } {                        
                    set joberror $Job(error)
                } else {
                    set joberror $err
                }
                LJdelete Job
                return -code error $joberror
            }
            LJdelete Job
            return $reply
        } -match regexp -result $expected
    
        if  { [ info exist outputs ] } {
            if  { [ catch {
  	#----------------------------------------------------------------
	# Copy the output here
	#----------------------------------------------------------------
	            set remote_file [lindex $outputs 0]
	            regexp {^.*/([^/]*)$} $remote_file -> file
	            if  { [ regexp {^/} $remote_file ] } {
	    # Add http protocol so LJcopy will be able to
	    #   retrieve the file
	                set remote_file "http://${host}${remote_file}"
	            }
	            set file "/var/tmp/[pid]_${file}"
	            LJcopy $remote_file $file $option
	
	#----------------------------------------------------------------
	# Read the file
	#----------------------------------------------------------------
	            set filwd [openILwdFile $file r]
	            set contp [readElement $filwd]
	            closeILwdFile $filwd
                
	#------------------------------------------------------------
	# Verify the file's contents
	#------------------------------------------------------------
	            set contp [refContainerElement $contp 0]
	            set numCols [getElementAttribute $contp "size"]
                set errors ""
                test regression:[ set ::${name}::thispr ]:$ldascmd:$Mode:$Table:validate {} -body {
	                foreach mode [list ascii binary] {
		                for {set i 0} {$i < $numCols} {incr i 1 } {
		                    set elmcontp [refContainerElement $contp $i]
		                    set entry [getElement $elmcontp $mode none]
		                    if {[regexp -- {^<.*dims='0'[^>]*>[^<][^<]*<.*>$} $entry match]} {
                                append errors "Table: $Table Mode: $mode" ;# - '$entry'\n"
                            } else {
                                Puts 5 "Table: $Table Mode: $mode"
                            }
                        }
                    }
                    set errors [ string trim $errors \n ]
                    if  { [ string length $errors ] } {
                        return -code error $errors 
                    }
                } -result {}
	        } err ] } {
                return -code error $err
            }
        }
	}
    ;## end of Check proc
      
    ;## main driver
    
    foreach case $testArgs {
        foreach { mode table } $case { break }
        if  { [ catch {
            Check $mode $table 
        } err ] } {
            Puts 0 "$mode $table error: $err"
        }
    }
    flush [outputChannel]
	#----------------------------------------------------------------
	# Get rid of the temporary file
	#----------------------------------------------------------------
	#file delete -force $file
    #--------------------------------------------------------------------
    # Cleanup resources
    cleanupTests
} ;## namespace ::QA::pr2221::test
