#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#
# pr2873 - getFrameData can produce files with names incorrectly indicating length is 0
#
#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2873::test {
    set FrameStart 795486900
    set DeltaT 500
    
    set cmdstem { getFrameData
    -subject { pr2873 - files with names of zero length }
	-returnprotocol http
	-outputformat frame
	-framequery {H1_RDS_C01_LX {} {} $time_range Proc(H1:LSC-STRAIN)} 
    }
} ;## namespace ::QA::pr2873::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2873::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]    
    set end [ expr $FrameStart + $DeltaT - 1 ]
    set time_range "${FrameStart}-${end}"

    #====================================================================
    # Execute a command 
    #====================================================================
    
    set cmd [ subst $cmdstem ]
    Puts 5 "cmd $cmd"
    set ldascmd [ lindex $cmd 0 ]
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    set expected "Your results:.+can be found at"
 
    test regression:$thispr:$ldascmd:$subject:submit {} -body {
            if 	{ [catch {
                SubmitJob job $cmd
            } err ] } {
                if  { [ info exist job(error) ] } {
                    set joberror $job(error)
                }
				LJdelete job
                return -code error $err
            }
            if  { [ info exist job(outputs) ] } {
                upvar outputs job(outputs)
                set outputs $job(outputs) 
                set jobreply $job(jobReply)
            } 
            LJdelete job
            Puts 5 $jobreply
            return $jobreply
    } -match regexp -result $expected

    if  { [ info exists outputs ] } {
        test regression:$thispr:$ldascmd:$subject:validate {} -body {
            set error ""
	        foreach file $outputs {
	            if { [ regexp -- {\-0\.gwf} $file ] } {
		            append error "$file has dt value of 0:\n"
                }
	        }
            if  { [ string length $error ] }  {
                return -code error [string trim $error \n ]
            }
	    } -result {}
    }
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2873::test
