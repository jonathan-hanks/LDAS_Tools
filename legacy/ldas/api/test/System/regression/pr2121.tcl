#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2121::test {

;## example of frame output 
    set FRAME_FILES [ list \
Z-P_LDAS_62501568_eventmonAPI_1-730522723-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_2-730522811-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_3-730522899-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_4-730522987-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_5-730523075-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_6-730523163-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_7-730523251-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_8-730523339-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_9-730523427-89.gwf \
Z-P_LDAS_62501568_eventmonAPI_10-730523515-89.gwf ]

    set FRAME_TIMES [ list 730522723 730522811 730522899 730522987 \
730523075 730523163 730523251 730523339 730523427 730523515 ]

    set cmd_case1 { dataPipeline
    -subject { pr2121 - md5sum substitutions }

    -dynlib libldasstochastic.so
    -filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)

    -np 2
    -multidimdatatarget frame
    -concatenate 1
    -realtimeratio 0.9
    -datacondtarget wrapper
    -datadistributor WRAPPER
    -outputformat { ilwd ascii }
    -database ldas_tst
	#-returnprotocol 
    #-setsingleem 1

    -framequery {
        { {} H {} 730522722-730523604 Adc(H2:LSC-AS_Q!resample!8!) }
        { {} L {} 730522722-730523604 Adc(L1:LSC-AS_Q!resample!8!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    }
    -aliases {
        #gw1 = H2\:LSC-AS_Q;
        #gw2 = L1\:LSC-AS_Q;
        gw1 = H2\:LSC-AS_Q::ProcData;
        gw2 = L1\:LSC-AS_Q::ProcData;
    }
    -algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

	# Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    } }
    
    set cmd_case2 {	dataPipeline
    -subject { pr2121  -returnprotocol specify non-job directory for frames }

    -dynlib libldasstochastic.so
    -filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)

    -np 2
    -multidimdatatarget frame
    -concatenate 1
    -realtimeratio 0.9
    -datacondtarget wrapper
    -datadistributor WRAPPER
    -outputformat { ilwd ascii }
    -database ldas_tst
	-returnprotocol file:$framedir/
    #-setsingleem 1

    -framequery {
        { {} H {} 730522722-730523604 Adc(H2:LSC-AS_Q!resample!8!) }
        { {} L {} 730522722-730523604 Adc(L1:LSC-AS_Q!resample!8!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    }
    -aliases {
        #gw1 = H2\:LSC-AS_Q;
        #gw2 = L1\:LSC-AS_Q;
        gw1 = H2\:LSC-AS_Q::ProcData;
        gw2 = L1\:LSC-AS_Q::ProcData;
    }
    -algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

	# Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    } }
    
    set cmd_case3 {	dataPipeline
    -subject { pr2121 specify http non-job directory for frames }

    -dynlib libldasstochastic.so
    -filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)

    -np 2
    -multidimdatatarget frame
    -concatenate 1
    -realtimeratio 0.9
    -datacondtarget wrapper
    -datadistributor WRAPPER
    -outputformat { ilwd ascii }
    -database ldas_tst
	-returnprotocol http:$framedir/
    #-setsingleem 1

    -framequery {
        { {} H {} 730522722-730523604 Adc(H2:LSC-AS_Q!resample!8!) }
        { {} L {} 730522722-730523604 Adc(L1:LSC-AS_Q!resample!8!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    }
    -aliases {
        #gw1 = H2\:LSC-AS_Q;
        #gw2 = L1\:LSC-AS_Q;
        gw1 = H2\:LSC-AS_Q::ProcData;
        gw2 = L1\:LSC-AS_Q::ProcData;
    }
    -algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

	# Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    } }
    
    set cmd_case3 { dataPipeline
    -subject { pr2121 - -returnprotocol specify http non-job directory for frames }

    -dynlib libldasstochastic.so
    -filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)

    -np 2
    -multidimdatatarget frame
    -concatenate 1
    -realtimeratio 0.9
    -datacondtarget wrapper
    -datadistributor WRAPPER
    -outputformat { ilwd ascii }
    -database ldas_tst
	-outputdir $framedir
    #-setsingleem 1

    -framequery {
        { {} H {} 730522722-730523604 Adc(H2:LSC-AS_Q!resample!8!) }
        { {} L {} 730522722-730523604 Adc(L1:LSC-AS_Q!resample!8!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    }
    -aliases {
        #gw1 = H2\:LSC-AS_Q;
        #gw2 = L1\:LSC-AS_Q;
        gw1 = H2\:LSC-AS_Q::ProcData;
        gw2 = L1\:LSC-AS_Q::ProcData;
    }
    -algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

	# Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    } }
	
    set cmd_case4 { dataPipeline
    -subject { pr2121 - -outputdir specify non-job directory for frames }

    -dynlib libldasstochastic.so
    -filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)

    -np 2
    -multidimdatatarget frame
    -concatenate 1
    -realtimeratio 0.9
    -datacondtarget wrapper
    -datadistributor WRAPPER
    -outputformat { ilwd ascii }
    -database ldas_tst
	-outputdir $framedir
    #-setsingleem 1

    -framequery {
        { {} H {} 730522722-730523604 Adc(H2:LSC-AS_Q!resample!8!) }
        { {} L {} 730522722-730523604 Adc(L1:LSC-AS_Q!resample!8!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    }
    -aliases {
        #gw1 = H2\:LSC-AS_Q;
        #gw2 = L1\:LSC-AS_Q;
        gw1 = H2\:LSC-AS_Q::ProcData;
        gw2 = L1\:LSC-AS_Q::ProcData;
    }
    -algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

	# Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    } }
    
    set cmd_case5 { dataPipeline
    -subject { pr2121 error -outputdir specify non-job non-existent subdirectory for frames }

    -dynlib libldasstochastic.so
    -filterparams (10,180428,0.0,0,0,0,0,360855,3840,40,0.25,3840,40,0.25,1,H,L,11)

    -np 2
    -multidimdatatarget frame
    -concatenate 1
    -realtimeratio 0.9
    -datacondtarget wrapper
    -datadistributor WRAPPER
    -outputformat { ilwd ascii }
    -database ldas_tst
	-outputdir $framedir
    #-setsingleem 1

    -framequery {
        { {} H {} 730522722-730523604 Adc(H2:LSC-AS_Q!resample!8!) }
        { {} L {} 730522722-730523604 Adc(L1:LSC-AS_Q!resample!8!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp1_H2:LSC-AS_Q.0-1024.0.25.ilwd,pass
        file:/ldas_outgoing/jobs/responsefiles/stochastic-resp2_L1:LSC-AS_Q.0-1024.0.25.ilwd,pass
    }
    -aliases {
        #gw1 = H2\:LSC-AS_Q;
        #gw2 = L1\:LSC-AS_Q;
        gw1 = H2\:LSC-AS_Q::ProcData;
        gw2 = L1\:LSC-AS_Q::ProcData;
    }
    -algorithms {
        z1 = value(gw1);
        clear(gw1);
        z2 = value(gw2);
        clear(gw2);
        z1 = slice(z1, 2058, 1804280, 1);
        z2 = slice(z2, 2058, 1804280, 1);

	# Patch to make psd's work
        z1s = slice(z1, 0, 1794048, 1);
        z2s = slice(z2, 0, 1794048, 1);

        psd1 = psd(z1s, 16384, _, 8192);
        output(psd1,_,_,,spectrum1);
        psd2 = psd(z2s, 16384, _, 8192);
        output(psd2,_,_,,spectrum2);
        output(z1,_,_,,data1);
        output(z2,_,_,,data2);
    } }
    
    set framedir /ldas_outgoing/frames/PR2121
    array set dirs [ list  cmd_case1 $framedir cmd_case2 [ list $framedir outputDir ] \
        cmd_case3 $framedir cmd_case4 $framedir cmd_case5 $framedir ]
} ;## namespace ::QA::pr2121::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2121 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2121::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]


#========================================================================
# PR2121 
#  -returnprotocol has no effect on frame or xml output; these are always
# placed in the job directory
# -outputdir will allow frame files to be placed in the specified directory
# but has no effect on xml output
# Note: make sure user stochastictest password is correct via /ldas/bin/ldaspw
#========================================================================

#========================================================================
# Establish ranges - These values should be changed as needed at the
#  test data evolves.
#========================================================================
#------------------------------------------------------------------------
# Needs rwxrwx*** on /ldas_outgoing/frames/RDSVerify for this to work on sunbuild
#------------------------------------------------------------------------

    proc testCleanup { framedir } {
        set name [ namespace current ]
	    if	{ ! [ file exist $framedir ] } {
		    file mkdir $framedir 
		    return
	    }
	    foreach file [ glob -nocomplain $framedir/*.gwf ] {
    	    catch { file delete [ file join $framedir $file ] }
        }
    }  
    
#------------------------------------------------------------------------
# Verify that 10 frame files are in output
#------------------------------------------------------------------------

    proc ValidateFrameOutput { ldascmd outputdir subject } {
        set name [ namespace current ]
	    set i 0
        set errors ""
        test regression:[ set ::${name}::thispr ]:$ldascmd:$subject:wrapper:validate {} -body {
	        foreach file [ lsort -dictionary [ glob -nocomplain $outputdir/*.gwf ] ] {
		        set pattern "Z-P_LDAS_\\d+_eventmonAPI_[ expr $i + 1 ]-[ lindex $::FRAME_TIMES $i ]-89.gwf"		
		        if	{ ! [ regexp $pattern $file ] } {
			        append errors "mismatch pattern $pattern\n$file"
        	        set retval 0
                }
		        incr i
            }
	        ;## no files are found 
	        if	{ ! $i } {
		        append errors "No frame files found in $outputdir"
		        return -code error $error
	        }
        } -result {}
    }

    proc doTest { cmdstem } {
        set name [ namespace current ]
        set framedir [ set ::${name}::framedir ]
        
        if  { [ catch {
            foreach directory [ set ::${name}::dirs($cmdstem) ] {                
                catch { ::${name}::testCleanup $directory }
            }
            
            set cmd [ subst [ set ::${name}::$cmdstem ] ]
            set ldascmd [ lindex $cmdstem 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            set expected "Your results.+gwf"
            test regression:[ set ::${name}::thispr ]:$ldascmd:$subject:wrapper:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar outputDir outputDir                
                    set outputDir $Job(outputDir)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected

            ;## can run only if there is a functional mpi so there is output
            if  { [ info exist outputDir ] } {
                foreach directory [ set ::${name}::dirs($cmdstem) ] {
                    ValidateFrameOutput $directory
                }
            }
        } err ] } {
            return -code error $err
        }
    }

    ;## main code
    foreach case [ list cmd_case1 cmd_case2 cmd_case3 cmd_case4 cmd_case5 ] {
        if  { [ catch {
            doTest $case
        } err ] } {
            Puts 0 "$case error: $err"
        }
    }
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2121::test
