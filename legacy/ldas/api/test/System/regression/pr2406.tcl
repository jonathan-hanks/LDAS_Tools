#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2406::test {
    set cmdstem_ok {	createRDS
        -subject { pr2406 successful jobs }
	    -times {$Start}
	    -type $Type
	    $usertype
	    -channels {H2:LSC-AS_Q${Resampling}}
	    $options
    }
    set cmdstem_error {	createRDS
        -subject { pr2406 error checking }
	    -times {$Start}
	    -type $Type
	    -usertype $UserTag
	    -channels {H2:LSC-AS_Q}
        -framechecksum 0
	    -filechecksum 0
    }
} ;## namespace ::QA::pr2406::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2530 - ---no-metadata-check option
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2406::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------

    set Start 730524000    ;# S2 dataset
    #set Start 751800032   ;# S3 dataset
    
    ;## args are Type UserTag Resampling
    
    set args_Check { { RDS_R_L1 ""  1 } { RDS_R_L1 RDS_R_L 1 } 
        { R RDS_R_L3    1 }     { RDS_R_L1 RDS_R_L3    1 }
        { RDS_R_L3 RDS_R_L4 1 } { RDS_R_L3 PR2406_R_L1 1 }
        { RDS_R_L1 "" 4 } { RDS_R_L1 RDS_R_L 4 }
        { R RDS_R_L3 4 } { RDS_R_L1 RDS_R_L3 4 }
        { RDS_R_L3 RDS_R_L4 4 } { RDS_R_L3 PR2406_R_L1 4 }
    }
    
    set args_ErrorCheck { { RDS_R_L3 RDS_R_L2 {does not exceed the minimum level} }
        { RDS_R_L3 RDS_R_L3 {does not exceed the minimum level} }
    }

    proc Check {Type UserTag Resampling} {
        set name [ namespace current ]
        if  { [ catch {
            set Start [ set ::${name}::Start ]
            set options [list -framechecksum 0 -filechecksum 0 -metadatacheck 0]
    #--------------------------------------------------------------------
    # Generate an RDS in our working directory
    #--------------------------------------------------------------------
            if  { [string length $UserTag] > 0 } {
	            set usertype "-usertype $UserTag"
                set UserTagText $UserTag
            } else {
	            set usertype ""
                set UserTagText none
            }
            if  { $Resampling > 1 } {
	            set Resampling "!$Resampling"
                set ResamplingText $Resampling
            } else {
	            set Resampling ""
                set ResamplingText "!0"
            }
            set cmd [ subst [ set ::${name}::cmdstem_ok ] ]
            set ldascmd createRDS 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            set expected "Your results.+gwf"
            test regression:[ set ::${name}::thispr ]:$ldascmd:Type=$Type:UserTag=$UserTagText:Resampling=$ResamplingText:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar outputs  outputs                
                    set outputs $Job(outputs)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
            if  { [ info exist outputs ] } { 
	#----------------------------------------------------------------
	# In some cases, recalculate the user tag
	#----------------------------------------------------------------
	            if  { [string length $UserTag] == 0 } {
	                regexp {^(.*[^0-9])([0-9]+)$} $Type match base level
	                set level [expr $level + 1]
	                set UserTag "$base$level"
	            }
	            if { [regexp {^.*[0-9]$} $UserTag match ] == 0 } {
	                regexp {^.*[^0-9]([0-9]+)$} $Type match level
	                set level [expr $level + 1]
	                set UserTag "$UserTag$level"
	            }
	#----------------------------------------------------------------
	# Get a list of files that were generated
	#----------------------------------------------------------------
                test regression:[ set ::${name}::thispr ]:$ldascmd:Type=$Type:UserTag=$UserTagText:Resampling=$ResamplingText:validate {} -body {
                    set errors ""
	                foreach remote_file $outputs {
	                    if  { ! [regexp "H-$UserTag-" $outputs match] } {
		                    append errors "${::LEADER}: Resulting user tag is bad - $remote_file (Expected User tag: $UserTag)"
                        } else {
                            Puts 5 "Created - $remote_file" 
                        }
                    }
                }
	        }
        } err ] } {
            return -code error $err
        }
    }

    proc ErrorCheck {Type UserTag Error} {
        set name [ namespace current ]
        if  { [ catch {
            set Start [ set ::${name}::Start ]
    #--------------------------------------------------------------------
    # Generate an RDS in our working directory
    #--------------------------------------------------------------------
            set cmd [ subst [ set ::${name}::cmdstem_error ] ]
            set ldascmd createRDS 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            test regression:[ set ::${name}::thispr ]:$ldascmd:Type=$Type:UserTag=$UserTag:Error='$Error' {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar reply reply                    
                    set reply $Job(jobReply)
                    Puts 5 $reply
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $Error
    
        } err ] } {
            return -code error $err 
        }
    }
    
    
    ;## start testing here
    foreach case $args_Check {
        foreach { Type UserTag Resampling } $case { break }
        if  { [ catch {
            Check $Type $UserTag $Resampling
        } err ] } {
            Puts 0 "Check Type $Type UserTag $UserTag Resampling $Resampling error: $err"
        }
    }   
    foreach case $args_ErrorCheck {
        foreach { Type UserTag Error } $case { break }
        if  { [ catch {
            ErrorCheck $Type $UserTag $Error
        } err ] } {
            Puts 0 "Check Type $Type UserTag $UserTag Error $Error error: $err"
        }
    }
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2406::test
