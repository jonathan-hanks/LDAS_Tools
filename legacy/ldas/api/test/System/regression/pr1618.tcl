#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1618::test {

    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------

    set retval 1
    set s2_start 730524000    ;# S2 dataset
    set s2_channels "H2:LSC-AS_Q"
	
    set cmdstem { concatFrameData
        -subject { pr 1618 - concatFrameData on downsampled frames gives worng dt and IFO type }
	    -framequery {
	        {RDS_R_L1 H {} $Start-$end Adc($Channels)}
	    }
	    -outputformat {frame}
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1705
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr1618::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]    
    # set s3_channels "H1:LSC-DARM_CTRL,H1:LSC-ETMX_EXC_DAQ,H1:LSC-DARM_CTRL_EXC_DAQ"
    #--------------------------------------------------------------------
    # Run through a series of tests
    #--------------------------------------------------------------------

    if  { [ catch {
        set Start $s2_start        
        set Dt 78
        set Channels $s2_channels
        set end [expr $Start + $Dt - 1]

        set cmd [ subst -nobackslashes $cmdstem ]
        set ldascmd [ lindex $cmd 0 ] 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
            
        set expected "Your results:.+gwf"
        
        test regression:$thispr:$ldascmd:start=$Start:Delta=$Dt:channel=$s2_channels:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd        
                    upvar url url
                    set url $Job(outputs)
                    set reply $Job(jobReply)
                    #Puts 5 $reply
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    upvar nodata nodata
                    set nodata 1
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
        } -match regexp -result $expected
            
        if  { [ info exist url ] } {
            test regression:$thispr:$ldascmd:start=$Start:Delta=$Dt:channel=$s2_channels:validate {} -body {
                if  { [ catch {
                    set errors ""
	                regexp -- {-([0-9]+)\.gwf} $url -> dt
	                if  {$dt != $Dt} {
                        append errors "$url has wrong dt value($dt): should have been: $Dt\n"
                    }
                    if  {[regexp -- {^Z-} $url]} {
	                    append errors "${::LEADER}: IFO specifier is Z\n"
	                }
                    if  { [ string length $errors ] } {
                        return -code error [ string trim $errors \n ]
                    }      
                } err ] } {
                    return -code error $errors
                }            
            } -result {}
        }
    } err ] } {
        Puts 0 $err        
    }
	#----------------------------------------------------------------
	# Copy the file and check the internals
	#----------------------------------------------------------------
	set base_name [file tail $url]
	#LJcopy $url $base_name
	#:TODO: Check the value of nData * dx ~= Dt
	#:TODO: Currently check visually
	#file delete $base_name
  
    #--------------------------------------------------------------------
    # Finished testing
    #--------------------------------------------------------------------

    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1618::test
