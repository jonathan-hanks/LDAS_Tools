#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2125::test {
    set ValidateMode 1
    set Start 730000064    
} ;## namespace ::QA::pr2122::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--startgps} {string} \
    {frame gps start time} \
    { set ::QA::pr2125:test $::QA::Options::Value }


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2125::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob

#========================================================================
# The start time given below is for S2 data
#========================================================================
    set Start 730000064

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
    set Dt 16
    set Finish [ expr $Start + $Dt - 1]

    set LEADER "PR2125"
    
    set basiccmd { createRDS
        -times { ${Start}-${Finish} }
        -type { R }
        -channels { H1:LSC-AS_Q!2 }
        -compressiontype { raw } }
    
    set cmd [ subst $basiccmd ]
    Puts 5 $cmd
    test regression:pr2125:createRDS:$Start {} -body {
        if  { [ catch {
            SubmitJob job $cmd
        } err ] } {
            if  { [ info exist job(error) ] } {
                set joberror $job(error)
            } else {
                set joberror $err
            }
            if { ! [ regexp {Unable to open frame file} $joberror match ] } {
               return "Error $err $joberror"
            } else {
                return $joberror
            }
        }
        LJdelete job

    } -match regexp -result {Unable to open frame file}
    flush [outputChannel]
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2125::test    
