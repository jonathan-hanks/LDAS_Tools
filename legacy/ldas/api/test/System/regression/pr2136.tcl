#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2136::test {
    set SpectrumStartSecond 609999999
    set SpectrumStartNanosecond 995117188
    
	set cmdstem { dataPipeline
	-subject { pr2136 - DBSPECTRUM:GOOD:${PushPass}:WRAPPER fails for inspiral }
	-dynlib {libldasinspiral.so}
	-filterparams (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
	-dbspectrum {
	    channel={resample(P2:LSC-AS_Q,1,8)}
	    spectrum_type=Welch
	    spectrum_length=131073
	    start_time=${SpectrumStartSecond}
	    start_time_ns=${SpectrumStartNanosecond}
	    start_frequency=0.0000000000000000e+00
	    delta_frequency=7.8125000000000000e-03
	    pushpass=${PushPass}
	    alias=${alias}
	}
	-datacondtarget wrapper
	-multidimdatatarget ligolw
	-database ldas_tst
	-responsefiles {
	    file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
	}
	-aliases {gw=P2\:LSC-AS_Q::AdcData;}
	-algorithms {
	    rgw = resample(gw, 1, 8);
	    output(rgw,_,_,:primary,resampled gw timeseries);
	    ${algo}
	}
	-framequery { R P {} 610000000-610000511 {Adc(P2:LSC-AS_Q)} }
    }
    
    set pushpassList { push pass }
    
} ;## namespace ::QA::pr2136::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2136 - dbSpectrum pass failed at inspiral
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2136::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    foreach PushPass $pushpassList {
        if  { $PushPass == "push" } {
	        set alias "spec"
	        set algo "output(${alias},_,_,spectrum:psd,spectrum data);"
        } else {
	        set alias "spectrum:psd"
	        set algo ""
        }
        set cmd [ subst $cmdstem ]
        set ldascmd [ lindex $cmd 0 ]
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        set expected "The job was not accepted by the LDAS manager|mpi::newJobCallback:"
        Puts 5 $cmd
        
        test regression:$thispr:$ldascmd:$PushPass:wrapper:submit {} -body {
        
            if  { [ catch {
                upvar Job Job
                SubmitJob Job $cmd
                set reply $Job(jobReply)
            } err] } {
                if  { [ info exist Job(error) ] } {                        
                    set joberror $Job(error)
                } else {
                    set joberror $err
                }
                LJdelete Job
                Puts 1 $joberror
                return -code error $joberror
            }
            LJdelete Job
            return $reply
        } -match regexp -result $expected
    }
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2136::test
