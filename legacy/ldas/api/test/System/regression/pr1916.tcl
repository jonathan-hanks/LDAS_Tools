#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1916::test {

    #--------------------------------------------------------------------
    # Establish local values
    #--------------------------------------------------------------------
    set Start 730522210
    
	set cmdstem1 { dataPipeline
	-subject { pr1916 - multiple result.ilwd files from datacond }
	-dynlib {}
	-datacondtarget datacond
	-outputformat { ilwd ascii }
	-database           ldas_tst
	-framequery { R H {} {${Start}-${Finish}} Adc(H2:LSC-AS_Q) }
	-responsefiles { file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass }
	-aliases { gw = H2\:LSC-AS_Q::AdcData; }
	-algorithms {
            rgw = resample(gw, 1, 8);
            p = psd( rgw, [ expr $scale * $delta_t ] );
            output(rgw,_,_,:primary,resampled gw timeseries);
            output(p,_,_,spectrum:psd,spectrum data);
	}
    }
    
    set  cmdstem2 {	dataPipeline
	-subject { pr1916 multiple result.ilwd files from datacond }
	-dynlib libldasinspiral.so
	-filterparams (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
	-multidimdatatarget ligolw
	-database           ldas_tst
	-np                 3
	-framequery { R H {} {${Start}-${Finish}} Adc(H2:LSC-AS_Q) }
	-responsefiles { file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass }
	-aliases { gw = H2\:LSC-AS_Q::AdcData; }
	-algorithms {
            rgw = resample(gw, 1, 8);
            p = psd( rgw, [ expr $scale * $delta_t ] );
            output(rgw,_,_,:primary,resampled gw timeseries);
            output(p,_,_,spectrum:psd,spectrum data);
	}
    }
    set scale1 64
    set scale2 512
    set delta1 1
    set delta2 512
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1940
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr1916::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    proc Check { cmdstem scale delta_t check_output } {
    
        if  { [ catch {
            set name [ namespace current ]
            set Start [ set ::${name}::Start ]
            set Finish [ expr $Start + $delta_t - 1 ]

    #========================================================================
    # Execute a command 
    #========================================================================
            
            set cmd [ subst -nobackslashes [ set ::${name}::$cmdstem ] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            set expected "Your results:.+results.ilwd.+No MPI processing will occur"
            
            test regression:[ set ::${name}::thispr ]:$ldascmd:Start=$Start:DeltaT=$delta_t:scale=$scale:Finish=$Finish:wrapper:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar outputs outputs               
                    set outputs $Job(outputs)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
            
            set expected  1
            
            if  { [ info exist outputs ] && $check_output } {
                test regression[ set ::${name}::thispr ]:$ldascmd:Start=$Start:DeltaT=$delta_t:scale=$scale:Finish=$Finish:wrapper:validate {} -body {
                    set count 0
	                foreach file $outputs {
	                    if  { [ regexp {/results[.]ilwd([.].*)?$} $file match ] } {
		                    set count [ expr $count + 1 ]
	                    }
                    }
                    return $count
                }  -match regexp -result $expected
            }
        } err ] } {
            Puts 0 $err 
        }
    }

    foreach case [ list 1 2 ] {
        if  { [ catch {
            if  { $case == 1 } {
                set check_output 1
            } else {
                set check_output 0
            }
            Check cmdstem$case [ set scale$case ] [ set delta$case ] $check_output
        } err ] } {
            Puts 0 $err
        }
    }    
    flush [outputChannel]
    #========================================================================
    # Exit with the negation of return value since programs terminate
    #   successfully with 0, unsuccessfully otherwise
    #========================================================================
    cleanupTests
} ;## namespace ::QA::pr1916::test
