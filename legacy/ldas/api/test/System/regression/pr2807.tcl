#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2807::test {
       
    set UserTag RDS_PR2807_1
    
	set cmdstem1 { createRDS
    -subject { $thispr unrestrict createRDS for Geo frames merge }	
    -times $Times
	-ifo $IFO
	-type $Type
	-channels { $Channels }
   	-usertype { $UserTag }
   	-secperframe $SecPerFrame
	    [join $args]
    }    
    set cmdstem2 { 	createRDS
    -subject { $thispr unrestrict createRDS for Geo frames merge }	
	-framequery { $Type $IFO {} $Times Chan($Channels) }
   	-usertype { $UserTag }
   	-secperframe $SecPerFrame
	[join $args]
    }
} ;## namespace ::QA::pr2807::test


#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2807 - ---no-metadata-check option
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2807::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    set dt 256

    set s2_start 730524432
    set s2_end [ expr $s2_start + $dt - 1]

    set s3_start 751791600
    set s3_end [ expr $s3_start + $dt - 1]
#========================================================================
# Should not have to modify any variables below this point
#========================================================================

proc CheckSameness {Times IFO Type Channels SecPerFrame args} {
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*
    
    set retval 1
    set UserTag RDS_PR2807_1

    #--------------------------------------------------------------------
    # First command uses legacy options
    #--------------------------------------------------------------------
    set thispr [ set ::${name}::thispr ]
    set cmd1 [ subst [ set ::${name}::cmdstem1 ] ]
    set ldascmd createRDS 
    regexp -- {-subject \{([^\}]+)} $cmd1 -> subject
    set subject [ string trim $subject ]
    
    test regression:[ set ::${name}::thispr ]:$ldascmd:$subject:$Times:times {} -body {
        if  { [ catch {
            upvar job_legacy job_legacy
            SubmitJob job_legacy $cmd1
            ::${name}::CheckOutput job_legacy $UserTag
        } err] } {
            SetJobId job_legacy
            if  { [ info exist job_legacy(error) ] } {                        
                set joberror $job_legacy(error)
            }
            LJdelete job_legacy
            upvar retval retval
            set retval 0
            return $joberror
        }
    } -result {} 
    
    if  { $retval } {
        set cmd2 [ subst [ set ::${name}::cmdstem2 ] ]
        regexp -- {-subject \{([^\}]+)} $cmd2 -> subject
        set subject [ string trim $subject ]
        test regression:[ set ::${name}::thispr ]:$ldascmd:$subject:$Times:framequery {} -body {
            if  { [ catch {
                upvar job_framequery job_framequery
                SubmitJob job_framequery $cmd2
                ::${name}::CheckOutput job_framequery $UserTag
            } err] } {
                SetJobId job_framequery
                if  { [ info exist job_framequery(error) ] } {                        
                    set joberror $job_framequery(error)
                }
                LJdelete job_framequery
                upvar retval retval
                set retval 0
                return $joberror
            }
        } -result {}
    } 

    #--------------------------------------------------------------------
    # Compare the jobs together
    #--------------------------------------------------------------------
    # Did they generate the same number of files
    #--------------------------------------------------------------------
    if { $retval } {
       test regression:$thispr:$ldascmd:$Times:compare {} -body {
            set len1 [llength $job_legacy(outputs) ]
            set len2 [ llength $job_framequery(outputs) ]
	        if  { $len1 != $len2 } {
	            return -code error "Jobs differ in #files generated: -times $len1 vs -framequery $len2"
            } 
            
        } -result {}
	} 

    #--------------------------------------------------------------------
    # Cleanup
    #--------------------------------------------------------------------
    LJdelete job_framequery
    LJdelete job_legacy

}

proc CheckOutput {Job UserTag} {
    #--------------------------------------------------------------------
    # Local variables
    #--------------------------------------------------------------------
    set retval 1
    #--------------------------------------------------------------------
    # Need to get the correct job information
    #--------------------------------------------------------------------
    upvar $Job job
    #--------------------------------------------------------------------
    # Get a list of files that were generated
    #--------------------------------------------------------------------
    foreach remote_file $job(outputs) {
	    if { ! [regexp -- "-$UserTag-" $job(outputs) match] } {
	        return -code error "$Job{JobId} Resulting user tag is bad - $remote_file (Expected User tag: $UserTag)"
	    } else {
	        Puts 5 "$Job{JobId} Created - $remote_file"
	    }
    }
}

    #--------------------------------------------------------------------
    # Run through a series of tests
    #--------------------------------------------------------------------
    CheckSameness ${s2_start}-${s2_end} \
			  H R H1:LSC-AS_Q,H2:LSC-AS_Q!2 256 \
			  [list -framechecksum 0 -filechecksum 0 \
			      -metadatacheck 0]

    CheckSameness ${s3_start}-${s3_end} \
			  H R H1:LSC-AS_Q,H2:LSC-AS_Q!2 $dt
    
    flush [outputChannel]        
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr2807::test
