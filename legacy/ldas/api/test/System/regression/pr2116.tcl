#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2116::test {

#========================================================================
# This tests for pr2116.
#========================================================================
#========================================================================
# Establish ranges - These values should be changed as needed at the
#  test data evolves.
#========================================================================

# Hanford
    set LHO_Start 747346352
# Livingston
    set LLO_Start 747350032

    set ifo_metadata [ list \
		   longitude \
		   latitude \
		   elevation \
		   armXazimuth \
		   armYazimuth \
		   armXaltitude \
		   armYaltitude \
		   armXmidpoint \
		   armYmidpoint ]

    set ifo_data [ list ]

    set ifo "LHO_4k"
    lappend ifo_data \
    ${ifo},prefix "H1" \
    ${ifo},longitude "-2.0840570926666260e+00" \
    ${ifo},latitude "8.1079500913619995e-01" \
    ${ifo},elevation "1.4255400e+02" \
    ${ifo},armXazimuth "5.6548781e+00" \
    ${ifo},armYazimuth "4.0840812e+00" \
    ${ifo},armXaltitude "-6.1950000e-04" \
    ${ifo},armYaltitude "1.2500000e-05" \
    ${ifo},armXmidpoint "1.9975420e+03" \
    ${ifo},armYmidpoint "1.9975220e+03" \

    set ifo "LHO_2k"
    lappend ifo_data \
    ${ifo},prefix "H2" \
    ${ifo},longitude "-2.0840570926666260e+00" \
    ${ifo},latitude "8.1079500913619995e-01" \
    ${ifo},elevation "1.4255400e+02" \
    ${ifo},armXazimuth "5.6548781e+00" \
    ${ifo},armYazimuth "4.0840812e+00" \
    ${ifo},armXaltitude "-6.1950000e-04" \
    ${ifo},armYaltitude "1.2500000e-05" \
    ${ifo},armXmidpoint "1.0045000e+03" \
    ${ifo},armYmidpoint "1.0045000e+03"

    set ifo "LLO_4k"
    lappend ifo_data \
    ${ifo},prefix "L1" \
     ${ifo},longitude "-1.5843089818954468e+00" \
     ${ifo},latitude "5.3342300653457642e-01" \
     ${ifo},elevation "-6.5739999e+00" \
     ${ifo},armXazimuth "4.4031782e+00" \
     ${ifo},armYazimuth "2.8323801e+00" \
     ${ifo},armXaltitude "-3.1209999e-04" \
     ${ifo},armYaltitude "-6.1069999e-04" \
     ${ifo},armXmidpoint "1.9975750e+03" \
     ${ifo},armYmidpoint "1.9975750e+03"

    array set ifos $ifo_data
    
    set cmdstem { concatFrameData
    -subject { pr2116 - framecpp armYaltitude output }
	-framequery {
	    { R ${IFO} {} ${Start}-${Finish} adc(${Prefix}:LSC-AS_Q) }
	}
	-returnprotocol result.ilwd
	-outputformat { ilwd ascii }
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2116 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2116::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    proc ValidateDetector { Detector ldascmd start } {
        if  { [ catch {
            set name [ namespace current ]
            regexp {[']([^:]*)::Detector:Frame} $Detector -> Site
            set error ""
            foreach metadata [ set ::${name}::ifo_metadata ] {
                if  { [ regexp name='$metadata'>(\[^<\]*)< $Detector -> Value ] } {
                    test regression:[ set ::${name}::thispr ]:$ldascmd:Start=$start:ifos=$Site:$metadata:validate {} -body {
                        if  { ! [ string equal [set ::${name}::ifos(${Site},${metadata}) ] $Value ] } {
	                        return "${Site}:${metadata}: $Value instead of [ set ::${name}::ifos(${Site},${metadata}) ]"
                        } 
                    } -result {}
                } else {
                    append error "regexp name='$metadata'>(\[^<\]*)< $Detector -> Value failed\n" ]
                }
            }
            set error [ string trim $error ]
            if  { [ string length $error ] } {
                return -code error $error
            }
        } err ] } {
            return -code error $err
        }
    }
   
    proc Check { Start } {
    #--------------------------------------------------------------------
    # These values should not be altered
    #--------------------------------------------------------------------
        set name [ namespace current ]
        if  { [ catch {
            set DeltaT 1
            set Finish [ expr $Start + $DeltaT - 1 ]
            set LHO_Start [ set ::${name}::LHO_Start ]
            if  { $Start == $LHO_Start } {
	            set IFO "H"
	            set Prefix "H1"
	            set Detectors { "H1" "H2" }
            } else {
	            set IFO "L"
	            set Prefix "L1"
	            set Detectors { "L1" }
            }
            set cmd [ subst [ set ::${name}::cmdstem ] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            set expected "Your results.+result.ilwd"
            
            test regression:[ set ::${name}::thispr ]:$ldascmd:Start$Start:submit {} -body {
                if  { [ catch {
                    upvar Job Job
                    SubmitJob Job $cmd
                    upvar outputs outputs               
                    set outputs $Job(outputs)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected

    #====================================================================
    # Examine the ilwd that was generated
    #====================================================================

            if  { [ info exist outputs ] } {
                set url $outputs
                set data [LJread $url]

                set detprocs [ regexp -all -inline {<ilwd[^>]*?Detector:Frame.+?</ilwd>} $data ]
                foreach detproc $detprocs {
	                ValidateDetector $detproc $ldascmd $Start
                }
	        }
        } err ] } {
            return -code error $err
        }
    } ;## end of Check
    
    ;## main
    foreach time [ list $LHO_Start $LLO_Start ] {
        if  { [ catch {
            Check $time
        } err ] } {
            Puts 0 "Check $time error: $err"
        }
    }
    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2116::test
