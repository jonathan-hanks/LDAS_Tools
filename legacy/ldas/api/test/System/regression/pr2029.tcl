#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2029::test {

#========================================================================
# The start time given below is for S2 data
#========================================================================
    set Start 730524016

#========================================================================
# Should not have to modify any variables below this point
#========================================================================
#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------
    set Dt 32
    set EndQuery [ expr $Start + $Dt - 1]
    set EndFreqSeq [ expr $Start + 1 ]

    set cmdstem1 { dataPipeline
    -subject { 
        pr2029 name conflict occurs for two different channels from datacond to frame #1 
    }
    -framequery {
      { R H {} $Start-$EndQuery Adc(H0:PEM-LVEA_SEISZ,H1:LSC-AS_Q,H1:LSC-AS_I) }
    }
    -aliases {
      asq = LSC-AS_Q;
      asi = LSC-AS_I;
    }
    -algorithms {
      # Sequence
      s1 = ramp(16384, 0.1, 1.0);
      s2 = ramp(16384, -1.0, 0.1);

      # TimeSeries
      ts1 = slice(asq, 0, 16384, 1);
      ts2 = slice(asi, 0, 16384, 1);
      ph = div(3.14159265, 2.0);
      ts3 = mix(ph, 0.2, ts1);

      # FrequencySequence (and transfer function)
      sc = complex(s1, s2);
      f = fseries(sc, -64.0, 1.0, $Start, 0, $EndFreqSeq, 0);

      # PSD and CSD
      p = psd(ts1, 16384);
      p3 = psd(ts3, 16384);
      c = csd(ts1, ts2, 16384);

      output(ts1, _, _, ts1, TimeSeries 1);
      output(ts2, _, _, ts2, TimeSeries 2);
      output(ts3, _, _, ts3, TimeSeries 3);
      output(f, _, _, fs1, FrequencySequence);
      output(p, _, _, psd, PSD);
      output(p3, _, _, psd3, PSD);
      output(c, _, _, csd, CSD);
    }
    -datacondtarget frame
    -setsingledc 1
    }
   
    set cmdstem2 { conditionData
    -subject { 
        pr2029 name conflict occurs for two different channels from datacond to frame #2 
    }
    -framequery {
	{ {} {} file:$JobDir/Z-P_LDAS_${JobId}_datacondAPI_LDAS-$Start-1.gwf {}
	    Proc(psd3!-8192!16384!,ts1,ts2,psd!0!8193!,ts3,fs1!-64!16384!,csd!0!8193!) }
    }      
    -algorithms {
      output(_ch0, _, _, ch0, comment);
      output(_ch1, _, _, ch1, comment);
      output(_ch2, _, _, ch2, comment);
      output(_ch3, _, _, ch3, comment);
      output(_ch4, _, _, ch4, comment);
      output(_ch5, _, _, ch5, comment);
      output(_ch6, _, _, ch6, comment);
    }
    -datacondtarget frame
    -outputformat { ilwd ascii }
    -setsingledc 1 
    }  
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2029
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr2029::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]

    foreach case [ list cmdstem1 cmdstem2 ] {
        set cmd [ subst -nobackslashes [ set $case ] ]
        set ldascmd [ lindex $cmd 0 ] 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        Puts 5 "cmd $cmd"
        set expected "Your results.+datacondAPI.+gwf"
        test regression:$thispr:$ldascmd:Start=$Start:Dt=$Dt:submit {} -body {
            if  { [ catch {
                    upvar JobId JobId
                    upvar JobDir JobDir
                    SubmitJob Job $cmd                               
                    set JobDir $Job(outputDir)
                    set JobId $Job(jobnum)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
        } -match regexp -result $expected
        if  { ! [ info exist JobDir ] } {
            break
        }
    }

#========================================================================
# Execute a command 
#========================================================================
#========================================================================
# Actually perform the action described in the Problem report
#========================================================================

#set Start 733543750
set Start 730522192
set Dt 1024
set CalRef1 729320845
set RefOffset 5283
set CalRefDt 63
set End [ expr $Start + $Dt ]
set End64 [ expr $Start + 64 ]
set CalRef2 [ expr $CalRef1 + $RefOffset ]
set CalRef1End [ expr $CalRef1 + $CalRefDt ]
set CalRef2End [ expr $CalRef2 + $CalRefDt ]

  set cmdstem3 { conditionData
    -subject { pr2029 name conflict occurs for two different channels from datacond to frame }
    -returnprotocol	file:H2H1-CAL+FS+DEBUG_4-${Start}-${Dt}.gwf
    -outputformat  	frame
    -datacondtarget	datacond
    -setsingledc  	1
    -framequery { { R H {} ${Start}-${End} adc(H2:LSC-AS_Q,H1:LSC-AS_Q) }
	{ SenseMonitor_V02_H2_M H {} ${Start}-${End} adc(H2:CAL-CAV_FAC.mean,H2:CAL-OLOOP_FAC.mean) }
	{ SenseMonitor_V02_H1_M H {} ${Start}-${End} adc(H1:CAL-CAV_FAC.mean,H1:CAL-OLOOP_FAC.mean) }
	{ CAL_REF H {} ${CalRef1}-${CalRef1End} proc(H2:CAL-CAV_GAIN!0!7000.0000!,H2:CAL-RESPONSE!0!7000.0000!) }
	{ CAL_REF H {} ${CalRef2}-${CalRef2End} proc(H1:CAL-CAV_GAIN!0!7000.0000!,H1:CAL-RESPONSE!0!7000.0000!) }
    }
    -aliases {
	ifo1_asq_chan  = H2\:LSC-AS_Q;
	ifo1_alpha_ts  = H2\:CAL-CAV_FAC.mean;
	ifo1_gamma_ts  = H2\:CAL-OLOOP_FAC.mean;
	ifo1_sensing   = H2\:CAL-CAV_GAIN;
	ifo1_reference = H2\:CAL-RESPONSE;
	ifo2_asq_chan  = H1\:LSC-AS_Q;
	ifo2_alpha_ts  = H1\:CAL-CAV_FAC.mean;
	ifo2_gamma_ts  = H1\:CAL-OLOOP_FAC.mean;
	ifo2_sensing   = H1\:CAL-CAV_GAIN;
	ifo2_reference = H1\:CAL-RESPONSE;
    }
    -algorithms { # datacond algorithm:
	############################
	### Calculating the time-dependent response function for H2
	############################
	alphas = double(ifo1_alpha_ts);
	gammas = double(ifo1_gamma_ts);
	alphas = complex(alphas);
	gammas = complex(gammas);
        ### From the S1 Calibration document, signal(f) = response(f) * AS_Q(f),
        ### where     response(f) = ( 1 + gamma(t) open_gain(f,t_0) ) / alpha(t) sensing(f,t_0)
        ### with         gamma(t) = alpha(t) * beta(t)
        ### and  open_gain(f,t_0) = sensing(f,t_0) * reference(f,t_0) - 1
	alpha_t = getElement(alphas,0);
	gamma_t = getElement(gammas,0);
	ifo1_open_gain = mul(ifo1_sensing,   ifo1_reference);
        ifo1_open_gain = sub(ifo1_open_gain, 1);
        ifo1_response = mul(gamma_t,       ifo1_open_gain);
        ifo1_response = add(1,             ifo1_response);
        ifo1_response = div(ifo1_response, ifo1_sensing);
        ifo1_response = div(ifo1_response, alpha_t);
	ifo1_response = fseries(ifo1_response, 0.0, 0.015625, ${Start}, 0, ${End}, 0);

	############################
	### Calculating the time-dependent response function for H1
	############################
	alphas = double(ifo2_alpha_ts);
	gammas = double(ifo2_gamma_ts);
	alphas = complex(alphas);
	gammas = complex(gammas);
        ### From the S1 Calibration document, signal(f) = response(f) * AS_Q(f),
        ### where     response(f) = ( 1 + gamma(t) open_gain(f,t_0) ) / alpha(t) sensing(f,t_0)
        ### with         gamma(t) = alpha(t) * beta(t)
        ### and  open_gain(f,t_0) = sensing(f,t_0) * reference(f,t_0) - 1
	alpha_t = getElement(alphas,0);
	gamma_t = getElement(gammas,0);
	ifo2_open_gain = mul(ifo2_sensing,   ifo2_reference);
        ifo2_open_gain = sub(ifo2_open_gain, 1);
        ifo2_response = mul(gamma_t,       ifo2_open_gain);
        ifo2_response = add(1,             ifo2_response);
        ifo2_response = div(ifo2_response, ifo2_sensing);
        ifo2_response = div(ifo2_response, alpha_t);
	ifo2_response = mul(ifo2_response, .77);
	ifo2_response = fseries(ifo2_response, 0.0, 0.015625, ${Start}, 0, ${End}, 0);
	

	### output the new, calculated response functions for each IFO
	output(ifo1_response, _, frame, H2:CAL-RESPONSE, H2:CAL-RESPONSE_FSERIES_0-7000);
	output(ifo2_response, _, frame, H1:CAL-RESPONSE, H1:CAL-RESPONSE_FSERIES_0-7000);
	

	############################
	### Calculating the Discrete Fourier Transforms of XX:LSC-AS_Q
	############################
	### From the Matlab help section, the DFT returns the same number of points as
	### we start with, but only the first (n/2) + 1 points are unique.  Thus we'll
	### slice off a power of 2 time series data and then slice again after we fft.
	### Note that the actual Fourier Transform is scaled by the sampling interval,
	### but we also include the negative frequency power by doubling the result.
	ifo1_fseries = slice(ifo1_asq_chan,0,1048576,1);
	ifo1_fseries = fft(ifo1_fseries);
	ifo1_fseries = div(ifo1_fseries, 8192);
	ifo1_fseries = slice(ifo1_fseries,0,448000,1);
	ifo1_fseries = fseries(ifo1_fseries, 0., 0.015625, ${Start}, 0, ${End64}, 0);
	ifo2_fseries = slice(ifo2_asq_chan,0,1048576,1);
	ifo2_fseries = fft(ifo2_fseries);
	ifo2_fseries = div(ifo2_fseries, 8192);
	ifo2_fseries = slice(ifo2_fseries,0,448000,1);
	ifo2_fseries = fseries(ifo2_fseries, 0., 0.015625, ${Start}, 0, ${End64}, 0);


	
	### output the FFT's for the AS_Q channels
	output(ifo1_fseries,  _, frame, H2:LSC-AS_Q_FFT,  H2:LSC-AS_Q_FSERIES_0-7000);
	output(ifo2_fseries,  _, frame, H1:LSC-AS_Q_FFT,  H1:LSC-AS_Q_FSERIES_0-7000);
    }
    }
    ;## retrain alias backslash
    set cmd [ subst -nobackslashes $cmdstem3 ]
    set ldascmd [ lindex $cmd 0 ] 
    regexp -- {-subject \{([^\}]+)} $cmd -> subject
    set subject [ string trim $subject ]
    set expected "Your results.+file_H2H1-CAL.+gwf"
    test regression:$thispr:$ldascmd:Start=$Start:Dt=$Dt:submit {} -body {
        if  { [ catch {
            SubmitJob Job $cmd
            upvar outputs outputs               
            set outputs $Job(outputs)
            set reply $Job(jobReply)
        } err] } {
            Puts 5 "job $Job(command)"
            if  { [ info exist Job(error) ] } {                        
                set joberror $Job(error)
            } else {
                set joberror $err
            }
            LJdelete Job
            return -code error $joberror
        }
        LJdelete Job
        return $reply
    } -match regexp -result $expected    

    flush [outputChannel]
#========================================================================
# Exit with the negation of return value since programs terminate
#   successfully with 0, unsuccessfully otherwise
#========================================================================
    cleanupTests
} ;## namespace ::QA::pr2029::test
