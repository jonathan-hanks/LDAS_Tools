#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr2108::test {

    set FirstFrame 730702000
    set SecondsDir 100
    set SecondsOfData 1000
    set SecondsPerFrame 16
   
    set cmdstem { createRDS 
        -subject { pr2108 - frame files crossing directory boundaries }
        -times ${Start}-${finish}
	    -ifo {}
	    -type R
	    -usertype ${usertype}
	    -channels H2\:LSC-AS_Q!${ResampleRate}
	    $Options
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr2108 - ---no-metadata-check option
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::pr2108::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    ;## pick up list of channels
    source  $::QA::LDAS_ROOT/lib/test/regression/pr2793ChannelList.tcl
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
     
#-----------------------------------------------------------------------
# Calculates the first frame in a directory
#-----------------------------------------------------------------------
    proc calc_dir_start { DirOffset } {
        set name [ namespace current ]
        set SecondsPerFrame [ set ::${name}::SecondsPerFrame ]
        set offset [ expr ( [ set ::${name}::FirstFrame ] + \
            ( [ set ::${name}::SecondsDir ] * $DirOffset ) ) ]
        set offset [ expr ( ( $offset + $SecondsPerFrame - 1 ) / \
			    $SecondsPerFrame ) * $SecondsPerFrame ]
        return $offset
    }

    proc do_rds { Message Start DeltaT ResampleRate } {
    
        set name [ namespace current ]
        set Options [list -framechecksum 0 -filechecksum 0 -metadatacheck 0]
        set thispr [ set ::${name}::thispr ]
        set usertype RDS_PEMCROSS_PHIL
        set expected_output_count [ expr $DeltaT / [ set ${name}::SecondsPerFrame ] ]
        set finish [ expr ${Start} + $DeltaT - 1 ]
        set cmd [ subst [ set ::${name}::cmdstem ] ]
        set ldascmd createRDS 
        regexp -- {-subject \{([^\}]+)} $cmd -> subject
        set subject [ string trim $subject ]
        set expected "Your results.+$usertype.+.gwf"
        set tag Start=$Start:DeltaT=$DeltaT:Resample=$ResampleRate
        test regression:$thispr:$ldascmd:$subject:$tag:submit {} -body {
            if  { [ catch {
                upvar Job Job
                upvar jobDone jobDone
                SubmitJob Job $cmd
                Puts 5 $cmd
                set jobDone 1
                if  { [ info exist Job(jobReply) ] } {
                    set reply $Job(jobReply)
                } else {
                    set reply none
                }           
            } err] } {
                if  { [ info exist Job(error) ] } {                        
                    set joberror "$Job(error) $err"
                }
                set jobDone -1
                return -code error $joberror
            }
            return $reply
        } -match regexp -result $expected
        
        #----------------------------------------------------------------
	    # Get a list of files that were generated
	    #----------------------------------------------------------------
    
        if  { $jobDone > -1 } {
            test regression:$thispr:$ldascmd:$subject:$tag:validate {} -body {
                return [ llength [ regexp -all -inline $usertype $Job(outputs) ] ] 
            } -result $expected_output_count
        }
        LJdelete Job    
    }

    set deltat [ expr 3 * $SecondsPerFrame ]
    #********************************************************************
    # Test when the data does exist, just in the previous directory
    #********************************************************************
    
    if  { [ catch {
        do_rds "Resample 1" [ calc_dir_start 1 ] $deltat 8
    } err ] } {
        Puts 0 "Resample 1 error: $err"
    }
    #********************************************************************
    # Test when the data does exist in the current directory
    #********************************************************************
    if  { [ catch {
	    do_rds "Resample 2" [ expr [ calc_dir_start 1 ] + $SecondsPerFrame ] $deltat 8
    } err ] } {
        Puts 0 "Resample 2 error: $err"
    }
    #********************************************************************
    # Test when the data does exists split between two directories
    #********************************************************************
    if  { [ catch {
        do_rds "Resample 3" \
		     [ expr [ calc_dir_start 1 ] - $SecondsPerFrame ] $deltat 8
    } err ] } {
        Puts 0 "Resample 3 error: $err"
    }
   
    LJdelete Job
   ;##==================================================================
   ;## Testing is complete
   ;##==================================================================
   flush [outputChannel]
   cleanupTests
} ;## namespace ::QA::pr2108::test
