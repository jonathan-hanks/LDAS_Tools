#!/bin/sh
# -*-: mode: tcl c-basic-offset: 4; indent-tabs-mode: nil -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::pr1997::test {
 
#========================================================================
# The start time given below is for S2 data
#========================================================================

    set DSO "libldasknownpulsardemod.so.0.0.0"

#------------------------------------------------------------------------
# First part of command to seed the data values
#------------------------------------------------------------------------

    set cmdstem1 { concatFrameData
        -subject { pr1997 - start_freq wrong when initial gap exists in requested SFTs }
	    -returnprotocol { http://result.ilwd }
	    -concatenate 0
	    -allowgaps 1
	    -outputformat { ilwd ascii }
	    -framequery {
	        P_LDAS_KPD_SFT_60_H1 H {} {${Start}-${Finish}}
	        Proc(0!${FreqBase}FREQ!3.6!)
	    }
    }
    
    set cmdstem2  { dataPipeline
    -subject { pr1997 - start_freq wrong when initial gap exists in requested SFTs }
	-returnprotocol http://testexpressFstat.xml
	${opt_mddapi}
	-database ldas_tst
	${opt_dynlib}
	-np 2
	-filterparams (2,1,${Start},2.44140625e-4,7372800,60,245760,${FrameCount},5.147162148,0.376696069,0.0,1283.5,2,-8.663317101e-14,0.0,0.0,0.0,0.0,'n',0,0.0,0.0,2.0,0.0,1,0.0,0.0,0.0,0.0,0.0,0.0,1,"H1","LHO","J1939",input,${FrameCount},216.0,${FreqBase},3.6,1,1)
	-autoexpand 1
	-concatenate 0
	-allowgaps 1
	-datacondtarget $DatacondTarget
	-outputformat { ilwd ascii }
	-responsefiles {
	    file:${::QA::LAL::earth03},pass
	    file:${::QA::LAL::sun03},pass
	}
	-aliases sft
	-algorithms {
	    output(_chN,ascii,,_chN,_chN data);
	}
	-framequery {
	    P_LDAS_KPD_SFT_60_H1 H {} {${Start}-${Finish}}
	    Proc(0!${FreqBase}FREQ!3.6!)
	}
    }
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing pr1997
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::pr1997::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    
    set thispr [ string toupper [file rootname [ file tail $argv0 ] ] ]
    
    proc CheckFrame { Desc Start Dt FreqBase expected } {
        set name [ namespace current ]
        if  { [ catch {
            set Finish [ expr $Start + $Dt - 1 ]
            set cmd [ subst -nobackslashes [ set ::${name}::cmdstem1 ] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            test regression:[ set ::${name}::thispr ]:$ldascmd:Desc=$Desc:Start=$Start:Dt=$Dt:FreqBase=$FreqBase:submit {} -body {
                if  { [ catch {
                    SubmitJob Job $cmd  
                    upvar urls urls
                    set urls $Job(outputs)
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected
            
	#----------------------------------------------------------------
	# Process the ilwd to see if it looks correct 
	#----------------------------------------------------------------
            if  { [ info exist urls ] } {
                test regression:[ set ::${name}::thispr ]:$ldascmd:Desc=$Desc:Start=$Start:Dt=$Dt:FreqBase=$FreqBase:validate {} -body {
	                foreach url $urls {
	                    set data [LJread $url]
	                    regexp {startx='([^']*)'} $data -> freq
	                    if  { [ expr abs ( $freq - $FreqBase) ] > 0 } {
		                    error "The FrequencyBase has the wrong value: $freq instead of $FreqBase"
                        }
                    }
                } -result {}
            }
        } err ] } {
            return -code error "[info level 0 ]: $err"
        }
    }

    proc Check { Desc DatacondTarget Start Dt FreqBase FrameCount expected } {
        set name [ namespace current ]
    #====================================================================
    # This job is a modified version from the job in the pr to dump the
    #   contents to an ilwd so verification of key pieces can be made
    #====================================================================
        if  { [ catch {
            if { $DatacondTarget == "datacond" } {
	            set opt_mddapi ""
	            set opt_dynlib "-dynlib {}"
            } else {
	            set opt_mddapi "-mddapi ligolw"
	            set opt_dynlib "-dynlib [ set ::${name}::DSO ]"
            }
            set Finish [ expr $Start + $Dt - 1 ]
            set cmd [ subst -nobackslashes [ set ::${name}::cmdstem2] ]
            set ldascmd [ lindex $cmd 0 ] 
            regexp -- {-subject \{([^\}]+)} $cmd -> subject
            set subject [ string trim $subject ]
            Puts 5 "cmd $cmd"
            
            test regression:[ set ::${name}::thispr ]:$ldascmd:Desc=$Desc:Start=$Start:Dt=$Dt:FreqBase=$FreqBase:FrameCount=$FrameCount:$DatacondTarget:submit {} -body {
                if  { [ catch {
                    SubmitJob Job $cmd  
                    upvar urls urls
                    set urls $Job(outputs)       
                    set reply $Job(jobReply)
                } err] } {
                    if  { [ info exist Job(error) ] } {                        
                        set joberror $Job(error)
                    } else {
                        set joberror $err
                    }
                    LJdelete Job
                    return -code error $joberror
                }
                LJdelete Job
                return $reply
            } -match regexp -result $expected

	        if  { $DatacondTarget == "datacond" && [ info exist urls ] } {
	    #------------------------------------------------------------
	    # Process the ilwd to see if it looks correct 
	    #------------------------------------------------------------
                test regression:[ set ::${name}::thispr ]:$ldascmd:$Desc:Start=$Start:Dt=$Dt:FreqBase=$FreqBase:FrameCount=$FrameCount:validate {} -body {
                    set errors ""
	                foreach file $urls {
		                if { ! [ regexp {testexpressFstat.ilwd} $file match ] } {
		                    continue
		                }
		                set data [LJread $file]

		                set fbases [ regexp -all -inline {name='FrequencyBase'>([^<']*)<} $data ]
		                foreach freq_pattern $fbases {
		                    regexp {name='FrequencyBase'>([^<']*)<} $freq_pattern -> freq
		                    if { [ expr  abs ( $freq - $FreqBase) ] > 0 } {
                                append errors "The FrequencyBase has the wrong value: $freq instead of $FreqBase"
                            }
                        }
                    }
                    if  { [ string length $errors ] } {
                        return -code error $errors
                    }
                } -result {}
            }
        } err ] } {
            return -code error $err
        }
    }

#========================================================================
# Run through a series of tests
#========================================================================

    CheckFrame "frame check" 730523880 1800 1281.7 "Your results.+ilwd"
    Check "data check" datacond 730523880 1800 1281.7 19 "No metadata for data pipeline.+Your results.+ilwd"
    Check "pr check" wrapper 730523880 1800 1281.7 19 "mpi::newJobCallback"

#========================================================================
# Return with an exit status that reflects the jobs run.
#========================================================================
    flush [outputChannel]        
    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
} ;## namespace ::QA::pr1997::test
