#! /ldcg/bin/tclsh
;## does not work for stochastic, tfcluster and stackslide
;## problem with sorting xml file

catch { exec ./standalone.tcl -s gateway } data
set date [ clock format [ clock seconds ] -format "%m%d" ]

set state 0
set output testlogs/standalone$date.ver
set fd [ open $output w ]

foreach line [ split $data \n ] {
	if	{ [ regexp -- {Writing ([^\.]+).schema} $line -> dso ] } {
		set state 0
		continue
	}

	if	{ [ regexp {Sending putStandAlone} $line ] } {
		set state 1
		continue
	}
	if	{ [ regexp {Sending dataPipeline} $line ] } {
		set state 2
		continue
	}
	switch $state {
		1 { if { [ regexp {Your results:.+(/ldas_outgoing/jobs/LDAS-DEV\S+)} $line -> dir ] } {
				puts $fd "\n$dso putStandAlone $dir"
				set putstandalone($dso) $dir
		  	}
		 }
		2 { if { [ regexp {Your results:.+(/ldas_outgoing/jobs/LDAS-DEV\S+)} $line -> dir ] } {
			puts $fd "$dso dataPipeline $dir"
			set datapipeline($dso) $dir
			if	{ ! [ string equal knownpulsardemod $dso ] } {
				set file knownpulsardemod_result
			} else {
				set file *
			}
			set cmd "exec sort [ glob [ set putstandalone($dso) ]/*xml ] > ${dso}1" 
			catch { eval $cmd } err1
			set cmd "exec sort [ glob [ set datapipeline($dso) ]/*xml ] > ${dso}2" 
			catch { eval $cmd } err2	
			puts $fd $err1
			puts $fd $err2	
			catch { exec diff ${dso}1 ${dso}2 } err 
			puts $fd $err
			set state 0
			file delete ${dso}1 ${dso}2
		  }
		 }
	}
}
close $fd 
catch { exec egrep -v "(process:process_id|wrapperAPI|GPS|abnormally|/ldas_outgoing|---)" $output } err
set fd [ open $output a+ ]
if	{ [ regexp {^[\n\s\dc,]+$} $err ] } {
	puts $fd PASSED
} else {
	puts $fd "FAILED\n$err"
}		
close $fd
