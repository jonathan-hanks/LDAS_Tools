#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

set ::auto_path ". /ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
puts "auto_path $auto_path, [ auto_execok use_ligotools ] "
#------------------------------------------------------------------------
# Establish GLOBALS
#------------------------------------------------------------------------
namespace eval ::tcltest {
    variable debug 1
}
set ::real 0

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::QA::Options::Add {} {-real} "" \
    {Only operate on real values} \
    { set ::real 1 }
::QA::Options::Add {} {-frame} {string} \
    {Operate on a specific frame} \
    { set ::frame $::QA::Options::Value }
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit
#------------------------------------------------------------------------
# Prepare to use the TCL test harness
#------------------------------------------------------------------------
package require qatest
#------------------------------------------------------------------------
# Prepare to communicate with LDAS
#------------------------------------------------------------------------
package require LDASJob

### MAIN ###

::tcltest::DebugPuts 1 "Initializing...\n"

set TYPE R_std_test_frame_ver6
set IFOS T
set TIMES 600000000-600000000

;## :TODO: add tests for detectSim and strain
set structList {
    FrameH
    AdcData
    Detector
    Event
    History
    Msg
    ProcData
    SerData
    SimData
    SimEvent
    StatData
    Summary
}

array set FrameH {
    name        lstring
    run         int_4s
    frame       int_4u
    dataQuality int_4u
    GTimeS      int_4u
    GTimeN      int_4u
    ULeapS      int_2u
    dt          real_8
}

array set AdcData {
    name          lstring
    comment       lstring
    channelGroup  int_4u
    channelNumber int_4u
    nBits         int_4u
    bias          real_4
    slope         real_4
    units         lstring
    sampleRate    real_8
    timeOffsetS   int_4s
    timeOffsetN   int_4u
    fShift        real_8
    phase         real_4
    dataValid     int_2u
}

array set Detector {
    name         lstring
    longitude    real_8
    latitude     real_8
    elevation    real_4
    armXazimuth  real_4
    armYazimuth  real_4
    armXaltitude real_4
    armYaltitude real_4
    armXmidpoint real_4
    armYmidpoint real_4
    localTime    int_4s
    dataQuality  int_4u
    qaBitList    lstring
}

array set Event {
    name        lstring
    comment     lstring
    inputs      lstring
    GTimeS      int_4u
    GTimeN      int_4u
    timeBefore  real_4
    timeAfter   real_4
    eventStatus int_4u
    amplitude   real_4
    probability real_4
    statistics  lstring
    nParam      int_2u
}

array set History {
    name    lstring
    time    int_4u
    comment lstring
}

array set Msg {
    alarm    lstring
    message  lstring
    severity int_4u
    GTimeS   int_4u
    GTimeN   int_4u
}

array set ProcData {
    name        lstring
    comment     lstring
    sampleRate  real_8
    timeOffsetS int_4u
    timeOffsetN int_4u
    fShift      real_8
    phase       real_4
}

array set SerData {
    name       lstring
    timeSec    int_4u
    timeNsec   int_4u
    sampleRate real_4
    data       lstring
}

array set SimData {
    name       lstring
    comment    lstring
    sampleRate real_4
    fShift     real_8
    phase      real_4
}

array set SimEvent {
    name       lstring
    comment    lstring
    inputs     lstring
    GTimeS     int_4u
    GTimeN     int_4u
    timeBefore real_4
    timeAfter  real_4
    amplitude  real_4
}

array set StatData {
    name           lstring
    comment        lstring
    representation lstring
    timeStart      int_4u
    timeEnd        int_4u
    version        int_4u
}

array set Summary {
    name    lstring
    comment lstring
    test    lstring
    GTimeS  int_4u
    GTimeN  int_4u
}

#array set TrigData {
#    name          lstring
#    comment       lstring
#    inputs        lstring
#    GTimeS        int_4u
#    GTimeN        int_4u
#    timeBefore    real_4
#    timeAfter     real_4
#    triggerStatus int_4u
#    amplitude     real_4
#    probability   real_4
#    statistics    real_4
#}

set exceptions {
    Event:GTimeS
    Event:GTimeN
    FrameH:GTimeS
    FrameH:GTimeN
    Msg:GTimeS
    Msg:GTimeN
    SimEvent:GTimeS
    SimEvent:GTimeN
    SerData:timeSec
    SerData:timeNsec
    Summary:GTimeS
    Summary:GTimeN
    ProcData:timeOffsetS
    ProcData:timeOffsetN
}

set nested {
    Detector
    History
}

array set expval {
    char       a
    char_u     b
    int_2u     1
    int_4u     2
    int_8u     3
    int_2s    -1
    int_4s    -2
    int_8s    -3
    real_4     1.0000000e+00
    real_8     2.0000000000000000e+00
    lstring    TesT
    complex_8  {3.0000000e+00 4.0000000e+00}
    complex_16 {5.0000000000000000e+00 6.0000000000000000e+00}
}

proc sendjob {cmd} {
    catch {LJrun job -nowait \
	       -manager ${::HOST}:${::PORT} \
	       -globus $::USE_GLOBUS_CHANNEL \
	       -gsi $::USE_GSI $cmd} err
    if {[ info exists err ] && ! [regexp $::QA::JobIDPattern $err]} {
	error "Error while starting LJrun: $err"
    }
    if {$LJerror} {
        set errmsg $::job(error)
        error "Error from LJrun: $errmsg"
    }

    ::tcltest::DebugPuts 1 $::job(jobid)

    catch {LJwait job}
    if {$LJerror} {
        set errmsg [trimReply $::job(error)]
        error "Error from LJrun: $errmsg"
    }

    ::tcltest::DebugPuts 1 $::job(outputs)
    set urlList $::job(outputs)
    LJdelete job

    return $urlList
}

proc geturl {url} {
    set file "temp[clock seconds][clock clicks].ilwd"
    #set file [file tail $url]

    if {[catch {LJcopy $url $file}]} {
        catch {file delete $file}
        error "Error retrieving output file from $url."
    }

    if {[catch {open $file r} fid]} {
        error "Error opening output file $file: $fid"
    }
    set ilwd [read -nonewline $fid]
    catch {close $fid}
    catch {file delete $file}

    return $ilwd
}

set usercmd {
    getFrameElements
    -returnprotocol http://foo
    -outputformat {ilwd ascii}
    -framequery { {$::TYPE} {$::IFOS} {} {$TIMES} $query }
}

if {$::real} {
    ;## skip the tests for structures that don't yet exist
    ;## in production frames
    ::tcltest::configure -skip {3:*}
}

;## Test Set 1
;## Get full structure in ilwd
;## Extract the elements, types, and values
;## Compare with expected types and values
::tcltest::DebugPuts 1 "****************"
::tcltest::DebugPuts 1 "* TEST SUITE 1 *"
::tcltest::DebugPuts 1 "****************"
foreach struct $structList {

    set query $struct
    append query "(0)"

    set branch "1,0"
    if {[lsearch -exact $nested $struct] != -1} {
        set branch "2,0"

    }

    ;## kludge to determine if a test failed or skipped
    set failed 0
    set skipped 0
    if {[info exists ::tcltest::numTests]} {
        set failed $::tcltest::numTests(Failed)
        set skipped $::tcltest::numTests(Skipped)
    }

    ::tcltest::test 1:${struct} {} {
        #set ilwd [getFrameElements $frame $query]
        set urlList [sendjob [subst -nobackslashes $usercmd]]
        set ilwd [geturl [lindex $urlList 0]]

        ::tcltest::DebugPuts 1 $ilwd
        set ilwdx [extractILWD $ilwd $branch 1]
        ::tcltest::DebugPuts 1 $ilwdx

        regexp -line {^(.+)$} $ilwd -> head
        list $head
    } {<?ilwd?>}
    LJdelete job

    ;## if the test failed or skipped, go to the next structure
    if {$::tcltest::numTests(Failed) > $failed
            || $::tcltest::numTests(Skipped) > $skipped} {
        continue
    }

    foreach item $ilwdx {
        foreach {type att val} $item {break}
        array set attr $att
        set class($attr(name)) $type
        set value($attr(name)) $val
    }

    ;## print list of elements received
    puts $::tcltest::outputChannel "Found attribute names:\n   [join [lsort [array names class]] "\n   "]"
    flush $::tcltest::outputChannel

    foreach name [lsort [array names $struct]] {
        ;## can't check name field because it is not
        ;## an individual element in the ilwd but rather
        ;## specified in the container header <ilwd name=....>
        if {[string equal "name" $name]} {continue}

        set desc ${struct}:${name}
        set type [set ${struct}($name)]

        set res {}
        if {[lsearch -exact $exceptions $desc] != -1} {
            regexp {^(.+)([SN])} $name -> name res
            switch -exact -- $res {
                S {set value($name) [lindex $value($name) 0]}
                N {set value($name) [lindex $value($name) 1]}
                default {}
            }
        }

        ::tcltest::test 1:${desc} {} {
            if {[lsearch [array names class] $name] == -1} {
                error "Can't find '$name'"
            }

            concat $class($name) [expr {$real?"":$value($name)}]
        } [concat $type [expr {$real?"":$expval($type)}]]

        flush $::tcltest::outputChannel
        after 1000
    }

    array unset class
    array unset attr
    array unset value
    puts $::tcltest::outputChannel ""
    flush $::tcltest::outputChannel
}

;## Test Set 2
;## Query for individual elements in structure
;## Compare with expected name and type
::tcltest::DebugPuts 1 "****************"
::tcltest::DebugPuts 1 "* TEST SUITE 2 *"
::tcltest::DebugPuts 1 "****************"
foreach struct $structList {
    foreach name [lsort [array names $struct]] {
        set desc ${struct}:${name}
        set type [set ${struct}($name)]

        set res {}
        if {[lsearch -exact $exceptions $desc] != -1} {
            regexp {^(.+)([SN])} $name -> name res
        }
        set query ${struct}[string toupper $name 0 0]
        append query "(0)"

        tcltest::test 2:${desc} {} {
            #set ilwd [getFrameElements $frame $query]
            set urlList [sendjob [subst -nobackslashes $usercmd]]
            set ilwd [geturl [lindex $urlList 0]]

            ::tcltest::DebugPuts 1 $ilwd
            set retval [extractILWD $ilwd]
            ::tcltest::DebugPuts 1 $retval

            foreach {class att val} [lindex $retval 0] {break}
            array set attr $att

            switch -exact -- $res {
                S {set val [lindex $val 0]}
                N {set val [lindex $val 1]}
                default {}
            }

            concat $attr(name) $class [expr {$real?"":$val}]
        } [concat $name $type [expr {$real?"":$expval($type)}]]

        array unset attr
    }
}

;## Test Set 3
::tcltest::DebugPuts 1 "****************"
::tcltest::DebugPuts 1 "* TEST SUITE 3 *"
::tcltest::DebugPuts 1 "****************"
;## kludge to determine if a test failed or skipped
set failed 0
set skipped 0
if {[info exists ::tcltest::numTests]} {
    set failed $::tcltest::numTests(Failed)
    set skipped $::tcltest::numTests(Skipped)
}

set query "AdcData(0)"

::tcltest::test 3:AdcData:Vector {} {
    #set ilwd [getFrameElements $frame AdcData(0)]
    set urlList [sendjob [subst -nobackslashes $usercmd]]
    set ilwd [geturl [lindex $urlList 0]]

    ::tcltest::DebugPuts 1 $ilwd
    set vect [extractILWD $ilwd 2,13]
    ::tcltest::DebugPuts 1 $vect

    regexp -line {^<([^ ]+) } $ilwd -> tag
    list $tag
} {ilwd}

;## continue only if the test passed
if {($::tcltest::numTests(Failed) == $failed) && ($::tcltest::numTests(Skipped) == $skipped)} {
    foreach item $vect {
        foreach {type att val} $item {break}
        switch -exact -- $type {
            char -
            char_u {
                set value($type) [string index [subst $val] 0]
            }

            complex_8 -
            complex_16 {
                set value($type) [lrange $val 0 1]
            }

            default {
                set value($type) [lindex $val 0]
            }
        }
    }

    ;## print list of types received
    puts $::tcltest::outputChannel "Found types:\n   [join [array names value] "\n   "]"
    flush $::tcltest::outputChannel

    foreach type [array names expval] {
        tcltest::test 3:DataClass:$type {} {
            if {[lsearch [array names value] $type] == -1} {
                error "Can't find '$type' type"
            }
            set value($type)
        } "$expval($type)"

        flush $::tcltest::outputChannel
        after 1000
    }

    array unset value
}

tcltest::cleanupTests

