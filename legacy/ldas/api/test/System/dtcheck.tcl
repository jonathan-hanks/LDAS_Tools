#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path ". /ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
puts "auto_path $auto_path, [ auto_execok LDASJobH ] "

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::dtcheck::test {
    ;##==================================================================
    ;## What to bring in from other namespaces
    ;##==================================================================
    namespace import ::tcltest::*
    namespace import ::QA::Debug::Puts
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::Read
    ;##==================================================================
    ;## Namespace variables
    ;##==================================================================
    variable DataRun S2
    variable DataSet
    array set DataSet {
        ;##-------------------------------------------------------------
        ;## E7
        ;##-------------------------------------------------------------
        E7:StartTime 693960016
        E7:Tolerance [expr {1/16384.0} ]
        E7:Channel H0:PEM-LVEA_SEISX
        ;##-------------------------------------------------------------
        ;## S1
        ;##-------------------------------------------------------------
        S1:StartTime 714384016
        S1:Tolerance [expr {1/16384.0} ]
        S1:Channel H0:PEM-LVEA_SEISX
        ;##-------------------------------------------------------------
        ;## E9
        ;##-------------------------------------------------------------
        E9:StartTime 727550016
        E9:Tolerance [expr {1/16384.0} ]
        E9:Channel H0:PEM-LVEA_SEISX
        ;##-------------------------------------------------------------
        ;## S2
        ;##-------------------------------------------------------------
        S2:StartTime 730524016
        S2:Tolerance [expr {1/16384.0} ]
        S2:Channel H0:PEM-LVEA_SEISX
    } ;## DataSet

    variable TRanges [list t (t+3) \
        t-(t+3) t-(t+15) t-(t+16) t-(t+19) t-(t+31) t-(t+32) t-(t+35) \
        (t+3)-(t+6) (t+3)-(t+15) (t+3)-(t+16) (t+3)-(t+19) \
        (t+3)-(t+31) (t+3)-(t+32) (t+3)-(t+35) ]

    ;##==================================================================
    ;## Testing procedures
    ;##==================================================================
    proc Init { } {
        ;##--------------------------------------------------------------
        ;## Variables needed from the namespace
        ;##--------------------------------------------------------------
        variable DataRun
        variable DataSet
        variable TRanges
        ;##--------------------------------------------------------------
        ;## Finish initializing time ranges
        ;##--------------------------------------------------------------
        set t $DataSet(${DataRun}:StartTime)
        regsub -all {[\(\)]} $TRanges {} TRanges
        regsub -all {t} $TRanges {$t} TRanges
        regsub -all {\$t\+\d+} $TRanges {[expr {&}]} TRanges
        set TRanges [subst $TRanges]
    }
    ;##==================================================================
    ;## Main Testing
    ;##==================================================================
    ;## Get ready for testing
    ;##------------------------------------------------------------------
    Init

    set channel $DataSet(${DataRun}:Channel)
    regsub {:} $channel {\:} echannel

    ;##------------------------------------------------------------------
    ;## Test several types of commands
    ;##------------------------------------------------------------------
    foreach command  {getFrameData getFrameElements concatFrameData} {
        ;##--------------------------------------------------------------
        ;## Over several time ranges
        ;##--------------------------------------------------------------
        foreach trange $TRanges {
            set failures [list]
            set usercmd "$command
                    -returnprotocol http://foo
                    -outputformat {ilwd ascii}
                    -framequery { R H {} $trange Adc($channel) }"
            test "dtcheck:${command}:${trange}" {} -body {
                ;##------------------------------------------------------
                ;## Run the job
                ;##------------------------------------------------------
                if [catch {SubmitJob job $usercmd} err] {
                    return $err
                }
            } -result {}
            ;##----------------------------------------------------------
            ;## Process results
            ;##----------------------------------------------------------
            set dttotal 0
            set dtexpect 1.0
            if { [regexp -- {-} $trange] } {
                set dtexpect [eval expr {1.0 - ($trange)}]
            }
            foreach url $job(outputs) {
                Read $url ilwd
                set ilwdx [extractILWD $ilwd]
                Puts 20 $ilwdx

                set firstdt 1
                foreach item $ilwdx {
                    foreach {type att val} $item {break}
                    array set attr $att

                    if {[string equal "dt" $attr(name)]} {
                        if {[string equal "getFrameData" $command] && $firstdt} {
                            set firstdt 0
                            continue
                        }
                        set dtchan [expr {$val}]
                        set dttotal [expr {$dttotal + $dtchan}]
                    } elseif {[string equal "sampleRate" $attr(name)]} {
                        set srate [expr {$val}]
                    } elseif {[string equal $echannel $attr(name)]} {
                        set duration [expr {$attr(dims) / $srate}]
                        test "dtcheck:${command}:$trange:dt=?duration" {} -body {
                            expr {$duration}
                        } -result $dtchan
                    }
                }
            }
        } ;## foreach trange
    } ;## foreach command

    ;##==================================================================
    ;## Cleanup
    ;##==================================================================
    cleanupTests

} ;## namespace - QA::dtcheck::test
namespace delete ::QA::dtcheck::test
