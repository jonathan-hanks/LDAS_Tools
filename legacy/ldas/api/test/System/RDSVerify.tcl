#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec ${TCLSH} "$0" ${1+"$@"}


#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for createRDS user command verification 
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./RDSVerify.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================
if { [info exists env(SRCDIR)] } {
    foreach path $env(SRCDIR) {
     lappend ::auto_path $path
    }
}
if { [info exists env(PREFIX)] } {
    lappend ::auto_path $env(PREFIX)/lib/genericAPI
} elseif { [file isdirectory /ldas/lib/genericAPI] } {
    lappend ::auto_path /ldas/lib/genericAPI
}
lappend ::auto_path . /ldas/lib/test /ldas/lib
if { ! [ regexp "/ldas/bin" $::env(PATH) ] } {
    set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::RDSVerify::test {
    #--------------------------------------------------------------------
    # check run time options
    #--------------------------------------------------------------------
    set 	Verify 1
    set 	minJobLength     16
    set 	frameLength      16
    set 	defaultJobLength 64
    set 	test2run \
        	[ list \
              TestOneToOne TestMultiFrame TestConcatFrame \
              TestMultiConcatFrame \
              TestOneToOneS3_L TestMultiFrameS3_L  TestConcatFrameS3_L \
              TestMultiConcatFrameS3_L \
              TestConcatFrame60 TestConcatFrame256 \
              TestMultiFrameE11 TestConcatFrameE11 TestMultiConcatFrameE11 \
             ]
    set 	dumpdir ""
    set 	dumpOnly 0
    
	;## 30 min long delay
    set	delay 1800
    set outputdir /ldas_outgoing/frames/RDSVerify
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

::QA::Options::Add {} {--dump} {string} \
    {Directory where to dump script files} \
    {set ::QA::RDSVerify::test::dumpdir $::QA::Options::Value;\
     set ::QA::RDSVerify::test::dumpOnly 1}
    
::QA::Options::Add {} {--verify} {string} \
    {verify frame output} \
    {set ::QA::RDSVerify::test::VERIFY $::QA::Options::Value}
    
::QA::Options::Add {} {--job_frame_time} {string} \
    {frame duration for this job} \
    {set ::QA::RDSVerify::test::defaultJobLength $::QA::Options::Value}
    
::QA::Options::Add {} {--tests} {string} \
    {tests to run} \
    {set ::QA::RDSVerify::test::test2run $::QA::Options::Value}    
    
::QA::Options::Add {} {--outputdir} {string} \
    {output frame directory} \
    {set ::QA::RDSVerify::test::outputdir $::QA::Options::Value}   
    
::QA::Options::Add {} {--startTime} {string} \
    {start frame time} \
    {set ::QA::RDSVerify::test::test2run $::QA::Options::Value}    
    
::QA::Options::Add {} {--endTime} {string} \
    {end frame time} \
    {set ::QA::RDSVerify::test::endTime $::QA::Options::Value}   
             
if { [info exists ::argv] \
         && ! [ regexp -- {[-]?-dump} $argv ] } {
    package require LDASJob
    package require genericAPI
}

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::RDSVerify::test {
	##-------------------------------------------------------------------
    	## Bring commonly used items into the local namespace
    	##-------------------------------------------------------------------
    	namespace import ::tcltest::*
    	namespace import ::QA::Debug::*
    	namespace import ::QA::TclTest::SubmitJob
    	namespace import ::QA::URL::List

#========================================================================
# Proc to retrieve the resampling of a channel
#   It returns the channel of the form <channel name>!<resample factor>
#========================================================================
proc GetChannelResample { DataSet Site Channel } {
    set channel [lsearch -regexp $::QA::DataSet($DataSet,$Site,level1) \
                     ^${Channel}(!\[0-9\]+)?$ ]
    set channel [lindex $::QA::DataSet($DataSet,$Site,level1) $channel]
    set fact 1
    set channel_name $channel
    regexp -- {([^!]+)[!]([0-9]+)} $channel -> channel_name fact
    return "${channel}!${fact}"
}

#========================================================================
#========================================================================
proc Delay {Leader Query Timeout} {
    Puts 1 "$Leader: Started waiting for syncing of diskcache [clock format [clock seconds]]"
    set start_wait [clock seconds]
    ::QA::DiskCache::WaitOnCache $Query $Timeout
    Puts 1 "$Leader: Waited [expr [clock seconds] - $start_wait] seconds for syncing diskcache"
}

#========================================================================
# Proc to check creation of concatinated frames
#========================================================================
proc TestConcatFrame {args} {
    set data_set $::QA::CurrentDataSet
    set testname ConcatFrame
    foreach val $args {
        #----------------------------------------------------------------
        # Determine if the argument is an option or a value
        #----------------------------------------------------------------
        switch -glob -- $val {
            -* {
                set option $val
                set val ""
            }
        }
        #----------------------------------------------------------------
        # Assign the value to the option
        #----------------------------------------------------------------
        switch -exact -- $option {
            -data_set {
                set data_set $val
            }
            -secperframe {
                set secperframe $val
            }
            -testname {
                set testname $val
            }
        }
    }
    if { ![info exists secperframe]} {
        set secperframe "-secperframe [expr 2 * $::QA::DataSet($data_set,dt)]"
    } else {
        set secperframe ""
    }
    return [eval TestGeneral $testname \
                $secperframe \
                $args]
}

#========================================================================
# Proc to handle general testing
#========================================================================
proc TestGeneral {TestName args} {

    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*
    set retval 0
    ;## this var cannot be called outputdir or error no such variable
    set outdir [ namespace eval $name set outputdir ]
    Puts 1 "[ info level -1 ] args $args outputdir $outdir "

    if	{ [ catch {
        #----------------------------------------------------------------
        # Establish default values
        #----------------------------------------------------------------
        #----------------------------------------------------------------
        # Calculate the start and end times
        #----------------------------------------------------------------
        regexp -- {^([0-9]+)-([0-9]+)$} [ set ::QA::RDSVerify::test::TimeRange ] -> start_time end_time
        set ifo "H"
        set data_set $::QA::CurrentDataSet
        #----------------------------------------------------------------
        # Parse argument list
        #----------------------------------------------------------------
        foreach val $args {
            #------------------------------------------------------------
            # Determine if the argument is an option or a value
            #------------------------------------------------------------
            switch -glob -- $val {
                -* {
                    set option $val
                    set val ""
                }
            }
            #------------------------------------------------------------
            # Assign the value to the option
            #------------------------------------------------------------
            if {[info exists option]} {
                regsub -- {^-} $option "" option_name
                set $option_name $val
         	  }
        }
        if {![info exists dt]} {
            set dt [expr $::QA::DataSet($data_set,dt) * 16]
        }
        if {[info exists start]} {
            set start_time $start
        }
        set type_name "${TestName}_${data_set}_"
        set TestName "${TestName}(${data_set})"

        #--------------------------------------------------------------------
        # generate the list of options
        #--------------------------------------------------------------------
        set rds_options ""
        if {[info exists val]} {
            unset val
        }
        foreach opt $::QA::RDS::valid_options {
            if {[info exists $opt]} {
                upvar 0 $opt val
                set rds_options "$rds_options -$opt $val"
                unset val
            }
        }
        set end_time [expr $start_time + $dt - 1]
        set time_range "$start_time-$end_time"
        #--------------------------------------------------------------------
        # Verify the generation of level 1 RDS frames
        #--------------------------------------------------------------------
        set sframes [ expr ( ( ( $end_time + 1 ) - $start_time ) / $::Dt ) + 2 ]
        set rframes [ expr ( ( $end_time + 1 ) - $start_time ) /  $::Dt ]
        #--------------------------------------------------------------------
        # Create Level 1 RDSs frome the Raw Frames
        #--------------------------------------------------------------------
        set orig_rds_options $rds_options
        if { [string equal $ifo L] } {
            set rds_options "$rds_options -framechecksum 0"
        }
	   
	if { $::QA::RDSVerify::test::dumpOnly } {
	    set rds_options "$rds_options -dumpdir [ set ::QA::RDSVerify::test::dumpdir ]"
	}
	
	test RDSVerify:frame:createRDS:$TestName:Level1 {} -body {
	    if	{ [ catch {
		set retval [eval ::QA::RDSCreate {"$TestName: Creation of Level 1: "} \
				$time_range \
				[list $::QA::DataSet($data_set,$ifo,level1)] \
				R ${type_name}1 $ifo \
				-outputdir $outdir $rds_options ]
		if { $retval == 0 } {	   	  
		    error "$TestName level 1 failed"
		}
	    } err ] } {
		return $err
	    }
	} -result {}
	
        #--------------------------------------------------------------------
        # Give some time for the files to get onto the disk and into the
        #  the diskcache
        #--------------------------------------------------------------------
        if { ! $::QA::RDSVerify::test::dumpOnly } {
            Delay $TestName "-ifo { $ifo } -types { ${type_name}1 } -times { $time_range }" \
		  [expr [ set ::QA::RDSVerify::test::delay ] * 1000]
        }
        #--------------------------------------------------------------------
        # Create Level 2/3 RDSs from the Level 1 RDSs
        #--------------------------------------------------------------------
        if {[info exists ::QA::DataSet($data_set,$ifo,level2)]} {
            set level 2
        } else {
            set level 3
        }
	test RDSVerify:frame:createRDS:$TestName:Level$level {} -body {
	    if	{ [ catch {		
		set retval [eval ::QA::RDSCreate {"$TestName: Creation of Level ${level}: "} \
				$time_range \
				[list $::QA::DataSet($data_set,$ifo,level${level})] \
				${type_name}1 ${type_name}${level}  $ifo \
				-outputdir $outdir $rds_options]
		if { $retval == 0 } {
		    error "$TestName level ${level} failed"
		}
	    } err ] } {
		return $err
	    }
	} -result {}
        #--------------------------------------------------------------------
	# Restore the options after the commands have been dumped
        #--------------------------------------------------------------------
        set rds_options $orig_rds_options
        #--------------------------------------------------------------------
        # Give some time for the files to get onto the disk and into the
        #  the diskcache
        #--------------------------------------------------------------------
        if { ! $::QA::RDSVerify::test::dumpOnly } {
            Delay $TestName "-ifo { $ifo } -types { ${type_name}${level} } -times { $time_range }" \
		  [expr [ set ::QA::RDSVerify::test::delay ] * 1000]
        }
        #--------------------------------------------------------------------
        # Verify the generation of level 2 RDS frames (optional)
        #--------------------------------------------------------------------
        set retval 1
        if { [ set ::QA::RDSVerify::test::Verify ] \
		 && ! $::QA::RDSVerify::test::dumpOnly } {
            set retval [Verify "${TestName}: Verification: " \
                            [expr $start_time - $::Dt] \
                            [expr $end_time + $::Dt] $sframes R \
                            $start_time $end_time $rframes ${type_name}${level} \
                            $data_set $ifo level${level}]
	}
        if { $retval == 0 } {
	    set retval 1
            error "$TestName verify failed"
        }
    } err ] } {
	Puts 1 "$TestName error $err"
    }
    return $retval
}

#========================================================================
# Proc to check creation of Multiple frames per file
#========================================================================
proc TestMultiConcatFrame {args} {
    set data_set $::QA::CurrentDataSet
    foreach val $args {
        #----------------------------------------------------------------
        # Determine if the argument is an option or a value
        #----------------------------------------------------------------
        switch -glob -- $val {
            -* {
                set option $val
                set val ""
            }
        }
        #----------------------------------------------------------------
        # Assign the value to the option
        #----------------------------------------------------------------
        switch -exact -- $option {
            -data_set {
                set data_set $val
            }
        }
    }
    return [eval TestGeneral MultiConcatFrame \
                -framesperfile 2 \
                -secperframe [expr 2 * $::QA::DataSet($data_set,dt)] \
                $args ]
}

#========================================================================
# Proc to check creation of Multiple frames per file
#========================================================================
proc TestMultiFrame {args} {
    return [eval TestGeneral MultiFrame -framesperfile 2 $args]
}

#========================================================================
# Proc to check creation of One-To-One RDSs
#========================================================================
proc TestOneToOne {args} {
    return [eval TestGeneral OneToOne $args]
}

#========================================================================
# Proc to check creation of Multiple frames per file
#========================================================================
proc TestMultiConcatFrameS3_L {args} {
    return [eval TestMultiConcatFrame -ifo L -framedatavalid 0 $args]
}

#========================================================================
# Proc to check creation of Concat frames of 8 seconds in length
#========================================================================
proc TestConcatFrame60 {args} {
    set data_set $::QA::CurrentDataSet
    set ifo H
    foreach val $args {
        #----------------------------------------------------------------
        # Determine if the argument is an option or a value
        #----------------------------------------------------------------
        switch -glob -- $val {
            -* {
                set option $val
                set val ""
            }
        }
        #----------------------------------------------------------------
        # Assign the value to the option
        #----------------------------------------------------------------
        switch -exact -- $option {
            -data_set {
                set data_set $val
            }
        }
    }
    set start [ expr $::QA::DataSet($data_set,start) + \
                   $::QA::DataSet($data_set,dt) ]
    set duration 60
    return [eval TestConcatFrame \
                -testname ConcatFrame${duration} \
                -secperframe $duration \
                -allowshortframes 1 \
                -start $start \
                -dt [expr 5 * $duration] \
                $args]
}

#========================================================================
# Proc to check creation of Concat frames of 256 seconds in length
#========================================================================
proc TestConcatFrame256 {args} {
    set data_set $::QA::CurrentDataSet
    set ifo H
    foreach val $args {
        #----------------------------------------------------------------
        # Determine if the argument is an option or a value
        #----------------------------------------------------------------
        switch -glob -- $val {
            -* {
                set option $val
                set val ""
            }
        }
        #----------------------------------------------------------------
        # Assign the value to the option
        #----------------------------------------------------------------
        switch -exact -- $option {
            -data_set {
                set data_set $val
            }
        }
    }
    set start [ expr $::QA::DataSet($data_set,start) + \
                   $::QA::DataSet($data_set,dt) ]
    set duration 32
    return [eval TestConcatFrame \
                -testname ConcatFrame256 \
                -secperframe 256 \
                -allowshortframes 1 \
                -start $start \
                -dt $duration \
                $args]
}

#========================================================================
# Proc to check creation of Concat frames for LLO
#========================================================================
proc TestConcatFrameS3_L {args} {
    return [eval TestConcatFrame -ifo L -framedatavalid 0 $args]
}

#========================================================================
# Proc to check creation of Multiple frames per file for LLO, turn off datavalid flag
#========================================================================
proc TestMultiFrameS3_L {args} {

    set ::QA::RDSVerify::test::test_status [expr [TestMultiFrame \
                                 -ifo L -framedatavalid 0 ] && [ set ::QA::RDSVerify::test::test_status] ]
}

#========================================================================
# Proc to check creation of One-To-One RDSs for LLO, turn off datavalid flag
#========================================================================
proc TestOneToOneS3_L {args} {
    set ::QA::RDSVerify::test::test_status [expr [TestOneToOne -ifo L \
                                 -framedatavalid 0 ] && [ set ::QA::RDSVerify::test::test_status] ]
}

#========================================================================
# Proc to check creation of MultiFrame RDSs for E11
#========================================================================
proc TestMultiFrameE11 {args} {
    set start [ expr $::QA::DataSet(E11pre1,start) + \
                    $::QA::DataSet(E11pre1,dt) * 2]
    set dt [expr $::QA::DataSet(E11pre1,dt) * 16]

    set ::QA::RDSVerify::test::test_status [expr [TestMultiFrame \
                                 -ifo C -data_set E11pre1 \
                                 -framechecksum 0 -filechecksum 0 \
                                 -start $start \
                                 -dt $dt] && [ set ::QA::RDSVerify::test::test_status]]

}

#========================================================================
# Proc to check creation of Concat frame RDSs for E11
#========================================================================
proc TestConcatFrameE11 {args} {
    set start [ expr $::QA::DataSet(E11pre1,start) + \
                    $::QA::DataSet(E11pre1,dt) * 2]
    set dt [expr $::QA::DataSet(E11pre1,dt) * 16]

    set ::QA::RDSVerify::test::test_status [expr [TestConcatFrame \
                                 -testname ConcatFrameE11 \
                                 -ifo C -data_set E11pre1 \
                                 -framechecksum 0 -filechecksum 0 \
                                 -start $start \
                                 -dt $dt] && [ set ::QA::RDSVerify::test::test_status] ]

}

#========================================================================
# Proc to check creation of Multi Concat frame RDSs for E11
#========================================================================
proc TestMultiConcatFrameE11 {args} {
    set start [ expr $::QA::DataSet(E11pre1,start) + \
                    $::QA::DataSet(E11pre1,dt) * 2]
    set dt [expr $::QA::DataSet(E11pre1,dt) * 16]

    set ::QA::RDSVerify::test::test_status [expr [TestMultiConcatFrame \
                                 -ifo C -data_set E11pre1 \
                                 -framechecksum 0 -filechecksum 0 \
                                 -start $start \
                                 -dt $dt] && [ set ::QA::RDSVerify::test::test_status] ]

}

#========================================================================
# proc to validate generated files
#========================================================================
proc Verify { Leader \
                  SourceStartTime SourceEndTime SourceFrames SourceType \
                  ResultStartTime ResultEndTime ResultFrames ResultType \
                  DataSet IFO Level } {
    #--------------------------------------------------------------------
    # Generate the aliases
    #--------------------------------------------------------------------
    set idx 0
    set alias {}
    set src_channel_list [list]
    set result_channel_adc_list [list]
    set result_channel_proc_list [list]
    set retval 1
    foreach channel $::QA::DataSet($DataSet,$IFO,$Level) {
        foreach [list channel_name fact] \
            [split [GetChannelResample $DataSet $IFO $channel] !] {
                break
            }
        if { [string length $fact] == 0} {
            set fact 1
        }
        lappend src_channel_list $channel_name
        regsub -- : $channel_name \\: channel_name
        append alias \
            "ch${idx}Raw=${channel_name}::AdcData:${SourceStartTime}:0:Frame;\n"
        if { $fact == 1 } {
            lappend result_channel_adc_list $channel_name
            append alias \
                "ch${idx}RDS=${channel_name}::AdcData:${ResultStartTime}:0:Frame;\n"
        } else {
            lappend result_channel_proc_list $channel_name
            append alias \
                "ch${idx}RDS=${channel_name}::ProcData:${ResultStartTime}:0:Frame;\n"
        }
        incr idx 1
    }
    #--------------------------------------------------------------------
    # Generate the algorithm section
    #--------------------------------------------------------------------
    set algorithm \
        "numFullFrames = value(${SourceFrames});
         numRDSFrames = value(${ResultFrames});
         SourceStartTime = value(${SourceStartTime});
         ResultStartTime = value(${ResultStartTime});
         ResultEndTime = value(${ResultEndTime});
         # Adding in the missing 1 second
         ResultEndTime = add(ResultEndTime,1);
         # Calculating the startup offset
         timeDelay = sub(ResultStartTime, SourceStartTime);"

    set idx 0
    foreach channel $::QA::DataSet($DataSet,$IFO,$Level) {
        set channel [GetChannelResample $DataSet $IFO $channel]
        set chRaw "ch${idx}Raw"
        set chRDS "ch${idx}RDS"
        regexp -- {([^!]+)[!]([0-9]+)} $channel -> channel_name fact

        append algorithm "

            # CHECKING CHANNEL ${idx}"
        if { $fact == 1 } {
            append algorithm "
            sz_rds = size(${chRDS});
            sz_raw = size(${chRaw});
            start = sub(sz_raw, sz_rds);
            start = div(start, 2);
            cmp = slice(${chRaw}, start, sz_rds, 1);"
        } else {
            append algorithm "
            cmp = resample(${chRaw}, 1, ${fact}, z);
            delay = getResampleDelay(z);
            delay = integer(delay);

            sampleRate = getSampleRate(cmp);
            sz = size(cmp);
            frSize = mul(timeDelay,sampleRate);
            start = add(frSize, delay);
            len = sub(ResultEndTime,ResultStartTime);
            len = mul(len,sampleRate);
            len = integer(len);
            start = integer(start);
            cmp = slice(cmp, start, len, 1);"
        }
        append algorithm "

            err = sub(cmp, ${chRDS});
            mean = mean( err );
            rms = rms( err );
            err = abs(err);
            err = max(err);
            output(mean, _, _, mean${idx}, Mean for channel ${idx});
            output(rms, _, _, rms${idx}, RMS for channel ${idx});
            output(err, _, _, err${idx}, Error for channel ${idx});"

        incr idx 1
    }
    #--------------------------------------------------------------------
    # create command string
    #--------------------------------------------------------------------
    set cmd "
        conditionData
        -outputformat {ilwd ascii}
        -framequery {
            { ${SourceType} $IFO {} ${SourceStartTime}-${SourceEndTime} Adc([join $src_channel_list {,}])}"

    if { [llength $result_channel_adc_list] > 0 } {
        append cmd "
        { ${ResultType} $IFO {} ${ResultStartTime}-${ResultEndTime}
          Adc([join $result_channel_adc_list {,}]) }"
    }
    if { [llength $result_channel_proc_list] > 0 } {
        append cmd "
        { ${ResultType} $IFO {} ${ResultStartTime}-${ResultEndTime}
          Proc([join $result_channel_proc_list {,}]) }"
    }
    append cmd  "
        }
        -aliases {
            $alias
        }
        -algorithms {
            $algorithm
        }
    "
    #--------------------------------------------------------------------
    # Do the actual verification based on conditionData command
    #--------------------------------------------------------------------
    ::QA::RunLDASJob $Leader $cmd -no-delete

    #--------------------------------------------------------------------
    # Check that each channel's abs(mean) is less than 10E-7
    #--------------------------------------------------------------------
    set max_mean 10E-7
	# Puts 1 "output $job(outputs) $job(jobReply), length [ string length $job(outputs)]"
	
	;## info exist $job(outputs) will fail 
	if	{ [ string length $job(outputs) ] } {
    	foreach result $job(outputs) {
        	regexp {^.*/([^/]*)$} $result -> base_name
        	set base_name "/tmp/$base_name"
        	if { [ regexp {^/} $result ] } {
            	# Add http protocol so LJcopy will be able to
            	#   retrieve the file
            	set result "http://${host}${result}"
        	}	
        	LJcopy $result $base_name
        	set ilwdFile [openILwdFile $base_name r]
       	 	set ilwd [readElement $ilwdFile]
        	closeILwdFile $ilwdFile
        	Puts 1 "${Leader}ILwd File contents: [getElement $ilwd]"
        	set idx 0
        	foreach channel $::QA::DataSet($DataSet,$IFO,$Level) {
            	set mean [getContainerElement $ilwd [ expr ${idx} * 3] ]
            	regsub -- {^<.*>(.*)</.*>$} $mean {\1} mean
            	if { [expr abs($mean)] > $max_mean } {
                	set retval 0
                	::QA::puts_fail "${Leader}Mean Verification: The mean absolute value of the mean ([expr abs($mean)]) is greater than expected ($max_mean) for channel $channel"
            	} else {
                	set retval 1
                	Puts 1 "${Leader}Mean Verification: passed"
            	}	
            	incr idx
        	}
        	destructElement $ilwd
        	file delete $base_name ${Leader}
    	}
	} else {
		::QA::puts_fail "${Leader} Verification failed: error $job(error), reply $job(jobReply)"
		set retval 0
	}
    LJdelete job
    return $retval
}

#--------------------------------------------------------------------
# Clean out information from previous runs
#--------------------------------------------------------------------
proc cleanUp {} {
    if	{ ! $::QA::RDSVerify::test::dumpOnly } {
	QA::Rexec $::DISKCACHE_API_HOST ldas \
	    [ subst { rm -rf [ set ::QA::RDSVerify::test::outputdir ] } ]
	QA::Rexec $::DISKCACHE_API_HOST ldas \
	    [ subst { mkdir -p [ set ::QA::RDSVerify::test::outputdir ] } ]
    }
}

#========================================================================
# proc to initialize generated files
#========================================================================

proc Init {} {
    set name [ namespace current ]
    namespace import [ namespace parent $name ]::*

    #--------------------------------------------------------------------
    # Allow the script to be run in a batch mode if not dumping RDS cmd
    #--------------------------------------------------------------------
    if { ! $::QA::RDSVerify::test::dumpOnly } {
        #----------------------------------------------------------------
        # Since not dumping, then we need to cleanup any previous runs
        #  of this test
        #----------------------------------------------------------------
        QA::ConnectToAgent
    }
    if { ![ info exist ::MANAGER_API_HOST ] } {
        set ::MANAGER_API_HOST [ exec uname -n ]
    }
    ;## set by --site parameter
    if { ! [ info exists ::env(LDASMANAGER) ] } {
        set ::env(LDASMANAGER) $::SITE
    }
    #--------------------------------------------------------------------
    # Get LDAS resource values
    #--------------------------------------------------------------------

    if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
        if { ! $::QA::RDSVerify::test::dumpOnly  } {
            source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
        }
    }

    if { $::QA::RDSVerify::test::dumpOnly } {
	set ::QA::RDSVerify::test::outputdir /ldas_outgoing/frames/RDSVerify
    } else {
	if { [ info exist ::DISKCACHE_API_HOST ] } {
	    set ::QA::RDSVerify::test::outputdir \
		[ ::QA::OutputDir $::DISKCACHE_API_HOST RDSVerify ]
	    cleanUp
	} 
    }

    #--------------------------------------------------------------------
    # set up datasets
    #--------------------------------------------------------------------
    ::QA::UseDataSet $::QA::CurrentDataSet
    set ::ResampleChannelList(H,level1) $::QA::ActiveDataSet(H,level1)
    if {[info exists ::QA::ActiveDataSet(H,level2)]} {
        set ::ResampleChannelList(H,level2) $::QA::ActiveDataSet(H,level2)
    } else {
        set ::ResampleChannelList(H,level3) $::QA::ActiveDataSet(H,level3)
    }
    set ::Dt $::QA::ActiveDataSet(dt)
    set start [ expr $::QA::ActiveDataSet(start) + \
                    ( 2 * $::QA::ActiveDataSet(dt) ) ]
    set end [ expr $start + ( 12 * $::QA::ActiveDataSet(dt) ) - 1 ]

    #--------------------------------------------------------------------
    # check run time options
    #--------------------------------------------------------------------
	
    if	{ [ info exist ::QA::RDSVerify::test::startTime ] } {
	set start [ set ::QA::RDSVerify::test::startTime ]
    }
    if      { [ info exist ::QA::RDSVerify::test::endTime  ] } {
        set end [ set ::QA::RDSVerify::test::endTime ]
    }
    set ::QA::RDSVerify::test::TimeRange $start-$end
}

#========================================================================
# Main run once
#========================================================================

set retval 0
Init 
regexp {(\d+)-(\d+)} $TimeRange -> start end

set start 
;## adjust the start
set timeS [expr {$start + 16}]
set timeE [expr {$timeS + $defaultJobLength - 1}]

if { [ catch {

    #----------------------------------------------------------------
    # Setup for tests
    #----------------------------------------------------------------
    set test_status 1
    #----------------------------------------------------------------
    # Run tests on latest science data, defaults to H
    #----------------------------------------------------------------
    foreach test $test2run {
	set rc [ $test ]
	if { [ string length $rc ] } {
	    set test_status [expr $rc && $test_status]
	}
    }


    #----------------------------------------------------------------
    # Run tests on latest science data, for ifo L, has datavalid issue L1:LSC-REFL_Q
    #----------------------------------------------------------------
    #set ::QA::RDSVerify::test::test_status [expr [ TestOneToOne -ifo L ] && [ set ::QA::RDSVerify::test::test_status ]
    #set ::QA::RDSVerify::test::test_status [expr [ TestMultiFrame -ifo L ] && [ set ::QA::RDSVerify::test::test_status]
    #set ::QA::RDSVerify::test::test_status [expr [ TestConcatFrame -ifo L ] && [ set ::QA::RDSVerify::test::test_status ]
    #set ::QA::RDSVerify::test::test_status [expr [ TestMultiConcatFrame -ifo L ] && [ set ::QA::RDSVerify::test::test_status ]

    #----------------------------------------------------------------
    # Verify flags
    #----------------------------------------------------------------
    #TestFlagAllowShortFrames
    # TestFlagGenerateFrameChecksum
    # TestFlagFrameChecksum
    # TestFlagFillMissingDataValid
    #----------------------------------------------------------------
    # Completion of tests
    #----------------------------------------------------------------
    if { [ set ::QA::RDSVerify::test::test_status ] != 1 } {
	error "$test failed"
    }
    QA::puts_pass "PASSED"
} err ] } {
    set retval 1
    QA::puts_fail $err
}

# flush [outputChannel]
flush $::tcltest::outputChannel
#========================================================================
# Testing is complete
#========================================================================
cleanupTests
} ;## namespace - ::QA::RDSVerify::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::RDSVerify::test
exit

