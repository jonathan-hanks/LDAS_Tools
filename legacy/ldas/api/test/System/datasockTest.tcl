#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for createRDS user command 
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datasockTest.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*createRDS_S3*"
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================
if { [info exists env(SRCDIR)] } {
    foreach path $env(SRCDIR) {
     lappend ::auto_path $path
    }
}

if { [info exists env(PREFIX)] } {
    lappend ::auto_path $env(PREFIX)/lib/genericAPI
} elseif { [file isdirectory /ldas/lib/genericAPI] } {
    lappend ::auto_path /ldas/lib
}

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set auto_path "[file dirname $argv0] $::auto_path"
}

if	{ [ file exist /ldas_outgoing/cntlmonAPI/cntlmon.state ] } {
	source /ldas_outgoing/cntlmonAPI/cntlmon.state
} else {
	set ::LDAS ./ldas
	set ::API frame
}
if	{ [ file exist /ldas_outgoing/LDASapi.rsc ] } {
	source /ldas_outgoing/LDASapi.rsc
} else {
	source /ldas/lib/LDASapi.rsc
}
package require genericAPI
package require generic

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------

namespace eval ::QA::datasockTest::test {
    set API "test_driver"
    set hostlist {}

    set cmds_sent 0
    set test_count 0
    set test_completed 0
    set wait 1
    set nowait 0
    set localhost [ exec uname -n ]
    
    ;## set some default values
    set client {}
    set server {}
    set dimset { 1 10 100 1000 10000 100000 1000000 }
    set types { char_s char_u int_2s int_2u int_4s int_4u int_8s int_8u real_4  \
        real_8 lstring_2 lstring_4 lstring_8 lstring_16 complex_8 complex_16} 
    set thread 0
    set withcont 1
    set samples 10
    set hostfile ""
    set ilwdfmt "binary"
    set logdir ""
    set testScript datasockClientServer.tcl
    set clientServerPattern [ file tail $testScript ]
    set resultdir ""
    set clearPrevious 1
    set ilwdformat binary
    set ilwdGenerator /ldas_usr/ldas/test/datasock/ilwd/elementilwd
    
    ;## official results of releases to compare
    set former_resultdir /ldas_usr/ldas/test/datasock
    set restart 1
    set desc(wallrate) "Wall Time Rate (MBytes/sec)"
    set desc(wallmean) "Wall Time Mean (seconds)"
    set desc(wallsd) "Wall Time Standard Seviation (seconds)"
    set desc(wallcov) "Wall Time Coefficient of Variation (%)"
    set desc(cpumean) "CPU Time Mean (seconds)"
    set desc(cpusd) "CPU Time Standard Deviation (seconds)"
    set desc(cpucov) "CPU Time Coefficient of Variation (%)"
    
    set bgcolor(1) "bgcolor=\"#ffe6ef\""
    set bgcolor(10) "bgcolor=\"#ffe6ab\""
    set bgcolor(100) "bgcolor=\"#ffe6cd\""
    set bgcolor(1000) "bgcolor=\"#ffe669\""
    set bgcolor(10000) "bgcolor=\"#ffe6ef\""
    set bgcolor(100000) "bgcolor=\"#ffe6ab\""
    set bgcolor(1000000) "bgcolor=\"#ffe6cd\""

    set platform(opterondata-dev) "Sun Opteron AMD"
    set platform(controlmon) "Linux Box"
    set platform(metaserver) "Centos 5"
    set platform(beowulf) "Centos 5"
    set platform(datacon) "Centos 5"
    
    set alternate(datacon) beowulf
    set alternate(opterondata) dataserver
    set alternate(dataserver) opterondata
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--hostfile} {string} \
    {hostfile name} \
    { set ::QA::datasockTest::test::hostfile $::QA::Options::Value }  
::QA::Options::Add {} {--ilwdformat} {string} \
    {ilwd format binary or ascii} \
    { set ::QA::datasockTest::test::ilwdfmt $::QA::Options::Value ;
      set ::QA::datasockTest::test::ilwdfmtCL 1 }   
 ::QA::Options::Add {} {--former-resultdir} {string} \
    {stats result directory for releases} \
    { set ::QA::datasockTest::test::former_resultdir $::QA::Options::Value }    
 ::QA::Options::Add {} {--resultdir} {string} \
    {stats result directory} \
    { set ::QA::datasockTest::test::resultdir $::QA::Options::Value }
 ::QA::Options::Add {} {--dims} {string} \
    {dims} \
    { set ::QA::datasockTest::test::dimset [ split $::QA::Options::Value , ] }
 ::QA::Options::Add {} {--clearPrevious} {string} \
    {dims} \
    { set ::QA::datasockTest::test::clearPrevious [ split $::QA::Options::Value , ] }
::QA::Options::Add {} {--ilwddir} {string} \
    {directory of ilwd test data files} \
    { set ::QA::datasockTest::test::ilwddir $::QA::Options::Value }                                                    	  
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::datasockTest::test {

	##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	
    set date [ clock format [ clock seconds ] -format "%m%d" ]
     
    ;## subdirectory to keep logs
    set logdir $::QA::LDAS_OUTGOING_ROOT/tmp/datasock/logs-$date
    
    ;## parent directory to which results are stored in subdirectory results-mmdd
    #set resultdir $::QA::LDAS_OUTGOING_ROOT/tmp/datasock/results-$date
    
    set DOMAINNAME $::LDAS_SYSTEM

    ;## domainname is queried from the system
    array set $DOMAINNAME [ list test_client $localhost ]
    array set $DOMAINNAME [ list test_server $localhost ]
    
    set TESTCLIENTPORT 7000
    #array set test_client  "host     $localhost"
    array set test_client  "operator  [ expr { $TESTCLIENTPORT +  0} ]"
    array set test_client  "emergency [ expr { $TESTCLIENTPORT +  1 } ]"
    array set test_client  "data      [ expr { $TESTCLIENTPORT +  2 } ]"

    set TESTSRVRPORT 9000
    array set test_server  "host      $localhost"
    array set test_server  "operator  [ expr { $TESTSRVRPORT +  0} ]"
    array set test_server  "emergency [ expr { $TESTSRVRPORT +  1 } ]"
    array set test_server  "data      [ expr { $TESTSRVRPORT +  2 } ]" 
	;## if this is not running on frame host, use ssh to execute it on frame host
	
	if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
		source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    } elseif { [ file exist /ldas/lib/genericAPI/LDASapi.rsc ] } {
		source /ldas/lib/genericAPI/LDASapi.rsc 
	} 
        
    ;## run ok from gateway
    ;## if run on opterondata-dev has connection refuse problems 
    QA::ConnectToAgent
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## addhosts - add host to list of testing hosts
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc addhosts {hosts {restart 0}} {
        set name [ namespace current ]
        foreach host $hosts {
            if { [ lsearch -exact [ set ::${name}::hostlist ] $host ] == -1 } {
                lappend ::${name}::hostlist $host
                set restart 1
            }
            if {$restart} {
                tester stop $host
                after 1000
                tester start $host
            }
        }

        after 3000
        return
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## cleanup - reset host list
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc cleanup {} {
        set name [ namespace current ]

        foreach host [ set ::${name}::hostlist ] {
            tester stop $host
        }
        set ::${name}::hostlist {}
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## tester - start or stop tests
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc tester {type host} {
        set name [ namespace current ]
        switch -exact -- $type {
            stop {Puts 1 "cleanup $host"; doStop $host }
            start {Puts 1 "startup $host"; doStart $host }
            default {}
        }             
    }

    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## getPids - return pids of client/server on remote hosts 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc getPids { host {re {}}} {
        set name [ namespace current ]
        set user $::tcl_platform(user)
        set cmd "ps -u $user -o pid,args"
        Puts 5 "getPids for $host" 
        if  { [ string equal [ set ::${name}::localhost ] $host ] } {
            catch { exec eval $cmd } psinfo
        } else {
            catch { Rexec $host $user "$cmd" } psinfo
        }
        
        ;##if {![string length $psinfo]} {return {}}
        set psinfo [lrange [split $psinfo "\n"] 1 end]

        set pidlist {}
        append exp {(\d+)[\w\s\t\[\.\/]+(?:tclsh[\w\s\t\.\/]+)?} "$re"
        foreach line $psinfo {
            if  {[regexp -- $exp $line -> pid]} {
            Puts 5 "$pid for '$line'"
                lappend pidlist $pid
            }
        }
        return $pidlist
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## printPids 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc printPids {pidlist} {
        if {![llength $pidlist]} {
            return
        }
        catch {exec ps -p "$pidlist" -o user,pid,pcpu,pmem,vsz,rss,stime,time,args >@stdout 2>@stderr} err
    }

    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## start client or server on host 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc doStart { host } {

        set name [ namespace current ]
        set localhost [lindex [split [info hostname] "."] 0]
        
        if  {[string equal -length 4 "ldas" $localhost]} {set localhost gateway}
        
        set newenv LD_LIBRARY_PATH=/ldas/lib:$::env(LD_LIBRARY_PATH)

        ;## Check for pre-existing processes
        set pids [ ::${name}::getPids $host [ set ::${name}::clientServerPattern ] ]
        Puts 5 "doStart pids $pids"
        if  {[llength $pids ]} {
            if  {![ set ::${name}::restart ]} {
                Puts 1 "Existing Processes:"
                return
            }

            doStop $host
            after 1000
        }

        set logdir [ set ::${name}::logdir]
      
        if {! [ file exists $logdir] } {
            catch {file mkdir $logdir } err
        }

        set ::${name}::logfileS [file join $logdir ${host}_server].log
        set ::${name}::logfileC [file join $logdir ${host}_client].log
        foreach logfile "[ set ::${name}::logfileS ] [ set ::${name}::logfileC ]" {
            if {[file exists ${logfile}]} {
                set timestamp [clock format [file mtime ${logfile}] -format "%m%d%H%M"]
                catch {file rename -force -- ${logfile} ${logfile}.${timestamp}} err
            }
        }
        
        set testScript [ set ::${name}::testScript ]
        set remote_testdir [ set ::${name}::resultdir ]
        set cmd "/usr/bin/env $newenv nohup $testScript --testmode \"test_server\" \
        --resultdir $remote_testdir \
        --qa-debug-level $::QA::Debug::Level >& [ set ::${name}::logfileS ] &; \
        /usr/bin/env $newenv nohup $testScript --testmode \"test_client\" \
        --resultdir $remote_testdir --ilwddir [ set ::${name}::ilwddir ] \
        --qa-debug-level $::QA::Debug::Level >& [ set ::${name}::logfileC ] &;"
        
        if  { [ string equal [ set ::${name}::localhost ] $host ] } {
            catch {exec $cmd} err
            Puts 5 "local cmd $cmd\n: $err"
        } else {
            catch { Rexec $host ldas $cmd } err
            Puts 5 "Rexec $cmd\n: $err"
        } 
        after 1000
        ;## wait for process to be up before connecting to it
        for { set i 1 } { $i < 10000 } { incr i 1000 } {
            if  { [ catch { 
                set pids [getPids $host [ set ::${name}::clientServerPattern ] ]
            } err ] } {
                return -code error "[ info level 0 ]: $cmd error: $err"
            }
            if  { [ string length $pids ] } {
                return
            }
            after 1000
        } 
        return -code error "$cmd unable to execute after 10 secs"    
    }  
       
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## doStop: stop client or server
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc doStop { host } {
        set name [ namespace current ]
        set pids [::${name}::getPids $host  [ set ::${name}::clientServerPattern ] ]
        set cmd { kill -TERM $pid >& /dev/null; kill -KILL $pid >& /dev/null}
        foreach pid $pids {
            set realcmd [ subst -nobackslashes $cmd ]
            if  { [ string equal [ set ::${name}::localhost ] $host ] } {
                catch { exec $realcmd } err
                Puts 5 "$realcmd: $err"
            } else {
                catch { Rexec $host ldas $realcmd } err
                Puts 5 "Rexec $realcmd: $err"
            } 
        }
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## readData: gets reply from client or server
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc readData { sid } {
        set name [ namespace current ]
        if { [gets $sid line] != -1 } {
            set line [ string trim $line ]
            if  { [ string length $line ] } {
                Puts 1 $line
            }
        } elseif  { [ eof $sid ] } {
            fileevent $sid readable {}
            catch { close $sid }
            set ::${name}::done 1
        }
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## send cmd to server or client
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc sendCmd { cmd host port { wait 0 } } {

        if  { [ catch {
            set name [ namespace current ]
            Puts 1 "sending \[$cmd\] to $host:$port"
		    set sid [ socket $host $port ]
            fconfigure $sid -buffering line
            puts $sid $cmd
            flush $sid
            if { $wait } {
                catch { unset ::${name}::done }
                fileevent $sid readable [ list ::${name}::readData $sid ]
                vwait ::${name}::done
            } else {
                catch { close $sid }
            }
	    } err ] } {
            set msg "Error: could not connect to $host:$port: $err\nfor cmd '$cmd'"
            Puts 1 "[ info level 0]: $err"
            return -code error $err
        }
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## procs for formatting stats
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
    
    proc formatData {fname} {
        if  { [ catch {
            set name [ namespace current ]
            set fin [ open $fname r ]
            set data [ split [read $fin] \n ]
            close $fin

            foreach line $data {
                if  { [ regexp {Data Socket Element Test ([\w\-]+)->([\w\-]+) (without|with) container (non-threaded|threaded) (\d+) samples} $line -> client server withcont thread samples] } {
                    Puts 5 "client=$client, server=$server, withcont=$withcont"
                }

                if { [ regexp {(char_[us]|int_[248us]+|real_[48]|lstring_[12468]+|complex_[168]+)} $line] } {
                    scan $line "%s %d %s %s %s %s %s %s %s" type dims wrate wmean wsd wcov cmean csd ccov
                    Puts 5 "type=$type, dims=$dims, rate=$wrate, wallmean=$wmean, sd=$wsd, cov=$wcov cpumean=$cmean, cpusd=$csd, cpucov=$ccov"
                    set ::${name}::wallmean($type,$dims) $wmean
                    set ::${name}::wallsd($type,$dims) $wsd
                    set ::${name}::wallcov($type,$dims) $wcov
                    set ::${name}::wallrate($type,$dims) $wrate
                    set ::${name}::cpumean($type,$dims) $cmean
                    set ::${name}::cpusd($type,$dims) $csd
                    set ::${name}::cpucov($type,$dims) $ccov
                }
            }
            writeData $client $server $withcont $thread $samples
        } err ] } {
            Puts 1 "[ info level 0]: $err"
            return -code error $err
        }
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## writeData - write table header of html file
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc writeData {client server withcont thread samples} {
        if  { [ catch {
            set name [ namespace current ]
            switch -exact -- $withcont {
                with {set cont "wc"}
                without {set cont "nc"}
            }
            switch -exact -- $thread {
                threaded {set t "_t"}
                non-threaded {set t ""}
            }
            set textfile "${client}_${server}_${cont}${t}.txt"
            set htmlfile "${client}_${server}_${cont}${t}.html"
            set ftext [ open $textfile w ]
            set fhtml [ open $htmlfile w ]

            ;## Text header
            puts $ftext "\tdata socket test ${client}<->${server}, $withcont container, $thread, $samples samples\n"

            ;## HTML header
            writeHTMLHeader $fhtml $client $server $withcont $thread $samples

            foreach timetype { wall cpu } {
                switch -exact -- $timetype {
                    wall { set varlist { rate mean sd cov } }
                    cpu { set varlist { mean sd cov } }
                }
                foreach var $varlist {
                    set timetypevar "${timetype}${var}"
                    puts $ftext [ set ::${name}::desc(${timetypevar}) ]
                    puts $ftext "Type \\ Dims\t[join [ set ::${name}::dimset ] \t]\n"
                    writeHTMLTableHeader $fhtml $timetypevar

                    foreach type [ set ${name}::types ] {
                        puts -nonewline $ftext "$type\t"
                        puts $fhtml "<th align=left width=100>$type</th>"

                        foreach dims [ set ::${name}::dimset ] {
                            if { [ info exists ::${name}::[ set timetypevar ]($type,$dims) ] } {
                                puts -nonewline $ftext [format "%-10.8f\t" [set ::${name}::[ set timetypevar ]($type,$dims)] ]]
                                puts $fhtml "<td align=right [ set ::${name}::bgcolor($dims) ]>[ format {%.6f} [set ::${name}::[ set timetypevar ]($type,$dims) ] ]</td>"
                            } else {
                                puts -nonewline $ftext "\t"
                                puts $fhtml "<td align=right [ set ::${name}::bgcolor($dims) ]> </td>"

                                ;## only print this once
                                if  {[string equal "wallrate" $timetypevar]} {
                                    Puts 1 "missing data for $type type, $dims dims"
                                }
                            }
                        }

                        puts $ftext ""
                        puts $fhtml "</tr>"
                        puts $fhtml ""
                    }
                    puts $ftext "\n"
                    puts $fhtml "</table></font>\n</p>"
                }
            }
            puts $fhtml "<p>Summary: \n</font>\n</body>\n</html>"
            close $ftext
            close $fhtml
        } err ] } {
            Puts 1 "[ info level 0]: $err"
            return -code error $err 
        }
    
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## writeHTMLHeader - write header of html file
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc writeHTMLHeader {fhtml client server withcont thread samples} {
    
        set name [ namespace current ] 
        if  { [info exists ::${name}::platform($client)] } {
            set platformC "[ set ::${name}::platform($client) ] ($client)"
        } else {
            set platformC "Unknown ($client)"
        }

        if  {[info exists :${name}::platform($server)]} {
            set platformS "[ set ::${name}::platform($server) ] ($server)"
        } else {
            set platformS "Unknown ($server)"
        }

        set loopback ""
        if { [ string equal "$client" "$server" ] } {
            set loopback "Loopback"
        }

        set verstr [ exec ls -ld /ldas/lib ]
        if { ! [ regexp {stow_pkgs/(ldas-[\d.]+)} $verstr -> version ] } {
            set version "ldas.0.0.11"
        }

        set domain [ exec cat /etc/ldasname ]
        set currtime [ clock format [clock seconds] -format "%B %d %Y" ]

        set heading "<!doctype html public \"-/w3c/dtd html 4.0 transitional/en\">
<html>
<head>
   <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">
   <title>LDAS Data Socket Transfer Rates at $domain</title>
</head>
<body>\n
<pre><b><font size=-1>LDAS Data Socket Roundtrip Transmission Rates at $domain</font></b></pre>
<font size=-2>
<table cellpadding=1>
<tr>
    <th align=left width=200>Test Period:</th>
    <td width=300>$currtime</td>
</tr>
<tr>
    <th align=left width=200>Input data:</th>
    <td width=300>Ilwd $withcont container</td>
    <td>
</tr>
<tr>
    <th align=left width=200>Socket type:</th>
    <td width=300>[string toupper ${thread} 0 0]</td>
    <td>
</tr>
<tr>
    <th align=left width=200>Sample Size:</th>
    <td width=300>$samples</td>
    <td>
</tr>
<tr>
    <th align=left width=200>Platforms:</th>
    <td width=300>$platformC <-> $platformS $loopback</td>
    <td>
</tr>
<tr>
    <th align=left width=200>LDAS software:</th>
    <td width=300>$version, Sun Solaris 2.7 for Sun Sparc and RedHat Linux 6.1 for Intel Pentium</td>
    <td>
</tr>
</table></font>"

        puts $fhtml $heading
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## writeHTMLTableHeader - write table header of html file
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc writeHTMLTableHeader {fhtml timetypevar} {
        set name [ namespace current ]
        puts $fhtml "<p>"
        puts $fhtml "<table border type=1>"
        puts $fhtml "<caption align=center><b>[ set ::${name}::desc($timetypevar) ]</b></caption>"
        puts $fhtml "<font size=-1>"
        puts $fhtml "<tr>"
        puts $fhtml "    <th align=left width=100>Type \\ dims</th>"
        puts $fhtml "    <th width=100 align=right>1</th>"
        puts $fhtml "    <th width=100 align=right>10</th>"
        puts $fhtml "    <th width=100 align=right>100</th>"
        puts $fhtml "    <th width=100 align=right>1000</th>"
        puts $fhtml "    <th width=100 align=right>10000</th>"
        puts $fhtml "    <th width=100 align=right>100000</th>"
        puts $fhtml "    <th width=100 align=right>1000000</th>"
        puts $fhtml "</tr>"
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## format the first output from test into .txt, .html and .log files
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    proc fmtTimes { filelist } {
     
        if  { [ catch {
            set name [ namespace current ] 
                  
            set samefileL {}
            set filenums [ llength $filelist ]
            for {set idx 0} {$idx <= $filenums } {incr idx} {
                set file [lindex $filelist $idx]
                if {![llength $samefileL]} {
                    set samefileL [list "$file [file size $file]"]
                    continue
                }
                if  {[string equal [file rootname $file] [file rootname [lindex [lindex $samefileL 0] 0]]]} {
                    lappend samefileL "$file [file size $file]"
                    Puts 1 "samefileL append $file"
                    continue
                }

                set samefileL [lsort -integer -decreasing -index 1 $samefileL]
                set oname [lindex [lindex $samefileL 0] 0]
                set fname [file rootname $oname].log

                file copy -force -- $oname $fname

                if  {[llength $samefileL] > 1} {
                    set logfd [open $fname a]
                    foreach pair [lrange $samefileL 1 end] {
                        set fd [open [lindex $pair 0] r]
                        puts $logfd [read -nonewline $fd]
                        close $fd
                    }
                    close $logfd
                }

                if  {[string length $file]} {
                    set samefileL [list "$file [file size $file]"]
                }

                if  {![file exists $fname]} {
                    Puts 0 "$fname: No such file"
                    continue
                }

                ;## clear values
                array unset ::${name}::wallrate
                array unset ::${name}::wallmean
                array unset ::${name}::wallsd
                array unset ::${name}::wallcov
                array unset ::${name}::cpumean
                array unset ::${name}::cpusd
                array unset ::${name}::cpucov

                Puts 5 "[ info level 0 ] Processing file $fname"
                formatData $fname
            }
        } err ] } {
            Puts 1 "[ info level 0]: $err"
            return -code error $err
        }
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## procs for calculating stats
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc collect {new old} {

        if  { [ catch {
            set old [ substFile $old ]
            set new [ substFile $new ]
            set name [ namespace current ]
            if {[file tail $new] != [file tail $old]} {
            ;## Warning
                Puts 5 "Warning: $old and $new do not have the same name."
            }

            array unset ::${name}::raw

            ;## Get data from files
            foreach age {new old} {
                set fd [open [set $age] r]
                set data [split [read $fd] "\n"]
                close $fd
    
                foreach line $data {
                    if  {[regexp -nocase {Data Socket Element Test (\S+)->(\S+) (without|with) container (non-threaded|threaded) (\d+) samples} $line -> client server withcont thread samples]} {
                        set title "data socket test statistics ${client}<->${server}, $withcont container, $thread, $samples samples"
                    }
                    if { [ regexp {(char_[us]|int_[248us]+|real_[48]|lstring_[12468]+|complex_[168]+)} $line] } {
                        scan $line "%s %d %s %s %s %s %s %s %s" dtype dim rate wmean wsd wcov cmean csd ccov
                        lappend ::${name}::raw(rate,$age,$dtype) $rate
                        lappend ::${name}::raw(rate,$age,$dim) $rate
                        lappend ::${name}::raw(rate,$age,all) $rate
                        lappend ::${name}::raw(wall,$age,$dtype) $wmean
                        lappend ::${name}::raw(wall,$age,$dim) $wmean
                        lappend ::${name}::raw(wall,$age,all) $wmean
                        lappend ::${name}::raw(cpu,$age,$dtype) $cmean
                        lappend ::${name}::raw(cpu,$age,$dim) $cmean
                        lappend ::${name}::raw(cpu,$age,all) $cmean
                    }
                }
            }
        } err ] } {
            Puts 1 "[ info level 0]: $err"
            return -code error $err
        }
            return $title
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;##
    ;## calculate - compute mean of current and previous result
    ;## 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc calculate {args} {
    
        if  { [ catch {
            set name [ namespace current ]
            array unset ::${name}::delta
            array unset ::${name}::mean

            ;## calculate mean
            foreach type {rate wall cpu} {
                foreach age {new old} {
                    foreach dtype [ set ${name}::types ] {
                        set ::${name}::mean($type,$age,$dtype) [mean [ set ::${name}::raw($type,$age,$dtype)] ]
                    }

                    foreach dim [ set ::${name}::dimset ] {
                        set ::${name}::mean($type,$age,$dim) [mean [ set ::${name}::raw($type,$age,$dim)] ]
                    }

                    set ::${name}::mean($type,$age,all) [mean [ set ::${name}::raw($type,$age,all)] ]
                }
            }

            ;## calculate differences
            foreach type {rate wall cpu} {
                foreach dtype [ set ${name}::types ] {
                    set newmean [ set ::${name}::mean($type,new,$dtype) ]
                    set oldmean [ set ::${name}::mean($type,old,$dtype) ]

                    if {$newmean == 0 || $oldmean == 0} {
                        set ::${name}::delta($type,$dtype) NA
                        continue
                    }

                    if  {[string equal "rate" $type]} {
                        set ::${name}::delta($type,$dtype) [expr {($newmean - $oldmean) / $oldmean}]
                    } else {
                        set ::${name}::delta($type,$dtype) [expr {($oldmean - $newmean) / $newmean}]
                    }
                }

                foreach dim [ set ::${name}::dimset ] {
                    set newmean [ set ::${name}::mean($type,new,$dim) ]
                    set oldmean [ set ::${name}::mean($type,old,$dim) ]

                    if  {$newmean == 0 || $oldmean == 0} {
                        set ::${name}::delta($type,$dim) NA
                        continue
                    }

                    if {[string equal "rate" $type]} {
                        set ::${name}::delta($type,$dim) [expr {($newmean - $oldmean) / $oldmean}]
                    } else {
                        set ::${name}::delta($type,$dim) [expr {($oldmean - $newmean) / $newmean}]
                    }
                }

                set newmean [ set ::${name}::mean($type,new,all) ]
                set oldmean [ set ::${name}::mean($type,old,all) ]

                if  {$newmean == 0 || $oldmean == 0} {
                    set ::${name}::delta($type,all) NA
                    continue
                }

                if  {[string equal "rate" $type]} {
                    set ::${name}::delta($type,all) [expr {($newmean - $oldmean) / $oldmean}]
                } else {
                    set ::${name}::delta($type,all) [expr {($oldmean - $newmean) / $newmean}]
                }
          }
        } err ] } {
            Puts 1 "[ info level 0]: $err"
            return -code error $err
        }   
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;##
    ;## substFile - replace diskcache host filenames due to upgrade of diskcache host
    ;## 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc substFile { fname } {
    
        if  { [ catch {
            set name [ namespace current ]
            set oldfile ""
            array set alternate [ array get ::${name}::alternate ]
            if  { ! [ file exist $fname ] } {
                set thisfile $fname
                ;## dictates the order by which substitution is made
                foreach altname [ list datacon opterondata dataserver ] {
                    set rc 1
                    while { $rc } {
                        set rc [ regsub $altname $thisfile [ set alternate($altname) ] oldfile ]                    
                        if  { $rc } {
                            if  { [ file exist $oldfile ] } {
                                Puts 1 "$oldfile exist for $fname"
                                set done 0
                                break
                            } else {
                                set thisfile $oldfile
                            }
                        }
                    }
                    if  { [ info exist done ] } {
                        break
                    }
                }
                if  { [ file exist $oldfile ] } {
                    set fname $oldfile
                } else {
                    error "$oldfile does not exist"
                }
            }
        } err ] } {
            Puts 0 "[ info level 0]: $err"
            return -code error $err
        }
        return $fname  
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;##
    ;## print - print rate diffs of current vs previous
    ;## 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
     
    proc print {new old out title} {
    
        if  { [ catch {
            set name [ namespace current ]
            set fout [open $out w]

            puts $fout "$title"
            puts $fout [clock format [clock seconds] -format {%B %d, %Y}]
            if  {[string length $new] && [string length $old]} {
                puts $fout "New file: [clock format [file mtime $new] -format {%B %d, %Y}]"
                puts $fout "Old file: [clock format [file mtime $old] -format {%B %d, %Y}]"
            }
            puts $fout ""

            array set text {
                cpu "CPU Time (s)"
                wall "Wall Time (s)"
                rate "Rate (MB/s)"
            }

            puts $fout [format {%-23s%-12s%-14s%-12s} " " "New Avg" "Old Avg" Analysis]
            
            foreach type {rate wall cpu} {
                if  {[string equal NA [ set ::${name}::delta($type,all) ] ]} {
                    set analysis "N/A"
                } else {
                    set analysis [format {%6.2f%% %s} \
                        [expr {100 * abs([ set ::${name}::delta($type,all) ])}] \
                        [expr { [ set ::${name}::delta($type,all) ] >= 0?"Faster":"Slower"}]]
                }
                puts $fout [format {%-23s%-12.6f%-12.6f%s} \
                    "Overall $text($type)" \
                [ set ::${name}::mean($type,new,all) ] \
                [ set ::${name}::mean($type,old,all) ] $analysis]
            }

            foreach type {rate wall cpu} {
                puts $fout ""
                puts $fout [format {%-23s%-12s%-14s%-12s} "$text($type) x Type" "New Avg" "Old Avg" Analysis]
                foreach dtype [ set ${name}::types ] {
                    if  {[string equal NA [ set ::${name}::delta($type,$dtype)] ]} {
                        set analysis "N/A"
                    } else {
                        set analysis [format {%6.2f%% %s} \
                            [expr {100 * abs([ set ::${name}::delta($type,$dtype) ])}] \
                            [expr {[ set ::${name}::delta($type,$dtype) ] >= 0?"Faster":"Slower"}]]
                    }
                    puts $fout [format {%-23s%-12.6f%-12.6f%s} \
                        $dtype \
                    [ set ::${name}::mean($type,new,$dtype) ] \
                    [ set ::${name}::mean($type,old,$dtype) ] \
                        $analysis]
                }
            }

            foreach type {rate wall cpu} {
                puts $fout ""
                puts $fout [format {%-23s%-12s%-14s%-12s} "$text($type) x Dim" "New Avg" "Old Avg" Analysis]
                foreach dim [ set ::${name}::dimset ] {
                    if  {[string equal NA [ set ::${name}::delta($type,$dim) ] ]} {
                        set analysis "N/A"
                    } else {
                        set analysis [format {%6.2f%% %s} \
                            [expr {100 * abs([ set ::${name}::delta($type,$dim) ])}] \
                            [expr {[ set ::${name}::delta($type,$dim) ] >= 0?"Faster":"Slower"}]]
                    }
                    puts $fout [format {%-23s%-12.6f%-12.6f%s} \
                        $dim \
                        [ set ::${name}::mean($type,new,$dim) ] \
                        [ set ::${name}::mean($type,old,$dim) ] \
                            $analysis]
                }
            }

            if  {![string match stdout $out]} {
                close $fout
            }
        } err ] } {
            Puts 1 "[ info level 0]: $err"
            return -code error $err
        }
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;##
    ;## compute mean of a set of numbers
    ;## 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
    
    proc mean {samples} {
        set mean 0.0
        set N [ llength $samples ]

        if { $N > 0 } {
            set mean [ expr ([ join $samples + ]) / $N. ]
        }

        return $mean
    }
    
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;##
    ;## datasockStats - compute comparsion stats with previous release
    ;## 
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
        
    proc datasockStats { filelist previous_resultdir } {

        if  { [ catch {
            set outfile {}
            set overall 1
            set overallfile "overall.stats"
            set name [ namespace current ]
            set dir $previous_resultdir 
        
            foreach newfile [lrange $filelist 0 end] {
                if  {[string length $dir]} {
                    set oldfile [file join $dir $newfile]
                } else {
                    set oldfile [lindex $filelist end]
                }
                set outfile [file rootname $newfile].stats
                set oldfile [ substFile $oldfile ]
                Puts 5 "oldfile $oldfile newfile $newfile"
        
                set title [collect $newfile $oldfile]
                calculate
                print $newfile $oldfile $outfile $title

                ;## Accumulate individual data for overall statistics
                if  {$overall} {
                    foreach type {rate wall cpu} {
                        foreach age {new old} {
                            foreach dtype [ set ${name}::types ] {
                                Puts 5 "[info level 0] type $type dtype $dtype age $age"
                                eval lappend ::${name}::rawG($type,$age,$dtype) [ set ::${name}::raw($type,$age,$dtype) ]
                            }
                            foreach dim [ set ::${name}::dimset ] {
                                eval lappend ::${name}::rawG($type,$age,$dim) [ set ::${name}::raw($type,$age,$dim) ]
                            }
                            eval lappend ::${name}::rawG($type,$age,all) [ set ::${name}::raw($type,$age,all) ]
                        }
                    }
                }
            }

            if  {$overall} {
                Puts 1 "\nCalculating overall statistics..."
                catch { array unset ::${name}::raw }
                array set ::${name}::raw [array get ::${name}::rawG]
                calculate
                print "" "" $overallfile "data socket test statistics overall"
            }
        } err ] } {
            Puts 1 "[ info level 0]: $err"
            return -code error $err
        }
    }

    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## generates test ilwds if not present
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    proc createILwdFiles { ilwddir } {
        set name [ namespace current ]
        set prog [ set ::${name}::ilwdGenerator ]
        if  { [ catch {
            ;## generate the ilwd files for each data type
            set ilwdformat [ set ::${name}::ilwdformat ] 
            set dims  [ set ::${name}::dimset ]
            set types [ set ::${name}::types ]
            
            if  { ![ file exist $ilwddir ] } {
                file mkdir $ilwddir
            }
            set filedir ${ilwddir}/$ilwdformat
            if  { ![ file exist $filedir ] } {
                file mkdir $filedir
            }            
            foreach type $types {
                foreach dim $dims {
                    set ilwdfile [ file join $filedir ${type}_${dim}.ilwd ]
                    if  { ! [ file exist $ilwdfile ] } {
                        set rc [ catch { exec $prog \
                        $dim $type $ilwdformat > [ file join $filedir ${type}_${dim}.ilwd ] } err ]
                        if  { $rc } {
                            Puts 0 "Error creating $ilwdformat ilwd for type $type dims $dim: $err "
                        } else {
                            Puts 5 "Created $ilwdformat ilwd for type $type dims $dim"
                        }
                    }
                }
            }
        } err] } {
            Puts 0 "$err $::errorInfo"
        }
    }
    
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## main 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
    
    ;## establish logdir and resultdir
    ;## directory where test script should run in
    ;## directory to output results
    
    set date [ clock format [ clock seconds ] -format "%m%d" ] 
    if  { ! [ string length $resultdir ] } {
        set resultdir ${::QA::LDAS_OUTGOING_ROOT}/tmp/datasock/results-$date
    }
    ;## directory to output logs
    if  { ! [ string length $logdir ] } {
        set logdir ${::QA::LDAS_OUTGOING_ROOT}/tmp/datasock
    }
    
    ;## create ilwd files if not present in ilwddir
    createILwdFiles $ilwddir
    
    if  { $clearPrevious } {
        catch { file delete -force $resultdir $logdir }
    }
    
    set testScript [ file join $::QA::LDAS_ROOT testbin $testScript ]
    
    set fd [ open $hostfile r ]
    set data [ split [ read $fd ] "\n" ]
    close $fd
        
    foreach line $data {
        set line [string trim $line]
        set client {}
        set server {}

        ;## skip comments
        if {[regexp {^(#|;##)} $line] || ![string length $line]} {
            continue
        }
        if {[regexp {^refresh} $line]} {
            cleanup
            continue
        }
        if {[regexp {^start} $line]} {
            set hosts [lrange [split [string trim $line]] 1 end]
            puts "hosts=$hosts"
            addhosts $hosts 1
            continue
        }
        if  { [ regexp -nocase -- {(\S+)=(\S+)\s+(\S+)=(\S+)} $line -> name1 value1 name2 value2 ] } {
            set $name1 $value1
            set $name2 $value2
        }
    
        if {[string length $client] && [string length $server]} {
            set hosts "$client $server"
            if {[string equal "$client" "$server"]} {
                set hosts "$client"
            }
            Puts 1 "adding host $hosts"
            addhosts "$hosts"
            incr test_count 1

            set serverCmd "serverInit"
            set clientCmd "clientInit $server [ join $dimset , ] [ join $types , ] $thread $samples $withcont $ilwdfmt"

            Puts 1 "client=$client, server=$server dims=$dimset, types=$types, thread=$thread, withcont=$withcont, samples=$samples, ilwdfmt=$ilwdfmt"
            after 2000
            set port [ set test_server(operator) ]
            if  {[catch {sendCmd $serverCmd $server $port $nowait} err]} {
                set err [ string trim $err ]
                if  { [ string length $err ] } {
                    Puts 1 "sendCmd $server $port error '$err'"
                }
                continue
            }
            incr cmds_sent 1
            
            after 1000
            set port [ set test_client(operator) ]
            if  {[catch {sendCmd $clientCmd $client $port $wait} err]} {
                set err [ string trim $err ]
                Puts 1 "sendCmd $clientCmd $client $port error:'$err'"
                continue
            }
            
            incr test_completed 1
        }
    }
    
    ## obtain results from client log
    catch { eval exec egrep {PASSED|FAIL} [ glob $logdir/*_client.log ] } result
    
    ;## compare results to previous versions
    cleanup
    
    test "Overall Result" {} -body {
        Puts 1 "resultdir $resultdir, $test_count tests, $cmds_sent cmds sent, $test_completed completed"
        if  { $cmds_sent != $test_completed || [ regexp -nocase {==.+FAIL} $result ] } {
            return -code error "sent $cmds_sent, completed $test_completed"
        }
        set errors [ list ]
        foreach line [ split $result \n ] {
            if  { [ regexp -nocase {==.+FAIL} $result ] } {
                lappend errors $line
            }
        }
        if  { [ llength $errors ] } {
            return -code error "[ join $errors \n ]"
        }
    } -result {} 
    
    ## generate the stats of comparsion between current and previous
    if  { [ file exist $resultdir ] } {
        cd $resultdir 
        catch { eval file delete -force 170 180 190 }
        fmtTimes [ glob -nocomplain * ]
        
        file mkdir 170 180 190
        foreach version [ list 170 180 190 ] {
            set rc [ catch { datasockStats [ glob -nocomplain *.log ] [ file join $former_resultdir results.$version ] } err ]
            if  { $rc } {
                Puts 0 "Failed to compare ldas $version stats: $err"
            } else {
                eval file copy -force [ glob -nocomplain *.stats ] $version
            }
        }     
    }
         
    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::datasockTest::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::datasockTest::test
exit
	    
