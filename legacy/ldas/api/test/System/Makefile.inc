# -*- mode: Makefile -*-
#------------------------------------------------------------------------
# This macro greatly simplifies what needs to be done to generate
#   the cmonClient tarball
#------------------------------------------------------------------------
define cmonClient-helper
dest=$(ldas_top_builddir)/$(cmonclientdistdir)/ldascmds/$$target; \
mkdir -p $$dest; \
for file in $$data; \
do \
  if test -f $(srcdir)/$$file; \
  then \
    cp $(srcdir)/$$file $$dest; \
  else \
    cp $$file $$dest; \
  fi; \
done
endef
