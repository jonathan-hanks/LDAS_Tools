#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
exec /ldcg/bin/tclsh "$0" ${1+"$@"}

;## does not work for stochastic, tfcluster and stackslide
;## problem with sorting xml file

catch { exec cat /etc/ldasname } data
set data [ string trim $data ]
set ::RUNCODE [ string toupper $data ]
set site $data

catch { exec ndas.tcl $site } data
puts $data

set i 1
set pat "Your results:.+(/ldas_outgoing/jobs/${::RUNCODE}\\S+)"

foreach line [ split $data \n ] {
	
	if 	{ [ regexp $pat $line -> dir ] } {
		set cmd "exec sort [ glob $dir/*.xml ] >& ndas$i"
		set ndas$i $dir
		catch { eval $cmd } err
		puts $err
		incr i 1
	}

}

catch { exec diff ndas1 ndas2 } err
puts "ndas1 vs ndas2  $ndas2\n$err"

catch { exec diff ndas3 ndas4 } err
puts "ndas3 $ndas3 vs ndas4 $ndas4\n$err"


catch { exec diff ndas5 ndas6 > ndas56 } err
puts "ndas1 $ndas1 vs ndas5 $ndas5"

catch { exec ./ndasdiff.tcl ndas56 } err
puts $err
;## "Largest Error 1 (gcc-3.3.3): 2.670140e-01 (4.6543826e-04 6.0886529e-04)\n"
;## set	passtext    "Largest Error 1: 2.670140e-01 (4.6543826e-04 6.0886529e-04)\n"
;## append passtext "Largest Error 2: 2.108760e-06 (1.4819121093750001e+04 1.4819152343749999e+04)"

#set	passtext "Largest Error 1 (gcc-3.4.3): 2.216890e-01 (1.0296344e-04 8.2415230e-05)\n"
#append passtext "Largest Error 2 (gcc-3.4.3): 3.581250e-06 (5.8627944335937500e+03 5.8627734375000000e+03)"
#
;## gcc 3.3.6
#set	passtext "Largest Error 1: 2.670101e-01 (4.6544586e-04 6.0887286e-04)\n"
#append	passtext "Largest Error 2: 1.054381e-06 (1.4819117187499999e+04 1.4819132812500000e+04)"

## libfftw3-3.1.a
#set	passtext "Largest Error 1: 2.546553e-01 (1.4322062e-05 1.1086810e-05)"
#append passtext "Largest Error 2: 4.996982e-07 (5.8629125976562504e+03 5.8629155273437501e+03)"

;## gcc 4.0.3
set	passtext "Largest Error 1: 2.541234e-01 (1.4317439e-05 1.1089225e-05)"
append passtext "\nLargest Error 2: 4.996983e-07 (5.8629111328125000e+03 5.8629140625000000e+03)"

if	{ [ string equal $passtext $err ] } {
	puts "TEST PASSED"
} else {
	puts "TEST FAILED got '$err"
}
catch { file delete -force ndas1 ndas2 ndas3 ndas4 ndas5 ndas56 ndas6 }
