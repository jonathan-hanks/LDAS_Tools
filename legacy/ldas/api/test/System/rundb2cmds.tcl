#! /ldcg/bin/tclsh

## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) cleanup script for /ldas_outgoing
##
## This script removes a process row and its associated
## child tables.
##
## usage: del_db_process.tcl <database> colname=xxxx
## e.g. del_db_process.tcl ldas_tst version=LDASTest
## deletion could take a while for large data
## ******************************************************** 

## DO NOT puts to stderr for ssh pipe runs

#! /ldcg/bin/64/tclsh

## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) cleanup script for /ldas_outgoing
##
## This script removes a process row and its associated
## child tables.
##
## usage: del_db_process.tcl <database> colname=xxxx
## e.g. del_db_process.tcl ldas_tst version=LDASTest
## deletion could take a while for large data
## ******************************************************** 

## DO NOT puts to stderr for ssh pipe runs


proc terminate {} {
    catch { file delete -force .del_db_process.lock }
    catch { close $::fd }
    exit
}

proc execdb2cmd {} {
	uplevel {
		set cmd "exec $::db2 $cmd"
        ;## if  { ![ regexp {connect to} $cmd ] } {
        ;##    debugPuts "$cmd"
        ;## }
		catch { eval $cmd } data
        puts "rundb2cmds: $cmd\n$data"
		flush stdout
	}
}

proc printcnts {} {
	uplevel {
		if	{ [ catch {
            set rows 0
			regexp -- {\n[\s\t]+(\d+)} $data -> rows
			if	{ ! [ info exist ::totalrows($table) ] } {
				set ::totalrows($table) 0
			}
			incr ::totalrows($table) $rows
            if  { $rows } {
			    append output "Found $rows rows for table $table\n"
            }
		} err ] } {
			append output "Error: $err,data=$data\n"
		}
	}
}

proc readcmd {} {

	set cmd ""	
	while { [ gets stdin line ] > -1 } {
		append cmd "$line;\n"
	}
	flush stdout
	execdb2cmd
	if	{ [ eof stdin ] } {
		puts stdout "closing stdin"
		flush stdout
		exit
	}
}


set TOPDIR /ldas_outgoing
set LDAS /ldas
source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc

set auto_path "$::LDAS/lib $auto_path"
source $::LDAS/lib/genericAPI/b64.tcl
source [ file join $::TOPDIR metadataAPI LDASdsnames.ini ] 
 
set dsname [ lindex $argv 0 ]

foreach { dbuser dbpasswd } [ set ::${dsname}(login) ] { break }
set cmd "connect to $dsname user $dbuser using [ decode64 $dbpasswd ]"
execdb2cmd

fconfigure stdin -buffering none -translation binary -blocking 0
fconfigure stdout -buffering none -translation binary
fileevent stdin readable [ list readcmd ]

vwait forever

#set cmd terminate
#execdb2cmd
