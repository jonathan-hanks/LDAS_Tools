lappend ::Commands(dbperformance-run:args) --dbperformance-database dev_1
set pos [lsearch -exact $::TestOptions --data-output-dir]
if {$pos < 0} {
    lappend ::TestOptions --data-output-dir [file join / ldas_usr ldas SystemTest Data]
}
