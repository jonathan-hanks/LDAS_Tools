#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

if  { ![ info exist ::env(LIGOTOOLS) ] } {
    set ::auto_path "/ldas/lib $::auto_path"
    puts "no ligotools, set auto_path $::auto_path, using LDASJobH [ auto_execok LDASJobH ] "
} else {
    set auto_path "$::env(LIGOTOOLS)/lib $auto_path"
    set ::env(PATH) "$::env(LIGOTOOLS)/bin:$::env(PATH)"
    puts "with ligotools $::env(LIGOTOOLS) using LDASJobH [ auto_execok LDASJobH ] "
}

;## a script to start all of loop testing
if	{ ! [ info exist ::WORKDIR ] } {
	set ::WORKDIR /ldas_outgoing/test
}
set ::LOOPDIR $::WORKDIR/loop2
set ::MISCDIR $::WORKDIR/misc
set ::DMTDIR $::WORKDIR/database
set ::RDSDIR $::WORKDIR/rdstest
set ::DCMANGLE_SHUTDOWN /tmp/dcmangle/sHuTdOwN
set ::DCMANGLE_CACHE /tmp/dcmangle/.frame.cache
set ::PUTSTANDALONE_DESTDIR $::WORKDIR/loop2/putStandAlone
set ::PUTSTANDALONE_SRCDIR /scratch/test/LDASTest/putStandAlone
set ::TRIGGERS_DIR /scratch/test/dmt/triggers2
set ::BINDIR /ldas_usr/ldas/test/bin
set ::DCMANGLE dcmangle.tcl
set ::PUTSTANDALONE_DSOS [ list stochastic inspiral tfclusters knownpulsardemod slope stackslide ] 
set ::PUTSTANDALONE_INPUT_RETAIN 5000
set ::GLOBUS_LOCATION /ldcg/lib

catch { exec cat /etc/ldasname } ldasname 

;## for ldas 1.8.0, do not use globus options
if	{ [ file exist /ldas/lib/cmonClient/ldas_version.tcl ] } {
	set fd [ open /ldas/lib/cmonClient/ldas_version.tcl r ]
	set ldas_version [ read -nonewline $fd ]
	close $fd
	if	{ [ regexp {1.8.0} $ldas_version ] } {
		set ::LDAS_VERSION_8 1
	}
}

;## delay between loops
set ::LOOP_DELAY 90 
set ::USE_GLOBUS_CHANNEL 0
set ::USE_GSI 0
set ::RUNCODE [ string toupper $ldasname ]

if	{ ! [ info exist ::SITE ] } {
	switch -regexp -- $ldasname {
	ldas-dev 	{ set ::SITE dev 
				  set ::LOOP_DELAY 70 }
	ldas-test 	{ set ::SITE test 
			  if 	{ [ info exist ::LDAS_VERSION_8 ] } {
				set ::LOOP_DELAY 120
			  } else {
			  	set ::LOOP_DELAY 80
			  }
			}
	ldas-cit	{ set ::SITE cit }
	ldas-wa		{ set ::SITE lho }
	ldas-la		{ set ::SITE llo }
	ldasbox*	{ set ::SITE $ldasname  
				  set ::LOOP_DELAY 500
				  set ::FRAME_TOPDIR /ldas_outgoing/frames
				  set ::RUNCODE LDASBOXI 
				  set ::PUTSTANDALONE_INPUT_RETAIN 10000}
	ldas-suntest1 { set ::SITE $ldasname 
					set ::LOOP_DELAY 500
				  	set ::PUTSTANDALONE_INPUT_RETAIN 10000
					set ::RUNCODE LDAS-SUNTESTI}
	tandem-ii		{ set ::SITE ldas-suntest2
				  set ::LOOP_DELAY 500
				  set ::RUNCODE TANDEM-II }				  				
	tandem-iii	{ set ::SITE ldas-suntest3
				  set ::LOOP_DELAY 500
				  set ::RUNCODE TANDEM-III }
	tandem-iv	{ set ::SITE ldas-suntest4
				  set ::LOOP_DELAY 500
				  set ::RUNCODE TANDEM-IV }			  
	tandem-v	{ set ::SITE ldas-suntest5
				  set ::LOOP_DELAY 500
				  set ::RUNCODE TANDEM-V }						  
	default		{ set ::SITE $::env(HOST)
				  set ::RUNCODE [ string toupper $::SITE ] }
	}
}

if	{ ![ info exist ::FRAME_TOPDIR ] } {
	set ::FRAME_TOPDIR /scratch/test/system/$ldasname
}

;## resource for use of globus in all tests

;## removes programs
proc killer { pattern } {
	catch { exec ps -A -o "pid,args" | grep $pattern } data
	foreach line [ split $data \n ]  {
		if	{ [ regexp {(\d+).+sh} $line -> pid ] } {
			puts "killing $pattern $pid, $line"
			catch { exec kill -9 $pid }
		}
	}
}

proc clean_putStandAlone {} {
	
	;## remove old putstandalone data
	set curtime [ clock seconds ]
	;## purge any log over 1 hours
	set patlist [ list process_1m process_2m ] 
	foreach dir $::PUTSTANDALONE_DSOS {
		foreach pat $patlist {
			catch { eval file delete [ glob -nocomplain $::PUTSTANDALONE_DESTDIR/${dir}/${pat}*.ilwd ] } 
		}
	}
}

proc getPid { pattern } {
        set pids [ list ]
        catch { exec ps -A -o "pid,args" | grep $pattern } data
        foreach line [ split $data \n ]  {
                if      { [ regexp {(\d+).+tcl} $line -> pid ] } {
                        lappend pids $pid
                }
        }
        return $pids
}


