#!/bin/sh
# \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

source /ldas_usr/ldas/test/bin/AllTest.rsc
;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

;## retain logs for 5 days
proc clean_dmt_logs {} {
	cd $::DMTDIR
	set dmtlogs [ lsort -decreasing [ glob -nocomplain dmt.log.* ] ]
	set maxtime 432000
	set now [ clock seconds ]
	foreach file $dmtlogs {
        set mtime [ file mtime $file ]
        if	{ [ expr $now - $mtime ] > $maxtime } {
			file delete $file
		}
	}
}

proc killdmt {} {	

	set now [ clock seconds ]
	catch { file copy -force dmt.log dmt.log.$now }
	catch { exec SeqInsert purge } err
	after 2000
    append msg "\nremoved any files in SeqInsert queue: $err"
    catch { exec SeqInsert shutdown & } err
    append msg "\nshut down any running SeqInsert server: $err"
	after 5000
	killer insertTrigsLoop
	killer SeqInsert	
}

killdmt
killer rundmt
clean_dmt_logs
