putStandAlone
    -subject {putStandAlone stochastic loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
	-multidimdatatarget {frame}
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/stochastic/process_1.ilwd  
		file:/ldas_outgoing/jobs/LDASTest/putStandAlone/stochastic/output_1.ilwd
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/stochastic/process_2.ilwd 
    }
    -database {ldas_tst}
