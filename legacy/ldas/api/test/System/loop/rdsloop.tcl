#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
. /ldas/libexec/setup_tclsh.sh 
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#exec /ldcg/bin/tclsh "$0" ${1+"$@"}

;# E7 data
#set startTime  693960000
#set endTime    693967199

;# S1 data
#set startTime  714384000
#set endTime    714387584

;# E9 data LHO
#set startTime  727491488
#set endTime    727713056

;# E9 data LLO
#set startTime  727489712
#set endTime    727722944

;# E9 tri-conincident data
#set startTime  727549920
#set endTime    727553535

;# S2 data
#set startTime  730522192
#set endTime    730525792

;# S3 data
set startTime  	751800000
set endTime    	751803600

set site          dev
set user          createrdstest
set type          R
set usejobdirs    0
set adjust        1 
set outputdir     ""
set usertype      ""
set filechecksum  0
set framechecksum 0
set timecheck     1
set datavalid     1
set compressiontype  raw
set compressionlevel 6
#!orig! set rdsDir  "/frame10/rds/S1"

set defaultJobLength 64
set minJobLength     16
set frameLength      16

set jobCtr      0
set rdsTime     0
set procTime    0
set totalHours  0
set hourCtr     0
set hourMod     2
set hourWait    600
set oneHour     3600
set errWait     5

set adcChanList [list]
set adcFileList [list]
#!orig! set adcFile "adcdecimate_lho_s1.txt"
#!orig! set homeDir "/home/ileonor/LdasJobs/S1"

set nullCmd "null or malformed command"
set md5Err  "md5Challenge"
set sockErr "socket closure"
#set noGPS   "file not found"
set noGPS   "no such file"
set fullQue "limited queue size"
set noData  "no data found"
set aborted "aborted after \[\\d\\.\]+ seconds"
set timeOut "Timeout waiting"
set exists  "file already exists"
set badfile "Verification exit code"

set usercmd {
    createRDS
        -times {$::queryTime}
        -type {$::type}
        -usertype {$::usertype}
        -outputdir {$::outputdir}
        -usejobdirs {$::usejobdirs}
        -compressiontype {$::compressiontype}
        -compressionlevel {$::compressionlevel}
        -filechecksum {$::filechecksum}
        -frametimecheck {$::timecheck}
        -framedatavalid {$::datavalid}
        -channels {$::adcquery}
}

proc buildQuery {adcChanList} {
    set query [list]
    foreach line $adcChanList {
        set line [string trim $line]

        # Skip blank and comment lines
        if {![string length $line] || [regexp {^\#} $line]} {
            continue
        }

        set adc {}
        set dec {}
        foreach {adc dec} $line {break}
        if {![string length $dec] || $dec == 1} {
            lappend query $adc
        } else {
            lappend query "${adc}!$dec"
        }
    }

    return $query
}

proc openLogFile {logFile} {
    if {[file exists $logFile]} {
        set timestamp [clock format [file mtime $logFile] -format "%m%d%H%M%S"]
        catch {file rename -force -- $logFile $logFile.${timestamp}}
    }
    set oid [open $logFile a]

    set clockSeconds [clock seconds]
    set locTime [clock format $clockSeconds -format %c]
    set utcTime [clock format $clockSeconds -format %c -gmt 1]
    puts "\nStart reduced dataset:  LOC $locTime  UTC $utcTime"
    puts $oid "Start reduced dataset:  LOC $locTime  UTC $utcTime"
    flush $oid

    return $oid
}

proc closeLogFile {oid rdsTime procTime wallTime jobCtr} {
    puts "\nTotal data processed  :  $rdsTime seconds ([duration $rdsTime])"
    puts "Total processing time :  $procTime seconds ([duration $procTime])"
    puts "Total elapsed time    :  $wallTime seconds ([duration $wallTime])"
    puts "Total jobs submitted  :  $jobCtr jobs"
    puts $oid "Total data processed  :  $rdsTime seconds ([duration $rdsTime])"
    puts $oid "Total processing time :  $procTime seconds ([duration $procTime])"
    puts $oid "Total elapsed time    :  $wallTime seconds ([duration $wallTime])"
    puts $oid "Total jobs submitted  :  $jobCtr jobs"

    set clockSeconds [clock seconds]
    set locTime [clock format $clockSeconds -format %c]
    set utcTime [clock format $clockSeconds -format %c -gmt 1]
    puts "End reduced data set :  LOC $locTime  UTC $utcTime"
    puts $oid "End reduced data set :  LOC $locTime  UTC $utcTime"

    catch {close $oid}
    return
}

proc checkExistingFrames {} {

	set time [ clock format [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]
	if	{ [ catch {
		;## check for frames before submitting job
		set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
		\"ls -ilR  $::outputdir\""
		catch { eval $cmd } data
		puts "$time: check frames in $::outputdir\n$data"
	} err ] } {
		puts "$time: $err"
	}
}

proc sendJob {site cmd {varlist ""}} {

	checkExistingFrames
	
    catch { LJrun job -nowait -manager $site -user $::user \
		-globus $::USE_GLOBUS_CHANNEL -gsi $::USE_GSI $cmd } errmsg
    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg $::job(error)
        }
        LJdelete job
        return -code error [list $errmsg]
    }

    # puts stdout "[string trim $::job(jobInfo)]\n"

    catch {LJwait job} errmsg
    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg $::job(error)
        }
        set jobnum $::job(jobnum)
		
		puts "$jobnum error $errmsg"
		checkExistingFrames
		
        LJdelete job
        return -code error [list $jobnum $errmsg]
    }

    set retval [list]
    foreach var $varlist {
        lappend retval $::job($var)
    }
    LJdelete job

    return $retval
}

proc duration {time {format "dhms"}} {
    array set div {d 86400 h 3600 m 60 s 1}
    array set str {d day h hour m minute s second}

    if {$time < 0} {
        set time 0
    } elseif {$time < 1} {
        set time [format "%g" $time]
    } elseif {$time >= 1} {
        set time [string trimleft $time 0]
    }

    set retval [list]
    foreach u [split [string tolower $format] {}] {
        foreach {secs frac} [split $time .] {break}
        set n [expr {$secs / $div($u)}]

        if {[string equal "s" $u]} {
            set n $time
        }

        if {$n == 0} {
            continue
        }

        if {$n != 1} {
            append str($u) "s"
        }

        lappend retval "$n $str($u)"
        set time [expr {$time - $n * $div($u)}]
    }

    if {![string length $retval]} {
        lappend retval "0 $str([string index $format end])s"
    }

    return [join $retval ", "]
}

proc printUsage {args} {
    exit 1
}

### MAIN ###
set idx [lsearch -glob $::argv "--dump*"]
if {$idx != -1} {
    ;## Special handling for dumping job scripts
    set dumpdir "."
    regexp -- {^--dump=(.+)$} [lindex $::argv $idx] -> dumpdir

    set adcFile [file join [file dirname $::argv0] rds_s2.lho]
    set fid [open $adcFile r]
    set adcChanList [split [read -nonewline $fid] "\n"]
    close $fid
    set adcquery [buildQuery [lrange $adcChanList 0 4]]

    set timeS [expr {$startTime + 16}]
    set timeE [expr {$timeS + $defaultJobLength - 1}]
    set queryTime "$timeS-$timeE"

    regsub -line -all {^    } $usercmd {} usercmd
    set fid [open [file join $dumpdir createRDS.cmd] w]
    puts $fid [string trim [subst -nobackslashes $usercmd]]
    close $fid

    exit 0
}

lappend ::auto_path /ldas/lib
if	{ ! [ regexp "/ldas/bin" $::env(PATH) ] } {
	set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

package require LDASJob
package require tconvert

;## set API test
catch { exec grep ::DISKCACHE_API_HOST /ldas_outgoing/LDASapi.rsc } cmd
catch { eval $cmd } data

if	{ ! [ info exist ::DISKCACHE_API_HOST ] } {
	set ::DISKCACHE_API_HOST [ exec uname -n ]
}

if	{ [ file exist /ldas_usr/ldas/test/bin/AllTest.rsc ] } {
	source /ldas_usr/ldas/test/bin/AllTest.rsc
} else {
	set ::USE_GLOBUS_CHANNEL 1
	set ::USE_GSI 1
}

;## Process command-line arguments
for {set idx 0} {$idx < $::argc} {incr idx} {
    switch -exact -- [lindex $::argv $idx] {
        -a {set adjust 0}
        -b {set startTime [lindex $::argv [incr idx]]}
        -c {set compressiontype [lindex $::argv [incr idx]]}
        -d {set homeDir [lindex $::argv [incr idx]]}
        -e {set endTime [lindex $::argv [incr idx]]}
        -f {lappend adcFileList [lindex $::argv [incr idx]]}
		-g {set ::USE_GLOBUS_CHANNEL [lindex $::argv [incr idx]] }
		-gsi { set ::USE_GSI [lindex $::argv [incr idx]] }
		-i {set ifo [lindex $::argv [incr idx]] }
        -j {set usejobdirs 1}
        -l {set defaultJobLength [lindex $::argv [incr idx]]}
        -m {set minJobLength [lindex $::argv [incr idx]]}
        -o {set outputdir [lindex $::argv [incr idx]]}
        -s {set site [lindex $::argv [incr idx]]}
        -t {set type [lindex $::argv [incr idx]]}
        -T {set usertype [lindex $::argv [incr idx]]}
        -u {set user [lindex $::argv [incr idx]]}
        -v {set datavalid 0}
        -x {set filechecksum 1}
        -X {set framechecksum 1}
        -y {set timecheck 0}
        -z {set compressionlevel [lindex $::argv [incr idx]]}
        -h {}
        -* {
            puts stderr "Error: Invalid option: $opt"
            printUsage
        }

        default {}
    }
}

if {$adjust} {
    set startTime [expr {$startTime + 16}]
    set endTime [expr {$endTime - 16}]
}

;## Read adc list from file
if {![llength $adcFileList]} {
    return -code error "No channel list provided."
}
foreach adcFile [join $adcFileList] {
    if {[catch {open $adcFile r} fid]} {
        return -code error "Error opening file '$adcFile': $fid"
    }
    lappend adcChanList [split [read -nonewline $fid] "\n"]
    catch {close $fid}
}
if {![llength $adcChanList]} {
    return -code error "Empty channel list."
}
set adcquery [buildQuery [join $adcChanList]]
#!orig! puts $adcquery

#!orig! set dataDir "$homeDir/Data$totalHours/"
#!orig! catch {file mkdir $dataDir}
#!orig! cd $dataDir

#!orig! set logFile [format "rdslog_%s-s1-%03d.txt" $site $totalHours]

set logFile [ file rootname [file tail $::argv0]]_${site}_${ifo}.log
set logfile $logFile

set oid [openLogFile $logFile]
set __tMark [clock seconds]

set outFile "[file rootname $logFile].dat"
set oid2 [open $outFile a]

set jobLength $defaultJobLength
set timeS $startTime
set timeE [expr {$timeS + $jobLength - 1}]

while {$timeS < $endTime} {
    set __tStart [clock seconds]

    #if {$rdsTime >= $oneHour} {
    #    set wallTime [expr {[clock seconds] - $__tMark}]
    #    closeLogFile $oid $rdsTime $procTime $wallTime $jobCtr

    #    incr totalHours
    #    set jobCtr   0
    #    set rdsTime  0
    #    set procTime 0

    #    if {($totalHours % $hourMod) == 0} {
    #        set gpsTimeNow [tconvert now]
    #        while {$gpsTimeNow < ($timeS + $oneHour * $hourMod)} {
    #            set locTime [clock format [clock seconds] -format %c]
    #            puts "\nsleeping for $hourWait seconds...  LOC $locTime  GPS $gpsTimeNow"
    #            after [expr {$hourWait * 1000}]
    #            set gpsTimeNow [tconvert now]
    #        }
    #    }

    #    #!orig! set dataDir "$homeDir/Data$totalHours/"
    #    #!orig! catch {file mkdir $dataDir}
    #    #!orig! cd $dataDir
    #    #!orig! set logFile [format "rdslog_%s-rds1-%03d.txt" $site $totalHours]
    #    openLogFile $logFile
    #    set __tMark [clock seconds]
    #}

    if {[expr {fmod($timeS,$frameLength)}] != 0} {
        puts "\nGPS time $timeS is not divisible by $frameLength"
        puts $oid "GPS time $timeS is not divisible by $frameLength"
        flush $oid
        break
    }

    while {$timeE > $endTime} {
        set timeE [expr {$timeE - $frameLength}]
        set jobLength [expr {$timeE - $timeS + 1}]
    }
    if {$jobLength < $minJobLength} {
        puts "\nQuery time ($timeS-$timeE) is less than the minimum of $minJobLength seconds"
        puts $oid "Query time ($timeS-$timeE) is less than the minimum of $minJobLength seconds"
        flush $oid
        break
    }

    set queryTime "$timeS-$timeE"

    puts "\n--------------"
    puts "rdsTime      :  $rdsTime seconds ([duration $rdsTime])"
    puts "procTime     :  $procTime seconds ([duration $procTime])"
    puts "JobCtr       :  [incr jobCtr]"
    puts "JobLength    :  $jobLength seconds"
    puts "queryTime    :  $queryTime  [tconvert $timeS]\n"

    puts -nonewline $oid [format "%s %03d %s %05d" $queryTime $jobLength $site $jobCtr]
    #flush $oid

    if {[catch {sendJob $site $usercmd [list status jobReply outputs jobnum jobTime]} msg]} {
        set jobError ""
        if {[llength $msg] != 2} {
            set jobError [join $msg]
            puts "Error from LJrun:\n[string trim $jobError]"
            puts $oid "\n[string trim $jobError]"
            flush $oid
        } else {
            foreach {jobnum jobError} $msg {break}
            puts "Error in LDAS job:\n[string trim $jobError]"
            puts $oid [format " %05d\n%s" $jobnum [string trim $jobError]]
            flush $oid
        }

        if {[regexp -nocase -- "$noGPS|$noData" $jobError] && [regexp {\d{9}} $jobError missingGPS]} {
            puts "\nmissing GPS:  $missingGPS"
            puts $oid "missing GPS:  $missingGPS"
            flush $oid

            if {$missingGPS > $timeS} {
                set timeE [expr {$missingGPS - $frameLength - 1}]
                set jobLength [expr {$timeE - $timeS + 1}]
                if {$jobLength >= $minJobLength} {
                    puts "\nAdjusting job length to remove frame gap"
                    puts $oid "Adjusting job length to remove frame gap"
                    flush $oid
                    continue
                }
            } elseif {$missingGPS < $timeS} {
                set timeS [expr {$timeS + $frameLength}]
            } else {
                set timeS [expr {$missingGPS + 2*$frameLength}]
            }

            ;## $missingGPS <= $timeS || $jobLength < $minJobLength
            puts "\nAdvancing job start time to find next frame"
            puts $oid "Advancing job start time to find next frame"
            flush $oid

            set jobLength $defaultJobLength
            set timeE [expr {$timeS + $jobLength - 1}]
            continue
        } elseif {[regexp -nocase -- $exists $jobError] && [regexp {(\d{9})\-\d+\.gwf} $jobError -> existingGPS]} {
            puts "\nexisting GPS: $existingGPS"
            puts $oid "existing GPS: $existingGPS"
            flush $oid

            set jobLength $defaultJobLength
            set timeS [expr {$existingGPS + $frameLength}]
            set timeE [expr {$timeS + $jobLength - 1}]
            continue
        } elseif {[regexp -nocase -- $badfile $jobError] && [regexp {(\d{9})\-\d+\.gwf} $jobError -> badGPS]} {
            puts "\nbad GPS: $badGPS"
            puts $oid "bad GPS: $badGPS"
            flush $oid

            set jobLength $defaultJobLength
            set timeS [expr {$badGPS + 2*$frameLength}]
            set timeE [expr {$timeS + $jobLength - 1}]
            continue
        } elseif {[regexp -nocase -- "$sockErr|$fullQue|$nullCmd|$md5Err|$aborted|$timeOut" $jobError]} {
            puts "\nresubmitting job in $errWait seconds..."
            after [expr {$errWait * 1000}]
            continue
        }
        break
    }

    foreach {status reply outFrames jobnum jobTime} $msg {break}
    set nFramesOut [llength $outFrames]
    puts "$status"
    puts "[string trim $reply]\n"
    #!orig! puts "$outFrames"
    puts [join $outFrames "\n"]
    puts "\nnumber of output frames:  $nFramesOut"

    #!orig! set outFile [format "rdsout_%s-%03d-%05d.txt" $site $totalHours $jobCtr]
    #!orig! set oid2 [open $outFile a]
    puts $oid2 [join $outFrames "\n"]
    flush $oid2
    #!orig! catch {close $oid2}

    set rdsTime [expr {$rdsTime + $nFramesOut * $frameLength}]
    set procTime [expr {$procTime + $jobTime}]
    puts $oid [format " %05d %07.2f" $jobnum $jobTime]
    flush $oid

    if {$nFramesOut != ($jobLength / $frameLength)} {
        puts "Invalid number of output frames!"
        break
    }

    set jobLength $defaultJobLength
    set timeS [expr {$timeS + $jobLength}]
    set timeE [expr {$timeS + $jobLength - 1}]

    #while {([clock seconds] - $__tStart) < $jobLength} {
    #    after 1000
    #}
}

set wallTime [expr {[clock seconds] - $__tMark}]
closeLogFile $oid $rdsTime $procTime $wallTime $jobCtr
catch {close $oid2}

puts "\nGrand Total data processed  :  $rdsTime seconds ([duration $rdsTime])"
puts "Grand Total processing time :  $procTime seconds ([duration $procTime])"
puts "Grand Total elapsed time    :  $wallTime seconds ([duration $wallTime])"
puts "Grand Total jobs submitted  :  $jobCtr jobs"
exit 0



