dataPipeline
    -subject BURST
    -returnprotocol http://results.iwld

    -dynlib libldasburstwrapper.so
    -filterparams (fileOutput:/dso-test/burstwrapper/jobH1.730524507.bin,noLALBurstOutput,H1:LSC-AS_Q,4915200,,,,,,0,,TFCLUSTERS,H1:LSC-AS_Q,0.0233572,1,0,0,0.125,130.0,400.0,-1.0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

    -np 4
    -datacondtarget wrapper
    -metadataapi ligolw

    -framequery {
        { R H {} 730524507-730524808 Adc(H1:LSC-AS_Q) }
        { CAL_FAC_V03_H1 H {} 730524507-730524808 Proc(H1:CAL-OLOOP_FAC,H1:CAL-CAV_FAC) }
        { CAL_REF_V03_H1 H {} 734073939-734074002 Proc(H1:CAL-CAV_GAIN!0!7000.0001!,H1:CAL-RESPONSE!0!7000.0001!) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/ilwd/filters/S2/S2_HPF_6_100_a.ilwd,push,fa1
        file:/ldas_outgoing/jobs/ilwd/filters/S2/S2_HPF_6_100_b.ilwd,push,fb1
        file:/ldas_outgoing/jobs/ilwd/filters/S2/S2_LPEF_H1_9_4_1_1024.ilwd,push,fb2
    }
    -aliases {
        x = H1\:LSC-AS_Q;
        h1oloop = H1\:CAL-OLOOP_FAC;
        h1cavfac = H1\:CAL-CAV_FAC;
        h1gain = H1\:CAL-CAV_GAIN;
        h1resp = H1\:CAL-RESPONSE;
    }
    -algorithms {
        gwchn = double(x);
        gwchn = linfilt(fb1,fa1,gwchn);
        gwchn = linfilt(fb2,gwchn);
        gwchns = slice(gwchn,16384,4915200,1);
        gwchns = float(gwchns);
        output(gwchns,_,_,GW_STRAIN_DATA:primary,GW_STRAIN_DATA);
        cavfac = float(h1cavfac);
        cavfaccplx = complex(cavfac);
        output(cavfaccplx,_,_,H1:CAL-CAV_FAC,H1 cavity factor);
        oloop = float(h1oloop);
        oloopcplx = complex(oloop);
        output(oloopcplx,_,_,H1:CAL-OLOOP_FAC,H1 open loop factor);
        output(h1gain,_,_,H1:CAL-CAV_GAIN,H1 reference cavity gain);
        output(h1resp,_,_,H1:CAL-RESPONSE,H1 reference response);
        h1cavfacf = float(h1cavfac);
        h1cavfacf = complex(h1cavfacf);
        h1oloopf = float(h1oloop);
        h1oloopf = complex(h1oloopf);
        spec = psd( gwchns, 307200 );
        output(spec,_,_,GW_STRAIN_PSD,GW_STRAIN_PSD);
    }
