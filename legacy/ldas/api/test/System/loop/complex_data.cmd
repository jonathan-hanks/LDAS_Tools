dataPipeline
    -subject         {complex data in frames DEMO}

    -dynlib          libldastrivial.so
    -filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))

    -datacondtarget  wrapper
    -metadatatarget  datacond
    -multidimdatatarget  ligolw
    -database        ldas_tst
    -state           {}
    -np 3

    -framequery {
        { R H {} {774982800-774982900} Adc(H2:DAQ-FAST_CH0_HDYNE,H2:DAQ-FAST_CH1_HDYNE,H2:DAQ-FAST_CH2_HDYNE,H2:DAQ-FAST_CH3_HDYNE)}
    }
    -responsefiles {}

    -aliases {
        gw = H2\:DAQ-FAST_CH0_HDYNE::AdcData;
    }
    -algorithms {
        rgw = resample(gw, 1, 2048);
        output(rgw,_,_,,resampled gw timeseries);       
    }


