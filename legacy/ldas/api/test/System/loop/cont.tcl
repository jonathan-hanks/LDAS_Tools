#!/bin/sh
# \
exec /ldcg/bin/tclsh "$0" ${1+"$@"}
#exec tclsh "$0" ${1+"$@"}

source ./joblist.tcl

set msec_i 0
set start [lindex $::argv 0]

if {![string length $start]} {
    ;## If no time is provided on the command-line,
    ;## default to the next whole minute.
    set start [clock format [clock scan "next minute"] -format {%m/%d/%y %H:%M}]
}

;## Save millisecond value, if any
if {[regexp {\.(\d+)} $start -> msec_i]} {
    regsub {\.\d+} $start {} start
}
set sec_i [clock scan $start]

set first 1
foreach port $meta_data {
    if {$first} {
        set sid [ socket [ info hostname ] $port ]
        puts $sid "wait"
        flush $sid
        set wait [gets $sid]
        set wait [expr $wait / [llength $meta_data]]
        close $sid

        set offset [expr $wait / 2.0]
        set msec_f "0.$msec_i"
        set sec_f [expr {$sec_i + $msec_f + $offset}]
        foreach {sec2 msec2} [split $sec_f "."] {break}
        set first 0
    }

    set sid [ socket [ info hostname ] $port ]
    puts $sid [list cont [clock format $sec2 -format "%x %X.$msec2"]]
    close $sid

    set msec_f "0.$msec2"
    set sec_f [expr {$sec2 + $msec_f + $wait}]
    foreach {sec2 msec2} [split $sec_f "."] {break}
}

set first 1
foreach port $pipelines {
    if {$first} {
        set sid [ socket [ info hostname ] $port ]
        puts $sid "wait"
        flush $sid
        set wait [gets $sid]
        set wait [expr $wait / [llength $pipelines]]
        close $sid

        set first 0
    }

    set sid [ socket [ info hostname ] $port ]
    puts $sid [list cont [clock format $sec_i -format "%x %X.$msec_i"]]
    close $sid

    set msec_f "0.$msec_i"
    set sec_f [expr {$sec_i + $msec_f + $wait}]
    foreach {sec_i msec_i} [split $sec_f "."] {break}
}

