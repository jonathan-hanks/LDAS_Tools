#!/bin/sh
# \
. /ldas/libexec/setup_tclsh.sh 
# \
exec  ${TCLSH} "$0" ${1+"$@"}

source /ldas_usr/ldas/test/bin/AllTest.rsc
;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

proc saveWait {} {

	catch { exec wait.tcl } data

	set fd [ open wait.txt w ]
	puts $fd $data
	close $fd
	set ::done 1
} 

after 0 saveWait 
after 10000 [ list set ::done 1 ]
vwait ::done
cd $::LOOPDIR 
if	{ [ file exist ./joblist.tcl ] } {
	source ./joblist.tcl 
	foreach port [ concat $meta_data $pipelines ] {
		if	{ [ catch {
    		set sid [ socket -async [ info hostname ] $port ]
    		puts $sid stop
			close $sid
		} err ] } {
			puts "$port $err"
		}
	}
}
after 10000
killer pipeloop\.tcl

clean_putStandAlone
exit
