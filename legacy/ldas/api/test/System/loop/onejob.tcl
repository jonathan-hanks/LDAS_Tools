#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

source /ldas_usr/ldas/test/bin/AllTest.rsc
package require LDASJob

;## override some defaults
if      { [ file exist AllTest.rsc ] } {
        source AllTest.rsc
}

set ::DB ""
set ::CMD ""
set ::SITE dev
set ::USER ""
set ::EMAIL ""
set ::CMDFILE ""
set ::VLEVEL 0

proc sendJob {site cmd {varlist ""}} {
    set useropt ""
    set emailopt ""
    set globusopt ""
    set gsiopt ""

    if {[string length $::USER]} {
        set useropt "-user $::USER"
    }
    if {[string length $::EMAIL]} {
        set emailopt "-email $::EMAIL"
    }
 
    if	{ [ info exist ::USE_GLOBUS_CHANNEL ] } {
    	set globusopt "-globus $::USE_GLOBUS_CHANNEL"
    }
    if	{ [ info exist ::GSI_AUTH_ENABLED ] } {
    	set gsiopt "-gsi $::GSI_AUTH_ENABLED"
    }
	
	if	{ [ info exist ::LDAS_VERSION_8 ] } {
		catch {eval LJrun job -nowait -manager $site $useropt $emailopt \$cmd} errmsg
	} else {
    	catch {eval LJrun job -nowait -manager $site $useropt $emailopt $globusopt $gsiopt \$cmd} errmsg
	}

    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg "$::job(error)"
        }
        LJdelete job
        return -code error "Error from LJrun: [string trim $errmsg]"
    }

    vputs [string trim $::job(jobInfo)] "$::job(jobid) ($::job(LDASVersion))"

    if {[string length $::EMAIL] } {
        vputs "Job results will be emailed to $::EMAIL."
        return ""
    }

    catch {LJwait job} errmsg
	
    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg "$::job(error)"
        }
        LJdelete job
        return -code error "Error from LDAS: [string trim $errmsg]"
    }
    vputs [string trim $::job(jobReply)] [strip $::job(jobReply)]

    set retval [list]
    foreach var $varlist {
        lappend retval $::job($var)
    }
    LJdelete job
    return $retval
}

proc vputs {msg1 {msg2 ""} args} {
    if {$::VLEVEL > 0} {
        puts stdout $msg1
    } elseif {[string length $msg2]} {
        puts stdout $msg2
    }
    # flush stdout

    return ""
}

proc strip {data} {
	;## do not replace blanks
    #regsub -all -- {[\n\s\t]+} $data { } data
    ;## some jobs have ===== in the reply
    set idx [string first "=====\n'LDAS API'" $data]
    if {$idx >= 0} {
        set data [string replace $data $idx end]
    }
    set data [ string trim $data = ]
    return [string trim $data \n]
}

proc printUsage {} {
    set scriptname [file tail $::argv0]
    puts stderr "Usage: $scriptname \[-v\] \[-s site\] \[-u user\] \[-e email\] \[-f cmdfile | user-command\] \[-db database\]"
    exit 1
}

if {$::argc == 0} {
    printUsage
}

for {set idx 0} {$idx < $::argc} {incr idx} {
    set opt [lindex $::argv $idx]
    switch -glob -- $opt {
        -db {set ::DB [lindex $::argv [incr idx]]}
        -e -
        -email {set ::EMAIL [lindex $::argv [incr idx]]}
        -f {set ::CMDFILE [lindex $::argv [incr idx]]}
        -h -
        -help -
        --help {printUsage}
        -s -
        -site {set ::SITE [lindex $::argv [incr idx]]}
        -u -
        -user {set ::USER [lindex $::argv [incr idx]]}
        -v {incr ::VLEVEL 1}
        -g { set ::USE_GLOBUS_CHANNEL [lindex $::argv [incr idx]] }
        -gsi { set ::GSI_AUTH_ENABLED [lindex $::argv [incr idx]] }
        -* {
            puts stderr "Error: Invalid option: $opt"
            printUsage
        }
        default {
            set ::CMD $opt
            if {[regexp {\.cmd$} $opt]} {
                set ::CMDFILE $opt
            }
        }
    }
}

if {[string length $::CMDFILE]} {
    if {[catch {
        set fid [open $::CMDFILE r]
        set ::CMD [read -nonewline $fid]
        close $fid
    } err ]} {
        catch {close $fid}
        return -code error "Error: $err"
    }
}

;## set database
if {[string length $::DB]} {
    regsub -- {(-database\s+)[\{]?[^\}]+[\}]?(\s+)?} $::CMD "\\1$::DB\\2" ::CMD
}
puts "SITE $::SITE"
if {[catch {sendJob $::SITE [subst -nobackslashes $::CMD]} errmsg]} {
    puts "$::CMD\n$errmsg"
    exit 1
}

exit 0

