conditionData
    -datacondtarget frame
    -setsingledc 1

    -framequery {
        { R H {} 730524000-730524031 Adc(H0:PEM-LVEA_SEISZ,H1:LSC-AS_Q,H1:LSC-AS_I) }
    }
    -aliases {
        asq = LSC-AS_Q;
        asi = LSC-AS_I;
    }
    -algorithms {
        # Sequence
        s1 = ramp(16384, 0.1, 1.0);
        s2 = ramp(16384, -1.0, 0.1);

        # TimeSeries
        ts1 = slice(asq, 0, 16384, 1);
        ts2 = slice(asi, 0, 16384, 1);
        ph = div(3.14159265, 2.0);
        ts3 = mix(ph, 0.2, ts1);

        # FrequencySequence (and transfer function)
        sc = complex(s1, s2);
        f = fseries(sc, -64.0, 1.0, 730524000, 0, 730524001, 0);

        # PSD and CSD
        p = psd(ts1, 16384);
        p3 = psd(ts3, 16384);
        c = csd(ts1, ts2, 16384);

        output(ts1,_,_,ts1,TimeSeries 1);
        output(ts2,_,_,ts2,TimeSeries 2);
        output(ts3,_,_,ts3,TimeSeries 3);
        output(f,_,_,f,FrequencySequence);
        output(p,_,_,p,PSD);
        output(p3,_,_,p3,PSD);
        output(c,_,_,c,CSD);
    }

