dataPipeline
    -subject {slope online running on H2:LSC-AS_Q }
    -usertag {E9_online_slope_H2:LSC-AS_Q}

    -dynlib libldasslope.so
    -filterparams (-c,,-ifo,H2,-smw,64,-pw,300,-sr,16384,-t,3.5,-sdtype,2,-adapt,yes)

    -np 2
    -datacondtarget wrapper
    -metadatatarget datacond
    -multidimdatatarget frame
    -realtimeratio 0.9
    -database ldas_tst

    -framequery { { R G {} 714096000-714096360 Adc(G1:DER_H_QUALITY) } }
    -responsefiles {
    }
    -aliases {
        x = G1\:DER_H_QUALITY::AdcData;
    }
    -algorithms {
		output(x,_,der_data_h.ilwd,x,input data);
    }

