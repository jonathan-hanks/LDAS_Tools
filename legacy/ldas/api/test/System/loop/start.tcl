#!/bin/sh
# -*- mode: tcl; -*-
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
exec /usr/bin/env tclsh "$0" ${1+"$@"}

#------------------------------------------------------------------------
# This process is responsible to starting the various loopscripts
#------------------------------------------------------------------------
proc start_loopscript { scripts } {
    foreach script_options $scripts {
	set script [ lindex $script_options 0 ]
	set script_options [ lreplace $script_options 0 0 ]
	set user [ split $script ! ]
	if { [ llength $user ] > 1 } {
	    set script [ lindex $user 0 ]
	    set script_options "$script_options -u [lindex $user 1 ]"
	}
	exec /usr/bin/nohup ./pipeloop.tcl $::flags $script_options -f ${script}.${::cmd_ext} >& /dev/null &
    }
}

#------------------------------------------------------------------------
# Query for the socket
#------------------------------------------------------------------------
proc query_loopscript_socket { scripts var } {
    foreach script_options $scripts {
	set script [ lindex $script_options 0 ]
	set script_options [ lreplace $script_options 0 0 ]
	set user [ split $script ! ]
	if { [ llength $user ] > 1 } {
	    set script [ lindex $user 0 ]
	    set script_options "$script_options -u [lindex $user 1 ]"
	}
	set sock_line [ exec grep SOCK ${script}.log ]
	set sock [ split $sock_line ]
	set sock [ lindex $sock [ expr [ llength $sock ] - 1 ] ]
	set $var [ concat [ set $var ] $sock ]
    }
}

#------------------------------------------------------------------------
# Parse the command line options
#------------------------------------------------------------------------
set flags ""
while { $argc > 0 } {
    switch -exact -- [ lindex $argv 0 ] {
	-sync {
	    set argv [ lreplace $argv 0 0 ]
	    set flags "$flags -y"
	}
	default {
	    break
	}
    }
}
#------------------------------------------------------------------------
# Figure out the job mix to use
#------------------------------------------------------------------------
if { [ llength [ lindex $argv 0 ] ] > 0 } {
    set cmd_file [ lindex $argv 0 ]
    source "./${cmd_file}.jm"
} else {
  source ./standard.jm
}
#------------------------------------------------------------------------
#
#------------------------------------------------------------------------
set cmd_ext "cmd"

set ::meta_data_cmds [ list ]
set ::pipelines_cmds [ list ]

if {[info exists meta_data]} {
  start_loopscript $meta_data
}
if {[info exists pipelines]} {
  start_loopscript $pipelines
}

after 5000 ;# Pause for 5 seconds to give the scripts a chance to start

if {[info exists meta_data]} {
    query_loopscript_socket $meta_data ::meta_data_cmds
} else {
    set ::meta_data_cmds [list]
}
if {[info exists pipelines]} {
    query_loopscript_socket $pipelines ::pipelines_cmds
} else {
    set ::pipelines_cmds [list]
}

#------------------------------------------------------------------------
# Store the joblist
#------------------------------------------------------------------------

set fid [ open joblist.tcl w ]
puts $fid "set meta_data { $meta_data_cmds } "
puts $fid "set pipelines { $pipelines_cmds } "
close $fid

#------------------------------------------------------------------------
# Ensure that the identity is known on how to communicate with LDAS
#   server
#------------------------------------------------------------------------

switch -regexp -- [ info hostname ] {
    .*test.* {
	exec ./site.tcl "[ info hostname ]:10001"
    }
}