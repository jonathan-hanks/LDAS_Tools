#!/bin/sh
#
# createrds
#
# A tclsh application for running and managing createRDS user commands
# on a remote LDAS system
#
# Don't change these lines - they are executed only by bash, not Tcl \
exec tclsh "$0" "$@"

lappend ::auto_path /ldas/lib
if      { ! [ regexp "/ldas/bin" $::env(PATH) ] } {
        set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

source /ldas_usr/ldas/test/bin/AllTest.rsc

;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 


# RECENT REVISIONS:
# 04/20/05 gam; all type to be a list; make ifoList global; add channelLists; allow option framequery syntax
# 04/29/05 gam; in normCacheDumpModMinJobLength use last end time instead of gpsNow to decide if gaps should be filled.
# 04/29/05 gam; if alignFrameStartTimes then align jobs with usualMinJobLength expect when producing short frames.
# 08/08/06 gam; Add md5sumregexp option for use with -md5sumregexp; gives sed like regular expression
#               to make replacements in outputdir to give location for md5sum files.
# 08/08/06 gam; Make usejobdirs an option; default is 0 (FALSE).

;##
;## Send a message to the list of users on notifyList
;##
proc sendMail { subject msg } {
  if { [ exec uname ] == "SunOS" } {
    set mailer /usr/ucb/mail
  } else {
    set mailer mail
  }

  exec $mailer -s $subject $::notifyList << $msg
}

;##
;## Send an error message to users on the email notification list
;##
proc errorNotify { msg } {
  set host [ exec hostname ]
  set domain [ exec domainname ]
  set full "${host}.${domain}"
  set subject "Error from createrds at $full"
  set msg "createrds running at: ${full}\nLDAS Site: ${::site}\nLDAS User: ${::user}\nError message:\n${msg}"
  #sendMail $subject $msg
  logfile $msg 
}

;##
;## Move the log files into the archive
;##
proc archiveLogfiles {} {
  set now [ tconvert now ]
  
  if { [ file exists "logs/createrds.log" ] } {
    file rename "logs/createrds.log" "logs/archive/createrds.${now}.log"
  }
  
  if { [ file exists "logs/DSOcreaterds.log.html" ] } {
    file rename "logs/DSOcreaterds.log.html" \
      "logs/archive/DSOcreaterds.${now}.log.html"
  }
}

;##
;## Add a message to the HTML log with the given colored ball
;##
proc addHtmlLogEntry { line { color "green" } }  {
  if { $color == "red" } {
     set gif "ball_red.gif"
     set state "ERROR"
  } elseif { $color == "yellow" } {
     set gif "ball_yellow.gif"
     set state "WARN"
  } elseif { $color == "blue" } {
     set gif "ball_blue.gif"
     set state "MESG"
  } else {
     set gif "ball_green.gif"
     set state "OK"
  }

  set gpsNow [ tconvert now ]
  set timeNow [ clock format [ clock seconds ] -format %c ]

  set outString "<img src=\"$gif\" width=\"14\" height=\"12\" alt=\"$timeNow\" title=\"$timeNow\"> <b>$gpsNow</b> <b><i><font color=\"$color\">$state</font></i></b> <b><i><font color=\"black\">createRDS</font></i></b> <tt>$line</tt><br>"

  puts $::html_oid $outString
  flush $::html_oid
}

;##
;## Write to the logfile
;##
proc logfile { line { color "green" } } {
  set clockSeconds [ clock seconds ]
  set locTime [ clock format $clockSeconds -format %c ]
  puts $::oid "$locTime $line"
  flush $::oid

  addHtmlLogEntry $line $color

  # Archive logs once they reach a certain size
  incr ::LOGLINES
  if { $::LOGLINES >= $::MAX_LOGLINES } {
    close $::oid
    close $::html_oid

    archiveLogfiles

    set ::oid [ open "logs/createrds.log" w ]
    set ::html_oid [ open "logs/DSOcreaterds.log.html" w ]

    set ::LOGLINES 0
  }
}

;##
;## Overwrite the status file. This information is used by dsorun
;## for displaying status information on the web
;##
proc writeStatusfile {} {
  # NOTE - dsorun doesn't like status file names with any capital letters
  set fid [ open "createrds.status" w ]

  puts $fid "$::STATUS $::LAST_SUBMITTED $::LAST_SUBMITTED_TIME \
$::LAST_SUBMITTED_STIME $::LAST_SUBMITTED_ETIME $::LAST_COMPLETED \
$::LAST_COMPLETED_TIME $::LAST_COMPLETED_STIME $::LAST_COMPLETED_ETIME 0"

  close $fid
}

;##
;## Exit with a code and a message. If the code is non-zero, the message
;## is also printed to stderr
;##
proc cleanupAndExit { code msg } {
  if { $code == 0 } {
    set color "green"
  } else {
    set color "red"
    puts stderr $msg
  }

  logfile "$msg" "$color"
  logfile "Total data processed this run: $::totalTimeReduced seconds"
  logfile "Grand total data processed: [setSize $::COMPLETED_RDS] seconds"
  close $::oid

  set recfid [ open "createrds.rec" w ]
  puts $recfid ";## Record of previous data reduction"
  puts $recfid ";##   LDAS System: $::site"
  puts $recfid ";##   Input IFOs: $::ifoList"; # 04/20/05 gam;
  puts $recfid ";##   Input frame types: $::type"; # 04/20/05 gam;
  puts $recfid ";##   Output frame type: [RDSType $::type $::userType]"
  if { [ llength $::COMPLETED_RDS ] == 0 } {
    puts $recfid "set ::COMPLETED_RDS {}"
  } else {
    puts $recfid "set ::COMPLETED_RDS \{ $::COMPLETED_RDS \}"
  }
  close $recfid

  file delete "sHuTdOwN"
  file delete "createrds.status"
  file delete $::LOCKFILE

  exit $code
}

;##
;## Return the RDS type based on the input frame type and the user type
;##
proc RDSType { type userType } {
  # if { [ regexp {[0-9]+$} $userType currentLevel ] } # 04/20/05 gam
  if { [ regexp {[0-9]+$} $userType currentLevel ] || [llength $::type] > 1 || $::leaveUserTypeAsIs } {
    # e.g., userType ends in 0-9 or type is a list, or option set to leave as is.
    return ${userType}
  } else {
    # Match any trailing digits in the input frame type
    if { [ regexp {[0-9]+$} $type lastLevel ] } {
      set level [ expr $lastLevel + 1 ]
    } else {
      set level 1
    }
    return ${userType}${level}
  }
}

;##
;## Build the list of channels and their respective downsampling factors
;## from a list of strings of the form "<channel name> <downsampling factor>"
;##
proc buildChannels { adcList } {
  set ::numAdcChannels 0
  set ::numProcChannels 0
  set channels {}
  set ifoListTmp {}; # 04/20/05 gam; change ifoList to ifoListTmp; if ::ifoList == "" set ::ifoList to $ifoListTmp below.

  foreach line $adcList {
    set line [ string trim $line ]

    # Skip blank and comment lines
    if { ![string length $line] || [regexp {^\#} $line] } {
      continue
    }

    if { [ llength $line ] != 2 } {
      cleanupAndExit 1 "Improperly formatted line in $::adcFile"
    }
    
    foreach { adc dec } $line { continue }

    set ifo [ string index $adc 0 ]
    if { [ lsearch -exact $ifoListTmp $ifo ] == -1 } {
      lappend ifoListTmp $ifo
    }

    if { $dec == 1 } {
      lappend channels $adc
      incr ::numAdcChannels
    } else {
      lappend channels "${adc}!$dec"
      incr ::numProcChannels
    }
  }

  if { $::numProcChannels > 0 } {
    set ::DOWNSAMPLING $::TRUE
  } else {
    set ::DOWNSAMPLING $::FALSE
  }

  # 04/20/05 gam; before sorting, save global ::ifoList if not set up in .rsc file.
  if { [llength $::ifoList] == 0} {
     set ::ifoList $ifoListTmp;
  }
  if { [llength $::type] != [llength $::ifoList] && $::useFrameQuery} {
          cleanupAndExit 1 "Error, invalid resource file parameter: type or ifoList. These parameters must be lists of the same length.";
  }

  set ifoListTmp [ lsort $ifoListTmp ]
  set ::RDSFILE_PREFIX {}
  foreach ifo $ifoListTmp {
    set ::RDSFILE_PREFIX ${::RDSFILE_PREFIX}${ifo}
  }

  return $channels
}

;## 04/20/05 gam
;## Build channelLists with is a list of channel lists for each IFO
;##
proc buildChannelLists { adcList } {
  set channelLists {}

  foreach ifo $::ifoList {

    # Get the channels for just this ifo
    set tmpChannels {}
    foreach line $adcList {
      set line [ string trim $line ]

      # Skip blank and comment lines
      if { ![string length $line] || [regexp {^\#} $line] } {
        continue
      }

      if { [ llength $line ] != 2 } {
        cleanupAndExit 1 "Improperly formatted line in $::adcFile"
      }
    
      foreach { adc dec } $line { continue }

      set qifo [ string index $adc 0 ]
      
      if { $qifo == $ifo } {
        if { $dec == 1 } {
          lappend tmpChannels $adc
        } else {
          lappend tmpChannels "${adc}!$dec"
        }
      }
      
    }
    # END foreach line $adcList

    if { [ llength $tmpChannels ] > 0 } {
         set tmpChannels [join $tmpChannels ","]
         lappend channelLists $tmpChannels
    }

  }
  # END foreach ifo $::ifoList
  
  return $channelLists
}

proc createChannelList {} {
  # Read channel list from file
  if { ! [ file exists $::adcFile ] } {
    cleanupAndExit 1 "File not found: $::adcFile"
  }

  if { [ catch { open $::adcFile r } fid ] } {
    cleanupAndExit 1 "Error opening file: $::adcFile"
  }

  set adcList [ split [ read -nonewline $fid ] "\n" ]
  close $fid

  if { ! [ llength $adcList ] } {
    cleanupAndExit 1 "Empty channel list in file \"$::adcFile\""
  }

  set ::channels [ buildChannels $adcList ]

  # 04/20/05 gam; also get ::channelLists = a list of channel lists for each IFO
  set ::channelLists [ buildChannelLists $adcList ]
  #foreach chanList $::channelLists {
  #    puts "chanList = $chanList"
  #}

}

;##
;## This function initializes all of the global variables and constants.
;## It ought to only be called once on startup.
;##
proc initializeGlobals {} {
  ;##
  ;## Global constants - these values should never be changed
  ;## 
  set ::TRUE 1
  set ::FALSE 0
  set ::RESOURCE_FILE "createrds.rsc"

  ;##
  ;## Each of these variables is a list of time intervals of
  ;## the form "{a b} {c d} ..." representing a set of start and end times
  ;##
  set ::DISKCACHE {}       ;## Time intervals known to the LDAS diskcache
  set ::RDS_DISKCACHE {}   ;## RDS time intervals known to the diskcache
  set ::IN_PROGRESS_SET {} ;## Set of time intervals current being reduced
  set ::ERROR_SET {}       ;## Set of time intervals where errors occurred

  ;## milliseconds between job status updates
  set ::UPDATE_INTERVAL [ expr 5*1000 ]

  ;## milliseconds to wait after a submit error
  set ::ERR_INTERVAL [ expr 1800*1000 ] ;## Changed from 120*1000 to 1800*1000 05/27/04

  ;## milliseconds to wait if the diskcache is empty
  #set ::diskCacheInterval [ expr 1800*1000 ] # 01/19/05 gam; now a .rsc parameter.
  
  ;## How many times to retry a failed job
  set ::maxRetries 3

  ;## List of object ID's in the progress bar canvas
  set ::OBJLIST {}

  ;## Variables for status file
  set ::STATUS "war"
  set ::LAST_SUBMITTED "-"
  set ::LAST_SUBMITTED_TIME 0
  set ::LAST_SUBMITTED_STIME 0
  set ::LAST_SUBMITTED_ETIME 0
  set ::LAST_COMPLETED "-"
  set ::LAST_COMPLETED_TIME 0
  set ::LAST_COMPLETED_STIME 0
  set ::LAST_COMPLETED_ETIME 0

  ;## Log lines to write before rolling over into archive
  set ::MAX_LOGLINES 1000

  ;## Create log directory(s) if necessary
  if { [ file exists "logs" ] } {
    if { ! [ file isdirectory "logs" ] } {
      puts stderr "File \"logs\" exists and is not a directory"
      exit 1
    }
  } else {
    file mkdir "logs"
  }

  if { [ file exists "logs/archive" ] } {
    if { ! [ file isdirectory "logs/archive" ] } {
      puts stderr "File \"logs/archive\" exists and is not a directory"
      exit 1
    }
  } else {
    file mkdir "logs/archive"
  }

  ;## Number of lines written to logs since last archiving
  set ::LOGLINES 0

  # Archive old logfiles
  archiveLogfiles

  # Open new log files
  set ::oid [ open "logs/createrds.log" a ]
  set ::html_oid [ open "logs/DSOcreaterds.log.html" a ]
  logfile "Started createrds.tcl"

  ;##
  ;## Global variables
  ;##

  ;## Set to TRUE if the user starts the script with the --start option
  ;## If true, the app will immediately begin reducing data
  # set ::AUTO_START $::FALSE; # Only used with GUI.

  # The list of time intervals that have been completed by this
  # instance of the app
  if { [ file exists "createrds.rec" ] } {
    if { [ catch { source createrds.rec } resultSourceRecFile ] } {
      puts stderr " File createrds.rec is not valid; sourcing this file resulted in the error:\n $resultSourceRecFile \n Please fix or remove this file and try again."
      file delete "createrds.status"
      # file delete $::LOCKFILE; # does not yet exist.
      exit 1    
    }
    if { ! [ info exists ::COMPLETED_RDS ] } {
      ;## This is an instance where we want to exit immediately without
      ;## going through the standard exit routine, because we don't want
      ;## to overwrite the corrupted .rec file
      puts stderr "File createrds.rec does not contain the record of previous data reduction. Please remove this file and try again."
      file delete "createrds.status"
      # file delete $::LOCKFILE; # does not yet exist.
      exit 1
    }  
  } else {
    set ::COMPLETED_RDS {}
  }

  # Percentage of the total time that has been reduced
  set ::percentComplete 0.0

  # Set the initial state and status bar message
  set ::STATE idle
  set ::SBTEXT "Idle"

} ;## initializeGlobals

;##
;## This function sets the minimum allowed job length based on createRDS parameters.
;##
proc setMinJobLength {} {
  set ::usualMinJobLength [expr $::framesperfile*$::secperframe] ; ## Usual minimum job length to use, except when filling in near a gap.
  if {$::allowshortframes} {  
    set ::minJobLength 1                                      ;## Min. data to reduce in seconds per job  
  } else {
    set ::minJobLength [expr $::framesperfile*$::secperframe] ;## Min. data to reduce in seconds per job
  }
}

;##
;## This function sets the initial defaults for createRDS parameters.
;## It should be run once at startup BEFORE reading the resource file,
;## and then never again.
;##
proc setCreateRDSDefaults {} {
  set ::startTime 729273600 ;## Start of S2
  set ::endTime 734374813   ;## End of S2
  set ::maxJobLength 256    ;## Max. data to reduce in seconds
  set ::maxJobs 2           ;## Max. simultaneous jobs to run
  set ::adcFile "$::BINDIR/adcdecimate_lho_s2.txt"
  set ::site cit            ;## LDAS site
  set ::user nobody         ;## LDAS username
  set ::notifyList "nobody@nowhere.com"
  set ::type "R"            ;## Input frame type
  set ::useFrameQuery 0     ;## 1 = TRUE 0 = FALSE Must be 1 if length type list > 1; if 1 use framequery syntax with createRDS cmd
  set ::ifoList [ list  ]   ;## List of IFOs; if empty will get set up based on channel list; 04/20/05 gam;
  set ::userType "RDS_R_L"  ;## User type for output frames
  set ::leaveUserTypeAsIs 0 ;## 04/20/05 gam; option to not change userType even if it does not end in a number
  set ::excludeRDS "Y"      ;## Exclude existing RDS data
  set ::frameLength 32      ;## Standard length of a frame file in seconds
  set ::updateFrameLength 0 ;## 1 = TRUE 0 = FALSE; dynamically update the frameLength when DOWNSAMPLING for every segment.
  set ::diskCacheInterval 1800;## Seconds to wait if the diskcache is empty
  set ::includedirs ""      ;## Reduce input data from these directories only
  set ::excludedirs ""      ;## Do not reduce input data from any of these directories
  set ::usejobdirs 0        ;## Default is use outputdir, not job directories
  set ::outputdir "/archive/LHO/rds/S2"
  set ::md5sumregexp ""     ;## Do not make sed like substitution of outputdir to get path to md5sum files.
  set ::binByTime "Y"
  set ::secPerBin "100000"  ;## Number of seconds of data to put into each directory (must be a multiple of 10 between 10 and 10000000; default is 100000)
  set ::compressiontype "gzip"
  set ::compressionlevel 1
  set ::filechecksum 1      ;## 1 = TRUE 0 = FALSE; if TRUE frame file checksums are validated before running job
  set ::frametimecheck 1    ;## 1 = TRUE 0 = FALSE; if TRUE frame times are validated against the frame name before running job
  set ::framedatavalid 1    ;## 1 = TRUE 0 = FALSE; if TRUE datavalid flags are checked for every channel before running job
  set ::framesperfile 1             ;## Number of frames to put in each output file
  #set ::secperframe $::frameLength  ;## Number of seconds of data to put into each frame
  #set ::secperframe "notSet"        ;## 11/12/04; number of seconds of data to put into each frame; will set to $::frameLength if not set in the resource file.
  set ::secperframe 32               ;## 01/19/05 gam; 11/12/04; number of seconds of data to put into each frame.
  set ::alignFrameStartTimes 1      ;## 04/29/05 gam; 1 = TRUE 0 = FALSE; if TRUE align frame start times with usualMinJobLength expect when producing short frames.
  set ::allowshortframes 0          ;## 1 = TRUE 0 = FALSE; if TRUE allow short frame with less than secperframe in a frame
  set ::allowShortForLatestSeg 0    ;## 1 = TRUE 0 = FALSE; if FALSE never allow short frames when running on the latest data segment
  set ::fillSegmentsIfThisOld 3600  ;## If allowshortframes is TRUE, fill in a segment with short frames if its end time is this old (in seconds).
  set ::generatechecksum 1          ;## 1 = TRUE 0 = FALSE; if TRUE generate frame checksums on a per frame basis
  set ::fillmissingdatavalid 0      ;## 1 = TRUE 0 = FALSE; if TRUE fill any missing channel datavalid aux vec's in the output frames
  #setMinJobLength                   ;## Call function to set ::minJobLength to minimum data to reduce in seconds per job
  #set ::minJobLength [expr $::framesperfile*$::frameLength] ;## 11/12/04; set default ::minJobLength to minimum data to reduce in seconds per job
  setMinJobLength                   ;## 01/19/05 gam; Call function to set ::minJobLength to minimum data to reduce in seconds per job  
  set ::totalTime [ expr $::endTime - $::startTime ];
}

;## 
;## This function vets the createrds resource file parameters.
;## Added 07/02/2004.
proc vetCreateRDSParameters {} {
  set vetOut "";
  set vetErrors 0;
  set vetEmptyString "";
  
  set vetIn 0;
  if { [ catch { set vetIn [expr $::startTime < 100000000 || $::startTime > 999999999 ] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: startTime. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: startTime. The startTime parameter is less < 100000000 or > 999999999. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr $::endTime < 100000000 || $::endTime > 999999999 ] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: endTime. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: endTime. The endTime parameter is less < 100000000 or > 999999999. \n";
       set vetErrors 1;
     }
  }
  
  set ::totalTime [ expr $::endTime - $::startTime ];
  
  set vetIn 0;  
  if { [ catch { set vetIn [expr $::maxJobLength < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: maxJobLength. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: maxJobLength. The maxJobLength parameter is less < 1. \n";
       set vetErrors 1;
     }
  }
  
  set vetIn 0;
  if { [ catch { set vetIn [expr $::maxJobs < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: maxJobs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: maxJobs. The maxJobs parameter is less < 1. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::adcFile $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: adcFile. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: adcFile. The adcFile parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::site $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: site. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: site. The site parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::user $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: user. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: user. The user parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::notifyList $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: notifyList. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank notifyList is OK.
  }

  set vetIn 0;
  set ::type [string trimleft $::type];  # 04/20/05 gam; make sure no extra spaces exist
  set ::type [string trimright $::type]; # 04/20/05 gam  
  if { [ catch { set vetIn [string equal $::type $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: type. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: type. The type parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::useFrameQuery != 0) && ($::useFrameQuery != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: useFrameQuery. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: useFrameQuery. The useFrameQuery parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }
  set vetIn 0;  
  if { [ catch { set vetIn [expr [llength $::type] > 1 && $::useFrameQuery != 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: useFrameQuery or type. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: useFrameQuery or type. The useFrameQuery parameter must be 1 if length of type list > 1. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [llength $::ifoList] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: ifoList. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn == 0} { 
       # This is OK, ::ifoList will get set up based on the channel list.
     } else {
        if { [llength $::type] != [llength $::ifoList] && $::useFrameQuery} {
          puts "\n Error, invalid resource file parameter: type or ifoList. These parameters must be lists of the same length. \n";
          set vetErrors 1;
        }
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::userType $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: userType. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: userType. The userType parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::leaveUserTypeAsIs != 0) && ($::leaveUserTypeAsIs != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: leaveUserTypeAsIs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: leaveUserTypeAsIs. The leaveUserTypeAsIs parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr [lsearch {y Y n N} $::excludeRDS] < 0] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: excludeRDS. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: excludeRDS. The excludeRDS parameter is not set to y, Y, n, or N. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;  
  if { [ catch { set vetIn [expr $::frameLength < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: frameLength. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: frameLength. The frameLength parameter is less < 1. \n";
       set vetErrors 1;
     }
  }
  
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::updateFrameLength != 0) && ($::updateFrameLength != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: updateFrameLength. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: updateFrameLength. The updateFrameLength parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }  

  set vetIn 0;  
  if { [ catch { set vetIn [expr $::diskCacheInterval < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: ::diskCacheInterval. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: ::diskCacheInterval. The ::diskCacheInterval parameter is less < 1. \n";
       set vetErrors 1;
     }
  }
  
  set vetIn 0;
  if { [ catch { set vetIn [string equal $::includedirs $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: includedirs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank includedirs is OK.
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::excludedirs $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: excludedirs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank excludedirs is OK.
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::usejobdirs != 0) && ($::usejobdirs != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: usejobdirs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: usejobdirs. The usejobdirs parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::outputdir $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: outputdir. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: outputdir. The outputdir parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::md5sumregexp $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: md5sumregexp. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank md5sumregexp is OK.
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr [lsearch {y Y n N} $::binByTime] < 0] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: binByTime. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: binByTime. The binByTime parameter is not set to y, Y, n, or N. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04: new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr $::secPerBin < 10 || $::secPerBin > 10000000 ] } vetOut ] } {  
     puts "\n Error, invalid resource file parameter: secPerBin. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn != 0} {
        puts "\n Error, invalid resource file parameter: secPerBin. The secPerBin parameter is out of range 10 to 10000000. \n";
       set vetErrors 1;
     }
  }
  
  # 11/10/04: further vet new parameter:  
  set vetIn 0;
  if { [ catch { set vetIn [expr $::secPerBin % 10] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: secPerBin. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn != 0} {
        puts "\n Error, invalid resource file parameter: secPerBin. The secPerBin parameter is not a multiple of 10. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::compressiontype $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: compressiontype. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: compressiontype. The compressiontype parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;  
  if { [ catch { set vetIn [expr $::compressionlevel < 0] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: compressionlevel. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: compressionlevel. The compressionlevel parameter is less < 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::filechecksum != 0) && ($::filechecksum != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: filechecksum. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: filechecksum. The filechecksum parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::frametimecheck != 0) && ($::frametimecheck != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: frametimecheck. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: frametimecheck. The frametimecheck parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::framedatavalid != 0) && ($::framedatavalid != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: framedatavalid. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: framedatavalid. The framedatavalid parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; add new parameter:
  set vetIn 0;  
  if { [ catch { set vetIn [expr $::framesperfile < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: framesperfile. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: framesperfile. The framesperfile parameter is less than 1. \n";
       set vetErrors 1;
     }
  }
  
  # 11/10/04; add new parameter:
  # 01/19/05 gam; secperframe now has a default value of 32.
  #if {$::secperframe == "notSet"} {
  #   set ::secperframe $::frameLength  ;## 11/12/04; number of seconds of data to put into each frame; set to $::frameLength if not set in the resource file.
  #}  
  set vetIn 0;  
  #if { [ catch { set vetIn [expr $::secperframe < $::frameLength] } vetOut ] }
  if { [ catch { set vetIn [expr $::secperframe < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        #puts "\n Error, invalid resource file parameter: secperframe. The secperframe parameter is less than the frameLength. \n";
        puts "\n Error, invalid resource file parameter: secperframe. The secperframe parameter is less than 1. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; next check that $::secperframe is an integer multiple of $::frameLength
  # 01/19/05 gam; no longer needs to be true with LDAS v 1.4.0
#  set vetIn 0;  
#  if { [ catch { set vetIn [expr 1.0*$::secperframe / (1.0*$::frameLength)] } vetOut ] } {
#     puts "\n Error, invalid resource file parameter: secperframe. Error message:\n $vetOut \n.";
#     set vetErrors 1;
#  } else {
#     if { [expr 1.0*floor($vetIn) - $vetIn] != 0 } {
#        puts "\n Error, invalid resource file parameter: secperframe. The secperframe parameter is not a multiple of the frameLength. \n";
#       set vetErrors 1;
#     }
#  }

  # 04/29/05 gam; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::alignFrameStartTimes != 0) && ($::alignFrameStartTimes != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: alignFrameStartTimes. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: alignFrameStartTimes. The alignFrameStartTimes parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::allowshortframes != 0) && ($::allowshortframes != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: allowshortframes. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: allowshortframes. The allowshortframes parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 01/25/05; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::allowShortForLatestSeg != 0) && ($::allowShortForLatestSeg != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: allowShortForLatestSeg. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: allowShortForLatestSeg. The allowShortForLatestSeg parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 01/25/05; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr $::fillSegmentsIfThisOld < -1000000000] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: fillSegmentsIfThisOld. The parameter should be a number. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: fillSegmentsIfThisOld. The fillSegmentsIfThisOld parameter must be number greater than -1000000000. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::generatechecksum != 0) && ($::generatechecksum != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: generatechecksum. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: generatechecksum. The generatechecksum parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }
  
  # 11/10/04; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::fillmissingdatavalid != 0) && ($::fillmissingdatavalid != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: fillmissingdatavalid. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: fillmissingdatavalid. The fillmissingdatavalid parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; 11/12/04; update minimum job length:
  # setMinJobLength  ;## Call function to set ::minJobLength to minimum data to reduce in seconds per job
  if { [ catch { setMinJobLength  } vetOut ] } {
     puts "\n Error, invalid resource file parameters: could not compute framesperfile*secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } 

  # 11/10/04; vet minJobLength:
  set vetIn 0;  
  if { [ catch { set vetIn [expr $::usualMinJobLength > $::maxJobLength] } vetOut ] } {
     puts "\n Error, invalid resource file parameters: could not compute framesperfile*secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameters: framesperfile*secperframe. The framesperfile*secperframe is less than the maxJobLength. \n";
       set vetErrors 1;
     }
  }
  
  # 11/11/04; further vet minJobLength:
  set vetIn 0;  
  if { [ catch { set vetIn [expr $::maxJobLength % $::usualMinJobLength] } vetOut ] } {
     puts "\n Error, invalid resource file parameters: could not compute maxJobLength mod framesperfile*secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn != 0} {
        puts "\n Error, invalid resource file parameters: maxJobLength or framesperfile*secperframe. The maxJobLength must be a multiple of framesperfile*secperframe. \n";
       set vetErrors 1;
     }
  }

  return $vetErrors;
}

;##
;## This function is run each time we press the Start button on the
;## createRDS jobs, in order to set all the internally used variables
;## back to their initial values.
;##
proc resetGlobalVariables {} {
  # Wall time at last start request
  set ::timeStartedS [ clock seconds ]
  set ::timeStarted [ clock format $::timeStartedS -format %c ]

  # A counter to keep track of how many times we've looped in
  # the running state. This is to get rid of spurious rate messages
  # occurring when we have to wait for new jobs
  set ::runningCtr 0

  # Total time reduced since the Start button was pushed
  set ::totalTimeReduced 0

  # This ensures that the diskcache is updated each time we restart
  # Not strictly necessary, but makes things a little safer
  set ::DISKCACHE {}

  set ::realtimeRatio 1.0
  set ::overallRealtimeRatio 1.0
  set ::realtimeRatioMsg "Seconds Of Data Reduced/Elapsed Wall Time: --"
  set ::realtimeRatioAlarm $::FALSE
  set ::jobCtr 0
  set ::failedJobCtr 0
  set ::currentExistingJobs 0
  set ::IN_PROGRESS_SET {}
} ;## resetGlobalVariables

;##
;## Decr function, to complement incr
;##
proc decr { a } {
  upvar 1 $a aa
  set aa [ expr $aa - 1 ]
}

;##
;## Return the list of times contained in the diskcache for a given
;## IFO and type
;##
proc diskcacheTimes { qifo qtype } {
  upvar ::DISKCACHE_IMAGE cache

  set output {}

  # New format, looks like this:
  # /archive/LLO/full/S1/L-R-7132,L,R,1,16 1044788590 1125 {713248208 713255408 713259008 713269808}

  # Loop over directories in the cache with type $qtype
  foreach { key value } [ array get cache *,*,$qtype,* ] {
    foreach { dir ifo type nperfile dt } [ split $key "," ] continue

    # If includedirs is not an empty list, reduce input data from includedirs only
    if { [string trim $::includedirs] > ""} {
       set dirMatchFound $::FALSE;
       foreach qdir $::includedirs {
         if { [ string match *${qdir}* $dir ] } {
            set dirMatchFound $::TRUE;
            break;
         }
       }
       if { ! $dirMatchFound } continue
    }

    # If excludedirs is not an empty list, do not reduce any input data from excludedirs
    if { [string trim $::excludedirs] > ""} {
       set dirMatchFound $::FALSE;
       foreach qdir $::excludedirs {
         if { [ string match *${qdir}* $dir ] } {
            set dirMatchFound $::TRUE;
            break;
         }
       }
       if { $dirMatchFound } continue
    }

    # Skip any that don't match this IFO
    if { ! [ string match *${qifo}* $ifo ] } continue

    # Get the list of times
    set timesList [ lindex $value 2 ]
    foreach { start end } $timesList {
      lappend output [ list $start $end ]
    }

  }

  # Sort output based on start-time
  set output [ lsort -integer -index 0 $output ]

  # Merge adjacent or overlapping segments
  set merged {}
  foreach interval $output {
    set merged [ setIntervalUnion $merged $interval ]
  }
  
  return $merged
}

;##
;## Merge the elements of the RDS cache dump
;##
proc normalizeRDSCacheDump {} {

  set rdsType [ RDSType $::type $::userType ]
  set ::RDS_DISKCACHE [ diskcacheTimes $::RDSFILE_PREFIX $rdsType ]
  set interval [ list $::startTime $::endTime ]
  set interval [ list $interval ]
  set ::RDS_DISKCACHE [ setIntersection $::RDS_DISKCACHE $interval ]

} ;## normalizeRDSCacheDump

;##
;## Process the raw diskcache info, merging adjacent segments that have the
;## same dt. Then, if downsampling is being done, remove one dt from the start
;## and end.
;##
;## The result is list of intervals, any subinterval of which will be 
;## a valid -times option to createRDS
;##
proc normalizeCacheDump {} {

  # Generate the list of times where raw data is present
  # This is created by running through each interferometer that
  # is needed (as determined from the output frame prefix) and
  # taking the intersection.

  # For each IFO, take the intersection with the current set
  # of time spans
  set merged {}
  #foreach ifo [ split $::RDSFILE_PREFIX {} ] # 04/20/05 gam
  set typeIndex 0; # rewrite to use type that goes with each IFO
  foreach ifo $::ifoList {
    set typeThisIFO [lindex $::type $typeIndex];
    incr typeIndex;
    # set T [ diskcacheTimes $ifo $::type ]
    set T [ diskcacheTimes $ifo $typeThisIFO ]
    if { [ llength $merged ] == 0 } {
      set merged $T
    } else {
      set merged [ setIntersection $merged $T ]
    }
  }
  
  # 01/19/05 gam
  if {$::updateFrameLength && $::DOWNSAMPLING} {
     # Trim the range of times down to what the user requested + a bit extra
     set interval [ list [expr $::startTime - $::maxJobLength] [expr $::endTime + $::maxJobLength] ]
     set merged [ setIntervalIntersection $merged $interval ]
  }

  #
  # If downsampling, trim off the ends of the intervals that we can't
  # actually process in createRDS anyway. Prevents errors due to
  # missing data. Otherwise, just use the given start and end times
  # 
  set result {}
  foreach interval $merged {
    foreach { start end } $interval {
      if { $::DOWNSAMPLING } {
        # 01/19/05 gam
        if {$::updateFrameLength} {
           #set dynamicFrameLength [getFrameLength $start $::frameLength] # 04/20/05 gam
           set dynamicFrameLength 0
           set qtypeIndex 0
           foreach qtype $::type {
              set thisDynamicFrameLength [getFrameLength $start $::frameLength $qtype $qtypeIndex]
              incr qtypeIndex;
              if {$thisDynamicFrameLength > $dynamicFrameLength} {
                 set dynamicFrameLength $thisDynamicFrameLength
              }
           }
           set newStart [ expr $start + $dynamicFrameLength ]
           #set dynamicFrameLength [getFrameLength [expr $end - 2] $::frameLength]; # Need -2 because of current way LDAS works.
           set dynamicFrameLength 0
           set qtypeIndex 0
           foreach qtype $::type {
              set thisDynamicFrameLength [getFrameLength [expr $end - 2] $::frameLength $qtype $qtypeIndex]
              incr qtypeIndex;
              if {$thisDynamicFrameLength > $dynamicFrameLength} {
                 set dynamicFrameLength $thisDynamicFrameLength
              }
           }
           set newEnd [ expr $end - $dynamicFrameLength ]
        } else {
          set newStart [ expr $start + $::frameLength ]
          set newEnd [ expr $end - $::frameLength ]
        }
        lappend result [ list $newStart $newEnd ]
      } else {
        lappend result [ list $start $end ]
      }
    }
  }
  
  # Trim the range of times down to what the user requested
  set interval [ list $::startTime $::endTime ]
  set ::DISKCACHE [ setIntervalIntersection $result $interval ]

} ;## normalizeCacheDump

;##
;## This procedure updates a text widget w with a message
;##
proc updateStatusWindow { w msg } {
}

;##
;## Submit a job in the background and retry until it is accepted.
;##
;## This function doesn't return until the job is accepted, but doesn't
;## block the event loop.
;##
proc submitJob { job cmd } {

  if	{ [ info exist ::LDAS_VERSION_8 ] } {
  		LJrun ::$job -manager $::site -user $::user -nowait $cmd } err
  } else {
      	LJrun ::$job -manager $::site -user $::user -nowait -globus $::USE_GLOBUS_CHANNEL -gsi $::USE_GSI $cmd
  }

  if { $::LJerror != 0 } {
    set errmsg [ set ::${job}(error) ]
    regsub -all -- {[\n\s\t]+} $errmsg { } errmsg
    LJdelete ::$job
    return -code error $errmsg
  } else {
    set jobInfo [ set ::${job}(jobInfo) ]
    regsub -all -- {[\n\s\t]+} $jobInfo { } jobInfo
    set ::LAST_SUBMITTED [ set ::${job}(jobid) ]
    set ::LAST_SUBMITTED_TIME [ tconvert now ]
    return
  }
}

;##
;## A blocking function to wait for an LDAS job to complete
;##
proc waitForJob { job } {
  LJstatus ::$job
  set status [ set ::${job}(status) ]
  while { $status == "running" || $status == "submitted" } {
    update idletasks
    after 1000
    LJstatus ::$job
    set status [ set ::${job}(status) ]
  }

  # Now call LJwait on the job (ought to be instantaneous) and return
  LJwait ::$job
}

;##
;## Query LDAS to get the frameLength for a give GPS time
;##
#proc getFrameLength {inputTimes defaultFrameLength} # 04/20/05 gam
proc getFrameLength {inputTimes defaultFrameLength qtype qtypeIndex} {

  set returnFrameLength $defaultFrameLength;
  
  # Clean up any old cacheGetFilenamesJob
  if { [ info exists ::cacheGetFilenamesJob ] } {
    LJwait ::cacheGetFilenamesJob
    LJdelete ::cacheGetFilenamesJob
  }

  #set cmd "cacheGetFilenames -times { $inputTimes } -type { $qtype } -channels { $::channels }" # 04/20/05 gam
  set tmpChannels [lindex $::channelLists $qtypeIndex]
  set cmd "cacheGetFilenames -times { $inputTimes } -type { $qtype } -channels { $tmpChannels }"
  submitJob cacheGetFilenamesJob $cmd    
  waitForJob cacheGetFilenamesJob
      
  if { $::cacheGetFilenamesJob(status) == "done" } {
    set frameNameJobOutput $::cacheGetFilenamesJob(jobReply);
    LJdelete ::cacheGetFilenamesJob    
    if { ! [ regexp {/.*\.gwf} $frameNameJobOutput frameNameList ] } {
       set msg "Could not find frame name to get frameLength, using default"
       log $msg
       logfile $msg red
       errorNotify $msg
       return $returnFrameLength;
    } else {
       foreach frameName [split $frameNameList] {
         regexp {\-([0-9]+)\-([0-9]+).gwf} $frameName match thisFrameStartTime thisFrameLength;
         if {$thisFrameLength > $returnFrameLength} {
            set returnFrameLength $thisFrameLength;
         }
       }
       set msg "Succesfully ran cacheGetFilenames on GPS time $inputTimes to update frameLength to $returnFrameLength s."
       logfile $msg
    }
  } elseif { $status == "error" } {
    set msg \
      "$::cacheGetFilenamesJob(jobid) Error running cacheGetFilenamesJob user command: \"$::cacheGetFilenamesJob(error)\""
    log $msg
    logfile $msg red
    errorNotify $msg
    #update idletasks
    LJdelete ::cacheGetFilenamesJob
  } else {
    cleanupAndExit 1 "cacheGetFilenamesJob: invalid status \"$::cacheGetFilenamesJob(status)\""
  }

  return $returnFrameLength;
}

;##
;## Query LDAS to get the current diskcache contents
;##
proc getDiskCache {} {
  # Clean up any old diskCacheJob's
  if { [ info exists ::diskCacheJob ] } {
    LJwait ::diskCacheJob
    LJdelete ::diskCacheJob
  }

  # Run an LDAS job to get the frame cache info
  set cmd "getFrameCache -returnprotocol http://frame.cache"
  submitJob diskCacheJob $cmd
  waitForJob diskCacheJob

  if { $::diskCacheJob(status) == "done" } {
    set framecache [ LJread $::diskCacheJob(outputs) ]
    catch { unset ::DISKCACHE_IMAGE }
    foreach line [ split $framecache "\n" ] {
      array set ::DISKCACHE_IMAGE [ list [ lindex $line 0 ] [ lrange $line 1 end ] ]
    }
    LJdelete ::diskCacheJob
    return
  } elseif { $status == "error" } {
    set msg \
      "$::diskCacheJob(jobid) Error running getFrameCache user command: \"$::diskCacheJob(error)\""
    log $msg
    logfile $msg
    errorNotify $msg
    update idletasks
    LJdelete ::diskCacheJob
    return -code error
  } else {
    cleanupAndExit 1 "getDiskCache: invalid status \"$::diskCacheJob(status)\""
  }

} ;## getDiskCache

;## 11/11/04
;## normalize ::DISKCACHE to multiples of minJobLength
;##
proc normCacheDumpModMinJobLength {} {

  if {$::minJobLength == 1} {
     ;## 01/19/05 gam; Only change last interval, as needed
     set gpsNow [ tconvert now ]
     set diskCacheLength [llength $::DISKCACHE]
     if {$diskCacheLength > 0} {
        # 04/29/05 gam; find the last end time.
        set lastInterval [lindex $::DISKCACHE end]
        foreach { lastStart lastEnd } $lastInterval { continue }
        #puts "::DISKCACHE = $::DISKCACHE\n";
        #puts "lastEnd = $lastEnd";
     }
     set intervalCount 0
     set result {}
     foreach interval $::DISKCACHE {
       incr intervalCount
       foreach { start end } $interval {
         if {$intervalCount < $diskCacheLength} {
           # 04/29/05 gam; check how old compared to most recent segment; if { [expr $gpsNow - $end] > $::fillSegmentsIfThisOld }
           if { [expr $lastEnd - $end] > $::fillSegmentsIfThisOld } {
             # Segment is old enough and not the last segment; go ahead and fill with short frames if needed.
             lappend result [ list $start $end ]; # do not adjust this interval
           } else {
             # Else, reduce this segment to a multiple of the usual job length; LDAS will NOT need to output any short frames.
             # 04/29/05 gam; if ::alignFrameStartTimes is TRUE align frames start time
             if {$::alignFrameStartTimes} {
                set extraTime [ expr $start % $::usualMinJobLength ]
                if { $extraTime > 0 } {
                   set start [ expr $start - $extraTime + $::usualMinJobLength ] ;## move start to next aligned time
                }
             }
             if { [expr $end - $start ] >= $::usualMinJobLength } {
               set extraTime [ expr ($end - $start) % $::usualMinJobLength ]
               set end [ expr $end - $extraTime ] ;## trim off extra
               if { [expr $end - $start ] >= $::usualMinJobLength } {
                lappend result [ list $start $end ]
               }
             }                        
           }
         } else {
           if { $::allowShortForLatestSeg  &&  ( [expr $gpsNow - $end] > $::fillSegmentsIfThisOld ) } {
             # If allowing short frames in the latest segment and end time of the segment is older than specified
             # then go ahead and run on this segment regardless of its length; LDAS will output short frames as needed.
             lappend result [ list $start $end ]; # do not adjust this interval                      
           } else {
             # Else, reduce this segment to a multiple of the usual job length; LDAS will NOT need to output any short frames.
             # 04/29/05 gam; if ::alignFrameStartTimes is TRUE align frames start time
             if {$::alignFrameStartTimes} {
                set extraTime [ expr $start % $::usualMinJobLength ]
                if { $extraTime > 0 } {
                   set start [ expr $start - $extraTime + $::usualMinJobLength ] ;## move start to next aligned time
                }
             }
             if { [expr $end - $start ] >= $::usualMinJobLength } {
               set extraTime [ expr ($end - $start) % $::usualMinJobLength ]
               set end [ expr $end - $extraTime ] ;## trim off extra
               if { [expr $end - $start ] >= $::usualMinJobLength } {
                lappend result [ list $start $end ]
               }
             }
           }
         }
       }
     }     
  } else {
     set result {}
     foreach interval $::DISKCACHE {
       foreach { start end } $interval {
         # 04/29/05 gam; if ::alignFrameStartTimes is TRUE align frames start time
         if {$::alignFrameStartTimes} {
            set extraTime [ expr $start % $::minJobLength ]
            if { $extraTime > 0 } {
               set start [ expr $start - $extraTime + $::minJobLength ] ;## move start to next aligned time
            }
         }
         if { [expr $end - $start ] >= $::minJobLength } {
           set extraTime [ expr ($end - $start) % $::minJobLength ]
           set end [ expr $end - $extraTime ] ;## trim off extra
           if { [expr $end - $start ] >= $::minJobLength } {
              lappend result [ list $start $end ]
           }
         }
       }
     }
  }
  set ::DISKCACHE $result

} ;## normCacheDumpModMinJobLength

;##
;## Update various pieces of diskcache information
;##
proc updateDiskCache {} {
  # Run an LDAS job to get the contents of the entire disk cache
  getDiskCache

  # Construct the "normalized" disk cache of RAW data specifically
  # for the IFO(s) and type of interest
  normalizeCacheDump

  # Set the value of the global variable holding the latest time in
  # the diskcache
  foreach { - ::DISKCACHE_LATEST } [ lindex $::DISKCACHE end ] { continue }

  if { $::excludeRDS == "Y" || $::excludeRDS == "y" } {
    # Construct the "normalized" disk cache of REDUCED data specifically
    # for the IFO and type of interest
    normalizeRDSCacheDump

    # Now subtract the reduced data from the raw data
    set ::DISKCACHE [ setDifference $::DISKCACHE $::RDS_DISKCACHE ]
  }

  # Now subtract the data that has been reduced by the current instance
  # of createrds (in case that's not visible to LDAS yet)
  set ::DISKCACHE [ setDifference $::DISKCACHE $::COMPLETED_RDS ]

  # Now subtract data where errors occurred
  set ::DISKCACHE [ setDifference $::DISKCACHE $::ERROR_SET ]
  
  ;## 11/11/04; normalize ::DISKCACHE to multiples of minJobLength
  normCacheDumpModMinJobLength

} ;## updateDiskCache

;##
;## Return a list of all time intervals present in the diskcache in the
;## in range stime to etime ie. the intersection of the diskcache with the
;## interval [$stime, $etime)
;##
proc getSubinterval { stime etime } {
  set subinterval {}

  set interval [ list $stime $etime ]
  set interval [ list $interval ]
  set subinterval [ setIntersection $::DISKCACHE $interval ]

  return $subinterval
}

;##
;## Get the next available time interval, which may be shorter than
;## the maximum interval allowed.
;##
;## Returns a time interval in the list "$stime $etime", or the empty set if
;## there are no more times to process
;##
proc getNextAvailableTime {} {
  # Check to see if there's any more data to process
  if { [ llength $::DISKCACHE ] == 0 } {
    return {}
  }

  # Set the start-time to the first available time in the diskcache
  set interval [ lindex $::DISKCACHE 0 ]
  foreach { stime etime } $interval { continue }

  # 04/29/05 gam; if aligning frame, check if extra time at start of the interval
  set extraTime 0;  # default
  if {$::alignFrameStartTimes} {
     set extraTime [ expr $stime % $::usualMinJobLength ]
     # Note that extraTime will be > 0 only if $::allowshortframes is TRUE.
     # Otherwise we have already aligned segment start times with usualMinJobLength!
  }
  if { $extraTime > 0 } {
    # 04/29/05 gam; set etime so that a shortframe job run on the extra time at start of interval!
    set etime [ expr $stime - $extraTime + $::usualMinJobLength ]    
  } else {
    # 04/29/05 gam; else do as before:
    
    # Make sure the end time doesn't stray into the next block of
    # $::secPerBin seconds, so we end up writing data to the correct directory
    # (note, relies on integer arithmetic)
    #set maxEtime [ expr ($stime/100000 + 1)*100000 ]
    set maxEtime [ expr ($stime/$::secPerBin + 1)*$::secPerBin ]
    set etime [ expr $stime + $::maxJobLength ]
    set etime [ min $etime $maxEtime ]
  
    # 11/10/04; 11/11/04; make sure interval is multiple of the minimum job length:
    # Intersection with DISKCACHE below will give actual interval to work on.  
    # 02/09/05; make interval a multiple of the ::usualMinJobLength.
    set extraTime [ expr ($etime - $stime) % $::usualMinJobLength ]
    set etime [ expr $etime - $extraTime ] ;## trim off extra
    if { [expr $etime - $stime] < $::usualMinJobLength } {
       set etime [ expr $stime + $::usualMinJobLength ]
    }
  }

  # Now find the intersection of this interval with what's in the diskcache
  # The intersection of an interval with the diskcache yields a set, which
  # may contain more than one interval if there are gaps, so we just take the
  # first interval from the set
  set interval [ list $stime $etime ]
  set interval [ setIntervalIntersection $::DISKCACHE $interval ]
  set interval [ lindex $interval 0 ]

  return $interval
} ;## getNextAvailableTime

;## 04/20/05 gam
;## Build the framequery for this job
;## 
proc buildFrameQuery { times } {
  
  set frameQueryString "";
  set typeIndex 0; # also gives channels from channelLists
  foreach ifo $::ifoList {
    set typeThisIFO [lindex $::type $typeIndex];
    set channelsThisIFO [lindex $::channelLists $typeIndex];
    if {$typeIndex > 0} {
       append frameQueryString " ";
    }
    append frameQueryString "{ $typeThisIFO $ifo {} $times Chan($channelsThisIFO) }";
    incr typeIndex;    
  }
  # puts "frameQueryString = \n$frameQueryString";
  
  return $frameQueryString;
}

;##
;## Build the createRDS command. The only thing that changes is the time
;## span of the query
;##
proc buildCmd { times } {
  
  set subLength [expr 9 - [ string length $::secPerBin] ]
  
  if { $::binByTime == "Y" || $::binByTime == "y" } {
    foreach { stime etime } $times { continue }

    # Get the first four digits of stime
    #set subtime [ string range $stime 0 3 ]
    set subtime [ string range $stime 0 $subLength ]
    
    # Determine the output RDS type
    set rdsType [ RDSType $::type $::userType ]

    set dir "${::outputdir}/${::RDSFILE_PREFIX}-${rdsType}-${subtime}"
  } else {
    set dir ${::outputdir}
  }

 if {$::useFrameQuery} { 

  # 04/20/05 gam; use -framequery syntax
  set frameQueryString [buildFrameQuery $times]
  set cmd "
createRDS
  -framequery { $frameQueryString }
  -usertype { $::userType }
  -outputdir { $dir }
  -usejobdirs { $::usejobdirs }
  -compressiontype { $::compressiontype }
  -compressionlevel { $::compressionlevel }
  -filechecksum { $::filechecksum }
  -frametimecheck { $::frametimecheck }
  -framedatavalid { $::framedatavalid } 
  -framesperfile { $::framesperfile }
  -secperframe { $::secperframe }
  -allowshortframes { $::allowshortframes }
  -generatechecksum { $::generatechecksum }
  -fillmissingdatavalid { $::fillmissingdatavalid }"  

  # puts $cmd;

 } else {

  # 04/20/05 gam; else do as before
  set cmd "
createRDS
  -times $times
  -type { $::type }
  -usertype { $::userType }
  -outputdir { $dir }
  -usejobdirs { $::usejobdirs }
  -channels { $::channels }
  -compressiontype { $::compressiontype }
  -compressionlevel { $::compressionlevel }
  -filechecksum { $::filechecksum }
  -frametimecheck { $::frametimecheck }
  -framedatavalid { $::framedatavalid }
  -framesperfile { $::framesperfile }
  -secperframe { $::secperframe }
  -allowshortframes { $::allowshortframes }
  -generatechecksum { $::generatechecksum }
  -fillmissingdatavalid { $::fillmissingdatavalid }"

 }

 if {$::md5sumregexp > ""} {
    append cmd " -md5sumregexp { $::md5sumregexp }";
 }

  return $cmd
}

;##
;## Print a log message to the end of the log window
;##
proc log { text } {
}

;##
;## Clear the log window
;##
proc clearLog {} {
}

;##
;## Print the status of job $jobIndex to the log window
;##
proc printJobStatus { jobIndex } {
  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  set jobNum [ expr $jobIndex + 1 ]
  set now [ clock seconds ]

  log "Job $jobNum of $::maxJobs"
  if { [ info exists job ] } {
    log "  Job ID    : $job(jobid)" 
    log "  Job times : $jobAux(times) ($jobAux(duration) seconds)"
    log "  Status    : $job(status)"
    log "  Runtime   : [ expr $now - $job(startTimeS) ] seconds"
  } else {
    log "  Job ID    : --" 
    log "  Job times : --"
    log "  Status    : Not running"
    log "  Runtime   : --"
  }
  log ""

}

;##
;## Update the canvas widget representing the data that has been reduced
;## and the data left to go
;##
proc updateProgressBar {} {
}

;##
;## Update the log window
;##
proc updateDisplay {} {
  
  #05/26/05 not used in nonGUI version:
  # set timeNowS [ clock seconds ]
  # set ::timeNow [ clock format $timeNowS -format %c ]
  # set elapsed [ expr $timeNowS - $::timeStartedS ]
  # set elapsedMsg "[expr $elapsed/3600]h [expr ($elapsed % 3600)/60]m [expr ($elapsed % 3600) % 60]s"
 
  # Work out the time actually spent running, with a fudge factor to
  # avoid division by zero
  # 02/10/05; update overall realtimeRatio only after a multiple of maxJobs has finished!
  set remainderForSetOfJobs [expr $::jobCtr % $::maxJobs]
  if { $remainderForSetOfJobs == 0 && $::jobCtr >= $::maxJobs } {
     set runningTime [ expr $::runningCtr*($::UPDATE_INTERVAL/1000) + 0.1 ]
     set ::overallRealtimeRatio \
      [ expr round(100.0*$::totalTimeReduced/$runningTime)/100.0 ]
  }

  # Determine the ratio of data reduced/real-time elapsed. It will be
  # considered a non-fatal error if this ratio falls below 1.0 while in
  # the running state
  set ::realtimeRatioMsg "Seconds Of Data Reduced/Elapsed Wall Time: $::overallRealtimeRatio"
  # if { $::realtimeRatio < 1.0 && $::totalTimeReduced > 0 && $::STATE == "running" } # send error only if at one set of maxJobs has run.
  if { $::overallRealtimeRatio < 1.0 && $::totalTimeReduced > 0 && $::STATE == "running" && $::jobCtr > $::maxJobs } {
    if { ! $::realtimeRatioAlarm } {
      set msg "$::RDSFILE_PREFIX $::type data reduction rate has fallen to $::overallRealtimeRatio times real-time"
      logfile $msg red
      errorNotify $msg
      set ::realtimeRatioAlarm $::TRUE
    }
  } else {
    # This variable prevents multiple email alerts when the realtime ratio
    # is below 1.0 for a long time
    set ::realtimeRatioAlarm $::FALSE
  }
  
  #05/26/05 not used in nonGUI version:
  #if { [ llength $::DISKCACHE ] > 0 } {
  #  set interval [ lindex $::DISKCACHE 0 ]
  #  foreach { nextJobStart dummy } $interval { continue }
  #  set nextJobStart "$nextJobStart ([ tconvert $nextJobStart ])"
  #} else {
  #  set nextJobStart "--"
  #}

  #05/26/05 not used in nonGUI version, except ::percentComplete, but this is already updated in reapSuccessfulJob
  #set allRDS [ setUnion $::RDS_DISKCACHE $::COMPLETED_RDS ]
  #set tDone [ setSize $allRDS ]
  #set ::percentComplete [ expr round(10000.0*$tDone/$::totalTime)/100.0 ]
  #set grandTotalTimeReduced [ setSize $::COMPLETED_RDS ]

}

;##
;## This procedure is called when a job has successfully completed
;##
proc reapSuccessfulJob { jobIndex } {

  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  if { $job(status) == "done" } {
    foreach { stime etime } [ split $jobAux(times) - ] { continue }
    set jobid $job(jobid)
    set ::STATUS "ok"
    set ::LAST_COMPLETED $jobid
    set ::LAST_COMPLETED_TIME [ tconvert now ]
    set ::LAST_COMPLETED_STIME $stime
    set ::LAST_COMPLETED_ETIME $etime
    set jobReply $job(jobReply)
    regsub -all -- {[\n\s\t]+} $jobReply { } jobReply
    # 02/10/05; add next two lines to get time to run this job
    set now [ clock seconds ]
    set jobTime [ expr $now - $job(startTimeS) + 0.1 ]
    LJdelete ::job$jobIndex
    decr ::currentExistingJobs 
    set interval [ list $stime [ expr $etime + 1 ] ]
    set ::COMPLETED_RDS [ setIntervalUnion $::COMPLETED_RDS $interval ]
    set ::IN_PROGRESS_SET [ setIntervalDifference $::IN_PROGRESS_SET $interval ]
    
    set ::totalTimeReduced [ expr $::totalTimeReduced + $jobAux(duration) ]

    # 02/09/05; update realtimeRatio when totalTimeReduced changes.
    #set runningTime [ expr $::runningCtr*($::UPDATE_INTERVAL/1000) + 0.1 ]
    #set ::realtimeRatio \
    #  [ expr round(100.0*$::totalTimeReduced/$runningTime)/100.0 ]
    # 02/10/05; compute real-time ratio for this one job only:    
    set ::realtimeRatio \
      [ expr round(100.0*$jobAux(duration)/$jobTime)/100.0 ]

    # 02/09/05; update percentComplete when COMPLETED_RDS changes above.
    set allRDS [ setUnion $::RDS_DISKCACHE $::COMPLETED_RDS ]
    set tDone [ setSize $allRDS ]
    set ::percentComplete [ expr round(10000.0*$tDone/$::totalTime)/100.0 ]

    # Take it out of the error set if necessary
    set ::ERROR_SET [ setIntervalDifference $::ERROR_SET $interval ]
    set jobAux(retry) $::FALSE
    incr ::jobCtr
    logfile "$jobid $::RDSFILE_PREFIX $::type $jobAux(times) ($jobAux(duration) seconds) SUCCEEDED. Rate $::realtimeRatio times real-time, $::percentComplete\% done"
  } else {
    cleanupAndExit 1 "reapSuccessfulJob: got job with status $job(status)"
  }

}

;##
;## When it turns out that file already exists for some time interval,
;## extract the file start-time and dt and add that interval to the
;## completed RDS
;## 
;##
proc handleExistingFile { msg jobInterval } {
  # Extract the filename from the message
  if { ! [ regexp {/.*\.gwf} $msg f ] } {
    logfile "File does not match *.gwf extension" red
    return
  }

  set f [ file tail $f ]
  set f [ file rootname $f ]

  # Work out the interval of time which this file covers
  #foreach { prefix type stime duration } [ split $f - ] { continue } # 04/20/05 gam; should make sure type is local var
  foreach { prefix qtype stime duration } [ split $f - ] { continue }
  set etime [ expr $stime + $duration ]
  set interval [ list $stime $etime ]
  
  # Add this file to the "completed" list
  set ::COMPLETED_RDS [ setIntervalUnion $::COMPLETED_RDS $interval ]
  
  # Add the remainder of the time interval back into the diskcache pool,
  # assuming the remainder has non-zero size
  foreach { - jobEtime } $jobInterval { continue }
  if { $etime < $jobEtime } {
    set interval [ list $etime $jobEtime ]
    set ::DISKCACHE [ setIntervalUnion $::DISKCACHE $interval ]
  }
}

;##
;## Handle failed jobs
;## 
proc reapFailedJob { jobIndex } {

  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  if { $job(status) == "error" } {
    set times $jobAux(times)
    foreach { stime etime } [ split $jobAux(times) - ] { continue }
    set interval [ list $stime [ expr $etime + 1 ] ]
    set ::STATUS "err"
    set ::LAST_COMPLETED $job(jobid)
    set ::LAST_COMPLETED_TIME [ tconvert now ]
    set ::LAST_COMPLETED_STIME $stime
    set ::LAST_COMPLETED_ETIME $etime

    set errmsg "$job(error)"
    regsub -all -- {[\n\s\t]+} $errmsg { } errmsg

    switch -glob $errmsg {
      "*limited queue size*" -
      "*aborted by LDAS*" -
      "*connection refused*" -
      "*please retry job*" -
      "*System Shutting Down*" {
        # If the job fails because LDAS shuts down, or because the queue is
        # full, we want to retry an infinite number of times. The simplest
        # way to handle it is to add the time interval back into the list of
        # available times and wait until LDAS starts up again.
        set ::DISKCACHE [ setIntervalUnion $::DISKCACHE $interval ]
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
      }
      "*Frame file already exists*" {
        # This often happens when a previous job fails partway through,
        # leaving some frame files behind. Special handling is needed
        # to account for the files that already exist.
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
        handleExistingFile $errmsg $interval
      }
      "*no data found for time*" {
        # If we get this error it usually means that our view of the
        # diskcache disagrees with LDAS, and we should update it.
        #
        # The easiest solution appears to be to set the DISKCACHE variable
        # back to the empty set, so that it will be updated on the next
        # pass through runningJob. This also means that having more than
        # one job fail this way will lead to inconsistencies (although it
        # may mean that the cache is updated a couple of times).
        #
        # Note we don't attempt to retry - this will happen automatically
        # when the cache is refreshed if this time interval is present.
        # We also don't add this interval to the error set.
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
        set ::DISKCACHE {}
      }
      "*socket closure*" -
      "*aborted after*seconds*" -
      "*aborting*" -
      "*job has been aborted*" -
      "*Timeout waiting*" -
      "*malformed command*" -
      "*no frames*" -
      "*API shutting down*" {
        # These should be retried only a finite number of times. Could
        # possibly be caused by bad data or random coredumps of the API
        set jobAux(retry) $::TRUE
      }
      "*Missing FrEndOfFile*" -
      "*At least three frame files must be specified*" - 
      default {
        # Jobs with these errors are unrecoverable, don't retry.
        # Default is also not to retry.
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
        set ::ERROR_SET [ setIntervalUnion $::ERROR_SET $interval ]
      }
    }

    set msg "$job(jobid) $::RDSFILE_PREFIX $::type $times ($jobAux(duration) seconds) FAIL($jobAux(tries)): $errmsg"
    logfile $msg red
    errorNotify $msg
    set ::IN_PROGRESS_SET [ setIntervalDifference $::IN_PROGRESS_SET $interval ]
    LJdelete ::job$jobIndex
    decr ::currentExistingJobs
    incr ::failedJobCtr
  } else {
    cleanupAndExit 1 "reapFailedJob: got job with status $job(status)"
  }

}

;##
;## Retry jobs that failed on the last main loop iteration
;## 
proc retryFailedJob { jobIndex } {

  upvar ::job${jobIndex}Aux jobAux

  set times $jobAux(times)
  foreach { stime etime } [ split $jobAux(times) - ] { continue }
  set interval [ list $stime [ expr $etime + 1 ] ]

  incr jobAux(tries)
  if { $jobAux(tries) <= $::maxRetries } {
    set cmd [ buildCmd $times ]
    submitJob job${jobIndex} $cmd
    set ::LAST_SUBMITTED_STIME $stime
    set ::LAST_SUBMITTED_ETIME $etime
    set ::IN_PROGRESS_SET [ setIntervalUnion $::IN_PROGRESS_SET $interval ]
    incr ::currentExistingJobs
  } else {
    logfile "$::RDSFILE_PREFIX $::type $times ($jobAux(duration) seconds) ABANDONED after $::maxRetries tries" red
    set ::job${jobIndex}Aux(retry) $::FALSE
    set ::ERROR_SET [ setIntervalUnion $::ERROR_SET $interval ]
  }

}

;##
;## Check the status of a job and start the next one if needed
;##
proc checkJobStatus { jobIndex } {

  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  switch -exact -- $job(status) {
    submitted {}
    running {}
    done { reapSuccessfulJob $jobIndex }
    error { reapFailedJob $jobIndex }
    default {
      cleanupAndExit 1 "checkJobStatus: Unknown status $job(status)"
    }
  }

}

;##
;## Reap all jobs which have finished (ie. not in the running state)
;##
proc reapJobs {} {

  for { set k 0 } { $k < $::maxJobs } { incr k } {
    if { [ info exists ::job$k ] } {
      LJstatus ::job$k
      checkJobStatus $k
    }
  }

}

;##
;## Start a new job in slot "jobIndex" using the time range provided
;##
proc launchJob { jobIndex timesList } {

  foreach { stime etime } $timesList { continue }
  set duration [ expr $etime - $stime ]
  set times $stime-[expr $etime - 1]
  set cmd [ buildCmd $times ]

  #
  # The jobNAux variable holds auxiliary information about the jobs
  # It's fields are
  #   times - the "times" parameter of the job
  #   duration - the duration of the times, in seconds
  #   tries - how many times we have tried to run this job
  #
  set ::job${jobIndex}Aux(times) "$times"
  set ::job${jobIndex}Aux(duration) $duration
  set ::job${jobIndex}Aux(retry) false
  set ::job${jobIndex}Aux(tries) 1

  submitJob job${jobIndex} $cmd
  incr ::currentExistingJobs

  set ::LAST_SUBMITTED_STIME $stime
  set ::LAST_SUBMITTED_ETIME $etime

  # Add this interval to the intervals in progress
  set ::IN_PROGRESS_SET [ setIntervalUnion $::IN_PROGRESS_SET $timesList ]

  # Remove the time interval for this job from availability
  set ::DISKCACHE [ setIntervalDifference $::DISKCACHE $timesList ]
}

;##
;## If a job slot is empty, start a new job or retry the job if it failed
;## in the last iteration
;##
proc startNewJobs {} {

  for { set k 0 } { $k < $::maxJobs } { incr k } {
    if { ! [ info exists ::job$k ] } {
      if { [ info exists ::job${k}Aux ] } {
        set retry [ set ::job${k}Aux(retry) ]
      } else {
        set retry $::FALSE
      }

      if { $retry } {
        retryFailedJob $k
      } else {
        set timesList [ getNextAvailableTime ]
        if { [ llength $timesList ] == 2 } {
          launchJob $k $timesList
        }
      }
    }
  }

}

;##
;## Poll the currently running jobs and update the display
;##
;## This is the main loop that runs in the event loop while we're in the
;## "running" state. It's state is changed by user activity (pressing the
;## Stop or Exit button), errors connecting to LDAS, or running out of data
;## to process.
;##
proc runningLoop {} {

  # First thing - write out the status file
  writeStatusfile

  # For off-site control
  if { [ file exists "sHuTdOwN" ] } {
    Exit
  }

  if { $::STATE == "running" } {

    # incr ::runningCtr # 02/09/05 gam; move after checking if DISKCACHE is empty to prevent initial rate seeming very low.

    if { [ llength $::DISKCACHE ] == 0 } {
      set ::STATE waiting
      set ::TIME_TO_WAIT 0
      after 0 runningLoop
      return
    }

    incr ::runningCtr

    set ::SBTEXT "Reaping jobs..."
    update idletasks
    reapJobs

    set ::SBTEXT "Starting new jobs..."
    update idletasks
    if { [ catch { startNewJobs } errmsg ] } {
      set msg "Error starting new jobs: \"$errmsg\""
      log $msg
      logfile $msg red
      errorNotify $msg
      set ::STATE error
      set ::STATUS "err"
      set ::TIME_TO_WAIT $::ERR_INTERVAL
      after 0 runningLoop
      return
    }

    set ::SBTEXT "Running ($::percentComplete\% done)"
    updateDisplay
    updateProgressBar
    update idletasks

    after $::UPDATE_INTERVAL runningLoop
    return

  } elseif { $::STATE == "waiting" } {

    set ::SBTEXT "Reaping old jobs..."
    update idletasks
    reapJobs
    #updateDisplay # 05/26/05; no need to update while waiting
    updateProgressBar

    if { $::currentExistingJobs == 0 && $::TIME_TO_WAIT <= 0 } {
      set ::SBTEXT "Refreshing disk cache..."
      update idletasks
      logfile "Refreshing disk cache"
      if { [ catch { updateDiskCache } errmsg ] } {
        set msg "Error updating cache: \"$errmsg\""
        log $msg
        logfile $msg red
        errorNotify $msg
        set ::STATE error
        set ::STATUS "err"
        set ::TIME_TO_WAIT $::ERR_INTERVAL
        after 0 runningLoop
        return
      }


      if { [ llength $::DISKCACHE ] == 0 } {
        #logfile "No new data in disk cache, waiting [expr $::diskCacheInterval/1000] seconds"
        logfile "No new data in disk cache, waiting $::diskCacheInterval seconds"
        #set ::TIME_TO_WAIT $::diskCacheInterval
        set ::TIME_TO_WAIT [ expr $::diskCacheInterval*1000 ]
        after 0 runningLoop
        return
      } else {
        set ::SBTEXT "Refreshing disk cache...done"
        update idletasks
        #logfile "Diskcache updated: $::DISKCACHE"
        #logfile "Reduced data     : $::RDS_DISKCACHE"
        logfile "The disk cache has been updated"
        set ::STATE running
        after 0 runningLoop
        return
      }

    } else {
      # Continue waiting until all the pending jobs have finished
      # 02/10/05; include time waiting for jobs to finish in runningCtr used to compute overall realtimeRatio.
      if { $::currentExistingJobs > 0 } {
         incr ::runningCtr      
      }
      set ::SBTEXT "No more data, waiting [expr $::TIME_TO_WAIT/1000] seconds"
      set ::TIME_TO_WAIT [ expr $::TIME_TO_WAIT - $::UPDATE_INTERVAL ]
      after $::UPDATE_INTERVAL runningLoop
      return
    }

  } elseif { $::STATE == "error" } {

    set ::SBTEXT "Error submitting job, waiting [expr $::TIME_TO_WAIT/1000] seconds"
    update idletasks
    reapJobs
    updateDisplay
    updateProgressBar
    if { $::TIME_TO_WAIT <= 0 } {
      set ::STATE running
      after 0 runningLoop
      return
    } else {
      set ::TIME_TO_WAIT [ expr $::TIME_TO_WAIT - $::UPDATE_INTERVAL ]
      after $::UPDATE_INTERVAL runningLoop
      return
    }

  } elseif { $::STATE == "exiting" } {

    reapJobs
    set ::SBTEXT "Exiting... ($::currentExistingJobs jobs remaining)"
    update idletasks
    updateDisplay

    if { $::currentExistingJobs > 0 } {
      after $::UPDATE_INTERVAL runningLoop
      return
    } else {
      cleanupAndExit 0 "Exiting"
    }

  } else {
    cleanupAndExit 1 "runningLoop: Unknown state $::STATE"
  }

}

;##
;## Print the current value of all settings to the log window
;##
proc printSettings {} {
  
  logfile "createRDS Control Utility settings"
  logfile "Run as user                : $::user"
  logfile "Email notification list    : $::notifyList"
  logfile "Site to process at         : $::site"
  logfile "Input frame type(s)        : $::type"
  logfile "Input IFO for each type    : $::ifoList"
  logfile "Use framequery syntax?     : $::useFrameQuery"
  logfile "Output frame IFO(s)        : $::RDSFILE_PREFIX"
  logfile "Output frame type          : [RDSType $::type $::userType]"
  logfile "Leave output type as is    : $::leaveUserTypeAsIs"
  logfile "Exclude existing RDS data? : $::excludeRDS"
  logfile "Frame file duration (secs) : $::frameLength"
  logfile "Get duration dynamically?  : $::updateFrameLength"
  logfile "sec. between cache updates : $::diskCacheInterval"
  logfile "Channel list file          : $::adcFile"
  logfile "No. of raw channels out    : $::numAdcChannels"
  logfile "No. of proc channels out   : $::numProcChannels"
  logfile "Total no. of channels out  : [ expr $::numAdcChannels + $::numProcChannels ]"
  logfile "Start time                 : $::startTime ([ tconvert $::startTime ])"
  logfile "End time                   : $::endTime ([ tconvert $::endTime ])"
  logfile "Get data from here only    : $::includedirs"
  logfile "Exclude data from here     : $::excludedirs"
  logfile "Use job directories?       : $::usejobdirs"
  logfile "Base output directory      : $::outputdir"
  logfile "Regex to give md5sum dir   : $::md5sumregexp"
  #logfile "Use 100000-second subdirs? : $::binByTime"
  logfile "Use binByTime subdirs?     : $::binByTime"
  logfile "No. sec. per subdirs?      : $::secPerBin"  
  logfile "Compression type           : $::compressiontype"
  logfile "Compression level          : $::compressionlevel"
  logfile "Validate file checksums?   : $::filechecksum"
  logfile "Validate frame times?      : $::frametimecheck"
  logfile "Check datavalid flags?     : $::framedatavalid"
  logfile "Maximum job length         : $::maxJobLength seconds"
  logfile "Maximum simultaneous jobs  : $::maxJobs"
  logfile "No. frames per output file : $::framesperfile"
  logfile "No. sec. per output frame  : $::secperframe"
  logfile "Align normal output frames?: $::alignFrameStartTimes"
  logfile "Allow short frames?        : $::allowshortframes"
  logfile "Allow short for latest seg?: $::allowShortForLatestSeg"
  logfile "Fill seg only if this old  : $::fillSegmentsIfThisOld"
  logfile "Generate frame checksums?  : $::generatechecksum"
  logfile "Fill datavalid aux vec's?  : $::fillmissingdatavalid"
}


;##
;## Change the state variable to "exiting" so that the event-driven
;## loop will wait for all pending jobs to complete, then exit
;##
proc Exit {} {
  if { $::STATE == "running" } {
    logfile "Exit requested"
    
    set ::STATE exiting
    set ::SBTEXT "Exiting... ($::currentExistingJobs jobs remaining)"
    
    updateDisplay
  } else {
    cleanupAndExit 0 "Exiting"
  }
}

;############################################################################
;##
;## Main
;##
;############################################################################

package require LDASJob
package require tconvert
source ${::BINDIR}/sets.tcl

;## Setup globals and default values
initializeGlobals
setCreateRDSDefaults

;## Read resource file, which may override defaults
if { [ file exists ./$::RESOURCE_FILE ] } {
  if { [ catch { source ./$::RESOURCE_FILE } resultSourceRscFile ] } {
      puts stderr " File $::RESOURCE_FILE is not valid; sourcing this file resulted in the error:\n $resultSourceRscFile \n Please fix this file and try again."
      file delete "createrds.status"
      exit 1
  }
  if {[vetCreateRDSParameters]} {
     exit 1; # vet parameters set in the resource file.
  }
}

;## Look for .ldaspw file
if { ! [ file exists ~/.ldaspw ] } {
  puts stderr "No password file found (~/.ldaspw) - please run ldaspw"
  exit 1
}

;## Look for command line arguments
if { [ llength $argv ] > 1 } {
  #cleanupAndExit 1 "Invalid number of arguments.\nUsage: createrds.tcl \[ --start \]"; # Only used with GUI
  cleanupAndExit 1 "Invalid number of arguments.\nUsage: createrds.tcl"
} else {
  foreach arg $argv {
    if { $arg == "--start" } {
      # set ::AUTO_START $::TRUE; # Only used with GUI
    } else {
      # cleanupAndExit 1 "$arg: invalid argument.\nUsage: createrds.tcl \[ --start \]"; # Only used with GUI
      cleanupAndExit 1 "$arg: invalid argument.\nUsage: createrds.tcl"
    }
  }
}

;##
;## See if a lock file exists, and create it if it doesn't
;##
if { [ catch {
         set locklist [ glob createrds.*.lock ]
         puts stderr "Lockfile $locklist exists! If no instance of createrds is running with this PID, please delete the lock file and try again"
         exit 1
} ] } {
  set PID [ pid ]
  set ::LOCKFILE createrds.${PID}.lock
  set lockfid [ open $::LOCKFILE w ]
  close $lockfid
  unset lockfid
  exec chmod ug+w $::LOCKFILE   ;## Needed so that the CGI scripts can delete
}

resetGlobalVariables
createChannelList

writeStatusfile

;## enter the event loop...
logfile "createrds.tcl starting"
printSettings

set ::STATE running
after 0 runningLoop
vwait forever
