#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.

source /ldas_usr/ldas/test/bin/AllTest.rsc
;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

;## specify location of tclglobus and globus if not a default
set ::env(TCLGLOBUS_DIR) /ldcg/lib

if	{ [ info exist ::env(LD_LIBRARY_PATH) ] } {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH)" 
} else {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib" 
}

proc cleanup_output {} {		
;## purge any log over 3 hours
	set curtime [ clock seconds ]
	foreach type { log dat } {
		foreach file [ glob -nocomplain *.${type}.* ] {
        	set mtime [ file mtime $file ]
        	if      { [ expr $curtime - $mtime ] > 10800 } {
                	puts "deleting $file"
                	file delete -force $file
        	}
		}
	}
}
proc clean_putStandAlone {} {
	
	;## remove old putstandalone data
	set curtime [ clock seconds ]
	;## purge any log over 1 hours
	set patlist [ list process_1m process_2m ] 
	foreach dir $::PUTSTANDALONE_DSOS {
		foreach pat $patlist {
			catch { eval file delete [ glob -nocomplain $::PUTSTANDALONE_DESTDIR/${dir}/${pat}*.ilwd ] } 
		}
	}
}
	
killer pipeloop\.tcl
cleanup_output
clean_putStandAlone

;## no burstwrapper
set datastandalone_types { burstwrapper-data.rds inspiral-data.rds stochastic-data.rds slope-data.rds stackslide-data.cmd \
tfclusters-data.rds knownpulsardemod-data.rds waveburst-data.rds }

;## no stackslide
# set datastandalone_types { burstwrapper-data.rds inspiral-data.rds stochastic-data.rds slope-data.rds \
# tfclusters-data.rds knownpulsardemod-data.rds waveburst-data.rds }

set putstandalone_types { burstwrapper-put.cmd inspiral-put.cmd stochastic-put.cmd slope-put.cmd \
stackslide-put.cmd tfclusters-put.cmd knownpulsardemod-put.cmd waveburst-put.cmd }

;## only burstwrapper
#set pipeline_types { burstwrapper.rds }
set getmeta_types  { dcmean.cmd getmeta.cmd }

set getmeta_pat [ join $getmeta_types | ]
regsub -all {\.cmd} $getmeta_pat {} getmeta_pat
puts $getmeta_pat

set pipeline_pat [ join $datastandalone_types | ]
regsub -all {\.rds|\.cmd} $pipeline_pat {} pipeline_pat
regsub -all -- {\-data} $pipeline_pat {} pipeline_pat
puts $pipeline_pat

foreach cmd [ concat $datastandalone_types $putstandalone_types ] {
	regexp {([^\-]+)\-} $cmd -> user
	set user ${user}test
	catch { exec $::BINDIR/pipeloop.tcl -u $user -p -W 10 -s $::SITE -db ldas_tst \
		-g $::USE_GLOBUS_CHANNEL -gsi $::USE_GSI $::BINDIR/$cmd & } 
}

catch { exec $::BINDIR/pipeloop.tcl -s $::SITE -p -db ldas_tst -g \
$::USE_GLOBUS_CHANNEL -gsi $::USE_GSI $::BINDIR/getmeta.cmd & } 
catch { exec $::BINDIR/pipeloop.tcl -s $::SITE -p -db ldas_tst -M -g \
$::USE_GLOBUS_CHANNEL -gsi $::USE_GSI $::BINDIR/dcmean.cmd & } 

;## extra slope to replace burstwrapper 
;## catch { exec ./pipeloop.tcl -s $site -p -db ldas_tst slope.rds -o slope1.log -d slope1.dat & } 
;## extra tfcluster to replace waveburst
;## catch { exec ./pipeloop.tcl -s $site -p -W 10 -db ldas_tst tfclusters.rds -o tfclusters1.log -d tfclusters1.dat & } 

after 15000
set meta_data [ list ]


set cmd "exec grep SOCK [ glob *.log ]"
catch { eval $cmd } data
set curtime [ clock format [ clock seconds ] -format "%m/%d/%y" ]
set pat "(\[^\.\]+)\.log:$curtime"
set text ""
set sockcnt 0

foreach line [ split $data \n ] {
	if	{ [ regexp $pat $line -> type ] } {
		set index [ string last " " $line ]
		if	{ $index > -1 } {
			incr index 1
			set sock [ string range $line $index end ]
			if	{ [ regexp $getmeta_pat $type ] } {
				lappend meta_data $sock
				
			} elseif { [ regexp $pipeline_pat $type ] } {
				lappend pipelines $sock
			}
			append text "# $line\n"
			incr sockcnt 1
		} else {
			puts "no SOCK Id from $type.log"
		}
	}
}
after 100
puts "Found $sockcnt sockets"
set fd [ open joblist.tcl w ]
puts $fd "set meta_data [ list $meta_data ]"
puts $fd "set pipelines [ list $pipelines ]"
puts $fd "\n[ string trim $text \n ]"
close $fd
# catch { exec cat joblist.tcl  } data
# puts "\n**** joblist.tcl ****\n\n$data" 

;## wait 120 for tandems
set wait 120
;## 75 gives a job rate of 2445 on dev
;## unable to maintain this rate with the
;## current diskcache mount points
;## wait 90 for tcl socket gives 2600 jobs/hr

set wait $::LOOP_DELAY

catch { exec $::BINDIR/wait.tcl $wait } data
puts $data		

after 10000
catch { exec $::BINDIR/cont.tcl } data

puts $data
