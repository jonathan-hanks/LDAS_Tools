#!/bin/sh
#
# createrdsGUI
#
# A wish application for running and managing createRDS user commands
# on a remote LDAS system
#
# Don't change these lines - they are executed only by bash, not Tcl \
if [ -z "${LIGOTOOLS}" ]; then \
  echo "Shell variable LIGOTOOLS undefined - please \"eval \`<ligotools path>/bin/use_ligotools\`\" and try again"; \
  exit 1; \
fi; \
exec wishexe "$0" "$@"

# RECENT REVISIONS:
# 04/20/05 gam; all type to be a list; make ifoList global; add channelLists; allow option framequery syntax
# 04/29/05 gam; in normCacheDumpModMinJobLength use last end time instead of gpsNow to decide if gaps should be filled.
# 04/29/05 gam; if alignFrameStartTimes then align jobs with usualMinJobLength expect when producing short frames.
# 08/08/06 gam; Add md5sumregexp option for use with -md5sumregexp; gives sed like regular expression
#               to make replacements in outputdir to give location for md5sum files.
# 08/08/06 gam; Make usejobdirs an option; default is 0 (FALSE).

;##
;## Send a message to the list of users on notifyList
;##
proc sendMail { subject msg } {
  if { [ exec uname ] == "SunOS" } {
    set mailer /usr/ucb/mail
  } else {
    set mailer mail
  }

  exec $mailer -s $subject $::notifyList << $msg
}

;##
;## Send an error message to users on the email notification list
;##
proc errorNotify { msg } {
  set host [ exec hostname ]
  set domain [ exec domainname ]
  set full "${host}.${domain}"
  set subject "Error from createrdsGUI at $full"
  set msg "createrdsGUI running at: ${full}\nLDAS Site: ${::site}\nLDAS User: ${::user}\nError message:\n${msg}"
  sendMail $subject $msg
}

;##
;## Move the log files into the archive
;##
proc archiveLogfiles {} {
  set now [ tconvert now ]
  
  if { [ file exists "logs/createrds.log" ] } {
    file rename "logs/createrds.log" "logs/archive/createrds.${now}.log"
  }
  
  if { [ file exists "logs/DSOcreaterds.log.html" ] } {
    file rename "logs/DSOcreaterds.log.html" \
      "logs/archive/DSOcreaterds.${now}.log.html"
  }
}

;##
;## Add a message to the HTML log with the given colored ball
;##
proc addHtmlLogEntry { line { color "green" } }  {
  if { $color == "red" } {
     set gif "ball_red.gif"
     set state "ERROR"
  } elseif { $color == "yellow" } {
     set gif "ball_yellow.gif"
     set state "WARN"
  } elseif { $color == "blue" } {
     set gif "ball_blue.gif"
     set state "MESG"
  } else {
     set gif "ball_green.gif"
     set state "OK"
  }

  set gpsNow [ tconvert now ]
  set timeNow [ clock format [ clock seconds ] -format %c ]

  set outString "<img src=\"$gif\" width=\"14\" height=\"12\" alt=\"$timeNow\" title=\"$timeNow\"> <b>$gpsNow</b> <b><i><font color=\"$color\">$state</font></i></b> <b><i><font color=\"black\">createRDS</font></i></b> <tt>$line</tt><br>"

  puts $::html_oid $outString
  flush $::html_oid
}

;##
;## Write to the logfile
;##
proc logfile { line { color "green" } } {
  set clockSeconds [ clock seconds ]
  set locTime [ clock format $clockSeconds -format %c ]
  puts $::oid "$locTime $line"
  flush $::oid

  addHtmlLogEntry $line $color

  # Archive logs once they reach a certain size
  incr ::LOGLINES
  if { $::LOGLINES >= $::MAX_LOGLINES } {
    close $::oid
    close $::html_oid

    archiveLogfiles

    set ::oid [ open "logs/createrds.log" w ]
    set ::html_oid [ open "logs/DSOcreaterds.log.html" w ]

    set ::LOGLINES 0
  }
}

;##
;## Overwrite the status file. This information is used by dsorun
;## for displaying status information on the web
;##
proc writeStatusfile {} {
  # NOTE - dsorun doesn't like status file names with any capital letters
  set fid [ open "createrds.status" w ]

  puts $fid "$::STATUS $::LAST_SUBMITTED $::LAST_SUBMITTED_TIME \
$::LAST_SUBMITTED_STIME $::LAST_SUBMITTED_ETIME $::LAST_COMPLETED \
$::LAST_COMPLETED_TIME $::LAST_COMPLETED_STIME $::LAST_COMPLETED_ETIME 0"

  close $fid
}

;##
;## Exit with a code and a message. If the code is non-zero, the message
;## is also printed to stderr
;##
proc cleanupAndExit { code msg } {
  if { $code == 0 } {
    set color "green"
  } else {
    set color "red"
    puts stderr $msg
  }

  logfile "$msg" "$color"
  logfile "Total data processed this run: $::totalTimeReduced seconds"
  logfile "Grand total data processed: [setSize $::COMPLETED_RDS] seconds"
  close $::oid

  set recfid [ open "createrds.rec" w ]
  puts $recfid ";## Record of previous data reduction"
  puts $recfid ";##   LDAS System: $::site"
  puts $recfid ";##   Input IFOs: $::ifoList"; # 04/20/05 gam
  puts $recfid ";##   Input frame types: $::type"; # 04/20/05 gam
  puts $recfid ";##   Output frame type: [RDSType $::type $::userType]"
  if { [ llength $::COMPLETED_RDS ] == 0 } {
    puts $recfid "set ::COMPLETED_RDS {}"
  } else {
    puts $recfid "set ::COMPLETED_RDS \{ $::COMPLETED_RDS \}"
  }
  close $recfid

  file delete "sHuTdOwN"
  file delete "createrds.status"
  file delete $::LOCKFILE

  exit $code
}

;##
;## Return the RDS type based on the input frame type and the user type
;##
proc RDSType { type userType } {
  # if { [ regexp {[0-9]+$} $userType currentLevel ] } # 04/20/05 gam
  if { [ regexp {[0-9]+$} $userType currentLevel ] || [llength $::type] > 1 || $::leaveUserTypeAsIs } {
    # e.g., userType ends in 0-9 or type is a list, or option set to leave as is.
    return ${userType}
  } else {
    # Match any trailing digits in the input frame type
    if { [ regexp {[0-9]+$} $type lastLevel ] } {
      set level [ expr $lastLevel + 1 ]
    } else {
      set level 1
    }
    return ${userType}${level}
  }
  
}

;##
;## Build the list of channels and their respective downsampling factors
;## from a list of strings of the form "<channel name> <downsampling factor>"
;##
proc buildChannels { adcList } {
  set ::numAdcChannels 0
  set ::numProcChannels 0
  set channels {}
  set ifoListTmp {}; # 04/20/05 gam; change ifoList to ifoListTmp; if ::ifoList == "" set ::ifoList to $ifoListTmp below.

  foreach line $adcList {
    set line [ string trim $line ]

    # Skip blank and comment lines
    if { ![string length $line] || [regexp {^\#} $line] } {
      continue
    }

    if { [ llength $line ] != 2 } {
      cleanupAndExit 1 "Improperly formatted line in $::adcFile"
    }
    
    foreach { adc dec } $line { continue }

    set ifo [ string index $adc 0 ]
    if { [ lsearch -exact $ifoListTmp $ifo ] == -1 } {
      lappend ifoListTmp $ifo
    }

    if { $dec == 1 } {
      lappend channels $adc
      incr ::numAdcChannels
    } else {
      lappend channels "${adc}!$dec"
      incr ::numProcChannels
    }
  }

  if { $::numProcChannels > 0 } {
    set ::DOWNSAMPLING $::TRUE
  } else {
    set ::DOWNSAMPLING $::FALSE
  }

  # 04/20/05 gam; before sorting, save global ::ifoList if not set up in .rsc file.
  if { [llength $::ifoList] == 0} {
     set ::ifoList $ifoListTmp;
  }
  if { [llength $::type] != [llength $::ifoList] && $::useFrameQuery} {
          cleanupAndExit 1 "Error, invalid resource file parameter: type or ifoList. These parameters must be lists of the same length.";
  }  

  set ifoListTmp [ lsort $ifoListTmp ]
  set ::RDSFILE_PREFIX {}
  foreach ifo $ifoListTmp {
    set ::RDSFILE_PREFIX ${::RDSFILE_PREFIX}${ifo}
  }

  return $channels
}

;## 04/20/05 gam
;## Build channelLists with is a list of channel lists for each IFO
;##
proc buildChannelLists { adcList } {
  set channelLists {}

  foreach ifo $::ifoList {

    # Get the channels for just this ifo
    set tmpChannels {}
    foreach line $adcList {
      set line [ string trim $line ]

      # Skip blank and comment lines
      if { ![string length $line] || [regexp {^\#} $line] } {
        continue
      }

      if { [ llength $line ] != 2 } {
        cleanupAndExit 1 "Improperly formatted line in $::adcFile"
      }
    
      foreach { adc dec } $line { continue }

      set qifo [ string index $adc 0 ]
      
      if { $qifo == $ifo } {
        if { $dec == 1 } {
          lappend tmpChannels $adc
        } else {
          lappend tmpChannels "${adc}!$dec"
        }
      }
      
    }
    # END foreach line $adcList

    if { [ llength $tmpChannels ] > 0 } {
         set tmpChannels [join $tmpChannels ","]
         lappend channelLists $tmpChannels
    }

  }
  # END foreach ifo $::ifoList
  
  return $channelLists
}

proc createChannelList {} {
  # Read channel list from file
  if { ! [ file exists $::adcFile ] } {
    cleanupAndExit 1 "File not found: $::adcFile"
  }

  if { [ catch { open $::adcFile r } fid ] } {
    cleanupAndExit 1 "Error opening file: $::adcFile"
  }

  set adcList [ split [ read -nonewline $fid ] "\n" ]
  close $fid

  if { ! [ llength $adcList ] } {
    cleanupAndExit 1 "Empty channel list in file \"$::adcFile\""
  }

  set ::channels [ buildChannels $adcList ]

  # 04/20/05 gam; also get ::channelLists = a list of channel lists for each IFO
  set ::channelLists [ buildChannelLists $adcList ]
  #foreach chanList $::channelLists {
  #    puts "chanList = $chanList"
  #}

}

;##
;## This function initializes all of the global variables and constants.
;## It ought to only be called once on startup.
;##
proc initializeGlobals {} {
  ;##
  ;## Global constants - these values should never be changed
  ;## 
  set ::TRUE 1
  set ::FALSE 0
  set ::RESOURCE_FILE "createrds.rsc"

  ;##
  ;## Each of these variables is a list of time intervals of
  ;## the form "{a b} {c d} ..." representing a set of start and end times
  ;##
  set ::DISKCACHE {}       ;## Time intervals known to the LDAS diskcache
  set ::RDS_DISKCACHE {}   ;## RDS time intervals known to the diskcache
  set ::IN_PROGRESS_SET {} ;## Set of time intervals current being reduced
  set ::ERROR_SET {}       ;## Set of time intervals where errors occurred

  ;## milliseconds between job status updates
  set ::UPDATE_INTERVAL [ expr 5*1000 ]

  ;## milliseconds to wait after a submit error
  set ::ERR_INTERVAL [ expr 1800*1000 ] ;## Changed from 120*1000 to 1800*1000 05/27/04

  ;## milliseconds to wait if the diskcache is empty
  #set ::diskCacheInterval [ expr 1800*1000 ] # 01/19/05 gam; now a .rsc parameter.
    
  ;## How many times to retry a failed job
  set ::maxRetries 3

  ;## List of object ID's in the progress bar canvas
  set ::OBJLIST {}

  ;## Variables for status file
  set ::STATUS "war"
  set ::LAST_SUBMITTED "-"
  set ::LAST_SUBMITTED_TIME 0
  set ::LAST_SUBMITTED_STIME 0
  set ::LAST_SUBMITTED_ETIME 0
  set ::LAST_COMPLETED "-"
  set ::LAST_COMPLETED_TIME 0
  set ::LAST_COMPLETED_STIME 0
  set ::LAST_COMPLETED_ETIME 0

  ;## Log lines to write before rolling over into archive
  set ::MAX_LOGLINES 1000

  ;## Create log directory(s) if necessary
  if { [ file exists "logs" ] } {
    if { ! [ file isdirectory "logs" ] } {
      puts stderr "File \"logs\" exists and is not a directory"
      exit 1
    }
  } else {
    file mkdir "logs"
  }

  if { [ file exists "logs/archive" ] } {
    if { ! [ file isdirectory "logs/archive" ] } {
      puts stderr "File \"logs/archive\" exists and is not a directory"
      exit 1
    }
  } else {
    file mkdir "logs/archive"
  }

  ;## Number of lines written to logs since last archiving
  set ::LOGLINES 0

  # Archive old logfiles
  archiveLogfiles

  # Open new log files
  set ::oid [ open "logs/createrds.log" a ]
  set ::html_oid [ open "logs/DSOcreaterds.log.html" a ]
  logfile "Started createrdsGUI.tcl"

  ;##
  ;## Global variables
  ;##

  ;## Set to TRUE if the user starts the script with the --start option
  ;## If true, the GUI will immediately begin reducing data
  set ::AUTO_START $::FALSE

  # The list of time intervals that have been completed by this
  # instance of the script
  if { [ file exists "createrds.rec" ] } {
    if { [ catch { source createrds.rec } resultSourceRecFile ] } {
      puts stderr " File createrds.rec is not valid; sourcing this file resulted in the error:\n $resultSourceRecFile \n Please fix or remove this file and try again."
      file delete "createrds.status"
      # file delete $::LOCKFILE; # does not yet exist.
      exit 1    
    }
    if { ! [ info exists ::COMPLETED_RDS ] } {
      ;## This is an instance where we want to exit immediately without
      ;## going through the standard exit routine, because we don't want
      ;## to overwrite the corrupted .rec file
      puts stderr "File createrds.rec does not contain the record of previous data reduction. Please remove this file and try again."
      file delete "createrds.status"
      # file delete $::LOCKFILE; # does not yet exist.
      exit 1
    }
  } else {
    set ::COMPLETED_RDS {}
  }

  # Percentage of the total time that has been reduced
  set ::percentComplete 0.0

  # Set the initial state and status bar message
  set ::STATE idle
  set ::SBTEXT "Idle"

} ;## initializeGlobals

;##
;## This function sets the minimum allowed job length based on createRDS parameters.
;##
proc setMinJobLength {} {
  set ::usualMinJobLength [expr $::framesperfile*$::secperframe] ; ## Usual minimum job length to use, except when filling in near a gap.
  if {$::allowshortframes} {  
    set ::minJobLength 1                                      ;## Min. data to reduce in seconds per job  
  } else {
    set ::minJobLength [expr $::framesperfile*$::secperframe] ;## Min. data to reduce in seconds per job
  }
}

;##
;## This function sets the initial defaults for createRDS parameters.
;## It should be run once at startup BEFORE reading the resource file,
;## and then never again.
;##
proc setCreateRDSDefaults {} {
  set ::startTime 729273600 ;## Start of S2
  set ::endTime 734374813   ;## End of S2
  set ::maxJobLength 256    ;## Max. data to reduce in seconds
  set ::maxJobs 2           ;## Max. simultaneous jobs to run
  set ::adcFile "adcdecimate_lho_s2.txt"
  set ::site cit            ;## LDAS site
  set ::user nobody         ;## LDAS username
  set ::notifyList "nobody@nowhere.com"
  set ::type "R"            ;## Input frame type
  set ::useFrameQuery 0     ;## 1 = TRUE 0 = FALSE Must be 1 if length type list > 1; if 1 use framequery syntax with createRDS cmd
  set ::ifoList [ list  ]   ;## List of IFOs; if empty will get set up based on channel list; 04/20/05 gam;
  set ::userType "RDS_R_L"  ;## User type for output frames
  set ::leaveUserTypeAsIs 0 ;## 04/20/05 gam; option to not change userType even if it does not end in a number
  set ::excludeRDS "Y"      ;## Exclude existing RDS data
  set ::frameLength 32      ;## Standard length of a frame file in seconds
  set ::updateFrameLength 0 ;## 1 = TRUE 0 = FALSE; dynamically update the frameLength when DOWNSAMPLING for every segment. 
  set ::diskCacheInterval 1800;## Seconds to wait if the diskcache is empty
  set ::includedirs ""      ;## Reduce input data from these directories only
  set ::excludedirs ""      ;## Do not reduce input data from any of these directories
  set ::usejobdirs 0        ;## Default is use outputdir, not job directories
  set ::outputdir "/archive/LHO/rds/S2"
  set ::md5sumregexp ""     ;## Do not make sed like substitution of outputdir to get path to md5sum files.
  set ::binByTime "Y"
  set ::secPerBin "100000"  ;## Number of seconds of data to put into each directory (must be a multiple of 10 between 10 and 10000000; default is 100000)
  set ::compressiontype "gzip"
  set ::compressionlevel 1
  set ::filechecksum 1      ;## 1 = TRUE 0 = FALSE; if TRUE frame file checksums are validated before running job
  set ::frametimecheck 1    ;## 1 = TRUE 0 = FALSE; if TRUE frame times are validated against the frame name before running job
  set ::framedatavalid 1    ;## 1 = TRUE 0 = FALSE; if TRUE datavalid flags are checked for every channel before running job
  set ::framesperfile 1             ;## Number of frames to put in each output file
  #set ::secperframe $::frameLength  ;## Number of seconds of data to put into each frame
  #set ::secperframe "notSet"        ;## 11/12/04; number of seconds of data to put into each frame; will set to $::frameLength if not set in the resource file.
  set ::secperframe 32               ;## 01/19/05 gam; 11/12/04; number of seconds of data to put into each frame.
  set ::alignFrameStartTimes 1      ;## 04/29/05 gam; 1 = TRUE 0 = FALSE; if TRUE align frame start times with usualMinJobLength expect when producing short frames.
  set ::allowshortframes 0          ;## 1 = TRUE 0 = FALSE; if TRUE allow short frame with less than secperframe in a frame
  set ::allowShortForLatestSeg 0    ;## 1 = TRUE 0 = FALSE; if FALSE never allow short frames when running on the latest data segment
  set ::fillSegmentsIfThisOld 3600  ;## If allowshortframes is TRUE, fill in a segment with short frames if its end time is this old (in seconds).
  set ::generatechecksum 1          ;## 1 = TRUE 0 = FALSE; if TRUE generate frame checksums on a per frame basis
  set ::fillmissingdatavalid 0      ;## 1 = TRUE 0 = FALSE; if TRUE fill any missing channel datavalid aux vec's in the output frames
  #setMinJobLength                   ;## Call function to set ::minJobLength to minimum data to reduce in seconds per job
  #set ::minJobLength [expr $::framesperfile*$::frameLength] ;## 11/12/04; set default ::minJobLength to minimum data to reduce in seconds per job
  setMinJobLength                   ;## 01/19/05 gam; Call function to set ::minJobLength to minimum data to reduce in seconds per job
  set ::totalTime [ expr $::endTime - $::startTime ];
}

;## 
;## This function vets the createrds resource file parameters.
;## Added 07/02/2004.
proc vetCreateRDSParameters {} {
  set vetOut "";
  set vetErrors 0;
  set vetEmptyString "";
  
  set vetIn 0;
  if { [ catch { set vetIn [expr $::startTime < 100000000 || $::startTime > 999999999 ] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: startTime. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: startTime. The startTime parameter is less < 100000000 or > 999999999. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr $::endTime < 100000000 || $::endTime > 999999999 ] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: endTime. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: endTime. The endTime parameter is less < 100000000 or > 999999999. \n";
       set vetErrors 1;
     }
  }

  set ::totalTime [ expr $::endTime - $::startTime ];

  set vetIn 0;  
  if { [ catch { set vetIn [expr $::maxJobLength < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: maxJobLength. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: maxJobLength. The maxJobLength parameter is less < 1. \n";
       set vetErrors 1;
     }
  }
  
  set vetIn 0;
  if { [ catch { set vetIn [expr $::maxJobs < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: maxJobs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: maxJobs. The maxJobs parameter is less < 1. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::adcFile $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: adcFile. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: adcFile. The adcFile parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::site $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: site. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: site. The site parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::user $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: user. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: user. The user parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::notifyList $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: notifyList. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank notifyList is OK.
  }

  set vetIn 0;
  set ::type [string trimleft $::type];  # 04/20/05 gam; make sure no extra spaces exist
  set ::type [string trimright $::type]; # 04/20/05 gam
  if { [ catch { set vetIn [string equal $::type $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: type. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: type. The type parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::useFrameQuery != 0) && ($::useFrameQuery != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: useFrameQuery. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: useFrameQuery. The useFrameQuery parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }
  set vetIn 0;  
  if { [ catch { set vetIn [expr [llength $::type] > 1 && $::useFrameQuery != 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: useFrameQuery or type. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: useFrameQuery or type. The useFrameQuery parameter must be 1 if length of type list > 1. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [llength $::ifoList] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: ifoList. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn == 0} { 
       # This is OK, ::ifoList will get set up based on the channel list.
     } else {
        if { [llength $::type] != [llength $::ifoList] && $::useFrameQuery} {
          puts "\n Error, invalid resource file parameter: type or ifoList. These parameters must be lists of the same length. \n";
          set vetErrors 1;
        }
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::userType $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: userType. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: userType. The userType parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::leaveUserTypeAsIs != 0) && ($::leaveUserTypeAsIs != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: leaveUserTypeAsIs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: leaveUserTypeAsIs. The leaveUserTypeAsIs parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr [lsearch {y Y n N} $::excludeRDS] < 0] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: excludeRDS. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: excludeRDS. The excludeRDS parameter is not set to y, Y, n, or N. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;  
  if { [ catch { set vetIn [expr $::frameLength < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: frameLength. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: frameLength. The frameLength parameter is less < 1. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::updateFrameLength != 0) && ($::updateFrameLength != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: updateFrameLength. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: updateFrameLength. The updateFrameLength parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }  

  set vetIn 0;  
  if { [ catch { set vetIn [expr $::diskCacheInterval < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: ::diskCacheInterval. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: ::diskCacheInterval. The ::diskCacheInterval parameter is less < 1. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::includedirs $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: includedirs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank includedirs is OK.
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::excludedirs $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: excludedirs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank excludedirs is OK.
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::usejobdirs != 0) && ($::usejobdirs != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: usejobdirs. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: usejobdirs. The usejobdirs parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::outputdir $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: outputdir. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: outputdir. The outputdir parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [string equal $::md5sumregexp $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: md5sumregexp. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     # Just continue; a blank md5sumregexp is OK.
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr [lsearch {y Y n N} $::binByTime] < 0] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: binByTime. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: binByTime. The binByTime parameter is not set to y, Y, n, or N. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04: new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr $::secPerBin < 10 || $::secPerBin > 10000000 ] } vetOut ] } {  
     puts "\n Error, invalid resource file parameter: secPerBin. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn != 0} {
        puts "\n Error, invalid resource file parameter: secPerBin. The secPerBin parameter is out of range 10 to 10000000. \n";
       set vetErrors 1;
     }
  }
  
  # 11/10/04: further vet new parameter:  
  set vetIn 0;
  if { [ catch { set vetIn [expr $::secPerBin % 10] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: secPerBin. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn != 0} {
        puts "\n Error, invalid resource file parameter: secPerBin. The secPerBin parameter is not a multiple of 10. \n";
       set vetErrors 1;
     }
  }
  
  set vetIn 0;
  if { [ catch { set vetIn [string equal $::compressiontype $vetEmptyString] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: compressiontype. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: compressiontype. The compressiontype parameter is set to an empty string. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;  
  if { [ catch { set vetIn [expr $::compressionlevel < 0] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: compressionlevel. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: compressionlevel. The compressionlevel parameter is less < 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::filechecksum != 0) && ($::filechecksum != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: filechecksum. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: filechecksum. The filechecksum parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::frametimecheck != 0) && ($::frametimecheck != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: frametimecheck. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: frametimecheck. The frametimecheck parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  set vetIn 0;
  if { [ catch { set vetIn [expr ($::framedatavalid != 0) && ($::framedatavalid != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: framedatavalid. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: framedatavalid. The framedatavalid parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }
  
  # 11/10/04; add new parameter:
  set vetIn 0;  
  if { [ catch { set vetIn [expr $::framesperfile < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: framesperfile. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: framesperfile. The framesperfile parameter is less than 1. \n";
       set vetErrors 1;
     }
  }
  
  # 11/10/04; add new parameter:
  # 01/19/05 gam; secperframe now has a default value of 32.  
  #if {$::secperframe == "notSet"} {
  #   set ::secperframe $::frameLength  ;## 11/12/04; number of seconds of data to put into each frame; set to $::frameLength if not set in the resource file.
  #}
  set vetIn 0;  
  #if { [ catch { set vetIn [expr $::secperframe < $::frameLength] } vetOut ] }
  if { [ catch { set vetIn [expr $::secperframe < 1] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        # puts "\n Error, invalid resource file parameter: secperframe. The secperframe parameter is less than the frameLength. \n";
        puts "\n Error, invalid resource file parameter: secperframe. The secperframe parameter is less than 1. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; next check that $::secperframe is an integer multiple of $::frameLength
  # 01/19/05 gam; no longer needs to be true with LDAS v 1.4.0  
#  set vetIn 0;  
#  if { [ catch { set vetIn [expr 1.0*$::secperframe / (1.0*$::frameLength)] } vetOut ] } {
#     puts "\n Error, invalid resource file parameter: secperframe. Error message:\n $vetOut \n.";
#     set vetErrors 1;
#  } else {
#     if { [expr 1.0*floor($vetIn) - $vetIn] != 0 } {
#        puts "\n Error, invalid resource file parameter: secperframe. The secperframe parameter is not a multiple of the frameLength. \n";
#       set vetErrors 1;
#     }
#  }

  # 04/29/05 gam; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::alignFrameStartTimes != 0) && ($::alignFrameStartTimes != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: alignFrameStartTimes. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: alignFrameStartTimes. The alignFrameStartTimes parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::allowshortframes != 0) && ($::allowshortframes != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: allowshortframes. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: allowshortframes. The allowshortframes parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 01/25/05; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::allowShortForLatestSeg != 0) && ($::allowShortForLatestSeg != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: allowShortForLatestSeg. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: allowShortForLatestSeg. The allowShortForLatestSeg parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 01/25/05; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr $::fillSegmentsIfThisOld < -1000000000] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: fillSegmentsIfThisOld. The parameter should be a number. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: fillSegmentsIfThisOld. The fillSegmentsIfThisOld parameter must be number greater than -1000000000. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::generatechecksum != 0) && ($::generatechecksum != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: generatechecksum. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: generatechecksum. The generatechecksum parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }
  
  # 11/10/04; add new parameter:
  set vetIn 0;
  if { [ catch { set vetIn [expr ($::fillmissingdatavalid != 0) && ($::fillmissingdatavalid != 1)] } vetOut ] } {
     puts "\n Error, invalid resource file parameter: fillmissingdatavalid. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameter: fillmissingdatavalid. The fillmissingdatavalid parameter is not set to 1 or 0. \n";
       set vetErrors 1;
     }
  }

  # 11/10/04; 11/12/04 update minimum job length:
  # setMinJobLength  ;## Call function to set ::minJobLength to minimum data to reduce in seconds per job
  if { [ catch { setMinJobLength  } vetOut ] } {
     puts "\n Error, invalid resource file parameters: could not compute framesperfile*secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  }

  # 11/10/04; vet minJobLength:
  set vetIn 0;  
  if { [ catch { set vetIn [expr $::usualMinJobLength > $::maxJobLength] } vetOut ] } {
     puts "\n Error, invalid resource file parameters: could not compute framesperfile*secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn} {
        puts "\n Error, invalid resource file parameters: framesperfile*secperframe. The framesperfile*secperframe is less than the maxJobLength. \n";
       set vetErrors 1;
     }
  }
     
  # 11/11/04; further vet minJobLength:
  set vetIn 0;  
  if { [ catch { set vetIn [expr $::maxJobLength % $::usualMinJobLength] } vetOut ] } {
     puts "\n Error, invalid resource file parameters: could not compute maxJobLength mod framesperfile*secperframe. Error message:\n $vetOut \n.";
     set vetErrors 1;
  } else {
     if {$vetIn != 0} {
        puts "\n Error, invalid resource file parameters: maxJobLength or framesperfile*secperframe. The maxJobLength must be a multiple of framesperfile*secperframe. \n";
       set vetErrors 1;
     }
  }
   
  return $vetErrors;
}

;##
;## This function is run each time we press the Start button on the
;## createRDS jobs, in order to set all the internally used variables
;## back to their initial values.
;##
proc resetGlobalVariables {} {
  # Wall time at last start request
  set ::timeStartedS [ clock seconds ]
  set ::timeStarted [ clock format $::timeStartedS -format %c ]

  # A counter to keep track of how many times we've looped in
  # the running state. This is to get rid of spurious rate messages
  # occurring when we have to wait for new jobs
  set ::runningCtr 0

  # Total time reduced since the Start button was pushed
  set ::totalTimeReduced 0

  # This ensures that the diskcache is updated each time we restart
  # Not strictly necessary, but makes things a little safer
  set ::DISKCACHE {}

  set ::realtimeRatio 1.0
  set ::overallRealtimeRatio 1.0
  set ::realtimeRatioMsg "Seconds Of Data Reduced/Elapsed Wall Time: --"
  set ::realtimeRatioAlarm $::FALSE
  set ::jobCtr 0
  set ::failedJobCtr 0
  set ::currentExistingJobs 0
  set ::IN_PROGRESS_SET {}
} ;## resetGlobalVariables

;##
;## Decr function, to complement incr
;##
proc decr { a } {
  upvar 1 $a aa
  set aa [ expr $aa - 1 ]
}

;##
;## Return the list of times contained in the diskcache for a given
;## IFO and type
;##
proc diskcacheTimes { qifo qtype } {
  upvar ::DISKCACHE_IMAGE cache

  set output {}

  # New format, looks like this:
  # /archive/LLO/full/S1/L-R-7132,L,R,1,16 1044788590 1125 {713248208 713255408 713259008 713269808}

  # Loop over directories in the cache with type $qtype
  foreach { key value } [ array get cache *,*,$qtype,* ] {
    foreach { dir ifo type nperfile dt } [ split $key "," ] continue

    # If includedirs is not an empty list, reduce input data from includedirs only
    if { [string trim $::includedirs] > ""} {
       set dirMatchFound $::FALSE;
       foreach qdir $::includedirs {
         if { [ string match *${qdir}* $dir ] } {
            set dirMatchFound $::TRUE;
            break;
         }
       }
       if { ! $dirMatchFound } continue
    }

    # If excludedirs is not an empty list, do not reduce any input data from excludedirs
    if { [string trim $::excludedirs] > ""} {
       set dirMatchFound $::FALSE;
       foreach qdir $::excludedirs {
         if { [ string match *${qdir}* $dir ] } {
            set dirMatchFound $::TRUE;
            break;
         }
       }
       if { $dirMatchFound } continue
    }

    # Skip any that don't match this IFO
    if { ! [ string match *${qifo}* $ifo ] } continue

    # Get the list of times
    set timesList [ lindex $value 2 ]
    foreach { start end } $timesList {
      lappend output [ list $start $end ]
    }

  }

  # Sort output based on start-time
  set output [ lsort -integer -index 0 $output ]

  # Merge adjacent or overlapping segments
  set merged {}
  foreach interval $output {
    set merged [ setIntervalUnion $merged $interval ]
  }
  
  return $merged
}

;##
;## Merge the elements of the RDS cache dump
;##
proc normalizeRDSCacheDump {} {

  set rdsType [ RDSType $::type $::userType ]
  set ::RDS_DISKCACHE [ diskcacheTimes $::RDSFILE_PREFIX $rdsType ]
  set interval [ list $::startTime $::endTime ]
  set interval [ list $interval ]
  set ::RDS_DISKCACHE [ setIntersection $::RDS_DISKCACHE $interval ]

} ;## normalizeRDSCacheDump

;##
;## Process the raw diskcache info, merging adjacent segments that have the
;## same dt. Then, if downsampling is being done, remove one dt from the start
;## and end.
;##
;## The result is list of intervals, any subinterval of which will be 
;## a valid -times option to createRDS
;##
proc normalizeCacheDump {} {

  # Generate the list of times where raw data is present
  # This is created by running through each interferometer that
  # is needed (as determined from the output frame prefix) and
  # taking the intersection.

  # For each IFO, take the intersection with the current set
  # of time spans
  set merged {}
  #foreach ifo [ split $::RDSFILE_PREFIX {} ] # 04/20/05 gam
  set typeIndex 0; # rewrite to use type that goes with each IFO
  foreach ifo $::ifoList {
    set typeThisIFO [lindex $::type $typeIndex];
    incr typeIndex;
    # set T [ diskcacheTimes $ifo $::type ]
    set T [ diskcacheTimes $ifo $typeThisIFO ]
    if { [ llength $merged ] == 0 } {
      set merged $T
    } else {
      set merged [ setIntersection $merged $T ]
    }
  }

  # 01/19/05 gam
  if {$::updateFrameLength && $::DOWNSAMPLING} {
     # Trim the range of times down to what the user requested + a bit extra
     set interval [ list [expr $::startTime - $::maxJobLength] [expr $::endTime + $::maxJobLength] ]
     set merged [ setIntervalIntersection $merged $interval ]
  }
  
  #
  # If downsampling, trim off the ends of the intervals that we can't
  # actually process in createRDS anyway. Prevents errors due to
  # missing data. Otherwise, just use the given start and end times
  # 
  set result {}
  foreach interval $merged {
    foreach { start end } $interval {          
      if { $::DOWNSAMPLING } {
        # 01/19/05 gam
        if {$::updateFrameLength} {
           #set dynamicFrameLength [getFrameLength $start $::frameLength] # 04/20/05 gam
           set dynamicFrameLength 0
           set qtypeIndex 0
           foreach qtype $::type {
              set thisDynamicFrameLength [getFrameLength $start $::frameLength $qtype $qtypeIndex]
              incr qtypeIndex;
              if {$thisDynamicFrameLength > $dynamicFrameLength} {
                 set dynamicFrameLength $thisDynamicFrameLength
              }
           }
           set newStart [ expr $start + $dynamicFrameLength ]
           #set dynamicFrameLength [getFrameLength [expr $end - 2] $::frameLength]; # Need -2 because of current way LDAS works.
           set dynamicFrameLength 0
           set qtypeIndex 0
           foreach qtype $::type {
              set thisDynamicFrameLength [getFrameLength [expr $end - 2] $::frameLength $qtype $qtypeIndex]
              incr qtypeIndex;
              if {$thisDynamicFrameLength > $dynamicFrameLength} {
                 set dynamicFrameLength $thisDynamicFrameLength
              }
           }
           set newEnd [ expr $end - $dynamicFrameLength ]
        } else {
          set newStart [ expr $start + $::frameLength ]
          set newEnd [ expr $end - $::frameLength ]
        }
        lappend result [ list $newStart $newEnd ]
      } else {
        lappend result [ list $start $end ]
      }
    }
  }
  
  # Trim the range of times down to what the user requested
  set interval [ list $::startTime $::endTime ]
  set ::DISKCACHE [ setIntervalIntersection $result $interval ]

} ;## normalizeCacheDump

;##
;## This procedure updates a text widget w with a message
;##
proc updateStatusWindow { w msg } {
  $w delete 1.0 end
  $w insert end "$msg"
  update idletasks
}

;##
;## Submit a job in the background and retry until it is accepted.
;##
;## This function doesn't return until the job is accepted, but doesn't
;## block the event loop.
;##
proc submitJob { job cmd } {
  LJrun ::$job -manager $::site -user $::user -nowait $cmd

  if { $::LJerror != 0 } {
    set errmsg [ set ::${job}(error) ]
    regsub -all -- {[\n\s\t]+} $errmsg { } errmsg
    .sub.status.alarm config -background red
    updateStatusWindow .sub.resp.text $errmsg
    LJdelete ::$job
    return -code error $errmsg
  } else {
    set jobInfo [ set ::${job}(jobInfo) ]
    regsub -all -- {[\n\s\t]+} $jobInfo { } jobInfo
    set ::LAST_SUBMITTED [ set ::${job}(jobid) ]
    set ::LAST_SUBMITTED_TIME [ tconvert now ]
    .sub.status.alarm config -background green
    updateStatusWindow .sub.resp.text $jobInfo
    return
  }
}

;##
;## A blocking function to wait for an LDAS job to complete
;##
proc waitForJob { job } {
  LJstatus ::$job
  set status [ set ::${job}(status) ]
  while { $status == "running" || $status == "submitted" } {
    update idletasks
    after 1000
    LJstatus ::$job
    set status [ set ::${job}(status) ]
  }

  # Now call LJwait on the job (ought to be instantaneous) and return
  LJwait ::$job
}

;##
;## Query LDAS to get the frameLength for a give GPS time
;##
#proc getFrameLength {inputTimes defaultFrameLength} # 04/20/05 gam
proc getFrameLength {inputTimes defaultFrameLength qtype qtypeIndex} {

  set returnFrameLength $defaultFrameLength;
  
  # Clean up any old cacheGetFilenamesJob
  if { [ info exists ::cacheGetFilenamesJob ] } {
    LJwait ::cacheGetFilenamesJob
    LJdelete ::cacheGetFilenamesJob
  }

  #set cmd "cacheGetFilenames -times { $inputTimes } -type { $qtype } -channels { $::channels }" # 04/20/05 gam
  set tmpChannels [lindex $::channelLists $qtypeIndex]
  set cmd "cacheGetFilenames -times { $inputTimes } -type { $qtype } -channels { $tmpChannels }"
  submitJob cacheGetFilenamesJob $cmd    
  waitForJob cacheGetFilenamesJob
      
  if { $::cacheGetFilenamesJob(status) == "done" } {
    set frameNameJobOutput $::cacheGetFilenamesJob(jobReply);
    LJdelete ::cacheGetFilenamesJob    
    if { ! [ regexp {/.*\.gwf} $frameNameJobOutput frameNameList ] } {
       set msg "Could not find frame name to get frameLength, using default"
       log $msg
       logfile $msg red
       errorNotify $msg
       return $returnFrameLength;
    } else {
       foreach frameName [split $frameNameList] {
         regexp {\-([0-9]+)\-([0-9]+).gwf} $frameName match thisFrameStartTime thisFrameLength;
         if {$thisFrameLength > $returnFrameLength} {
            set returnFrameLength $thisFrameLength;
         }
       }
       set msg "Succesfully ran cacheGetFilenames on GPS time $inputTimes to update frameLength to $returnFrameLength s."
       logfile $msg
    }
  } elseif { $status == "error" } {
    set msg \
      "$::cacheGetFilenamesJob(jobid) Error running cacheGetFilenamesJob user command: \"$::cacheGetFilenamesJob(error)\""
    log $msg
    logfile $msg red
    errorNotify $msg
    #update idletasks
    LJdelete ::cacheGetFilenamesJob
  } else {
    cleanupAndExit 1 "cacheGetFilenamesJob: invalid status \"$::cacheGetFilenamesJob(status)\""
  }

  return $returnFrameLength;
}

;##
;## Query LDAS to get the current diskcache contents
;##
proc getDiskCache {} {
  # Clean up any old diskCacheJob's
  if { [ info exists ::diskCacheJob ] } {
    LJwait ::diskCacheJob
    LJdelete ::diskCacheJob
  }

  # Run an LDAS job to get the frame cache info
  set cmd "getFrameCache -returnprotocol http://frame.cache"
  submitJob diskCacheJob $cmd
  waitForJob diskCacheJob

  if { $::diskCacheJob(status) == "done" } {
    set framecache [ LJread $::diskCacheJob(outputs) ]
    catch { unset ::DISKCACHE_IMAGE }
    foreach line [ split $framecache "\n" ] {
      array set ::DISKCACHE_IMAGE [ list [ lindex $line 0 ] [ lrange $line 1 end ] ]
    }
    LJdelete ::diskCacheJob
    return
  } elseif { $status == "error" } {
    set msg \
      "$::diskCacheJob(jobid) Error running getFrameCache user command: \"$::diskCacheJob(error)\""
    log $msg
    logfile $msg
    errorNotify $msg
    update idletasks
    LJdelete ::diskCacheJob
    return -code error
  } else {
    cleanupAndExit 1 "getDiskCache: invalid status \"$::diskCacheJob(status)\""
  }

} ;## getDiskCache

;## 11/11/04
;## normalize ::DISKCACHE to multiples of minJobLength
;##
proc normCacheDumpModMinJobLength {} {

  if {$::minJobLength == 1} {
     ;## 01/19/05 gam; Only change last interval, as needed
     set gpsNow [ tconvert now ]
     set diskCacheLength [llength $::DISKCACHE]
     if {$diskCacheLength > 0} {
        # 04/29/05 gam; find the last end time.
        set lastInterval [lindex $::DISKCACHE end]
        foreach { lastStart lastEnd } $lastInterval { continue }
        #puts "::DISKCACHE = $::DISKCACHE\n";
        #puts "lastEnd = $lastEnd";
     }
     set intervalCount 0
     set result {}
     foreach interval $::DISKCACHE {
       incr intervalCount
       foreach { start end } $interval {
         if {$intervalCount < $diskCacheLength} {
           # 04/29/05 gam; check how old compared to most recent segment; if { [expr $gpsNow - $end] > $::fillSegmentsIfThisOld }
           if { [expr $lastEnd - $end] > $::fillSegmentsIfThisOld } {
             # Segment is old enough and not the last segment; go ahead and fill with short frames if needed.
             lappend result [ list $start $end ]; # do not adjust this interval
           } else {
             # Else, reduce this segment to a multiple of the usual job length; LDAS will NOT need to output any short frames.
             # 04/29/05 gam; if ::alignFrameStartTimes is TRUE align frames start time
             if {$::alignFrameStartTimes} {
                set extraTime [ expr $start % $::usualMinJobLength ]
                if { $extraTime > 0 } {
                   set start [ expr $start - $extraTime + $::usualMinJobLength ] ;## move start to next aligned time
                }
             }
             if { [expr $end - $start ] >= $::usualMinJobLength } {
               set extraTime [ expr ($end - $start) % $::usualMinJobLength ]
               set end [ expr $end - $extraTime ] ;## trim off extra
               if { [expr $end - $start ] >= $::usualMinJobLength } {
                lappend result [ list $start $end ]
               }
             }                        
           }
         } else {
           if { $::allowShortForLatestSeg  &&  ( [expr $gpsNow - $end] > $::fillSegmentsIfThisOld ) } {
             # If allowing short frames in the latest segment and end time of the segment is older than specified
             # then go ahead and run on this segment regardless of its length; LDAS will output short frames as needed.
             lappend result [ list $start $end ]; # do not adjust this interval                      
           } else {
             # Else, reduce this segment to a multiple of the usual job length; LDAS will NOT need to output any short frames.
             # 04/29/05 gam; if ::alignFrameStartTimes is TRUE align frames start time
             if {$::alignFrameStartTimes} {
                set extraTime [ expr $start % $::usualMinJobLength ]
                if { $extraTime > 0 } {
                   set start [ expr $start - $extraTime + $::usualMinJobLength ] ;## move start to next aligned time
                }
             }
             if { [expr $end - $start ] >= $::usualMinJobLength } {
               set extraTime [ expr ($end - $start) % $::usualMinJobLength ]
               set end [ expr $end - $extraTime ] ;## trim off extra
               if { [expr $end - $start ] >= $::usualMinJobLength } {
                lappend result [ list $start $end ]
               }
             }
           }
         }
       }
     }     
  } else {
     set result {}
     foreach interval $::DISKCACHE {
       foreach { start end } $interval {
         # 04/29/05 gam; if ::alignFrameStartTimes is TRUE align frames start time
         if {$::alignFrameStartTimes} {
            set extraTime [ expr $start % $::minJobLength ]
            if { $extraTime > 0 } {
               set start [ expr $start - $extraTime + $::minJobLength ] ;## move start to next aligned time
            }
         }
         if { [expr $end - $start ] >= $::minJobLength } {
           set extraTime [ expr ($end - $start) % $::minJobLength ]
           set end [ expr $end - $extraTime ] ;## trim off extra
           if { [expr $end - $start ] >= $::minJobLength } {
              lappend result [ list $start $end ]
           }
         }
       }
     }
  }
  set ::DISKCACHE $result

} ;## normCacheDumpModMinJobLength

;##
;## Update various pieces of diskcache information
;##
proc updateDiskCache {} {
  # Run an LDAS job to get the contents of the entire disk cache
  getDiskCache

  # Construct the "normalized" disk cache of RAW data specifically
  # for the IFO(s) and type of interest
  normalizeCacheDump

  # Set the value of the global variable holding the latest time in
  # the diskcache
  foreach { - ::DISKCACHE_LATEST } [ lindex $::DISKCACHE end ] { continue }

  if { $::excludeRDS == "Y" || $::excludeRDS == "y" } {
    # Construct the "normalized" disk cache of REDUCED data specifically
    # for the IFO and type of interest
    normalizeRDSCacheDump

    # Now subtract the reduced data from the raw data
    set ::DISKCACHE [ setDifference $::DISKCACHE $::RDS_DISKCACHE ]
  }

  # Now subtract the data that has been reduced by the current instance
  # of createrds (in case that's not visible to LDAS yet)
  set ::DISKCACHE [ setDifference $::DISKCACHE $::COMPLETED_RDS ]

  # Now subtract data where errors occurred
  set ::DISKCACHE [ setDifference $::DISKCACHE $::ERROR_SET ]

  ;## 11/11/04; normalize ::DISKCACHE to multiples of minJobLength
  normCacheDumpModMinJobLength

} ;## updateDiskCache

;##
;## Return a list of all time intervals present in the diskcache in the
;## in range stime to etime ie. the intersection of the diskcache with the
;## interval [$stime, $etime)
;##
proc getSubinterval { stime etime } {
  set subinterval {}

  set interval [ list $stime $etime ]
  set interval [ list $interval ]
  set subinterval [ setIntersection $::DISKCACHE $interval ]

  return $subinterval
}

;##
;## Get the next available time interval, which may be shorter than
;## the maximum interval allowed.
;##
;## Returns a time interval in the list "$stime $etime", or the empty set if
;## there are no more times to process
;##
proc getNextAvailableTime {} {

  # Check to see if there's any more data to process
  if { [ llength $::DISKCACHE ] == 0 } {
    return {}
  }

  # Set the start-time to the first available time in the diskcache
  set interval [ lindex $::DISKCACHE 0 ]
  foreach { stime etime } $interval { continue }

  # 04/29/05 gam; if aligning frame, check if extra time at start of the interval
  set extraTime 0;  # default
  if {$::alignFrameStartTimes} {
     set extraTime [ expr $stime % $::usualMinJobLength ]
     # Note that extraTime will be > 0 only if $::allowshortframes is TRUE.
     # Otherwise we have already aligned segment start times with usualMinJobLength!
  }
  if { $extraTime > 0 } {
    # 04/29/05 gam; set etime so that a shortframe job run on the extra time at start of interval!
    set etime [ expr $stime - $extraTime + $::usualMinJobLength ]
  } else {
    # 04/29/05 gam; else do as before:
    
    # Make sure the end time doesn't stray into the next block of
    # $::secPerBin seconds, so we end up writing data to the correct directory
    # (note, relies on integer arithmetic)
    #set maxEtime [ expr ($stime/100000 + 1)*100000 ]
    set maxEtime [ expr ($stime/$::secPerBin + 1)*$::secPerBin ]
    set etime [ expr $stime + $::maxJobLength ]
    set etime [ min $etime $maxEtime ]
  
    # 11/10/04; 11/11/04; make sure interval is multiple of the minimum job length:
    # Intersection with DISKCACHE below will give actual interval to work on.  
    # 02/09/05; make interval a multiple of the ::usualMinJobLength.
    set extraTime [ expr ($etime - $stime) % $::usualMinJobLength ]
    set etime [ expr $etime - $extraTime ] ;## trim off extra
    if { [expr $etime - $stime] < $::usualMinJobLength } {
       set etime [ expr $stime + $::usualMinJobLength ]
    }
  }
  
  # Now find the intersection of this interval with what's in the diskcache
  # The intersection of an interval with the diskcache yields a set, which
  # may contain more than one interval if there are gaps, so we just take the
  # first interval from the set
  set interval [ list $stime $etime ]
  set interval [ setIntervalIntersection $::DISKCACHE $interval ]
  set interval [ lindex $interval 0 ]

  return $interval
} ;## getNextAvailableTime

;## 04/20/05 gam
;## Build the framequery for this job
;## 
proc buildFrameQuery { times } {
  
  set frameQueryString "";
  set typeIndex 0; # also gives channels from channelLists
  foreach ifo $::ifoList {
    set typeThisIFO [lindex $::type $typeIndex];
    set channelsThisIFO [lindex $::channelLists $typeIndex];
    if {$typeIndex > 0} {
       append frameQueryString " ";
    }
    append frameQueryString "{ $typeThisIFO $ifo {} $times Chan($channelsThisIFO) }";
    incr typeIndex;    
  }
  # puts "frameQueryString = \n$frameQueryString";
  
  return $frameQueryString;
}

;##
;## Build the createRDS command. The only thing that changes is the time
;## span of the query
;##
proc buildCmd { times } {

  set subLength [expr 9 - [ string length $::secPerBin] ]
    
  if { $::binByTime == "Y" || $::binByTime == "y" } {
    foreach { stime etime } $times { continue }

    # Get the first four digits of stime
    #set subtime [ string range $stime 0 3 ]
    set subtime [ string range $stime 0 $subLength ]
    
    # Determine the output RDS type
    set rdsType [ RDSType $::type $::userType ]

    set dir "${::outputdir}/${::RDSFILE_PREFIX}-${rdsType}-${subtime}"
  } else {
    set dir ${::outputdir}
  }

 if {$::useFrameQuery} { 

  # 04/20/05 gam; use -framequery syntax
  set frameQueryString [buildFrameQuery $times]
  set cmd "
createRDS
  -framequery { $frameQueryString }
  -usertype { $::userType }
  -outputdir { $dir }
  -usejobdirs { $::usejobdirs }
  -compressiontype { $::compressiontype }
  -compressionlevel { $::compressionlevel }
  -filechecksum { $::filechecksum }
  -frametimecheck { $::frametimecheck }
  -framedatavalid { $::framedatavalid } 
  -framesperfile { $::framesperfile }
  -secperframe { $::secperframe }
  -allowshortframes { $::allowshortframes }
  -generatechecksum { $::generatechecksum }
  -fillmissingdatavalid { $::fillmissingdatavalid }"  

  # puts $cmd;

 } else {

  # 04/20/05 gam; else do as before 
  set cmd "
createRDS
  -times $times
  -type { $::type }
  -usertype { $::userType }
  -outputdir { $dir }
  -usejobdirs { $::usejobdirs }
  -channels { $::channels }
  -compressiontype { $::compressiontype }
  -compressionlevel { $::compressionlevel }
  -filechecksum { $::filechecksum }
  -frametimecheck { $::frametimecheck }
  -framedatavalid { $::framedatavalid } 
  -framesperfile { $::framesperfile }
  -secperframe { $::secperframe }
  -allowshortframes { $::allowshortframes }
  -generatechecksum { $::generatechecksum }
  -fillmissingdatavalid { $::fillmissingdatavalid }"
  
 }

 if {$::md5sumregexp > ""} {
    append cmd " -md5sumregexp { $::md5sumregexp }";
 }

  return $cmd
}

;##
;## Print a log message to the end of the log window
;##
proc log { text } {
  .t.log insert end "$text\n"
}

;##
;## Clear the log window
;##
proc clearLog {} {
  .t.log delete 1.0 end
}

;##
;## Print the status of job $jobIndex to the log window
;##
proc printJobStatus { jobIndex } {
  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  set jobNum [ expr $jobIndex + 1 ]
  set now [ clock seconds ]

  log "Job $jobNum of $::maxJobs"
  if { [ info exists job ] } {
    log "  Job ID    : $job(jobid)" 
    log "  Job times : $jobAux(times) ($jobAux(duration) seconds)"
    log "  Status    : $job(status)"
    log "  Runtime   : [ expr $now - $job(startTimeS) ] seconds"
  } else {
    log "  Job ID    : --" 
    log "  Job times : --"
    log "  Status    : Not running"
    log "  Runtime   : --"
  }
  log ""

}

;##
;## Draw colored rectangles representing a set of intervals into
;## the progress bar canvas
;##
proc drawSet { h w x0 T set color } {
  foreach interval $set {
    foreach { a b } $interval { continue }

    set x1 [ expr round((double($a) - $x0)*$w/$T) ]
    set x2 [ expr round((double($b) - $x0)*$w/$T) ]
    
    if { [ expr abs($x2 - $x1) ] <= 0 } {
      set x2 [ expr $x1 + 1 ]
    }

    .prog.c.can create rectangle $x1 0 $x2 $h -fill $color -outline $color
  }
}

;##
;## Update the canvas widget representing the data that has been reduced
;## and the data left to go
;##
proc updateProgressBar {} {
  set h [ winfo height .prog.c.can ]
  set w [ expr double([ winfo width .prog.c.can ]) ]
  set x0 [ expr double($::startTime) ]
  set T [ expr double($::endTime) - $x0 ]

  # First delete the old rectangles
  .prog.c.can delete all

  drawSet $h $w $x0 $T $::RDS_DISKCACHE yellow
  drawSet $h $w $x0 $T $::DISKCACHE orange
  drawSet $h $w $x0 $T $::COMPLETED_RDS green
  drawSet $h $w $x0 $T $::IN_PROGRESS_SET blue
  drawSet $h $w $x0 $T $::ERROR_SET red
}

;##
;## Update the log window
;##
proc updateDisplay {} {

  set timeNowS [ clock seconds ]
  set ::timeNow [ clock format $timeNowS -format %c ]
  set elapsed [ expr $timeNowS - $::timeStartedS ]
  set elapsedMsg "[expr $elapsed/3600]h [expr ($elapsed % 3600)/60]m [expr ($elapsed % 3600) % 60]s"
 
  # Work out the time actually spent running, with a fudge factor to
  # avoid division by zero
  # 02/10/05; update overall realtimeRatio only after a multiple of maxJobs has finished!
  set remainderForSetOfJobs [expr $::jobCtr % $::maxJobs]
  if { $remainderForSetOfJobs == 0 && $::jobCtr >= $::maxJobs } {
     set runningTime [ expr $::runningCtr*($::UPDATE_INTERVAL/1000) + 0.1 ]
     set ::overallRealtimeRatio \
      [ expr round(100.0*$::totalTimeReduced/$runningTime)/100.0 ]
  }

  # Determine the ratio of data reduced/real-time elapsed. It will be
  # considered a non-fatal error if this ratio falls below 1.0 while in
  # the running state
  set ::realtimeRatioMsg "Seconds Of Data Reduced/Elapsed Wall Time: $::overallRealtimeRatio"
  # if { $::realtimeRatio < 1.0 && $::totalTimeReduced > 0 && $::STATE == "running" } # send error only if at one set of maxJobs has run.
  if { $::overallRealtimeRatio < 1.0 && $::totalTimeReduced > 0 && $::STATE == "running" && $::jobCtr > $::maxJobs } {
    .prog.ratio.status.alarm config -background red
    if { ! $::realtimeRatioAlarm } {
      set msg "$::RDSFILE_PREFIX $::type data reduction rate has fallen to $::overallRealtimeRatio times real-time"
      logfile $msg red
      errorNotify $msg
      set ::realtimeRatioAlarm $::TRUE
    }
  } else {
    # This variable prevents multiple email alerts when the realtime ratio
    # is below 1.0 for a long time
    set ::realtimeRatioAlarm $::FALSE
    .prog.ratio.status.alarm config -background green
  }

  if { [ llength $::DISKCACHE ] > 0 } {
    set interval [ lindex $::DISKCACHE 0 ]
    foreach { nextJobStart dummy } $interval { continue }
    set nextJobStart "$nextJobStart ([ tconvert $nextJobStart ])"
  } else {
    set nextJobStart "--"
  }

  # set totalTime [ expr $::endTime - $::startTime ] # 02/09/05; make total time global var
  set allRDS [ setUnion $::RDS_DISKCACHE $::COMPLETED_RDS ]
  set tDone [ setSize $allRDS ]
  set ::percentComplete [ expr round(10000.0*$tDone/$::totalTime)/100.0 ]
  set grandTotalTimeReduced [ setSize $::COMPLETED_RDS ]

  clearLog

  log "Started at                          : $::timeStarted"
  # 05/26/05 does not update while waiting: log "Current time                        : $::timeNow"
  log "Last time job was running           : $::timeNow"
  log "Elapsed time                        : $elapsedMsg"
  log "IFO(s)                              : $::RDSFILE_PREFIX"
  log "Number of jobs completed            : $::jobCtr"
  log "Number of failed jobs               : $::failedJobCtr"
  log "Time processed this run             : $::totalTimeReduced seconds"
  log "Grand total time processed          : $grandTotalTimeReduced seconds"
  log "Next job will work on data starting : $nextJobStart"
  log ""

  for { set k 0 } { $k < $::maxJobs } { incr k } {
    printJobStatus $k
  }

  update idletasks
}

;##
;## This procedure is called when a job has successfully completed
;##
proc reapSuccessfulJob { jobIndex } {

  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  if { $job(status) == "done" } {
    foreach { stime etime } [ split $jobAux(times) - ] { continue }
    set jobid $job(jobid)
    set ::STATUS "ok"
    set ::LAST_COMPLETED $jobid
    set ::LAST_COMPLETED_TIME [ tconvert now ]
    set ::LAST_COMPLETED_STIME $stime
    set ::LAST_COMPLETED_ETIME $etime
    set jobReply $job(jobReply)
    regsub -all -- {[\n\s\t]+} $jobReply { } jobReply
    updateStatusWindow .job.resp.text $jobReply
    .job.status.alarm config -background green
    # 02/10/05; add next two lines to get time to run this job
    set now [ clock seconds ]
    set jobTime [ expr $now - $job(startTimeS) + 0.1 ]
    LJdelete ::job$jobIndex
    decr ::currentExistingJobs 
    set interval [ list $stime [ expr $etime + 1 ] ]
    set ::COMPLETED_RDS [ setIntervalUnion $::COMPLETED_RDS $interval ]
    set ::IN_PROGRESS_SET [ setIntervalDifference $::IN_PROGRESS_SET $interval ]
    
    set ::totalTimeReduced [ expr $::totalTimeReduced + $jobAux(duration) ]

    # 02/09/05; update realtimeRatio when totalTimeReduced changes.
    #set runningTime [ expr $::runningCtr*($::UPDATE_INTERVAL/1000) + 0.1 ]
    #set ::realtimeRatio \
    #  [ expr round(100.0*$::totalTimeReduced/$runningTime)/100.0 ]
    # 02/10/05; compute real-time ratio for this one job only:    
    set ::realtimeRatio \
      [ expr round(100.0*$jobAux(duration)/$jobTime)/100.0 ]

    # 02/09/05; update percentComplete when COMPLETED_RDS changes above.
    set allRDS [ setUnion $::RDS_DISKCACHE $::COMPLETED_RDS ]
    set tDone [ setSize $allRDS ]
    set ::percentComplete [ expr round(10000.0*$tDone/$::totalTime)/100.0 ]

    # Take it out of the error set if necessary
    set ::ERROR_SET [ setIntervalDifference $::ERROR_SET $interval ]
    set jobAux(retry) $::FALSE
    incr ::jobCtr
    logfile "$jobid $::RDSFILE_PREFIX $::type $jobAux(times) ($jobAux(duration) seconds) SUCCEEDED. Rate $::realtimeRatio times real-time, $::percentComplete\% done"
  } else {
    cleanupAndExit 1 "reapSuccessfulJob: got job with status $job(status)"
  }

}

;##
;## When it turns out that file already exists for some time interval,
;## extract the file start-time and dt and add that interval to the
;## completed RDS
;## 
;##
proc handleExistingFile { msg jobInterval } {
  # Extract the filename from the message
  if { ! [ regexp {/.*\.gwf} $msg f ] } {
    logfile "File does not match *.gwf extension" red
    return
  }

  set f [ file tail $f ]
  set f [ file rootname $f ]

  # Work out the interval of time which this file covers
  #foreach { prefix type stime duration } [ split $f - ] { continue } # 04/20/05 gam; should make sure type is local var
  foreach { prefix qtype stime duration } [ split $f - ] { continue }
  set etime [ expr $stime + $duration ]
  set interval [ list $stime $etime ]
  
  # Add this file to the "completed" list
  set ::COMPLETED_RDS [ setIntervalUnion $::COMPLETED_RDS $interval ]
  
  # Add the remainder of the time interval back into the diskcache pool,
  # assuming the remainder has non-zero size
  foreach { - jobEtime } $jobInterval { continue }
  if { $etime < $jobEtime } {
    set interval [ list $etime $jobEtime ]
    set ::DISKCACHE [ setIntervalUnion $::DISKCACHE $interval ]
  }
}

;##
;## Handle failed jobs
;## 
proc reapFailedJob { jobIndex } {

  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  if { $job(status) == "error" } {
    set times $jobAux(times)
    foreach { stime etime } [ split $jobAux(times) - ] { continue }
    set interval [ list $stime [ expr $etime + 1 ] ]
    set ::STATUS "err"
    set ::LAST_COMPLETED $job(jobid)
    set ::LAST_COMPLETED_TIME [ tconvert now ]
    set ::LAST_COMPLETED_STIME $stime
    set ::LAST_COMPLETED_ETIME $etime

    set errmsg "$job(error)"
    regsub -all -- {[\n\s\t]+} $errmsg { } errmsg

    switch -glob $errmsg {
      "*limited queue size*" -
      "*aborted by LDAS*" -
      "*connection refused*" -
      "*please retry job*" -
      "*System Shutting Down*" {
        # If the job fails because LDAS shuts down, or because the queue is
        # full, we want to retry an infinite number of times. The simplest
        # way to handle it is to add the time interval back into the list of
        # available times and wait until LDAS starts up again.
        set ::DISKCACHE [ setIntervalUnion $::DISKCACHE $interval ]
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
      }
      "*Frame file already exists*" {
        # This often happens when a previous job fails partway through,
        # leaving some frame files behind. Special handling is needed
        # to account for the files that already exist.
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
        handleExistingFile $errmsg $interval
      }
      "*no data found for time*" {
        # If we get this error it usually means that our view of the
        # diskcache disagrees with LDAS, and we should update it.
        #
        # The easiest solution appears to be to set the DISKCACHE variable
        # back to the empty set, so that it will be updated on the next
        # pass through runningJob. This also means that having more than
        # one job fail this way will lead to inconsistencies (although it
        # may mean that the cache is updated a couple of times).
        #
        # Note we don't attempt to retry - this will happen automatically
        # when the cache is refreshed if this time interval is present.
        # We also don't add this interval to the error set.
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
        set ::DISKCACHE {}
      }
      "*socket closure*" -
      "*aborted after*seconds*" -
      "*aborting*" -
      "*job has been aborted*" -
      "*Timeout waiting*" -
      "*malformed command*" -
      "*no frames*" -
      "*API shutting down*" {
        # These should be retried only a finite number of times. Could
        # possibly be caused by bad data or random coredumps of the API
        set jobAux(retry) $::TRUE
      }
      "*Missing FrEndOfFile*" -
      "*At least three frame files must be specified*" - 
      default {
        # Jobs with these errors are unrecoverable, don't retry.
        # Default is also not to retry.
        set jobAux(retry) $::FALSE
        set jobAux(tries) 1
        set ::ERROR_SET [ setIntervalUnion $::ERROR_SET $interval ]
      }
    }

    updateStatusWindow .job.resp.text $errmsg
    .job.status.alarm config -background red
    set msg "$job(jobid) $::RDSFILE_PREFIX $::type $times ($jobAux(duration) seconds) FAIL($jobAux(tries)): $errmsg"
    logfile $msg red
    errorNotify $msg
    set ::IN_PROGRESS_SET [ setIntervalDifference $::IN_PROGRESS_SET $interval ]
    LJdelete ::job$jobIndex
    decr ::currentExistingJobs
    incr ::failedJobCtr
  } else {
    cleanupAndExit 1 "reapFailedJob: got job with status $job(status)"
  }

}

;##
;## Retry jobs that failed on the last main loop iteration
;## 
proc retryFailedJob { jobIndex } {

  upvar ::job${jobIndex}Aux jobAux

  set times $jobAux(times)
  foreach { stime etime } [ split $jobAux(times) - ] { continue }
  set interval [ list $stime [ expr $etime + 1 ] ]

  incr jobAux(tries)
  if { $jobAux(tries) <= $::maxRetries } {
    set cmd [ buildCmd $times ]
    submitJob job${jobIndex} $cmd
    set ::LAST_SUBMITTED_STIME $stime
    set ::LAST_SUBMITTED_ETIME $etime
    set ::IN_PROGRESS_SET [ setIntervalUnion $::IN_PROGRESS_SET $interval ]
    incr ::currentExistingJobs
  } else {
    logfile "$::RDSFILE_PREFIX $::type $times ($jobAux(duration) seconds) ABANDONED after $::maxRetries tries" red
    set ::job${jobIndex}Aux(retry) $::FALSE
    set ::ERROR_SET [ setIntervalUnion $::ERROR_SET $interval ]
  }

}

;##
;## Check the status of a job and start the next one if needed
;##
proc checkJobStatus { jobIndex } {

  upvar ::job$jobIndex job
  upvar ::job${jobIndex}Aux jobAux

  switch -exact -- $job(status) {
    submitted {}
    running {}
    done { reapSuccessfulJob $jobIndex }
    error { reapFailedJob $jobIndex }
    default {
      cleanupAndExit 1 "checkJobStatus: Unknown status $job(status)"
    }
  }

}

;##
;## Reap all jobs which have finished (ie. not in the running state)
;##
proc reapJobs {} {

  for { set k 0 } { $k < $::maxJobs } { incr k } {
    if { [ info exists ::job$k ] } {
      LJstatus ::job$k
      checkJobStatus $k
    }
  }

}

;##
;## Start a new job in slot "jobIndex" using the time range provided
;##
proc launchJob { jobIndex timesList } {

  foreach { stime etime } $timesList { continue }
  set duration [ expr $etime - $stime ]
  set times $stime-[expr $etime - 1]
  set cmd [ buildCmd $times ]

  #
  # The jobNAux variable holds auxiliary information about the jobs
  # It's fields are
  #   times - the "times" parameter of the job
  #   duration - the duration of the times, in seconds
  #   tries - how many times we have tried to run this job
  #
  set ::job${jobIndex}Aux(times) "$times"
  set ::job${jobIndex}Aux(duration) $duration
  set ::job${jobIndex}Aux(retry) false
  set ::job${jobIndex}Aux(tries) 1

  submitJob job${jobIndex} $cmd
  incr ::currentExistingJobs

  set ::LAST_SUBMITTED_STIME $stime
  set ::LAST_SUBMITTED_ETIME $etime

  # Add this interval to the intervals in progress
  set ::IN_PROGRESS_SET [ setIntervalUnion $::IN_PROGRESS_SET $timesList ]

  # Remove the time interval for this job from availability
  set ::DISKCACHE [ setIntervalDifference $::DISKCACHE $timesList ]
}

;##
;## If a job slot is empty, start a new job or retry the job if it failed
;## in the last iteration
;##
proc startNewJobs {} {

  for { set k 0 } { $k < $::maxJobs } { incr k } {
    if { ! [ info exists ::job$k ] } {
      if { [ info exists ::job${k}Aux ] } {
        set retry [ set ::job${k}Aux(retry) ]
      } else {
        set retry $::FALSE
      }

      if { $retry } {
        retryFailedJob $k
      } else {
        set timesList [ getNextAvailableTime ]
        if { [ llength $timesList ] == 2 } {
          launchJob $k $timesList
        }
      }
    }
  }

}

;##
;## Poll the currently running jobs and update the display
;##
;## This is the main loop that runs in the event loop while we're in the
;## "running" state. It's state is changed by user activity (pressing the
;## Stop or Exit button), errors connecting to LDAS, or running out of data
;## to process.
;##
proc runningLoop {} {

  # First thing - write out the status file
  writeStatusfile

  # For off-site control
  if { [ file exists "sHuTdOwN" ] } {
    Exit
  }

  if { $::STATE == "running" } {

    # incr ::runningCtr # 02/09/05 gam; move after checking if DISKCACHE is empty to prevent initial rate seeming very low.

    if { [ llength $::DISKCACHE ] == 0 } {
      set ::STATE waiting
      set ::TIME_TO_WAIT 0
      after 0 runningLoop
      return
    }    

    incr ::runningCtr

    set ::SBTEXT "Reaping jobs..."
    update idletasks
    reapJobs

    set ::SBTEXT "Starting new jobs..."
    update idletasks
    if { [ catch { startNewJobs } errmsg ] } {
      set msg "Error starting new jobs: \"$errmsg\""
      log $msg
      logfile $msg red
      errorNotify $msg
      set ::STATE error
      set ::STATUS "err"
      set ::TIME_TO_WAIT $::ERR_INTERVAL
      after 0 runningLoop
      return
    }

    set ::SBTEXT "Running ($::percentComplete\% done)"
    updateDisplay
    updateProgressBar
    update idletasks

    after $::UPDATE_INTERVAL runningLoop
    return

  } elseif { $::STATE == "waiting" } {

    set ::SBTEXT "Reaping old jobs..."
    update idletasks
    reapJobs
    #updateDisplay  # 05/26/05; no need to update while waiting
    updateProgressBar

    if { $::currentExistingJobs == 0 && $::TIME_TO_WAIT <= 0 } {
      set ::SBTEXT "Refreshing disk cache..."
      update idletasks
      logfile "Refreshing disk cache"
      if { [ catch { updateDiskCache } errmsg ] } {
        set msg "Error updating cache: \"$errmsg\""
        log $msg
        logfile $msg red
        errorNotify $msg
        set ::STATE error
        set ::STATUS "err"
        set ::TIME_TO_WAIT $::ERR_INTERVAL
        after 0 runningLoop
        return
      }


      if { [ llength $::DISKCACHE ] == 0 } {
        #logfile "No new data in disk cache, waiting [expr $::diskCacheInterval/1000] seconds"
        logfile "No new data in disk cache, waiting $::diskCacheInterval seconds"
        #set ::TIME_TO_WAIT $::diskCacheInterval
        set ::TIME_TO_WAIT [ expr $::diskCacheInterval*1000 ]
        after 0 runningLoop
        return
      } else {
        set ::SBTEXT "Refreshing disk cache...done"
        update idletasks
        logfile "Diskcache updated: $::DISKCACHE"
        logfile "Reduced data     : $::RDS_DISKCACHE"
        set ::STATE running
        after 0 runningLoop
        return
      }

    } else {
      # Continue waiting until all the pending jobs have finished
      # 02/10/05; include time waiting for jobs to finish in runningCtr used to compute overall realtimeRatio.
      if { $::currentExistingJobs > 0 } {
         incr ::runningCtr      
      }
      set ::SBTEXT "No more data, waiting [expr $::TIME_TO_WAIT/1000] seconds"
      set ::TIME_TO_WAIT [ expr $::TIME_TO_WAIT - $::UPDATE_INTERVAL ]
      after $::UPDATE_INTERVAL runningLoop
      return
    }

  } elseif { $::STATE == "error" } {

    set ::SBTEXT "Error submitting job, waiting [expr $::TIME_TO_WAIT/1000] seconds"
    update idletasks
    reapJobs
    updateDisplay
    updateProgressBar
    if { $::TIME_TO_WAIT <= 0 } {
      set ::STATE running
      after 0 runningLoop
      return
    } else {
      set ::TIME_TO_WAIT [ expr $::TIME_TO_WAIT - $::UPDATE_INTERVAL ]
      after $::UPDATE_INTERVAL runningLoop
      return
    }

  } elseif { $::STATE == "stopping" } {

    reapJobs
    set ::SBTEXT "Stopping... ($::currentExistingJobs jobs remaining)"
    update idletasks
    updateDisplay
    updateProgressBar

    if { $::currentExistingJobs > 0 } {
      after $::UPDATE_INTERVAL runningLoop
      return
    } else {
      set ::STATE idle
      set ::SBTEXT Idle
      # Warn monitors that the script is not processing data
      set ::STATUS "war"
      .t.log config -cursor left_ptr
      .bar.set config -state active
      .bar.update config -state active
      .bar.start config -state active
      .bar.stop config -state disabled
      .bar.exit config -state active
      logfile "Stopped"
      return
    }

  } elseif { $::STATE == "exiting" } {

    reapJobs
    set ::SBTEXT "Exiting... ($::currentExistingJobs jobs remaining)"
    update idletasks
    updateDisplay

    if { $::currentExistingJobs > 0 } {
      after $::UPDATE_INTERVAL runningLoop
      return
    } else {
      cleanupAndExit 0 "Exiting"
    }

  } else {
    cleanupAndExit 1 "runningLoop: Unknown state $::STATE"
  }

}

;##
;## Print the current value of all settings to the log window
;##
proc printSettings {} {
  
  clearLog
  log "createRDS Control Utility settings\n"
  log "Run as user                : $::user"
  log "Email notification list    : $::notifyList"
  log "Site to process at         : $::site"
  log "Input frame type(s)        : $::type"
  log "Input IFO for each type    : $::ifoList"
  log "Use framequery syntax?     : $::useFrameQuery"
  log "Output frame IFO(s)        : $::RDSFILE_PREFIX"
  log "Output frame type          : [RDSType $::type $::userType]"
  log "Leave output type as is    : $::leaveUserTypeAsIs"
  log "Exclude existing RDS data? : $::excludeRDS"
  log "Frame file duration (secs) : $::frameLength"
  log "Get duration dynamically?  : $::updateFrameLength"
  log "sec. between cache updates : $::diskCacheInterval"
  log "Channel list file          : $::adcFile"
  log "No. of raw channels out    : $::numAdcChannels"
  log "No. of proc channels out   : $::numProcChannels"
  log "Total no. of channels out  : [ expr $::numAdcChannels + $::numProcChannels ]"
  log "Start time                 : $::startTime ([ tconvert $::startTime ])"
  log "End time                   : $::endTime ([ tconvert $::endTime ])"
  log "Get data from here only    : $::includedirs"
  log "Exclude data from here     : $::excludedirs"
  log "Use job directories?       : $::usejobdirs"
  log "Base output directory      : $::outputdir"
  log "Regex to give md5sum dir   : $::md5sumregexp"
  # log "Use 100000-second subdirs? : $::binByTime"
  log "Use binByTime subdirs?     : $::binByTime"
  log "No. sec. per subdirs?      : $::secPerBin"
  log "Compression type           : $::compressiontype"
  log "Compression level          : $::compressionlevel"
  log "Validate file checksums?   : $::filechecksum"
  log "Validate frame times?      : $::frametimecheck"
  log "Check datavalid flags?     : $::framedatavalid"
  log "Maximum job length         : $::maxJobLength seconds"
  log "Maximum simultaneous jobs  : $::maxJobs"
  log "No. frames per output file : $::framesperfile"
  log "No. sec. per output frame  : $::secperframe"
  log "Align normal output frames?: $::alignFrameStartTimes"
  log "Allow short frames?        : $::allowshortframes"
  log "Allow short for latest seg?: $::allowShortForLatestSeg"
  log "Fill seg only if this old  : $::fillSegmentsIfThisOld"
  log "Generate frame checksums?  : $::generatechecksum"
  log "Fill datavalid aux vec's?  : $::fillmissingdatavalid"
  log ""
  log "Press Start to begin data reduction"
  update idletasks

}

;##
;## Apply the settings made in the Settings dialog window and write them
;## to the resource file
;##
proc SettingsApply {} {

  if { $::siteTmp != $::site \
         || $::typeTmp != $::type \
         || $::userTypeTmp != $::userType } {
    set choice [ tk_messageBox -type okcancel -message "Warning: changing the LDAS system, input frame type or user frame type will reset the record of previously completed data reduction." ]
    if { $choice == "cancel" } {
      return
    } else {
      set ::COMPLETED_RDS {}
      set ::ERROR_SET {}
    }
  }

  set ::startTime $::startTimeTmp
  set ::endTime $::endTimeTmp
  set ::maxJobLength $::maxJobLengthTmp
  set ::maxJobs $::maxJobsTmp
  set ::adcFile $::adcFileTmp
  set ::site $::siteTmp
  set ::user $::userTmp
  set ::type $::typeTmp
  set ::ifoList $::ifoListTmp
  set ::useFrameQuery $::useFrameQueryTmp
  set ::userType $::userTypeTmp
  set ::leaveUserTypeAsIs $::leaveUserTypeAsIsTmp
  set ::notifyList $::notifyListTmp
  set ::excludeRDS $::excludeRDSTmp
  set ::frameLength $::frameLengthTmp
  set ::updateFrameLength $::updateFrameLengthTmp
  set ::diskCacheInterval $::diskCacheIntervalTmp
  set ::includedirs [string trim $::includedirsTmp]
  set ::excludedirs [string trim $::excludedirsTmp]
  set ::usejobdirs $::usejobdirsTmp
  set ::outputdir $::outputdirTmp
  set ::md5sumregexp $::md5sumregexpTmp
  set ::binByTime $::binByTimeTmp
  set ::secPerBin $::secPerBinTmp  
  set ::compressiontype $::compressiontypeTmp
  set ::compressionlevel $::compressionlevelTmp
  set ::filechecksum $::filechecksumTmp
  set ::frametimecheck $::frametimecheckTmp
  set ::framedatavalid $::framedatavalidTmp  
  set ::framesperfile $::framesperfileTmp
  set ::secperframe $::secperframeTmp
  set ::alignFrameStartTimes $::alignFrameStartTimesTmp
  set ::allowshortframes $::allowshortframesTmp
  set ::allowShortForLatestSeg $::allowShortForLatestSegTmp
  set ::fillSegmentsIfThisOld $::fillSegmentsIfThisOldTmp
  set ::generatechecksum $::generatechecksumTmp
  set ::fillmissingdatavalid $::fillmissingdatavalidTmp

  if {[vetCreateRDSParameters]} {
    set choice [ tk_messageBox -type okcancel -message "Warning: some parameters are not valid. Check command line for error messages. Continue to save these to file (note changes already made in memory)?" ]
    if { $choice == "cancel" } {
      set writeRSCFile 0;
    } else {
      set writeRSCFile 1;    
    }
  } else {
    set writeRSCFile 1;
  }
  
  if {$writeRSCFile} {
    # Write new values to file
    set rscFile [ open $::RESOURCE_FILE w ]

    set locTime [ clock format [ clock scan now ] -format %c ]
    puts $rscFile ";## Resource file for createrds.tcl and createrdsGUI.tcl, created $locTime"
    puts $rscFile ";## Last modified on $locTime"
    puts $rscFile ""

    puts $rscFile ";## Start and end time"
    puts $rscFile ";## All data in the interval \[startTime, endTime\) will be processed"
    puts $rscFile "set startTime $::startTime"
    puts $rscFile "set endTime $::endTime"
    puts $rscFile ""

    puts $rscFile ";## Maximum length of each data set, in seconds"
    puts $rscFile "set maxJobLength $::maxJobLength"
    puts $rscFile ""

    puts $rscFile ";## Maximum number of jobs to run at one time"
    puts $rscFile "set maxJobs $::maxJobs"
    puts $rscFile ""

    puts $rscFile ";## File of channels to reduce"
    puts $rscFile "set adcFile $::adcFile"
    puts $rscFile ""

    puts $rscFile ";## LDAS site to run at"
    puts $rscFile "set site $::site"
    puts $rscFile ""

    puts $rscFile ";## User to run as"
    puts $rscFile "set user $::user"
    puts $rscFile ""

    puts $rscFile ";## Email notification list (must be a space-separated"
    puts $rscFile ";## list of email addresses enclosed in double quotes)"
    puts $rscFile "set notifyList \"$::notifyList\""
    puts $rscFile ""

    puts $rscFile ";## Frame type(s) to read data from"
    puts $rscFile "set type \[ list $::type \]"; # 04/20/05 gam
    puts $rscFile ""

    puts $rscFile ";## Input IFO for each type. If this is an empty list then"
    puts $rscFile ";## this gets set based on the channel list.  Otherwise this"
    puts $rscFile ";## must be a list of the same length as the type list above."
    puts $rscFile "set ifoList \[ list $::ifoList \]"
    puts $rscFile ""

    # 04/20/05 gam
    puts $rscFile ";## 1 = TRUE 0 = FALSE. Must be 1 if length of type list > 1."
    puts $rscFile ";## If 1 use framequery syntax with createRDS cmd."
    puts $rscFile "set useFrameQuery $::useFrameQuery";
    puts $rscFile ""

    puts $rscFile ";## Output frame type of reduced data (see leaveUserTypeAsIs below)"
    puts $rscFile "set userType $::userType"
    puts $rscFile ""

    puts $rscFile ";## 1 = TRUE 0 = FALSE; if FALSE and userType does not end in a number a"
    puts $rscFile ";## number is appended to the end of the userType one level higher than"
    puts $rscFile ";## the input type. If the level of the input type cannot be determined"
    puts $rscFile ";## a 1 is appened. However if leaveUserTypeAsIs is TRUE userType is not"
    puts $rscFile ";## change at all, but used exactly as set above."
    puts $rscFile "set leaveUserTypeAsIs $::leaveUserTypeAsIs"
    puts $rscFile ""

    puts $rscFile ";## Exclude any existing data of this user type."
    puts $rscFile ";## If set to \"Y\", the script will not attempt to reduce"
    puts $rscFile ";## data which is already available in reduced form in LDAS"
    puts $rscFile "set excludeRDS $::excludeRDS"
    puts $rscFile ""

    puts $rscFile ";## Duration of input frame files in seconds"
    puts $rscFile "set frameLength $::frameLength"
    puts $rscFile ""

    puts $rscFile ";## 1 = TRUE 0 = FALSE; dynamically update the frameLength when DOWNSAMPLING to adjust segment lengths"
    puts $rscFile "set updateFrameLength $::updateFrameLength"
    puts $rscFile ""

    puts $rscFile ";## seconds to wait between updates of the frame cache"
    puts $rscFile "set diskCacheInterval $::diskCacheInterval"
    puts $rscFile ""
  
    puts $rscFile ";## If includedirs is not an empty list, reduce input data from these directories only:"
    puts $rscFile "set includedirs \[ list $::includedirs \]"
    puts $rscFile ""

    puts $rscFile ";## If excludedirs is not an empty list, do not reduce input data from any of these directories:"
    puts $rscFile "set excludedirs \[ list $::excludedirs \]"
    puts $rscFile ""

    puts $rscFile ";## Option for output to go into job directories rather than the outputdir"
    puts $rscFile ";## The default value is 0 (FALSE)"    
    puts $rscFile "set usejobdirs $::usejobdirs"
    puts $rscFile ""

    puts $rscFile ";## Directory where reduced data will be put"
    puts $rscFile "set outputdir $::outputdir"
    puts $rscFile ""

    puts $rscFile ";## Optional comma delimited sed options to substitute into outputdir to give path to md5sum files."
    puts $rscFile ";## Example: set md5sumregexp \"s,frames,meta/frames,i\" "
    puts $rscFile ";## The default value is an empty string and the md5sumregexp option is not used)"
    puts $rscFile "set md5sumregexp $::md5sumregexp"
    puts $rscFile ""

    #puts $rscFile ";## Use 100000-second subdirectories of the form H-9999?"
    puts $rscFile ";## Use subdirectories of the form, e.g., H-nnnn... where number of n's = 9 - string length of secPerBin?"
    puts $rscFile "set binByTime $::binByTime"
    puts $rscFile ""

    puts $rscFile ";## If binByTime, seconds of data to put into each directory (must be a multiple of 10 between 10 and 10000000; default is 100000)"
    puts $rscFile "set secPerBin $::secPerBin"
    puts $rscFile ""    
    
    puts $rscFile ";## Compression type, raw or gzip"
    puts $rscFile "set compressiontype $::compressiontype"
    puts $rscFile ""

    puts $rscFile ";## Compression level"
    puts $rscFile "set compressionlevel $::compressionlevel"
    puts $rscFile ""
  
    puts $rscFile ";## 1 = TRUE 0 = FALSE; if TRUE frame file checksums are validated before running job" 
    puts $rscFile "set filechecksum $::filechecksum"
    puts $rscFile ""
  
    puts $rscFile ";## 1 = TRUE 0 = FALSE; if TRUE frame times are validated against the frame name before running job"
    puts $rscFile "set frametimecheck $::frametimecheck"
    puts $rscFile ""
  
    puts $rscFile ";## 1 = TRUE 0 = FALSE; if TRUE datavalid flags are checked for every channel before running job"
    puts $rscFile "set framedatavalid $::framedatavalid"
    puts $rscFile ""

    puts $rscFile ";## Number of frames to put in each output file"
    puts $rscFile "set framesperfile $::framesperfile"
    puts $rscFile ""
    
    puts $rscFile ";## Number of seconds of data to put into each frame"
    puts $rscFile "set secperframe $::secperframe"
    puts $rscFile ""

    puts $rscFile ";## 1 = TRUE 0 = FALSE; if TRUE align the normal length output frame files"
    puts $rscFile ";## so that their start times mod framesperfile*secperframe is zero. (Note"
    puts $rscFile ";## that short frames, if allowed, will not be aligned.)"
    puts $rscFile "set alignFrameStartTimes $::alignFrameStartTimes"
    puts $rscFile ""

    puts $rscFile ";## 1 = TRUE 0 = FALSE; if TRUE allow short frame with less than secperframe in a frame"
    puts $rscFile "set allowshortframes $::allowshortframes"
    puts $rscFile ""

    puts $rscFile ";## 1 = TRUE 0 = FALSE; if FALSE never allow short frames when running on the latest data segment"
    puts $rscFile "set allowShortForLatestSeg $::allowShortForLatestSeg"
    puts $rscFile ""

    puts $rscFile ";## If allowshortframes is TRUE, fill in a segment with short frames if its end time is this old (in seconds)."
    puts $rscFile "set fillSegmentsIfThisOld $::fillSegmentsIfThisOld"
    puts $rscFile ""

    puts $rscFile ";## 1 = TRUE 0 = FALSE; if TRUE generate frame checksums on a per frame basis"
    puts $rscFile "set generatechecksum $::generatechecksum"
    puts $rscFile ""

    puts $rscFile ";## 1 = TRUE 0 = FALSE; if TRUE fill any missing channel datavalid aux vec's in the output frames"
    puts $rscFile "set fillmissingdatavalid $::fillmissingdatavalid"
    puts $rscFile ""

    close $rscFile
    
    logfile "Changed settings in resource file"
  
  } else {
    logfile "Changed settings in memory only"
  }

  createChannelList

  destroy .set

  wm title . "createRDS Control Utility ($::RDSFILE_PREFIX-[RDSType $::type $::userType]@$::site)"
  printSettings
  updateProgressBar

  .t.log config -cursor left_ptr
  .bar.set config -state active
  .bar.update config -state active
  .bar.start config -state active
  .bar.exit config -state active
  .bar.abort config -state active

} ;## SettingsApply

;##
;## Cancel any changes made in the Settings dialog
;##
proc SettingsCancel {} {
  destroy .set

  .t.log config -cursor left_ptr
  .bar.set config -state active
  .bar.update config -state active
  .bar.start config -state active
  .bar.exit config -state active
  .bar.abort config -state active
}

;##
;## Open a dialog window for changing settings. Activated by
;## the Settings button
;##
proc Settings {} {
  .t.log config -cursor watch
  .bar.set config -state disabled
  .bar.start config -state disabled
  .bar.update config -state disabled
  .bar.exit config -state disabled
  .bar.abort config -state disabled

  # Prime the variables
  set ::startTimeTmp $::startTime
  set ::endTimeTmp $::endTime
  set ::maxJobLengthTmp $::maxJobLength
  set ::maxJobsTmp $::maxJobs
  set ::adcFileTmp $::adcFile
  set ::siteTmp $::site
  set ::userTmp $::user
  set ::notifyListTmp $::notifyList
  set ::typeTmp $::type
  set ::ifoListTmp $::ifoList
  set ::useFrameQueryTmp $::useFrameQuery
  set ::userTypeTmp $::userType
  set ::leaveUserTypeAsIsTmp $::leaveUserTypeAsIs
  set ::excludeRDSTmp $::excludeRDS
  set ::frameLengthTmp $::frameLength
  set ::updateFrameLengthTmp $::updateFrameLength
  set ::diskCacheIntervalTmp $::diskCacheInterval
  set ::includedirsTmp $::includedirs
  set ::excludedirsTmp $::excludedirs  
  set ::usejobdirsTmp $::usejobdirs
  set ::outputdirTmp $::outputdir
  set ::md5sumregexpTmp $::md5sumregexp
  set ::binByTimeTmp $::binByTime
  set ::secPerBinTmp $::secPerBin
  set ::compressiontypeTmp $::compressiontype
  set ::compressionlevelTmp $::compressionlevel
  set ::filechecksumTmp $::filechecksum
  set ::frametimecheckTmp $::frametimecheck
  set ::framedatavalidTmp $::framedatavalid  
  set ::framesperfileTmp $::framesperfile
  set ::secperframeTmp $::secperframe
  set ::alignFrameStartTimesTmp $::alignFrameStartTimes
  set ::allowshortframesTmp $::allowshortframes
  set ::allowShortForLatestSegTmp $::allowShortForLatestSeg
  set ::fillSegmentsIfThisOldTmp $::fillSegmentsIfThisOld
  set ::generatechecksumTmp $::generatechecksum
  set ::fillmissingdatavalidTmp $::fillmissingdatavalid

  # Create a toplevel window for the dialog
  toplevel .set
  wm title .set "createRDS Settings"

  # Create a frame for the labels and entry widgets
  frame .set.entries -borderwidth 2 -relief groove

  # Create the labels and widgets
  label .set.entries.startTimeLabel -text "Start GPS time:" -anchor w
  entry .set.entries.startTimeEntry -background grey92 -textvariable startTimeTmp
  grid .set.entries.startTimeLabel .set.entries.startTimeEntry -sticky news

  label .set.entries.endTimeLabel -text "End GPS time:" -anchor w
  entry .set.entries.endTimeEntry -background grey92 -textvariable endTimeTmp
  grid .set.entries.endTimeLabel .set.entries.endTimeEntry -sticky news

  label .set.entries.jobLengthLabel -text "Max. Job Length (seconds):" -anchor w
  entry .set.entries.jobLengthEntry -background grey92 -textvariable maxJobLengthTmp
  grid .set.entries.jobLengthLabel .set.entries.jobLengthEntry -sticky news

  label .set.entries.maxJobsLabel -text "Max. Simultaneous Jobs:" -anchor w
  entry .set.entries.maxJobsEntry -background grey92 -textvariable maxJobsTmp
  grid .set.entries.maxJobsLabel .set.entries.maxJobsEntry -sticky news

  label .set.entries.adcFileLabel -text "Channel List File:" -anchor w
  entry .set.entries.adcFileEntry -background grey92 -textvariable adcFileTmp
  grid .set.entries.adcFileLabel .set.entries.adcFileEntry -sticky news

  label .set.entries.siteLabel -text "LDAS System:" -anchor w
  entry .set.entries.siteEntry -background grey92 -textvariable siteTmp
  grid .set.entries.siteLabel .set.entries.siteEntry -sticky news

  label .set.entries.userLabel -text "LDAS User:" -anchor w
  entry .set.entries.userEntry -background grey92 -textvariable userTmp
  grid .set.entries.userLabel .set.entries.userEntry -sticky news

  label .set.entries.notifyListLabel -text "Email Notification List:" -anchor w
  entry .set.entries.notifyListEntry -background grey92 -textvariable notifyListTmp
  grid .set.entries.notifyListLabel .set.entries.notifyListEntry -sticky news

  label .set.entries.typeLabel -text "Input Frame Type(s):" -anchor w
  entry .set.entries.typeEntry -background grey92 -textvariable typeTmp
  grid .set.entries.typeLabel .set.entries.typeEntry -sticky news

  label .set.entries.ifoListLabel -text "Input IFO For Each Type:" -anchor w
  entry .set.entries.ifoListEntry -background grey92 -textvariable ifoListTmp
  grid .set.entries.ifoListLabel .set.entries.ifoListEntry -sticky news

  label .set.entries.useFrameQueryLabel -text "Use framequery syntax (0/1)?:" -anchor w
  entry .set.entries.useFrameQueryEntry -background grey92 -textvariable useFrameQueryTmp
  grid .set.entries.useFrameQueryLabel .set.entries.useFrameQueryEntry -sticky news

  label .set.entries.userTypeLabel -text "Output Frame Type:" -anchor w
  entry .set.entries.userTypeEntry -background grey92 -textvariable userTypeTmp
  grid .set.entries.userTypeLabel .set.entries.userTypeEntry -sticky news

  label .set.entries.leaveUserTypeAsIsLabel -text "Leave output type as is (0/1)?:" -anchor w
  entry .set.entries.leaveUserTypeAsIsEntry -background grey92 -textvariable leaveUserTypeAsIsTmp
  grid .set.entries.leaveUserTypeAsIsLabel .set.entries.leaveUserTypeAsIsEntry -sticky news

  label .set.entries.excludeRDSLabel -text "Exclude Existing RDS Data? (Y/N):" -anchor w
  entry .set.entries.excludeRDSEntry -background grey92 -textvariable excludeRDSTmp
  grid .set.entries.excludeRDSLabel .set.entries.excludeRDSEntry -sticky news

  label .set.entries.frameLengthLabel -text "Frame File Duration (seconds):" -anchor w
  entry .set.entries.frameLengthEntry -background grey92 -textvariable frameLengthTmp
  grid .set.entries.frameLengthLabel .set.entries.frameLengthEntry -sticky news
  
  label .set.entries.updateFrameLengthLabel -text "Get duration dynamically (0/1)?:" -anchor w
  entry .set.entries.updateFrameLengthEntry -background grey92 -textvariable updateFrameLengthTmp
  grid .set.entries.updateFrameLengthLabel .set.entries.updateFrameLengthEntry -sticky news

  label .set.entries.diskCacheIntervalLabel -text "Seconds between cache updates:" -anchor w
  entry .set.entries.diskCacheIntervalEntry -background grey92 -textvariable diskCacheIntervalTmp
  grid .set.entries.diskCacheIntervalLabel .set.entries.diskCacheIntervalEntry -sticky news

  label .set.entries.includedirsLabel -text "Get data from here only:" -anchor w
  entry .set.entries.includedirsEntry -background grey92 -textvariable includedirsTmp
  grid .set.entries.includedirsLabel .set.entries.includedirsEntry -sticky news

  label .set.entries.excludedirsLabel -text "Exclude data from here:" -anchor w
  entry .set.entries.excludedirsEntry -background grey92 -textvariable excludedirsTmp
  grid .set.entries.excludedirsLabel .set.entries.excludedirsEntry -sticky news

  label .set.entries.usejobdirsLabel -text "Use job directories (0/1)?" -anchor w
  entry .set.entries.usejobdirsEntry -background grey92 -textvariable usejobdirsTmp
  grid .set.entries.usejobdirsLabel .set.entries.usejobdirsEntry -sticky news

  label .set.entries.outputdirLabel -text "Base Output Directory:" -anchor w
  entry .set.entries.outputdirEntry -background grey92 -textvariable outputdirTmp
  grid .set.entries.outputdirLabel .set.entries.outputdirEntry -sticky news

  label .set.entries.md5sumregexpLabel -text "Regex to give md5sum dir:" -anchor w
  entry .set.entries.md5sumregexpEntry -background grey92 -textvariable md5sumregexpTmp
  grid .set.entries.md5sumregexpLabel .set.entries.md5sumregexpEntry -sticky news

  #label .set.entries.binByTimeLabel -text "Use 100000-second subdirs (Y/N)?:" -anchor w
  label .set.entries.binByTimeLabel -text "Use binByTime subdirs (Y/N)?:" -anchor w
  entry .set.entries.binByTimeEntry -background grey92 -textvariable binByTimeTmp
  grid .set.entries.binByTimeLabel .set.entries.binByTimeEntry -sticky news

  label .set.entries.secPerBinLabel -text "Number of seconds per subdirs:" -anchor w
  entry .set.entries.secPerBinEntry -background grey92 -textvariable secPerBinTmp
  grid .set.entries.secPerBinLabel .set.entries.secPerBinEntry -sticky news

  label .set.entries.compressiontypeLabel -text "Compression Type (raw/gzip):" -anchor w
  entry .set.entries.compressiontypeEntry -background grey92 -textvariable compressiontypeTmp
  grid .set.entries.compressiontypeLabel .set.entries.compressiontypeEntry -sticky news

  label .set.entries.compressionlevelLabel -text "Compression Level (1-6):" -anchor w
  entry .set.entries.compressionlevelEntry -background grey92 -textvariable compressionlevelTmp
  grid .set.entries.compressionlevelLabel .set.entries.compressionlevelEntry -sticky news

  label .set.entries.filechecksumLabel -text "Validate file checksums (0/1)?:" -anchor w
  entry .set.entries.filechecksumEntry -background grey92 -textvariable filechecksumTmp
  grid .set.entries.filechecksumLabel .set.entries.filechecksumEntry -sticky news

  label .set.entries.frametimecheckLabel -text "Validate frame times (0/1)?:" -anchor w
  entry .set.entries.frametimecheckEntry -background grey92 -textvariable frametimecheckTmp
  grid .set.entries.frametimecheckLabel .set.entries.frametimecheckEntry -sticky news

  label .set.entries.framedatavalidLabel -text "Check datavalid flags (0/1)?:" -anchor w
  entry .set.entries.framedatavalidEntry -background grey92 -textvariable framedatavalidTmp
  grid .set.entries.framedatavalidLabel .set.entries.framedatavalidEntry -sticky news

  label .set.entries.framesperfileLabel -text "Number of frames per output file:" -anchor w
  entry .set.entries.framesperfileEntry -background grey92 -textvariable framesperfileTmp
  grid .set.entries.framesperfileLabel .set.entries.framesperfileEntry -sticky news

  label .set.entries.secperframeLabel -text "Number of seconds per output frame:" -anchor w
  entry .set.entries.secperframeEntry -background grey92 -textvariable secperframeTmp
  grid .set.entries.secperframeLabel .set.entries.secperframeEntry -sticky news

  label .set.entries.alignFrameStartTimesLabel -text "Align normal output frames (0/1)?:" -anchor w
  entry .set.entries.alignFrameStartTimesEntry -background grey92 -textvariable alignFrameStartTimesTmp
  grid .set.entries.alignFrameStartTimesLabel .set.entries.alignFrameStartTimesEntry -sticky news
  
  label .set.entries.allowshortframesLabel -text "Allow short frames (0/1)?:" -anchor w
  entry .set.entries.allowshortframesEntry -background grey92 -textvariable allowshortframesTmp
  grid .set.entries.allowshortframesLabel .set.entries.allowshortframesEntry -sticky news

  label .set.entries.allowShortForLatestSegLabel -text "Allow short for latest seg (0/1)?:" -anchor w
  entry .set.entries.allowShortForLatestSegEntry -background grey92 -textvariable allowShortForLatestSegTmp
  grid .set.entries.allowShortForLatestSegLabel .set.entries.allowShortForLatestSegEntry -sticky news

  label .set.entries.fillSegmentsIfThisOldLabel -text "Fill seg only if this old (seconds):" -anchor w
  entry .set.entries.fillSegmentsIfThisOldEntry -background grey92 -textvariable fillSegmentsIfThisOldTmp
  grid .set.entries.fillSegmentsIfThisOldLabel .set.entries.fillSegmentsIfThisOldEntry -sticky news

  label .set.entries.generatechecksumLabel -text "Generate frame checksums (0/1)?:" -anchor w
  entry .set.entries.generatechecksumEntry -background grey92 -textvariable generatechecksumTmp
  grid .set.entries.generatechecksumLabel .set.entries.generatechecksumEntry -sticky news

  label .set.entries.fillmissingdatavalidLabel -text "Fill missing datavalid aux vec's? (0/1)?:" -anchor w
  entry .set.entries.fillmissingdatavalidEntry -background grey92 -textvariable fillmissingdatavalidTmp
  grid .set.entries.fillmissingdatavalidLabel .set.entries.fillmissingdatavalidEntry -sticky news
        
  # Create a frame for the buttons
  frame .set.buttons

  button .set.buttons.apply -text "Apply" -command SettingsApply
  button .set.buttons.cancel -text "Cancel" -command SettingsCancel
  pack .set.buttons.apply -side left
  pack .set.buttons.cancel -side right

  pack .set.entries
  pack .set.buttons -fill x

  focus .set

} ;## Settings

;##
;## Start reducing data. Activated by the Start button
;##
proc Start {} {

  if { $::STATE == "idle" } {
    .bar.start config -state disabled
    .bar.update config -state disabled
    .bar.set config -state disabled

    logfile "Start requested"

    resetGlobalVariables
    update idletasks

    .bar.stop config -state active

    set ::STATE running
    after 0 runningLoop
  } else {
    cleanupAndExit 1 "Start: should not be able to start in state \"$::STATE\""
  }

}

;##
;## Change the state variable to "stopping" so that the event-driven
;## loop will wait for all pending jobs to complete, then go into the
;## "idle" state
;##
proc Stop {} {
  .t.log config -cursor watch
  .bar.stop config -state disabled
  .bar.start config -state disabled
  .bar.exit config -state disabled

  logfile "Stop requested"

  set ::STATE stopping
  set ::SBTEXT "Stopping... ($::currentExistingJobs jobs remaining)"

  updateDisplay
}

;##
;## Change the state variable to "exiting" so that the event-driven
;## loop will wait for all pending jobs to complete, then exit
;##
proc Exit {} {
  if { $::STATE == "running" } {
    .t.log config -cursor watch
    .bar.stop config -state disabled
    .bar.start config -state disabled
    .bar.exit config -state disabled
    
    logfile "Exit requested"
    
    set ::STATE exiting
    set ::SBTEXT "Exiting... ($::currentExistingJobs jobs remaining)"
    
    updateDisplay
  } else {
    cleanupAndExit 0 "Exiting"
  }
}

;##
;## Abort immediately, without waiting for jobs to complete
;##
proc Abort {} {
  cleanupAndExit 0 "Aborting"
}

;##
;## This proc disables keystrokes and mouse clicks in a text window,
;## so that its contents can't be changed. Very useful for allowing
;## text to be cut-and-pasted without allowing the user to change the
;## contents of the widget (message widgets don't allow cut-and-paste).
;##
proc disableKeys { textWidget } {

  # Disable all key sequences for text widget, except
  # the cursor navigation keys (regardless of the state ctrl/shift/etc.)
  # and Ctrl-C (Copy to Clipboard).
  bind $textWidget <KeyPress> {
    switch -- %K {
      "Up" -
      "Left" -
      "Right" -
      "Down" -
      "Next" -
      "Prior" -
      "Home" -
      "End" {
      }

      "c" -
      "C" {
        if {(%s & 0x04) == 0} {
          break
        }
      }

      default {
        break
      }
    }
  }

  # Addendum: also a good idea disable the cut and paste events.
  bind $textWidget <<Paste>> "break"
  bind $textWidget <<Cut>> "break"

  # Disable the cursor appearing on mouseclicks in the widget
  bind $textWidget <ButtonPress> "break"

} ;## disableKeys

;##
;## Manually update the diskcache information.
;##
proc Update {} {
  .t.log config -cursor watch
  .bar.set config -state disabled
  .bar.chans config -state disabled
  .bar.update config -state disabled
  .bar.start config -state disabled

  set ::SBTEXT "Refreshing disk cache..."
  update idletasks
  logfile "Manually updating disk cache"
  if { [ catch { updateDiskCache } errmsg ] } {
    set msg "Error updating cache: \"$errmsg\""
    log $msg
    logfile $msg red
    ;## Don't bother emailing, since this is an interactive action
  }

  if { [ llength $::DISKCACHE ] == 0 } {
    #logfile "No new data in disk cache, waiting [expr $::diskCacheInterval/1000] seconds"
    logfile "No new data in disk cache, waiting $::diskCacheInterval seconds"
  } else {
    #logfile "Diskcache updated: $::DISKCACHE"
    #logfile "Reduced data     : $::RDS_DISKCACHE"
    logfile "The disk cache has been updated"
  }

  .t.log config -cursor left_ptr
  .bar.set config -state active
  .bar.chans config -state active
  .bar.update config -state active
  .bar.start config -state active

  set ::SBTEXT "Idle"
  updateProgressBar
  update idletasks
}

;##
;## Open a window displaying the contents of the channel list file
;##
proc Channels {} {
  .bar.chans config -state disabled

  toplevel .chans
  wm title .chans "Channel List"

  frame .chans.t
  text .chans.t.text -background grey92 -width 80 -height 40 \
    -borderwidth 1 -relief sunken -setgrid true \
    -yscrollcommand { .chans.t.scroll set }
  
  scrollbar .chans.t.scroll -command { .chans.t.text yview }
  pack .chans.t.text -side left -fill both -expand true
  pack .chans.t.scroll -side right -fill y
  pack .chans.t -side top
  
  disableKeys .chans.t.text
  
  frame .chans.bar
  button .chans.bar.close -text "Close" -command { destroy .chans; .bar.chans config -state active }
  pack .chans.bar.close
  pack .chans.bar
  
  set fid [ open $::adcFile ]
  .chans.t.text insert end [ read $fid ]
  close $fid
}

;##
;## Open a window displaying the help file
;##
proc Help {} {
  .bar.help config -state disabled
  toplevel .h
  wm title .h "Help"

  frame .h.t
  text .h.t.text -background grey92 -width 80 -height 40 \
    -borderwidth 1 -relief sunken -setgrid true \
    -yscrollcommand { .h.t.scroll set }
  
  scrollbar .h.t.scroll -command { .h.t.text yview }
  pack .h.t.text -side left -fill both -expand true
  pack .h.t.scroll -side right -fill y
  pack .h.t -side top
  
  disableKeys .h.t.text
  
  frame .h.bar
  button .h.bar.close -text "Close" -command { destroy .h; .bar.help config -state active }
  pack .h.bar.close
  pack .h.bar
  
  set fid [ open "README.createrdsAndcreaterdsGUI" ]
  .h.t.text insert end [ read $fid ]
  close $fid
}

;##
;## Open a window displaying the last N lines of the log file
;##
proc Log { N } {
  .bar.log config -state disabled
  toplevel .l
  wm title .l "logs/createrds.log (last $N lines)"

  frame .l.t
  text .l.t.text -background grey92 -width 80 -height $N \
    -borderwidth 1 -relief sunken -setgrid true \
    -yscrollcommand { .l.t.scroll set }
  
  scrollbar .l.t.scroll -command { .l.t.text yview }
  pack .l.t.text -side left -fill both -expand true
  pack .l.t.scroll -side right -fill y
  pack .l.t -side top
  
  disableKeys .l.t.text
  
  frame .l.bar
  button .l.bar.close -text "Close" -command { destroy .l; .bar.log config -state active }
  pack .l.bar.close
  pack .l.bar
  
  catch { exec tail -n$N "logs/createrds.log" } result
  .l.t.text insert end $result
}

;##
;## Set up the main window of the application
;##
proc setupWindow {} {

  wm title . "createRDS Control Utility ($::RDSFILE_PREFIX-[RDSType $::type $::userType]@$::site)"
  wm protocol . WM_DELETE_WINDOW Abort

  frame .prog -borderwidth 2 -relief groove
  frame .prog.leg
  label .prog.leg.msg -text "Data Reduction Progress: "

  frame .prog.leg.raw -borderwidth 1 -relief sunken -background orange \
    -height 13 -width 13
  label .prog.leg.rawmsg -text "Raw data "

  frame .prog.leg.rds -borderwidth 1 -relief sunken -background yellow \
    -height 13 -width 13
  label .prog.leg.rdsmsg -text "Excluded RDS data "

  frame .prog.leg.comp -borderwidth 1 -relief sunken -background green \
    -height 13 -width 13
  label .prog.leg.compmsg -text "New RDS data "

  frame .prog.leg.prog -borderwidth 1 -relief sunken -background blue \
    -height 13 -width 13
  label .prog.leg.progmsg -text "In progress "

  frame .prog.leg.err -borderwidth 1 -relief sunken -background red \
    -height 13 -width 13
  label .prog.leg.errmsg -text "Error"

  pack .prog.leg.msg \
    .prog.leg.raw .prog.leg.rawmsg \
    .prog.leg.rds .prog.leg.rdsmsg \
    .prog.leg.comp .prog.leg.compmsg \
    .prog.leg.prog .prog.leg.progmsg \
    .prog.leg.err .prog.leg.errmsg \
    -side left
  
  pack .prog.leg -anchor w

  frame .prog.c
  canvas .prog.c.can -height 12 -background grey92 -borderwidth 1 -relief sunken
  pack .prog.c.can -fill x
  pack .prog.c -fill x

  # Real-time ratio sub-frame
  frame .prog.ratio
  frame .prog.ratio.status
  frame .prog.ratio.status.alarm -background grey92 -height 20 -width 40 -borderwidth 3 -relief raised
  label .prog.ratio.status.label -textvariable ::realtimeRatioMsg
  pack .prog.ratio.status.label -side left -anchor w
  pack .prog.ratio.status.alarm -side right -anchor e
  pack .prog.ratio.status -fill x
  pack .prog.ratio -fill x

  pack .prog -fill x

  # Submission status frame
  frame .sub -borderwidth 2 -relief groove
  frame .sub.status
  frame .sub.status.alarm -background grey92 -height 20 -width 40 -borderwidth 3 -relief raised
  label .sub.status.label -text "Last Job Submission Response:"
  pack .sub.status.label -side left -anchor w
  pack .sub.status.alarm -side right -anchor e
  pack .sub.status -fill x

  frame .sub.resp
  text .sub.resp.text -background grey92 -height 5 -borderwidth 1 -relief sunken
  pack .sub.resp.text -fill x
  pack .sub.resp -fill x
  pack .sub -fill x
  disableKeys .sub.resp.text

  # Job status frame
  frame .job -borderwidth 2 -relief groove
  frame .job.status
  frame .job.status.alarm -background grey92 -height 20 -width 40 -borderwidth 3 -relief raised
  label .job.status.label -text "Last Job Completion Response:"
  pack .job.status.label -side left -anchor w
  pack .job.status.alarm -side right -anchor e
  pack .job.status -fill x

  frame .job.resp
  text .job.resp.text -background grey92 -height 5 -borderwidth 1 -relief sunken
  pack .job.resp.text -fill x
  pack .job.resp -fill x
  pack .job -fill x
  disableKeys .job.resp.text

  # Job monitor frame
  frame .t -borderwidth 2 -relief groove
  label .t.label -text "Job Monitor:"

  text .t.log -background grey92 -width 100 -height 40 \
              -borderwidth 1 -relief sunken -setgrid true \
              -yscrollcommand { .t.scroll set }

  scrollbar .t.scroll -command { .t.log yview }
  pack .t.label -side top -anchor w
  pack .t.scroll -side right -fill y
  pack .t.log -side left -fill both -expand true
  pack .t -fill both -expand true

  disableKeys .t.log

  # Make the cursor an arrow
  .t.log config -cursor left_ptr

  # Control buttons
  frame .bar -borderwidth 1 -relief sunken

  button .bar.set -text "Settings" -command Settings
  button .bar.chans -text "Channels" -command Channels
  button .bar.update -text "Refresh Diskcache" -command Update
  button .bar.log -text "Log" -command { Log 25 }
  button .bar.start -text "Start" -command Start
  button .bar.stop -text "Stop" -state disabled -command Stop
  button .bar.exit -text "Exit" -command Exit
  button .bar.abort -text "Abort" -command Abort
  button .bar.help -text "Help" -command Help
  pack .bar.set .bar.chans .bar.update .bar.log .bar.start .bar.stop .bar.exit .bar.abort .bar.help -side left -fill x -expand true

  pack .bar -anchor w -fill x

  # Status bar
  frame .status
  label .status.label -textvariable SBTEXT -anchor w
  grid .status.label -sticky news
  pack .status -anchor w

  update
} ;## setupWindow

;############################################################################
;##
;## Main
;##
;############################################################################

package require LDASJob
package require tconvert
source sets.tcl

;## Setup globals and default values
initializeGlobals
setCreateRDSDefaults

;## Read resource file, which may override defaults
if { [ file exists ./$::RESOURCE_FILE ] } {
  if { [ catch { source ./$::RESOURCE_FILE } resultSourceRscFile ] } {
      puts stderr " File $::RESOURCE_FILE is not valid; sourcing this file resulted in the error:\n $resultSourceRscFile \n Please fix this file and try again."
      file delete "createrds.status"
      exit 1
  }
  if {[vetCreateRDSParameters]} {
     exit 1; # vet parameters set in the resource file.
  }
}

;## Look for .ldaspw file
if { ! [ file exists ~/.ldaspw ] } {
  puts stderr "No password file found (~/.ldaspw) - please run ldaspw"
  exit 1
}

;## Look for command line arguments
if { [ llength $argv ] > 1 } {
  cleanupAndExit 1 "Invalid number of arguments.\nUsage: createrdsGUI.tcl \[ --start \]"
} else {
  foreach arg $argv {
    if { $arg == "--start" } {
      set ::AUTO_START $::TRUE
    } else {
      cleanupAndExit 1 "$arg: invalid argument.\nUsage: createrdsGUI.tcl \[ --start \]"
    }
  }
}

;##
;## See if a lock file exists, and create it if it doesn't
;##
if { [ catch {
         set locklist [ glob createrds.*.lock ]
         puts stderr "Lockfile $locklist exists! If no instance of createrds.tcl or createrdsGUI.tcl is running with this PID, please delete the lock file and try again"
         exit 1
} ] } {
  set PID [ pid ]
  set ::LOCKFILE createrds.${PID}.lock
  set lockfid [ open $::LOCKFILE w ]
  close $lockfid
  unset lockfid
  exec chmod ug+w $::LOCKFILE   ;## Needed so that the CGI scripts can delete
}

resetGlobalVariables
createChannelList
setupWindow
updateProgressBar
printSettings

writeStatusfile

if { $::AUTO_START } {
  Start
}

;## else, enter the event loop...
