#!/bin/sh
# \
exec tclsh "$0" ${1+"$@"}	
# delete user output large than 1MB

set auto_path "/ldas/lib $auto_path"

;## seconds e.g. 2 days = 172800
;## 1 day is 86400
;## 1 year is 31536000

set limit [ lindex $argv 0 ]

set curtime [ clock seconds ]
set limit [ lindex $argv 0 ]
if	{ !$argc } {
	puts "please supply a limit in seconds"
}

set curlimit [ expr $curtime - $limit ]
puts "purge log files older than $curlimit [ clock format $curlimit -format "%m-%d-%Y %H:%M:%S" ]"
set cwd [ pwd ]

cd /ldas_outgoing/logs/archive
set dirs [ list jobstats_archive memusage nodeusage beowulf shutdown_status ]

proc cleanup {} {

	foreach dir $::dirs {
		foreach file [ glob -nocomplain $dir/* ] {
			set modtime [  file mtime $file ]
			if	{ $modtime < $::curlimit } {
				puts "deleting $file [ clock format $modtime -format "%m-%d-%Y %H:%M:%S" ]"
				catch { file delete $file }
    		}
		}
	}
	# after 3600000 cleanup
}

cleanup
;## cleanup dmt

source /ldas_usr/ldas/test/bin/AllTest.rsc
;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

if	{ ! [ string equal [ pwd ] $::DMTDIR ] } {
	cd $::DMTDIR 
}
foreach file [ glob -nocomplain ./dmt.log.*] {
	set modtime [  file mtime $file ]
	if	{ $modtime < $::curlimit } {
		puts "deleting $file [ clock format $modtime -format "%m-%d-%Y %H:%M:%S" ]"
		catch { file delete $file }
    }
}
	
cd $cwd
