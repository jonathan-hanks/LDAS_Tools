#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

source /ldas_usr/ldas/test/bin/AllTest.rsc

;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

foreach dir [ list $::LOOPDIR $::DMTDIR $::RDSDIR $::PUTSTANDALONE_DESTDIR $::MISCDIR ] {
	if	{ ! [ file exist $dir ] } {
		file mkdir $dir 
	}
	catch { exec ln -f AllTest.rsc $dir/AllTest.rsc } 
}

catch {exec /ldas/bin/ssh-agent-mgr --agent-file=/ldas_outgoing/managerAPI/.ldas-agent --shell=tclsh check} err
eval $err
if { ! [ info exists ::env(SSH_AUTH_SOCK) ]} {
    puts $err
    puts "need to setup env(SSH_AUTH_SOCK) for this test"
    exit
}

;## specify location of tclglobus and globus if not a default
set ::env(TCLGLOBUS_DIR) /ldcg/lib

if	{ [ info exist ::env(LD_LIBRARY_PATH) ] } {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH)" 
} else {
	set ::env(LD_LIBRARY_PATH) "${::GLOBUS_LOCATION}/lib:/ldcg/lib" 
}

proc clean_putStandAlone {} {
	
	;## remove old putstandalone data
	set curtime [ clock seconds ]
	;## purge any log over 1 hours
	set patlist [ list process_1m process_2m ] 
	foreach dir [ list stochastic inspiral tfclusters knownpulsardemod slope stackslide ] {
		foreach pat $patlist {
			catch { eval file delete [ glob -nocomplain $::PUTSTANDALONE_DIR/${dir}/${pat}*.ilwd ] } 
		}
	}
}

set ::env(PATH) "/ldcg/bin/64:/ldcg/bin64:/ldcg/bin:/ldas/bin:/ldas_usr/ldas/test/bin:$::env(PATH)"

clean_putStandAlone
catch { file delete -force cache.begin cache.end }

set basiccmd "/usr/bin/env PATH=$::env(PATH) LD_LIBRARY_PATH=$::env(LD_LIBRARY_PATH) "
;## needs to run in background 

cd $::LOOPDIR
catch { eval exec "$basiccmd pipeloop-all.all $::SITE &" } data
puts "data/put Standalone loops $data"

after 10000
cd $::DMTDIR

catch { eval exec "$basiccmd rundmt.tcl $::SITE >& $::DMTDIR/rundmt.log &" } data
catch { exec cat $::DMTDIR/rundmt.log } data
puts "dmt $data"

catch { exec startrds.tcl } err
puts "createRDS $err"

cd $::WORKDIR
catch { exec uname -n } hostname
catch { exec grep DISKCACHE_API_HOST /ldas_outgoing/LDASapi.rsc } data
regexp {set ::DISKCACHE_API_HOST\s+(\S+)} $data -> ::DISKCACHE_API_HOST

;## remove shutdown file for dcmangle
if	{ ! [ string equal $hostname $::DISKCACHE_API_HOST ] } {
	set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
        \"rm -f $::DCMANGLE_SHUTDOWN \""
	catch { eval $cmd } err
	puts "$cmd\n$err"
} else {
	catch { file delete $::DCMANGLE_SHUTDOWN }
}

cd $::MISCDIR
;## start getFrameCache loop
catch { exec loopcmd.test getFrameCache.cmd >& getFrameCache.log & } err 
puts "getFrameCache looping started: $err"
catch { exec getFrameData.test getFrameData.cmd >& getFrameData.log  & } err 
puts "getFrameData looping started: $err"
