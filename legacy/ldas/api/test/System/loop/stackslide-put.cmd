putStandAlone
    -subject {putStandAlone stackslide loop test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
	-multidimdatatarget ligolw
    -wrapperdata {
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/stackslide/process_1.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/stackslide/output_1.ilwd 
        file:/ldas_outgoing/jobs/LDASTest/putStandAlone/stackslide/process_2.ilwd 
    }
    -database {ldas_tst}
