#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

source /ldas_usr/ldas/test/bin/AllTest.rsc
;## override some defaults
if	{ [ file exist AllTest.rsc ] } {
	source AllTest.rsc
} 

if	{ ! [ info exist ::FRAME_TOPDIR ] } {
	set ::FRAMEDIR /scratch/test/system/$::SITE/S3_RDS/[ string toupper $type ]
} else {
	set ::FRAMEDIR $::FRAME_TOPDIR
}

foreach dir [ lsort [ glob -nocomplain $::FRAMEDIR/* ] ] {
	set files [ glob -nocomplain $dir/*.gwf ] 
	if	{ [ llength $files ] } {
		set cmd "exec ls -l $files | wc -l"
		catch { eval $cmd } data
		puts "$dir: $data files"
	} else {
		puts "$dir: 0 files"
	}
}
