dataPipeline 
    -subject         {geo data in frames DEMO} 
    -dynlib          /ldas/lib/wrapperAPI/libio.so 
    -filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0)) 
    -datacondtarget  wrapper 
    -metadatatarget  datacond 
    -multidimdatatarget  ligolw 
    -database        ldas_tst 
    -state           {} 
    -np 3 
    -framequery {       
        { R G {} {714096132-714096251} Adc(G1:DER_H_QUALITY,G1:DER_H_HP-EP)} 
    } 
    -responsefiles {} 
    -aliases { 
        gw = G1\:DER_H_QUALITY::AdcData; 
    } 
    -algorithms { 
        rgw = resample(gw, 1, 8); 
        output(rgw,_,_,,resampled gw timeseries);     
    }
