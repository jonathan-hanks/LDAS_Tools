#!/bin/sh
# \
. /ldas/libexec/setup_tclsh.sh 
## The next line tells sh to execute the script using tclshexe \
# \
exec  ${TCLSH} "$0" ${1+"$@"}

set ::env(LD_LIBRARY_PATH) /ldas/lib:/opt/IBM/db2/V8.1/lib:/ldcg/lib

proc execdb2cmd {} {
	uplevel {
		set cmd "exec $::db2 $cmd"
		catch { eval $cmd } data
		;## dont record password
		if	{ [ regexp {(connect to \S+)\s+} $cmd -> cmd ] } {
			;
		}
		puts "cmd='$cmd'\nresult='$data'"
	}
}

proc printcnts {} {
	uplevel {
		if	{ [ catch {
			regexp -- {\n[\s\t]+(\d+)} $data -> rows
			incr ::totalrows($table) $rows
			puts "Found $rows rows for table $table"
		} err ] } {
			puts "$err,data=$data"
		}
	}
}

set TOPDIR /ldas_outgoing
set LDAS /ldas
if { [ file exist /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc ] } {
	source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc
} else {
	source $::LDAS/bin/LDASdb2utils.rsc
}
# puts "db2=$db2"

set auto_path "$::LDAS/lib $auto_path"
set API test
# package require generic
source /ldas/lib/genericAPI/b64.tcl
source [ file join $::TOPDIR metadataAPI LDASdsnames.ini ]

set dsname [ lindex $argv 0 ]
if	{ $argc == 2 } {
	set cleanup [ lindex $argv 1 ]
} else {
	set cleanup 0
}
puts "cleanup $cleanup"

set ::expected(process) 9
set ::expected(gds_trigger) 34450
set ::expected(segment) 489

set ::totalrows(process) 0
set ::totalrows(gds_trigger) 0
set ::totalrows(segment) 0

if  { $argc == 3 } {
    set delete [ lindex $argv 2 ]
} else {
	set delete 1
}

foreach { dbuser dbpasswd } [ set ::${dsname}(login) ] { break }
;## this is needed to retrieve error messages

set ::env(DB2INSTANCE) $dbuser

set cmd "connect to $dsname user $dbuser using [ decode64 $dbpasswd ]"
execdb2cmd

source $::DB2SCRIPTS/dbtables.tcl

foreach program { PSLmon glitchMon BitTest LockLoss SegGener } {
	set childtables [ list gds_trigger segment ]

	# first select the process
	set cmd "select process_id from process where program='$program'"
	execdb2cmd

	set data [ split $data \n ]
	set process_id ""
	set processes [ list ]

	foreach line $data {
		if	{ [ regexp {^(x'\d+')$} $line -> process_id ] } {
			lappend processes $process_id
		}
	}
	set numprocesses [ llength $processes ]
	set processes [ join $processes , ]
	incr ::totalrows(process) $numprocesses
	if	{ ! $numprocesses } {
		puts "No rows found in process table for program=$program"
		continue
	} else {
		puts "$program processes $processes"
	}

	foreach table $childtables {
    	set cmd "select count(*) from $table where process_id in ($processes)"
		execdb2cmd
		printcnts
		if  { $delete && $rows } {
			for { set i 0 } { $i < 10000 } { incr i 1 } {
				set cmd "delete from $table where process_id in ($processes)"
				set stime [ clock format [ clock seconds ] -format "%x %X %Z" ]
				execdb2cmd
				if	{ [ regexp -nocase {command completed successfully|No row was found} $data ] } {
					set cmd commit
					execdb2cmd
					set etime [ clock format [ clock seconds ] -format "%x %X %Z" ]
					puts "$stime-$etime: deleted from $table"
					break
				} else {
					set curtime [ clock format [ clock seconds ] -format "%x %X %Z" ]
					puts "$curtime retry $i times cmd '$cmd'"
					after 2000
				}
			}
			set cmd "select count(*) from $table where process_id in ($processes)"
			execdb2cmd
			printcnts
		}	
	} 

	;## delete process
	if  { $delete && $numprocesses } {
		set table process	
		set cmd "select count(*) from process where program='$program'"
		execdb2cmd
		set stime [ clock format [ clock seconds ] -format "%x %X %Z" ]	
		for { set i 0 } { $i < 20 } { incr i 1 } {
			set cmd "delete from process where program='$program'"
			execdb2cmd
			if	{ [ regexp {command completed successfully|No row was found} $data ] } {
				set cmd "commit"
				execdb2cmd
				set etime [ clock format [ clock seconds ] -format "%x %X %Z" ]
				puts "$stime-$etime: deleted from process $program"
				break 
			} else {
				set curtime [ clock format [ clock seconds ] -format "%x %X %Z" ]
				puts "$curtime retry $i times cmd '$cmd'"
			}
		}

		;## verify the deletion
		set cmd "select count(*) from process where program='$program'"
		execdb2cmd
		printcnts
	}
	;## end of all programs
}

;## compare results
set err ""

#if	{ !$cleanup } {
	foreach table [ concat process $childtables ] {
		if 	{ $::expected($table) != $::totalrows($table) } {
			append err "table $table mismatch: expected $::expected($table), deleted $::totalrows($table)\n"
			puts $err
			set error 1
		} else {
			puts "$table expected $::expected($table), found $::totalrows($table)"
		}
	}
#} 
set endtime [ clock format [ clock seconds ] -format "%x %X %Z" ]
if	{ [ string length $err ] } {
	puts "DMT error at $endtime: [ string trim $err \n ]"
} else {
	puts "DMT passed at $endtime"
}
set cmd terminate
execdb2cmd




