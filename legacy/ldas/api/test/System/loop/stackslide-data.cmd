
    dataStandAlone
        -subject { sftRequest }
        -returnprotocol {http://}
        -outputformat {ilwd binary}
        -database {ldas_tst}
        -framequery { { SFT_H2_60_P_1 Z {} {730522208-730524007} Proc(0!1282.0!3.0FREQ!) } }
        -responsefiles { file:/ldas_usr/ldas/test/bin/earth00-04.ilwd,pass file:/ldas_usr/ldas/test/bin/sun00-04.ilwd,pass }
        -aliases {sft}
        -algorithms { output(_chN,,,_chN,_chN data); }

        -concatenate 0 -allowgaps 1 -autoexpand 1

