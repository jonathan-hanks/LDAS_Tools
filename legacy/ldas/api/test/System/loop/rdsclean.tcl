#! /bin/sh
## The next line tells sh to execute the script using tclshexe \
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

# script for rds output

## rdsclean.tcl delete - deletes frames from top level
## rdsclean.tcl <framedir> - delete frames at this framedir

source /ldas_usr/ldas/test/bin/AllTest.rsc
;## override some defaults
if      { [ file exist AllTest.rsc ] } {
        source AllTest.rsc
}
if	{ ! [ info exist ::FRAMEDIR ] } {
	set ::FRAMEDIR $::FRAME_TOPDIR
}

;## /scratch is now mounted from dev, not dataserver
catch {exec /ldas/bin/ssh-agent-mgr --agent-file=/ldas_outgoing/managerAPI/.ldas-agent --shell=tclsh check} err
eval $err
if { ! [ info exists ::env(SSH_AUTH_SOCK) ]} {
    puts $err
    puts "need to setup env(SSH_AUTH_SOCK) for this test"
    exit
}

;## save inode info before deleting
proc record_inode { dir } {	
	#set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
		\"ls -ilR  $::FRAMEDIR\""
	#catch { eval $cmd } data
 	catch { exec /bin/ls -ilR $dir } data 
	puts "$dir data $data"
    set fd [ open $::env(HOME)/inodelist a+ ]
	set time [ clock format [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]
    puts $fd "$time\n$data"
    close $fd
}


proc check_deleted_files { files { round 0 } } {
	
	if	{ ! [ llength $files ] } {
		set ::done 1
		return
	}
	;## now make sure the files are deleted 
	set tempfiles [ list ]
	puts "check_deleted_files round $round, files $files"
	foreach file $files {
		if	{ [ catch { 
			set fd [ open $file r ]
			lappend tempfiles $file 
			close $fd 
		} err ] } {
			catch { close $fd }
			continue
		}
	}
	if	{ [ string length $tempfiles ] && ( $round < 5 ) } {
		puts "files $tempfiles still exist"
		# eval file delete -force $tempfiles		
		incr round 
		after 0 [ list check_deleted_files $tempfiles $round ]
	} else {
		set ::done 1
	}
}


;## cleanup on dataserver local file system
proc cleanup {} {

	# record_inode $::FRAMEDIR
	
	;## take a list of files in the frame directory
	if	{ ![ file exist ${::FRAMEDIR} ] } {
		puts "$::FRAMEDIR does not exist"
		after 0 [ list set ::done 1 ]
		return
	}
	set ::tmpdir ${::FRAMEDIR}[pid]
	catch { exec mv -f $::FRAMEDIR $::tmpdir } err
	puts $err
	catch { exec rm -rf $::tmpdir } err
	puts $err
	set files [ glob -nocomplain ${::tmpdir}/* ]
	set count [ llength $files ]
	if	{ $count } {
		puts "after rm -rf files '$files' [ llength $files ]"
	}
	# record_inode $::FRAMEDIR
	if	{ [ llength $files ] } {
		check_deleted_files $files 
	} else {
		after 0 [ list set ::done 1 ]
	}
	
}	

;## delete file twice due to dev file problems
proc cleanup_logs {} {
	set curtime [ clock seconds ]
	foreach file [ glob -nocomplain rdsloop_*.log* rdsloop_*.dat rdsloop_*.out* ] {
        regexp {\d+} $file -> filetime
		catch {
        	set mtime [ file mtime $file ]
        	if	{ [ expr $curtime - $mtime ] > 1800 } {
				catch { exec rm -f $file }
			}
        } 
	
	}
}

set ::delete 0
if	{ $argc >= 1 } {
	set args1 [ lindex $argv 0 ]	
	if	{ [ string equal delete $args1 ] } {
		set ::FRAMEDIR $::FRAME_TOPDIR
		set ::delete 1
	} else {
		set ::FRAMEDIR $args1
		set ::delete 0
	}
}

catch { unset ::done }
after 10000 [  list set ::done 1 ]
cleanup_logs
cleanup
vwait ::done

if	{ $::delete } {
	killer dords
	killer rdsloop
}
