dataPipeline
    -subject         {INSPIRAL DEMO}

    -dynlib          /dso-test/libldasinspiral.so
    -filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))

    -datacondtarget  wrapper
    -metadatatarget  datacond
    -multidimdatatarget  ligolw
    -database        ldas_tst
    -state           {}
    -np 3

    -framequery {
        { R H {} {730522210-730522721} Adc(H2:LSC-AS_Q) }
    }
    -responsefiles {
        file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
    }
    -aliases {
        gw = H2\:LSC-AS_Q::AdcData;
    }
    -algorithms {
        rgw = resample(gw, 1, 8);
        p = psd( rgw, 262144 );
        output(rgw,_,_,:primary,resampled gw timeseries);
        output(p,_,_,spectrum:psd,spectrum data);
    }

