#!/bin/sh
# \
exec /ldcg/bin/tclsh "$0" ${1+"$@"}
#exec tclsh "$0" ${1+"$@"}

set site [ lindex $::argv 0 ]

source ./joblist.tcl
foreach port [ concat $meta_data $pipelines ] {
    set sid [ socket [ info hostname ] $port ]
    puts $sid "site $site"
    close $sid
}
