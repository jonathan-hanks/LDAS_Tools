putStandAlone
    -subject {putStandAlone test}
    -metadataapi {metadata}
    -returnprotocol {http://result}
    -wrapperdata {
        http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/test/data/putStandAlone/process_1.ilwd 
        http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/test/data/putStandAlone/output_1.ilwd 
        http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/test/data/putStandAlone/process_2.ilwd 
    }
    -database {ldas_tst}
