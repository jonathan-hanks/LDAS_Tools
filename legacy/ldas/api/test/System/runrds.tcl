#! /ldcg/bin/tclsh

set ::test2run [ list TestOneToOne TestMultiFrame TestConcatFrame TestMultiConcatFrame \
	TestOneToOneS3_L TestMultiFrameS3_L TestConcatFrameS3_L TestMultiConcatFrameS3_L \
	TestMultiFrameE11 TestConcatFrameE11 TestMultiConcatFrameE11 ]
	
set idx 0

foreach test $::test2run {
	catch { exec ./RDSVerify.tcl -l 1 -v 0 -delay 10  \
		-outputdir /scratch/frames/RDSVerify$idx \
		--test [ lindex $::test2run $idx ] >& RDSVerify.log${idx} & } err
	puts "$idx $err"
	incr idx 1
}
