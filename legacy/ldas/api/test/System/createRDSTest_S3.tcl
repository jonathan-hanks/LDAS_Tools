#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
# 
#./createRDSTest.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*ndas*"
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::createRDSTest_S3::test {

	##-------------------------------------------------------------------
    	## Bring commonly used items into the local namespace
    	##-------------------------------------------------------------------
    	namespace import ::tcltest::*
    	namespace import ::QA::Debug::*
    	namespace import ::QA::TclTest::SubmitJob
    	namespace import ::QA::URL::List
	
	set BINDIR /ldas_usr/ldas/test/bin
	set pipes [ list ]
	set framedir /scratch/test/system/$::SITE/S3_RDS
	
	set basiccmd { $::QA::LDAS_ROOT/testbin/createRDSTest.tcl --site $::SITE -debug 0 -verbose lpse 
	--enable-globus $::USE_GLOBUS_CHANNEL \
 	--enable-gsi $::USE_GSI --qa-debug-level $::QA::Debug::Level \
	--qa-debug-auto-flush $::QA::Debug::AutoFlush \
 	--user createrdstest } 
 
	proc cleanup { framedir } {
		catch { exec /ldas_usr/ldas/test/bin/rdsclean.tcl $framedir } err
		Puts 1 "cleanup $framedir $err"
	}
		
	proc readPipe { fd } {
		
		if	{ [ catch {
			set lines [ read -nonewline $fd ]
			if	{ [ string length [ string trim $lines ] ] } {
				Puts 1 $lines	
				Puts 2 [ ::QA::createRDSTest_S3::test::verifyRDS $::QA::createRDSTest_S3::test::framedir  ]
			} else {
				if	{ [ eof $fd ] } {
					error "$fd eof"
				}
			}
		} err ] } {
			Puts 1 $err
			catch { close $fd }
			catch { fileevent readable $fd {} }
			set pipes [ set ::QA::createRDSTest_S3::test::pipes ]
			set index [ lsearch -exact $pipes $fd ]
			if	{ $index > -1 } {
				set ::QA::createRDSTest_S3::test::pipes [ lreplace $pipes $index $index ]
				Puts 1 "pipes remaining [ set ::QA::createRDSTest_S3::test::pipes ]"				 
				if	{ ! [ llength $::QA::createRDSTest_S3::test::pipes ] } {
					set ::QA::createRDSTest_S3::test::done 1
				}
			}			
		}
	}
	
	proc runTest { ifo adcfilelist output args } {	
	
		if	{ [ catch {
			set name [ namespace current ]
            	namespace import [ namespace parent $name ]::*
			set basiccmd [ namespace eval $name set basiccmd ]
			set BINDIR [ namespace eval $name set BINDIR ]
			set framedir [ namespace eval $name set framedir ]
			set cmd [ subst $basiccmd ]
			foreach file $adcfilelist {	
				append cmd " --adcFileList [ file join $BINDIR $file ] "
			}
			append cmd " --defaultJobLength 128 --ifo $ifo --outputdir $framedir/$output"
			append cmd " [ join $args " " ]" 
			Puts 1 $cmd	
			set pipe [ open  "|$cmd" r ]
			Puts 1 "$ifo pipe $pipe"
			
			fconfigure $pipe -blocking off -buffering line
			lappend ::QA::createRDSTest_S3::test::pipes  $pipe
			Puts 1 "pipes [ set ::QA::createRDSTest_S3::test::pipes ]"
			fileevent $pipe readable [ list ::QA::createRDSTest_S3::test::readPipe $pipe ]
			incr ::QA::createRDSTest_S3::test::numTests 1
		} err ] } {
			Puts 0 "[info level 0 ]: $err"
		}
	}
	
	proc verifyRDS { framedir } {
	
		if	{ [ catch {
			set result ""
			foreach dir [ lsort [ glob -nocomplain $framedir/* ] ] {
				set files [ glob -nocomplain $dir/*.gwf ] 
				if	{ [ llength $files ] } {
					set cmd "exec ls -l $files | wc -l"
					catch { eval $cmd } data
					append result "$dir:\t$data files\n"   
				}
			}      
		} err ] } {
			return -code error  "[info level 0]: $err"
		}
		return $result
	}	

		
	;## level 1 for lho and llo
	if	{ [ catch {
		set ::QA::createRDSTest_S3::test::numTests 0
		catch { unset ::QA::createRDSTest_S3::test::done }
		cleanup $framedir
		runTest H1 rds_s3.lho LHO 
		runTest L1 rds_s3.llo LLO
		runTest HL [ list rds_s3.lho rds_s3.llo ] MERGED "--usertype RDS_MERGED_L"
		vwait ::QA::createRDSTest_S3::test::done
		unset ::QA::createRDSTest_S3::test::done
		runTest H2 rds_l2.lho LH2 "--type RDS_R_L1"
		runTest L2 rds_l2.llo LL2 "--type RDS_R_L1"
		vwait ::QA::createRDSTest_S3::test::done
		
		test createRDSTest_S3:frame:createRDS:verifyS3Frames {} -body {
			set result [ verifyRDS $framedir ]
			set error ""
			foreach line [ split $result \n ] {
				if	{ [ regexp {([^:]+):[\s\t]*(\d+)\s+files} $line -> dir framecount ] } {
					if	{ $framecount != 223 } {
						append error "$dir has $framecount frames, expected 223\n"
					} else {
						Puts 1 $line
					}
				}
			}
			if	{ [ string length $error ] } {
				error $error
			}
		} -match exact -result {}
	} err ] } {
		Puts 0 $err
	}	
	
	flush [outputChannel]
    
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::createRDSTest::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::createRDSTest_S3::test
