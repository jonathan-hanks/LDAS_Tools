#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::dbException::test {

	set dbutilsrsc /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc 
	set TESTLIB /ldas_usr/ldas/test/database
	set unix_procidList1 [ list 15896 23690 23695 23683 ]
	set jobIdList [ list 10558145 ]
	set programList [ list program_1029177 ]
	set unix_procIdList2 [ list 1029177926 1029177927 1029177928 1029177929 \
	1029177930 1029177931 1029177932 1029177933 1029177934 1029177935 ]
	set user ldas
	set cmd_putMetaData "putMetaData \
    -database \$dsname \
	-ingestdata file:"
	
	;## series of exception tests with 3 entries <desription> <filename> <expected>
	set testlist { 
	"optional group name 1" optname1.xml "Inserted 3 rows" 
	"optional group name 2" optname2.xml "Inserted 3 rows" 
	"optional group name 3" optname3.xml "Inserted 3 rows" 
	"test no group/table name" test_bare_names.xml "Unable to find transaction within ilwd"
	"test no seqid" test_no_seqid.xml "Inserted 3 rows"
	"single set insertion" try1.xml "Inserted 14 rows into" 
	"test double set insertion" txn2.ilwd "Inserted 32 rows into"
	"bad data in ilwd" baddata.xml "extra_data: char"
	"skip comments metadata0101" metadata0101.xml "Inserted 30 rows" 
	"skip comments comments" comments10.ilwd "Inserted 10 rows" }
	
	
	set rollbacks { 
	"invalid data rollback" rollback1.ilwd "ThisIsAnInvalidKey"
	"duplicate data rollback" rollback.ilwd  "SQL0803N  One or more values.+SQL:"}
	
	set NON_STD_DYNLIB /dso-test
	
	;## series of exception tests in 3 entries per list <description> <cmd> <expected>
	;## unofficial must use test database ldas_tst
	set cmdlist  {
		"bad database name putMetaData" 		
		"putMetaData 
		-subject {bad database} 
    	-database bad_db 
		-ingestdata file:$TESTLIB/comments10.ilwd"
		"bad_db is not a valid database name"
		
		"bad database name getMetaData"
		"getMetaData 
    	-returnprotocol file:/getMeta 
    	-outputformat {ilwd ascii}
    	-database notdb
    	-sqlquery \{select * from runlist\}"
		"notdb is not a valid database name"
		
		"uppper case database name getMetaData" 
		"getMetaData
    	-returnprotocol file:/getMeta
    	-outputformat {ilwd ascii}
    	-database LDAS_TST
    	-sqlquery \{select * from runlist\}"
		"LDAS_TST is not a valid database name"
		
		"uppper case database name putMetaData"
		"putMetaData
    	-database LDAS_TST
		-ingestdata http://www.ldas-dev.ligo.caltech.edu/ldas_outgoing/jobs/LDASTest/TrigMgr20001208205115000006010000.xml"
		"LDAS_TST is not a valid database name"
	
		"dbquality channel on test database ok" 	
		"dataPipeline 
		-subject {DBQCHANNEL:EXTRA:PUSH:LIGOLW} 
		-dynlib {} -filterparams (0) -dbqualitychannel { 1024 680894200 0 680895300 0 
		{select start_time as startsec, end_time as StopSec, start_time_ns as StartnanoSec, end_time_ns 
		as stopNanosec, version from segment where version=13615955} push qchan } 
		-datacondtarget ligolw 
		-multidimdatatarget ligolw 
		-metadataapi ligolw 
		-database ldas_tst -aliases {gw=P2:LSC-AS_Q::AdcData;} 
		-algorithms { output(qchan,_,_,,quality channel); } 
		-framequery { {} {} {/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/P-R-610000000-512.gwf} {} {Adc(P2:LSC-AS_Q)} }" 	
		"Your results.+No MPI processing will occur" 
	
		"gaps no database data" 	
		"dataPipeline 
	 	-subject GAPTEST -dynlib /ldas/lib/wrapperAPI/libio.so 
		-filterparams (0) -np 2 -datacondtarget wrapper -multidimdatatarget ligolw 
		-database ldas_tst -aliases \{gw=_ch0\} -allowgaps 1 
		-algorithms \{ sz=size(gw); sr=getSampleRate(gw); dur=div(sz,sr); act=integer(dur); 
		exp=value(80); diff=sub(act,exp); abs=abs(diff); neg=mul(abs,-1); getElement(gw,neg); 
		output(gw,_,_,gw,resampled gw channel); \} 
		-framequery { R_GAP_TEST H {} {727500000-727500079} {Adc(H0:PEM-LVEA_SEISX)} }" 	
		"No metadata for data pipeline.+Your results:"
		
		"ligolw exception from bad xml" 	
		"putMetaData 
    	-database ldas_tst \
		-ingestdata file:$TESTLIB/file4733_1.xml" 	
		"Invalid document structure" 	
	
		"frame not supported"
	
		"getMetaData 
			-subject {frame not supported}
    		-returnprotocol file:/getMeta 
    		-outputformat {frame} 
    		-database {$dbname} 
    		-sqlquery {select * from runlist}"		
		"-outputformat frame is not currently supported" 	
		
		"invalid ilwd format"	
		"getMetaData 
			-subject {ilwd not ascii or binary} 
    		-returnprotocol file:/getMeta 
    		-outputformat {ilwd junk} 
    		-database \{$dbname\} 
    		-sqlquery \{select * from runlist\}" 
		"-outputformat.+invalid" 	
		"getMetaData outputformat default"
	
		"getMetaData
			-subject {ilwd default to ascii}
    		-returnprotocol file:/getMeta
    		-outputformat {ilwd}
    		-database {$dbname}
    		-sqlquery {select * from runlist}"		
		"Your results:\\s*\\n(\[^\\n\]+)\\n.+(/ldas_outgoing/jobs\[^\\n\]+)"
	
		"unknown table" 
		"putMetaData -database $dbname -ingestdata file:$TESTLIB/out-of-range.ilwd"	
		"unknown table requested" 	
		
		"out of range"	
	 	"putMetaData -database ldas_tst -ingestdata file:$TESTLIB/nulldata.binary.ilwd" 	 
	 	"out_of_range"  
		
		"Unofficial DSO"
		"dataPipeline
    	-subject         {Unofficial DSO test}
    	-dynlib          /dso-test/libldasinspiral.so
   		 -filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
    	-datacondtarget  wrapper
    		-metadatatarget  datacond
    		-multidimdatatarget  ligolw
    		-database        ldas_tst
    		-state           {}
    		-np 3
    		-framequery {
        	{ R H {} {730522210-730522721} Adc(H2:LSC-AS_Q) }
    		}
    	-responsefiles {
        	file:/ldas_outgoing/jobs/ldasmdc_data/inspiral/inspiral/response_2048_18.ilwd,pass
    	}
    	-aliases {
        	gw = H2\\\:LSC-AS_Q::AdcData;
    	}
    	-algorithms {
        	rgw = resample(gw, 1, 8);
        	p = psd( rgw, 262144 );
        	output(rgw,_,_,:primary,resampled gw timeseries);
        	output(p,_,_,spectrum:psd,spectrum data);
    	}"
		"Inserted"
	}
	
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--database} {string} \
    {database name} \
    { set ::QA::dbException::test::dbname $::QA::Options::Value }
	  
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

set ::auto_path "[file join $::LDAS lib] $::auto_path"
set API cntlmon

source [file join $::TOPDIR cntlmonAPI cntlmon.state]
source [file join $::TOPDIR LDASapi.rsc]

package require base64
package require generic

set ::env(RUNDIR) $::TOPDIR 

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::dbException::test {

    ##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	
	if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
		source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    } elseif { [ file exist /ldas/lib/genericAPI/LDASapi.rsc ] } {
		source /ldas/lib/genericAPI/LDASapi.rsc 
	} else {
		set ::METADATA_API_HOST metaserver
	}
	QA::ConnectToAgent
	
	##-------------------------------------------------------------------
    ## Local variable declaration
    ##-------------------------------------------------------------------
	
    set outputformat LIGO_LW
	set index 1
	set returnprotocol "file://descMeta$index.xml"
    set cmd_putMetaData "putMetaData \
    	-database \$dbname \
		-ingestdata file:$TESTLIB/\$testfile \
		-subject \$subject"
        
    ;## commands to db2 should be in quotes
	set runlist_clean_cmd "\"delete from runlist where site='sit' and run_type='run_type_1084562386'\""	
    ;## +c or -c command line options are separated from sql statement
	set autocommitOffCmd "+c \"insert into runlist (site,run,start_time,end_time,run_type) values ('sit', \
				1084562386,768597598,768597598,'run_type_1084562386')\""
    ##-------------------------------------------------------------------
    ## Executes db2 cmd remotely, must terminate connection via terminate
    ##-------------------------------------------------------------------
	if	{ [ file exist $dbutilsrsc ] } {
		source $dbutilsrsc
	} 
	
	proc execdb2cmd { fid cmd { wait 1 } } { 
		set reply "" 
		if	{ [ catch {
			set reply [ ::QA::DB::PutsCmd $fid $cmd ]
			Puts 15 "execdb2cmd $cmd\n$reply"
		} err ] } {			
			return -code error $err
		}
		return $reply
	}
	
	;##==============================================================
	;## Allow us to run commands the machine hosting the database
	;##==============================================================

	::QA::DB::DB2::Init
	::QA::ConnectToAgent

	;##==============================================================
	;## Open a stream to talk with database
	;##==============================================================

	if {[catch {set dbstream [::QA::DB::DB2::Open $dbname]} err]} {
	    puts $err
	}
  
	;## delete unix_procid list 1
	proc clearDatabase {} {
		set name [ namespace current ]
        namespace import [ namespace parent $name ]::*

		foreach unix_procid [ set ::${name}::unix_procidList1 ] {
			set err [ Rexec $::METADATA_API_HOST [ set ::${name}::user ] "/usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
			DB2INSTANCE=ldasdb del_db_process.tcl [ set ::${name}::dbname ] unix_procid=$unix_procid" ]
			Puts 1 "$unix_procid $err"
		}
		foreach program [ set ::${name}::programList ] {
			set err [ Rexec $::METADATA_API_HOST [ set ::${name}::user ] "/usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
			DB2INSTANCE=ldasdb del_db_process.tcl [ set ::${name}::dbname ] program=$program" ]
			Puts 1 "delete program $program $err"
		}
		foreach jobId [ set ::${name}::jobIdList ] {
			set err [ Rexec $::METADATA_API_HOST [ set ::${name}::user ] "/usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
			DB2INSTANCE=ldasdb del_db_process.tcl [ set ::${name}::dbname ] jobId=$jobId" ] 
			Puts 1 "delete jobId $jobId $err"
		}
	}
	Puts 1 "clearing out database $dbname test data ..."
	clearDatabase
	
	foreach { desc testfile expected } $testlist  {
		set subject [ list $desc ]
		set cmd "${cmd_putMetaData}${testfile}"	
		set scmd [ subst $cmd_putMetaData ]

		test dbException:putMetaData:$desc {} -body {
     		if {[catch {SubmitJob job $scmd} err]} {
				LJdelete job
	    		return $err
			}
			set reply $job(jobReply)
			LJdelete job
			return $reply
    	} -match regexp -result $expected
	}
	
	set expected "/ldas_outgoing/jobs/${::RUNCODE}.+/getMeta"
	test dbException:getMetaData:notInvalidElement {} -body {

		set cmd_getMetaData "getMetaData \
    	-returnprotocol file:/getMeta \
    	-outputformat {ilwd ascii} \
    	-database \{$dbname\} \
    	-sqlquery \{select * from runlist\} \
		-subject {Not invalid element}"
		
		set scmd [ subst $cmd_getMetaData ]
		Puts 5 "scmd $scmd\nexpected $expected"
     	if {[catch {SubmitJob job $scmd} err]} {
	    	return $err
		}
		set outfile [lindex $job(outputs) 0]
        LJdelete job
		return $outfile
    } -match regexp -result $expected 	
	
	catch { ::exec /usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
		DB2INSTANCE=ldasdb del_db_process.tcl $dbname unix_procid=23683 } err	 

	set expected "ThisIsAnInvalidKey"
	set testfile rollback1.ilwd
	
	foreach { desc testfile expected } $rollbacks {

		test dbException:metadata:putMetaData:$desc {} -body {
			set cmd "${cmd_putMetaData}${testfile}"	
			set scmd [ subst $cmd_putMetaData ]
     		if {[catch {SubmitJob job $scmd} err]} {
				LJdelete job
	    		return $err
			}
			set reply $job(jobReply)
			LJdelete job
			return $reply
    	} -match regexp -result $expected
		
		;## validate there are no rows inserted due to rollback on error
		set cmd "getMetaData \
    	-returnprotocol file:/getMeta \
    	-outputformat {ilwd ascii} \
    	-database \{$dbname\} \
    	-sqlquery \{select count(*) from process where unix_procid=23683\} \
		-subject {No rows due to rollback}"
		
		set desc "$desc validate"
		set expected {}
		test dbException:getMetaData:$desc {} -body {
     		if {[catch {SubmitJob job $cmd} err]} {
				LJdelete job
	    		return $err
			}
			set output $job(outputs)
			set data [ open $output r ]
			LJdelete job
			if	{ [ regexp {<int_4s name='1'>0</int_4s>} $data ] } {
				list
			} else {
				return "No match for 0 rows: '$data'"
			}
    	} -match regexp -result $expected
		
	}
	
	foreach dso { inspiral slope } {
    	set fd [ open /ldas/share/ldas/ldascmds/dataPipeline/$dso.cmd r ]
		set cmd_$dso [ read -nonewline $fd ]
		close $fd
		set cmd [ set cmd_${dso} ]
		regsub -- {-database\s+\S+} $cmd "-database dev_1" cmd
		regsub -- {-dynlib\s+\S+} $cmd "-dynlib $NON_STD_DYNLIB/libldas$dso.so" cmd
		set expected "non standard DSO"
		Puts 5 "dso $dso cmd $cmd"
		set desc "non-standard DSO $dso error"
		test dbException:dataPipeline:wrapper:$desc {} -body {
     		if {[catch {SubmitJob job $cmd} err]} {
				LJdelete job
	    		return $err
			}
			set reply job(jobReply)
			LJdelete job
			return $reply
    	} -match regexp -result $expected
    }
	
	foreach dso { inspiral slope } {
		set cmd [ set cmd_${dso} ]
		regsub -- {-database\s+\S+} $cmd "-database ldas_tst" cmd
		regsub -- {-dynlib\s+\S+} $cmd "-dynlib $NON_STD_DYNLIB/libldas$dso.so" cmd
		set expected "Inserted \\d+ rows."
		set desc "non standard DSO $dso ok"
		test dbException:dataPipeline:wrapper:$desc {} -body {
     		if {[catch {SubmitJob job $cmd} err]} {
				LJdelete job
	    		return $err
			}
			set reply $job(jobReply)
			LJdelete job
			return $reply
    	} -match regexp -result $expected
    }
	

	foreach { desc cmd expected } $cmdlist {
		;## do no substitue unofficial dso
		if	{ ![ regexp -nocase -- {unofficial dso} $desc ] } {
			set cmd [ subst $cmd ]
		}
		regexp {(\S+)\s+} $cmd -> usercmd 
		test dbException:$usercmd:wrapper:$desc {} -body {
			if 	{[catch {SubmitJob job $cmd} err]} {
				LJdelete job
	    		return $err
			}
			set reply $job(jobReply)
			LJdelete job
			return $reply
    	} -match regexp -result $expected
	}
	
	;## rollback from deadlock
	if	{ [ file exist $dbutilsrsc ] } {

		set desc "database deadlock rollback"
		set overall_expected ""

		test dbException:putMetaData:$desc  {} -body {
				
				set cmd $runlist_clean_cmd
				execdb2cmd $dbstream $cmd				
				after 2000
						
				;## turn off autocommit
				set cmd $autocommitOffCmd
				execdb2cmd $dbstream $cmd
				after 2000
				
				;## the data in ilwd must match the one in db2 insert cmd for this to deadlock
				set insertcmd "putMetaData \
    			-database $dbname
				-ingestdata file:$TESTLIB/runlist-lock.ilwd \
				-subject \{rollback from deadlock\}"
				
				;## test row locking
				set expected "rolled back because of a deadlock or timeout.+SQL:"
								
				if 	{[catch {SubmitJob job $insertcmd} err]} {
					set reply $err
				} else {
					set reply $job(jobReply)
				}
				LJdelete job
				Flush
				Puts 1 $reply
				if	{ ! [ regexp $expected $reply ] } {
					return $reply
				}		
				;## clear row inserted
				set cmd $runlist_clean_cmd
				execdb2cmd $dbstream $cmd
				after 1000
				
				;## commit and reset
				set cmd "connect reset"
				execdb2cmd $dbstream $cmd
				
				set expected "Inserted"
				
				if 	{[catch {SubmitJob job $insertcmd} err]} {
					Puts 0 "err $err"
					set reply $err
				} else {
					set reply $job(jobReply)
				}
				Puts 1 $reply
				LJdelete job
				Flush
				if	{ ! [ regexp $expected $reply ] } {
					return $reply
				}
				list				
		} -match regexp -result $overall_expected

	}
	::QA::DB::DB2::Conclude
	close $dbstream		
								
	flush [outputChannel]
    
   	#========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::descMetaData::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::dbException::test
	
