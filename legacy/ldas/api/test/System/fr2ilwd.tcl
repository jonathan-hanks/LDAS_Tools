#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for createRDS user command 
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./createRDSTest_s3.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 --qa-debug-level 0 -match "*createRDS_S3*"
# 
# The namespace name should be ::QA::<script base name>::test
#
#For the individual tests, use :<script base name>:<LDAS user 
#Command>:<test description>:[<any other descriptive text>:]
#========================================================================
if { [info exists env(SRCDIR)] } {
    foreach path $env(SRCDIR) {
     lappend ::auto_path $path
    }
}

if { [info exists env(PREFIX)] } {
    lappend ::auto_path $env(PREFIX)/lib/genericAPI
} elseif { [file isdirectory /ldas/lib/genericAPI] } {
    lappend ::auto_path /ldas/lib
}

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set auto_path "[file dirname $argv0] $::auto_path"
}

if	{ [ file exist /ldas_outgoing/cntlmonAPI/cntlmon.state ] } {
	source /ldas_outgoing/cntlmonAPI/cntlmon.state
} else {
	set ::LDAS ./ldas
	set ::API frame
}
if	{ [ file exist /ldas_outgoing/LDASapi.rsc ] } {
	source /ldas_outgoing/LDASapi.rsc
} else {
	source /ldas/lib/LDASapi.rsc
}
package require genericAPI
package require generic
package require frameAPI

#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::fr2ilwd::test {

	set scriptname [file tail $::argv0]
	set auto_path "/ldas/lib $::auto_path"
	set nfsframesdir "/ldas_outgoing/test/Data0"
	set locframesdir "/scratch/test/frames/Data0"
	
	set sampleframefile "/scratch/test/frames/Data0/sample_frame"
	
	set numfiles 100
	set createnfs 0
	set createloc 0
	set cleanup 1
	set doloc 1
	set donfs 1
	set filemess {}
	set fileext "gwf"
	set TIMESERIES [ list ]

	set passdelta 200
	
	for { set i 0 } { $i < 9 } { incr i } {
		lappend TIMESERIES [ expr int(pow(2,$i)) ]
	}

	set ITERATIONS [ list 1 5 10 100 ]
	
	set DATA_TYPES [ list INT_2U INT_2S INT_4U INT_4S \
	INT_8U INT_8S REAL_4 REAL_8 COMPLEX_8 COMPLEX_16 ]

	set COLUMNS [ list open read extract initialize insert write total ]
	set cleanupFlag 1
    set debugframe 0
}

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--nfsframe} {string} \
    {nfs frames directory} \
    { set ::QA::fr2ilwd::test::nfsframesdir $::QA::Options::Value }
::QA::Options::Add {} {--locframesdir} {string} \
    {local frames directory} \
    { set ::QA::fr2ilwd::test::locframesdir $::QA::Options::Value }    
::QA::Options::Add {} {--sample} {string} \
    {sample frame file} \
    { set ::QA::fr2ilwd::test::sampleframefile $::QA::Options::Value }
::QA::Options::Add {} {--ext} {string} \
    {adjust frame time} \
    { set ::QA::fr2ilwd::test::fileext $::QA::Options::Value }       
::QA::Options::Add {} {--noclean} {string} \
    {clean up frames option} \
    { set ::QA::fr2ilwd::test::cleanup $::QA::Options::Value }
::QA::Options::Add {} {--createnfs} {string} \
    {create nfs frames directory} \
    { set ::QA::fr2ilwd::test::createnfs $::QA::Options::Value }    
::QA::Options::Add {} {--createloc} {string} \
    {create local frames directory} \
    { set ::QA::fr2ilwd::test::createloc $::QA::Options::Value }
::QA::Options::Add {} {--doloc} {string} \
    {test local frames} \
    { set ::QA::fr2ilwd::test::doloc $::QA::Options::Value }     
::QA::Options::Add {} {--donfs} {string} \
    {test local frames} \
    { set ::QA::fr2ilwd::test::donfs $::QA::Options::Value }       
::QA::Options::Add {} {--passdelta} {string} \
    {maximum time difference tolerated} \
    { set ::QA::fr2ilwd::test::passdelta $::QA::Options::Value } 
::QA::Options::Add {} {--resultsdir} {string} \
    {directory holding previous results} \
    { set ::QA::fr2ilwd::test::resultsdir $::QA::Options::Value }     
::QA::Options::Add {} {--oldfile} {string} \
    {previous result file} \
    { set ::QA::fr2ilwd::test::oldfile $::QA::Options::Value }
::QA::Options::Add {} {--debugframe} {string} \
    {previous result file} \
    { set ::QA::fr2ilwd::test::debugframe $::QA::Options::Value }                                	  
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::	
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

namespace eval ::QA::fr2ilwd::test {
	##-------------------------------------------------------------------
    ## Bring commonly used items into the local namespace
    ##-------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::TclTest::SubmitJob
    namespace import ::QA::URL::List
	namespace import ::QA::Rexec
	
	set resultdir $::DATA_OUTPUT_DIR
	Puts 1 "resultdir $resultdir"
    
	;## if this is not running on frame host, use ssh to execute it on frame host
	
	if { [ file exist ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc ]  } {
		source ${::QA::LDAS_OUTGOING_ROOT}/LDASapi.rsc
    } elseif { [ file exist /ldas/lib/genericAPI/LDASapi.rsc ] } {
		source /ldas/lib/genericAPI/LDASapi.rsc 
	} else {
		set ::FRAME_API_HOST opterondata-dev
	}
	set localhost [ exec uname -n ]
     
	if	{ ! [ string equal $::FRAME_API_HOST $localhost ] &&
           ! [ regexp {suntest} $localhost ] &&
           ! [ regexp {ldasbox} $localhost ]} {
		QA::ConnectToAgent
	
		set cmd {  $::QA::LDAS_ROOT/testbin/fr2ilwd.tcl --site $::SITE -debug 0 -verbose lpse \
		--enable-globus $::USE_GLOBUS_CHANNEL --enable-gsi $::USE_GSI \
		--qa-debug-level $::QA::Debug::Level --doloc 1 --donfs 0  } 
	
		set cmd [ subst $cmd ]
        Puts 1 "cmd $cmd"
		catch {Rexec $::FRAME_API_HOST ldas $cmd} result
		Puts 1 "remote fr2ilwd result:\n$result"        
        
		cleanupTests
		return
	}
			
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## compute the mean of sample times
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

	proc mean {samples} {
    		set N [llength $samples]
    		set mean [expr ([join $samples "+"]) / ${N}.0]
    		return $mean
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## check if frame file path is local
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
	proc islocal {path} {
    ;## expand any tildes (~)
    set path [file nativename $path]

    ;## prepend [pwd] to a relative path
    if {[string equal "relative" [file pathtype $path]]} {
        set path [file join [pwd] $path]
    }

    ;## look for symlinks and expand them
    set newpath "/"
    foreach dir [file split $path] {
        switch -exact -- $dir {
            . {continue}
            .. {set newpath [file dirname $newpath]; continue}
            / {continue}
            default {}
        }
        set newpath [file join $newpath $dir]
        if {![file exists $newpath]} {continue}
        if {[string equal link [file type $newpath]]} {
            set newpath [file readlink $newpath] 
        }
    }
    set path $newpath

    ;## mnttab for Solaris, mtab for Linux
    switch -exact -- $::tcl_platform(os) {
        Linux {set tabfile "/etc/mtab"}
        SunOS {set tabfile "/etc/mnttab"}
        default {}
    }
    catch {open $tabfile} fid
    set data [split [read $fid] "\n"]
    catch {close $fid}

    foreach line $data {
        if {[regexp -- {(\S+):\S+\s+(\S+)\s+.*} $line -> host mountp]} {
            ;##lappend mounts($host) $mountp
            set thishost [ info hostname ]
            if { [string equal $thishost $host] || 
                [ regexp $host $thishost ] } {continue}
            set mountp [string trimright $mountp {/}]
            if {[regexp -- "\^$mountp" $path]} {
                Puts 5 "$path is mounted on $mountp from $host"
                return 0
            }
        }
    }
    return 1
    }
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## basic checking of filepaths
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	proc sanityCheck {dir create type} {

    	if {![file isdirectory $dir]} {
        if {$create} {
            if {[catch {file mkdir $dir} err]} {
                puts stderr "Error: $err.\nSkipping $type test."
                return 0
            }
        } else {
            puts stderr "Error: $dir is not a valid directory.\nSkipping $type test."
            return 0
        }
    	}

    	switch -exact -- $type {
        nfs {
            if {[islocal $dir]} {
                puts stderr "Error: $dir is not $type mounted.\nSkipping $type test."
                return 0
            }
        }
        local {
            if {![islocal $dir]} {
                puts stderr "Error: $dir is not $type mounted.\nSkipping $type test."
                return 0
            }
        }
        default {}
    	}

   	if {$create} {
       	createFrameFiles $dir 
	}
    		return 1
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## create the sample frame files needed for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

	proc createFrameFiles	{dir {nof -1}} {
		set name [ namespace current ]
    	namespace import [ namespace parent $name ]::*
	    if	{ [ catch {
		    catch { file delete eval [ glob -nocomplain Z*.gwf ] } 
		    if	{ ! [ file exist [ set ::${name}::locframesdir ] ] } {
			    file mkdir [ set ::${name}::locframesdir ]
		    } else {
			    catch { eval file delete [ glob -nocomplain [ set ::${name}::locframesdir]/*.gwf ] } err
			    Puts 1 "rm files: $err"
			
		    }
		    foreach iteration [ set ::${name}::ITERATIONS ] {
			    foreach duration [ set ::${name}::TIMESERIES ] {
				    foreach type [ set ::${name}::DATA_TYPES ] {
					    set start [ expr [ gpsTime now ] + $duration ]
					    set cmd "exec $::FRSAMPLE -i$iteration -d$duration -s$start -tH_${type}_${iteration} -c$type"
					    set rc [ catch { eval $cmd } err ]
					    Puts 1 "$cmd\n$err"
					    if	{ $rc } {
						    error $err
					    }
				    }
			    }
			    ;## move the frames to mount point area
			    catch { eval file rename [ glob -nocomplain Z*.gwf ] [ set ::${name}::locframesdir ] } data
			    catch { exec ls -lt [ set ::${name}::locframesdir ] } data
			    Puts 1 "frames $data"
		    }
	    } err ] } {
		    Puts 0 "error:$err"
	
	    }
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## clean out existing frame files
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	proc cleanUp {} {
		set name [ namespace current ]
    		namespace import [ namespace parent $name ]::*
		set cleanupFlag [ namespace eval $name set cleanupFlag ]
   		if 	{!$cleanupFlag} {return}
    			foreach file [ namespace eval $name set filemess ] {
        			catch {[file delete -- $file]}
    			}
    			return
	}

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## print results of current testing to file
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	proc printResults {type {new 1}} {
	
	    set name [ namespace current ]
    	namespace import [ namespace parent $name ]::*
		
		if	{ [ catch {
			set resultdir [ namespace eval $name set resultdir ]
    		set result_subdir [ file join $resultdir results-[ clock format [ clock seconds ] -format "%m%d" ] ]
    		if 	{![file exists $result_subdir]} {
        		file mkdir $result_subdir
    		}
    		set logfile [file join $result_subdir [file rootname [ namespace eval $name set scriptname] ]].log

    		if 	{$new && [file exists $logfile]} {
        		set mtime [clock format [file mtime $logfile] -format "%H%M%S"]
        		file rename -force -- $logfile ${logfile}.${mtime}
    		}

   			set fid [ open $logfile a ]
			
    		if 	{$new} {
        		puts $fid "Frame to ILWD Rates at [exec cat /etc/ldasname]"
        		puts $fid [clock format [clock seconds] -format "%B %d, %Y"]
    		}

    		puts $fid {}
    		puts $fid "$type file system"
    		puts $fid "files: [ set ::${name}::numfiles ]"
    		puts $fid [format "%-10.10s\t%10.10s\t%-15.15s\t%-10.10s\t%-10.10s\t%-10.10s\t%-10.10s\t%-10.10s\t%-10.10s\t%-10.10s" \
			    channel dims duration open read attributes initialize insert write total ]

			foreach type [ namespace eval $name set DATA_TYPES ] {
		        foreach dim [ namespace eval $name set ITERATIONS ] {
				    foreach duration [ namespace eval $name set TIMESERIES ] {
        				set openfile [ set ::${name}::open_frame_file($type,$dim,$duration) ]
        				set readframe [ set ${name}::read_frame($type,$dim,$duration) ]
        				set getattr [ set ::${name}::get_frame_attributes($type,$dim,$duration) ]
        				set initframe [ set ::${name}::init_raw_frame($type,$dim,$duration) ]
        				set insert [ set ::${name}::insert_adc_chans($type,$dim,$duration) ]
					    set write  [ set ::${name}::write_frame($type,$dim,$duration) ]
        				set total [expr {$openfile + $readframe + $getattr + $initframe + $insert } ]
        				puts $fid [format "%-10.10s\t%8.8s\t%10.10s\t%10.8f\t%10.8f\t%10.8f\t%10.8f\t%10.8f\t%10.8f\t%10.8f" \
            			$type $dim $duration $openfile $readframe $getattr $initframe $insert $write $total]
				    }
				    puts $fid \n
			    }
    	    }

    		flush $fid
    		close $fid
		} err ] } {
			Puts 1 "[ info level 0 ] error $err"
    		return
		}
	}
    
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for running the timing tests on frame files of various types 
;## and dimensions
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    proc writeFrameDebug { framep file } {
	    set name [ namespace current ]
        namespace import [ namespace parent $name ]::*
        if  { [ set ::${name}::debugframe ] } {
            set outframe "debug_[ file tail $file ]"
            Puts 1 "writing $outframe"
            set fp1 [ openFrameFile $outframe w ]
	        writeFrame $fp1 $framep
	        closeFrameFile $fp1
        }

    }
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for running the timing tests on frame files of various types 
;## and dimensions
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	proc runTest {dir} {

	if	{ [ catch {
		set name [ namespace current ]
    		namespace import [ namespace parent $name ]::*
		set fileext [ namespace eval $name set fileext ]
		set seqpt ""
        
        ;## drop a status file 
        set fd [ open $::TMP/fr2ilwd.log w ]
        
		catch {
		uplevel {
    		;## Clear some global variables
    			array unset open_frame_file
    			array unset read_frame
    			array unset get_frame_attributes
    			array unset init_raw_frame
    			array unset insert_adc_chans
    			array unset trig_adc_index_range		
		}
		}
		
    	set filelist [glob -nocomplain [file join $dir *.$fileext]]
		set file [ namespace eval $name set sampleframefile ]
		set outframe sample_out_frame
	
    	;## The first time through we are going to run
    	;## readDaqFrame, on all subsequent calls we are
    	;## going to run updateDaqFrame.
		
		set seqpt "openFrameFile $file r"
    	set fp [ openFrameFile $file r ]
       
		set framep [ readFrame $fp ]
        writeFrameDebug $framep $file
       
		;## the following cmd does not exist
		# puts "number of frames [ getFrameNumber $fp ]"
    	#set framep [ readDaqFrame $fp ]
		#set seqpt "getFrameDetectorProc $framep 0"
		
		set expected  "1.+p_FrDetector 4698"
		test fr2ilwd:frame:getFrameDetectorProc&Num:$file {} -body {
			set num [ getFrameDetectorProcNum $framep ]	
    			set detectorp [ getFrameDetectorProc $framep 0 ]
			set maxnoc [getFrameAdcDataNum $framep]           
			return "$num $detectorp $maxnoc"	
		} -match regexp -result $expected
		
		;## do not close out the file
    		;## closeFrameFile $fp

        	;## iterate over filelist
        	foreach file $filelist {
			regexp {Z-H_([^-]+)_(\d+)-(\d+)-(\d+)} $file -> type dims starttime duration 
            puts $fd "fr2ilwd:frame:$type:$dims:$duration"
            flush $fd
            
			test fr2ilwd:frame:$type:$dims:$duration:$file {} -body {
            	;## open the file
			set seqpt "openFrameFile $file r"
           	 __t::start
			set fp [ openFrameFile $file r ]
            lappend ::${name}::open_frame_file($type,$dims,$duration) [ __t::mark ]
            
			__t::start
			set framep1 [ readFrame $fp ]
            writeFrameDebug $framep1  $file
            lappend ::${name}::read_frame($type,$dims,$duration) [ __t::mark ]
            closeFrameFile $fp

            set retval {}
            __t::start
            foreach attr {Name Run Frame GTime ULeapS Dt} {
                lappend retval $attr [ getFrameFrameH$attr $framep ]
            }

            foreach { var value } $retval {

                lappend ptrs $value
                set value [ getElement $value ]
                regexp -- {>(.+)<} $value -> value
                if { [ llength $value ] == 2 } {
                    foreach "GTimeS GTimeN" $value { break }
                } else {
                    set $var $value
                }
            }
            lappend ::${name}::get_frame_attributes($type,$dims,$duration) [ __t::mark ]

            set ilwdp [ createFrame $Name $Run $Frame $GTimeS $GTimeN $ULeapS $Dt ]

            __t::start
            set raw [ createRawData rawdata ]

            insertFrameData $ilwdp $raw
			
			set seqpt "DetectorProc2container $detectorp"
            set detectorilwdp [DetectorProc2container $detectorp]
			set seqpt "insertFrameData $ilwdp  $detectorilwdp"
	
            insertFrameData $ilwdp  $detectorilwdp
            #insertFrameData $ilwdp  $detectorp
            #insertDetector $framep $ilwdp
			
            lappend ::${name}::init_raw_frame($type,$dims,$duration) [ __t::mark ]

			__t::start
            insertAdcChanList $framep $ilwdp $dims
            lappend ::${name}::insert_adc_chans($type,$dims,$duration) [ __t::mark ]

			__t::start
			set fp1 [ openFrameFile $outframe w ]
			writeFrame $fp1 $framep 
			lappend ::${name}::write_frame($type,$dims,$duration) [ __t::mark ]
			closeFrameFile $fp1
			
			file delete $outframe 
			
            destructElement $ilwdp
            destructElement $raw
			destructFrame $framep1
            foreach ptr $ptrs { 
                destructElement $ptr 
            }
            set ptrs {}
		  } -result {}
        }
    		catch { destructFrame $framep }
    		##catch { destructElement $detectorp }
    		#catch { destructElement $detectorilwdp }
	
	} err ] } {
		Puts 1 "$file $err"
	}
        catch { close $fd }
    	return
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## general stats
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	proc stats {samples} {
	    	
		set N [ llength $samples ]

    		if 	{ $N < 3 } {
        		return -code error "stats: sample size too small: ($N items)"
    		}
		
		if	{ [ catch {
    			set mean 0.0
    			set S2   0.0
    			set cov  0.0
    			;## calculate the arithmetic mean
    			set mean [ expr ([ join $samples + ]) / $N. ]

    			;## calculate the standard deviation
    			foreach s $samples {
        			set S2 [ expr { $S2+pow(($s-$mean),2) } ]
    			}
    			set S2 [ expr { $S2/($N-1) } ]
    			set S  [ expr { sqrt($S2) } ]
		} err ] } {
    			return [list $mean $S]
		}
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## compare old and new stats for passing delta 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
	proc compare {new old out} {
		set name [ namespace current ]
    		namespace import [ namespace parent $name ]::*
		
    		if 	{![file exists $old]} {
			set error "Error: ${old}: No such file."
        		return -code error $error
    		}
    		if 	{![file exists $new]} {
        		set error  "Error: ${new}: No such file."
        		return -code error $error
    		}	
    		if 	{[file tail $new] != [file tail $old]} {
        		;## Warning
        		Puts 1 "Warning: $old and $new do not have the same name."
   	 	}
		regexp {results-(\d+)} $new -> date

		if	{ [ catch {
    		     foreach age {new old} {
				Puts 1 "file $age [ set $age ]"
        			set fd [open [set $age] r]
        			set data [split [read $fd] "\n"]
        			close $fd

        			foreach line $data {
          			if 	{[regexp {(nfs|local) file system} $line var]} {
                			set var [string range $var 0 2]
                			continue
            			}
					if	{ [ regexp -nocase {channel} $line ] } {
						foreach item $line {
							lappend columns_${age} $item
						}
						set numColumns_$age [ llength [ set columns_$age ] ]
						Puts 2 "set numColumns_${age} columns [ set columns_$age ]"
						continue
					}

					if	{ ![ info exist columns_$age ] } {
						continue
					}
            			set conv [eval scan \"$line\" \"%s %d %d %f %f %f %f %f %f %f\" [ set columns_$age ] ]
					foreach item [ set columns_$age ] {
						Puts 2 "item $item [ set $item ]"
					}
            			if 	{$conv != [ set numColumns_$age ] } {continue}
					set functions [ lrange [ set columns_$age ] 3 end ]
					Puts 2 "functions $functions"
            			foreach var $functions {
                			set ${var}_${age}($channel,$dims,$duration) [set $var]
						Puts 2 "set ${var}_${age}($channel,$dims,$duration) [set $var]"
    	   				}
			
				}

				if	{ [ set numColumns_$age ] != [ set numColumns_$age ] } {
					Puts 1 "old and new columns differ" 				
				}
			}

    		;## calculate the delta between the total time
	  	set resultdir [ namespace eval $name set resultdir ]
	  	set fd [ open $resultdir/results-$date/fr2ilwd.stats  w ]
	  	puts $fd "frame to ilwd test statistics"
	  	puts $fd [ clock format [clock seconds] -format {%B %d, %Y}]
	  	puts $fd "New file [ set new ]\nOld file [ set old ]\n"
	  
	  	puts $fd [ format "%-10.10s\t%10.10s\t%15.15s\t\t%15.15s\t%10.10s\t%10.10s" \
			channel dims duration delta total_average %faster ]
		set passdelta [ namespace eval $name set passdelta ]
		
		foreach channel [ set ::${name}::DATA_TYPES ] {
			foreach dims [ set ::${name}::ITERATIONS ] {
        			foreach time [ set ::${name}::TIMESERIES ]  {
					test fr2ilwd:frame:$channel:$dims:$time:cmp {} -body {
						set delta [ expr [ set total_new($channel,$dims,$time) ] - [ set total_old($channel,$dims,$time) ] ]
						set average [ expr ([ set total_new($channel,$dims,$time) ] + [ set total_old($channel,$dims,$time) ]/2.0) ]	
									
						puts $fd [ format "%-10.10s\t%10.10s\t%15.15s\t%15.15f\t%10.10f\t%10.10f"\
						 $channel $dims $time $delta $average [ expr ($delta/$average) *100] ]
						if	{ $delta > $passdelta } {
							return -code error "$delta exceeded ${passdelta}%"
						}
					} -result {}
				}
			}
       	}
    		close $fd 
 
		} err ] } {
			return -code error $err
		}
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## output the .stats file 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

	proc outStats {new old out} {
		set name [ namespace current ]
    	namespace import [ namespace parent $name ]::*
		if	{ [ catch {
    			if 	{[string match stdout $out]} {
        			set fout $out
    			} else {
        			set fout [open $out w]
    			}

    			puts $fout "frame to ilwd test statistics"
    			puts $fout [clock format [clock seconds] -format {%B %d, %Y}]
    			puts $fout "New file: [clock format [file mtime $new] -format {%B %d, %Y}]"
    			puts $fout "Old file: [clock format [file mtime $old] -format {%B %d, %Y}]"
    			puts $fout ""

    			puts $fout [format "%-7s%-10s%-10s" " " Mean "Std Dev"]
			    set statsnfsG [ set ::${name}::statsnfsG ]
    			puts $fout [format "%-6s%- 10.6f%- 10.6f" NFS [lindex $statsnfsG 0] [ lindex $statsnfsG 1]]
    			puts $fout [format "%-6s%- 10.6f%- 10.6f" Local [lindex $statslocG 0] [lindex $statslocG 1]]

    			foreach fstype {nfs loc} {
        			puts $fout ""
        			puts $fout [format "%-7s%-10s%-10s" [set ::${fstype}text] "Mean" "Std Dev"]
        			foreach chann $::Channels {
            			puts $fout [format "%-6s%- 10.6f%- 10.6f" $chann \
                		[lindex [set ::${name}::stats${fstype}($chann)] 0] [lindex [set ::${name}::stats${fstype}($chann)] 1]]
        			}	
    			}

    			foreach fstype {nfs loc} {
        			puts $fout ""
        			puts $fout [eval format "%-9s%-10s%-10s%-10s%-10s%-10s%-10s%-10s" \"[set ::${fstype}text]\" \
				[lrange [ set ::${name}::Times ] 0 end]]
        			puts -nonewline $fout [format "%-8s" Mean]
        			foreach time [ set ::${name}::Times ] {
            			puts -nonewline $fout [format "%- 10.6f" [lindex [set ::${name}::stats${fstype}($time)] 0]]
        			}

        			puts $fout ""
        			puts -nonewline $fout [format "%-8s" "Std Dev"]
        			foreach time [ set ::${name}::Times ] {
            			puts -nonewline $fout [format "%- 10.6f" [lindex [set ::${name}::stats${fstype}($time)] 1]]
        			}

        			puts $fout "\n"
    			}

    			if 	{![string match stdout $out]} {
        			close $fout
   			 }
		} err ] } {
			return -code error $err
		}
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## compare previous and current stats; pass if delta does not exceet $passdelta
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	

	proc framestats {} {
		set name [ namespace current ]
    	namespace import [ namespace parent $name ]::*
		set resultdir [ namespace eval $name set resultdir ]
		set max_mtime 0
		set date [ clock format [ clock seconds ] -format "%m%d" ]
		set outfile $resultdir/result-${date}/fr2ilwd.stats
		
		if	{ [ catch {
		
			if	{ ! [ info exist ::${name}::oldfile ] } {
				set resultdirs [ lsort -decreasing [ glob $resultdir/results-* ] ]
				foreach dir $resultdirs {
        			set mtime [ file mtime $dir ]
        			if	{ $mtime > $max_mtime } {
        				if  { [ regexp {results-(\d+)} $dir -> rundate ] } {
                        	if	{ ! [ string equal $rundate $date ] } {
                                set max_mtime $mtime
							    set olddir $dir
                        	}
                		}
   	 			    }
				}
                if  { [ info exist olddir ] } {
				    set oldfile $olddir/fr2ilwd.log
                } else {
                    error "No previous statistics to compare in $resultdir"
                }
			}
			
        	if 	{ [file exists $outfile] } {
            	set mtime [clock format [file mtime $outfile] -format "%H%M%S"]
            	file rename -force -- $outfile ${outfile}.${mtime}
        	}
			set newfile $resultdir/results-$date/fr2ilwd.log
    		compare $newfile $oldfile $outfile
		} err ] } {
			return -code error $err
		}
	}
	
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## main 
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
	
	if	{ [ catch {
		if 	{$donfs} {
    		if 	{[sanityCheck $nfsframesdir $createnfs nfs]} {
        		runTest $nfsframesdir
        		printResults nfs 1
    		}
		}

		if 	{$doloc} {
    		if 	{[sanityCheck $locframesdir $createloc local]} {
        		runTest $locframesdir
			}
    		printResults local 1
		}

		framestats 
	
		cleanUp

		# flush [outputChannel]
     	flush $::tcltest::outputChannel
	} err ] } {
		Puts 0 "Error: $err, exiting"
	}
    #========================================================================
    # Testing is complete
    #========================================================================
    cleanupTests
} ;## namespace - ::QA::fr2ilwd::test
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace delete ::QA::fr2ilwd::test
exit
	
