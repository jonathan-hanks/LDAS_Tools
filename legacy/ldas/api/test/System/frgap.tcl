#!/bin/sh
# -*- indent-tabs-mode: nil -*-
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Namespace global variables with default values for options
#------------------------------------------------------------------------
namespace eval ::QA::frgap::test {
    variable WindowSize 16
}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path ". /ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions
::QA::Options::Add {} {--frgap-window-size} {string} \
    {Window size} \
    { set ::QA::frgap::test::WindowSize $::QA::Options::Value }
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
qaInit

#------------------------------------------------------------------------
# Establish GLOBALS
#------------------------------------------------------------------------
namespace eval ::QA::frgap::test {
    ;##------------------------------------------------------------------
    ;## Importing of external elements to simplify coding
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::Flush
    namespace import ::QA::Debug::Puts
    namespace import ::QA::TclTest::SubmitJob

    ;##------------------------------------------------------------------
    ;## Definition of test data
    ;##------------------------------------------------------------------
    variable WindowInfo
    array set WindowInfo {
        1,fsize   1
        1,first   688011000
        1,last    688011061
        1,window  15
        1,step    1

        16,fsize   16
        16,frames  {691990000 691990032 691990048}
        16,gaps    {691990016 691990031}
        16,window  16
        16,step    1

        18,fsize   16
        18,frames  {691990000 691990032 691990048}
        18,gaps    {691990016 691990031}
        18,window  18
        18,step    1

        48,fsize   16
        48,frames  {727500000 727500032 727500064}
        48,gaps    {727500016 727500031 727500048 727500063}
        48,window  48
        48,step    1

        80,fsize   16
        80,frames  {727500000 727500032 727500064}
        80,gaps    {727500016 727500031 727500048 727500063}
        80,window  48
        80,step    1
    }

    ;##------------------------------------------------------------------
    ;## Test specific procedures
    ;##------------------------------------------------------------------
    proc Gappy {Start End Gaps AllowGaps} {
        set glob {}

        foreach {gs ge} $Gaps {
            if [expr ( ($Start >= $gs) \
                           && ( $Start <= $ge) ) \
                    && ( ($End >= $gs) \
                             && ($End <= $ge) ) ] {
                ;##------------------------------------------------------
                ;## Entire request resides within a gap
                ;## if the start or end is within a gap
                ;## or the gap is within the boundings of start - end
                ;##------------------------------------------------------
                set glob "*No frame files found*"
            } elseif [expr ( ( ! $AllowGaps ) && \
                                 ( ( ($Start >= $gs) \
                                         && ( $Start <= $ge) ) \
                                       || ( ($End >= $gs) \
                                                && ($End <= $ge) ) \
                                       || ( ($Start < $gs) \
                                                && ($End > $ge ) ) ) )] {
                ;##------------------------------------------------------
                ;## Part of the query resides within a gap
                ;##------------------------------------------------------
                set glob "*Gaps are detected:*"

            }
        }
        ;##--------------------------------------------------------------
        ;## Return pattern space for matching
        ;##--------------------------------------------------------------
        set glob
    }

    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    ;## Setup of tests
    ;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    set gaps [list]

    foreach var {fsize first last window step frames gaps} {
        if [info exists WindowInfo($WindowSize,$var)] {
            set $var $WindowInfo($WindowSize,$var)
        }
    }

    if { ! ( [info exists fsize ] && \
                 [ info exists window ] && \
                 [ info exists step] && \
                 ( ( [info exists first] && [ info exists last] ) || \
                       [info exists frames] ) ) } {
        return -code error "Invalid window size: $WindowSize"
    }

    set channel H0\:PEM-LVEA_SEISX
    if { ! [info exists first] && [info exists frames] } {
        set first [expr {[lindex $frames 0]}]
    }
    if { ! [info exists last] && [info exists frames] } {
        set last [expr {[lindex $frames end] + $fsize}]
    }
    if { ! [info exists frames] } {
        ;## TODO: populate the frame element
        set frames [list] ;## Currently a work around so debug line works
    }

    Puts 5 "INFO: type $WindowSize, window $window frames $frames site $::SITE"

    #========================================================================
    ;## 1 - concatFrameData -> ilwd, ligolw
    #========================================================================
    set allowgaps 1
    set last_start [expr {$last - $window}]
    for {set start $first} {$start <= $last_start} {incr start $step} {
        set end [expr {$start + $window} - 1]
        ;##--------------------------------------------------------------
        ;## Determine if gaps should be detected
        ;##--------------------------------------------------------------
        set result [Gappy $start $end $gaps $allowgaps]
        ;##--------------------------------------------------------------
        ;## Try both ilwd and ascii
        ;##--------------------------------------------------------------
        foreach format {{ilwd ascii} LIGO_LW} {
            set cmd "
            concatFrameData
            -returnprotocol {http://${start}-${end}}
            -outputformat {$format}
            -allowgaps $allowgaps
            -framequery { R_GAP_TEST H {} {${start}-${end}} {Adc($channel)} }
        "

            test 1:frgap:${start}-${end}:[lindex $format 0] {} -body {
                if [catch {SubmitJob job $cmd} err] {
                    return $err
                }
                set urlList $job(outputs)
                foreach url $urlList {
                    Puts 5 "INFO: $url"
                    checkURL $url
                }

                list
            }  -match glob -result $result
            Flush
        }
    }

    #========================================================================
    ;## 2 - nogaps concatFrameData -> wrapper, ligolw
    #========================================================================
    set allowgaps 0
    set last_start [expr {$last - $window}]
    for {set start $first} {$start <= $last_start} {incr start $step} {
        set end [expr {$start + $window} - 1]
        ;##--------------------------------------------------------------
        ;## Determine if gaps should be detected
        ;##--------------------------------------------------------------
        set result [Gappy $start $end $gaps $allowgaps]
        ;##--------------------------------------------------------------
        ;## Try both ilwd and ascii
        ;##--------------------------------------------------------------
        foreach format {{ilwd ascii} LIGO_LW} {
            set cmd "
            concatFrameData
            -returnprotocol {http://${start}-${end}}
            -outputformat {$format}
            -allowgaps $allowgaps
            -framequery { R_GAP_TEST H {} {${start}-${end}} {Adc($channel)} }
        "

            test 2:frgap:${start}-${end}:[lindex $format 0] {} -body {
                if [catch {SubmitJob job $cmd} err] {
                    return $err
                }
                set urlList $job(outputs)
                foreach url $urlList {
                    Puts 5 $url
                    checkURL $url
                }

                list
            } -match glob -result $result
            Flush
            LJdelete job
        }
    }

    #========================================================================
    ;## 3 - dataPipeline -> wrapper
    #========================================================================
    set allowgaps 1
    set last_start [expr {$last - $window}]
    for {set start $first} {$start <= $last_start} {incr start $step} {
        set end [expr {$start + $window} - 1]
        set result [Gappy $start $end $gaps $allowgaps]

        ;## Calculate expected duration
        set duration $window
        if {$start < [lindex $frames 0]} {
            set duration [expr {$duration - ([lindex $frames 0] - $start)}]
        }
        set cliff [expr {[lindex $frames end] + $fsize - 1}]
        if {$end > $cliff} {
            set duration [expr {$duration - ($end - $cliff)}]
        }

        for {set idx1 0; set idx2 1} {$idx2 < [llength $frames]} {incr idx1; incr idx2} {
            set frame1 [lindex $frames $idx1]
            set frame2 [lindex $frames $idx2]
            set cliff [expr {$frame1 + $fsize - 1}]

            if {($start > $cliff) && ($start < $frame2)} {
                set duration [expr {$duration - ($frame2 - $start)}]
                continue
            }

            if {($end > $cliff) && ($end < $frame2)} {
                set duration [expr {$duration - ($end - $cliff)}]
                break
            }
        }

        #set dynlib "libldastrivial.so"
        set dynlib "/ldas/lib/wrapperAPI/libio.so"
        set cmd "
        dataPipeline
        -subject GAPTEST
        -dynlib $dynlib
        -filterparams (0)
        -np 2
        -datacondtarget wrapper
        -multidimdatatarget ligolw
        -database ldas_tst
        -aliases {gw=_ch0}
        -allowgaps $allowgaps
        -algorithms {
            sz=size(gw);
            sr=getSampleRate(gw);
            dur=div(sz,sr);
            act=integer(dur);
            exp=value($duration);

            diff=sub(act,exp);
            abs=abs(diff);
            neg=mul(abs,-1);
            getElement(gw,neg);

            #output(diff,_,diff.ilwd,diff,diff);

            #rgw = resample(gw,1,2);
            output(gw,_,_,gw,resampled gw channel);
        }
        -framequery { R_GAP_TEST H {} {${start}-${end}} {Adc($channel)} }
    "


        test 3:frgap:${start}-${end}:wrapper {} -body {
            if {[catch {SubmitJob job $cmd} err]} {
                return $err
            }
            set urlList $job(outputs)
            set reply $job(jobReply)
            LJdelete job
            Puts 10 [trimReply $reply]
            list
        } -match glob -result $result
        Flush
    }
    #========================================================================
    # Testing is complete
    #========================================================================

    cleanupTests

} ;## namespace - ::QA::frgap::test
namespace delete ::QA::frgap::test
