#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

cd ~install/public_html/nightly_test_results
set resultdirs [ lsort -decreasing [ glob nightly_test_result_*.html ] ]
set lastfile [ lindex $resultdirs 0 ]
catch { exec date } date
if	{ [ regexp {Sat|Sun|Mon} $date ] } {
	set date [ clock format [clock scan "last Friday"] -format "%y%m%d" ]
} else {
	set date [ clock format [clock scan yesterday ] -format "%y%m%d" ]
}

set filename nightly_test_result_$date.html
puts "copying $lastfile to $filename"
file copy -force $lastfile $filename
file delete -force index.html
catch { exec ln -s $filename index.html } 
set data [ file readlink index.html ]
set cwd [ pwd ]
puts "$cwd/index.html -> $cwd/$data"
catch { exec nedit index.html & }
