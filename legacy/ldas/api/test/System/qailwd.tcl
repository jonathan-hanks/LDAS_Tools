
package provide qailwd 1.0

namespace eval ilwd {
    set tags {
        CHAR CHAR_S CHAR_U
        INT_2S INT_2U INT_4S INT_4U INT_8S INT_8U
        REAL_4 REAL_8
        COMPLEX_8 COMPLEX_16
        ILWD
        EXTERNAL
        LSTRING
    }
    set attrs {
        name    compression
        ndim    byteorder
        dims    comment
        units   format
        mdorder bytes
        size    parser
    }

    ;## regex of the base 64 character class
    set b64_charset {[a-zA-Z0-9\+\/ ]+}

    ;## regex of a start tag (<tag opt1="foo" opt2="bar">)
    set start {<(\w+)([^>]+)?>}

    ;## regex of an end tag (</tag>)
    set end {</(\w+)>}

    ;## regex of array element
    set element {^<(\w+)([^>]+)?>([^<]+)?</(\w+)>}

    ;## regex of external
    set external {^<(EXTERNAL)([^>]+)?>(.+)</(EXTERNAL)>}

    ;## regex of beginning of a container
    set begcontainer {^<(ILWD)([^>]+)?>}

    ;## regex of end of a container
    set endcontainer {^<(/ILWD)>}

    ;## regex of an attribute (key="value")
    set attr {(\w+)=['\"]([^'\"]+)['\"]}

    ;## regex of a dcd (document class descriptor)
    ;## (<!FOO BAR!> or <?FOO BAR?>)
    set dcd {^<([\!\?])([^>]+)>}
}

## ********************************************************
##
## Name: ilwd::preprocess
##
## Description:
## Rationalises whitespace in input.
##
## Usage:
##        set data [ ilwd::preprocess $data ]
##
## Comments:
## Might damage externals that contain formatted content.

proc ilwd::preprocess { { data "" } } {
    regsub -all {\n}   $data {}  data
    #regsub -all {\s+}  $data " " data
    regsub -all {< +}  $data "<" data
    regsub -all { +<}  $data "<" data
    regsub -all {> +}  $data ">" data
    regsub -all { +>}  $data ">" data
    regsub -all {</ +}  $data "/" data
    regsub -nocase -- $ilwd::dcd $data {} data

    return $data
}
## ********************************************************

## ********************************************************
##
## Name: ilwd::parse
##
## Description:
## Parse the ILWD into a structure.  See the comment
## section of parseILWD.  Recursive.
##
## Usage:
##
## Comments:
## The regex patterns are NOT strict.

proc ilwd::parse { {data ""} } {
    array set contents {}
    set level 0
    set elem($level) 0

    while {[string length $data]} {
        set match {}
        set tag {}
        set att {}
        set dat {}
        set end {}

        if {[regexp -nocase $ilwd::begcontainer $data match tag att]  ||
            [regexp -nocase $ilwd::endcontainer $data match tag]   ||
            [regexp -nocase $ilwd::external $data match tag att dat end] ||
            [regexp -nocase $ilwd::element $data match tag att dat end]} {
        }

        set tag [ string trim $tag ]
        set att [ string trim $att ]
        set dat [ string trim $dat ]
        set end [ string trim $end ]
        regsub -all "\"" $att "\\\"" att

        switch -exact -- [string tolower $tag] {
            "ilwd" {
                set nextlevel [expr {$level + 1}]
                if {![info exists elem($nextlevel)]} {
                    set elem($nextlevel) 0
                }
                set contents($level,$elem($level)) [list $tag $att [list $elem($nextlevel)]]
                incr level
            }

            "/ilwd" {
                set prevlevel [expr {$level - 1}]
                foreach {tag att elemrange} $contents($prevlevel,$elem($prevlevel)) {break}
                lappend elemrange [expr {$elem($level) - 1}]
                set contents($prevlevel,$elem($prevlevel)) [list $tag $att $elemrange]
                incr level -1
                incr elem($level)
            }

            default {
                if {![ string equal -nocase $tag $end ]} {
                    set msg "ILWD Parser: start tag '$tag' and end tag '$end' do not match."
                    return -code error $msg
                }

                if {![ regexp -nocase $tag $ilwd::tags ]} {
                    set msg "ILWD Parser: start tag '$tag' is not a valid ILWD tag."
                    return -code error $msg
                }

                set contents($level,$elem($level)) [ list $tag $att $dat ]
                incr elem($level)
            }
        }

        set x [string first $match $data]
        set y [expr {$x + [string length $match] - 1}]
        set data [string replace $data $x $y]
    }

    return [ array get contents ]
}
## ********************************************************

## ********************************************************
##
## Name: ilwd::parseattr
##
## Description:
## Converts a list of attributes parsed from a start tag
## into a Tcl list of attribute/value pairs and returns
## the list.
##
## Usage:
##
## Comments:
## This proc is NOT called by the parseILWD routine,
## it is here for convenience to read the attribute lists
## returned by ilwd::parse.

proc ilwd::parseattr { { data "" } } {
    if {![ string length $data ]} {
        return {}
    }
    set attrlist [ list ]
    ;## preprocess
    regsub -all {\s+=}  $data "="  data
    regsub -all {=\s+}  $data "="  data
    while {[ regexp $ilwd::attr $data match att val ]} {
        set att [ string trim $att ]
        set val [ string trim $val ]

        #if {![regexp -nocase $att $ilwd::attrs]} {
        #    set msg "ILWD Parser: attribute '$att' is not a valid ILWD attribute."
        #    return -code error $msg
        #}

        lappend attrlist $att $val

        set x [string first $match $data]
        set y [expr {$x + [string length $match] - 1}]
        set data [string replace $data $x $y]
    }

    return $attrlist
}
## ********************************************************

## ********************************************************
##
## Name: ilwd::extract
##
## Description:
##
## Usage:
##
## Comments:
##

proc ilwd::extract {{struct ""} {branch ""} {levels 0}} {
    array set tree $struct

    if {[string length $branch]} {
        if {![string equal "ilwd" [lindex $tree($branch) 0]]} {
            return -code error "ILWD Extract: '$branch' is not a valid branch."
        }
        set level [expr {[lindex [split $branch ","] 0] + 1}]
        foreach {start end} [lindex $tree($branch) 2] {break}
    } else {
        set level 0
        set start 0
        set end [expr {[llength [array names tree "$level,*"]] - 1}]
        set levels 0
    }

    return [ilwd::ExtractCont tree $level $start $end $levels]
}

proc ilwd::ExtractCont {arrName level start end levels} {
    upvar $arrName tree
    set elemlist [list]
    incr levels -1

    for {set elem $start} {$elem <= $end} {incr elem} {
        foreach {tag att dat} $tree($level,$elem) {break}
        if {[string equal "ilwd" $tag]} {
            if {$levels != 0} {
                set elemlist [concat $elemlist [eval ilwd::ExtractCont tree [expr {$level + 1}] $dat $levels]]
            }
        } else {
            set attrlist [ilwd::parseattr $att]
            lappend elemlist [list $tag $attrlist $dat]
        }
    }

    return $elemlist
}
## ********************************************************

proc ilwd::dump { {struct ""} {chan stdout} } {
    array set tree $struct

    if {[catch {puts $chan "<?ilwd?>"} err]} {
        return -code error "Error writing to channel $chan: $err"
    }

    set level0 [array names tree {0,*}]
    ilwd::DumpCont tree $chan 0 0 [expr {[llength $level0] - 1}]
}

proc ilwd::DumpCont {arrName chan level start end} {
    upvar $arrName tree
    set space "    "

    for {set elem $start} {$elem <= $end} {incr elem} {
        foreach {tag att dat} $tree($level,$elem) {break}
        set indent [string repeat $space $level]
        if {[string equal "ilwd" $tag]} {
            puts $chan "$indent<ilwd [concat $att]>"
            eval ilwd::DumpCont tree $chan [expr {$level + 1}] $dat
            puts $chan "$indent</ilwd>"
        } else {
            puts $chan "$indent<$tag [concat $att]>$dat</$tag>"
        }
    }
}

