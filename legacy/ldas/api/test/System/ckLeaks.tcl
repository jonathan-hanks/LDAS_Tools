#! /ldcg/bin/tclsh
# e.g. ckLeaks.tcl client6 2

if	{ $argc != 3 } {
	puts "ckLeaks.tcl <client> <log page number> <from-to>"
	exit
}

set client [ lindex $argv 0 ]
set loglevel [ lindex $argv 1 ]
set range [ lindex $argv 2 ]
source /ldas_outgoing/cntlmonAPI/cntlmon.state

;## gpstime range
regexp {(\d+)-(\d+)} $range -> start_time end_time ] 
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/jobstats_archive/job_stats.log.* ] ] {
			regexp {(\d+)$} [ file tail $file ] -> jobendtime 
			if	{ $jobendtime < $start_time } {
				continue
		 	}
			if	{ $jobendtime > $end_time } {
				lappend filelist $file 
				set done 1
				break 
			} else {
				lappend filelist $file
			}
		}
		if	{ ! $done } {
			lappend filelist [ file join $::LDASLOG job_stats.log ]
		}

puts "files $filelist"		
;## catch { exec [ glob /ldas_outgoing/logs/archive/jobstats_archive/job_stats.log.8* ] | head -10 } filelist

set ftimes [ list ]
foreach line [ split $filelist \n ] {
	if	{ [ regexp {job_stats.log.(\d+)} $line -> ftime ] } {
		lappend ftimes $ftime
	}
}

set logfile /ldas_outgoing/logs/cmonClient/${client}_Log-Filter-${loglevel}_Page1.html

set fd [ open $logfile r ]
set data [ read -nonewline $fd  ]
close $fd

foreach line [ split $data \n ] {
	if	{ [ regexp {<b>(\d+)</b>.+(LDAS-DEV\d+)} $line -> time jobid ] } {
		foreach file $ftimes {		
			if	{ $time > $file } {
				break
			} else {
				set found $file
			}
		}
		set fname /ldas_outgoing/logs/archive/jobstats_archive/job_stats.log.$found
		# puts "grep $fname for $jobid, $time "
		set rc [ catch { exec grep -i ${jobid} $fname } data ]
        if   { !$rc } {
			 puts $data
		}
	}
}
