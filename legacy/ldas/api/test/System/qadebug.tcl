package provide qadebug 1.0

package require qaoptions

namespace eval QA::Debug {

    variable Level 0
    variable AutoFlush 0

    ;##------------------------------------------------------------------
    ;## Force accumulated output to be written
    ;##------------------------------------------------------------------
    namespace export Flush
    proc Flush {} {
	::flush [set ::tcltest::outputChannel]
	return
    }
    ;##------------------------------------------------------------------
    ;## Procedure to output a message to the output channel
    ;##------------------------------------------------------------------
    namespace export Puts
    proc Puts {OutputLevel Message} {
	variable Level
	variable AutoFlush

	if {$Level >= $OutputLevel} {
	   ::puts [set ::tcltest::outputChannel] $Message
	}
	if {$AutoFlush} {
	    Flush
	}
	return
    }
} ;## namespace QA::Debug
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register package specific options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::QA::Options::Add {} {--qa-debug-level} {string} \
    {Level of debugging output} \
    { set ::QA::Debug::Level $::QA::Options::Value }
::QA::Options::Add {} {--qa-debug-auto-flush} {string} \
    {Boolean controlling if debugging information is immediately flushed} \
    { set ::QA::Debug::AutoFlush $::QA::Options::Value }
