package provide qaS4 1.0

namespace eval QA {
    set CurrentDataSet S4

    set DataSet(${CurrentDataSet},start) 793130413
    set DataSet(${CurrentDataSet},dt) 32
    foreach site [list H L] {
	foreach level [list 1 3] {
	    set DataSet(${CurrentDataSet},$site,level$level) \
		[ ::QA::LoadChannelInfo \
		      adcdecimate_${site}-RDS_R_L${level}-${CurrentDataSet}.txt
		 ]
	} ;# foreach - level
    } ;# foreach - site

    set CurrentDataSet S3

} ;# namespace - QA
