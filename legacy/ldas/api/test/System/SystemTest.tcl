#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Script for running suite of System tests
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
package require LDAS
package require qaprocess

namespace import ::QA::Process::SpawnInBackground
namespace import ::QA::Process::ProcessCB
namespace import ::QA::Process::Wait
namespace import ::QA::Process::Walltime
;##---------------------------------------------------------------------
;## :WARNING: DebugMode should be turned off unless trying to debug
;## :WARNING:   the event processing
;##---------------------------------------------------------------------
set DebugMode 0 ;## Only used for debugging
;##---------------------------------------------------------------------
;## Establish some variable that will be used
;##---------------------------------------------------------------------
set TestOptions $::argv
set pos [lsearch -exact $TestOptions -tmpdir]
if {$pos >= 0} {
    set ::SystemTestDir [lindex $TestOptions [incr pos]]
} else {
    set ::SystemTestDir "/ldas_outgoing/tmp"
}
set ::SystemTestDir [file join $::SystemTestDir [join [list SystemTest $::LDAS::Info(VERSION) [clock format [clock seconds] -format "%Y.%m.%d.%T"]] "-" ] ]
if {$pos >= 0} {
    set TestOptions [lreplace $TestOptions $pos $pos $::SystemTestDir]
} else {
    lappend TestOptions -tmpdir $::SystemTestDir
}
;##---------------------------------------------------------------------
;## Structure containing the list of commands to execute
;##---------------------------------------------------------------------
array set Commands {
    RDSVerify:script		   RDSVerify.tcl 
    complex_data:script            complex_data.tcl
    compresstest:script            compresstest.tcl
    crawlink:script                crawlink.tcl
    crawlink:args                  {--resultdir /ldas_outgoing/logs --baseurl http://www.ldas-dev.ligo.caltech.edu}
    createRDSTest_S3:script	   createRDSTest_S3.tcl
    datacondMDC:script	           datacondMDC.tcl
    datasockTest:script            datasockTest.tcl
    datasockTest:args              {--hostfile /ldas_usr/ldas/test/datasock/hostfile_tclchannel --ilwddir /ldas_usr/ldas/mlei/datasock}
    dbntuple:script	           dbntuple.tcl
    dbperformance-run:script       dbperformance.tcl
    dbperformance-run:args	   {--dbperformance-modes test}
    dbperformance-validate:script  dbperformance.tcl
    dbperformance-validate:args	   {--dbperformance-modes validate}
    dbperformance-stats:script     dbperformance.tcl
    dbperformance-stats:args	   {--dbperformance-modes statistics}
    dbqchannel:script	   	   dbqchannel.tcl
    dbquery:script	           dbquery.tcl 
    dbspectrum:script	           dbspectrum.tcl
    dc2frame:script	           dc2frame.test
    dcmean:script	           dcmean.tcl
    dbException:script	           dbException.tcl
    dbException:args	           {--database dev_1}
    descMetaData:script            descMetaData.tcl           
    dtcheck:script                 dtcheck.tcl
    fracc6:script                  fracc6.tcl
    frtest:script                  frtest.tcl
    frgap-16:script                frgap.tcl
    frgap-16:args                  {--frgap-window-size 16}
    frgap-18:script                frgap.tcl
    frgap-18:args                  {--frgap-window-size 18}
    frgap-48:script                frgap.tcl
    frgap-48:args                  {--frgap-window-size 48}
    frgap-80:script                frgap.tcl
    frgap-80:args                  {--frgap-window-size 80}
    fr2ilwd:script		   fr2ilwd.tcl
    fr2ilwd:args		   {--doloc 1 --donfs 0 --data-output-dir /ldas_usr/ldas/test/frame} 
    geotest:script                 geotest.tcl
    lsyncTest:script               lsyncTest.tcl
    lsyncTest:args                 {--iterations 10}
    ndastest:script		   ndastest.tcl
    ndastest:args	   	   {--database ldas_tst}
    procReadWrite:script           procReadWrite.tcl
    putStandAlone:script           putStandAlone.tcl
    putStandAlone:args             {--runtimes 3 --database dev_1}
    regression:script              regression.tcl
    regression:args                {--testThrottle 5}
    wrapperstate:script	           wrapperstate.tcl	
}

;##---------------------------------------------------------------------
;## Show the order in which to run the jobs
;##---------------------------------------------------------------------
set Seperator ,
set KeySortOption -integer
;## see if user has specified a job order

set pos [lsearch -exact $TestOptions --joborderfile]
if {$pos >= 0} {
    set joborderfile [lindex $TestOptions [incr pos]]
    if  { [ file exist $joborderfile ] } {
        source $joborderfile
    } else {
        puts "Job order file $joborderfile does not exist, exiting"
        exit
    }
} else {
    array set JobOrder {
    1,1,1       regression
    1,2,1       procReadWrite
    1,2,2       geotest
    1,2,3       dc2frame
    1,2,4       dcmean
    1,3,5	dbntuple
    1,3,6	dbqchannel
    1,3,7	dbquery
    1,4,8	dbspectrum
    1,4,9 	wrapperstate
    1,4,10      frgap-16
    2,1,1       compresstest
    2,1,2       complex_data
    2,1,3       dtcheck 
    2,1,4       frgap-18   
    3,1         frgap-48
    3,2         frgap-80
    3,3 	fracc6
    3,4 	frtest
    4,1,1       descMetaData
    4,1,2       dbException
    4,2         putStandAlone
    5,1,1       datacondMDC
    5,2,2       dbperformance-run
    5,2,3       dbperformance-validate
    5,2,4       dbperformance-stats
    5,3	        createRDSTest_S3
    6,1,1	ndastest
    6,1,2       datasockTest
    6,2,1       crawlink
    7,1,1       fr2ilwd
    8,1         RDSVerify
    } ;# JobOrder
}

if {$::DebugMode} {
    array set ::Commands {
	procReadWrite:script   /usr/bin/sleep
	procReadWrite:args     {1}
	fracc6:script          /usr/bin/sleep
	fracc6:args            {19}
	dtcheck:script         /usr/bin/sleep
	dtcheck:args           {10}
	compresstest:script    /usr/bin/sleep
	compresstest:args      {20}
	frtest:script          /usr/bin/sleep
	frtest:args            {17}
	frgap-16:script        /usr/bin/sleep
	frgap-16:args          {33}
	frgap-18:script        /usr/bin/sleep
	frgap-18:args          {31}
	frgap-48:script        /usr/bin/sleep
	frgap-48:args          {23}
	frgap-80:script        /usr/bin/sleep
	frgap-80:args          {23}
    }
    set ::TestOptions ""
}

;##:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
;## Site specific configurations
;##:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
set pos [lsearch -exact $TestOptions --site]
if {$pos >= 0} {
    set ::SITE [lindex $TestOptions [incr pos]]
}
if {[info exists ::SITE]} {
    if {[string compare [file dirname $argv0] "." ] == 0} {
	set base ..
    } else {
	set base [file dirname [file dirname $argv0]]
    }
    set resource [file join $base share ldas system_tests site_config ${::SITE}.tcl]
    if [file exists $resource] {
	source $resource
    }
}

;##---------------------------------------------------------------------
;## Prepare for the worst
;##---------------------------------------------------------------------
proc bgerror {err} {
    set trace {}
    append ::out "\nError: $err"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
	puts stderr "*** Stack Trace ***\n$trace"
    }	
    exit
}
set ::StartKey {\#Start\#}
set ::RunGroupIndex 0
set ::TaskIndex 0
array set TaskStateGraph {}
;##---------------------------------------------------------------------
;##---------------------------------------------------------------------
;##---------------------------------------------------------------------
;##---------------------------------------------------------------------
proc FormatCommand {Task} {
    variable Commands
    variable TestOptions

   if {[string compare [file pathtype $Commands($Task:script)] relative] \
	    == 0} {
       set prefix [file dirname $::argv0]
    } else {
	set prefix {}
    }
    return "[file join $prefix $Commands($Task:script)] $TestOptions $Commands($Task:args)"
}
;##---------------------------------------------------------------------
;##---------------------------------------------------------------------
proc LogNameCompare {a b} {
    return [string compare [lindex $a 1] [lindex $b 1]]
}

proc GroupCompare {a b} {
    global Seperator
    set alist [split $a $Seperator]
    set blist [split $b $Seperator]
    return [expr [lindex $alist end] - [lindex $blist end]]
}

proc SortGrouping {Grouping} {
    upvar $Grouping grouping

    set grouping [lsort -command GroupCompare -unique $grouping]
}

proc TaskStateCreateParents {TaskKey Serial} {
    global Seperator
    global StartKey
    global TaskStateGraph

    if {[string compare $TaskKey $StartKey] == 0} {
	return
    }

    if {[catch {set parentkey [ParentKey $TaskKey]}]} {
	return
    }
    ;##------------------------------------------------------------------
    ;## Add back reference to the parent group
    ;##------------------------------------------------------------------
    if { ![info exists TaskStateGraph($parentkey,Grouping)]} {
	set TaskStateGraph($parentkey,Grouping) [list]
	set TaskStateGraph($parentkey,Serial) [expr ! $Serial]
    }
    lappend TaskStateGraph($parentkey,Grouping) $TaskKey
    if {[string compare $parentkey $StartKey] != 0} {
	TaskStateCreateParents $parentkey [expr ! $Serial]
    }
}

proc GenTaskStateGraph {} {
    ;##------------------------------------------------------------------
    ;## Declaration of variables outside local scope
    ;##------------------------------------------------------------------
    global JobOrder
    global TaskStateGraph
    global Seperator
    global StartKey
    ;##------------------------------------------------------------------
    ;## Initialize the TaskStateGraph
    ;##------------------------------------------------------------------
    array set TaskStateGraph {}
    ;##------------------------------------------------------------------
    ;## Remove anything that looks like a comment
    ;##------------------------------------------------------------------
    foreach x [concat [array names JobOrder {\#*}] \
		   [array names JobOrder {;\#*}]] {
	unset JobOrder($x)
    }
    ;##------------------------------------------------------------------
    ;## Loop over all depths of the JobArray
    ;##------------------------------------------------------------------
    foreach key [array names JobOrder {*}] {
	set key_parts [split $key $Seperator]
	set serial [expr [llength $key_parts] % 2]
	if {[llength $key_parts] == 1} {
	    ;#----------------------------------------------------------
	    ;# This is the start of tasks
	    ;#----------------------------------------------------------
	    set taskkey $StartKey
	} else {
	    set last [expr [llength $key_parts] - 1]
	    set taskkey [join [lreplace $key_parts $last end] $Seperator]
	}
	if { ![info exists TaskStateGraph($taskkey,Grouping)]} {
	    set TaskStateGraph($taskkey,Grouping) [list]
	    set TaskStateGraph($taskkey,Serial) $serial
	}
	lappend TaskStateGraph($taskkey,Grouping) $key
	;##--------------------------------------------------------------
	;## Create back references as needed
	;##--------------------------------------------------------------
	TaskStateCreateParents $taskkey $serial
    }
    ;##------------------------------------------------------------------
    ;## Fix up the information
    ;##------------------------------------------------------------------
    foreach key [array names TaskStateGraph {*,Grouping}] {
	SortGrouping TaskStateGraph($key)
    }
}

proc TaskStateSpawnNext {TaskKey} {
    global RunTaskStateGraphDone
    global TaskStateGraph
    global StartKey

    set serial $TaskStateGraph(${TaskKey},Serial)
    if {$serial} {
	if {![info exists TaskStateGraph(${TaskKey},Offset)]} {
	    ;##----------------------------------------------------------
	    ;## Just starting this set of serial tasks
	    ;##----------------------------------------------------------
	    set TaskStateGraph(${TaskKey},Offset) -1
	}
	incr TaskStateGraph(${TaskKey},Offset)
	if {$TaskStateGraph(${TaskKey},Offset) \
		>= [llength $TaskStateGraph(${TaskKey},Grouping)]} {
	    ;##----------------------------------------------------------
	    ;## Completed this goup of serial tasks
	    ;##----------------------------------------------------------
	    if {[string compare $TaskKey $StartKey] == 0} {
		;##------------------------------------------------------
		;## Everything is finished
		;##------------------------------------------------------
		set RunTaskStateGraphDone 1
		return
	    } else {
		;##------------------------------------------------------
		;## This group of serial tasks has completed.
		;##   Let the parent know
		;##------------------------------------------------------
		set parent [ParentKey $TaskKey]
		TaskStateSpawnNext $parent
	    }
	} else {
	    ;##----------------------------------------------------------
	    ;## Start the next serial task
	    ;##----------------------------------------------------------
	    Start [lindex $TaskStateGraph(${TaskKey},Grouping) \
		       $TaskStateGraph(${TaskKey},Offset)] \
		$TaskKey
	}
    } else {
	;##--------------------------------------------------------------
	;## Run parallel tasks
	;##--------------------------------------------------------------
	if {![info exists TaskStateGraph(${TaskKey},Countdown)]} {
	    ;##----------------------------------------------------------
	    ;## Just starting this set of parallel tasks
	    ;##----------------------------------------------------------
	    set TaskStateGraph(${TaskKey},Countdown) \
		[llength $TaskStateGraph(${TaskKey},Grouping)]
	    Start $TaskStateGraph(${TaskKey},Grouping) $TaskKey
	} else {
	    incr TaskStateGraph(${TaskKey},Countdown) -1
	    if {$TaskStateGraph(${TaskKey},Countdown) == 0} {
		;##------------------------------------------------------
		;## This group of parallel tasks has completed.
		;##   Let the parent know
		;##------------------------------------------------------
		set parent [ParentKey $TaskKey]
		TaskStateSpawnNext $parent
	    }
	}
    }
}

proc GroupKey {TaskKey} {
    global Seperator
    global StartKey

    set key_parts [split $TaskKey $Seperator]
    set len [llength $key_parts]

    if {$len == 0} {
	set groupkey $StartKey
    } else {
	set groupkey [join [lreplace $key_parts [incr len -1] end] $Seperator]
    }
    return $groupkey
}

proc ParentKey {TaskKey} {
    global Seperator
    global StartKey

    set key_parts [split $TaskKey $Seperator]
    set len [llength $key_parts]

    if {$len == 0} {
	return -code error
    } elseif {$len == 1} {
	set parentkey $StartKey
    } else {
	set parentkey [join [lreplace $key_parts [incr len -1] end] $Seperator]
    }
    return $parentkey
}

proc Finished {Task} {
    global TaskStateGraph

    if {[catch {set groupkey [ParentKey $Task]}]} {
	return -code error "Task has no parent"
    }
    TaskStateSpawnNext $groupkey
}

proc Start {Tasks Key} {
    global TaskStateGraph
    global JobOrder
    global Commands

    foreach task $Tasks {
	if {[info exists TaskStateGraph($task,Grouping)]} {
	    ;##----------------------------------------------------------
	    ;## Need to launch a group
	    ;##----------------------------------------------------------
	    TaskStateSpawnNext $task
	} else {
	    ;##----------------------------------------------------------
	    ;## Spawn an actual job
	    ;##----------------------------------------------------------
	    set taskname $JobOrder($task)
	    set cmd [FormatCommand $taskname]
	    set pid [SpawnInBackground $cmd]
	    set Commands($taskname:pid) $pid
	    ProcessCB $pid [subst "::Finished $task"]	    
	}
    }
}

proc RunTaskStateGraph {} {
    global StartKey
    global TaskStateGraph
    global RunTaskStateGraphDone

    if {[llength [array get TaskStateGraph]] > 0} {
	set RunTaskStateGraphDone 0
	TaskStateSpawnNext $StartKey
	vwait RunTaskStateGraphDone
    }
}
;##---------------------------------------------------------------------
;## This is the main call to run all of the tests
;##---------------------------------------------------------------------
proc Run {} {
    GenTaskStateGraph
    RunTaskStateGraph
}
;##---------------------------------------------------------------------
;## Ensure the logging directory exits
;##---------------------------------------------------------------------
catch {[file delete -force $::SystemTestDir]} err ;## Remove all files
catch {[file mkdir $::SystemTestDir]} err
;##---------------------------------------------------------------------
;## Open the log file
;##---------------------------------------------------------------------
if {[catch {set fid [open [file join $::SystemTestDir summary_report.txt] w+]} err]} {
    ;##------------------------------------------------------------------
    ;## Fatal condition
    ;##------------------------------------------------------------------
    puts stderr "ERROR: Could not open the summary report file: $err"
    exit 1
}
;##----------------------------------------------------------------------
;## Log startup information
;##----------------------------------------------------------------------
puts $fid "Command: $argv0 $argv"
puts $fid "Started:  [clock format [clock seconds] -format {%a %b %e %T %Z %Y}]"
;##----------------------------------------------------------------------
;## Fill in standard information
;##----------------------------------------------------------------------
if {! $::DebugMode} {
    foreach s [array names Commands {*:script}] {
	regsub -- {^(.*):script$} $s {\1} testname
	if {[info exists Commands($testname:args)]} {
	    set pos [lsearch -exact $Commands($testname:args) -outfile]
	    if {$pos < 0} {
		;##------------------------------------------------------
		;## Log file option has not been specified
		;##------------------------------------------------------
		lappend Commands($testname:args) -outfile ${testname}.log
	    }
	} else {
	    set Commands($testname:args) [list -outfile ${testname}.log]
	}
    }
}
;##----------------------------------------------------------------------
;## :TODO: Run through the tests
;##----------------------------------------------------------------------
Run
;##----------------------------------------------------------------------
;## Generate a list of when jobs ran
;##----------------------------------------------------------------------
set jobs [list]
foreach x [array names Commands {*:pid}] {
    regsub {^(.*):pid$} $x {\1} x
    lappend jobs [list $Commands($x:pid) $x]
}
set jobs [lsort -index 0 -integer $jobs]
puts $fid {}
if {[llength $jobs] > 0} {
    puts $fid [format {%20.20s %10.10s %-20.20s  %-20.20s} \
		   "TASK" "PID" "START" "STOP"]
    foreach x $jobs {
	set pid [lindex $x 0]
	set job [lindex $x 1]
	set start $::QA::Process::ProcessTable($pid,Start)
	set stop $::QA::Process::ProcessTable($pid,Stop)
	puts $fid [format {%20.20s %10d %20.20s  %20.20s} $job $pid \
		       [clock format $start -format {%Y.%m.%d.%T}] \
		       [clock format $stop -format {%Y.%m.%d.%T}] ]
    }
    puts $fid {}
}
;##----------------------------------------------------------------------
;## The outer loop determins which commands actually ran
;##----------------------------------------------------------------------
set logfiles [list]
foreach x [array names Commands {*:pid}] {
    regsub {(.*):pid$} $x {\1} x
    if [info exists Commands($x:args)] {
	set pos [lsearch -exact $Commands($x:args) -outfile]
	if {$pos < 0} {
	    continue
	}
	lappend logfiles [list $x \
			      [lindex $Commands($x:args) [incr pos]]]
    }
}
if {[llength $logfiles] > 0} {
    set logfiles [lsort -command LogNameCompare -increasing $logfiles]
    set hdr {%20.20s: %7.7s %7.7s %7.7s %7.7s %10.10s}
    set form {%20.20s: %7d %7d %7d %7d %10.10s}
    puts $fid [format $hdr LOG TOTAL PASS SKIP FAIL {WALL TIME}]
    set ttotal 0
    set tpass 0
    set tskip 0
    set tfail 0
    foreach loginfo $logfiles {
	set key [lindex $loginfo 0]
	set log [lindex $loginfo 1]
	if {[catch {set lid [open [file join $::SystemTestDir $log]]} err]} {
	    puts $fid "ERROR: Unable to open log file: $log: $err"
	    continue
	}
	set walltime 0
	if {[info exists Commands($key:pid)]} {
	    set walltime [Walltime $Commands($key:pid)]
	}
	while {[gets $lid line] >= 0 } {
	    if {[regexp -- {^.+:.+Total\s+(\d+)\s+Passed\s+(\d+)\s+Skipped\s+(\d+)\s+Failed\s+(\d+)\s*$} $line => total pass skip fail]} {
		puts $fid [format $form $log $total $pass $skip $fail [clock format $walltime -format {%T} -gmt 1]]
		incr ttotal $total
		incr tpass $pass
		incr tskip $skip
		incr tfail $fail
	    }
	}
	close $lid
    }
    puts $fid "================================================================"
    puts $fid [format $form {*** TOTALS ***} $ttotal $tpass $tskip $tfail ""]
} ;## if 
;##----------------------------------------------------------------------
;## Close out the summary report
;##----------------------------------------------------------------------
puts $fid "Finished: [clock format [clock seconds] -format {%a %b %e %T %Z %Y}]"
close $fid
exit 0
