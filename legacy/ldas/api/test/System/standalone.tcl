#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
exec tclsh "$0" ${1+"$@"}

lappend ::auto_path /ldas/lib
if	{ ! [ regexp "/ldas/bin" $::env(PATH) ] } {
	set ::env(PATH) "/ldas/bin:$::env(PATH)"
}
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exist [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

set ::HOST [info hostname]
set ::PORT 10001
set ::SITEFILE "/etc/ldasname"
set ::LAMBHOST "lam-bhost.def"
set ::WRAPPER_RSC "LDASwrapper.rsc"
set ::CMDDIR "/ldas_usr/ldas/test/loop"
set ::VLEVEL 0
set ::REMOTE 0
set ::USEGRID 0
set ::PROTOCOL "/"
set ::METADATAAPI "ligolw"
set ::MDDTARGET "ligolw"
set ::USERTAG ""
set ::OUTPUTFORMAT {ilwd binary}

if	{ [ string equal beowulf $::HOST ] } {
	set ::LAMBOOTCMD /usr/bin/lamboot
	set ::MPIRUNCMD /usr/bin/mpirun
} else {
	set ::LAMBOOTCMD /ldcg/bin/lamboot
	set ::MPIRUNCMD /ldcg/bin/mpirun
}
set ::DSOLIST [list]
# set ::DSOALL {inspiral.cmd stochastic.cmd tfclusters.cmd slope.cmd stackslide.cmd knownpulsardemod.cmd}

;## does not work on waveburst for dataStandAlone via grid
set host [ exec uname -n ]
if   	{ [ string match beowulf $::HOST ] } {
	set ::DSOALL {burstwrapper.rds inspiral.rds stochastic.rds tfclusters.rds slope.rds stackslide.cmd knownpulsardemod.rds waveburst.rds }
} else {
	set ::DSOALL {inspiral.rds stochastic.rds tfclusters.rds slope.rds stackslide.cmd knownpulsardemod.rds }
}

set ::DATABASE {ldas_tst}

set ::schema {
    -np [set ${dso}(-np)]
    /ldas/bin/wrapperAPI
    -nodelist="(1-[expr [set ${dso}(-np)]-1])"
    -dynlib="/lal/lib/lalwrapper/[set ${dso}(-dynlib)]"
    -filterparams="[set ${dso}(-filterparams)]"
    -doLoadBalance=FALSE
    -jobID=$jobnum
    -inputFile="$inputfile"
    -userTag="$::USERTAG"
}

set ::DSAcmd {
    dataStandAlone
        -subject {[set ${dso}(-subject)]}
        -returnprotocol {[set ${dso}(-returnprotocol)]}
        -outputformat {$::OUTPUTFORMAT}
        -database {$::DATABASE}
        -framequery {[set ${dso}(-framequery)]}
        -responsefiles {[set ${dso}(-responsefiles)]}
        -aliases {[set ${dso}(-aliases)]}
        -algorithms {[set ${dso}(-algorithms)]}

        $::extra
}

set ::PSAcmd {
    putStandAlone
        -subject {[set ${dso}(-subject)]}
        -metadataapi {$::METADATAAPI}
        -multidimdatatarget {$::MDDTARGET}
        -returnprotocol {http://${dso}_result}
        -wrapperdata {[set ${dso}(-wrapperdata)]}
        -database {$::DATABASE}
}

set ::DPLcmd {
    dataPipeline
        -subject {[set ${dso}(-subject)]}
        -returnprotocol {http://${dso}_result}

        -np {[set ${dso}(-np)]}
        -dynlib {[set ${dso}(-dynlib)]}
        -filterparams {[set ${dso}(-filterparams)]}
        -usertag {$::USERTAG}

        -database {$::DATABASE}
        -metadataapi {$::METADATAAPI}
        -multidimdatatarget {$::MDDTARGET}

        -framequery {[set ${dso}(-framequery)]}
        -responsefiles {[set ${dso}(-responsefiles)]}
        -aliases {[set ${dso}(-aliases)]}
        -algorithms {[set ${dso}(-algorithms)]}

        $::extra
}

proc vputs {msg1 {msg2 ""} args} {
    if {$::VLEVEL > 0} {
        puts stdout $msg1
    } elseif {[string length $msg2]} {
        puts stdout $msg2
    }
    flush stdout

    return ""
}

proc sendJob {site cmd {varlist ""}} {
    vputs $cmd
    catch {LJrun job -nowait -manager $site $cmd} errmsg
    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg "$::job(error)"
        }
        LJdelete job
        return -code error "Error from LJrun: [string trim $errmsg]"
    }

    puts "$::job(jobid) ($::job(LDASVersion))"

    catch {LJwait job} errmsg
    if {$LJerror} {
        if {[info exists ::job(error)]} {
            set errmsg "$::job(error)"
        }
        LJdelete job
        return -code error "Error from LDAS: [string trim $errmsg]"
    }

    puts [strip $::job(jobReply)]

    set retval [list]
    foreach var $varlist {
        lappend retval $::job($var)
    }
    LJdelete job

    return $retval
}

proc strip {data} {
    regsub -all -- {[\n\s\t]+} $data { } data
    set idx [string first "=====" $data]
    if {$idx >= 0} {
        set data [string replace $data $idx end]
    }
    return [string trim $data]
}

proc guessSite {args} {
    if {[file exists $::SITEFILE]} {
        set fid [open $::SITEFILE r]
        gets $fid line
        close $fid

        set site [string trim $line]
        set code [string toupper $site]

        regexp {\-(\w+)$} $site -> site

        switch -exact -- $site {
            la {set site llo}
            wa {set site lho}
        }
    } else {
        set site "${::HOST}:${::PORT}"
        set code [string toupper $::HOST]
    }

    set ::SITE $site
    set ::RUNCODE $code
    return
}

proc runLamboot {args} {
    set ::env(LAMBHOST) "[pwd]/$::LAMBHOST"
    set ::env(WRAPPER_RESOURCE_FILE) "[pwd]/"

    puts "-Writing $::LAMBHOST"
    set fid [open $::LAMBHOST w]
    puts $fid [info hostname]
    close $fid

    set fid [open /ldas/bin/$::WRAPPER_RSC r]
    set data [read -nonewline $fid]
    close $fid

    regsub -line -- {^enable_mpiAPI.*$} $data {enable_mpiAPI FALSE} data
    regsub -line -- {^enable_resultAPI.*$} $data {enable_resultAPI FALSE} data
    #regsub -line -- {^enable_ldas_trace.*$} $data {enable_ldas_trace TRUE} data
    regsub -line -- {^dump_data_directory.*$} $data {dump_data_directory /ldas_outgoing/jobs} data
    regsub -line -- {^run_code.*$} $data "run_code $::RUNCODE" data
    
    puts "-Writing $::WRAPPER_RSC"
    set fid [open $::WRAPPER_RSC w]
    puts $fid $data
    close $fid

    puts "-Starting lamboot"
    exec $::LAMBOOTCMD -s >@stdout
}

proc readCmd {dsofile} {
    if {![file isdirectory $::CMDDIR]} {
        set ::CMDDIR [pwd]
    }
    set dso [file rootname $dsofile]
    set dsofile [file join $::CMDDIR ${dsofile}]

    if {[catch {
        set fid [open $dsofile r]
        set data [read -nonewline $fid]
        close $fid
    } err ]} {
        catch {close $fid}
        return -code error "Error: $err"
    }

    ;## strip comments
    foreach line [split $data "\n"] {
        set line [string trim $line]
        if {[regexp {^\s*#} $line]} {
            continue
        }
        lappend tmp $line
    }
    set data [join $tmp "\n"]
    regsub -all -- {[\s\n\t]+} $data { } data

    set data [lrange $data 1 end]
    array set ::$dso $data

    if {![info exists ::${dso}(-np)]} {
        set ::${dso}(-np) 3
    }
    if {![info exists ::${dso}(-responsefiles)]} {
        set ::${dso}(-responsefiles) {}
    }
    if {[info exists ::${dso}(-mddapi)]} {
        set ::${dso}(-multidimdatatarget) [set ::${dso}(-mddapi)]
    }
    if {[info exists ::${dso}(-dynlib)]} {
        if {[string equal "sft" $dso]} {
            set ::${dso}(-dynlib) libldasknownpulsardemod.so
        }
        set ::${dso}(-dynlib) [file tail [set ::${dso}(-dynlib)]]
    }

    set prot "http:/"
    if {$::USEGRID} {
        set prot "gridftp:"
    }
    append prot $::PROTOCOL
    set ::${dso}(-returnprotocol) $prot

    set ::extra [list]
    if {[info exists ::${dso}(-dbspectrum)]} {
        lappend ::extra "-dbspectrum" [ list [set ::${dso}(-dbspectrum)] ]
	puts "extra $::extra"
    }
    if {[info exists ::${dso}(-concatenate)]} {
        lappend ::extra "-concatenate" [ list [set ::${dso}(-concatenate)] ]
	puts "extra $::extra"
    }
    if {[info exists ::${dso}(-allowgaps)]} {
        lappend ::extra "-allowgaps" [ list [set ::${dso}(-allowgaps)] ]
	puts "extra $::extra"
    }
    if {[info exists ::${dso}(-autoexpand)]} {
        lappend ::extra "-autoexpand" [ list [set ::${dso}(-autoexpand)] ]
	puts "extra $::extra"
    }

    return
}

proc cleanup {args} {
    puts "-Cleaning up"
    catch {exec /ldcg/bin/wipe -v}
    #file delete -force $::LAMBHOST
    #file delete -force $::WRAPPER_RSC
    foreach dso $::DSOLIST {
        file delete -force ${dso}.schema
    }
}

proc printUsage {args} {
    exit 1
}

### MAIN ###
set idx [lsearch -glob $::argv "--dump*"]
if {$idx != -1} {
    ;## Special handling for dumping job scripts
    set dumpdir "."
    regexp -- {^--dump=(.+)$} [lindex $::argv $idx] -> dumpdir
    set ::CMDDIR [file join [file dirname $::argv0] loop]

    regsub -line -all {^    } $::DSAcmd {} ::DSAcmd
    regsub {\n\s*\n} $::DSAcmd "\n" ::DSAcmd
    set ::DSOLIST $::DSOALL
    if {[lsearch $::DSOALL knownpulsardemod] == -1} {
        set ::DSOLIST "$::DSOALL knownpulsardemod.cmd"
    }
    foreach dsofile $::DSOLIST {
        set dso [file rootname $dsofile]

        if {[catch {readCmd $dsofile} msg]} {
            return -code error "$msg"
        }

        ;## beautify the algorithm section
        regsub -all {; } [set ${dso}(-algorithms)] ";\n        " ${dso}(-algorithms)
        set ${dso}(-algorithms) "\n        [string trim [set ${dso}(-algorithms)]]\n    "

        ;## beautify the aliases section
        regsub -all {; } [set ${dso}(-aliases)] ";\n        " ${dso}(-aliases)
        set ${dso}(-aliases) "\n        [string trim [set ${dso}(-aliases)]]\n    "

        ;## beautify the responsefiles section
        if {[llength [set ${dso}(-responsefiles)]] > 0} {
            set ${dso}(-responsefiles) [join [set ${dso}(-responsefiles)] "\n        "]
            set ${dso}(-responsefiles) "\n        [string trim [set ${dso}(-responsefiles)]]\n    "
        }

        ;## beautify the framequery section
        set ${dso}(-framequery) [join [set ${dso}(-framequery)] "\}\n        \{"]
        set ${dso}(-framequery) "\n        \{ [string trim [set ${dso}(-framequery)]] \}\n    "

        ;## beautify the dbspectrum section
        if {[info exists ::${dso}(-dbspectrum)]} {
            set ${dso}(-dbspectrum) [join [set ${dso}(-dbspectrum)] "\n        "]
            set ${dso}(-dbspectrum) "\n        [string trim [set ${dso}(-dbspectrum)]]\n    "
        }

        ;## remove extras from kpd
        if {[string equal "knownpulsardemod" $dso]} {
            regsub -line {^.*qc1.*\n} [set ${dso}(-framequery)] {} ${dso}(-framequery)
            regsub -line {^.*qc1.*\n} [set ${dso}(-aliases)] {} ${dso}(-aliases)
            regsub -line {^.*qc1.*\n} [set ${dso}(-algorithms)] {} ${dso}(-algorithms)
        }

        set fid [open [file join $dumpdir ${dso}.cmd] w]
        puts $fid [string trim [subst -nobackslashes [subst -nobackslashes $::DSAcmd]]]
        close $fid
    }
    exit 0
}
package require LDASJob

guessSite
for {set idx 0} {$idx < $::argc} {incr idx} {
    set opt [lindex $::argv $idx]
    switch -glob -- $opt {
        -c {set ::RUNCODE [lindex $::argv [incr idx]]}
        -d {set ::CMDDIR [lindex $::argv [incr idx]]}
        -g {set ::USEGRID 1}
        -m {set ::METADATAAPI [lindex $::argv [incr idx]]}
        -o {set ::OUTPUTFORMAT [lindex $::argv [incr idx]]}
        -p {set ::PROTOCOL [lindex $::argv [incr idx]]}
        -r {set ::REMOTE 1}
        -s {set ::SITE [lindex $::argv [incr idx]]}
        -t {set ::MDDTARGET [lindex $::argv [incr idx]]}
        -T {set ::USERTAG [lindex $::argv [incr idx]]}
        -v {incr ::VLEVEL 1}
        -db {set ::DATABASE [lindex $::argv [incr idx]]}
	-dcmd { set ::DUMP_CMD_2_FILE 1 }
        -* {
            ;## Unknown option
            puts stderr "Error: Invalid option '$opt'."
            printUsage
        }
         default {
            lappend ::DSOLIST $opt
        }
    }
}

if {![llength $::DSOLIST]} {
    set ::DSOLIST $::DSOALL
}

cleanup
if {[catch {runLamboot} msg]} {
    cleanup
    return -code error "$msg"
}

foreach dsofile $::DSOLIST {
    set dso [file rootname $dsofile]
    set ext [ file extension $dsofile ]
    puts "\n*** [string toupper $dso] ***"

    puts "-Reading $dsofile"
    if {[catch {readCmd $dsofile} msg]} {
        cleanup
        return -code error "$msg"
    }

    puts "-Sending dataStandAlone"	
    set dsacmd [subst -nobackslashes $::DSAcmd]

    if	{ [ info exist ::DUMP_CMD_2_FILE ] } {
	set fd [ open ${dso}${ext} w ]
	puts $fd $dsacmd
	close $fd
    }
	
    if {[catch {sendJob $::SITE [subst -nobackslashes $dsacmd] {jobnum outputs outputDir}} msg]} {
        cleanup
        return -code error "$msg"
    }

    foreach {jobnum outputs outputDir} $msg {break;}
    regsub -all -- {((ftp|http)://[^/]*)|(gridftp:)} $outputs {} outputs
    regsub -all -- {((ftp|http)://[^/]*)|(gridftp:)} $outputDir {} outputDir

    set idx [lsearch -regexp $outputs "wrapperdata"]
    if {$idx < 0} {set idx 0}
    set inputfile [lindex $outputs $idx]

    puts "-Writing ${dso}.schema"
    set fid [open ${dso}.schema w]
    regsub -all -- {[\n\s\t]+} $::schema { } ::schema
    regsub -all -- {:} [set ${dso}(-filterparams)] {\:} ${dso}(-filterparams)
    regsub -all -- {'} [set ${dso}(-filterparams)] {} ${dso}(-filterparams)
    
    puts $fid [subst -nobackslashes $::schema]
    close $fid

    puts "-Starting mpirun"
    if {[catch {exec $::MPIRUNCMD -v -O ${dso}.schema >@stdout} msg]} {
    puts "msg $msg"
        cleanup
        return -code error "$msg"
    }

    puts "-Sending putStandAlone"
    set filelist [list]
    if {[catch {
        set filelist process_1.ilwd
        eval lappend filelist [lsort -dictionary [glob -nocomplain {output_[0-9]*.ilwd}]]
        lappend filelist process_2.ilwd
        eval file copy -force $filelist $outputDir
        eval file delete -force $filelist
    } msg ]} {
        cleanup
        return -code error "$msg"
    }

    foreach file $filelist {
        set fileloc "file:"
        if {$::USEGRID} {
            set fileloc "gridftp:"
        }
        append fileloc "${outputDir}/${file}"
        lappend ${dso}(-wrapperdata) $fileloc
    }

    if {[catch {sendJob $::SITE [subst -nobackslashes $::PSAcmd]} msg]} {
        cleanup
        return -code error "$msg"
    }

    puts "-Sending dataPipeline"
    set dplcmd [subst -nobackslashes $::DPLcmd]
    if {[catch {sendJob $::SITE [subst -nobackslashes $dplcmd]} msg]} {
        cleanup
        return -code error "$msg"
    }
}

cleanup
exit 0

