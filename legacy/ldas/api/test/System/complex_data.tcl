#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"
#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
#========================================================================
if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}
#------------------------------------------------------------------------
# Forward declaration of namespace
#------------------------------------------------------------------------
namespace eval ::QA::complex_data::test {
} ;## namespace ::QA::complex_data::test

#------------------------------------------------------------------------
# Load in the Quality assurance package
#------------------------------------------------------------------------
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Register local options here
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qaoptions

::QA::Options::Add {} {--dump} {string} \
    {Directory where to dump script files} \
    {set ::QA::frtest::test::DumpDirectory $::QA::Options::Value}
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Load in Quality Assurance Library
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
package require qalib
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# Initialize the QA package
#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
qaInit

;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
;## Namespace for testing
;##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
namespace eval ::QA::complex_data::test {
    ;##------------------------------------------------------------------
    ;## Bring commonly used items into the local namespace
    ;##------------------------------------------------------------------
    namespace import ::tcltest::*
    namespace import ::QA::Debug::*
    namespace import ::QA::Rexec
    namespace import ::QA::TclTest::SubmitJob

    ;##==================================================================

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## use getFrameData to retrieve complex data in LIGO_LW format
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    set cmd [ concat \
		  "getFrameData" \
		  " -returnprotocol http://results" \
		  " -outputformat \$outputformat" \
		  " -framequery {R H {} 774982800-774982815 Adc(H2:DAQ-FAST_CH0_HDYNE,H2:DAQ-FAST_CH1_HDYNE,H2:DAQ-FAST_CH2_HDYNE,H2:DAQ-FAST_CH3_HDYNE)}" ]
    set outputformat LIGO_LW
    test :[file tail $argv0]:getFrameData:[subst $outputformat]: {} -body {
	set scmd [subst $cmd]
	if {[catch {SubmitJob job $scmd} err]} {
	    return $err
	}
	set job(jobReply)
    } -match regexp -result ".*Your results:\nresults.xml.*"

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## use getFrameData to retrieve complex data in frame format
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    set outputformat frame
    test :[file tail $argv0]:getFrameData:[subst $outputformat]: {} -body {
	set scmd [subst $cmd]
	if {[catch {SubmitJob job $scmd} err]} {
	    return $err
	}
	set output [lindex $job(outputs) 0]
	regsub {^http://[^/]*} $output "" output
	set job(jobReply)
    } -match regexp -result "Your results:\n(\.+.gwf).+(\[^\n\]+)"

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## Verify the frame file with some standard tools
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    test :[file tail $argv0]:FrCheck: {} -body {
	catch { exec FrCheck -i $output } data
	set data
    } -match regexp -result {1 frame\(s\) in file. No read error. File Checksum OK \(.*\)}

    test :[file tail $argv0]:$::FRVERIFY: {} -body {
	catch { exec $::FRVERIFY --verbose $output } data
	set data
    } -match regexp -result "^$output 0\$"

    test :[file tail $argv0]:$::FRDUMP: {} -body {
	catch { exec $::FRDUMP -H -a H2\:DAQ-FAST_CH0_HDYNE -e -l adc $output } data
	set data
    } -match regexp -result "H2:DAQ-FAST_CH3_HDYNE"

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## createRDS base command
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    set cmd [concat "createRDS" \
		 " -times {\$time_range}" \
		 " -type {R}" \
		 " -usertype {}" \
		 " -outputdir {}" \
		 " -usejobdirs {0}" \
		 " -compressiontype {raw}" \
		 " -compressionlevel {6}" \
		 " -filechecksum {0}" \
		 " -frametimecheck {1}" \
		 " -framedatavalid {1}" \
		 " -channels {H2:DAQ-FAST_CH0_HDYNE\$resample H2:DAQ-FAST_CH1_HDYNE\$resample H2:DAQ-FAST_CH2_HDYNE\$resample H2:DAQ-FAST_CH3_HDYNE\$resample}"]

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## createRDS partial frame exception
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    test :[file tail $argv0]:createRDS:partial_result: {} -body {
	set time_range 774982800-774982900
	set resample ""
	set scmd [subst $cmd]
	if {[catch {SubmitJob job $scmd} err]} {
	    return $err
	}
	set job(jobReply)
    } -match regexp -result "yield a final frame file which is shorter in length than the rest"

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## createRDS 
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    test :[file tail $argv0]:createRDS:normal: {} -body {
	set time_range 774982800-774982911
	set resample ""
	set scmd [subst $cmd]
	if {[catch {SubmitJob job $scmd} err]} {
	    return $err
	}
	set job(jobReply)
	# :TODO: Verify the contents of the file
    } -match regexp -result "Your results:\n(H-R\[^\n\]+)\n.+(\[^\n\]+)"

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## createRDS - resample
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    test :[file tail $argv0]:createRDS:resample: {} -body {
	set time_range 774982816-774982895
	set resample "!2"
	set scmd [subst $cmd]
	if {[catch {SubmitJob job $scmd} err]} {
	    return $err
	}
	# :TODO: Verify the contents of the file
	set job(jobReply)
    } -match regexp -result "Your results:\n(H-R\[^\n\]+)\n.+(\[^\n\]+)"


    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## guild command: getChannels
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    test :[file tail $argv0]:getChannels:guild: {} -body {
	set cmd "getChannels -returnprotocol http://daq -interferometer H -frametype R -time 774982800 -metadata 1"
	if {[catch {SubmitJob job $cmd} err]} {
	    return $err
	}
	set job(jobReply)
    } -match regexp -result "Your results:\nH-R-774982800-16.channels"

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## dataPipeline
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    test :[file tail $argv0]:dataPipeline:wrapper: {} -body {
	set cmd { dataPipeline 
	    -subject         {complex data in frames DEMO}
	    -dynlib          libldastrivial.so
	    -filterparams    (262144,7,131072,40.0,0,69.0,8,0.9,1,(30.0,0.0),(300.0,0.0),0,1,0,1,1,(4.0,4.0))
	    -datacondtarget  wrapper
	    -metadatatarget  datacond
	    -multidimdatatarget  ligolw
	    -database        ldas_tst
	    -state           {}
	    -np 3
	    -framequery {
		{ R H {} {774982800-774982900} Adc(H2:DAQ-FAST_CH0_HDYNE,H2:DAQ-FAST_CH1_HDYNE,H2:DAQ-FAST_CH2_HDYNE,H2:DAQ-FAST_CH3_HDYNE)}
	    }
	    -responsefiles {}
	    -aliases {
		gw = H2\:DAQ-FAST_CH0_HDYNE::AdcData;
	    }
	    -algorithms {
		rgw = resample(gw, 1, 2048);
		output(rgw,_,_,,resampled gw timeseries);       
	    }
	}
	if {[catch {SubmitJob job $cmd} err]} {
	    return $err
	}
	set job(jobReply)
    } -match regexp -result "produced no products"

    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    ;## Open file checking
    ;##::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    test :[file tail $argv0]:lsof: {} -body {
        if {[catch {::QA::ConnectToAgent} err]} {
            return $err
        }
        if {[catch {source /ldas_outgoing/LDASapi.rsc} err]} {
            return $err
        }
        regexp {(\d+)} [ glob -nocomplain ${::TOPDIR}/frameAPI/.frame*.lock ] -> fpid
	set cmd "lsof -p $fpid | grep -c complex_data"
        Puts 5 "INFO: Rexec: $::DISKCACHE_API_HOST ldas $cmd"
        catch {Rexec $::DISKCACHE_API_HOST ldas $cmd} result
	puts $result
        return $result
    } -match regexp -result {^0\s+}

    ;##==================================================================
    ;## Testing is complete
    ;##==================================================================
    cleanupTests
    
} ;## namespace ::QA::complex_data::test

namespace delete ::QA::complex_data::test

if {0} {

proc dumpRDSFrameData {} {
	uplevel {
	set cmd "exec $::FRDUMP -H -e -l -proc [ join $::resultfiles " " ]"
	puts $cmd
	catch { eval $cmd } data
	puts "$::FRDUMP of proc data\n$data"		
	}
}
;## createRDS
set cmd "createRDS \
    -times {774982800-774982911} \
    -type {R} \
    -usertype {} \
    -outputdir {} \
    -usejobdirs {0} \
    -compressiontype {raw} \
    -compressionlevel {6} \
    -filechecksum {0} \
    -frametimecheck {1} \
    -framedatavalid {1} \
    -channels {H2:DAQ-FAST_CH0_HDYNE H2:DAQ-FAST_CH1_HDYNE H2:DAQ-FAST_CH2_HDYNE H2:DAQ-FAST_CH3_HDYNE}"
    
set expected "Your results:\n(H-R\[^\n\]+)\n.+($::TOPDIR\[^\n\]+)"
sendCmd
dumpRDSFrameData

;## createRDS with resampling ( request 16 sec after and 16 sec before to support the 2 extra frames
;## for resample and to avoid gaps.

set cmd "createRDS \
    -times {774982816-774982895} \
    -type {R} \
    -usertype {} \
    -outputdir {} \
    -usejobdirs {0} \
    -compressiontype {raw} \
    -compressionlevel {6} \
    -filechecksum {0} \
    -frametimecheck {1} \
    -framedatavalid {1} \
    -channels {H2:DAQ-FAST_CH0_HDYNE!2 H2:DAQ-FAST_CH1_HDYNE!2 H2:DAQ-FAST_CH2_HDYNE!2 H2:DAQ-FAST_CH3_HDYNE!2}"
    
set expected "Your results:\n(H-R\[^\n\]+)\n.+($::TOPDIR\[^\n\]+)"
sendCmd
dumpRDSFrameData

;## PR3067
;## /scratch is now mounted from dev, not dataserver
catch { eval exec /ldas/bin/ssh-agent-mgr --agent-file=/ldas_outgoing/managerAPI/.ldas-agent --shell=tclsh check} err
eval $err
if { ! [ info exists ::env(SSH_AUTH_SOCK) ]} {
    puts $err
    puts "need to setup env(SSH_AUTH_SOCK) for this test"
    exit
}

source /ldas_outgoing/LDASapi.rsc
regexp {(\d+)} [ glob -nocomplain /ldas_outgoing/frameAPI/.frame*.lock ] -> fpid

set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
                \"lsof -p $fpid | grep complex\""
catch { eval $cmd } data
puts "$cmd\n$data"
} ;## if {0}
