#! /ldcg/bin/tclsh

set i 0
foreach file $argv {
puts "file $file"
	set slave [ interp create ]
	$slave eval source $file
	set list$i [ $slave eval set ::MOUNT_PT ]
	puts "list$i\n [set list$i] "
	incr i 1
}

set ptlists [ info vars list* ]

set i 1
set len [ llength $ptlists ]
foreach ptlist $ptlists {
	if	{ $i < $len } {
		set pts1 [ lsort -dictionary [ set $ptlist ] ]
		set pts2 [ lsort -dictionary [ set [ lindex $ptlists $i ] ] ]
		puts "list1 [ llength $pts1 ] entries, list2 [ llength $pts2 ] entries"
		foreach entry $pts1 {
			if	{ [ lsearch -exact $pts2 $entry ] == -1 } {
				lappend missing$i $entry
			}
		}
	}
	incr i 1
}
set missed [ info vars missing* ]

foreach entry $missed {
	regexp {missing(\d+)} $entry -> index
	puts "missing from [ lindex $argv $index ]: [ set $entry ]"
}
