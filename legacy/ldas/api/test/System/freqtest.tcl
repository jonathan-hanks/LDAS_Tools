#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

if {![info exists ::env(LIGOTOOLS)]} {
    return -code error "Can't find LIGOtools."
}

lappend ::auto_path $::env(LIGOTOOLS)/lib .
package require qalib
package require qatest
package require LDASJob

qaInit

set ::tcltest::debug 1
set ::dctarget "datacond"

#set ::times 693633680-693639824
#set ::times 693633680-693635727
set ::times 693656368-693658415

set ::testScript {
    catch {LJrun job -nowait -manager $::SITE $::cmd}
    if {$LJerror} {
        set errmsg $::job(error)
        LJdelete ::job
        error "Error from LJrun: $errmsg"
    }

    #::tcltest::DebugPuts 1 $::job(jobid)
    puts $::job(jobid)
    catch {LJwait ::job}
    if {$LJerror} {
        set errmsg [trimReply $::job(error)]
        LJdelete ::job
        error "Error from LJrun: $errmsg"
    }

    set urlList $::job(outputs)
    LJdelete ::job
    foreach url $urlList {
        #::tcltest::DebugPuts 1 $url
        puts $url
        checkURL $url
    }

    list
}

set ::cmd {
    $::command
        -outputformat { $::format }
        -returnprotocol { http://reduced }
        -framequery { P_SFT_H2 H {} $::times Proc(0!612.1!0.3!) }
}

set ::command "getFrameData"
set ::format "ilwd ascii"
::tcltest::test "1:${::command}:${::format}" {} $::testScript {}
set format LIGO_LW
::tcltest::test "1:${::command}:${::format}" {} $::testScript {}

set ::command "getFrameElements"
set ::format "ilwd ascii"
::tcltest::test "2:${::command}:${::format}" {} $::testScript {}
set format LIGO_LW
::tcltest::test "2:${::command}:${::format}" {} $::testScript {}

exit 0

set ::cmd {
    dataPipeline
        -subject { $::subject }
        -returnprotocol {http://freq}
        -outputformat {ilwd ascii}
        -datacondtarget {$::dctarget}
        -setsingledc {$::setsingle}

        -algorithms {$::algorithm}
        -framequery { P H {} $::times Proc(0!8!8!) }
}

foreach {::subject ::channels} $::testCases {
    ::tcltest::test "3:dataconcAPI:all resample:${::subject}" {} $::testScript {}
}

