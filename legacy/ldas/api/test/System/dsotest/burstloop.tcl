#!/bin/sh
# ALL the continued lines following this one are interpreted
# by the bourne shell and ignored by Tcl, which picks up
# execution after the exec line.
# \
use_ligotools="/ligotools/bin/use_ligotools"
# \
test -x $use_ligotools && eval `$use_ligotools`
# \
exec /ldcg/bin/tclsh "$0" ${1+"$@"}
#exec tclsh "$0" ${1+"$@"}

if {![info exists ::env(LIGOTOOLS)]} {
    return -code error "Can't find LIGOtools."
}

lappend ::auto_path $::env(LIGOTOOLS)/lib
package require LDASJob
package require tconvert

################################################################
#  burstloop.tcl               AJW, 8/10/02
#
#  master script for submitting LDAS jobs for burst search, ONLINE.
#
################################################################

set manager dev
if {$::argc > 0} {
    set manager [lindex $::argv 0]
}

;## get all dso setup parameters
source dsoSetup.rsc

;## load in the burstloop procedures:
source ${top}/dsoprocs.tcl

set runname S2
set table SNGL_BURST
set putindb 0
set seggrp ${ifo}:IFOLocked

#set db_seg ${manager}_s2
#set db ${manager}_s2
set db_seg ldas_tst
set db ldas_tst

set channel  ${ifo}:LSC-AS_Q

#set tbegrun 727495213 ;##  tconvert 1/24/03 18:00 PST
#set tbegrun 729331213

#set tbegrun 730522192 ;## 3-coincident playground data
#set tendrun 730525808

# S2
#set tbegrun 730000813 ;## tconvert 2/22/03 18:00 PST
set tbegrun 729273613 ;## tconvert 2/14/03 8:00 PST
set tendrun 734367613 ;## tconvert 4/14/03 8:00 PST

#set tendrun 999999999 ;## modify to reflect available data

if {$::argc > 1} {
    set tbegrun [lindex $::argv 1]
}

if {$::argc > 2} {
    set tendrun [lindex $::argv 2]
}

set duration 361
if {$dso == "inspi"} {
    set duration 1025
}

;## Special handling for ldas-test
#if {[string equal "test" $manager]} {
#    set db_seg tst_s2
#}

;## run control global variables:
setupJobControl

;## read state file to see where we left off last time
readStateFile

;## begin where we left off last time:
set tbegin [ expr $::lasttstop > $tbegrun ? $::lasttstop : $tbegrun ]

;## we currently do not have any segments to filter
set havesegs 0

;## this is the main loop

while { ! [ file exists $stopfile ] } {
    ;## get next pair of times to run over.
    ;## if there's nothing in the DB, it will sleep for 500 secs.
    ;## If it finds suitable times, havesegs will be set.

    getLockedTimes
    if { ! $havesegs } { continue }
    if { ! $usestop  } { continue }

    ;## submit the job and process the results:
    dsoSearch
}

endCleanly
exit 0
