## ********************************************************
##
## Name: dsoprocs.tcl
##
## Description: All the various processes we like to use
##              for running (burst) dsos on LDAS, online.
##
## Alan Weinstein, 8/02
## lifted from Peter Shawhan, Duncan Brown, LDAS folks, etc.
##
##

## ********************************************************
##
## Name: setupJobControl
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc setupJobControl { } {

    global logfid sumfid stopfile lockname
    global runname manager db_seg seggrp db table putindb
    global dso channel tbegrun duration ifo

    ;## create a global variables that we use a lot
    ;## RUNDIR is the current working directory
    ;## DSO is the name of the search group (in this
    ;## case it will be "test"). We call it DSO for
    ;## want of a better name. It doesn't have to be
    ;## the name of a shared object at all.
    ;## SEARCH is the name of this particular search
    ;## under the DSO directory.

    set ::RUNDIR [ pwd ]
    regexp {[a-z_\-]+/[0-9A-Za-z_\-]+$} $::RUNDIR mydir
    set ::DSO [ lindex [ split $mydir / ] 0 ]
    set ::SEARCH [ lindex [ split $mydir / ] 1 ]

    ;## check for the presence of a lock file and
    set globpat [ file join $::RUNDIR $::DSO.*.lock ]
    set locks [ glob -nocomplain $globpat ]

    ;## quit if more than one lock file exists
    if { [ llength $locks ] > 1 } {
        puts stderr "multiple $::DSO DSO lock files exists!: $locks"
        exit
    }

    ;## if an error lock file exists set the variable
    ;## ERRORLOCK to 1 as we may need to cleanup from
    ;## a previous failed job later
    if { [ regexp error $locks ] } {
        set ::ERRORLOCK 1
        file delete -force -- $locks
        set locks [ list ]
    }

    ;## if a lock file exists exit, otherwise create one
    if { [ llength $locks ] == 1 } {
        puts stderr "$::DSO DSO lock file exist!: $locks"
        exit
    } else {
        set ::ERRORLOCK 0
        set lockname [ file join $::RUNDIR $::DSO.[ pid ].lock ]
        file mkdir [ file dirname $lockname ]
        set fid [ ::open $lockname w ]
        puts $fid "$::DSO is locked"
        close $fid
        file attributes $lockname -permissions 0600
    }

    ;## create log directories if they do not exist
    set ::LOGDIR [ file join $::RUNDIR logs ]
    set ::ARCDIR [ file join $::LOGDIR archive ]
    if { ! [ file isdirectory $::LOGDIR ] } {
        file mkdir $::ARCDIR
    }

    ;## make sure browsers can read the logs in the
    ;## archive directory
    set htaccess [ file join $::ARCDIR .htaccess ]
    if { ! [ file exists $htaccess ] } {
        set fid [ open $htaccess w ]
        puts $fid "DefaultType text/html"
        ::close $fid
        file attributes $htaccess -permissions 0644
    }

    ;## name of the logfile and statusfile that we write to
    set ::LOGFILE [ file join $::LOGDIR DSO$::DSO.log.html ]
    set ::STATUSFILE [ file join $::RUNDIR $::DSO.status ]

    ;## move any existing logfiles out of the way
    if { [ file exists $::LOGFILE ] } {
        file rename -force $::LOGFILE  [ file join $::ARCDIR DSO$::DSO.log.[ tconvert now ] ]
    }

    ;## open the log file and write the html header
    set logfid [ open $::LOGFILE w ]
    puts $logfid "<html>\n<head><title>burstloop log for $::DSO $::SEARCH</title>"
    puts $logfid "<body>\n<pre>\n"

    ;## open a summary file with one-line summary of every job:
    set ::SUMFILE [ file join $::RUNDIR $::DSO.$::SEARCH.sum ]
    set sumfid [ open $::SUMFILE a ]

    ;## make sure there is a directory for xml output:
    set ::XMLDIR [ file join $::RUNDIR xml  ]
    if { ! [ file isdirectory $::XMLDIR ] } { file mkdir $::XMLDIR }

    ;## make sure there is a directory for tmp output:
    set ::TMPDIR [ file join $::RUNDIR tmp  ]
    if { ! [ file isdirectory $::TMPDIR ] } { file mkdir $::TMPDIR }

    ;## the cgi script will create a file called sHuTdOwN
    ;## if it wants the search to stop cleanly
    set stopfile [ file join $::RUNDIR sHuTdOwN ]

    ;## if an error lock file was found, do some cleanup
    if { $::ERRORLOCK } {
        puts $logfid "An error lock file was found on startup"
        puts $logfid "Cleaning up previous aborted search"
        flush $logfid

        ;## do something like parse the status file to find
        ;## out where we should pick up from
    }

    set nowgps [tconvert now]
    set now [tconvert -l $nowgps]

    puts $logfid "_____________________________________________"
    puts $logfid "burstloop.tcl starting at $now, GPS = $nowgps"
    puts $logfid "::DSO = $::DSO"
    puts $logfid "::SEARCH = $::SEARCH"
    puts $logfid "::LOGFILE = $::LOGFILE"
    puts $logfid "::STATUSFILE = $::STATUSFILE"
    puts $logfid "::SUMFILE = $::SUMFILE"
    puts $logfid ""
    puts $logfid "From dsoSetup.rsc:"
    puts $logfid "runname   = $runname"
    puts $logfid "manager   = $manager"
    puts $logfid "db_seg    = $db_seg"
    puts $logfid "seggrp    = $seggrp"
    puts $logfid "db        = $db"
    puts $logfid "table     = $table"
    puts $logfid "dso       = $dso"
    puts $logfid "channel   = $channel"
    puts $logfid "tbegrun   = $tbegrun"
    puts $logfid "duration  = $duration"
    puts $logfid ""
    puts $logfid "_____________________________________________"

    flush $logfid
}

## ********************************************************
##
## Name: readStateFile
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc readStateFile { } {

    global logfid

    ;## read status file, if any
    if { [ catch { set fid [ open $::STATUSFILE r ] } err ] } {
        set ::state      off
        set ::currjid    NONE
        set ::currtsub   0
        set ::currtstart 0
        set ::currtstop  0
        set ::lastjid    0
        set ::lasttsub   0
        set ::lasttstart 0
        set ::lasttstop  0
        set ::lastevents 0
        return
    } else {
        set status [ read $fid ]
        set ::state      [ lindex $status 0 ]
        set ::currjid    [ lindex $status 1 ]
        set ::currtsub   [ lindex $status 2 ]
        set ::currtstart [ lindex $status 3 ]
        set ::currtstop  [ lindex $status 4 ]
        set ::lastjid    [ lindex $status 5 ]
        set ::lasttsub   [ lindex $status 6 ]
        set ::lasttstart [ lindex $status 7 ]
        set ::lasttstop  [ lindex $status 8 ]
        set ::lastevents [ lindex $status 9 ]
    }

    set ::state      start
    set ::currjid    NONE
    set ::currtsub   0
    set ::currtstart 0
    set ::currtstop  0

    ;## write state file. this should be a single line
    ;## containg the following
    ;##
    ;## [ok|err] for the summary page output light
    ;## current jobid
    ;## current job submit time
    ;## current job start time
    ;## current job end time
    ;## last jobid
    ;## last job submit time
    ;## last job start time
    ;## last job end time
    ;## number of events in last job

    set status "$::state $::currjid $::currtsub $::currtstart $::currtstop \
        $::lastjid $::lasttsub $::lasttstart $::lasttstop $::lastevents"
    
    set fid [ open $::STATUSFILE w ]
    puts $fid "$status"
    close $fid

    set now [tconvert now]
    puts $logfid "readStateFile status at $now = $status"
}

## ********************************************************
##
## Name: jobSubmitState
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc jobSubmitState { nstate ncurrjid ncurrtsub ncurrtstart ncurrtstop } {

    global logfid

    set ::state      $nstate
    set ::currjid    $ncurrjid
    set ::currtsub   $ncurrtsub
    set ::currtstart $ncurrtstart
    set ::currtstop  $ncurrtstop

    set status "$::state $::currjid $::currtsub $::currtstart $::currtstop \
        $::lastjid $::lasttsub $::lasttstart $::lasttstop $::lastevents"

    set fid [ open $::STATUSFILE w ]
    puts $fid "$status"
    close $fid

    set now [tconvert now]
    puts $logfid "jobSubmit status at $now = $status"
    flush $logfid
}

## ********************************************************
##
## Name: jobSucceedState
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc jobSucceedState { nevents } {

    global logfid

    set ::state      ok
    set ::lastjid    $::currjid
    set ::lasttsub   $::currtsub
    set ::lasttstart $::currtstart
    set ::lasttstop  $::currtstop
    set ::lastevents $nevents
    set ::currjid    NONE
    set ::currtsub   0
    set ::currtstart 0
    set ::currtstop  0

    set status "$::state $::currjid $::currtsub $::currtstart $::currtstop \
        $::lastjid $::lasttsub $::lasttstart $::lasttstop $::lastevents"

    set fid [ open $::STATUSFILE w ]
    puts $fid "$status"
    close $fid

    set now [tconvert now]
    puts $logfid "jobSucceed status at $now = $status"
    flush $logfid
}

## ********************************************************
##
## Name: jobFailedState
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc jobFailedState { nevents } {

    global logfid

    set ::state      fail
    set ::currjid    NONE
    set ::currtsub   0
    set ::currtstart 0
    set ::currtstop  0
    set ::lastjid    $::currjid
    set ::lasttsub   $::currtsub
    set ::lasttstart $::currtstart
    set ::lasttstop  $::currtstop
    set ::lastevents $nevents

    set status "$::state $::currjid $::currtsub $::currtstart $::currtstop \
        $::lastjid $::lasttsub $::lasttstart $::lasttstop $::lastevents"

    set fid [ open $::STATUSFILE w ]
    puts $fid "$status"
    close $fid

    set now [tconvert now]
    puts $logfid "jobFailed status at $now = $status"
    flush $logfid
}

## ********************************************************
##
## Name: getLockedTimes
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc getLockedTimes { } {

    global havesegs seglist
    global logfid
    global seggrp tbegin duration
    global usestart usestop

    set havesegs 0
    set usestart 0
    set usestop 0
    set firstquery 1
    if { ! [info exists seglist] } { set seglist {} }

    #-- Loop until we have a suitable segment
    while { $havesegs == 0 } {

        #-- Check whether there is a suitable segment in the current list
        while { [llength $seglist] > 0 } {

            #-- Evaluate the FIRST segment in the current list
            set sstart [lindex $seglist 0]
            set sstop [lindex $seglist 1]
            set slen [ expr $sstop - $sstart + 1 ]

            #-- Check the length of the segment.  If longer than the required
            #-- duration, we're all set.  If shorter, then remove this segment
            #-- from the list, UNLESS it is the last segment in the list, in
            #-- which case keep it because it might be extended later.
            if { $slen >= $duration } {
                #-- We found a suitable segment!
                set havesegs 1
                break
            } elseif { [llength $seglist] == 2 } {
                #-- This is the only remaining segment in the list, so keep it
                #-- in case it is extended later
                break
            } else {
                #-- This segment is too short, and is not the last segment in
                #-- the list, so remove it
                puts $logfid "discarding short segment $sstart - $sstop ($slen seconds)"
                set seglist [ lreplace $seglist 0 1 ]
            } ;# end if { $slen >= $duration }

        } ;#-- End of while loop over segments in existing seglist

        #-- If we found a suitable segment, then we're all set
        if { $havesegs } { break }

        #------------------
        #-- If we get here, then we need to get more segment info from the database

        #-- If we've already queried the database at least once in this
        #-- invocation of getLockedTimes, then sleep a while before trying
        #-- again (return to calling program first, to check for sHuTdOwN)
        if { $firstquery == 0 } {
            ;## To start all over again
            #puts $logfid "Starting over from beginning ($::tbegrun)"
            #set ::tbegin $::tbegrun
            #set ::seglist {}

            ;## To shutdown the script
            puts "shutting down $firstquery == 0"
            catch {close [open sHuTdOwN a]}

            ;## To continue running
            #set ::currtsub [tconvert now]
            #jobSubmitState ok Wait-Lock $::currtsub 0 0
            #puts $logfid "No suitable segment found. Sleeping for 200 seconds"
            #after 200000

            ;# now return to calling program
            return
        }

        set firstquery 0

        puts $logfid "Getting list of locked segments $seggrp from $tbegin"

        ;## get a list of new segments from the database
        if { [ catch { getLockedSegments $seggrp $tbegin } newseglist ] } {
            #-- Error getting segments!  Error message is in $newseglist
            puts $logfid $newseglist
        } else {
            #-- We successfully ran the job to check for segments
            set numsegsret [ expr [ llength $newseglist ] / 2 ]
            if { $numsegsret } {
                #-- We found one or more new segments
                puts $logfid "Got $numsegsret new segments"

                #-- Combine the new segment info with the segment info we already had
                set seglist [ condenseSegmentList [concat $seglist $newseglist] ]
                puts $logfid "seglist = $seglist"
            } ;# end if { $numsegsret }
        } ;# end getLockedSegments

        #-- Now loop back and evaluate the updated list
    } ;# end while { $havesegs == 0 }

    #-- If we get here, then $havesegs must be true.  But put in a check
    #-- just in case...
    if { ! $havesegs } { return }

    ;## get the next start/stop time to use:
    set usestart [ lindex $seglist 0 ]
    set usestop [ expr $usestart + $duration - 1 ]

    #-- Update the lower limit for the next segment query
    set tbegin [lindex $seglist end-1]

    ;## shutdown/restart script if beyond available data
    if {$usestop >= $::tendrun} {
    puts $logfid "shutdown usestop=$usestop > tendrun $::tendrun, usestart=$usestart, duration $duration"
        set usestop 0
        set havesegs 0

        ;## Shutdown
        catch {close [open sHuTdOwN a]}

        ;## Restart
        #set ::seglist {}
        #set ::tbegin $::tbegrun
    }

    return
}


## ********************************************************
##
## Name: getLockedSegments
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc getLockedSegments { seggrp { start 0 } } {

    global logfid
    global runname manager db_seg

    ;## construct the start time of the query
    if { $start > 0 } {
        set qual "and start_time > $start"
    } else {
        set qual ""
    }

    set dsouser ""
    switch -- $::dso {
        power -
        slope {set dsouser "${::dso}test"}
        tfclu {set dsouser "${::dso}sterstest"}
        inspi {set dsouser "${::dso}raltest"}
    }

    ;## run an ldas job to get the segment table
    if { [ catch {
        LJrun seglist_job -user $dsouser -manager $manager -nowait {
            getMetaData
                -returnprotocol http://out.xml
                -outputformat LIGO_LW
                -database $db_seg
                -sqlquery "select start_time,end_time from segment\
                    where segment_group='$seggrp' $qual\
                    order by start_time,end_time"
        }
    } err ] } {
        LJdelete seglist_job
        return -code error $err
    }

    if { [ catch {
        set ::jobid $::seglist_job(jobid)
    } err ] } {
        puts $logfid "$::seglist_job(error)"
        LJdelete seglist_job
        return -code error "Error while retrieving segment list: could not submit LDAS job to managerAPI"
    }

    LJwait seglist_job

    if { $LJerror } {
        puts $logfid "$::seglist_job(error)"
        set msg "Error while retrieving segment list: LDAS job $seglist_job(jobid) failed to complete"
        set ::jobid {}
        LJdelete seglist_job
        return -code error $msg
    }

    set segjobnow [tconvert -l [tconvert now] ]
    puts $logfid "LDAS Job $seglist_job(jobid) completed at $segjobnow after $::seglist_job(jobTime) seconds"
    set segjob $seglist_job(jobid)
    set segout $seglist_job(outputs)
    set ::jobid {}

    ;## delete the ldasJob
    LJdelete seglist_job

    ;## check that the job produced the correct number of
    ;## output files
    set nout [ llength $segout ]
    if { $nout == 0 } {
        set msg "Error while retrieving segment list: no output from LDAS job $segjob"
        return -code error $msg
    } elseif { $nout > 1 } {
        set segout [lindex $segout 0]
        #set msg "Error while retrieving segment list: more than one output from LDAS job $segjob"
        #return -code error $msg
    }

    ;## download the job output to a local file with a unique name
    ;## set localsegfile [ file join "tmp" "seg[clock seconds].xml" ]
    set ofile "seg[clock seconds].xml"
    set ifile $segout

    catch {GetFile $ifile $ofile} localsegfile

    if { [ file readable $localsegfile ] } {
        set seglist [ exec lwtprint $localsegfile -d " " ]
        file delete -force $localsegfile
    } else {
        set msg "Error while retrieving segment list: could not copy $segout to local machine"
        return -code error $msg
    }


    ;## condense the list of segments
    set seglist [ condenseSegmentList $seglist ]

    ;## return the list of segments
    return $seglist
}

## ********************************************************

## ********************************************************
##
## Name: condenseSegmentList
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc condenseSegmentList { seglist } {

    if { [llength $seglist] < 2 } {
        return {}
    }

    set newlist [ list ]
    set curstart [ lindex $seglist 0 ]
    set curstop [ lindex $seglist 1 ]

    foreach { sstart sstop } $seglist {
        if { $sstart <= $curstop } {
            if { $sstop > $curstop } {
                set curstop $sstop
            }
        } else {
            lappend newlist $curstart $curstop
            set curstart $sstart
            set curstop $sstop
        }
    }

    lappend newlist $curstart $curstop

    return $newlist
}
## ********************************************************

## ********************************************************
##
## Name: dsoSearch
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dsoSearch { } {

    global runname manager db_seg seggrp db table putindb
    global dso channel tbegrun duration ifo
    global usestart usestop
    global stime etime seglist
    global logfid sumfid urldir
    global dso_job

    ;## Construct the LDAS command:
    set stime $usestart
    set etime $usestop
    set cmd [MakeCmd]
    set outs "$channel $stime $etime $dso $manager"
    set jsecbegin [tconvert now]
    set jobstart [tconvert -l $jsecbegin]
    set ::jobid {}

    ;##-- Print out parameter values
    puts $logfid " "
    puts $logfid "*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++* "
    puts $logfid "Job started at $jobstart"
    puts $logfid "Running dso = $dso at $manager in database $db and table $table"
    puts $logfid "Processing data for $channel at ifo  $ifo"
    puts $logfid "Time interval $stime-$etime"
    puts $logfid "cmd = $cmd"
    puts $logfid " "
    flush $logfid

    set dsouser ""
    switch -- $::dso {
        power -
        slope {set dsouser "${::dso}test"}
        tfclu {set dsouser "${::dso}sterstest"}
        inspi {set dsouser "${::dso}raltest"}
    }

    ;## process the current chunk of data
    LJrun dso_job -user $dsouser -manager $manager -nowait { $cmd }

    ;## did it submit successfully?
    if { [ catch { set ::jobid $dso_job(jobid) } err ] } {
        ;## job has been submitted. Update state file, log file, summary file:
        set ::state nosub
        set ::currjid FAIL_SUB
        set ::currtstart $stime
        set ::currtstop  $etime
        set ::currtsub [tconvert now]
        set jsecend [tconvert now]
        set jtimend [tconvert -l $jsecend]
        set jnowend [tconvert -l -f %Y%m%d%H%M%S $jsecend]
        jobSubmitState $::state $::currjid $::currtsub $::currtstart $::currtstop
        set msg "LDAS job failed to submit to managerAPI at $jtimend"
        puts $logfid "msg"
        puts $logfid "$dso_job(error)"
        puts $logfid "Waiting 60 seconds before resubmitting"
        set outsd "$outs 0 $jnowend NOJOBID 0 0 0 0 0 0 0 0 NOTRIGFILE"
        puts $logfid $outsd
        puts $sumfid $outsd
        flush $logfid
        flush $sumfid
        LJdelete dso_job
        after 60000
        ;## return -code error $msg
        return
    }

    ;## job has been submitted. Update state file, log file, summary file:
    set ::state ok
    set ::currjid $dso_job(jobid)
    set ::currtstart $stime
    set ::currtstop  $etime
    set ::currtsub [tconvert now]
    set jnowsub [tconvert -l $::currtsub]
    jobSubmitState $::state $::currjid $::currtsub $::currtstart $::currtstop
    puts $logfid "LDAS job $dso_job(jobid) successfully submitted to managerAPI at $jnowsub"
    flush $logfid

    ;## now wait for job to run:
    ;## after 2000
    LJwait dso_job

    ;## did it run successfully?
    if { $LJerror } {
        set ::state jobfail
        jobSubmitState $::state $::currjid $::currtsub $::currtstart $::currtstop
        set jsecend [tconvert now]
        set jtimend [tconvert -l $jsecend]
        set jnowend [tconvert -l -f %Y%m%d%H%M%S $jsecend]
        set msg "LDAS job $dso_job(jobid) failed in dso search at $jtimend"
        puts $logfid "msg"
        puts $logfid "$dso_job(error)"
        set outsd "$outs 0 $jnowend $dso_job(jobid) 0 0 0 0 0 0 0 0 NOTRIGFILE"
        puts $logfid $outsd
        puts $sumfid $outsd
        flush $logfid
        flush $sumfid
        LJdelete dso_job
        set seglist [ lreplace $seglist 0 0  $etime ]
        ;## return -code error $msg
        return
    }

    ;## Job completed successfully!
    set ::jobid $dso_job(jobid)

    ;##-- job results into log file:
    set jsecend [tconvert now]
    set jnowend [tconvert -l $jsecend]
    set msg "LDAS job $dso_job(jobid) completed at $jnowend after $dso_job(jobTime) seconds"
    puts $logfid "$msg"
    puts $logfid $dso_job(jobReply)
    puts $logfid "outputs  = $dso_job(outputs)"
    flush $logfid

    if { [catch {set urldir $dso_job(outputDir)}] } { set urldir ""}
    if { [string first "http" $urldir] < 0 } {
        #-- Construct the URL directory for outputs
        regexp {[^\d]+} $dso_job(jobid) runcode
        set hierdir "${runcode}_[expr $dso_job(jobnum)/10000]"
        set urldir "http://$dso_job(managerIP)/ldas_outgoing/jobs/$hierdir/$dso_job(jobid)"
    }

    ;##-- get trigger file:
    set gtrig [GetTrigFile]
    set trigfile [lindex $gtrig 0]
    set nrows [lindex $gtrig 1]
    set nrowf [format "%5.1d" $nrows]

    ;##-- get power spectrum::
    set hs [GetPowerSpectrum]

    ;## Update state file
    set ::state ok
    set ::lastevents $nrows
    jobSucceedState $::lastevents

    ;## Assemble job results:
    set ltimes [format "%5.1f %5.1f %5.1f" $dso_job(jobTime) $dso_job(mpiTime) $dso_job(datacondTime)]

    set doit 1

    ;## job timing:
    set jsecend [tconvert now]
    set secdiff [expr $jsecend-$jsecbegin]
    set jtimend [tconvert -l $jsecend]
    set jnowend [tconvert -l -f %Y%m%d%H%M%S $jsecend]

    ;##-- construct output job parameters for summary file:
    set outsd "$outs $doit $jnowend $dso_job(jobid) $ltimes $hs $nrowf $trigfile"
    puts $sumfid $outsd
    flush $sumfid

    puts $logfid $outsd
    puts $logfid " "
    puts $logfid "Job $dso $stime $etime $channel $manager ended at $jtimend after $secdiff seconds."
    puts $logfid "*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++* "
    flush $logfid

    ;## delete the job results
    LJdelete dso_job

    ;## update the segment list to remove the processed data
    set seglist [ lreplace $seglist 0 0  $etime ]

}
## ********************************************************

#----------------------------------
proc MakeCmd {} {

    global runname manager db_seg seggrp db table putindb
    global dso channel tbegrun duration ifo
    global stime etime

    # first make the datacond algorithm:
    # special handling for power:
    #   resample, put out the psd, and detector response file:

    #-- until we verify that the others work with resam=8...
    set resam 1
    set resample "# no resampling"
    set outp "#output(p,_,wrapper,,spectrum data);"

    if {$dso == "power"} {
        set resam 8

        set resample "# now resample:
        gw = resample(gw, 1, $resam);"

        set outp "pr = psd( gw, 2048 );
        output(pr,_,wrapper,,spectrum data);"
    }

    set rsamp 16384
    set srate 16384.0
    set nsamp [ expr $rsamp * $duration ]
    set frsam [ expr $rsamp / $resam ]
    # skip the first second of data:
    set slic1 [ expr 1 * $frsam ]
    set slic2 [ expr $nsamp / $resam - $slic1 ]

    set alias "# datacond aliases:
        x = LSC-AS_Q;"

    set alg "# datacond algorithm:
        zz = slice(x,0,$nsamp,1);
        gw = tseries(zz, $srate, $stime, 0);
        # filter through whitener, and detrend:
        gw = linfilt(b1,a1,gw);
        gw = linfilt(b2,a2,gw);
        gw = meandetrend(gw);
        # get psd, before resampling:
        p = psd( gw, 2048 );
        $resample
        # slice off first second, and output to wrapper:
        gw = slice(gw,$slic1,$slic2,1);
        output(gw,_,wrapper,:primary,resampled gw timeseries);
        $outp
        # outputs to disk:
        output(p,ilwd_ascii,pzs.ilwd,p,spectrum data);"

    #gwr = resample(gw,1,4);
    #output(gwr,ilwd_ascii,gwr.ilwd,gwr,gwr);
    # st = all(gw);
    # output(st,ilwd_ascii,stat.ilwd,stat,stats of ch0); # this one doesnt work!

    # then the dso-specific things:

    #set froot "file:/ldas_outgoing/jobs/ldasmdc_data/burst-stochastic"
    set froot "file:/ldas_outgoing/jobs"
    set responsefile "# no detector response file:"

    if {$manager == "mit"} {
        set dtype "RS"
    } else {
        #set dtype "R"
        set dtype "RDS_R_L1"
    }
    set rtr 0.9
    set site [string index $channel 0]
    set framequery "{ $dtype $site {} $stime-$etime Adc($channel) }"

    # whitening and HP filters for all dsos:
    switch -exact -- $ifo {
        H1 {set hplim "HP150"}
        H2 {set hplim "HP150"}
        L1 {set hplim "HP150"}
    }

    set SR "S1"
    set filterfiles "# pre-whitening filters:
        $froot/${ifo}${SR}a.ilwd,push,a1
        $froot/${ifo}${SR}b.ilwd,push,b1
        $froot/HP${hplim}a.ilwd,push,a2
        $froot/HP${hplim}b.ilwd,push,b2"

    # AJW, for E9: switch order, and try shourov's filters
    set filterfiles "# pre-whitening filters:
        $froot/hpf150_a.ilwd,push,a1
        $froot/hpf150_b.ilwd,push,b1
        $froot/${ifo}${SR}a.ilwd,push,a2
        $froot/${ifo}${SR}b.ilwd,push,b2"

    if {$putindb} {
        set metadataapi "#-metadataapi ligolw"
    } else {
        #set metadataapi "-metadataapi ligolw"
        set metadataapi "-metadataapi tee"
    }

    if {$dso == "power"} {
        set dsolib libldaspower.so
        # 9/24/02 version of power:
        #-- Raw data sample rate in Hz
        set rawSrate $rsamp
        #-- How many seconds to skip at the start
        set initialSkip 1
        #-- How many seconds to analyze
        set analyze [ expr ( $duration - 1 ) ]
        #-- Total amount of data to request in seconds
        set length [expr {$initialSkip + $analyze}]
        #-- Total number of points to be requested
        set rawSlice [expr {$length * $rawSrate}]
        #-- Downsample data by this factor
        set dsoResmpl $resam
        #-- How many seconds in each segment of data to analyze
        set dsoSecPerSeg 1
        #-- Start and stop parameters for slice command
        set dsoStrtSl [expr {$initialSkip * $rawSrate / $dsoResmpl}]
        set dsoSliLen [expr {$analyze * $rawSrate / $dsoResmpl}]
        #-- Number of points per segment
        set dsoNumPts [expr {($dsoSecPerSeg * $rawSrate / $dsoResmpl)+2}]
        #-- How much to overlap each segment
        set dsoOvrlap [expr {$dsoNumPts / 2}]
        #-- How many segments to analyze
        set dsoNumSeg [ expr ( $duration - 1 ) * 2 - 2 ]
        #-- How many segments should be analyzed at one time by each node
        #set dsoSegDty [expr {$dsoNumSeg/2}]
        set dsoSegDty [expr {16}]
        #-- Low frequency bound of search
        set dsoFlow 600.0
        set dsoFlow 300.0    ;#  AJW change
        set dsoFlow 100.0    ;#  AJW change 10/16/02
        #-- Frequency steps
        set deltaF 1.0
        #-- Search band of this width (Hz) above dsoFlow
        set dsoFBand 32
        set dsoFBand 724     ;#  AJW change
        set dsoFBand 1024    ;#  AJW change 10/16/02
        #-- Detection threshold
        set dsoThresh 1.0e-5
        set dsoThresh 1.0e-36 ;# AJW change 10/16/02
        #-- Number of events above threshold to communicate from slave to master
        set dsoE2Mast 10
        ;# AJW 10/18/02 test:
        set dsoFlow 100.0
        set dsoFBand 920
        #set dsoThresh 1.0e-5
        ;# AJW for E9:
        #-- Number of points per segment
        set dsoNumPts [expr {($dsoSecPerSeg * $rawSrate / $dsoResmpl)}]
        #-- How much to overlap each segment
        set dsoOvrlap [expr {$dsoNumPts / 2}]
        #-- How many segments to analyze
        set dsoNumSeg [ expr ( $duration - 1 ) * 2 - 1 ]
        set dsoFlow 100.0
        set dsoFBand 512
        set dsoSegDty [expr {32}]
        set dsoThresh 1.0e-13

        set responsefile "#detector response file:
        $froot/responsefiles/E7-H2-AS_Q-1024-131073.ilwd,pass"

        set dsoopts "# dso-specific ldas options:
    -filterparams ($dsoNumPts,$dsoNumSeg,$dsoOvrlap,3,2,2,$dsoFlow,$deltaF,$dsoFBand,2.0,0.5,$dsoSegDty,$dsoThresh,$dsoE2Mast,$channel,0,useMedian,2)
    -np 4"


    } elseif {$dso == "tfclu"} {
        set dsolib libldastfclusters.so
        set overl [ expr $rsamp * 3 / $resam]
        set nsegs [ expr ($slic2-$overl)/3+$overl ]
        set thrH 0.01
        set fmax [ expr $rsamp / $resam / 2 ]
        set dsoopts "# dso-specific ldas options:
    -filterparams ($channel,$table,$slic2,$nsegs,0.125,$frsam,$overl,$thrH,1e-6,1,8,$fmax,9.5,5,0,0,0,0,0,0,2,3,4,4)
    -np 4"

    } elseif {$dso == "slope"} {
        set dsolib libldasslope.so
        # AJW 8/31/02   set slopethr 54
        set slopethr 1000
        ;# AJW 9/3/02_18:50 UTC: change slopethr from 1000 to following ifo-dependent values:
        switch -exact -- $ifo {
            H1 {set slopethr 1250}
            H2 {set slopethr 2500}
            L1 {set slopethr  625}
        }
        set slopechan ""
        #  -filterparams (-c,$slopechan,-ifo,$ifo,-smw,64,-pw,300,-sr,$frsam,-t,$slopethr,-sdtype,2)
        # adaptive!
        set slopethr 3.5
        set dsoopts "# dso-specific ldas options:
    -filterparams (-c,$slopechan,-ifo,$ifo,-smw,64,-pw,300,-sr,$frsam,-t,$slopethr,-sdtype,2,-adapt,yes)
    -np 2"

    } elseif {$dso == "inspi"} {
        set rtr 0.5
        set dsolib libldasinspiral.so
        set filterfiles "# no filters"
        set responsefile "# no response files"

        set putindb 0
        #set metadataapi "-metadataapi ligolw"
        set metadataapi "-metadataapi tee"

        switch -exact -- $ifo {
            H1 { set caltime 750476039-750476039 }
            H2 { set caltime 750560855-750560855 }
            L1 { set caltime 750625737-750625737 }
        }

        set framequery "{ $dtype $site {} $stime-$etime Adc(${channel}!resample!4!) }
        { CAL_REF_V02_${ifo} $site {} $caltime proc(${ifo}:CAL-CAV_GAIN!0!2048.0001!,${ifo}:CAL-RESPONSE!0!2048.0001!) }
        { SenseMonitor_V02_${ifo}_M $site {} $stime-$etime adc(${ifo}:CAL-CAV_FAC.mean,${ifo}:CAL-OLOOP_FAC.mean) }"

        set alias "# datacond aliases:
        rawchan = LSC-AS_Q;
        cav_gain = CAL-CAV_GAIN;
        resp = CAL-RESPONSE;
        cav_fac = CAL-CAV_FAC;
        oloop_fac = CAL-OLOOP_FAC;"

        set alg "# datcond algorithm:
        chan = slice( rawchan, 10, 4194304, 1);
        output(chan,_,_,_,channel);
        p = psd(chan,1048576,_,524288);
        output(p,_,_,_,psd);
        output(cav_gain,_,_,,CAL-CAV_GAIN);
        output(resp,_,_,,CAL-RESPONSE);
        cav_fac_cplx = float( cav_fac );
        cav_fac_cplx = complex( cav_fac_cplx );
        output(cav_fac_cplx,_,_,,CAL-CAV_FAC);
        oloop_fac_cplx = float( oloop_fac );
        oloop_fac_cplx = complex( oloop_fac_cplx );
        output(oloop_fac_cplx,_,_,,CAL-OLOOP_FAC);"

        set dsoopts "# dso-specific ldas options:
    -filterparams (1048576,7,524288,70.0,65536,69.0,8,0.9,1,(64.0,0.0),(20.0,0.0),4,5,0,0,(1.0,1.1,0.01,1))
    -np 4"

        ;## Uncomment the lines below to use trivial dso
        #set dsolib libldastrivial.so
        #set dsoopts "# dso-specific ldas options:
    #-filterparams (0)
    #-np 2"

    } else {
        puts "DSO = $dso not recognized! Quitting..." ; exit 1
    }

    set cmd "dataPipeline
    -subject {$dso online running on $channel }
    -usertag {${runname}_online_${dso}_${channel}}
    -dynlib $dsolib
    $dsoopts
    -responsefiles {# response files:
        $responsefile
        $filterfiles
    }
    -realtimeratio $rtr
    -framequery {
        $framequery
    }
    -datacondtarget wrapper
    -metadatatarget datacond
    -multidimdatatarget ligolw
    $metadataapi
    -database {$db}
    -aliases {
        $alias
    }
    -algorithms {
        $alg
    }"

    return $cmd
}

#-----------------------------------------------------------------------------
# Procedure to retrieve triggers from the database
proc Triglist {} {
    global dso ifo table manager db job logfid

    set dso2 [string range $dso 0 1]
    set ofile $dso2$ifo$job.xml

    set dsouser ""
    switch -- $::dso {
        power -
        slope {set dsouser "${::dso}test"}
        tfclu {set dsouser "${::dso}sterstest"}
        inspi {set dsouser "${::dso}raltest"}
    }

    #-- Run the job
    LJrun TriglistJob -user $dsouser -manager $manager {
        getMetaData
            -returnprotocol http://out.xml
            -outputformat LIGO_LW
            -database $db
            -sqlquery "with zzz(process_id,creator_db) as (select process_id, creator_db from process where jobid = $job)\
                select * from $table where $table.process_id = (select process_id from zzz) and \
                sngl_burst.creator_db = (select creator_db from zzz ) \
                FETCH FIRST 20000 ROWS ONLY FOR READ ONLY" }

    set error $::TriglistJob(error)
    set tjobid $::TriglistJob(jobid)
    set tout $::TriglistJob(outputs)
    set nout [llength $tout]

    #-- Delete the information about the LDAS job
    LJdelete TriglistJob

    #-- Check whether the job failed
    if $LJerror {
        return -code error "Error while retrieving trigger list: LDAS job failed\n$error"
    }

    #-- Make sure there was an output file
    if { $nout == 0 } {
        return -code error "Error while retrieving trigger list: no output from LDAS job $tjobid"
    } elseif { $nout > 1 } {
        set tout [lindex $tout 0]
        #return -code error "Error while retrieving trigger list: more than one output from LDAS job $tjobid"
    }

    #-- Download the job output to a local file with a unique name
    puts $logfid "copy from $tout to $ofile"
    LJcopy $tout $ofile

    #-- Return filename of triggers
    return $ofile
}

#--------------------------------------------------------------------------
proc GetFile {ifile ofile} {

    global logfid

    set ncnt 0
    while { $ncnt < 5 } {
        puts $logfid "LJcopy $ifile $ofile"
        if [catch {LJcopy $ifile $ofile} msg ] {
            set ncnt [ expr $ncnt + 1 ]
        } else {
            puts $logfid "LJcopy Succeeded! Continuing..."
            return $ofile
        }
    }

    puts $logfid "LJcopy FAILED with $msg ! Continuing..."
    return -code error
    # if LJcopy fails, try getUrl from the command line
}

#--------------------------------------------------------------------------
proc GetTrigFile {} {
    global putindb dso_job dso ifo table manager db job urldir
    global logfid

    set trigfile "NOTRIGFILE"
    set nrows 0
    set nrowsj 0

    if { [catch {set job $dso_job(jobnum)}] } {
        return "NOTRIGFILE $nrows"
    }
    #if { [catch {set ifile $dso_job(outputs)}] } {
    #    return "NOTRIGFILE $nrows"
    #}

    if {![llength $dso_job(outputs)]} {
        return "NOTRIGFILE $nrows"
    }

    set ifile $urldir/results.xml
    set dso2 [string range $dso 0 1]
    set ofile $dso2$ifo$job.xml

    if {$putindb} {

        ;## does this way work? apparently not!
        ;## if { [ info exists dso_job(rows:$table) ] } { set nrows $dso_job(rows:$table) }
        ;## puts $logfid "Parsing the LDAS nrows, nrows = $nrows"

        ;## parse the job reply:
        set I1 [lsearch $dso_job(jobReply) "sngl_burst" ]
        set I2 [expr $I1-6]
        set nrowsj [lindex $dso_job(jobReply) $I2 ]
        puts $logfid "Parsing the LDAS reply, nrows = $nrowsj"

        puts $logfid "Getting trigger file for job $job from $manager $db $table, into $ofile"
        #-- this code gets triggers from db, if  -metadataapi ligolw is not used.
        if [catch {Triglist} trigfile] {
            puts $logfid "Failed! msg = $trigfile"
            return "NOTRIGFILE $nrowsj"
        } else {
            #-- Now parse out the number of rows in the trigger table:
            exec cat $trigfile > T.xml
        }

    } else {

        puts $logfid "Getting trigger file for job $job from $ifile into $ofile"
        if [catch {GetFile $ifile $ofile} trigfile] {
            puts $logfid "ERROR getting trigger file for job $job from $ifile into $ofile"
            return "NOTRIGFILE 0"
        } else {
            exec lwtselect $trigfile -t 3 > T.xml
        }

    }

    # yank the 3rd table out with: lwtselect, scan with lwtscan, extract nrows:
    # KLUDGE: slope sometimes has 0 events; thus, no burst table.
    # But it DOES have a search_summary table, which none of the other dso's have.
    # so, for now, it has to be handled differently.
    if {$dso == "slope" && ! $putindb } {
        if { [catch {exec lwtselect $trigfile -t 4 > S.xml}] } {
            exec lwtselect $trigfile -t 3 > S.xml
        }
        if { [catch {exec lwtprint S.xml -c nevents } nrows   ] } {
            set nrows 0
        }
    } else {
        if { [catch {exec lwtprint T.xml -c start_time} trigscan] } {
            set nrows 0
        } else {
            set nrows [llength $trigscan]
        }
    }

    # another way?
    #set trscan [ exec lwtscan T.xml  ]
    #set I [lsearch $trscan "rows" ]
    #set I1 [expr $I - 1 ]
    #set nrows [lindex $trscan $I1 ]

    #-- put the file someplace safe:
    file rename $trigfile $::XMLDIR

    puts $logfid "Triggers from job $job are in file $trigfile , nrows = $nrows"
    flush $logfid

    if { $putindb && $nrowsj > $nrows } {
        set nrows $nrowsj
    }
    return "$trigfile $nrows"
}

#--------------------------------------------------------------------------
proc GetPowerSpectrum {} {
    global dso dso_job ifo logfid urldir

    set hs "       0        0        0        0"
    # Get the power spectrum:

    if { [catch {set job $dso_job(jobnum)}] } {
        return $hs
    }

    if { $dso == "inspi" } {
        return $hs
    }

    set ifile $urldir/pzs.ilwd
    set ofile p$ifo$job.ilwd

    puts $logfid "Get power spectrum from $ifile to $ofile"
    if { [catch {GetFile $ifile $ofile} pfile] } {
        return $hs
    }

    # sum up the power in some bands.
    # Hope that the format of this ilwd file doesn't change,
    # because we're "parsing" it blind.
    set plist [exec cat $pfile ]
    # set I1 [lsearch $plist "<real_4"]
    set I1 [lsearch $plist "dims='1025'" ]
    set I320  [expr $I1 +  320 / 8 ]
    set I400  [expr $I1 +  400 / 8 ]
    set I600  [expr $I1 +  600 / 8 ]
    set I1600 [expr $I1 + 1600 / 8 ]
    set I3000 [expr $I1 + 3000 / 8 ]
    set h1 0
    set h2 0
    set h3 0
    set h4 0
    set I 0
    foreach {a} $plist {
        set I [expr $I+1]
        #set a [lindex $plist $I]
        if {  $I > $I320  &&  $I <= $I400  } { set h1 [expr $h1+$a] }
        if {  $I > $I400  &&  $I <= $I600  } { set h2 [expr $h2+$a] }
        if {  $I > $I600  &&  $I <= $I1600 } { set h3 [expr $h3+$a] }
        if {  $I > $I1600 &&  $I <= $I3000 } { set h4 [expr $h4+$a] }
    }
    #-- power in various bands:
    set hs [format "%8.2e %8.2e %8.2e %8.2e" $h1 $h2 $h3 $h4 ]

    #-- put the file someplace safe:
    file rename $ofile $::TMPDIR

    return $hs
}

## ********************************************************
##
## Name: endCleanly
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc endCleanly {  } {

    global logfid stopfile lockname

    set nowgps [tconvert now]
    set now [tconvert -l $nowgps]

    puts $logfid ""
    puts $logfid "found $stopfile"
    puts $logfid "_____________________________________________"
    puts $logfid "burstloop.tcl exiting at $now, GPS = $nowgps"

    ;## delete the shutdown file and the lock file
    ;## then exit cleanly
    file delete -force -- $stopfile
    file delete -force -- $lockname

    return
}
