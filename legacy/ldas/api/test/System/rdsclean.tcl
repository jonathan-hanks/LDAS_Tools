#! /ldcg/bin/tclsh
source /ldas_outgoing/cntlmonAPI/cntlmon.state
source /ldas_outgoing/LDASapi.rsc

catch {exec /ldas/bin/ssh-agent-mgr --agent-file=/ldas_outgoing/managerAPI/.ldas-agent --shell=tclsh check} err
puts "agent $err"
eval $err
if { ! [ info exists ::env(SSH_AUTH_SOCK) ]} {
    puts $err
    puts "need to setup env(SSH_AUTH_SOCK) for this test"
    exit
}


;## remove a program
proc killer { pattern } {
	catch { exec ps -A -o "pid,args" | grep $pattern } data
	foreach line [ split $data \n ]  {
		if	{ [ regexp {(\d+).+sh} $line -> pid ] } {
			puts "killing $pattern $pid, $line"
			catch { exec kill -9 $pid }
		}
	}
}

;## cleanup on dataserver local file system
proc cleanup { {round 0 } } {
    set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
        \"rm -rf $::framedir\""
    catch { eval $cmd } err
	puts "rm $err"
	if	{ [ string length $err ] } {
		puts "$cmd\n$err"
	}
	set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
		\"ls -l $::framedir\""
	catch { eval $cmd } err	
	if	{ ! [ string match "*No such file or directory*" $err ]  }  {
		incr round 1
		if	{ $round < 5 }  {
			cleanup $round
		}
	}
	puts "round $round $err"
}	

killer dords
killer rdsloop
set curtime [ clock seconds ]


;## purge any log over 3 hours
foreach type [ list log dat out ] {
	foreach file [ glob -nocomplain rdsloop_*.$type ] {
		regexp {\d+} $file -> filetime
		set mtime [ file mtime $file ]
		if	{ [ expr $curtime - $mtime ] > 1800 } {
			puts "deleting $file"
			file delete -force $file
		}
	}
}

if	{ [ regexp {tandem} $::LDAS_SYSTEM ] } {
	set ::framedir /scratch/test/system/$::LDAS_SYSTEM/S3_RDS
	set site [ info hostname ]
} else {
	set ::framedir /scratch/test/system/site/S3_RDS
	regsub {ldas-} $::LDAS_SYSTEM {} site
}
puts "site $site, frame output $::framedir"

;## clear the log files
if	{ $argc } {
	set clearopt [ lindex $argv 0 ]
	if	{ [ string equal $clearopt delete ] } {
		cleanup
	}
}
