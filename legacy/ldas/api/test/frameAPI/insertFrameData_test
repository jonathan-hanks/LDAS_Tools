#!/ldcg/bin/tclsh
;## will create a reduced frame with a single adc data channel.
;## good test of compatibility between frames and frame API.
;## exceptions from the c++ layer will bubble up with trace
;## info.

lappend auto_path /ldas/lib
if { [ info exists env(MY_AUTOPATH) ] } {
   set auto_path [ concat $::env(MY_AUTOPATH) $auto_path ]
   }

set API frame

;## LDAS libraries we will use
package require frameAPI
package require frame
package require genericAPI
package require generic

;## redefine some required procs
proc debugPuts { args } { puts $args }
proc myName {} { }

;## the list of frame attributes we will query
set attrs {
          run
          frame
          GTime
          ULeapS
          localTime
          dt
          }

;## glob all the sample frames
set files [ glob sample_frame* ]

foreach file $files {
   catch { 
         set ilwdp {}
         destructFrame $ffp
         unset data
         }
   set data {}      

   puts "FRAME FILE: $file"
   
   ;## open the file, read it, close it, and destruct the file_p
   set ffp [ frame::file2ptr $file r ]

   if { [ catch {
      ;## initialise the raw target frame container
      foreach { ilwdp - } [ frame::initraw $file $ffp ] { break }
      } err ] } {
      puts "     exception: $err"
      puts "     failed to create reduced frame"
      }

   ;## if we successfully initialised, then try to insert a channel
   if { [ llength $ilwdp ] } {
      if { [ catch {
         ;## and try to add channel 0 to the raw frame
         insertadcchanlist $ffp $ilwdp 0
         puts "     successfully created reduced frame"
         } err ] } {
         puts "     exception: $err"
         puts "     failed to create reduced frame"
         }
      }   

   ;## convert the raw frame to ilwd text
   catch { set data [ getElement $ilwdp ] }
   catch { destructElement $ilwdp }

   ;## if the thing was not a "standard" frame file, it may still
   ;## be a readable frame, so try to get at it's attributes
   if { ! [ llength $data ] } {
      foreach { ilwdp - } [ getFrameData $ffp full ] { break } 
      foreach i { 0 1 2 3 4 5 6 } {
         append data [ getContainerElement $ilwdp $i ]
         }
      } else {
      ;## and maybe it was a standard frame, so see if it
      ;## has history as element 7 and adc data as 8
      foreach { ilwdp - } [ getFrameData $ffp full ] { break }      
      append data [ getContainerElement $ilwdp 7 ]
      catch { append data [ getContainerElement $ilwdp 8 ] }
      }
      
    set numchans [ llength [ getChannelList $ffp ] ]  
    catch { destructFrame $ffp }
    
   ;## and parse out some useful data
   foreach attr $attrs {
      set $attr {}
      regexp "$attr'>(\[^<\]+)" $data -> $attr
      puts "     $attr: [ set $attr ]"
      }

   ;## does the frame contain a history section?
   if { [ regexp {History} $data ] } {
      puts "     frame has history element"
      } else {
      puts "     frame has NO history element"
      }
   
   set ch_dt "unavailable due to exception"
   set chname "unavailable due to exception"
   regexp {:([A-Z][0-9A-Z_-]+)} $data -> chname
   puts "     channel name: $chname"
   regsub 'dt' $data "" data   
   regexp {dt'>([^<]+)} $data -> ch_dt
   puts "     channel dt: $ch_dt"
   puts "     total # of channels in source frame: $numchans\n"
}

exit
