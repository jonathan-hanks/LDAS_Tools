#ifndef TOKEN_HH
#define	TOKEN_HH

#include <stdarg.h>
#include <stdexcept>
#include <valarray>

#include "genericAPI/swigexception.hh"

#include "CallChain.hh"

#define CATCH( LM_T) \
  catch ( CallChain::Exception& e )					\
  {									\
    LM_T.Check(false) << "CallChain Exception : "			\
                       << e.GetMessage() << endl;			\
  }									\
  catch ( SwigException& e )						\
  {									\
    LM_T.Check(false) << "SwigException: " << e.getResult()		\
                       << " Info: " << e.getInfo() << endl;		\
  }									\
  catch ( std::invalid_argument& e )					\
  {									\
    LM_T.Check(false) << "invalid argument: " << e.what() << endl;	\
  }									\
  catch ( std::logic_error& e )						\
  {									\
    LM_T.Check(false) << "logic error: " << e.what() << endl;	\
  }									\
  catch ( std::runtime_error& e )					\
  {									\
    LM_T .Check(false) << "runtime error: " << e.what() << endl;	\
  }									\
  catch ( std::exception& e )						\
  {									\
    LM_T.Check(false) << "std exception: " <<  e.what() << endl;	\
  }									\
  catch(...)								\
  {									\
    LM_T.Check(false) << "Caught an Unknown exception" << endl;	\
  }

const int PARAMETERS_MAX_PARMS = 10;

class Parameters
{
public:
  inline Parameters(void);
  inline char** set(int Count, ...);

private:
  char*		m_buffer[ PARAMETERS_MAX_PARMS ];
};

Parameters::
Parameters(void)
{
}

char** Parameters::
set(int Count, ...)
{
  int		c = 0;
  va_list	ap;
  va_start(ap, Count);
  for (c = 0; (c < Count) && (c < PARAMETERS_MAX_PARMS ); c++)
  {
    m_buffer[c] = va_arg(ap, char*);
  }
  va_end(ap);
  m_buffer[c] = (char*)NULL;
  return m_buffer;
}

template<class VClass>
std::ostream& operator<<(std::ostream& vout, const std::valarray<VClass>& ValArray);

std::ostream& operator<<(std::ostream& vout, const std::complex<int>& x);

template<class VClass>
void check_valarray(const std::valarray<VClass>& Input,
		    CallChain::Symbol* Result,
		    std::string Text);

void check_udt_valarray(const datacondAPI::udt& Input,
			CallChain::Symbol* Result,
			std::string Text);

template<class DataType_>
void check_value(DataType_ Value, const CallChain::Symbol& Symbol,
		 const std::string& Message);

//: Read a ilwd from a file
const ILwd::LdasElement* read_ilwd(const char* Filename,
				   const char* Env = "SRCDIR");

#endif	/* TOKEN_HH */
