#include <sstream>
   
#include "dbaccess/Table.hh"

#include "frframe.hh"

#include "general/unittest.h"	// Needed for doing "make check"

#include "token.hh"		// Common header for token testing programs.
#include "util.hh"

#include "CSDSpectrumUDT.hh"
#include "DatabaseUDT.hh"
#include "FrequencySequenceUDT.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"

using namespace std;   
using General::GPSTime;

General::UnitTest	Test;		// Class supporting testing

#if 0
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

static void CheckTimeSeries()
try
{
  using namespace datacondAPI;

  Sequence<REAL_4>	data( 0.0, 256 );
  for ( INT_4U x( 0 ); x < data.size( ); x++ )
  {
    // Ramp the data in the range of [0,1) to get some interesting numbers
    data[x] = REAL_4(x) / REAL_4(data.size( ));
  }
  TimeSeries<REAL_4>*	ts( new TimeSeries<REAL_4>(data, 1.0 ) );
  CallChain		cmds;
  cmds.StoreIntermediateResult( ts,
				"TimeSeries",
				"CheckTimeSeries",
				udt::TARGET_FRAME,
				CallChain::IntermediateResult::MAYBE_PRIMARY,
				datacondAPI::DEFAULT_COMPRESSION_METHOD,
				datacondAPI::DEFAULT_COMPRESSION_LEVEL,
				false );
  ILwd::LdasContainer*	c( dynamic_cast<ILwd::LdasContainer*>
			   ( ts->ConvertToIlwd( cmds, udt::TARGET_FRAME ) ) );

#define DEBUG
#ifdef DEBUG
  if ( Test.IsVerbose() )
  {
    Test.Message( ) << "TimeSeries: ILwd output: " << std::endl;
    c->write(2, 2, Test.Message(false), ILwd::ASCII );
    Test.Message( false ) << std::endl;
  }
#endif	/* DEBUG */
  
  Test.Check( container2frame( *c ) ) << ": TimeSeries" << std::endl;
}
CATCH( Test )

static void CheckCSDSpectrum( )
try
{
  using namespace datacondAPI;

  CSDSpectrum<REAL_4>*	_sp = new CSDSpectrum<REAL_4>();
  CSDSpectrum<REAL_4>&	sp = *_sp;
  const unsigned int welch_length(100);

  sp.resize( welch_length );

  sp.SetFrequencyBase( -0.5 );
  sp.SetFrequencyDelta( 1.0/welch_length );
  for ( unsigned int x = 0; x < welch_length; x++ )
  {
    sp[x] = (double)x;
  }

  CallChain		cmds;
  cmds.StoreIntermediateResult( _sp,
				"CSDSpectrum",
				"CheckCSDSpectrum",
				udt::TARGET_FRAME,
				CallChain::IntermediateResult::MAYBE_PRIMARY,
				datacondAPI::DEFAULT_COMPRESSION_METHOD,
				datacondAPI::DEFAULT_COMPRESSION_LEVEL,
				false );
  ILwd::LdasContainer*	c( dynamic_cast<ILwd::LdasContainer*>
			   ( _sp->ConvertToIlwd( cmds, udt::TARGET_FRAME ) ) );
  
  Test.Check( container2frame( *c ) ) << ": CSDSpectrum" << std::endl;
}
CATCH( Test )

static void CheckDatabase( )
try
{
#if WORKING
  using namespace datacondAPI;

  std::string general_table_string =
"    <ilwd comment='SQL=SELECT * FROM PROCESS ORDER BY start_time FETCH FIRST 5 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_table:table' size='17'>"
"        <int_4s dims='5' name='CREATOR_DB'>1 1 1 1 1</int_4s>"
"        <lstring dims='5' name='PROGRAM' size='88'>wrapperAPI      \\,wrapperAPI      \\,wrapperAPI      \\,program_1000424 \\,program_1000424 </lstring>"
"        <lstring dims='5' name='VERSION' size='59'>1.0.0\\,1.0.0\\,1.0.0\\,version_1000424969\\,version_1000424970</lstring>"
"        <lstring dims='5' name='CVS_REPOSITORY' size='163'>/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,cvs_repository_1000424969\\,cvs_repository_1000424970</lstring>"
"        <int_4s dims='5' name='CVS_ENTRY_TIME'>681531199 681531199 681531199 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='COMMENT' size='143'>wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,comment_1000424969\\,comment_1000424970</lstring>"
"        <int_4s dims='5' name='IS_ONLINE'>0 0 0 0 0</int_4s>"
"        <lstring dims='5' name='NODE' size='59'>beowulf\\,beowulf\\,beowulf\\,node_1000424969\\,node_1000424970</lstring>"
"        <lstring dims='5' name='USERNAME' size='88'>search07        \\,search06        \\,search05        \\,username_100042 \\,username_100042 </lstring>"
"        <int_4s dims='5' name='UNIX_PROCID'>24270 24522 24735 1000424969 1000424970</int_4s>"
"        <int_4s dims='5' name='START_TIME'>684460096 684460107 684460128 684460181 684460182</int_4s>"
"        <int_4s dims='5' name='END_TIME'>684460200 684460217 684460228 684460181 684460182</int_4s>"
"        <ilwd name='PROCESS_ID' size='5'>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\120\\111\\220\\143\\043\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\121\\003\\170\\226\\162\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\121\\023\\140\\040\\151\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\111\\066\\204\\007\\051\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\111\\066\\204\\007\\145\\000\\000\\000</char_u>"
"        </ilwd>"
"        <int_4s dims='5' name='PARAM_SET' nullmask='4'>0 0 0 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='IFOS' nullmask='4' size='32'>\\,\\,\\,ifos_100042 \\,ifos_100042 </lstring>"
"        <int_4s dims='5' name='JOBID'>38151 38148 38149 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='DOMAIN' size='66'>ldas-dev\\,ldas-dev\\,ldas-dev\\,domain_1000424969\\,domain_1000424970</lstring>"
"    </ilwd>"
;

  istringstream		is( general_table_string );
  ILwd::Reader 		reader(is);

  ILwd::LdasContainer* 	ilwd
    ( dynamic_cast<ILwd::LdasContainer*>( ILwd::LdasElement::createElement( reader ) ) );

  Database		db( *( DB::Table::MakeTableFromILwd( *ilwd, true ) ),
			    CallChain::QUERY_GENERIC );
  CallChain		cmds;
  ILwd::LdasContainer*	c( dynamic_cast<ILwd::LdasContainer*>
			   ( db.ConvertToIlwd( cmds, udt::TARGET_FRAME ) ) );
  
#if defined( DEBUG )
  if ( Test.IsVerbose() )
  {
    Test.Message( ) << "Database: ILwd output: " << std::endl;
    c->write(2, 2, Test.Message(false), ILwd::ASCII );
    Test.Message( false ) << std::endl;
  }
#endif	/* DEBUG */
  
  Test.Check( container2frame( *c ) ) << ": Database" << std::endl;
#endif /* WORKING */
}
CATCH( Test )

static void CheckFrequencySequence( )
try
{
  using namespace datacondAPI;

  AT();
  const unsigned int		length( 256 );
  AT();
  FrequencySequence<REAL_4>*	_s = new FrequencySequence<REAL_4>( length );
  FrequencySequence<REAL_4>&	s( *_s );
  AT();

  s.SetFrequencyBase( -0.5 );
  AT();
  s.SetFrequencyDelta( 1.0/length );

  CallChain			cmds;
  cmds.StoreIntermediateResult( _s,
				"FrequencySequence",
				"CheckFrequencySequence",
				udt::TARGET_FRAME,
				CallChain::IntermediateResult::MAYBE_PRIMARY,
				datacondAPI::DEFAULT_COMPRESSION_METHOD,
				datacondAPI::DEFAULT_COMPRESSION_LEVEL,
				false );
  ILwd::LdasContainer*		c( dynamic_cast<ILwd::LdasContainer*>
				   ( s.ConvertToIlwd( cmds, udt::TARGET_FRAME ) ) );
  
  AT();
  Test.Check( container2frame( *c ) ) << ": FrequencySequence" << std::endl;
}
CATCH( Test )

static void CheckSequence( )
try
{
  using namespace datacondAPI;

  const unsigned int	length( 256 );
  Sequence<REAL_4>*	_s = new Sequence<REAL_4>( 0.0, length );
  Sequence<REAL_4>&	s( *_s );

  CallChain		cmds;
  cmds.StoreIntermediateResult( _s,
				"Sequence",
				"CheckSequence",
				udt::TARGET_FRAME,
				CallChain::IntermediateResult::MAYBE_PRIMARY,
				datacondAPI::DEFAULT_COMPRESSION_METHOD,
				datacondAPI::DEFAULT_COMPRESSION_LEVEL,
				false );
  ILwd::LdasContainer*	c( dynamic_cast<ILwd::LdasContainer*>
			   ( s.ConvertToIlwd( cmds, udt::TARGET_FRAME ) ) );
  
  Test.Check( container2frame( *c ) ) << ": Sequence" << std::endl;
}
CATCH( Test )

static void CheckWelchCSDSpectrum( )
try
{
  using namespace datacondAPI;

  AT();
  const unsigned int welch_length(100);
  WelchCSDSpectrum<REAL_4>*	_sp = new WelchCSDSpectrum<REAL_4>();
  WelchCSDSpectrum<REAL_4>&	sp( *_sp );

  sp.resize( welch_length );

  sp.SetFrequencyBase( -0.5 );
  sp.SetFrequencyDelta( 1.0/welch_length );
  for ( unsigned int x = 0; x < welch_length; x++ )
  {
    sp[x] = (double)x;
  }

  sp.SetStartTime( GPSTime( 0, 0 ) );
  sp.SetEndTime( GPSTime( 10, 0 ) );

  sp.SetNameOfChannel1("channel1");
  sp.SetNameOfChannel2("channel2");

  AT();
  CallChain			cmds;
  AT();
  cmds.StoreIntermediateResult( _sp,
				"WelchCSDSpectrum",
				"CheckWelchCSDSpectrum",
				udt::TARGET_FRAME,
				CallChain::IntermediateResult::MAYBE_PRIMARY,
				datacondAPI::DEFAULT_COMPRESSION_METHOD,
				datacondAPI::DEFAULT_COMPRESSION_LEVEL,
				false );
  ILwd::LdasContainer*		c( dynamic_cast<ILwd::LdasContainer*>
				   ( sp.ConvertToIlwd( cmds, udt::TARGET_FRAME ) ) );
  
  AT();
  Test.Check( container2frame( *c ) ) << ": WelchCSDSpectrum" << std::endl;
  AT();
}
CATCH( Test )

static void CheckWelchSpectrum( )
try
{
  using namespace datacondAPI;

  const unsigned int welch_length(100);
  WelchSpectrum<REAL_4>*	_sp = new WelchSpectrum<REAL_4>();
  WelchSpectrum<REAL_4>&	sp( *_sp );

  sp.resize( welch_length );

  sp.SetFrequencyBase( -0.5 );
  sp.SetFrequencyDelta( 1.0/welch_length );
  for ( unsigned int x = 0; x < welch_length; x++ )
  {
    sp[x] = (double)x;
  }

  sp.SetStartTime( GPSTime( 0, 0 ) );
  sp.SetEndTime( GPSTime( 10, 0 ) );

  sp.SetNameOfChannel("channel");

  CallChain			cmds;
  cmds.StoreIntermediateResult( _sp,
				"WelchSpectrum",
				"CheckWelchSpectrum",
				udt::TARGET_FRAME,
				CallChain::IntermediateResult::MAYBE_PRIMARY,
				datacondAPI::DEFAULT_COMPRESSION_METHOD,
				datacondAPI::DEFAULT_COMPRESSION_LEVEL,
				false );
  ILwd::LdasContainer*		c( dynamic_cast<ILwd::LdasContainer*>
				   ( sp.ConvertToIlwd( cmds, udt::TARGET_FRAME ) ) );
  
  Test.Check( container2frame( *c ) ) << ": WelchSpectrum" << std::endl;
}
CATCH( Test )

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  CheckTimeSeries( );
  CheckSequence( );
  CheckFrequencySequence( );
  CheckCSDSpectrum( );
  CheckWelchSpectrum( );
  CheckWelchCSDSpectrum( );

  CheckDatabase( );

  Test.Exit();
}
