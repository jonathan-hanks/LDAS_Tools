//! author="Edward Maros"
// Test support code for CallChain

#include <iostream>   
   
//-----------------------------------------------------------------------
// These blocks of code are shared with other test programs.
//-----------------------------------------------------------------------
#include "ilwd/util.hh"
#include "general/unittest.h"

#include "token.hh"

#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

using namespace std;   
   
extern General::UnitTest	Test;

template<class VClass>
bool _check_valarray(const std::valarray<VClass>& Input,
		CallChain::Symbol* Result);

template<class VClass>
ostream& operator<<(ostream& vout, const std::valarray<VClass>& ValArray)
{
  vout << "{";
  for (size_t x = 0; x < ValArray.size(); x++)
  {
    if (x)
    {
      vout << ", ";
    }
    vout << ValArray[x];
  }
  vout << "}";
  return vout;
}

ostream& operator<<(ostream& vout, const std::complex<int>& x)
{
    vout << "(" << x.real() << ", " << x.imag() << ")";
    return vout;
}

template<class VClass>
bool
_check_valarray(const std::valarray<VClass>& Input,
		CallChain::Symbol* Result)
{
  datacondAPI::Sequence<VClass>*	sequence;

  sequence = dynamic_cast<datacondAPI::Sequence<VClass>*>(Result);
  if (!sequence)
  {
    return false;
  }
    
  const std::valarray<VClass>&	r(*sequence);
    
  if (r.size() != Input.size())
  {
    return false;
  }

  bool match(true);
  for (size_t x = 0; x < r.size(); x++)
  {
    if (r[x] != Input[x])
    {
      match = false;
      break;
    }
  }
  return match;
}

template<class VClass>
void
check_valarray(const std::valarray<VClass>& Input,
	       CallChain::Symbol* Result,
	       std::string Text)
{
    using namespace datacondAPI;
    bool ok = true;

    Sequence<VClass>& result = dynamic_cast<Sequence<VClass>&>(*Result);

    for (size_t k = 0; k < result.size(); k++)
    {
	if (result[k] != Input[k])
	{
	    ok = false;
	    break;
	}
    }

    std::valarray<VClass> diff(Input - result);
    
    Test.Check(ok) << Text << Input << " =?= " << result
		   << " " << diff << endl;
}

template void check_valarray(const std::valarray<float>& Input,
			     CallChain::Symbol* Result,
			     std::string Text);
template void check_valarray(const std::valarray<double>& Input,
			     CallChain::Symbol* Result,
			     std::string Text);
template void check_valarray(const std::valarray<std::complex<float> >& Input,
			     CallChain::Symbol* Result,
			     std::string Text);
template void check_valarray(const std::valarray<std::complex<double> >& Input,
			     CallChain::Symbol* Result,
			     std::string Text);

void check_udt_valarray(const datacondAPI::udt& Input,
		    CallChain::Symbol* Result,
		    std::string Text)
{
  using namespace datacondAPI;

  if (udt::IsA<Sequence<float> >(Input))
  {
    return check_valarray(udt::Cast<Sequence<float> >(Input),
			  Result, Text);
  }
  else if (udt::IsA<Sequence<double> >(Input))
  {
    return check_valarray(udt::Cast<Sequence<double> >(Input),
			  Result, Text);
  }
  else if (udt::IsA<Sequence<std::complex<float> > >(Input))
  {
    return check_valarray(udt::Cast<Sequence<std::complex<float> > >(Input),
			  Result, Text);
  }
  else if (udt::IsA<Sequence<std::complex<double> > >(Input))
  {
    return check_valarray(udt::Cast<Sequence<std::complex<double> > >(Input),
			  Result, Text);
  }
  std::string	msg("check_udt_valarray: Input is not a Sequence<>: ");
  msg += TypeInfoTable.GetName(typeid(Input));
  throw std::invalid_argument(msg);
}

template<class DataType_>
void check_value(DataType_ Value, const CallChain::Symbol& Symbol,
		 const std::string& Message)
{
  using namespace datacondAPI;
  bool       status = true;
  DataType_ comparison_value
      = udt::Cast<Scalar<DataType_> >(Symbol).GetValue();

  status = (Value == comparison_value);

  Test.Check(status) << Message << " "
		     << Value << " =?= " << comparison_value
		     << endl;
}

template void check_value(int Value, const CallChain::Symbol& Symbol,
			  const std::string& Message);
template void check_value(float Value, const CallChain::Symbol& Symbol,
			  const std::string& Message);
template void check_value(double Value, const CallChain::Symbol& Symbol,
			  const std::string& Message);

template void check_value(std::complex<float> Value, const CallChain::Symbol& Symbol,
			  const std::string& Message);
template void check_value(std::complex<double> Value, const CallChain::Symbol& Symbol,
			  const std::string& Message);

const ILwd::LdasElement*
read_ilwd(const char* Filename, const char* Env)
{
  const char*	srcdir = getenv(Env);
  std::string	full_filename;

  if (!srcdir)
  {
    Test.Check(false) << "IngestSampleData: Unable to locate source directory"
		      << endl << flush;
    return (const ILwd::LdasElement*)NULL;
  }
  if (strlen(srcdir) > 0)
  {
    full_filename = srcdir;
    full_filename += "/";
  }
  full_filename += Filename;

  ifstream	in(full_filename.c_str());

  try {
    try {
      ILwd::Reader r( in );
      ILwd::readHeader(in);

      try {
	r.skipWhiteSpace();
	try {
	  ILwd::LdasElement* e(ILwd::LdasElement::createElement( r ) );
	  return e;
	}
	catch (...)
	{
	  cerr << "FAIL: createElement" << endl;
	  throw;
	}
      }
      catch (...)
      {
	cerr << "FAIL: Skipping white spaces" << endl;
	throw;
      }
    }
    catch (...)
    {
      cerr << "FAIL: ILwd::Reader" << endl;
      throw;
    }
    
  }
  catch(...)
  {
    cerr << "FAIL: read_ilwd(" << full_filename << ")" << endl;
    throw;
  }
}
