## ******************************************************** 
## frame_concat.tcl Version 1.0
##
## Frame API routines for concatentating all sorts of
## frame data.
##
## Concatenation functions available in the C++ layer
## include:
##  
## 1.  concatAdcDataList    Concatenate AdcData
## 2.  concatAdcDataList_r  Concatenate AdcData  - return value
## 3.  concatAdcDataList_t  Concatenate AdcData  - threaded
## 4.  concatFrameList      Concatenate Frames
## 5.  concatFrameList_r    Concatenate Frames   - return value
## 6.  concatFrameList_t    Concatenate Frames   - Threaded
## 7.  concatProcDataList   Concatenate ProcData
## 8.  concatProcDataList_r Concatenate ProcData - return value
## 9.  concatProcDataList_t Concatenate ProcData - threaded
## 10. concatSerDataList    Concatenate SerData
## 11. concatSerDataList_r  Concatenate SerData  - return value
## 12. concatSerDataList_t  Concatenate SerData  - threaded
## 13. concatSimDataList    Concatenate SimData
## 14. concatSimDataList_r  Concatenate SimData  - return value
## 15. concatSimDataList_t  Concatenate SimData  - threaded 
## ;#ol
##
## All of these function similarly:
##
## They all operate on ilwd formatted data, not channel
## ptrs.
## They all return a ptr to the concatenated object.
##
## ******************************************************** 

;#barecode

package provide frame_concat 1.0

;#end


## ******************************************************** 
##
## Name: frame::concatModifyOutputFormat
##
## Description:
## If we have N buckets we need to find a format not of a
## frame type to satisfy the request.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::concatModifyOutputFormat { jobid format } {
     
     if { [ catch {
     
        if { [ regexp -nocase {frame} $format ] } {
           if { ! [ regexp {(ilwd|binary|ascii)} $format ] } {
              set format [ list ilwd ascii ]
           } elseif { [ regexp -nocase {binary} $format ] } {
              set format [ list ilwd binary ]
           } else {
              set format [ list ilwd ascii ]
           }
        }
        set ::${jobid}(-outputformat) $format

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $format
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::calcTimeRangeFromFilenames
##
## Description:
## Special handling for -framequery option where multiple
## contiguous frames are specified by name but the timerange
## is set to {}.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::calcTimeRangeFromFilenames { jobid filenames } {
     
     if { [ catch {
        
        set fn0 [ lindex $filenames 0 ]
        set fnN [ lindex $filenames end ]
        foreach [ list type0 gps0 N0 dt0 tdt0 ] \
           [ frame::rationalizedFilenameData $jobid $fn0 ] { break }
        foreach [ list typeN gpsN NN dtN tdtN ] \
           [ frame::rationalizedFilenameData $jobid $fnN ] { break }
        set dt [ expr { $gpsN + $tdtN - $gps0 } ] 
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $gps0 $dt ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::timeRanger
##
## Description:
## helper function for frame::concatElements
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::timeRanger { jobid ct1 ct2 timerange } {
     
     if { [ catch {
        set timerange [ lindex [ split $timerange : ] 0 ]
                 
        ;## mabe frame names were spec'd, but no time
        ;## range... could happen!
        set tr1 [ list ]
        set tr2 [ list ]
        foreach [ list tr1 tr2 ] [ split $timerange - ] { break }
        if { ! [ string length $tr1 ] } { set tr1 $ct1 }
        if { ! [ string length $tr2 ] } {
           set tr2 $tr1
           set timerange ${tr1}-$tr2
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $tr1 $tr2 $timerange ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::concatGaps
##
## Description:
## Analyze the list of filenames which are going to be
## concatenated across, and create metadata for handling
## gaps (missing files) if files are missing and gap
## handling was requested.
##
## This is called once for each 'times' argument to
## frame::concatElements
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::concatGaps { jobid timerange filenames gapflag } {
     
     if { [ catch {
        
        ;## handle both the -allowgaps option and the
        ;## t0-t1:allow_gaps syntax. see PR #1860
        if { $gapflag } {
           set allow_gaps 1
           uplevel set ag 1
        } elseif { [ regexp {:allow_gaps} $timerange ] } {
           set allow_gaps 1
           uplevel set ag 1
        } else {
           set allow_gaps 0
        }
        
        ;## flail around wildly trying to establish the time
        ;## range!!
        set tstart [ list ]
        set tend   [ list ]
        foreach [ list tstart tend ] [ split $timerange "-:" ] { break }
        if { ! [ string length $tstart ] } {
           set file0 [ file tail [ lindex $filenames 0 ] ]
           set fileN [ file tail [ lindex $filenames end ] ]
           if { [ regexp {\d{9,10}} $file0 tstart ] } {
                  regexp {\d{9,10}} $fileN tend
           }
        }
        if { ! [ string length $tend ] } {
           set tend [ expr { $tstart + 1 } ] 
        } else {
           set tend [ expr { $tend   + 1 } ] 
        }
        
        set filenames [ join $filenames ]
        
        array set frame [ frame::concatGapsCurry $jobid $filenames ]
        
        ;## parse the first frame file name for time info
        ;## all other files MUST be the same!
        if { [ catch {
           set proto [ lindex $frame(0) 0 ]
        } err ] } {
           set msg    "no frame files exists for the specified "
           append msg "time interval."
           return -code error $msg
        }
        
        array set temp [ frame::analyzeFilename $jobid $proto ]
        
        if { [ string equal unknown $temp(typ) ] } {
           set msg "cannot concat files with non-conforming "
           append msg "filenames like: '$proto'"
           return -code error $msg
        }
        
        set N $temp(frn)
        if { ! [ string length $N ] } { set N 1 }
        set dt $temp(tdt)
        if { ! [ string length $dt ] } { set dt 1 }
        set start_time $temp(gps)
        set dt [ expr { $N * $dt } ]
        
        foreach bucket [ lsort -integer [ array names frame ] ] {
           set file0 [ file tail [ lindex $frame($bucket) 0 ] ]
           set fileN [ file tail [ lindex $frame($bucket) end ] ]
	   array set temp [ frame::analyzeFilename $jobid $file0 ] 
	   set start $temp(gps)
	   array set temp [ frame::analyzeFilename $jobid $fileN ] 
	   set end $temp(gps)
           set end [ expr { $end + $dt } ]
           set Dt [ expr { $end - $start } ]
           ;## the frames in the bucket should still be in order
           set retval($bucket) \
              [ list $tstart $tend $start $end $Dt $dt $frame($bucket) ]
        }

        if { ! [ array exists retval ] } {
           set msg "something is wrong, got filenames: ($filenames)"
           return -code error $msg
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ array get retval ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::concatGapsCurry
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## Removed file existence checks on 10/14/03 as described
## in PR #2171

proc frame::concatGapsCurry { jobid filenames } {
     
     if { [ catch {
        
        set bucket 0
        set etime  0
        set dt     0
        set nextdt 0
		
        foreach filename $filenames {
           set last $etime
		   set dt $nextdt
           set tail [ file tail $filename ]
           
           if { [ regexp {(\d{9,10})\-(\d+)} $tail -> etime nextdt ] } {
              if { ! $last } {
                 set frame($bucket) $filename
              } elseif { [ expr { $last + $dt } ] < $etime } {
                 incr bucket
                 set frame($bucket) $filename
              } else {
                 lappend frame($bucket) $filename
              }
           } else {
              set etime 0
              set dt    0
			  set nextdt 0
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ array get frame ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::incrConcatTimeData
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::incrConcatTimeData { offset delay dt } {
     
     if { [ catch {
        set jobid [ uplevel set jobid ]
        ;## if we timesliced that needs to be taken
        ;##into account ALSO
        if { $dt != 0 } {
           ;## delay is negative offset (begin time of
           ;## data moves back on resampling)
           set offset [ expr { $offset - $delay } ]
        } else {
           set offset -$delay
           set dt 0
        }
                   
        set framep [ lindex [ frame::getImage $jobid ] 1 ]
                   
        foreach [ list i j q qq k ] \
           [ frame::timeData $framep $offset $dt ] { break }
                    
        ;## heads and tails dt overrides k
        if { $dt != 0 } { set k $dt }
                 
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $offset $dt $i $j $k ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::incrConcatDetAndHist
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::incrConcatDetAndHist { jobid { thing both } } {
     
     if { [ catch {
        foreach [ list frame framep ] [ frame::getImage $jobid ] { break }
        
        ;## handle 'proc' frames... they have not necessarily
        ;## got history or detector info.
        if { [ regexp {(both|detector)} $thing ] } {
           if { [ catch {
              set detector [ frame::detectorAndHistory \
                 $frame $framep getFrameDetectorProc all 1 ]
              frame::managePointers $jobid add $detector
              uplevel 2 lappend detectors $detector
           } err ] } {
              addLogEntry "no detector structs found in '$frame'" yellow 
           }
        }   
        
        if { [ regexp {(both|histor)} $thing ] } {
           if { [ catch {
              set history [ frame::detectorAndHistory \
                 $frame $framep getFrameHistory all 0 ]
              frame::managePointers $jobid add $history
              uplevel 2 lappend histories $history
           } err ] } {
              addLogEntry "no history structs found in '$frame'" yellow 
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::destroyResampleState
##
## Description:
## Call the correct resample handler with the correct
## options to destruct the resample state for the current
## job.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::destroyResampleState { jobid args } {
     
     if { [ catch {
        
        if { [ uplevel array exists resample_state ] } {
           array set resample_state \
              [ uplevel array get resample_state ]
           ;## how we destruct the resample state
           foreach name [ array names resample_state ] {
              foreach [ list ctype idx ] [ split $name "," ] { break } 
              frame::resampleChanPointer $jobid 0 0 $resample_state($name)
              uplevel ::unset resample_state($name)
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::timeSliceChannel
##
## Description:
##
## Note that for multi-frame files this particular version
## fails.
##
## Note also, however, that this function can be called with
## an offset and delta value in the 'time' and 'end'
## arguments to get a slice of a single file.
##
## Parameters:
##
## Usage:
##
## Comments:
## Frame at front of bucket  has delta of zero, frame at
## end of bucket has offset of zero.

proc frame::timeSliceChannel { jobid filename channel time end } {
     
     if { [ catch {
        set seqpt {}

        foreach [ list type gps N dt tdt ] \
           [ frame::rationalizedFilenameData \
              $jobid $filename ] { break }
       
        if { [ string equal head $end ] } {
           set offset [ expr { $time - $gps } ]
           set deltat [ expr { $tdt - $offset } ]
        } elseif { [ string equal tail $end ] } {
           set offset 0
           set deltat [ expr { $time - $gps } ] 
        } elseif { [ regexp {\d+} $end ] } {
           set offset $time
           set deltat $end
        } else {
           set msg "I don't know how to slice a '$end'"
           return -code error $msg
        }
        set newstart [ expr { $gps + $offset } ]
 
        if { ($offset + $deltat) > $tdt } {
           set msg    "offset: '$offset' and delta: '$deltat' "
           append msg "exceed total dt of frame: '$tdt'"
           return -code error $msg
        }
        
        ;## either we were passed a channel pointer or a
        ;## list of a command and a channel name/index
        if { [ regexp {^_[0-9a-f]+_p_} $channel ] } {
           set chanp $channel
        } else {   
           set cmd  [ lindex $channel 0 ]
           set chan [ lindex $channel 1 ]
           
           foreach [ list chan offset deltat ] \
              [ frame::parseChannelSlice $chan ] { break }
           
           foreach [ list -- framep ] \
              [ frame::getImage $jobid ] { break } 
           set seqpt "${cmd}($filename $framep $chan): "
           set chanp [ $cmd $framep $chan ]
           ;## these pointers are not destructible
           #frame::managePointers $jobid add $chanp
           set seqpt {}
        }
        
        ;## if slicing really is required...
        if { $deltat != $dt } {
           set chanp [ frame::timeWindow $jobid $chanp $offset $deltat ]
        }
        
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]:$seqpt $err"
        }
     }
     return $chanp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::runChangeBucket 
##
## Description:
## If the frame file size changes within the time range
## requested, the files on either side of the change must
## be concatenated seperately and the results concatenated.
## So this proc takes two buckets and stitches them together
## if there is no gap between them.
##
## Parameters:
##
## Usage:
##
## Comments:
## 

proc frame::runChangeBucket { jobid bucket1 bucket2 } {
     
     if { [ catch {
        set msg    "you don't need to do this, the chunker can"
        append msg " handle it transparently by just creating a"
        append msg " new bucket, which will get it's own pass"
        append msg " through the concatenation routine."
        append msg " OR, do we want to stitch them so we can "
        append msg " handle non gappy, but run spanning data??"
        return -code error $msg
        getFrameFrameHRun
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::resampleParser
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::resampleParser { channel { toss_slice 0 } } {
     
     if { [ catch {
        ;## matches any slice type action with "resample"
        ;## in the 'offset' position.
        set resample_rx {([^!]+)!(resample)!([^!]+)!}
        
        ;## handle resampling on a per-index basis, setting
        ;## the variable "resample" to the factor if it is valid.
        set resample 0
        if { [ regexp $resample_rx $channel -> chan flag factor ] } {
           set flag [ string tolower $flag ]
           if { [ string equal resample $flag ] } {
              if { [ regexp {(2|4|8|16)} $factor ] } {
                 set resample $factor
                 set channel $chan
              } else {
                 set msg "invalid resample factor: '$factor'"
                 return -code error $msg
              }
           }
        ;## toss anything else that looks like a slice
        ;## if the toss_slice flag is set
        } else {
           if { $toss_slice } {
              set channel [ lindex [ split $channel ! ] 0 ]
           }
        }
        
        set retval [ list $channel $resample ]
        
     } err ] } {
        return -code error "[ myName ]: (channel:'$channel') $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::resampleChanPointer
##
## Description:
## Almost entirely brain-dead first pass at resampling.
##
## The 'delay' value must be SUBTRACTED from the start_time
## metadata value set by frame::concatElements
## Parameters:
##
## Usage:
##
## initialisation:
##
## frame::resampleChanPointer $jobid $adcp $resample {}
##
## cleanup:
##
## frame::resampleChanPointer $jobid 0  0  $r_state
##
## Comments:
## So far we only know how to deal with Adc pointers.

proc frame::resampleChanPointer { jobid chanp resample { r_state "" } } {
     
     if { [ catch {
        set seqpt {}
        
        set ptr_rx {(Adc|Proc|Ser|Sim)}
        
        if { $resample && ! [ regexp $ptr_rx $chanp -> ctype ] } {
           set msg "attempt to resample unknown channel type: '$chanp'"
           return -code error $msg
        }
        
        set chanp_resampled  [ list ]
        set delay 0
        set new_channel 0
        
        ;## first time this is called on a run there is
        ;## no resample state
        if { $resample && ! [ string length $r_state ] } {
           set seqpt "createResampleState($resample $chanp):"
           set r_state [ createResampleState $resample $chanp ]
           set new_channel 1
        }
        
        ;## after a run call with resample set to 0 to clean up
        if { $resample } {
           set seqpt "resample${ctype}Data($chanp $r_state):"
           set chanp_resampled [ resample${ctype}Data $chanp $r_state ]
           set seqpt {}
           frame::managePointers $jobid add $chanp_resampled
           set seqpt "resampleDelay($chanp_resampled $r_state):"
           set delay [ resampleDelay $chanp_resampled $r_state ]
           set seqpt {}
           ;## I thought that I only wanted the delay from the
           ;## first section, but apparently this only works when
           ;## they are all updated... funny.
           #if { ! $new_channel } { set delay 0 }
        } else {   
           set seqpt "destructResampleState($r_state):"
           destructResampleState $r_state
        } 
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return [ list $chanp_resampled $delay $r_state ] 
}
## ********************************************************

## ******************************************************** 
##
## Name: frame::concatThread
##
## Description:
## Efficient threaded concat routine that does NOT handle
## gaps.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::concatThread { jobid framesandchans of rp } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        set errs [ list ]
        if { ! [ string length $framesandchans ] } {
           lappend errs "empty framelist received!"
        }

        if { [ llength $errs ] } { return $errs }
        
        set seqpt "createFrameGroupMulti_t($framesandchans):"
        set tid [ createFrameGroupMulti_t $framesandchans ]
        
        frame::registerTid $jobid $tid
        
        if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
             [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
           ::__t::start $tid
        }
        
        if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
             [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
           addLogEntry \
              "frames: '$framesandchans' tid: '$tid'" purple
        }
        
        after 30 [ list frame::concatThreadJoin $jobid $tid ]
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::concatThreadJoin
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::concatThreadJoin { jobid tid } {
     
     if { [ catch {
        set seqpt "setAlert($tid):"
        ::setAlert $tid ::$tid
        ::setTIDCallback $tid "frame::concatThreadCallback $jobid $tid"
     } err ] } {
        if { [ info exists ::${jobid}(cid) ] } {
           set cid  [ set ::${jobid}(cid) ]
           frame::newJobAbort $jobid $cid $err
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::concatThreadProto
##
## Description:
## Entry point for all concatenation of frame data.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::concatThreadProto { jobid ifo times type frames of rp dq ag } {
     
     if { [ catch {
        set seqpt {}
        set errs [ list ]
        set detectors [ list ]
        set histories [ list ]
        set filesandchannels [ list ]
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
       
        ;## collect all the filenames for this ifo
        set i 0
        
        set cache [ set ::filecache$job ]
        if { [ llength $cache ] == 1 } {
           set cache [ lindex $cache 0 ]
        }
        
        ;## remove extra listification if first element
        ;## in cache looks like a full cache entry with a
        ;## timerange, frames and a gapflag.
        set test_timerange [ lindex $cache 0 ]
        if { [ llength $test_timerange ] == 3 } {
           set cache [ join $cache ] 
        }
        
        ;## if we have a complex query we need to iterate,
        ;## otherwise we collapse the filelist and make a
        ;## single pass. 09-21-2003
        if { [ llength $dq ] > 1 } {
           set multi_queries $dq
           set dq [ list ]
        } else {
           foreach [ list timerange filenames gapflag ] $cache {
              lappend files $filenames
           }
           set cache [ list $timerange [ join $files ] $gapflag ]
        }
        
        ;## we were doing a 'join $cache', but this was
        ;## causing lists of filenames to get flattened
        ;## out so that gapflag was a filename...
        foreach [ list timerange filenames gapflag ] $cache {
           
           if { ! [ string length $gapflag ] } {
              if { [ llength $timerange ] == 3 } {
                 foreach \
                    [ list timerange filenames gapflag ] \
                    $timerange { break } 
              }
           }
           
           if { [ info exists multi_queries ] } {
              set dq [ stack::pop multi_queries ]
           }
          
           ;## each element in the array data, whose indices
           ;## are 0,1,2... is a list of:
           ;## starttime endtime dt filelist
           ;## for each contiguous set of files.
           array set data [ frame::concatGaps \
              $jobid $timerange $filenames $gapflag ]
           
           set nchunks [ llength [ array names data ] ] 
           
           if { ! $ag && $nchunks > 1 } {
              set msg "some frame data is missing, aborting job."
              append msg " (number of frames reported found by "
              append msg "diskcache API is a calculated value.) "
              append msg "(did you mean to use ${timerange}:"
              append msg "allow_gaps? or the -allowgaps flag?)"
              return -code error $msg
           }
              
           set chans \
              [ frame::concatThreadParseChans $jobid $data(0) $dq ]
           
           lappend filesandchannels [ list $filenames $chans ]
        }
        
        frame::concatThread $jobid $filesandchannels $of $rp
        
     } err ] } {
        if { [ string length $err ] } {
           set trace {}
           catch { set trace $::errorInfo }
           if { $::DEBUG > 1 && [ string length $trace ] } {
              set err $trace
           }
           return -code error "[ myName ]:$seqpt $err"
        }
     }
     if { [ info exists contp ] } {
        return [ list $jobid $contp ]
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::concatThreadParseChans
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## The offset and/or dt value of a slice can be suffixed
## with TIME, FREQ, or TIMEFREQ to supply hints to the
## C++ layer about the kind of data it is dealing with.
##

proc frame::concatThreadParseChans { jobid data rawdq } {
     
     if { [ catch {
        
        ;## the regexps that do all the work
        ;## parses Chan(FOO_BAR-BAZ!n.n!n.n!)
        set type_rx {(.+)\((.+)\)}
        set resample_rx {([^!]+)!resample!([^!]+)!}
        
        ;## data contains the time data array
        foreach [ list st et fst fet fdt ] $data { break }
        ;## frame end time offset from last requested time
        ;## frame end time may be > last requested time
        set feo [ expr { $fet - $et } ]
        ;## total dt requested
        set tdt [ expr { ($et - $st) }  ]
        ;## frame start time offset from requested start time
        ;## frame start time may be < requested start time
        set fso [ expr { $st - $fst } ]
        
        set retval [ list ]
        
        regsub {(all|full)\(.*\)} $rawdq {Adc(all) Proc(all)} rawdq
        
        foreach chan $rawdq {
           if { [ regexp $type_rx $chan -> ctype names ] } {
              set ctype ::[ string toupper $ctype ]_CHANNEL
              set names [ split $names "," ]
              foreach name $names {
                 
                 ;## data array type hint -- see below
                 if { [ regexp -nocase -- {(TIMEFREQ|FREQ)} $name -> hint ] } {
                    set Hz 1
                 } else {
                    set Hz 0
                    set hint TIME
                 }
                 
                 set flag 1
                 if { [ regexp $resample_rx $name -> chan fact ] } {
                    set off 0
                    set dt $tdt
                 } elseif { [ regexp {\!} $name ] } {
                    foreach [ list chan off dt ] \
                       [ frame::parseChannelSlice $name ] { break }
                    set fact 1
                 } else {
                    set chan $name
                    set off 0
                    set dt $tdt
                    set flag 0
                    set fact 1
                 }
                 
                 if { $dt != 0 && $Hz == 0 } {
                    set off [ expr { $off + $fso } ]
                 }
                 
                 ;## removed 'hint' from last position of the list
                 lappend retval \
                    [ list $chan $fact $ctype $off $dt $flag ]
              }
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::concatThreadCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::concatThreadCallback { jobid tid args } {
     
     if	{ ! [ info exist ::$tid ] } {
    	addLogEntry "::$tid does not exist" purple
        return
     }
     if { [ catch {
        regexp {\d+} $jobid job
        set seqpt {}
        
        if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
                [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
           set msg "$tid got to concatThreadCallback ([ set ::$tid])"
           addLogEntry $msg purple
        }
        
        ;## orphans are handled specially
        if { ! [ array exists ::$jobid ] } {
           return {}
        }
        
        set of   [ set ::${jobid}(-outputformat) ]
        set targ [ set ::${jobid}(-frametarget)  ]
        set cid  [ set ::${jobid}(cid)           ]
        
        set thread_state [ set ::$tid ]
        
        ;## this should handle both setAlert and polling
        ;## (or blocking) thread handlers.
        if { [ string equal FINISHED $thread_state ] || \
             [ string equal $thread_state $::TID_FINISHED ] } {
           
           set seqpt "createFrameGroupMulti_r($tid):"
           if { [ catch {
		    set contps [ createFrameGroupMulti_r $tid ]
           
              frame::unregisterTid $jobid $tid
          
              if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
                   [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] && \
                   [ info exists ::__t::S$tid ] } {
                 ::__t::end "thread $tid completed in" $tid
                 ::__t::cancel $tid
              }
              
              ;## for new multi job
              foreach contp $contps {
                 frame::managePointers $jobid add $contp
                 set seqpt {}
                 set contp [ list $jobid $contp ]
              
                 lappend ::outputs($jobid) \
                    [ list $jobid $targ $contp $of ]
           
              }
           } err ] } {
              frame::unregisterTid $jobid $tid
              lappend ::outputs($jobid,error) $err
		 }

           if { [ llength $::threadcount($jobid) ] == 0 } {
              
		    if { [ info exists ::outputs($jobid,error) ] } {
		       set err $::outputs($jobid,error)
			  ::unset ::outputs($jobid,error)
			  return $err
		    }
		    
		    foreach output $::outputs($jobid) {
                 lappend ::${jobid}_outfiles \
                    [ eval frame::output $output ]
              }
              ::unset ::outputs($jobid)
              ::unset ::threadcount($jobid)
              set outfiles [ set ::${jobid}_outfiles ]
              ::unset ::${jobid}_outfiles
              if { [ info exists ::filecache$job ] } {
                 ::unset ::filecache$job
              } 
              frame::managePointers $jobid destroy .+
        
              if { [ info exists outfiles ] } {
                 ;## this will set the global var ::$cid
                 frame::reattachMsg $jobid $cid $outfiles
              } else {
                 set ::$cid [ list 0 0 0 ]
              }

              ::reattach $jobid $cid
              if { [ info exists ::$jobid ] } {
                 ::unset ::$jobid
              }
           } else {
              if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
                 [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
                 foreach tid $::threadcount($jobid) {
                    catch { getThreadStatus $tid } state
                    set msg "thread '$tid' is in state '$state'"
                    if { [ regexp {invalid_tid} $state ] } {
                       addLogEntry $msg red
                    } else {
                       addLogEntry $msg purple
                    }
                 }
              }   
           }
        }
     } err ] } {
        if { [ string length $err ] } {
           if { [ info exists ::outputs($jobid,error) ] } {
		    lappend err $::outputs($jobid,error)
		    ::unset ::outputs($jobid,error)
		 }
		 set err "[ myName ]:$seqpt$err"
           frame::newJobAbort $jobid $cid $err
        }
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: frame::frHistory 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Given the arguments 'foo 123456789 "wagga wagga"'
##
## createHistory returns a contp that looks like:
##
## <ilwd name='foo::History:Frame' size='2'>
##     <int_4u name='time'>123456789</int_4u>
##     <lstring name='comment' size='11'>wagga wagga</lstring>
## </ilwd>
##
## Comments:
##

proc frame::frHistory { jobid comment { time "" } { name "" } } {
     
     if { [ catch {   
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        if { [ string length $time ] < 9 || \
           ! [ regexp {^\d+$} $time ]    || \
           $time < 200000000 } {
          set time [ gpsTime ]
        }
        
        if { ! [ string length $name ] } {
           set name ${jobid}-[ ::key::intgen ]
        }
        
        set ilwdhistoryp [ createHistory \
                  $name    \
                  $time    \
                  $comment \
                  ]
        frame::managePointers $jobid add $ilwdhistoryp          
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $ilwdhistoryp
}
## ********************************************************

## ******************************************************** 
##
## Name: frame::createFramesProto
##
## Description:
## Entry point for all concatenation of frame data.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::createFramesProto { jobid ifo times type frames of rp dq ag cat expand } {

     if { [ catch {
        set seqpt {}
        set errs [ list ]
        set detectors [ list ]
        set histories [ list ]
        set subtasks  [ list ]
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
       
        ;## collect all the filenames for this ifo
        set i 0
        
        set cache [ set ::filecache$job ]
        
        if { [ llength $cache ] == 1 } {
           set cache [ lindex $cache 0 ]
        }
        ;## this may not be what is needed...
        set test_timerange [ lindex $cache 0 ]
        if { [ llength $test_timerange ] == 3 } {
           set cache [ join $cache ] 
        }
        
        if { ! [ regexp {^[\d\,\-]+$} [ lindex $cache 0 ] ] } {
           set cache [ list $times $cache $ag ]
        }
        
        if { [ llength $dq    ] > 1 && \
             [ llength $cache ] > 3 } {
           set multi_queries $dq
           set dq [ list ]
        }
        
        ;## we were doing a 'join $cache', but this was
        ;## causing lists of filenames to get flattened
        ;## out so that gapflag was a filename...

        foreach [ list timerange filenames gapflag ] $cache {
           addLogEntry "time $timerange filenames $filenames gapflag $gapflag" purple
           if { [ info exists multi_queries ] } {
              set dq [ stack::pop multi_queries ]
           }
           
           ;## each element in the array data, whose indices
           ;## are 0,1,2... is a list of:
           ;## starttime endtime dt filelist
           ;## for each contiguous set of files.
           array set data [ frame::concatGaps \
              $jobid $timerange $filenames $gapflag ]

           set nchunks [ llength [ array names data ] ] 
           if { ! $ag && $nchunks > 1 } {
              set msg "some frame data is missing, aborting job."
              append msg " (number of frames reported found by "
              append msg "diskcache API is a calculated value.) "
              append msg "(did you mean to use ${timerange}:"
              append msg "allow_gaps? or the -allowgaps flag?)"
              return -code error $msg
           }
              
           set start_time [ lindex $data(0) 0 ]
           set end_time   [ lindex $data(0) 1 ]
           set fstime     [ lindex $data(0) 2 ]
           set fetime     [ lindex $data(0) 3 ]
           set fdt        [ lindex $data(0) 5 ]
           
           ;## was messing up greg's self-expanding pipeline.
           if { $expand == 0 } {
              set filenames  [ lindex $data(0) end ]
           }
           
           set chans \
              [ frame::concatThreadParseChans $jobid $data(0) $dq ]
           
           ;## not sure this is right...$filenames $chans $cat 
           if { [ regexp -nocase {Proc} $chans ] } { set cat 0 }
           
           lappend subtasks \
              [ list $filenames $chans $cat ]
			addLogEntry "added subtasks $filenames $chans $cat" purple
        }
        
        frame::createFrames $jobid $subtasks $of $rp
        
     } err ] } {
        if { [ string length $err ] } {
           set trace {}
           catch { set trace $::errorInfo }
           if { $::DEBUG > 1 && [ string length $trace ] } {
              set err $trace
           }
           return -code error "[ myName ]:$seqpt $err"
        }
     }
     if { [ info exists contp ] } {
        return [ list $jobid $contp ]
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::createFrames
##
## Description:
## Efficient threaded concat routine that does NOT handle
## gaps.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::createFrames { jobid subtasks of rp } {
     
     if { [ catch {
        set seqpt {} 
        set errs [ list ]
        
        foreach task $subtasks {
           foreach [ list frames chans cat ] $task {
        
              if { ! [ string length $frames ] } {
                 lappend errs "empty framelist received!"
              }
              if { ! [ string length $chans ] } {
                 lappend errs "empty channel list received!"
              }
           
           }
        }
        
        if { [ llength $errs ] } { return $errs }
       
        set seqpt "createFrameSetMulti_t($subtasks):"
        set tid [ createFrameSetMulti_t $subtasks ]
        
        frame::registerTid $jobid $tid
       
        if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
             [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
           ::__t::start $tid
        }
        
        if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
             [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
           addLogEntry "$seqpt '$tid'" purple
        }
        
        ::setAlert $tid ::$tid
        ::setTIDCallback $tid "frame::createFramesCallback $jobid $tid $subtasks"
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::createFramesCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::createFramesCallback { jobid tid subtasks args } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set seqpt {}
        
        if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
                [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
           set msg "$tid got to createFramesCallback ([ set ::$tid])"
           addLogEntry $msg purple
        }
        
        ;## orphans are handled specially
        if { ! [ array exists ::$jobid ] } {
           return {}
        }
        
        set of   [ set ::${jobid}(-outputformat) ]
        set targ [ set ::${jobid}(-frametarget)  ]
        set cid  [ set ::${jobid}(cid)           ]
        
        set thread_state [ set ::$tid ] 
        
        ;## this should handle both setAlert and polling
        ;## (or blocking) thread handlers.
        if { [ string equal FINISHED $thread_state ] || \
             [ string equal $thread_state $::TID_FINISHED ] } {
           
           set seqpt "createFrameSetMulti_r($tid):"
           set Frameps [ createFrameSetMulti_r $tid ]
           
           frame::unregisterTid $jobid $tid
            
           if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
                [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] && \
                [ info exists ::__t::S$tid ] } {
              ::__t::end \
                 "thread $tid (returned: $Frameps) completed in" $tid
              ::__t::cancel $tid
           }
           
           set i 0
           foreach frameps $Frameps {
              set frames [ lindex [ lindex $subtasks $i ] 0 ]
              set cat    [ lindex [ lindex $subtasks $i ] 2 ]
              incr i
              frame::managePointers $jobid add $frameps
           
              set seqpt {}
           
              lappend ::outputs($jobid) \
                 [ list $jobid $frames $frameps $of $targ $cat ]
           }
           
           if { [ llength $::threadcount($jobid) ] == 0 } {
              foreach output $::outputs($jobid) {
                 eval frame::createFramesOutput $output
              }
              ::unset ::outputs($jobid)
              ::unset ::threadcount($jobid)
              
              set outfiles [ set ::${jobid}_outfiles ]
              ::unset ::${jobid}_outfiles
              if { [ info exists ::filecache$job ] } {
                 ::unset ::filecache$job
              }

              frame::managePointers $jobid destroy .+
        
              if { [ info exists outfiles ] } {
                 ;## this will set the global var ::$cid
                 frame::reattachMsg $jobid $cid $outfiles
              } else {
                 set ::$cid [ list 0 0 0 ]
              }
              
              ::reattach $jobid $cid
              if { [ info exists ::$jobid ] } {
                 ::unset ::$jobid
              }
           } else {
              if { [ info exists ::DEBUG_CREATE_FRAME_GROUP ] && \
                 [ string equal 1 $::DEBUG_CREATE_FRAME_GROUP ] } {
                 foreach tid $::threadcount($jobid) {
                    catch { getThreadStatus $tid } state
                    set msg "thread '$tid' is in state '$state'"
                    if { [ regexp {invalid_tid} $state ] } {
                       addLogEntry $msg red
                    } else {
                       addLogEntry $msg purple
                    }
                 }
              } 
           }
        }

     } err ] } {
        if { [ string length $err ] } {
           set err "[ myName ]:$seqpt$err"
           frame::newJobAbort $jobid $cid $err
        }
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: frame::createFramesOutput
##
## Description:
## Output routine specific to createFrames when it creates
## frames.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::createFramesOutput { jobid frames frameps format target cat } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        ;## we force frame named output since this function
        ;## outputs frames
        if { $cat == 0 && \
           [ llength $frames ] == [ llength $frameps ] } {
           set ::${jobid}(Frame) 1
           foreach  file $frames framep $frameps {
              set framep [ list $file $framep ]
              lappend ::${jobid}_outfiles \
                 [ frame::output $jobid $target $framep $format ]
           }
        } else {
           set file [ file tail [ lindex $frames 0 ] ]
           set ::${jobid}(Frame) 1
           foreach framep $frameps {
              set framep [ list $file $framep ]
              lappend ::${jobid}_outfiles \
                 [ frame::output $jobid $target $framep $format ]
           }
        }
        ::unset ::${jobid}(Frame)
        
        if { ! [ info exists ::${jobid}_outfiles ] } {
           set err "got these framep's: '$frameps' "
           append err "but no output files produced!"
           return -code error $err
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

