## ******************************************************** 
## frame.tcl Version 1.0
##
## Wraps the frameAPI.so and the genericAPI.so and the
## genericAPI.tcl for use by the frame API.
## 
## frame::cache() is a list of directories and the gps
## timestamps of the first and last frames in those
## directories.
##
## frame.tcl requires two other frame API specific Tcl
## source files:
##
## 1. frame_concat.tcl
## 2. frame_output.tcl
## ;#ol
##
## ******************************************************** 

;#barecode
set ::RCS_ID_frametcl {$Id: frame.tcl,v 1.667 2008/10/16 21:30:04 mlei Exp $}
set ::RCS_ID_frametcl [ string trim $::RCS_ID_frametcl "\$" ]

package provide frame 1.0

package require frameAPI

#package require frame_cache
package require frame_output
package require frame_concat

set ::last_job_start_time [ clock seconds ]

namespace eval frame {
   set errlvl 1
   ;## the current interferometer id
   set ifoid  {}
   ;## images are pairs of jobid and framep
   set images [ list ]
   ;## the list of pending jobs
   set newjobqueue [ list ]
   
   ;## options are defined with a name, a regex pattern
   ;## which matches them, and a default value if applicable.
   ;## the variables will not get substituted at this point.
   ;## note that the REQUIRED options have no default and
   ;## will raise exceptions if not provided.
   set options {
       -interferometers {.*} {}
       -outputformat    {.*} {ilwd ascii}
       -returnprotocol  {.*} {}
       -concatenate     {^(0|\-?1)$} 0
       -framequery      {.+}     {}
       -targetapi       {.*}     {}
       -allowgaps       {^[01]$} 0
       -frametarget     {.*}     {}
       -jobid           {\d+}    $jobid
   }

   ;## atoms are framequery string fragments which must be
   ;## properly capitalised for calculating the appropriate
   ;## accessor method for the framequery.
   ;## all dataqueries are forced to lowercase and then
   ;## substituted with thse patterns.
   ;## ORDER MATTERS!!
   set atoms [ list \
       FrameH Frame Proc DataQuality Data List Sim Ser Adc Dt \
       NAuxParam AuxParam AuxParamNames \
       Raw Channel Comment Group Valid History FShift Number \
       Num Name Sample Rate Arm Xazimuth Yazimuth Elevation Phase \
       Detector Latitude Longitude Xaltitude Xmidpoint Yaltitude \
       Ymidpoint Time Local Msg Message Severity EventStatus Event Statistics \
       Trigger Test End Start Inputs OffsetN OffsetS Probability \
       Status Offset Amplitude After Before Stat Representation \
       Version Strain Summary Trig ULeapS Daq Binary GTime Quality \
       Attribute Units QaBitList Slope NBits Group Bias Aux Run \
       BW SubType Type FRange TRange \
       NParam Parameters ParameterNames \
   ]
 }  

;#end

## ******************************************************** 
##
## Name: frame::destructElementWrap
##
## Description:
## destructElement and then remove swig names
## 
## Parameters:
##
## Usage:
##
## Comments:

proc frame::destructElementWrap { ptr } {

	if	{ [ catch {
    	destructElement $ptr
       	if  { [ string length [ info commands $ptr ] ] } {
            # addLogEntry "rename $ptr [ info commands $ptr ] to null" purple
            rename $ptr {}
        }
  	} err ] } {
    	return -code error $err
    }
}

## ******************************************************** 
##
## Name: frame::init
##
## Description:
## Entry point for the LDASgwap script to initialise
## the frame API.  Initialises the frame name cache, and
## starts the bgLoop (see genericAPI.tcl) controlled frame
## cache update loop.
##
## Parameters:
##
## Usage:
##
## Comments:
## It was possible to initialise the frame API on a machine
## which automounted the frame server, using about %5 of the
## cpu, with an average load of 2 with nothing else running.

proc frame::init { } {
     if { [ catch {
        
        if { ! [ info exists ::DEBUG_QUERY_EXPANSION ] } {
           set ::DEBUG_QUERY_EXPANSION 0
        } elseif { [ string equal false [ string tolower \
           $::DEBUG_QUERY_EXPANSION ] ] } {
           set ::DEBUG_QUERY_EXPANSION 0
        } elseif { ! [ regexp {^[01]$} $::DEBUG_QUERY_EXPANSION ] } {
           set ::DEBUG_QUERY_EXPANSION 1
        }

        ;## ::DEBUG_FILE2PTR must follow the same rules.
        if { ! [ info exists ::DEBUG_FILE2PTR ] } {
           set ::DEBUG_FILE2PTR 0
        } elseif { [ string equal false [ string tolower \
           $::DEBUG_FILE2PTR ] ] } {
           set ::DEBUG_FILE2PTR 0
        } elseif { ! [ regexp {^[01]$} $::DEBUG_FILE2PTR ] } {
           set ::DEBUG_FILE2PTR 1
        }
       
        ;## ::DEBUG_FILECACHE must follow the same rules.
        if { ! [ info exists ::DEBUG_FILECACHE ] } {
           set ::DEBUG_FILECACHE 0
        } elseif { [ string equal false [ string tolower \
           $::DEBUG_FILECACHE ] ] } {
           set ::DEBUG_FILECACHE 0
        } elseif { ! [ regexp {^[01]$} $::DEBUG_FILECACHE ] } {
           set ::DEBUG_FILECACHE 1
        }
        
        if { ! [ info exists ::FRAME_NEW_JOB_RATE ] } {
           set ::FRAME_NEW_JOB_RATE 2
        }
        set msg "::FRAME_NEW_JOB_RATE is set to "
        append msg "$::FRAME_NEW_JOB_RATE seconds. "
        append msg "(1 to 15 seconds is usual, default "
        append msg "is 2)."
        addLogEntry $msg green

		# if	{ ! [ info exist ::DUMPDIR ] } {
        #	set ::DUMPDIR /ldas_outgoing/test/frame_params
        #}
        # if	{ ! [ file exist $::DUMPDIR ] } {
        #   	file mkdir $::DUMPDIR
        #}
        # addLogEntry "job parameters are dumped in $::DUMPDIR" purple
        
        if { [ catch {
           #frame::sanity STARTUP0
        } err ] } {
           addLogEntry "Frame API init error: $err" 2
           puts stderr "Frame API init error: $err"
        } 
       	
        if	{ ![ info exist ::REAP_THREAD_DELAY ] } {
        	set ::REAP_THREAD_DELAY 5000
        }
        if { ! [ info exists ::DEVICE_IO_CONFIGURATION ] } {
	    set ::DEVICE_IO_CONFIGURATION [ list ]
        }
        if  { ! [ info exists ::DELAY_RECV_FRAMES_FROM_DISKCACHE_SECS ] } {
	        set ::DELAY_RECV_FRAMES_FROM_DISKCACHE_SECS 120
        }
    	if	{ [ regexp {8.4} $::tcl_version ] } {
    		trace add variable ::DEVICE_IO_CONFIGURATION { write } \
	    	frame::updateDeviceIOConfiguration
        	trace add variable ::DEVICE_IO_CONFIGURATION { unset } \
	    	frame::updateDeviceIOConfiguration
    	} else {
			trace variable ::DEVICE_IO_CONFIGURATION wu \
	    	frame::updateDeviceIOConfiguration
        }
	    frame::updateDeviceIOConfiguration

        bgLoop framejobqueue frame::newJob 3
        bgLoop defunctjobs frame::defunctJobs 300
        bgLoop deviceioconfig frame::bgUpdateDeviceIOConfiguration 300
           
     } err ] } {
        addLogEntry "Frame API init error: $err" 2
        puts stderr "Frame API init error: $err"
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::dictionary
##
## Description:
## Read the frame dictionary and creates an associative
## array which holds all of the data types for all of the
## frame structures.
##
## The array is named ::frame::frame()
##
## Parameters:
##
## Usage:
##
## Comments:
## As of today (12/18/00) there are 208 distinct objects,
## any of which may be a ptr to an array of the objects.
## An ADC channel, for instance, is an element of an array
## of an ::frame::frame(FrRawData,firstAdc) and
## ::frame::frame(FrAdcData,next) which reference an array
## of type PTR_STRUCT(FrAdcData *).

proc frame::dictionary { } {
     
     if { [ catch {
        set seqpt "getFrameDictionary:"
        set dic [ getFrameDictionary ]
        array set ::frame::frame $dic
        foreach struct [ array names ::frame::frame ] {
           foreach [ list name type ] [ set ::frame::frame($struct) ] {
              set ::frame::frame($struct,$name) $type   
           }
           unset ::frame::frame($struct)
        }
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return [ llength [ array names ::frame::frame ] ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::parseFrameQuery 
##
## Description:
## Parse the -framequery option into multiple associated
## requests if it looks like a compound query.
##
## The -frames, -times and -ifos options are NOT used
## when a compound -framequery is detected.
##
## Compound arguments are always formatted as a list of
## 4 elements with empty elements represented as "{}".
##
## The elements must be presented in correct order:
##
## { types ifos frames times query }
##
## Parameters:
##
## Usage:
##
##   supports -framequery options in several formats:
##
##  -framequery { F {H L} {} 600000000 Adc(0) }
##  -framequery { { F H {} 600000000 Adc(0) } { R L {} 600000000 Adc(100) } }
##
## Comments:
##

proc frame::parseFrameQuery { jobid args } {
     if { [ catch {
        set retval [ list ]
        
        if { [ llength $args ] == 1 } {
           ;## Determine whether this is a simple query
           ;## or a complex query
           set qlist [ lindex $args 0 ]
           if { [ llength $qlist ] == 5 && \
                [ llength [ lindex $qlist 0 ] ] != 5 } {
              ;## This is a simple query
           } else {
              ;## This is a complex query,
              ;## so modify args to be a list of
              ;## the component simple-queries
              set args $qlist
           }
        }
          
        foreach arg $args {
           if { ! [ string length $arg ] } { continue }
           foreach [ list types ifos times frames channels ] \
                   [ frame::analyzeQueryElement $jobid $arg ] { break } 
           lappend retval [ list $types $ifos $times $frames $channels ]
        }
        
        if { $::DEBUG_QUERY_EXPANSION } {
           addLogEntry "(types ifos times frames chans): '$retval'" purple
        }     
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::analyzeQueryElement
##
## Description:
## Calculates the frame files and object list for a
## -framequery option.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::analyzeQueryElement { jobid args } {
     
     if { [ llength $args ] == 1 } {
        set args [ lindex $args 0 ]
     }
     
     if { [ catch {
        set types           [ list ]
        set frames          [ list ]
        set channels        [ list ]
        set interferometers [ list ]
        set times           [ list ]
        foreach [ list typs ifos frms tims ques ] $args { break }
        
        if { ! [ string length $typs ] } { set typs R }
        if { ! [ string length $ifos ] } { set ifos 0 }
      
        set typ_length [ llength $typs ]
        set ifo_length [ llength $ifos ]
        set frm_length [ llength $frms ]
        set tim_length [ llength $tims ]
        set que_length [ llength $ques ]
           
        foreach frame $frms {
           array set temp [ frame::analyzeFilename $jobid $frame ]
           set name $temp(type)
        }
        if { [ info exists frms_tmp ] && [ string length $frms_tmp ] } {
           set frms $frms_tmp
        }
           
        ;## specific channels from explicitly named
        ;## frames.
        if { $frm_length && $que_length } {
           set interferometers {}
           set ifo_length 0
           set types           {}
           set typ_length 0
           
           if { [ regexp {\d{9,10}} $tims ] } {
              set times $tims
           } else {
              set gps $temp(gps)
              set times $gps-[ expr {$gps + $temp(tdt) - 1} ]
           }
           set frames $frms
           set channels $ques
        }
           
        ;## specific channels over some time range with
        ;## ifo(s) specified.  this is slightly complicated
        ;## by the fact that frames will have channels for
        ;## more than one ifo, as in H0, H1 and H2 channels in
        ;## a Hanford frame...
        if { $typ_length && $ifo_length && $tim_length && $que_length } {
           foreach ifo $ifos {
              set types $typs
              set interferometers $ifos
              set times $tims
              set frames {}
              set channels $ques
           }
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $types $interferometers $times $frames $channels ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::expandDataQuery
##
## Description:
## Parses the framequery option from the frame commands and
## returns a list of two elements for each query item,
## the c++ command that should be called, and the indices
## to call it over if applicable.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::expandDataQuery { query } {
     set errors [ list ]
     set item   [ list ]
     
     set query_pat {(\S+)\s*\(\s*([^\)]*)\s*\)} 
     
     if { [ llength $query ] == 1 } {
        set query [ lindex $query 0 ]
     }
     
     ;## seed the query accessor method name
     foreach item $query {
        if { [ catch {
           set pathological 0
           set names  [ list ]
           
           ;## either it matches or it doesn't, no big deal
           regexp $query_pat $item -> item names
           regsub {\s+} $names "," names
           
           ;## should probably be a nocase match...
           ;## PR #2134 - nocase where appropriate 04/13/04
           switch -regexp -- $item {
                     {concat} { ;## this may not belong here
                           set op concat
                           }
           {([Aa]ll|[Ff]ull)} {
                           set op fullFrame2container
                           set names [ list ]
                           }
                {[Dd]etector$} {
                           set op DetectorProc
                           }
              {[Dd]etector[Pp]roc} {
                           set op DetectorProc
                           }
              {[Dd]etector[Ss]im} {
                           set op DetectorSim
                           }
       {([Aa][Dd][Cc]|[Ss][Ee][Rr]|[Ss][Ii][Mm]|[Pp][Rr][Oo][Cc])} {
                           regexp -nocase {Adc|Ser|Sim|Proc} $item op
                           set op [ ucase -strict $op ]
                           }
	       {([Ss]tat[Dd]ata|[Ss]ummary)} {
                           regexp -nocase {Stat|Summary} $item op
                           set op [ ucase -strict $op ]
                           }
                   default {
                           set pathological 1
                           set op {}
                           }
           }
           
           set names [ numRange $names ]
           
           ;## expand query accessor seed into command name
           ;## by appending atoms to op as long as they match
           ;## the query string
           while { 1 } {
              foreach atom $::frame::atoms {
                 if { [ regexp -nocase -- ^$op$atom $item ] } {
                    set op $op$atom
                    break
                 }
                 ;## don't leave a dangling atom if there was
                 ;## no match
                 set atom {}
              }
              ;## stop name building the first time nothing
              ;## matches
              if { ! [ string length $atom ] } { break }
           }
           
        } err ] } {
           ;## collect errors, and try to continue
           lappend errors $err
        }
        
        ;## attach "getFrame" prefix as appropriate
        if { ! [ regexp -nocase {((get|full)Frame|concat)}  $op ] } {
           set op getFrame$op
        }
        
        ;## interpose "Data" atom if we think it's lacking
        if { ! $pathological } {
           if { ! [ llength [ info commands $op ] ] } {
              regsub {getFrame[A-Z][a-z]+} $op {&Data} op
           }
        }   
        
        ;## a couple of simple replacements
        regsub {SimDataEvent} $op SimEvent op
        regsub {SummaryData}  $op Summary  op
 
        ;## and test the resulting operation for existence
        if { ! [ llength [ info commands $op ] ] } {
           if { [ string equal getFrame $op ] } {
              lappend errors "badly formed query atom: '$item'"
           } else {
              lappend errors "calculated operation: '$op' does not exist" 
           }
        } else {
           ;## and if it does, add it to the ops list
           lappend retval [ list $op $names ]
        }   
     
     } ;## end of foreach
     
     if { [ llength $errors ] && [ info exists retval ] } {
        return -code error "[ myName ]: $errors"
     }
     if { [ info exists retval ] } {
        return $retval 
     } else {
        return -code error "[ myName ]: $errors (malformed query?)"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::getOptArray
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::getOptArray { jobid } {
     set args     [ list ]
     set errs     [ list ]
     set opts     [ list ]
 
     if { [ catch {
        ;## automagically get all arguments which are
        ;## relevant to this API
        foreach [ list name rx default ] $::frame::options {
           set name [ string trim $name "=" ]
           set name [ string tolower $name ]
           if { [ info exists ::${jobid}($name) ] } {
              set arg [ set ::${jobid}($name) ]
              if { [ regexp -- $rx $arg ] } {
                 lappend args $name $arg
              } else {
                 lappend errs "$name '$arg'"
              }
           }
           lappend opts $name $default
        }

        if { [ llength $errs ] } {
           return -code error "malformed arguments: $errs"
        }
        
        foreach [ list opt val ] [ expandOpts ] {
           set opt [ string trim $opt - ]
           lappend arglist $opt $val
        }
     
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $arglist
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::newJob
##
## Description:
## Insure that two jobs do not start on each other's heels,
## causing FILO behaviour.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::newJob { { jobid "" } } {
     if { [ catch {
        
        set now [ clock seconds ]
        
        ;## get list of existing jobs
        set jobqueue $::frame::newjobqueue
        set ::frame::newjobqueue [ list ]
        
        ;## collect a new job if called by macro
        if { [ string length $jobid ] } {
           ;## uplevel #1 is the operator socket handler
           set cid [ set ::${jobid}(cid) ]
           ;## detach from the assistant manager
           if	{ [ regexp {8.4} $::tcl_version ] } {
            	foreach entry [ trace info variable ::$cid ] {
        			foreach { oplist cmd } $entry { break }
        			trace remove variable ::$cid $oplist $cmd
                }
			} else {
           		trace vdelete ::$cid w "reattach $jobid $cid"
           	}
           lappend jobqueue [ list $jobid $cid $now 0 ]
        }
        
        ;## make certain that at least 5 seconds have elapsed
        ;## since the last job was started
        set rate [ expr { $now - $::last_job_start_time } ]
        if { $rate < $::FRAME_NEW_JOB_RATE } {
           set ::frame::newjobqueue $jobqueue
           return {}
        }
        
        set triggered 0
        
        foreach item $jobqueue {
           
           foreach [ list jobid cid stime metric ] $item { break }
        
           regexp {\d+} $jobid job
           set jobid $::RUNCODE$job
          
           if { $triggered } {
              ;## the job goes back on the list...
              lappend ::frame::newjobqueue \
                 [ list $jobid $cid $stime $metric ]
           } elseif { [ info exists ::filecache$job ] } {
              
              set test [ set ::filecache$job ]
              
              if { ! [ string length $test ] } {
                 set err "aborted job $jobid"
                 frame::newJobAbort $jobid $cid $err
                 continue
              }
           
              if { [ llength $test ] == 1 && \
                   [ llength [ lindex [ join $test ] 0 ] ] == 3 } {
                 set ::filecache$job [ join [ set ::filecache$job ] ]
              }
          
              if { [ info exists ::DEBUG_FILECACHE ] && \
                   [ string equal 1 $::DEBUG_FILECACHE ] } {
                 addLogEntry [ set ::filecache$job ] purple
              }
           
              ;## okay, we're ready to go!!
              set triggered 1
              set ::last_job_start_time $now
              after 0 frame::newJobCallback $jobid $cid
        
           } else {
              
              if { ($now - $stime) > $::DELAY_RECV_FRAMES_FROM_DISKCACHE_SECS } {
                 set err "no frames received from diskcache API"
                 frame::newJobAbort $jobid $cid $err
              } else {
                 ;## the job goes back on the list...
                 lappend ::frame::newjobqueue \
                    [ list $jobid $cid $stime $metric ]
              }
           }
        }
      } err ] } {
        if { [ string length $err ] } {
           if { ! [ info exists cid ] } {
              set cid unknown_sockid
           }
           frame::newJobAbort $jobid $cid $err
         }
      }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::newJobAbort
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::newJobAbort { jobid cid err } {
     
     if { [ catch {
        set flag 0
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        addLogEntry $err red

        #set fd [ open ${::DUMPDIR}/${jobid}.params w ]
        #puts $fd [ array get ::$jobid ] 
        #close $fd
        
        set ::$cid [ list 3 $err error! ]
        ::reattach $jobid $cid 
        if { [ info exists ::${jobid}(Frame) ] } {
           ::unset ::${jobid}(Frame)
        }
        set flag 1
        frame::killJob $jobid
     } err ] } {
        addLogEntry $err red
        if { $flag == 0 } {
           frame::killJob $jobid
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::newJobCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::newJobCallback { jobid cid } {
     set outfiles [ list ]
     
     if { [ catch {
        
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        array set opt [ frame::getOptArray $jobid ]
        
        set rp     $opt(returnprotocol)
        set of     $opt(outputformat)
        set concat $opt(concatenate)
        set targ   $opt(frametarget)
        set ag     $opt(allowgaps)
        
        ;## Peter Shawhan asked for this (PR#823)
        regsub -- {(http|ftp|file|port):([^\/])} $rp {\2} rp
        
        ;## are results going to another API?
        set target $opt(frametarget)
        if { [ string length $target ] } {
           set api_rx ^([ join $::API_LIST | ])\$
           ;## determine whether the target is an api or a file
           if { [ regexp -- $api_rx $target ] } {
              set rp $target
           }
        }   
       
        ;## new style complex framequery handler with
        ;## full backwards compatibility
        set _ifos   [ list ]
        set _times  [ list ]
        set _frames [ list ]
        set _dq     [ list ]
        
        set parsed_frame_query \
           [ frame::parseFrameQuery $jobid $opt(framequery) ]
        ;## how many framequeries were chained together?   
        set N_queries [ llength $parsed_frame_query ]
        
        foreach retval $parsed_frame_query {
           foreach [ list _types _ifos _times _frames _dq ] $retval {
              ;## old style dataqueries will return a short list
              if { ! [ string length $_dq ] } {
                 set dq $opt(framequery) 
              } elseif { ! [ string length $_types ] } {
                 set types R
                 set ifos   $_ifos
                 set times  $_times
                 set frames $_frames
                 set dq     $_dq
              } else {
                 set types  $_types
                 set ifos   $_ifos
                 set times  $_times
                 set frames $_frames
                 set dq     $_dq
              }
              
              foreach item { types ifos times frames dq } {
                 if { [ llength [ set $item ] ] == 1 } {
                    set $item [ lindex [ set $item ] 0 ]
                 }
              }
           }
           
           set rawdq $dq
           
           set dq [ frame::expandDataQuery $dq ]
           
           ;## some fancy flow control here so we can, if
           ;## necessary, synch up framequeries.
           if { $concat != -1 && $N_queries > 1 } {
              eval lappend dqs $dq
              incr N_queries -1
              eval lappend rawdqs $rawdq
              continue
           }
           
           if { [ info exists dqs ] } {
              eval lappend dqs $dq
              eval lappend rawdqs $rawdq
              set dq $dqs
              set rawdq $rawdqs
              unset rawdqs
              unset dqs
           }
          
           ;## default interferometer is '0'
           if { ! [ llength $ifos ] } { set ifos 0 }

           if { $::DEBUG_QUERY_EXPANSION } {
              set debug "types: $types ifos: '$ifos'  frames: '$frames' "
              append debug " times: '$times'  query: '$dq' "
              append debug " of: '$of'  rp: '$rp'"
              addLogEntry $debug purple
           }
           
           foreach ifo $ifos {
              foreach type $types {
                 lappend contptrs [ frame::runNewJob \
                    $jobid $concat $ifo $frames $times $dq $type \
                    $of $rp $ag $rawdq ]
              }
           }
        } ;## end of foreach on -framequery option
        
        ;## we only handle output up here if we are going through
        ;## collectElements, which uses a special -concatenate
        ;## option value of '-1'.
        if { $concat == -1 } {
           unset ::filecache$job
           set contptrs [ frame::mergePointers $jobid $contptrs ]
           set contptrs [ list $jobid $contptrs ]
           set outfiles [ frame::output $jobid $targ $contptrs $of ]
        
           frame::destructImage $jobid -nocomplain
           frame::managePointers $jobid destroy .+
           
           frame::reattachMsg $jobid $cid $outfiles
     
           reattach $jobid $cid
           if { [ info exists ::$jobid ] } {
              ::unset ::$jobid
           }
        }
     } err ] } {
        frame::newJobAbort $jobid $cid $err
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::reattachMsg
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::reattachMsg { jobid cid filenames } {
     
     if { [ catch {
        set of      [ set ::${jobid}(-outputformat)   ]
        set rp      [ set ::${jobid}(-returnprotocol) ]
        set target  [ set ::${jobid}(-targetapi)      ]
        set subj    [ set ::${jobid}(-subject)        ]
        set query   [ set ::${jobid}(-framequery)     ]
        set ftarget [ set ::${jobid}(-frametarget)    ]
        
        ;## default return value is a continuation
        if { [ regexp {LIGO_LW} $of ] || \
             [ string length $target ] } {
           set msg [ list  0 0 0 ]
        }
        
        if { ! [ string length $subj ] } {
           set subj "frame API output"
        }
        
        set protocol http
    
        ;## if the frame API is the target, and the user
        ;## supplied an explicit return protocol, use it.
        ;## otherwise use http.
        if { [ string length $rp ] && 
             [ regexp -nocase {frame} $target ] } {   
           regexp -nocase {^(file|ftp|http)} $rp protocol
           set msg [ macroReturnMsg $jobid $protocol $filenames ]    
           set msg [ list 2 [ lindex $msg 1 ] $subj ] 
        ;## no specified return protocol but frame is target 
        } elseif { [ regexp -nocase {frame} $target ] } {
           set jobdir [ jobDirectory ]
           regsub $::HTTPDIR $jobdir {} jobdir
           set msg    "Your results:\n${filenames}\ncan be found at:\n"
           append msg "$::HTTPURL$jobdir/"
           set msg [ list 2 $msg $subj ]
        ;## another API is the target, but we dropped files
        ;## somehow.
        } elseif { ! [ info exists msg ] } {
           regexp -nocase {^(file|ftp|http)} $rp protocol
           set msg [ macroReturnMsg $jobid $protocol $filenames ]    
           set msg [ list 2 [ lindex $msg 1 ] $subj ] 
        }
        
        set ::$cid $msg 
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::runNewJob 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::runNewJob { jobid concat ifo frames times query type of rp ag rawdq } {
     
     if { [ catch {
        set contptrs [ list ]
        
        set dir [ jobDirectory ]

        set target [ set ::${jobid}(-frametarget) ]

        if { [ info exists ::${jobid}(-autoexpand) ] } {
           set expand 1
        } else {
           set expand 0
        }
       
        ;## if we are going to concatenate, we need to know
        ;## if we're making frames or just packing up channels
        if { [ regexp -nocase {frame} $of ] && $concat == 1 } {
           set fo 1
        } else {
           set fo 0
        }
        
        if { $concat == 1 && $fo == 0 } {   
           frame::concatThreadProto \
              $jobid $ifo $times $type $frames $of $rp $rawdq $ag
        } elseif { $concat == -1 } { 
           set contptrs \
              [ frame::collectElements $jobid $frames $times $query ]
        } else {
           frame::createFramesProto  \
              $jobid $ifo $times $type $frames \
              $of $rp $rawdq $ag $concat $expand
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $contptrs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::mergePointers
##
## Description:
## Merge return pointers into a single container on a per
## job basis.
##
## Parameters:
##
## Usage:
##
## Comments:
## Pointers as received here are in fact jobid/ptr pairs
##

proc frame::mergePointers { jobid contptrs } {
     
     if { [ catch {
        set seqpt {}
        set ptr   {}
        set newcontp [ lindex $contptrs 0 ]
        set newcontp [ lindex $newcontp 1 ]
        if { [ llength $contptrs ] > 1 } {
           set newcontp [ ilwd::newcontp ]
           frame::managePointers $jobid add $newcontp
           foreach pair $contptrs {
              foreach [ list jobid ptr ] $pair {
                 set seqpt "addContainerElement($newcontp $ptr):"
                 ::addContainerElement $newcontp $ptr
                 set seqpt {}
                 frame::managePointers $jobid delete $ptr
              }
           }
        }
     } err ] } {
        return -code error "[ myName ]:$seqpt: $err"
     }
     return $newcontp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::analyzeFilename
##
## Description:
## Encapsulates all the file naming conventions ever
## proposed, with heaviest weight to the "current" proposal.
##
## First fallback is the so-called Thursday format, which
## Hanford started using for some time on 11-03-2001.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::analyzeFilename { jobid filename } {
     
     if { [ catch {
        set atoms [ frame::analyzeOfficialFilename $jobid $filename ] 
        if { [ llength $atoms ] == 6 } {
           set frame(type) official
           set frame(ifo) [ lindex $atoms 1 ]
           set frame(typ) [ lindex $atoms 2 ]
           set frame(gps) [ lindex $atoms 3 ]
           ;## we will have to use the tdt and
           ;## getFrameNumber to set the "N" and "dt" later
           set frame(frn) {}
           set frame(tdt) [ lindex $atoms 4 ]
           set frame(ext) gwf
           set atoms [ array get frame ]
        } else {
           set msg "frame filename formats not adhereing to "
           append msg "the 'official' naming convention are "
           append msg "no longer supported, and the frame: "
           append msg "'$filename' does not conform to the "
           append msg "ifo-frame_type-gps-dt.gwf format." 
           return -code error $msg
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $atoms
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::analyzeOfficialFilename
##
## Description:
## Analyze filenames adhereing to the current proposed
## frame file naming specification, which looks like:
##
## S-G-T-D.gwf
##
## where "S" is the interferometer (Site), "G" is the
## GPS time stamp, "T" is the dt of the file, and "D"
## is what used to be the extension.
##
## "N" can now only be conned at access time by using
## the getFrameNumber on an open frame file pointer.
##
## Parameters:
##
## Usage:
##
## Comments:
## Fallback support for 'Thursday' frames added per
## Peter during meeting on 11-05-2001.

proc frame::analyzeOfficialFilename { jobid filename } {
      
     if { [ catch {
        set atoms [ list ]
        ;## this regexp will return 6 elements when it
        ;## matches:
        ;## given:
        ;##          G-R-6666666666-1.gwf
        ;## returns:
        ;##          G-R-6666666666-1.gwf G R 6666666666 1 .gwf
        ;##
        set rx {^([^-]+)-([^-]+)-(\d{1,10})-(\d+)(\.gwf)$}
        set tail [ file tail [ string trim $filename ] ]
        set tail [ string trim $tail ]
        set atoms [ regexp -inline -- $rx $tail ] 
        
        ;## if the pattern matched, but the filename was
        ;## somehow malformed... should not be possible
        if { [ llength $atoms ] } {
           if { [ regsub -all -- {-} $tail {} foo ] != 3 } {
              set msg "malformed 'official' frame filename: '$tail' "
              append msg "has extra '-' character(s), so does not "
              append msg "conform to gps-frame_type-gps-dt.gwf format."
              return -code error $msg
           }
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $atoms
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::getNumber
##
## Description:
## Given a filename, open it and use getFrameNumber to
## find out how many frames it has.  Clean up!
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::getNumber { jobid filename } {
     
     if { [ catch {
        set seqpt "openFrameFileThread($filename r):"
        set ffp [ openFrameFileThread $jobid $filename r ]
        set seqpt "getFrameNumber($ffp):"
        set frn [ getFrameNumber $ffp ]
        set seqpt "closeFrameFile($ffp):"
        closeFrameFile $ffp
        set ffp [ list ]
        if { ! $frn } {
           set msg "getFrameNumber reported '0' frames in '$filename'"
           return -code error $msg
        }
     } err ] } {
        if { [ info exists ffp ] } {
           if { [ string length $ffp ] } {
              catch { closeFrameFile $ffp }
           }
        }   
        return -code error "[ myName ]:$seqpt $err"
     }
     return $frn
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::rationalizedFilenameData 
##
## Description:
## Get important values from the frame name for
## frame::insertElements and frame::collectElements
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::rationalizedFilenameData { jobid filename } {
     
     if { [ catch {

        array set temp [ frame::analyzeFilename $jobid $filename ]

        set gps $temp(gps)
        set dt  $temp(tdt)
        set N   $temp(frn)
           
        if { !  [ string length $N  ] } { set N  1 }
        if { !  [ string length $dt ] } { set dt 1 }
        set tdt [ expr { $N * $dt } ]
         
        ;## if we have an unparsible frame name, go for broke!!
        if { [ string equal $temp(type) unknown ] } {
           set gps 0
           set dt 1
           set N 1
           set tdt 1
        }
        
        set type $temp(typ)
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $type $gps $N $dt $tdt ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::collectElements
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::collectElements { jobid frames times query } {
    
     if { [ catch {
        set ptrs [ list ]
        set errs [ list ]

        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job

        foreach [ list times frames ] \
           [ frame::fixTimesAndFrames $times $frames ] { break }
        
        array set block \
           [ frame::matchTimesAndFrames $jobid $times $frames ]
        
        set contp [ ilwd::newcontp ]
        frame::managePointers $jobid add $contp
        setElementAttribute $contp comment "$jobid $frames $query"
        
        foreach frame $frames {
           if { ! [ string length $frame ] } { continue }
           set ptrs [ list ]
           
           set times $block($frame)
           
           foreach [ list type gps N fdt tdt ] \
              [ frame::rationalizedFilenameData $jobid $frame ] { break }
           
           if { ! $gps } { set gps [ lindex $times 0 ] }
           
           set framep [ frame::file2ptr $jobid $frame ]
           
           foreach time $times {
              
              set id [ file tail $frame ]
              
              foreach item $query {
                 foreach [ list method chans ] $item { break }
                 if { ! [ llength $chans ] } { set chans 0 }
        
                 if { [ regexp {(\d+)-(\d+)} $time -> start end ] } {
                    set deltat [ expr { $end - $start } ]
                 } else {
                    set start [ lindex $time 0 ]
                    set deltat 1
                 }
        
                 if { $start > $gps && $start < ($gps + $fdt) } {
                    set offset [ expr { $start - $gps } ] 
                 } else {
                    set offset 0
                 }

                 set exp !${offset}!${deltat}!
                 
                 if { [ catch {
                    foreach chan $chans {
                       ;## is it a resample request?
                       foreach [ list chan resample ] \
                          [ frame::resampleParser $chan ] { break }
                       
                       if { ! [ regexp {!} $chan ] } {
                          set chan $chan$exp
                       }
                       
                       set ptr \
                          [ frame::method2ptr $jobid $item $chan $id 1 $resample ]
                       ilwd::addElement $ptr $contp
                       frame::destructElementWrap $ptr
                    }
                 } err ] } {
                    lappend errs "$id: $err"
                 }
              }
           } ;## end of foreach on time
           update
        }
        if { [ llength $errs ] } {
           return -code error $errs
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $jobid $contp ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::fixTimesAndFrames
##
## Description:
## Fix various formats of times and frames while things are
## still in flux.  I hope this gets cleaner soon.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::fixTimesAndFrames { times frames } {
     
     if { [ catch {
        set jobid [ uplevel set jobid ]
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        set intimes $times
        set inframes $frames
        
        if { ! [ string length [ lindex $frames 0 ] ] } {
           set frames [ set ::filecache$job ]
           if { [ llength $frames ] == 1 } {
              set frames [ lindex $frames 0 ]
           }
           ;## cache::concatGetFilenames could return a list
           ;## of three elements, times frames and gapflag...
           ;## maybe can't happen here.
           if { [ regexp {^\d{9,10}-\d{9,10}$} [ lindex $frames 0 ] ] } {
              set times  [ lindex $frames 0 ]
              set frames [ lindex $frames 1 ]
           }
        }
        
     if { ! [ string length $times ] } { set times 0 }

     if { [ llength $times ] == 1 } {
        set times [ lindex $times 0 ]
     }
     if { [ llength $frames ] == 1 } {
        set frames [ lindex $frames 0 ]
     }
     
     if { $::DEBUG_QUERY_EXPANSION } {
        set msg "times_in: '$intimes' frames_in: '$inframes' "
        append msg "times_out: '$times' frames_out: '$frames'"
        addLogEntry $msg purple
     }
     
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $times $frames ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::matchTimesAndFrames
##
## Description:
## Associate a list of times with each frame.  Try to
## collapse times to ranges per frame as possible.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::matchTimesAndFrames { jobid times frames } {
     
     if { [ catch {
        set ranges [ list ]
        set intimes $times
        set times [ numRange $times ]
        
        set inframes $frames
        
        foreach frame $frames {

           array set temp [ frame::analyzeFilename $jobid $frame ]
           set N $temp(frn)
           if { ! [ string length $N  ] } { set N  1 }
           set dt $temp(tdt)
           if { ! [ string length $dt ] } { set dt 1 }
           set start $temp(gps)
           set dt  [ expr { $N * $dt } ]
           set end [ expr $start + $dt ]
           
           set timedata($frame) [ list ]
           
           if { [ string equal 0 $times ] } {
              set times $start
           }
          
           ;## we CAN prune the time list because a single
           ;## grouping of frames will be uniquely matched.
           ;## other matching frames will be grouped seperately.
           set i 0
           set tmp [ list ]
           foreach time $times {
               if { $time >= $start && $time < $end } {
                  lappend timedata($frame) $time
               } elseif { $time < $start } {
                  lappend tmp $time
               } elseif { $time >= $end } {
                  set tmp [ concat $tmp [ lrange $times $i end ] ]
                  break
               }
               incr i
           } 
           set times $tmp
           
           ;## collapse timedata into ranges
           if { [ llength $timedata($frame) ] } {
              set t0 [ lindex $timedata($frame) 0 ]
              set tend $t0
              set ranges [ list ]
              foreach time [ lrange $timedata($frame) 1 end ] {
                 if { $time == $tend + 1 } {
                    set tend $time
                 } else {
                    if { $t0 == $tend } {
                       lappend ranges $t0
                    } else {
                       lappend ranges ${t0}-$tend
                    }
                    set t0 $time
                    set tend $t0
                 }
              }
              if { $t0 == $tend } {
                 lappend ranges $t0
              } else {
                 lappend ranges ${t0}-$tend
              }
              set timedata($frame) $ranges
              
           } ;## end of collapse of time data
           
           set last [ lindex $ranges end ]
           if { [ regexp {(\d+)-(\d+)} $last -> start end ] } {
              set last ${start}-[ expr { $end + 1 } ]
           } elseif { [ llength $timedata($frame) ] == 1 } {
              set last $timedata($frame)
           } else {
              set last ${end}-[ expr { $end + 1 } ]
           }
           set timedata($frame) [ lreplace $ranges end end $last ]
        } ;## end of foreach on frames
        
        if { $::DEBUG_QUERY_EXPANSION } {
           set msg "times_in: '$intimes' frames_in: '$inframes' "
           foreach frame [ array names timedata ] {
              append msg "times in '$frame': '$timedata($frame)'"
           }
           addLogEntry $msg purple
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ array get timedata ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::timeWindow
##
## Description:
## Return a new channelp containing time-sliced OR
## frequency sliced data.
##
## Parameters:
## 
## Usage:
##
## Comments:
## Works with Adc and Proc data so far.

proc frame::timeWindow { jobid ptr { offset 0 } { dt 1 } } {
     
     if { [ catch {
        set seqpt {}
        if { $offset == 0 && $dt == 0 } {
           set err "offset and dt both '0'! "
           append err "a channel with a dt of 0 would result."
           return -code error $err
        }
        if { [ regexp {(Adc|Proc)} $ptr type ] } {
           set seqpt "getFrame${type}DataSlice($ptr $offset $dt):"
           set outptr [ getFrame${type}DataSlice $ptr $offset $dt ]
           frame::managePointers $jobid add $outptr
           frame::managePointers $jobid destroy $ptr
        } else {
           set outptr $ptr
        }
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $outptr
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::inFrame
##
## Description:
## Very slow function for accessing frames in frame
## Parameters:
## filename - name of frame file to open
## offset - offset into frame file of target frame
## delta - 
##
## Usage:
##
## Comments:
## Daq functions do not work on multi frame frames.
##
## Calls readFrame repeatedly to position the wanted frame
## at the top of the framep.  Then iterate through the
## read frames...
## 
## Concatenation will require the use of the old concat
## functions.

proc frame::inFrame { jobid filename { offset 0 } { delta 1 } } {
     if { [ catch {
        array set finfo [ frame::analyzeFilename $jobid $filename ]
        
        set seqpt "openFrameFileThread($filename r):"
        set fp [ openFrameFileThread $jobid $filename r ]
        set seqpt {}
        frame::managePointers $jobid add $fp 
        ;## if the file has more than one frame
        if { $finfo(frn) > 1 } {
           ;## set the max frame offset to n
           set n $finfo(frn)
        } else {
           ;## otherwise set it to 1
           set n 1
        }
        
        set seqpt "getFrameNumber($filename $fp):"
        set N [ getFrameNumber $fp ]
        if { $N != $n } {
           return -code error "$filename has '$N' frames, not '$n'"
        }
        set seqpt {}
        
        ;## calculate time offset to nearest second (round up)
        set delta [ expr { double($delta) / $finfo(tdt) } ]
        set delta [ expr { int(ceil($delta)) } ]
       
        set framen [ expr { $offset + $delta } ]
        if { $framen > $N } {
           return -code error "offset > N for $filename: $framen > $N" 
        }
        
        set i 0
        while { [ incr i ] <= $framen } {
           set seqpt "readFrame($fp):"
           set framep [ readFrame $fp ]
        }
        set seqpt {}
        frame::managePointers $jobid add $framep
        frame::setImage $jobid $filename $framep
        
     } err ] } {
        frame::managePointers $jobid destroy .+
        return -code error "[ myName ]:$seqpt $err"
     }
     return $framep
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::method2ptr
##
## Description:
## Contains all the switching code for accessing arbitrary
## frame objects.  Special cases are handled as generally
## as possible. Returns an ilwd container pointer.
##
## Parameters:
## jobid - the job i.d.
## item - a query atom in expanded form
## index - the current index from item being processed
## fname - tail of the filename of the current frame
## ilwd - if set to 0, return frame pointers NOT ilwd
##
## Usage:
##
## Comments:
## This is the "guts" of frame::collectElements

proc frame::method2ptr { jobid item index frame { ilwd 1 } { resample 0 } } {
     set seqpt {} 
     set ptr [ list ]
     ;## index 0 has inner accessor, index 1 has outer accessor.
     ;## if 0 and 1 are identical there is no inner accessor call.
     set class_rx {(getFrame[A-Z][a-z]+(?:Data|Event)?)(\S*)}
     ;## identifies an ilwd pointer as opposed to a framecpp ptr
     set ilwd_rx {^_[0-9a-f]+_p_Ldas(Element|Container|ArrayBase)$}
     ;## a stat structure
     set stat_rx {(getFrameStatData)([a-zA-Z]+)?}
     set frame_rx {^getFrame(Adc|Proc|Sim|Ser|Stat)Data$}
     
     if { [ catch {
        
        if { [ file exists $frame ] } {
           set framep [ frame::open_r $jobid $frame ]
        } else {
           foreach [ list frame framep ] [ frame::getImage $jobid ] {
              break
           }
        }

        if { [ info exists ::DEBUG_METHOD2PTR ] && \
             [ string equal 1 $::DEBUG_METHOD2PTR ] } {
           set msg "frame: $frame channel: $index method: $item"
           addLogEntry $msg purple
        }
        
        foreach [ list type gps N fdt tdt ] \
           [ frame::rationalizedFilenameData $jobid $frame ] { break }
        
        foreach [ list idx off dt ] \
           [ frame::parseChannelSlice $index ] { break }
        
        ;## class_rx matches all methods.  inner accessor
        ;## points to structure, outer accessor points to
        ;## element of structure IF different from inner.
        set method [ lindex $item 0 ]
        set inner $method
        set outer $method
        regexp $class_rx $method inner outer
        
        ;## are we slicing?
        if { $dt != $fdt && [ regexp {(Adc|Proc)Data$} $inner ] } {
           
           set ptr [ frame::timeSliceChannel \
              $jobid $frame [ list $outer $index ] $off $dt ]

           ;## at this point the offset and dt need to be set to 0
           ;## if the Proc structure did NOT contain time series data.
           ;## and we know that because samplerate will be '-1'.
           if { [ regexp {Proc} $ptr ] } {
              set seqpt "getFrameProcDataType($ptr):"
              set samplerate [ getFrameProcDataType $ptr ]
              set seqpt {}
              if { $samplerate != 1 } { set samplerate -1 }
              if { [ string equal -1 $samplerate ] } {
                 set off 0
                 set dt  0
              }
           }
        } elseif { [ regexp {fullFrame2container} $method ] } {
           set ptr [ frame::framep2ilwdp $jobid $framep ] 
        ;## are we after a detector or history structure?
        } elseif { [ regexp {(History|Detector)} $method ] } {
           set ptr \
              [ frame::detectorAndHistory $frame $framep $method $index 1 ] 
        ;## a StatData thingy, which comes from a detector (?)
        } elseif { [ regexp $stat_rx $method --> outer inner ] } {
           ;## need index of Detector *and* StatData structure ... oy.
           set err "Sorry, access to StatData structures is not yet "
           append err "supported."
           return -code error $err
        ;## are we after an arbitrary frame element?
        } else {
           if { [ info exists idx ] } { set index $idx }
           set index [ lindex [ split $index ! ] 0 ]
           ;## exception flow control
           ;## these are framecpp pointers into the current frame
           if { [ catch {
              set seqpt "${inner}($framep $index):"
              set ptr [ $inner $framep $index ]
           } err ] } {
              if { [ regexp $frame_rx $inner ] } {
                 return -code error "$err $index"
              }
              set msg "1st try: '$seqpt $err'"
              if { [ catch {
                 set seqpt "${inner}($framep):"
                 set ptr [ $inner $framep ]
              } err ] } {
                 append msg " 2nd try: '$seqpt $err'"
                 if { [ catch {
                    set seqpt "${outer}($framep $index):"
                    set ptr [ $outer $framep $index ]
                 } err ] } {
                    append msg " 3rd try: '$seqpt $err'"
                    if { [ catch {
                       set seqpt "${outer}($framep):"
                       set ptr [ $outer $framep ]
                    } err ] } {
                       append msg " 4th try: '$seqpt $err'"
                       return -code error $msg
                    }
                 }
              }
           } ;## end of exception flow control block
           
           ;## framecpp pointer element handler
           if { ! [ regexp $ilwd_rx $ptr ] } {
              ;## okay, we got a framecpp pointer above...
              if { ! [ string equal $inner $outer ] } {   
                 ;## exception flow control
                 set msg {}
                 if { [ catch {
                    set seqpt "${inner}($ptr):"
                    set ptr [ $inner $ptr ]
                  } err ] } { 
                    set msg "1st try: '$seqpt $err'"
                    if { [ catch {
                       set seqpt "${inner}($ptr $index):"
                       set ptr [ $inner $ptr $index ]
                    } err ] } {
                       append msg " 2nd try: '$seqpt $err'"
                       return -code error $msg
                    }
                 } ;## end of exception flow control block
              }
           } ;## end of framecpp pointer element handler
        }
        
        ;## should never happen
        if { [ string equal NULL $ptr ] } {
           return -code error "returned 'NULL' pointer!"
        }
         
        ;## we are resampling an Adc_p, we get a Proc_p back
        if { $resample } {
           ;## tricky way of making resample states transparent
           ;## and persistent as required.  reaches back up to
           ;## frame::reduce and maintains the state up there.
           if { [ uplevel 2 info exists resample_state($index) ] } {
              set state [ uplevel 2 set resample_state($index) ]
           } else {
              set state {}
           }
           foreach [ list delay ptr state ] \
           [ frame::resampleOneAdc $jobid $ptr $resample $state ] {
              break
           }
           uplevel 2 set resample_state($index) $state
           ;## offset returned has a negative sign
           set off [ expr { $off - $delay } ]
        }
        
        ;## and if we have a non-ilwd pointer type, render
        ;## it into ilwd
        if { $ilwd } {
           if { ! [ regexp $ilwd_rx $ptr ] } {
              ;## data type of 1 is time-series
              if { [ regexp {Proc} $ptr ] } {
                 set datatype [ getFrameProcDataType $ptr ]
              } else {
                 set datatype 1
              }
              if { $datatype != 1 } {
                 set off $gps
                 set dt  $fdt
              }
              set ptr [ frame::chanp2ilwdp $jobid $ptr $off $dt ]
           }
        }   
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $ptr
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::resampleOneAdc
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## Time slicing is always done BEFORE resampling.
##

proc frame::resampleOneAdc { jobid adcp resample { state "" } } {
     
     if { [ catch {
        foreach [ list new_adcp delay state ] \
           [ frame::resampleChanPointer $jobid $adcp $resample $state ] {
           break
        }
        
        ;## register the derived Adc_p for destruction later
        frame::managePointers $jobid add $new_adcp

        set procp $new_adcp
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $delay $procp $state ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::detectorAndHistory
##
## Description:
## Special handler for linked-lists of detector and
## history structures.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::detectorAndHistory { frame framep method index wrap } {
     
     if { [ catch {
        
        set seqpt {}
        set wrapper [ list ]
        set ilwdp   [ list ]
        set ilwdps  [ list ]
        set index [ lindex [ split $index ! ] 0 ]
        
        ;## test for valid method for this proc
        set rx_pat {(History|Detector(Proc|Sim)?)}

        ;## test for request for whole structure
        set Rx_pat {(History|Detector(Proc|Sim)?)$}

        ;## matches all valid requests for History or
        ;## Detector structures and components
        set RX_pat {(History|Detector(Proc|Sim)?)\S*}
        
        if { ! [ regexp $rx_pat $method type ] } {
           set msg "called for incompatible method: '$method'"
           return -code error $msg
        }

        if { ! [ regexp -nocase {^(all|^\d+)$} $index ] } {
           set msg "called with bad index: '$index'"
        }
        
        ;## an integer or 'all'
        if { [ string equal Detector $type ] } {
           set type DetectorProc
        }
        
        if { [ regexp {\d+} $index ] } {
           set i $index
           set n [ getFrame${type}Num $framep ]
           set N [ expr { $i + 1 } ]
        } else {
           set i 0
           set N [ getFrame${type}Num $framep ]
           set n $N
        }
        
        ;## is requested index within range?
        if { $i > ($n - 1) } {
           set msg "index out of range: '$index' "
           append msg "there is/are only '$n' $type structures "
           append msg "in the current frame: '$frame' "
           append msg "(indexing starts at '0')."
           return -code error $msg
        }
        
        ;## if we're getting full History or Detector
        ;## strucutures, we need to pack them into a
        ;## wrapper with a properly constructed name.
        if { $wrap && [ regexp $Rx_pat $method Type ] } {
           set seqpt "createOuter${type}Container():"
           set wrapper [ createOuter${type}Container ]
           set seqpt {}
        } elseif { [ regexp $Rx_pat $method Type ] } {
        } else {
           set Type $type
        }
        
        regexp $RX_pat $method TYPE
        
        while { $i < $N } {
           ;## get the Historyp or Detectorp framecpp object
           ;## this is a  pointer into the frame and does not
           ;## need to be destructed.
           if { [ catch {
              set seqpt "getFrame${Type}($framep $i):"
              set ${type}p [ getFrame$Type $framep $i ]
           } err ] } {
              if { ! [ regexp {(Proc|Sim)} $Type ] } {
                 if { [ catch {
                    set seqpt "getFrame${Type}Proc($framep $i):"
                    set ${type}p [ getFrame${Type}Proc $framep $i ]
                 } junk ] } {
                    return -code error $err
                 }
              }
           }
           
           ;## compare the specific method to the
           ;## specific type to see if we are just going
           ;## to return a weenie little ilwd constant.
           if { [ string equal $Type $TYPE ] } {
              set seqpt "${type}2container([ set ${type}p ]):"
              set ilwdp [ ${type}2container [ set ${type}p ] ]
              if { $wrap } {
                 set seqpt "addContainerElement($ilwdp):"
                 addContainerElement $wrapper $ilwdp
                 frame::destructElementWrap $ilwdp
              } else {
                 lappend ilwdps $ilwdp
              }
           } else {
              ;## requests for Num are made against the framep
              ;## and are unadorned integers!!  YECCCCCHHH!!!
              if { [ regexp {Num$} $TYPE ] } {
                 set type frame
                 
                 if { [ catch {
                    set seqpt "getFrame${TYPE}([ set ${type}p ]):"
                    set num [ getFrame$TYPE [ set ${type}p ] ]
                 } err ] } {
                    if { ! [ regexp {(Proc|Sim)} $TYPE ] } {
                       regsub Detector $TYPE DetectorProc TYPE
                       if { [ catch {
                          set seqpt \
                             "getFrame${TYPE}([ set ${type}p ]):"
                          set ${type}p \
                             [ getFrame${TYPE} [ set ${type}p ] ]
                       } junk ] } {
                          return -code error $err
                       }
                    }
                 }
                 
                 set ilwd  [ ilwd::str2ilwd $num ]
                 set ilwdp [ putElement $ilwd ]
                 setElementAttribute $ilwdp name "$TYPE $frame"
                 lappend ilwdps $ilwdp
              } else {
                 set seqpt "getFrame${TYPE}([ set ${type}p ]):"
                 lappend ilwdps [ getFrame$TYPE [ set ${type}p ] ]
              }
           }
           set seqpt {}
           incr i
        }
        
        ;## determine return type
        if { [ string length $wrapper ] } {
           set retval $wrapper
        } elseif { [ string length $ilwdps ] } {
           set retval $ilwdps
        } else {
           return -code error "unknown error"
        }
        
     } err ] } {
        catch { frame::destructElementWrap $wrapper }
        catch { frame::destructElementWrap $ilwdp }
        foreach ilwdp $ilwdps {
           catch { frame::destructElementWrap $ilwdp }
        }
        return -code error "[ myName ]:$seqpt $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::timeData
##
## Description:
## Return the timestamp and delta t of a frame.
## These values do not exist in channel structures, so
## they must be added when converting the channels to
## ilwd objects.
##
## Parameters:
## jobid - can be a Frame_p or job i.d.
## offset - optional offset for a sliced data structure
## dt - optional dt for a sliced data structure
##
## Usage:
##
## Comments:
## When dt > 0 we switch into slicing mode.
## Alas, right now we are modifying the frame start time,
## I think this should instead be timeOffset :TODO:

proc frame::timeData { jobid { offset 0 } { dt 0 } } {
     if { [ catch {
        set seqpt {}
        if { [ regexp {^_[0-9a-f]+_p_FrameH$} $jobid ] } {
           set framep $jobid
           set jobid [ uplevel set jobid ]
        } else {
           foreach [ list - framep ] [ frame::getImage $jobid ] { break }
        }   
        
        set seqpt "getFrameFrameHGTime($framep):"
        set arrayp [ getFrameFrameHGTime $framep ]
        set seqpt "getFrameFrameHDt($framep):"
        set floatp [ getFrameFrameHDt $framep ]
        set seqpt "getElement($arrayp):"
        set array [ getElement $arrayp ]
        set seqpt "getElement($floatp):"
        set float [ getElement $floatp ]
        set seqpt {}
        
        frame::destructPointers $jobid $arrayp $floatp
       
        regexp {>(\d+)\s+(\d+)<} $array -> GTimeS GTimeN
        regexp {>(.+)<} $float -> Dt
        
        foreach [ list Ss Sn Es En ] \
           [ gpsOffset $GTimeS $GTimeN $offset $dt ] { break }
    
     } err ] } {
        set msg "[ myName ]:$seqpt $err (offset: '$offset' dt: '$dt')"
        return -code error $msg
     }
     return [ list $Ss $Sn $Es $En $Dt ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::filenameCalculator
##
## Description:
## Used to build the name of an output frame file for the
## frame::outputFrames function.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::filenameCalculator { jobid filename pointer } {
     
     if { [ catch {
        set seqpt {}
        ;## CAPTURE THIS NAMING CONVENTION SCHEME!! :TODO:
        regexp {\{(.+)\}} $filename -> filename
        set tail  [ file tail $filename ]
         
        if { [ catch { 
           array set temp [ frame::analyzeFilename $jobid $tail ]
           set ifo $temp(ifo)
           set typ $temp(typ)
           if { ! [ string length $typ ] } { set typ P }
        } err ] } {
           set ifo  Z
           set typ  P
        }
        
        if { [ catch {
           set gps [ getFrameFrameHGTime $pointer ]
           frame::managePointers $jobid add $gps
        } err ] } {
           if { [ catch {
              set seqpt "refContainerElement($pointer 3):"
              set gps [ refContainerElement $pointer 3 ]
              set seqpt {}
           } err ] } {
              set gps 000000000
              set gpsS $gps
              set gpsN $gps
           }
        }
        
        if { ! [ string equal 000000000 $gps ] } {
           set gps [ getElement $gps ]
           regexp {>(\d+)\s+(\d+)<} $gps -> gpsS gpsN
        }
        
        set tso [ expr { 0.000000001 * $gpsN } ]
       
        if { [ catch {
           set tdt [ getFrameFrameHDt $pointer ]
           frame::managePointers $jobid add $tdt
        } err ] } {
           if { [ catch {
              set seqpt "refContainerElement($pointer 5):"
              set tdt [ refContainerElement $pointer 5 ]
              set seqpt {}
           } err ] } {
              set tdt 1
           }
        }
        
        if { ! [ string equal 1 $tdt ] } {
           set tdt [ getElement $tdt ]
           regexp {name='dt'>(.+)<} $tdt -> tdt
           ;## if this is not a frame the tdt will be garbage...
           if { [ string length $tdt ] > 32 } {
              set tdt [ string range $tdt 0 1023 ]...
              return -code error "error dereferencing tdt: '$tdt'"
           }
        }
        
        set et  [ expr { $tso + $tdt } ]
        ;## account for roundoff by subtracting a tiny time
        ;## corresponding to a MHz sampling rate
        set fdt [ expr { int(ceil($et - 0.000001)) } ]
           
        set dir [ file dirname $filename ]
        regsub -all -- - $::LDAS_SYSTEM _ ls
        regsub -all -- - $jobid _ jobid
        regsub -all -- - $typ _ typ

        if { [ info exists ::FRAME_FILENAME_REGSUBS ] } {
           foreach [ list pattern replacement ] $::FRAME_FILENAME_REGSUBS {
              regsub -all -- - $replacement _ replacement
              foreach variable { typ jobid ls } {
                 if { [ catch {
                    regsub -all -- $pattern [ set $variable ] \
                                   $replacement   $variable 
                 } err ] } {
                    addLogEntry $err blue
                    break
                 }
              }
           }
        }

        set filename \
           [ file join $dir "${ifo}-${typ}_${ls}_${jobid}-${gpsS}-${fdt}.gwf" ]
     
        regsub -all -- __ $filename _ filename
     
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $filename
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::file2ptr 
##
## Description:
## Read a frame from a file.  Returns a pointer to a
## frame object.  Will short-circuit efficiently if a
## Frame_p is passed ( <100us ).
##
## Parameters:
## filename - the name of a frame file
## mode - the mode for the open file, currently d or u.
##
## returns - pointer to frame data.
##
## Usage:
##       if { [ catch {
##          set framep [ frame::file2ptr $jobid $filename d ]
##          } err ] } {
##          ;# do something
##          }
##
## Comments:
## Calls to frame::file2ptr MUST be caught!
## An errlvl of 3 causes the assistant manager to abort.
##
## For accesses to frame-in-frame data, this function needs
## to be called once for each frame accessed.  Slow, but
## steady.

proc frame::file2ptr { { jobid "" } { file "" } { time "" } { mode r } } {
     set seqpt {}
     set framep [ list ]
     
     switch -regexp -- $file {
        {^$} { ;## a null string
             return -code error "[ myName ]: missing argument 'file'"
             }
        {^_[0-9a-f]+_p_FrameH$} { ;## a frame pointer
             set framep $file
             }
        default { ;## a filename or junk
                  if { ! [ file exists $file ] } {
                     set msg "file not found '$file'"
                     return -code error $msg
                  }
                }  
     } ;## end of switch
     
     if { [ catch {
        if { ! [ string length $framep ] } {
           array set fileinfo [ frame::analyzeFilename $jobid $file ]
           if { ! [ string length $fileinfo(ifo) ] } {
              set mode r 
           }
           
           ;## here is where we go looking for frame-in-frame
           if { $fileinfo(frn) > 1 } {
              if { [ string length $time ] } {
                 
                 ;## if a multi frame frame is specified and no time
                 ;## range is specified, return the data from the
                 ;## first frame.
                 if { [ string equal 0 $time ] } {
                    set time [ expr { $fileinfo(gps) + 1.0 } ]
                 }
                 
                 if { [ regexp {(\d+)-(\d+)} $time -> beg end ] } {
                    set gps $fileinfo(gps)
                    set offset [ expr { ($beg - $gps) / $fileinfo(tdt) } ]
                    set dt     [ expr { $end - $beg } ]
                 } else {
                    ;## dt here is just the tdt from the frame name
                    foreach [ list offset dt ] \
                       [ frame::findFrameInFrameOffset \
                          $jobid $file $time ] { break }
                 }
                 if { ! [ string length $offset ] } {
                    return -code error "mismatch: file: '$file' time: '$time'"
                 }
                 
                 if { $::DEBUG_FILE2PTR } {
                    addLogEntry "time used for calcs: '$time'" blue
                    set fifopts    "options passed to frame::inFrame: "
                    append fifopts "file: '$file'  offset: '$offset'  "
                    append fifopts "dt: '$dt'" 
                    addLogEntry $fifopts blue
                    set ary "fileinfo array: [ array get fileinfo ]"
                    addLogEntry $ary blue
                 }
                 
                 set framep [ frame::inFrame $jobid $file $offset $dt ]
              } else {
                 set framep [ frame::open_$mode $jobid $file ]
              }
           } else {
              set framep [ frame::open_$mode $jobid $file ]
           }   
        }   
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     
     if { [ regexp -- NULL $framep ] } {
        return -code error "[ myName ]: readFrame returned NULL!"
     }
     
     return $framep
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::open_r
##
## Description:
## Open frame and read whole thing into framep.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::open_r { jobid file } {
     if { [ catch {
        set seqpt {}
        set seqpt "openFrameFileThread($file r):"
        set fp [ openFrameFileThread $jobid $file r ]
        frame::managePointers $jobid add $fp
        frame::destructImage $jobid -nocomplain
        #set seqpt "readFrame($fp):"
        #set framep [ readFrame $fp ] 
        
        ;## use threaded function for reading frame
        set tid [ readFrame_t $fp ]
        after 20
        set framep [ readFrame_r $tid ]
        frame::managePointers $jobid add $framep 
        set seqpt {}
        set seqpt "isFrameValid($framep):"
        if { ! [ isFrameValid $framep ] } {
           return -code error "invalid frame object!"
        }
        set seqpt {}
        frame::setImage $jobid $file $framep
        frame::fullRehash $jobid $framep
     } err ] } {
        if { [ info exists framep ] } {
           frame::managePointers $jobid destroy $framep
        }
        if { [ info exists fp ] } {
           frame::managePointers $jobid destroy $fp
        }
        frame::destructImage $jobid -nocomplain 
        return -code error "[ myName ]:$seqpt $err"
     }
     return $framep
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::open_w
##
## Description:
## Open frame for writing (?)
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::open_w { jobid file } {
     if { [ catch {
        return -code error "unsupported file mode 'w'"
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::open_d
##
## Description:
## Open frame and initalise memory map, but do not read
## frame structures now.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::open_d { jobid file } {
     if { [ catch {
        set seqpt {}
        set seqpt "openFrameFileThread($file d):"
        set fp [ openFrameFileThread $jobid $file d ]
        frame::managePointers $jobid add $fp
        frame::destructImage $jobid -nocomplain
        set seqpt "readDaqFrame($fp):"
        set framep [ readDaqFrame $fp ]
        frame::managePointers $jobid add $framep
        set seqpt {}
        #set seqpt "daqActivateAllADCs($framep):"
        #daqActivateAllADCs $framep
        set seqpt "daqResetADCs($framep):"
        daqResetADCs $framep
        set seqpt {}
        set seqpt "isFrameValid($framep):"
        if { ! [ isFrameValid $framep ] } {
           ;## outer catch will format this correctly
           return -code error "invalid frame object!"
        }   
        set seqpt {}
        frame::fullRehash $jobid $framep
     } err ] } {
        if { [ info exists framep ] } {
           frame::managePointers $jobid destroy $framep
        }
        if { [ info exists fp ] } {
           frame::managePointers $jobid destroy $fp
        }
        frame::destructImage $jobid -nocomplain 
        return -code error "[ myName ]:$seqpt $err"
     }
     frame::setImage $jobid $file $framep
     return $framep
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::open_u
##
## Description:
## If possible, update a memory map created with
## frame::open_d, reusing the already allocated memory map.
##
## If that fails, fall back to frame::open_d.
##
## If that also fails, fall back to frame::open_r.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::open_u { jobid file } {
     if { [ catch {
        set err [ list ]
        set framep [ list ]
        set seqpt {}
        
        if { [ catch {
           foreach [ list - framep ] [ frame::getImage $jobid ] { break }
           set seqpt "openFrameFileThread($file u):"
           set fp [ openFrameFileThread $jobid $file u ]
           frame::managePointers $jobid add $fp
           set seqpt "updateDaqFrame($framep $fp):"
           updateDaqFrame $framep $fp
        } err1 ] } {
           set seqpt {}
           lappend err $err1
           frame::destructImage $jobid -nocomplain
           if { [ catch {
              set framep [ frame::open_d $jobid $file ]
           } err2 ] } {
              lappend err $err2
              if { [ catch {
                 set framep [ frame::open_r $jobid $file ]
              } err3 ] } {
                 lappend err $err3
                 return -code error "Could not read frame: '$file'. $err"
              } ;## end of err1 handler
           } ;## end of err2 handler
        } ;## end of err3 handler  
        ;## try to yield to the event loop
        after 1 
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $framep
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::open_cat
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::open_cat { jobid file } {
     if { [ catch {
        set seqpt  {}
        set err    [ list ]
        set framep [ list ]
        set fp     [ list ]
        
        if { [ catch {
           foreach [ - framep ] [ frame::getImage $jobid ] { break }
           set seqpt "openFrameFileThread($file u):"
           set fp [ openFrameFileThread $jobid $file u ]
           frame::managePointers $jobid add $fp
           set seqpt "catDaqFrame($framep $fp):"
		 catDaqFrame $framep $fp
           set seqpt {}
        } err1 ] } {
           lappend err $err1
           frame::managePointers $jobid destroy $fp
           frame::destructImage $jobid -nocomplain
           if { [ catch {
              set framep [ frame::open_d $jobid $file ]
           } err2 ] } {
              lappend err $err2
              return -code error "frames have changed! $err"
           }   
        }
        ;## Yield to the event loop processing
        after 1
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $framep
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::fullRehash
##
## Description:
## Generate hash tables of all channel structures active
## in the current frame.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::fullRehash { jobid framep } {
     
     if { [ catch {
        set errs [ list ]
        if { [ info exists ::frame::hashcommands ] } {
           set cmds $::frame::hashcommands
        } else {
           ;## get all the rehash commands
           set cmds [ list ]
           foreach cmd [ info commands ] {
              if { ! [ string length $cmd ] } { break }
              if { [ regexp {rehash} $cmd ] } {
                 lappend cmds $cmd
              }
           }
           set ::frame::hashcommands $cmds
        }   
        
        ;## run all the rehash commands
        foreach cmd $cmds {
           set seqpt "${cmd}($framep):"
           if { [ catch { 
              $cmd $framep
           } err ] } {
              lappend errs $cmd
           }
        }

        if { $::DEBUG > 1 } {
           if { [ llength $errs ] } {
              addLogEntry "failed rehashing actions: $errs" purple
           }
        }
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::getImage
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::getImage { jobid } {
     regexp {\d+} $jobid jobin
     set image [ list ]
     set filename [ list ]
     if { [ catch {
        if { ! [ info exists jobin ] } {
           return -code error "invalid jobid: '$jobid'"
        }
        foreach [ list jobid filename image ] $::frame::images {
           regexp {\d+} $jobid jobout 
           if { ! [ info exists jobout ] } {
              set msg "bad jobid in ::frame::images: '$jobid'"
              return -code error $msg
           }
           if { [ string equal $jobin $jobout ] } {
              break
           }
           set image [ list ]
           set filename [ list ]
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $filename $image ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::setImage
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::setImage { jobid1 { filename1 "" } { framep "" } } {
     regexp {\d+} $jobid1 jobin
     if { [ catch {
        set tmp [ list ]
        foreach [ list jobid2 filename2 image ] $::frame::images {
           regexp {\d+} $jobid2 jobout 
           if { [ string equal $jobin $jobout ] } {
              ;## we remove the image info for the job and
              ;## only update it if we got a framep arg
           } else {
           lappend tmp $jobid2 $filename2 $image
           }
        }

        ;## here's where we update if we have a framep
        if { [ string length $framep ] } {
           lappend tmp $jobid1 $filename1 $framep
        }   
        set ::frame::images $tmp
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::destructImage
##
## Description:
## Thorough, verbose destructor for the frame image.
## Parameters:
##
## Usage:
##
## Comments:
## If any second option is given, no exception will
## be thrown.  I suggest -nocomplain.

proc frame::destructImage { { jobid "" } { option "" } } {
     set seqpt {}
     set img   [ list ]
     
     if { [ catch {
        foreach [ list frame img ] [ frame::getImage $jobid ] { break }
        if { ! [ string length $img ] } {
           return -code error "frame image not found for job: $jobid"
        }
        frame::managePointers $jobid delete $img
        set seqpt {}
        frame::setImage $jobid
     } err ] } {
        frame::setImage $jobid
        if { ! [ string length $option ] } {
           if { [ regexp {unexpected_exception} $err ] } {
              set err "invalid_frame ($err)"
           }
           return -code error "[ myName ]:$seqpt $err"
        }
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::managePointers
##
## Description:
## Add pointers to a job's pointer list or destruct and
## remove pointers from a jos pointer list.
## When the number of pointers in the list drops to zero
## The job's entry in the pointer list goes away.
##
## Parameters:
## jobid - the job i.d., with or without prepended ::RUNCODE
## action - add, delete, or destroy (destroy supresses exceptions)
## lifetime - "long", "short", or "all"
## args - list of pointers or "all"
## Usage:
##
## Comments:
##

proc frame::managePointers { jobid action args } {
     
     set err [ list ]
     set destruct [ list ]
     set ptrs [ list ]
     set ptrlist [ list ]
     set seqpt {}
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     
     set ptrs [ lsort -unique [ join [ join $args ] ] ]
    
     if { ! [ string length $args ] } {
        return
     }
    
     if { [ catch {
        
        switch -exact -- $action {
            add {
                  frame::graftPointers $jobid $ptrs
                }
         delete {
                  set ptrlist [ frame::prunePointers $jobid $ptrs ]
                  set seqpt "frame::destructPointers($jobid $ptrlist):"
                  frame::destructPointers $jobid $ptrlist
                }
        destroy {
                  set ptrlist \
                     [ frame::prunePointers $jobid $ptrs -nocomplain ]
                  set seqpt "frame::destructPointers($jobid $ptrlist):"   
                  frame::destructPointers $jobid $ptrlist -nocomplain
                }
        default {
                return -code error "unknown action: '$action'"
                }
        } ;## end of switch on action
        
     } err ] } {
        return -code error \
        "[ myName ]:($jobid $action $args):$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::graftPointers
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::graftPointers { jobid ptrs } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job

        if { ! [ info exists ::frame::pointers($job) ] } {
           set ::frame::pointers($job) $ptrs
        } else {
           set oldlist $::frame::pointers($job)
           set ptrs [ lsort -unique [ concat $oldlist $ptrs ] ]
           set ::frame::pointers($job) $ptrs
        }
        
        if { [ llength $::frame::pointers($job) ] == 0 } {
           ::unset ::frame::pointers($job)
        }
     
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::prunePointers
##
## Description:
## Specially optimized stack::prune for use by
## frame::managePointers
##
## Parameters:
## jobid - the jobid in full ::RUNCODE prepended form
## pattern - a list of pointer names, a list index pair, or .+
##
## Usage:
##    frame::prunePointers $jobid [ list _XXXX_Element_p ... ]
##    frame::prunePointers $jobid .+
##    frame::prunePointers $jobid [ list end-3 end ]
##
## Comments:
## 

proc frame::prunePointers { jobid ptrs { flag "" } } {
     
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     
     if { [ catch {
        set pattern $ptrs
        set prunes [ list ]
        
        if { ! [ info exists ::frame::pointers($job) ] } {
           return -code error "no pointers registered for job!"
        }
        
        set ptrlist $::frame::pointers($job)
        ;## if we are after destroying all pointers
        if { [ string equal .+ $pattern ] } {
           set prunes $ptrlist
           set ptrlist [ list ]
        ;## or maybe a range of indices    
        } elseif { [ llength $pattern ] == 2 && \
                 ! [ regexp {_p} $pattern ] } {
           set min [ lindex $pattern 0 ]
           set max [ lindex $pattern 1 ]
           if { [ llength $ptrlist ] > $min } {
              set prunes  [ lrange   $ptrlist $min $max ]
              set ptrlist [ lreplace $ptrlist $min $max ]
           }   
        ;## or maybe we want to search and destroy
        } else {
           foreach ptr $pattern {
              set k [ lsearch -exact $ptrlist $ptr ]
              if { $k == -1 } { continue }
              set ptrlist [ lreplace $ptrlist $k $k ]
              lappend prunes $ptr
           }
        }
        if { [ llength $ptrlist ] } {
           set ::frame::pointers($job) $ptrlist
        } else {
           ::unset ::frame::pointers($job)
        }
     } err ] } {
        if { ! [ string length $flag ] || \
             ! [ regexp {no pointers} $err ] } {
           return -code error "[ myName ]: $err"
         }
     }
     return $prunes
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::destructPointers
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## The destructAdcData destructor is for Adc pointers
## created by the getFrameAdcDataSlice method.

proc frame::destructPointers { jobid args } {
     set seqpt {}
     set errs [ list ]
     set ptrs [ list ]
     set nocomplain 0
     
     if { [ llength $args ] == 1 } { 
        set args [ lindex $args 0 ] 
     }
     if { ! [ string length $args ] } { return }
     
     if { [ regexp -nocase -- {-nocomplain} [ lindex $args end ] ] } {
        set args [ lrange $args 0 end-1 ]
        if { [ llength $args ] == 1 } { 
           set args [ lindex $args 0 ] 
        }
        set nocomplain 1
     }
     
     ;## destroy FrameFile pointers last
     set ptrs [ lsort -dictionary -unique [ join $args ] ]
     set end  [ llength $ptrs ]
     while { $end } {
        set i [ lsearch $ptrs *FrameFile* ]
        if { $i == -1 } { break }
        set ff [ lindex $ptrs $i ]
        set ptrs [ concat [ lreplace $ptrs $i $i ] $ff ]
        decr end
     }
     
     if { ! [ string length $ptrs ] } { return }
    
     foreach ptr $ptrs {
        if { [ catch {   
           switch -regexp -- $ptr {
              {(Element|Container|ArrayBase)} {
                    set seqpt "destructElement($ptr):"
                    frame::destructElementWrap $ptr
                    }
              {FrameFile} {
                    set seqpt "closeFrameFile($ptr):"
                    closeFrameFile $ptr
                    }
              {FrameH$} {
                    set seqpt "destructFrame($ptr):"
                    if { ! [ info exists ::${ptr}_lock ] } {
                       destructFrame $ptr
                    }   
                    }
              {Adc} {
                    set seqpt "destructAdcData($ptr):"
                    destructAdcData $ptr
                    }
             {Proc} {
                    set seqpt "destructProcData($ptr):"
                    destructProcData $ptr
                    }
            {(Detector|History)} {        
                    set msg "'$ptr' is not a REGISTERED pointer type"
                    addLogEntry $msg blue
                    }
              {tid} {
                    #set msg "cleaned up objects for thread i.d.: '$ptr'"
                    #addLogEntry $msg blue
                    }
            default {
                     if { [ regexp {^_[0-9a-f]+_p_} $ptr ] } {
                        return -code error "unknown ptr type: '$ptr'" 
                     } else {
                        addLogEntry "not a pointer: '$ptr'" blue
                     }
                    }
           } ;## end of switch
           set seqpt {}
        } err ] } {
           lappend errs "$seqpt $err"
        }
     } ;## end of foreach
        
     if { [ llength $errs ] } {
        if { $nocomplain } {
           addLogEntry $errs 1
        } else {
           return -code error "[ myName ]: $errs"
        }  
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::chanlist
##
## Description:
## This is the replacement function for the former version
## that returned a list of alternating channel names and
## sample-rates.
##
## Parameters:
##
## Usage:
##    When the meta flag is '0' the list returned consists
##    of alternating channel names and type strings, with
##    the types represented as ADC, PROC, SIM, or SERIAL.
##
## Comments:
##

proc frame::chanlist { jobid filename chantypes meta } {
     
     set seqpt [ list ]
     if { [ catch {        
        if { [ llength $chantypes ] == 1 } {
           set chantypes [ lindex $chantypes 0 ]
        }
       
        set metalist [ list ]
        
        ;## known channel types include Adc, Proc, Sim
        ;## and Ser types. Not all types may be supported.
        
       	foreach metainfo $meta {
           switch -regexp -- $metainfo {
           	  0 { set metalist 0 }
			  1 { lappend metalist $::META_INFO_ALL }
			  2 { lappend metalist $::META_INFO_ALL $::EXTENDED_META_INFO_ALL }          	 
           	  
              {[Ee][Xx][Tt][Ee[Nn][Dd][Ee][Dd]_[Aa][Dd][Cc]} {
              		lappend metalist $::EXTENDED_META_INFO_ADC  
              }
              {[Ee][Xx][Tt][Ee[Nn][Dd][Ee][Dd]_[Pp][Rr][Oo][Cc]} {
              		lappend metalist $::EXTENDED_META_INFO_PROC  
              }
              {[Ee][Xx][Tt][Ee[Nn][Dd][Ee][Dd]_[Ss][Ii][Mm]} {
              		lappend metalist $::EXTENDED_META_INFO_PROC  
              }
              {[Ee][Xx][Tt][Ee[Nn][Dd][Ee][Dd]_[Ss][Ee][Rr]} {
              		lappend metalist $::EXTENDED_META_INFO_PROC  
              }
              {[Aa][Dd][Cc]} {
                    lappend metalist $::META_INFO_ADC
              }           
          	  {[Pp][Rr][Oo][Cc]} {
                   	lappend metalist $::META_INFO_PROC
                             }
              {[Ss][Ii][Mm]} {
                    lappend metalist $::META_INFO_SIM
                             }
              {[Ss][Ee][Rr]} {
                    lappend metalist $::META_INFO_SERIAL
                             }
              {[Ee][Xx][Tt][Ee[Nn][Dd][Ee][Dd]_[Aa][Ll][Ll]} {
              	    lappend metalist $::EXTENDED_META_INFO_ALL
                 			 }       
              {[Aa][Ll][Ll]} {
                    lappend metalist $::META_INFO_ALL
                             }
              
                     default {
                              set err "unknown metadata type: '$metainfo'"
                              error $err
                             }
			}
        }				

        ;## uniquify and OR-join chantypes
        set metalist [ join [ lsort -unique $metalist ] | ]
        set metalist [ expr $metalist ]
        set types [ list ]
        
        ;## known channel types include Adc, Proc, Sim
        ;## and Ser types. Not all types may be supported.
        
        if	{ [ llength $chantypes ] } {
        	foreach type $chantypes {
           		switch -regexp -- $type {
              	{[Aa][Dd][Cc]} {
                              lappend types $::CHANNEL_ADC            
                             }
          		{[Pp][Rr][Oo][Cc]} {
                              lappend types $::CHANNEL_PROC
                             }
              	{[Ss][Ii][Mm]} {
                              lappend types $::CHANNEL_SIM
                             }
              	{[Ss][Ee][Rr]} {
                              lappend types $::CHANNEL_SERIAL
                             }
                     default {
                              set err "unknown channel type: '$type'"
                              error $err
                             }
               	}
           }
        } else {
        	lappend types $::CHANNEL_ADC $::CHANNEL_PROC 
        }
               
        ;## uniquify and OR-join chantypes
        set types [ join [ lsort -unique $types ] | ]
        set types [ expr $types ]
        ;## arguments are filename, metadata return flag,
        ;## and channel types as OR'd enumerated types.
        set seqpt "getChannelListFromFrameFile($filename $metalist $types):"
        set rawdata \
           [ getChannelListFromFrameFile $filename $metalist $types ] 
        
     } err ] } {
        return -code error "$seqpt $err"
     }
     return $rawdata
}

## ******************************************************** 

## ******************************************************** 
##
## Name: frame::addChans
##
## Description:
## Uses frame API third generation "daq" style functions
## to add a list of channels to a target frame ilwd
## object created with createRawFrame.
##
## Parameters:
## framep - the source frame ilwd object
## ilwdp - the target frame ilwd object
## list - the list of channel names or indices to add
##
## Usage:
## Call this function on the first frame of a group of
## frames that are being similarly reduced.
##
## Comments:
##

proc frame::addChans { jobid ilwdp type chlist } {
     set seqpt {}
     set errs [ list ]
     
     if { [ catch {
        set type [ ucase -strict $type ]
        foreach [ list frame framep ] [ frame::getImage $jobid ] { break }
        
        if { [ regexp {all} $chlist ] } {
           set max [ getFrame${type}DataNum $framep ]
           set chlist 0-[ expr { $max-1 } ]
        }

        ;## we dont handle slices here yet :TODO:
        set tmp [ list ]
        foreach chan $chlist {
           set chan [ lindex [ split $chan ! ] 0 ]
           lappend tmp $chan
        }
        set chlist $tmp
        unset tmp

        
        foreach chan $chlist {
           if { [ catch {
              switch -regexp -- $chan {
                 {^\d+$} {
                         set seqpt \
                            "daqTrigger${type}Index($framep $chan 1):"
                         daqTrigger${type}Index $framep $chan 1
                         }
             {^\d+-\d+$} {
                         set i 0
                         set j 0
                         regexp {(\d+)-(\d+)} $chan -> i j
                         set seqpt \
                            "daqTrigger${type}Range($framep $i $j 1):"
                         daqTrigger${type}IndexRange $framep $i $j 1
                         }
                 default {
                         set seqpt \
                            "daqTrigger${type}Name($framep $chan 1):"
                         daqTrigger${type}Name  $framep $chan 1
                         }
              } ;## end of switch
           } err ] } {
              lappend errs "$seqpt $err"
           }
        } ;## end of foreach
                
        set seqpt "insert${type}ChanList($framep $ilwdp $chlist):"   
        insert${type}ChanList $framep $ilwdp $chlist

        if { [ llength $errs ] } {
           addLogEntry "$errs (partial result likely)" 1
        }
        
     } err ] } {
        addLogEntry "$seqpt $err ($errs)" 2
        return -code error "[ myName ]:$seqpt $err"
     }
}   
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::chanp2ilwdp
##
## Description:
## Convert a list of channel pointers to a list of ilwd
## container pointers.
##
## The GtimeS and GtimeN values, as well as the Dt, must
## be got from the frame image.
##
## If the pointers are passed in a list alternating with
## some identifier, the idetifier will travel with the
## output.
##
## Parameters:
## 
## Usage:
##
## Comments:
## Can operate on all sorts of pointers: AdcData_p,
## SerData_p, Sim(Data|Event)_p, ProcData_p, Detector_p, and
## History_p.

proc frame::chanp2ilwdp { jobid ptrs { off 0 } { dt 0 } } {
     
     if { [ catch {
        set seqpt {}
        
        ;## if dt is 0 we get it from the frame
        if { ! $dt } {
           foreach [ list frame framep ] \
              [ frame::getImage $jobid ] { break }
           set dtp [ getFrameFrameHDt $framep ]
           set float [ getElement $dtp ]
           frame::destructElementWrap $dtp 
           regexp {>(.+)<} $float -> dt
        }
        
        foreach [ list i j q qq k ] \
           [ frame::timeData $jobid $off $dt ] { break }
       
        ;## if a dt was provided, use it!!
        if { $dt } { set k $dt }
        
        if { $off > 2000000 } {
           set i $off
           set j $dt
        }
        
        ;## maybe we have id/chan pairs
        foreach [ list chan ptr ] $ptrs {
           set seqpt "chan: '$chan' ptr: '$ptr'"
           ;## if not, and we have a pointer in chan, break
           ;## and just process a list of pointers (below)
           if { [ regexp {^_[0-9a-f]+_p_} $chan ] } { break }
           
           regexp {^_[0-9a-f]+_p_Fr([A-Z][a-z]+)(Data|Event)?$} $ptr -> type
           if { [ llength [ info commands ${type}2container ] ] } {
              if { [ regexp {History|Detector(Proc|Sim)} $type ] } {
                 set seqpt "${type}2container($ptr):"
                 lappend ilwdptrs $chan \
                    [ ${type}2container $ptr ]
              } elseif { [ regexp {Proc} $type ] } {
                 set seqpt "${type}2container($ptr $i $j):"
                 lappend ilwdptrs $chan \
                    [ ${type}2container $ptr $i $j ]
              } elseif { [ regexp {_p_FrSimData} $ptr ] } {
                 set seqpt "SimData2container($ptr $i $j):"
                 lappend ilwdptrs $chan \
                    [ SimData2container $ptr $i $j $k ]
              } elseif { [ regexp {_p_FrSimEvent} $ptr ] } {
                 set seqpt "SimEvent2container($ptr):"
                 lappend ilwdptrs $chan \
                    [ SimEvent2container $ptr $i $j $k ]
              } else {
                 set seqpt "${type}2container($ptr $i $j $k):"
                 lappend ilwdptrs $chan \
                    [ ${type}2container $ptr $i $j $k ]
              }
              set seqpt {}
           } else {
              lappend ilwdptrs $chan $ptr
           }
        }
       
        ;## and proceed on a list of bare pointers
        if { ! [ info exists ilwdptrs ] } {
           foreach ptr $ptrs {
              set seqpt "ptr: '$ptr'"
              regexp {_p_Fr([A-Z][a-z]+)(Data|Event)?$} $ptr -> type
              if { [ llength [ info commands ${type}2container ] ] } {
                 if { [ regexp {History|Detector(Proc|Sim)} $type ] } {
                    set seqpt "${type}2container($ptr):"
                    lappend ilwdptrs [ ${type}2container $ptr ]
                 } elseif { [ regexp {Proc} $type ] } {
                    lappend ilwdptrs [ ${type}2container $ptr $i $j ]
                 } else {
                    set seqpt "${type}2container($ptr $i $j $k):"
                    lappend ilwdptrs [ ${type}2container $ptr $i $j $k ]
                 }
                 set seqpt {}
              } elseif { [ regexp {_p_FrSimData} $ptr ] } {
                 set seqpt "SimData2container($ptr $i $j):"
                 lappend ilwdptrs [ SimData2container $ptr $i $j $k ]
              } elseif { [ regexp {_p_FrSimEvent} $ptr ] } {
                 set seqpt "SimEvent2container($ptr):"
                 lappend ilwdptrs [ SimEvent2container $ptr $i $j $k ]
              } else {
                 lappend ilwdptrs $ptr
              }
           }
        }
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $ilwdptrs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::getChanp
##
## Description:
## Returns a list of pointers alternating with the index
## or name of an adc/ser/sim/proc channel in a frame.
##
## Parameters:
## type - Adc, Ser, Sim, Proc
## chlist - names or indices
##
## Usage:
##
## Comments:
##

proc frame::getChanp { jobid type chlist } {
     set seqpt {}
     set retval [ list ]
     
     if { [  catch {
        foreach [ list - framep ] [ frame::getImage $jobid ] { break }
        set type [ string toupper $type ]
        set seqpt "daqActivateAll${type}s($framep):"
        daqActivateAll${type}s $framep
        set seqpt {}
        set type [ ucase -strict $type ]
        
        ;## handle "all" keyword
        if { [ regexp {all} $chlist ] } {
           set max [ getFrame${type}DataNum $framep ]
           set chlist 0-[ expr { $max-1 } ]
        }
        
        set chlist [ numRange $chlist ]
        
        ;## uniquify list items
        set tmp [ list ]
        foreach item $chlist {
           if { [ lsearch $tmp $item ] == -1 } {
              lappend tmp $item
           }
        }
        
        set chlist $tmp
        
        foreach chan $chlist { 
           set seqpt "getFrame${type}Data($framep $chan):"
           set chanp [ getFrame${type}Data $framep $chan ]
           lappend retval $chan $chanp
        }
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::globChanNames
##
## Description:
## Expand a channel name pattern if possible.
##
## Parameters:
##
## Usage:
##
## Comments:
## frame::chanlist is incomplete at present :TODO:

proc frame::globChanNames { jobid filename types pat } {
     
     if { [ catch {
        set names   [ list ]
        set matches [ list ]
        
        set data [ frame::chanlist $jobid $filename $types ]
        
        foreach item $data {
           foreach [ list name type ] $item {
              lappend names $name
           }
        }
        
        foreach name $names {
           if { [ regexp -nocase -- $pat $name ] } {
           lappend matches $name
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $matches
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::attributes
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::attributes { jobid } {
     
     if { [ catch {
        set localp 0
        set commands getFrameFrameHGTime
        foreach [ list - framep ] [ frame::getImage $jobid ] { break }
        if { ! [ string length $framep ] } {
           set localp 1
           set fname [ frame::latest $jobid ]
           set framep [ frame::file2ptr $jobid $fname ]
        }
        
        set items [ array names ::frame::frame ]
        
        foreach item $items {
           if { [ regexp {FrameH,([^\s\,]+)} $item -> attr ] } {
              lappend commands getFrameFrameH[ ucase $attr ]
           }
        }
        
        catch { ::unset attr }
        
        foreach command $commands {
           set attr {}
           set rawp {}
           regexp {getFrameFrameH(.+)} $command -> attr
           if { [ llength [ info commands $command ] ] } { 
              if { [ catch {
                 set rawp [ $command $framep ]
                 set raw [ getElement $rawp ]
                 frame::destructElementWrap $rawp
                 regexp {>(.+)<} $raw -> cooked
                 set ary($attr) $cooked
              } err ] } {
                 lappend debugstuff $err
              }
           }
        }
        
        ;## some frames at Hanford were missing GTime ??
        if { [ info exists ary(GTime) ] } {
           foreach [ list ary(GTimeS) ary(GTimeN) ] $ary(GTime) { break }
        }
        
        if { $localp } {
           frame::destructImage $jobid -nocomplain
        }

        if { [ info exists debugstuff ] } {
           addLogEntry $debugstuff red
        }
        
     } err ] } {
        if { $localp } {
           frame::destructImage $jobid -nocomplain
        }   
        return -code error "[ myName ]: $err"
     }
     return [ array get ary ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::latest
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::latest { jobid } {
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        set q [ key::time ]
        set sid [ sock::open diskcache emergency ]
        puts $sid "$::MGRKEY puts \$cid \[ cache::latest $jobid \]"
        fileevent $sid readable "set ::latest_$q 1"
        vwait ::latest_$q
        catch { unset ::latest_$q }
        set filename [ cmd::result $sid ]
        set err_rx {diskcache\s+emergency\s+error:}
        if { [ regexp $err_rx $filename ] } {
           return -code error $filename
        }
        set filename [ lindex $filename 0 ]
        ::close $sid
        if { ! [ string length $filename ] } {
           set err "no filename returned from diskcache API!"
           return -code error $err
        }
     } err ] } {
        catch { unset ::latest_$q }
        catch { ::close $sid }
        return -code error "[ myName ]: $err"
     }
     return $filename
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::output
##
## Description:
## Create a disk file from an ilwd frame object, or send it
## to another API.
##
## Parameters:
## target - the name of the file or API to be written to
## ilwdp - the ilwd frame object to write to target
## outputformat - "frame" for binary or "ilwd" for text
##
## Usage:
##
## Comments:
## Errors propagate back to caller.
## returns a string of zero length if the target is an api.

proc frame::output { jobid target ilwdptrs format } {
     
     #if { ! [ info exist ::${jobid} ] } {
     #   addLogEntry "no ::${jobid} array, target $target ilwdptrs $ilwdptrs format $format, skipped" purple
     #   return
     #}
     if { [ catch { 
        if { [ info exists ::DEBUG_FRAME_OUTPUT ] && \
             [ string equal 1 $::DEBUG_FRAME_OUTPUT ] } {
           set caller [ uplevel myName ]
           set caller_target $target
           set caller_format $format
        }
     
        if { $::DEBUG == [ expr 0xdb ] } {
           foreach ptr $ilwdptrs {
              frame::0xdb $jobid $ptr http://${ptr}_dump.ilwd
           }   
        }
     
        ;## we might have modified outputformat in concatElements
        if { [ regexp -nocase {frame} $format ] } {
           set test [ set ::${jobid}(-outputformat) ]
           if { ! [ regexp -nocase {frame} $test ] } {
              set format $test
           }
        }
     
        if { ! [ regexp -nocase {(ascii|binary)} $format ] } {
           regsub {ilwd} $format {ilwd binary} format
        }
     
        if { ! [ string length $target ] } {
           set target $jobid
        }
     
        ;## The order of items in this switch is CRITICAL!!
        switch -regexp -- $format {
           {ilwd} {
                  set filenames \
                     [ frame::outputIlwd \
                        $jobid $target $ilwdptrs $format ]
                  }
          {frame} {
                  set filenames \
                    [ frame::outputFrames \
                        $jobid $target $ilwdptrs $format ]
                  }
        {LIGO_LW} {
                  set filenames [ list ]
                  frame::outputLigolw $jobid $target $ilwdptrs
                  }
          default {
                  return -code error "unrecognised output format: '$format'"
                  }
           } ;## end of switch       
        
        if { [ info exists ::DEBUG_FRAME_OUTPUT ] && \
             [ string equal 1 $::DEBUG_FRAME_OUTPUT ] } {
           set dbg "caller: '$caller' caller_target: '$caller_target' "
           append dbg "caller_format: '$caller_format' target: '$target' "
           append dbg "format: '$format' filenames: '$filenames'"
           addLogEntry $dbg purple
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     
     return $filenames
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::outputFrames
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::outputFrames { jobid target objects format } {
     
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     set outfiles [ list ]
     
     if { [ catch {
        if { [ llength $objects ] == 1 } {
           set objects [ lindex $objects 0 ]
        }
        
        ;## send data to another API?
         set api_rx ^([ join $::API_LIST | ])$
         if { [ regexp -nocase $api_rx $target ] } {
         } else {
            if { [ regexp -nocase {ligo_?lw} $format ] } {
               set target ligolw
            }
         }
        
        set i 0
        foreach [ list frame ilwdp ] $objects {
           if { ! [ string length $ilwdp ] } {
              continue
           }

           ;## output frame file
           if { [ file exists $ilwdp ] } {
              set outfile \
                 [ lindex [ url2file $jobid file:/$ilwdp ] 1 ]
              bak $outfile
              file copy -force -- $ilwdp $outfile
              lappend outfiles $outfile
           } elseif { [ string equal frame $target ] } {
              if { ! [ regexp -- Frame $ilwdp ] } {
                 foreach [ list ilwdp method level ] \
                    [ frame::ilwdp2framep $jobid $ilwdp ] { break }
              }
              set fname \
              [ frame::filenameCalculator $jobid $frame $ilwdp ]
              
              lappend outfiles [ publicFile $jobid $fname $ilwdp ]
           ;## send to another API
           } elseif { [ regexp -nocase $api_rx $target ] } {
              if { [ regexp -- Frame $ilwdp ] } {
                 set ilwdp [ frame::framep2ilwdp $jobid $ilwdp ]
              }
              dataSend $jobid $target $ilwdp
           } else {
              if { ! [ regexp -- Frame $ilwdp ] } {
                 foreach [ list ilwdp method level ] \
                    [ frame::ilwdp2framep $jobid $ilwdp ] { break }
              }
              set fname \
                 [ frame::filenameCalculator $jobid $frame $ilwdp ]
              lappend outfiles [ publicFile $jobid $fname $ilwdp ]
           }
           
           set url \
              [ frame::outputURL $jobid [ lindex $outfiles end ] $format ]
           if { [ string length $url ] } {
              set outfiles [ lreplace $outfiles end end $url ] 
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     if { [ info exists outfiles ] } {
        return $outfiles
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::outputIlwd
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::outputIlwd { jobid fname objects format } {
     
     if { [ catch {
        set seqpt {}
        if { [ llength $objects ] == 1 } {
           set objects [ lindex $objects 0 ]
        }
        
        set api_rx ^([ join $::API_LIST | ])\$
        
        set i 0
        foreach [ list frame ilwdp ] $objects {
           if { ! [ string length $ilwdp ] } {
              continue
           }
           
           ;## this will return the 'correct' frame name
           ;## without an extension
           if { [ catch {
              set frame \
                 [ frame::filenameCalculator $jobid $frame $ilwdp ]
              set frame [ file rootname [ file tail $frame ] ].ilwd
           } err ] } {
              addLogEntry $err red
           }
           
           set rp [ set ::${jobid}(-returnprotocol) ]
           
           if { [ regexp -- {_p_FrameH} $ilwdp ] } {
              set ilwdp [ frame::framep2ilwdp $jobid $ilwdp ]
           }
           if { [ info exists ::${jobid}(Frame) ] } {    
              if { [ regexp {^(ftp|http|file):} $fname ] } {
                 set rp [ lrange [ split $fname / ] 0 end-1 ]
                 set rp [ join [ concat $rp $frame ] / ]
              } 
           } else {
              set frame $rp
           }
           
           ;## add an iterator to the name if there will be
           ;## multiple products
           if { [ llength $objects ] > 2 } {
              set root [ file rootname  $fname ]
              set fname ${root}_[ incr i ].ilwd
           }
          
           set of [ lindex $format end ]
           
           ;## determine whether the target is an api or a file
           if { [ regexp -- $api_rx $fname ] && \
              ! [ regexp -nocase {^frame$} $fname ] } {
              dataSend $jobid $fname $ilwdp
           } else {
              if { [ string equal frame $of ] } {
                 ;## user has specified a -targetapi or
                 ;## -frametarget and screwed up his of!!
                 set of [ set ::${jobid}(-outputformat) ] 
              }
              
              lappend outfiles \
                 [ publicFile $jobid $frame $ilwdp $of ]
              
              set url \
              [ frame::outputURL $jobid [ lindex $outfiles end ] $format ]
              if { [ string length $url ] } {
                 set outfiles [ lreplace $outfiles end end $url ] 
              }
           }
        }   
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     if { [ info exists outfiles ] } {
        return $outfiles
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::outputURL
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## The user must create the local directory under ::PUBDIR
## using the makeFtpDirectory user command, q.v.
##

proc frame::outputURL { jobid fname format } {
     
     #if { ! [ info exist ::${jobid} ] } {
     #   addLogEntry "no ::${jobid} array, fname $fname format $format, skipped" purple
     #   return
     #}
     if { [ catch {
        set url [ list ]
        
        if { ! [ string length [ join $fname ] ] } { return }
        
        set rp   [ set ::${jobid}(-returnprotocol) ]
        set targ [ set ::${jobid}(-frametarget)    ]
       
        set caller [ uplevel myName ]
     
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        set jobdir [ jobDirectory ]
        
        if { [ regexp -nocase {frame} $format ] } {
           set format frame
        }
        
        if { ! [ string length $rp ] } {
           if { [ regexp {^(file|http|ftp):/*} $targ ] } {
              set rp ${targ}[ file tail $fname ]
           }
        }

        if { [ regexp {^(http|file):/*(.+)} $rp -> proto rp ] } {
           set rp /$rp
        }

        if { [ string length [ join $rp ] ] < 3 } { return }
        
        if { [ string length [ file dirname $rp ] ] < 3 } { return }
        
        ;## returnprotocol provided is a directory on
        ;## local filesystem
        if { [ file isdirectory $rp ] } {
           bak $rp[ file tail $fname ]
           file copy -force ${jobdir}/[ file tail $fname ] $rp
           regsub $::HTTPDIR $rp {} rp
           set url $::HTTPURL$rp[ file tail $fname ]
        } elseif { [ file isdirectory [ file dirname $rp ] ] } {
           bak $rp
           file copy -force ${jobdir}/[ file tail $fname ] \
              [ file dirname $rp ]
           regsub $::HTTPDIR $rp {} rp
           set url $::HTTPURL$rp
        }
        
        ;## returnprotocol is a local or remote ftp site
        if { [ regexp {^ftp:/*(.+)} $rp -> path ] } {
           set path /$path
           if { [ file isdirectory $path ] } {
              set rp $path
              bak $rp[ file tail $fname ]
              file copy -force ${jobdir}/[ file tail $fname ] $rp
              regsub $::FTPDIR $rp {} rp
              set url $::FTPURL$rp[ file tail $fname ]
           } elseif { [ file isdirectory [ file dirname $path ] ] } {
              set rp $path
              bak $rp
              file copy -force ${jobdir}/[ file tail $fname ] \
                 [ file dirname $rp ]
              regsub $::FTPDIR $rp {} rp
              set url $::FTPURL$rp
           } else {
              foreach [ list protocol host port path ] \
                 [ parseURL $rp ] { break }
              if { [ string length [ join $path ] ] } {
                 if { [ regexp {\.} $host ] } {
                    outputUrls $jobid [ list $fname $rp ]
                 }
              }
           }
        }
        
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        }
     }
     return $url
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::outputLigolw
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::outputLigolw { jobid fname objects } {
     
     if { [ catch {
        if { [ llength $objects ] == 1 } {
           set objects [ lindex $objects 0 ]
        }
        foreach [ list frame ilwdp ] $objects {
           if { [ regexp -- Frame $ilwdp ] } {
              set newfp $ilwdp
              set ilwdp [ frame::framep2ilwdp $jobid $newfp ]
              set seqpt {}
           }
           
           dataSend $jobid ligolw $ilwdp
           
           if { [ info exists newfp ] } {
              foreach [ list - oldfp ] [ frame::getImage $jobid ] { break }
              
              if { ! [ string equal $newfp $oldfp ] } {
                 frame::managePointers $jobid destroy $newfp
                 unset newfp
              }   
           }
        
           if { [ string length $ilwdp ] } {
              frame::managePointers $jobid destroy $ilwdp
              set ilwdp [ list ]
           }
        }   
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::writeFile
##
## Description:
## 
## Parameters:
##
## Usage:
##
## Comments:
## this function is called per each frame file to be written
## and takes up to 0.5 secs if done in main thread.

proc frame::writeFile { jobid fname framep { method "" } { level "" } } {
     
     if { [ catch {
        if { ! [ string length $method ] } {
           if { [ info exists ::${jobid}(-compressiontype) ] } {
              set method [ set ::${jobid}(-compressiontype) ]
              if { ! [ string length $method ] } {
                 set method $::FRAMEDEFAULTCOMPRESSIONMETHOD
              }
           } else {
              set method $::FRAMEDEFAULTCOMPRESSIONMETHOD
           }
        }
        if { ! [ string length $level ] } {
           if { [ info exists ::${jobid}(-compressionlevel) ] } {
              set level [ set ::${jobid}(-compressionlevel) ]
              if { ! [ string length $level ] } {
                 set level $::FRAMEDEFAULTCOMPRESSIONLEVEL
              }
           } else {
              set level $::FRAMEDEFAULTCOMPRESSIONLEVEL
           }
        }   
        set seqpt {}
        ;## take ownership of the pointer 
        frame::prunePointers $jobid $framep
        set root [ file rootname $fname ]
        set fname ${root}.gwf
        set seqpt "openFrameFileThread($fname,w):"
        set fp [ openFrameFileThread $jobid $fname w ]
        set seqpt "writeFrame_t($fp $framep $method $level):"
        set tid [ writeFrame_t $fp $framep $method $level ]
        addLogEntry "$seqpt $tid created" purple
        if  { [ info exist ::${jobid}($tid) ] } {
            unset ::${jobid}($tid)
        }  
        after 20
        ::setAlert $tid ::$tid
        ::setTIDCallback $tid "frame::writeFileCallback $tid $jobid $fname $framep $method $level" 

        vwait ::${jobid}($tid)
        if  { [ info exist ::${jobid}($tid) ] } {
            unset ::${jobid}($tid)
        }
        if  { [ info exist ::${jobid}(errors) ] } {
            error [ set ::${jobid}(errors) ]
        }
        set seqpt "destructFrame($framep):"
        destructFrame $framep
        set seqpt "closeFrameFile($fp):"
        closeFrameFile $fp
     } err ] } {
        set err "$seqpt $err (about to destruct '$framep' "
        append err "and close '$fp')"
        addLogEntry $err red
        if { [ info exists framep ] } {
           destructFrame $framep
        }
        if { [ info exists fp ] } {
           closeFrameFile $fp
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::writeFileCallback
##
## Description:
## writeFile callback
##
## Parameters:
##
## Usage:
##
## Comments:

proc frame::writeFileCallback { tid jobid fname framep { method "" } { level "" } args } {

    if  { ! [ info exist ::$tid ] } {
        ;## addLogEntry "::$tid does not exist" purple
        return
    }
    set seqpt ""
    if  { [ catch {
        set safe  0
        set thread_state [ getThreadStatus $tid ]
        if  { [ string equal FINISHED $thread_state ] || \
               [ string equal $thread_state $::TID_FINISHED ] } {
            catch { set reaper [ getThreadFunction $tid ] }
            set seqpt "$reaper ($tid):"
            catch { ${reaper}_r $tid } result
            ::unset ::$tid
            addLogEntry "$tid reaped result '$result'" purple
            ;## this enables the trace
            if  { [ array exist ::${jobid} ] } {
                set ::${jobid}($tid) 1
            } else {
                error "::${jobid} array does not exist"
            }
        }
   } err ] } {
      if    { [ array exist ::${jobid} ] } {
            set ::${jobid}(errors) "$fname $tid error: $err"
      } else {
            addLogEntry "[ myName ]:$seqpt $err" red
      }
   }
}

## ******************************************************** 
##
## Name: frame::ilwdp2framep
##
## Description:
## Convert an ilwd container object to a frame object.
##
## Parameters:
##
## Usage:
##
## Comments:
## The ilwd object must map to a frame object for this to
## succeed

proc frame::ilwdp2framep { jobid ilwdp { blocking off } } {
     
     if { [ catch {
        if { $::DEBUG == [ expr 0xdb ] } {
           frame::0xdb $jobid $ilwdp http://${ilwdp}_ilwd2frame_dump.ilwd
        }
        foreach [ list method level ] \
           [ frame::ilwdCompressionMetadata $jobid $ilwdp ] { break }
        set seqpt "ilwd2frame_t($ilwdp):"
        set tid [ ilwd2frame_t $ilwdp ]
        after 20 
        set seqpt "ilwd2frame_r($tid $ilwdp):"
        set framep [ ilwd2frame_r $tid ]
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     frame::managePointers $jobid add $framep
     return [ list $framep $method $level ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::framep2ilwdp
##
## Description:
## Convert a frame object to an ilwd container object.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::framep2ilwdp { jobid { framep "" } { blocking off } } {
     
     if { [ catch {
        if { ! [ string length $framep ] } {
           foreach [ list - framep ] [ frame::getImage $jobid ] { break }
        }
        set seqpt "fullFrame2container_t($framep):"
        set tid  [ fullFrame2container_t $framep ]
        after 20
        set seqpt  "fullFrame2container_r($tid $framep):"
        set ilwdp [ fullFrame2container_r $tid ]
        debugPuts "converted $framep to $ilwdp"
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     frame::managePointers $jobid add $ilwdp
     return $ilwdp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::createIlwdPrototypeFrame
##
## Description:
## Given a frame name or framep, return an ilwd object
## representing that frame's header information including
## all history and detector info.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::createIlwdPrototypeFrame { jobid framep } {
     
     if { [ catch {
        set seqpt {}
        set local 0
        if { [ file exists $framep ] } {
           set local 1
           set framep [ frame::open_r $jobid $framep ]
        }
        set seqpt "createRawFrame($framep):"
        set rawframep [ ::createRawFrame $framep ]
        set seqpt {}
        set ilwdp [ frame::framep2ilwdp $jobid $rawframep ]
        frame::managePointers $jobid add $ilwdp
        set seqpt "destructFrame($rawframep):"
        ::destructFrame $rawframep
        ::unset rawframep
        if { $local } {
           frame::destructImage $jobid
        }
        
     } err ] } {
        if { $local } {
           frame::destructImage $jobid -nocomplain
        }
        if { [ info exists rawframep ] } {
           ::destructFrame $rawframep
        }
        if { [ info exists ilwdp ] } {
           frame::managePointers $jobid destroy $ilwdp
        }
        return -code error "[ myName ]:$seqpt $err"
     }
     return $ilwdp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::next
##
## Description:
## EXPERIMENTAL
## Register a block of code to be run on every new frame.
## The event is triggered by frame::cacheupdate after a
## successful update by setting frame::trigger.
##
## Parameters:
##
## Usage:
## to start:
## frame::next $::jobid {puts $::jobid running!}
##
## to kill:
## frame::next $::jobid kill
## that is, if the job id was debug1:
##   frame::next debug1 kill
##
## Comments:
## Demonstrates another method to achieve the same ends
## as bgLoop in the genericAPI.tcl
## must always be called in a "catch" context.

proc frame::next { { job "" } { code "" } } {
     if { [ string length $code ] } {
        if { [ regexp -- {^kill$} $code ] } {
           if { [ catch {
              trace vdelete ::frame::trigger w frame::_next_$job
              rename frame::_next_$job {}
              addLogEntry "job $job killed" 5
           } err ] } {
              catch { rename frame::next_$job {} }
              addLogEntry "frame::next: $err" 2
              return -code error "frame::next: $err"
           }
        } else {
           if { [ catch {
              eval proc _next_$job { args } { $code }
              trace variable ::frame::trigger w frame::_next_$job
              addLogEntry "job $job started" 5
           } err ] } {
              addLogEntry "frame::next: $err" 2
              return -code error "frame::next: $err"
           }
        }
     }   
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::sanity
##
## Description:
## Examine [ lindex $::frame::cache() 0 ] and make a log entry
## with some useful structural information, including:
## 1. file size
## 2. interferometer name
## 3. run
## 4. frame
## 5. data quality
## 6. gtimes.gtimen
## 7. leap seconds
## 8. local time offset from gmt
## 9. delta t of frame data
## 10. number of adc channels in the frame
## ;#ol
##
## Parameters:
## none
##
## Usage:
##
## Comments:
## Called at startup to record changes in the frame files.

proc frame::sanity { jobid { fname "" } } {
     set seqpt {}
     if { [ catch {
        if { ! [ string length $fname ] } {
           set fname [ frame::latest $jobid ]
        }
        set framep [ frame::file2ptr $jobid $fname ]
        frame::dictionary
        
        if { ! [ file exists $fname ] } {
           set msg "calculated file: '$fname' does not exist. "
           append msg "looks like I can't make sense out of "
           append msg "your frame cache, sorry!"
           return -code error $msg
        }
        
        set seqpt "openFrameFileThread($fname,r):"
        set fp [ openFrameFileThread $jobid $fname r ]
        set seqpt "getFrameNumber($fp):"
        set N [ getFrameNumber $fp ]
        set seqpt "closeFrameFile($fp):"
        closeFrameFile $fp
        set seqpt {}
        set Name        not_defined
        set Run         not_defined
        set Frame       not_defined
        set DataQuality not_defined
        set GTimeS      not_defined
        set GTimeN      not_defined
        set ULeapS      not_defined
        set LocalTime   not_defined
        set Dt          not_defined
        foreach [ list name value ] [ frame::attributes $jobid ] {
           set $name $value
        }
        
        set time unknown
        regexp -- {\d{9,10}} $fname time
        set seqpt {}
        set here $::LDAS_SYSTEM
        set msg    "frame_name: $fname\n"
        append msg "location: $here\nlocal_gps_time: $time\n"
        append msg "frame_file_size: [ file size $fname ]\n"
        append msg "interferometer: $Name\n"
        append msg "run: $Run\n"
        append msg "frame: $Frame\n"
        append msg "data_quality: $DataQuality\n"
        append msg "frame_gpstime: $GTimeS.$GTimeN\n"
        append msg "leapsecs: $ULeapS\n"
        append msg "localtime_offset: $LocalTime\n"
        append msg "delta_t: $Dt\n"
        if { [ regexp {_p_FrameH$} $framep ] } {
           set seqpt "getFrameAdcDataNum($framep):"
           set numchans [ getFrameAdcDataNum $framep ]
           append msg "number_of_adc_channels: $numchans\n"
        } else {
        }
        append msg "number_of_frames_in_frame: $N"
        addLogEntry $msg 0
        frame::destructImage $jobid -nocomplain
     } err ] } {
        frame::destructImage $jobid -nocomplain
        addLogEntry "$seqpt $err" 3
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::killJob
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::killJob { jobid args } {

     if { [ catch {
        set now [ clock seconds ]
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        if { [ info exists ::filecache$job ] } {
           ::unset ::filecache$job
        }
        if { [ info exists ::${jobid}_outfiles ] } {
           ::unset ::${jobid}_outfiles
        }
        if { [ info exists ::threadcount($jobid) ] } {
           if { [ llength $::threadcount($jobid) ] } {
              set msg "threads running under this jobid will be "
              append msg "orphaned and garbage collected as possible."
              foreach tid $::threadcount($jobid) {
                 catch { getThreadStatus $tid } state
                 if { [ regexp {RUNNING} $state ] } {
                    set ::runningorphans($tid) $jobid
                    after $::REAP_THREAD_DELAY frame::reapOrphanThread $tid
                 } 
                 if { ! [ regexp {invalid_tid} $state ] } {
                    append msg " (thread '$tid' is in state '$state')"
                 }
                 if	{ [ regexp {FINISHED} $state ] } {
                    catch { set reaper [ getThreadFunction $tid ] } err
                    catch { ${reaper}_r $tid } result
                    addLogEntry "reaped finished thread $tid via $reaper: $err $result" purple
                 }
              }
              append msg " running threads will be reaped when state "
              append msg " reaches FINISHED."
              addLogEntry $msg red
           }
           ::unset ::threadcount($jobid)
        }
        frame::destructImage $jobid -nocomplain
        frame::managePointers $jobid destroy .+
        if { [ info exist ::$jobid ] } {
           frame::killJobReattach $jobid
        }
        ;## if this is called from defunctJobs we need to unset the job array here
        catch { ::unset ::${jobid} }
        ::emptyDataBucket $jobid
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::killJobReattach
##
## Description:
## Causes a reattach when the killJob was made by request
## from the manager
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::killJobReattach { jobid } {
   
     if { [ catch {
        set caller [ uplevel 2 myName ]
        if { [ string equal kIlLjOb $caller ] } {
           if { [ info exists ::${jobid}(cid) ] } {
              set cid  [ set ::${jobid}(cid) ]
              set msg "jobid '$jobid' cid $cid killed at manager's request."
              set ::$cid [ list 1 $msg error! ]
              addLogEntry $msg purple
              reattach $jobid $cid
              catch { ::unset ::$jobid }
           }
        }   
     } err ] } {
        catch { ::unset ::$jobid }
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::registerTid
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::registerTid { jobid tid } {
     
     if { [ catch {
        if { [ array exists ::threadcount ] } {
           foreach name [ array names ::threadcount ] {
              set i [ lsearch $::threadcount($name) $tid ]
              set ::threadcount($name) \
                 [ lreplace $::threadcount($name) $i $i ]
           }
        }
        if { [ info exists ::runningorphans($tid) ] } {
           ::unset ::runningorphans($tid)
        }
        if { [ info exists ::$tid ] } {
           ::unset ::$tid
        }
        if { [ info exists ::threadcount($jobid) ] } {
           lappend ::threadcount($jobid) $tid 
        } else {
           set  ::threadcount($jobid) $tid
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::unregisterTid
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::unregisterTid { jobid tid } {
     
     if { [ catch {
        ::unset ::$tid
        set i [ lsearch $::threadcount($jobid) $tid ]
        set ::threadcount($jobid) \
           [ lreplace $::threadcount($jobid) $i $i ]

        foreach tid $::threadcount($jobid) {
           catch { getThreadStatus $tid } state
           if { [ regexp {invalid_tid} $state ] } {
              set i [ lsearch $::threadcount($jobid) $tid ]
              set ::threadcount($jobid) \
                 [ lreplace $::threadcount($jobid) $i $i ]
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::0xdb
##
## Description:
## Setting the level of ::DEBUG for this API to 0xdb will
## cause this function to be called to dump database table
## formatted output to an ilwd text file.
##
## Parameters:
##
## Usage:
##
## Comments:
## Set ::DEBUG to 0xdb by connecting to the e   frame::managePointers $jobid delete $ptrmergency socket
## and sending: ::MGRKEY "set ::DEBUG 219"

proc frame::0xdb { jobid ptr { rp http://frame_dump.ilwd } } {
     if { [ catch {
        foreach [ list url fn ] [ url2file $jobid $rp ] { break }
        set fn [ publicFile $jobid $fn $ptr ascii ]
     } err ] } {
        addLogEntry $err 1
     }
     addLogEntry "frame data dumped to $url" 5
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::registry
##
## Description:
## Reports number of ilwd elements registered in memory
## at the time of the call.  Then destroys those elements
## with a call to the new destroyElements function.
## 
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::registry { jobid { destroy 0 } } {
     set names [ getElementList ]
     set N1 [ eval llength $names ]
     
     if { $N1 } {
        debugPuts "$N1 undestructed elements in registry: $names"
        set eln [ eval lindex $names end ]
        set mystery [ getElement $eln ]
        set mystery [ string range $mystery 0 255 ]...
        addLogEntry "sample contents of $eln: '$mystery'" blue
        regexp {\{(.+)\}} $names -> names 
        
        if { $destroy } {
           foreach el $names {
              frame::destructElementWrap $el
           }   
        
           set names [ getElementList ]
           set N2 [ eval llength $names ]
        
           if { $N2 } {
              set msg "$N2 orphan elements ($names) "
              append msg "could not be destroyed!"
           } else {
              set msg "all orphan elements successfully destroyed."
           }
           addLogEntry $msg blue
        }
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: frame::rdsDirectory
##
## Description:
## Determines the correct directory to write the output
## RDS into.
##
## This was formerly strongly tied to a standard location
## for FTP toplevel, but we don't support FTP anymore, so
## the whole thing is getting an http face.
##
## Parameters:
##
## Usage:
##
## Comments:
## The directory MUST already exist!

proc frame::rdsDirectory { jobid dir perjobdirs } {
     
     if { [ catch {
        
        ;## /ldas_outgoing/jobs/LDAS-SITE_N/LDAS-SITEN
        set jobdir [ jobDirectory ]
        
        if { [ info exists ::${jobid}(-userid) ] } {
           set user [ set ::${jobid}(-userid) ]
        } else {
           set user NULL
        }
        
        if { [ string length [ join $dir ] ] } {
           ;## if the directory specified is NOT under ::PUBDIR
           ;## then we need to verify that the particular user
           ;## is authorised to write in the directory.
           if { ! [ string match ${::PUBDIR}* $dir ] } {
              ;## users can write to dirs specifically
              ;## declared in the ::USER_CAN_WRITE_RDS_TO_DIRS
              ;## ARRAY.
              frame::restrictedUserDirs $user $dir
           }
           
           ;## users in the ::USERS_WHO_CAN_CREATE_RDS_DIRS
           ;## list have unrestricted directory creation
           ;## permission.
           frame::unrestrictedUserDirs $user $dir
           
           if { [ file isdirectory $dir ] && $perjobdirs } {
              ;## write into a per job directory hierarchy under
              ;## the specified -outputdir
              set jobdir \
                 [ join [ lrange [ file split $jobdir ] end-1 end ] / ]
              set dir $dir/$jobdir
              file mkdir $dir
           } elseif { [ file isdirectory $dir ] } { 
              ;## the createRDS user command can write into a
              ;## single directory repeatedly and it will manage
              ;## the filenames properly
              set dir $dir
           } else {
              set msg "the directory specified: '$dir' does not exist "
              append msg "or is not a directory."
              return -code error $msg
           }
        } else {
           set dir $jobdir
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $dir
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::restrictedUserDirs
##
## Description:
## 
## Parameters:
##
## Usage:
## Users can be declared to have restricted access to
## directories not under ldas_outgoing by declaring a
## list of specially accessible directories on a per-user
## basis in the LDASframe.rsc file like this:
##
## set ::USER_CAN_WRITE_RDS_TO_DIRS(joe_user) dir1
##
##  or:
##
## set ::USER_CAN_WRITE_RDS_TO_DIRS(joe_user) [ list dir1 dir2 ]
##
## Where dir1 and dir2 must be the full paths to existing
## directories.
##
## Comments:
##

proc frame::restrictedUserDirs { user dir } {
     
     if { [ catch {
        if { [ info exists ::USER_CAN_WRITE_RDS_TO_DIRS($user) ] } {
           if { [ lsearch $::USER_CAN_WRITE_RDS_TO_DIRS($user) $dir ] < 0 } {
              set err    "user $user does not have permission "
              append err "to write to $dir. if this user should "
              append err "be allowed to write to that directory, "
              append err "then ::USER_CAN_WRITE_RDS_TO_DIRS($user) "
              append err "should be modified accordingly."
              return -code error $err
           }
        } elseif { ! [ info exists ::USERS_WHO_CAN_CREATE_RDS_DIRS ] } {
           set err "user '$user' is not allowed to write to '$dir'"
           return -code error $err
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $dir
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::unrestrictedUserDirs
##
## Description:
## Users declared in the ::USERS_WHO_CAN_CREATE_RDS_DIRS
## variable in the LDASframe.rsc file can write to and
## create directories freely within the permissions of the
## user who starts LDAS.
##
## Note that it is a significant drop in security to give
## users this privelege. A secure method also exists via the
## restricted user access array ::USER_CAN_WRITE_RDS_TO_DIRS().
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::unrestrictedUserDirs { user dir } {
         
     if { [ catch {
        if { [ info exists ::USERS_WHO_CAN_CREATE_RDS_DIRS ] && \
             [ lsearch $::USERS_WHO_CAN_CREATE_RDS_DIRS $user ] > -1 } {
             if { ! [ file isdirectory $dir ] } {
              file mkdir $dir
              set msg "created directory '$dir' for user '$user'"
              addLogEntry $msg blue
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::callbackRDS
##
## Description:
##
## Parameters:
## Callback at end of createRDS
##
## Usage:
##
## Comments:
##

proc frame::callbackRDS { jobid tid cid rdsargs args } {
     
     if	{ ! [ info exist ::$tid ] } {
    	addLogEntry "::$tid does not exist" purple
        return
     }
     if { [ catch {
        regexp {\d+} $jobid job
         
        ;## PR #2168 - error handler was generating
        ;## undefined variable exception on 'callback'!
        set callback "[ getThreadFunction $tid ]_r"
        
        set state [ set ::$tid ]
        
        ;## orphans are handled specially
        if { ! [ array exists ::$jobid ] } {
           addLogEntry "::$jobid does not exist" purple
           return {}
        }
        
        if { [ string equal $state $::TID_FINISHED ] || \
             [ string equal FINISHED $state ] } {
           if { [ info exists ::filecache$job ] } {
              ::unset ::filecache$job
           }
                      
           set subj [ set ::${jobid}(-subject)   ]
           set dir  [ set ::${jobid}(-outputdir) ]
           
           if { ! [ string length [ join $subj ] ] } {
              set subj results
           }
           
           ;## CALLBACK (_r function) is called HERE
           set filenames [ $callback $tid ]
           
           frame::unregisterTid $jobid $tid
           
           if { [ llength $::threadcount($jobid) ] == 0 } {
              ::unset ::threadcount($jobid)
           }
           
           set files [ list ]
           foreach filename $filenames {
              lappend files [ file tail $filename ]
           }
           
           if { [ regsub ^$::HTTPDIR $dir {} dir ] } {
              set there $::HTTPURL$dir
           } else {
              regsub $::HTTPDIR\$ $::HTTPURL {} url
              set there $url$dir
           }
           
           frame::managePointers $jobid destroy .+
           
           ;## if no filenames are returned and no exception
           ;## is thrown.
           if { ! [ string length [ lindex $files 0 ] ] } {
              set err "OOPS!! thread returned null string!!"
              return -code error $err
           }
           
           set msg "Your results:\n$files\ncan be found at:\n$there/"
           
           if	{ [ info exist ::${jobid}(-md5sumoutdir) ] } {
           		set md5sumoutdir [ set ::${jobid}(-md5sumoutdir) ]
                set md5file [ file tail [ set ::${jobid}(-outputdir) ] ].md5 
                if { [ regsub ^$::HTTPDIR $md5sumoutdir {} md5sumoutdir ] } {
              		set there $::HTTPURL$md5sumoutdir
           		} else {
              		regsub $::HTTPDIR\$ $::HTTPURL {} url
              		set there $url$md5sumoutdir
           		}
           		append msg "\nmd5sum $md5file is in $there"
           }
           	
           set ::$cid [ list 4 $msg $subj ]
           ::reattach $jobid $cid
           if { [ info exists ::$jobid ] } {
              ::unset ::$jobid
           }
          
        }
     } err ] } {
        if { [ string length $err ] } {
           frame::notifyBadFrames $jobid $callback $err
           set err "[ myName ]:tid: $tid callback: $callback error: $err"
           
        	;## addLogEntry "writing '$rdsargs' to ${::DUMPDIR}/${jobid}.createRDS" purple
        	;## set fd [ open ${::DUMPDIR}/${jobid}.createRDS w ]
        	;## puts $fd "[array get ::$jobid ]\n$rdsargs"
        	;## close $fd
            
           frame::newJobAbort $jobid $cid $err
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::notifyBadFrames
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::notifyBadFrames { jobid callback error } {
     
     if { [ catch {
        set exception_rx {(CHECKSUM_ERROR|UNSUPPORTED_FRAME_SPEC|FRAME_SPEC_CONFORMANCE|UNSUPPORTED_CHECKSUM_TYPE|NO_CHECKSUM|INVALID_FRAME_STRUCTURE|FILE_TRUNCATION)}
        if { [ regexp -- $exception_rx $error ] } {
           set subject "$::LDAS_SYSTEM corrupt frame file detected!"
           set body "$subject\n\n$jobid ${callback}:\n$error"
           addLogEntry "Subject: ${subject}; Body: $body" email 
        }
     } err ] } {
        addLogEntry $err red 
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::waitForRDSData
##
## Description:
## Waits up to one minute for the diskcache API to set
## the filename list
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::waitForRDSData { jobid cid { i 0 } } {
      
     if { [ catch {
        regexp {\d+} $jobid job
        
        ;## first time through detach from assistant manager
        if { $i == 0 } {
           trace vdelete ::$cid w "reattach $jobid $cid"
        }
        
        ;## and wait for up to 60 seconds for the diskcache
        ;## API to populate the filecache list
        if { [ info exists ::filecache$job ] && \
             [ string length [ set ::filecache$job ] ] } {
           set filenames [ join [ set ::filecache$job ] ]
           frame::createRDSFrames $jobid $cid $filenames
           unset ::filecache$job
         } elseif { $i >= $::DELAY_RECV_FRAMES_FROM_DISKCACHE_SECS } {
            set err "no frames received from diskcache API"
            return -code error $err
         } else {
            incr i
            after 1000 [ list frame::waitForRDSData $jobid $cid $i ]
         }

     } err ] } {
        set err "[ myName ]: $err" 
        frame::newJobAbort $jobid $cid $err
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::parseRDSFramequery
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::parseRDSFramequery { jobid } {
     
     if { [ catch {
        set channels   [ set ::${jobid}(-channels)   ]
        set timerange  [ set ::${jobid}(-times)      ]
        set framequery [ set ::${jobid}(-framequery) ]
        
        if { ! [ string length $channels ] && \
             ! [ string length $timerange ] } {
           set framequery [ frame::parseFrameQuery $jobid $framequery ]
           foreach query $framequery {
              lappend channels [ lindex $query 4 ]
              if { ! [ string length $timerange ] } {
                 set timerange [ lindex $query 2 ]
              } else {
                 if { ! [ string equal $timerange [ lindex $query 2 ] ] } {
                    set msg "Inconsistent -framequery time ranges!"
                    return -code error $msg
                 }
              }
           }
        }     
        
        ;## fiddle with the calling context
        uplevel set channels  [ list $channels ]
        uplevel set timerange $timerange
        
     } err ] } {
        return -code error "[ myName ]: $::errorInfo"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::createRDSFrames
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::createRDSFrames { jobid cid filenames } {

     if { [ catch {
        set seqpt {}
        set channels      [ set ::${jobid}(-channels)   ]
        set dir           [ set ::${jobid}(-outputdir)  ]
        set type          [ set ::${jobid}(-usertype)   ]
        set timerange     [ set ::${jobid}(-times)      ]
        set perjobdirs    [ set ::${jobid}(-usejobdirs) ]
        set comptype      [ set ::${jobid}(-compressiontype)  ]
        set complevel     [ set ::${jobid}(-compressionlevel) ]
        set cksum         [ set ::${jobid}(-filechecksum)     ]
        set framecksum    [ set ::${jobid}(-framechecksum)    ]
        set timecheck     [ set ::${jobid}(-frametimecheck)   ]
        set metadatacheck [ set ::${jobid}(-metadatacheck)    ]
        set framesperfile [ set ::${jobid}(-framesperfile)    ]
        set secperframe   [ set ::${jobid}(-secperframe)      ]
        set validcheck    [ set ::${jobid}(-framedatavalid)   ]
        set allowshort    [ set ::${jobid}(-allowshortframes)     ]
        set filldatavalid [ set ::${jobid}(-fillmissingdatavalid) ]
        set generatecksum [ set ::${jobid}(-generatechecksum)     ]
        set md5sumregexp  [ set ::${jobid}(-md5sumregexp)     ]
        
        ;## PR #2807 adds support for -framequery option to
        ;## the createRDS user command.
        frame::parseRDSFramequery $jobid
        
        ;## massages framequery style chanspecs into RDS style
        set ::DEBUG_CREATE_RDS 1
        set tmp [ list ]
        foreach chanset $channels {
           regexp {[a-zA-Z]+\(([^\)]*)\)} $chanset -> chanset
           lappend tmp [ split $chanset , ]
        }
        set channels [ join $tmp ]
        
        foreach [ list cksum framecksum timecheck validcheck metadatacheck ] \
        [ frame::inputFileFlagHandler $cksum $framecksum $timecheck $validcheck $metadatacheck ] \
        { break }
        
        foreach [ list allowshort filldatavalid generatecksum ] \
        [ frame::outputFileFlagHandler $allowshort $filldatavalid $generatecksum ] \
        { break }
        
        if { ! [ string length $type ] } { set type {} }
        
        set comptype [ string toupper $comptype ]
        
        foreach [ list channels factors ] \
           [ frame::createRDSParseChannels $channels ] { break }

        ;## PR #2715 
        if { [ regexp {^(\d+)$} $timerange -> start ] } {
           set names [ join [ split $filenames "," ] ]
           set test [ lindex $names end ]
           
           if { [ regexp {[2486]} $factors ] } {
              set test [ lindex $names end-1 ]
           }
           
           if { [ regexp -- {(\d{9,10})-(\d+)} $test -> ts dt ] } {
              set end [ expr { $ts + $dt - 1 } ]
              set timerange ${start}-$end
           }
        }
        
        if { ! [ regexp {(\d+)-(\d+)} $timerange -> start end ] } {
           set msg "invalid time range provided by user: '$timerange'"
           return -code error $msg
        }
        
        ;## Per PR #1828
        # set bad_rx {(DIFF_ZERO_SUPPRESS_SHORT|ZERO_SUPPRESS_INT_FLOAT)}
	set bad_rx { }
        
        if { [ regexp -- $bad_rx $comptype bad ] } {
           set msg "Compression type $bad is not supported!"
           return -code error $msg
        }
        
        set dir [ frame::rdsDirectory $jobid $dir $perjobdirs ]
        
        ;## reset the user option to the real directory
        set ::${jobid}(-outputdir) $dir

        ;## force filenames into a cslist
        set filenames [ join [ split $filenames ] "," ]
        
        ;## PR 2953 - create outputdir for md5sum based
        ;## regexp substitution of outputdir
        
        set md5sumoutdir [ frame::md5sumOutputDir $jobid $dir ]
        
        if	{ ! [ string equal $dir $md5sumoutdir ] } {
        	set ::${jobid}(-md5sumoutdir) $md5sumoutdir
        }
        
        if { [ info exists ::DEBUG_CREATE_RDS ] && \
             [ string equal 1 $::DEBUG_CREATE_RDS ] } {
           set dbgmsg "channels: '$channels' resample_factors: '$factors'"
           append dbgmsg " filenames: '$filenames' directory: '$dir'"
           append dbgmsg " user specified type: '$type'"  
           append dbgmsg " compression: '$comptype'"
           append dbgmsg " compression level: '$complevel'"
           append dbgmsg " filechecksum: '$cksum' timecheck: '$timecheck'"
           append dbgmsg " framechecksum: '$framecksum'"
           append dbgmsg " nometadatacheck: '$metadatacheck'"
           append dbgmsg " validcheck: '$validcheck'"
           append dbgmsg " framesperfile: '$framesperfile'"
           append dbgmsg " secperframe: '$secperframe'"
           append dbgmsg " allowshort: '$allowshort'"
           append dbgmsg " filldatavalid: '$filldatavalid'"
           append dbgmsg " generatecksum: '$generatecksum'"
           append dbgmsg " start: '$start' end: '$end'"
           append dbgmsg " md5sum output: '$md5sumoutdir'"
           addLogEntry $dbgmsg purple
        }
        
        if { [ regexp -- {[2468]} [ join $factors ] ] } {
           set seqpt "resampleRawFrames_t($start $end $filenames $channels $factors $dir $type $comptype $complevel $cksum \
           $framecksum $timecheck $validcheck $framesperfile $secperframe $allowshort $generatecksum $filldatavalid $metadatacheck $md5sumoutdir):"
           set rdsargs $seqpt
           set tid \
              [ ::resampleRawFrames_t $start $end $filenames $channels $factors $dir $type $comptype $complevel $cksum \
              $framecksum $timecheck $validcheck $framesperfile $secperframe $allowshort $generatecksum $filldatavalid $metadatacheck $md5sumoutdir ]
           frame::registerTid $jobid $tid
           set seqpt "tid: $tid callback: ::resampleRawFrames_r"
        } else {
           set seqpt "reduceRawFrames_t($start $end $filenames $channels $dir $type $comptype $complevel $cksum $framecksum \
           $timecheck $validcheck $framesperfile $secperframe $allowshort $generatecksum $filldatavalid $metadatacheck $md5sumoutdir):"
           set rdsargs $seqpt
           set tid \
              [ ::reduceRawFrames_t $start $end $filenames $channels $dir $type $comptype $complevel $cksum $framecksum $timecheck \
              $validcheck $framesperfile $secperframe $allowshort $generatecksum $filldatavalid $metadatacheck $md5sumoutdir ]
           frame::registerTid $jobid $tid
           set seqpt "tid: $tid callback: ::reduceRawFrames_r"
        } 
        
        if { [ info exists ::DEBUG_CREATE_RDS ] && \
             [ string equal 1 $::DEBUG_CREATE_RDS ] } {
           addLogEntry $seqpt purple
        }   
       
        set seqpt "setAlert($tid):"
        ::setAlert $tid ::$tid
        ::setTIDCallback $tid "frame::callbackRDS $jobid $tid $cid $rdsargs"
     } err ] } {
        set err "[ myName ]:$seqpt error: $err"
        
        ;## if	{ [ info exist rdsargs ] } {
        ;##	addLogEntry "writing '$rdsargs' to ${::DUMPDIR}/${jobid}.createRDS" purple
        ;##	set fd [ open ${::DUMPDIR}/${jobid}.createRDS w ]
        ;##	puts $fd "[array get ::$jobid ]\n$rdsargs"
        ;##	close $fd
        ;## }
        
        frame::newJobAbort $jobid $cid $err
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::inputFileFlagHandler
##
## Description:
## Handler for the ::FRAMECHECKSUM_FLAG ::FRAMETIMECHECK_FLAG
## ::FILECHECKSUM_FLAG and ::FRAMEDATAVALID_FLAG resource
## variables and the values explicitly provided via user
## command options.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::inputFileFlagHandler { filechecksum checksum timecheck datavalid metadatacheck } {
     
     if { [ catch {
        foreach flag [ list filechecksum checksum timecheck datavalid metadatacheck ] {
           set Flag [ string toupper $flag ]
           if { [ regexp -nocase {default} [ set $flag ] ] } {
              set $Flag [ set ::FRAME${Flag}_FLAG ]
           } else {
              set $Flag [ set $flag ]
           }
           if { ! [ regexp {[01]} [ set $Flag ] ] } {
              set err "invalid $Flag flag: '[ set $flag ]'.  "
              append err "must be '0', '1', or 'default'."
              return -code error $err
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $FILECHECKSUM $CHECKSUM $TIMECHECK $DATAVALID $METADATACHECK ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::outputFileFlagHandler
##
## Description:
## Handler for the ::ALLOWSHORT_FLAG ::FILLDATAVALID_FLAG
## and ::GENERATECKSUM_FLAG resource variables and the values
## explicitly provided via user command options.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::outputFileFlagHandler { allowshort filldatavalid generatecksum } {
     
     if { [ catch {
        foreach flag [ list allowshort filldatavalid generatecksum ] {
           set Flag [ string toupper $flag ]
           if { [ regexp -nocase {default} [ set $flag ] ] } {
              set $Flag [ set ::FRAME${Flag}_FLAG ]
           } else {
              set $Flag [ set $flag ]
           }
           if { ! [ regexp {[01]} [ set $Flag ] ] } {
              set err "invalid $Flag flag: '[ set $flag ]'.  "
              append err "must be '0', '1', or 'default'."
              return -code error $err
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $ALLOWSHORT $FILLDATAVALID $GENERATECKSUM ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::createRDSParseChannels
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::createRDSParseChannels { channels } {
     
     if { [ catch {
        set chans   [ list ]
        set factors [ list ]
        
        foreach channel $channels {
           set chan   [ list ]
           set factor [ list ]
           foreach [ list chan factor ] [ split $channel ! ] { break }
           lappend chans $chan
           if { ! [ string length $factor ] } { set factor 1 }
           lappend factors $factor
        }

        if { [ llength $chans ] != [ llength $factors ] } {
           set msg    "internal error:\n"
           append msg "length of channel list: [ llength $chans ]\n"
           append msg "not equal to length of resample factor list: "
           append msg [ llength $factors ]
           addLogEntry $msg email
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $chans $factors ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::parseChannelSlice
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## The offset and/or dt value of a slice can be suffixed
## with TIME, FREQ, or TIMEFREQ to supply hints to the
## C++ layer about the kind of data it is dealing with.
##

proc frame::parseChannelSlice { channel } {
     
     if { [ catch {
        set chan [ split $channel ! ]
        switch -exact -- [ llength $chan ] {
                 1 { 
                    set offset 0
                    set delta  0
                   }
                 4 {
                    set offset [ lindex $chan 1 ]
                    set delta  [ lindex $chan 2 ]
                    set chan   [ lindex $chan 0 ]
                   }
           default {
                    set err "invalid channel spec: '$channel'"
                    return -code error $err
                   } 
        }
        
        ;## trim off Hz and seconds specs.
        set trims TIMEFRQ
        set offset [ string trimright $offset $trims ]
        set delta  [ string trimright $delta  $trims ]
        
        foreach item [ list $offset $delta ] {
           if { [ regsub -all -- {\.} $item {} test ] > 1 || \
                [ regsub -all -- {\-} $item {} test ] > 1 || \
              ! [ regexp {^\-?\.?\d+\.?\d*$} $item ] } {
              set err "invalid channel spec: '$channel'"
              return -code error $err
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $chan $offset $delta ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::channelsToIfos
##
## Description:
## Takes all forms of channel spec and generates a sorted
## list of the associated interferometers.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::channelsToIfos { args } {
     
     if { [ llength $args ] == 1 } { 
        set args [ lindex $args 0 ] 
     }
     
     if { [ catch {
        set ifos [ list ]
        foreach chanspec $args {
           ;## clean up something like Adc(...)
           regexp {\S+\(([^\)]+)\)} $chanspec -> chanspec
           set chanspec [ join [ split $chanspec " ," ] ]
           foreach channel $chanspec {
              lappend ifos [ string index $channel 0 ] 
           }
        }
        set ifos [ lsort -unique $ifos ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $ifos
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::defunctJobs
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## DO not clean up the jobs if there are no live jobs
## since the frame works asynchronously.

proc frame::defunctJobs { args } {
     
     if { [ catch {
        
        if { [ info exists ::SORTED_LIVE_JOB_LIST_REPORT ] } {
           set oldest [ lindex $::SORTED_LIVE_JOB_LIST_REPORT 0 ]
           regexp {\d+} $oldest oldest
           set jobs [ info vars ::${::RUNCODE}* ]
           set killed [ list ]
           if { [ string length $oldest ] } {
              foreach jobid $jobs {
                 regexp {\d+} $jobid job
                 set jobid $::RUNCODE$job
                 if { $job + 10 < $oldest  } {
                    frame::killJob $job
                    lappend killed $job
                    set numDefunct [ llength $killed ]
                    if	{ $numDefunct > 29 } {
             			addLogEntry "removed $numDefunct defunct jobs $killed" blue
                        set killed [ list ]
                    }
                 }
              }
           }
           ;## write out the remaining defunct jobids
           set numDefunct [ llength $killed ]
           if	{ $numDefunct } {
            	addLogEntry "removed $numDefunct defunct jobs $killed" blue
           }
        }
     } err ] } {
        set subject "$::LDAS_SYSTEM $::API API error!"
        set report "[ myName ]: $err"
        addLogEntry "Subject: ${subject}; Body: $report" email
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: frame::ilwdCompressionMetadata
##
## Description:
## Get and set ilwdp frame compression metadata used by
## writeFrame. The method and level were either already
## set in the ilwdp, or they are set based on global
## user command options, or failing that they are set based
## on resource variable default values.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc frame::ilwdCompressionMetadata { jobid ilwdp } {
     
     if { [ catch {
        if { [ catch {
           set method [ getElementMetadata $ilwdp compression_method ]
        } err ] } {
           if { [ info exists  ::${jobid}(-compressiontype) ] } {
              set method [ set ::${jobid}(-compressiontype) ]
              if { ! [ string length $method ] } {
                 set method $::FRAMEDEFAULTCOMPRESSIONMETHOD
              }
           } else {
              set method $::FRAMEDEFAULTCOMPRESSIONMETHOD
           }
           setElementMetadata $ilwdp compression_method $method
        }
        if { [ catch {
           set level  [ getElementMetadata $ilwdp compression_level  ]
        } err ] } {
           if { [ info exists  ::${jobid}(-compressionlevel) ] } {
              set level [ set  ::${jobid}(-compressionlevel) ]
              if { ! [ string length $level ] } {
                 set level $::FRAMEDEFAULTCOMPRESSIONLEVEL
              }
           } else {
              set level $::FRAMEDEFAULTCOMPRESSIONLEVEL
           }
           setElementMetadata $ilwdp compression_level $level
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $method $level ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::updateDeviceIOConfiguration
##
## Description:
##   This handles updating the database of device io configuration.
##   It is called as the result of ::DEVICE_IO_CONFIGURATION being
##   modified (either by writing or by unsetting.
##
## Parameters:
##   None
##
## Usage:
##	frame::updateDeviceIOConfiguration
##
## Comments:
##
proc frame::updateDeviceIOConfiguration { args } {
     if { ! [ info exists ::DEVICE_IO_CONFIGURATION ] } {
	   ;## Reestablish the variable
	   set ::DEVICE_IO_CONFIGURATION [ list ]
	   ;## re-register the trace
	   trace variable ::DEVICE_IO_CONFIGURATION wu \
	      frame::updateDeviceIOConfiguration
    }
    ;## Update the database
	
	;## PR3092 frame core dumps if this is set to {[list]}
	if	{ [ regexp {^\[[\s\t]*list[\t\s]*\]$} $::DEVICE_IO_CONFIGURATION ] } {
		addLogEntry "Invalid data for resource ::DEVICE_IO_CONFIGURATION '$::DEVICE_IO_CONFIGURATION'; \
			::DEVICE_IO_CONFIGURATION reset to \[ list \]" orange
		return
	} 
	addLogEntry "Setting ::DEVICE_IO_CONFIGURATION to '$::DEVICE_IO_CONFIGURATION'" purple
    set warnings [ResetDeviceIOConfiguration $::DEVICE_IO_CONFIGURATION]
    if { [ string length $warnings ] > 0 } {
	foreach line [split $warnings "\n"] {
	    addLogEntry $line yellow
	}
    }
    
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::bgUpdateDeviceIOConfiguration
##
## Description:
##   Background callback to ensure that this variable
##   is always in the system
##
## Parameters:
##   None
##
## Usage:
##	frame::bgUpdateDeviceIOConfiguration
##
## Comments:
##
proc frame::bgUpdateDeviceIOConfiguration { args } {
     if { ! [ info exists ::DEVICE_IO_CONFIGURATION ] } {
	   frame::updateDeviceIOConfiguration
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::md5sumOutputDir
##
## Description:
## Determines the correct directory to write the output
## RDS md5sum file into.
##
## PR 2953 - allow md5sum to be written into 
## directory based on regexp substitution of 
## for FTP toplevel, but we don't support FTP anymore, so
## the whole thing is getting an http face.
##
## Parameters:
##
## Usage:
##
## Comments:
## PR 2953 - specify md5sum directory from outputdir

proc frame::md5sumOutputDir { jobid outputdir } {
     
     if	{ [ info exists ::${jobid}(-md5sumregexp) ] } {
        set md5sumregexp [ set ::${jobid}(-md5sumregexp) ]
        if	{ ! [ string length $md5sumregexp ] } {
        	return $outputdir
        }
     } else {
     	return $outputdir
     }
     
     if { [ catch {
        
        if { [ info exists ::${jobid}(-userid) ] } {
           set user [ set ::${jobid}(-userid) ]
        } else {
           set user NULL
        }        
        set md5pat ""
        set md5result ""
        set global ""
        set dir ""
        if	{ [ info exists md5sumregexp ] } {
           	if	{ [ regexp {s,(\S+),(\S+),([gi]*)} $md5sumregexp -> md5pat md5result global ] } {
            	addLogEntry "user $user md5pat $md5pat, md5result $md5result global $global" purple
           	;## throw error for  spaces
           		if	{ ! [ string is print $md5result ] } {
           			error "md5sum directory specification contains non-printable characters '$md5result'"
           		}
                set flags ""
                if	{ [ string first g $global ] > -1 } {
                	append flags "-all "
                }
                if	{ [ string first i $global ] > -1 } {
                	append flags "-nocase "
                }
                append flags "--"
                if	{ ! [ string length $md5result ] } {
                	error "No md5sum replacement specified in -md5sumregexp $md5sumregexp"
                }
                set rc [ eval regsub $flags $md5pat $outputdir $md5result dir ]
                addLogEntry "'regsub $flags $md5pat $outputdir $md5result rc=$rc, dir $dir'" purple
            } else {
            	error "invalid md5sum specification $md5sumregexp, must be s,regexp,replacement,\[gi\]"
            }
        } 
        
        if	{ [ info exist rc ] } {
        	addLogEntry "md5sum directory is $dir" purple
           
        if { [ string length [ join $dir ] ] } {
           ;## if the directory specified is NOT under ::PUBDIR
           ;## then we need to verify that the particular user
           ;## is authorised to write in the directory.
           if { ! [ string match ${::PUBDIR}* $dir ] } {
              ;## users can write to dirs specifically
              ;## declared in the ::USER_CAN_WRITE_RDS_TO_DIRS
              ;## ARRAY.
              frame::restrictedUserDirs $user $dir
           }
           
           ;## users in the ::USERS_WHO_CAN_CREATE_RDS_DIRS
           ;## list have unrestricted directory creation
           ;## permission.
           frame::unrestrictedUserDirs $user $dir
           
		   if { [ file isdirectory $dir ] } { 
              ;## the createRDS user command can write into a
              ;## single directory repeatedly and it will manage
              ;## the filenames properly
              set dir $dir
           } else {
           	 error "User $user is not authorized to create md5sum output directory $dir"
           } 
        } else {
           set dir $jobdir
        }
       }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $dir
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::reapOrphanThread
##
## Description:
## reap orphan threads for jobs that are aborted or timed out
##
## PR 3132 - frameAPI leaks createRDS threads when running on dev
##
## Parameters:
##
## Usage:
##
## Comments:
## 8/27/08 all finished and orphaned threads should be reapable as the reaper is
## from getThreadFunction

proc frame::reapOrphanThread { tid  { duration 0 } } {

	if	{ [ catch {
    	set state [ getThreadStatus $tid ] 
        set jobid none
    	if	{ [ regexp {FINISHED} $state ] || \
              [ string equal $thread_state $::TID_FINISHED ] } {
            if  { [ info exist ::runningorphans($tid) ] } {
                set jobid $::runningorphans($tid)
            }
            catch { set reaper [ getThreadFunction $tid ] } err
            set seqpt "$reaper ($tid): $err"
            catch { ${reaper}_r $tid } result
            addLogEntry "reaped finished thread $tid via $reaper for job $jobid: $err $result" purple
            catch { unset ::runningorphans($tid) }
       	} else {
        	if	{ $duration < $::THREAD_TIMEOUT } {
            	incr duration $::REAP_THREAD_DELAY
            	after $::REAP_THREAD_DELAY frame::reapOrphanThread $tid $duration
            } else {
            	addLogEntry "$tid still not in FINISHED state after $duration secs, unable to reap." red
            }
        }
	} err ] } {
    	addLogEntry "$tid error: $err" red
    }
}

## ******************************************************** 
##
## Name: frame::openFrameFileThread
##
## Description:
## reap orphan threads for jobs that are aborted or timed out
##
## PR 3132 - frameAPI leaks createRDS threads when running on dev
##
## Parameters:
##
## Usage:
##
## Comments:

proc frame::openFrameFileThread { jobid filename mode } {

    if  { [ catch {
        set fp ""
        set seqpt "openFrameFile_t($filename $mode)"
        set tid [ openFrameFile_t $filename $mode ]
        addLogEntry "$seqpt $tid created" purple
        if  { [ info exist ::${jobid}($tid) ] } {
            addLogEntry "unset pre-existing ::${jobid}($tid)" orange
            unset ::${jobid}($tid)
        }  
        ::setAlert $tid ::$tid
        ::setTIDCallback $tid "frame::openFrameFileCallback $tid $jobid $filename $mode" 
        ;## just in case tid already finished
         
        vwait ::${jobid}($tid)
        set fp [ set ::${jobid}($tid) ]
        unset ::${jobid}($tid)

        if  { [ info exist ::${jobid}(errors) ] } {
            error [ set ::${jobid}(errors) ]
        }
    } err ] } {
        addLogEntry "$tid error: $err" red
        return -code error $err
    }
    return $fp
}

## ******************************************************** 
##
## Name: frame::openFrameFileCallback
##
## Description:
## writeFile callback
##
## Parameters:
##
## Usage:
##
## Comments:

proc frame::openFrameFileCallback { tid jobid filename mode args } {

    if  { ! [ info exist ::$tid ] } {
        ;## addLogEntry "::$tid does not exist" purple
        return
    }
    set seqpt ""
    if  { [ catch {
        set safe  0
        set thread_state [ getThreadStatus $tid ]
        if  { [ string equal FINISHED $thread_state ] || \
               [ string equal $thread_state $::TID_FINISHED ] } {
            catch { set reaper [ getThreadFunction $tid ] }
            set seqpt "$reaper ($tid):"
            catch { ${reaper}_r $tid } result
            ::unset ::$tid
            addLogEntry "$tid reaped result '$result'" purple
            ;## this enables the trace
            if  { [ array exist ::${jobid} ] } {
                set ::${jobid}($tid) $result
            } else {
                error "::${jobid} array does not exist"
            }
        }
   } err ] } {
      if    { [ array exist ::${jobid} ] } {
            set ::${jobid}(errors) "$filename $tid error: $err"
            set ::${jobid}($tid) error
      } else {
            addLogEntry "[ myName ]:$seqpt $err" red
      }
   }
}
