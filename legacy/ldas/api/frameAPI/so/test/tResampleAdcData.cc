#include "config.h"   

#include <iostream>
#include <memory>
#include <stdexcept>

#include "general/AtExit.hh"

#include "filters/Resample.hh"

#include "framecpp/FrameCPP.hh"
#include "framecpp/Dimension.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/Time.hh"

#include "ResampleCmd.hh"

using namespace std;

template<typename TIn>
FrameCPP::FrVect::data_types_type type_map()
{
    throw std::invalid_argument("type_map: bad type");

    return 0;
}

template<>
FrameCPP::FrVect::data_types_type type_map<INT_2S>()
{
    return FrameCPP::FrVect::FR_VECT_2S;
}

template<>
FrameCPP::FrVect::data_types_type type_map<INT_4S>()
{
    return FrameCPP::FrVect::FR_VECT_4S;
}

template<>
FrameCPP::FrVect::data_types_type type_map<REAL_4>()
{
    return FrameCPP::FrVect::FR_VECT_4R;
}

template<>
FrameCPP::FrVect::data_types_type type_map<REAL_8>()
{
    return FrameCPP::FrVect::FR_VECT_8R;
}

template<>
FrameCPP::FrVect::data_types_type type_map<COMPLEX_8>()
{
    return FrameCPP::FrVect::FR_VECT_8C;
}

template<>
FrameCPP::FrVect::data_types_type type_map<COMPLEX_16>()
{
    return FrameCPP::FrVect::FR_VECT_16C;
}

template<class TIn, class TOut>
bool TestResampleAdcData()
{
    bool pass = true;

    const int p = 1;
    const int q = 2;

    const size_t nData = 16384;

    TIn* const in = new TIn[nData];

    try
    {
      for (size_t k = 0; k < nData; ++k)
      {
        in[k] = 1;
      }

      const size_t nDataOut = p*nData/q;

      FrameCPP::Time	offset(6100000, 10);
      std::auto_ptr< FrameCPP::FrAdcData >
	adc( new FrameCPP::FrAdcData( "H2:LSC-AS_Q", /* name */
				      0,	/* group */
				      0,	/* channel */
				      16,	/* nBits */
				      16384.0L,	/* sampleRate */
				      0.0,	/* bias */
				      1.0,	/* slope */
				      "strain",	/* units */
				      0.0,	/* fShift */
				      offset.GetTime( ) /* timeOffset */ ) );
      
      FrameCPP::Dimension newDims( nData,
				   1.0L,
				   "sec",
				   1.0);

      FrameCPP::FrAdcData::data_type::value_type
	newVect( new FrameCPP::FrVect( "Data",
				       type_map<TIn>(),
				       1,
				       &newDims,
				       FrameCPP::BYTE_ORDER_HOST,
				       in,
				       "" ) );
      adc->RefData().append( newVect );
    
      Filters::ResampleBase* const r( createResampleState(q, adc.get( ) ) );
      
      FrameCPP::FrProcData* const newProc( resampleAdcData(adc.get( ), r ) );

      TOut* const out = (TOut*) newProc->RefData()[0]->GetData( ).get( );

      // Only count the good data, earlier data affected by filter start-up
      const size_t start = (size_t) (2*r->getDelay());
      for (size_t k = start; k < nDataOut; ++k)
      {
	const double diff = abs(out[k] - TOut(1));
	if (diff > 1.0e-5)
	{
	  std::cerr << "DEBUG: diff: " << diff << std::endl;
	  pass = false;
	}
      }

      delete[] in;
    }
    catch( ... )
    {
      delete[] in;
      throw;
    }

    if (!pass)
    {
        cerr << "FAIL: Downsample Adc -> Proc" << std::endl;
    }

    return pass;
}

int main()
{
    bool pass = true;

    try
    {
        pass = pass && TestResampleAdcData<INT_2S, REAL_4>();
        pass = pass && TestResampleAdcData<INT_4S, REAL_4>();
        pass = pass && TestResampleAdcData<REAL_4, REAL_4>();
        pass = pass && TestResampleAdcData<REAL_8, REAL_8>();
        pass = pass && TestResampleAdcData<COMPLEX_8, COMPLEX_8>();
        pass = pass && TestResampleAdcData<COMPLEX_16, COMPLEX_16>();
    }
    catch(std::exception& e)
    {
        pass = false;
        cerr << e.what() << endl;
    }
    catch(SwigException& e)
    {
        pass = false;
        cerr << e.getResult() << endl;
        cerr << e.getInfo() << endl;
    }
    catch(...)
    {
        pass = false;
        cerr << "Unknown exception" << endl;
    }

    General::AtExit::Cleanup( );
    return (pass == true ? 0 : 1);
}
