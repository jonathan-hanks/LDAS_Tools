#include "../src/config.h"

#include "general/unittest.h"

General::UnitTest	Test;

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Initialize the testing environment
  //---------------------------------------------------------------------

  Test.Init( ArgC, ArgV );

  //---------------------------------------------------------------------
  // Terminate in a way that make check understands.
  //---------------------------------------------------------------------

  Test.Exit( );
}
