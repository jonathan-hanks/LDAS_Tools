/* -*- mode: c++; c-basic-ofset: 2; -*- */
// This file test the normalization of frame times within a frame ilwd.
#include "../src/config.h"

#include <unistd.h>
#include <cstdlib>   

#include <cstdarg>

#include <memory>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include "general/ldasexception.hh"
#include "general/unittest.h"
#include "general/Memory.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/reader.hh"
#include "ilwd/util.hh"

#include "genericAPI/swigexception.hh"

#include "FrameHCmd.hh"
   
// using namespace std;   
using NAMESPACE_GENERAL_MEMORY :: unique_ptr;
   

typedef std::vector<std::string>	ilwd_group_type;

General::UnitTest	Test;

#if 0
#define	 AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl
#else	/* 0 */
#define	AT()
#endif	/* 0 */

enum
{
  TEST_EMPTY,
  TEST_FRAME_HDR_ONLY,
  TEST_FRAME_RAW,
  TEST_FRAME_RAW_POS_SHIFT,
  TEST_FRAME_RAW_POS_SHIFT_METADATA,
  TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT,
  TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT_METADATA,
  TEST_FRAME_RAW_NEG_SHIFT,
  TEST_FRAME_RAW_NEG_SHIFT_METADATA,
  TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT,
  TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT_METADATA,
  TEST_FRAME_REAL_WORLD_1,
  TEST_MAX
};

static ilwd_group_type& init_input_ilwds( );
static ilwd_group_type& init_output_ilwds( );

ilwd_group_type& InputILwds = init_input_ilwds( );
ilwd_group_type& OutputILwds = init_output_ilwds( );

static std::string
create_adc_group( const char* first, ... )
{
  std::string group;

  if ( first )
  {
    group = "  <ilwd name=':adcdata:Container(AdcData)'>";
    group += first;
    const char* next;

    va_list	ap;
    va_start(ap, first );

    while( ( next = va_arg(ap, const char*) ) )
    {
      group += next;
    }

    va_end(ap);

    group += "  </ilwd>";
  }
  return group;
}

static std::string
create_adc( const std::string& Channel,
	    const INT_4U StartSeconds, const INT_4U StartNanoseconds,
	    const INT_4S OffsetSeconds, const INT_4S OffsetNanoseconds,
	    const char* first = (const char*)NULL,  ... )
{
  std::ostringstream	adc;

  adc << "<ilwd name='"
      << Channel
      << "::AdcData:"
      << StartSeconds << ":" << StartNanoseconds << ":Frame'";

  if ( first )
  {
    const char* next;

    adc << "metadata='" << first;
    va_list	ap;
    va_start(ap, first);

    while( ( next = va_arg(ap,const char*) ) )
    {
      adc << ":" << next;
    }
    adc << "' ";

    va_end(ap);
  }

  adc << " size='13'>"
      << "  <lstring name='comment'></lstring>"
      << "  <int_4u name='channelgroup'>5</int_4u>"
      << "  <int_4u name='channelnumber'>265</int_4u>"
      << "  <int_4u name='nBits'>32</int_4u>"
      << "  <real_4 name='bias'>0.0000000e+00</real_4>"
      << "  <real_4 name='slope'>6.1028000e-05</real_4>"
      << "  <lstring name='units' size='5'>volts</lstring>"
      << "  <real_8 name='sampleRate'>1.0000000000000000e+00</real_8>"
      << "  <int_4s dims='2' name='timeOffset'>"
      << OffsetSeconds << " " << OffsetNanoseconds << "</int_4s>"
      << "  <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
      << "  <int_2u name='dataValid'>0</int_2u>"
      << "  <real_8 name='dt'>1.0000000000000000e+00</real_8>"
      << "  <ilwd name=':data:Container(Vect):Frame'>"
      << "    <real_4 dims='1' name='a' units='count'>7.2930084e+01</real_4>"
      << "  </ilwd>"
      << "</ilwd>"
    ;

  return adc.str();
}

static std::string
create_frame( INT_4U StartSeconds, INT_4U StartNanoseconds,
	      REAL_8 Delta,
	      const char* first, ... )
{
  std::list<const char*>	elements;

  if ( first )
  {
    elements.push_back( first );
    const char* next;

    va_list	ap;
    va_start(ap, first );

    while( ( next = va_arg(ap, const char*) ) )
    {
      elements.push_back( next );
    }

    va_end(ap);
  }

  std::ostringstream frame;

  frame
    << "<ilwd name='LIGO::Frame' size='" << elements.size() + 7 << "'>"
    << "  <int_4s name='run'>331</int_4s>"
    << "  <int_4u name='frame'>8586</int_4u>"
    << "  <int_4u name='dataquality'>0</int_4u>"
    << "  <int_4u dims='2' name='GTime'>"
    << StartSeconds << " " << StartNanoseconds << "</int_4u>"
    << "  <int_2u name='ULeapS'>32</int_2u>"
    << "  <real_8 name='dt'>" << Delta << "</real_8>"
    << "  <int_4s name='localTime'>-25200</int_4s>"
    ;
  for ( std::list<const char*>::const_iterator e = elements.begin();
	e != elements.end();
	e++ )
  {
    frame << (*e);
  }
  frame << "</ilwd>";

  return frame.str();
}

std::string
create_metadata_delta_time( REAL_8 Delta )
{
  std::ostringstream	delta;

  delta << "delta_t=" << Delta;

  return delta.str();
}

std::string
create_metadata_start( INT_4U Seconds, INT_4U Nanoseconds )
{
  std::ostringstream	start_time;

  start_time << "start_time={" << Seconds << " " << Nanoseconds << "}";

  return start_time.str();
}

static std::string
create_raw_group( const char* first, ... )
{
  std::string group;

  if ( first )
  {
    group = "<ilwd name='rawdata:rawData:RawData:Frame'>";
    group += first;
    const char* next;

    va_list	ap;
    va_start(ap, first );

    while( ( next = va_arg(ap, const char*) ) )
    {
      group += next;
    }

    va_end(ap);

    group += "  </ilwd>";
  }
  return group;
}

ILwd::LdasElement*
read_ilwd( const std::string& ILwdString )
{
  std::istringstream	ss( ILwdString );

  ILwd::Reader		r( ss );
  ILwd::readHeader( ss );

  return ILwd::LdasElement::createElement( r );
}

void
test( const char* TestName, unsigned int TestNum )
try {
  if ( InputILwds[ TestNum ].length() <= 0 )
  {
    throw std::logic_error( "No data associated with input" );
  }
  if ( OutputILwds[ TestNum ].length() <= 0 )
  {
    throw std::logic_error( "No data associated with output" );
  }
  AT();
  unique_ptr< ILwd::LdasContainer >
    out_ilwd( dynamic_cast< ILwd::LdasContainer* >
	      ( read_ilwd( OutputILwds[ TestNum ] ) ) );
  AT();
  unique_ptr< ILwd::LdasContainer >
    in_ilwd( dynamic_cast< ILwd::LdasContainer* >
	     ( read_ilwd( InputILwds[ TestNum ] ) ) );

  AT();
  normalizeFrameTime( in_ilwd.get( ) );

  AT();
  bool pass( *in_ilwd == *out_ilwd );

  AT();
  Test.Check( pass ) << TestName << std::endl;

  if ( ! pass )
  {
    Test.Message( ) << std::endl << "  *** Expected ***" << std::endl;
    out_ilwd->write(2, 2, Test.Message( false ), ILwd::ASCII );
    Test.Message( false ) << std::endl;
    Test.Message( false ) << std::endl << "  *** Generated ***" << std::endl;
    in_ilwd->write(2, 2, Test.Message( false ), ILwd::ASCII );
    Test.Message( false ) << std::endl;
    if ( Test.IsVerbose( 5 ) )
    {
      char	fn[2][16] = { "/tmp/diffXXXXXX", "/tmp/diffXXXXXX" };

      std::ofstream in( fn[0] );
      in_ilwd->write(2, 2, in, ILwd::ASCII );
      in.close();

      std::ofstream out( fn[1] );
      out_ilwd->write(2, 2, out, ILwd::ASCII );
      out.close();

      std::string cmd_string = "diff ";
      cmd_string += fn[0];
      cmd_string += " ";
      cmd_string += fn[1];

      char line[256];

      Test.Message() << std::endl << "   *** Difference" << std::endl;
      FILE* cmd( popen( cmd_string.c_str(), "r" ) );
      while ( fgets(line, sizeof(line) - 1, cmd ) )
      {
	Test.Message( false ) << line << std::endl;
      }
      
      fclose( cmd );

      unlink( fn[0] );
      unlink( fn[1] );
    }
  }
}
catch( const LdasException& exc )
{
  Test.Check(false)
    << TestName << " : "
    << "LdasException: " << std::endl;
  for ( size_t x = 0; x < exc.getSize( ); x++ )
  {
    Test.Message( false ) << "  " << exc.getError( x ).getMessage()
			  << " (" << exc.getError( x ).getInfo( ) << ")"
			  << std::endl;
  }
}      
catch ( const SwigException& e )
{
  Test.Check(false)
    << TestName << " : "
    << "SwigException: " << e.getResult()
    << " Info: " << e.getInfo() << std::endl;
}
catch( const std::exception& e )
{
  Test.Check( false )
    << TestName << ": "
    << "Caught exception: " << e.what() << std::endl;
}
catch( ... )
{
  Test.Check( false )
    << TestName << ": "
    << "Caught unknown exception" << std::endl;
}

int
main( int ArgC, char** ArgV )
{
  Test.Init( ArgC, ArgV );

  struct {
    const char*		test_name;
    unsigned int	test;
  } cases[] = {
    // { "Empty", TEST_EMPTY, TEST_EMPTY },	// :TODO: Fix code
    { "Only have FrameH", TEST_FRAME_HDR_ONLY },
    { "Raw Frame", TEST_FRAME_RAW  },
    { "Positive shift of ADC by 1 second", TEST_FRAME_RAW_POS_SHIFT },
    { "Positive fractional shift of ADC by 1.5 second",
      TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT },
    { "Negative shift of ADC by 1 second", TEST_FRAME_RAW_NEG_SHIFT },
    { "Negative fractional shift of ADC by 1.5 second",
      TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT },
    { "Positive shift of ADC by 1 second (via metadata)",
      TEST_FRAME_RAW_POS_SHIFT_METADATA },
    { "Positive fractional shift of ADC by 1.5 second (via metadata)",
      TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT_METADATA },
    { "Negative shift of ADC by 1 second (via metadata)",
      TEST_FRAME_RAW_NEG_SHIFT_METADATA },
    { "Negative shift of ADC by 1.5 second (via metadata)",
      TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT_METADATA },
    { "Real World test #1",
      TEST_FRAME_REAL_WORLD_1 },
  };
      
  for ( unsigned int x = 0; x < sizeof( cases ) / sizeof( *cases ); x++ )
  {
    test( cases[ x ].test_name, cases[ x ].test );
  }

  
  Test.Exit();
}

static ilwd_group_type&
init_input_ilwds( )
{
  static ilwd_group_type		input( TEST_MAX );

  input[ TEST_EMPTY ] =
    "<ilwd size='0'></ilwd>";

  //---------------------------------------------------------------------

  input[ TEST_FRAME_HDR_ONLY ] =
    create_frame
    ( 663711990,0,1.0,
      (const char*)NULL) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW ] =
    create_frame
    ( 663711990,0,1.0,
      create_raw_group
      ( create_adc_group
	( create_adc("H2\\:LSC-AS_Q",663711990,0,0,0).c_str(), // adc
	  (const char*)NULL).c_str(), // adc_group
	(const char*)NULL).c_str(), // raw_group
      (const char*)NULL) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_POS_SHIFT ] =
    create_frame
    ( 663711990,0,1.0,
      create_raw_group
      ( create_adc_group
	( create_adc("H2\\:LSC-AS_Q",663711991,0,0,0).c_str(), // adc
	  (const char*)NULL).c_str(), // adc_group
	(const char*)NULL).c_str(), // raw_group
      (const char*)NULL) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_POS_SHIFT_METADATA ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711990,0,0,0,
		    create_metadata_start(663711991,0).c_str(),
		    create_metadata_delta_time(1.0).c_str(),
		    (const char*)NULL ).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711991,500000000,0,0).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT_METADATA ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711990,500000000,0,0).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT_METADATA ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711990,0,0,0,
		    create_metadata_start(663711991,500000000).c_str(),
		    create_metadata_delta_time(1.0).c_str(),
		    (const char*)NULL ).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_NEG_SHIFT ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711990,0,-1,0).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_NEG_SHIFT_METADATA ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711990,0,0,0,
		    create_metadata_start(663711989,0).c_str(),
		    create_metadata_delta_time(1.0).c_str(),
		    (const char*)NULL).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711990,0,-1,500000000).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT_METADATA ] =
    create_frame
    (663711990,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711990,0,0,0,
		    create_metadata_start(663711988,500000000).c_str(),
		    create_metadata_delta_time(1.0).c_str(),
		    (const char*)NULL).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  input[ TEST_FRAME_REAL_WORLD_1 ] =
"<ilwd name='LIGO::Frame' size='10'>"
"    <int_4s name='run'>1168</int_4s>"
"    <int_4u name='frame'>9746</int_4u>"
"    <int_4u name='dataQuality'>0</int_4u>"
"    <int_4u dims='2' name='GTime'>693960000 0</int_4u>"
"    <int_2u name='ULeapS'>32</int_2u>"
"    <real_8 name='dt'>1.6000000000000000e+01</real_8>"
"    <ilwd name=':detectProc:Container(Detector):Frame'>"
"        <ilwd name='LIGO_1:detectProc:Detector:Frame' size='12'>"
"            <real_8 name='longitude'>4.6450157138887384e+01</real_8>"
"            <real_8 name='latitude'>-1.1859234286096360e+02</real_8>"
"            <real_4 name='elevation'>1.4255400e+02</real_4>"
"            <real_4 name='armXazimuth'>1.2599938e+02</real_4>"
"            <real_4 name='armYazimuth'>2.1599942e+02</real_4>"
"            <real_4 name='armXaltitude'>0.0000000e+00</real_4>"
"            <real_4 name='armYaltitude'>0.0000000e+00</real_4>"
"            <real_4 name='armXmidpoint'>0.0000000e+00</real_4>"
"            <real_4 name='armYmidpoint'>0.0000000e+00</real_4>"
"            <int_4s name='localTime'>0</int_4s>"
"            <int_4u name='dataQuality'>0</int_4u>"
"            <lstring name='qaBitList'></lstring>"
"        </ilwd>"
"    </ilwd>"
"    <ilwd name=':history:Container(History):Frame'>"
"        <ilwd name='::History:Frame' size='2'>"
"            <int_4u name='time'>0</int_4u>"
"            <lstring name='comment' size='32'>framebuilder, framecpp-0.0.23pre</lstring>"
"        </ilwd>"
"    </ilwd>"
"    <ilwd name='rawdata:rawData:RawData:Frame'>"
"        <ilwd name=':adcdata:Container(AdcData)' size='2'>"
"            <ilwd name='H2\\:SUS-MC1_COIL_LL::AdcData:693960000:0:Frame' size='14'>"
"                <lstring name='comment'></lstring>"
"                <int_4u name='channelGroup'>4</int_4u>"
"                <int_4u name='channelNumber'>1</int_4u>"
"                <int_4u name='nBits'>16</int_4u>"
"                <real_4 name='bias'>0.0000000e+00</real_4>"
"                <real_4 name='slope'>6.1028000e-05</real_4>"
"                <lstring name='units' size='1'>V</lstring>"
"                <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"                <int_4s dims='2' name='timeOffset'>0 0</int_4s>"
"                <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"                <real_4 name='phase'>0.0000000e+00</real_4>"
"                <int_2u name='dataValid'>0</int_2u>"
"                <real_8 name='dt'>1.6000000000000000e+01</real_8>"
"                <ilwd name=':data:Container(Vect):Frame'>"
"                    <int_2s dataValueUnit='counts' dims='1' dx='0.000488281' name='H2\\:SUS-MC1_COIL_LL' units='time'>-856</int_2s>"
"                </ilwd>"
"            </ilwd>"
"            <ilwd name='H2\\:SUS-MMT2_COIL_UL::AdcData:693960000:0:Frame' size='14'>"
"                <lstring name='comment'></lstring>"
"                <int_4u name='channelGroup'>4</int_4u>"
"                <int_4u name='channelNumber'>38</int_4u>"
"                <int_4u name='nBits'>16</int_4u>"
"                <real_4 name='bias'>0.0000000e+00</real_4>"
"                <real_4 name='slope'>6.1028000e-05</real_4>"
"                <lstring name='units' size='1'>V</lstring>"
"                <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"                <int_4s dims='2' name='timeOffset'>0 0</int_4s>"
"                <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"                <real_4 name='phase'>0.0000000e+00</real_4>"
"                <int_2u name='dataValid'>0</int_2u>"
"                <real_8 name='dt'>1.6000000000000000e+01</real_8>"
"                <ilwd name=':data:Container(Vect):Frame'>"
"                    <int_2s dataValueUnit='counts' dims='1' dx='0.000488281' name='H2\\:SUS-MMT2_COIL_UL' units='time'>-401</int_2s>"
"                </ilwd>"
"            </ilwd>"
"        </ilwd>"
"    </ilwd>"
"    <ilwd name=':procdata:Container(ProcData)' size='2'>"
"        <ilwd name='resample(H2\\:SUS-MC1_COIL_UL,1,2)::ProcData:693960000:9765625:Frame' size='6'>"
"            <lstring name='comment' size='19'>1:2 in the frameAPI</lstring>"
"            <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"            <int_4u dims='2' name='timeOffset'>0 0</int_4u>"
"            <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"            <real_4 name='phase'>0.0000000e+00</real_4>"
"            <ilwd name=':data:Container(Vect):Frame'>"
"                <real_4 dataValueUnit='counts' dims='1' dx='0.000976562' name='data' units='time'>1.2599790e-16</real_4>"
"            </ilwd>"
"        </ilwd>"
"        <ilwd name='resample(H2\\:LSC-AS_Q,1,8)::ProcData:693960000:4882813:Frame' size='6'>"
"            <lstring name='comment' size='19'>1:8 in the frameAPI</lstring>"
"            <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"            <int_4u dims='2' name='timeOffset'>0 0</int_4u>"
"            <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"            <real_4 name='phase'>0.0000000e+00</real_4>"
"            <ilwd name=':data:Container(Vect):Frame'>"
"                <real_4 dataValueUnit='counts' dims='1' dx='0.000488281' name='data' units='time'>5.7096009e-17</real_4>"
"            </ilwd>"
"        </ilwd>"
"    </ilwd>"
"</ilwd>"
    ;
  //---------------------------------------------------------------------

  return input;
}

static ilwd_group_type&
init_output_ilwds( )
{
  static ilwd_group_type	output( TEST_MAX );

  //---------------------------------------------------------------------

  output[ TEST_EMPTY ] = InputILwds[ TEST_EMPTY ];

  //---------------------------------------------------------------------

  output[ TEST_FRAME_HDR_ONLY ] = InputILwds[ TEST_FRAME_HDR_ONLY ];

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW ] = InputILwds[ TEST_FRAME_RAW ];

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_POS_SHIFT ] =
    create_frame
    (663711991,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711991,0,0,0).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;
  
  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_POS_SHIFT_METADATA ] =
    create_frame
    (663711991,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc
	 ("H2\\:LSC-AS_Q",663711991,0,0,0).c_str(), //adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL) // frame
    ;

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT ] =
    create_frame
    (663711991,0,2.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711991,0,0,500000000).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_POS_FRACTIONAL_SHIFT_METADATA ] =
    create_frame
    (663711991,0,2.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711991,0,0,500000000).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_NEG_SHIFT ] =
    create_frame
    (663711989,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711989,0,0,0).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_NEG_SHIFT_METADATA ] =
    create_frame
    (663711989,0,1.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711989,0,0,0).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT ] =
    create_frame
    (663711988,0,2.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711988,0,0,500000000).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  output[ TEST_FRAME_RAW_NEG_FRACTIONAL_SHIFT_METADATA ] =
    create_frame
    (663711988,0,2.0,
     create_raw_group
     ( create_adc_group
       ( create_adc("H2\\:LSC-AS_Q",663711988,0,0,500000000).c_str(), // adc
	 (const char*)NULL ).c_str(), // adc_group
       (const char*)NULL ).c_str(), // raw_group
     (const char*)NULL ) // frame
    ;

  //---------------------------------------------------------------------

  output[ TEST_FRAME_REAL_WORLD_1 ] =
"  <ilwd name='LIGO::Frame' size='10'>"
"    <int_4s name='run'>1168</int_4s>"
"    <int_4u name='frame'>9746</int_4u>"
"    <int_4u name='dataQuality'>0</int_4u>"
"    <int_4u dims='2' name='GTime'>693960000 0</int_4u>"
"    <int_2u name='ULeapS'>32</int_2u>"
"    <real_8 name='dt'>1.7000000000000000e+01</real_8>"
"    <ilwd name=':detectProc:Container(Detector):Frame'>"
"      <ilwd name='LIGO_1:detectProc:Detector:Frame' size='12'>"
"        <real_8 name='longitude'>4.6450157138887384e+01</real_8>"
"        <real_8 name='latitude'>-1.1859234286096360e+02</real_8>"
"        <real_4 name='elevation'>1.4255400e+02</real_4>"
"        <real_4 name='armXazimuth'>1.2599938e+02</real_4>"
"        <real_4 name='armYazimuth'>2.1599942e+02</real_4>"
"        <real_4 name='armXaltitude'>0.0000000e+00</real_4>"
"        <real_4 name='armYaltitude'>0.0000000e+00</real_4>"
"        <real_4 name='armXmidpoint'>0.0000000e+00</real_4>"
"        <real_4 name='armYmidpoint'>0.0000000e+00</real_4>"
"        <int_4s name='localTime'>0</int_4s>"
"        <int_4u name='dataQuality'>0</int_4u>"
"        <lstring name='qaBitList'></lstring>"
"      </ilwd>"
"    </ilwd>"
"    <ilwd name=':history:Container(History):Frame'>"
"      <ilwd name='::History:Frame' size='2'>"
"        <int_4u name='time'>0</int_4u>"
"        <lstring name='comment' size='32'>framebuilder, framecpp-0.0.23pre</lstring>"
"      </ilwd>"
"    </ilwd>"
"    <ilwd name='rawdata:rawData:RawData:Frame'>"
"      <ilwd name=':adcdata:Container(AdcData)' size='2'>"
"        <ilwd name='H2\\:SUS-MC1_COIL_LL::AdcData:693960000:0:Frame' size='14'>"
"          <lstring name='comment'></lstring>"
"          <int_4u name='channelGroup'>4</int_4u>"
"          <int_4u name='channelNumber'>1</int_4u>"
"          <int_4u name='nBits'>16</int_4u>"
"          <real_4 name='bias'>0.0000000e+00</real_4>"
"          <real_4 name='slope'>6.1028000e-05</real_4>"
"          <lstring name='units' size='1'>V</lstring>"
"          <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"          <int_4s dims='2' name='timeOffset'>0 0</int_4s>"
"          <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"          <real_4 name='phase'>0.0000000e+00</real_4>"
"          <int_2u name='dataValid'>0</int_2u>"
"          <real_8 name='dt'>1.6000000000000000e+01</real_8>"
"          <ilwd name=':data:Container(Vect):Frame'>"
"            <int_2s dataValueUnit='counts' dx='0.000488281' name='H2\\:SUS-MC1_COIL_LL' units='time'>-856</int_2s>"
"          </ilwd>"
"        </ilwd>"
"        <ilwd name='H2\\:SUS-MMT2_COIL_UL::AdcData:693960000:0:Frame' size='14'>"
"          <lstring name='comment'></lstring>"
"          <int_4u name='channelGroup'>4</int_4u>"
"          <int_4u name='channelNumber'>38</int_4u>"
"          <int_4u name='nBits'>16</int_4u>"
"          <real_4 name='bias'>0.0000000e+00</real_4>"
"          <real_4 name='slope'>6.1028000e-05</real_4>"
"          <lstring name='units' size='1'>V</lstring>"
"          <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"          <int_4s dims='2' name='timeOffset'>0 0</int_4s>"
"          <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"          <real_4 name='phase'>0.0000000e+00</real_4>"
"          <int_2u name='dataValid'>0</int_2u>"
"          <real_8 name='dt'>1.6000000000000000e+01</real_8>"
"          <ilwd name=':data:Container(Vect):Frame'>"
"            <int_2s dataValueUnit='counts' dx='0.000488281' name='H2\\:SUS-MMT2_COIL_UL' units='time'>-401</int_2s>"
"          </ilwd>"
"        </ilwd>"
"      </ilwd>"
"    </ilwd>"
"    <ilwd name=':procdata:Container(ProcData)' size='2'>"
"      <ilwd name='resample(H2\\:SUS-MC1_COIL_UL,1,2)::ProcData:693960000:0:Frame' size='6'>"
"        <lstring name='comment' size='19'>1:2 in the frameAPI</lstring>"
"        <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"        <int_4u dims='2' name='timeOffset'>0 9765625</int_4u>"
"        <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"        <real_4 name='phase'>0.0000000e+00</real_4>"
"        <ilwd name=':data:Container(Vect):Frame'>"
"          <real_4 dataValueUnit='counts' dx='0.000976562' name='data' units='time'>1.2599790e-16</real_4>"
"        </ilwd>"
"      </ilwd>"
"      <ilwd name='resample(H2\\:LSC-AS_Q,1,8)::ProcData:693960000:0:Frame' size='6'>"
"        <lstring name='comment' size='19'>1:8 in the frameAPI</lstring>"
"        <real_8 name='sampleRate'>6.250000000000000e-02</real_8>"
"        <int_4u dims='2' name='timeOffset'>0 4882813</int_4u>"
"        <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"        <real_4 name='phase'>0.0000000e+00</real_4>"
"        <ilwd name=':data:Container(Vect):Frame'>"
"          <real_4 dataValueUnit='counts' dx='0.000488281' name='data' units='time'>5.7096009e-17</real_4>"
"        </ilwd>"
"      </ilwd>"
"    </ilwd>"
"  </ilwd>"
    ;
  //---------------------------------------------------------------------
  return output;
}
