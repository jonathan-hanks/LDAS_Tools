#include "../src/config.h"

#include <iostream>
#include <memory>
#include <stdexcept>
   
// LDAS Header Files
#include "general/util.hh"
#include "general/unittest.h"
#include "general/objectregistry.hh"
#include "general/ldasexception.hh"
#include "general/Memory.hh"

#include "genericAPI/util.hh"
#include "genericAPI/registry.hh"
#include "genericAPI/swigexception.hh"
#include "genericAPI/ilwdfile.hh"
   
// ILWD Header Files
#include <ilwd/reader.hh>
#include <ilwd/util.hh>      
   
// FrameAPI Local Header Files
#include "framecmd.hh"
#include "convert.hh"   
   
#include <ospace/stream/tstream.h>   

using namespace ILwd;   
using namespace FrameCPP;   
// using namespace std;  
using NAMESPACE_GENERAL_MEMORY :: unique_ptr;

using std::string;
using std::endl;
using std::ifstream;
using std::runtime_error;   
using std::exception;
   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true    
   
General::UnitTest detectorTest;

static const char* detector2_file( "../../../../api/test/frameAPI/detector2.ilwd" );      
   
static LdasContainer* readILwd( const char* name );   
static void reportError( const string& error_msg );   
   
   
int main( int argc, char** argv )
try   
{
   // Initialize test
   detectorTest.Init( argc, argv );

   std::string file( getenv( "SRCDIR" ) ? getenv( "SRCDIR" ) : "" );
   if ( file.length( ) > 0 )
   {
     file += "/";
   }
   file += detector2_file;
   
   // Read frame with 1 detector
   unique_ptr< LdasContainer > elem( readILwd( file.c_str( ) ) );

   if ( elem.get( ) )
   {
     //------------------------------------------------------------------
     // Insert the appropriate metadata
     //------------------------------------------------------------------
     elem->setMetadata( "FrameCPPVersion",  FrameCPP::GetVersion( ) );
     elem->setMetadata( "DataFormatVersion",
			FrameCPP::GetDataFormatVersion( ) );
     elem->setMetadata( "FrameLibraryMinorVersion",
			FrameCPP::GetDataFormatVersion( ) );

   }
   
   // Extract detector names
   const LdasContainer* detectCont( findContainerType( *elem, "detectProc" ) );   

   const unsigned int detector_num( 2 );   
   detectorTest.Check( detectCont->size() == detector_num )
      << "number of detectors: expected=" << detector_num
      << " read from the file=" << detectCont->size() << endl;            
   
   // Get ILWD detector references
   LdasContainer::const_iterator iter( detectCont->begin() );
   
   const LdasContainer& d1_elem( dynamic_cast< const LdasContainer& >( 
      **iter ) );
   
   ++iter;
   const LdasContainer& d2_elem( dynamic_cast< const LdasContainer& >( 
      **iter ) );   
   
   
   // Convert to FrameCPP::Frame 
   unique_ptr< FrameH > frame( ilwd2frame( elem.get( ) ) );
   // frameRegistry's destructor will take care of newly allocated frame   

   const unsigned int frame_detector_num( frame->RefDetectProc().size() );
   detectorTest.Check( detector_num == frame_detector_num ) 
      << "expected detectorProcNum: " << detector_num 
      << " received: " << frame_detector_num << endl;   
   
   // Extract first detector:
   unique_ptr< FrDetector > d1( getFrameDetectorProc( frame.get( ), "0" ) );
   detectorTest.Check( d1->GetName() == d1_elem.getName( 0 ) ) 
      << "comparing name for detector1: ilwd=\"" << d1_elem.getName( 0 )
      << "\" framecpp=\"" << d1->GetName() << "\"" << endl;         
   
   // Extract second detector:
   unique_ptr< FrDetector > d2( getFrameDetectorProc( frame.get( ), "1" ) );
   detectorTest.Check( d2->GetName() == d2_elem.getName( 0 ) ) 
      << "comparing name for detector2: ilwd=\"" << d2_elem.getName( 0 )
      << "\" framecpp=\"" << d2->GetName() << "\"" << endl;            
   
   
   LdasContainer* elem_back( fullFrame2container( frame.get( ) ) );
   if ( ( *elem != *elem_back ) && detectorTest.IsVerbose( 20 ) )
   {
     detectorTest.Message( ) << "ILwd conversion failure:" << std::endl;
     elem->write( 2, 2, detectorTest.Message( false ), ILwd::ASCII );
     detectorTest.Message( false ) << std::endl << "!=" << std::endl;
     elem_back->write( 2, 2, detectorTest.Message( false ), ILwd::ASCII );
     detectorTest.Message( false ) << std::endl;
   }
   detectorTest.Check( *elem == *elem_back ) 
      << "comparing original ILWD and ilwd-->frame-->ilwd conversion" << endl;      

   Registry::elementRegistry.reset();                
   
   detectorTest.Exit();
   return 0;
}
catch( const exception& e )
{
   reportError( e.what() );
}
catch( const LdasException& exc )
{
   reportError( SwigException( exc ).getResult() );
}      
catch( SwigException& e )
{
   reportError( e.getResult() );
}       
catch( ... )
{
   reportError( "unknown exception" );
}

   
//------------------------------------------------------------------------------
//   
//: Reads ILWD format LdasContainer element from the file.
// 
//!param: const char* name - File name.
//
//!return: LdasContainer* - A pointer to the ILWD format container.
//   
LdasContainer* readILwd( const char* name )
{
   ifstream ilwdStream( name );
   
   if( !ilwdStream )
   {
      string msg( "File does not exist: " );
      msg += name;
      throw runtime_error( msg );   
   }
   
   // Read file header
   if( !ILwd::readHeader( ilwdStream ) )   
   {
      string msg( "Not ILWD format input file: " );
      msg += name;
      throw runtime_error( msg );
   }
   
   ILwd::Reader r( ilwdStream );
   r.skipWhiteSpace();   

   LdasElement* elem( LdasElement::createElement( r ) );
   
   ilwdStream.close();
   
   if( elem == 0 || elem->getElementId() != ID_ILWD )
   {
      string msg( "Malformed ILWD input file: " );
      msg += name;
      throw runtime_error( msg );   
   }
   Registry::elementRegistry.registerObject( elem );
   return dynamic_cast< LdasContainer* >( elem );   
}

   
void reportError( const string& error_msg )
{
   // cleanup memory
   Registry::elementRegistry.reset();                   
   
   detectorTest.Check( false ) << error_msg << endl;
   detectorTest.Exit();      

   return;
}
