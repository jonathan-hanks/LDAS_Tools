#include "../src/config.h"

#include <unistd.h>

#include <list>
#include <string>

#include "general/ldasexception.hh"
#include "general/unittest.h"

#include "ilwd/ldascontainer.hh"

#include "CreateFrame.hh"
#include "createFrameGroupCommon.hh"
#include "frframe.hh"

int main( int ArgC, char** ArgV );

using namespace FrameAPI;

#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

General::UnitTest	Test;

static INT_4U NumberOfCycles( 1 );

int
main( int ArgC, char** ArgV )
try
{
  Test.Init( ArgC, ArgV );

  AT();
  init_tests( );
  for( INT_4U k = 0; k < NumberOfCycles; k++ )
  {
    int	test_number( 0 );
    AT();
    for ( std::list< test_data_type >::const_iterator t( test_data.begin( ) );
	  t != test_data.end( );
	  t++, test_number++ )
    {
      bool	bad_file = false;
      AT();
      Test.Message( ) << "Doing test number: " << test_number << std::endl;
      for ( ConditionData::frame_files_type::const_iterator
	      f( (*t).s_files.begin( ) );
	    ( bad_file == false) && ( f != (*t).s_files.end( ) );
	    f++ )
      {
	if ( access( (*f).c_str( ), R_OK ) != 0 )
	{
	  Test.Message( )
	    << "Warning: Could not access file: " << *f
	    << ". Test skipped"
	    << std::endl;
	  bad_file = true;
	  break;
	}
      }
      if ( bad_file == true )
      {
	continue;
      }
	
      typedef std::list< FrameCPP::FrameH* >	frames_type;

      CreateFrame	cf( (*t).s_files, /* Frame Files */
			    (*t).s_channels, /* Channels */
			    false /* Concatination */ );
      bool		pass( true );

      AT();
      frames_type	frames;

      AT();
      cf.Eval( frames );
      for( frames_type::const_iterator i( frames.begin( ) );
	   i != frames.end( );
	   i++ )
      {
	try
	{
	  AT();
	  bool	whole_frame( ( Test.IsVerbose( 30 ) ) ? true : false );
	  ILwd::LdasContainer*	c( frame2container( *(*i),
						    whole_frame, /* wholeFrame */
						    false /* daqFrame */ ) );
	  AT();
	  Test.Check( c != (ILwd::LdasContainer*)NULL )
	    << "Generated ILwd" << std::endl;
	  AT();
	  if ( c && Test.IsVerbose( 20 ) )
	  {
	    AT();
	    Test.Message( ) << "Generated: " << std::endl;
	    c->write( 2, 2, Test.Message( false ), ILwd::ASCII );
	    Test.Message( false ) << std::endl;
	  }
	  AT();
	  delete c;
	}
	catch( ... )
	{
	  pass = false;
	}
	
	AT();
	Test.Check( ( pass ) && ( frames.size( ) > 0 ) )
	  << "Generated frames"
	  << std::endl;
      } // for

      for( frames_type::const_iterator fi( frames.begin( ) );
	   fi != frames.end( );
	   fi++ )
      {
	delete *fi;
      }
					  
    }
  }
  AT();
  Test.Exit( );
}
catch( const SwigException& e )
{
  Test.Check( false ) << "Caught exception: " << e.getResult( ) << e.getInfo( ) << std::endl;
  Test.Exit( );
}
catch( const std::exception& e )
{
  Test.Check( false ) << "Caught exception: " << e.what( ) << std::endl;
  Test.Exit( );
}
catch( ... )
{
  Test.Check( false ) << "Caught unknown exception" << std::endl;
  Test.Exit( );
}
