#include "../src/config.h"

#include <unistd.h>

#include <stdexcept>

// LDAS Header Files
#include "general/unittest.h"
#include "general/AtExit.hh"

#include "genericAPI/RegistryILwd.hh"

#include "framecmd.hh"

General::UnitTest Test;

//-----------------------------------------------------------------------
// Forward references to tests of specific problem reports.
//-----------------------------------------------------------------------
static void pr3114( );

int
main( int argc, char** argv )
{
  //---------------------------------------------------------------------
  // Initialize test
  //---------------------------------------------------------------------
  Test.Init( argc, argv );

  try   
  {
    pr3114( );
  }
  catch( ... )
  {
    try
    {
      throw;
    }
    catch( const std::exception& e )
    {
      Test.Check( false ) << e.what() << std::endl;;
    }
    catch( const LdasException& exc )
    {
      Test.Check( false ) << SwigException( exc ).getResult() << std::endl;
    }      
    catch( SwigException& e )
    {
      Test.Check( false ) << e.getResult() << std::endl;
    }       
    catch( ... )
    {
      Test.Check( false ) << "unknown exception" << std::endl;
    }
  }

  //---------------------------------------------------------------------
  // Prepare for departure
  //---------------------------------------------------------------------
  Registry::elementRegistry.reset();	// Cleanup memory
   
  General::AtExit::Cleanup( );
  Test.Exit();
  return 1;
}

//-----------------------------------------------------------------------
// Formal declarations of regression tests for specific problem reports
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
//: Running getChannelListFromFrameFile() generates a core file.
//
// This function tests the code by using the frame name(s) and
// associated parameters that have been determined to be problematic
//-----------------------------------------------------------------------

void
pr3114( )
{
  static struct TestInfoType {
    const char* s_filename;
    const int	s_meta_info;
    const int	s_channel_flag;
  } TestInfo[] = {
    { "/scratch/test/frames/S2/MERGED/HL-RDS_MERGED_L1-730524992-16.gwf",
      META_INFO_ALL | EXTENDED_META_INFO_ALL,
      CHANNEL_ADC | CHANNEL_PROC | CHANNEL_SERIAL | CHANNEL_SIM }
  };

  for ( TestInfoType
	  *cur = &( TestInfo[ 0 ] ),
	  *last = &( TestInfo[ sizeof( TestInfo ) / sizeof( *TestInfo ) ] );
	cur != last;
	++cur )
  {
    if ( access( cur->s_filename, R_OK ) != 0 )
    {
      Test.Message( 20 ) << "INFO: Unable to access file "
			 << cur->s_filename
			 << " (SKIPPING)"
			 << std::endl;
      continue;
    }
    std::string clist = 
      getChannelListFromFrameFile( cur->s_filename,
				   cur->s_meta_info,
				   cur->s_channel_flag );
    Test.Message( 30 ) << "INFO: Frame: " << cur->s_filename
		       << " List of Channels: " << clist
		       << std::endl;
				   
  }
}
