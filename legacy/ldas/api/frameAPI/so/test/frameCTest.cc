//-----------------------------------------------------------------------
// Test the tcl interface to createFrameGroup command
//-----------------------------------------------------------------------
#include "../src/config.h"

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#include <sstream>

#include <string>

#include "general/unittest.h"
#include "general/AtExit.hh"

#include "ilwd/ldascontainer.hh"

#include "createFrameGroupCommon.hh"
#include "framecmd.hh"


#define	LM_DEBUG 0
#define LM_SUPPORT_THREADED 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

General::UnitTest	Test;

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Initialize the testing environment
  //---------------------------------------------------------------------

  Test.Init( ArgC, ArgV );

  //---------------------------------------------------------------------
  // evaluate some commands
  //---------------------------------------------------------------------

#if LM_SUPPORT_THREADED
  bool threaded( false );
#endif /* LM_SUPPORT_THREADED */

  for ( int x = 1; x < ArgC; x++ )
  {
    if ( *ArgV[ x ] == '-' )
    {
      if ( strcmp( "-t", ArgV[ x ] ) == 0 )
      {
#if LM_SUPPORT_THREADED
	threaded = true;
#endif /* LM_SUPPORT_THREADED */
	continue;
      }
    }
  }
  if ( ArgC == 1 )
  {
    init_tests( );

    int	test_count( 0 );

    for ( std::list< test_data_type >::const_iterator t( test_data.begin( ) );
	  t != test_data.end( );
	  t++ )
    {
      bool		bad_file = false;

      Test.Message( ) << "Performing test: " << test_count++ << std::endl;
      for ( ConditionData::frame_files_type::const_iterator
	      f( (*t).s_files.begin( ) );
	    ( bad_file == false) && ( f != (*t).s_files.end( ) );
	    f++ )
      {
	if ( access( (*f).c_str( ), R_OK ) != 0 )
	{
	  Test.Message( )
	    << "Warning: Could not access file: " << *f
	    << ". Test skipped"
	    << std::endl;
	  bad_file = true;
	  break;
	}
	if ( bad_file )
	{
	  continue;
	}
	Test.Message( ) << "Working wih file: : " << *f << std::endl;
	try
	{
	  AT( );
	  FrameFile*		ffile( openFrameFile( (*f).c_str( ), "r" ) );
	  FrameCPP::FrameH*	frame( readFrame( ffile ) );

	  AT( );
	  ILwd::LdasContainer*	frame_ilwd( fullFrame2container( frame ) );

	  AT( );
	  closeFrameFile( ffile );

	  AT( );
	  destructFrame( frame );

	  AT( );
	  FrameCPP::FrameH*	new_frame( ilwd2frame( frame_ilwd ) );

	  AT( );
	  destructElement( frame_ilwd );

	  AT( );
	  FrameFile*		offile( openFrameFile( "out.gwf",  "w" ) );

	  AT( );
	  writeFrame( offile, new_frame);

	  AT( );
	  destructFrame( new_frame );

	  AT( );
	  closeFrameFile( offile );
	}
	catch( const std::exception& e )
	{
	  Test.Check( false ) << "FAIL: Caught exception: " << e.what( )
			      << std::endl;
	}
	catch( ... )
	{
	  Test.Check( false ) << "FAIL: Caught unknown exception"
			      << std::endl;
	}
      }

    }
  }

  //---------------------------------------------------------------------
  // Cleanup
  //---------------------------------------------------------------------
  General::AtExit::Cleanup( );
  //---------------------------------------------------------------------
  // Terminate in a way that make check understands.
  //---------------------------------------------------------------------

  Test.Exit( );
}
