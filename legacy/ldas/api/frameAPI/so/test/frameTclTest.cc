//-----------------------------------------------------------------------
// Test the tcl interface to createFrameGroup command
//-----------------------------------------------------------------------

#include "../src/config.h"

#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#include <sstream>

#include <string>

#include <tcl.h>

#include "general/unittest.h"
#include "general/AtExit.hh"

#include "createFrameGroupCommon.hh"


General::UnitTest	Test;

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Initialize
  //---------------------------------------------------------------------
  bool verbose( false );
  const std::string verbose_env_flag( ( getenv( "TEST_VERBOSE_MODE" ) )
				      ? getenv( "TEST_VERBOSE_MODE" )
				      : "false"
				      );

  if ( verbose_env_flag == "true" )
  {
    verbose = true;
  }

  //---------------------------------------------------------------------
  // Make sure that some environment variables exist.
  //---------------------------------------------------------------------

  assert( getenv( "LDAS_TOP_BUILD_DIR" ) );

  std::string ldas_top_build_dir( getenv("LDAS_TOP_BUILD_DIR") );

  std::string	generic_api( ldas_top_build_dir +
			     "/api/genericAPI/so/src/.libs/libgenericAPI.so" );
  std::string	frame_api( ldas_top_build_dir +
			   "/api/frameAPI/so/src/.libs/libframeAPI.so" );

  //---------------------------------------------------------------------
  // Allocate the interpreter
  //---------------------------------------------------------------------

  Tcl_Interp* interp( Tcl_CreateInterp() );

  std::string	tcl_cmd;

  //---------------------------------------------------------------------
  // Load some basic modules
  //---------------------------------------------------------------------

  tcl_cmd = "load ";
  tcl_cmd += generic_api;
  tcl_cmd += "\n";
  if ( Tcl_GlobalEval( interp,
		       const_cast<char*>( tcl_cmd.c_str( ) ) ) == TCL_ERROR )
  {
    std::cerr << "Unable to load "
	      << generic_api
	      << " into TCL interpreter"
	      << std::endl;
    exit(1);
  }
  tcl_cmd = "load ";
  tcl_cmd += frame_api;
  tcl_cmd += "\n";
  if ( Tcl_GlobalEval( interp,
		       const_cast<char*>( tcl_cmd.c_str( ) ) ) == TCL_ERROR )
  {
    std::cerr << "Unable to load "
	      << frame_api
	      << " into TCL interpreter"
	      << std::endl;
    exit(1);
  }

  //---------------------------------------------------------------------
  // evaluate some commands
  //---------------------------------------------------------------------

  bool threaded( false );

  for ( int x = 1; x < ArgC; x++ )
  {
    if ( *ArgV[ x ] == '-' )
    {
      if ( strcmp( "-t", ArgV[ x ] ) == 0 )
      {
	threaded = true;
	continue;
      }
    }

    std::ostringstream	cmd;

    if ( threaded )
    {
      cmd << "set tid [ createFrameGroup_t " << ArgV[ x ] << " ]" << std::endl
	  << "set ilwd [ createFrameGroup_r $tid ]" << std::endl;
      if (verbose )
      {
	cmd << "puts \"Created(In Thread): $ilwd\"" << std::endl;
      }
    }
    else
    {
      cmd << "set ilwd [ createFrameGroup " << ArgV[ x ] << " ]" << std::endl;
      if ( verbose )
      {
	cmd << "puts \"Created (In main process): $ilwd\"" << std::endl;
      }
    }
    if ( verbose )
    {
      cmd << "puts [ getElement $ilwd ]" << std::endl;
    }
    
    if ( Tcl_GlobalEval( interp,
			 const_cast<char*>( cmd.str( ).c_str( ) ) )
	 == TCL_ERROR )
    {
      std::cerr << "Unable to evaluate: "
		<< cmd.str( )
		<< std::endl
		<< "\t" << Tcl_GetStringResult( interp )
		<< std::endl;
    }
  }
  if ( ArgC == 1 )
  {
    init_tests( );

    std::ostringstream	cmd;

    for ( std::list< test_data_type >::const_iterator t( test_data.begin( ) );
	  t != test_data.end( );
	  t++ )
    {
      bool		bad_file = false;

      for ( ConditionData::frame_files_type::const_iterator
	      f( (*t).s_files.begin( ) );
	    ( bad_file == false) && ( f != (*t).s_files.end( ) );
	    f++ )
      {
	if ( access( (*f).c_str( ), R_OK ) != 0 )
	{
	  Test.Message( )
	    << "Warning: Could not access file: " << *f
	    << ". Test skipped"
	    << std::endl;
	  bad_file = true;
	  break;
	}
	if ( bad_file )
	{
	  continue;
	}
	cmd << "set ffile [ openFrameFile " << *f << " r ]" << std::endl;
	if ( verbose )
	{
	  cmd << "puts \"DEBUG: ffile - $ffile\"" << std::endl;
	}
	cmd << "set frame [ readFrame $ffile ]" << std::endl;
	if ( verbose )
	{
	  cmd << "puts \"DEBUG: frame - $frame\"" << std::endl;
	}
	cmd << "set frame_ilwd [ fullFrame2container $frame ]" << std::endl;
	if ( verbose )
	{
	  cmd << "puts \"DEBUG: frame_ilwd - $frame_ilwd\"" << std::endl;
	}
	cmd << "destructFrame $frame" << std::endl
	    << "set new_frame [ ilwd2frame $frame_ilwd ]" << std::endl;
	if ( verbose )
	{
	  cmd << "puts \"DEBUG: new_frame - $new_frame\"" << std::endl;
	}
	cmd << "destructElement $frame_ilwd" << std::endl
	    << "set offile [ openFrameFile out.gwf w ]" << std::endl
	    << "writeFrame $offile $new_frame" << std::endl
	    << "destructFrame $new_frame" << std::endl
	    << "closeFrameFile $offile" << std::endl
	    << "closeFrameFile $ffile" << std::endl
	  ;
	if ( verbose )
	{
	  std::cerr << "DEBUG: cmd: " << cmd.str( ) << std::endl;
	}
	if ( Tcl_GlobalEval( interp,
			     const_cast<char*>( cmd.str( ).c_str( ) ) )
	     == TCL_ERROR )
	{
	  std::cerr << "Unable to evaluate: "
		    << cmd.str( )
		    << std::endl
		    << "\t" << Tcl_GetStringResult( interp )
		    << std::endl;
	}
      }
    }
  }

  tcl_cmd = "exit 0\n";
  if ( Tcl_GlobalEval( interp,
		       const_cast<char*>( tcl_cmd.c_str( ) ) ) == TCL_ERROR )
  {
    std::cerr << "Unable to execute: "
	      << tcl_cmd
	      << std::endl;
    General::AtExit::Cleanup( );
    exit(1);
  }

  //---------------------------------------------------------------------
  // Delete the interpreter
  //---------------------------------------------------------------------

  Tcl_DeleteInterp( interp );
  General::AtExit::Cleanup( );
}
