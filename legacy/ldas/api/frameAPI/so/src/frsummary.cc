/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "config.h"

// System Header Files
#include <sstream>      
   
#include "framecpp/Time.hh"

#include "frsummary.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "util.hh"


using FrameCPP::FrSummary;
using FrameCPP::FrTable;
using FrameCPP::FrVect;

using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
   
using namespace std;   
   
//!ignore_begin:


enum
{
    FR_NAME,
    FR_COMMENT,
    FR_TEST,
    FR_GTIMES,
    FR_GTIMEN,
    FR_MOMENTS,
    FR_TABLE
};


QueryHash initSummaryHash()
{
    QueryHash h;
    h[ "name"    ] = FR_NAME;
    h[ "comment" ] = FR_COMMENT;
    h[ "test"    ] = FR_TEST;
    h[ "gtimes"  ] = FR_GTIMES;
    h[ "gtimen"  ] = FR_GTIMEN;
    h[ "moments" ] = FR_MOMENTS;
    h[ "table"   ] = FR_TABLE;
    return h;
}


static QueryHash summaryHash( initSummaryHash() );    
    
    
//-----------------------------------------------------------------------------
std::string getAttribute( const FrSummary& summary, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        summaryHash.find( q.getName().c_str() );
    if ( iter == summaryHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << summary.GetName();
                break;
                
            case FR_COMMENT:
                res << summary.GetComment();
                break;
                
            case FR_TEST:
                res << summary.GetTest();
                break;
                
            case FR_GTIMES:
                res << summary.GetGTime().getSec();
                break;
            
            case FR_GTIMEN:
                res << summary.GetGTime().getNSec();
                break;

            case FR_MOMENTS:
	        return getAttribute( summary.RefMoments(), dq );

            case FR_TABLE:
                return getAttribute( summary.RefTable(), dq );
                
            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_MOMENTS:
                return getAttribute(
				    getContained( summary.RefMoments(),
						  q, start, index ),
                    dq, start, index + 1 );
                break;

            case FR_TABLE:
                return getAttribute(
				    getContained( summary.RefTable(),
						  q, start, index ),
                    dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}


//-----------------------------------------------------------------------------
LdasElement* getData( const FrSummary& summary, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        return summary2container( summary );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        summaryHash.find( q.getName().c_str() );
    if ( iter == summaryHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_MOMENTS:
            return getData( getContained( summary.RefMoments(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;

        case FR_TABLE:
            return getData( getContained( summary.RefTable(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;
                
        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}
//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrSummary.
//
//!usage_ooi: FrSummary* sum = container2summary( c );
//!param: const LdasContainer& c - FrSummary container.
//!return: FrSummary* sum = FrameCPP::FrSummary newly allocated converted result.
//!exc: SwigException - bad summary container data.
//
//-----------------------------------------------------------------------------
FrSummary* container2summary( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
    if ( strcasecmp( c_name.c_str(), "Summary" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrSummary container." );
    }
    
    const LdasArray< INT_4U >& te = findArrayType< INT_4U >( c, "GTime" );
    FrSummary* sum = new FrSummary(
        c.getName( 0 ),
        findLStringType( c, "comment" ).getString(),
        findLStringType( c, "test" ).getString(),
        FrameCPP::Time( te.getData()[ 0 ], te.getData()[ 1 ] ));
    try
    {
        const LdasContainer* moments( findContainerType( c, "moments" ) );
        if ( moments != 0 )
        {
            for ( unsigned int i = 0; i < moments->size(); ++i )
            {
		FrSummary::moments_type::value_type
		    m( array2vect( dynamic_cast< const LdasArrayBase& >( *(*moments)[ i ] ) ) );

                sum->RefMoments().append( m );
            }
        }

        const LdasContainer* table( findContainerType( c, "table" ) );
        if ( table != 0 )
        {
            for ( unsigned int i = 0; i < table->size(); ++i )
            {
		FrSummary::table_type::value_type
		    t( container2table( dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) ) );

                sum->RefTable().append( t );
            }
        }
    }
    catch( std::bad_cast& )
    {
      delete sum;
      sum = (FrameCPP::FrSummary*)NULL;
      throw SWIGEXCEPTION( "invalid_format" );
    }
    
    return sum;
}


//-----------------------------------------------------------------------------
//
//: Convert FrSummary to ILWD.
//
//!usage_ooi: LdasContainer* sumContainer = summary2container( sum );
//!param: const FrSummary& sum - FrameCPP::FrSummary to convert.
//!return: LdasContainer* sumContainer - newly alllocated container.
//
//-----------------------------------------------------------------------------
LdasContainer* summary2container( const FrSummary& sum )
{
    LdasContainer* c = new LdasContainer( "::Summary:Frame" );
    c->setName( 0, sum.GetName() );
    
    LdasString* comment( new LdasString( sum.GetComment(), "comment" ) );
    LdasString* test( new LdasString( sum.GetTest(), "test" ) );
    INT_4U time[ 2 ] = { sum.GetGTime().getSec(),
                         sum.GetGTime().getNSec() };
    LdasArray< INT_4U >* gTime( new LdasArray< INT_4U >( time, 2, "GTime" ));

    c->push_back( comment,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( test,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( gTime,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

    size_t momentsSize( sum.RefMoments().size() );
    if ( momentsSize > 0 )
    {
        LdasContainer* moments(
            new LdasContainer( ":moments:Container(Vect):Frame" ) );
        c->push_back( moments,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrSummary::const_iterator iter( sum.RefMoments().begin() );
        for( size_t i = momentsSize; i != 0; --i, ++iter )
        {
            moments->push_back( vect2array( **iter ),
				ILwd::LdasContainer::NO_ALLOCATE,
				ILwd::LdasContainer::OWN );
        }
    }

    size_t tableSize( sum.RefTable().size() );
    if ( tableSize > 0 )
    {
        LdasContainer* table(
            new LdasContainer( ":table:Container(Table):Frame" ) );
        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrSummary::const_table_iterator iter( sum.RefTable().begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }
    
    return c;
}


//-----------------------------------------------------------------------------
//
//: Insert FrSummary container into Frame container.
//
//!usage_ooi: insertSummary( fr, s );
//!param: LdasContainer& fr - frame container.
//!param: LdasContainer& s - FrSummary container.
//!exc: SwigException - bad container data.
//-----------------------------------------------------------------------------
void insertSummary( LdasContainer& fr, LdasContainer& s )
{
    LdasElement* t = fr.find( "summaryData", 1 );
    if ( t == 0 )
    {
        fr.push_back( LdasContainer( ":summaryData:Container(Summary)" ) );
        t = fr.back();
    }
    
    if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_frame" );
    }

    dynamic_cast< LdasContainer& >( *t ).push_back( s );
}
