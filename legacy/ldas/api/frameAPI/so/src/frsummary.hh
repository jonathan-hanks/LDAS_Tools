#ifndef FrameApiSummaryHH
#define FrameApiSummaryHH

#include <deque>
#include <string>

#include "framecpp/FrSummary.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"


std::string
getAttribute( const FrameCPP::FrSummary& summary,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD

ILwd::LdasElement*
getData( const FrameCPP::FrSummary& summary,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index );

FrameCPP::FrSummary* container2summary( const ILwd::LdasContainer& c );

ILwd::LdasContainer* summary2container( const FrameCPP::FrSummary& sum );

void insertSummary( ILwd::LdasContainer& fr, ILwd::LdasContainer& s );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiSummaryHH
