#ifndef FRAME_H_CMD_HH
#define FRAME_H_CMD_HH

namespace ILwd
{
  class LdasContainer;
}

//: Normalize the time information in a frame ilwd.
//
// This function ensures that the time information within the ilwd frame
//   object is consistant.
//
//!param: ILwd::LdasContainer* Frame - Pointer to ilwd frame object
void normalizeFrameTime( ILwd::LdasContainer* Frame );

#endif	/* FRAME_H_CMD_HH */
