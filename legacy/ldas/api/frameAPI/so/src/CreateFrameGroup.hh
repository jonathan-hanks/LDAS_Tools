#ifndef FRAME_API__CREATE_FRAME_GROUP_HH
#define FRAME_API__CREATE_FRAME_GROUP_HH

#include "ConditionData.hh"

namespace FrameAPI
{
  class CreateFrameGroup: public ConditionData
  {
  public:
    //-------------------------------------------------------------------
    //: Constructor
    //
    // This constructor initializes the class with a list of frame files
    //   and a list of channel request. Data validation of the input
    //   parameters is performed.
    //!param: const frame_files_type& FrameFiles - a list of fully
    //+		qualified frame file names.
    //!param: const std::list< channel_input_type>& Channels - List of
    //+		channels as described by struct channel_input_type
    //-------------------------------------------------------------------
    CreateFrameGroup( const frame_files_type& FrameFiles,
		      const std::list< channel_input_type>& Channels );


    //-------------------------------------------------------------------
    //: Generate the frame_group_ilwd
    //-------------------------------------------------------------------
#if HAVE_LDAS_PACKAGE_ILWD
    ILwd::LdasElement* Eval( );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

  private:
#if WORKING
    CreateFrameGroup( );
    CreateFrameGroup( CreateFrameGroup& );
#endif /* WORKING */
    const CreateFrameGroup& operator=( CreateFrameGroup& );
  }; // class - CreateFrame

  inline const CreateFrameGroup& CreateFrameGroup::
  operator=( CreateFrameGroup& )
  {
    return *this;
  }

} // namespace - FrameAPI

#endif /* FRAME_API__CREATE_FRAME_GROUP_HH */
