#ifndef FrameApiEventHH
#define FrameApiEventHH

#include <deque>
#include <string>

#include "framecpp/FrEvent.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

std::string
getAttribute( const FrameCPP::FrEvent& event,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrEvent& event,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index,
	 const FrameCPP::Time& gtime,
	 const REAL_8& dt );

FrameCPP::FrEvent* container2event( const ILwd::LdasContainer& c );

ILwd::LdasContainer*
event2container( const FrameCPP::FrEvent& event,
		 const FrameCPP::Time& gtime,
		 const REAL_8& dt );

void
insertEvent( ILwd::LdasContainer& fr,
	     ILwd::LdasContainer& event,
	     const bool validateTime = true );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiEventHH
