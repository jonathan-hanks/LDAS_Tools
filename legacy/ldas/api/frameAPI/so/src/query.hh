#ifndef QueryHH
#define QueryHH

#include <general/regex.hh>
#include <genericAPI/swigexception.hh>


//------------------------------------------------------------------------------
//
//: Query class.
//
// FrameAPI internal usage only. Used to parse and store parsed query.
//
class Query
{
public:
    
    Query( const std::string& name, bool isQuery = false, const std::string& query = "",
           const std::string& value = "" );
    //!exc: SwigException      
    Query( const char* start, const char** final );
    
    //!exc: None.
    const std::string& getName() const;
    //!exc: None.   
    bool isQuery() const;
    //!exc: None.   
    const std::string& getQuery() const;
    //!exc: None.   
    const std::string& getValue() const;
    
private:
    
    std::string mName;
    bool mIsQuery;
    std::string mQuery;
    std::string mValue;

    static const Regex re_item;
    static const Regex re_iquery;
    static const Regex re_query1;
    static const Regex re_query2;    
};


//
//: Constructor.
//
// Assignment and no parsing.
//
inline
Query::Query( const std::string& name,
	      bool isQuery,
	      const std::string& query,
	      const std::string& value )
  : mName( name ), mIsQuery( isQuery ), mQuery( query ), mValue( value )
{
}

//
//: Get name attribute
//
//!exc: None.   
//   
inline const std::string& Query::getName() const 
{
    return mName;
}


//
//: See if this is parsed query
//
//!exc: None.   
//      
inline bool Query::isQuery() const
{
    return mIsQuery;
}


//
//: Get query type
//
//!exc: None.   
//      
inline const std::string& Query::getQuery() const
{
    return mQuery;
}

//
//: Get query value
//
//!exc: None.   
//      
inline const std::string& Query::getValue() const
{
    return mValue;
}


#endif // QueryHH
