#include "ldas_tools_config.h"

#include <memory>

#include <general/autoarray.hh>
#include "framecpp/Dimension.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldasarray.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwdfcs/FrVect.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "frvect.hh"
#include "getdata.hh"
#include "util.hh"


using FrameCPP::FrVect;
using FrameCPP::Dimension;

#if ! CORE_API
#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasArray;
using ILwd::LdasArrayBase;
using ILwd::LdasElement;
using ILwd::LdasContainer;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
#endif /* ! CORE_API */

using namespace std;
   
//!ignore_begin:

#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

#if LM_DEBUG
#define	INFO(a) std::cerr << a << " (" << __FILE__ << " " << __LINE__ << ")"<< std::endl;
#else
#define	INFO(a)
#endif

#if FRAME_SPEC_CURRENT <= 5
#ifdef WORDS_BIGENDIAN
const FrameCPP::FrVect::ByteOrder HOST_ORDER( FrameCPP::FrVect::BIG );
#else	/* WORDS_BIGENDIAN */
const FrameCPP::FrVect::ByteOrder HOST_ORDER( FrameCPP::FrVect::LITTLE );
#endif /* WORDS_BIGENDIAN */
#else
#ifdef WORDS_BIGENDIAN
const FrameCPP::byte_order_type HOST_ORDER( FrameCPP::BYTE_ORDER_BIG_ENDIAN );
#else	/* WORDS_BIGENDIAN */
const FrameCPP::byte_order_type HOST_ORDER( FrameCPP::BYTE_ORDER_LITTLE_ENDIAN );
#endif /* WORDS_BIGENDIAN */
#endif


enum
{
    FR_NAME,
    FR_COMPRESS,
    FR_TYPE,
    FR_NDATA,
    FR_NBYTES,
    FR_NDIM,
    FR_NX,
    FR_DX,
    FR_STARTX,
    FR_UNITX,
    FR_UNITY
};


#if CORE_API

QueryHash initVectHash()
{
    QueryHash h;
    h[ "name"     ] = FR_NAME;
    h[ "compress" ] = FR_COMPRESS;
    h[ "type"     ] = FR_TYPE;
    h[ "ndata"    ] = FR_NDATA;
    h[ "nbytes"   ] = FR_NBYTES;
    h[ "ndim"     ] = FR_NDIM;
    h[ "nx"       ] = FR_NX;
    h[ "dx"       ] = FR_DX;
    h[ "startx"   ] = FR_STARTX;
    h[ "unitx"    ] = FR_UNITX;
    h[ "unity"    ] = FR_UNITY;
    return h;
}


static QueryHash vectHash( initVectHash() );

#endif /* CORE_API */
    
using namespace FrameAPI::FrVect;

//-----------------------------------------------------------------------------

#if ! API_CORE
void FrameAPI::FrVect::
appendStructures( FrameAPI::FrVect::Container& Dest,
		  const FrameAPI::FrVect::Container& Source )
{
  for ( FrameAPI::FrVect::Container::const_iterator
	  i( Source.begin( ) );
	i != Source.end( );
	i++ )
  {
    Dest.append( *i );
  }
}
#endif /* ! API_CORE */

//-----------------------------------------------------------------------------

#if CORE_API
void FrameAPI::FrVect::
copy( void* Dest,
      const FrameAPI::FrVect::Container& Source )
{
  FrameCPP::FrVect::data_type			expanded;
  FrameCPP::FrVect::data_const_pointer_type	src;

  char*	pos( (char*)Dest );

  for ( FrameAPI::FrVect::Container::const_iterator
	  i( Source.begin( ) );
	i != Source.end( );
	i++ )
  {
    FrameCPP::FrVect::nBytes_type
      bytes( FrameCPP::FrVect::GetTypeSize( (*i)->GetType( ) ) *
	     (*i)->GetNData( ) );

    src = (*i)->GetDataUncompressed( expanded );
    memcpy( pos, src, bytes );
    pos += bytes;
  }
}
#endif /* CORE_API */

//-----------------------------------------------------------------------------

#if CORE_API
FrameCPP::FrVect* FrameAPI::FrVect::
concat( const FrVectList& List )
{
  FrameCPP::FrVect::nData_type		samples( 0 );
  FrameCPP::FrVect::nDim_type       	dim_zero( 0 );
  std::list< INT_4U >			segment_samples;
  General::AutoArray< CHAR_U >		expanded;

  const FrameCPP::FrVect&	pattern( *( ( List.front( ) )->front( ) ) );

  //---------------------------------------------------------------------
  // Information gathering loop
  //---------------------------------------------------------------------

  AT();
  for ( FrVectList::const_iterator i( List.begin( ) );
	i != List.end( );
	i++ )
  {
    AT();
    segment_samples.push_back( getSamples( (*i)->begin( ), (*i)->end( ) ) );
    samples += segment_samples.back( );
    for ( FrameAPI::FrVect::Container::const_iterator
	    j( (*i)->begin( ) );
	  j != (*i)->end( );
	  j++ )
    {
      dim_zero += (*j)->GetDim( 0 ).GetNx( );
    }
  }

  //---------------------------------------------------------------------
  // Create a new vector
  //---------------------------------------------------------------------

  AT();
  vector< Dimension > d( pattern.GetNDim( ) );
  AT();
  for ( INT_4U i( pattern.GetNDim( ) ); i-- > 0; )
  {
    AT();
    d[ i ] = pattern.GetDim( i );
  }
  AT();

  FrameCPP::Dimension dim( samples,
			   d[ 0 ].GetDx( ),
			   d[ 0 ].GetUnitX( ),
			   d[ 0 ].GetStartX( ) );

  INFO( "samples: " << samples );
  // Doesn't look necessary to do
  //std::copy( d, &(d[ pattern.GetNDim( ) ]), d );
  AT();
  FrameCPP::FrVect::data_type
    data( reinterpret_cast< FrameCPP::FrVect::data_pointer_type >
	  ( createVector( pattern.GetType( ), samples ) ) );
  std::unique_ptr< FrameCPP::FrVect >
    ret( new FrameCPP::FrVect( pattern.GetName( ),
			       FrameCPP::FrVect::RAW,
			       pattern.GetType( ),
			       pattern.GetNDim( ),
			       &dim,
			       samples,
			       FrameCPP::FrVect::GetTypeSize( pattern.GetType( ) )
			       * samples,
			       data,
			       pattern.GetUnitY( ) ) );

  // Adjust the array
  const FrameCPP::Dimension&	zdim( ret->GetDim( 0 ) );
  ret->GetDim( 0 ) = FrameCPP::Dimension( dim_zero,
						zdim.GetDx( ),
						zdim.GetUnitX( ),
						zdim.GetStartX( ) );

  //---------------------------------------------------------------------
  // Data population loop
  //---------------------------------------------------------------------
  {
    AT();
    FrameCPP::FrVect::data_pointer_type	data_ptr( ret->GetDataRaw( ).get( ) );
    std::list< INT_4U >::const_iterator	ssi( segment_samples.begin( ) );
    INT_4U				t_size( FrameCPP::FrVect::GetTypeSize( ret->GetType( ) ) );
    INFO( "t_size: " << t_size );

    for ( FrVectList::const_iterator i( List.begin( ) );
	  i != List.end( );
	  data_ptr += *ssi * t_size,
	    i++,
	    ssi++ )
    {
      AT();
      FrameAPI::FrVect::copy( data_ptr, *(*i) );
    }
  }

  //---------------------------------------------------------------------
  // return the results to the user.
  //---------------------------------------------------------------------
  return ret.release( );
}

#endif /* CORE_API */

//-----------------------------------------------------------------------------

#if CORE_API
void* FrameAPI::FrVect::
createVector( INT_2U Type, INT_4U Size )
{
  AT();
  switch( Type )
  {
  case FrameCPP::FrVect::FR_VECT_C:	return new char[ Size ];
  case FrameCPP::FrVect::FR_VECT_2S:	return new INT_2S[ Size ];
  case FrameCPP::FrVect::FR_VECT_8R:	return new REAL_8[ Size ];
  case FrameCPP::FrVect::FR_VECT_4R:	return new REAL_4[ Size ];
  case FrameCPP::FrVect::FR_VECT_4S:	return new INT_4S[ Size ];
  case FrameCPP::FrVect::FR_VECT_8S:	return new INT_8S[ Size ];
  case FrameCPP::FrVect::FR_VECT_8C:	return new REAL_4[ Size*2 ];
  case FrameCPP::FrVect::FR_VECT_16C:	return new REAL_8[ Size*2 ];
  case FrameCPP::FrVect::FR_VECT_2U:	return new INT_2U[ Size ];
  case FrameCPP::FrVect::FR_VECT_4U:	return new INT_4U[ Size ];
  case FrameCPP::FrVect::FR_VECT_8U:	return new INT_8U[ Size ];
  case FrameCPP::FrVect::FR_VECT_1U:	return new CHAR_U[ Size ];
  default:
    AT();
    throw SWIGEXCEPTION( "Unable to create data vector since type not known" );
  }
}

#endif /* CORE_API */

//-----------------------------------------------------------------------------

#if CORE_API

INT_4U FrameAPI::FrVect::
getSamples( FrameAPI::FrVect::Container::const_iterator Start,
	    FrameAPI::FrVect::Container::const_iterator Stop )
{
  INT_4U	retval( 0 );

  for ( FrameAPI::FrVect::Container::const_iterator v( Start );
	v != Stop;
	v++ )
  {
    retval += (*v)->GetNData( );
  }
  return retval;
}

#endif /* CORE_API */

//-----------------------------------------------------------------------------

#if CORE_API

std::string
getAttribute( const FrameCPP::FrVect& vect, std::deque< Query >& dq,
	      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        vectHash.find( q.getName().c_str() );
    if ( iter == vectHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << vect.GetName();
                break;
                
            case FR_COMPRESS:
                res << vect.GetCompress();
                break;
                
            case FR_TYPE:
                switch( vect.GetType() )
                {
                    case FrameCPP::FrVect::FR_VECT_C:
                        res << "CHAR";
                        break;

                    case FrameCPP::FrVect::FR_VECT_2S:
                        res << "INT_2S";
                        break;

                    case FrameCPP::FrVect::FR_VECT_8R:
                        
                        res << "REAL_8";
                        break;

                    case FrameCPP::FrVect::FR_VECT_4R:
                        res << "REAL_4";
                        break;

                    case FrameCPP::FrVect::FR_VECT_4S:
                        res << "INT_4S";
                        break;

                    case FrameCPP::FrVect::FR_VECT_8S:
                        res << "INT_8S";
                        break;

                    case FrameCPP::FrVect::FR_VECT_8C:
                        res << "COMPLEX_8";
                        break;

                    case FrameCPP::FrVect::FR_VECT_16C:
                        res << "COMPLEX_16";
                        break;

                    case FrameCPP::FrVect::FR_VECT_2U:
                        res << "INT_2U";
                        break;

                    case FrameCPP::FrVect::FR_VECT_4U:
                        res << "INT_4U";
                        break;

                    case FrameCPP::FrVect::FR_VECT_8U:
                        res << "INT_8U";
                        break;

                    case FrameCPP::FrVect::FR_VECT_1U:
                        res << "CHAR_U";
                        break;
                        
                    default:
                        res << "UNKNOWN" << vect.GetType();
                        break;
                }
                break;
                
            case FR_NDATA:
                res << vect.GetNData();
                break;
                
            case FR_NBYTES:
                res << vect.GetNBytes();
                break;
                
            case FR_NDIM:
                res << vect.GetNDim();
                break;

            case FR_NX:
            case FR_DX:
            case FR_STARTX:
            case FR_UNITX:
                if ( dq.size() == 0 || dq.front().getName() != "size" )
                {
                    throw SWIGEXCEPTION( "bad_query" );
                }
                res << vect.GetNDim();
                break;
                
            case FR_UNITY:
                res << vect.GetUnitY();
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        if ( q.getQuery() != "index" )
        {
            throw SWIGEXCEPTION( "bad_query" );
        }

        INT_4U i = strtoul( q.getValue().c_str(), 0, 10 );
        if ( i >= vect.GetNDim() )
        {
            throw SWIGEXCEPTION( "index_out_of_range" );
        }
        
        switch( iter->second )
        {
            case FR_NX:
                res << vect.GetDim( i ).GetNx();
                break;
                
            case FR_DX:
                res << vect.GetDim( i ).GetDx();
                break;
                
            case FR_STARTX:
                res << vect.GetDim( i ).GetStartX();
                break;

            case FR_UNITX:
                res << vect.GetDim( i ).GetUnitX();
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}

#endif /* CORE_API */


//-----------------------------------------------------------------------------

#if ! CORE_API

LdasArrayBase*
getData( const FrameCPP::FrVect& vect, std::deque< Query >& dq,
	 std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        return vect2array( vect );
    }
    
    throw SWIGEXCEPTION( "bad_query" );
}

#endif /* ! CORE_API */

#if ! CORE_API
template< class T > FrameCPP::FrVect* array2vect( const LdasArray< T >& a );
template< class T > LdasArray< T >* vect2array( const FrameCPP::FrVect& v );

// template specializations      
template<> LdasArray< COMPLEX_8 >* vect2array< COMPLEX_8 >( const FrameCPP::FrVect& v );
template<> LdasArray< COMPLEX_16 >* vect2array< COMPLEX_16 >( const FrameCPP::FrVect& v );
#endif /* ! CORE_API */
   

#if ! CORE_API

FrameCPP::FrVect* array2vect( const LdasArrayBase& ab )
{
    std::string id = ab.getIdentifier();
 
    if ( id == "int_2s" )
    {
        return array2vect( dynamic_cast< const LdasArray< INT_2S >& >( ab ) );
    }
    else if ( id == "int_2u" )
    {
        return array2vect( dynamic_cast< const LdasArray< INT_2U >& >( ab ) );
    }
    else if ( id == "int_4s" )
    {
        return array2vect( dynamic_cast< const LdasArray< INT_4S >& >( ab ) );
    }
    else if ( id == "int_4u" )
    {
        return array2vect( dynamic_cast< const LdasArray< INT_4U >& >( ab ) );
    }
    else if ( id == "int_8s" )
    {
        return array2vect( dynamic_cast< const LdasArray< INT_8S >& >( ab ) );
    }
    else if ( id == "int_8u" )
    {
        return array2vect( dynamic_cast< const LdasArray< INT_8U >& >( ab ) );
    }
    else if ( id == "real_4" )
    {
        return array2vect( dynamic_cast< const LdasArray< REAL_4 >& >( ab ) );
    }
    else if ( id == "real_8" )
    {
        return array2vect( dynamic_cast< const LdasArray< REAL_8 >& >( ab ) );
    }
    else if ( id == "char" )
    {
        return array2vect( dynamic_cast< const LdasArray< CHAR >& >( ab ) );
    }
    else if ( id == "char_u" )
    {
        return array2vect( dynamic_cast< const LdasArray< CHAR_U >& >( ab ) );
    }
    else if ( id == "complex_8" )
    {
        return array2vect( dynamic_cast< const LdasArray< COMPLEX_8 >& >( ab ) );
    }
    else if ( id == "complex_16" )
    {
        return array2vect( dynamic_cast< const LdasArray< COMPLEX_16 >& >( ab ) );
    }
    else
    {
        throw SWIGEXCEPTION( "unknown_element_type" );
    }
}

#endif /* ! CORE_API */

#if ! CORE_API

template< class T >
FrameCPP::FrVect* array2vect( const LdasArray< T >& a )
{
    General::AutoArray< Dimension > dims( a.getNDim() == 0
					  ? 0
					  : new Dimension[ a.getNDim() ] );
    for ( unsigned int i = 0; i < a.getNDim(); ++i )
    {
        dims[ i ] = Dimension( a.getDimension( i ), a.getDx( i ),
			       a.getUnits( i ), a.getStartX( i ) );
    }
     
    FrameCPP::FrVect* v
      = new FrameCPP::FrVect( a.getName( 0 ),
			      a.getNDim(), dims.get(),
			      a.getData(), a.getDataValueUnit() );

    return v;
}

#endif /* ! CORE_API */

#if ! CORE_API   

LdasArrayBase* vect2array( const FrameCPP::FrVect& v )
{
    // this needs to be changed!!! 
    FrameCPP::FrVect::data_types_type id
      = FrameCPP::FrVect::data_types_type( v.GetType() );
 
    if ( id == FrameCPP::FrVect::FR_VECT_2S )
    {
        return vect2array< INT_2S >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_2U )
    {
        return vect2array< INT_2U >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_4S )
    {
        return vect2array< INT_4S >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_4U )
    {
        return vect2array< INT_4U >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_8S )
    {
        return vect2array< INT_8S >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_8U )
    {
        return vect2array< INT_8U >( v );
    }   
    else if ( id == FrameCPP::FrVect::FR_VECT_8C )
    {
        return vect2array< COMPLEX_8 >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_16C )
    {
        return vect2array< COMPLEX_16 >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_4R )
    {
        return vect2array< REAL_4 >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_8R )
    {
        return vect2array< REAL_8 >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_C )
    {
        return vect2array< CHAR >( v );
    }
    else if ( id == FrameCPP::FrVect::FR_VECT_1U )
    {
        return vect2array< CHAR_U >( v );
    }
    else
    {
        throw SWIGEXCEPTION( "unsupported_type" );
    }
}
    
#endif /* ! CORE_API */

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

#if ! CORE_API

template< class T >
LdasArray< T >* vect2array( const FrameCPP::FrVect& v )
{
    FrameCPP::FrVect::data_type	expanded;

    FrameCPP::FrVect::data_const_pointer_type
        vdata( v.GetDataUncompressed( expanded ) );
    
    const INT_4U v_ndim( v.GetNDim() );

    General::AutoArray< ILwd::LdasArrayBase::size_type >
      dims( v_ndim == 0
	    ? 0
	    : ( new ILwd::LdasArrayBase::size_type[ v_ndim ] ) );
    General::AutoArray< string > units( v_ndim == 0
					? 0
					: ( new string[ v_ndim ] ) );
    General::AutoArray< REAL_8 > dx( v_ndim == 0
				     ? 0
				     : ( new REAL_8[ v_ndim ] ) );
    General::AutoArray< REAL_8 > startx( v_ndim == 0
					 ? 0
					 : ( new REAL_8[ v_ndim ] ) );

    for ( unsigned int i = 0; i < v_ndim; ++i )
    {
        dims[ i ] = v.GetDim( i ).GetNx();
        units[ i ] = v.GetDim( i ).GetUnitX();
        dx[ i ] = v.GetDim( i ).GetDx();
	startx[ i ] = v.GetDim( i ).GetStartX();
    }

    LdasArray< T >* a =
      new LdasArray< T >( reinterpret_cast< const T* >( vdata ),
			  v.GetNDim( ), dims.get( ),
			  "", units.get( ), dx.get( ), startx.get( ),
			  v.GetUnitY( ) );

    a->insertName( 0, v.GetName( ) );
    a->setWriteFormat( ILwd::BASE64 );
    
    return a;
}

#endif /* ! CORE_API */
   
// template specializations      

#if ! CORE_API

template<> LdasArray< COMPLEX_8 >* vect2array< COMPLEX_8 >( const FrameCPP::FrVect& v )
{
    FrameCPP::FrVect::data_type	expanded;

    FrameCPP::FrVect::data_const_pointer_type
        vdata( v.GetDataUncompressed( expanded ) );
    
    const INT_4U vndim( v.GetNDim() );

    General::AutoArray< ILwd::LdasArrayBase::size_type >
      dims( vndim == 0
	    ? 0
	    : ( new ILwd::LdasArrayBase::size_type[ vndim ] ) );
    General::AutoArray< string > units( vndim == 0
					? 0
					: ( new string[ vndim ] ) );
    General::AutoArray< REAL_8 > dx( vndim == 0
				     ? 0
				     : ( new REAL_8[ vndim ] ) );
    General::AutoArray< REAL_8 > startx( vndim == 0
					 ? 0
					 : ( new REAL_8[ vndim ] ) );

    INT_4U ndata( 1 );

    for ( unsigned int i = 0; i < vndim; ++i )
    {
        dims[ i ] = v.GetDim( i ).GetNx();
        units[ i ] = v.GetDim( i ).GetUnitX();
        dx[ i ] = v.GetDim( i ).GetDx();
	startx[ i ] = v.GetDim( i ).GetStartX();
        ndata *= dims[ i ];
    }

    const REAL_4* buf( reinterpret_cast< const REAL_4* >( vdata ) );

    General::AutoArray< COMPLEX_8 > array_buf( 0 );

    if( vndim && ndata )
    {
       array_buf.reset( new COMPLEX_8[ ndata ] );

       // create array elements
       COMPLEX_8* data_elem( array_buf.get() );
       const COMPLEX_8* const data_end( array_buf.get() + ndata - 1 );
       while( data_elem <= data_end )
       {
          *data_elem = COMPLEX_8( *buf, *( buf + 1 ) );
          buf += 2;
          ++data_elem;
       }
    }
    

    LdasArray< COMPLEX_8 >* a =
      new LdasArray< COMPLEX_8 >( array_buf.get( ), vndim, dims.get( ),
				  "", units.get( ), dx.get( ), startx.get( ),
				  v.GetUnitY() );

    a->insertName( 0, v.GetName() );
    a->setWriteFormat( ILwd::BASE64 );
    return a;
}

#endif /* ! CORE_API */
   
#if ! CORE_API

template<> LdasArray< COMPLEX_16 >* vect2array< COMPLEX_16 >( const FrameCPP::FrVect& v )
{
    FrameCPP::FrVect::data_type	expanded;

    FrameCPP::FrVect::data_const_pointer_type
        vdata( v.GetDataUncompressed( expanded ) );
    
    const INT_4U v_ndim( v.GetNDim() );

    General::AutoArray< ILwd::LdasArrayBase::size_type >
      dims( v_ndim == 0
	    ? 0
	    : ( new ILwd::LdasArrayBase::size_type[ v_ndim ] ) );
    General::AutoArray< string > units( v_ndim == 0
					? 0
					: ( new string[ v_ndim ] ) );
    General::AutoArray< REAL_8 > dx( v_ndim == 0
				     ? 0
				     : ( new REAL_8[ v_ndim ] ) );
    General::AutoArray< REAL_8 > startx( v_ndim == 0
					 ? 0
					 : ( new REAL_8[ v_ndim ] ) );

    ILwd::LdasArrayBase::size_type ndata( 1 );
    for ( unsigned int i = 0; i < v_ndim; ++i )
    {
        dims[ i ] = v.GetDim( i ).GetNx();
        units[ i ] = v.GetDim( i ).GetUnitX();
        dx[ i ] = v.GetDim( i ).GetDx();
	startx[ i ] = v.GetDim( i ).GetStartX();
        ndata *= dims[ i ];
    }

    const REAL_8* buf( reinterpret_cast< const REAL_8* >( vdata ) );

    General::AutoArray< COMPLEX_16 > array_buf( 0 );
    if( v_ndim && ndata )
    {
       array_buf.reset( new COMPLEX_16[ ndata ] );

       // create array elements
       COMPLEX_16* data_elem( array_buf.get() );
       const COMPLEX_16* const data_end( array_buf.get() + ndata - 1 );
       while( data_elem <= data_end )
       {
          *data_elem = COMPLEX_16( *buf, *( buf + 1 ) );
          buf += 2;
          ++data_elem;
       }
    }
    

    LdasArray< COMPLEX_16 >* a =
      new LdasArray< COMPLEX_16 >( array_buf.get( ), v_ndim, dims.get( ),
				   "", units.get( ), dx.get( ), startx.get( ),
				   v.GetUnitY( ) );

    a->insertName( 0, v.GetName() );
    a->setWriteFormat( ILwd::BASE64 );
    
    return a;
}

#endif /* ! CORE_API */

//!ignore_end:

//------------------------------------------------------------------------------
//
//: Concatenate data of two specified ILWD elements.
//
// This function concatenates data of two specified elements representing frame
// data: adcdata, auxdata, inputdata, moredata.
//
// The pointer returned from this object is a heavy weight pointer.
//
//!param: LdasElement* c1 - ILWD element.
//!param: LdasElement* c2 - ILWD element to concatenate to the above element.
//
//!return: LdasArrayBase* - A pointer to the instantiated in the C++ layer 
//+	object - a result of the concatenation. User is responsible for
//+	destructing the pointer.
//
//!exc: bad_data - Data is malformed. 
//!exc: unsupported_data - The containers contain data but one or more 
//+     of them contain either more than one data vector or a 
//+     multi-dimensional data vector.
//!exc: incompatible_data - The containers contain data but they are 
//+     of different types.
//!exc: SwigException   
//!exc: std::bad_alloc - Memory allocation failed.   
//

#if ! CORE_API

LdasArrayBase*
concatElementData( const LdasContainer* c1, 
		   const LdasContainer* c2 )
{
   if ( c2 == 0 ||
        c1->size() != c2->size() )
   {
      throw SWIGEXCEPTION( "bad_data" );
   }

   //:TODO: Current implementation assumes that container element 
   // contains only one type of data and there is only one element
   // in the container representing that data. This should be changed
   // since container can have a list of data of different types.
   // For now only one element list is implemented.
   if ( c1->size() != 1 )
   {
      throw SWIGEXCEPTION( "unsupported_data" );
   }

    // Get the data
    const LdasElement* t1( ( *c1 )[ 0 ] );
    const LdasElement* t2( ( *c2 )[ 0 ] );

    if ( t1->getElementId() != t2->getElementId() )
    {
        throw SWIGEXCEPTION( "incompatible_data" );
    }
    if ( t1->getElementId() != ILwd::ID_INT_2U &&
         t1->getElementId() != ILwd::ID_INT_2S &&
         t1->getElementId() != ILwd::ID_INT_4U &&
         t1->getElementId() != ILwd::ID_INT_4S &&
         t1->getElementId() != ILwd::ID_INT_8U &&
         t1->getElementId() != ILwd::ID_INT_8S &&
         t1->getElementId() != ILwd::ID_REAL_4 &&
         t1->getElementId() != ILwd::ID_REAL_8 &&
         t1->getElementId() != ILwd::ID_COMPLEX_8 &&
         t1->getElementId() != ILwd::ID_COMPLEX_16 )
    {
        throw SWIGEXCEPTION( "bad_data" );
    }
    const LdasArrayBase& data1( dynamic_cast< const LdasArrayBase& >( *t1 ) );
    const LdasArrayBase& data2( dynamic_cast< const LdasArrayBase& >( *t2 ) );

    // Check the data dimension
    if ( data1.getNDim() != 1 || data2.getNDim() != 1 )
    {
        throw SWIGEXCEPTION( "unsupported_data: can't concatenate multi"
                             "dimensional data." );
    }

    if ( data1.getNameString() != data2.getNameString() )
    {
        throw SWIGEXCEPTION( "incompatible_data: data has different "
                             "names" );
    }   
   
    const INT_4U dim_index( 0 );
    if ( data1.getDx( dim_index ) != data2.getDx( dim_index ) )
    {
        throw SWIGEXCEPTION( "incompatible_data: data has different "
                             "scale factor." );
    }   

    if ( data1.getStartX( dim_index ) != data2.getStartX( dim_index ) )
    {
        throw SWIGEXCEPTION( "incompatible_data: data has different "
                             "dimension origin." );
    }   

    if ( data1.getUnits() != data2.getUnits() )
    {
        throw SWIGEXCEPTION( "incompatible_data: data has different "
                             "dimension units." );
    }
  
    if ( data1.getDataValueUnit() != data2.getDataValueUnit() )
    {
        throw SWIGEXCEPTION( "incompatible_data: data has different "
                             "value units." );
    }

    // Now concatenate the data
    switch( data1.getElementId() )
    {
        case ILwd::ID_INT_2U:
        {
            return concatVect< INT_2U >( data1, data2 );
            break;
        }
        case ILwd::ID_INT_2S:
        {
            return concatVect< INT_2S >( data1, data2 );
            break;
        }    
        case ILwd::ID_INT_4U:
        {
            return concatVect< INT_4U >( data1, data2 );
            break;
        }    
        case ILwd::ID_INT_4S:
        {
            return concatVect< INT_4S >( data1, data2 );
            break;
        }    
        case ILwd::ID_INT_8U:
        {
            return concatVect< INT_8U >( data1, data2 );
            break;
        }   
        case ILwd::ID_INT_8S:
        {
            return concatVect< INT_8S >( data1, data2 );
            break;
        }
        case ILwd::ID_REAL_4:
        {
            return concatVect< REAL_4 >( data1, data2 );
            break;
        }
        case ILwd::ID_REAL_8:
        {
            return concatVect< REAL_8 >( data1, data2 );
            break;
        }
        case ILwd::ID_COMPLEX_8:
        {
            return concatVect< COMPLEX_8 >( data1, data2 );
            break;
        }
        case ILwd::ID_COMPLEX_16:
        {
            return concatVect< COMPLEX_16 >( data1, data2 );
            break;
        }
        default:
        {
            throw SWIGEXCEPTION( "bad_data" );
            break;
        }
    }
    return 0;
}

#endif /* ! CORE_API */
