#ifndef FrameApiRawDataHH
#define FrameApiRawDataHH

#include <deque>
#include <string>

#include "framecpp/FrRawData.hh"
#include "framecpp/Time.hh"

#include "genericAPI/swigexception.hh"

// ILWD Header Files
#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */
   
#include "query.hh"

namespace FrameAPI
{
  namespace FrRawData
  {
    //: merge two FrRawData pointers into one
    FrameCPP::FrRawData*
    merge( FrameCPP::FrRawData* Primary,
	   const FrameCPP::FrRawData* Secondary );
  }
}

std::string
getAttribute( const FrameCPP::FrRawData& rawdata,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrRawData& rawdata,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index,
	 const FrameCPP::Time& gtime,
	 const REAL_8& dt );

FrameCPP::FrRawData* container2rawData( const ILwd::LdasContainer& c );

ILwd::LdasContainer*
rawData2container( const FrameCPP::FrRawData& rawdata,
		   const FrameCPP::Time& gtime,
		   const REAL_8& dt
#if DAQ_SUPPORT
		   , const std::vector<FrameCPP::Version::DaqFrame::adc_update_struct>* adcUpdateVect = 0
#endif	/* DAQ_SUPPORT */
		   );

void insertRawData( ILwd::LdasContainer& fr, ILwd::LdasContainer& r );

//!exc: SwigException      
ILwd::LdasContainer*
concatRawData( const ILwd::LdasContainer* c1, 
	       const ILwd::LdasContainer* c2 );
   
#endif /* HAVE_LDAS_PACKAGE_ILWD */
   
#endif // FrameApiRawDataHH
