#ifndef FrameApiHistoryHH
#define FrameApiHistoryHH

#include <deque>
#include <string>

#include "framecpp/FrHistory.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

std::string
getAttribute( const FrameCPP::FrHistory& history,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrHistory& history,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index );

FrameCPP::FrHistory* container2history( const ILwd::LdasContainer& c );

ILwd::LdasContainer* history2container( const FrameCPP::FrHistory& history );

void insertHistory( ILwd::LdasContainer& fr, ILwd::LdasContainer& h );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiHistoryHH
