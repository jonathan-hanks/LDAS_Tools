/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "ldas_tools_config.h"

// System header files
#include <sstream>
#include <memory>   

// LDAS Header Files
#include "ldas/ldasconfig.hh"

#include "general/objectregistry.hh"

#include "framecpp/Dimension.hh"
#include "framecpp/FrTOC.hh"
#include "framecpp/FrVect.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldasarray.hh"
#include "ilwd/style.hh"
#include "ilwd/ldasstring.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "filters/Resample.hh"
#include "filters/basic_array.hh"

#include "genericAPI/swigexception.hh"
#if DEPRICATED
#include "genericAPI/util.hh"
#include "genericAPI/RegistryILwd.hh"
#include "genericAPI/elementcmd.hh"
#endif /* DEPRICATED */

// Local Header Files
#include "DeviceIOConfiguration.hh"
#include "framecmd.hh"
#include "frframe.hh"
#include "fradcdata.hh"
#include "frserdata.hh"
#include "frdetector.hh"
#include "frmsg.hh"
#include "frhistory.hh"
#include "frrawdata.hh"
#include "frprocdata.hh"
#include "frsimdata.hh"
#include "frsimevent.hh"
#include "frsummary.hh"
#include "frevent.hh"
#include "frserdata.hh"
#include "getattribute.hh"
#include "ResampleCmd.hh"
#include "util.hh"
#include "math.h"

#include "filereader.hh"

using namespace FrameCPP;

#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasElement;
using ILwd::LdasString;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

using General::GPSTime;

using FrameAPI::DeviceIOConfiguration;
using FrameAPI::StrToCompressionScheme;

using namespace std;   
   
#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#define	INFO( a )						\
    std::cerr << a << " Line#: " << __LINE__ << std::endl
#define INFO_DECL( a ) a
#else
#define	AT()
#define INFO( a )
#define INFO_DECL( a )
#endif

#if CORE_API
#define EXTERN_API
#else /* CORE_API */
#define EXTERN_API extern
#endif /* CORE_API */

//!ignore_begin:
typedef REAL_8					sample_rate_type;
   
//-----------------------------------------------------------------------
/// \brief Registry of frame objects
//-----------------------------------------------------------------------
EXTERN_API ObjectRegistry< FrameH > frameRegistry;
//-----------------------------------------------------------------------
/// \brief Registry of frame file objects
//-----------------------------------------------------------------------
EXTERN_API ObjectRegistry< FrameFile > fileRegistry;
//-----------------------------------------------------------------------
/// \brief Registry of FrAdcData objects
//-----------------------------------------------------------------------
EXTERN_API ObjectRegistry< FrAdcData > adcDataRegistry;
//-----------------------------------------------------------------------
/// \brief Registry of FrProcData objects
//-----------------------------------------------------------------------
EXTERN_API ObjectRegistry< FrProcData > procDataRegistry;

static std::ostringstream debug_stream;

//-----------------------------------------------------------------------
/// \brief Text for invalid frame file object.
//-----------------------------------------------------------------------
ORRLExceptionDesc invalid_frame_file( "invalid_frame_file" );
//-----------------------------------------------------------------------
/// \brief Resouce locking for fileRegistry objects.
///
/// Objects of this type will hold a lock on a framme file object until
/// the object goes out of scope.
//-----------------------------------------------------------------------
typedef ObjectRegistryResourceLock< FrameFile,
				    fileRegistry,
				    invalid_frame_file,
				    SwigException> file_registry_lock;

//-----------------------------------------------------------------------
/// \brief Text for invalid frame object.
//-----------------------------------------------------------------------
ORRLExceptionDesc invalid_frame( "invalid_frame" );
//-----------------------------------------------------------------------
/// \brief Resouce locking for frameRegistry objects.
///
/// Objects of this type will hold a lock on a frame object until
/// the object goes out of scope.
//-----------------------------------------------------------------------
typedef ObjectRegistryResourceLock< FrameH,
				    frameRegistry,
				    invalid_frame,
				    SwigException> frame_registry_lock;

//-----------------------------------------------------------------------------
//
//: True if the FrAdcData object is valid.
//
//!param: const AdcData* adc - Adc data pointer.
//
//!return: bool
//
//!exc: SwigException   
//   
inline bool
isAdcDataValid( const FrAdcData* adc )
{
    return adcDataRegistry.isRegistered( adc );
}

//-----------------------------------------------------------------------------
//
// True if the frame is valid.
//
//!param: FclHeader* frame -)
//
//!return: bool
//
//!exc: None.
//   
inline bool
isFileValid( FrameFile* file )
{
    return fileRegistry.isRegistered( file );
}


//-----------------------------------------------------------------------------
//
//: True if the frame is valid.
//
//!param: const FrameH* frame - frame pointer.
//
//!return: bool
//
//!exc: SwigException   
//   
#if ! CORE_API
bool
isFrameValid( const FrameH* frame )
{
    return frameRegistry.isRegistered( frame );
}
#endif /* ! CORE_API */


//: Helper functional to display FrameCPP object registries content:
//: object pointer and name attribute of the object.
#if ! CORE_API
template< class T > class listName
{
public:
    listName( const char* name ) : mName( name ){}
   
    void operator()( T* obj ) const  
    {
	debug_stream << makeTclPointer( obj, mName )
		     << ":" << obj->GetName() << " ";   
	return;
    }
private:
    const char* mName;
};
#endif /* ! CORE_API */

   
//: Helper functional to display FrameCPP object registries content
#if ! CORE_API
template< class T > class listPointer
{
public:
    listPointer( const char* name ) : mName( name ){}
   
    void operator()( T* obj ) const  
    {
	debug_stream << makeTclPointer( obj, mName ) << " ";
	return;
    }
private:
    const char* mName;
};
#endif /* ! CORE_API */

   
//
// This is a helper function to assist debugging.
//
#if ! CORE_API
std::string dumpFrameCPPRegistry()
{
    debug_stream.str( "" );
   
    debug_stream << "Frame { ";
    frameRegistry.for_each( listName< FrameH >( "Frame" ) );
   
#if FRAME_SPEC_CURRENT <= 5
    debug_stream << "} FrameFile { ";
    fileRegistry.for_each( listPointer< FrameFile >( "FrameFile" ) );
#endif
   
    debug_stream << "} AdcData { ";
    adcDataRegistry.for_each( listName< FrAdcData >( "AdcData" ) );   
   
    debug_stream << "} ProcData { ";
    procDataRegistry.for_each( listName< FrProcData >( "ProcData" ) );   

    debug_stream << "}";
    return debug_stream.str();
}
#endif /* ! CORE_API */
   
 
//-----------------------------------------------------------------------------
//
//: Create a simple template frame.
//
//!usage: set ptFrame [createRawFrame ptSourceFrame]
//
//!param: Frame* ptSourceFrame - Frame object pointer.
//
//!return: Frame* ptFrame - Newly created raw frame object.
//
//!exc: SwigException   
//!exc: std::bad_alloc   
//   
#if ! CORE_API
FrameH* createRawFrame(FrameH* frame)
{
    frame_registry_lock l( frame );
   
    FrameH* newFrame
	= new FrameCPP::FrameH( frame->GetName( ),
				frame->GetRun( ),
				frame->GetFrame( ),
				frame->GetGTime( ),
				frame->GetULeapS( ),
				frame->GetDt( ),
				frame->GetDataQuality( )
				);
    // use empty string for FrRawData.name, otherwise "none" gets
    // inserted by default constructor
    string rawdata_name( "" );
    FrameCPP::FrameH::rawData_type
	rawData( frame->GetRawData( ) );
    if( rawData )
    {
	rawdata_name = rawData->GetName();
    }
   
    rawData.reset( new FrameCPP::FrameH::rawData_type::element_type( rawdata_name ) );
    newFrame->SetRawData( rawData );

    {
	typedef FrameCPP::FrameH::detectSim_type::value_type::element_type
	    element_type;
	FrameCPP::FrameH::detectSim_type& detectorSim
	    = frame->RefDetectSim( );   
	FrameCPP::FrameH::detectSim_type::value_type
	    detect_sim;

	for ( FrameCPP::FrameH::detectSim_type::const_iterator
		  iter = detectorSim.begin();
	      iter != detectorSim.end(); ++iter )
	{
	    detect_sim.reset( new element_type( **iter ) );
	    newFrame->RefDetectSim( ).append(  detect_sim );
	}
    }

    {
	typedef FrameCPP::FrameH::detectProc_type::value_type
	    value_type;
	typedef value_type::element_type
	    element_type;
	FrameCPP::FrameH::detectProc_type& detectorProc
	    = frame->RefDetectProc();
	value_type	detect_proc;

	for ( FrameCPP::FrameH::detectProc_type::const_iterator
		  iter = detectorProc.begin();
	      iter != detectorProc.end();
	      ++iter )
	{
	    detect_proc.reset( new element_type( **iter ) );
	    newFrame->RefDetectProc( ).append( detect_proc );
	}
    }


    {
	typedef FrameCPP::FrameH::history_type::value_type
	    value_type;
	typedef value_type::element_type
	    element_type;
	FrameCPP::FrameH::history_type& hist = frame->RefHistory();
   
	value_type	history;

	for ( FrameCPP::FrameH::history_type::const_iterator
		  iter = hist.begin();
	      iter != hist.end();
	      ++iter )
	{
	    history.reset( new FrameCPP::FrHistory( **iter ) );
	    newFrame->RefHistory().append( history );
	}
    }

    frameRegistry.registerObject( newFrame );
    return newFrame;

}
#endif /* ! CORE_API */
   
//-----------------------------------------------------------------------------
//
//: Creates empty container to hold Detector data structures.
//
//!usage: set contPtr [ createOuterDetectorProcContainer ]
//
//!return: Ldascontainer* ptResult - Newly allocated.
//
//
#if ! CORE_API
ILwd::LdasContainer* createOuterDetectorProcContainer()
{
    LdasContainer* c( 
		     new LdasContainer( ":detectProc:Container(Detector):Frame" ) );   
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */


   
//-----------------------------------------------------------------------------
//
//: Creates empty container to hold simulated Detector data structures.
//
//!usage: set contPtr [ createOuterDetectorSimContainer ]
//
//!return: Ldascontainer* ptResult - Newly allocated.
//
//
#if ! CORE_API
ILwd::LdasContainer* createOuterDetectorSimContainer()
{
    LdasContainer* c( 
		     new LdasContainer( ":detectSim:Container(Detector):Frame" ) );   
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//
//: Creates empty container to hold History data structures.
//
//!usage: set contPtr [ createOuterHistoryContainer ]
//
//!return: Ldascontainer* ptResult - Newly allocated object.
//
//
#if ! CORE_API
ILwd::LdasContainer* createOuterHistoryContainer()
{
    LdasContainer* c( 
		     new LdasContainer( ":history:Container(History):Frame" ) );   
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */
   

//!exc: SwigException      
#if ! CORE_API
unsigned int getFrameStatData (FrDetector* detector, char *statName,
			       char *statRepresentation,
			       std::vector<FrStatData *>& statData )
{
    if( detector == 0 )
    {
	throw SWIGEXCEPTION( "null_detector_pointer" );
    }
#if FRAME_SPEC_CURRENT <= 5
    FrameCPP::FrDetector::StatDataContainer&
	statContainer( detector->refStatData() );
    // `statName' is either a name or an index
    // it could also be empty, `statRepresentation' will be used then only
    //
    unsigned int statNameLen = strlen( statName );
    unsigned int statRepresentationLen = strlen( statRepresentation );
    if ( statNameLen ) {
	char* endptr;
	int index = strtoul( statName, &endptr, 10 );
	if ( *endptr == '\0' ) // Whole thing converted to integer
	{
	    if ( index < 0 || index >= (int) statContainer.size() )
	    {
		return 0; // nothing found
	    }
	    statData.push_back( statContainer[ index ] );
	    return 1;
	}
    }
    // Perform linear search on name & representation
    // Empty string matches all
    for ( unsigned int i = 0; i < statContainer.size(); i++ )
    {
	if ( statNameLen && statContainer[ i ]->getName() != statName )
	    continue;
	if ( statRepresentationLen
	     && statContainer[ i ]->getRepresentation() != statRepresentation )
	    continue;
	statData.push_back( statContainer[ i ] );	    
    }
    return statData.size();
#else
    return 0;
#endif

}
#endif /* ! CORE_API */


//!exc: None.
#if ! CORE_API
std::string getFrameDictionary()
{
#if WORKING
    ldas_ostrstream ss;
    FrameCPP::Version::Dictionary *dictionary = FrameCPP::Version::library.getVersionDictionary();
    for (FrameCPP::Version::Dictionary::const_iterator i = dictionary->begin();
	 i != dictionary->end(); i++) {
	FrameCPP::Version::SH sh = i->second.getSH();
	ss <<  sh.getName() << " { ";
	FrameCPP::Common::Container<FrameCPP::Version::SE> seContainer = sh.getSE();
	for (FrameCPP::Common::Container<FrameCPP::Version::SE>::const_iterator j = seContainer.begin();
	     j != seContainer.end(); j++) {
	    ss << (*j)->getName() << " { " << (*j)->getClass() << " } ";
	}
	ss << " } ";
    }
    ss << std::ends;
    std::string res = ss.str();
    ss.freeze( false );
    return res;
#else
    return "Dictionary function currently not supported";
#endif
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Get Attribute
//
//!param: FrameH* frame
//!param: const char* command 
//!param: char** s
//
//!return: std::string
//
//!exc: SwigException
//
#if ! CORE_API
std::string getFrameAttribute( FrameH* frame, const char* command, char** s )
{
    frame_registry_lock l( frame );     

    std::vector< size_t > sarray;
    char** start( s );
    if ( start != 0 ) {
	while( *start != 0 ) {
	    sarray.push_back( strtoul( *start, 0, 10 ) );
	    ++start;
	}
    }
    
    std::deque< Query > dq;
    tokenize( command, dq );

    ostringstream ss;
    ss << "{" <<  getAttribute( *frame, dq, sarray, 0 ) << "} {";
    for ( unsigned int i=0; i < sarray.size(); ++i ) {
	if ( i != 0 ) {
	    ss << " ";
	}
	ss << sarray[ i ];
    }
    ss << "}";

    return ss.str();
}
#endif /* ! CORE_API */


//!exc: SwigException      
#if ! CORE_API
std::string getFrameData( FrameH* frame, const char* command, char** s )
{

    // Make sure the frame is valid
    frame_registry_lock l( frame );

    // A vector containing indices indicating where the search should begin
    // (this is for container elements).
    std::vector< size_t > sarray;
  
    ILwd::LdasElement* element = getFrameDataCore( frame, command, &sarray );
    Registry::elementRegistry.registerObject( element );
    
    // Construct the return value.
    ostringstream ss;
    ss << createElementPointer( element ) << " {";
    for ( unsigned int i=0; i < sarray.size(); ++i ) {
	if ( i != 0 ) {
	    ss << " ";
	}
	ss << sarray[ i ];
    }
    ss << "}";

    return ss.str();
}
#endif /* ! CORE_API */


//!exc: SwigException      
#if ! CORE_API
ILwd::LdasElement* getFrameDataCore( FrameH* frame, const char* command, std::vector< size_t >* sarray )
{
    frame_registry_lock l( frame );

    // Tokenize the query
    std::deque< Query > dq;
    tokenize( command, dq );

    // Get the element & register it.
    ILwd::LdasElement* element = getData( *frame, dq, *sarray, 0 );
    return element;

}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
void writeFrame( FrameFile* file, FrameH* frame,
		 const char* CompressionMethod,
		 int CompressionLevel )
{
    INFO_DECL( static const char* method = "SWIG: writeFrame" );
    INFO( method << ": Entry" );

    file_registry_lock	file_lock( file );
    frame_registry_lock frame_lock( frame );

    //-------------------------------------------------------------------
    // Add history information (PR#1388)
    //-------------------------------------------------------------------
    {
	typedef FrameCPP::FrameH::history_type::value_type value_type;

	GPSTime gps_time;
	gps_time.Now( );

	string ldas_msg( "LDAS, Version: " );
	ldas_msg += LDAS_VERSION;
	
   
	value_type	history( new FrHistory( "FrameAPI::writeFrame",
						gps_time.GetSeconds( ),
						ldas_msg ) );

	frame->RefHistory( ).append( history );
    }

    //-------------------------------------------------------------------
    // Write out the frame file
    //-------------------------------------------------------------------
    FileWriter* writer = dynamic_cast< FileWriter* >( file );
    INFO( method << ": Writing frame" );
    {
	typedef FrameCPP::IFrameStream::frame_h_type frame_h_type;

	frame_h_type	out_frame( new frame_h_type::element_type( *frame ) );

	writer->WriteFrame( out_frame,
			    StrToCompressionScheme( CompressionMethod ),
			    CompressionLevel );
    }

    INFO( method << ": Exit" );
    return;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
INT_4U
getFrameNumber( FrameFile* file )
{
    if ( !isFileValid( file ) )
    {
        throw SWIGEXCEPTION( "invalid_frame_file" );
    }
    
    try
    {
        FileReader* reader = dynamic_cast< FileReader* >( file );
	if ( reader == NULL )
	{
	    throw std::bad_cast();
	}
	const FrameCPP::Common::FrTOC*	toc( reader->GetTOC( ) );
	if ( toc == (const FrameCPP::Common::FrTOC*)NULL )
	{
	    throw SWIGEXCEPTION( "" );
	}
        return toc->nFrame( );
    }
    catch( std::bad_cast& )
    {
        throw SWIGEXCEPTION( "invalid_file_mode: file not open for input." );
    }
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrameH* readFrame( FrameFile* file )
{
    FrameH* frame = 0;

    if ( !isFileValid( file ) )
    {
        throw SWIGEXCEPTION( "invalid_frame_file" );
    }
    
    try
    {
	FrameCPP::IFrameStream::frame_h_type	frameh;

        FileReader* reader = dynamic_cast< FileReader* >( file );
	if ( reader == NULL )
	{
	    throw std::bad_cast();
	}
        frameh = reader->ReadNextFrame();
	frame = new FrameCPP::FrameH( *frameh );
        frameRegistry.registerObject( frame );
    }
    catch( std::bad_cast& )
    {
        throw SWIGEXCEPTION( "invalid_file_mode: file not open for input." );
    }

    return frame;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Deletes an ADC object from memory.
//
//!usage: destructADC adcPtr
//
//!param: FrAdcData* adcPtr - A pointer to an ADC object.
//
//!exc: invalid_adcdata - The ADC does not exist in the C++ layer.
//!exc: SwigException      
//
#if ! CORE_API
void destructAdcData( FrAdcData* adc )
{
    if ( !isAdcDataValid( adc ) )
    {
        throw SWIGEXCEPTION( "invalid_adcdata" );
    }

    adcDataRegistry.removeObject( adc );

    delete adc;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Deletes FrProcData object from memory.
//
//!usage: destructProcData procPtr
//
//!param: FrProcData* procPtr - A pointer to ProcData object.
//
//!exc: invalid_procdata - The FrProcData object does not exist in the C++ layer.
//!exc: SwigException      
//
#if ! CORE_API
void destructProcData( FrameCPP::FrProcData* proc )
{
    if ( !procDataRegistry.isRegistered( proc ) )
    {
	throw SWIGEXCEPTION( "invalid_procdata" );
    }

    procDataRegistry.removeObject( proc );

    delete proc;

}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Deletes ProcData object from memory.
//
//!usage: unregisterProcData procPtr
//
//!param: ProcData* procPtr - A pointer to ProcData object.
//
//!exc: invalid_procdata - The ProcData object does not exist in the C++ layer.
//!exc: SwigException      
//
#if CORE_API
void unregisterProcData( FrameCPP::FrProcData* proc )
{
    if ( !procDataRegistry.isRegistered( proc ) )
    {
	throw SWIGEXCEPTION( "invalid_procdata" );
    }
  
    procDataRegistry.removeObject( proc );
}
#endif /* CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
void destructFrame( FrameH* frame )
{
    if ( frameRegistry.destructObject( frame ) == false )
    {
        throw SWIGEXCEPTION( "invalid_frame" );
    }
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Open a Frame File for reading/writing
//
// This function opens a frame file.  This function does not check to make
// sure the file is actually a frame file (if the file is opened for reading).
// This will occur when the <code>readFrame</code> function is called.
//
//!param: const char* filename - The filename to open
//!param: const char* mode - The mode to use when opening.  This is one of: +
//!param:     <ul><li>r - Open for reading</li>                             +
//!param:         <li>w - Open for writing</li></ul>
//
//!return: FrameFile*
//
//!exc: SwigException - One of: <ul> +
//!exc:     <li>invalid_mode - An unknown mode was used.</li>
//!exc:     <li>permission_denied - Permission to access the file or a +
//!exc:         directory component was denied.</li>+
//!exc:     <li>file_not_found - The file or a directory component was not +
//!exc:         found</li>+
//!exc:     <li>std::bad_alloc - Insufficient kernel memory to open the file</li>+
//!exc:     <li>io_error - An unknown I/O error occurred</li>
//!exc:     <li>file_creation_failed - The file could not be created.</li></ul>
//
#if ! CORE_API
FrameFile* openFrameFile( const char* filename, const char* mode )
{
    FrameFile* file = 0;

    if ( strcasecmp( mode, "r" ) == 0 )
    {
	unsigned int	buffer_size;
	bool		enable_memory_mapped_io;

	DeviceIOConfiguration::GetConfiguration( filename,
						 buffer_size,
						 enable_memory_mapped_io );
        file = new FileReader( filename,
			       buffer_size,
			       (char*)NULL,
			       enable_memory_mapped_io );
    }
    else if ( strcasecmp( mode, "w" ) == 0 )
    {
        // This is to meet the requirement of writing to a temporary file
        // before writing to the final destination (PR#1911).
        std::string	tmp( filename );
	tmp += ".tmp";

	unsigned int	buffer_size;
	bool		enable_memory_mapped_io;

	DeviceIOConfiguration::GetConfiguration( filename,
						 buffer_size,
						 enable_memory_mapped_io );
        file = new FileWriter( filename,
			       buffer_size,
			       (char*)NULL,
			       enable_memory_mapped_io,
			       tmp.c_str( ) );
    }
    else
    {
        throw SWIGEXCEPTION( "invalid_mode" );
    }
    
    fileRegistry.registerObject( file );
    return file;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
void closeFrameFile( FrameFile* file )
{
    INFO_DECL( static const char* method = "SWIG: closeFrameFile" );

    INFO( method << ": Entry" );
    if ( !isFileValid( file ) )
    {
	INFO( method << ": Exit: Exception - invalid_frame_file" );
        throw SWIGEXCEPTION( "invalid_frame_file" );
    }

    fileRegistry.destructObject( file );
    INFO( method << ": Exit" );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Convert whole FrameCPP Frame into LDAS Container
//
//!usage: set container [ fullFrame2container frame ]
//
//!param: FrameH* frame -- frame pointer
//
//!return: Ldascontainer* -- newly allocated frame container
//
//!exc: invalid_frame -- frame is not registered 
//!exc: SwigException      
//
#if ! CORE_API
LdasContainer* fullFrame2container( FrameH* frame )
{
    frame_registry_lock l( frame );

    LdasContainer* c = frame2container( *frame );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */
   

//-----------------------------------------------------------------------------
//
//: Convert FrameCPP Frame Header into LDAS Container
//
//!usage: set container [ getFrameFrameH frame ]
//
//!param: FrameH* frame -- frame pointer
//
//!return: Ldascontainer* -- newly allocated frame container
//
//!exc: invalid_frame -- frame is not registered 
//!exc: SwigException      
//
#if ! CORE_API
LdasContainer* getFrameFrameH( FrameH* frame )
{
    frame_registry_lock l( frame );

    LdasContainer* c = frame2container( *frame, false );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------
//: Get the list of channels based on the TOC
//
// This fuction takes a frame file as the argument and generates a list
// of channels as specified by the Channels parameter.
//
//!param: const char* Filename - Name of the frame file
//
//!param: const int MetaInfo - Flag composed of the oring together of
//+  values described in the table below indicating which meta information
//+	 associated with the channels should be retrieved.
//+      Standard meta information is the meta information that can
//+      be retieved by reading only the top most Fr structure
//+      for the specified channel type.
//+      Extended meta information requires the reading of the entire
//+      structure including all FrVect structures. This is more
//+      costly as some channels may have large data sets.
//+ <table border=2 cellpadding=4>
//+   <tr><td>META_INFO_ADC
//+       <td>Obtain the standard metadata for an FrAdcData structure.
//+   <tr><td>META_INFO_PROC
//+       <td>Obtain the standard metadata for an FrProcData structure.
//+   <tr><td>META_INFO_SIM
//+        <td>Obtain the standard metadata for an FrSimEvent structure.
//+   <tr><td>META_INFO_SERIAL
//+        <td>Obtain the standard metadata for an FrSerData structure.
//+   <tr><td>META_INFO_ALL
//+        <td>Obtain the standard metadata for all Fr structures listed above.
//+   <tr><td>EXTENDED_META_INFO_ADC
//+       <td>Obtain the extended metadata for an FrAdcData structure.
//+   <tr><td>EXTENDED_META_INFO_PROC
//+       <td>Obtain the extended metadata for an FrProcData structure.
//+   <tr><td>EXTENDED_META_INFO_SIM
//+        <td>Obtain the extended metadata for an FrSimEvent structure.
//+   <tr><td>EXTENDED_META_INFO_SERIAL
//+        <td>Obtain the extended metadata for an FrSerData structure.
//+   <tr><td>EXTENDED_META_INFO_ALL
//+        <td>Obtain the extended metadata for all Fr structures listed above.
//+ </table>
//
//!param: const int Channels - Flag specifying list of channels. The valid
//+       values are CHANNEL_ADC, CHANNEL_PROC, CHANNEL_SERIAL. The
//+ 	  default value is CHANNEL_ADC|CHANNEL_PROC.
//
//!exc: SwigException      
#if ! CORE_API
std::string
getChannelListFromFrameFile( const char* Filename,
			     const int MetaInfo,
			     const int Channels )
{
    //---------------------------------------------------------------------
    // Open the frame file
    //---------------------------------------------------------------------
    unsigned int	buffer_size;
    bool		enable_memory_mapped_io;

    DeviceIOConfiguration::GetConfiguration( Filename,
					     buffer_size,
					     enable_memory_mapped_io );
    std::unique_ptr< FileReader >
	file( new FileReader( Filename,
			      buffer_size,
			      (char*)NULL,
			      enable_memory_mapped_io ) );
    AT();
    //---------------------------------------------------------------------
    // Obtain the channel list from the toc
    //---------------------------------------------------------------------
    std::ostringstream		ss;

    AT();
    const FrameCPP::Common::FrTOC*	toc( file->GetTOC( ) );
    //---------------------------------------------------------------------
    // Obtain the adc channels
    //---------------------------------------------------------------------
    AT();
    if ( Channels & CHANNEL_ADC )
    {
	using FrameCPP::Common::FrTOC;
	AT();
	const FrTOC::cmn_name_container_type&	c( toc->nameADC( ) );
	for ( FrTOC::cmn_name_container_type::const_iterator
		  cur = c.begin( ),
		  end = c.end( );
	      cur != end;
	      cur++ )
	{
	    ss << " {" << *cur << " ADC";
	    if ( MetaInfo & ( META_INFO_ADC | EXTENDED_META_INFO_ADC ) )
	    {
		AT();
		FrameCPP::IFrameStream::fr_adc_data_type
		    adc( file->ReadFrAdcStruct( 0 , *cur ) );
		AT();
		ss << " channelGroup=" << adc->GetChannelGroup( )
		   << " channelNumber=" << adc->GetChannelNumber( )
		   << " nBits=" << adc->GetNBits( )
		   << " bias=" << adc->GetBias( )
		   << " slope=" << adc->GetSlope( )
		   << " units=" << adc->GetUnits( )
		   << " sampleRate=" << adc->GetSampleRate( )
		   << " timeOffset=" << adc->GetTimeOffset( )
		   << " fShift=" << adc->GetFShift( )
		   << " phase=" << adc->GetPhase( )
		   << " dataValid=" << adc->GetDataValid( );
	    }
	    ss << "}";
	    AT();
	}
    }
    //---------------------------------------------------------------------
    // Obtain the proc channels
    //---------------------------------------------------------------------
    AT();
    if ( Channels & CHANNEL_PROC )
    {
	using FrameCPP::Common::FrTOC;
	AT();
	const FrTOC::cmn_name_container_type&	c( toc->nameProc( ) );
	for ( FrTOC::cmn_name_container_type::const_iterator
		  cur = c.begin( ),
		  end = c.end( );
	      cur != end;
	      cur++ )
	{
	    AT();
	    ss << " {" << *cur << " PROC";
	    if ( MetaInfo & ( META_INFO_PROC | EXTENDED_META_INFO_PROC ) )
	    {
		AT();
		//---------------------------------------------------------------
		// If the extended data is being requested, then need to read
		//   all of the data as the extended data is derived from the
		//   FrVect structure.
		//---------------------------------------------------------------
		FrameCPP::FrameH::procData_type::value_type
		    proc( ( MetaInfo & EXTENDED_META_INFO_PROC )
			  ? file->ReadFrProcData( 0 , *cur )
			  : file->ReadFrProcStruct( 0 , *cur ) );
		std::string
		    type( FrameCPP::FrProcData::IDTypeToString( proc->GetType( ) ) );
		if ( MetaInfo & META_INFO_PROC )
		{
		    if ( type.length( ) > 0 )
		    {
			ss << " {type=" << type << "}";
		    }
		    else
		    {
			ss << " type=";
		    }
		    type = FrameCPP::FrProcData::IDSubTypeToString( proc->GetType( ),
								    proc->GetSubType( ) );
		    if ( type.length( ) > 0 )
		    {
			ss << " {subType=" << type << "}";
		    }
		    else
		    {
			ss << " subType=";
		    }
		    //-----------------------------------------------------------
		    // Supply standard metadata
		    //-----------------------------------------------------------
		    ss << " timeOffset=" << proc->GetTimeOffset( )
		       << " tRange=" << proc->GetTRange( )
		       << " fShift=" << proc->GetFShift( )
		       << " phase=" << proc->GetPhase( )
		       << " fRange=" << proc->GetFRange( )
		       << " BW=" << proc->GetBW( );
		}
		if ( MetaInfo & EXTENDED_META_INFO_PROC )
		{
		    //-----------------------------------------------------------
		    // Supply sampleRate if it can be calculated
		    //-----------------------------------------------------------
		    if ( ( proc->GetType( ) == FrameCPP::FrProcData::TIME_SERIES )
			 && ( proc->RefData( )[ 0 ] ) )
		    {
			ss << " sampleRate="
			   << ( 1.0 / proc->RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) );
		    }
		}
	    }
	    ss << "}";
	    AT();
	}
    }
    //---------------------------------------------------------------------
    // Obtain the serial channels
    //---------------------------------------------------------------------
    AT();
    if ( Channels & CHANNEL_SERIAL )
    {
	using FrameCPP::Common::FrTOC;
	AT();
	const FrTOC::cmn_name_container_type&	c( toc->nameSer( ) );
	for ( FrTOC::cmn_name_container_type::const_iterator
		  cur = c.begin( ),
		  end = c.end( );
	      cur != end;
	      cur++ )
	{
	    AT();
	    ss << " {" << *cur << " SERIAL";
	    if ( MetaInfo & ( META_INFO_SERIAL | EXTENDED_META_INFO_SERIAL ) )
	    {
		AT();
		FrameCPP::IFrameStream::fr_ser_data_type
		    serial( file->ReadFrSerStruct( 0 , *cur ) );
		if ( MetaInfo & META_INFO_SERIAL )
		{
		    ss << " timeSec=" << serial->GetTime( ).GetSeconds( )
		       << " timeNsec=" << serial->GetTime( ).GetNanoseconds( )
		       << " sampleRate=" << serial->GetSampleRate( );
		}
	    }
	    ss << "}";
	    AT();
	}
    }
    //---------------------------------------------------------------------
    // Obtain the simulation channels
    //---------------------------------------------------------------------
    AT();
    if ( Channels & CHANNEL_SIM )
    {
	using FrameCPP::Common::FrTOC;
	AT();
	const FrTOC::cmn_name_container_type&	c( toc->nameSim( ) );
	for ( FrTOC::cmn_name_container_type::const_iterator
		  cur = c.begin( ),
		  end = c.end( );
	      cur != end;
	      cur++ )
	{
	    AT();
	    ss << " {" << *cur << " SIM";
	    if ( MetaInfo & ( META_INFO_SIM | EXTENDED_META_INFO_SIM ) )
	    {
		AT();
		FrameCPP::IFrameStream::fr_sim_data_type
		    sim( file->ReadFrSimStruct( 0 , *cur ) );
		if ( MetaInfo & META_INFO_SIM )
		{
		    ss << " sampleRate=" << sim->GetSampleRate( )
		       << " timeOffset=" << sim->GetTimeOffset( )
		       << " fShift=" << sim->GetFShift( )
		       << " phase=" << sim->GetPhase( );
		}
	    }
	    ss << "}";
	    AT();
	}
    }
    //---------------------------------------------------------------------
    // Close the frame file
    //---------------------------------------------------------------------
    AT();
    file.reset( (FileReader*)NULL );
    //---------------------------------------------------------------------
    // Return the results
    //---------------------------------------------------------------------
    AT();
    return ss.str();
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//: Get the list of channels based on a FrameH
//
// This fuction takes a pointer to a FrameH structure and generates a list
// of channels as specified by the Channels parameter.
//
//!param: const char* Filename - Name of the frame file
//!param: int Channels - Flag specifying list of channels. The valid
//+       values are CHANNEL_ADC, CHANNEL_PROC, CHANNEL_SERIAL. The
//+ 	  default value is CHANNEL_ADC|CHANNEL_PROC.
//
//!exc: SwigException      
#if ! CORE_API
std::string
getChannelList( const FrameH* frame, int Channels )
{
    ostringstream ss;
    
    if ( frame->GetRawData() )
    {
	//-------------------------------------------------------------------
	// Obtain ADC channels
	//-------------------------------------------------------------------
	if ( Channels & CHANNEL_ADC )
	{
	    const Common::Container< FrAdcData >&
		c( frame->GetRawData()->RefFirstAdc( ) );
	    for ( Common::Container< FrAdcData >::const_iterator
		      cur = c.begin( ),
		      end = c.end( );
		  cur != end;
		  cur++ )
	    {
		ss << (*cur)->GetName( ) << " ";
	    }
	}
	//-------------------------------------------------------------------
	// Obtain Serial channels
	//-------------------------------------------------------------------
	if ( Channels & CHANNEL_SERIAL )
	{
	    const Common::Container< FrSerData >&
		c( frame->GetRawData()->RefFirstSer( ) );
	    for ( Common::Container< FrSerData >::const_iterator
		      cur = c.begin( ),
		      end = c.end( );
		  cur != end;
		  cur++ )
	    {
		ss << (*cur)->GetName( ) << " ";
	    }
	}
    }
    //---------------------------------------------------------------------
    // Obtain the proc channels
    //---------------------------------------------------------------------
    if ( Channels & CHANNEL_PROC )
    {
	const Common::Container< FrProcData >& c( frame->RefProcData( ) );
	for ( Common::Container< FrProcData >::const_iterator
		  cur = c.begin( ),
		  end = c.end( );
	      cur != end;
	      cur++ )
	{
	    ss << (*cur)->GetName( ) << " ";
	}
    }
    //---------------------------------------------------------------------
    // Obtain the simulation channels
    //---------------------------------------------------------------------
    if ( Channels & CHANNEL_PROC )
    {
	const Common::Container< FrSimData >& c( frame->RefSimData( ) );
	for ( Common::Container< FrSimData >::const_iterator
		  cur = c.begin( ),
		  end = c.end( );
	      cur != end;
	      cur++ )
	{
	    ss << (*cur)->GetName( ) << " ";
	}
    }
    //---------------------------------------------------------------------
    // Return the results
    //---------------------------------------------------------------------
    return ss.str();
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Creates a frame object as ILWD.
//
//!param: const char* name - The name for the frame.
//!param: unsigned int run - the run number
//!param: unsigned int frame - The frame number
//!param: gtimes - The GPS start time of the frame in seconds
//!param: gtimen - The residual nanoseconds to the GPS start time
//!param: uleaps - The integer number of leap seconds between GPS/TAI and UTC
//!param: dt - The frame length in seconds
//
//!return: LdasContainer* - A pointer to the created ILWD frame.
//
//!exc: SwigException -
//!exc: LdasException   
//
#if ! CORE_API
LdasContainer* createFrame(
			   const char* name, INT_4U run, INT_4U frame, 
			   INT_4U gtimes, INT_4U gtimen, INT_2U uleaps,
			   REAL_8 dt )
{
    FrameH f( name, run, frame, FrameCPP::Time(gtimes,gtimen), 
	      uleaps, dt );
    LdasContainer* c = frame2container( f );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Creates a FrProcData object as ILWD.
//
//!param: const char* name - The name for the FrProcData.
//!param: const char* comment - Comment.
//!param: double sampleRate - sampling rate.
//!param: int timeOffsetS - time offset second.
//!param: int timeOffsetN - time offset nanoseconds.
//!param: double fShift - frequency shift.
//!param: gtimes - The GPS start time of the target frame in seconds.
//!param: gtimen - The residual nanoseconds to the GPS start time.
//
//!return: LdasContainer* - A pointer to the created ILWD FrProcData.
//
//!exc: SwigException -
//!exc: LdasException   
//
#if ! CORE_API
LdasContainer* createProcData(
			      const char* name, const char* comment,
			      REAL_8 sampleRate,
			      INT_4U timeOffsetS, INT_4U timeOffsetN,
			      REAL_8 fShift, REAL_4 phase,
			      INT_4U gtimes, INT_4U gtimen )
{
    using FrameCPP::FrProcData;

    FrameCPP::FrProcData p( name,
			    comment,
			    FR_PROC_DATA_DEFAULT_TYPE,
			    FR_PROC_DATA_DEFAULT_SUB_TYPE,
			    FrameCPP::Time( timeOffsetS, timeOffsetN ) - GPSTime( 0, 0 ),
			    FR_PROC_DATA_DEFAULT_TRANGE,
			    fShift,
			    phase,
			    FR_PROC_DATA_DEFAULT_FRANGE,
			    FR_PROC_DATA_DEFAULT_BW );
    p.AppendComment( comment );
    LdasContainer* c = procData2container( p, FrameCPP::Time( gtimes, gtimen ) );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Creates a detector object as ILWD.
//
//!param: const char* name - Detector name.
//!param: short int longitudeD - Detector vertex longitude, degrees.
//!param: short int longitudeM - Detector vertex longitude, minutes.
//!param: float longitudeS - Detector vertex longitude, seconds.
//!param: short int lattitudeD - Detector vertex lattitude, degrees.
//!param: short int lattitudeM - Detector vertex lattitude, minutes.
//!param: float lattitudeS - Detector vertex lattitude, seconds.
//!param: float elevation - Vertex elevation, meters.
//!param: float armXazimuth - Orientation of X arm.
//!param: float armYazimuth - Orientation of Y arm.
//
//!return: LdasContainer* - A pointer to the created ILWD object.
//
//!exc: SwigException -
//!exc: LdasException   
//
#if ! CORE_API
ILwd::LdasContainer* createDetector(
				    const char* name,
				    REAL_8 longitude, REAL_8 lattitude,
				    REAL_4 elevation,
				    REAL_4 armXazimuth, REAL_4 armYazimuth,
				    REAL_4 armXaltitude, REAL_4 armYaltitude,
				    REAL_4 armXmidpoint, REAL_4 armYmidpoint,
				    INT_4S localTime, INT_4U dataQuality,
				    const char* qaBitList)
{
    FrameCPP::FrDetector p( name, "  ",
			    longitude, lattitude,
			    elevation,
			    armXazimuth, armYazimuth,
			    armXaltitude, armYaltitude,
			    armXmidpoint, armYmidpoint,
			    localTime );

    LdasContainer* c = detector2container( p );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */
   
    
//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* createMsg( const char* alarm, const char* message,
                          INT_4U severity, INT_4U gtimes, INT_4U gtimen )
{
    FrameCPP::FrMsg m( alarm, message, severity, FrameCPP::Time( gtimes, gtimen ) );
    LdasContainer* c = msg2container( m );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* createHistory( const char* name, INT_4U time,
                              const char* comment )
{
    FrameCPP::FrHistory h( name, time, comment );
    LdasContainer* c = history2container( h );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
//!exc: LdasException   
#if ! CORE_API
LdasContainer* createRawData( const char* name )
{
    FrameCPP::FrRawData r( name );
    LdasContainer* c = rawData2container( r, FrameCPP::Time( 0, 0 ), 0 );
    Registry::elementRegistry.registerObject( c );
    c->setName( 1, "rawData" );
    return c;
}
#endif /* ! CORE_API */


enum
    {
	FR_FRAME,
	FR_ADCDATA,
	FR_DETECTOR,
	FR_MSG,
	FR_HISTORY,
	FR_RAWDATA,
	FR_PROCDATA,
	FR_SIMDATA,
	FR_SERDATA,
	FR_STATDATA,
	FR_VECT,
	FR_EVENT,
	FR_SUMMARY
    };


QueryHash initFrameObjectsHash()
{
    QueryHash h;
    h[ "frame"      ] = FR_FRAME;
    h[ "adcdata"    ] = FR_ADCDATA;
    h[ "detector"   ] = FR_DETECTOR;
    h[ "msg"        ] = FR_MSG;
    h[ "history"    ] = FR_HISTORY;
    h[ "rawdata"    ] = FR_RAWDATA;
    h[ "procdata"   ] = FR_PROCDATA;
    h[ "simdata"    ] = FR_SIMDATA;
    h[ "serdata"    ] = FR_SERDATA;
    h[ "statdata"   ] = FR_STATDATA;
    h[ "vect"       ] = FR_VECT;
    h[ "event"      ] = FR_EVENT;
    h[ "summary"    ] = FR_SUMMARY;
    return h;
}

static QueryHash frameObjectsHash( initFrameObjectsHash() );
    

//-----------------------------------------------------------------------------
//
//: Insert ILWD Frame Data into an ILWD frame
//
//!param: LdasContainer* f - A valid ILWD frame object to which the data      +
//!param:     be added.
//!param: LdasContainer* c - A valie ILWD frame data object which represents  +
//!param:     the data to be added to the frame.
//!param: const bool validateTime - A flag to enable/disable time stamp
//+       validation for data insertion. True to enable, false to disable.       
//
//!exc: SwigException - one of: <ul type=disc>                                +
//!exc:     <li>invalid_container - One of the container objects does not     +
//!exc:         exist in the C++ layer.</li>                                  +
//!exc:     <li>invalid_frame - The frame container does not represent a frame+
//!exc:         </li>                                                         +
//!exc:     <li>invalid_frame_data - The frame data object being added does   +
//!exc:         not represent frame data.</li>                                +
//!exc:     <li>invalid_request - The second container is also a frame object +
//!exc:         (adding a frame object to a frame object is not supported) or +
//!exc:         it is not a frame data object at all.</li>                    +
//!exc:     <li>incompatible_time - The second container represents a data    +
//!exc:         object which corresponds to a different time than the frame   +
//!exc:         object.</li>                                                  +
//!exc:     <li>incompatible_length - The second container represents a data  +
//!exc:         object which has a different 'dt' than the frame to which it  +
//!exc:         is being added.</li>                                          +
//!exc:     <li>no_rawdata - The ILWD frame data object represents FrAdcData,   +
//!exc:         SerData or a LogMsg there is no FrRawData                       +
//!exc:         object in the frame.</li>                                     +
//!exc:     <li>rawdata_exists - The ILWD frame data object represents a frame+
//!exc:         FrRawData structure but there is already one present in the     +
//!exc:         frame.</li>                                                   +
//!exc:     <li>bad_frame - The ILWD frame object is malformed.</li>          +
//!exc:     <li>bad_adcdata - The ILWD frame data object represents FrAdcData   +
//!exc:         but it is malformed.</li>                                     +
//!exc:     <li>bad_detector - The ILWD frame data object represents a        +
//!exc:         detector but it is malformed.</li>                            +
//!exc:     <li>bad_procdata - The ILWD frame data object represents FrProcData +
//!exc:         but it is malformed.</li>                                     +
//!exc:     <li>bad_simdata - The ILWD frame data object represents SimData   +
//!exc:         but it is malformed.</li>                                     +
//!exc:     <li>bad_serdata - The ILWD frame data object represents SerData   +
//!exc:         but it is malformed.</li>                                     +
//!exc:     <li>bad_event - The ILWD frame data object represents Event       +
//!exc:         but it is malformed.</li>
//
//!todo: Add support for inserting FrStatData!
//
#if ! CORE_API
void insertFrameData( LdasContainer* f, LdasContainer* c,
		      const bool validateTime )
{
    if ( !Registry::isElementValid( f ) || !Registry::isElementValid( c ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    const string& f_name( f->getName( 2 ) );
    if ( strcasecmp( f_name.c_str(), "frame" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_frame" );
    }

    if ( c->getNameSize() == 0 )
    {
        throw SWIGEXCEPTION( "invalid_frame_data" );
    }
    
    std::string tmp( slower( c->getName( 2 )));
    QueryHash::const_iterator iter =
        frameObjectsHash.find( tmp.c_str() );
    if ( iter == frameObjectsHash.end() )
    {
        throw SWIGEXCEPTION( "invalid_frame_object" );
    }

    switch( iter->second )
    {
    case FR_FRAME:
	throw SWIGEXCEPTION( "invalid_request" );

    case FR_ADCDATA:
	insertAdcData( *f, *c, validateTime );
	break;

    case FR_DETECTOR:
	insertDetector( *f, *c );
	break;

    case FR_MSG:
	insertMsg( *f, *c );
	break;

    case FR_HISTORY:
	insertHistory( *f, *c );
	break;

    case FR_RAWDATA:
	insertRawData( *f, *c );
	break;

    case FR_PROCDATA:
	insertProcData( *f, *c, validateTime );
	break;

    case FR_SIMDATA:
	insertSimData( *f, *c, validateTime );
	break;
            
    case FR_SERDATA:
	insertSerData( *f, *c, validateTime );
	break;
            
	//        case FR_STATDATA:
	//            insertStatData( *f, *c );
	//            break;
            
    case FR_EVENT:
	insertEvent( *f, *c, validateTime );
	break;
            
    case FR_SUMMARY:
	insertSummary( *f, *c );
	break;

    default:
	throw SWIGEXCEPTION( "invalid_request" );
    }
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrameH* ilwd2frame( LdasContainer* c )
{
    if ( !Registry::isElementValid( c ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    FrameH* frame = container2frame( *c );
    frameRegistry.registerObject( frame );
    return frame;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatAdcDataElem( const LdasContainer* c1, const LdasContainer* c2 )
{
    std::string name( slower( c1->getName( 2 )));
    QueryHash::const_iterator iter = frameObjectsHash.find( name.c_str() );
    if ( iter == frameObjectsHash.end() )
    {
        throw SWIGEXCEPTION( "invalid_frame_object" );
    }

    if( iter->second == FR_ADCDATA )
    {
        return concatAdcData( c1, c2 );
    }
    throw SWIGEXCEPTION( "not_adcdata" );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatAdcDataList( std::vector<LdasContainer*> vec )
{
    const size_t vec_size( vec.size() );
   
    // There must be some data in there to concatenate
    if ( vec_size < 1 )
        throw SWIGEXCEPTION( "invalid_list_size: adcdata" );
    // This is to allow processing of the requests to concatenate
    // just one element, i.e. to create a deep copy of the element
    if ( vec_size == 1 )
    {
	const LdasContainer* c( vec[ 0 ] );   
	if( c == 0 ||
	    Registry::isElementValid( c ) == false )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: adcdata" );
	}
	LdasContainer* concatData( new LdasContainer( *c ) );
	Registry::elementRegistry.registerObject( concatData );   
   
	return concatData;
    }

    if( !Registry::isElementValid( vec[0] )
	|| !Registry::isElementValid( vec[1] ))
    {
	throw SWIGEXCEPTION( "invalid_ilwd_element: adcdata" );
    }
    LdasContainer* concatData = concatAdcDataElem( vec[0], vec[1] );

    for( unsigned int i = 2; i < vec_size; ++i )
    {
	if( !Registry::isElementValid( vec[i] ) )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: adcdata" );
	}
	LdasContainer* tmp = concatData;
	concatData = concatAdcDataElem( concatData, vec[i] );
	delete tmp;
    }

    Registry::elementRegistry.registerObject( concatData );
    return concatData;
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatProcDataElem( const LdasContainer* c1, const LdasContainer* c2 )
{
    std::string name( slower( c1->getName( 2 )));
    QueryHash::const_iterator iter = frameObjectsHash.find( name.c_str() );
    if ( iter == frameObjectsHash.end() )
    {
        throw SWIGEXCEPTION( "invalid_frame_object" );
    }

    if( iter->second == FR_PROCDATA )
    {
        return concatProcData( c1, c2 );
    }
    throw SWIGEXCEPTION( "not_procdata" );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatProcDataList( std::vector<LdasContainer*>& vec )
{
    const size_t vec_size( vec.size() );

    // There must be some data in there to concatenate
    if ( vec_size < 1 )
        throw SWIGEXCEPTION( "invalid_list_size: procdata" );
    // This is to allow processing of the requests to concatenate
    // just one element, i.e. to create a deep copy of the element 
    if ( vec_size == 1 )
    {
	const LdasContainer* c( vec[ 0 ] );
	if( c == 0 ||
	    Registry::isElementValid( c ) == false )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: procdata" );
	}
   
	LdasContainer* concatData( new LdasContainer( *c ) );
	Registry::elementRegistry.registerObject( concatData );   
   
	return concatData;   
    }

    if( !Registry::isElementValid( vec[0] )
	|| !Registry::isElementValid( vec[1] ))
    {
	throw SWIGEXCEPTION( "invalid_ilwd_element: procdata" );
    }
    LdasContainer* concatData = concatProcDataElem( vec[0], vec[1] );
    for (unsigned int i = 2; i < vec_size; ++i)
    {
	if( !Registry::isElementValid( vec[i] ) )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: procdata" );
	}
	LdasContainer* tmp = concatData;
	concatData = concatProcDataElem( concatData, vec[i] );
	delete tmp;
    }

    Registry::elementRegistry.registerObject( concatData );
    return concatData;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatSimDataElem( const LdasContainer* c1, const LdasContainer* c2 )
{
    std::string name( slower( c1->getName( 2 )));
    QueryHash::const_iterator iter = frameObjectsHash.find( name.c_str() );
    if ( iter == frameObjectsHash.end() )
    {
        throw SWIGEXCEPTION( "invalid_frame_object" );
    }

    if( iter->second == FR_SIMDATA )
    {
        return concatSimData( c1, c2 );
    }
    throw SWIGEXCEPTION( "not_simdata" );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatSimDataList( std::vector<LdasContainer*> vec )
{
    const size_t vec_size( vec.size() ); 
   
    // There must be some data in there to concatenate
    if( vec_size < 1 )
        throw SWIGEXCEPTION( "invalid_list_size: simdata" );
    // This is to allow processing of the requests to concatenate
    // just one element, i.e. to create a deep copy of the element
    if( vec_size == 1 )
    {
	const LdasContainer* c( vec[ 0 ] );   
	if( c == 0 ||
	    Registry::isElementValid( c ) == false )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: simdata" );
	}
   
	LdasContainer* concatData( new LdasContainer( *c ) );
	Registry::elementRegistry.registerObject( concatData );   
   
	return concatData;   
    }

    if( !Registry::isElementValid( vec[0] )
	|| !Registry::isElementValid( vec[1] ))
    {
	throw SWIGEXCEPTION( "invalid_ilwd_element: simdata" );
    }
    LdasContainer* concatData = concatSimDataElem( vec[0], vec[1] );
    for( unsigned int i = 2; i < vec_size; ++i )
    {
	if( !Registry::isElementValid( vec[i] ) )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: simdata" );
	}
	LdasContainer* tmp = concatData;
	concatData = concatSimDataElem( concatData, vec[i] );
	delete tmp;
    }

    Registry::elementRegistry.registerObject( concatData );
    return concatData;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatSerDataElem( const LdasContainer* c1, const LdasContainer* c2 )
{
    std::string name( slower( c1->getName( 2 )));
    QueryHash::const_iterator iter = frameObjectsHash.find( name.c_str() );
    if ( iter == frameObjectsHash.end() )
    {
        throw SWIGEXCEPTION( "invalid_frame_object" );
    }

    if( iter->second == FR_SERDATA )
    {
        return concatSerData( c1, c2 );
    }
    throw SWIGEXCEPTION( "not_serdata" );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatSerDataList( std::vector<LdasContainer*> vec )
{
    const size_t vec_size( vec.size() );
    // There must be some data in there to concatenate
    if( vec_size < 1 )
    {
	throw SWIGEXCEPTION( "invalid_list_size: serdata" );
    }
   
    // This is to allow processing of the requests to concatenate
    // just one element, i.e. to create a deep copy of the element
    if( vec_size == 1 )
    {
	const LdasContainer* c( vec[ 0 ] );   
	if( c == 0 ||
	    Registry::isElementValid( c ) == false )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: serdata" );
	}
   
	LdasContainer* concatData( new LdasContainer( *c ) );
	Registry::elementRegistry.registerObject( concatData );   
   
	return concatData;   
    }

    if( !Registry::isElementValid( vec[0] )
	|| !Registry::isElementValid( vec[1] ))
    {
	throw SWIGEXCEPTION( "invalid_ilwd_element: serdata" );
    }
    LdasContainer* concatData = concatSerDataElem( vec[0], vec[1] );
    for( unsigned int i = 2; i < vec_size; ++i )
    {
	if( !Registry::isElementValid( vec[i] ) )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: serdata" );
	}
	LdasContainer* tmp = concatData;
	concatData = concatSerDataElem( concatData, vec[i] );
	delete tmp;
    }

    Registry::elementRegistry.registerObject( concatData );
    return concatData;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Insert all history structures from the `frame' into the LDAS Container `f'
//
//!param: Frame *frame - Valid FrameCPP frame object where the data is      +
//!param:     copied from.
//!param: LdasContainer *f - Valid ILWD frame object to which the data      +
//!param:     be added.
// 
//!exc: SwigException      
#if ! CORE_API
void insertHistory( FrameH* frame, LdasContainer* f)
{
    frame_registry_lock frame_lock( frame );

    // Target container must be registered
    if ( !Registry::isElementValid( f ))
    {
	throw SWIGEXCEPTION( "invalid_container" );
    }

    // Target container must be a frame
    const string& f_name( f->getName( 2 ) );
    if ( strcasecmp( f_name.c_str(), "frame" ) != 0 )
    {
	throw SWIGEXCEPTION( "bad_ilwd_frame" );
    }
    for ( Common::Container< FrHistory >::iterator
	      cur = frame->RefHistory( ).begin( ),
	      last = frame->RefHistory( ).end( );
	  cur != last;
	  ++cur )
    {
	// Doesn't really make sense to do a deep copy of just created object,
	// then destruct that new object( insertHistory will do it ).
	// Should insert object itself.
	LdasContainer*
	    c = history2container( *(*cur) );
	insertHistory ( *f, *c );
	delete c;
    }
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Insert detector structure from the `frame' into the LDAS Container `f'
//
//!param: Frame *frame - Valid FrameCPP frame object where the data is      +
//!param:     copied from.                                                  +
//!param: LdasContainer *f - Valid ILWD frame object to which the data      +
//!param:     be added.                                                     +
// 
//!exc: SwigException      
//!exc: LdasException
//   
#if ! CORE_API
void insertDetector( FrameH* frame, LdasContainer* f)
{
    frame_registry_lock frame_lock( frame );

    // Target container must be registered
    if ( !Registry::isElementValid( f ))
    {
	throw SWIGEXCEPTION( "invalid_container" );
    }

    // Target container must be a frame
    const string& f_name( f->getName( 2 ) );
    if ( strcasecmp( f_name.c_str(), "frame" ) != 0 )
    {
	throw SWIGEXCEPTION( "bad_ilwd_frame" );
    }

    const size_t detectorSize( frame->RefDetectProc().size() );
    if ( detectorSize == 0 )
    {
	throw SWIGEXCEPTION( "detector_not_found" );
    }

    FrameCPP::FrameH::const_detectProc_iterator
	iter( frame->RefDetectProc().begin() );
    LdasContainer* c( 0 );
    for ( size_t i = detectorSize; i != 0 ; --i, ++iter )
    {
	c = detector2container( **iter, detectorProcName ),
	    insertDetector( *f, *c );
	delete c;
	c = 0;
    }
   
    return;
}
#endif /* ! CORE_API */
   
   
//-----------------------------------------------------------------------------
//
//: Convert FrameCPP Summary data structure into LDAS Container.
//
//!usage: set contPtr [ Summary2container histPtr]
//
//!param: Summary* sumPtr - Summary pointer.
//
//!return: Ldascontainer* ptResult - Newly allocated.
//
//!exc: SwigException      
//!exc: null summary pointer - Summary pointer is NULL.
//!exc: bad_alloc - Memory allocation failed.       
//
#if ! CORE_API
ILwd::LdasContainer* Summary2container( FrameCPP::FrSummary* sumPtr)
{
    if( sumPtr == 0 )
    {
	throw SWIGEXCEPTION( "null summary pointer" );
    }
   
    LdasContainer *c =  summary2container( *sumPtr );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Convert FrameCPP History data structure into LDAS Container.
//
//!usage: set contPtr [ History2container histPtr]
//
//!param: History* histPtr - History pointer.
//
//!return: Ldascontainer* ptResult - Newly allocated.
//
//!exc: SwigException 
//!exc: null history pointer - History pointer is NULL.
//!exc: bad_alloc - Memory allocation failed.         
//
#if ! CORE_API
ILwd::LdasContainer* History2container( FrameCPP::FrHistory* histPtr)
{
    if( histPtr == 0 )
    {
	throw SWIGEXCEPTION( "null history pointer" );
    }
    LdasContainer *c =  history2container( *histPtr );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Convert FrameCPP Detector data structure into LDAS Container.
//
//!usage: set contPtr [ DetectorProc2container detectorPtr]
//
//!param: Detector* detectorPtr - Detector pointer.
//
//!return: Ldascontainer* ptResult - Newly allocated.
//
//!exc: SwigException      
//!exc: LdasException        
//!exc: null detector pointer - Detector pointer is NULL.
//!exc: bad_alloc - Memory allocation failed.  
//
#if ! CORE_API
ILwd::LdasContainer* DetectorProc2container( FrameCPP::FrDetector* detectorPtr )
{
    if( detectorPtr == 0 )
    {
	throw SWIGEXCEPTION( "null detector pointer" );
    }
   
    LdasContainer *c =  detector2container( *detectorPtr, detectorProcName );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */
   
   
//-----------------------------------------------------------------------------
//
//: Convert FrameCPP simulated Detector data structure into LDAS Container.
//
//!usage: set contPtr [ DetectorSim2container detectorPtr]
//
//!param: Detector* detectorPtr - Detector pointer.
//
//!return: Ldascontainer* ptResult - Newly allocated.
//
//!exc: SwigException      
//!exc: LdasException      
//
#if ! CORE_API
ILwd::LdasContainer* DetectorSim2container( FrameCPP::FrDetector* detectorPtr)
{
    LdasContainer *c =  detector2container( *detectorPtr, detectorSimName );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//
//: Convert FrameCPP Serial data structure into LDAS Container.
//
//!usage: set contPtr [ Ser2container serPtr sec nsec dt ]
//
//!param: SerData* serPtr - Serial data pointer.
//!param: unsigned int sec       - GPS frame timestamp.
//!param: unsigned int sec       - GPS frame timestamp.
//!param: double dt              - Frame delta time.
//
//!return: Ldascontainer* ptResult - newly allocated.
//
//!exc: SwigException      
//!exc: LdasException      
//
#if ! CORE_API
ILwd::LdasContainer* Ser2container( FrameCPP::FrSerData* ser, unsigned int sec,
				    unsigned int nsec, double dt )
{
    if( ser == 0 )
    {
	throw SWIGEXCEPTION( "null serdata pointer" );
    }
   
    FrameCPP::Time t(sec,nsec);
    LdasContainer *c =  serData2container( *ser, t, dt );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//
//: Convert FrameCPP SimData structure into LDAS Container.
//
//!usage: set contPtr [ SimData2container simPtr sec nsec dt ]
//
//!param: SimData* simPtr - Sim data pointer.
//!param: unsigned int sec       - GPS frame timestamp.
//!param: unsigned int sec       - GPS frame timestamp.
//!param: double dt              - Frame delta time.
//
//!return: Ldascontainer* ptResult - newly allocated.
//
//!exc: SwigException   
//!exc: LdasException      
//I
#if ! CORE_API
ILwd::LdasContainer* SimData2container( FrameCPP::FrSimData* sim, unsigned int sec,
					unsigned int nsec, double dt )
{
    if( sim == 0 )
    {
	throw SWIGEXCEPTION( "null simdata pointer" );
    }
    FrameCPP::Time t(sec,nsec);
    LdasContainer *c =  simData2container( *sim, t, dt );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//
//: Convert FrameCPP Event structure into LDAS Container.
//
//!usage: set contPtr [ Event2container simPtr sec nsec dt ]
//
//!param: Event* simPtr    - Sim data pointer.
//!param: unsigned int sec - GPS frame timestamp.
//!param: unsigned int sec - GPS frame timestamp.
//!param: double dt        - Frame delta time.
//
//!return: Ldascontainer* ptResult - newly allocated.
//
//!exc: SwigException   
//!exc: LdasException      
//
#if ! CORE_API
ILwd::LdasContainer* Event2container( FrameCPP::FrEvent* sim, unsigned int sec,
				      unsigned int nsec, double dt )
{
    if( sim == 0 )
    {
	throw SWIGEXCEPTION( "null simevent pointer" );
    }  
   
    FrameCPP::Time t(sec,nsec);
    LdasContainer *c =  event2container( *sim, t, dt );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */
   
//-----------------------------------------------------------------------------
//
//: Convert FrameCPP SimEvent structure into LDAS Container.
//
//!usage: set contPtr [ SimEvent2container simPtr sec nsec dt ]
//
//!param: SimEvent* simPtr - Sim data pointer.
//!param: unsigned int sec       - GPS frame timestamp.
//!param: unsigned int sec       - GPS frame timestamp.
//!param: double dt              - Frame delta time.
//
//!return: Ldascontainer* ptResult - newly allocated.
//
//!exc: SwigException   
//!exc: LdasException      
//
#if ! CORE_API
ILwd::LdasContainer* SimEvent2container( FrameCPP::FrSimEvent* sim, unsigned int sec,
					 unsigned int nsec, double dt )
{
    if( sim == 0 )
    {
	throw SWIGEXCEPTION( "null simevent pointer" );
    }  
   
    FrameCPP::Time t(sec,nsec);
    LdasContainer *c =  simEvent2container( *sim, t, dt );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */
   

//-----------------------------------------------------------------------------
//
//: Convert FrameCPP Proc data structure into LDAS Container.
//
//!usage: set contPtr [ Proc2container procPtr sec nsec dt ]
//
//!param: SimData* simPtr - Proc data pointer.
//!param: unsigned int sec       - GPS frame timestamp.
//!param: unsigned int sec       - GPS frame timestamp.
//
//!return: Ldascontainer* ptResult - newly allocated.
//
//!exc: SwigException      
//!exc: LdasException      
//
#if ! CORE_API
ILwd::LdasContainer* Proc2container( FrameCPP::FrProcData* proc, unsigned int sec,
				     unsigned int nsec )
{
    if( proc == 0 )
    {
	throw SWIGEXCEPTION( "null procdata pointer" );
    }  
   
    FrameCPP::Time t(sec,nsec);
    LdasContainer *c =  procData2container( *proc, t );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Convert FrameCPP ADC data structure into LDAS Container
//
//!usage: set contPtr [ Adc2container adcPtr sec nsec dt ]
//
//!param: FrAdcData* adcPtr - adc data pointer
//!param: sec, nsec       - GPS frame timestamp
//!param: dt              - frame delta time
//
//!return: Ldascontainer* -- newly allocated
//
//!exc: SwigException      
//!exc: LdasException      
//
#if ! CORE_API
LdasContainer* Adc2container( FrAdcData* adc, unsigned int sec, unsigned int nsec,
			      double dt )
{
    if( adc == 0 )
    {
	throw SWIGEXCEPTION( "null adcdata pointer" );
    }     
    FrameCPP::Time t(sec,nsec);
    LdasContainer *c =  adcData2container( *adc, t, dt );
    Registry::elementRegistry.registerObject( c );
    return c;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrHistory* getFrameHistory( FrameH* frame, char* historyName )
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::history_type& hist( frame->RefHistory() );

    // `historyName' is ether a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( historyName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) hist.size())
	{
	    throw SWIGEXCEPTION( "history_not_found" );
	}
	return hist[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::history_hash_iterator,
	    FrameCPP::FrameH::history_hash_iterator > p( hist.hashFind( historyName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "history_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */

   
//--------------------------------------------------------------------------------
//
//: Get frame detector
// (Note: frame pointer has to be checked for validity before this call).
//
//!exc: SwigException      
#if ! CORE_API
FrDetector* getFrameDetectorProc( FrameH* frame, const char* detectorName ) 
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::detectProc_type&
	detectorProc( frame->RefDetectProc() );   
    const size_t detSize( detectorProc.size() );
    if( detSize == 0 )
    {
	throw SWIGEXCEPTION( "detectorProc_not_found" );
    }
   
    // Check if this is an integer decimal (an index)
    char* endptr;
    int index( strtoul( detectorName, &endptr, 10 ) );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if( index < 0 || index >= (int) detSize )
	{
	    throw SWIGEXCEPTION( "detectorProc_not_found" );
	}
	return detectorProc[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::detectProc_hash_iterator,
	    FrameCPP::FrameH::detectProc_hash_iterator >
	    p( detectorProc.hashFind( detectorName ) );
	if( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "detectorProc_not_found" );
	}
	return p.first->second.get( );
    }   

    // Should never reach
    return 0;
}
#endif /* ! CORE_API */

   
//--------------------------------------------------------------------------------
//
//: Get frame simulation detector
// (Note: frame pointer has to be checked for validity before this call).
//
//!exc: SwigException      
#if ! CORE_API
FrDetector* getFrameDetectorSim( FrameH* frame, char* detectorName )
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::detectSim_type&
	detectorSim( frame->RefDetectSim() );   
    const size_t detSize( detectorSim.size() );   
    if( detSize == 0 )
    {
	throw SWIGEXCEPTION( "detectorSim_not_found" );
    }

    // Check if this is an integer decimal (an index)
    char* endptr;
    int index( strtoul( detectorName, &endptr, 10 ) );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if( index < 0 || index >= (int) detSize )
	{
	    throw SWIGEXCEPTION( "detectorSim_not_found" );
	}
	return detectorSim[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::detectSim_hash_iterator,
	    FrameCPP::FrameH::detectSim_hash_iterator >
	    p( detectorSim.hashFind( detectorName ) );
	if( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "detectorSim_not_found" );
	}
	return p.first->second.get( );
    }   

    // Should never reach
    return 0;   
}
#endif /* ! CORE_API */
   
   
//!exc: SwigException      
#if ! CORE_API
FrSimData* getFrameSimData( FrameH* frame, char* simName )
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::simData_type& sim( frame->RefSimData () );

    // `simName' is either a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( simName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) sim.size())
	{
	    throw SWIGEXCEPTION( "simdata_not_found" );
	}
	return sim[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::simData_hash_iterator,
	    FrameCPP::FrameH::simData_hash_iterator >
	    p( sim.hashFind( simName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "simdata_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */


//!exc: SwigException      
#if ! CORE_API
FrEvent* getFrameEvent( FrameH* frame, char* eventName )
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::event_type& event( frame->RefEvent() );

    // `eventName' is either a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( eventName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) event.size())
	{
	    throw SWIGEXCEPTION( "event_not_found" );
	}
	return event[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::event_hash_iterator,
	    FrameCPP::FrameH::event_hash_iterator >
	    p( event.hashFind( eventName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "event_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */

   
//!exc: SwigException      
#if ! CORE_API
FrSimEvent* getFrameSimEvent( FrameH* frame, char* simName )
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::simEvent_type& sim( frame->RefSimEvent () );

    // `simName' is either a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( simName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) sim.size())
	{
	    throw SWIGEXCEPTION( "simevent_not_found" );
	}
	return sim[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::simEvent_hash_iterator,
	    FrameCPP::FrameH::simEvent_hash_iterator >
	    p( sim.hashFind( simName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "simevent_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */

   
//!exc: SwigException      
#if ! CORE_API
FrSummary* getFrameSummary( FrameH* frame, char* sumName )
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::summaryData_type& sum( frame->RefSummaryData () );

    // `sumName' is either a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( sumName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) sum.size())
	{
	    throw SWIGEXCEPTION( "sum_not_found" );
	}
	return sum[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::summaryData_hash_iterator,
	    FrameCPP::FrameH::summaryData_hash_iterator >
	    p( sum.hashFind( sumName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "sum_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrProcData* getFrameProcData( FrameH* frame, char* procName )
{
    frame_registry_lock l( frame );

   
    FrameCPP::FrameH::procData_type& proc( frame->RefProcData () );

    // `procName' is ether a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( procName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) proc.size())
	{
	    throw SWIGEXCEPTION( "procdata_not_found" );
	}
	return proc[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrameH::procData_hash_iterator,
	    FrameCPP::FrameH::procData_hash_iterator >
	    p( proc.hashFind( procName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "procdata_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException   
//!exc: invalid_offset - Specified offset is not valid for the data contained
//+     in the ADC.
//!exc: invalid_dt - Specified delta time is too long.
//!exc: bad_alloc - Memory allocation failed.   
//!exc: null adcdata - FrAdcData pointer is NULL.   
#if ! CORE_API
FrAdcData* getFrameAdcDataSlice( FrAdcData* adc, REAL_8 offs, REAL_8 dT, bool reg )
{
    if( adc == 0 )
    {
	throw SWIGEXCEPTION( "null adcdata" );
    }
   
    REAL_8 samplingRate = adc->GetSampleRate();
    // Do not support multiple vectors or multidimensional vectors
    General::SharedPtr< FrameCPP::FrVect > vect(adc->RefData()[0]);
    if( ! vect )
    {
	throw SWIGEXCEPTION( "FrAdcData: null vector" );
    }
    INT_4U nData = vect->GetNData();
    REAL_8 deltaT = ((REAL_8) nData) / samplingRate;
    if (offs < 0 || offs > deltaT )
    {
	throw SWIGEXCEPTION( "invalid_offset" );
    }
    if ( dT < 0 || offs + dT > deltaT )
    {
	throw SWIGEXCEPTION( "invalid_dt" );
    }
    std::unique_ptr< FrAdcData >
	newAdc( new FrAdcData( adc->GetName(),
			       adc->GetChannelGroup(),
			       adc->GetChannelNumber(),
			       adc->GetNBits(),
			       adc->GetSampleRate(),
			       adc->GetBias(),
			       adc->GetSlope(),
			       adc->GetUnits(),
			       adc->GetFShift(),
			       adc->GetTimeOffset(),
			       adc->GetDataValid( ) ) );
    // Now slice the data vector
    FrameCPP::Dimension& dims( vect->GetDim(0) );
    REAL_8 index( samplingRate * offs );
    REAL_8 stop_index( samplingRate * ( offs + dT ) );
    INT_8S offset_index( 0 );
    if( ( index - ( INT_8S )( index ) ) > 0.0f )
    {
	// if falls between samples, take next sample
	offset_index = ( INT_8S )( index + 1 );
    }
    else
    {
	offset_index = ( INT_8S )( index );
    }
    //---------------------------------------------------------------------
    // Reset the offset
    //   Calculate number of seconds the data is being moved forward,
    //     add previous offset,
    //     subtract out the slice offset
    //---------------------------------------------------------------------
    newAdc->SetTimeOffset( ( ( offset_index *
			       ( 1.0 / samplingRate ) ) +
			     newAdc->GetTimeOffset( ) ) - offs);

    INT_8S range_index( 0 );   
    REAL_8 index_reminder( stop_index - ( INT_8S )( stop_index ) );
    if( index_reminder )
    {
	range_index = ( INT_8S )( stop_index );
    }   
    else
    {
	// if falls into the sample, exclude it from the data set
	range_index = ( INT_8S )( stop_index - 1 );
    }
   
    //-------------------------------------------------------------------
    // 
    //-------------------------------------------------------------------
    FrameCPP::Dimension			newDims( (INT_4U)( range_index - offset_index + 1 ),
						 dims.GetDx(), dims.GetUnitX(),
						 dims.GetStartX() );
    INT_2U				type( vect->GetType() );
    const FrameCPP::byte_order_type	byte_order( FrameCPP::BYTE_ORDER_HOST );
    FrameCPP::FrVect::data_type		expanded;
    const CHAR_U*
	vdata( vect->GetDataUncompressed( expanded ) );

    FrameCPP::FrAdcData::data_type::value_type
	newVect( new FrameCPP::FrVect( vect->GetName(),
				       type,
				       1,
				       &newDims,
				       byte_order, 
				       ( vdata + offset_index * FrameCPP::FrVect::GetTypeSize( type ) ),
				       vect->GetUnitY( ) ) );

    newAdc->RefData( ).append( newVect );
    if ( reg )
    {
	adcDataRegistry.registerObject( newAdc.get( ) );
    }
    return newAdc.release( );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
//!exc: std::bad_alloc - Memory allocation failed.   
//!exc: null procdata - FrProcData pointer is NULL.   
//!exc: invalid_offset - Specified offset is not valid for the data contained 
//+     in the FrProcData.
//!exc: invalid_length - Specified length is too long.   
#if ! CORE_API
FrameCPP::FrProcData* getFrameProcDataSliceIndex( FrameCPP::FrProcData* proc,
						  INT_4U offs, INT_4U length, bool reg )
{
    if( proc == 0 )
    {
	throw SWIGEXCEPTION( "null procdata" );
    }
   
    // Do not support multiple vectors or multidimensional vectors
    General::SharedPtr< FrameCPP::FrVect > vect( proc->RefData()[0] );
    if( ! vect )
    {
	throw SWIGEXCEPTION( "FrProcData: null vector" );
    }
    INT_4U nData = vect->GetNData();
    if ( offs > nData )
    {
	throw SWIGEXCEPTION( "invalid_offset" );
    }
    if ( offs + length > nData )
    {
	throw SWIGEXCEPTION( "invalid_length" );
    }
    FrameCPP::FrProcData* newProc
	= FrameAPI::FrProcData::cloneHeader( *proc );

    //-------------------------------------------------------------------
    // Now slice the data vector
    //-------------------------------------------------------------------
    FrameCPP::Dimension&	dims( vect->GetDim(0) );
    FrameCPP::Dimension		newDims( length, dims.GetDx(), 
					 dims.GetUnitX(),
					 dims.GetStartX() + ( offs * dims.GetDx( ) ) );
    INT_2U			type( vect->GetType( ) );
    FrameCPP::FrVect::data_type	exapnded;
    const CHAR_U*
	vdata( vect->GetDataUncompressed( exapnded ) );
    FrameCPP::FrProcData::data_type::value_type
	newVect( new FrameCPP::FrVect( vect->GetName(),
				       type,
				       1,
				       &newDims,
				       FrameCPP::BYTE_ORDER_HOST, 
				       ( vdata + offs *
					 FrameCPP::FrVect::GetTypeSize( type ) ),
				       vect->GetUnitY( ) ) );

    INFO( "dims.getStartX( ): " << dims.GetStartX( ) \
	                        << " offs: " << offs \
	                        << " dims.getDx( ): " << dims.GetDx( ) );
    newProc->RefData( ).append( newVect );

    switch( proc->GetType( ) )
    {
    case FrameCPP::FrProcData::TIME_SERIES:
	//-------------------------------------------------------------------
	// Reset the offset
	//   Calculate number of seconds the data is being moved forward,
	//     add previous offset,
	//     subtract out the slice offset
	//-------------------------------------------------------------------
	newProc->SetTimeOffset( newVect->GetDim( 0 ).GetStartX( ) +
				proc->GetTimeOffset( ) -
				( offs * newVect->GetDim( 0 ).GetDx( ) ) );
	newProc->SetTRange( newVect->GetDim( 0 ).GetDx( ) *
			    newVect->GetDim( 0 ).GetNx( ) );
	newProc->SetFRange( proc->GetFRange( ) );
	break;
    case FrameCPP::FrProcData::FREQUENCY_SERIES:
	newProc->SetTRange( proc->GetTRange( ) );
	newProc->SetFRange( newVect->GetDim( 0 ).GetDx( ) *
			    newVect->GetDim( 0 ).GetNx( ) );
	break;
    default:
	newProc->SetTRange( proc->GetTRange( ) );
	newProc->SetFRange( proc->GetFRange( ) );
	break;
    }

    if (reg)
    {
	procDataRegistry.registerObject( newProc );
    }

    return newProc;
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrameCPP::FrProcData* getFrameProcDataSlice( FrameCPP::FrProcData* proc,
                                             REAL_8 sliceStart,
                                             REAL_8 delta,
                                             bool reg )
{
    if( proc == 0 )
    {
	throw SWIGEXCEPTION( "null procdata" );
    }
   
    // Do not support multiple vectors or multidimensional vectors
    if( proc->RefData().size() > 1 )
    {
	throw SWIGEXCEPTION( "FrProcData:: multiple vectors are not supported" );
    }
   
    const General::SharedConstPtr< FrameCPP::FrVect > vect( proc->RefData()[ 0 ] );
    if( ! vect )
    {
	throw SWIGEXCEPTION( "FrProcData: null vector" );
    }
   
    // Don't support time-frequency domain for now( only time or frequency )
    if( vect->GetNDim() > 1 )
    {
	throw SWIGEXCEPTION( "FrProcData: multidimensional vector is not supported" );
    }

    const INT_4U nData = vect->GetNData();
    const FrameCPP::Dimension& dims( vect->GetDim( 0 ) );   
    const REAL_8 startX = dims.GetStartX();
    const REAL_8 dX = dims.GetDx();
    const REAL_8 sliceEnd = sliceStart + delta;

    //:NOTE: the method for calculating the indices will fail drastically
    // if dX is not strictly positive
    if( dX <= 0.0 )
    {
	throw SWIGEXCEPTION( "procdata:nonpositive_dx" );
    }

    // Check the parameters
    if( sliceStart < startX )
    {
	throw SWIGEXCEPTION( "invalid_procdata_offset" );
    }
   
    if ( sliceEnd > (startX + nData * dX) )
    {
	std::ostringstream	msg;
	msg << "procdata:delta_too_long "
	    << sliceEnd << " > "
	    << ( startX + nData * dX )
	    << " [ name: " << proc->GetName( )
	    << " startX: " << startX
	    << " nData: " << nData
	    << " dX: " << dX
	    << "]"
	    ;
	throw SWIGEXCEPTION( msg.str( ) );
    }

    // Delta should not be negative
    if( delta < 0.0 )
    {
	throw SWIGEXCEPTION( "procdata:negative_delta" );
    }

    //
    // For either time-series or frequency-series, the formula for the
    // x-axis is
    //
    //     x_k = startX + k * dX
    //
    // To work out the index for xStart, we need to choose k to be the
    // first integer k_start such that
    //
    //     startX + k_start * dX >= sliceStart
    //
    // while for the end index, we need to choose length to be the
    // first integer such that
    //
    //     startX + (k_start + length) * dX >= sliceEnd
    //
    // That way the last frequency will be
    //
    //     startX + (k_start + length - 1) * dx
    //
    // and strictly less than sliceEnd. An inefficient but infallible way to do
    // it is by running through the x-values. Note that it's much more 
    // efficient to do this by using ceil/floor functions but Greg Mendell was
    // very concerned that small round-off errors may be a problem eg. a 
    // roundoff may yield (say) 1.99999999, which will floor to 1, even though
    // with infinite precision the correct index would be 2.
    //

    INT_4U kStart = 0;
    // :TRICKY: x needs to be volatile to prevent gcc's optimization of
    // :TRICKY:   floating point numbers generating slightly off numbers
    // :TRICKY:   since it uses more digits of precission than the IEEE
    // :TRICKY:   standard
    volatile REAL_8 x = startX + kStart * dX;
    while (x < sliceStart)
    {
	++kStart;
	x = startX + kStart * dX;
    }
    INFO( "getFrameProcDataSlice: x: " << x );
    // At this point, x >= sliceStart so we're done. Don't change kStart!!

    // Continue to work out the length of the slice in samples.
    INT_4U length = 0;
    x = startX + (kStart + length) * dX;
    while (x < sliceEnd)
    {
	++length;
	x = startX + (kStart + length) * dX;
    }
    // At this point, x = startX + (kStart + length) >= sliceEnd, so
    // startX + (kStart + length - 1) < sliceEnd

    INFO( "getFrameProcDataSlice: kStart: " << kStart << " length: " << length << " reg: " << reg );

    return getFrameProcDataSliceIndex( proc, kStart, length, reg );
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//!exc: SwigException      
//!exc: std::bad_alloc - Memory allocation failed.   
//!exc: null adcdata - FrAdcData pointer is null.
//   
#if ! CORE_API
FrameCPP::FrProcData* adcPointer2ProcPointer( const FrameCPP::FrAdcData* adc )
{
    if( adc == 0 )
    {
	throw SWIGEXCEPTION( "null adcdata" );
    }
   
    FrameCPP::FrProcData* newProc =
	new FrameCPP::FrProcData( adc->GetName(), adc->GetComment( ),
				  FrProcData::TIME_SERIES,
				  FrProcData::UNKNOWN_SUB_TYPE,
				  adc->GetTimeOffset(),
				  0, /* TRange */
				  adc->GetFShift(), 
				  adc->GetPhase(),
				  0, /* FRange */
				  0  /* BW */ );

    //-------------------------------------------------------------------
    // Append data vectors
    //-------------------------------------------------------------------
    const size_t	data_size( adc->RefData( ).size( ) );

    if( data_size > 0 )
    {   
	FrAdcData::const_iterator 			iter( adc->RefData().begin() );
	FrameCPP::FrProcData::data_type::value_type	v;

	for( size_t i = data_size; i != 0; --i, ++iter )
	{
	    v.reset( new FrameCPP::FrVect( **iter ) );
	    newProc->RefData().append( v );
	}
    }
   
    //-------------------------------------------------------------------
    // Append aux vectors
    //-------------------------------------------------------------------
    const size_t	aux_size( adc->RefAux().size() );

    if( aux_size > 0 )
    {   
	FrAdcData::const_iterator			iter( adc->RefAux().begin() );
	FrameCPP::FrProcData::aux_type::value_type	v;

	for( size_t i = aux_size; i != 0; --i, ++iter )
	{
	    v.reset( new FrameCPP::FrVect( **iter ) );
	    newProc->RefAux().append( v );
	}
    }   
   
    procDataRegistry.registerObject( newProc );
    return newProc;
}
#endif /* ! CORE_API */
   
   
//-----------------------------------------------------------------------------
//!exc: SwigException      
//!exc: invalid_procdata - FrProcData object is invalid.
//   
#if ! CORE_API
void setFrameProcDataTimeOffset( FrameCPP::FrProcData* procdata, 
				 const INT_4U sec, const INT_4U nan )   
{
    if( !procDataRegistry.isRegistered( procdata ) )
    {
	throw SWIGEXCEPTION( "invalid_procdata" );
    }   
   
    FrameCPP::Time t( sec, nan );

    procdata->SetTimeOffset( t.GetTime( ) );
    return;
}
#endif /* ! CORE_API */
   
//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrMsg* getFrameMsg( FrameH* frame, char* msgName )
{
    frame_registry_lock l( frame );
   
    if ( ! frame->GetRawData() )
	throw SWIGEXCEPTION( "no_raw_data" );
    FrameCPP::FrRawData::logMsg_type& msg( frame->GetRawData()->RefLogMsg () );

    // `msgName' is ether a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( msgName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) msg.size())
	{
	    throw SWIGEXCEPTION( "msg_not_found" );
	}
	return msg[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrRawData::logMsg_hash_iterator,
	    FrameCPP::FrRawData::logMsg_hash_iterator >
	    p( msg.hashFind( msgName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "msg_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */

   
//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrAdcData* getFrameAdcData( FrameH* frame, char* adcName )
{
    frame_registry_lock l( frame );
   
    if ( ! frame->GetRawData() )
	throw SWIGEXCEPTION( "no_raw_data" );
    FrameCPP::FrRawData::firstAdc_type&
	adc( frame->GetRawData()->RefFirstAdc () );

    // `adcName' is ether a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( adcName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) adc.size())
	{
	    throw SWIGEXCEPTION( "channel_not_found" );
	}
	return adc[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrRawData::firstAdc_hash_iterator,
	    FrameCPP::FrRawData::firstAdc_hash_iterator >
	    p( adc.hashFind( adcName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "channel_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
FrSerData* getFrameSerData( FrameH* frame, char* serName )
{
    frame_registry_lock l( frame );
   
    if ( ! frame->GetRawData() )
	throw SWIGEXCEPTION( "no_raw_data" );
    FrameCPP::FrRawData::firstSer_type&
	ser( frame->GetRawData()->RefFirstSer() );

    // `serName' is ether a name or an index
    //

    // Check if this is an integer decimal (an index)
    //
    char* endptr;
    int index = strtoul( serName, &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	if (index < 0 || index >= (int) ser.size())
	{
	    throw SWIGEXCEPTION( "serdata_not_found" );
	}
	return ser[ index ].get( );
    }
    else
    {
	// hash find the channel by name
	pair< FrameCPP::FrRawData::firstSer_hash_iterator,
	    FrameCPP::FrRawData::firstSer_hash_iterator >
	    p( ser.hashFind( serName ));
	if ( p.first == p.second )
	{
	    throw SWIGEXCEPTION( "serdata_not_found" );
	}
	return p.first->second.get( );
    }
    // never reached
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
// Rehash FrProcData search container
//
//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
void rehashProc( FrameCPP::FrameH* frame ) 
{
    frame_registry_lock l( frame );
   
    frame->RefProcData().rehash();
    return;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
// Adc hash accessor
// Rehashing must be done before `insertchanlist()' call with 
// channel names in the list
//
//!exc: SwigException      
//-----------------------------------------------------------------------------
#if ! CORE_API
void rehashAdc( FrameCPP::FrameH* frame ) 
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::rawData_type
	rawData( frame->GetRawData( ) );

    if ( ! rawData )
    {
	throw SWIGEXCEPTION( "invalid_frame: null rawData" );
    }

    rawData->RefFirstAdc().rehash();
}
#endif /* ! CORE_API */

//
// Serial data hash accessor
// Rehashing must be done before `insertchanlist()' call with 
// serial channel names in the list
//
//!exc: SwigException   
//   
#if ! CORE_API
void rehashSer( FrameCPP::FrameH* frame ) 
{
    frame_registry_lock l( frame );
   
    FrameCPP::FrameH::rawData_type
	rawData( frame->GetRawData( ) );
    if ( ! rawData )
    {
	throw SWIGEXCEPTION( "invalid_frame: null rawData" );
    }

    rawData->RefFirstSer( ).rehash();
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Implements a sort of parsing in the constructor.
//
// Used locally in `insertchanlist()' only.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
class NameIndexToken : public std::string
{
public:
    enum Type {
	INDEX,
	RANGE,
	NAME
    };

    NameIndexToken( char* s, int start, int len );

    unsigned int getType () { return type; };
    unsigned int getFirstInRange() { return first; };
    unsigned int getLastInRange() { return last; };
    const char* getValue() { return c_str (); };
    
private:
    unsigned int type;
    unsigned int first;
    unsigned int last;
};


NameIndexToken::
NameIndexToken( char* s, int start, int len )
    : std::string( s, start, len ), type( NAME ),
      first( 0 ), last( 0 )
{
    char* endptr;
    first = strtoul( getValue(), &endptr, 10 );
    if ( *endptr == '\0' ) // Whole thing converted to integer
    {
	type = INDEX;
    }
    else
    {
	if ( *endptr == '-' )
	{
	    char* last_endptr;
	    last = strtoul( endptr + 1, &last_endptr, 10 );
	    if ( *last_endptr == '\0' )
	    {
		if ( last < first )
		{
		    last = first;
		}
		type = RANGE;
	    }
	    else // there is extra data at the end, must be a name
	    {
		type = NAME;
	    }
	}
	else // Conversion stopped on something other than a dash
	{
	    type = NAME;
	}
    }
#if NEEDED
    if ( first < 0 )
    {
	first = - first;
    }
    if ( last < 0 )
    {
	last = - last;
    }
#endif /* NEEDED */
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Transfer Frame ADC or Serial Data channels into an ILWD frame
//
//!param: Frame *frame - Valid FrameCPP frame object where the data is      +
//!param:     copied from.
//!param: LdasContainer *f - Valid ILWD frame object to which the data      +
//!param:     be added.
//!param: const char *channels[] - ADC/Serial data channel names, indexes,  +
//!param: index ranges. Empty list means all ADCs.
//!param: bool adcData - adcData is processed if true,                      +
//!param: otherwise serial data is processed.
//
//!exc: SwigException   
//   
#if ! CORE_API
void insertchanlist( FrameCPP::FrameH* frame,
		     LdasContainer* f,
		     char* channelList = 0,
		     bool adcData = true)
{
    frame_registry_lock frame_lock( frame );

    // Target container must be registered
    if ( !Registry::isElementValid( f ))
    {
	throw SWIGEXCEPTION( "invalid_container" );
    }

    // Target container must be a frame
    const string& f_name( f->getName( 2 ) );
    if ( strcasecmp( f_name.c_str(), "frame" ) != 0 )
    {
	throw SWIGEXCEPTION( "invalid_container" );
    }

    unsigned int nChannelsTransferred = 0;

    // Assuming here that the valid frame always contains raw data
    // information. This assumption is probably valid for this function, since the
    // sole purpose of it is to transfer the ADC channels, which cannot be stored
    // in a frame unless there is a raw data structure present in it.
    FrameCPP::FrameH::rawData_type
	rawData( frame->GetRawData( ) );

    if ( !rawData )
    {
	throw SWIGEXCEPTION( "invalid_frame" );
    }

    FrameCPP::FrRawData::firstAdc_type& adc( rawData->RefFirstAdc( ) );
    FrameCPP::FrRawData::firstSer_type& ser( rawData->RefFirstSer( ) );

    FrameCPP::Time gtime = frame->GetGTime();
    REAL_8 dt = frame->GetDt();
    //  adc.rehash();

    // Make sure there is a FrRawData object in the frame
    LdasElement* t = f->find( "rawdata", 1 );
    if ( t == 0 || t->getElementId() != ILwd::ID_ILWD )
    {
	throw SWIGEXCEPTION( "no_rawdata" );
    }
    // Both type of casts work, but dynamic cast is safer
    LdasContainer& rawdata( dynamic_cast< LdasContainer& >( *t ) );
    //  LdasContainer& rawdata( (LdasContainer&)( *t ) );

    // Find the container in rawdata if it exists
    t = rawdata.find( adcData? "adcdata": "serdata", 1 );
    if ( t == 0 )
    {
	// it didn't exist so create it.
	// FIXME: create with the number of elements needed to insert all channels (???)
	//
	rawdata.push_back( LdasContainer( adcData? ":adcdata:Container(AdcData)":
					  ":serdata:Container(SerData)" ) );
	t = rawdata.back();
    }
    // Check the container
    else if ( t->getElementId() != ILwd::ID_ILWD )
    {
	throw SWIGEXCEPTION( "bad_rawdata" );
    }

    LdasContainer& data_container( dynamic_cast< LdasContainer& >( *t ) );
    int original_container_size = data_container.size();
    LdasContainer emptyContainer ( adcData? "::AdcData": "::SerData" );

    try {

	// If channel list is NULL or empty, then insert all ADC channels
	//
	int channelListLength = 0;
	if ( channelList != NULL )
	{
	    channelListLength =  strlen( channelList );
	}
	if ( channelListLength == 0 )
	{
	    unsigned int size = adcData? adc.size(): ser.size();
	    // Insert empty containers
	    for ( unsigned int i = 0; i < size; i++ )
	    {
		data_container.push_back( emptyContainer );
	    }
	
	    // Put all channels in
	    for ( unsigned int i = 0; i < size; i++ )
	    {
		// Get a pointer to container IN the vector
		LdasContainer* container
		    = dynamic_cast< LdasContainer* >( data_container[ original_container_size + i ]);
		// conversion
		if ( adcData )
		{
		    adcData2container( *adc[i], gtime, dt, container );
		}
		else
		{
		    serData2container( *ser[i], gtime, dt, container );
		}
	    }
	}
	else
	{
	    // Do it one channel at a time, going through the channel list.
	    // Channel names, indexes and index ranges are supported
	    //

	    char *start = channelList;
	    char *cur_pos = channelList;
	    for (;;)
	    {
		// skip white space
		while (isspace( *start))
		{
		    start++;
		}
		if ( *start == 0 )
		{
		    break; // end of list
		}
		// find end of token
		for ( cur_pos = start;
		      !isspace( *cur_pos ) && *cur_pos != 0;
		      cur_pos++ );

		NameIndexToken q( start, 0, cur_pos-start );

		switch ( q.getType() ) {
		case NameIndexToken::INDEX:
		    {
			unsigned int idx = q.getFirstInRange();
			unsigned int size = adcData? adc.size(): ser.size();
			if ( idx >= size )
			{
			    throw SWIGEXCEPTION( "bad_index" );
			}

			// Insert empty element first
			data_container.push_back( emptyContainer );
			LdasContainer* container
			    = dynamic_cast< LdasContainer* >(data_container[ original_container_size + nChannelsTransferred ]);
			// conversion
			if (adcData)
			{
			    adcData2container( *adc[ idx ], gtime, dt, container );
			}
			else
			{
			    serData2container( *ser[ idx ], gtime, dt, container );
			}
			nChannelsTransferred++;
		    }
		    break;
		case NameIndexToken::RANGE:
		    {
			unsigned int size = adcData? adc.size(): ser.size();
			if ( q.getFirstInRange() >= size
			     || q.getLastInRange() >= size
			     || q.getLastInRange() < q.getFirstInRange() )
			{
			    throw SWIGEXCEPTION( "bad_index" );
			}

			unsigned int num_channels = q.getLastInRange() - q.getFirstInRange() + 1;

			// Insert empty elements first

			for ( unsigned int i=0; i<num_channels; i++ )
			{
			    data_container.push_back( emptyContainer );
			}

			// Reference empty elements and insert data
			for ( unsigned int i=0; i<num_channels; i++ )
			{
			    LdasContainer* container
				= dynamic_cast< LdasContainer* >(data_container[ original_container_size + nChannelsTransferred ]);
			    // conversion
			    if ( adcData )
			    {
				adcData2container( *adc[q.getFirstInRange() + i], gtime, dt, container );
			    }
			    else
			    {
				serData2container( *ser[q.getFirstInRange() + i], gtime, dt, container );
			    }
			    nChannelsTransferred++;
			}
		    }
		    break;
		default: // channel name
		    {
			if ( adcData )
			{
			    pair< FrameCPP::FrRawData::firstAdc_hash_iterator,
				FrameCPP::FrRawData::firstAdc_hash_iterator >
				p( adc.hashFind( q.getValue() ));
			    if ( p.first == p.second )
			    {
				throw SWIGEXCEPTION( "channel_not_found" );
			    }
		
			    // Insert empty element first
			    data_container.push_back( emptyContainer );
			    LdasContainer* container = dynamic_cast< LdasContainer* >(data_container[ original_container_size + nChannelsTransferred ]);
			    // conversion
			    adcData2container( *p.first->second, gtime, dt, container );
			    nChannelsTransferred++;
			}
			else
			{
			    pair< FrameCPP::FrRawData::firstSer_hash_iterator,
				FrameCPP::FrRawData::firstSer_hash_iterator >
				p( ser.hashFind( q.getValue() ));
			    if ( p.first == p.second )
			    {
				throw SWIGEXCEPTION( "channel_not_found" );
			    }
		
			    // Insert empty element first
			    data_container.push_back( emptyContainer );
			    LdasContainer* container = dynamic_cast< LdasContainer* >(data_container[ original_container_size + nChannelsTransferred ]);
			    // conversion
			    serData2container( *p.first->second, gtime, dt, container );
			    nChannelsTransferred++;
			}
		    }
		    break;
		}

		// advance to next token
		start=cur_pos;
	    }
	}
    }
    catch( const LdasException& exc )   
    {
	// cleanup
	unsigned int nRemove = data_container.size() - original_container_size;
	for ( unsigned int i=0; i<nRemove; i++ )
	{
	    data_container.pop_back();
	}   
   
	throw SwigException( exc );  
    }      
    catch (...) {
	// cleanup
	unsigned int nRemove = data_container.size() - original_container_size;
	for ( unsigned int i=0; i<nRemove; i++ )
	{
	    data_container.pop_back();
	}
	throw;
    }
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
#if ! CORE_API
LdasContainer* concatFrameElem( const LdasContainer* c1, const LdasContainer* c2 )
{
    std::string name( slower( c1->getName( 2 )));
    QueryHash::const_iterator iter = frameObjectsHash.find( name.c_str() );
    if ( iter == frameObjectsHash.end() )
    {
        throw SWIGEXCEPTION( "invalid_frame_object" );
    }

    if( iter->second == FR_FRAME )
    {
        return concatFrame( c1, c2 );
    }
    throw SWIGEXCEPTION( "not_frame" );
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//!exc: SwigException      
//!exc: LdasException      
#if ! CORE_API
LdasContainer* concatFrameList( std::vector<LdasContainer*> vec )
{
    const size_t vec_size( vec.size() );
    // There must be some data in there to concatenate
    if ( vec_size < 1 )
    {
	throw SWIGEXCEPTION( "invalid_list_size: frame" );
    }
    // This is to allow processing of the requests to concatenate
    // just one element, i.e. to create a deep copy of the element
    if( vec_size == 1 )
    {
	const LdasContainer* c( vec[ 0 ] );   
	if( c == 0 ||
	    Registry::isElementValid( c ) == false )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: frame" );
	}

	LdasContainer* concatData( new LdasContainer( *c ) );
	Registry::elementRegistry.registerObject( concatData );   
   
	return concatData;   
    }

    if( !Registry::isElementValid( vec[0] ) 
	|| !Registry::isElementValid( vec[1] ))
    {
	throw SWIGEXCEPTION( "invalid_ilwd_element: frame" );
    }
    LdasContainer* concatData = concatFrameElem( vec[0], vec[1] );
    for( unsigned int i = 2; i < vec_size; ++i )
    {
	if( !Registry::isElementValid( vec[i] ) )
	{
	    throw SWIGEXCEPTION( "invalid_ilwd_element: frame" );
	}
	LdasContainer* tmp = concatData;
	concatData = concatFrameElem( concatData, vec[i] );
	delete tmp;
    }

    Registry::elementRegistry.registerObject( concatData );
    return concatData;
}
#endif /* ! CORE_API */

#if ! CORE_API
CREATE_THREADED2( openFrameFile, FrameFile*, const char*, const char* )
CREATE_THREADED1( readFrame, FrameH*, FrameFile* )
CREATE_THREADED4V( writeFrame, FrameFile*, FrameH*, const char*, int )

CREATE_THREADED1( concatAdcDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> )
CREATE_THREADED1( concatProcDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*>& )
CREATE_THREADED1( concatSimDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> )
CREATE_THREADED1( concatSerDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> )
CREATE_THREADED1( concatFrameList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> )
CREATE_THREADED1( ilwd2frame, FrameCPP::FrameH*, ILwd::LdasContainer* )
CREATE_THREADED1( fullFrame2container, LdasContainer*, FrameH* )
CREATE_THREADED2( resampleAdcData,
		  FrameCPP::FrProcData*,
		  const FrameCPP::FrAdcData* const,
		  Filters::ResampleBase* const )
#endif /* ! CORE_API */
   
   
//-----------------------------------------------------------------------------

template class ObjectRegistry< FrameH >;
template class ObjectRegistry< FrameFile >;
template class ObjectRegistry< FrAdcData >;
template class ObjectRegistry< FrProcData >;
template class ObjectRegistry< Filters::ResampleBase >;

//-----------------------------------------------------------------------------
//
//!ignore_end:
