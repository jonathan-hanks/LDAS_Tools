#include <stdexcept>
#include <typeinfo>

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

//!ignore_begin:
#if HAVE_LDAS_PACKAGE_ILWD
template< class T > const ILwd::LdasArray< T >& findArrayType(
    const ILwd::LdasContainer& c, const std::string& name );
const ILwd::LdasContainer* findContainerType(
    const ILwd::LdasContainer& c, const std::string& name );
const ILwd::LdasString& findLStringType(
    const ILwd::LdasContainer& c, const std::string& name );
#endif /* HAVE_LDAS_PACKAGE_ILWD */
//!ignore_end:


//--------------------------------------------------------------------------------
//
//: Find an element by name; cast it to ILwd::LdasArray of template type.
//
// This functions tries to find an ILwd::LdasElement in the container `c'
// by `name'. Then it tries to dynamicly cast found element to
// ILwd::LdasArray&lt; T >.
//
//!usage_ooi: const ILwd::LdasArray&lt; T > &arrayElem
//+	= findArrayType( c, name );
//
//!param: const ILwd::LdasContainer& c - source container.
//!param: const std::string& name - target array element name.
//
//!return: const ILwd::LdasArray&lt; T >& arrayElem - found element.
//
//!exc: SwigException - not found or failed to cast.
//
//-----------------------------------------------------------------------
#if HAVE_LDAS_PACKAGE_ILWD
template< class T >
const ILwd::LdasArray< T >& findArrayType(
    const ILwd::LdasContainer& c, const std::string& name )
{
    const ILwd::LdasElement* ab = c.find( name );
    
    if ( ab == 0 )
    {
        std::string msg = std::string( "invalid_format: \"" ) + name +
            std::string( "\" not found." );
        throw SWIGEXCEPTION( msg.c_str() );
    }

    try
    {
        return dynamic_cast< const ILwd::LdasArray< T >& >( *ab );
    }
    catch( const std::bad_cast& Exception )
    {
        std::string msg = std::string( "invalid_format: \"" ) + name +
            std::string( "\" is unexpected type." );
        throw SWIGEXCEPTION( msg.c_str() );
    }
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */
