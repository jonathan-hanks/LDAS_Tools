#include "config.h"

#include "getattribute.hh"
//!ignore_begin


//!exc: SwigException      
void tokenize( const std::string& s, std::deque< Query >& q )
{
    const char* end = s.c_str() + s.size();
    const char* index = s.c_str();
    const char* next;
    
    while ( index < end )
    {
        q.push_back( Query( index, &next ) );
        index = next;
    }
}

//!ignore_end:
