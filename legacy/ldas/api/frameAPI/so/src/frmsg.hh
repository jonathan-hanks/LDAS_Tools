#ifndef FrameApiMsgHH
#define FrameApiMsgHH

#include <deque>
#include <string>

#include "framecpp/FrMsg.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

std::string
getAttribute( const FrameCPP::FrMsg& msg,
	      std::deque< Query >& dq,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD

ILwd::LdasElement*
getData( const FrameCPP::FrMsg& msg,
	 std::deque< Query >& dq,
	 std::vector< size_t >& start,
	 size_t index );

FrameCPP::FrMsg* container2msg( const ILwd::LdasContainer& c );

ILwd::LdasContainer* msg2container( const FrameCPP::FrMsg& msg );

void insertMsg( ILwd::LdasContainer& fr, ILwd::LdasContainer& m );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // FrameApiMsgHH
