#ifndef FrameApiFrameHH
#define FrameApiFrameHH

#include <deque>
#include <string>

#include "framecpp/FrameH.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/swigexception.hh"

#include "query.hh"

namespace FrameAPI
{
  void Normalize( FrameCPP::FrameH& Frame );

  namespace FrameH
  {
    void merge( FrameCPP::FrameH& Primary,
		const FrameCPP::FrameH& Secondary );
  } // namespace - FrameH
} // namespace - FrameAPI

std::string
getAttribute( const FrameCPP::FrameH& frame,
	      std::deque< Query >& q,
	      std::vector< size_t >& start,
	      size_t index );

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::LdasElement*
getData( const FrameCPP::FrameH& frame,
	 std::deque< Query >& q,
	 std::vector< size_t >& start, size_t index );

FrameCPP::FrameH* container2frame( const ILwd::LdasContainer& c );

ILwd::LdasContainer*
frame2container( const FrameCPP::FrameH& frame,
		 bool wholeFrame = true,
		 bool daqFrame = false );

//!exc: SwigException   
ILwd::LdasContainer*
concatFrame( const ILwd::LdasContainer* c1,
	     const ILwd::LdasContainer* c2 );
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#endif // FrameApiFrameHH
