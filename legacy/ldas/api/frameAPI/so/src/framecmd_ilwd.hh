#ifndef FrameCmdILwdHH
#define FrameCmdILwdHH

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldasarraybase.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

//!exc: SwigException      
ILwd::LdasElement* getFrameDataCore( FrameCPP::FrameH* frame, 
                     		     const char* command, 
				     std::vector< size_t > * sarray );
/*std::string insertRawAdcDataList( FrameCPP::FrameH* frame, 
                             ILwd::LdasContainer *f, 
                             const char* command, char** s )
                             throw( SwigException );
*/

//!exc: SwigException      
void insertDetector( FrameCPP::FrameH* frame, ILwd::LdasContainer* f);

//!exc: SwigException      
void insertHistory( FrameCPP::FrameH* frame, ILwd::LdasContainer* f);
//!exc: SwigException      
ILwd::LdasContainer* Summary2container( FrameCPP::FrSummary* sumPtr );
//!exc: SwigException      
ILwd::LdasContainer* History2container( FrameCPP::FrHistory* history );
//!exc: SwigException      
ILwd::LdasContainer* DetectorProc2container( FrameCPP::FrDetector* detector );
//!exc: SwigException      
ILwd::LdasContainer* DetectorSim2container( FrameCPP::FrDetector* detector );
//!exc: SwigException      
ILwd::LdasContainer* Adc2container( FrameCPP::FrAdcData* adc, unsigned int sec,
				    unsigned int nsec, double dt );
//!exc: SwigException      
ILwd::LdasContainer* Ser2container( FrameCPP::FrSerData* adc, unsigned int sec,
				    unsigned int nsec, double dt );
//!exc: SwigException      
ILwd::LdasContainer* SimData2container( FrameCPP::FrSimData* adc, unsigned int sec,
				    unsigned int nsec, double dt );
//!exc: SwigException      
ILwd::LdasContainer* Event2container( FrameCPP::FrEvent* adc, unsigned int sec,
				      unsigned int nsec, double dt );
//!exc: SwigException      
ILwd::LdasContainer* SimEvent2container( FrameCPP::FrSimEvent* adc, unsigned int sec,
				    unsigned int nsec, double dt );
//!exc: SwigException      
ILwd::LdasContainer* Proc2container( FrameCPP::FrProcData* adc, unsigned int sec,
				    unsigned int nsec );
//!exc: SwigException      
ILwd::LdasContainer* fullFrame2container( FrameCPP::FrameH* frame );
CREATE_THREADED1_DECL( fullFrame2container, ILwd::LdasContainer*, FrameCPP::FrameH* );
//!exc: SwigException      
ILwd::LdasContainer* getFrameFrameH( FrameCPP::FrameH* frame );

#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif /* FrameCmdILwdHH */
