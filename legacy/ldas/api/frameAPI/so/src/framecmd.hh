#ifndef FrameCmdHH
#define FrameCmdHH

// System Header Files
#include <string>   

#include "general/types.hh"

// LDAS Header Files
#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrDetector.hh"
#include "framecpp/FrEvent.hh"
#include "framecpp/FrHistory.hh"
#include "framecpp/FrMsg.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrSerData.hh"
#include "framecpp/FrStatData.hh"
#include "framecpp/FrSimData.hh"
#include "framecpp/FrSimEvent.hh"
#include "framecpp/FrStatData.hh"
#include "framecpp/FrSummary.hh"
#include "framecpp/FrVect.hh"

#include "genericAPI/swigexception.hh"
#if DEPRICATED
#include "genericAPI/util.hh"
#include "genericAPI/threaddecl.hh"
#include "genericAPI/elementcmd.hh"
#endif /* DEPRICATED */
#if OS_TOOLKIT_ERROR_HERE
#include "genericAPI/registry.hh"
#endif /* OS_TOOLKIT_ERROR_HERE */


#include "filereader.hh"
   
//-----------------------------------------------------------------------------
// Frame Specification Types as Macros to make Swig Happy
//-----------------------------------------------------------------------------

#if SWIG
#include <float.h>

#if SIZEOF_SHORT == 2
#define INT_2S short
#define INT_2U unsigned short
#elif SIZEOF_INT == 2
#define INT_2S int
#define INT_2U unsigned int
#else
#   error "No 2 byte integer found"
#endif

#if SIZEOF_INT == 4
#define INT_4S int
#define INT_4U unsigned int
#elif SIZEOF_LONG == 4
#define INT_4S long
#define INT_4U unsigned long
#else
#   error "No 4 byte integer found"
#endif

#if SIZEOF_LONG == 8
#define INT_8S long
#define INT_8U unsigned long
#elif SIZEOF_LONG_LONG == 8
#define INT_8S long long
#define INT_8U unsigned long long
#else
#   error "No 8 byte integer found"
#endif

#if SIZEOF_FLOAT == 4
#define REAL_4 float
#else
#   error "No 4 byte float found"
#endif

#if SIZEOF_DOUBLE == 8
#define REAL_8 double
#else
#   error "No 8 byte float found"
#endif

#define CHAR char 
#define CHAR_U unsigned char
#endif /* SWIG */

const int META_INFO_ADC = 0x0001;
const int META_INFO_PROC = 0x0002;
const int META_INFO_SERIAL = 0x0004;
const int META_INFO_SIM = 0x0008;

const int META_INFO_ALL = ( META_INFO_ADC
			    | META_INFO_PROC
			    | META_INFO_SERIAL
			    | META_INFO_SIM );

const int EXTENDED_META_INFO_ADC = 0x0010;
const int EXTENDED_META_INFO_PROC = 0x0020;
const int EXTENDED_META_INFO_SERIAL = 0x0040;
const int EXTENDED_META_INFO_SIM = 0x0080;

const int EXTENDED_META_INFO_ALL = ( EXTENDED_META_INFO_ADC
				     | EXTENDED_META_INFO_PROC
				     | EXTENDED_META_INFO_SERIAL
				     | EXTENDED_META_INFO_SIM );

const int CHANNEL_ADC = 0x0001;
const int CHANNEL_PROC = 0x0002;
const int CHANNEL_SERIAL = 0x0004;
const int CHANNEL_SIM = 0x0008;

#include "framecmd_ilwd.hh"

//!ignore_begin:

//!exc: None.   
std::string getFrameDictionary();

std::string dumpFrameCPPRegistry();   
   
//-----------------------------------------------------------------------------
// Frame Accessors
//-----------------------------------------------------------------------------

//!exc: SwigException   
std::string getFrameAttribute( FrameCPP::FrameH* frame, const char* command,
                               char** s );
//!exc: SwigException      
std::string getFrameData( FrameCPP::FrameH* frame, const char* command, 
                          char** s );
std::string getChannelListFromFrameFile( const char* Filename,
					 const int GetMetaInfo,
					 const int Channels = ( CHANNEL_ADC |
								CHANNEL_PROC ) );
std::string getChannelList( const FrameCPP::FrameH* frame,
			    int Channels = ( CHANNEL_ADC |
					     CHANNEL_PROC ) );
//!exc: SwigException   
unsigned int getFrameStatData (FrameCPP::FrDetector* detector, char *statName, char *statRepresentation,
			       std::vector<FrameCPP::FrStatData *> &statData );
//!exc: SwigException      
FrameCPP::FrAdcData* getFrameAdcData(FrameCPP::FrameH* frame, char* adcName );
//!exc: SwigException      
FrameCPP::FrMsg* getFrameMsg(FrameCPP::FrameH* frame, char* msgName);
//!exc: SwigException      
FrameCPP::FrHistory* getFrameHistory( FrameCPP::FrameH* frame, char* historyName );
//!exc: SwigException      
FrameCPP::FrDetector* getFrameDetectorProc( FrameCPP::FrameH* frame, const char* detectorName ); 
//!exc: SwigException      
FrameCPP::FrDetector* getFrameDetectorSim( FrameCPP::FrameH* frame, char* detectorName );
//!exc: SwigException   
FrameCPP::FrSimData* getFrameSimData( FrameCPP::FrameH* frame, char* simName );
//!exc: SwigException      
FrameCPP::FrSummary* getFrameSummary( FrameCPP::FrameH* frame, char* sumName );
//!exc: SwigException      
FrameCPP::FrEvent* getFrameEvent( FrameCPP::FrameH* frame, char* eventName );
//!exc: SwigException      
FrameCPP::FrSimEvent* getFrameSimEvent( FrameCPP::FrameH* frame, char* simName );
//!exc: SwigException      
FrameCPP::FrProcData* getFrameProcData( FrameCPP::FrameH* frame, char* procName );
//!exc: SwigException      
FrameCPP::FrProcData* getFrameStrain( FrameCPP::FrameH* frame, char* strainName );
//!exc: SwigException      
FrameCPP::FrSerData* getFrameSerData (FrameCPP::FrameH* frame, char* serName);
//!exc: SwigException      
bool isFrameValid( const FrameCPP::FrameH* frame );
//!exc: SwigException      
bool isAdcDataValid( const FrameCPP::FrAdcData* frame );

//-----------------------------------------------------------------------------
// Frame I/O
//-----------------------------------------------------------------------------

// Forward declarations
namespace Filters {
    class ResampleBase;
}

//!exc: SwigException      
FrameFile* openFrameFile( const char* filename, const char* mode );
#if DEPRICATED
//!exc: SwigException   
CREATE_THREADED2_DECL( openFrameFile, FrameFile*, const char*, const char* );
#endif /* DEPRICATED */
//!exc: SwigException   
void closeFrameFile( FrameFile* file );
//!exc: SwigException   
INT_4U getFrameNumber( FrameFile* file );
//!exc: SwigException   
FrameCPP::FrameH* readFrame( FrameFile* file );
#if DEPRICATED
CREATE_THREADED1_DECL( readFrame, FrameCPP::FrameH*, FrameFile* );
#endif /* DEPRICATED */
//!exc: SwigException      
void writeFrame( FrameFile* file, FrameCPP::FrameH* frame,
		 const char* CompressionMethod = "gzip",
		 int CompressionLevel = 1 );
#if DEPRICATED
CREATE_THREADED4V_DECL( writeFrame, FrameFile*, FrameCPP::FrameH*,  const char*, int );
#endif /* DEPRICATED */

//!exc: SwigException   
void destructAdcData( FrameCPP::FrAdcData* adc );
//!exc: SwigException      
void destructProcData( FrameCPP::FrProcData* adc );

void unregisterProcData( FrameCPP::FrProcData* proc );

//!exc: SwigException      
FrameCPP::FrAdcData* getFrameAdcDataSlice( FrameCPP::FrAdcData* adc, REAL_8 offs, REAL_8 dT, bool reg = true );
//!exc: SwigException      
FrameCPP::FrProcData* getFrameProcDataSliceIndex( FrameCPP::FrProcData* proc,
   INT_4U offs, INT_4U length, bool reg = true );
//!exc: SwigException      
FrameCPP::FrProcData* getFrameProcDataSlice( FrameCPP::FrProcData* proc, 
   REAL_8 offset, REAL_8 delta, bool reg = true );
FrameCPP::FrProcData* adcPointer2ProcPointer( const FrameCPP::FrAdcData* adc );   
void setFrameProcDataTimeOffset( FrameCPP::FrProcData* procdata, 
                                 const unsigned int sec, const unsigned int nan );      
//!exc: SwigException      
FrameCPP::FrAdcData* adjustAdcData( const FrameCPP::FrAdcData* const adc,
				  Filters::ResampleBase* const r );

//-----------------------------------------------------------------------------
// Frame Destruction
//-----------------------------------------------------------------------------

//!exc: SwigException      
void destructFrame( FrameCPP::FrameH* frame );


//-----------------------------------------------------------------------------
// Frame Mutators
//-----------------------------------------------------------------------------

#if DEPRICATED
//!exc: SwigException      
void insertFrameData( ILwd::LdasContainer* f, ILwd::LdasContainer* c, 
   const bool validateTime );
//!exc: SwigException   
void rehashProc( FrameCPP::FrameH* frame );
//!exc: SwigException      
void rehashAdc( FrameCPP::FrameH* frame );
//!exc: SwigException      
void rehashSer( FrameCPP::FrameH* frame );
//!exc: SwigException      
void insertchanlist ( FrameCPP::FrameH* frame,
		      ILwd::LdasContainer* f,
		      char* channelList,
		      bool adcData);
#endif /* DEPRICATED */

   
//-----------------------------------------------------------------------------
// Frame Creation
//-----------------------------------------------------------------------------

//!exc: SwigException      
FrameCPP::FrameH* createRawFrame(FrameCPP::FrameH* frame);
#if DEPRICATED
//!exc: SwigException      
ILwd::LdasContainer* createFrame(
    const char* name, INT_4U run, INT_4U frame, 
    INT_4U gtimes, INT_4U gtimen, INT_2U uleaps,
    REAL_8 dt );
//!exc: SwigException   
ILwd::LdasContainer* createProcData(
    const char* name, const char* comment,
    REAL_8 sampleRate,
    INT_4U timeOffsetS, INT_4U timeOffsetN,
    REAL_8 fShift, REAL_4 phase,
    INT_4U gtimes, INT_4U gtimena );
//!exc: SwigException   
ILwd::LdasContainer* createDetector(
    const char* name,
    REAL_8 longitude, 
    REAL_8 lattitude,
    REAL_4 elevation,
    REAL_4 armXazimuth, REAL_4 armYazimuth,
    REAL_4 armXaltitude, REAL_4 armYaltitude,
    REAL_4 armXmidpoint, REAL_4 armYmidpoint,
    INT_4S localTime, INT_4U dataQuality,
    const char* qaBitList);
//!exc: SwigException   
ILwd::LdasContainer* createMsg(
    const char* alarm, const char* message, INT_4U severity,
    unsigned int gtimes, unsigned int gtimen );
//!exc: SwigException   
ILwd::LdasContainer* createHistory(
                                   const char* name, INT_4U time, const char* comment );
//!exc: SwigException      
ILwd::LdasContainer* createRawData( const char* name );
ILwd::LdasContainer* createOuterDetectorProcContainer();   
ILwd::LdasContainer* createOuterDetectorSimContainer();      
ILwd::LdasContainer* createOuterHistoryContainer();         
#endif /* DEPRICATED */


//-----------------------------------------------------------------------------
// Frame Conversion
//-----------------------------------------------------------------------------

#if DEPRICATED
FrameCPP::FrameH* ilwd2frame( ILwd::LdasContainer* c );
CREATE_THREADED1_DECL( ilwd2frame, FrameCPP::FrameH*, ILwd::LdasContainer* );
//!exc: SwigException      
ILwd::LdasContainer* concatAdcDataList( std::vector<ILwd::LdasContainer*> );
CREATE_THREADED1_DECL( concatAdcDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> );
//!exc: SwigException      
ILwd::LdasContainer* concatProcDataList( std::vector<ILwd::LdasContainer*>& );
CREATE_THREADED1_DECL( concatProcDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*>& );
//!exc: SwigException      
ILwd::LdasContainer* concatSimDataList( std::vector<ILwd::LdasContainer*> );
CREATE_THREADED1_DECL( concatSimDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> );
//!exc: SwigException      
ILwd::LdasContainer* concatSerDataList( std::vector<ILwd::LdasContainer*> );
CREATE_THREADED1_DECL( concatSerDataList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> );
//!exc: SwigException      
ILwd::LdasContainer* concatFrameList( std::vector<ILwd::LdasContainer*> );
CREATE_THREADED1_DECL( concatFrameList, ILwd::LdasContainer*, std::vector<ILwd::LdasContainer*> );
#endif /* DEPRICATED */

//!ignore_end:

#endif // FrameCmdHH
