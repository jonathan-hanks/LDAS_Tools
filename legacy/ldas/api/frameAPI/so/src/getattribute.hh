#ifndef GetAttributeHH
#define GetAttributeHH

#include <deque>
#include <sstream>

#include "framecpp/FrameCPP.hh"
#include "framecpp/Common/Container.hh"
#include "framecpp/Common/SearchContainer.hh"

#include "genericAPI/swigexception.hh"

#include "query.hh"

//!ignore_begin:

//!exc: SwigException   
void tokenize( const std::string& s, std::deque< Query >& q );



template< class T >
const T& getContained(
    const FrameCPP::Common::Container< T >& c,
    Query& q, std::vector< size_t >& start, size_t index )
{
    if ( index == start.size() )
    {
        start.push_back( 0 );
    }

    if ( q.getQuery() == "index" )
    {
      size_t i;
      std::istringstream b( q.getValue( ) );
      b >> i;
      if ( i >= c.size() )
      {
	throw SWIGEXCEPTION( "not_found" );
      }

      start[ index ] = i;
      return *c[ i ];
    }
    else
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
}


template< class T, const std::string& (T::*F)() const >
const T& getContained(
    const FrameCPP::Common::SearchContainer< T, F >& c,
    Query& q, std::vector< size_t >& start, size_t index )
{
    if ( index == start.size() )
    {
        start.push_back( 0 );
    }

    if ( q.getQuery() == "index" )
    {
      size_t	i;
      std::istringstream b( q.getValue( ) );
      b >> i;
      if ( i >= c.size() )
      {
	throw SWIGEXCEPTION( "not_found" );
      }

      start[ index ] = i;
      return *c[ i ];
    }

    typename FrameCPP::Common::SearchContainer< T, F >::const_iterator siter(
        c.begin() + start[ index ] );
    if ( siter < c.begin() || siter >= c.end() )
    {
        throw SWIGEXCEPTION( "not_found" );
    }
    
    if ( q.getQuery() == "name" )
    {
        typename FrameCPP::Common::SearchContainer< T, F >::const_iterator iter(
            c.find( q.getValue(), siter ) );
        if ( iter == c.end() )
        {
            throw SWIGEXCEPTION( "not_found" );
        }

        start[ index ] = iter - c.begin();
        return **iter;
    }
    else if ( q.getQuery() == "regex" )
    {
        typename FrameCPP::Common::SearchContainer< T, F >::const_iterator iter(
            c.regexFind( q.getValue(), siter ) );
        if ( iter == c.end() )
        {
            throw SWIGEXCEPTION( "not_found" );
        }

        start[ index ] = iter - c.begin();
        return **iter;
    }
    else
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
}

template< class T >
std::string getAttribute(
    const FrameCPP::Common::Container< T >& c,
    std::deque< Query >& dq )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    std::ostringstream res;

    if ( q.isQuery() || q.getName() != "size" )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    res << c.size();
    return res.str();
}

template< class T, const std::string& (T::*F)() const >
std::string getAttribute(
    const FrameCPP::Common::SearchContainer< T, F >& c,
    std::deque< Query >& dq )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    std::ostringstream res;

    if ( q.isQuery() || q.getName() != "size" )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    res << c.size();
    return res.str();
}

//!ignore_end:
#endif // GetAttributeHH
