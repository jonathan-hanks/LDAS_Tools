/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "ldas_tools_config.h"

// System Header Files
#include <cmath>
#include <sstream>   

// Local Header Files
#include "FrameException.hh"
#include "frprocdata.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frhistory.hh"
#include "frtable.hh"
#include "frvect.hh"
#include "util.hh"


using FrameCPP::FrHistory;
using FrameCPP::FrProcData;
using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

#if ! CORE_API
#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasArrayBase;
using ILwd::LdasElement;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
#endif /* ! CORE_API */

using namespace std;   
   

namespace FrameAPI
{
#if CORE_API
  template <>
  REAL_8
  SampleRate( const FrameCPP::FrProcData& C )
  {
      switch( C.GetType( ) )
      {
      case FrameCPP::FrProcData::TIME_SERIES:
	  {
	      if ( C.RefData( )[ 0 ] )
	      {
		  return REAL_8( 1.0 /
				 C.RefData( )[ 0 ]->GetDim( 0 ).GetDx( ) );
	      }
	  }
	  break;
      default:
	  break;
      }
      std::ostringstream	oss;

      oss << "Unable to obtain or calculate the sample rate";
      throw SWIGEXCEPTION( oss.str( ) );
  }
#endif /* CORE_API */
}

enum
{
    FR_NAME,
    FR_COMMENT,
    FR_SAMPLERATE,
    FR_TIMEOFFSETS,
    FR_TIMEOFFSETN,
    FR_FSHIFT,
    FR_PHASE,
    FR_DATA,
    FR_TABLE,
    FR_AUX
};


//!ignore_begin:
#if CORE_API
QueryHash initProcDataHash()
{
    QueryHash h;
    h[ "name"        ] = FR_NAME;
    h[ "comment"     ] = FR_COMMENT;
    h[ "samplerate"  ] = FR_SAMPLERATE;
    h[ "timeoffsets" ] = FR_TIMEOFFSETS;
    h[ "timeoffsetn" ] = FR_TIMEOFFSETN;
    h[ "fshift"      ] = FR_FSHIFT;
    h[ "phase"       ] = FR_PHASE;
    h[ "data"        ] = FR_DATA;
    h[ "table"       ] = FR_TABLE;
    h[ "aux"         ] = FR_AUX;
    return h;
}


QueryHash procDataHash( initProcDataHash() );    
#else
extern QueryHash procDataHash;
#endif /* CORE_API */
    
    
using namespace FrameAPI::FrProcData;

#if CORE_API
FrameCPP::FrProcData* FrameAPI::FrProcData::
cloneHeader( const FrameCPP::FrProcData& Source )
{
  FrameCPP::FrProcData* ret
      = new FrameCPP::FrProcData( Source.GetName( ),
				  Source.GetComment( ),
				  Source.GetType( ),
				  Source.GetSubType( ),
				  Source.GetTimeOffset( ),
				  Source.GetTRange( ),
				  Source.GetFShift( ),
				  Source.GetPhase( ),
				  Source.GetFRange( ),
				  Source.GetBW( ) );
  return ret;
}
#endif /* CORE_API */

#if CORE_API
FrameCPP::FrProcData* FrameAPI::FrProcData::
concat( const std::list< FrameCPP::Base* >& Segment )
{
    //:TODO: Need to ad support for aux data
    //-------------------------------------------------------------------
    // Loop over the parts getting the statistics
    //-------------------------------------------------------------------

    INT_4U	samples( 0 );

    for( std::list< FrameCPP::Base* >::const_iterator s( Segment.begin() );
	 s != Segment.end( );
	 s++ )
    {
      FrameCPP::FrProcData*	src( dynamic_cast< FrameCPP::FrProcData* >(*s) );

      samples += FrameAPI::FrVect::getSamples( src->RefData( ).begin( ),
					       src->RefData( ).end( ) );
    }
    
    //-------------------------------------------------------------------
    // Allocate a single channel to hold the data
    //-------------------------------------------------------------------
    std::unique_ptr< FrameCPP::FrProcData >
      c( FrameAPI::FrProcData::cloneHeader( *dynamic_cast< FrameCPP::FrProcData* >
					   ( Segment.front( ) ) ) );
    c->SetTRange( 0 );
    //-------------------------------------------------------------------
    // Fill in the data
    //-------------------------------------------------------------------

    FrameAPI::FrVect::FrVectList	vector_list;

    for( std::list< FrameCPP::Base* >::const_iterator s( Segment.begin() );
	 s != Segment.end( );
	 s++ )
    {
      FrameCPP::FrProcData*	src( dynamic_cast< FrameCPP::FrProcData* >(*s) );

      // Append all comments
      c->AppendComment( src->GetComment( ) );
      c->SetTRange( src->GetTRange( ) + c->GetTRange( ) );
      if ( s == Segment.begin( ) )
      {
	  c->SetFRange( src->GetFRange( ) );
      }
      else
      {
	  if ( c->GetFRange( ) != src->GetFRange( ) )
	  {
	      throw std::logic_error( "Unable to concatinate channels due to varying Frequency Ranges" );
	  }
      }
      // Append auxiliary information
      FrameAPI::FrVect::appendStructures( c->RefAux( ), src->RefAux( ) );
      // Build a list of vectors to concat
      vector_list.push_back( &( src->RefData( ) ) );
    }
    //-----------------------------------------------------------------
    // Create the new vector and attach it to the channel
    //-----------------------------------------------------------------
    {
	FrameCPP::FrProcData::data_type::value_type
	    d( FrameAPI::FrVect::concat( vector_list ) );

	c->RefData( ).append( d );
    }
    //-----------------------------------------------------------------
    // Return the results
    //-----------------------------------------------------------------

    return c.release( );
}

#endif /* CORE_API */

//-----------------------------------------------------------------------------

#if CORE_API

std::string getAttribute( const FrProcData& procData, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        procDataHash.find( q.getName().c_str() );
    if ( iter == procDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << procData.GetName();
                break;
                
            case FR_COMMENT:
                res << procData.GetComment();
                break;
                
            case FR_SAMPLERATE:
                res << FrameAPI::SampleRate( procData );
                break;

            case FR_TIMEOFFSETS:
                res << INT_4S( procData.GetTimeOffset( ) );
                break;
                
            case FR_TIMEOFFSETN:
	        {
		    REAL_8 x = procData.GetTimeOffset( );
		    x -= INT_4S( x );
		    x *= 1000000000L;
		    res << x;
		}
                break;

            case FR_FSHIFT:
                res << procData.GetFShift();
                break;

            case FR_PHASE:
                res << procData.GetPhase();
                break;

            case FR_DATA:
                return getAttribute( procData.RefData(), dq );

            case FR_TABLE:
                return getAttribute( procData.RefTable(), dq );
                
            case FR_AUX:
                return getAttribute( procData.RefAux(), dq );

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_DATA:
                return getAttribute( getContained( procData.RefData(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            case FR_TABLE:
                return getAttribute( getContained( procData.RefTable(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;
                
            case FR_AUX:
                return getAttribute( getContained( procData.RefAux(),
						   q, start, index ),
				     dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}

#endif /* CORE_API */


//-----------------------------------------------------------------------------

#if ! CORE_API

LdasElement* getData( const FrProcData& procData, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index,
                      const Time& gtime )
{
    if ( dq.size() == 0 )
    {
        return procData2container( procData, gtime );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        procDataHash.find( q.getName().c_str() );
    if ( iter == procDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_DATA:
            return getData( getContained( procData.RefData(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;

        case FR_TABLE:
            return getData( getContained( procData.RefTable(),
					  q, start, index ),
			    dq, start, index + 1 );
            break;
                
        case FR_AUX:
            return getData( getContained( procData.RefAux(), q, start, index ),
			    dq, start, index + 1 );
            break;

        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}

#endif /* ! CORE_API */

//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//:Convert an LdasContainer to FrProcData.
//
//!usage_ooi: FrProcData* procData = container2procData( procDataContainer );
//
//!param: const LdasContainer& c - FrProcData LDAS container.
//
//!return: FrProcData* procData - FrameCPP::FrProcData structure pointer
//+ 	(newly allocated).
//!exc: SwigException - invalid container.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

FrProcData* container2procData( const LdasContainer& c )
{
    //-------------------------------------------------------------------
    // Check if the container is possible
    //-------------------------------------------------------------------

    const string& c_name( c.getName( 2 ) );
    if ( strcasecmp( c_name.c_str(), "procData" ) != 0 )
    {
        throw SWIGEXCEPTION( "FrProcData: invalid container." );
    }
    
    //-------------------------------------------------------------------
    // core
    //-------------------------------------------------------------------

    std::string comment( "" );
    INT_2U	type( 0 );
    INT_2U	subType( 0 );
    REAL_8	timeOffset( 0 );
    REAL_8	tRange( 0.0 );
    REAL_8	fShift( 0.0 );
    REAL_4	phase( 0.0 );
    REAL_8	fRange( 0.0 );
    REAL_8	bw( 0.0 );

    try
    {
	comment = findLStringType( c, "comment" ).getString();
    }
    catch( ... )
    {
    }
    try
    {
	type = *findArrayType< INT_2U >( c, "type" ).getData( );
    }
    catch( ... )
    {
    }
    try
    {
	subType = *findArrayType< INT_2U >( c, "subType" ).getData( );
    }
    catch( ... )
    {
    }
    try
    {
	timeOffset = *findArrayType< REAL_8 >( c, "timeOffset" ).getData( );
    }
    catch( ... )
    {
    }
    try
    {
	tRange = *findArrayType< REAL_8 >( c, "tRange" ).getData( );
    }
    catch( ... )
    {
    }
    try
    {
	fShift = *findArrayType< REAL_8 >( c, "fShift" ).getData( );
    }
    catch( ... )
    {
    }
    try
    {
	phase = *findArrayType< REAL_4 >( c, "phase" ).getData( );
    }
    catch( ... )
    {
    }
    try
    {
	fRange = *findArrayType< REAL_8 >( c, "fRange" ).getData( );
    }
    catch( ... )
    {
    }
    try
    {
	bw = *findArrayType< REAL_8 >( c, "BW" ).getData( );
    }
    catch( ... )
    {
    }

    std::unique_ptr< FrProcData >
	proc( new FrProcData( c.getName( 0 ),
			      comment,
			      type,
			      subType,
			      timeOffset,
			      tRange,
			      fShift,
			      phase,
			      fRange,
			      bw ) );
			

    //-------------------------------------------------------------------
    // data
    //-------------------------------------------------------------------

    try
    {
        const LdasContainer* data( findContainerType( c, "data" ) );
        if ( data != 0 )
        {
            for ( unsigned int i = 0; i < data->size(); ++i )
            {
		FrameCPP::FrProcData::data_type::value_type
		    v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*data)[ i ] ) ) );

                proc->RefData( ).append( v );
            }
        }
    }
    catch( std::bad_cast& )
    {
      throw SWIGEXCEPTION( "FrProcData: invalid data format" );
    }
    
    //-------------------------------------------------------------------
    // aux
    //-------------------------------------------------------------------

    try
    {
        const LdasContainer* aux( findContainerType( c, "aux" ) );
        if ( aux != 0 )
        {
            for ( unsigned int i = 0; i < aux->size(); ++i )
            {
		FrameCPP::FrProcData::aux_type::value_type
		    v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*aux)[ i ] ) ) );

                proc->RefAux().append( v );
            }
        }
    }
    catch( std::bad_cast& )
    {
      throw SWIGEXCEPTION( "FrProcData: invalid aux format" );
    }
    
    //-------------------------------------------------------------------
    // table
    //-------------------------------------------------------------------

    try
    {
        const LdasContainer* table( findContainerType( c, "table" ) );
        if ( table != 0 )
        {
            for ( unsigned int i = 0; i < table->size(); ++i )
            {
		FrameCPP::FrProcData::table_type::value_type
		    t( container2table( dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) ) );

                proc->RefTable( ).append( t );
            }
        }
    }
    catch( std::bad_cast& )
    {
      throw SWIGEXCEPTION( "FrProcData: invalid aux format" );
    }
    
    //-------------------------------------------------------------------
    // history
    //-------------------------------------------------------------------

    try
    {
        const LdasContainer* history( findContainerType( c, "history" ) );
        if ( history != 0 )
        {
            for ( unsigned int i = 0; i < history->size(); ++i )
            {
		FrameCPP::FrProcData::history_type::value_type
		    h( container2history( dynamic_cast< const LdasContainer& >( *(*history)[ i ] ) ) );

                proc->RefHistory( ).append( h );
            }
        }
    }
    catch( std::bad_cast& )
    {
      throw SWIGEXCEPTION( "FrProcData: invalid aux format" );
    }
    
    //-------------------------------------------------------------------
    // Return the results
    //-------------------------------------------------------------------

    return proc.release( );
}

#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Convert FrProcData to ILWD.
//
//!usage_ooi: LdasContainer* procDataContainer = procData2container( proc, gtime);
//
//!param: const FrProcData& proc - FrameCPP::FrProcData structure pointer.
//!param: const Time& gtime - FrameCPP::Frame time.
//
//!return: LdasContainer* procDataContainer - newly allocated LDAS container.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

LdasContainer* procData2container( const FrProcData& proc, const Time& gtime )
{
    std::ostringstream	sec;
    std::stringstream	nsec;

    sec << gtime.getSec( );
    nsec << gtime.getNSec( );

    LdasContainer* c = new LdasContainer( "::ProcData" );
    c->setName( 0, proc.GetName() );
    
    c->appendName( sec.str() );
    c->appendName( nsec.str() );

    c->appendName( "Frame" );

    c->push_back( new LdasString( proc.GetComment( ), "comment" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< INT_2U >( proc.GetType( ), "type" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< INT_2U >( proc.GetSubType( ), "subType" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< REAL_8 >( proc.GetTimeOffset( ),
					   "timeOffset" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< REAL_8 >( proc.GetTRange( ), "tRange" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< REAL_8 >( proc.GetFShift( ), "fShift" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< REAL_4 >( proc.GetPhase( ), "phase" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< REAL_8 >( proc.GetFRange( ), "fRange" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( new LdasArray< REAL_8 >( proc.GetBW( ), "BW" ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

    size_t 	dataSize( proc.RefData().size() );
    if ( dataSize > 0 )
    {
        LdasContainer*
	    data( new LdasContainer( ":data:Container(Vect):Frame" ) );
        c->push_back( data,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrProcData::const_iterator iter( proc.RefData().begin() );
        for( size_t i = dataSize; i != 0; --i, ++iter )
        {
            data->push_back( vect2array( **iter ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
        }
    }

    size_t auxSize( proc.RefAux().size() );
    if ( auxSize > 0 )
    {
        LdasContainer*
	    aux( new LdasContainer( ":aux:Container(Vect):Frame" ) );
        c->push_back( aux,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrProcData::const_aux_iterator iter( proc.RefAux().begin() );
        for( size_t i = auxSize; i != 0; --i, ++iter )
        {
            aux->push_back( vect2array( **iter ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
        }
    }
    
    size_t tableSize( proc.RefTable().size() );
    if ( tableSize > 0 )
    {
        LdasContainer*
	    table( new LdasContainer( ":table:Container(Table):Frame" ) );
        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrProcData::const_table_iterator iter( proc.RefTable().begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }

    size_t historySize( proc.RefHistory().size() );
    if ( tableSize > 0 )
    {
        LdasContainer*
	    history( new LdasContainer( ":history:Container(History):Frame" ) );
        c->push_back( history,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrProcData::const_history_iterator iter( proc.RefHistory().begin() );
        for( size_t i = historySize; i != 0; --i, ++iter )
        {
            history->push_back( history2container( **iter ),
				ILwd::LdasContainer::NO_ALLOCATE,
				ILwd::LdasContainer::OWN );
        }
    }

    return c;
}

#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//:Insert FrProcData container into Frame container.
//
//!usage_ooi: insertProcData( fr, proc );
//
//!param: LdasContainer& fr - frame container.
//!param: LdasContainer& proc -- procData container.
//!param: const bool validateTime - A flag to enable/disable time stamp
//+       validation for data insertion. True to enable, false to disable.       
//+       Default is TRUE.      
//
//!exc: SwigException -  bad container data.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

void insertProcData( LdasContainer& fr, LdasContainer& proc,
   const bool validateTime )
{
    // Find the GTime  elements of the frame container. 
    LdasElement* fr_gtime = fr.find( "gtime" );
    LdasElement* fr_dt = fr.find( "dt" );

    // Check to make sure the frame data is valid
    if ( fr_gtime == 0 || fr_gtime->getElementId() != ILwd::ID_INT_4U )
    {
       throw SWIGEXCEPTION( "bad_frame: gtime" );
    }  
   
    if( fr_dt == 0 || fr_dt->getElementId() != ILwd::ID_REAL_8 )
    {
       throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check the characteristics of the GTime element.
    LdasArray< INT_4U >& gtime(
        dynamic_cast< LdasArray< INT_4U >& >( *fr_gtime ) );
    if ( gtime.getNDim() != 1 || gtime.getDimension( 0 ) != 2 )
    {
        throw SWIGEXCEPTION( "bad_frame: gtime" );
    }

    // Check the characteristics of the Frame Dt element.
    LdasArray< REAL_8 >& fdt( dynamic_cast< LdasArray< REAL_8 >& >( *fr_dt ) );
    if ( fdt.getNDim() != 1 || fdt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // If 'fdt' is not zero, then we must have some data in the frame.  Check
    // to make sure the start times and length for the data matches that of the
    // frame.
    if( validateTime && fdt.getData()[ 0 ] != 0 )
    {
        INT_4U time;
        
        // Check the GPS seconds.
        std::string tmp( proc.getName( 3 ) );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 0 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(seconds): frame vs. procdata" );
        }

        // Check the GPS nanoseconds.
        tmp = proc.getName( 4 );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 1 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(nanoseconds): "
                                 "frame vs. procdata" );
        }
    }

    // Find the procdata container
    LdasElement* t = fr.find( "procdata", 1 );
    if ( t == 0 )
    {
        // it didn't exist so create it.
        fr.push_back( LdasContainer( ":procdata:Container(ProcData)" ) );
        t = fr.back();
    }
    // Check the procdata container
    else if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_frame: malformed ProcData" );
    }

#ifdef RESET_FRAME_TIME

    // If this is the first data element in the frame, set the frame GPS time
    // and length to correspond to the inserted object.
    if( gtime.getData()[ 0 ] == 0 && gtime.getData()[ 1 ] == 0 )
    {
        std::string tmp( proc.getName( 3 ) );
        gtime.getData()[ 0 ] = strtoul( tmp.c_str(), 0, 10 );
        tmp = proc.getName( 4 );
        gtime.getData()[ 1 ] = strtoul( tmp.c_str(), 0, 10 );
        fdt.getData()[ 0 ] = adt.getData()[ 0 ];
    }    
#endif

    // OK, everything checks out so add the procdata
    dynamic_cast< LdasContainer& >( *t ).push_back( proc );
    return;
}

#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//:Concatenate two FrProcData containers.
//
//!usage_ooi: LdasContainer* concatContainer = concatProcData( c1, c2 );
//
//!param: const LdasContainer* c1 - first FrProcData container.
//!param: const LdasContainer* c2 - second FrProcData container.
//
//!exc: SwigException -  invalid container data.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

LdasContainer* concatProcData(
   const LdasContainer* c1, const LdasContainer* c2 )
try   
{
    // Make sure this is frame data.
    if ( c1->getName( 5 ) != "Frame" ||
         c2->getName( 5 ) != "Frame" )
    {
        throw SWIGEXCEPTION( "bad_procdata: not_frame_data" );
    }
    
    // Make sure these are both procdata.
    string c1_name( c1->getName( 2 ) );
    string c2_name( c2->getName( 2 ) );   
   
    if ( strcasecmp( c1_name.c_str(), "procdata" ) != 0 ||
         strcasecmp( c2_name.c_str(), "procdata" ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_types" );
    }

    // Make sure the data/channel names are the same.
    c1_name = c1->getName( 0 );
    c2_name = c2->getName( 0 );
   
    if ( strcasecmp( c1_name.c_str(), c2_name.c_str() ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_channels" );
    }

    // Make sure that the sample rate exists, is the proper type, and is the
    // same on both containers.
    const LdasArray< REAL_8 >& sr1( findArrayType< REAL_8 >( *c1, "samplerate" ) ); 
    const LdasArray< REAL_8 >& sr2( findArrayType< REAL_8 >( *c2, "samplerate" ) );       

    if ( sr1 != sr2 ||
         sr1.getNDim() != 1 || sr1.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "incompatible_procdata: samplerate" );
    }

    // Make sure the fshift fields are the same.
    const LdasArray< REAL_8 >& f1( findArrayType< REAL_8 >( *c1, "fshift" ) ); 
    const LdasArray< REAL_8 >& f2( findArrayType< REAL_8 >( *c2, "fshift" ) );    

    if ( f1 != f2 )
    {
        throw SWIGEXCEPTION( "incompatible_procdata: fshift" );
    }


    // Make sure the phase fields are the same.
    const LdasArray< REAL_4 >& p1( findArrayType< REAL_4 >( *c1, "phase" ) ); 
    const LdasArray< REAL_4 >& p2( findArrayType< REAL_4 >( *c2, "phase" ) );    

    if ( p1 != p2 )
    {
        throw SWIGEXCEPTION( "incompatible_procdata: phase" );
    }

    // Find the timeOffset fields and make sure they are the right type.
    const LdasArray< REAL_8 >&
	t1( findArrayType< REAL_8 >( *c1, "timeoffset" ) );
    const LdasArray< REAL_8 >&
	t2( findArrayType< REAL_8 >( *c2, "timeoffset" ) );   

    if ( t1 != t2 )
    {
        throw SWIGEXCEPTION( "bad_procdata: timeoffset" );
    }
    
    if( ! FrameAPI::Continuous( *c1, *c2 ) )
    {
        throw GAP_EXCEPTION( "incompatible_adcdata: The second FrProcData "
                             "structure does not start where the first "
                             "ended." );       
    }
    
    // Get the GPS Start time from the container names.
    // What if the strtoul fails? Use strstream?

    // Get the sub-containers containing the data.
    const LdasContainer* cn1( findContainerType( *c1, "data" ) );
    const LdasContainer* cn2( findContainerType( *c2, "data" ) );  
    if( cn1 == 0 || cn1->size() == 0 )
    {
       throw SWIGEXCEPTION( "bad_procdata: data" );
    }

    LdasContainer* concatContainer( 0 );
    try
    {
       // Now create a copy of the first container and replace its data with
       // the new data.
       concatContainer = new LdasContainer( *c1 );
       LdasContainer* cdata( dynamic_cast< LdasContainer* >(
          concatContainer->find( "data", 1 ) ) );
       cdata->erase( cdata->begin(), cdata->end() );
       cdata->push_back( concatElementData( cn1, cn2 ),
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );

       // Concatenate the comments.
       LdasElement* e = concatContainer->find( "comment" );   
       if( e == 0 || e->getElementId() != ILwd::ID_LSTRING )
       {
          throw SWIGEXCEPTION( "bad_procdata: comment" );
       }
       const LdasString& comment( findLStringType( *c2, "comment" ) );   
       if( comment.getString().empty() == false )
       {
          dynamic_cast< LdasString& >( *e ).setString( 
             dynamic_cast< const LdasString& >( *e ).getString() + 
             "\n" + comment.getString() );    
       }

       // Concatenate 'table' data if it's present in the container:
       LdasElement* elem = concatContainer->find( "table", 1 );
       if( elem != 0 )
	 {
	   if( elem->getElementId() != ILwd::ID_ILWD )
	     {
	       throw SWIGEXCEPTION( "bad_procdata: table" );
	     } 
	   cn1 = findContainerType( *c1, "table" );
	   cn2 = findContainerType( *c2, "table" );
	   if( cn1->size() == 0 || cn2 == 0 ||
             cn1->size() != cn2->size() ) 
	     {
	       throw SWIGEXCEPTION( "incompatible_procdata: table" );
	     }

	   cdata = dynamic_cast< LdasContainer* >( elem );
	   cdata->erase( cdata->begin(), cdata->end() );
	   for ( unsigned int i = 0; i < cn1->size(); ++i )
	     {
	       cdata->push_back( concatTable
				 ( dynamic_cast< const LdasContainer* >
				   ( ( *cn1 )[ i ] ),
				   dynamic_cast< const LdasContainer* >
				   ( ( *cn2 )[ i ] ) ),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
	     }
	 }

       // Concatenate 'aux' data if it's present in the container:
       cn1 = findContainerType( *c1, "aux" );
       cn2 = findContainerType( *c2, "aux" );   
       if( cn1 == 0 && cn2 == 0 )
       {
          return concatContainer;
       }

       if ( cn1 == 0 || cn1->size() == 0 )
       {
          throw SWIGEXCEPTION( "bad_procdata: aux" );
       }
   
       cdata = dynamic_cast< LdasContainer* >(
          concatContainer->find( "aux", 1 ) );
       cdata->erase( cdata->begin(), cdata->end() );
       cdata->push_back( concatElementData( cn1, cn2 ),
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
    }
    catch(...)
    {
       delete concatContainer;
       concatContainer = (ILwd::LdasContainer*)NULL;
       throw;
    }

    return concatContainer;
}
catch( std::bad_cast& )
{
   throw SWIGEXCEPTION( "procdata: invalid_input_format" );
}

#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//:Test if two proc containers can be concatinated
//
//!usage: bool can_concat = concatProcDataCheck( c1, c2 );
//
//!param: const LdasContainer* c1 - first FrProcData container.
//!param: const LdasContainer* c2 - second FrProcData container.
//
//!exc: SwigException -  invalid container data.
//
//-----------------------------------------------------------------------------

#if ! CORE_API

bool
concatProcDataCheck( const LdasContainer* c1, const LdasContainer* c2 )
{
    // Make sure this is frame data.
    if ( c1->getName( 5 ) != "Frame" ||
         c2->getName( 5 ) != "Frame" )
    {
        throw SWIGEXCEPTION( "bad_procdata: not_frame_data" );
    }
    
    // Make sure these are both procdata.
    string c1_name( c1->getName( 2 ) );
    string c2_name( c2->getName( 2 ) );   
   
    if ( strcasecmp( c1_name.c_str(), "procdata" ) != 0 ||
         strcasecmp( c2_name.c_str(), "procdata" ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_types" );
    }

    // Make sure the data/channel names are the same.
    c1_name = c1->getName( 0 );
    c2_name = c2->getName( 0 );
   
    if ( strcasecmp( c1_name.c_str(), c2_name.c_str() ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_channels" );
    }

    // Make sure that the sample rate exists, is the proper type, and is the
    // same on both containers.
    const LdasArray< REAL_8 >& sr1( findArrayType< REAL_8 >( *c1, "samplerate" ) ); 
    const LdasArray< REAL_8 >& sr2( findArrayType< REAL_8 >( *c2, "samplerate" ) );       

    if ( sr1 != sr2 ||
         sr1.getNDim() != 1 || sr1.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "incompatible_procdata: samplerate" );
    }

    // Make sure the fshift fields are the same.
    const LdasArray< REAL_8 >& f1( findArrayType< REAL_8 >( *c1, "fshift" ) ); 
    const LdasArray< REAL_8 >& f2( findArrayType< REAL_8 >( *c2, "fshift" ) );    

    if ( f1 != f2 )
    {
        throw SWIGEXCEPTION( "incompatible_procdata: fshift" );
    }


    // Make sure the phase fields are the same.
    const LdasArray< REAL_4 >& p1( findArrayType< REAL_4 >( *c1, "phase" ) ); 
    const LdasArray< REAL_4 >& p2( findArrayType< REAL_4 >( *c2, "phase" ) );    

    if ( p1 != p2 )
    {
        throw SWIGEXCEPTION( "incompatible_procdata: phase" );
    }

    // Find the timeOffset fields and make sure they are the right type.
    const LdasArray< REAL_8 >&
	t1( findArrayType< REAL_8 >( *c1, "timeoffset" ) );
    const LdasArray< REAL_8 >&
	t2( findArrayType< REAL_8 >( *c2, "timeoffset" ) );   
    if ( t1 != t2 )
    {
        throw SWIGEXCEPTION( "bad_procdata: timeoffset" );
    }
    
    if( ! FrameAPI::Continuous( *c1, *c2 ) )
    {
        throw GAP_EXCEPTION( "incompatible_adcdata: The second FrProcData "
                             "structure does not start where the first "
                             "ended." );       
    }
    
    // Get the GPS Start time from the container names.
    // What if the strtoul fails? Use strstream?

    // Get the sub-containers containing the data.
    const LdasContainer* cn1( findContainerType( *c1, "data" ) );
    if( cn1 == 0 || cn1->size() == 0 )
    {
       throw SWIGEXCEPTION( "bad_procdata: data" );
    }

    return true;
}

#endif /* ! CORE_API */
