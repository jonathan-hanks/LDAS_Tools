#include "config.h"

#include <cmath>
#include <list>

#include "general/gpstime.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ilwdtoc.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

typedef std::list<ILwd::TOC>	time_object_list_type;

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrRawData.hh"
#include "ilwdfcs/FrVect.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if 0
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl
#else /* 0 */
#define	AT()
#endif	/* 0 */

using std::ceil;


namespace
{
  //---------------------------------------------------------------------
  // Create a list of Keys
  //---------------------------------------------------------------------

  enum {
    TIME_OFFSET,		// Time Offset (version 4)
    TIME_OFFSET_SECONDS,	// Relative time (seconds)
    TIME_OFFSET_NANOSECONDS,	// Relative time (nanoseconds)
    TIME_SECONDS,		// Absolute time (seconds)
    TIME_NANOSECONDS,		// Absolute time (nanoseconds)
    SAMPLE_RATE,		// Sample rate
    DATA,			// Place were data is stored
    DELTA_TIME			// Delta time
  };

  struct time_object_info_type
  {
    bool	is_absolute_time;
  };

  //---------------------------------------------------------------------
  // Forward declaration of local functions
  //---------------------------------------------------------------------

  bool init( );

#if HAVE_LDAS_PACKAGE_ILWD
  void add_time_objects( ILwd::TOC::key_type Key, const ILwd::TOC& Container,
			 time_object_list_type& TimeObjects );

  INT_4U get_sample_count( const ILwd::LdasContainer& Vector );

  bool read_absolute_time( const ILwd::TOC& TimeObject,
			   General::GPSTime& Start,
			   General::GPSTime& End );

  General::GPSTime
  read_start_time_and_delta( const ILwd::TOC& TimeObject,
			     REAL_8& Delta );

  REAL_8
  read_time_offset( const ILwd::TOC& TimeObject );

  bool read_relative_time( const ILwd::TOC& TimeObject,
			   General::GPSTime& Start,
			   General::GPSTime& End );

  bool write_relative_time( ILwd::TOC& TimeObject,
			    const General::GPSTime& Start,
			    const General::GPSTime& End );

  void
  write_time_offset( ILwd::TOC& TimeObject,
		     const General::GPSTime& Offset );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

  //---------------------------------------------------------------------
  // Declaration and initialization of local variables.
  //---------------------------------------------------------------------

#if HAVE_LDAS_PACKAGE_ILWD
  ILwd::TOC::Keys_type	time_keys;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

  static const bool initialized( init( ) );
}
//: Normalizeb the time info for the frame
//
//

void normalizeFrameTime( ILwd::LdasContainer* Frame )
{
  using General::GPSTime;

  using ILwd::LdasContainer;
  using ILwd::TOC;

  using ILwdFCS::FrameH;
  using ILwdFCS::FrRawData;

  AT();
  //---------------------------------------------------------------------
  // Calculate what the values should be
  //---------------------------------------------------------------------

  TOC	frame_toc( *Frame, FrameH::TOCKeys );

  General::GPSTime	start_time(0,0);
  General::GPSTime	end_time(0,0);
  REAL_8		delta;
  
  AT();
  time_object_list_type	time_objects;

  //---------------------------------------------------------------------
  // Create a list of objects to review
  //---------------------------------------------------------------------

  //---------------------------------------------------------------------
  // FrProcData
  //---------------------------------------------------------------------

  AT();
  add_time_objects( FrameH::PROC_DATA, frame_toc, time_objects );
  
  //---------------------------------------------------------------------
  // FrRawData
  //---------------------------------------------------------------------
  try
  {
    AT();
    LdasContainer& c = frame_toc.GetILwd<ILwd::LdasContainer>
      ( FrameH::RAW_DATA );
    AT();
    TOC	raw_data_typeoc( c, FrRawData::TOCKeys );

    AT();
    add_time_objects( FrRawData::SERIAL_DATA, raw_data_typeoc, time_objects );
    AT();
    add_time_objects( FrRawData::ADC_DATA, raw_data_typeoc, time_objects );
  }
  catch( ... )
  {
  }
  
  //---------------------------------------------------------------------
  // Create a list of important data about each time object
  //---------------------------------------------------------------------

  std::list<time_object_info_type>	to_info;

  //---------------------------------------------------------------------
  // Loop through objects to find appropriate values for start time and
  //   end time
  //---------------------------------------------------------------------

  AT();
  for ( time_object_list_type::const_iterator to = time_objects.begin();
	to != time_objects.end();
	to++ )
  {
    to_info.push_back( time_object_info_type() );
    if ( read_absolute_time( *to, start_time, end_time ) )
    {
      to_info.back().is_absolute_time = true;
    }
    else
    {
      to_info.back().is_absolute_time = false;
      read_relative_time( *to, start_time, end_time );
    }
  }

  if ( ( GPSTime(0,0) == start_time ) &&
       ( GPSTime(0,0) == end_time ) )
  {
    try
    {
      // Version 5 of frame files
      start_time = General::GPSTime( frame_toc.GetData<INT_4U>
				     ( FrameH::GPS_TIME_SECONDS),
				     frame_toc.GetData<INT_4U>
				     ( FrameH::GPS_TIME_NANOSECONDS) );
    }
    catch( ... )
    {
      // Version 4 of frame files
      ILwd::LdasArray< INT_4U >& start
	( frame_toc.GetILwd<ILwd::LdasArray< INT_4U > >( FrameH::GPS_TIME ) );
      
      start_time = General::GPSTime( start.getData()[0], start.getData()[1] );
    }
    delta = frame_toc.GetData<REAL_8>( FrameH::DELTA_TIME, 0 );
    end_time = start_time + delta;
  }

  //---------------------------------------------------------------------
  // Ensure starting on second boundry
  //---------------------------------------------------------------------

  start_time = start_time + ( GPSTime( 0, 0) -
			      GPSTime(0, start_time.GetNanoseconds() ) );

  //---------------------------------------------------------------------
  // Recalculate delta
  //---------------------------------------------------------------------

  AT();
  delta = end_time - start_time;
  delta = ceil( delta );

  //---------------------------------------------------------------------
  // Adjust the time values in the frame header
  //---------------------------------------------------------------------

  try
  {
    // Version 5 of frame files
    AT();
    frame_toc.SetData( FrameH::GPS_TIME_SECONDS, start_time.GetSeconds( ) );
    AT();
    frame_toc.SetData( FrameH::GPS_TIME_NANOSECONDS,
		       start_time.GetNanoseconds( ) );
  }
  catch( ... )
  {
    AT();
    // Version 4 of frame files
    ILwd::LdasArray< INT_4U >& start
      ( frame_toc.GetILwd<ILwd::LdasArray< INT_4U > >( FrameH::GPS_TIME ) );
    
    start.getData()[0] = start_time.GetSeconds( );
    start.getData()[1] = start_time.GetNanoseconds( );
  }
  AT();
  frame_toc.SetData( FrameH::DELTA_TIME, delta );
  
  //---------------------------------------------------------------------
  // Loop through objects to adjust the time data
  //---------------------------------------------------------------------

  AT();
  for ( time_object_list_type::iterator to = time_objects.begin();
	to != time_objects.end();
	to++, to_info.pop_front() )
  {
    if ( ! to_info.front().is_absolute_time )
    {
      AT();
      write_relative_time( *to, start_time, end_time );
    }
  }
  AT();
}

namespace
{
  using ILwd::LdasContainer;
  using ILwd::LdasArrayBase;

  void
  add_time_objects( ILwd::TOC::key_type Key, const ILwd::TOC& Container,
		    time_object_list_type& TimeObjects )
  try {
    AT();
    const LdasContainer& c( Container.GetILwd<LdasContainer>( Key ) );
    
    for ( LdasContainer::const_iterator e = c.begin();
	  e != c.end();
	  e++ )
    {
      if ( *e )
      {
	AT();
	TimeObjects.push_back( ILwd::TOC( *(*e), time_keys ) );
      }
    }
  }
  catch( ... )
  {
    AT();
  }

  INT_4U
  get_sample_count( const ILwd::LdasContainer& Vector )
  {
    using ILwd::LdasContainer;
    using ILwd::TOC;

    INT_4U		retval( 0 );

    ILwd::LdasArrayBase*	base( (ILwd::LdasArrayBase*)NULL );
    for ( LdasContainer::const_iterator c = Vector.begin();
	  c != Vector.end();
	  c++ )
    {
      base = dynamic_cast<LdasArrayBase*>( *c );
      if ( base )
      {
	for ( unsigned int x = 0; x < base->getNDim(); x++ )
	{
	  retval += base->getDimension( x );
	}
      }
    }
    return retval;
  }

  bool
  init( )
  {
    time_keys[ "timeOffset" ] = TIME_OFFSET;
    time_keys[ "timeOffsetS" ] = TIME_OFFSET_SECONDS;
    time_keys[ "timeOffsetN" ] = TIME_OFFSET_NANOSECONDS;
    time_keys[ "timeS" ] = TIME_SECONDS;
    time_keys[ "timeN" ] = TIME_NANOSECONDS;
    time_keys[ "dt" ] = DELTA_TIME;
    time_keys[ "sampleRate" ] = SAMPLE_RATE;
    time_keys[ ":data:Container(Vect):Frame" ] = DATA;
    return true;
  }

  bool
  read_absolute_time( const ILwd::TOC& TimeObject,
		      General::GPSTime& Start,
		      General::GPSTime& End )
  {
    using General::GPSTime;

    try
    {
      AT();
      GPSTime	lstart( TimeObject.GetData<INT_4U>( TIME_SECONDS ),
			TimeObject.GetData<INT_4U>( TIME_NANOSECONDS ) );
      AT();
      GPSTime	lend( lstart );
      AT();
      REAL_8	sample_rate( TimeObject.GetData<REAL_8>( SAMPLE_RATE, 0.0 ) );
      AT();
      INT_4U	samples( get_sample_count( TimeObject.GetILwd<LdasContainer>
					   ( DATA ) ) );


      AT();
      if ( sample_rate == 0.0 )
      {
	AT();
	sample_rate = TimeObject.GetData<REAL_4>( SAMPLE_RATE );
      }

      AT();
      if ( ( lstart < Start ) || ( GPSTime(0,0) == Start ) )
      {
	AT();
	Start = lstart;
      }
      AT();
      lend += ( samples / sample_rate );
      if ( ( lend > End ) || ( GPSTime(0,0) == End ) )
      {
	AT();
	End = lend;
      }
      AT();
      return true;
    }
    catch( ... )
    {
      AT();
      return false;
    }
  }

  bool
  read_relative_time( const ILwd::TOC& TimeObject,
		      General::GPSTime& Start,
		      General::GPSTime& End )
  {
    using General::GPSTime;
    using ILwd::LdasContainer;

    try
    {
      REAL_8	delta;
      GPSTime	lstart = read_start_time_and_delta( TimeObject, delta );


      REAL_8	offset( read_time_offset( TimeObject ) );
      
      if ( offset < 0.0 )
      {
	lstart += offset;
      }
    
      GPSTime	lend( lstart + delta);


      if ( ( lstart < Start ) || ( GPSTime(0,0) == Start ) )
      {
	Start = lstart;
      }
      if ( ( lend > End ) || ( GPSTime(0,0) == End ) )
      {
	End = lend;
      }
      return true;
    }
    catch( ... )
    {
      return false;
    }
  }

  General::GPSTime
  read_start_time_and_delta( const ILwd::TOC& TimeObject,
			     REAL_8& Delta )
  {
    const LdasContainer& c( TimeObject.GetContainer() );
    
    AT();
    try
    {
      // Give presidence to the values in the metadata fields
      
      AT();
      Delta = c.getMetadata<REAL_8>( "delta_t" );
      AT();
      return c.getMetadata<General::GPSTime>( "start_time" );
    }
    catch( ... )
    {
      AT();
    }

    AT();
    INT_4U	sec;
    INT_4U	nanosec;
  
    //-------------------------------------------------------------------
    // Get the start time
    //-------------------------------------------------------------------
    AT();
    std::istringstream( c.getName(3) ) >> sec;
    std::istringstream( c.getName(4) ) >> nanosec;

    //-------------------------------------------------------------------
    // Calculate the delta
    //-------------------------------------------------------------------
    AT();
    REAL_8	sr( TimeObject.GetData<REAL_8>( SAMPLE_RATE ) );
    AT();
    INT_4U	samples( get_sample_count( TimeObject.GetILwd<LdasContainer>
					   ( DATA ) ) );

    if ( sr == -1.0 )
    {
      Delta = 1; // :TODO: Fix; Currently assume one second.
    }
    else
    {
      Delta = samples / sr;
    }
    
    //-------------------------------------------------------------------
    // Verify the dt in the time object if it is supplied
    //-------------------------------------------------------------------
    AT();
    try
    {
      AT();
      REAL_8 d = TimeObject.GetData<REAL_8>( DELTA_TIME );
      if ( sr == -1.0 )
      {
	//---------------------------------------------------------------
	// For frequency series, use the dt value if supplied
	//---------------------------------------------------------------
	Delta = d;
      }
      AT();
      if ( d != Delta )
      {
	//---------------------------------------------------------------
	// Adjust the delta since it is wrong
	//---------------------------------------------------------------
	AT();
	TimeObject.SetData<REAL_8>( DELTA_TIME, Delta );
      }
    }
    catch( ... )
    {
      AT();
    }
    AT();
    //-------------------------------------------------------------------
    // Return start time
    //-------------------------------------------------------------------
    return ( General::GPSTime( sec, nanosec ) );
  }

  REAL_8
  read_time_offset( const ILwd::TOC& TimeObject )
  {
    using General::GPSTime;

    REAL_8	retval( 0.0 );
    
    //===================================================================
    // Version 5
    //===================================================================
    AT();
    try
    {
      AT();
      return
	GPSTime( TimeObject.GetData<INT_4U>( TIME_OFFSET_SECONDS ),
		 TimeObject.GetData<INT_4U>( TIME_OFFSET_NANOSECONDS ) ) -
	GPSTime( 0, 0 );
    }
    catch( ... )
    {
    }
    try
    {
      AT();
      INT_4S s( TimeObject.GetData<INT_4S>( TIME_OFFSET_SECONDS ) );
      AT();
      INT_4U ns( TimeObject.GetData<INT_4U>( TIME_OFFSET_NANOSECONDS ) );
      if ( s < 0 )
      {
	AT();
	retval = -1;
	s *= -1;
      }
      else
      {
	AT();
	retval = 1;
      }
      AT();
      retval *= ( GPSTime( s, ns ) - GPSTime( 0 , 0) );
      return retval;
    }
    catch( ... )
    {
    }
    //===================================================================
    // Version 4
    //===================================================================
    try
    {
      AT();
      const ILwd::LdasArray< INT_4U >& offset
	( TimeObject.GetILwd<ILwd::LdasArray< INT_4U > >
	  ( TIME_OFFSET) );
      return
	GPSTime( offset.getData()[0], offset.getData()[1] )
	- GPSTime( 0, 0 );
    }
    catch( ... )
    {
    }
    try
    {
      AT();
      const ILwd::LdasArray< INT_4S >& offset
	( TimeObject.GetILwd<ILwd::LdasArray< INT_4S > >
	  ( TIME_OFFSET) );
      INT_4S s( offset.getData()[0] );
      INT_4S ns( offset.getData()[1] );
      
      if ( ns < 0 )
      {
	throw std::logic_error( "Cannot handle negitive nanoseconds" );
      }
      if ( s < 0 )
      {
	AT();
	retval = -1;
	s *= -1;
      }
      else
      {
	AT();
	retval = 1;
      }
      AT();
      retval *= ( GPSTime( s, ns ) - GPSTime( 0 , 0) );
      return retval;
    }
    catch( ... )
    {
      AT();
      return retval;
    }
  }

  bool
  write_relative_time( ILwd::TOC& TimeObject,
		       const General::GPSTime& Start,
		       const General::GPSTime& End )
  {
    using General::GPSTime;
    using ILwd::LdasContainer;

    AT();
    try
    {
      AT();
      LdasContainer& c( const_cast<LdasContainer&>
			( TimeObject.GetContainer() ) );

      AT();
      REAL_8	offset;
      AT();
      GPSTime	lstart = read_start_time_and_delta( TimeObject, offset );

      AT();
      offset = read_time_offset( TimeObject );
      AT();
      offset = ( lstart - Start ) + offset;

      AT();
      GPSTime new_offset( GPSTime( 0, 0 ) + offset );

      c.unsetMetadata( "start_time" );
      c.unsetMetadata( "delta_t" );

      AT();
      std::ostringstream	ss_sec;
      std::ostringstream	ss_nanosec;

      AT();
      ss_sec << Start.GetSeconds();
      ss_nanosec << Start.GetNanoseconds();

      // Actaully update all the information.
      AT();
      c.setName(3, ss_sec.str(), true);
      c.setName(4, ss_nanosec.str(), true);

      AT();
      write_time_offset( TimeObject, new_offset );

      AT();
      try
      {
	c.getMetadata<General::GPSTime>( "start_time" );
	c.setMetadata( "start_time", Start );
      }
      catch( ... )
      {
      }
      
      AT();
      return true;
    }
    catch( ... )
    {
      AT();
      return false;
    }
    AT();
  }

  void
  write_time_offset( ILwd::TOC& TimeObject,
		     const General::GPSTime& Offset )
  {
    //===================================================================
    // Version 5
    //===================================================================
    try
    {
      AT();
      TimeObject.SetData<INT_4U>( TIME_OFFSET_SECONDS,
				  Offset.GetSeconds() );
      AT();
      TimeObject.SetData<INT_4U>( TIME_OFFSET_NANOSECONDS,
				  Offset.GetNanoseconds() );
      return;
    }
    catch( ... )
    {
      AT();
    }
    try
    {
      AT();
      TimeObject.SetData<INT_4S>( TIME_OFFSET_SECONDS,
				  Offset.GetSeconds() );
      AT();
      TimeObject.SetData<INT_4U>( TIME_OFFSET_NANOSECONDS,
				  Offset.GetNanoseconds() );
      return;
    }
    catch( ... )
    {
      AT();
    }
    //===================================================================
    // Version 4
    //===================================================================
    try
    {
      AT();
      ILwd::LdasArray< INT_4U >& offset
	( TimeObject.GetILwd<ILwd::LdasArray< INT_4U > >
	  ( TIME_OFFSET) );
      offset.getData()[0] = Offset.GetSeconds();
      offset.getData()[1] = Offset.GetNanoseconds();
      return;
    }
    catch( ... )
    {
      AT();
    }
    try
    {
      AT();
      ILwd::LdasArray< INT_4S >& offset
	( TimeObject.GetILwd<ILwd::LdasArray< INT_4S > >
	  ( TIME_OFFSET) );
      offset.getData()[0] = Offset.GetSeconds();
      offset.getData()[1] = Offset.GetNanoseconds();
    }
    catch( ... )
    {
      AT();
      return;
    }
  }

} // namespace - anonymous
