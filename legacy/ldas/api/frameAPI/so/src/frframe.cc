/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "ldas_tools_config.h"

// System Header Files
#include <algorithm>
#include <cmath>
#include <sstream>   

   
#include "general/gpstime.hh"
#include "general/unordered_map.hh"

#include "framecpp/FrameCPP.hh"
#include "framecpp/FrAdcData.hh"

// Local Header Files 
#include "frframe.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "frrawdata.hh"
#include "frprocdata.hh"
#include "frsimdata.hh"
#include "frevent.hh"
#include "frsimevent.hh"
#include "frsummary.hh"
#include "frhistory.hh"
#include "frdetector.hh"
#include "util.hh"


using General::GPSTime;

using FrameCPP::FrameH;
using FrameCPP::FrDetector;
using FrameCPP::FrEvent;
using FrameCPP::FrHistory;
using FrameCPP::FrProcData;
using FrameCPP::FrSimData;
using FrameCPP::FrRawData;
using FrameCPP::FrSimEvent;
using FrameCPP::FrSummary;
using FrameCPP::FrTable;
using FrameCPP::FrVect;

#if ! CORE_API
using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasArray;
using ILwd::LdasString;
using ILwd::LdasContainer;
#endif /* ! CORE_API */
   
using namespace std;   
//!ignore_begin:

#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

#if ! CORE_API
enum
{
    FR_NAME,
    FR_RUN,
    FR_FRAME,
    FR_DATAQUALITY,
    FR_GTIMES,
    FR_GTIMEN,
    FR_ULEAPS,
    FR_DT,
    FR_TYPE,
    FR_USER,
    FR_DETECTSIM,
    FR_DETECTPROC,
    FR_HISTORY,
    FR_RAWDATA,
    FR_PROCDATA,
    FR_SIMDATA,
    FR_EVENT,
    FR_SIMEVENT,
    FR_SUMMARYDATA,
    FR_AUXDATA,
    FR_AUXTABLE,
    FR_FULL
};


QueryHash initFrameHash()
{
    QueryHash h;
    h[ "name"        ] = FR_NAME;
    h[ "run"         ] = FR_RUN;
    h[ "frame"       ] = FR_FRAME;
    h[ "dataquality" ] = FR_DATAQUALITY;
    h[ "gtimes"      ] = FR_GTIMES;
    h[ "gtimen"      ] = FR_GTIMEN;
    h[ "uleaps"      ] = FR_ULEAPS;
    h[ "dt"          ] = FR_DT;
    h[ "type"        ] = FR_TYPE;
    h[ "user"        ] = FR_USER;
    h[ "detectsim"   ] = FR_DETECTSIM;
    h[ "detectproc"  ] = FR_DETECTPROC;
    h[ "history"     ] = FR_HISTORY;
    h[ "rawdata"     ] = FR_RAWDATA;
    h[ "procdata"    ] = FR_PROCDATA;
    h[ "simdata"     ] = FR_SIMDATA;
    h[ "event"       ] = FR_EVENT;
    h[ "simevent"    ] = FR_SIMEVENT;
    h[ "summarydata" ] = FR_SUMMARYDATA;
    h[ "auxdata"     ] = FR_AUXDATA;
    h[ "auxtable"    ] = FR_AUXTABLE;
    h[ "full"        ] = FR_FULL;
    return h;
}


static QueryHash frameHash( initFrameHash() );
#endif /* CORE_API */

namespace
{
    //-------------------------------------------------------------------
    // Local constant values
    //-------------------------------------------------------------------
    static const INT_4S MAX_RUN = 0x7FFFFFFF;
    //:TODO: DETECTOR_PREFIX_LENGTH needs to be part of FrDetector
    static const INT_2U DETECTOR_PREFIX_LENGTH = 2;

#if CORE_API
    //-------------------------------------------------------------------
    // Routine to add the run number into the history record
    //-------------------------------------------------------------------
    void
    add_run( FrameCPP::FrameH& Primary,
	     const FrameCPP::FrameH& Secondary )
    {
	if ( Primary.GetRun( ) != MAX_RUN )
	{
	    //-----------------------------------------------------------
	    // Add record for primary run number
	    //-----------------------------------------------------------
	    for ( FrameH::detectProc_type::const_iterator
		      cur = Primary.RefDetectProc( ).begin( ),
		      last = Primary.RefDetectProc( ).end( );
		  cur != last;
		  ++cur )
	    {
		std::ostringstream	comment;
		const char*		prefix( (*cur)->GetPrefix( ) );

		for ( INT_2U x = 0; x < DETECTOR_PREFIX_LENGTH; ++x, ++prefix )
		{
		    if ( *prefix )
		    {
			comment << (char)(*prefix);
		    }
		}
		comment << ":" << Primary.GetRun( );

		FrameCPP::FrameH::history_type::value_type
		    history( new FrHistory( "run",
					    GPSTime::NowGPSTime( ).GetSeconds( ),
					    comment.str( ) ) );
		Primary.RefHistory( ).append( history );
	    }
	    Primary.SetRun( MAX_RUN );
	}
	if ( Secondary.GetRun( ) == MAX_RUN )
	{
	    // Merging will happen when the history records are merged together
	    return;
	}
	//---------------------------------------------------------------
	// Create a hash of known run info
	//---------------------------------------------------------------
	typedef General::unordered_map< std::string, INT_4S > run_info_type;
	run_info_type	run_info;


	for ( FrameH::history_type::const_iterator
		  cur = Primary.RefHistory( ).begin( ),
		  last = Primary.RefHistory( ).end( );
	      cur != last;
	      ++cur )
	{
	    if ( (*cur)->GetName( ) == "run" )
	    {
		INT_4S		run;
		std::string	ifo;
		std::string::size_type
		    colon_pos = (*cur)->GetComment( ).find_first_of( ":" );

		if ( colon_pos == std::string::npos )
		{
		    //---------------------------------------------------
		    // Malformed run number
		    //---------------------------------------------------
		    continue;
		}
		std::istringstream
		    ifo_str( (*cur)->GetComment( ).substr( 0, colon_pos ) );
		ifo_str >> ifo;

		std::istringstream
		    run_str( (*cur)->GetComment( ).substr( colon_pos + 1 ) );
		run_str >> run;
		run_info[ ifo ] = run;
	    }
	}
	//---------------------------------------------------------------
	// Loop over list of detectors
	//---------------------------------------------------------------
	for ( FrameH::detectProc_type::const_iterator
		  dcur = Secondary.RefDetectProc( ).begin( ),
		  dlast = Secondary.RefDetectProc( ).end( );
	      dcur != dlast;
	      ++dcur )
	{
	    const char*		prefix( (*dcur)->GetPrefix( ) );
	    std::ostringstream	p;
	    for ( INT_2U x = 0;
		  x < DETECTOR_PREFIX_LENGTH;
		  ++x, ++prefix )
	    {
		if ( *prefix )
		{
		    p << (char)(*prefix);
		}
	    }

	    std::ostringstream	comment;
	    run_info_type::const_iterator
		pos( run_info.find( p.str( ) ) );

	    if ( pos == run_info.end( ) )
	    {
		comment << p.str( ) << ":" << Secondary.GetRun( );
		
		FrameCPP::FrameH::history_type::value_type
		    history( new FrHistory( "run",
					    GPSTime::NowGPSTime( ).GetSeconds( ),
					    comment.str( ) ) );
		Primary.RefHistory( ).append( history );
		run_info[ p.str( ) ] = Secondary.GetRun( );
	    }
	    else
	    {
		if ( Secondary.GetRun( ) != pos->second )
		{
		    std::ostringstream	msg;

		    msg << "Run numbers differ for ifo: "
			<< pos->first
			<< " " << Secondary.GetRun( )
			<< " vs. "
			<< pos->second;
		    throw std::runtime_error( msg.str( ) );
		}
	    }
	}
    }
#endif

#if ! CORE_API
    //-------------------------------------------------------------------
    // Routine to calculate the dt of an adc
    //-------------------------------------------------------------------
    double get_dt( const FrameCPP::FrAdcData& Adc )
    {
	using FrameCPP::FrAdcData;

	INT_4U samples( 0 );
	for ( FrAdcData::const_iterator d( Adc.RefData( ).begin( ) );
	      d != Adc.RefData( ).end( );
	      d++ )
	{
	    samples += (*d)->GetDim( 0 ).GetNx( );
	}
	return samples / Adc.GetSampleRate( );
    }

    //-------------------------------------------------------------------
    // Routine to calculate the dt of an proc
    //-------------------------------------------------------------------
    double get_dt( const FrameCPP::FrProcData& Proc )
    {
	return Proc.GetTRange( );
    }

    //-------------------------------------------------------------------
    // Class to calculate the minimum start time and the maximum end
    //   time.
    //-------------------------------------------------------------------
    class time_range
    {
    public:
	typedef General::SharedPtr< FrameCPP::FrAdcData > adc_type;
	typedef General::SharedPtr< FrameCPP::FrProcData > proc_type;

	time_range( const FrameCPP::FrameH& Frame );

	void operator()( adc_type Data );

	void operator()( proc_type Data );

	REAL_8 GetDt( ) const;

	REAL_8 GetOffset( ) const;

	const General::GPSTime& GetStartTime( ) const;

    private:
	General::GPSTime		m_frame_start;
	mutable General::GPSTime	m_start;
	mutable General::GPSTime	m_end;
	bool				m_initialized;
	mutable bool			m_finalized;

	void finalize( ) const;
    };

    time_range::
    time_range( const FrameCPP::FrameH& Frame )
	: m_frame_start( Frame.GetGTime( ) ),
	  m_start( ),
	  m_end( ),
	  m_initialized( false ),
	  m_finalized( false )
    {
    }

    REAL_8 time_range::
    GetDt( ) const
    {
	return m_end - m_start;
    }

    REAL_8 time_range::
    GetOffset( ) const
    {
	finalize( );
	return m_start - m_frame_start;
    }

    const General::GPSTime& time_range::
    GetStartTime( ) const
    {
	return m_start;
    }

    void time_range::
    operator()( adc_type Data )
    {
	if ( m_initialized )
	{
	    General::GPSTime	start( m_frame_start +
				       Data->GetTimeOffset( ) );
	    General::GPSTime	end( start + get_dt( *Data ) );

	    if ( start < m_start )
	    {
		m_start = start;
	    }
	    if ( end > m_end )
	    {
		m_end = end;
	    }
	}
	else
	{
	    m_start = m_frame_start + Data->GetTimeOffset( );
	    m_end = m_start + get_dt( *Data );
	    m_initialized = true;
	}
    }

    void time_range::
    operator()( proc_type Data )
    {
	if ( m_initialized )
	{
	    General::GPSTime	start( m_frame_start +
				       Data->GetTimeOffset( ) );
	    General::GPSTime	end( start + get_dt( *Data ) );

	    if ( start != end )
	    {
		if ( start < m_start )
		{
		    m_start = start;
		}
		if ( end > m_end )
		{
		    m_end = end;
		}
	    }
	}
	else
	{
	    m_start = m_frame_start + Data->GetTimeOffset( );
	    m_end = m_start + get_dt( *Data );
	    if ( m_start != m_end )
	    {
		m_initialized = true;
	    }
	}
    }

    void time_range::
    finalize( ) const
    {
	if ( m_start.GetNanoseconds( ) != 0 )
	{
	    // Round down to the nearest second
	    m_start = General::GPSTime( m_start.GetSeconds( ), 0 );
	}
	if ( m_end.GetNanoseconds( ) != 0 )
	{
	    // Round up to the nearest second
	    m_end = General::GPSTime( m_end.GetSeconds( ) + 1, 0 );
	}
	m_finalized = true;
    }


    //-------------------------------------------------------------------
    // Class to set the start time and offset
    //-------------------------------------------------------------------
    class set_time_range
    {
    public:
	typedef General::SharedPtr< FrameCPP::FrAdcData >	adc_type;	
	typedef General::SharedPtr< FrameCPP::FrProcData >	proc_type;
	
	set_time_range( const time_range& TimeRange );

	void Reset( );

	void operator()( adc_type AdcData );

	void operator()( proc_type ProcData );

    private:
	const REAL_8		m_offset; // Offset of m_start from frame start
	bool			m_first;
    };

    set_time_range::
    set_time_range( const time_range& TimeRange )
	: m_offset( TimeRange.GetOffset( ) ),
	  m_first( true )
    {
    }

    inline void set_time_range::
    Reset( )
    {
	m_first = true;
    }

    void set_time_range::
    operator()( adc_type AdcData )
    {
	if ( m_first )
	{
	    AdcData->SetTimeOffset( AdcData->GetTimeOffset( ) - m_offset );
#if 0
	    m_first = false;
#endif
	}
    }

    void set_time_range::
    operator()( proc_type ProcData )
    {
	if ( m_first )
	{
	    ProcData->SetTimeOffset( ProcData->GetTimeOffset( ) - m_offset );
#if 0
	    m_first = false;
#endif
	}
    }

    //-------------------------------------------------------------------
    // Algorithm to loop over all Adc channels
    //-------------------------------------------------------------------
    template< class LM_Function_type >
    LM_Function_type for_each_adc( FrameCPP::FrameH& Frame, LM_Function_type f )
    {
	if ( Frame.GetRawData( ) )
	{
	    FrRawData::firstAdc_type&
		adc( Frame.GetRawData( )->RefFirstAdc( ) );
	    return std::for_each( adc.begin( ), adc.end( ), f );
	}
	return f;
    }

    //-------------------------------------------------------------------
    // Algorithm to loop over all Adc channels
    //-------------------------------------------------------------------
    template< class LM_Function_type >
    LM_Function_type for_each_proc( FrameCPP::FrameH& Frame, LM_Function_type f )
    {
	FrameH::procData_type&
	    proc( Frame.RefProcData( ) );
	return std::for_each( proc.begin( ), proc.end( ), f );
    }
#endif /* CORE_API */

} // namespace - anonymous

namespace FrameAPI
{
    namespace FrameH
    {
#if CORE_API
	void
	merge( FrameCPP::FrameH& Primary,
	       const FrameCPP::FrameH& Secondary )
	{
#if 0	/* This is currently commented out because it breaks backwards compatability */
	    //---------------------------------------------------------------
	    // Sanity Checks
	    //   Only allow merging like FrameH data
	    //---------------------------------------------------------------
	    if ( ( Primary.GetGTime( ) != Secondary.GetGTime( ) )
		 || ( Primary.GetDt( ) != Secondary.GetDt( ) ) )
	    {
		std::ostringstream	msg;

		msg << "Cannot merge FrameH structures of different times"
		    << " ( Primary: GTime: " << Primary.GetGTime( )
		    << " Dt: " << Primary.GetDt( )
		    << " vs. Secondary: GTime: "
		    << Secondary.GetGTime( ) << " )"
		    << " Dt: " << Secondary.GetDt( )
		    << " )";
		throw SWIGEXCEPTION( msg.str( ) );
	    }
#endif /* 0 */
	    //---------------------------------------------------------------
	    // Merge run number
	    //---------------------------------------------------------------
	    if ( Primary.GetRun( ) != Secondary.GetRun( ) )
	    {
		add_run( Primary, Secondary );
	    }
	    //---------------------------------------------------------------
	    // Merge DataQuality info
	    //---------------------------------------------------------------
	    Primary.SetDataQuality( Primary.GetDataQuality( ) |
				    Secondary.GetDataQuality( ) );
	    //---------------------------------------------------------------
	    // Merge FrRawData info
	    //---------------------------------------------------------------
	    FrameCPP::FrameH::rawData_type::element_type*
		rd( FrameAPI::FrRawData::merge( Primary.GetRawData( ).get( ),
						Secondary.GetRawData( ).get( ) ) );
	    if ( rd != Primary.GetRawData( ).get( ) )
	    {
		FrameCPP::FrameH::rawData_type
		    d( rd );
		Primary.SetRawData( d );
	    }
	    //---------------------------------------------------------------
	    // Merge auxData - High resolution of dataQuality
	    //---------------------------------------------------------------
	    Primary.RefAuxData( ).Merge( Secondary.RefAuxData( ) );
	    //---------------------------------------------------------------
	    // Merge auxTable
	    //---------------------------------------------------------------
	    Primary.RefAuxTable( ).Merge( Secondary.RefAuxTable( ) );
	    //---------------------------------------------------------------
	    // Sim Detectors
	    //---------------------------------------------------------------
	    for ( FrameCPP::FrameH::detectSim_type::const_iterator
		      current( Secondary.RefDetectSim( ).begin( ) ),
		      end( Secondary.RefDetectSim( ).end( ) );
		  current != end;
		  current++ )
	    {
		bool	duplicate( false );

		for ( FrameCPP::FrameH::detectSim_type::const_iterator
			  pcurrent( Primary.RefDetectSim( ).begin( ) ),
			  pend( Primary.RefDetectSim( ).end( ) );
		      pcurrent != pend;
		      pcurrent++ )
		{
		    if ( (*current)->GetName( ) == (*pcurrent)->GetName( ) )
		    {
			duplicate = true;
			break;
		    }
		}
		if ( !duplicate )
		{
		    FrameCPP::FrameH::detectSim_type::value_type
			detector( new FrDetector( **current ) );
		    Primary.RefDetectSim( ).append(  detector );
		}
      
	    }
	    //---------------------------------------------------------------
	    // Detectors
	    //---------------------------------------------------------------
	    for ( FrameCPP::FrameH::detectProc_type::const_iterator
		      current( Secondary.RefDetectProc( ).begin( ) ),
		      end( Secondary.RefDetectProc( ).end( ) );
		  current != end;
		  current++ )
	    {
		bool	duplicate( false );

		for ( FrameCPP::FrameH::detectProc_type::const_iterator
			  pcurrent( Primary.RefDetectProc( ).begin( ) ),
			  pend( Primary.RefDetectProc( ).end( ) );
		      pcurrent != pend;
		      pcurrent++ )
		{
		    if ( (*current)->GetName( ).compare( (*pcurrent)->GetName( ) ) == 0 )
		    {
			duplicate = true;
			break;
		    }
		}
		if ( !duplicate )
		{
		    FrameCPP::FrameH::detectProc_type::value_type
			detector( new FrDetector( **current ) );
		    Primary.RefDetectProc( ).append( detector );
		}
	    }
	    //---------------------------------------------------------------
	    // History
	    //---------------------------------------------------------------
	    for ( FrameCPP::FrameH::history_type::const_iterator
		      current( Secondary.RefHistory( ).begin( ) ),
		      end( Secondary.RefHistory( ).end( ) );
		  current != end;
		  current++ )
	    {
		FrameCPP::FrameH::history_type::value_type
		    history( new FrHistory( **current ) );

		Primary.RefHistory( ).append( history );
	    }
	}
#endif /* CORE_API */
    } // namespace - FrameH
} // namespace - FrameAPI

#if ! CORE_API
void FrameAPI::
Normalize( FrameCPP::FrameH& Frame )
{
    time_range	tr( Frame );

    //-------------------------------------------------------------------
    // Loop over FrProcData finding the minimum start and max stop times
    //-------------------------------------------------------------------
    tr = for_each_proc( Frame, tr );
    //-------------------------------------------------------------------
    // Loop over FrAdcData finding the minimum start and max stop times
    //-------------------------------------------------------------------
    tr = for_each_adc( Frame, tr );
    //-------------------------------------------------------------------
    // Adjust start time, delta time, and offsets
    //-------------------------------------------------------------------
    set_time_range	str( tr );

    Frame.SetGTime( tr.GetStartTime( ) );
    Frame.SetDt( tr.GetDt( ) );

    for_each_adc( Frame, str );
    for_each_proc( Frame, str );
}

//-----------------------------------------------------------------------------
std::string getAttribute( const FrameH& frame, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    Query q = dq.front();
    dq.pop_front();
    ostringstream res;

    QueryHash::const_iterator iter =
        frameHash.find( q.getName().c_str() );
    if ( iter == frameHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
	case FR_NAME:
	    res << frame.GetName();
	    break;
                
	case FR_RUN:
	    res << frame.GetRun();
	    break;
            
	case FR_FRAME:
	    res << frame.GetFrame();
	    break;

	case FR_DATAQUALITY:
	    res << frame.GetDataQuality();
	    break;
            
	case FR_GTIMES:
	    res << frame.GetGTime().getSec();
	    break;
            
	case FR_GTIMEN:
	    res << frame.GetGTime().getNSec();
	    break;

	case FR_ULEAPS:
	    res << frame.GetULeapS();
	    break;
                
	case FR_DT:
	    res << frame.GetDt();
	    break;
                
	case FR_TYPE:
	    return getAttribute( frame.RefType(), dq );
                
	case FR_USER:
	    return getAttribute( frame.RefUser(), dq );
                
	case FR_DETECTSIM:
	    if ( frame.RefDetectSim().size() == 0 )
	    {
		res << "NULL";
	    }
	    else
	    {
		return getAttribute( *frame.RefDetectSim()[0], dq,
				     start, index );
	    }
	    break;
                
	case FR_DETECTPROC:
	    if ( frame.RefDetectProc().size() == 0 )
	    {
		res << "NULL";
	    }
	    else
	    {
		return getAttribute( *frame.RefDetectProc()[0], dq,
				     start, index );
	    }
	    break;
                
	case FR_HISTORY:
	    return getAttribute( frame.RefHistory(), dq );
                
	case FR_RAWDATA:
	    if ( ! frame.GetRawData() )
	    {
		res << "NULL";
	    }
	    else
	    {
		return getAttribute( *frame.GetRawData(), dq,
				     start, index );
	    }
	    break;
                
	case FR_PROCDATA:
	    return getAttribute( frame.RefProcData(), dq );
                
	case FR_SIMDATA:
	    return getAttribute( frame.RefSimData(), dq );
                
	case FR_EVENT:
	    return getAttribute( frame.RefEvent(), dq );

	case FR_SIMEVENT:
	    return getAttribute( frame.RefSimEvent(), dq );
                
	case FR_SUMMARYDATA:
	    return getAttribute( frame.RefSummaryData(), dq );
                
	case FR_AUXDATA:
	    return getAttribute( frame.RefAuxData(), dq );

	case FR_AUXTABLE:
	    return getAttribute( frame.RefAuxTable(), dq );

	default:
	    throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
	case FR_TYPE:
	    return getAttribute( getContained( frame.RefType(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;
                
	case FR_USER:
	    return getAttribute( getContained( frame.RefUser(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;

	case FR_HISTORY:
	    return getAttribute( getContained( frame.RefHistory(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;

	case FR_PROCDATA:
	    return getAttribute( getContained( frame.RefProcData(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;

	case FR_SIMDATA:
	    return getAttribute( getContained( frame.RefSimData(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;

	case FR_EVENT:
	    return getAttribute( getContained( frame.RefEvent(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;

	case FR_SIMEVENT:
	    return getAttribute( getContained( frame.RefSimEvent(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;
                
	case FR_SUMMARYDATA:
	    return getAttribute( getContained( frame.RefSummaryData(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;
                
	case FR_AUXDATA:
	    return getAttribute( getContained( frame.RefAuxData(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;

	case FR_AUXTABLE:
	    return getAttribute( getContained( frame.RefAuxTable(),
					       q, start, index ),
				 dq, start, index + 1 );
	    break;

	default:
	    throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}


//-----------------------------------------------------------------------------
LdasElement* getData( const FrameH& frame, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        frameHash.find( q.getName().c_str() );
    if ( iter == frameHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        LdasElement* c( 0 );

        switch( iter->second )
        {
	case FR_DETECTSIM:
	    if ( frame.RefDetectSim().size() == 0 )
	    {
		throw SWIGEXCEPTION( "no_detectsim" );
	    }
	    if ( dq.size() == 0 )
	    {
		c = getData( *frame.RefDetectSim()[0], dq, start, index );
		c->setName( 1, "detectSim" );
	    }
	    else
	    {
		c = getData( *frame.RefDetectSim()[0], dq, start, index );
	    }
	    return c;
                
	case FR_DETECTPROC:
	    if ( frame.RefDetectProc().size() == 0 )
	    {
		throw SWIGEXCEPTION( "no_detectproc" );
	    }
	    if ( dq.size() == 0 )
	    {
		c = getData( *frame.RefDetectProc()[0], dq, start, index );
		c->setName( 1, "detectProc" );
	    }
	    else
	    {
		c = getData( *frame.RefDetectProc()[0], dq, start, index );
	    }
	    return c;
                
	case FR_RAWDATA:
	    if ( ! frame.GetRawData() )
	    {
		throw SWIGEXCEPTION( "no_rawdata" );
	    }
	    if ( dq.size() == 0 )
	    {
		c = getData( *frame.GetRawData(), dq, start, index,
			     frame.GetGTime(), frame.GetDt() );
		c->setName( 1, "rawData" );
	    }
	    else
	    {
		c = getData( *frame.GetRawData(), dq, start, index,
			     frame.GetGTime(), frame.GetDt() );
	    }
	    return c;

	case FR_FULL:
	    return frame2container( frame );

	default:
	    throw SWIGEXCEPTION( "bad_query" );
        }
    }
    
    switch( iter->second )
    {
    case FR_TYPE:
	return getData( getContained( frame.RefType(), q, start, index ),
			dq, start, index + 1 );
	break;
                
    case FR_USER:
	return getData( getContained( frame.RefUser(), q, start, index ),
			dq, start, index + 1 );
	break;

    case FR_HISTORY:
	return getData( getContained( frame.RefHistory(),
				      q, start, index ),
			dq, start, index + 1 );
	break;

    case FR_PROCDATA:
	return getData( getContained( frame.RefProcData(),
				      q, start, index ),
			dq, start, index + 1,
			frame.GetGTime() );
	break;

    case FR_SIMDATA:
	return getData( getContained( frame.RefSimData(),
				      q, start, index ),
			dq, start, index + 1,
			frame.GetGTime(), frame.GetDt() );
	break;


    case FR_EVENT:
	return getData( getContained( frame.RefEvent(), q, start, index ),
			dq, start, index + 1,
			frame.GetGTime(), frame.GetDt() );
	break;

    case FR_SIMEVENT:
	return getData( getContained( frame.RefSimEvent(),
				      q, start, index ),
			dq, start, index + 1,
			frame.GetGTime(), frame.GetDt() );
	break;
                
    case FR_SUMMARYDATA:
	return getData( getContained( frame.RefSummaryData(),
				      q, start, index ),
			dq, start, index + 1 );
	break;
                
    case FR_AUXDATA:
	return getData( getContained( frame.RefAuxData(), q, start, index ),
			dq, start, index + 1 );
	break;

    case FR_AUXTABLE:
	return getData( getContained( frame.RefAuxTable(), q, start, index ),
			dq, start, index + 1 );
	break;
    }
    
    throw SWIGEXCEPTION( "bad_query" );
}
//!ignore_end:

//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to Frame.
//
//!usage_ooi: FrameH* frame = container2frame( c );
//
//!param: const LdasContainer& c - frame container.
//
//!return: FrameH* frame - FrameCPP::FrameH newly allocated.
//
//!exc: SwigException - bad container data.
//-----------------------------------------------------------------------------
FrameH* container2frame( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
    if ( c.getNameSize() < 3 ||
         strcasecmp( c_name.c_str(), "Frame" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not Frame container." );
    }
    
    const LdasArray< INT_4U >& te = findArrayType< INT_4U >( c, "GTime" );
    FrameH* frame = new FrameH(
			       c.getName( 0 ),
			       *findArrayType< INT_4S >( c, "run" ).getData(),
			       *findArrayType< INT_4U >( c, "frame" ).getData(),
			       FrameCPP::Time( te.getData()[ 0 ], te.getData()[ 1 ] ),
			       *findArrayType< INT_2U >( c, "ULeapS" ).getData(),
			       *findArrayType< REAL_8 >( c, "dt" ).getData(),
			       *findArrayType< INT_4U >( c, "dataquality" ).getData() );

    try
    {
        const LdasContainer* type( findContainerType( c, "type" ) );
        if ( type != 0 )
        {
            for ( unsigned int i = 0; i < type->size(); ++i )
            {
		FrameCPP::FrameH::type_type::value_type
		    v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*type)[ i ] ) ) );

                frame->RefType().append( v );
            }
        }
        
        const LdasContainer* user( findContainerType( c, "user" ) );
        if ( user != 0 )
        {
            for ( unsigned int i = 0; i < user->size(); ++i )
            {
		FrameCPP::FrameH::user_type::value_type
		    v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*user)[ i ] ) ) );

                frame->RefUser().append( v );
            }
        }
        
        const LdasContainer* detectSim( findContainerType( c, "detectSim" ) );
        if ( detectSim != 0 )
        {
            LdasContainer::const_iterator iter( detectSim->begin() );
            for( unsigned int i = detectSim->size(); i != 0; --i, ++iter )
            {
		FrameCPP::FrameH::detectSim_type::value_type
		    d( container2detector( dynamic_cast< const LdasContainer& >( **iter ) ) );

		frame->RefDetectSim().append( d );
	    }
        }

        const LdasContainer* detectProc( findContainerType( c, "detectProc" ) );
        if ( detectProc != 0 )
        {
            LdasContainer::const_iterator iter( detectProc->begin() );
            for( unsigned int i = detectProc->size(); i != 0; --i, ++iter )
            {
		FrameCPP::FrameH::detectProc_type::value_type
		    d( container2detector( dynamic_cast< const LdasContainer& >( **iter ) ) );

		frame->RefDetectProc( ).append( d );
	    }   
        }

        const LdasContainer* history( findContainerType( c, "history" ) );
        if ( history != 0 )
        {
            for ( unsigned int i = 0; i < history->size(); ++i )
            {
		FrameCPP::FrameH::history_type::value_type
		    a( container2history( dynamic_cast< const LdasContainer& >( *(*history)[ i ] ) ) );

                frame->RefHistory().append( a );
            }
        }
        
        const LdasContainer* rawData( findContainerType( c, "rawData" ) );
        if ( rawData != 0 )
        {
	    FrameCPP::FrameH::rawData_type
		r( container2rawData( *rawData ) );
            frame->SetRawData( r );
        }

        const LdasContainer* procData( findContainerType( c, "procData" ) );
        if ( procData != 0 )
        {
            for ( unsigned int i = 0; i < procData->size(); ++i )
            {
		FrameCPP::FrameH::procData_type::value_type
		    a( container2procData( dynamic_cast< const LdasContainer& >( *(*procData)[ i ] ) ) );

                frame->RefProcData().append( a );
            }
        }
        
        const LdasContainer* simData( findContainerType( c, "simData" ) );
        if ( simData != 0 )
        {
            for ( unsigned int i = 0; i < simData->size(); ++i )
            {
		FrameCPP::FrameH::simData_type::value_type
		    a( container2simData( dynamic_cast< const LdasContainer& >( *(*simData)[ i ] ) ) );

                frame->RefSimData().append( a );
            }
        }
        
        const LdasContainer* event( findContainerType( c, "event" ) );
        if ( event != 0 )
        {
            for ( unsigned int i = 0; i < event->size(); ++i )
            {
		FrameCPP::FrameH::event_type::value_type
		    a( container2event( dynamic_cast< const LdasContainer& >( *(*event)[ i ] ) ) );

                frame->RefEvent().append( a );
            }
        }

        const LdasContainer* simEvent( findContainerType( c, "simEvent" ) );
        if ( simEvent != 0 )
        {
            for ( unsigned int i = 0; i < simEvent->size(); ++i )
            {
		FrameCPP::FrameH::simEvent_type::value_type
		    a( container2simEvent( dynamic_cast< const LdasContainer& >( *(*simEvent)[ i ] ) ) );

                frame->RefSimEvent().append( a );
            }
        }

        const LdasContainer* summaryData(
					 findContainerType( c, "summaryData" ) );
        if ( summaryData != 0 )
        {
            for ( unsigned int i = 0; i < summaryData->size(); ++i )
            {
		FrameCPP::FrameH::summaryData_type::value_type
		    a( container2summary( dynamic_cast< const LdasContainer& >( *(*summaryData)[ i ] ) ) );

                frame->RefSummaryData().append( a );
            }
        }
        
        const LdasContainer* aux( findContainerType( c, "auxdata" ) );
        if ( aux != 0 )
        {
            for ( unsigned int i = 0; i < aux->size(); ++i )
            {
		FrameCPP::FrameH::auxData_type::value_type
		    v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*aux)[ i ] ) ) );
                frame->RefAuxData().append( v );
            }
        }

        const LdasContainer* table( findContainerType( c, "auxtable" ) );
        if ( table != 0 )
        {
            for ( unsigned int i = 0; i < table->size(); ++i )
            {
		FrameCPP::FrameH::auxTable_type::value_type
		    t( container2table( dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) ) );

                frame->RefAuxTable().append( t );
            }
        }
    }
    catch( std::bad_cast& )
    {
	delete frame;
	frame = (FrameCPP::FrameH*)NULL;
	throw SWIGEXCEPTION( "invalid_input_format" );
    }
    
    return frame;
}


//-----------------------------------------------------------------------------
//
//: Convert Frame to ILWD.
//
//!usage_ooi: LdasContainer* frameContainer = frame2container( frame, fullFrame, daqFrame );
//
//!param: const FrameH& frame - FrameCPP::FrameH object to convert.
//!param: bool fullFrame - convert all frame structures if true.
//!param: bool daqFrame - treat frame as DaqFrame, convert only activated ADCs.
//
//!return: LdasContainer* frameContainer - converted frame container.
//
//-----------------------------------------------------------------------------
LdasContainer* frame2container( const FrameH& frame, bool fullFrame, bool daqFrame )
{
    AT( );
    LdasContainer* c = new LdasContainer( "::Frame" );
    c->setName( 0, frame.GetName() );
    // Identify the version of FrameCPP that was used.
    c->setMetadata( "FrameCPPVersion",  FrameCPP::GetVersion( ) );
    c->setMetadata( "DataFormatVersion",
		    FrameCPP::GetDataFormatVersion( ) );
    c->setMetadata( "FrameLibraryMinorVersion",
		    FrameCPP::GetDataFormatVersion( ) );

    AT( );
    LdasArray< INT_4S >* run(
			     new LdasArray< INT_4S >( frame.GetRun(), "run" ) );
    LdasArray< INT_4U >* frameNumber(
				     new LdasArray< INT_4U >( frame.GetFrame(), "frame" ) );

    LdasArray< INT_4U >* dataquality(
				     new LdasArray< INT_4U >( frame.GetDataQuality(), "dataQuality" ) );
    
    INT_4U time[ 2 ] = { frame.GetGTime().getSec(),
                         frame.GetGTime().getNSec() };
    LdasArray< INT_4U >* gTime(
			       new LdasArray< INT_4U >( time, 2, "GTime" ) );
    
    LdasArray< INT_2U >* uLeapS(
				new LdasArray< INT_2U >( frame.GetULeapS(), "ULeapS" ) );
    LdasArray< REAL_8 >* dt(
			    new LdasArray< REAL_8 >( frame.GetDt(), "dt" ) );

    c->push_back( run,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( frameNumber,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( dataquality,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( gTime,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( uLeapS,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( dt,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

    if ( fullFrame  )
    {

	AT( );
	size_t typeSize( frame.RefType().size() );
	if ( typeSize > 0 )
	{
	    AT( );
	    LdasContainer* type(
				new LdasContainer( ":type:Container(Vect):Frame" ) );
	    c->push_back( type,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_type_iterator iter( frame.RefType().begin() );
	    for( size_t i = typeSize; i != 0; --i, ++iter )
	    {
		type->push_back( vect2array( **iter ),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
	    }
	}
    
	size_t userSize( frame.RefUser().size() );
	if ( userSize > 0 )
	{
	    AT( );
	    LdasContainer* user(
				new LdasContainer( ":user:Container(Vect):Frame" ) );
	    c->push_back( user,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_user_iterator iter( frame.RefUser().begin() );
	    for( size_t i = userSize; i != 0; --i, ++iter )
	    {
		user->push_back( vect2array( **iter ),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
	    }
	}

    } // if ( fullFrame )


    size_t detectSimSize( frame.RefDetectSim().size() );
    if ( detectSimSize != 0 )
    {
	AT( );
        LdasContainer* detectSim(
				 new LdasContainer( ":detectSim:Container(Detector):Frame" ) );
        c->push_back( detectSim,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );

        FrameH::const_detectSim_iterator iter( frame.RefDetectSim().begin() );
        for( size_t i = detectSimSize; i != 0; --i, ++iter )
        {
	    detectSim->push_back( detector2container( **iter, detectorSimName ),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	}
    }
    
    size_t detectProcSize( frame.RefDetectProc().size() );
    if ( detectProcSize != 0 )
    {
	AT( );
        LdasContainer* detectProc(
				  new LdasContainer( ":detectProc:Container(Detector):Frame" ) );
	AT( );
        c->push_back( detectProc,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
   
	AT( );
        FrameH::const_detectProc_iterator
	    iter( frame.RefDetectProc().begin() );
	AT( );
        for( size_t i = detectProcSize; i != 0; --i, ++iter )
        {
	    AT( );
	    detectProc->push_back( detector2container( **iter, detectorProcName ),
				   ILwd::LdasContainer::NO_ALLOCATE,
				   ILwd::LdasContainer::OWN );
	}   
    }

    size_t historySize( frame.RefHistory().size() );
    if ( historySize > 0 )
    {
	AT( );
        LdasContainer* history(
			       new LdasContainer( ":history:Container(History):Frame" ) );
        c->push_back( history,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );

        FrameH::const_history_iterator iter( frame.RefHistory().begin() );
        for( size_t i = historySize; i != 0; --i, ++iter )
        {
            history->push_back( history2container( **iter ),
				ILwd::LdasContainer::NO_ALLOCATE,
				ILwd::LdasContainer::OWN );
        }
    }

    if ( fullFrame  )
    {
    

	AT( );
	if ( frame.GetRawData() )
	{
	    LdasContainer* d = rawData2container( *frame.GetRawData(),
						  frame.GetGTime(),
						  frame.GetDt()
#if DAQ_SUPPORT
						  , daqFrame? &((dynamic_cast< const FrameCPP::Version::DaqFrame& >( frame )).RefAdcUpdateVect()):(std::vector<FrameCPP::Version::DaqFrame::adc_update_struct>*)0
#endif
						  );
	    d->setName( 1, "rawData" );
	    c->push_back( d,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
	}

	size_t procDataSize( frame.RefProcData().size() );
	if ( procDataSize > 0 )
	{
	    AT( );
	    LdasContainer* procdata(
				    new LdasContainer( ":procData:Container(ProcData):Frame" ) );
	    c->push_back( procdata,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_procData_iterator iter( frame.RefProcData().begin() );
	    for( size_t i = procDataSize; i != 0; --i, ++iter )
	    {
		procdata->push_back( procData2container( **iter, frame.GetGTime() ),
				     ILwd::LdasContainer::NO_ALLOCATE,
				     ILwd::LdasContainer::OWN );
	    }
	}

	size_t simSize( frame.RefSimData().size() );
	if ( simSize > 0 )
	{
	    AT( );
	    LdasContainer* simData(
				   new LdasContainer( ":simData:Container(SimData):Frame" ) );
	    c->push_back( simData,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_simData_iterator iter( frame.RefSimData().begin() );
	    for( size_t i = simSize; i != 0; --i, ++iter )
	    {
		simData->push_back(
				   simData2container( **iter, frame.GetGTime(), frame.GetDt() ),
				   ILwd::LdasContainer::NO_ALLOCATE,
				   ILwd::LdasContainer::OWN );
	    }
	}

	size_t eventSize( frame.RefEvent().size() );
	if ( eventSize > 0 )
	{
	    AT( );
	    LdasContainer* event(
				 new LdasContainer( ":event:Container(Event):Frame" ) );
	    c->push_back( event,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_event_iterator iter( frame.RefEvent().begin() );
	    for( size_t i = eventSize; i != 0; --i, ++iter )
	    {
		event->push_back(
				 event2container( **iter, frame.GetGTime(), frame.GetDt() ),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
	    }
	}

	size_t simEventSize( frame.RefSimEvent().size() );
	if ( simEventSize > 0 )
	{
	    AT( );
	    LdasContainer* simEvent(
				    new LdasContainer( ":simEvent:Container(SimEvent):Frame" ) );
	    c->push_back( simEvent,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_simEvent_iterator iter( frame.RefSimEvent().begin() );
	    for( size_t i = simEventSize; i != 0; --i, ++iter )
	    {
		simEvent->push_back(
				    simEvent2container( **iter, frame.GetGTime(), frame.GetDt() ),
				    ILwd::LdasContainer::NO_ALLOCATE,
				    ILwd::LdasContainer::OWN );
	    }
	}

	size_t summarySize( frame.RefSummaryData().size() );
	if ( summarySize > 0 )
	{
	    AT( );
	    LdasContainer* summaryData(
				       new LdasContainer( ":summaryData:Container(Summary):Frame" ) );
	    c->push_back( summaryData,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_summaryData_iterator
		iter( frame.RefSummaryData().begin() );
	    for( size_t i = summarySize; i != 0; --i, ++iter )
	    {
		summaryData->push_back( summary2container( **iter ),
					ILwd::LdasContainer::NO_ALLOCATE,
					ILwd::LdasContainer::OWN );
	    }
	}
    
	size_t auxSize( frame.RefAuxData().size() );
	if ( auxSize > 0 )
	{
	    AT( );
	    LdasContainer* aux(
			       new LdasContainer( ":auxData:Container(Vect)" ) );
	    c->push_back( aux,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_auxData_iterator iter( frame.RefAuxData().begin() );
	    for( size_t i = auxSize; i != 0; --i, ++iter )
	    {
		aux->push_back( vect2array( **iter ),
				ILwd::LdasContainer::NO_ALLOCATE,
				ILwd::LdasContainer::OWN );
	    }
	}

	size_t tableSize( frame.RefAuxTable().size() );
	if ( tableSize > 0 )
	{
	    AT( );
	    LdasContainer* table(
				 new LdasContainer( ":auxtable:Container(Table)" ) );
	    c->push_back( table,
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );

	    FrameH::const_auxTable_iterator
		iter( frame.RefAuxTable().begin() );
	    for( size_t i = tableSize; i != 0; --i, ++iter )
	    {
		table->push_back( table2container( **iter ),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    }
	}


    }    // if ( fullFrame )

    return c;
}

//-----------------------------------------------------------------------------
//
//: Concatenate two frames.
//
//!usage_ooi: LdasContainer* concatFrame = concatFrame( c1, c2 );
//
//!param: const LdasContainer* c1 - first FrameCPP::FrameH.
//!param: const LdasContainer* c2 - second FrameCPP::FrameH.
//
//!return: LdasContainer* concatFrame - newly allocated FrameCPP:Frame.
//
//!exc: SwigException - bad frame container data.
//
//-----------------------------------------------------------------------------
LdasContainer* concatFrame(
			   const LdasContainer* c1, const LdasContainer* c2 )
{
    if ( c1->getName( 2 ) != "Frame" ||
         c2->getName( 2 ) != "Frame" )
    {
        throw SWIGEXCEPTION( "not_frame_data" );
    }

   
    const string& c1_name( c1->getName( 0 ) );
    const string& c2_name( c2->getName( 0 ) );   
   
    if ( strcasecmp( c1_name.c_str(), c2_name.c_str() ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_frames" );
    }

    const LdasArray< INT_4S >& r1( findArrayType< INT_4S >( *c1, "run" ) );
    const LdasArray< INT_4S >& r2( findArrayType< INT_4S >( *c2, "run" ) );

    if ( r1.getNDim() != 1 || r1.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame_run" );
    }
    if ( r1 != r2 )
    {
        throw SWIGEXCEPTION( "incompatible_frame_run" );
    }

    const LdasArray< INT_4U >& f1( findArrayType< INT_4U >( *c1, "frame" ) );
    const LdasArray< INT_4U >& f2( findArrayType< INT_4U >( *c2, "frame" ) );
    if ( f1.getNDim() != 1 || f1.getDimension( 0 ) != 1 ||
         f2.getNDim() != 1 || f2.getDimension( 0 ) != 1 )  
    {
        throw SWIGEXCEPTION( "bad_frame_frame" );
    }
    //:TODO: For now this doesn't work since all frames have
    // the same value for 'frame' in the frame header.
#ifdef TEST_FRAME_PARAM
    if ( f1.getData()[ 0 ] + 1 != f2.getData()[ 0 ] )
    {
        throw SWIGEXCEPTION( "incompatible_frame_frame" );
    }
#endif // TEST_FRAME_PARAM

    /*
      Check data quality -- it must be the same number for both frames
    */
    const LdasArray< INT_4U >& dq1( findArrayType< INT_4U >( *c1, "dataquality" ) );
    const LdasArray< INT_4U >& dq2( findArrayType< INT_4U >( *c2, "dataquality" ) );
    if ( dq1.getNDim() != 1 || dq1.getDimension( 0 ) != 1 ||
         dq2.getNDim() != 1 || dq2.getDimension( 0 ) != 1 )  
    {
        throw SWIGEXCEPTION( "bad_frame_dataquality" );
    }
    if ( dq1.getData()[ 0 ] != dq2.getData()[ 0 ] )
    {
        throw SWIGEXCEPTION( "incompatible_frame_dataquality" );
    }

    const LdasArray< INT_4U >& gt1( findArrayType< INT_4U >( *c1, "GTime" ) );
    const LdasArray< INT_4U >& gt2( findArrayType< INT_4U >( *c2, "GTime" ) );   
    if ( gt1.getNDim() != 1 || gt1.getDimension( 0 ) != 2 ||
         gt2.getNDim() != 1 || gt2.getDimension( 0 ) != 2 )
    {
        throw SWIGEXCEPTION( "bad_frame_GTime" );
    }

    INT_4U gs1( gt1.getData()[ 0 ] );
    INT_4U gn1( gt1.getData()[ 1 ] );
    INT_4U gs2( gt2.getData()[ 0 ] );
    INT_4U gn2( gt2.getData()[ 1 ] );

    // 'ULeapS' of the first frame will be copied into 
    // the concatenated frame.
    const LdasArray< INT_2U >& uleaps1( findArrayType< INT_2U >( *c1, "ULeapS" ) );
    const LdasArray< INT_2U >& uleaps2( findArrayType< INT_2U >( *c2, "ULeapS" ) );   
    if ( uleaps1.getNDim() != 1 || uleaps1.getDimension( 0 ) != 1 ||
         uleaps2.getNDim() != 1 || uleaps2.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame_ULeapS" );
    }
    
    const LdasArray< REAL_8 >& d1( findArrayType< REAL_8 >( *c1, "dt" ) );
    const LdasArray< REAL_8 >& d2( findArrayType< REAL_8 >( *c2, "dt" ) );

    if ( d1.getNDim() != 1 || d1.getDimension( 0 ) != 1 ||
         d2.getNDim() != 1 || d2.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame_dt" );
    }
    REAL_8 dt1( d1.getData()[ 0 ] );
    REAL_8 dt2( d2.getData()[ 0 ] );

    INT_4U nextgs = gs1 + static_cast< INT_4U >( dt1 );
    INT_4U nextgn = gn1 + static_cast< INT_4U >( fmod( dt1, 1.0 ) * 1e9 );
    if ( nextgn >= 1000000000 )
    {
        ++nextgs;
        nextgn -= 1000000000;
    }

    if ( nextgs != gs2 || nextgn != gn2 )
    {
        throw SWIGEXCEPTION( "incompatible_frame: The second Frame "
                             "structure does not start where the first "
                             "ended." );
    }

    // Now concatenate all data:
    LdasContainer* concatContainer( 0 );
    try
    {
        concatContainer = new LdasContainer( *c1 );
        // If there is 'type' data ===> concatenate it.
        const LdasContainer* cn1( findContainerType( *c1, "type" ) );
        const LdasContainer* cn2( findContainerType( *c2, "type" ) );
        //:TODO: This should change in a future: for now only 
        //:TODO:   container with size = 1 is supported( will be: size >= 1 ).
        LdasElement* elem( concatContainer->find( "type", 1 ) );
        LdasContainer* cdata( 0 );
   
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD ||
		cn1->size() == 0 || cn2 == 0 )
	    {
		throw SWIGEXCEPTION( "bad_frame: type" );
	    }

	    cdata = dynamic_cast< LdasContainer* >( elem );              
	    cdata->erase( cdata->begin(), cdata->end() );
	    cdata->push_back( concatElementData( cn1, cn2 ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
   
        // If there is 'user' data ===> concatenate it.
        elem = concatContainer->find( "user", 1 );
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: user" );
	    }   
	    cn1 = findContainerType( *c1, "user" );
	    cn2 = findContainerType( *c2, "user" );
	    if( cn1->size() == 0 || cn2 == 0 ) 
	    {
		throw SWIGEXCEPTION( "bad_frame: user" );
	    }

	    cdata = dynamic_cast< LdasContainer* >( elem ); 
	    cdata->erase( cdata->begin(), cdata->end() );
	    cdata->push_back( concatElementData( cn1, cn2 ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }

        // 'detectSim' data should be the same.
        cn1 = findContainerType( *c1, "detectSim" );
        cn2 = findContainerType( *c2, "detectSim" );
        if( ( cn1 == 0 && cn2 != 0 ) ||
            ( cn1 != 0 && cn2 == 0 ) )
        {
	    throw SWIGEXCEPTION( "bad_frame: detectSim" );
        }
        
        if( cn1 != 0 && cn2 != 0 &&
            cn1->size() != cn2->size() )   
        {
	    throw SWIGEXCEPTION( "incompatible_frame: detectSim" );
        }
      
        // This is to disable time consuming detector checking
        // for two frames. Since error checking is already 
        // complete for the 'run' and 'frame' parameters, this 
        // should garantee that detector data will be the same 
        // for these frames unless software generating detector
        // data structures is buggy.
#ifdef FRAME_DETECTOR_CHECK
        if ( cn1 != cn2 )
        {
	    throw SWIGEXCEPTION( "incompatible_frame_data: detectSim" );     
        }
#endif

        // 'detectProc' data should be the same.
        cn1 = findContainerType( *c1, "detectProc" );
        cn2 = findContainerType( *c2, "detectProc" );
        if( ( cn1 == 0 && cn2 != 0 ) ||
            ( cn1 != 0 && cn2 == 0 ) )
        {
	    throw SWIGEXCEPTION( "bad_frame: detectProc" );
        }
   
        if( cn1 != 0 && cn2 != 0 &&
            cn1->size() != cn2->size() )   
        {
	    throw SWIGEXCEPTION( "incompatible_frame: detectProc" );
        }
   
#ifdef FRAME_DETECTOR_CHECK
        if ( cn1 != cn2 )
        {
	    throw SWIGEXCEPTION( "incompatible_frame: detectProc" );     
        }
#endif

        // Append 'history' data.
        elem = concatContainer->find( "history", 1 );
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: history" );
	    }
	    cn2 = findContainerType( *c2, "history" );
	    if( cn2 != 0 )
	    {
		cdata = dynamic_cast< LdasContainer* >( elem );
		if( cdata->size() == 0 )
		{
		    throw SWIGEXCEPTION( "bad_frame: history" );
		}

		for( unsigned int i = 0; i < cn2->size(); ++i )
		{
		    cdata->push_back( ( *cn2 )[ i ] );
		}
	    }
        }   

        // Concatenate 'rawData'.        
        elem = concatContainer->find( "rawData", 1 );      
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: rawData" );
	    }
	    cn1 = findContainerType( *c1, "rawData" );
	    cn2 = findContainerType( *c2, "rawData" );
	    if( cn1->size() == 0 || cn2 == 0 ||
		cn1->size() != cn2->size() ) 
	    {
		throw SWIGEXCEPTION( "bad_frame: rawdata" );
	    }
  
	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cdata->erase( cdata->begin(), cdata->end() );
	    //for( unsigned int i = 0; i < cn1->size(); ++i )
	    //{
	    cdata->push_back( concatRawData( cn1, cn2 ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
	    //}
        }
   
        // Concatenate 'procData'.        
        elem = concatContainer->find( "procData", 1 );
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: procData" );
	    } 
	    cn1 = findContainerType( *c1, "procData" );
	    cn2 = findContainerType( *c2, "procData" );
	    if( cn1->size() == 0 || cn2 == 0 ||
		cn1->size() != cn2->size() )
	    {
		throw SWIGEXCEPTION( "bad_frame: procData" );
	    }

	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cdata->erase( cdata->begin(), cdata->end() );
	    for( unsigned int i = 0; i < cn1->size(); ++i )
	    {
		cdata->push_back( concatProcData
				  ( dynamic_cast< const LdasContainer* >
				    ( ( *cn1 )[ i ] ), 
				    dynamic_cast< const LdasContainer* >
				    ( ( *cn2 )[ i ] ) ),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    }
        }

        // Concatenate 'simData'.        
        elem = concatContainer->find( "simData", 1 );
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: simData" );
	    }    
	    cn1 = findContainerType( *c1, "simData" );
	    cn2 = findContainerType( *c2, "simData" );
	    if( cn1->size() == 0 || cn2 == 0 ||
		cn1->size() != cn2->size() )
	    {
		throw SWIGEXCEPTION( "bad_frame: simData" );
	    }

	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cdata->erase( cdata->begin(), cdata->end() );
	    for( unsigned int i = 0; i < cn1->size(); ++i )
	    {
		cdata->push_back( concatSimData
				  ( dynamic_cast< const LdasContainer* >
				    ( ( *cn1 )[ i ] ), 
				    dynamic_cast< const LdasContainer* >
				    ( ( *cn2 )[ i ] ) ),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    }
        }

        // Append 'event'.
        elem = concatContainer->find( "event", 1 );
        if( elem != 0 )
        {   
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: event" );
	    }
	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cn2 = findContainerType( *c2, "event" );
	    if( cn2 == 0 ||
		cdata->size() == 0 ||
		cn2->size() == 0 )
	    {
		throw SWIGEXCEPTION( "bad_frame: event" );
	    }

	    for( unsigned int i = 0; i < cn2->size(); ++i )
	    {
		cdata->push_back( ( *cn2 )[ i ] );
	    }
        }

        // Append 'simEvent'.
        elem = concatContainer->find( "simEvent", 1 );
        if( elem != 0 )
        {   
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: simEvent" );
	    }
	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cn2 = findContainerType( *c2, "simEvent" );
	    if( cn2 == 0 ||
		cdata->size() == 0 ||
		cn2->size() == 0 )
	    {
		throw SWIGEXCEPTION( "bad_frame: simEvent" );
	    }

	    for( unsigned int i = 0; i < cn2->size(); ++i )
	    {
		cdata->push_back( ( *cn2 )[ i ] );
	    }
        }
   
        // Append 'summaryData'.
        elem = concatContainer->find( "summaryData", 1 );
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: summaryData" );
	    }
	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cn2 = findContainerType( *c2, "summaryData" );
	    if( cn2 == 0 ||
		cdata->size() == 0 ||
		cn2->size() == 0 )
	    {
		throw SWIGEXCEPTION( "bad_frame: summaryData" );
	    }

	    for( unsigned int i = 0; i < cn2->size(); ++i )
	    {
		cdata->push_back( ( *cn2 )[ i ] );
	    }
        }
   
        // Concatenate 'auxData'.
        elem = concatContainer->find( "auxData", 1 );   
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: auxData" );
	    }   
	    cn1 = findContainerType( *c1, "auxData" );
	    cn2 = findContainerType( *c2, "auxData" );
	    if( cn1->size() == 0 || cn2 == 0 )
	    {
		throw SWIGEXCEPTION( "bad_frame: auxData" );
	    }

	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cdata->erase( cdata->begin(), cdata->end() );
	    cdata->push_back( concatElementData( cn1, cn2 ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }   

        // Concatenate 'auxTable'.
        elem = concatContainer->find( "auxtable", 1 );   
        if( elem != 0 )
        {
	    if( elem->getElementId() != ILwd::ID_ILWD )
	    {
		throw SWIGEXCEPTION( "bad_frame: auxtable" );
	    }   
	    cn1 = findContainerType( *c1, "auxtable" );
	    cn2 = findContainerType( *c2, "auxtable" );
	    if( cn1->size() == 0 || cn2 == 0 ||
		cn1->size() != cn2->size() )
	    {
		throw SWIGEXCEPTION( "bad_frame: auxtable" );
	    }

	    cdata = dynamic_cast< LdasContainer* >( elem );
	    cdata->erase( cdata->begin(), cdata->end() );
	    for ( unsigned int i = 0; i < cn1->size(); ++i )
	    {
		cdata->push_back( concatTable
				  ( dynamic_cast< const LdasContainer* >
				    ( ( *cn1 )[ i ] ),
				    dynamic_cast< const LdasContainer* >
				    ( ( *cn2 )[ i ] ) ),
				  ILwd::LdasContainer::NO_ALLOCATE,
				  ILwd::LdasContainer::OWN );
	    }
        }

        // Update 'dt' field
        elem = concatContainer->find( "dt" );
        const REAL_8 newdt[] = { dt1 + dt2 };
        dynamic_cast< LdasArray< REAL_8 >& >( *elem ).setData( newdt );
    }
    catch(...)
    {
	delete concatContainer;
	concatContainer = (ILwd::LdasContainer*)NULL;
	throw;
    }

    return concatContainer;
   
}
#endif /* CORE_API */
