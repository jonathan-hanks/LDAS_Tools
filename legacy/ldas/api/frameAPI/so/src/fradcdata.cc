#include "ldas_tools_config.h"

// System Header Files
#include <cmath>
#include <sstream>

// Local Header Files
#include "FrameException.hh"
#include "fradcdata.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "util.hh"


using FrameCPP::FrAdcData;
using FrameCPP::FrVect;
using FrameCPP::Time;

#if ! CORE_API
#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
#endif /* ! CORE_API */
   
using namespace std;   

#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

enum
{
    FR_NAME,
    FR_COMMENT,
    FR_CHANNELGROUP,
    FR_CHANNELNUMBER,
    FR_NBITS,
    FR_BIAS,
    FR_SLOPE,
    FR_UNITS,
    FR_SAMPLERATE,
    FR_TIMEOFFSETS,
    FR_TIMEOFFSETN,
    FR_FSHIFT,
    FR_PHASE,
    FR_DATAVALID,
    FR_DATA,
    FR_AUX
};


//!ignore_begin:
//--------------------------------------------------------------------------------
//
//: Initialize query hash.
//
//!usage_ooi: QueryHash queryHash = initAdcDataHash();
//
//!return: QueryHash queryHash - query hash object initialized with ADC structure
//+ attribute names.
//
//--------------------------------------------------------------------------------

QueryHash initAdcDataHash()
{
    QueryHash h;
    h[ "name"        ] = FR_NAME;
    h[ "comment"     ] = FR_COMMENT;
    h[ "channelgroup"  ] = FR_CHANNELGROUP;
    h[ "channelnumber" ] = FR_CHANNELNUMBER;
    h[ "nbits"       ] = FR_NBITS;
    h[ "bias"        ] = FR_BIAS;
    h[ "slope"       ] = FR_SLOPE;
    h[ "units"       ] = FR_UNITS;
    h[ "samplerate"  ] = FR_SAMPLERATE;
    h[ "timeoffsets" ] = FR_TIMEOFFSETS;
    h[ "timeoffsetn" ] = FR_TIMEOFFSETN;
    h[ "fshift"      ] = FR_FSHIFT;
    h[ "phase"       ] = FR_PHASE;
    h[ "datavalid"   ] = FR_DATAVALID;
    h[ "data"        ] = FR_DATA;
    h[ "aux"         ] = FR_AUX;
    return h;
}


//--------------------------------------------------------------------------------
//
// Query hash for the ADC structure.
//
//--------------------------------------------------------------------------------
static QueryHash adcDataHash( initAdcDataHash() );    
    
using namespace FrameAPI::FrAdcData;

#if CORE_API
void FrameAPI::FrAdcData::
appendAuxData( FrameCPP::FrAdcData& Dest,
	       const FrameCPP::FrAdcData& Source )
{
  for ( FrameCPP::FrAdcData::aux_type::const_iterator
	  a( Source.RefAux( ).begin( ) );
	a != Source.RefAux( ).end( );
	a++ )
  {
    //:TODO: At some point, may need to put in an 'empty' container to
    //:TODO:   preserve the fact that a frame had no Auxiliary info.
    //:TODO: If the above action is taken, then all segments must be
    //:TODO:   checked for aux info. If no adc segments have aux info
    //:TODO:   then don't write any 'empty' containers.
    FrameCPP::FrAdcData::aux_type::iterator
      dref = Dest.RefAux( ).begin( ),
      last = Dest.RefAux( ).end( );

    while ( (dref != last ) &&
	    ( (*dref)->GetName( ).compare( (*a)->GetName( ) )  != 0 ) )
    {
      if ( ( (*dref)->GetName( ).length( ) == 0 ) &&
	   ( (*a)->GetName( ).length( ) == 0 )  )
      {
	break;
      }
      dref++;
    }
    if ( dref != last )
    {
      FrameAPI::FrVect::FrVectList	vect_list;
      FrameCPP::FrAdcData::aux_type	a_aux;
      FrameCPP::FrAdcData::aux_type	dref_aux;

      a_aux.append( *a );
      dref_aux.append( *dref );

      vect_list.push_back( &dref_aux );
      vect_list.push_back( &a_aux );

      FrameCPP::FrAdcData::aux_type::value_type
	result( FrameAPI::FrVect::concat( vect_list ) );
      Dest.RefAux( ).erase( dref, dref + 1 );
      Dest.RefAux( ).append( result );
    }
    else
    {
      Dest.RefAux( ).append( *a );
    }
  }
}
#endif /* CORE_API */

#if CORE_API
FrameCPP::FrAdcData* FrameAPI::FrAdcData::
cloneHeader( const FrameCPP::FrAdcData& Source )
{
  return new FrameCPP::FrAdcData( Source.GetName( ),
				  Source.GetChannelGroup( ),
				  Source.GetChannelNumber( ),
				  Source.GetNBits( ),
				  Source.GetSampleRate( ),
				  Source.GetBias( ),
				  Source.GetSlope( ),
				  Source.GetUnits( ),
				  Source.GetFShift( ),
				  Source.GetTimeOffset( ),
				  Source.GetDataValid( ),
				  Source.GetPhase( ) );
}
#endif /* CORE_API */

#if CORE_API
FrameCPP::FrAdcData* FrameAPI::FrAdcData::
concat( const std::list< FrameCPP::Base* >& Segment )
{
  //:TODO: Need to ad support for aux data
  //-------------------------------------------------------------------
  // Loop over the parts getting the statistics
  //-------------------------------------------------------------------

  AT();

  INT_4U	samples( 0 );
  INT_2U	data_valid( 0 );

  AT();
  for( std::list< FrameCPP::Base* >::const_iterator s( Segment.begin() );
       s != Segment.end( );
       s++ )
  {
    AT();
    FrameCPP::FrAdcData*	src( dynamic_cast< FrameCPP::FrAdcData* >(*s) );
    
    samples += FrameAPI::FrVect::getSamples( src->RefData( ).begin( ),
					     src->RefData( ).end( ) );
    if ( ( data_valid == 0 ) || ( src->GetDataValid( ) != 0 ) )
    {
      AT();
      data_valid = src->GetDataValid( );
    }
  }
  
  //-------------------------------------------------------------------
  // Allocate a single channel to hold the data
  //-------------------------------------------------------------------
  AT();
  std::unique_ptr< FrameCPP::FrAdcData >
    c( FrameAPI::FrAdcData::cloneHeader( *dynamic_cast< FrameCPP::FrAdcData* >
					 ( Segment.front( ) ) ) );
  AT();
  c->SetDataValid( data_valid );

  //-------------------------------------------------------------------
  // Fill in the data
  //-------------------------------------------------------------------

  AT();
  FrameAPI::FrVect::FrVectList	vector_list;

  AT();
  for( std::list< FrameCPP::Base* >::const_iterator s( Segment.begin() );
       s != Segment.end( );
       s++ )
  {
    AT();
    FrameCPP::FrAdcData*	src( dynamic_cast< FrameCPP::FrAdcData* >(*s) );
    
    // Append all comments
    AT();
    c->AppendComment( src->GetComment( ) );
    // Append auxiliary information
    AT();
    FrameAPI::FrAdcData::appendAuxData( *c, *src );
    // Build a list of vectors to concat
    vector_list.push_back( &( src->RefData( ) ) );
  }

  //-------------------------------------------------------------------
  // Create the new vector and attach it to the channel
  //-------------------------------------------------------------------

  AT();
  FrameCPP::FrAdcData::data_type::value_type
    data( FrameAPI::FrVect::concat( vector_list ) );
  c->RefData( ).append( data );
#if LM_DEBUG
  std::cerr << "DEBUG: Number of Points: " << c->RefData( ).back( )->GetDim( 0 ).GetNx( )
	    << std::endl;
#endif /* LM_DEBUG */

  //-------------------------------------------------------------------
  // Return the results
  //-------------------------------------------------------------------

  AT();
  return c.release( );
}
#endif /* CORE_API */

#if CORE_API
bool
areAdcsCompatable( const FrAdcData& Adc1, const FrAdcData& Adc2 )
{
  const string& adc1_name( Adc1.GetName() );
  const string& adc2_name( Adc2.GetName() );
   
  if ( strcasecmp( adc1_name.c_str( ), adc2_name.c_str( ) ) != 0 )
  {
    throw SWIGEXCEPTION( "incompatible_channels" );
  }

  // Do simple comparison of simple datatypes within the class
#define ADC_COMPARE(LM_ATTR,LM_EXCEPTION) \
  if ( Adc1.LM_ATTR() != Adc2.LM_ATTR() ) \
  { \
    std::ostringstream msg; \
    msg << LM_EXCEPTION << " (" << Adc1.LM_ATTR( ) << " != " << Adc2.LM_ATTR( ) << ") - Channel Name: " << Adc1.GetName( ) ; \
    throw SWIGEXCEPTION( msg.str( ) ); \
  }

  ADC_COMPARE(GetChannelGroup,"incompatible_adcdata: channelgroup");
  ADC_COMPARE(GetChannelNumber,"incompatible_adcdata: channelnumber");
  ADC_COMPARE(GetBias,"incompatible_adcdata: bias");
  ADC_COMPARE(GetSlope,"incompatible_adcdata: slope");
  ADC_COMPARE(GetUnits,"incompatible_adcdata: units");
  ADC_COMPARE(GetSampleRate,"incompatible_adcdata: samplerate");
  ADC_COMPARE(GetFShift,"incompatible_adcdata: fshift");
  ADC_COMPARE(GetPhase,"incompatible_adcdata: phase");
    
#undef ADC_COMPARE

  return true;
}
#endif /* CORE_API */

//-----------------------------------------------------------------------------
#if CORE_API
std::string
getAttribute( const FrAdcData& adcData, std::deque< Query >& dq,
	      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        adcDataHash.find( q.getName().c_str() );
    if ( iter == adcDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << adcData.GetName();
                break;
                
            case FR_COMMENT:
                res << adcData.GetComment();
                break;
                
            case FR_CHANNELGROUP:
                res << adcData.GetChannelGroup();
                break;

	    case FR_CHANNELNUMBER:
                res << adcData.GetChannelNumber();
                break;
                
            case FR_NBITS:
                res << adcData.GetNBits();
                break;
                
            case FR_BIAS:
                res << adcData.GetBias();
                break;

            case FR_SLOPE:
                res << adcData.GetSlope();
                break;

            case FR_UNITS:
                res << adcData.GetUnits();
                break;

            case FR_SAMPLERATE:
                res << adcData.GetSampleRate();
                break;

            case FR_TIMEOFFSETS:
	        res << INT_4S( adcData.GetTimeOffset( ) );
                break;
                
            case FR_TIMEOFFSETN:
	        {
		    REAL_8 x = adcData.GetTimeOffset( );
		    x -= INT_4S( x );
		    x *= 1000000000L;
		    res << x;
		}
                break;

            case FR_FSHIFT:
                res << adcData.GetFShift();
                break;

            case FR_PHASE:
                res << adcData.GetPhase();
                break;

            case FR_DATAVALID:
                res << adcData.GetDataValid();
                break;

            case FR_DATA:
                return ::getAttribute< FrVect >( adcData.RefData(), dq );
            
            case FR_AUX:
                return ::getAttribute< FrVect >( adcData.RefAux(), dq );

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_DATA:
                return getAttribute(
		    getContained( adcData.RefData(), q, start, index ),
                    dq, start, index + 1 );
                break;
                
            case FR_AUX:
                return getAttribute(
                    getContained( adcData.RefAux(), q, start, index ),
                    dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }


    return res.str();
}
#endif /* CORE_API */


#if ! CORE_API
LdasElement* getData( const FrAdcData& adcData, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index,
                      const Time& gtime, const REAL_8& dt )
{
    if ( dq.size() == 0 )
    {
        return adcData2container( adcData, gtime, dt );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        adcDataHash.find( q.getName().c_str() );
    if ( iter == adcDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_DATA:
	    return getData( getContained( adcData.RefData(), q, start, index ),
			    dq, start, index + 1 );
            break;
                
        case FR_AUX:
            return getData( getContained( adcData.RefAux(), q, start, index ),
			    dq, start, index + 1 );
            break;

        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}

#endif /* ! CORE_API */

//!ignore_end:

//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrAdcData.
//
//!usage_ooi: FrAdcData *adcData = container2adcData ( c )
//
//!param: const LdasContainer& c - ADC structure container.
//
//!return: FrAdcData* adcData - newly allocated FrameCPP::FrAdcData structure
//
//!exc: SwigException - invalid format of the LDAS container `c'.
//
//--------------------------------------------------------------------------------

#if ! CORE_API

FrAdcData* container2adcData( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
   
    if ( strcasecmp( c_name.c_str(), "adcdata" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrAdcData container." );
    }
    
    FrAdcData* adc = new FrAdcData(
        c.getName( 0 ),
        *findArrayType< INT_4U >( c, "channelgroup" ).getData(),
        *findArrayType< INT_4U >( c, "channelnumber" ).getData(),
        *findArrayType< INT_4U >( c, "nBits" ).getData(),
        *findArrayType< REAL_8 >( c, "sampleRate" ).getData(),
        *findArrayType< REAL_4 >( c, "bias" ).getData(),
        *findArrayType< REAL_4 >( c, "slope" ).getData(),
        findLStringType( c, "units" ).getString(),
        *findArrayType< REAL_8 >( c, "fShift" ).getData(),
	*findArrayType< REAL_8 >( c, "timeOffset" ).getData( ),
        *findArrayType< INT_2U >( c, "dataValid" ).getData() != 0,
        *findArrayType< REAL_4 >( c, "phase" ).getData() );
    adc->AppendComment( findLStringType( c, "comment" ).getString() );
    try
    {
        const LdasContainer* aux( findContainerType( c, "aux" ) );
        if ( aux != 0 )
        {
            for ( unsigned int i = 0; i < aux->size(); ++i )
            {
	        FrameCPP::FrAdcData::aux_type::value_type
		  v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*aux)[ i ] ) ) );
                adc->RefAux().append( v );
            }
        }
    }
    catch( std::bad_cast& )
    {
      delete adc;
      adc = (FrameCPP::FrAdcData*)NULL;
      throw SWIGEXCEPTION( "invalid_aux_format" );
    }
    
    try
    {
        const LdasContainer* data( findContainerType( c, "data" ) );
        if ( data != 0 )
        {
            for ( unsigned int i = 0; i < data->size(); ++i )
            {
	        FrameCPP::FrAdcData::aux_type::value_type
		  v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*data)[ i ] ) ) );
                adc->RefData().append( v );
            }
        }
    }
    catch( std::bad_cast& )
    {
      delete adc;
      adc = (FrameCPP::FrAdcData*)NULL;
      throw SWIGEXCEPTION( "invalid_data_format" );
    }

    return adc;
}

#endif /* ! CORE_API */


//--------------------------------------------------------------------------------
//
//: Convert an FrAdcData to LdasContainer.
//
//!usage_ooi: LdasContainer *adcContainer = adcData2container ( adc, gtime, dt, c )
//
//!param: const FrAdcData& adc - FrameCPP::FrAdcData struture pointer.
//!param: const Time& gtime - Frame time.
//!param: const REAL_8& dt - Frame delta.
//!param: LdasContainer* c - optional result container.
//
//!return: LdasContainer* adcContainer - newly allocated container or `c'.
//
//--------------------------------------------------------------------------------

#if ! CORE_API

LdasContainer* adcData2container( const FrAdcData& adc, const Time& gtime,
                                  const REAL_8& dt, LdasContainer* c )
{
    if ( c == NULL )
      {
        c = new LdasContainer( "::AdcData" );
      }
    c->setName( 0, adc.GetName() );

    ostringstream ss;
    ss.precision( REAL_8_DIGITS + 1 );
    ss << gtime.getSec();
    c->appendName( ss.str() );

    ss.str( "" );
    ss << gtime.getNSec();
    c->appendName( ss.str() );

    c->appendName( "Frame" );

    LdasString* comment( new LdasString( adc.GetComment(), "comment" ) );
    LdasArray< INT_4U >* channelgroup(
        new LdasArray< INT_4U >( adc.GetChannelGroup(), "channelGroup" ) );
    LdasArray< INT_4U >* channelnumber(
        new LdasArray< INT_4U >( adc.GetChannelNumber(), "channelNumber" ) );
    LdasArray< INT_4U >* nBits(
        new LdasArray< INT_4U >( adc.GetNBits(), "nBits" ) );
    LdasArray< REAL_4 >* bias(
        new LdasArray< REAL_4 >( adc.GetBias(), "bias" ) );
    LdasArray< REAL_4 >* slope(
        new LdasArray< REAL_4 >( adc.GetSlope(), "slope" ) );
    LdasString* units( new LdasString( adc.GetUnits(), "units" ) );
    LdasArray< REAL_8 >* sampleRate(
        new LdasArray< REAL_8 >( adc.GetSampleRate(), "sampleRate" ) );
    LdasArray< REAL_8 >* timeOffset(
        new LdasArray< REAL_8 >( adc.GetTimeOffset( ), "timeOffset" ) );
    LdasArray< REAL_8 >* fShift(
        new LdasArray< REAL_8 >( adc.GetFShift(), "fShift" ) );
    LdasArray< REAL_4 >* phase(
        new LdasArray< REAL_4 >( adc.GetPhase(), "phase" ) );
    LdasArray< INT_2U >* dataValid(
        new LdasArray< INT_2U >( adc.GetDataValid(), "dataValid" ) );    
    LdasArray< REAL_8 >* e_dt( new LdasArray< REAL_8 >( dt, "dt" ) );

    c->push_back( comment, 
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( channelgroup,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( channelnumber,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( nBits,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( bias,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( slope,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( units,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( sampleRate,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( timeOffset,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( fShift,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( phase,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( dataValid,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( e_dt,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );


    size_t dataSize( adc.RefData().size() );
    if ( dataSize > 0 )
    {
        LdasContainer* data(
            new LdasContainer( ":data:Container(Vect):Frame" ) );
        c->push_back( data,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );

        FrAdcData::const_iterator iter( adc.RefData().begin() );
        for( size_t i = dataSize; i != 0; --i, ++iter )
        {
            data->push_back( vect2array( **iter ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
        }
    }

    size_t auxSize( adc.RefAux().size() );
    if ( auxSize > 0 )
    {
        LdasContainer* aux(
            new LdasContainer( ":aux:Container(Vect):Frame" ) );
        c->push_back( aux, 
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );

        FrAdcData::const_aux_iterator iter( adc.RefAux().begin() );
        for( size_t i = auxSize; i != 0; --i, ++iter )
        {
            aux->push_back( vect2array( **iter ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
        }
    }
    
#if LM_DEBUG && LM_DEBUG > 1
    std::cerr << "DEBUG: ";
    c->write( 2, 2, std::cerr, ILwd::ASCII );
    std::cerr << std::endl;
#endif
    return c;
}

#endif /* ! CORE_API */


//--------------------------------------------------------------------------------
//
//: Insert FrAdcData container into Frame container.
//
//!usage_ooi: insertAdcData( frameContainer, adcContainer );
//
//!param: LdasContainer& fr - Frame container.
//!param: LdasContainer& adc - FrAdcData container.
//!param: const bool validateTime - A flag to enable/disable time stamp
//+       validation for data insertion. True to enable, false to disable.       
//+       Default is TRUE.   
//
//!exc: SwigException - bad container format.
//
//--------------------------------------------------------------------------------

#if ! CORE_API

void insertAdcData( LdasContainer& fr, LdasContainer& adc,
   const bool validateTime )
{
    // Find the GTime & dt elements of the frame container. 
    LdasElement* fr_gtime = fr.find( "gtime" );
    LdasElement* fr_dt = fr.find( "dt" );
    LdasElement* adc_dt = adc.find( "dt" );

    // Check to make sure the frame data is valid
    if ( fr_gtime == 0 || fr_gtime->getElementId() != ILwd::ID_INT_4U )
    {
       throw SWIGEXCEPTION( "bad_frame: gtime" );
    }
   
    if( fr_dt == 0 || fr_dt->getElementId() != ILwd::ID_REAL_8 )
    {
       throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check to make sure the adc data is valid
    if ( adc_dt == 0 || adc_dt->getElementId() != ILwd::ID_REAL_8 )
    {
        throw SWIGEXCEPTION( "bad_adcdata: dt" );
    }

    // Check the characteristics of the GTime element.
    LdasArray< INT_4U >& gtime(
        dynamic_cast< LdasArray< INT_4U >& >( *fr_gtime ) );
    if ( gtime.getNDim() != 1 || gtime.getDimension( 0 ) != 2 )
    {
        throw SWIGEXCEPTION( "bad_frame: gtime" );
    }

    // Check the characteristics of the Frame Dt element.
    LdasArray< REAL_8 >& fdt( dynamic_cast< LdasArray< REAL_8 >& >( *fr_dt ) );
    if ( fdt.getNDim() != 1 || fdt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check the characteristics of the adc Dt element.
    LdasArray< REAL_8 >& adt(
        dynamic_cast< LdasArray< REAL_8 >& >( *adc_dt ) );
    if ( adt.getNDim() != 1 || adt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_adcdata: dt" );
    }

    // Make sure there is a RawData object in the frame
    LdasElement* t = fr.find( "rawdata", 1 );
    if ( t == 0 || t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_frame: no rawdata" );
    }
    LdasContainer& rawdata( dynamic_cast< LdasContainer& >( *t ) );

    // If 'fdt' is not zero, then we must have some data in the frame.  Check
    // to make sure the start times and length for the data matches that of the
    // frame.

    //__FIXME__ if fdt.GetData()[ 0 ] == 1 we always get an exception.

    if( validateTime && fdt.getData()[ 0 ] != 0 )
    {
       INT_4U time;
        
       // Check the GPS seconds.
       std::string tmp( adc.getName( 3 ) );
       time = strtoul( tmp.c_str(), 0, 10 );
       if ( time != gtime.getData()[ 0 ] )
       {
          throw SWIGEXCEPTION( "incompatible_time(seconds): frame vs. addata" );
       }

       // Check the GPS nanoseconds.
       tmp = adc.getName( 4 );
       time = strtoul( tmp.c_str(), 0, 10 );
       if ( time != gtime.getData()[ 1 ] )
       {
          throw SWIGEXCEPTION( "incompatible_time(nanoseconds): "
                               "frame vs. adcdata" );
       }

       // Check the length
       if ( fdt.getData()[ 0 ] < adt.getData()[ 0 ] )
       {
          throw SWIGEXCEPTION( "incompatible_length: frame vs. adcdata" );
       }
    }


    // Find the adcdata container in rawdata if it exists
    t = rawdata.find( "adcdata", 1 );
    if ( t == 0 )
    {
        // it didn't exist so create it.
        rawdata.push_back( LdasContainer( ":adcdata:Container(AdcData)" ) );
        t = rawdata.back();
    }
    // Check the adcdata container
    else if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_rawdata" );
    }

#ifdef RESET_FRAME_TIME   
    // If this is the first data element in the frame, set the frame GPS time
    // and length to correspond to the inserted object.
    if( fdt.GetData()[ 0 ] == 0 && 
        gtime.GetData()[ 0 ] == 0 && gtime.GetData()[ 1 ] == 0 )
    {
        std::string tmp( adc.GetName( 3 ) );
        gtime.GetData()[ 0 ] = strtoul( tmp.c_str(), 0, 10 );
        tmp = adc.GetName( 4 );
        gtime.GetData()[ 1 ] = strtoul( tmp.c_str(), 0, 10 );
        fdt.GetData()[ 0 ] = adt.GetData()[ 0 ];
    }    
#endif

    // OK, everything checks out so add the adcdata
    dynamic_cast< LdasContainer& >( *t ).push_back( adc );
    return;
}

#endif /* ! CORE_API */

//--------------------------------------------------------------------------------
//
//: Concatenate two FrAdcData containers.
//
//!usage_ooi: LdasContainer* concatContainer = concatAdcData( c1, c2 );
//
//!param: const LdasContainer* c1 - first FrAdcData container.
//!param: const LdasContainer* c2 - second FrAdcData container.
//
//!exc: SwigException - bad container data.
//
//--------------------------------------------------------------------------------

#if ! CORE_API

LdasContainer* concatAdcData(
   const LdasContainer* c1, const LdasContainer* c2 )
try
{
    // Make sure this is frame data.
    if ( c1->getName( 5 ) != "Frame" ||
         c2->getName( 5 ) != "Frame" )
    {
        throw SWIGEXCEPTION( "bad_adcdata: not_frame_data" );
    }

    static const size_t name_pos( 2 );
    string c1_name( c1->getName( name_pos ) );
    string c2_name( c2->getName( name_pos ) );   

    if ( strcasecmp( c1_name.c_str(), "adcdata" ) != 0 ||
         strcasecmp( c2_name.c_str(), "adcdata" ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_types" );
    }

    static const size_t c_name_pos( 0 );
    c1_name = c1->getName( c_name_pos );
    c2_name = c2->getName( c_name_pos );   
   
    if ( strcasecmp( c1_name.c_str(), c2_name.c_str() ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_channels" );
    }
   
    //:TODO: For now don't concatenate FrAdcDatas with name='Slow',
    // this is for temporary use only!!!
    if( c1->getName( 0 ) == "Slow" )
    {
       return new LdasContainer(*c1);
    }

    const LdasArray< INT_4U >& grp1( findArrayType< INT_4U >( *c1, "channelgroup" ) );
    const LdasArray< INT_4U >& grp2( findArrayType< INT_4U >( *c2, "channelgroup" ) );

    if ( grp1 != grp2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: channelgroup" );
    }


    const LdasArray< INT_4U >& ch1( findArrayType< INT_4U >( *c1, "channelnumber" ) );
    const LdasArray< INT_4U >& ch2( findArrayType< INT_4U >( *c2, "channelnumber" ) );

    if ( ch1 != ch2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: channelnumber" );
    }

    const LdasArray< INT_4U >& nb1( findArrayType< INT_4U >( *c1, "nBits" ) );
    const LdasArray< INT_4U >& nb2( findArrayType< INT_4U >( *c2, "nBits" ) );

    if ( nb1 != nb2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: nbits" );
    }
    
    const LdasArray< REAL_4 >& b1( findArrayType< REAL_4 >( *c1, "bias" ) );
    const LdasArray< REAL_4 >& b2( findArrayType< REAL_4 >( *c2, "bias" ) );

    if ( b1 != b2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: bias" );
    }
    
    const LdasArray< REAL_4 >& sl1( findArrayType< REAL_4 >( *c1, "slope" ) );
    const LdasArray< REAL_4 >& sl2( findArrayType< REAL_4 >( *c2, "slope" ) );

    if ( sl1 != sl2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: slope" );
    }
    
    const LdasString& u1( findLStringType( *c1, "units" ) );
    const LdasString& u2( findLStringType( *c2, "units" ) );

    if ( u1 != u2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: units" );
    }

    const LdasArray< REAL_8 >& sr1( findArrayType< REAL_8 >( *c1, "samplerate" ) ); 
    const LdasArray< REAL_8 >& sr2( findArrayType< REAL_8 >( *c2, "samplerate" ) );    

    if ( sr1 != sr2 ||
         sr1.getNDim() != 1 || sr1.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: samplerate" );
    }
    
    // The (common) sampling rate of the two Adc's
    const REAL_8 sr( sr1.getData()[ 0 ] );
    
    const LdasArray< REAL_8 >& f1( findArrayType< REAL_8 >( *c1, "fshift" ) ); 
    const LdasArray< REAL_8 >& f2( findArrayType< REAL_8 >( *c2, "fshift" ) ); 

    if ( f1 != f2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: fshift" );
    }

    const LdasArray< REAL_4 >& p1( findArrayType< REAL_4 >( *c1, "phase" ) ); 
    const LdasArray< REAL_4 >& p2( findArrayType< REAL_4 >( *c2, "phase" ) ); 

    if ( p1 != p2 )
    {
        throw SWIGEXCEPTION( "incompatible_adcdata: phase" );
    }

    const LdasArray< INT_2U >& dv1( findArrayType< INT_2U >( *c1, "dataValid" ) ); 
    const LdasArray< INT_2U >& dv2( findArrayType< INT_2U >( *c2, "dataValid" ) );    
   
    if ( dv1.getNDim() != 1 || dv1.getDimension( 0 ) != 1 ||
         dv2.getNDim() != 1 || dv2.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_adcdata: dataValid" );
    }
   
    const LdasArray< REAL_8 >& d1( findArrayType< REAL_8 >( *c1, "dt" ) ); 
    const LdasArray< REAL_8 >& d2( findArrayType< REAL_8 >( *c2, "dt" ) ); 
    if ( d1.getNDim() != 1 || d1.getDimension( 0 ) != 1 ||
         d2.getNDim() != 1 || d2.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_adcdata: dt" );
    }

    const REAL_8 dt1( d1.getData()[ 0 ] );
    //:UNUSED: const REAL_8 dt2( d2.GetData()[ 0 ] );

    const LdasArray< REAL_8 >&
      t1( findArrayType< REAL_8 >( *c1, "timeoffset" ) );
    const LdasArray< REAL_8 >&
      t2( findArrayType< REAL_8 >( *c2, "timeoffset" ) );

    if ( t1 != t2 )
    {
      throw SWIGEXCEPTION( "incompatible_adcdata: timeoffset" );
    }

    REAL_8 timeOffset( t1.getData()[ 0 ] );

#ifdef OLD_VERIFICATION
    // What if the strtoul fails? Should use strstream?
    INT_4U gs1( strtoul( c1->getName( 3 ).c_str(), 0, 10 ) );
    INT_4U gs2( strtoul( c2->getName( 3 ).c_str(), 0, 10 ) );
    INT_4U gn1( strtoul( c1->getName( 4 ).c_str(), 0, 10 ) );
    INT_4U gn2( strtoul( c2->getName( 4 ).c_str(), 0, 10 ) );

    INT_4U nextgs = gs1 + static_cast< INT_4U >( dt1 );
    INT_4U nextgn = gn1 + static_cast< INT_4U >( fmod( dt1, 1.0 ) * 1e9 );
    if ( nextgn >= 1000000000 )
    {
        ++nextgs;
        nextgn -= 1000000000;
    }
   
    cout << "1: gs1=" << gs1 << " gn1=" << gn1 << " dt1=" << dt1
         << " nextgs=" << nextgs << " nextgn=" << nextgn << endl;
    cout << "2: gs2=" << gs2 << " gn2=" << gn2 << " dt2=" << dt2
         << endl;
   
    if ( nextgs != gs2 || nextgn != gn2 )
    {
        throw GAP_EXCEPTION( "incompatible_adcdata: The second FrAdcData "
                             "structure does not start where the first "
                             "ended." );
    }
#endif

    const LdasContainer* cn1( findContainerType( *c1, "data" ) );
    const LdasContainer* cn2( findContainerType( *c2, "data" ) );

    if ( cn1 == 0 || cn1->size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_adcdata: data" );
    }

    const LdasElement* elem1( ( *cn1 )[ 0 ] );
    const LdasArrayBase* data1( dynamic_cast< const LdasArrayBase* >( elem1 ) );
    // The calculated dt (span) of the data in the first container
    const REAL_8 calcdt( data1->getDimension( 0 ) / sr + timeOffset );

    if( ! FrameAPI::Continuous( *c1, *c2 ) )
    {
        throw GAP_EXCEPTION( "incompatible_adcdata: The second FrAdcData "
                             "structure does not start where the first "
                             "ended." );       
    }
    
    if ( fabs( ceil( calcdt ) - ceil( dt1 ) ) > 1e-9 )
    {
        std::ostringstream	msg;
        msg << "incomplete_adcdata: data: (calcdt: " << ceil( calcdt )
	    << " dt1: " << ceil( dt1 )
	    << ") for channel: " << c1_name;
        throw SWIGEXCEPTION( msg.str( ) );
    }


    LdasContainer* concatContainer( 0 );
    try
    {
       // Now create a copy of the first container and replace its data with
       // the new data.
       concatContainer = new LdasContainer( *c1 );
       LdasContainer* cdata( dynamic_cast< LdasContainer* >( 
          concatContainer->find( "data", 1 ) ) );
       cdata->erase( cdata->begin(), cdata->end() );
       cdata->push_back( concatElementData( cn1, cn2 ),
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
   
       // Update the 'dt' field also - safest way is to pull the relevant
       // data out of the new concatenated object and use that to calculate
       // the dt
       LdasElement* e( concatContainer->find( "dt" ) );
       
       // Get the new (concatenated) data length
       const LdasContainer* const concatDataContainer
           = findContainerType( *concatContainer, "data" );
       const LdasElement* const concatElt = ( *concatDataContainer )[ 0 ];
       const LdasArrayBase* const concatData =
           dynamic_cast< const LdasArrayBase* >( concatElt );

       // Calculate the new dt, rounding up to the nearest integer
       const REAL_8 newdt
           = ceil( timeOffset + concatData->getDimension(0)/sr);
       const REAL_8 newdtArray[] = { newdt };
       dynamic_cast< LdasArray< REAL_8 >& >( *e ).setData( newdtArray );

       // Update the 'dataValid' field.
       e = concatContainer->find( "dataValid" );
       INT_2U dataValid1( dv1.getData()[ 0 ] );
       INT_2U dataValid2( dv2.getData()[ 0 ] );
       const INT_2U newDataValid[] = { INT_2U( dataValid1 | dataValid2 ) };
       dynamic_cast< LdasArray< INT_2U >& >( *e ).setData( newDataValid );

       // Concatenate the comments
       e = concatContainer->find( "comment" );
       const LdasString& comment( findLStringType( *c2, "comment" ) );
       if( e == 0 ||
           e->getElementId() != ILwd::ID_LSTRING ) 
       {
          throw SWIGEXCEPTION( "bad_adcdata: comment" );
       }
       if( comment.getString().empty() == false )
       {
          dynamic_cast< LdasString& >( *e ).setString( 
             dynamic_cast< const LdasString& >( *e ).getString() + 
             "\n" + comment.getString() );    
       }

       // Concatenate 'aux' data if it's present in the container:
       cn1 = findContainerType( *c1, "aux" );
       cn2 = findContainerType( *c2, "aux" );   
       if( cn1 == 0 && cn2 == 0 )
       {
          return concatContainer;
       }

       if ( cn1 == 0 || cn1->size() == 0 )
       {
          throw SWIGEXCEPTION( "bad_adcdata: aux" );
       }

       elem1 = ( *cn1 )[ 0 ];
       data1 = dynamic_cast< const LdasArrayBase* >( elem1 );
       const REAL_8 calcdtAux( data1->getDimension( 0 ) / sr + timeOffset );
       if ( fabs( ceil( calcdtAux ) - ceil( dt1 ) ) > 1e-9 )
       {
	   std::ostringstream	msg;
	   msg << "incomplete_adcdata: data: (calcdt: " << ceil( calcdt )
	       << " dt1: " << ceil( dt1 )
	       << ") for channel: " << c1_name;
          throw SWIGEXCEPTION( "incomplete_adcdata: aux" );
       }

       cdata = dynamic_cast< LdasContainer* >( 
          concatContainer->find( "aux", 1 ) );
       cdata->erase( cdata->begin(), cdata->end() );
       cdata->push_back( concatElementData( cn1, cn2 ),
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
    }
    catch(...)
    {
       delete concatContainer;
       concatContainer = (LdasContainer*)NULL;
       throw;
    }

    return concatContainer;
}
catch( std::bad_alloc& )
{
   throw SWIGEXCEPTION( "adcdata: memory_allocation_failed" );
}
catch( std::bad_cast& )
{
   throw SWIGEXCEPTION( "adcdata: invalid_input_format" );
}

#endif /* ! CORE_API */

#if CORE_API

namespace FrameAPI
{
  template <>
  REAL_8
  SampleRate< FrameCPP::FrAdcData >( const FrameCPP::FrAdcData& C )
  {
    return C.GetSampleRate( );
  }
}

#endif /* CORE_API */
