extern "C" {
#include <tcl.h>
}

#include <fstream>
#include <iomanip>
#include <iostream>
#include <list>
#include <sstream>
#include <stdexcept>
#include <string>

#include "general/ErrorLog.hh"

#include "DeviceIOConfiguration.hh"

namespace FrameAPI
{
  // Forward declarations of functions that are defined in the
  // SWIG interface file
  extern std::unique_ptr< DeviceIOConfiguration::descriptions_type >
  TclObjToDeviceIOConfigurationDescriptionsType( Tcl_Interp*,
						 Tcl_Obj* );
}

namespace
{
  static const char* FRAME_RSC_FILENAME = "/ldas_outgoing/frameAPI/LDASframe.rsc";
  static const char* DISKCACHE_RSC_FILENAME = "/ldas_outgoing/diskcacheAPI/LDASdiskcache.rsc";

  //=====================================================================
  // Tcl Interpreter
  //=====================================================================
  class TclInterp
  {
  public:
    TclInterp( );
    ~TclInterp( );
    void DumpMountPT( ) const;
    void IngestRSC( const std::string& ResourceFilename );

    int EnableMemoryMappedIO( );
    int StreamBufferSize( );

  private:
    Tcl_Interp*		m_interp;
    bool		m_vars_valid;
    
    int				m_stream_buffer_size;
    int				m_enable_memory_mapped_io;
    std::list< std::string >	m_mount_pt;

    std::string::size_type	m_max_len_mount_pt;

    void		init_vars( );

    long		var_int( const char* VarName ) const;
  };

  //---------------------------------------------------------------------
  // Creation
  //---------------------------------------------------------------------
  TclInterp::
  TclInterp( )
    : m_interp( Tcl_CreateInterp( ) ),
      m_vars_valid( false )
  {
    Tcl_Init( m_interp );
  }

  //---------------------------------------------------------------------
  // Destructor
  //---------------------------------------------------------------------
  TclInterp::
  ~TclInterp( )
  {
    Tcl_DeleteInterp( m_interp );
  }

  void TclInterp::
  DumpMountPT( ) const
  {
    for( std::list< std::string >::const_iterator
	   cur = m_mount_pt.begin( ),
	   last = m_mount_pt.end( );
	 cur != last;
	 ++cur )
    {
      FrameAPI::DeviceIOConfiguration::size_type	buffer_size;
      bool						use_mmap_io;
      std::string					fstype;

      try
      {
	FrameAPI::DeviceIOConfiguration::
	  GetConfiguration( cur->c_str( ), buffer_size, use_mmap_io, fstype );
      }
      catch( const std::exception& Exception )
      {
	std::cout << std::setw( m_max_len_mount_pt + 2 ) << std::left << *cur
		  << Exception.what( )
		  << std::endl;
	continue;
      }
      std::cout << std::setw( m_max_len_mount_pt + 2 ) << std::left << *cur
		<< std::setw( 10 )
		<< fstype
		<< std::setw( 8 )
		<< buffer_size
		<< ( use_mmap_io ? "memory mapped io" : "non-memory mapped io" )
		<< std::endl;
    }
  }

  void TclInterp::
  IngestRSC( const std::string& ResourceFilename )
  {
    //-------------------------------------------------------------------
    // When reading resource files, need to invalidate all previous
    //   knowledge about variables.
    //-------------------------------------------------------------------
    m_vars_valid = false;

    //-------------------------------------------------------------------
    // Load the symbols into the tcl interpreter
    //-------------------------------------------------------------------
    Tcl_EvalFile( m_interp, ResourceFilename.c_str( ) );
  }

  inline int TclInterp::
  EnableMemoryMappedIO( )
  {
    if ( m_vars_valid == false )
    {
      init_vars( );
    }
    return m_enable_memory_mapped_io;
  }

  inline int TclInterp::
  StreamBufferSize( )
  {
    if ( m_vars_valid == false )
    {
      init_vars( );
    }
    return m_stream_buffer_size;
  }

  void TclInterp::
  init_vars( )
  {
    if ( m_vars_valid == false )
    {
      //-----------------------------------------------------------------
      // STREAM_BUFFER_SIZE
      //-----------------------------------------------------------------
      try
      {
	m_stream_buffer_size = var_int( "::STREAM_BUFFER_SIZE" );
      }
      catch ( const std::runtime_error& Exception )
      {
	std::cerr << "ERROR: " << Exception.what( ) << std::endl;
      }
      //-----------------------------------------------------------------
      // ENABLE_MEMORY_MAPPED_IO
      //-----------------------------------------------------------------
      try
      {
	m_enable_memory_mapped_io = var_int( "::ENABLE_MEMORY_MAPPED_IO" );
      }
      catch ( const std::runtime_error& Exception )
      {
	std::cerr << "ERROR: " << Exception.what( ) << std::endl;
      }
      //-----------------------------------------------------------------
      // MOUNT_PT
      //-----------------------------------------------------------------
      try
      {
	m_mount_pt.erase( m_mount_pt.begin( ), m_mount_pt.end( ) );
	m_max_len_mount_pt = 0;

	static const char*	varname = "::MOUNT_PT";
	std::ostringstream	msg;

	Tcl_Obj *obj = Tcl_GetVar2Ex( m_interp, varname, NULL, 0 );
	if ( obj == NULL )
	{
	  msg << "Invalid Integer Variable: " << varname;
	  throw std::runtime_error( msg.str( ) );
	}
	Tcl_Obj	**listobjv;
	int	nitems;

	if ( Tcl_ListObjGetElements( m_interp, obj, &nitems, &listobjv )
	       != TCL_OK )
	{
	  msg << "Unable to split: " << varname;
	  throw std::runtime_error( msg.str( ) );
	}
	for ( int i = 0; i < nitems; ++i )
	{
	  m_mount_pt.push_back( Tcl_GetString( listobjv[ i ] ) );
	  if ( m_mount_pt.back( ).size( ) > m_max_len_mount_pt )
	  {
	    m_max_len_mount_pt = m_mount_pt.back( ).size( );
	  }
	}
      }
      catch ( const std::runtime_error& Exception )
      {
	std::cerr << "ERROR: " << Exception.what( ) << std::endl;
      }
      //-----------------------------------------------------------------
      // DEVICE_IO_CONFIGURATION
      //-----------------------------------------------------------------
      try
      {
	using namespace FrameAPI;

	static const char*	varname = "::DEVICE_IO_CONFIGURATION";
	std::ostringstream	msg;

	Tcl_Obj *obj = Tcl_GetVar2Ex( m_interp, varname, NULL, 0 );
	if ( obj == NULL )
	{
	  msg << "Invalid Integer Variable: " << varname;
	  throw std::runtime_error( msg.str( ) );
	}
	std::unique_ptr< DeviceIOConfiguration::descriptions_type >
	  dioc = 
	  TclObjToDeviceIOConfigurationDescriptionsType( m_interp,
							 obj );
	//---------------------------------------------------------------
	// Save the device configuration information
	//---------------------------------------------------------------
	FrameAPI::DeviceIOConfiguration::Reset( *dioc );
      
      }
      catch ( const std::runtime_error& Exception )
      {
	std::cerr << "ERROR: " << Exception.what( ) << std::endl;
      }
      //-----------------------------------------------------------------
      // Completed everything
      //-----------------------------------------------------------------
      m_vars_valid = true;
    }
  }

  long TclInterp::
  var_int( const char* VarName ) const
  {
    int		retval;
    int		status;

    Tcl_Obj *obj = Tcl_GetVar2Ex( m_interp, VarName, NULL, 0 );
    if ( obj )
    {
      status = Tcl_GetIntFromObj( m_interp, obj, &retval );
    }
    else
    {
      status = TCL_ERROR;
    }
    if ( status == TCL_OK )
    {
      return retval;
    }
    std::ostringstream	msg;

    msg << "Invalid Integer Variable: " << VarName;
    throw std::runtime_error( msg.str( ) );
  }

} // namespace - anonymous

//-----------------------------------------------------------------------
// Main program
//-----------------------------------------------------------------------
int
main( int /* ArgC */, char** /* ArgV */ )
{
  //---------------------------------------------------------------------
  // Initialize the system
  //---------------------------------------------------------------------
  General::StdErrLog.IsOpen( false );

  //---------------------------------------------------------------------
  // Parse it
  //---------------------------------------------------------------------
  TclInterp	interp;

  interp.IngestRSC( DISKCACHE_RSC_FILENAME );
  interp.IngestRSC( FRAME_RSC_FILENAME );

  std::cout << "Default stream buffer size: " << interp.StreamBufferSize( ) << std::endl;
  std::cout << "Default memory mapped i/o:  " << interp.EnableMemoryMappedIO( ) << std::endl;

  std::cout << std::endl;
  interp.DumpMountPT( );
}
