#ifndef FRAME_API__CREATE_FRAME_HH
#define FRAME_API__CREATE_FRAME_HH

#include "ConditionData.hh"

namespace FrameAPI
{
  class CreateFrame: public ConditionData
  {
  public:
    //-------------------------------------------------------------------
    //: Constructor
    //
    // This constructor initializes the class with a list of frame files
    //   and a list of channel request. Data validation of the input
    //   parameters is performed.
    //!param: const frame_files_type& FrameFiles - a list of fully
    //+		qualified frame file names.
    //!param: const std::list< channel_input_type>& Channels - List of
    //+		channels as described by struct channel_input_type
    //!param: bool Concatination - true if continuous segments should
    //		be concatinated together.
    //-------------------------------------------------------------------
    CreateFrame( const frame_files_type& FrameFiles,
		 const std::list< channel_input_type>& Channels,
		 bool Concatination );

    void Eval( std::list< FrameCPP::FrameH* >& Frames );

  private:
#if WORKING
    inline CreateFrame( ) {};
    inline CreateFrame( CreateFrame& ) {};
#endif /* WORKING */
    const CreateFrame& operator=( CreateFrame& );
  }; // class - CreateFrame

  inline const CreateFrame& CreateFrame::
  operator=( CreateFrame& )
  {
    return *this;
  }

} // namespace - FrameAPI

#endif /* FRAME_API__CREATE_FRAME_HH */
