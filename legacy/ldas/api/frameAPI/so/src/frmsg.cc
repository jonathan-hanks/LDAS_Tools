#include "ldas_tools_config.h"

// System Header Files
#include <sstream>   
   
#include "framecpp/Time.hh"

#include "frmsg.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "util.hh"

using FrameCPP::FrMsg;

#if ! CORE_API
using ILwd::LdasArrayBase;
using ILwd::LdasElement;
using ILwd::LdasArray;
using ILwd::LdasString;
using ILwd::LdasContainer;
#endif /* ! CORE_API */
   
using namespace std;   

//!ignore_begin:

enum
{
    FR_ALARM,
    FR_MESSAGE,
    FR_SEVERITY,
    FR_GTIMES,
    FR_GTIMEN
};


QueryHash initMsgHash()
{
    QueryHash h;
    h[ "alarm"    ] = FR_ALARM;
    h[ "message"  ] = FR_MESSAGE;
    h[ "severity" ] = FR_SEVERITY;
    h[ "gtimes"      ] = FR_GTIMES;
    h[ "gtimen"      ] = FR_GTIMEN;
    return h;
}


static QueryHash msgHash( initMsgHash() );    


//-----------------------------------------------------------------------------
#if CORE_API
std::string getAttribute( const FrMsg& msg, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        msgHash.find( q.getName().c_str() );
    if ( iter == msgHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_ALARM:
                res << msg.GetAlarm();
                break;
                
            case FR_MESSAGE:
                res << msg.GetMessage();
                break;
                
            case FR_SEVERITY:
                res << msg.GetSeverity();
                break;
                
            case FR_GTIMES:
                res << msg.GetGTime().getSec();
                break;
            
            case FR_GTIMEN:
                res << msg.GetGTime().getNSec();
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    return res.str();
}
#endif /* CORE_API */


//-----------------------------------------------------------------------------
#if ! CORE_API
LdasElement* getData( const FrMsg& msg, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        return msg2container( msg );
    }
    
    throw SWIGEXCEPTION( "bad_query" );
}
#endif /* ! CORE_API */
//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrMsg.
//
//!usage_ooi: FrMsg* msg = container2msg( c );
//
//!param: const LdasContainer& c  - FrMsg container.
//!return: FrMsg* msg - FrameCPP::FrMsg newly allocated.
//!exc: SwigException - bad container data.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
FrMsg* container2msg( const LdasContainer& c )
{
   const string& c_name( c.getName( 2 ) );
    if ( strcasecmp( c_name.c_str(), "Msg" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrMsg container." );
    }

    const LdasArray< INT_4U >& te = findArrayType< INT_4U >( c, "GTime" );    
    FrMsg* msg = new FrMsg(
        c.getName( 0 ),
        findLStringType( c, "message" ).getString(),
        *findArrayType< INT_4U >( c, "severity" ).getData(),
        FrameCPP::Time( te.getData()[ 0 ], te.getData()[ 1 ] ));
    
    return msg;
}
#endif /* ! CORE_API */


//-----------------------------------------------------------------------------
//
//: Convert FrMsg to ILWD.
//
//!usage_ooi: LdasContainer* msgcontainer = msg2container( msg );
//!param: const FrMsg& msg - FrameCPP::FrMsg to convert.
//!return: LdasContainer* msgcontainer - new allocated container.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
LdasContainer* msg2container( const FrMsg& msg )
{
    LdasContainer* c = new LdasContainer( "::Msg:Frame" );
    c->setName( 0, msg.GetAlarm() );
    
    LdasString* message( new LdasString( msg.GetMessage(), "message" ) );
    LdasArray< INT_4U >* severity(
        new LdasArray< INT_4U >( msg.GetSeverity(), "severity" ) );
    INT_4U time[ 2 ] = { msg.GetGTime().getSec(),
                         msg.GetGTime().getNSec() };
    LdasArray< INT_4U >* gTime(
        new LdasArray< INT_4U >( time, 2, "GTime" ) );

    c->push_back( message,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( severity,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
    c->push_back( gTime,
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );

    return c;
}
#endif /* ! CORE_API */

//-----------------------------------------------------------------------------
//
//: Insert FrMsg container into Frame container.
//
//!usage_ooi: insertMsg( fr, m );
//!param: LdasContainer& fr - Frame container.
//!param: LdasContainer& m - FrMsg container.
//!exc: SwigException - bad container data.
//
//-----------------------------------------------------------------------------
#if ! CORE_API
void insertMsg(
    LdasContainer& fr, LdasContainer& m )
{
    LdasElement* t = fr.find( "rawdata", 1 );
    if ( t == 0 )
    {
        throw SWIGEXCEPTION( "no_rawdata" );
    }

    if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_frame" );
    }

    LdasContainer& rawdata( dynamic_cast< LdasContainer& >( *t ) );
    t = rawdata.find( "logmsg", 1 );
    if ( t == 0 )
    {
        rawdata.push_back( LdasContainer( ":logMsg:Container(Msg)" ) );
        t = rawdata.back(); // Not thread safe
    }
    
    if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_rawdata" );
    }
    
    dynamic_cast< LdasContainer& >( *t ).push_back( m );
}
#endif /* ! CORE_API */

