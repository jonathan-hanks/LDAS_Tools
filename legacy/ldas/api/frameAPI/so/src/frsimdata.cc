/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "config.h"

// System Header Files
#include <cmath>
#include <sstream>   

// Local Header Files
#include "frsimdata.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "util.hh"


using FrameCPP::FrSimData;
using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

using ILwd::LdasArrayBase;
using ILwd::LdasElement;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasString;
   
using namespace std;   

//!ignore_begin:

enum
{
    FR_NAME,
    FR_COMMENT,
    FR_SAMPLERATE,
    FR_FSHIFT,
    FR_PHASE,
    FR_DATA,
    FR_INPUT,
    FR_TABLE
};


QueryHash initSimDataHash()
{
    QueryHash h;
    h[ "name"       ] = FR_NAME;
    h[ "comment"    ] = FR_COMMENT;
    h[ "samplerate" ] = FR_SAMPLERATE;
    h[ "fshift"     ] = FR_FSHIFT;
    h[ "phase"      ] = FR_PHASE;
    h[ "data"       ] = FR_DATA;
    h[ "input"      ] = FR_INPUT;
    h[ "table"      ] = FR_TABLE;
    return h;
}


static QueryHash simDataHash( initSimDataHash() );    
    
    
//-----------------------------------------------------------------------------
std::string getAttribute( const FrSimData& simData, std::deque< Query >& dq,
                     std::vector< size_t >& start, size_t index )
{
    if ( dq.size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }
    
    Query q = dq.front();
    dq.pop_front();
    ostringstream res;
    
    QueryHash::const_iterator iter =
        simDataHash.find( q.getName().c_str() );
    if ( iter == simDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        switch( iter->second )
        {
            case FR_NAME:
                res << simData.GetName();
                break;
                
            case FR_COMMENT:
                res << simData.GetComment();
                break;
                
            case FR_SAMPLERATE:
                res << simData.GetSampleRate();
                break;

            case FR_FSHIFT:
                res << simData.GetFShift();
                break;

            case FR_PHASE:
                res << simData.GetPhase();
                break;

            case FR_DATA:
	        return getAttribute( simData.RefData(), dq );
                
            case FR_INPUT:
                return getAttribute( simData.RefInput(), dq );

            case FR_TABLE:
                return getAttribute( simData.RefTable(), dq );

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }
    else
    {
        switch( iter->second )
        {
            case FR_DATA:
                return getAttribute(
				    getContained( simData.RefData(),
						  q, start, index ),
                    dq, start, index + 1 );
                break;
                
            case FR_INPUT:
                return getAttribute(
				    getContained( simData.RefInput(),
						  q, start, index ),
                    dq, start, index + 1 );
                break;

            case FR_TABLE:
                return getAttribute(
				    getContained( simData.RefTable(),
						  q, start, index ),
                    dq, start, index + 1 );
                break;

            default:
                throw SWIGEXCEPTION( "bad_query" );
        }
    }

    return res.str();
}


//-----------------------------------------------------------------------------
LdasElement* getData( const FrSimData& simData, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index,
                      const Time& gtime, const REAL_8& dt )
{
    if ( dq.size() == 0 )
    {
        return simData2container( simData, gtime, dt );
    }
    
    Query q( dq.front() );
    dq.pop_front();
    
    QueryHash::const_iterator iter =
        simDataHash.find( q.getName().c_str() );
    if ( iter == simDataHash.end() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    if ( !q.isQuery() )
    {
        throw SWIGEXCEPTION( "bad_query" );
    }

    switch( iter->second )
    {
        case FR_DATA:
            return getData(
			   getContained( simData.RefData(), q, start, index ),
			   dq, start, index + 1 );
            break;
                
        case FR_INPUT:
            return getData(
			   getContained( simData.RefInput(), q, start, index ),
                dq, start, index + 1 );
            break;

        case FR_TABLE:
            return getData(
			   getContained( simData.RefTable(), q, start, index ),
                dq, start, index + 1 );
            break;

        default:
            throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}

//!ignore_end:

//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to FrSimData.
//
//!usage_ooi: FrSimData* simData = container2simData( c );
//
//!param: const LdasContainer& c - simData container.
//
//!return: FrSimData* simData - FrameCPP::FrSimData newly allocated object.
//
//!exc: SwigException - invalid simData container.
//
//-----------------------------------------------------------------------------
FrSimData* container2simData( const LdasContainer& c )
{
    const string& c_name( c.getName( 2 ) );
   
    if ( strcasecmp( c_name.c_str(), "SimData" ) != 0 )
    {
        throw SWIGEXCEPTION( "invalid_format: Not FrSimData container." );
    }
    
    FrSimData* sim = new FrSimData(
        findLStringType( c, "name" ).getString(),
        findLStringType( c, "comment" ).getString(),
        *findArrayType< REAL_4 >( c, "sampleRate" ).getData(),
        *findArrayType< REAL_8 >( c, "fShift" ).getData(),
        *findArrayType< REAL_4 >( c, "phase" ).getData()
	);

    try
      {	
	try
	  {
	    const LdasContainer* data( findContainerType( c, "data" ) );
	    if ( data != 0 )
	      {
		for ( unsigned int i = 0; i < data->size(); ++i )
		  {
		      FrSimData::data_type::value_type
			  v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*data)[ i ] ) ) );

		      sim->RefData().append( v );
		  }
	      }
	  }
	catch( std::bad_cast& )
	  {
	    throw SWIGEXCEPTION( "invalid_data_format" );
	  }
      
	try
	  {
	    const LdasContainer* input( findContainerType( c, "input" ) );
	    if ( input != 0 )
	      {
		for ( unsigned int i = 0; i < input->size(); ++i )
		  {
		      FrSimData::input_type::value_type
			  v( array2vect( dynamic_cast< const LdasArrayBase& >( *(*input)[ i ] ) ) );

		      sim->RefInput().append( v );
		  }
	      }
	  }
	catch( std::bad_cast& )
	  {
	    throw SWIGEXCEPTION( "invalid_input_format" );
	  }

	try
	  { 
	    const LdasContainer* table( findContainerType( c, "table" ) );
	    if ( table != 0 )
	      {
		for ( unsigned int i = 0; i < table->size(); ++i )
		{
		    FrSimData::table_type::value_type
			t( container2table( dynamic_cast< const LdasContainer& >( *(*table)[ i ] )) );

		    sim->RefTable().append( t );
		}
	      }
	  }
	catch( std::bad_cast& )
	  {
	    throw SWIGEXCEPTION( "invalid_table_format" );
	  }
      } catch ( ... ) {
	delete sim;
	sim = (FrSimData*)NULL;
	throw;
      }
    return sim;
}


//-----------------------------------------------------------------------------
//
//: Convert FrSimData to ILWD.
//
//!usage_ooi: LdasContainer* simDataContainer = simData2container( sim, gtime, dt );
//
//!param: const FrSimData& sim - FrameCPP::FrSimData to convert.
//!param: const Time& gtime - FrameCPP::Frame time.
//!param: const REAL_8& dt - FrameCPP::Frame delta time.
//
//!return: LdasContainer* simDataContainer - newly allocated LDAS container.
//
//-----------------------------------------------------------------------------
LdasContainer* simData2container( const FrSimData& sim, const Time& gtime,
                                  const REAL_8& dt )
{
    LdasContainer* c = new LdasContainer( "::SimData" );
    c->setName( 0, sim.GetName() );
    
    ostringstream ss;
    ss.precision( REAL_8_DIGITS + 1 );
    ss << gtime.getSec();
    c->appendName( ss.str() );

    ss.str( "" );
    ss << gtime.getNSec();
    c->appendName( ss.str() );

    c->appendName( "Frame" ); 

    LdasString* comment( new LdasString( sim.GetComment(), "comment" ) );
    LdasArray< REAL_4 >* sampleRate(
        new LdasArray< REAL_4 >( sim.GetSampleRate(), "sampleRate" ) );
    LdasArray< REAL_8 >* timeOffset(
        new LdasArray< REAL_8 >( sim.GetTimeOffset( ), "timeOffset" ) );
    LdasArray< REAL_8 >* fShift(
        new LdasArray< REAL_8 >( sim.GetFShift(), "fShift" ) );
    LdasArray< REAL_4 >* phase(
        new LdasArray< REAL_4 >( sim.GetPhase(), "phase" ) );
    LdasArray< REAL_8 >* e_dt( new LdasArray< REAL_8 >( dt, "dt" ) );

    c->push_back( comment, 
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( sampleRate,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( timeOffset,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( fShift,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( phase,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
    c->push_back( e_dt,
		  ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );

    size_t dataSize( sim.RefData().size() );
    if ( dataSize > 0 )
    {
        LdasContainer* data(
            new LdasContainer( ":data:Container(Vect):Frame" ) );
        c->push_back( data, 
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );
        
        FrSimData::const_iterator iter( sim.RefData().begin() );
        for( size_t i = dataSize; i != 0; --i, ++iter )
        {
            data->push_back( vect2array( **iter ),
			     ILwd::LdasContainer::NO_ALLOCATE,
			     ILwd::LdasContainer::OWN );
        }
    }

    size_t inputSize( sim.RefInput().size() );
    if ( inputSize > 0 )
    {
        LdasContainer* input(
            new LdasContainer( ":input:Container(Vect):Frame" ) );
        c->push_back( input, 
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );

        FrSimData::const_input_iterator iter( sim.RefInput().begin() );
        for( size_t i = inputSize; i != 0; --i, ++iter )
        {
            input->push_back( vect2array( **iter ), 
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }

    size_t tableSize( sim.RefTable().size() );
    if ( tableSize > 0 )
    {
        LdasContainer* table(
            new LdasContainer( ":table:Container(Table):Frame" ) );
        c->push_back( table,
		      ILwd::LdasContainer::NO_ALLOCATE,
		      ILwd::LdasContainer::OWN );

        FrSimData::const_table_iterator iter( sim.RefTable().begin() );
        for( size_t i = tableSize; i != 0; --i, ++iter )
        {
            table->push_back( table2container( **iter ),
			      ILwd::LdasContainer::NO_ALLOCATE,
			      ILwd::LdasContainer::OWN );
        }
    }
    
    return c;
}

//-----------------------------------------------------------------------------
//
//: Insert FrSimData container into a Frame container.
//
//!param: LdasContainer& fr - frame container.
//!param: LdasContainer& sim - simData container.
//!param: const bool validateTime - A flag to enable/disable time stamp
//+       validation for data insertion. True to enable, false to disable.       
//+       Default is TRUE.      
//
//!exc: SwigException - invalid container data.
//
//-----------------------------------------------------------------------------
void insertSimData( LdasContainer& fr, LdasContainer& sim,
   const bool validateTime )
{
    // Find the GTime & dt elements of the frame container. 
    LdasElement* fr_gtime = fr.find( "gtime" );
    LdasElement* fr_dt = fr.find( "dt" );
    LdasElement* sim_dt = sim.find( "dt" );

    // Check to make sure the frame data is valid
    if ( fr_gtime == 0 || fr_gtime->getElementId() != ILwd::ID_INT_4U )
    {
       throw SWIGEXCEPTION( "bad_frame: gtime" );
    }
   
    if( fr_dt == 0 || fr_dt->getElementId() != ILwd::ID_REAL_8 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check to make sure the sim data is valid
    if ( sim_dt == 0 || sim_dt->getElementId() != ILwd::ID_REAL_8 )
    {
        throw SWIGEXCEPTION( "bad_simdata: dt" );
    }

    // Check the characteristics of the GTime element.
    LdasArray< INT_4U >& gtime(
        dynamic_cast< LdasArray< INT_4U >& >( *fr_gtime ) );
    if ( gtime.getNDim() != 1 || gtime.getDimension( 0 ) != 2 )
    {
        throw SWIGEXCEPTION( "bad_frame: gtime" );
    }

    // Check the characteristics of the Frame Dt element.
    LdasArray< REAL_8 >& fdt( dynamic_cast< LdasArray< REAL_8 >& >( *fr_dt ) );
    if ( fdt.getNDim() != 1 || fdt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_frame: dt" );
    }

    // Check the characteristics of the sim Dt element.
    LdasArray< REAL_8 >& adt(
        dynamic_cast< LdasArray< REAL_8 >& >( *sim_dt ) );
    if ( adt.getNDim() != 1 || adt.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_simdata: dt" );
    }

    // If 'fdt' is not zero, then we must have some data in the frame.  Check
    // to make sure the start times and length for the data matches that of the
    // frame.
    if( validateTime && fdt.getData()[ 0 ] != 0 )
    {
        INT_4U time;
        
        // Check the GPS seconds.
        std::string tmp( sim.getName( 3 ) );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 0 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(seconds): frame vs.simdata" );
        }

        // Check the GPS nanoseconds.
        tmp = sim.getName( 4 );
        time = strtoul( tmp.c_str(), 0, 10 );
        if ( time != gtime.getData()[ 1 ] )
        {
            throw SWIGEXCEPTION( "incompatible_time(nanoseconds): "
                                 "frame vs. simdata" );
        }

        // Check the length
        if ( fdt.getData()[ 0 ] < adt.getData()[ 0 ] )
        {
            throw SWIGEXCEPTION( "incompatible_length: frame vs. simdata" );
        }
    }

    // Find the simdata container in rawdata if it exists
    LdasElement* t = fr.find( "simdata", 1 );
    if ( t == 0 )
    {
        // it didn't exist so create it.
        fr.push_back( LdasContainer( ":simdata:Container(SimData)" ) );
        t = fr.back();
    }
    // Check the simdata container
    else if ( t->getElementId() != ILwd::ID_ILWD )
    {
        throw SWIGEXCEPTION( "bad_frame: malformed FrSimData" );
    }

#ifdef RESET_FRAME_TIME      
    // If this is the first data element in the frame, set the frame GPS time
    // and length to correspond to the inserted object.
    if( gtime.getData()[ 0 ] == 0 &&
        gtime.getData()[ 1 ] == 0 &&
        fdt.getData()[ 0 ] == 0 )
    {
        std::string tmp( sim.getName( 3 ) );
        gtime.getData()[ 0 ] = strtoul( tmp.c_str(), 0, 10 );
        tmp = sim.getName( 4 );
        gtime.getData()[ 1 ] = strtoul( tmp.c_str(), 0, 10 );
        fdt.getData()[ 0 ] = adt.getData()[ 0 ];
    }    
#endif   

    // OK, everything checks out so add the simdata
    dynamic_cast< LdasContainer& >( *t ).push_back( sim );
    return;
}


//-----------------------------------------------------------------------------
//
//: Concatenate two FrSimData structures.
//
//!usage_ooi: LdasContainer* concatSimDatacontainer = concatSimData( c1, c2 );
//
//!param: const LdasContainer* c1 - first FrSimData container.
//!param: const LdasContainer* c2 - second FrSimData container.
//
//!return: LdasContainer* concatSimDatacontainer - newly allocated concatenated
//+	FrSimData container.
//
//!exc: SwigException - invalid container data.
//
//-----------------------------------------------------------------------------
LdasContainer* concatSimData(
   const LdasContainer* c1, const LdasContainer* c2 )
try
{
    // Make sure this is frame data.
    if ( c1->getName( 5 ) != "Frame" ||
         c2->getName( 5 ) != "Frame" )
    {
        throw SWIGEXCEPTION( "bad_simdata: not_frame_data" );
    }

    string c1_name( c1->getName( 2 ) );
    string c2_name( c2->getName( 2 ) );   
   
    if ( strcasecmp( c1_name.c_str(), "simdata" ) != 0 ||
         strcasecmp( c2_name.c_str(), "simdata" ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_types" );
    }

    c1_name = c1->getName( 0 );
    c2_name = c2->getName( 0 );   
   
    if ( strcasecmp( c1_name.c_str(), c2_name.c_str() ) != 0 )
    {
        throw SWIGEXCEPTION( "incompatible_channels" );
    }

    const LdasArray< REAL_8 >& sr1( findArrayType< REAL_8 >( *c1, "samplerate" ) ); 
    const LdasArray< REAL_8 >& sr2( findArrayType< REAL_8 >( *c2, "samplerate" ) );          

    if ( sr1 != sr2 ||
         sr1.getNDim() != 1 || sr1.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "incompatible_simdata: samplerate" );
    }

    // Make sure the fshift fields are the same.
    const LdasArray< REAL_8 >& f1( findArrayType< REAL_8 >( *c1, "fshift" ) ); 
    const LdasArray< REAL_8 >& f2( findArrayType< REAL_8 >( *c2, "fshift" ) );    

    if ( f1 != f2 )
    {
        throw SWIGEXCEPTION( "incompatible_simdata: fshift" );
    }


    // Make sure the phase fields are the same.
    const LdasArray< REAL_4 >& p1( findArrayType< REAL_4 >( *c1, "phase" ) ); 
    const LdasArray< REAL_4 >& p2( findArrayType< REAL_4 >( *c2, "phase" ) );    

    if ( p1 != p2 )
    {
        throw SWIGEXCEPTION( "incompatible_simdata: phase" );
    }


    REAL_8 sr( sr1.getData()[ 0 ] );

    const LdasArray< REAL_8 >& d1 = findArrayType< REAL_8 >( *c1, "dt" ); 
    const LdasArray< REAL_8 >& d2 = findArrayType< REAL_8 >( *c2, "dt" );     
    if ( d1.getNDim() != 1 || d1.getDimension( 0 ) != 1 ||
         d2.getNDim() != 1 || d2.getDimension( 0 ) != 1 )
    {
        throw SWIGEXCEPTION( "bad_simdata: dt" );
    }
    REAL_8 dt1( d1.getData()[ 0 ] );
    REAL_8 dt2( d2.getData()[ 0 ] );

    // What if the strtoul fails? Should use strstream?
    string tmp( c1->getName( 3 ) );
    INT_4U gs1( strtoul( tmp.c_str(), 0, 10 ) );
   
    tmp = c2->getName( 3 );
    INT_4U gs2( strtoul( tmp.c_str(), 0, 10 ) );
   
    tmp = c1->getName( 4 );
    INT_4U gn1( strtoul( tmp.c_str(), 0, 10 ) );
   
    tmp = c2->getName( 4 );
    INT_4U gn2( strtoul( tmp.c_str(), 0, 10 ) );

    // Calculate what the start time for the 2nd container should be
    INT_4U nextgs = gs1 + static_cast< INT_4U >( dt1 );
    INT_4U nextgn = gn1 + static_cast< INT_4U >( fmod( dt1, 1.0 ) * 1e9 );
    if ( nextgn >= 1000000000 )
    {
        ++nextgs;
        nextgn -= 1000000000;
    }

    if ( nextgs != gs2 || nextgn != gn2 )
    {
        throw SWIGEXCEPTION( "incompatible_simdata: The second FrSimData "
                             "structure does not start where the first "
                             "ended." );
    }

    const LdasContainer* cn1( findContainerType( *c1, "data" ) );
    const LdasContainer* cn2( findContainerType( *c2, "data" ) );  
    if( cn1 == 0 ||  cn1->size() == 0 )
    {
        throw SWIGEXCEPTION( "bad_simdata: data" );
    }

    const LdasElement* elem1( ( *cn1 )[ 0 ] );
    const LdasArrayBase* data1( dynamic_cast< const LdasArrayBase* >( elem1 ) );
    REAL_8 calcdt( data1->getDimension( 0 ) / sr );
    if ( fabs( calcdt - dt1 ) > 1e-9 )
    {
        throw SWIGEXCEPTION( "incomplete_serdata: data" );
    }

    LdasContainer* concatContainer( 0 );
    try
    {
       // Now create a copy of the first container and replace its data with
       // the new data.
       concatContainer = new LdasContainer( *c1 );
       LdasContainer* cdata( dynamic_cast< LdasContainer* >( 
          concatContainer->find( "data", 1 ) ) );
       cdata->erase( cdata->begin(), cdata->end() );
       cdata->push_back( concatElementData( cn1, cn2 ),
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
   
       // Update the 'dt' field also.
       LdasElement* e( concatContainer->find( "dt" ) );
       const REAL_8 newdt[] = { dt1 + dt2 };
       dynamic_cast< LdasArray< REAL_8 >& >( *e ).setData( newdt );      

       // Concatenate the comments
       e = concatContainer->find( "comment" );
       if( e == 0 || e->getElementId() != ILwd::ID_LSTRING )
       {
         throw SWIGEXCEPTION( "bad_simdata: comment" );
       }
       const LdasString& comment( findLStringType( *c2, "comment" ) );   
       if( comment.getString().empty() == false )
       {
          dynamic_cast< LdasString& >( *e ).setString( 
             dynamic_cast< const LdasString& >( *e ).getString() + 
             "\n" + comment.getString() );    
       }
   
       // Concatenate 'input' data if it's present in the container:
       cn1 = findContainerType( *c1, "input" );
       cn2 = findContainerType( *c2, "input" );   
       if( cn1 != 0 || cn2 != 0 )
       {
	 if ( cn1 == 0 || cn1->size() == 0 )
	   {
	     throw SWIGEXCEPTION( "bad_simdata: input" );
	   }
	 
	 elem1 = ( *cn1 )[ 0 ];
	 data1 = dynamic_cast< const LdasArrayBase* >( elem1 );
	 calcdt = data1->getDimension( 0 ) / sr;
	 if ( fabs( calcdt - dt1 ) > 1e-9 )
	   {
	     throw SWIGEXCEPTION( "incomplete_simdata: input" );
	   }

	 cdata = dynamic_cast< LdasContainer* >(
			concatContainer->find( "input", 1 ) );
	 cdata->erase( cdata->begin(), cdata->end() );
	 cdata->push_back( concatElementData( cn1, cn2 ),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
       }

       // Concatenate 'table' data if it's present in the container:
       LdasElement* elem = concatContainer->find( "table", 1 );
       if( elem != 0 )
	 {
	   if( elem->getElementId() != ILwd::ID_ILWD )
	     {
	       throw SWIGEXCEPTION( "bad_simdata: table" );
	     } 
	   cn1 = findContainerType( *c1, "table" );
	   cn2 = findContainerType( *c2, "table" );
	   if( cn1->size() == 0 || cn2 == 0 ||
	       cn1->size() != cn2->size() ) 
	     {
	       throw SWIGEXCEPTION( "incompatible_simwdata: table" );
	     }

	   cdata = dynamic_cast< LdasContainer* >( elem );
	   cdata->erase( cdata->begin(), cdata->end() );
	   for ( unsigned int i = 0; i < cn1->size(); ++i )
	     {
	       cdata->push_back( concatTable
				 ( dynamic_cast< const LdasContainer* >
				   ( ( *cn1 )[ i ] ),
				   dynamic_cast< const LdasContainer* >
				   ( ( *cn2 )[ i ] ) ),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
	     }
	 }
    }
    catch(...)
    {
       delete concatContainer;
       concatContainer = (ILwd::LdasContainer*)NULL;
       throw;
    }
 
    return concatContainer;
}
catch( std::bad_cast& )
{
   throw SWIGEXCEPTION( "simdata: invalid_input_format" );
}
