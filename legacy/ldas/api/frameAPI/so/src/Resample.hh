#ifndef FRAME_API__RESAMPLE_CMD_HH
#define FRAME_API__RESAMPLE_CMD_HH

#include "genericAPI/threaddecl.hh"

//!exc: SwigException   
Filters::ResampleBase* createResampleState(const int q,
					   const FrameCPP::FrAdcData* const adc);
//!exc: SwigException      
Filters::ResampleBase* createResampleState(const int q,
					   const FrameCPP::FrProcData* const proc);   
//!exc: SwigException   
void destructResampleState(Filters::ResampleBase* const r);

//!exc: SwigException      
REAL_8 resampleDelay( const FrameCPP::FrProcData* const proc,
		      const Filters::ResampleBase* const r );

//!exc: SwigException   
FrameCPP::FrProcData* ResampleAdcData( const FrameCPP::FrAdcData* const adc,
				       Filters::ResampleBase* const r );
//!exc: SwigException   
FrameCPP::FrProcData* ResampleProcData( const FrameCPP::FrProcData* const proc,
					Filters::ResampleBase* const r );
   
#endif /* FRAME_API__RESAMPLE_CMD_HH */
