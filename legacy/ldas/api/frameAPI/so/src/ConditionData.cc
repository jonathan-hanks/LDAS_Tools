#include "config.h"

#include <cmath>

#include <algorithm>
#include <memory>
#include <sstream>
#include <sstream>
#include <stdexcept>

#include "general/toss.hh"
#include "general/gpstime.hh"
#include "general/Memory.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldasarray.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "framecpp/Dimension.hh"
#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrRawData.hh"
#include "framecpp/FrVect.hh"

#include "ConditionData.hh"
#include "FrameException.hh"
#include "convert.hh"
#include "filereader.hh"
#include "fradcdata.hh"
#include "framecmd.hh"
#include "ResampleCmd.hh"
#include "frprocdata.hh"
#include "util.hh"

using std::fabs;
using std::ceil;

using General::toss;

using FrameCPP::Common::SearchContainer;
using FrameCPP::Common::FrTOC;

#if 0
using FrameCPP::Version::FrameH;
using FrameCPP::Version::FrAdcData;
using FrameCPP::Version::FrDetector;
using FrameCPP::Version::FrHistory;
using FrameCPP::Version::FrProcData;
using FrameCPP::Version::FrRawData;
#endif /* 0 */

using namespace FrameCPP::Version;

using FrameAPI::ConditionData;
using FrameAPI::NANO_SECOND;

#define	LM_DEBUG 0

#if LM_DEBUG
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

#if LM_DEBUG
#define	INFO( a )  std::cerr << a << std::endl
#else
#define INFO( a )
#endif

static const FrRawData::firstAdc_type null_adc;
static const FrameH::procData_type null_proc;

//-----------------------------------------------------------------------
// Local helper functions
//-----------------------------------------------------------------------

namespace {
  struct frame_group_info_type {
    General::GPSTime	start;
    REAL_8		delta;
    bool		initialized;

    frame_group_info_type( );
  };

  //---------------------------------------------------------------------
  // calc_dt
  //---------------------------------------------------------------------

  inline REAL_8
  calc_dt( const ConditionData::continuous_segment_type& Segment )
  {
    return ( Segment.back( ).m_start + Segment.back( ).m_dt ) -
      Segment.front( ).m_start;
  }

  ConditionData::data_type
  get_data_type( const FrameCPP::Base* Data )
  {
    if ( dynamic_cast< const FrameCPP::FrAdcData* >( Data ) )
    {
      return ConditionData::ADC;
    }
    if ( dynamic_cast< const FrameCPP::FrProcData* >( Data ) )
    {
      return ConditionData::PROC;
    }
    return ConditionData::UNKNOWN_DATA;
  }

  //---------------------------------------------------------------------
  // getSamples
  //
  // Helper function to do retrive the number of samples from a channel
  //---------------------------------------------------------------------

  template < class channel_type >
  INT_4U
  getSamples( const channel_type* Channel );

  template < >
  INT_4U
  getSamples( const FrameCPP::FrAdcData* Channel )
  {
    AT();
    INT_4U	samples( 0 );
    AT();
    const FrameCPP::FrAdcData::data_type& data( Channel->RefData( ) );

    AT();
    for ( FrameCPP::FrAdcData::data_type::const_iterator d( data.begin() );
	  d != data.end( );
	  d++ )
    {
      AT();
      samples += (*d)->GetNData( );
    }
    AT();
    return samples;
  }

  template < >
  INT_4U
  getSamples( const FrameCPP::FrProcData* Channel )
  {
    AT();
    INT_4U	samples( 0 );
    const FrameCPP::FrProcData::data_type& data( Channel->RefData( ) );

    AT();
    for ( FrameCPP::FrProcData::data_type::const_iterator d( data.begin() );
	  d != data.end( );
	  d++ )
    {
      AT();
      samples += (*d)->GetNData( );
    }
    AT();
    return samples;
  }

  //---------------------------------------------------------------------
  // concat_channel
  //---------------------------------------------------------------------

  template< class channel_type >
  channel_type*
  concat_channel( const ConditionData::continuous_segment_type& Segment,
		  const General::GPSTime& FrameStart );

  template< >
  FrameCPP::FrAdcData*
  concat_channel<FrameCPP::FrAdcData>( const
				       ConditionData::continuous_segment_type&
				       Segment,
				       const General::GPSTime& FrameStart )
  {
    std::list< FrameCPP::Base* >	l;
    for ( ConditionData::continuous_segment_type::const_iterator
	    i ( Segment.begin( ) );
	  i != Segment.end( );
	  i++ )
    {
      l.push_back( (*i).GetBase( ) );
    }
    AT();
    FrameCPP::FrAdcData* ret( FrameAPI::FrAdcData::concat( l ) );
    //-------------------------------------------------------------------
    // Calculate the new offset
    //-------------------------------------------------------------------
    REAL_8 new_offset( ( Segment.front( ).m_start + ret->GetTimeOffset( ) )
		       - FrameStart );

    ret->SetTimeOffset( new_offset );

    return ret;
  }

  template< >
  FrameCPP::FrProcData*
  concat_channel<FrameCPP::FrProcData>( const
					ConditionData::continuous_segment_type&
					Segment,
					const General::GPSTime& FrameStart )
  {
    std::list< FrameCPP::Base* >	l;
    for ( ConditionData::continuous_segment_type::const_iterator
	    i ( Segment.begin( ) );
	  i != Segment.end( );
	  i++ )
    {
      l.push_back( (*i).GetBase( ) );
    }
    AT();
    FrameCPP::FrProcData*	ret( FrameAPI::FrProcData::concat( l ) );
#if FRAME_SPEC_CURRENT <= 5
    General::GPSTime	diff( General::GPSTime() +
			      ( Segment.front().m_start - FrameStart ) );
    FrameCPP::Time	new_offset( ret->GetTimeOffset( ) +
				    FrameCPP::Time( diff.GetSeconds( ),
						    diff.GetNanoseconds( ) ) );
#else
    REAL_8
      new_offset = ret->GetTimeOffset( )
      + Segment.front().m_start - FrameStart;
#endif

    
    ret->SetTimeOffset( new_offset );
    INFO( "timeoffset: " << ret->GetTimeOffset( ) );

    for ( ConditionData::continuous_segment_type::const_iterator
	    i ( Segment.begin( ) );
	  i != Segment.end( );
	  i++ )
    {
      (*i).ResetBase( );
    }
    return ret;
  }


  //---------------------------------------------------------------------
  // create_segment
  //---------------------------------------------------------------------

  template< class channel_type >
  FrameCPP::Base*
  create_segment( const ConditionData::continuous_segment_type& Segment,
		  const General::GPSTime& FrameStart )
  {
    AT();
    return concat_channel< channel_type >( Segment, FrameStart );
  }

  //---------------------------------------------------------------------
  // getChannel
  //
  // Helper routine to retrieve a specific channel out from a group of
  //   channels
  //---------------------------------------------------------------------

  template< class Output, class Iterator, class Input >
  Output*
  getChannel( const std::string& Name, Input& Container, const std::string& Type );

  //---------------------------------------------------------------------
  // getStartTime
  //
  // Helper function to do retrive the start time of a channel
  //---------------------------------------------------------------------

  template < class channel_type >
  General::GPSTime
  getStartTime( const channel_type& Channel,
		const General::GPSTime& FrameStart,
		INT_4U Resampling = 1,
		REAL_8 Delay = 0.0 );

  template < class channel_type >
  General::GPSTime
  getStartTime( const channel_type& Channel,
		const General::GPSTime& FrameStart,
		INT_4U Resampling,
		REAL_8 Delay )
  {
    AT( );
    General::GPSTime	retval( FrameStart );
#if FRAME_SPEC_CURRENT <= 5
    AT( );
    FrameCPP::Time	offset( Channel.getTimeOffset( ) );

    AT( );
    retval += (double)( offset.getSec( ) + offset.getNSec( ) * NANO_SECOND );
#else
    AT( );
    retval += Channel.GetTimeOffset( );
#endif
    AT( );
    retval -= ( static_cast<double>( Delay * Resampling )
		/ FrameAPI::SampleRate( Channel ) );
    return retval;
  }

  //---------------------------------------------------------------------
  // resample_data
  //
  // Helper routine to do resampling
  //---------------------------------------------------------------------
  template< class Data_ >
  FrameCPP::FrProcData*
  resample_data( const Data_& Data,
		 Filters::ResampleBase* const R );

  template< >
  inline FrameCPP::FrProcData*
  resample_data( const FrameCPP::FrAdcData& Data,
		 Filters::ResampleBase* const R )
  {
    AT( );
    FrProcData* ret( resampleAdcData( &Data, R ) );
    //:TRICKY: Unregister object since it will never be seen by TCL
    //:TRICKY:   The C++ code will take care of its memory management
    try
    {
      unregisterProcData( ret );
    }
    catch( ... )
    {
    }
    return ret;
  }

  template< >
  inline FrameCPP::FrProcData*
  resample_data( const FrameCPP::FrProcData& Data,
		 Filters::ResampleBase* const R )
  {
    AT( );
    FrameCPP::FrProcData* ret( resampleProcData( &Data, R ) );
    //:TRICKY: Unregister object since it will never be seen by TCL
    //:TRICKY:   The C++ code will take care of its memory management
    try
    {
      unregisterProcData( ret );
    }
    catch( ... )
    {
    }
    return ret;
  }

  //---------------------------------------------------------------------
  // setTimeOffset
  //
  // Helper function to do set the start time of a channel
  //---------------------------------------------------------------------

  template < class channel_type >
  General::GPSTime
  setTimeOffset( channel_type& Channel, double Offset );


  template < >
  General::GPSTime
  setTimeOffset( FrameCPP::FrProcData& Channel, double Offset )
  {
    General::GPSTime	frame_delay;

    if ( Offset < 0 )
    {
      frame_delay = General::GPSTime( (INT_4U)( ceil( fabs( Offset ) ) ), 0 );
    }

    General::GPSTime	channel_offset( frame_delay + Offset );
    
    Channel.SetTimeOffset( channel_offset.GetTime( ) );

    return frame_delay;
  }

  template < class channel_type >
  General::GPSTime
  setTimeOffset( channel_type& Channel,  double Offset )
  {
    General::GPSTime	frame_delay;

    if ( Offset < 0 )
    {
      frame_delay = General::GPSTime( (INT_4U)( ceil( abs( Offset ) ) ), 0 );
    }

    General::GPSTime	channel_offset( frame_delay + Offset );

    Channel.setTimeOffset( FrameCPP::Time( channel_offset.GetSeconds( ),
					   channel_offset.GetNanoseconds( ) ) );

    return frame_delay;
  }

  //---------------------------------------------------------------------
  // Helper function to trim a frame
  //---------------------------------------------------------------------
  template < class channel_type >
  channel_type* trimChannel( channel_type* Channel, REAL_8 Offset, REAL_8 Dt );

  template<>
  inline FrameCPP::FrAdcData*
  trimChannel( FrameCPP::FrAdcData* Channel, REAL_8 Offset, REAL_8 Dt )
  {
    AT( );
    return getFrameAdcDataSlice( Channel, Offset, Dt,
				 false /* Don't regiser */ );
  }

  template<>
  inline FrameCPP::FrProcData*
  trimChannel( FrameCPP::FrProcData* Channel, REAL_8 Offset, REAL_8 Dt )
  {
    AT( );
    return getFrameProcDataSlice( Channel, Offset, Dt,
				  false /* Don't register */ );
  }

}

//-----------------------------------------------------------------------
// CondititionData
//-----------------------------------------------------------------------

ConditionData::
ConditionData( const frame_files_type& FrameFiles,
	       const std::list< channel_input_type >& Channels,
	       bool Concatination )
  : m_frame_files( FrameFiles ),
    m_concatinate( Concatination ),
    m_mode( 0 )
{
  AT();
  //---------------------------------------------------------------------
  // Sanity check - Make sure the resample is appropriate
  //---------------------------------------------------------------------
  INFO( "Number of Frame files: " << FrameFiles.size( ) );
  AT();
  for ( std::list< channel_input_type >::const_iterator c = Channels.begin();
	c != Channels.end();
	c++ )
  {
    if ( ( c->s_q != 1 ) &&
	 ( c->s_q != 2 ) &&
	 ( c->s_q != 4 ) &&
	 ( c->s_q != 8 ) &&
	 ( c->s_q != 16) )
    {
      toss< std::range_error >( "std::range_error",
				  __FILE__, __LINE__,
				  "Can only resample at 2, 4, 8, or 16" );
    }
    AT();
    m_channels.push_back( channel_info_type( *c ) );
    AT();
  }
  AT();
}

ConditionData::
~ConditionData( )
{
  AT();
  //---------------------------------------------------------------------
  // Delete the data that is still known
  //---------------------------------------------------------------------

  for ( channels_type::iterator ci = m_channels.begin( );
	ci != m_channels.end( );
	ci++ )
  {
    for ( frame_data_groups_type::iterator fdgi =
	    (*ci).m_frame_data_groups.begin( );
	  fdgi != (*ci).m_frame_data_groups.end( );
	  fdgi++ )
    {
      for ( continuous_segment_type::iterator csi = (*fdgi).begin( );
	    csi != (*fdgi).end( );
	    csi++ )
      {
	(*csi).ResetBase( );
	(*csi).ResetFrameH( );
      }
    }
  }

  for ( frame_list_type::iterator fli( m_frames.begin( ) );
	fli != m_frames.end( );
	fli++ )
  {
    delete *fli;
    *fli = static_cast< FrameCPP::FrameH* >( NULL );
  }
  m_frames.resize( 0 );

  //---------------------------------------------------------------------
  // Delete Frame Group data
  //---------------------------------------------------------------------
  for ( channels_type::iterator ch( m_channels.begin( ) );
	ch != m_channels.end( );
	ch++ )
  {
    AT();
    (*ch).m_frame_data_groups.resize( 0 );
  }
  m_channels.resize( 0 );

  //---------------------------------------------------------------------
  // Delete History information
  //---------------------------------------------------------------------
  AT();
  for ( std::list< FrameCPP::FrHistory* >::const_iterator
	  h( m_history.begin( ) );
	h != m_history.end( );
	h++ )
  {
      delete *h;
  }
  m_history.erase( m_history.begin( ),
		   m_history.end( ) );
    
  for ( std::list< FrameCPP::FrDetector* >::const_iterator
	  d( m_detectors.begin( ) );
	d != m_detectors.end( );
	d++ )
  {
    delete *d;
  }
  m_detectors.erase( m_detectors.begin( ),
		     m_detectors.end( ) );
  AT();
}

void ConditionData::
eval( )
{
  static const char* method = "FrameAPI::ConditionData::eval";
  AT();

  frame_group_info_type		fgi;

  fgi.initialized = false;

  //--------------------------------------------------------------------
  // Loop over each frame
  //--------------------------------------------------------------------
  AT();
  for ( frame_files_type::const_iterator ff = m_frame_files.begin();
	ff != m_frame_files.end();
	ff++ )
  {
    General::Thread::Self::CancellationCheck( method,
					      __FILE__, __LINE__ );
    INFO( "processing frame file: " << *ff );
    process_frame_file( *ff, fgi );
  }

  //--------------------------------------------------------------------
  // Validate that each channel at least got some data
  //--------------------------------------------------------------------

  std::string	bad_channels( "" );

  for ( channels_type::iterator channel = m_channels.begin();
	channel != m_channels.end();
	channel++ )
  {
    if ( (*channel).m_first_frame_start == General::GPSTime( ) )
    {
      if ( bad_channels.length( ) != 0 )
      {
	bad_channels += ", ";
      }
      if ( (*channel).m_info.s_mode == channel_input_type::NAME)
      {
	bad_channels += (*channel).m_info.s_name;
      }
      else
      {
	std::ostringstream	num;

	num << (*channel).m_info.s_index;
	bad_channels += num.str( );
      }
    }
  }

  if ( bad_channels.length( ) > 0 )
  {
    std::ostringstream	msg;

    msg << "The following channels were not found in any of the frames processed: "
	<< bad_channels;
    throw std::runtime_error( msg.str( ) );
  }

  //--------------------------------------------------------------------
  // Concat the channels
  //--------------------------------------------------------------------

  if ( m_channels.size( ) && m_channels.front( ).m_frame_data_groups.size( ) )
  {
    m_max =
      m_min =
      m_channels.front( ).m_frame_data_groups.front( ).front( ).m_start;

    AT();
    for ( channels_type::iterator ch( m_channels.begin( ) );
	  ch != m_channels.end( );
	  ch++ )
    {
      AT();
      General::GPSTime	ts( (*ch).m_frame_data_groups.front( ).front( ).m_start );
      General::GPSTime	te( (*ch).m_frame_data_groups.back( ).back( ).m_start +
			    (*ch).m_frame_data_groups.back( ).back( ).m_dt );
      if ( ts < m_min )
      {
	m_min = ts;
      }
      if ( te > m_max )
      {
	m_max = te;
      }
    }

    INFO( "m_min: " << m_min );
    INFO( "m_max: " << m_max );

    AT();
    for ( channels_type::iterator ch( m_channels.begin( ) );
	  ch != m_channels.end( );
	  ch++ )
    {
      AT();
      for ( frame_data_groups_type::iterator
	      fdg( (*ch).m_frame_data_groups.begin( ) );
	    fdg != (*ch).m_frame_data_groups.end( );
	    fdg++ )
      {
	AT();
	(*ch).m_data.push_back( segment_info_type() );
	segment_info_type&	data( (*ch).m_data.back( ) );

	data.m_start = m_min;
	data.m_dt = calc_dt( *fdg );
	switch( (*ch).m_output_data_type )
	{
	case ADC:
	  AT();
	  data.m_segment.
	    reset( create_segment< FrameCPP::FrAdcData >( *fdg, m_min ) );
	  break;
	case PROC:
	  AT();
	  data.m_segment.
	    reset( create_segment< FrameCPP::FrProcData >( *fdg, m_min ) );
	  break;
	default:
	  AT();
	  //:TODO:
	  break;
	}
	data.ResetFrameH( (*fdg).front( ).ReleaseFrameH( ) );
      }
    }
  }
}

ConditionData::frame_list_type ConditionData::
releaseFrames( )
{
  frame_list_type	retval( m_frames );
  m_frames.resize( 0 );
  return retval;
}

template< class channel_type >
void ConditionData::channel_info_type::
reduce( const channel_type* Channel,
	const General::GPSTime& Start,
	const FrameH* Frame,
	const General::GPSTime& ChannelStart,
	const General::GPSTime& ChannelEnd,
	const REAL_8 StartFrequency,
	const REAL_8 DeltaFrequency,
	const bool Concatinate,
	FrameCPP::FrameH* FramePtr )
{
  //---------------------------------------------------------------------
  // Initialize some local variables
  //---------------------------------------------------------------------

  AT();
  const FrameCPP::Time& xfst( Frame->GetGTime( ) );

  General::GPSTime	fstart_time( xfst.getSec( ), xfst.getNSec( ) );
  General::GPSTime	fend_time( fstart_time + Frame->GetDt( ) );

  //---------------------------------------------------------------------
  // Check to see if there is anything to be done here
  //---------------------------------------------------------------------

  AT();
  if ( ! ( ( ChannelStart < fend_time ) &&
	   ( ChannelEnd > fstart_time ) ) )
  {
    // Nothing to do since the current frame has no data to contribute
    AT();
    return;
  }

  //---------------------------------------------------------------------
  // :TRICKY: freeable_channel only gets a value when a new channel
  //          get allocated. Such is the case when trimming a channel.
  //          It is here to prevent memory leaks.
  //	      The rest of the code references Channel.
  //---------------------------------------------------------------------
  AT();

  std::unique_ptr<const channel_type>	freeable_channel;

  //---------------------------------------------------------------------
  // Trim the data
  //---------------------------------------------------------------------
  AT();
  REAL_8	offset( ( ChannelStart >= fstart_time )
			? ( ChannelStart - fstart_time )
			: 0 );
  REAL_8	dt( ( ( fend_time > ChannelEnd )
		      ? ( ChannelEnd - fstart_time )
		      : ( Frame->GetDt( ) ) ) -
		    offset );
  channel_type*	tmp_channel;

  if ( ( DeltaFrequency == 0.0 ) &&
       ( ( offset != 0.0 ) || ( dt != Frame->GetDt( ) ) ) )
  {
    // Handle time series
    AT( );
    tmp_channel = trimChannel( const_cast<channel_type *>( Channel ),
			       offset, dt );
    
    if ( tmp_channel != Channel )
    {
      Channel = tmp_channel;
      freeable_channel.reset( Channel );
    }
  }
  else if ( DeltaFrequency != 0.0 )
  {
    // Handle frequency series
    AT( );
    tmp_channel = trimChannel( const_cast<channel_type*>( Channel ),
			       StartFrequency,
			       DeltaFrequency );
    if ( tmp_channel != Channel )
    {
      Channel = tmp_channel;
      freeable_channel.reset( Channel );
    }
    offset = Channel->GetTimeOffset( );
  }

  //---------------------------------------------------------------------
  // Resample the data
  //---------------------------------------------------------------------

  continuous_segment_info_type	csi;

  AT();
  if ( m_info.s_q > 1 )
  {
    //-------------------------------------------------------------------
    // Perform resampling on the data
    //-------------------------------------------------------------------

    AT();
    std::unique_ptr< FrameCPP::FrProcData >
      r ( resample( *Channel,
		    Channel->RefData( ),
		    Channel->RefData( ).begin( ),
		    Channel->RefData( ).end( ),
		    ( ChannelStart > fstart_time )
		    ? ChannelStart
		    : fstart_time,
		    csi.m_start ) );
    AT();
    csi.ResetBase( r.release( ) );
    csi.m_dt = dt;
    csi.ResetFrameH( FramePtr );
  }
  else
  {
    //-------------------------------------------------------------------
    // No resampling, just store the object to concat later
    //-------------------------------------------------------------------
    AT();
    csi.ResetBase( new channel_type( *Channel ) );
    csi.m_start = fstart_time + offset;
    INFO( "fstart_time: " << fstart_time << " offset: " << offset );
    csi.m_dt = dt;
    csi.ResetFrameH( FramePtr );
  }

  //---------------------------------------------------------------------
  // Descide if the segment is continuous or not
  //---------------------------------------------------------------------

  AT();
  if ( m_frame_data_groups.size( ) > 0 )
  {
    AT();
    General::GPSTime	end( m_frame_data_groups.back( ).back( ).m_start +
			     m_frame_data_groups.back( ).back( ).m_dt );
    bool		continuous( Continuous( csi.m_start, end ) );

    INFO( "FramePtr: " << (void*)FramePtr );
    if ( ( Concatinate == false ) || ( continuous == false ) )
    {
      if ( FramePtr && !continuous && Concatinate )
      {
	INFO( "INFO: Concatinate: " << Concatinate << " continuous: " << continuous );
	throw std::runtime_error( "Can not create frame with gaps in data" );
      }
      // Create a new segment group.
      continuous_segment_type new_elem;
      m_frame_data_groups.push_back( new_elem );
    }
  }
  else
  {
    continuous_segment_type new_elem;
    m_frame_data_groups.push_back( new_elem );
  }

  //---------------------------------------------------------------------
  // Add segment to the list
  //---------------------------------------------------------------------

  AT();
  m_frame_data_groups.back( ).push_back( csi );

  AT();
  //---------------------------------------------------------------------
  // If there is more than one element, then do sanity checks. When
  //   there is only one element, then sanity is self defined.
  //---------------------------------------------------------------------

  AT();
  if ( ( m_frame_data_groups.size( ) > 1 ) ||
       ( ( m_frame_data_groups.size( ) == 1 ) &&
	 ( m_frame_data_groups.back( ).size( ) > 1 ) ) )
  {
    AT();
    switch( m_output_data_type )
    {
    case ADC:
      {
	AT();
	const FrameCPP::FrAdcData&
	  adc1( dynamic_cast< const FrameCPP::FrAdcData& >
		( *(m_frame_data_groups.front( ).front( ).GetBase( ) ) ) );
	AT();
	const FrameCPP::FrAdcData&
	  adc2( dynamic_cast< const FrameCPP::FrAdcData& >
		( *(m_frame_data_groups.back( ).back( ).GetBase( ) ) ) );

	AT();
	//:TRICKY: let any exception cascade up
	areAdcsCompatable( adc1, adc2 );
      }
      break;
    case PROC:
      break;
    default:
      break;
    }
  }
  else
  {
    // Establish the output type
    AT();
    m_output_data_type = get_data_type( m_frame_data_groups.back( ).back( ).GetBase( ) );

  }
  AT();
  
}

template< class channel_type, class data_container_type, class data_iterator_type >
FrameCPP::FrProcData* ConditionData::channel_info_type::
resample( const channel_type& Channel,
	  data_container_type& Container,
	  data_iterator_type Begin,
	  data_iterator_type End,
	  const General::GPSTime& FrameStart,
	  General::GPSTime& SegmentStart )
{
  //---------------------------------------------------------------------
  // Determine if this is continuous
  //---------------------------------------------------------------------

  AT( );
  if ( m_resampler )
  {
    AT( );
    // Calculate the start of the channel 
    General::GPSTime	start( getStartTime( Channel,
					     FrameStart,
					     m_info.s_q,
					     m_resampler->getDelay( ) ) );
    General::GPSTime	end( m_frame_data_groups.back( ).back( ).m_start +
			     m_frame_data_groups.back( ).back( ).m_dt );


    AT( );
    if ( Continuous( start, end ) == false )
    {
      //-----------------------------------------------------------------
      // The start time and end time are not continuous. Release the
      //   resources associated with the resampler and get ready to
      //   allocate a new one.
      //-----------------------------------------------------------------
      destructResampleState( m_resampler );
      m_resampler = (Filters::ResampleBase*)NULL;
    }
  }

  //---------------------------------------------------------------------
  // ensure there is a resampler
  //---------------------------------------------------------------------

  if ( m_resampler == (Filters::ResampleBase*)NULL )
  {
    AT( );
    m_resampler = createResampleState( m_info.s_q, &Channel );
  }
    

  //---------------------------------------------------------------------
  // Setup the default return value
  //---------------------------------------------------------------------

  AT( );
  std::unique_ptr< FrameCPP::FrProcData >
    ret( resample_data( Channel, m_resampler ) );

  AT( );
  SegmentStart = getStartTime( Channel,
			       FrameStart,
			       m_info.s_q,
			       m_resampler->getDelay( ) );
  AT( );
  return ret.release( );
}

//-----------------------------------------------------------------------
// Local helper functions
//-----------------------------------------------------------------------

namespace {

  frame_group_info_type::
  frame_group_info_type( )
    : start( ),
      delta( 0.0 ),
      initialized( false )
  {
  }

  template< class Output, class Iterator, class Input >
  Output*
  getChannel( const std::string& Name,
	      Input& Container,
	      const std::string& Type )
  {
    Output*	result( (Output*)NULL );

    unsigned int	offset;			// Store the number
    std::string		junk;			// Left overs
    std::istringstream	i( Name.c_str( ) );	// input buffer

    if ( ( i >> offset ) && ! ( i >> junk ) )
    {
      //-----------------------------------------------------------------
      // Lookup the channel by index
      //-----------------------------------------------------------------
      if ( ( offset < 0 ) || ( offset >= Container.getSize( ) ) )
      {
	std::ostringstream	msg;

	msg << offset
	    << " is an invalid channel index. The valid range is 0 through "
	    << ( Container.getSize( ) - 1 )
	    << ".";
	throw std::range_error( msg.str( ) );
      }
      result = Container[ offset ];
    }
    else
    {
      //-----------------------------------------------------------------
      // Lookup the channel by name
      //-----------------------------------------------------------------
      std::pair< Iterator, Iterator >p( Container.hashFind( Name ) );

      if ( p.first == p.second )
      {
	std::ostringstream	msg;
	
	msg << Type << " Channel not found: " << Name;
	throw SWIGEXCEPTION( msg.str( ) );
      }
      result = p.first->second;
    }
    return result;
  }
}

//-----------------------------------------------------------------------
// ConditionData::channel_input_type
//-----------------------------------------------------------------------

ConditionData::channel_input_type::
channel_input_type( )
  : s_mode( INDEX ),
    s_name( "" ),
    s_index( 0 ),
    s_q( 1 ),
    s_data_type( UNKNOWN_DATA ),
    m_offset( 0 ),
    m_delta( 0 ),
    m_slice_specified( true )
{
}

ConditionData::channel_input_type::
channel_input_type( const ConditionData::channel_input_type& Source )
  : s_mode( Source.s_mode ),
    s_name( Source.s_name ),
    s_index( Source.s_index ),
    s_q( Source.s_q ),
    s_data_type( Source.s_data_type ),
    m_offset( Source.m_offset ),
    m_delta( Source.m_delta ),
    m_slice_specified( Source.m_slice_specified )
{
}

//-----------------------------------------------------------------------
// ConditionData::channel_info_type
//-----------------------------------------------------------------------
ConditionData::channel_info_type::
channel_info_type( )
  : m_info( ),
    m_data( ),
    m_frame_data_groups( ),
    m_start( ),
    m_end( ),
    m_output_data_type( UNKNOWN_DATA ),
    m_resampler( (Filters::ResampleBase*)NULL )
{
}

ConditionData::channel_info_type::
channel_info_type( const ConditionData::channel_input_type& ChannelInfo )
  : m_info( ChannelInfo ),
    m_data( ),
    m_frame_data_groups( ),
    m_start( ),
    m_end( ),
    m_output_data_type( UNKNOWN_DATA ),
    m_resampler( (Filters::ResampleBase*)NULL )
{
}

ConditionData::channel_info_type::
~channel_info_type( )
{
  AT();
  //---------------------------------------------------------------------
  // Deallocate memory used by class
  //---------------------------------------------------------------------
  AT();
  for ( frame_data_groups_type::iterator fdg( m_frame_data_groups.begin( ) );
	fdg != m_frame_data_groups.end( );
	fdg++ )
  {
    AT();
    for ( continuous_segment_type::iterator cs( (*fdg).begin( ) );
	  cs != (*fdg).end( );
	  cs++ )
    {
      AT();
      (*cs).ResetBase( );
    }
    AT();
  }
  AT();
  if ( m_resampler )
  {
    AT();
    destructResampleState( m_resampler );
    AT();
    m_resampler = (Filters::ResampleBase*)NULL;
  }
  AT();
}

void ConditionData::
process_frame( INT_4U Frame,
	       FileReader& Reader,
	       frame_group_info_type& FrameGroupInfo )
{
  static const char* method = "FrameAPI::ConditionData::process_frame";

  //---------------------------------------------------------------------
  // Read the frame header and some of the pieces
  //---------------------------------------------------------------------

  General::SharedPtr< FrameH > frame( Reader.ReadFrameH( Frame,
							 FrameH::HISTORY |
							 FrameH::DETECT_SIM |
							 FrameH::DETECT_PROC ) );

  if ( ! frame )
  {
    return;
  }

  //---------------------------------------------------------------------
  // record the start time and the delta
  //---------------------------------------------------------------------

  FrameCPP::Time	frame_time( frame->GetGTime( ) );

  //---------------------------------------------------------------------
  // Remember the table of contents
  //---------------------------------------------------------------------

  const FrameCPP::Common::FrTOC*		toc( Reader.GetTOC( ) );

  if ( FrameGroupInfo.initialized == true )
  {
    FrameGroupInfo.delta =
      ( frame_time - FrameGroupInfo.start ) +
      frame->GetDt( );
  }
  else
  {
    FrameGroupInfo.start = frame_time;
    FrameGroupInfo.delta = frame->GetDt( );
    FrameGroupInfo.initialized = true;
    
    // Remember the number of origional frame channels that were supplied.
    INT_4U			cnt( m_channels.size( ) );
    // Remember where in the vector of channels we are.
    INT_4U			pos( 0 );

    while ( ( cnt-- ) && ( pos != m_channels.size( ) ) )
    {
      General::Thread::Self::CancellationCheck( method,
						__FILE__, __LINE__ );
      if ( ( m_channels[ pos ].m_info.s_mode == channel_input_type::NAME ) &&
	   ( m_channels[ pos ].m_info.s_name == "all" ) )
      {
	//---------------------------------------------------------------
	// Replace the keyword 'all' with a list of appropriate channel
	//   names.
	//---------------------------------------------------------------
	
	channel_input_type		channel_input( m_channels[ pos ].m_info );

	// Remove the node containing the channel named 'all'.
	m_channels.erase( m_channels.begin( ) + pos );

	switch ( channel_input.s_data_type )
	{
	case ADC:
	  {
	    const FrameCPP::Common::FrTOC::cmn_name_container_type&
	      adcs( toc->nameADC( ) );
	    for ( FrameCPP::Common::FrTOC::cmn_name_container_type::const_iterator
		    cur = adcs.begin( ),
		    last = adcs.end( );
		  cur != last;
		  ++cur )
	    {
	      channel_input.s_name = *cur;
	      m_channels.push_back( channel_input );
	    }
	  }
	  break;
	case PROC:
	  {
	    // Add all entries of type FrProcData
	    const FrameCPP::Common::FrTOC::cmn_name_container_type&
	      procs( toc->nameProc( ) );
	    for ( FrameCPP::Common::FrTOC::cmn_name_container_type::const_iterator
		    cur = procs.begin( ),
		    last = procs.end( );
		  cur != last;
		  ++cur )
	    {
	      channel_input.s_name = *cur;
	      m_channels.push_back( channel_input );
	    }
	  }
	  break;
	default:
	  AT();
	  throw SWIGEXCEPTION( "Unsupported channel type" );
	  break;
	}
      }
      else
      {
	// This is not a channel named 'all'. Skip past it.
	pos++;
      }
    }
  }

  FrameCPP::FrameH* frameh( (FrameCPP::FrameH*)NULL );

  AT( );
  if ( m_mode & MODE_FRAME )
  {
    INFO( "Doing Frame Mode: " << m_mode << " MODE_FRAME: " << MODE_FRAME );
    AT( );
    if ( ( ( m_concatinate == true ) && ( m_frames.size( ) <= 0 ) ) ||
	 ( m_concatinate == false ) )
    {
      //-----------------------------------------------------------------
      // Duplicate the frame header including the history and detector
      //   information.
      //-----------------------------------------------------------------
      AT( );
      frameh = new FrameCPP::FrameH( *frame );

      frameh->SetGTime( FrameGroupInfo.start );
      AT( );
      m_frames.push_back( frameh );
    }
    else
    {
      frameh = m_frames.back( );
    }
  }
  if ( !( ( m_mode & MODE_FRAME ) && ( m_concatinate == false ) ) )
  {
    //-------------------------------------------------------------------
    // Extract out the history information
    //-------------------------------------------------------------------

    FrameH::history_type&	f_history( frame->RefHistory( ) );
  
    for ( FrameH::const_history_iterator h( f_history.begin( ) );
	  h != f_history.end( );
	  h++ )
    {
      m_history.push_back( new FrameCPP::FrHistory( *(*h) ) );
    }

    //-------------------------------------------------------------------
    // Extract out the detector information
    //-------------------------------------------------------------------

    if ( m_detectors.size( ) <= 0 )
    {
      FrameH::detectProc_type&	f_detectProc( frame->RefDetectProc( ) );
      
      for ( FrameH::const_detectProc_iterator d( f_detectProc.begin( ) );
	    d != f_detectProc.end( );
	    d++ )
      {
	m_detectors.push_back( new FrameCPP::FrDetector( *(*d) ) );
      }
    }
  }

  //---------------------------------------------------------------------
  // Process each channel
  //---------------------------------------------------------------------
  AT();
  for ( channels_type::iterator channel = m_channels.begin();
	channel != m_channels.end();
	channel++ )
  {
    //-------------------------------------------------------------------
    // Validate that the channel exists in the frame
    //-------------------------------------------------------------------
    try
    {
      switch( (*channel).m_info.s_data_type )
      {
      case ADC:
	if ( ( (*channel).m_info.s_mode == channel_input_type::NAME)
	     ||
	     ( (*channel).m_info.s_mode == channel_input_type::INDEX) )
	{
	  // Do Nothing
	}
	break;
      case PROC:
	if ( ( (*channel).m_info.s_mode == channel_input_type::NAME)
	     || ( (*channel).m_info.s_mode == channel_input_type::INDEX) )
	{
	  // Do Nothing
	}
	break;
      default:
	throw std::runtime_error( "Unknown channel type" );
	break;
      }
    }
    catch( ... )
    {
      continue;
    }
    
    //-------------------------------------------------------------------
    // local variables
    //-------------------------------------------------------------------
    REAL_8	start_frequency( 0.0 );
    REAL_8	delta_frequency( 0.0 );
    bool	first( false );

    //-------------------------------------------------------------------
    // Do some initilization if this is the first frame
    //-------------------------------------------------------------------
    if ( (*channel).m_first_frame_start == General::GPSTime( ) )
    {
      (*channel).m_first_frame_start = frame->GetGTime( );
      first = true;
    }
    //-------------------------------------------------------------------
    // Work with the channel data in the current frame
    //-------------------------------------------------------------------
    switch( (*channel).m_info.s_data_type )
    {
    case ADC:
      {
	if ( first )
	{
	  //-------------------------------------------------------------
	  // Calculate the time range requested by the user
	  //-------------------------------------------------------------
	  
	  channel->m_start = FrameGroupInfo.start + channel->m_info.m_offset;
	  channel->m_end = channel->m_start + channel->m_info.m_delta;
	}

	AT();
	General::SharedPtr< FrameCPP::FrAdcData > adc_data;
	if ( (*channel).m_info.s_mode ==
	     ConditionData::channel_input_type::NAME )
	{
	  AT();
	  adc_data = Reader.ReadFrAdcData( Frame,
					   (*channel).m_info.s_name );
	  if ( ! adc_data )
	  {
	    std::ostringstream	msg;
	
	    msg << "FrAdcData Channel not found: "
		<< (*channel).m_info.s_name;
	    throw SWIGEXCEPTION( msg.str( ) );
	  }
	}
	else
	{
	  AT();
	  adc_data = Reader.ReadFrAdcData( Frame,
					   (*channel).m_info.s_index );
	  if ( ! adc_data )
	  {
	    std::ostringstream	msg;
	
	    msg << "FrAdcData Channel not found: "
		<< (*channel).m_info.s_index;
	    throw SWIGEXCEPTION( msg.str( ) );
	  }
	}
	if ( adc_data )
	{
	  if ( ! ( adc_data->RefData( )[0] ) )
	  {
	    std::ostringstream oss;

	    oss << "Read invalid ADC Data channel: ";
	    if ( (*channel).m_info.s_mode ==
		 ConditionData::channel_input_type::NAME )
	    {
	      oss << (*channel).m_info.s_name;
	    }
	    else
	    {
	      oss << (*channel).m_info.s_index;
	    }
	    throw std::runtime_error( oss.str( ) );
	  }
	  /// \todo reduce needs to take a General::SharedPtr
	  (*channel).reduce( adc_data.get( ),
			     FrameGroupInfo.start,
			     frame.get( ),
			     (*channel).m_start, (*channel).m_end,
			     start_frequency,
			     delta_frequency,
			     m_concatinate,
			     frameh );
	}
      }
      break;
    case PROC:
      {
	AT();
	General::SharedPtr< FrameCPP::FrProcData > proc_data;
	
	if ( (*channel).m_info.s_mode ==
	     ConditionData::channel_input_type::NAME )
	{
	  AT();
	  proc_data = Reader.ReadFrProcData( Frame,
					     (*channel).m_info.s_name );
	  if ( ! proc_data )
	  {
	    std::ostringstream	msg;
	
	    msg << "FrProcData Channel not found: "
		<< (*channel).m_info.s_name;
	    throw SWIGEXCEPTION( msg.str( ) );
	  }
	}
	else
	{
	  AT();
	  proc_data = Reader.ReadFrProcData( Frame,
					     (*channel).m_info.s_index );
	  if ( ! proc_data )
	  {
	    std::ostringstream	msg;
	
	    msg << "FrProcData Channel not found: "
		<< (*channel).m_info.s_index;
	    throw SWIGEXCEPTION( msg.str( ) );
	  }
	}
	AT();
	if ( proc_data )
	{
	  if ( first )
	  {
	    //-----------------------------------------------------------
	    // Calculate the time range requested by the user
	    //-----------------------------------------------------------
	  
	    switch( proc_data->GetType( ) )
	    {
	    case FrameCPP::FrProcData::TIME_SERIES:
	      channel->m_start = FrameGroupInfo.start +
		channel->m_info.m_offset;
	      channel->m_end = channel->m_start + channel->m_info.m_delta;
	      break;
	    case FrameCPP::FrProcData::FREQUENCY_SERIES:
	      if ( channel->m_info.m_slice_specified == false )
	      {
		std::ostringstream oss;

		oss << "No slice specified for Frequency channel ( ";
		if ( channel->m_info.s_mode ==
		     ConditionData::channel_input_type::NAME )
		{
		  oss << channel->m_info.s_name;
		}
		else
		{
		  oss << channel->m_info.s_index;
		}
		oss << " )";
		throw std::runtime_error( oss.str( ).c_str( ) );
	      }
	      channel->m_start = frame->GetGTime( ) +
		proc_data->GetTimeOffset( );
	      channel->m_end = frame->GetGTime( ) + frame->GetDt( );
	      AT( );

	      start_frequency = channel->m_info.m_offset;
	      delta_frequency = channel->m_info.m_delta;
	      break;
	    default:
	      {
		std::ostringstream	oss;

		oss << "Channel of unknown type requested: ";
		if ( (*channel).m_info.s_mode ==
		     ConditionData::channel_input_type::NAME )
		{
		  AT();
		  oss << "name(" << (*channel).m_info.s_name << ")";
		}
		else
		{
		  AT();
		  oss << "index(" << (*channel).m_info.s_index  << ")";
		}
		
		throw std::runtime_error( oss.str( ).c_str( ) );
	      }
	      break;
	    }
	  }
	  else
	  {
	    switch( proc_data->GetType( ) )
	    {
	    case FrameCPP::FrProcData::FREQUENCY_SERIES:
	      channel->m_end = frame->GetGTime( ) + frame->GetDt( );
	      start_frequency = channel->m_info.m_offset;
	      delta_frequency = channel->m_info.m_delta;
	      break;
	    }
	  }

	  if ( ! ( proc_data->RefData( )[0] ) )
	  {
	    std::ostringstream oss;

	    oss << "Read invalid Proc Data channel: ";
	    if ( (*channel).m_info.s_mode ==
		 ConditionData::channel_input_type::NAME )
	    {
	      oss << (*channel).m_info.s_name;
	    }
	    else
	    {
	      oss << (*channel).m_info.s_index;
	    }
	    throw std::runtime_error( oss.str( ) );
	  }
	  /// \todo reduce needs to take a General::SharedPtr
	  (*channel).reduce( proc_data.get( ),
			     FrameGroupInfo.start,
			     frame.get( ),
			     (*channel).m_start, (*channel).m_end,
			     start_frequency,
			     delta_frequency,
			     m_concatinate,
			     frameh );
	}
      }
      break;
    default:
      AT();
      throw SWIGEXCEPTION( "Unsupported channel type" );
      break;
    }
  }
  AT();
}

void ConditionData::
process_frame_file( const std::string& InputFile,
		    frame_group_info_type& FrameGroupInfo )
{
  AT();
#if LM_DEBUG
  std::cerr << "DEBUG: File: " << InputFile << std::endl;
#endif
  FileReader	reader( InputFile.c_str( ) );

  if ( ! reader.good( ) )
  {
    std::ostringstream	oss;

    oss << "Bad frame input file: " << InputFile;
    throw std::runtime_error( oss.str( ).c_str( ) );
  }
  AT();
  INT_4U	frames( reader.GetTOC( )->nFrame( ) );
  for ( INT_4U x = 0; x < frames; x++ )
  {
    try
    {
      AT();
#if LM_DEBUG
      std::cerr << "DEBUG: Frame: " << x
		<< " of File: " << InputFile
		<< std::endl
	;
#endif
      process_frame( x, reader, FrameGroupInfo );
    }
    catch( const std::exception& e )
    {
      std::ostringstream	oss;
      oss << "Exception reading frame " << x
	  << " from file " << InputFile
	  << " (" << e.what( ) << ") "
	  << std::endl
	;
      throw std::runtime_error( oss.str( ).c_str( ) );
    }
  }
}


ConditionData::segment_info_type::
segment_info_type( )
  : m_segment( ),
    m_start( ),
    m_dt( 0 ),
    m_frameh( static_cast< FrameCPP::FrameH* >( NULL ) )

{
}

ConditionData::segment_info_type::
~segment_info_type( )
{
  ResetFrameH( );
}

ConditionData::segment_info_type::
segment_info_type( const segment_info_type& Source )
  : m_segment( Source.m_segment ),
    m_start( Source.m_start ),
    m_dt( Source.m_dt ),
    m_frameh( const_cast< segment_info_type& >( Source ).m_frameh )
{
}

const ConditionData::segment_info_type& ConditionData::segment_info_type::
operator=( const segment_info_type& Source )
{
  m_segment = Source.m_segment;
  m_start = Source.m_start;
  m_dt = Source.m_dt;
  m_frameh = const_cast< segment_info_type& >( Source ).m_frameh;
  return *this;
}
