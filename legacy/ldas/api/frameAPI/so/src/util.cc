#include "ldas_tools_config.h"

#include <cmath>
#include <iomanip>
#include <sstream>
#include <string>

#include "general/types.hh"
#include "general/gpstime.hh"

#include "framecpp/Common/FrameFilename.hh"
#include "framecpp/Common/MD5Sum.hh"

#include "framecpp/FrameH.hh"
#include "framecpp/FrAdcData.hh"
#include "framecpp/FrProcData.hh"
#include "framecpp/FrTable.hh"
#include "framecpp/FrVect.hh"

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/FileLock.hh"
#include "genericAPI/swigexception.hh"

#include "DeviceIOConfiguration.hh"

#include "convert.hh"
#include "util.hh"

#if ! CORE_API
#if HAVE_LDAS_PACKAGE_ILWD
using namespace ILwd;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
#endif /* ! CORE_API */

using namespace FrameAPI;
using std::string;

using FrameCPP::Common::FrameFilename;
using GenericAPI::FileLock;


int FrameAPI::EnableMemoryMappedIO = false;
int FrameAPI::StreamBufferSize
= int( FrameCPP::Common::FrameBufferInterface::M_BUFFER_SIZE_DEFAULT );

#if ! CORE_API
bool FrameAPI::
Continuous( const ILwd::LdasContainer& C1,
	    const ILwd::LdasContainer& C2 )
try {
  return Continuous( GetEndTime( C1 ), GetStartTime( C2 ) );
}
catch( ... )
{
  return false;
}
#endif /* ! CORE_API */


#if CORE_API
bool FrameAPI::
Continuous( const General::GPSTime& T1,
	    const General::GPSTime& T2 )
try {
  const REAL_8	t1( T1.GetSeconds( ) +
		    ( T1.GetNanoseconds( ) * NANO_SECOND ) );
  const REAL_8	t2( T2.GetSeconds( ) +
		    ( T2.GetNanoseconds( ) * NANO_SECOND ) );

  return( std::abs( t1 - t2 ) <= TIME_TOLERANCE );
}
catch( ... )
{
  return false;
}
#endif /* CORE_API */

#if ! CORE_API
General::GPSTime FrameAPI::
GetEndTime( const ILwd::LdasContainer& C )
{
  const REAL_8 sr( findArrayType< REAL_8 >( C, "samplerate" ).getData()[0] );
  const LdasContainer* cn1( findContainerType( C, "data" ) );
  const LdasElement* elem1( ( *cn1 )[ 0 ] );
  const LdasArrayBase* data1( dynamic_cast< const LdasArrayBase* >( elem1 ) );

  return General::GPSTime( GetStartTime( C ) +
			   ( data1->getDimension( 0 ) / sr ) );
}
#endif /* ! CORE_API */

#if ! CORE_API
General::GPSTime FrameAPI::
GetStartTime( const ILwd::LdasContainer& C )
{
  //---------------------------------------------------------------------
  // Calculate the start time of C
  //---------------------------------------------------------------------
  INT_4U	offset[ 2 ];

  //---------------------------------------------------------------------
  // Extract the offset value
  //---------------------------------------------------------------------
  try
  {
    const LdasArray< INT_4S >&
      t( findArrayType< INT_4S >( C, "timeoffset" ) );
    const INT_4S* o( t.getData() );

    offset[ 0 ] = (INT_4U)(o[0]);
    offset[ 1 ] = (INT_4U)(o[1]);
  }
  catch( ... )
  {
    const LdasArray< INT_4U >&
      t( findArrayType< INT_4U >( C, "timeoffset" ) );
    const INT_4U* o( t.getData() );

    offset[ 0 ] = o[0];
    offset[ 1 ] = o[1];
  }

  //---------------------------------------------------------------------
  // Return frame start time plus the offset. The GPSTime class will
  //   normalize the input.
  //---------------------------------------------------------------------
  const string& tmp_sec( C.getName( 3 ) );
  const string& tmp_nan( C.getName( 4 ) );  
   
  INT_4U	seconds;
  INT_4U	nanoseconds;

  std::istringstream	b( tmp_sec );
  b >> seconds;
  b.str( tmp_nan );
  b >> nanoseconds;

  return General::GPSTime( seconds + offset[ 0 ],
			   nanoseconds + offset[ 1 ] );
}
#endif /* ! CORE_API */

#if CORE_API
void FrameAPI::
LogMD5Sum( const std::string& FFilename,
	   const FrameCPP::Common::MD5Sum& MD5,
	   const std::string& OutputDir )
{
  const FrameFilename		ffname( FFilename );
  std::ostringstream		md5_string;
  std::string::size_type	p = ffname.Dir( ).rfind( '/' );

  if ( p == std::string::npos )
  {
    return;
  }
  std::string	filename( ( OutputDir.length( ) > 0 )
			  ? OutputDir
			  : ffname.Dir( ) );
  filename += "/";
  filename += ffname.Dir( ).substr( ++p );
  filename += ".md5";
  
  FileLock	md5_file( filename );
  md5_file.Buffer( ) << MD5 << "  " << ffname.Base( ) << std::endl;
}
#endif /* CORE_API */

#if CORE_API
FrameCPP::FrVect::compression_scheme_type FrameAPI::
StrToCompressionScheme( const char* Method )
{
   //-------------------------------------------------------------------
   // Sanity checks on compression method parameter
   //-------------------------------------------------------------------
   std::string method( slower( Method ) );
   FrameCPP::FrVect::compression_scheme_type
      retval = FrameCPP::FrVect::RAW;
   if ( method.length( ) )
   {
     using namespace FrameCPP;

     if ( method == "raw" )
     {
       retval = FrVect::RAW;
     }
     else if ( method == "gzip" )
     {
       retval = FrVect::GZIP;
     }
     else if ( method == "diff_gzip" )
     {
       retval = FrVect::DIFF_GZIP;
     }
     else if ( method == "zero_suppress_short" )
     {
       retval = FrVect::ZERO_SUPPRESS_SHORT;
     }
     else if ( method == "zero_suppress_int_float" )
     {
       retval = FrVect::ZERO_SUPPRESS_INT_FLOAT;
     }
     else if ( method == "zero_suppress_otherwise_gzip" )
     {
       retval = FrVect::ZERO_SUPPRESS_OTHERWISE_GZIP;
     }
     else
     {
       std::ostringstream	oss;

       oss << "Bad compression method: " << Method;
       throw SWIGEXCEPTION( oss.str( ) );
     }
   }
   return retval;
}
#endif /* CORE_API */
