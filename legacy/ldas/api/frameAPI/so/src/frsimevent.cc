/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "config.h"

// System Header Files
#include <sstream>      
   
#include "frsimevent.hh"
#include "getattribute.hh"
#include "convert.hh"
#include "frvect.hh"
#include "frtable.hh"
#include "util.hh"


using FrameCPP::FrSimEvent;
using FrameCPP::FrTable;
using FrameCPP::FrVect;
using FrameCPP::Time;

using ILwd::LdasElement;
using ILwd::LdasContainer;
using ILwd::LdasArray;
using ILwd::LdasArrayBase;
using ILwd::LdasString;
   
using namespace std;   
   
//!ignore_begin:

enum
{
  FR_NAME,
  FR_COMMENT,
  FR_INPUTS,
  FR_GTIMES,
  FR_GTIMEN,

  FR_TIMEBEFORE,
  FR_TIMEAFTER,
  FR_AMPLITUDE,

  FR_DATA,
  FR_TABLE
};


QueryHash initsimEventHash()
{
  QueryHash h;
  h[ "name"        ] = FR_NAME;
  h[ "comment"     ] = FR_COMMENT;
  h[ "inputs"      ] = FR_INPUTS;
  h[ "gtimes"      ] = FR_GTIMES;
  h[ "gtimen"      ] = FR_GTIMEN;
  h[ "timebefore"  ] = FR_TIMEBEFORE;
  h[ "timeafter"   ] = FR_TIMEAFTER;
  h[ "amplitude"   ] = FR_AMPLITUDE;
  h[ "data"        ] = FR_DATA;
  h[ "table"       ] = FR_TABLE;

  return h;
}


static QueryHash simEventHash( initsimEventHash() );    
    
    
//-----------------------------------------------------------------------------
std::string getAttribute( const FrSimEvent& simEvent, std::deque< Query >& dq,
                          std::vector< size_t >& start, size_t index )
{
  if ( dq.size() == 0 )
    {
      throw SWIGEXCEPTION( "bad_query" );
    }
    
  Query q = dq.front();
  dq.pop_front();
  ostringstream res;
    
  QueryHash::const_iterator iter =
    simEventHash.find( q.getName().c_str() );
  if ( iter == simEventHash.end() )
    {
      throw SWIGEXCEPTION( "bad_query" );
    }

  if ( !q.isQuery() )
    {
      switch( iter->second )
        {
	case FR_NAME:
	  res << simEvent.GetName();
	  break;
                
	case FR_COMMENT:
	  res << simEvent.GetComment();
	  break;
                
	case FR_INPUTS:
	  res << simEvent.GetInputs();
	  break;
                
	case FR_GTIMES:
	  res << simEvent.GetGTime().getSec();
	  break;
                
	case FR_GTIMEN:
	  res << simEvent.GetGTime().getNSec();
	  break;
                
	case FR_TIMEBEFORE:
	  res << simEvent.GetTimeBefore();
	  break;
                
	case FR_TIMEAFTER:
	  res << simEvent.GetTimeAfter();
	  break;

	case FR_AMPLITUDE:
	  res << simEvent.GetAmplitude();
	  break;

	case FR_DATA:
	  return getAttribute( simEvent.RefData(), dq );

	case FR_TABLE:
	  return getAttribute( simEvent.RefTable(), dq );

	default:
	  throw SWIGEXCEPTION( "bad_query" );
        }
    }
  else
    {
      switch( iter->second )
        {
	case FR_DATA:
	  return getAttribute( getContained( simEvent.RefData(),
					     q, start, index ),
			       dq, start, index + 1 );
	  break;

	case FR_TABLE:
	  return getAttribute( getContained( simEvent.RefTable(),
					     q, start, index ),
			       dq, start, index + 1 );
	  break;

	default:
	  throw SWIGEXCEPTION( "bad_query" );
        }
    }

  return res.str();
}


//-----------------------------------------------------------------------------
LdasElement* getData( const FrSimEvent& simEvent, std::deque< Query >& dq,
                      std::vector< size_t >& start, size_t index,
                      const Time& gtime, const REAL_8& dt )
{
  if ( dq.size() == 0 )
    {
      return simEvent2container( simEvent, gtime, dt );
    }
    
  Query q( dq.front() );
  dq.pop_front();
    
  QueryHash::const_iterator iter = simEventHash.find( q.getName().c_str() );
  if ( iter == simEventHash.end() )
    {
      throw SWIGEXCEPTION( "bad_query" );
    }

  switch( iter->second )
    {
    case FR_DATA:
      return getData( getContained(simEvent.RefData(),
				   q, start, index ),
		      dq, start, index + 1 );
      break;

    case FR_TABLE:
      return getData( getContained( simEvent.RefTable(),
				    q, start, index ),
		      dq, start, index + 1 );
      break;

    default:
      throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "bad_query" );
}

//!ignore_end:


//-----------------------------------------------------------------------------
// ILWD Conversion
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//
//: Convert an LdasContainer to simEvent.
//
//!usage_ooi: FrSimEvent* simEvent = container2simEvent( c );
//
//!param: const LdasContainer& c - simEvent container.
//
//!return: FrSimEvent* simEvent - FrameCPP::FrSimEvent newly allocated object.
//
//!exc: SwigException - invalid simEvent container.
//
//-----------------------------------------------------------------------------
FrSimEvent* container2simEvent( const LdasContainer& c )
{
   const string& c_name( c.getName( 2 ) );
   
   if ( strcasecmp( c_name.c_str(), "simEvent" ) != 0 )
   {
      throw SWIGEXCEPTION( "invalid_format: Not simEvent container." );
   }
    
   FrSimEvent:: ParamList_type	params;
   const LdasArray< INT_4U >& te = findArrayType< INT_4U >( c, "GTime" );
   FrSimEvent* sim
       = new FrSimEvent(c.getName( 0 ),
			findLStringType( c, "comment" ).getString(),
			findLStringType( c, "inputs" ).getString(),
			FrameCPP::Time( te.getData()[ 0 ], te.getData()[ 1 ] ),
			*findArrayType< REAL_4 >( c, "timebefore" ).getData(),
			*findArrayType< REAL_4 >( c, "timeafter" ).getData(),
			*findArrayType< REAL_4 >( c, "amplitude" ).getData(),
			params );
  try
    {
      const LdasContainer* data( findContainerType( c, "data" ) );
      if ( data != 0 )
        {
	  for ( unsigned int i = 0; i < data->size(); ++i )
            {
		FrSimEvent::data_type::value_type
		    d( array2vect( dynamic_cast< const LdasArrayBase& >( *(*data)[ i ] ) ) );

		sim->RefData().append( d );
            }
        }

      const LdasContainer* table( findContainerType( c, "table" ) );
      if ( table != 0 )
        {
	  for ( unsigned int i = 0; i < table->size(); ++i )
            {
		FrSimEvent::table_type::value_type
		    t( container2table( dynamic_cast< const LdasContainer& >( *(*table)[ i ] ) ) );

	      sim->RefTable().append( t );
            }
        }
    }
  catch( std::bad_cast& )
    {
      delete sim;
      sim = ( FrameCPP::FrSimEvent*)NULL;
      throw SWIGEXCEPTION( "invalid_format" );
    }
    
  return sim;
}

//-----------------------------------------------------------------------------
//
//: Convert FrSimEvent to ILWD.
//
//!usage_ooi: LdasContainer* simEventContainer =
//+	simEvent2container( simEvent, gtime, dt );
//
//!param: const FrSimEvent& simEvent - FrameCPP::FrSimEvent object to convert.
//!param: const Time& gtime - FrameCPP::Frame time.
//!param: const REAL_8& dt - FrameCPP::Frame delta time.
//
//!return: LdasContainer* simEventContainer - newly created FrSimEvent container.
//
//-----------------------------------------------------------------------------
LdasContainer* simEvent2container( const FrSimEvent& simEvent,
				   const Time& gtime,
				   const REAL_8& dt )
{
  LdasContainer* c = new LdasContainer( "::simEvent" );
  c->setName( 0, simEvent.GetName() );
    
  ostringstream ss;
  ss.precision( REAL_8_DIGITS + 1 );
  ss << gtime.getSec();
  c->appendName( ss.str() );
 
  ss.str( "" );
  ss << gtime.getNSec();
  c->appendName( ss.str() );

  c->appendName( "Frame" );

  LdasString* comment( new LdasString( simEvent.GetComment(), "comment" ) );
  LdasString* inputs( new LdasString( simEvent.GetInputs(), "inputs" ) );

  INT_4U time[ 2 ] = { simEvent.GetGTime().getSec(),
		       simEvent.GetGTime().getNSec() };

  LdasArray< INT_4U >* gTime( new LdasArray< INT_4U >( time, 2, "GTime" ) );
  LdasArray< REAL_4 >* timebefore( new LdasArray< REAL_4 >( simEvent.GetTimeBefore(), "timeBefore" ) );
  LdasArray< REAL_4 >* timeafter( new LdasArray< REAL_4 >( simEvent.GetTimeAfter(), "timeAfter" ) );
  LdasArray< REAL_4 >* amplitude( new LdasArray< REAL_4 >( simEvent.GetAmplitude(), "amplitude" ) );
  LdasArray< REAL_8 >* e_dt( new LdasArray< REAL_8 >( dt, "dt" ) );

  c->push_back( comment,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
  c->push_back( inputs,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
  c->push_back( gTime,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
  c->push_back( timebefore,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN  );
  c->push_back( timeafter,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
  c->push_back( amplitude,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );
  c->push_back( e_dt,
		ILwd::LdasContainer::NO_ALLOCATE, ILwd::LdasContainer::OWN );

  size_t dataSize( simEvent.RefData().size() );
  if ( dataSize > 0 )
    {
      LdasContainer* data( new LdasContainer( ":data:Container(Vect):Frame" ) );
      c->push_back( data,
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
        
      FrSimEvent::const_iterator iter( simEvent.RefData().begin() );
      for( size_t i = dataSize; i != 0; --i, ++iter )
        {
	  data->push_back( vect2array( **iter ),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN  );
        }
    }

  size_t tableSize( simEvent.RefTable().size() );
  if ( tableSize > 0 )
    {
      LdasContainer* table( new LdasContainer( ":table:Container(Table):Frame" ) );
      c->push_back( table, 
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
        
      FrSimEvent::const_table_iterator iter( simEvent.RefTable().begin() );
      for( size_t i = tableSize; i != 0; --i, ++iter )
        {
	  table->push_back( table2container( **iter ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN  );
        }
    }
    
  return c;
}



//-----------------------------------------------------------------------------
//
//: Insert FrSimEvent container into Frame container.
//
//!usage_ooi: insertSimEvent( fr, sim );
//
//!param: LdasContainer& fr - Frame container.
//!param: LdasContainer& sim - FrSimEvent container.
//
//!exc: SwigException - bad container data.
//
//-----------------------------------------------------------------------------
void insertSimEvent( LdasContainer& fr, LdasContainer& sim )
{
  bool firstdata( true );
    
  // Find the GTime & dt elements of the frame container. 
  LdasElement* fr_gtime = fr.find( "gtime" );
  LdasElement* fr_dt = fr.find( "dt" );
  LdasElement* sim_dt = sim.find( "dt" );

  // Check to make sure the frame data is valid
  if ( fr_gtime == 0 || fr_gtime->getElementId() != ILwd::ID_INT_4U ||
       fr_dt == 0 || fr_dt->getElementId() != ILwd::ID_REAL_8 )
    {
      throw SWIGEXCEPTION( "bad_frame" );
    }

  // Check to make sure the FrSimEvent data is valid
  if ( sim_dt == 0 || sim_dt->getElementId() != ILwd::ID_REAL_8 )
    {
      throw SWIGEXCEPTION( "bad_simEvent" );
    }

  // Check the characteristics of the GTime element.
  LdasArray< INT_4U >& gtime( dynamic_cast< LdasArray< INT_4U >& >( *fr_gtime ) );
  if ( gtime.getNDim() != 1 || gtime.getDimension( 0 ) != 2 )
    {
      throw SWIGEXCEPTION( "bad_frame" );
    }

  // Check the characteristics of the Frame Dt element.
  LdasArray< REAL_8 >& fdt( dynamic_cast< LdasArray< REAL_8 >& >( *fr_dt ) );
  if ( fdt.getNDim() != 1 || fdt.getDimension( 0 ) != 1 )
    {
      throw SWIGEXCEPTION( "bad_frame" );
    }

  // Check the characteristics of the sim Dt element.
  LdasArray< REAL_8 >& adt( dynamic_cast< LdasArray< REAL_8 >& >( *sim_dt ) );
  if ( adt.getNDim() != 1 || adt.getDimension( 0 ) != 1 )
    {
      throw SWIGEXCEPTION( "bad_simEvent" );
    }

  // If 'fdt' is not zero, then we must have some data in the frame.  Check
  // to make sure the start times and length for the data matches that of the
  // frame.
  if ( fdt.getData()[ 0 ] != 0 )
    {
      INT_4U time;
        
      // Check the GPS seconds.
      std::string tmp( sim.getName( 3 ) );
      time = strtoul( tmp.c_str(), 0, 10 );
      if ( time != gtime.getData()[ 0 ] )
        {
	  throw SWIGEXCEPTION( "incompatible_time" );
        }

      // Check the GPS nanoseconds.
      tmp = sim.getName( 4 );
      time = strtoul( tmp.c_str(), 0, 10 );
      if ( time != gtime.getData()[ 1 ] )
        {
	  throw SWIGEXCEPTION( "incompatible_time" );
        }

      // Check the length
      if ( fdt.getData()[ 0 ] != adt.getData()[ 0 ] )
        {
	  throw SWIGEXCEPTION( "incompatible_length" );
        }

      firstdata = false;
    }

  // Find the simEvent container in frame if it exists
  LdasElement* t = fr.find( "simEvent", 1 );
  if ( t == 0 )
    {
      // it didn't exist so create it.
      fr.push_back( LdasContainer( ":simEvent:Container(SimEvent)" ) );
      t = fr.back();
    }
  // Check the simEvent container
  else if ( t->getElementId() != ILwd::ID_ILWD )
    {
      throw SWIGEXCEPTION( "bad_frame" );
    }

  // If this is the first data element in the frame, set the frame GPS time
  // and length to correspond to the inserted object.
  if ( firstdata )
    {
      std::string tmp( sim.getName( 3 ) );
      gtime.getData()[ 0 ] = strtoul( tmp.c_str(), 0, 10 );
      tmp = sim.getName( 4 );
      gtime.getData()[ 1 ] = strtoul( tmp.c_str(), 0, 10 );
      fdt.getData()[ 0 ] = adt.getData()[ 0 ];
    }    

  // OK, everything checks out so add the simEvent
  dynamic_cast< LdasContainer& >( *t ).push_back( sim );
}
