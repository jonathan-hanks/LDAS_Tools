## ********************************************************
##
## Name: runMpiJob.mpi
##
## Description:
##
## Parameters:
## returnformat - "frame" for binary, "ilwd" for text.
## returnprotocol - ftp, http, mail, file or port, as available.
##
## Usage:
## 
## Comments:
##
## ******************************************************** 

## ******************************************************** 
## Initialise global variables from the user-supplied and
## default options.  These are not all required.  It is
## not a requirement to initialise them at the global level.
##
## ******************************************************** 

proc runMpiJob { jobid } {

     ;## some convenient local variables
     set dll    [ set ::${jobid}(-dynlib)         ]
     set dctarg [ set ::${jobid}(-datacondtarget) ]
     set algos  [ set ::${jobid}(-algorithms)     ]
     set if     [ set ::${jobid}(-inputfile)      ]
     
     if { [ info exists ::${jobid}(-dbquery) ] } {
        set dbquery [ set ::${jobid}(-dbquery) ]
     } else {
        set dbquery [ list ]
     }
     
     ;## send a short-circuit reply if the args are
     ;## useless
     if { [ string length $dll ] < 3 } {
        if { ! [ regexp -nocase {(wrapper|mpi)} $dctarg ] } {
           set msg "NOTE: datacond API target not wrapper. "
           append msg "No MPI processing will occur."
           return [ list 0 $msg 0 ]
        } 
        return [ list 3 "no -dynlib option provided" error! ]
     }
     if { ! [ regexp -- {\S{3,}} ${algos}${if}${dbquery} ] } {
        set msg "NOTE: No input data available for mpi/wrapper "
        append msg "processing.  No MPI processing will occur."
        return [ list 0 $msg 0 ]
     }
     
     ;## process request
     if { [ catch {
        
        set msg [ mpi::newJob $jobid low ]
        if { [ string length $msg ] && \
           ! [ llength $msg ] == 3 } {
           return -code error $msg
        }
        
      } err ] } {
         ;## send a short-circuit reply to the mgr on a
         ;## fatal error
         return [ list 3 "$jobid: $err" error! ]
      }
      return $msg
}
## ********************************************************

;#barecode
set msg [ runMpiJob $jobid ]
unset ::$jobid
if { [ string length $msg ] > 3 } {
   return $msg
 }

