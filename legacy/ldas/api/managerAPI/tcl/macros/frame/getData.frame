## ********************************************************
##
## Name: getData.frame
##
## Description:
## Returns frames or reduced frames as binary frame data
## or as ilwd text data by ftp, e-mail, or direct reply
## to a listening socket of the type defined in the
## User API Specification (in process).
## 
## Parameters:
## returnformat - "frame" for binary, "ilwd" for text.
## returnprotocol - ftp, http, mail, file or port, as available.
## times - a list of gps times, or "now" or "next".
## ifos - a list of interferometers which supply conforming data.
## dataquery - see usage section.
##
## Usage:
##   The -dataquery option:
##       Arguments to the -dataquery option consist of a
##       Tcl list of paired elements each consisting likewise
##       of a Tcl list.  The second (and all other even
##       ordered elements) element may be a null entry of the
##       form "{}".
##
##       The first and all other odd ordered elements are
##       classed as "channel" entries, and can consist of
##       any of the following types:
##
##          all - complete copy or dump of the entire frame.
##                all other arguments to the option will be
##                discarded.
##    
##    FrAdcData - adcdata(chanName1 chanName2 ..)
##                adcdata(N N-N ..) (N's are integers)
##                adcdata(all)
##                channel indices and names CAN be mixed.
##
##    FrSerData - serdata(chanName1 chanName2 ..)
##                serdata(N N-N ..) (N's are integers)
##                serdata(all)
##
##    FrHistory - history  (?)
##
##   FrDetector - detector (?)
##
##       The second and all other even ordered elements are
##       classed as "attributes" and can consist of the
##       keyword "all" or a list of attributes appropriate
##       to the "channel" type with which they are paired.
##       If these elements are nulls of the form "{}", "all"
##       will be assumed to have been intended.
##
##   Simplest example:
##                       -dataquery { all {} }
##   will produce a dump.
##
## Comments:
##
## ******************************************************** 

## ******************************************************** 
##
## Name: getFrameData 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc getFrameData { jobid } {

     set of    [ set ::${jobid}(-outputformat)   ]
     set rp    [ set ::${jobid}(-returnprotocol) ]
     set targ  [ set ::${jobid}(-targetapi)      ]
     set ftarg [ set ::${jobid}(-frametarget)    ]
     set subj  [ set ::${jobid}(-subject)        ]
     set query [ set ::${jobid}(-framequery)     ]
     
     ;## to short circuit frame API
     if { [ string length [ join $query ] ] < 3 } {
        set msg "no -framequery option specified, no frame output created"
        set msg [ list 2 $msg 0 ]
        return $msg
     }
     
     if { ! [ regexp -nocase {frame} $of ] } {
        set of [ ::concat frame $of ]
        set ::${jobid}(-outputformat) $of
     }
     
     ;## -frametarget supercedes -targetapi, but we
     ;## can support back-compatibility by replacing
     ;## -frametarget with the non-default value of
     ;## -targetapi (since both default to datacond
     if { [ string equal datacond $ftarg ] } {
        if { ! [ string equal datacond $targ ] } {
           if { [ string length $targ ] } {
              set ::${jobid}(-frametarget) $targ
           }
        }
     }

     ;## same for meta macros where targets default to "{}"
     if { ! [ string length $ftarg ] && [ string length $targ ] } {
        set ::${jobid}(-frametarget) $targ
     }

     frame::newJob $jobid
}
## ******************************************************** 

;#barecode
set msg [ getFrameData $jobid ]
if { [ string length $msg ] } {
   unset ::$jobid
   return $msg
 }
