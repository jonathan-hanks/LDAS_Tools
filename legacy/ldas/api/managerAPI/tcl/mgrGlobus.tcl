## ******************************************************** 
##
## Name: mgrGlobus 
##
## Description:
## handler for use of globus sockets 
## for network 
##
## Usage:
##
## Comments:
##

;#barecode

package provide mgrGlobus 1.0

;## 
;## globus libraries loaded in LDAScntlmon.ini
;## namespace eval mgrGlobus 

namespace eval mgrGlobus {} {
	set server_cs($::TCLGLOBUS_HOST_PORT) $::GLOBUS_MANAGER_API_HOST:$::TCLGLOBUS_HOST_PORT
    set server_cs($::TCLGLOBUS_USER_PORT) $::GLOBUS_MANAGER_API_HOST:$::TCLGLOBUS_USER_PORT
}

;#end

## ******************************************************** 
##
## Name: mgrGlobus::readCallback
##
## Description:
## handler for data read off globus socket from client
##
## Usage:
## 
##
## Comments:

proc mgrGlobus::readCallback { user_parms handle result buffer data_desc } {

	if	{ [ catch {
    	addLogEntry "read callback $user_parms $handle $result buffer $buffer data_desc $data_desc" purple
        set port [ lindex $user_parms 0 ]
        
    	if	{ [ regexp -nocase -- {end of file} $result ] } {
        	mgrGlobus::readDone $handle $port
    	} elseif { ! [ string length $buffer ] } {
    		mgrGlobus::readDone $handle $port
        } else {
    		append ::mgrGlobus::dataRecv($port) $buffer
        	set len 0
        	catch { set len [ llength $::mgrGlobus::dataRecv($port) ] } err
            
            ;## properly formed ldas cmd
            
        	if	{ [ info exist len ] && $len == 3 } {
        		mgrGlobus::readDone $handle $port
        	} else {
        	;## read some more until all data is read in
        	;## now handle over the readPoll to the client namespace
  				lassign [ globus_xio_register_read $handle $::RawGlobus::buffersize \
    	 			$::RawGlobus::waitforbytes NULL \
    				[ list mgrGlobus::readCallback $user_parms ] ] result 
        	}
        }
   } err ] } {
   		addLogEntry $err 2
        RawGlobus::sendCmd $err $handle
        mgrGlobus:::readReset $port
   }
}

## ******************************************************** 
##
## Name: mgrGlobus::readRest
##
## Description:
## reset buffer for next read 
##
## Usage:
## 
##
## Comments:

;## reset buffer accumulating data to empty for the next read 
proc mgrGlobus:::readReset { port } {

	set ::mgrGlobus::dataRecv($port) ""

}

## ******************************************************** 
##
## Name: mgrGlobus::readDone
##
## Description:
## process data if all has been read in 
##
## Usage:
## 
##
## Comments:

;## accumulate data into local var and process if msg has been read
proc mgrGlobus::readDone { handle port } {

	if	{ [ string length $::mgrGlobus::dataRecv($port) ] } {
    	mgrGlobus::operator_callback $::mgrGlobus::dataRecv($port) $handle
     	mgrGlobus::readReset $port
    }
}

## ******************************************************** 
##
## Name: operator_callback
##
## Description:
## This is where the user meets the manager API.  The user
## must submit a properly formatted request, or will get
## nothing back at all. (During ALPHA release, a helpful
## message will get returned.
##
## Usage:
##
## Challenge/Response protocol:
##
##  The client sends the string md5protocol instead of the password
##  when the user command is sent:
##     
##     ... -password md5protocol ...
##
##  The manager returns a salt value:
##
##     md5salt 0384566195
##
##  The client returns the md5 hash of the password with the salt
##  appended to it (md5sum $password$salt):
## 
##
##     md5digest af2058b1b115a2aec77e76e07b85d031
##
##  The manager can then validate the username/password, and no
##  useful data for sniffing the password has been exchanged.
##
## Comments:
## Commands must be of the form:
## username "option list" "meta_command option_list"

proc mgrGlobus::operator_callback { cmd handle } {
     set seqpt {}
     set persist not_persistent

     ;## PR 1852 - mismatched braces caused the job to get stuck
     ;## in the manager.
     if { [ mgr::gotMismatchedBraces $cmd ] } {
        set msg \
           "0\n$::API operator: mismatched braces in input"
        RawGlobus::sendCmd $msg $handle
        RawGlobus::close $handle
        return {}
     }
     
     set cmd [ mgr::removeBadChars $cmd ]
     
     ;## martian addresses can pass cleartext passwords
     ;## Fix: this may not needed since globus is external
     # set martian [ mgr::martianAddress $cid ]
     
     if { [ regexp -nocase {MGRKEY} $cmd ] } {
        set msg \
           "0\n$::API operator: character sequence MGRKEY reserved"
        RawGlobus::sendCmd $msg $handle
        RawGlobus::close $handle  
        return {}
     }
     
     if { [ catch { llength $cmd } err ] || \
        ! [ string length $cmd ] } {
        set msg \
           "0\n$::API operator: null or malformed command received"
        RawGlobus::sendCmd $msg $handle
        RawGlobus::close $handle 
        return {}
     }
     
     ;## September 2002 we reject plaintext passwords from all
     ;## but martian addresses
     ;## this may not be needed
     # if { $::REJECT_CLEARTEXT_PASSWORDS } {
     #   set md5_rx {([0-9a-f]{32}|md5protocol)}
     #   if { [ regexp -- {-password\s+(\S+)} $cmd -> tmp ] } {
     #      if { ! [ regexp -nocase $md5_rx $tmp ] && ! $martian } {
     #         set msg \
     #         "0\n$::API operator: cleartext passwords forbidden!"
     #         mgr::reply $cid $msg
     #         catch { ::close $cid }
     #         return {}
     #      }
     #      unset tmp 
     #   }
     #}
     
     set tmp $cmd
     regsub -- "^$::MGRKEY " $tmp "::MGRKEY " tmp
     regsub -all -- "(\[\\s\\'\])$::MGRKEY " $tmp {\1::MGRKEY } tmp
     #regsub -all -- {-password\s+\S+}  $tmp {-password *****}  tmp
     #regsub -all -- {-md5digest\s+\S+} $tmp {-md5digest *****} tmp
     #regsub -all -- {-md5salt\s+\S+}   $tmp {-md5salt *****}   tmp
     
     ;## cmd::md5Unpack will also set salt and digest via uplevel
     #set cmd [ cmd::md5Unpack $cmd ]
     
     if { [ llength $cmd ] != 3 } {
        set msg    "0\n"
        append msg "$::API operator: malformed command recieved!\n"
        append msg "Command format: USER_KEY USERINFO command_string"
        RawGlobus::sendCmd $msg $handle
        RawGlobus::close $handle 
        addLogEntry "cmd: '$tmp'" orange
        return {}
     }
     
     ;## first hurdle: is this an ldasJob?
     if { ! [ string match ldasJob [ lindex $cmd 0 ] ] } {
        set msg "0\n$::API operator: command received not an ldasJob"
        append msg ", or client disconnected prematurely."
        RawGlobus::sendCmd $msg $handle
        RawGlobus::close $handle 
        addLogEntry "cmd: '$tmp'" orange
        return {}
     }
     
     ;## were all three user info fields entered?
     if { [ llength [ lindex $cmd 1 ] ] != 6 } {
        set msg    "0\n"
        append msg "$::API operator: malformed user info recieved!\n"
        append msg "User info must include name, password, and e-mail"
        RawGlobus::sendCmd $msg $handle
        RawGlobus::close $handle 
        addLogEntry "cmd: '$tmp'" orange
        return {}
     }
    
     ;## really lame first hurdle for command.  Is it long
     ;## enough to BE a command?
     if { [ string length [ lindex $cmd 2 ] ] < 7 } {
        if { [ regexp {^[nN]ull$} [ lindex $cmd 2 ] ] } {
           catch { mgr::userInfo \
              [ concat [ lindex $cmd 1 ] -jobid NULL ] } msg
           regsub -all -- {-password \S+} $msg {-password *****} msg
           RawGlobus::sendCmd $msg $handle
           RawGlobus::close $handle 
        } else {
           set msg    "0\n"
           append msg "$::API operator: malformed command recieved!\n"
           append msg "Command \"[ lindex $cmd 2 ]\" invalid."
           RawGlobus::sendCmd $msg $handle
           RawGlobus::close $handle 
           addLogEntry "cmd: '$tmp'" orange
        }
        return {}
     }
     
     if { [ catch {
        set color orange
        set username [ lindex [ lindex $cmd 1 ] 1 ]
        
        mgr::preValidate $cmd
        
        ;## persistent sockets will cause 'persist' to be set to
        ;## the socket i.d., otherwise it is set to 'not_detected'.
        foreach [ list client persist ] \
           [ mgrGlobus::detectPersistentCommSocket $handle $cmd ] { break }
        regsub -nocase -- {persistent_socket[0-9\:]*} $cmd $client cmd   

        ;## check user info against database
        set info [ mgrGlobus::userInfo [ lindex $cmd 1 ] ]

        ;## pick the command (3rd item) from the input
        set cmd [ lindex $cmd 2 ]
        regsub -all -- {\;} $cmd {|} cmd
                     
        ;## pre-validate user supplied options
        ;## Kent wants this done *before* we get the jobid 11/07/03
        mgr::validateopts $cmd $username
        
        ;## expand the command
        set data [ mgr::expandCmd $cmd ]
        regsub {\{} $data "\{ -persistentsocket $persist " data
       
        ;## new command for setting jobid 'late'.
        set jobid [ mgr::assignJobid $info ]
        
        set color red
        
        addLogEntry "cmd: '$tmp'" green
        
        ;## tack the current job id to the userinfo
        set userinfo [ concat [ lindex $cmd 1 ] -jobid $jobid ]
        
        ;## mary asked for userid to be tacked onto all commands
        regsub {\{} $data "\{ -userid $username " data

        ;## so we can reference the user command name
        set cmdname [ lindex $cmd 0 ]
        ;## mary asked for the meta name to be tacked on
        regsub {\{} $data "\{ -commandname $cmdname " data
        set ::QUEUE($jobid,cmd) $data
        set seqpt "createAssistant($jobid $cmdname):"
        createAssistant $jobid $cmdname $cmd
        ;## save handle for persistent sockets (namespace of assistant not present yet
        set ::${jobid}_globusHandle $handle
     } msg ] } { 
        addLogEntry "${seqpt}\n$msg" $color
     } else { 
        array set user $info
        set msg    "1\nYour job is running as: \"$jobid\"\n"
        append msg "you will be e-mailed at: \"$user(-email)\"\n"
        append msg "when your job is completed, with information\n"
        append msg "on how to retrieve your results.\n"
        append msg "($::LDAS_SYSTEM running LDAS version "
        append msg "$::LDAS_VERSION)\n"
        unset user
     }
     
     addLogEntry "$jobid for $username at $client" blue
     
     mgrGlobus::reply $handle $msg
     
     ;## NOW we want to keep the socket around if the comm
     ;## protocol is `port' and the port number given is the
     ;## same as the one collected via peerinfo. PR #2749
     
     if { [ string equal not_persistent $persist ] } {
        RawGlobus::close $handle
     }
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mgr::detectPersistentCommSocket
##
## Description:
## Detect a request for a persistent socket with a client.
##
## When the current connection matches the -email option
## the connection will be handled 'connectionlessly', that
## is, as a persistent connection.
##
## This is potentially unreliable, but required by strict
## client firewall rules.
##
## Parameters:
##
## Usage:
##
## Comments:
## Per PR #2749

proc mgrGlobus::detectPersistentCommSocket { handle cmd } {
     
     if { [ catch { 
        set nat_rx {-email\s+persistent_socket} 
        ;## peerinfo will be a 3 element list of I.P. address,
        ;## canonical name, and remote port number.
        
        lassign [ globus_xio_handle_cntl \
              $handle \
              $::RawGlobus::tcpdriver \
              $::GLOBUS_XIO_TCP_GET_REMOTE_CONTACT \
             ] status client
        set client_rx "-email\\s+$client\[\\s\\\}\]+"
        
        if { [ regexp -nocase -- $client_rx $cmd ] } {
           set socketid $handle
        } elseif { [ regexp -nocase -- $nat_rx $cmd ] } {
           set socketid $handle
        } else {
           set socketid not_persistent 
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     
     return [ list $client $socketid ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mgrGlobus::userInfo 
##
## Description:
## Update user info stored in QUEUE(USERS).
## all ldasJob requests must have a user info argument as
## their first argument, and it must inlude the username,
## password, and the user's e-mail address.
## Usage:
##
## Comments:
## There will always be at least the userkey and a new
## job id.  New user info is optional
##
## format of a user in ::QUEUE(USERS)
## -name cgiuser -password <md5sum>
## -email blackburn_k@ligo.caltech.edu -fullname {JKent KBlackburn} 
## -phone 1-626-395-3185 -expires 2035-12-31 {}
## delete user when done ??

proc mgrGlobus::userInfo { args } {
     set return_info [ list ]
     set scratch     [ list ]
     set now [ clock seconds ]
     
     if { [ catch {
        if { [ catch { 
           array set input $args
        } ] } {
           eval array set input $args
        }  
 
        foreach user $::QUEUE(USERS) { 
           if { ! [ llength $user ] } { continue }
           if { [ llength $user ] != 13 } {
              mgr::reportCorruptedQueue $user
           }
           set info [ lrange $user 0 end-1 ]
           array set q $info
           if { [ string equal $q(-name) $input(-name) ] } {
           		set found 1
                break
           } 
        }
        
        if	{ ! [ info exist found ] } {
        	set newuser [ list -name $input(-name) -password $input(-password) -email \
            	$input(-email) -fullname $input(-name) -phone 1-800-123-4567 -expires 2035-12-31 {} ]
            lappend ::QUEUE(USERS) $newuser
            puts "new user [ lindex $::QUEUE(USERS) end ]"
        }
        
        foreach user $::QUEUE(USERS) { 

           ;## each user entry contains the three user info item
           ;## pairs and a list of the last N jobs processed for
           ;## that user.
           set info [ lrange $user 0 end-1 ]
           set jobs [ lindex $user end ]
        
           array set q $info
 
           if { [ string equal $q(-name) $input(-name) ] } {
          
              ;## here we mangle the args so that the user password
              ;## is not updated when the other user data is updated!
              regsub -- $input(-password) $args $q(-password) args
           
		      set info [ expandOpts info ]
              set return_info $info
              if { [ llength $return_info ] < 12 } {
                 set msg    "QUEUE(USERS) has been corrupted!\n"
                 append msg "Please check the user info file for bad\n"
                 append msg "entries, missing \"'s, etc."
                 error $msg
              }
           } 
           lappend scratch [ concat $info [ list $jobs ] ]
        } ;## end of foreach
     
        if { [ llength $return_info ] } {
           set ::QUEUE(USERS) $scratch
 
           if { ! [ regexp "\[a-z\]+" $::QUEUE(USERS) ] } {
              set msg    "The QUEUE(USERS) queue has been\n"
              append msg "initialised, but is empty."
              return -code error $msg
           }
        }  else {
           error "unknown user: '$input(-name)'"
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $return_info    
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mgrGlobus::deleteUserFromUsersQueue 
##
## Description:
## Remove a user from the queue if there are no jobs outstanding
##
## Usage:
##
## Comments:

proc mgrGlobus::deleteUserFromUsersQueue { user } {

     if { [ catch {
 		set index 0
        foreach user $::QUEUE(USERS) {
           incr index 1 
           if { ! [ llength $user ] } { continue }
           if { [ llength $user ] != 13 } {
              mgr::reportCorruptedQueue $user
           }
        
           ;## each user entry contains the three user info item
           ;## pairs and a list of the last N jobs processed for
           ;## that user.
           set info [ lrange $user 0 end-1 ]
           set jobs [ lindex $user end ]
        
           array set q $info
 
           if { [ string equal $q(-name) $user ] } {
          
              if	{ [ llength $jobs ] } {
              		set delete 0
              } else {
              		set delete 1
              }
              break
           }
           
        } ;## end of foreach
     
        if 	{ [ info exist delete ] } {
        	if	{ $delete } {
            	set ::QUEUE(USERS) [ lreplace $::QUEUE(USERS) $index $index ]
                addLogEntry "$user removed from users queue" blue
            }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     } 
}

## ******************************************************** 
##
## Name: mgrGlobus::reply
##
## Description:
## Just a slightly smart catter, tries to avoid actually
## sending junk back.
##
## Usage:
##            mgr::reply $cid $msg
##
## Comments:
## This channel should NEVER be flushed!!

proc mgrGlobus::reply { { cid "" } { args "" } } {

     set cid [ string trim $cid ]
     if { ! [ regexp {_globus_xio_handle_t$} $cid ] } {
        	set msg    "mgrGlobus::reply:\n"
        	append msg "received: '$cid $args'\n"
        	append msg "First argument must be channel i.d."
        	addLogEntry $msg 1
     }
     if { [ string length $args ] > 4 } {
        if { [ catch {
           RawGlobus::sendCmd $args $cid
        } err ] } {
           catch { RawGlobus::close $cid }
           set msg     "mgrGlobus::reply:\n"
           lappend msg "$err\n"
           lappend msg "Lost contact with my caller!"
           addLogEntry $msg 2
        }
     }
     return {}
}
## ******************************************************** 
	
mgrGlobus::readReset $::TCLGLOBUS_HOST_PORT
mgrGlobus::readReset $::TCLGLOBUS_USER_PORT
