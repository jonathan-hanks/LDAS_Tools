## ******************************************************** 
##
## Name: mgrGlobusChannel 
##
## Description:
## handler for use of globus sockets 
## for network 
##
## Usage:
##
## Comments:
##

;#barecode

package provide mgrGlobusChannel 1.0

;## 
;## globus libraries loaded in LDAScntlmon.ini
;## namespace eval mgrGlobusChannel 

namespace eval mgrGlobusChannel {}

if { [ info exist ::GLOBUS_LOCATION ] } {
    set env(GLOBUS_LOCATION) $::GLOBUS_LOCATION
}

;#end
## ******************************************************** 
##
## Name: mgr::GlobusChannelAccept
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc mgrGlobusChannel::accept { newsock addr port } {

	if	{ [ catch {
    	if	{ [ info exist ::userSid($newsock) ] } {
        	error "$newsock is already assigned to [ set ::userSid($newsock) ]"
        }
        fconfigure $newsock -buffering full -blocking 0
        fconfigure $newsock -translation binary -encoding binary
        fileevent $newsock readable [ list mgrGlobusChannel::readChannel $newsock ]
    } err ] } {
    	addLogEntry $err 2 
    }
}

## ******************************************************** 
##
## Name: mgr:GlobusChannel::init
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc mgrGlobusChannel::init { host port type } {

	if	{ [ catch {
    	set mode "gsi service"
    	set cmd "gt_xio_socket -server mgrGlobusChannel::accept"
    	if	{ [ info exist ::GSI_AUTH_ENABLED ] } {
        	if	{ [ string equal "-gsi_auth_enabled" $::GSI_AUTH_ENABLED ] } {
        		append cmd " $::GSI_AUTH_ENABLED"
            } else {
            	set mode "non-gsi service"
            }
        } 
        if	{ [ info exist ::GRIDMAP_ENABLED ] } {
        	if	{ $::GRIDMAP_ENABLED } {
        		append cmd " -gridmap_file"
            } 
        } 
        append cmd " -myaddr $host $port"
        addLogEntry "cmd $cmd" purple
        
    	#set ::mgrGlobusChannel::Globus${type}ListenSocket [ gt_xio_socket -server \
        #	mgrGlobusChannel::accept $::GSI_AUTH_ENABLED -myaddr $host $port ]
        
        set ::mgrGlobusChannel::Globus${type}ListenSocket [ eval $cmd ]
        
        addLogEntry "Globus channel [ set ::mgrGlobusChannel::Globus${type}ListenSocket ] opened for $mode at $port" blue
    } err ] } {
        return -code error $err 
    }
}

## ******************************************************** 
##
## Name: operator_callback
##
## Description:
## This is where the user meets the manager API.  The user
## must submit a properly formatted request, or will get
## nothing back at all. (During ALPHA release, a helpful
## message will get returned.
##
## Usage:
##
## Challenge/Response protocol:
##
##  The client sends the string md5protocol instead of the password
##  when the user command is sent:
##     
##     ... -password md5protocol ...
##
##  The manager returns a salt value:
##
##     md5salt 0384566195
##
##  The client returns the md5 hash of the password with the salt
##  appended to it (md5sum $password$salt):
## 
##
##     md5digest af2058b1b115a2aec77e76e07b85d031
##
##  The manager can then validate the username/password, and no
##  useful data for sniffing the password has been exchanged.
##
## Comments:
## Commands must be of the form:
## username "option list" "meta_command option_list"

proc mgrGlobusChannel::operator_callback { cmd sid } {
     set seqpt {}
     set persist not_persistent

     ;## PR 1852 - mismatched braces caused the job to get stuck
     ;## in the manager.
     if { [ mgr::gotMismatchedBraces $cmd ] } {
        set msg \
           "0\n$::API operator: mismatched braces in input"
        puts $sid $msg
        flush $sid
        addLogEntry "$sid closed $msg" purple
        close $sid
        return {}
     }
     
     set cmd [ mgr::removeBadChars $cmd ]
     
     ;## martian addresses can pass cleartext passwords
     ;## Fix: this may not needed since globus is external
     # set martian [ mgr::martianAddress $cid ]
     
     if { [ regexp -nocase {MGRKEY} $cmd ] } {
        set msg \
           "0\n$::API operator: character sequence MGRKEY reserved"
        puts $sid $msg
        flush $sid
        close $sid
        addLogEntry "$sid closed $msg" purple
        return {}
     }
     
     if { [ catch { llength $cmd } err ] || \
        ! [ string length $cmd ] } {
        set msg \
           "0\n$::API operator: null or malformed command received"
        puts $sid $msg
        flush $sid
        close $sid 
        addLogEntry "$sid closed $msg" purple
        return {}
     }
     
     ;## September 2002 we reject plaintext passwords from all
     ;## but martian addresses
     ;## this may not be needed
     # if { $::REJECT_CLEARTEXT_PASSWORDS } {
     #   set md5_rx {([0-9a-f]{32}|md5protocol)}
     #   if { [ regexp -- {-password\s+(\S+)} $cmd -> tmp ] } {
     #      if { ! [ regexp -nocase $md5_rx $tmp ] && ! $martian } {
     #         set msg \
     #         "0\n$::API operator: cleartext passwords forbidden!"
     #         mgr::reply $cid $msg
     #         catch { ::close $cid }
     #         return {}
     #      }
     #      unset tmp 
     #   }
     #}
     
     set tmp $cmd
     regsub -- "^$::MGRKEY " $tmp "::MGRKEY " tmp
     regsub -all -- "(\[\\s\\'\])$::MGRKEY " $tmp {\1::MGRKEY } tmp
     #regsub -all -- {-password\s+\S+}  $tmp {-password *****}  tmp
     #regsub -all -- {-md5digest\s+\S+} $tmp {-md5digest *****} tmp
     #regsub -all -- {-md5salt\s+\S+}   $tmp {-md5salt *****}   tmp
     
     ;## cmd::md5Unpack will also set salt and digest via uplevel
     #set cmd [ cmd::md5Unpack $cmd ]
     
     if { [ llength $cmd ] != 3 } {
        set msg    "0\n"
        append msg "$::API operator: malformed command received!\n"
        append msg "Command format: USER_KEY USERINFO command_string"
        puts $sid $msg
        flush $sid
        close $sid   
        addLogEntry "cmd: '$tmp'" orange
        return {}
     }
     
     ;## first hurdle: is this an ldasJob?
     if { ! [ string match ldasJob [ lindex $cmd 0 ] ] } {
        set msg "0\n$::API operator: command received not an ldasJob"
        append msg ", or client disconnected prematurely."
        puts $sid $msg
        flush $sid
        close $sid   
        addLogEntry "cmd: '$tmp'" orange
        return {}
     }
     
     ;## were all three user info fields entered?
     if { [ llength [ lindex $cmd 1 ] ] != 6 } {
        set msg    "0\n"
        append msg "$::API operator: malformed user info recieved!\n"
        append msg "User info must include name, password, and e-mail"
        puts $sid $msg
        flush $sid
        close $sid   
        addLogEntry "cmd: '$tmp'" orange
        return {}
     }
    
     ;## really lame first hurdle for command.  Is it long
     ;## enough to BE a command?
     if { [ string length [ lindex $cmd 2 ] ] < 7 } {
        if { [ regexp {^[nN]ull$} [ lindex $cmd 2 ] ] } {
           catch { mgr::userInfo \
              [ concat [ lindex $cmd 1 ] -jobid NULL ] } msg
           regsub -all -- {-password \S+} $msg {-password *****} msg
           puts $sid $msg
           flush $sid
           close $sid  
        } else {
           set msg    "0\n"
           append msg "$::API operator: malformed command received!\n"
           append msg "Command \"[ lindex $cmd 2 ]\" invalid."
           puts $sid $msg
           flush $sid
           close $sid  
           addLogEntry "cmd: '$tmp'" orange
        }
        return {}
     }
     
     if { [ catch {
        set color orange
        set username [ lindex [ lindex $cmd 1 ] 1 ]
        
        mgr::preValidate $cmd
        
        ;## persistent sockets will cause 'persist' to be set to
        ;## the socket i.d., otherwise it is set to 'not_detected'.
        foreach [ list client persist ] \
           [ mgr::detectPersistentCommSocket $sid $cmd ] { break }
        # addLogEntry "client $client persist $persist" purple
        
        regsub -nocase -- {persistent_socket[0-9\:]*} $cmd $client cmd   

		;## get subject name from channel
        set thisname ""
        set subjectName [ fconfigure $sid -x509_subj_name ]
        if	{ [ regexp  {CN=([^\d]+)[^\n]+[\n\s]*(\d+)} $subjectName -> thisname ] } {
            regsub -all {\s+} $thisname "_" thisname
            set name [ string trim $thisname _ ]
                
        }            
        addLogEntry "client $client persist $persist subject name $thisname" purple
        
        ;## check user info against database
        set info [ mgrGlobusChannel::userInfo [ lindex $cmd 1 ] ]

        ;## pick the command (3rd item) from the input
        set cmd [ lindex $cmd 2 ]
        regsub -all -- {\;} $cmd {|} cmd
                     
        ;## pre-validate user supplied options
        ;## Kent wants this done *before* we get the jobid 11/07/03
        mgr::validateopts $cmd $username
        
        ;## expand the command
        set data [ mgr::expandCmd $cmd ]
        regsub {\{} $data "\{ -persistentsocket $persist " data
       
        ;## new command for setting jobid 'late'.
        set jobid [ mgr::assignJobid $info ]
        
        set color red
        
        # addLogEntry "$client $sid $persist cmd: '$tmp'" green
        
        ;## tack the current job id to the userinfo
        set userinfo [ concat [ lindex $cmd 1 ] -jobid $jobid ]
        
        ;## mary asked for userid to be tacked onto all commands
        regsub {\{} $data "\{ -userid $username " data

        ;## so we can reference the user command name
        set cmdname [ lindex $cmd 0 ]
        ;## mary asked for the meta name to be tacked on
        regsub {\{} $data "\{ -commandname $cmdname " data
        set ::QUEUE($jobid,cmd) $data
        set seqpt "createAssistant($jobid $cmdname):"
        createAssistant $jobid $cmdname $cmd
     } msg ] } { 
        addLogEntry "${seqpt}\n$msg" $color
        set closeSocket 1
     } else { 
        array set user $info
        set msg    "1\nYour job is running as: \"$jobid\"\n"
        append msg "you will be e-mailed at: \"$user(-email)\"\n"
        append msg "when your job is completed, with information\n"
        append msg "on how to retrieve your results.\n"
        append msg "($::LDAS_SYSTEM running LDAS version "
        append msg "$::LDAS_VERSION)\n"
        unset user
     }
     if	{ [ info exist jobid ] } {
     	set ::userSid($sid) $jobid
     	set ::${jobid}(userSid) $sid
     	addLogEntry "$sid $username at $client cmd: '$tmp'" blue
     }
     mgr::reply $sid $msg
          
     ;## NOW we want to keep the socket around if the comm
     ;## protocol is `port' and the port number given is the
     ;## same as the one collected via peerinfo. PR #2749
     
     if { [ string equal not_persistent $persist ] || \
     	  [ info exist closeSocket ] } {
     	addLogEntry "closing non-persistent socket $sid" purple
        close $sid
        catch { mgrGlobus::deleteUserFromUsersQueue $username } 
     }
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mgrGlobusChannel::userInfo 
##
## Description:
## Update user info stored in QUEUE(USERS).
## all ldasJob requests must have a user info argument as
## their first argument, and it must inlude the username,
## password, and the user's e-mail address.
## Usage:
##
## Comments:
## There will always be at least the userkey and a new
## job id.  New user info is optional
##
## format of a user in ::QUEUE(USERS)
## -name cgiuser -password <md5sum>
## -email blackburn_k@ligo.caltech.edu -fullname {JKent KBlackburn} 
## -phone 1-626-395-3185 -expires 2035-12-31 {}
## delete user when done ??

proc mgrGlobusChannel::userInfo { args } {
     set return_info [ list ]
     set scratch     [ list ]
     set now [ clock seconds ]
     
     if { [ catch {
        if { [ catch { 
           array set input $args
        } ] } {
           eval array set input $args
        }  
 
        foreach user $::QUEUE(USERS) { 
           if { ! [ llength $user ] } { continue }
           if { [ llength $user ] != 13 } {
              mgr::reportCorruptedQueue $user
           }
           set info [ lrange $user 0 end-1 ]
           array set q $info
           if { [ string equal $q(-name) $input(-name) ] } {
           		set found 1
                break
           } 
        }
        
        if	{ ! [ info exist found ] } {
        	set newuser [ list -name $input(-name) -password $input(-password) -email \
            	$input(-email) -fullname $input(-name) -phone 1-800-123-4567 -expires 2035-12-31 {} ]
            lappend ::QUEUE(USERS) $newuser
        }
        
        foreach user $::QUEUE(USERS) { 

           ;## each user entry contains the three user info item
           ;## pairs and a list of the last N jobs processed for
           ;## that user.
           set info [ lrange $user 0 end-1 ]
           set jobs [ lindex $user end ]
        
           array set q $info
 
           if { [ string equal $q(-name) $input(-name) ] } {
          
              ;## here we mangle the args so that the user password
              ;## is not updated when the other user data is updated!
              regsub -- $input(-password) $args $q(-password) args
           
		      set info [ expandOpts info ]
              set return_info $info
              if { [ llength $return_info ] < 12 } {
                 set msg    "QUEUE(USERS) has been corrupted!\n"
                 append msg "Please check the user info file for bad\n"
                 append msg "entries, missing \"'s, etc."
                 error $msg
              }
           } 
           lappend scratch [ concat $info [ list $jobs ] ]
        } ;## end of foreach
     
        if { [ llength $return_info ] } {
           set ::QUEUE(USERS) $scratch
 
           if { ! [ regexp "\[a-z\]+" $::QUEUE(USERS) ] } {
              set msg    "The QUEUE(USERS) queue has been\n"
              append msg "initialised, but is empty."
              return -code error $msg
           }
        }  else {
           error "unknown user: '$input(-name)'"
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $return_info    
}
## ******************************************************** 

## ******************************************************** 
##
## Name: mgrGlobusChannel::deleteUserFromUsersQueue 
##
## Description:
## Remove a user from the queue if there are no jobs outstanding
##
## Usage:
##
## Comments:

proc mgrGlobusChannel::deleteUserFromUsersQueue { user } {

     if { [ catch {
 		set index 0
        foreach user $::QUEUE(USERS) {
           incr index 1 
           if { ! [ llength $user ] } { continue }
           if { [ llength $user ] != 13 } {
              mgr::reportCorruptedQueue $user
           }
        
           ;## each user entry contains the three user info item
           ;## pairs and a list of the last N jobs processed for
           ;## that user.
           set info [ lrange $user 0 end-1 ]
           set jobs [ lindex $user end ]
        
           array set q $info
 
           if { [ string equal $q(-name) $user ] } {
          
              if	{ [ llength $jobs ] } {
              		set delete 0
              } else {
              		set delete 1
              }
              break
           }
           
        } ;## end of foreach
     
        if 	{ [ info exist delete ] } {
        	if	{ $delete } {
            	set ::QUEUE(USERS) [ lreplace $::QUEUE(USERS) $index $index ]
                addLogEntry "$user removed from users queue" blue
            }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     } 
}


## ******************************************************** 
##
## Name: mgrGlobusChannel::read
##
## Description:
## Remove a user from the queue if there are no jobs outstanding
##
## Usage:
##
## Comments

proc mgrGlobusChannel::readChannel { sid } {

		if	{ [ catch {
        	if	{ [ eof $sid ] } {
            	set eof 1
            	error "$sid end of file"
  	 		}
			set bufdata [ ::read $sid ]
        	if	{ [ string length $bufdata ] }  {
            	# addLogEntry "bufdata $bufdata" purple
                mgrGlobusChannel::operator_callback $bufdata $sid
            }
        } err ] } {
        	catch { close $sid } err1
            addLogEntry "$err $sid closed $err1" purple
        	if	{ ! [ info exist eof ] } {
   				#addLogEntry "$err; closing globus channel $sid" 2
        		# catch { puts $sid "nocallback\n0\n3\n$err\n" }
           	}
   	    }
}
