#include "../../src/config.h"

#include <cstring>
#include <iostream>
#include <fstream>

#include "lwdocument.hh"   

void
create_dtd_file( const char* Filename )
{
  std::ofstream dtd_file( Filename );
   
  if( !dtd_file )
  {
    std::cerr << "Cannot open " << Filename << " file for writing."
	      << std::endl;
    return;
  }
   
  const char* const DTD( LWDocument::getDTD() );
   
  dtd_file.write( DTD, strlen( DTD ) );
    
  dtd_file.close();
}
   

int main()
{
  create_dtd_file( "ligolw_dtd.txt" );
  create_dtd_file( "ligo_lw.dtd" );
  return 0;
}
