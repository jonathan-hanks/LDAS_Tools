#include "config.h"

#include <sys/types.h>

// System Header Files
#include <new>

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
using ILwd::LdasContainer;
using ILwd::LdasElement;  

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// Local Header Files
#include "lwligolw.hh"
#include "lwcomment.hh"
#include "lwarray.hh"
#include "lwtable.hh"
#include "lwparam.hh"
#include "lwdim.hh"
#include "lwframe.hh"
#include "lwadcdata.hh"
#include "lwtime.hh"
#include "lwelement.hh"
#include "lwdetector.hh"
#include "lwadcinterval.hh"   
#include "query.hh"
#include "lwutil.hh"
#include "util.hh"

using namespace std;
using namespace XERCES_CPP_NAMESPACE;
   
   
static LwDataHash& initLigoLwHash()
{
   static LwDataHash h;
   
   h[ LW_LIGO_LW_ID     ] = ligoLw2ILwd;
   h[ LW_ARRAY_ID       ] = array2ILwd;
   h[ LW_TABLE_ID       ] = table2ILwd;   
   h[ LW_PARAM_ID       ] = param2ILwd;
   h[ LW_COMMENT_ID     ] = comment2ILwd;
   h[ LW_DIM_ID         ] = dim2ILwd;
   h[ LW_FRAME_ID       ] = lwFrame2ILwd;
   h[ LW_ADCDATA_ID     ] = lwAdcData2ILwd;
   h[ LW_ADCINTERVAL_ID ] = lwAdcInterval2ILwd;   
   h[ LW_TIME_ID        ] = time2ILwd;
   h[ LW_DETECTOR_ID    ] = lwDetector2ILwd;
   
   return h;
}


static LwDataHash& ligoLwHash = initLigoLwHash( );
static const char* xml_name = ":LIGO_LW:XML";


//-----------------------------------------------------------------------
//
//: Convert LIGO_LW element into ILWD format container.
//
//!param: node - LIGO_LW element to convert.
//!param: dq - Tokenized command.
//
//!return: ILWD container element.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: SwigException
//   
unique_ptr< LdasElement > ligoLw2ILwd( const DOMNode* const node,
   std::deque< Query > dq )
{
   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   
   if( name.find( xml_name ) == string::npos )
   {
      string type( nodeAttribute( node, LIGO_LW::typeAttName ) );
      if( type.empty() == false )
      {
         name += ':';
         name += type;
      }
      name.append( xml_name );
   }

   unique_ptr< LdasContainer > c( new LdasContainer( name ) );
   

   DOMNode* child( node->getFirstChild() );
   LwType type;
   
   while( child != 0) 
   {
      if ( child->getNodeType() == DOMNode::ELEMENT_NODE )
      {
         type = getLwType( child );      

         if( ligoLwHash.find( type ) != ligoLwHash.end() )
         {
            c->push_back( ligoLwHash[ type ]( child, dq ).release(),
                          LdasContainer::NO_ALLOCATE,
                          LdasContainer::OWN );
         }
         else if( type != LW_STREAM_ID )
         {
            ostringstream s;
            s << "ligolw_unsupported_element_type: " 
              << type;
   
            throw SWIGEXCEPTION( s.str() );
         }
      }
   
      child = child->getNextSibling();
   }
   
   return unique_ptr< LdasElement >( c.release() );
}
