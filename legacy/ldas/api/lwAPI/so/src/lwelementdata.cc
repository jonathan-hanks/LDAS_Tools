#include "config.h"

// System Header Files
#include <algorithm>

#include "general/Memory.hh"

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// ILWD Header Files   
#include <ilwd/ldaselement.hh>   
using ILwd::LdasElement;   
   
// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// Local Header Files
#include "lwligolw.hh"
#include "lwtable.hh"
#include "lwarray.hh"
#include "lwparam.hh"
#include "lwcomment.hh"
#include "lwcolumn.hh"
#include "lwdim.hh"
#include "lwframe.hh"
#include "lwadcdata.hh"
#include "lwdetector.hh"
#include "lwtime.hh"
#include "getattribute.hh"
#include "lwelementdata.hh"
#include "lwgetchild.hh"
#include "query.hh"
#include "vect.hh"
#include "lwadcinterval.hh"   
   
using namespace std;
using namespace XERCES_CPP_NAMESPACE;
   
   
//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert LIGO_LW element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert LIGO_LW element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLigoLwData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "ligo_lw" );
      e.insert( "table" );
      e.insert( "array" );
      e.insert( "igwdframe" );
      e.insert( "adcdata" );
      e.insert( "param" );
      e.insert( "comment" );
      e.insert( "time" );
      e.insert( "adcinterval" );
      e.insert( "full" );
   }


   static convFP c( ligoLw2ILwd );   
   static Data ligolwData( e, c );
   return ligolwData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Array element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Array element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwArrayData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "dim" );
   }

   static convFP c( array2ILwd );
   static Data arrayData( e, c );
   return arrayData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Table element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Table element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwTableData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "comment" );
      e.insert( "column" );
   }

   static convFP c( table2ILwd );
   static Data tableData( e, c );
   return tableData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Dim element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Dim element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwDimData()
{
   static ElementSet e;

   static convFP c( dim2ILwd );
   static Data dimData( e, c );
   return dimData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Param element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Param element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwParamData()
{
   static ElementSet e;

   static convFP c( param2ILwd );
   static Data paramData( e, c );
   return paramData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Column element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Column element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwColumnData()
{
   static ElementSet e;

   static convFP c( column2ILwd );
   static Data columnData( e, c );
   return columnData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Comment element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Comment element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwCommentData()
{
   static ElementSet e;

   static convFP c( comment2ILwd );
   static Data commentData( e, c );
   return commentData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Frame header element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Frame header element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwFrameData()
{
   static ElementSet e;
   
   if( e.empty() )
   {
      e.insert( "ligo_lw" );
      e.insert( "param" );
      e.insert( "comment" );
      e.insert( "igwdframe" );
      e.insert( "array" );      // should be time
      e.insert( "detector" );
      e.insert( "time" );
      e.insert( "adcdata" );
   }

   static convFP c( lwFrame2ILwd );
   static Data frameData( e, c );
   return frameData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert AdcData element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert AdcData element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwAdcDataData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "ligo_lw" );
      e.insert( "adcdata" );
      e.insert( "param" );
      e.insert( "time" );
      e.insert( "comment" );
   }

   static convFP c( lwAdcData2ILwd );
   static Data adcData( e, c );
   return adcData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Detector element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Detector element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwDetectorData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "ligo_lw" );
      e.insert( "param" );
      e.insert( "comment" );
   }

   static convFP c( lwDetector2ILwd );
   static Data detectorData( e, c );
   return detectorData;
}


//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert Time element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert Time element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwTimeData()
{
   static ElementSet e;

   static convFP c( time2ILwd );
   static Data timeData( e, c );
   return timeData;
}

   
//------------------------------------------------------------------------------
//
//: Initialize Data structure to convert AdcInterval element into ILWD format.
//
// This method initializes set and function pointer to have appropriate 
// information to convert AdcInterval element into ILWD format.
//
//!returns: Data - Initialized structure.
//
ElementData::Data& ElementData::initLwAdcIntervalData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "adcdata" );
      e.insert( "time" );
      e.insert( "comment" );
   }

   static convFP c( lwAdcInterval2ILwd );
   static Data adcIntervalData( e, c );
   return adcIntervalData;
}
   

//------------------------------------------------------------------------------
//
//: Initialize LwData unordered_map.
//
// This method initializes hash map used to convert LIGO_LW elements into 
// ILWD format. It associates hash maps with a different element types in
// LIGO_LW document.
//
//!returns: LwData - Hash map.
//
ElementData::LwData& ElementData::initAllData()
{
   static LwData h;
    
   if( h.empty() )
   {
      h[ "ligo_lw"     ] = dataPair( "LIGO_LW", initLigoLwData() );
      h[ "table"       ] = dataPair( "Table", initLwTableData() );
      h[ "array"       ] = dataPair( "Array", initLwArrayData() );
      h[ "dim"         ] = dataPair( "Dim", initLwDimData() );
      h[ "column"      ] = dataPair( "Column", initLwColumnData() );
      h[ "comment"     ] = dataPair( "Comment", initLwCommentData() );
      h[ "param"       ] = dataPair( "Param", initLwParamData() );
      h[ "igwdframe"   ] = dataPair( "IGWDFrame", initLwFrameData() );
      h[ "adcdata"     ] = dataPair( "AdcData", initLwAdcDataData() );
      h[ "detector"    ] = dataPair( "Detector", initLwDetectorData() );
      h[ "time"        ] = dataPair( "Time", initLwTimeData() );
      h[ "adcinterval" ] = dataPair( "AdcInterval", initLwAdcIntervalData() );   
   }

   return h;
}


ElementData::LwData& ElementData::mAllData = initAllData( );


//------------------------------------------------------------------------------
//
//: ( Default ) Constructor.
//   
ElementData::ElementData()
   : mData( initLigoLwData() )
{}
   
  
//------------------------------------------------------------------------------
// 
//: Constructor.
//
//!param: Data& a - Data structure to initialize with.
//   
ElementData::ElementData( Data& a )
   : mData( a )
{}
   
   
//------------------------------------------------------------------------------
//
//: Operator() for ElementData.
//
// This operator extracts data from specified LIGO_LW element.
//
//!param: node - LIGO_LW element to search for specified data.
//!param: dq - Tokenized command.
//!param: start - Vector to record element position in the document tree.
//!param: index - Current index into position vector.
//
//!return: LdasElement* - Pointer to ILWD format element.
//
//!exc: not_valid_element - Specified element is not valid.
//!exc: bad_query - Query is bad.
//!exc: ElementData_unknown_error - Unknown error( should never                 +
//!exc:    happen ).
//!exc: bad_alloc - Memory allocation failed.   
//
unique_ptr< LdasElement > ElementData::operator()( 
   DOMNode* const node, 
   std::deque<Query> dq,
   std::vector< size_t >& start, const size_t index ) 
{
    if ( node == 0 )
    {
       throw SWIGEXCEPTION( "not_valid_element" );
    }
   
    if ( dq.size() == 0 )
    {
      return mData.Convert( node, dq );
    }

    Query q = dq.front();
    dq.pop_front();
    const CHAR* hashIndex( q.getName().c_str() );

    // It's not a query
    if ( !q.isQuery() )
    {
       if( mData.Element.size() != 0 )
       {
          if( mData.Element.count( hashIndex ) == 1 )
          {
             if ( strcmp( hashIndex, "full" ) == 0 )
             {
                return mData.Convert( node, dq );          
             }       
   
             const vector< DOMNode* > nodeVector( getChildByTag( node, 
                                                     mAllData[ hashIndex ].first ) );
   
             // Set up new element set and conversion function
             ElementData e( mAllData[ hashIndex].second );
             return e.find( nodeVector, dq, start, index );
          }
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
    else
    {
       if( mData.Element.size() != 0 )
       {
          if( mData.Element.find( hashIndex ) != mData.Element.end() )
          {
             const vector< DOMNode* > nodeVector( getChildByTag( node, 
                                                     mAllData[ hashIndex ].first ) );

             DOMNode* const elem( getContained( nodeVector, q, start, index ) ); 

             ElementData e( mAllData[ hashIndex ].second );
             return e( elem, dq, start, index + 1 );
          }
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
    throw SWIGEXCEPTION( "ElementData_unknown_error" );
}


//------------------------------------------------------------------------------
//
//: Find method for ElementData.
//
// This method extracts specified data from the list of LIGO_LW elements.
//
//!param: nodeList - List of LIGO_LW elements to be searched for specified     +
//!param:    data.
//!param: dq - Tokenized command.
//!param: start - Vector to record element position in the document tree.
//!param: index - Current index into position vector.
//
//!return: ILWD format element.
//
//!exc: not_valid_element - Specified element is not valid.
//!exc: ElementData_unknown_error - Unknown error( should never                +
//!exc:    happen ).
//
unique_ptr< LdasElement > ElementData::find(
   const std::vector< DOMNode* >& nodeList, 
   std::deque< Query >& dq, 
   std::vector< size_t >& start, const size_t index )
{
   if ( nodeList.empty() )
   {
      throw SWIGEXCEPTION( "not_valid_element" );
   }
   
   if ( index == start.size() )
   {
      start.push_back( 0 );
   }

   INT_4U size( start.size() );
   
   // Flag to indicate that return value is "not_valid_element" 
   bool notValidRet( false );
   unique_ptr< LdasElement > c( 0 );

   
   for( vector< DOMNode* >::const_iterator iter = nodeList.begin(),
        end_iter = nodeList.end(); iter != end_iter; ++iter )
   {
      start[ index ] = iter - nodeList.begin();
      unwind( start, size );
   
      try
      {
         c.reset( operator()( *iter, dq, start, index + 1 ).release() );
      }
      catch ( SwigException& e )
      {
	 if ( e.getResult() == "not_valid_element" )
	 {
	    notValidRet = true;
	 }
         else
	 {
	    throw;
	 }
      }

      if( c.get() != 0 )
      {
         return unique_ptr< LdasElement >( c.release() );
      }
   }

   
   unwind( start, size );
   if ( notValidRet == true )
   {
     // If all of the elements didn't contain specified element
     throw SWIGEXCEPTION ( "not_valid_element" );
   }
   
   throw SWIGEXCEPTION( "ElementData_unknown_error" );
   return unique_ptr< LdasElement >( 0 );
}

