#ifndef LwPutDataHH
#define LwPutDataHH

// Local Header Files
#include "util.hh"

   
//------------------------------------------------------------------------------
//
//: ElementPutData functional.
//
// This recursive functional is designed for placing a raw data of a  specified 
// LIGO_LW element into external file.
//
class ElementPutData
{

private:

  bool find(
     std::vector< XERCES_CPP_NAMESPACE::DOMNode* >& nodeList, 
     std::deque< Query >& dq, 
     std::vector< size_t >& start, const size_t index );

   
  //------------------------------------------------------------------------------
  //: Data structure.
  // 
  // This structure is designed to keep an information for each of the 
  // LIGO_LW elements to place its raw data into external file. This structure
  // contains: Element is a set of allowed child elements for the current element.
  //
  struct putData 
  {
     ElementSet Element;

     putData(){}
     putData( const ElementSet& e ) : Element( e ) {}
     putData( const putData& a ) : Element( a.Element ){}
  };

  typedef struct putData PutData;
  typedef std::pair< const CHAR*, PutData > putDataPair;
  typedef General::unordered_map< std::string, putDataPair >
  LwPutData;

  static PutData& initLigoLwPutData();
  static PutData& initLwTablePutData();
  static PutData& initLwArrayPutData();
  static PutData& initLwFramePutData();
  static PutData& initLwAdcDataPutData();
  static PutData& initLwDetectorPutData();
  static PutData& initLwStreamPutData();
  static PutData& initLwAdcIntervalPutData();   
  static LwPutData& initAllPutData();

  PutData mData;
  static LwPutData& mAllPutData;

  // Url address of external file.
  static std::string mUrl;
  // Directory where to place external file.
  static std::string mDir;

public:

  /* ( Default ) Constructor. */
  ElementPutData();
  /* Constructors */
  ElementPutData( const std::string& url, const std::string& dir );
  ElementPutData( const PutData& a );
 
  //: Overloaded operator().
  //
  // This operator puts raw data of LIGO_LW element into external file.
  //
  //!param: const DOMNode* const node - A pointer to the LIGO_LW element containing
  //+       the data. 
  //!param: std::deque< Query > dq - Tokenized command.
  //!param: std::vector< size_t > start - Vector to record element position in the  
  //+       document tree.
  //!param: const size_t index - Current index into position vector.
  //
  //!return: const bool - True if data was placed in external file, false otherwise.
  //
  bool operator()( 
     XERCES_CPP_NAMESPACE::DOMNode* node, 
     std::deque< Query > dq,
     std::vector< size_t >& start, const size_t index );
};


#endif
