#ifndef GetAttributeHH
#define GetAttributeHH

// System Includes
#include <vector>

namespace XERCES_CPP_NAMESPACE  
{
   class DOMNode;
}   
   
class Query;


XERCES_CPP_NAMESPACE::DOMNode* getContained( 
   const std::vector< XERCES_CPP_NAMESPACE::DOMNode* >& nodeList, 
   Query& q, std::vector< size_t >& start, const size_t index );


#endif
