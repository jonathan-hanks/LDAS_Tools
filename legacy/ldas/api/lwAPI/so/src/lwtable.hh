#ifndef LwTableHH
#define LwTableHH

// System Header Files
#include <deque>
#include <memory>   

#include "general/Memory.hh"

#include "query.hh"

// Forward declarations
namespace ILwd
{
   class LdasElement;
}

namespace XERCES_CPP_NAMESPACE  
{
   class DOMNode;
}
   
std::unique_ptr< ILwd::LdasElement > table2ILwd( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node,
   std::deque< Query > dq ); 


#endif
