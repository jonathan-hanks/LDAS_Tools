#include "config.h"

// System Header Files
#include <algorithm>

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// Local Header Files
#include "lwputdata.hh"
#include "query.hh"
#include "vect.hh"
#include "lwstream.hh"
#include "lwgetchild.hh"
#include "getattribute.hh"
#include "lwelement.hh"
#include "lwconsts.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE;   
   

//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW element.
//
// This method initializes PutData structure for LIGO_LW element to put raw data 
// into external file.
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLigoLwPutData()
{
   static ElementSet e;
   if( e.empty() )
   {
     e.insert( "ligo_lw" );
     e.insert( "table" );
     e.insert( "array" );
     e.insert( "igwdframe" );
     e.insert( "adcdata" );
     e.insert( "stream" );
     e.insert( "adcinterval" );
   }

   static PutData ligolwPutData( e );
   return ligolwPutData;
}


//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW Table element.
//
// This method initializes PutData structure for Table element to put raw data 
// into external file.
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLwTablePutData()
{
   static ElementSet e;

   if( e.empty() )
   {
      e.insert( "stream" );
   }

   static PutData tablePutData( e );
   return tablePutData;
}


//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW Array element.
//
// This method initializes PutData structure for Array element to put raw data 
// into external file.
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLwArrayPutData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "stream" );
   }

   static PutData arrayPutData( e );
   return arrayPutData;
}


//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW Stream element.
//
// This method initializes PutData structure for Stream element to put raw data 
// into external file. 
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLwStreamPutData()
{
   static ElementSet e;
   static PutData streamPutData( e );
   return streamPutData;
}


//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW Frame header element.
//
// This method initializes PutData structure for Frame header element to put 
// raw data into external file.
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLwFramePutData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "adcdata" );
      e.insert( "detector" );
      e.insert( "igwdframe" );
      e.insert( "array" );
      e.insert( "ligo_lw" );
   }

   static PutData framePutData( e );
   return framePutData;
}


//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW AdcData element.
//
// This method initializes PutData structure for AdcData element to put raw data 
// into external file.
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLwAdcDataPutData()
{
   static ElementSet e;
   if( e.empty() )
   {
     e.insert( "adcdata" );
     e.insert( "ligo_lw" );
   }


   static PutData adcPutData( e );
   return adcPutData;
}


//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW Detector element.
//
// This method initializes PutData structure for Detector element to put raw 
// data into external file.
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLwDetectorPutData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "ligo_lw" );
   }

   static PutData detectorPutData( e );
   return detectorPutData;
}


//------------------------------------------------------------------------------
//
//: Initialize PutData structure for LIGO_LW AdcInterval element.
//
// This method initializes PutData structure for AdcInterval element to put 
// raw data into external file.
//
//!returns: PutData - Initialized structure.
//
ElementPutData::PutData& ElementPutData::initLwAdcIntervalPutData()
{
   static ElementSet e;
   if( e.empty() )
   {
      e.insert( "adcdata" );
   }

   static PutData adcIntervalPutData( e );
   return adcIntervalPutData;
}
   

//------------------------------------------------------------------------------
//
//: Initialize LwPutData unordered_map.
//
// This method initializes hash map used to place LIGO_LW elements raw data 
// into external file. It associates hash maps with a different element types in
// LIGO_LW document.
//
//!returns: LwPutData - Hash map.
//
ElementPutData::LwPutData& ElementPutData::initAllPutData()
{
   static LwPutData h;
   if( h.empty() )
   {
      h[ "ligo_lw"     ] = putDataPair( "LIGO_LW", initLigoLwPutData() );
      h[ "table"       ] = putDataPair( "Table", initLwTablePutData() );
      h[ "array"       ] = putDataPair( "Array", initLwArrayPutData() );
      h[ "igwdframe"   ] = putDataPair( "IGWDFrame", initLwFramePutData() );
      h[ "adcdata"     ] = putDataPair( "AdcData", initLwAdcDataPutData() );
      h[ "detector"    ] = putDataPair( "Detector", initLwDetectorPutData() );
      h[ "stream"      ] = putDataPair( "Stream", initLwStreamPutData() );
      h[ "adcinterval" ] = putDataPair( "AdcInterval", initLwAdcIntervalPutData() );   
   }

   return h;
}


ElementPutData::LwPutData& ElementPutData::mAllPutData = initAllPutData( );


string ElementPutData::mUrl( "" );
string ElementPutData::mDir( "" );


//------------------------------------------------------------------------------
//   
//: ( Default ) Constructor.
//   
ElementPutData::ElementPutData()
   : mData( initLigoLwPutData() ) 
{}
   
   
//------------------------------------------------------------------------------
//   
//: Constructor
//
//!param: const std::string& url - Url address for placing the data.
//!param: const std::string& dir - Directory for placing the data.
//   
ElementPutData::ElementPutData( const std::string& url, const std::string& dir )
   : mData( initLigoLwPutData() )
{  
   mUrl = url; 
   mDir = dir; 
}


//------------------------------------------------------------------------------
//   
//: Constructor
//
//!param: PutData& a - PutData structure to initialize with.
//   
ElementPutData::ElementPutData( const PutData& a )
   : mData( a )
{}
   

//------------------------------------------------------------------------------
//
//: Find method for ElementPutData.
// 
// This method puts data of the specified element from the list of LIGO_LW 
// elements into external file. 
//
//!param: nodeList - List of LIGO_LW elements to be searched for specified     +
//!param:    data.
//!param: dq - Tokenized command.
//!param: start - Vector to record element position in the document tree.
//!param: index - Current index into position vector.
//
//!return: True if element was placed in external file, false otherwise.
//
//!exc: not_valid_element - Specified element is not valid.
//
bool ElementPutData::find( std::vector< DOMNode* >& nodeList, 
   std::deque< Query >& dq, std::vector< size_t >& start, const size_t index )
{
   if ( nodeList.empty() )
   {
      throw SWIGEXCEPTION( "not_valid_element" );
   }
   
   if ( index == start.size() )
   {
      start.push_back( 0 );
   }

   
   const INT_4U size( start.size() );
   
   // Flag to indicate that return value is "not_valid_element" 
   bool notValidRet( false );

   
   for( vector< DOMNode* >::iterator iter = nodeList.begin(),
        end_iter = nodeList.end(); iter != end_iter; ++iter )
   {
      start[ index ] = iter - nodeList.begin();
      unwind( start, size );
   
      try
      {
         if( getLwType( *iter ) == LW_STREAM_ID )
         {
            if( stream2File( *iter, dq, mUrl, mDir ) == true )
            {
               return true;
            }
         }
         else
         {
            if( operator()( *iter, dq, start, index + 1 ) == true )
            {
               return true;
            }
         }
      }
      catch ( SwigException& e )
      {
	 if ( e.getResult() == "not_valid_element" )
	 {
	    notValidRet = true;
	 }
         else
         {
            throw;
         }
      }
   }

   
   unwind( start, size );
   if ( notValidRet == true )
   {
      // If any of the elements didn't contain specified element
      throw SWIGEXCEPTION ( "not_valid_element" );
   }
   
   return false;
}


//------------------------------------------------------------------------------
//
//: Operator() for ElementPutData.
//
// This operator puts raw data of LIGO_LW element into external file.
//
//!param: node - LIGO_LW element.
//!param: dq - Tokenized command.
//!param: start - Vector to record element position in the document tree.
//!param: index - Current index into position vector.
//
//!return: True if data was placed in external file, false otherwise.
//
//!exc: SwigException
//   
bool ElementPutData::operator()( 
   DOMNode* node, 
   std::deque< Query > dq,
   std::vector< size_t >& start, const size_t index )
{
    if ( node == 0 )
    {
       throw SWIGEXCEPTION( "not_valid_element" );
    }
   
   
    if( dq.size() == 0 )
    {
       if( mData.Element.size() != 0 )
       {
          if( mData.Element.count( "stream" ) == 1 )
          {
             vector< DOMNode* > nodeVector( getChildByTag( node,
                                                           LIGO_LW::streamTag ) );
             return find( nodeVector, dq, start, index );
          }
          throw SWIGEXCEPTION( "bad_query" );
       }
       throw SWIGEXCEPTION( "bad_query" );
    }

   
    Query q = dq.front();
    dq.pop_front();
    const CHAR* hashIndex( q.getName().c_str() );

   
    // It's not a query
    if ( !q.isQuery() )
    {
       if( mData.Element.size() != 0 )
       {
          if( mData.Element.count( hashIndex ) == 1 )
          {
             vector< DOMNode* > nodeVector(
                                               getChildByTag( node,
                                                  mAllPutData[ hashIndex ].first ) );
   
             // Set up new element set and conversion function
             ElementPutData e( mAllPutData[ hashIndex].second );
             return e.find( nodeVector, dq, start, index );
          }
          throw SWIGEXCEPTION( "bad_query" );
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
    else
    {
       if( mData.Element.size() != 0 )
       {
          if( mData.Element.find( hashIndex ) != mData.Element.end() )
          {
             vector< DOMNode* > nodeVector( getChildByTag( node,
                                               mAllPutData[ hashIndex ].first ) );

             DOMNode* elem( getContained( nodeVector, q, start, index ) ); 

             if ( strcmp( hashIndex, "stream" ) == 0 )
             {
                return stream2File( elem, dq, mUrl, mDir );          
             }       
   
             ElementPutData e( mAllPutData[ hashIndex ].second );
             return e( elem, dq, start, index + 1 );
          }
   
          throw SWIGEXCEPTION( "bad_query" );
       }
       throw SWIGEXCEPTION( "bad_query" );
    }
   
    return false;
}
