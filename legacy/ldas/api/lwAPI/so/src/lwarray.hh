#ifndef LwArrayHH
#define LwArrayHH

// System Header Files
#include <vector>
#include <deque>
#include <new>
#include <memory>   

#include "general/Memory.hh"

// ILWD Header Files
#include <ilwd/ldasarray.hh>

// General Header Files
#include <general/ldasexception.hh>   
#include <general/autoarray.hh>

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>

// Local Header Files
#include "lwutil.hh"
#include "lwgetchild.hh"
#include "convertstr.hh"
#include "lwstream.hh"
#include "query.hh"
#include "lwconsts.hh"   


// array2ILwd: converts LW format Array into ILWD format array.
std::unique_ptr< ILwd::LdasElement > array2ILwd( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node, std::deque< Query > dq );

   
template< class T >
void checkByteOrder( const std::string& encoding, T* data, 
   const INT_4U num ) 
{
   #ifdef WORDS_BIGENDIAN
   if( contain( encoding, LIGO_LW::littleEndianEncodingAttValue ) )
   #else
   if( contain( encoding, LIGO_LW::bigEndianEncodingAttValue ) )
   #endif
   {
      reverse< sizeof( T ) >( data, num );
   }   
   
   return;
}
   

// Template specializations         
template<>
void checkByteOrder< COMPLEX_8 >( const std::string& encoding,
				  COMPLEX_8* data, 
				  const INT_4U num );    
   
template<>
void checkByteOrder< COMPLEX_16 >( const std::string& encoding,
				   COMPLEX_16* data, 
				   const INT_4U num );       
   

//-------------------------------------------------------------------------------
//   
//: Converts xml element to the ILWD format array.
//
//!exc: SwigException
//!exc: LdasException   
//!exc: bad_alloc - Memory allocation failed.
//   
template< class T >
std::unique_ptr< ILwd::LdasElement > array2ILwd( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node ) 
{
    INT_4U num_data( 1 );    
   
    const std::vector< XERCES_CPP_NAMESPACE::DOMNode* > dimVector( 
       getChildByTag( node, LIGO_LW::dimTag ) );
    
    if ( dimVector.empty() )
    {
      throw SWIGEXCEPTION( "bad_array_element" );
    }

    typename ILwd::LdasArray< T >::size_type num_dims( dimVector.size() );
   
    General::AutoArray< typename ILwd::LdasArray< T >::size_type >
      dims( num_dims == 0 ? 0 : ( new typename ILwd::LdasArray< T >::size_type[ num_dims ] ) );
    General::AutoArray< std::string > units( num_dims == 0 ? 0 : ( new std::string[ num_dims ] ) );
    General::AutoArray< REAL_8 > dx( num_dims == 0 ? 0 : ( new REAL_8[ num_dims ] ) );
    General::AutoArray< REAL_8 > startx( num_dims == 0 ? 0 : ( new REAL_8[ num_dims ] ) );

    const size_t tmp_dim( 1 );
   
    XERCES_CPP_NAMESPACE::ArrayJanitor< const REAL_8 > tmp( 0 );
   
   
    for( INT_4U i = 0; i < num_dims; ++i )
    {
       dims[ i ]  = strtoul( nodeValue( dimVector[ i ] ).c_str(), 0, 10 );
   
       units[ i ] = nodeAttribute( dimVector[ i ], LIGO_LW::unitAttName );
   
       num_data *= dims[ i ];
   
   
       tmp.reset( nodeAttribute< REAL_8 >( dimVector[ i ], LIGO_LW::scaleAttName,
                                           tmp_dim ) );
       if( tmp.get() == 0 )
       {
          dx[ i ] = defaultScaleX;
       }
       else
       {
          dx[ i ] = tmp[ 0 ];
       }
   
       tmp.reset( nodeAttribute< REAL_8 >( dimVector[ i ], LIGO_LW::startAttName,
                                           tmp_dim ) );
       if( tmp.get() == 0 )
       {
          startx[ i ] = defaultStartX;
       }
       else
       {
          startx[ i ] = tmp[ 0 ];
       }   
    }

    // Don't need the memory anymore
    tmp.reset( 0 );
   
    
    std::string name( nodeAttribute( node, LIGO_LW::nameAttName ) );

    static const char* xml_name = ":Array:XML";
    if( name.find( xml_name ) == std::string::npos )
    {
       name.append( xml_name );
    }
   
    std::string unity( nodeAttribute( node, LIGO_LW::unitAttName ) );      

    XERCES_CPP_NAMESPACE::ArrayJanitor< T > sData( 0 );
   
    // If it's a zero-dimensional array      
    if( num_data == 0 )
    {
       const typename ILwd::LdasArray< T >::size_type a_ndims( 0 );
       const typename ILwd::LdasArray< T >::size_type* a_dims( 0 );      
       return std::unique_ptr< ILwd::LdasElement >( new ILwd::LdasArray< T >(
                                                         sData.get(),
                                                         a_ndims,
                                                         a_dims,
                                                         name ) );
    }

   
    const std::vector< XERCES_CPP_NAMESPACE::DOMNode* > streamVector(
       getChildByTag( node, LIGO_LW::streamTag ) );
   
    if( streamVector.empty() )  
    {
       // For now Array has to have internal stream which contains 
       // array's data 
       throw SWIGEXCEPTION( "stream_is_missing" );
    }

    // Get Stream data: 
    //!TODO: If array element contains more than one stream,
    //       need to handle them all, for now only first stream
    //       element will be converted to ILWD.

    XERCES_CPP_NAMESPACE::DOMNode* stream( streamVector.front() );

    std::string encoding( nodeAttribute( stream, LIGO_LW::encodingAttName ) );
   
    std::string stream_delimiter( nodeAttribute( stream, LIGO_LW::delimiterAttName ) );
   
   
    // Delimiter must be defined for text data
    if( stream_delimiter.empty() &&
        ( encoding.empty() || 
        ( !contain( encoding, LIGO_LW::base64EncodingAttValue ) && 
          !contain( encoding, LIGO_LW::binaryEncodingAttValue ) ) ) )
    {
       throw SWIGEXCEPTION( "stream_delimiter_is_missing" );
    }

   
    std::string stream_type( nodeAttribute( stream, LIGO_LW::typeAttName ) );
   
    // If stream type is not specified, just assume it's Local
    if( ( stream_type.empty() == false ) &&
          stream_type == LIGO_LW::remoteTypeAttValue )   
    {
       if ( file2Stream( stream ) == false )
       {
          throw SWIGEXCEPTION( "file2stream_failed" );
       }
    }

    std::string stream_value( nodeValue( stream ) );
    if( stream_value.empty() )
    {
       throw SWIGEXCEPTION( "stream_data_is_missing" );
    }
 
    // Handle formated data:
    // There's base64 or binary encoding
    if( contain( encoding, LIGO_LW::base64EncodingAttValue ) ||
        contain( encoding, LIGO_LW::binaryEncodingAttValue ) ) 
    {
       ILwd::Style style;
    
       style.setWriteFormat( 
          contain( encoding, LIGO_LW::base64EncodingAttValue ) == true ? ILwd::BASE64 : 
                                                                         ILwd::BINARY );
   
       sData.reset( style.template convertFromFormat< T >( stream_value.c_str(),
                                                           stream_value.size(), 
                                                           num_data ) );
    }
    else if( encoding.empty() == true || 
             contain( encoding, LIGO_LW::textEncodingAttValue ) ||
             contain( encoding, LIGO_LW::littleEndianEncodingAttValue ) ||
             contain( encoding, LIGO_LW::bigEndianEncodingAttValue ) )
    {
       // Erase any preceeding spaces:
       stream_value.erase( stream_value.begin(), 
                           find_if( stream_value.begin(), stream_value.end(), not_space ) );
       sData.reset( string2ArrayData< T >( stream_value, num_data,
                                           stream_delimiter[ 0 ] ) );
    }
    else
    {
       std::string msg( "Unknown stream format: " );
       msg += encoding;
       throw SWIGEXCEPTION( msg );
    }
   

    checkByteOrder( encoding, sData.get(), num_data );


    const bool allocate( false );
    const bool own( true );
   
    std::unique_ptr< ILwd::LdasElement > a( new ILwd::LdasArray< T >( 
                                                 sData.release(),
                                                 num_dims, dims.get(), 
                                                 name, units.get(), 
                                                 allocate, own,
                                                 dx.get(), startx.get(), unity ) );
    return a;          
}

   
#endif
