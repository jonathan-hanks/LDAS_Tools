#ifndef LwElementHH
#define LwElementHH

namespace XERCES_CPP_NAMESPACE  
{
   class DOMNode;
}


enum LwType
{
   LW_LIGO_LW_ID = 1,
   LW_FRAME_ID,
   LW_ADCDATA_ID,
   LW_ADCINTERVAL_ID,   
   LW_DETECTOR_ID,
   LW_TIME_ID,
   LW_TABLE_ID,
   LW_COLUMN_ID,
   LW_ARRAY_ID,
   LW_PARAM_ID,
   LW_COMMENT_ID,
   LW_STREAM_ID,
   LW_DIM_ID,
   LW_UNDEFINED        
};


LwType getLwType( const XERCES_CPP_NAMESPACE::DOMNode* const node );


#endif
