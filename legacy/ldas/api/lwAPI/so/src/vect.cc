// Local Header Files
#include "config.h"

#include "vect.hh"

   
//-------------------------------------------------------------------------------
//   
//: Unwinds position vector to the requested size   
//
//!praram: vector< size_t >& v - Position vector.
//!param: size_t size - Final vector size.
//
//!return: Nothing.
//   
void unwind( std::vector< size_t >& v, size_t size )
{
   if( v.size() <= size )
   {
      return;
   }

   v.erase( v.begin() + size, v.end() );   
   
   return;
}
