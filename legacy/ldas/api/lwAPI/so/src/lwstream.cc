#include "config.h"

// System Header Files
#include <fstream>
#include <memory>
#include <vector>

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// XML Header Files
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMAttr.hpp>
#include <xercesc/dom/DOMText.hpp>
#include <xercesc/util/XMLURL.hpp>
#include <xercesc/util/BinInputStream.hpp>
#include <xercesc/dom/DOMNodeList.hpp>


// Local Header Files
#include "lwstream.hh"
#include "lwutil.hh"
#include "lwgetchild.hh"
#include "query.hh"
#include "urlutil.h"
#include "lwconsts.hh"   

using namespace std;
using namespace XERCES_CPP_NAMESPACE;  
   
   
//-------------------------------------------------------------------------------
//: Get a number of external references in the document element.
//
// This method counts the number of external references in the document.
// Only Stream elements can have external references.
//
//!param: const DOMElement* const - Document element.
//
//!return: Number of external references in the element.
//
static INT_4U initExtReferences( const DOMElement* const elem )
{
   // Transcode name from char* to the xmlch*
   LIGO_LW::XString stream_name( LIGO_LW::streamTag );
   
   const DOMNodeList* elemList( elem->getElementsByTagName( stream_name.unicodeForm() ) );
   
   if( elemList == 0 || elemList->getLength() == 0 )
   {
      return 0;
   }
   
   
   INT_4U num( 0 );
   for ( INT_4U i = 0, length = elemList->getLength(); i < length; ++i )
   {
      if( nodeAttribute( elemList->item( i ), LIGO_LW::typeAttName ) == 
          LIGO_LW::remoteTypeAttValue )
      {
         ++num;
      }
   }
   
   return num;
}


//-----------------------------------------------------------------------
//
//: Put data of LIGO_LW Stream element into external file.
//
//!param: node - LIGO_LW Stream element.
//!param: dq - Tokenized command.
//!param: urlAddr - URL address.
//!param: urlDir - URL directory.
//
//!return: True if data was placed into external file, false otherwise.
//
//!exc: SwigException
//!exc: bad_cast - Invalid stream format.   
//   
bool stream2File( DOMNode* const node, std::deque< Query > dq,
		  const std::string& urlAddr, const std::string& urlDir ) 
{
  if( node == 0 )
  {
     throw SWIGEXCEPTION( "not_valid_element" );
  }
   
  if ( dq.size() != 0 )
  {
     throw SWIGEXCEPTION( "bad_query" );
  }

   
  string stream_type( nodeAttribute( node, LIGO_LW::typeAttName ) );

  if( stream_type == LIGO_LW::remoteTypeAttValue )
  {
     throw SWIGEXCEPTION( "remote_data" );
  }
  else
  if( stream_type.empty() || 
      stream_type == LIGO_LW::localTypeAttValue )
  {
     DOMDocument* const doc( node->getOwnerDocument() );
     INT_4U numReferences( initExtReferences( doc->getDocumentElement() ) );
 
     ostringstream ss;
     ifstream infile;
   
     // Check how many entity files already exist
     // (to figure out name for the next file to create)
     do 
     {
        if ( infile != 0 )
        {
           infile.close();
        }
        ss.str( "" );
        ss << urlDir << "edata" << numReferences << ".dat";
        infile.open( ss.str().c_str() );
        if ( !infile.fail() )
        {
           ++numReferences;
        }
     }
     while( infile != 0 );

   
     // Create file name for new reference
     ss.str( "" );
     ss << "edata" << numReferences << ".dat";
     const char* name( ss.str().c_str() );
     

     string dir( urlDir );
     dir += name;
   
     // Write data into the file
     ofstream out( dir.c_str() );
     if ( out == 0 )   
     {
        string msg( "Error opening the file: " );
        msg += dir;
        throw SWIGEXCEPTION( msg );
     }
     out << nodeValue( node );
     out.close();

     string entityDir( urlAddr );
     dir += name;

   
     // Put external reference into the Stream:
     // Transcode entityDir from char* to the xmlch*
     DOMText* addr( doc->createTextNode( LIGO_LW::XML_STRING( entityDir.c_str() ) ) );     
     DOMNode* old_node( node->replaceChild( addr, node->getFirstChild() ) );
   
     // This is optional: orphan node would be freed when document it
     // originally belonged to would be freed.
     old_node->release();


     // Reset Type attribute to Remote:
   
     // It's safe to dynamic_cast to DOMElement since earlier call to
     // "nodeAttribute" already checked for the object type to be 
     // DOMNode::ELEMENT_NODE
     DOMAttr* attr = ( dynamic_cast< const DOMElement* const >( node ) )->getAttributeNode( 
                          LIGO_LW::XML_STRING( LIGO_LW::typeAttName ) );
   
     attr->setValue( LIGO_LW::XML_STRING( LIGO_LW::remoteTypeAttValue ) );
     return true;
  }
  else 
  {
     throw SWIGEXCEPTION( "unknown_stream_type" );
  }
   
  return false;
}

   
// Function declaration
const string openURL( const char* id );


//-----------------------------------------------------------------------
//
//: Put data from external file into LIGO_LW Stream element.
//
//!param: node - LIGO_LW Stream element.
//
//!return: True if data was placed into the Stream, false otherwise.
//
//!exc: SwigException 
//!exc: DOMDOMException - XML format error.  
//   
bool file2Stream( DOMNode* const node )
{
   if( node == 0 )
   {
      throw SWIGEXCEPTION( "not_valid_element" );
   }
 
   const string stream_type( nodeAttribute( node, LIGO_LW::typeAttName ) );
   if( stream_type.empty() )  
   {
      throw SWIGEXCEPTION( "stream_type_missing" );
   }
   else if( stream_type == LIGO_LW::localTypeAttValue )
   {
      throw SWIGEXCEPTION( "local_data" );
   }
 
   const string text( openURL( nodeValue( node ).c_str() ) );
   if( text.empty() == true )
   {
      throw SWIGEXCEPTION( "empty_url_file" );
   }

   
   // Put file content into the Stream
   DOMText* text_node( node->getOwnerDocument()->createTextNode( 
                          LIGO_LW::XML_STRING( text.c_str() ) ) );
   DOMNode* old_node = node->replaceChild( text_node, node->getFirstChild() );   
   
   
   // This is optional: orphan node would be freed when document it
   // originally belonged to would be freed.
   old_node->release();   
   
   
   // Reset Type attribute to "Local":
 
   // It's safe to dynamic_cast to DOMElement since earlier call to
   // "nodeAttribute" already checked for the object type to be 
   // DOMNode::ELEMENT_NODE   
   DOMAttr* attr = ( dynamic_cast< DOMElement* const >( node ) )->getAttributeNode( 
                        LIGO_LW::XML_STRING( LIGO_LW::typeAttName ) );
   
   
   attr->setValue( LIGO_LW::XML_STRING( LIGO_LW::localTypeAttValue ) );
   return true;
}


//   
//!exc: SwigException
//   
CHAR* openURLFile( XMLURL& url )
{
   LIGO_LW::XString tmpFullPath( url.getURLText() );
   LIGO_LW::XString tmpFileName( url.getPath() );
   LIGO_LW::XString tmpHostName( url.getHost() );

   
   INT_4S urlOpenError = 0;
   CHAR* res = openFTP( tmpFullPath.charForm(), 
                        tmpHostName.charForm(),
                        url.getPortNum(),
                        tmpFileName.charForm(), 
	                &urlOpenError );
   
   if( urlOpenError )
   {
      throw SWIGEXCEPTION( "ftp_failed");
   }
   
   return res;
}

   
//-----------------------------------------------------------------------
//
//: Open URL.
//
//!param: sysID - URL address to access.
//
//!return: Remote file content.
//
//!exc: open_file_failed - Couldn't open URL file.
//
const string openURL( const char* sysId ) 
{
   XMLURL tmpURL;   
   
   size_t len( strlen( sysId ) );
   ostringstream id;
   while( len-- > 0 )
   {
     if( !isspace( *sysId ) )
     {
       id << *sysId;
     }
     sysId++;
   }

   //cerr << "URL string is `" << id.str() << "'" << endl;
   try
   {
      tmpURL.setURL( LIGO_LW::XML_STRING( id.str().c_str() ) );
   }
   catch( const MalformedURLException& )
   {
      throw SWIGEXCEPTION( "malformed_url" );      
   }
   catch (...)
   {
      throw SWIGEXCEPTION( "url_parsing_failed" );      
   }

   
#if 0
   cerr << "URL got parsed as follows: " << endl;
   cerr << "\tProtocol number=" << tmpURL.getProtocol() << endl;
   if (tmpURL.getProtocol() != XMLURL::Unknown) 
	if (tmpURL.getProtocolName())
     		cerr << "\tProtocol=" << XMLString::transcode(tmpURL.getProtocolName()) << endl;
   if (tmpURL.getHost())
	cerr << "\tHost=" << XMLString::transcode(tmpURL.getHost()) << endl;
   if (tmpURL.getPath())
     	cerr << "\tPath=" << XMLString::transcode(tmpURL.getPath()) << endl;
   cerr << "\tPortNum=" << tmpURL.getPortNum() << endl;
#endif // if 0

   
   // See if this is local file 
   if ( tmpURL.getProtocol() == XMLURL::File 
	|| tmpURL.getProtocol() == XMLURL::Unknown )
   {
      ifstream in( LIGO_LW::CHAR_STRING( tmpURL.getPath() ) );
      if( in == 0 )
      {
         throw SWIGEXCEPTION( "open_file_failed" );      
      }
   
      ostringstream ss;
      ss << in.rdbuf();
      in.close();
   
      return ss.str();
   }
   
   
   // use local code for FTP access
   if ( tmpURL.getProtocol() == XMLURL::FTP )
   {
      CHAR *res = openURLFile( tmpURL );
      ostringstream ss;
      ss << res;
      free(res);

      return ss.str();
   }

   // HTTP handling here
   try
   { 
      if( tmpURL.getPath() == 0 )
      {
         throw SWIGEXCEPTION( "bad_url" );
      }

      unique_ptr< BinInputStream > bstream( tmpURL.makeNewStream() );

      int nread = 0;
      string ss;

      do
      {
         XMLByte bytes[2049];
         nread = bstream->readBytes(bytes,2048);
         char *chr = (char *)bytes;
         chr[nread]=0;
         ss += chr;
      } while(nread);

      return ss;
   } 
   catch (MalformedURLException&)
   {
      throw SWIGEXCEPTION( "unsupported_url_protocol" );
   }
   catch (...)
   {
      throw SWIGEXCEPTION( "failed_to_open_url" );
   }
   
   throw SWIGEXCEPTION( "failed_to_open_url" );
   return "";
}
