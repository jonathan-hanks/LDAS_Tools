#include "config.h"

// Local Header Files
#include "lwarray.hh"

using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasArray;
using ILwd::Style;
using ILwd::BASE64;
using ILwd::BINARY;

using namespace XERCES_CPP_NAMESPACE;
using namespace std;   
   

//-----------------------------------------------------------------------
//: Converts lightweight format Array into ILWD format array.
//
//!param: node - LIGO_LW array element to convert.
//!param: query - User command to specify the element to extract.
//
//!return: LdasElement* - A pointer to the ILWD format element.
//
//!exc: not_valid_element - Specified element is not valid.
//!exc: bad_query - Query is bad.
//!exc: unsupported_type - Element type is not supported.
//!exc: bad_alloc - Memory allocation failed.   
//
unique_ptr< LdasElement > array2ILwd( 
   const DOMNode* const node, std::deque< Query > dq ) 
{
   if ( node == 0 )
   {
      throw SWIGEXCEPTION( "not_valid_element" );
   }
   
   if ( dq.size() != 0 )
   {
      throw SWIGEXCEPTION( "bad_query" );
   }

   
   string type = nodeAttribute( node, LIGO_LW::typeAttName ); 
   if ( ( type == "char" ) || ( type == "byte" ) )
   {
      return array2ILwd< CHAR >( node );
   }
   else if ( type == "char_u" )
   {
      return array2ILwd< CHAR_U >( node );
   }
   else if ( ( type == "int_2s" ) || ( type == "short" ) )
   {
      return array2ILwd< INT_2S >( node );
   }
   else if ( type == "int_2u" )
   {
      return array2ILwd< INT_2S >( node );
   }
   else if ( ( type == "int_4s" ) || ( type == "int" ) )
   {
      return array2ILwd< INT_4S >( node );
   }
   else if ( type == "int_4u" )
   {
      return array2ILwd< INT_4U >( node );
   }
   else if ( ( type == "int_8s" ) || ( type == "long" ) )
   {
      return array2ILwd< INT_8S >( node );
   }
   else if ( type == "int_8u" )
   {
      return array2ILwd< INT_8U >( node );
   }
   else if ( ( type == "real_4" ) || ( type == "float" ) )
   {
      return array2ILwd< REAL_4 >( node );
   }
   else if ( ( type == "real_8" ) || ( type == "double" ) )
   {
      return array2ILwd< REAL_8 >( node );
   }
   else if( ( type == "complex_8" ) || ( type == "floatComplex" ) )
   {
      return array2ILwd< COMPLEX_8 >( node );
   }
   else if( ( type == "complex_16" ) || ( type == "doubleComplex" ) )
   {
      return array2ILwd< COMPLEX_16 >( node );
   }   
   else
   {
      string msg( "unsupported_array_type: " );
      msg += type;
      throw SWIGEXCEPTION( msg );
   }
   
   return unique_ptr< LdasElement >( 0 );
}


template<>
void checkByteOrder< COMPLEX_8 >( const std::string& encoding,
				  COMPLEX_8* data, 
				  const INT_4U num )
{
   #ifdef WORDS_BIGENDIAN
   if( contain( encoding, LIGO_LW::littleEndianEncodingAttValue ) )
   #else
   if( contain( encoding, LIGO_LW::bigEndianEncodingAttValue ) )
   #endif
   {
      REAL_4* img( new REAL_4[ num ] );
      REAL_4* real( 0 );
      try
      {
         real = new REAL_4[ num ];   
      }
      catch( std::bad_alloc& )
      {
         delete[] img;
         throw;
      }

      COMPLEX_8* data_elem( data ),
               *const data_end( data + num - 1 );;
      REAL_4* real_elem( real ),
            * img_elem( img );
   
      while( data_elem <= data_end )
      {
         *img_elem  = data_elem->imag();
         *real_elem = data_elem->real();
         
         ++data_elem;
         ++img_elem;
         ++real_elem;
      }   
        
      reverse< sizeof( REAL_4 ) >( img, num );
      reverse< sizeof( REAL_4 ) >( real, num );   
    
      data_elem = data;
      img_elem  = img;
      real_elem = real;
   
      while( data_elem <= data_end )
      {
         *data_elem = COMPLEX_8( *real_elem, *img_elem );

         ++data_elem;
         ++real_elem;
         ++img_elem;
      }

      delete[] img;
      delete[] real;
   }   
   
   return;   
}
   
   
template<>
void checkByteOrder< COMPLEX_16 >( const std::string& encoding,
				   COMPLEX_16* data, 
				   const INT_4U num )
{
   #ifdef WORDS_BIGENDIAN
   if( contain( encoding, LIGO_LW::littleEndianEncodingAttValue ) )
   #else
   if( contain( encoding, LIGO_LW::bigEndianEncodingAttValue ) )
   #endif
   {
      REAL_8* img( new REAL_8[ num ] );
      REAL_8* real( 0 );
      try
      {
         real = new REAL_8[ num ];   
      }
      catch( std::bad_alloc& )
      {
         delete[] img;
         throw;
      }

      COMPLEX_16* data_elem( data ),
                *const data_end( data + num - 1 );;
      REAL_8* real_elem( real ),
            * img_elem( img );
   
      while( data_elem <= data_end )
      {
         *img_elem  = data_elem->imag();
         *real_elem = data_elem->real();
         
         ++data_elem;
         ++img_elem;
         ++real_elem;
      }   
        
      reverse< sizeof( REAL_8 ) >( img, num );
      reverse< sizeof( REAL_8 ) >( real, num );   
    
      data_elem = data;
      img_elem  = img;
      real_elem = real;
   
      while( data_elem <= data_end )
      {
         *data_elem = COMPLEX_16( *real_elem, *img_elem );

         ++data_elem;
         ++real_elem;
         ++img_elem;
      }

      delete[] img;
      delete[] real;
   }   
   
   return;      
}
   
