#ifndef LwElementAttributeHH
#define LwElementAttributeHH

// Local Header Files
#include "util.hh"

   
//------------------------------------------------------------------------------
//
//: ElementAttribute functional.
//
// This recursive functional is designed for extracting a specified attribute 
// from a LIGO_LW element.
//
class ElementAttribute
{
public:
   /* Typedefs */
  typedef General::unordered_map< std::string, const CHAR* > AttrHash;

private:

   const std::string find( 
      const std::vector< XERCES_CPP_NAMESPACE::DOMNode* >& nodeList, 
      std::deque< Query >& dq, std::vector< size_t >& start, 
      const size_t index );
 
   //------------------------------------------------------------------------------
   //: Attribute structure.
   // 
   // This structure is designed to keep an attribute information for each of the 
   // LIGO_LW elements. This structure contains two elements: sHash is a unordered_map 
   // of element attributes, and sSet is a set of child elements for the current
   // element.
   //
   struct attr 
   {
      AttrHash   Attr;
      ElementSet Element;
 
      attr(){}
      attr( const AttrHash& a, const ElementSet& e ) : Attr( a ), Element( e ){}
      attr( const attr& a ) : Attr( a.Attr ), Element( a.Element ){}
   };
    
   typedef struct attr Attribute;
   typedef std::pair< const CHAR*, const Attribute > attrPair;
   typedef General::unordered_map< std::string, attrPair* > LwAttribute;
 
   static Attribute& initLigoLw();
   static Attribute& initLwTable();
   static Attribute& initLwArray();
   static Attribute& initLwDim();
   static Attribute& initLwColumn();
   static Attribute& initLwStream();
   static Attribute& initLwParam();
   static Attribute& initLwFrame();
   static Attribute& initLwAdcData();
   static Attribute& initLwDetector();
   static Attribute& initLwTime();
   static Attribute& initLwAdcInterval();   
   static LwAttribute& initAllElements();
 
   Attribute mHash;
   static LwAttribute& mAllElements;
 
 public:
 
   /* Default Constructor. */
   ElementAttribute();
   /* Constructor. */
   ElementAttribute( const Attribute& a );

   //: Overloaded operator().
   //
   // This operator gets an attribute of the specified LIGO_LW element.
   //
   //!param: const DOMNode* const node - LIGO_LW element to be searched for specified +
   //!param:    attribute.
   //!param: std::deque< Query > dq - Tokenized command.
   //!param: std::vector< size_t >& start - Vector to record element position   +
   //!param:    in the document tree. 
   //!param: size_t index - Current index into position vector.
   //
   //!return: Attribute value or "NULL" if attribute is not found.
   //
   //!exc: bad_query - Query is bad.
   //!exc: not_valid_element - Element is not valid.
   //!exc: ElementAttribute_unknown_error - Unknown error has occured.
   //   
   const std::string operator() ( 
      XERCES_CPP_NAMESPACE::DOMNode* const node,
      std::deque< Query > dq, std::vector< size_t >& start,
      const size_t index );
};


#endif
