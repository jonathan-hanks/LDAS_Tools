#include "config.h"

// System Header Files
#include <string>
#include <cctype>
#include <utility>      
#include <iomanip>
#include <stdexcept>   
#include <cstring>   

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
#include <ilwd/style.hh>   
   
// General Header Files   
#include <general/util.hh>   

// XML Header Files
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMDocument.hpp>   
#include <xercesc/dom/DOMText.hpp>   
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMImplementation.hpp>   
#include <xercesc/dom/DOMImplementationRegistry.hpp>      
   
// Local Header Files
#include "lwdocument.hh"
#include "lwwrite.hh"
#include "lwutil.hh"
#include "lwstream.hh"
#include "lwelement.hh"
#include "lwgetchild.hh"
#include "lwconsts.hh"
#include "util.hh"
   
using namespace std;
using namespace ILwd;   
using namespace XERCES_CPP_NAMESPACE;   

   
// Static data initialization
bool LWDocument::mExpandReferences( false );

const CHAR* const LWDocument::mDTD(
    "<!ELEMENT LIGO_LW ((LIGO_LW|Comment|Param|Table|Array|Stream|"
                        "IGWDFrame|AdcData|AdcInterval|Time|Detector)*)>\n"
    "<!ATTLIST LIGO_LW\n" 
    "          Name CDATA #IMPLIED\n"
    "          Type CDATA #IMPLIED>\n\n"
    "<!ELEMENT Comment (#PCDATA)>\n\n"
    "<!ELEMENT Param (#PCDATA|Comment)*>\n"
    "<!ATTLIST Param \n"
    "          Name CDATA #IMPLIED\n"
    "          Type CDATA #IMPLIED\n"
    "          Start CDATA #IMPLIED\n"   
    "          Scale CDATA #IMPLIED\n"      
    "          Unit CDATA #IMPLIED\n"   
    "          DataUnit CDATA #IMPLIED>\n\n"
    "<!ELEMENT Table (Comment?,Column*,Stream?)>\n"
    "<!ATTLIST Table \n"
    "          Name CDATA #IMPLIED\n"
    "          Type CDATA #IMPLIED>\n\n"
    "<!ELEMENT Column EMPTY>\n"
    "<!ATTLIST Column\n"
    "          Name CDATA #IMPLIED\n"
    "          Type CDATA #IMPLIED\n"
    "          Unit CDATA #IMPLIED"         
#ifdef LIGO_LW_COLUMN_EXTRA_ATTS  
    "\n"
    "          Start CDATA #IMPLIED\n"   
    "          Scale CDATA #IMPLIED\n"      
    "          DataUnit CDATA #IMPLIED>\n\n" 
#else  
    ">\n\n"
#endif   
    "<!ELEMENT Array (Dim*,Stream?)>\n"
    "<!ATTLIST Array \n"
    "          Name CDATA #IMPLIED\n"
    "          Type CDATA #IMPLIED\n"
    "          Unit CDATA #IMPLIED>\n\n"
    "<!ELEMENT Dim (#PCDATA)>\n"
    "<!ATTLIST Dim \n"
    "          Name  CDATA #IMPLIED\n"
    "          Unit CDATA #IMPLIED\n"
    "          Start CDATA #IMPLIED\n"
    "          Scale CDATA #IMPLIED>\n\n"
    "<!ELEMENT Stream (#PCDATA)>\n"
    "<!ATTLIST Stream \n"
    "          Name      CDATA #IMPLIED\n"
    "          Type      (Remote|Local) \"Local\"\n"
    "          Delimiter CDATA \",\"\n"
    "          Encoding  CDATA #IMPLIED\n" 
    "          Content   CDATA #IMPLIED>\n\n"
    "<!ELEMENT IGWDFrame ((Comment|Param|Time|Detector|AdcData|LIGO_LW|Stream?|Array|IGWDFrame)*)>\n"
    "<!ATTLIST IGWDFrame \n"
    "          Name CDATA #IMPLIED>\n\n"
    "<!ELEMENT Detector ((Comment|Param|LIGO_LW)*)>\n"
    "<!ATTLIST Detector \n"
    "          Name CDATA #IMPLIED>\n\n"
    "<!ELEMENT AdcData ((AdcData|Comment|Param|Time|LIGO_LW|Array)*)>\n"
    "<!ATTLIST AdcData \n"
    "          Name CDATA #IMPLIED>\n\n"
    "<!ELEMENT AdcInterval ((AdcData|Comment|Time)*)>\n"
    "<!ATTLIST AdcInterval \n"
    "          Name CDATA #IMPLIED\n"
    "          StartTime CDATA #IMPLIED\n"
    "          DeltaT CDATA #IMPLIED>\n\n"   
    "<!ELEMENT Time (#PCDATA)>\n"
    "<!ATTLIST Time \n"
    "          Name CDATA #IMPLIED\n"
    "          Type (GPS|Unix|ISO-8601) \"ISO-8601\">" );
   
   
//------------------------------------------------------------------------------
//
//: Initialize LWDocument hash map to generate LIGO_LW elements.
//
// This method initializes hash map used to generate LIGO_LW elements in the
// document based on the name key.
//
//!return: LWHash - Hash map.
//
const LWDocument::LWHash LWDocument::initHash()
{
    LWHash h;
    typedef LWHash::value_type val_type;   
   
    h.insert( val_type( "frame", vpair( "IGWDFrame", &LWDocument::makeLigoLw ) ) );
    h.insert( val_type( "table", vpair( "Table", &LWDocument::makeLigoLw ) ) );
    h.insert( val_type( "adcdata", vpair( "AdcData", &LWDocument::makeLigoLw ) ) );
    h.insert( val_type( "frame_group", vpair( "AdcInterval", &LWDocument::makeLigoLw ) ) );   
    h.insert( val_type( "detector", vpair( "Detector", &LWDocument::makeLigoLw ) ) );
    h.insert( val_type( "rawdata", vpair( "RawData", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "procdata", vpair( "ProcData", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "simdata", vpair( "SimData", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "logmsg", vpair( "MessageLog", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "serdata", vpair( "SerialData", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "statdata", vpair( "StaticData", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "trigdata", vpair( "TriggerData", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "strain", vpair( "Strain", &LWDocument::makeLigoLwType ) ) );   
    h.insert( val_type( "summarydata", vpair( "SummaryData", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "history", vpair( "History", &LWDocument::makeLigoLwType ) ) );

    return h;
}


LWDocument::LWHash LWDocument::mILwdHash( LWDocument::initHash() );
   
   
   
//------------------------------------------------------------------------------
//
//: Initialize LWDocument hash map to generate LIGO_LW container elements.
//
// This method initializes hash map used to generate LIGO_LW container elements 
// with a specific type in the document based on the name key.
//
//!return: LWHash - Hash map.
//
const LWDocument::LWHash LWDocument::initContainerHash()
{
    LWHash h;
    typedef LWHash::value_type val_type;   
   
    h.insert( val_type( "container(vect)", vpair( "", &LWDocument::makeLigoLwType ) ) );
    h.insert( val_type( "container(detector)", vpair( "Detector", &LWDocument::makeLigoLwType ) ) );      
    h.insert( val_type( "container(adcdata)", vpair( "AdcData", &LWDocument::makeLigoLwType ) ) );         

    return h;
}


LWDocument::LWHash LWDocument::mILwdContainerHash( LWDocument::initContainerHash() );   

   
//------------------------------------------------------------------------------
//
//: Initialize AdcInterval metadata hash map.
//
// This method initializes LIGO_LW:AdcInterval metadata hash map used to
// generate element attributes.
//
//!return: MetadataHash - Hash map.
//
MetadataHash& LWDocument::initAdcIntervalHash()
{
    static MetadataHash h;
   
    if( h.empty() )
    {
       h.insert( metadataType( "start_time", "StartTime" ) );
       h.insert( metadataType( "delta_t", "DeltaT" ) );   
    }

    return h;
}
   

//------------------------------------------------------------------------------
//
//: Initialize LWDocument metadata hash map.
//
// This method initializes metadata hash map used to generate LIGO_LW elements
// attributes in the document.
//
//!return: LWMetadata - Hash map.
//
LWDocument::LWMetadata& LWDocument::initMetadataHash()
{
    static LWMetadata h;

    if( h.empty() )
    { 
       h[ "AdcInterval" ] = &( initAdcIntervalHash() );
    }

    return h;
}

   
LWDocument::LWMetadata& LWDocument::mMetadata = LWDocument::initMetadataHash( );
   
   
//------------------------------------------------------------------------------
//
//: Initialize String-to-Format hash map.
//
// This method initializes hash map used to associate encoding string with
// ILwd::Format value.
//
//!return: StringFormatHash - Hash map.
//
LWDocument::StringFormatHash LWDocument::initStringFormatHash()
{
    StringFormatHash h;
    typedef StringFormatHash::value_type val_type;
   
    h.insert( val_type( LIGO_LW::base64EncodingAttValue, ILwd::BASE64 ) );
    h.insert( val_type( LIGO_LW::textEncodingAttValue, ILwd::ASCII ) );   
    h.insert( val_type( LIGO_LW::binaryEncodingAttValue, ILwd::BINARY ) );   

    return h;
}
   
   
LWDocument::StringFormatHash LWDocument::mStringFormatHash( 
   LWDocument::initStringFormatHash() );

   
//------------------------------------------------------------------------------
//
//: Initialize Format-to-String hash map.
//
// This method initializes hash map used to associate ILwd::Format value with
// a string representation.
//
//!return: LWMetadata - Hash map.
//
LWDocument::FormatStringHash LWDocument::initFormatStringHash()
{
    FormatStringHash h;
    typedef FormatStringHash::value_type val_type;
   
    h.insert( val_type( ILwd::BASE64, LIGO_LW::base64EncodingAttValue ) );
    h.insert( val_type( ILwd::ASCII, LIGO_LW::textEncodingAttValue ) );   
    h.insert( val_type( ILwd::BINARY, LIGO_LW::binaryEncodingAttValue ) );   

    return h;
}
   
   
LWDocument::FormatStringHash LWDocument::mFormatStringHash( 
   LWDocument::initFormatStringHash() );
   
   
//-------------------------------------------------------------------------------
//
//: ( Default )Constructor.
//
//!param: None.
//
//!exc: Could not obtain a DOMImlementation object from xercesc library -
//      - Could not access a DOMImlementation (required as a factory for DOMDocument)   
//!exc: DOMImplementation could not create DOMDocument object. - Xercesc
//+     factory failed to create a DOMDocument object.
//!exc: DOMException - Xercesc exception occured.  
//
LWDocument::LWDocument() 
   : mDocument( 0 ), mCurrentParent( 0 ), mNodeStack( 64 ), 
     mStreamFormat( ILwd::ASCII ) 
{
   // Xercesc library requires this "Core" keyword to obtain the implementation
   DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(
                                LIGO_LW::XML_STRING( "Core" ) );
   
   if( impl == 0 )
   {
      throw runtime_error( "Could not obtain a DOMImlementation object "
                               "from xercesc library." );
   }

   mDocument = impl->createDocument( 0,                                  // root element namespace URI.
                                     LIGO_LW::XML_STRING( LIGO_LW::ligoLwTag ), // root element name
                                     0 );                                // document type object (DTD).
   
   if( mDocument == 0 )
   {
      throw runtime_error( "DOMImplementation could not create "
                               "DOMDocument object." );
   }
   
   mCurrentParent = mDocument->getDocumentElement();
   mNodeStack.push_back( mCurrentParent );
}


//-------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor is called if the Document node of LIGO_LW document is already
// created, which means that LIGO_LW document tree is already built.
//
//!param: const DOMDocument& doc - Document node of LIGO_LW document.
//
//!exc: file2stream_failed - Error expanding external references.
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: LWDocument::LWDocument() could not clone DOMDocument object. -
//+     Could not close existing DOMDocument object.   
//
LWDocument::LWDocument( const DOMDocument* doc ) 
   : mDocument( 0 ), mCurrentParent( 0 ), mNodeStack( 64 ), 
     mStreamFormat( ILwd::ASCII ) 
{
   if( doc == 0 )
   {
      throw runtime_error( "LWDocument::LWDocument() cannot deep copy NULL "
                           "DOMDocument object." );
   }
   const bool deep_copy( true );
   DOMNode* new_doc = doc->cloneNode( deep_copy );
   if( new_doc == 0 )
   {
      throw runtime_error( "LWDocument::LWDocument() could not clone "
                           "DOMDocument object." );      
   }
   
   
   if( new_doc->getNodeType() != DOMNode::DOCUMENT_NODE )
   {
      new_doc->release();
      throw runtime_error( "LWDocument::LWDocument() clone of "
                               "DOMDocument object is not of DOCUMENT_NODE type" );   
   }
   
   
   mDocument = dynamic_cast< DOMDocument* >( new_doc );
   expandReferences();
   mCurrentParent = mDocument->getDocumentElement();
}


//-------------------------------------------------------------------------------
//
//: Destructor.
//
LWDocument::~LWDocument()
{
   mDocument->release();
   mDocument = 0;
   
   mCurrentParent = 0;
}


//-------------------------------------------------------------------------------
// 
//: Set mExpandReference data member of the class.
//
// This method sets mExpandReferences data member to a specified value,
// which controls either all external references get expanded or not 
// when reading a LIGO_LW document in.
//
//!param: const char* value - Value to set mExpandReference to. One of on | off.
//
//!return: Nothing.
//
//!exc: undefined_value - Specified value is incorrect.
//
void LWDocument::setExpandReferences( const CHAR* const value )
{
   // Can use unordered_map ( only for 2 values ??? )
   if ( strcasecmp( value, "on" ) == 0 )
   {
      mExpandReferences = true;
   }
   else if ( strcasecmp( value, "off" ) == 0 )
   {
      mExpandReferences = false;
   }
   else
   {
     throw SWIGEXCEPTION( "undefined_value" );
   }
   
   return;
}

  
//-------------------------------------------------------------------------------
// 
//: Set encoding for Stream elements.
//
// This method sets mStreamEncoding data member to the passed value: one of
// base64, Text, Binary.   
// which specifies 
//
//!param: const char* value - Value to set mExpandReference to. One of 
//+          
//
//!return: Nothing.
//
//!exc: undefined_value - Specified value is incorrect.
//   
void LWDocument::setStreamEncoding( const CHAR* format )
{
   StringFormatHash::const_iterator iter( mStringFormatHash.find( format ) );
   if( iter == mStringFormatHash.end() )
   {
      string msg( "Unknown stream encoding specified: " );
      msg += format;
      msg += " (must be one of ";
      msg += LIGO_LW::base64EncodingAttValue;
      msg += ' ';
      msg += LIGO_LW::textEncodingAttValue;   
      msg += ' ';
      msg += LIGO_LW::binaryEncodingAttValue;   
      msg += ")";
   
      throw runtime_error( msg );
   }
   
   // Will set to one of ASCII, BASE64 or BINARY
   mStreamFormat = iter->second;
   return;
}
   

//-------------------------------------------------------------------------------
//
//: Expand external references.
//
// This method expands external references in the LIGO_LW document.
// This method is envoked only for LIGO_LW Stream elements.
//
//!param: None.
//   
//!return: Nothing.
//
//!exc: file2stream_failed - Error bringing the data into the document.
//
void LWDocument::expandReferences() 
{
   if ( mExpandReferences == true )
   {
      DOMNodeList* elemList = getRootElement()->getElementsByTagName( 
                                 LIGO_LW::XML_STRING( LIGO_LW::streamTag ) );
   
      const XMLSize_t len( elemList->getLength() );
      if( len == 0 )
      {
         return;
      }
   

      DOMNode* elem( 0 );
      for( INT_4U i = 0; i < len; ++i )
      {
         elem = elemList->item( i );
         if( nodeAttribute( elem, LIGO_LW::typeAttName ) ==
             LIGO_LW::remoteTypeAttValue )
         {
	    // Bring the data in:
            if ( file2Stream( elem ) == false )
            {
               throw SWIGEXCEPTION( "file2stream_failed" );
            }
         }
      }
   }
   
   return;
}


//-------------------------------------------------------------------------------
// 
//: Get DTD of the LIGO_LW document.
//
// This method gets the DTD of the LWDocument object.
//   
//!param: None.
//   
//!return: const char* - DTD as a string.
//
const CHAR* LWDocument::getDTD()
{
   return mDTD;
}

   
//------------------------------------------------------------------------------
//
//: Get root element of LIGO_LW document.
//
// This methods gets the root element of LIGO_LW document tree.
//
//!return: DOMElement* - Root element of the document tree.
//
DOMElement* LWDocument::getRootElement() const
{
   return mDocument->getDocumentElement();
}
   
   
//-----------------------------------------------------------------------------
//
//: Convert ILWD format element into LIGO_LW format element.
//
// This method converts ILWD format element into corresponding LIGO_LW format
// element. This method is recursive.
//
//!param: LdasElement* elem - A pointer to the ILWD format element to be       +
//!param:    converted into LIGO_LW element.
//!param: const bool is_root - Flag to indicate that root element of the object
//+       is being converted. Default is FALSE.
//
//!return: Nothing.
//
//!exc: unsupported_element_EXTERNAL - ILWD External element is not supported  +
//!exc:    in the LIGO_LW format.
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: std::bad_cast - Invalid ILWD format is specified.   
//
void LWDocument::convertToLW ( const ILwd::LdasElement* const elem,
   const bool is_root ) 
{
  if( elem == 0 )
  {
     throw runtime_error( "LWDocument::convertToLW(): NULL ILWD element " );
  }

   
  switch( elem->getElementId() )
  {
     case (ID_ILWD):
     {
       if( getLwType( mCurrentParent ) == LW_TABLE_ID )
       {
          makeColumn( elem );
          break;
       }
       // Insert container element in LW document:
       startElement( elem, is_root );

       const LdasContainer& cont( 
          dynamic_cast< const LdasContainer& >( *elem ) );
   
       // Step through all elements of container   
       for( LdasContainer::const_iterator iter = cont.begin(), 
            end_iter = cont.end(); iter != end_iter; ++iter )
       {
          convertToLW( *iter );
       }
   
       if( ( getLwType( mCurrentParent ) == LW_TABLE_ID ) &&
           ( cont.size() != 0 ) )
       {
          makeStream( elem );
       }
   
       endElement( elem, is_root );
       break;
     }
     case (ID_EXTERNAL):
     {
        throw SWIGEXCEPTION( "unsupported_element_EXTERNAL" );
        break;
     }
     default:
     {
        startElement( elem );
        endElement( elem );
        break;
     }

  }

  return; 
}

   
/* Function declaration */
static void addName( DOMElement* elem, const std::string& name ); 
static bool parseName( const LdasElement* elem, const CHAR* nameValue );


//-----------------------------------------------------------------------------
//
//: Convert ILWD format element into LIGO_LW format TimeSeries element.
//
// This method converts ILWD format element into corresponding LIGO_LW format
// TimeSeries element.
//
//!param: LdasElement* elem - A pointer to the ILWD format element to be       +
//!param:    converted into LIGO_LW TimeSeries element.
//!param: const bool is_root - Flag to indicate that root element of the object
//+       is being converted. Default is FALSE.   
//
//!return: Nothing.
//
//!exc: unsupported_element_EXTERNAL - ILWD External element is not supported           +
//!exc:    in the LIGO_LW format.
//!exc: std::bad_cast - Invalid ILWD format is specified.   
//!exc: std::bad_alloc - Memory allocation failed.
//   
void LWDocument::convertToTimeSeries ( const ILwd::LdasElement* const elem,
   const bool is_root ) 
{
   static const CHAR* const adc_name( "adcdata" );
   static const CHAR* const dt_name( "dt" );   
   
   if( elem == 0 )
   {
      throw runtime_error( "LWDocument::convertToTimeSeries(): NULL ILWD element " );
   }
   
   
   switch ( elem->getElementId() )
   {
      case (ID_ILWD):
      {
         bool found( parseName( elem, adc_name ) );
         if( found )
         {
            makeLigoLwType( elem, is_root, LIGO_LW::timeseriesTypeAttValue );
         }
 
         const LdasContainer& cont( 
            dynamic_cast< const LdasContainer& >( *elem ) );

         for( LdasContainer::const_iterator iter = cont.begin(),
              end_iter = cont.end(); iter != end_iter; ++iter )
         {
            convertToTimeSeries( *iter );
         }

         if( found )
         {
            endElement( elem, is_root );
         }
         break;
      }
      case (ID_LSTRING):
      {
         convertToLW( elem );
         break;
      }
      case (ID_EXTERNAL):
      {
         throw SWIGEXCEPTION( "unsupported_element_EXTERNAL" );
         break;
      }
      default:
      {
         //convertToLW( elem );
         // Arrays with one element should be converted to Param: 
         const LdasArrayBase* bb(
            dynamic_cast< const LdasArrayBase* >( elem ) );
         if ( bb->getNData() == 1 )        
         {
            // This may need to be changed since current
            // conversion just copies whatever value it will find 
            // dt to be set to into LIGO_LW Parameter element.
            // Is dt an amount of nanoseconds? May be at this point
            // software should add this duration time to the start
            // time to generate EndTime instead of Duration???
            if( parseName( elem, dt_name ) )
            {
               makeParam( bb, LIGO_LW::durationNameValue );
            }
            else
            {
               makeParam( bb );
            }
          }
          else
          {
#ifdef OLD_XERCES_IMPL   
              if( parseName( elem, "timeoffset" ) )
              {
                 // For now timeOffset of AdcData is converted 
                 // into LIGO_LW StartTime. Is it true?
                 makeTime( elem, "StartTime" );
              }
              else
              {
                 makeArray( bb );
              }   
#endif   
             makeArray( bb );
          }
          break;
       }
    }
    
   return;
}
   
   
//-----------------------------------------------------------------------------
//
//: Write LIGO_LW Document to a stream.
//
// This method writes LIGO_LW document to the stream using an identation.
//
//!param: ostream& stream - The stream to write to.
//
//!return: Nothing.
//
//!exc: empty_document - LIGO_LW document is empty.
//
void LWDocument::write( std::ostream& stream )
{
   if ( mDocument->getFirstChild() == 0 )
   {
      throw SWIGEXCEPTION( "empty_document" );
   }

   const size_t indent( 0 );
   const size_t delta( 3 );
   return writeElement( indent, delta, stream, mDocument );
}
   
   
//-------------------------------------------------------------------------------
// 
//: Create LIGO_LW Column element.
//
// This method creates new LIGO_LW Column element and appends it to
// the current node of the LIGO_LW document tree.
//
//!param: LdasElement* elem - A pointer to the ILWD format element which will    +
//!param: be converted into LIGO_LW Column element.
//
//!return: Nothing.
//
//!exc: different_element_ids - ILWD container within Table element has elements +
//!exc:    of different types.
//!exc: missing_data_type - ILWD container within Table element is missing a     +
//!exc:    type specification. One of char | char_u.   
//!exc: DOMException - Invalid XML format specified.   
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: std::bad_cast - Invalid ILWD format is specified.   
// 
void LWDocument::makeColumn( const ILwd::LdasElement* const elem ) 
{
   if( elem == 0 )
   { 
      return;
   }


   DOMElement* col_elem = mDocument->createElement( 
                             LIGO_LW::XML_STRING( LIGO_LW::columnTag ) );
   
   addName( col_elem, elem->getNameString() );
   
   string units;
   string type( elem->getIdentifier() );
   
   switch( elem->getElementId() )
   {
      case(ID_LSTRING):
      {
         const LdasString* const ss( dynamic_cast< const LdasString* const >( elem ) );
         const size_t dims( ss->getNUnits() );
         if( !dims )
         {
            break;
         }

         size_t dim_index( 0 ); 
         units += ss->getUnits( dim_index );
   
         for( ++dim_index; dim_index < dims; ++dim_index )
         {
            units += ':';   
            units += ss->getUnits( dim_index );
         }
   
         break;
      }
      case(ID_ILWD):
      {
          const LdasContainer& c( 
             dynamic_cast< const LdasContainer& >( *elem ) );
          if( c.size() == 0 )
          {
             type.append(":null");
          }
          else
          {
             LdasContainer::const_iterator iter = c.begin();
             ILwd::ElementId ilwd_type( ( *iter )->getElementId() );
             bool allids( true );
   
             for( LdasContainer::const_iterator end_iter = c.end();
                  iter != end_iter; ++iter )
             {
                allids = allids && 
                         ( ( *iter )->getElementId() == ilwd_type );
             } 
   
             // All container elements must have the same id's
             if( !allids )
             {
                throw SWIGEXCEPTION( "different_element_ids" );
             }
          
             iter = c.begin();
             type += ':';
             type.append( ( *iter )->getIdentifier() );
          }
          break;
       }
       default:
       {
          const LdasArrayBase& bb( 
             dynamic_cast< const LdasArrayBase& >( *elem ) );
   
          const size_t dim( bb.getNDim() );
          bool add_unit( false );
   
#ifdef LIGO_LW_COLUMN_EXTRA_ATTS   
          bool add_startx( false );
          bool add_scalex( false );   
#endif   
   
          for( size_t i = 0; i < dim; ++i )
          {
             if( add_unit == false &&
                 bb.getUnits( i ).empty() == false )
             {
                add_unit = true;
             }

#ifdef LIGO_LW_COLUMN_EXTRA_ATTS      
             if( add_scalex == false && 
                 bb.getDx( i ) != defaultScaleX )
             {
                add_scalex = true;
             }

             if( add_startx == false && 
                 bb.getStartX( i ) != defaultStartX )
             {
                add_startx = true;
             }
#endif   
          }

          if( add_unit )
          {
             if( dim )
             {
                const size_t index( 0 );
                units += bb.getUnits( index );
             }
             for( size_t i = 1; i < dim; ++i )
             {
                units += ':';
                units += bb.getUnits( i );
             }
          }   
   
#ifdef LIGO_LW_COLUMN_EXTRA_ATTS      
          string unity( bb.getDataValueUnit() );
          if( unity.empty() == false )
          {
             col_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::dataUnitAttName ),
                                     LIGO_LW::XML_STRING( unity.c_str() ) );
          }   

          if( add_scalex )
          {
             ostringstream ss;
             ss.precision( REAL_8_DIGITS + 1 );
             ss.setf( ios::scientific, ios::floatfield );     

             if( dim )
             {
                const size_t index( 0 );
                ss << bb.getDx( index );
             }
             for( size_t index = 1; index < dim; ++index )
             {
                ss << ':' << bb.getDx( index );   
             }

             col_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::scaleAttName ),
                                     LIGO_LW::XML_STRING( ss.str() ) );         
          }
   
          if( add_startx )
          {
             ostringstream ss;
             ss.precision( REAL_8_DIGITS + 1 );
             ss.setf( ios::scientific, ios::floatfield );     
   
             if( dim )
             {
                const size_t index( 0 );
                ss << bb->getStartX( index );   
             }       
             
             for( size_t index = 1; index < dim; ++index )
             {
                ss << ':' << bb->getStartX( i );   
             }

             col_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::startAttName ), 
                                     LIGO_LW::XML_STRING( ss.str() ) );            
          }
#endif // LIGO_LW_COLUMN_EXTRA_ATTS      
   
          break;
       }

    }

    col_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ), 
                            LIGO_LW::XML_STRING( type.c_str() ) );   
   
    if( units.empty() == false )
    {
       col_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::unitAttName ),
                               LIGO_LW::XML_STRING( units.c_str() ) );
    }
   
    mCurrentParent->appendChild( col_elem );
    return;
}
   

//-----------------------------------------------------------------------------
//
//: Create LIGO_LW Parameter element.
//
// This command creates a Parameter element of the following format:
// <Param Name="name" Type="lstring">value</Param>, and appends it to the 
// current node of the LIGO_LW document tree.
// 
//!param: LdasString* elem - A pointer to the ILWD format string to be         +
//!param:    converted into LIGO_LW Param element.
//
//!return: Nothing.
//
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: SwigException   
// 
void LWDocument::makeParam( const LdasString* const elem ) 
{
   if( elem == 0 )    
   {
      return;
   }

   DOMElement* param_elem = mDocument->createElement( 
                               LIGO_LW::XML_STRING( LIGO_LW::paramTag ) );
   
   addName( param_elem, elem->getNameString() );
   param_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ),
                             LIGO_LW::XML_STRING( LIGO_LW::lstringTypeAttValue ) );
   
   if ( elem->getComment().empty() == false )
   {
      // Remember current parent
      mNodeStack.push_back( mCurrentParent ); 
      mCurrentParent = param_elem;
   
      makeComment( elem );

      // Restore current parent
      mCurrentParent = mNodeStack.back();
      mNodeStack.pop_back();   
   }

   
   // Insert text content into Parameter element:
   DOMText* text_elem = mDocument->createTextNode(
                           LIGO_LW::XML_STRING( elem->getString().c_str() ) );
   param_elem->appendChild( text_elem );
   mCurrentParent->appendChild( param_elem );
   
   return;
}
   

//-----------------------------------------------------------------------------
//
//: Create LIGO_LW Parameter element.
//
// This command creates a Parameter element of the following format:
// <Param Name="name" Type="type">value</Param>, and appends it to the current
// node of the LIGO_LW document tree. This element is a LIGO_LW conversion of  +
// ILWD format array element.
// 
//!param: const LdasArrayBase* elem - A pointer to the ILWD format array       +
//!param:    element to be converted into LIGO_LW Param element.
//!param: const std::string& name - Value for the name attribute. 
//
//!return: Nothing.
//
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: DOMException - XML format error occured.
//!exc: SwigException   
//   
void LWDocument::makeParam( const ILwd::LdasArrayBase* const elem, 
   const std::string& name )
{
   if( elem == 0 )    
   {
      return;
   }

   DOMElement* param_elem = mDocument->createElement( 
                               LIGO_LW::XML_STRING( LIGO_LW::paramTag ) );
   
   const string elem_name( name.empty() ? elem->getNameString() : name );
   addName( param_elem, elem_name );
   
   param_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ), 
                             LIGO_LW::XML_STRING( elem->getIdentifier().c_str() ) );   

   // When this method is called, it's already verified that
   // it's an one-element array
   const size_t index( 0 );
   
   string unit( elem->getUnits( index ) );
   if( unit.empty() == false )
   {
      param_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::unitAttName ),
                                LIGO_LW::XML_STRING( unit.c_str() ) );
   }
   
   string unity( elem->getDataValueUnit() );
   if( unity.empty() == false )
   {
      param_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::dataUnitAttName ), 
                                LIGO_LW::XML_STRING( unity.c_str() ) );
   }

   REAL_8 scale( elem->getDx( index ) );   
   if( scale != defaultScaleX )
   {
      ostringstream ss;
      ss.precision( REAL_8_DIGITS + 1 );
      ss.setf( ios::scientific, ios::floatfield );   
      ss << scale;
   
      param_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::scaleAttName ), 
                                LIGO_LW::XML_STRING( ss.str().c_str() ) );
   }
   
   REAL_8 startx( elem->getStartX( index ) );
   if( startx != defaultStartX )
   {
      ostringstream ss;   
      ss.precision( REAL_8_DIGITS + 1 );
      ss.setf( ios::scientific, ios::floatfield );      
      ss << startx;
   
      param_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::startAttName ), 
                                LIGO_LW::XML_STRING( ss.str().c_str() ) );   
   }

   
   string data;
   
   // Special care for "prefix"
   if( parseName( elem, LIGO_LW::detectorPrefix ) && 
       elem->getElementId() == ILwd::ID_CHAR )
   {
      // Reset the Type to "lstring"
      param_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ), 
                                LIGO_LW::XML_STRING( LIGO_LW::lstringTypeAttValue ) );         
   
      const LdasArray< CHAR >* const a( dynamic_cast< const LdasArray< CHAR >* const >( elem ) );
      data.assign( a->getData(), a->getNData() );   
   }
   else
   {
      // It is already verified that ILWD array element has only one 
      // element in it ===> extract that one element and put it into
      // LIGO_LW Param element:
      ostringstream data_str;
      insertData( elem, 0, data_str );
      data = data_str.str();
   }
   

   DOMText* text_elem = mDocument->createTextNode( 
                           LIGO_LW::XML_STRING( data.c_str() ) );

   param_elem->appendChild( text_elem );
   mCurrentParent->appendChild( param_elem );
   
   return;
}
   

//-----------------------------------------------------------------------------
//
//: Create Comment element.
//
// This method creates a Comment element of the following format:
// <Comment>comment_string</Comment>, and appends it to the current node of
// the LIGO_LW document tree.
// 
//!param: LdasElement* elem - A pointer to the ILWD format element to be      +
//!param:    converted into LIGO_LW Comment element.
//
//!return: Nothing.
//
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: DOMException - Invalid XML format is specified.   
//!exc: SwigException   
//   
void LWDocument::makeComment( const ILwd::LdasElement* const elem )
{
   if( elem == 0 )
   {
      return;
   }

   DOMElement* comment_elem = mDocument->createElement(
                                 LIGO_LW::XML_STRING( LIGO_LW::commentTag ) );
   
   // Insert text content into comment element:
   DOMText* text_elem;
   if( elem->getElementId() == ID_LSTRING )
   {
      const LdasString& ss( dynamic_cast< const LdasString& >( *elem ) );
      text_elem = mDocument->createTextNode( 
                     LIGO_LW::XML_STRING( ss.getString().c_str() ) );
   }
   else
   {
      text_elem = mDocument->createTextNode( 
                     LIGO_LW::XML_STRING( elem->getComment().c_str() ) );
   }
   
   comment_elem->appendChild( text_elem );
   mCurrentParent->appendChild( comment_elem );
   
   return;
}
   

//-----------------------------------------------------------------------------
//
//: Create Time element.
//
// This method creates a Time element of the following format:
// <Time Type="type">time_value</Time>, and appends it to the current node of
// the LIGO_LW document tree. Default type is ISO-6801( as specified in LIGO_LW 
// DTD ). 
// 
//!param: LdasArrayBase* elem - A pointer to the ILWD format element to be     +
//!param:    converted into LIGO_LW Time element.
//!param: const std::string& name - Name value to assign to the element. Default is +
//!param:    empty string.
//
//!return: Nothing.
//
//!exc: illegal_timeoffset_size - ILWD format element representing timeOffset   +
//!exc:    has illegal number of elements( should be 2 ).
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: DOMException - Illegal XML format.
//   
void LWDocument::makeTime( const ILwd::LdasElement* const elem,
   const std::string& name ) 
{
   if( elem == 0 )
   {
      return;
   }
   

   DOMElement* time_elem = mDocument->createElement( 
                              LIGO_LW::XML_STRING( LIGO_LW::timeTag ) );
   
   const string elem_name( name.empty() ? elem->getNameString() : name );
   addName( time_elem, elem_name );
   
   
   const CHAR* type( 0 );
   string data;
   
   switch( elem->getElementId() )
   {
      case(ID_LSTRING):
      {
         type = LIGO_LW::isoTypeAttValue;
   
         const LdasString* const s_elem( dynamic_cast< const LdasString* const >( elem ) );
         data = s_elem->getString();
   
         break;
      }
      case(ID_INT_4U):
      {  
         type = LIGO_LW::gpsTypeAttValue;
   
         const LdasArray< INT_4U >* const a_elem( 
            dynamic_cast< const LdasArray< INT_4U >* >( elem ) );
   
         // ILWD array element representing Time should always contain
         // 2 values: seconds and nanoseconds or 1 value: seconds.
         const INT_4U dim( a_elem->getNData() );
         if( dim != 1 && dim != 2 )
         {
            ostringstream msg;
            msg << "ILWD array element representing time can have 1 or 2 elements: name="
                << elem->getNameString() << " dim=" << dim;
   
            throw SWIGEXCEPTION( msg.str() );   
         }
   
         data = getData( a_elem, '.' );
         break;
      }
      default:
      {
         string msg( "ILWD element representing time must be of INT_4U or String type: name=" );
         msg += elem->getNameString();
   
         throw SWIGEXCEPTION( msg );
      }   
   }

   
   time_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ), 
                            LIGO_LW::XML_STRING( type ) );
   
   DOMText* text_elem( mDocument->createTextNode( LIGO_LW::XML_STRING( data.c_str() ) ) );

   time_elem->appendChild( text_elem );
   mCurrentParent->appendChild( time_elem );
   
   return;
}
   
   
//-----------------------------------------------------------------------------
//
//: Sets metadata attributes.
//
// This method sets metadata attributes for the current element in the document.
// It parses ILWD 'metadata' attribute for element specific values, and sets
// appropriate attributes for corresponding LIGO_LW element.  
// 
//!param: const char* name - Name of the LIGO_LW element.
//!param: LdasElement* elem - A pointer to the ILWD format element.
//
//!return: Nothing.
//
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: XML_format_error - Illegal XML format.
//!exc: std::range_error( unable to retrieve metadata ) - Requested 
//+     metadata doesn't exist for ILWD format element.   
//   
void  LWDocument::setMetadata( const CHAR* const name, 
   const ILwd::LdasElement* const elem )
{
   LWMetadata::iterator m_iter( mMetadata.find( name ) );
   
   if( m_iter == mMetadata.end() )
   {
      return;
   }

   if( mCurrentParent->getNodeType() != DOMNode::ELEMENT_NODE )
   {
      // Can't set attributes for non DOMElement object.   
      return;
   }
   
   DOMElement* parent_elem( dynamic_cast< DOMElement* >( mCurrentParent ) );
   
   MetadataHash* h( m_iter->second );

   for( MetadataHash::iterator iter = h->begin(), end_iter = h->end();
        iter != end_iter; ++iter )
   {
      try
      {
         parent_elem->setAttribute( LIGO_LW::XML_STRING( iter->second ), 
                                    LIGO_LW::XML_STRING( elem->getMetadata<std::string>( iter->first ).c_str() ) );  
      }
      catch( const exception& exc )
      {
         string msg( "Error extracting " );
         msg += iter->first;
         msg += " for ";
         msg += name;
         msg += ": ";
         msg += exc.what();
   
         throw SWIGEXCEPTION( msg.c_str() );
      }
   }
   
   return;
}

   
//-----------------------------------------------------------------------------
//
//: Create Dimension element.
//
// This method creates dimension elements of the following format:
// <Dim name="name">value</Dim>, and appends them to the current node 
// of the LIGO_LW document tree.
// 
//!param: LdasArrayBase* elem - A pointer to the ILWD element which           +
//!param:    dimension attribute will be converted into LIGO_LW Dim elements.
//
//!return: Nothing.
//   
//!exc: DOMException - Invalid XML format is specified.   
//!exc: std::bad_alloc - Error allocating the memory.   
//!exc: SwigException - ILWD library exception.   
//
void LWDocument::makeDim( const ILwd::LdasArrayBase* const elem )
{
    if( elem == 0 )
    {
       return;
    }

    const size_t size( elem->getNDim() );
    if( size )
    {
       ostringstream ss;
       ss.precision( REAL_8_DIGITS + 1 );
       ss.setf( ios::scientific, ios::floatfield );        
   
       ostringstream dim_ss;
       REAL_8 startx( defaultStartX );
       REAL_8 scalex( defaultScaleX );
   
       for( size_t i = 0; i < size; ++i )
       {
          DOMElement* dim_elem = mDocument->createElement( 
                                    LIGO_LW::XML_STRING( LIGO_LW::dimTag ) );

          // LIGO_LW Dim element should have Name attribute,
          // but ILWD dim attribute does not have it ===> just
          // skip Name attribute for now.
          if ( elem->getUnits( i ).empty() == false )
          {
             dim_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::unitAttName ),
                                     LIGO_LW::XML_STRING( elem->getUnits( i ).c_str() ) );
          }

          // Convert startx value into the string   
          startx = elem->getStartX( i );
          if( startx != defaultStartX )
          {

             ss.str( "" );
             ss << startx;
             dim_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::startAttName ), 
                                     LIGO_LW::XML_STRING( ss.str().c_str() ) );
          }
   
          // Convert dx value into the string
          scalex = elem->getDx( i );
          if( scalex != defaultScaleX )      
          {
             ss.str( "" );
             ss << scalex;
             dim_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::scaleAttName ), 
                                     LIGO_LW::XML_STRING( ss.str().c_str() ) );   
          }
   

          // Convert dimension value into the string
          dim_ss.str( "" );
          dim_ss << elem->getDimension( i );

          DOMText* value_elem = mDocument->createTextNode(
                                   LIGO_LW::XML_STRING( dim_ss.str().c_str() ) );
          dim_elem->appendChild( value_elem );
          mCurrentParent->appendChild( dim_elem );   
       }
    }
    else
    {
       // The array is empty
       DOMElement* dim_elem = mDocument->createElement( 
                                 LIGO_LW::XML_STRING( LIGO_LW::dimTag ) );

       // LIGO_LW Dim element should have Name attribute,
       // but ILWD dim attribute does not have it, so just
       // skip Name attribute for now.
       // Units are not available for zero-dimensional arrays

       // String representation of dimension value
       const XMLCh* const dim_str( 0 );

       DOMText* value_elem = mDocument->createTextNode( dim_str );
       dim_elem->appendChild( value_elem );
       mCurrentParent->appendChild( dim_elem );      
    }
   
    return;
}
   

//-----------------------------------------------------------------------------
//
//: Insert data from ILWD Array element into LIGO_LW Stream element.
//
// This method extracts raw data from ILWD format array element and inserts   
// it into current LIGO_LW Stream element.
//
//!param: LdasElement* p - A pointer to the ILWD array element which       +
//!param:    contains the data.
//
//!return: Nothing.
//
//!exc: "insertData: unsupported_data_type" - Undefined ILWD array type.
//!exc: DOMException - Invalid XML format is specified.   
//!exc: std::bad_alloc - Error allocating the memory.   
//
void LWDocument::insertData( const ILwd::LdasArrayBase* const p ) 
{
   if( p == 0 )
   {
      throw runtime_error( "LWDocument::insertData(): NULL ILWD::LdasArrayBase element" );
   }


   // A bit of overhead if Encoding is Text, oh well...
   Style style;
   style.setWriteFormat( static_cast< ILwd::Format >( mStreamFormat ) );
   size_t dest_size( 0 );
   
   
   string data;
   switch( p->getElementId() )
   {
      case (ID_CHAR):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< CHAR >(
               ( dynamic_cast< const LdasArray< CHAR >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {
            data = getArrayData( dynamic_cast< const LdasArray< CHAR >* >( p ), ',' );       
         }
         break;
      }
      case (ID_CHAR_U):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< CHAR_U >( 
               ( dynamic_cast< const LdasArray< CHAR_U >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {   
            data = getArrayData( dynamic_cast< const LdasArray< CHAR_U >* >( p ), ',' );       
         }
         break;
      }
      case (ID_INT_2S):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< INT_2S >( 
               ( dynamic_cast< const LdasArray< INT_2S >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {      
            data = getData( dynamic_cast< const LdasArray< INT_2S >* >( p ), ',' );       
         }
         break;
      }
      case (ID_INT_2U):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< INT_2U >( 
               ( dynamic_cast< const LdasArray< INT_2U >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {         
            data = getData( dynamic_cast< const LdasArray< INT_2U >* const >( p ), ',' );       
         }
         break;
      }
      case (ID_INT_4S):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< INT_4S >( 
               ( dynamic_cast< const LdasArray< INT_4S >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {            
            data = getData( dynamic_cast< const LdasArray< INT_4S >* >( p ), ',' );       
         }
         break;
      }
      case (ID_INT_4U):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< INT_4U >( 
               ( dynamic_cast< const LdasArray< INT_4U >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {            
            data = getData( dynamic_cast< const LdasArray< INT_4U >* >( p ), ',' );       
         }
         break;
      }
      case (ID_INT_8S):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< INT_8S >( 
               ( dynamic_cast< const LdasArray< INT_8S >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {               
            data = getData( dynamic_cast< const LdasArray< INT_8S >* >( p ), ',' );       
         }
         break;
      }
      case (ID_INT_8U):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< INT_8U >( 
               ( dynamic_cast< const LdasArray< INT_8U >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {               
            data = getData( dynamic_cast< const LdasArray< INT_8U >* >( p ), ',' );       
         }
         break;
      }
      case (ID_REAL_4):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< REAL_4 >( 
               ( dynamic_cast< const LdasArray< REAL_4 >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {               
            data = getData( dynamic_cast< const LdasArray< REAL_4 >* >( p ), ',' );       
         }
         break;
      }
      case (ID_REAL_8):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< REAL_8 >( 
               ( dynamic_cast< const LdasArray< REAL_8 >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {                  
            data = getData( dynamic_cast< const LdasArray< REAL_8 >* >( p ), ',' );       
         }
         break;
      }
      case (ID_COMPLEX_8):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< COMPLEX_8 >( 
               ( dynamic_cast< const LdasArray< COMPLEX_8 >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {                  
            data = getData( dynamic_cast< const LdasArray< COMPLEX_8 >* >( p ), ',' );       
         }
         break;
      }
      case (ID_COMPLEX_16):
      {
         if( mStreamFormat != ILwd::ASCII )
         {
            // Format the data
            ArrayJanitor< CHAR > buf( style.convertToFormat< COMPLEX_16 >( 
               ( dynamic_cast< const LdasArray< COMPLEX_16 >* const >( p ) )->getData(),
               p->getNData(), &dest_size ) );   
   
            data = buf.get();
         }   
         else
         {                     
            data = getData( dynamic_cast< const LdasArray< COMPLEX_16 >* >( p ), ',' );       
         }
         break;
      }   
      default:
      {
         throw SWIGEXCEPTION( "insertData: unsupported data type" );
         break;
      }
   }

   
   DOMText* data_elem = mDocument->createTextNode(
                           LIGO_LW::XML_STRING( data.c_str() ) );
   mCurrentParent->appendChild( data_elem );
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Insert specified element of ILWD Array element into LIGO_LW Stream element.
//
// This method extracts specified by index element from ILWD array element      
// and inserts it into output string stream.
//
//!param: LdasElement* p - A pointer to the ILWD array element which         +
//!param:    contains the data.
//!param: size_t i - Index to specify array element.
//!param: std::ostringstream oss - Stream stream to output the specified element to.
//
//!return: Nothing.
//
//!exc: "insertData(i) - unsupported_data_type" - Undefined ILWD Array type.
//!exc: out_of_range_index - Array index is out of range.   
//
void LWDocument::insertData( const ILwd::LdasArrayBase* const p, size_t i,
   std::ostringstream& oss ) 
{
   switch( p->getElementId() )
   {
      case (ID_CHAR):
      {
         CHAR temp( getData( dynamic_cast< const LdasArray< CHAR >* >( p ), i ) );
         if( isprint( temp ) )
         {
            oss << dquoteChar;
            if(  temp == bslashChar ||
                 temp == ',' )
            {
               oss << bslashChar;
            }
            oss << temp << dquoteChar;
         }
         else
         {
            oss << '\\' << setw( 3 ) << setfill( '0' ) << oct 
                << static_cast< INT_2S >( temp ) << dec;
         }
         break;
      }
      case (ID_CHAR_U):
      {
         CHAR_U temp( getData( 
                         dynamic_cast< const LdasArray< CHAR_U >* >( p ), i ) );
         oss << '\\' << setw( 3 ) << setfill( '0' ) << oct 
             << static_cast< INT_2S >( temp ) << dec;   
         break;
      }
      case (ID_INT_2S):
      {
         oss << getData( dynamic_cast< const LdasArray< INT_2S >* >( p ), i );       
         break;
      }
      case (ID_INT_2U):
      {
         oss << getData( dynamic_cast< const LdasArray< INT_2U >* >( p ), i );       
         break;
      }
      case (ID_INT_4S):
      {
         oss << getData( dynamic_cast< const LdasArray< INT_4S >* >( p ), i );       
         break;
      }
      case (ID_INT_4U):
      {
         oss << getData( dynamic_cast< const LdasArray< INT_4U >* >( p ), i );       
         break;
      }
      case (ID_INT_8S):
      {
         oss << getData( dynamic_cast< const LdasArray< INT_8S >* >( p ), i );       
         break;
      }
      case (ID_INT_8U):
      {
         oss << getData( dynamic_cast< const LdasArray< INT_8U >* >( p ), i );       
         break;
      }
      case (ID_REAL_4):
      {
         ostringstream s;
         s.precision( REAL_4_DIGITS + 1 );
         s.setf( ios::scientific, ios::floatfield );   
   
         s << getData( dynamic_cast< const LdasArray< REAL_4 >* >( p ), i );       
         oss << s.str();

         break;
      }
      case (ID_REAL_8):
      {
         ostringstream s;
         s.precision( REAL_8_DIGITS + 1 );
         s.setf( ios::scientific, ios::floatfield );   
   
         s << getData( dynamic_cast< const LdasArray< REAL_8 >* >( p ), i );       
         oss << s.str();

         break;
      }
      case (ID_COMPLEX_8):
      {
         ostringstream s;
         s.precision( REAL_4_DIGITS + 1 );
         s.setf( ios::scientific, ios::floatfield );   
   
         s << getData( dynamic_cast< const LdasArray< COMPLEX_8 >* >( p ), i );       
         oss << s.str();

         break;
      }
      case (ID_COMPLEX_16):
      {
         ostringstream s;
         s.precision( REAL_8_DIGITS + 1 );
         s.setf( ios::scientific, ios::floatfield );   
   
         s << getData( dynamic_cast< const LdasArray< COMPLEX_16 >* >( p ), i );       
         oss << s.str();

         break;
      }   
      default:
      {
         throw SWIGEXCEPTION( "insertData(i): unsupported_data_type" );
         break;
      }
   }
   
   return;
}


//------------------------------------------------------------------------------
//
//: Get data from ILWD format element( to use it as a Table element data ).
//
// This method gets data from ILWD format element.
//
//!param: LdasElement* elem - ILWD format element containing the data.
//
//!return: std::string - Data extracted from ILWD format element.
//
//!exc: empty_table - ILWD element representing the table is empty.
//!exc: wrong_element_type - ILWD External element is used as child node of    +
//!exc:    the table element.
//!exc: different_element_size - Table elements have different sizes.
//!exc: unsupported_table_element - ILWD containers within Table elements can  +
//!exc:    contain "char" and "char_u"
//!exc: std::bad_cast - Invalid ILWD format is specified.
//!exc: std::bad_alloc - Memory allocation failed.
//   
const std::string LWDocument::getTableData( const ILwd::LdasElement* const elem )
{
   if( elem == 0 )
   {
      return "";
   }
   
   if( elem->getElementId() != ID_ILWD )
   {
      throw runtime_error( "LWDocument::getTableData() can be called for ILWD"
                               " format container only." );
   }
   
   const LdasContainer& cont( dynamic_cast< const LdasContainer& >( *elem ) );
   
   if( cont.size() == 0 )
   {
      return "";
   }

   LdasContainer::const_iterator iter( cont.begin() ),
                                 end_iter( cont.end() );
   
   //  Get the dimension of first container element   
   size_t dim( getSize( *iter ) );
   ++iter;
   
   bool alldims( true );
   
   
   while( iter != end_iter && alldims )
   {
      alldims = alldims && ( getSize( *iter ) == dim );
      ++iter;
   }

   
   if( !alldims ) 
   { 
      string msg( "Different column size for table data: " );
      msg += elem->getNameString();
   
      throw SWIGEXCEPTION( msg );
   }
   
   if( dim == 0 )
   {
      return "";
   }

   
   ostringstream oss;
   LdasContainer::const_iterator begin_iter( cont.begin() );
   
   
   // Step through all elements of container:
   for( size_t i = 0; i < dim; ++i )
   {
      for( iter = cont.begin(); iter != end_iter; ++iter )
      {
         if ( ( i != 0 ) || ( iter != begin_iter ) )
         {
            oss << ',';
         }
   
         ILwd::ElementId type = ( *iter )->getElementId();
         switch( type )
         {
            case (ID_LSTRING):
            {
               // Check if i-th element of the lstring is a valid entry
               if( ( *iter )->isNull( i ) == false )
               {
                  oss << '\"' << getData( 
                         dynamic_cast< const LdasString* >( *iter), i, ',' ) 
                      << '\"';
               }
               break;
            }
            case(ID_ILWD):
            {
               // Access [ i ] element of the container.
               const LdasContainer* const cc( 
                        dynamic_cast< const LdasContainer* const >( *iter ) );
   
               // Check if i-th element of the container is a valid entry
               if( cc->isNull( i ) == false )
               {
                  if( ( *cc )[ i ] == 0 )
                  {
                     string msg( "NULL element when nullmask is not NULL for "
                                 "ILWD format container: " );
                     msg += cc->getNameString();
                     throw runtime_error( msg );
                  }
   
                  const LdasArrayBase& p(
                           dynamic_cast< const LdasArrayBase& >( *( *cc )[ i ] ) );
                  switch( p.getElementId() )
                  {
                     case(ID_CHAR):
                     {
                        oss << '\"' << getData( 
                           dynamic_cast< const LdasArray< CHAR >* >( &p ), ' ' ) 
                            << '\"';
                        break;
                     }
                     case(ID_CHAR_U):
                     {
                        oss << '\"' << getData( 
                           dynamic_cast< const LdasArray< CHAR_U >* >( &p ), ' ' ) 
                            << '\"';
                        break;
                     }
                     default:
                     {
                        string msg( "Unsupported table element type is specified in ILWD "
                                    " format container element with name: " );
                        msg += p.getNameString();
   
                        throw SWIGEXCEPTION( msg );
                     }
                  }
               }
               break;
            }
            default:
            {
               // Check if i-th array element is a valid entry
               if( ( *iter )->isNull( i ) == false )
               {
                  // Support Array elements
                  insertData( dynamic_cast< const LdasArrayBase* >( *iter ),
                              i, oss );
               }
               break;
            }
         }
      }  // for( iter ) 
   }  // for( i )


   return oss.str();
}


//-----------------------------------------------------------------------------
//: Create LIGO_LW Stream element.
//
// This method creates LIGO_LW Stream element. For now Stream exists only as
// a child of LIGO_LW "Array" or "Table" element.
// 
//!param: LdasElement* elem - ILWD element which data will be converted        +
//!param:    into LIGO_LW Stream element.
//
//!return: Nothing
//
//!exc: See exceptions for LWDocument::getTableData and LWDocument::insertData +
//!exc:    methods.
//!exc: std::bad_alloc - Error allocating the memory.   
//!exc: std::bad_cast - Invalid ILWD format.   
//!exc: DOMException - Invalid XML format is specified.
//!exc: invalid_input_format - Invalid ILWD format is specified.   
//
void LWDocument::makeStream( const ILwd::LdasElement* const elem )
{
   DOMElement* stream_elem = mDocument->createElement( 
                                LIGO_LW::XML_STRING( LIGO_LW::streamTag ) );

   
   // Add name to Stream element( for now stream will have the
   // same name as element it belongs to ).
   addName( stream_elem, elem->getNameString() );

   // Set other attributes
   
   // Delimiter is relevant only if it's Table's Stream or
   // Encoding is a "Text"
   if( getLwType( mCurrentParent ) == LW_TABLE_ID ||
       strcmp( mFormatStringHash[ mStreamFormat ], LIGO_LW::textEncodingAttValue ) == 0 )
   {
      stream_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::delimiterAttName ),
                                 LIGO_LW::XML_STRING( "," ) );
   }
   
   stream_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ), 
                              LIGO_LW::XML_STRING( LIGO_LW::localTypeAttValue ) );
   
   mCurrentParent->appendChild( stream_elem );


   if( getLwType( mCurrentParent ) == LW_TABLE_ID )
   {
      const string temp( getTableData( elem ) );
      if( !temp.empty() )
      {
         DOMText* data_elem = mDocument->createTextNode( 
                                 LIGO_LW::XML_STRING( temp.c_str() ) );
         stream_elem->appendChild( data_elem );
      }
   }
   else
   {
      // Save current parent
      mNodeStack.push_back( mCurrentParent );
      mCurrentParent = stream_elem;
   
      // Get data from ILWD Array element, encode if mStreamFormat 
      // is not ILwd::ASCII
      insertData( dynamic_cast< const LdasArrayBase* >( elem ) );
   
      // Set encoding attribute, default is "Text" + endianness
      string encode( mFormatStringHash[ mStreamFormat ] );
      encode += ',';   
   
#ifdef WORDS_BIGENDIAN
      encode += LIGO_LW::bigEndianEncodingAttValue;
#else
      encode += LIGO_LW::littleEndianEncodingAttValue;
#endif      
         
      stream_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::encodingAttName ),
                                 LIGO_LW::XML_STRING( encode.c_str() ) );   
   
      // Restore the parent
      mCurrentParent = mNodeStack.back();
      mNodeStack.pop_back();
   }
   
   return;
}
   

//-----------------------------------------------------------------------------
//: Create LIGO_LW Array element.
//
// This method creates LIGO_LW Array element.
//
//!param: LdasArrayBase* elem - ILWD element to be converted into LIGO_LW     +
//!param:    Array element.
//
//!return: Nothing.
//
//!exc: DOMException - Invalid XML format is specified.   
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: std::bad_cast - Invalid ILWD format is specified.   
//   
void LWDocument::makeArray( const ILwd::LdasArrayBase* const elem )
{
   if( elem == NULL )
   {
      return;
   }

   DOMElement* array_elem = mDocument->createElement( 
                               LIGO_LW::XML_STRING( LIGO_LW::arrayTag ) );
   
   addName( array_elem, elem->getNameString() );

   mCurrentParent->appendChild( array_elem );

   // Array element becomes a CurrentParent in scope 
   // of this function.
   mNodeStack.push_back( mCurrentParent );
   mCurrentParent = array_elem;

   // Add dimension elements into array
   makeDim( elem );
    
   if( elem->getIdentifier().empty() == false )
   {
      array_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ),
                                LIGO_LW::XML_STRING( elem->getIdentifier().c_str() ) );
   }

   // Add data units
   string data_units( elem->getDataValueUnit() );
   if( data_units.empty() == false )
   {
      array_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::unitAttName ), 
                                LIGO_LW::XML_STRING( data_units.c_str() ) );
   }
   
   // Insert data into Array element by creating an
   // internal Stream element
   makeStream( elem );
   
   // Restore the parent
   mCurrentParent = mNodeStack.back();
   mNodeStack.pop_back();
   
   return;
}
   

//-----------------------------------------------------------------------------
//: Create LIGO_LW container element..
//
// This method creates LIGO_LW container element of specified type.
//
//!param: LdasElement* elem - A pointer to the ILWD element to be converted    +
//!param:    into LIGO_LW element.
//!param: const char* name - Type of the container element to create. 
//+       Default is 'LIGO_LW'.   
//
//!return: bool - True if element was created, false otherwise.
//
//!exc: std::exception   
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: SwigException   
//!exc: DOMException - Invalid XML format is specified.   
//   
bool LWDocument::makeLigoLw( const ILwd::LdasElement* const elem,
   const bool is_root, const CHAR* const name ) 
{
   DOMElement* lw_elem( 0 );   
   
   
   // Don't create new LIGO_LW element if "root" element is being 
   // converted
   if( is_root == true &&                        // root element and 
       strcmp( name, LIGO_LW::ligoLwTag ) == 0 ) // LIGO_LW would need to be generated
   {
      lw_elem = dynamic_cast< DOMElement* >( mCurrentParent );
   }
   else
   {
      // Create container element in LW format document: 
      lw_elem = mDocument->createElement( LIGO_LW::XML_STRING( name ) );
   
      mCurrentParent->appendChild( lw_elem );
   
      mNodeStack.push_back( mCurrentParent );
      mCurrentParent = lw_elem;
   }

   
   addName( lw_elem, elem->getNameString() );

   // DTT support: set Type for all AdcData elements ===>
   // DTT knows what to do with it.
   if( ( name != 0 ) && ( strcasecmp( name, LIGO_LW::adcDataTag ) == 0 ) )
   {
      lw_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ),
                             LIGO_LW::XML_STRING( LIGO_LW::DTTTimeSeries ) );   
   }
   

   if( elem->getComment().empty() == false )
   {
      makeComment( elem );           
   }
   
   setMetadata( name, elem );
   return true;
}
   

//-----------------------------------------------------------------------------
//: Create LIGO_LW container element with a specified type.
//
// This method creates LIGO_LW container type element with specified type.
//
//!param: LdasElement* elem - A pointer to the ILWD element to be converted    +
//!param:    into LIGO_LW element.
//!param: const char* name - Type of the container element to create.
//
//!return: bool - True if element was created, false otherwise.
//
//!exc: std::bad_alloc - Error allocating the memory.
//!exc: DOMException - XML format error.
//   
bool LWDocument::makeLigoLwType( const ILwd::LdasElement* const elem,
   const bool is_root, const CHAR* const type ) 
{
   DOMElement* lw_elem( 0 );
   
   // Don't create LIGO_LW container if root element is being converted
   if( is_root )
   {
      lw_elem = dynamic_cast< DOMElement* >( mCurrentParent );
   }
   else
   {
      // Create container element in LW format document: 
      lw_elem = mDocument->createElement( 
                               LIGO_LW::XML_STRING( LIGO_LW::ligoLwTag ) );
   
      mCurrentParent->appendChild( lw_elem );
      mNodeStack.push_back( mCurrentParent );
   
      mCurrentParent = lw_elem;
   }

   
   addName( lw_elem, elem->getNameString() );
   
   
   if( ( type != 0 ) && ( isalpha( *type ) ) )
   {
      lw_elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::typeAttName ),
                             LIGO_LW::XML_STRING( type ) );   
   }
   

   if( elem->getComment().empty() == false )
   {
      makeComment( elem );           
   }
   
   return true;
}
   

//-----------------------------------------------------------------------------
//: Insert an element into LIGO_LW document.
//
// This method inserts a new LIGO_LW element into document tree. A decision 
// on what kind of LIGO_LW element should be generated next is based on  
// hashing name fields of the ILWD format element.
//
//!param: LdasElement *elem - A pointer to the ILWD format element to convert  +
//!param:    into LIGO_LW format.
//!param: const bool is_root - A flag to indicate that root element of the object
//+       is being converted into XML format.   
//
//!return: Nothing
//
//!exc: unsupported_element_EXTERNAL - ILWD External element is not supported  +
//!exc:    in LIGO_LW format.
//!exc: std::bad_alloc - Memory allocation failed.
//!exc: std::bad_cast - Invalid input ILWD format is specified.   
//
void LWDocument::startElement( const ILwd::LdasElement* const elem,
   const bool is_root ) 
{
   if( elem == 0 )
   {
      return;
   }
 
    
   switch ( elem->getElementId() )
   {
      case (ID_ILWD):
      {
         bool found( false );

   
         // Check if element represents one of the containers in 
         // mILWDContainerHash
         for( LWHash::const_iterator iter = mILwdContainerHash.begin(),
              end_iter = mILwdContainerHash.end();
              !found && iter != end_iter; ++iter )
         {
	    if( parseName( elem, (*iter).first.c_str( ) ) )
            {
               ( this->*( (*iter).second.second ) )( 
                          elem, is_root, (*iter).second.first );
               found = true;               
            }
         }

   
         LWHash::iterator end_iter( mILwdHash.end() );      
         const CHAR* tmp_str;   
   
         for( size_t i = 0, name_size = elem->getNameSize(); 
              !found && ( i < name_size ); ++i )
         {
            const string tmp( slower( elem->getName( i ) ) );
   
            // Check non-empty and non-XML names only:
            if( tmp.size() && tmp != LIGO_LW::XMLTag )
            {
               tmp_str = tmp.c_str();
   
               if( mILwdHash.find( tmp_str ) != end_iter )
               {
                  LWHash::mapped_type& type( mILwdHash[ tmp_str ] );   
   
                  ( this->*( type.second ) )( elem, is_root, type.first );
                  found = true;
               }
            }
         }

   
         if( !found )
         {
            makeLigoLw( elem, is_root ); 
         }
         break;
      }
 
      case (ID_LSTRING):
      {
         // Hash based on CurrentParent type:
         switch( getLwType( mCurrentParent ) )
         {
            case( LW_TABLE_ID ):
            {
               makeColumn( elem );
               break;
            }
            default: 
            {
               if( parseName( elem, LIGO_LW::commentTag ) )
               {
                  makeComment( elem );
                  break;
               }
               else if( parseName( elem, LIGO_LW::timeTag ) )
               {
                  makeTime( elem );
                  break;
               }
    
               // Make Param element
               makeParam( dynamic_cast< const LdasString* >( elem ) );
               break;
            }
         }
         break;
      }
 
      case (ID_EXTERNAL):
      {
         throw SWIGEXCEPTION( "unsupported_element_EXTERNAL" );
         break;
      }
 
      // All arrays will be handled here:
      default:
      {
         // Hash based on CurrentParent type:
         switch( getLwType( mCurrentParent ) )
         {
            case( LW_TABLE_ID ):
            {
               // Arrays within Table element should be converted to Column+Stream
               // elements.
               makeColumn( elem );
               break;
            }
            default: 
            {
               // timeOffset is of REAL_8 type ---> will have problems
               // when converting Time to ILWD format: right now it's 
               // converted only to the INT_4U 2-dim array
               if( //parseName( elem, "timeoffset" ) || 
                   parseName( elem, "gtime" ) ||
                   parseName( elem, "time" ) ||   
                   parseName( elem, "starttime" ) ||
                   parseName( elem, "endtime" ) )
               {
                  makeTime( elem );
                  break;
               }

               const LdasArrayBase* bb( 
                  dynamic_cast< const LdasArrayBase* >( elem ) );
   
               // Arrays with one element should be converted to Param,
               // and FrDetector::prefix (char[2])
               if( bb->getNData() == 1 ||
                   ( parseName( elem, LIGO_LW::detectorPrefix ) &&
                     elem->getElementId() == ILwd::ID_CHAR ) )        
               {
                  makeParam( bb );
               }
               else
               {
                  makeArray( bb );
               }
   
               break;
            }
         }
         break;
      }
   }
 
   return;
}


//-----------------------------------------------------------------------------
//
//: Finish an element in LIGO_LW document.
//
//!param: LdasElement *elem - A pointer to the ILWD format element to finish   +
//!param:    in the LIGO_LW document.
//
//!return: Nothing.
//
void LWDocument::endElement( const ILwd::LdasElement* const elem,
   const bool is_root )
{
   switch ( elem->getElementId() )
   {
      case (ID_ILWD):
      {
         if( is_root == false ||      // This is not root element conversion OR
             mNodeStack.size() == 2 ) // it's root element with child container
         {
            mCurrentParent = mNodeStack.back();
            mNodeStack.pop_back();
         }
    
         break;
      }
      default:
      {
         // Do nothing for all other than LIGO_LW container elements
         break;
      }
   }
 
   return;
}


template<>
const std::string LWDocument::getData( const ILwd::LdasArray< CHAR >* const elem, 
   const CHAR& delimiter )
{
    const CHAR* buffer = elem->getData(); 
    if( buffer == 0 )
    {
       return "";
    }   
   
    // How many elements are in the array
    const INT_4U num_of_elem( elem->getNData() ); 
   
    if( num_of_elem == 0 )
    {
       return "";
    }
   
    bool all_printable( true );   
    INT_4U i( 0 );
    const CHAR* buf_iter( buffer );
   
    while( all_printable && ( i < num_of_elem ) )
    {
       all_printable = all_printable && isprint( *buf_iter );
       ++buf_iter;
       ++i;
    }
   
   
    bool has_printable( false );
    buf_iter = buffer;
    i = 0;
    
    if( !all_printable )
    {
       while( !has_printable && ( i < num_of_elem ) )
       {
          has_printable = has_printable | isprint( *buf_iter );
          ++buf_iter;
          ++i;
       }
    }

   
    // Escape delimiter, bslashChar and comma( stream separator )
    string escape( 1, delimiter );
    escape += bslashChar;
    if( delimiter != ',' )
    {
       escape += ',';
    }
   
    ostringstream ss;
    ss.setf( ios::oct, ios::basefield );   
    buf_iter = buffer;
   
    for( INT_4U i = 0; i < num_of_elem; ++i )
    {
       if( i && has_printable )
       {
          ss << delimiter;
       }
   
       // Only if nullmask to be supported in all LIGO_LW 
       // elements!!!   
       //if( elem->isNull( i ) == false )
       //{
       if( isprint( *buf_iter ) )
       {
          if( escape.find_first_of( *buf_iter ) != string::npos )
          {
             ss << bslashChar;
          }
          ss << *buf_iter;   
       }
       else
       {
          ss << '\\' << setw( 3 ) << setfill( '0' ) 
             << static_cast< INT_2S >( *buf_iter );
       }
       //}
       ++buf_iter;
    }

    return ss.str();
}


template<>
const std::string LWDocument::getData( 
   const ILwd::LdasArray< CHAR_U >* const elem,
   const CHAR& delimiter )
{
    const CHAR_U* buffer = elem->getData(); 
    if( buffer == 0 )
    {
       return "";
    }

    const INT_4U num_of_elem( elem->getNData() ); 
   
    if( num_of_elem == 0 )
    {
       return "";
    }
    
    ostringstream ss;
    ss.setf( ios::oct, ios::basefield );
    for( INT_4U i = 0; i < num_of_elem; ++i, ++buffer )
    {
       // Binary data will not have a data delimiter
       // Only if nullmask to be supported in all LIGO_LW 
       // documents!!!   
       // if( elem->isNull( i ) == false )
       //{
       ss << '\\' << setw( 3 ) << setfill( '0' ) 
          << static_cast< INT_2U >( *buffer );   
       //}
    }

    return ss.str();
}
   
   
template<>
const std::string LWDocument::getData(
   const ILwd::LdasArray< COMPLEX_8 >* const elem,
   const CHAR& delimiter )
{
    const COMPLEX_8* buffer = elem->getData(); 
    if( buffer == 0 )
    {
        return "";
    }

    const INT_4U num_of_elem( elem->getNData() ); 
   
    if( num_of_elem == 0 )
    {
       return "";
    }
   
    ostringstream ss;
    ss.precision( REAL_4_DIGITS + 1 );
    ss.setf( ios::scientific, ios::floatfield );    
   
    ss << buffer->real() << delimiter << buffer->imag();
    ++buffer;
    
    for( INT_4U i = 1; i < num_of_elem; ++i, ++buffer )
    {
       ss << delimiter << buffer->real() << delimiter << buffer->imag();
    }
   
    return ss.str();
}   
   
   
template<>
const std::string LWDocument::getData(
   const ILwd::LdasArray< COMPLEX_16 >* const elem,
   const CHAR& delimiter )
{
    const COMPLEX_16* buffer = elem->getData(); 
    if( buffer == 0 )
    {
        return "";
    }

    const INT_4U num_of_elem( elem->getNData() ); 
   
    if( num_of_elem == 0 )
    {
       return "";
    }

    ostringstream ss;
    ss.precision( REAL_8_DIGITS + 1 );
    ss.setf( ios::scientific, ios::floatfield );    
   
    ss << buffer->real() << delimiter << buffer->imag();
    ++buffer;
   
    for( INT_4U i = 1; i < num_of_elem; ++i, ++buffer )
    {
       ss << delimiter << buffer->real() << delimiter << buffer->imag();
    }
   
    return ss.str();
}      

   
template<>
const std::string LWDocument::getArrayData( 
   const ILwd::LdasArray< CHAR >* const elem, 
   const CHAR& delimiter )
{
    const CHAR* buffer = elem->getData(); 
    if( buffer == 0 )
    {
       return "";
    }   
   
    // How many elements are in the array
    const INT_4U num_of_elem( elem->getNData() ); 
   
    if( num_of_elem == 0 )
    {
       return "";
    }
   
    // Escape bslashChar and comma( stream separator )
    string escape( 1, delimiter );
    escape += bslashChar;
    if( delimiter != ',' )
    {
       escape += ',';
    }
   
    ostringstream ss;
    ss.setf( ios::oct, ios::basefield );   
   
    for( INT_4U i = 0; i < num_of_elem; ++i, ++buffer )
    {
       if( i )
       {
          ss << delimiter;
       }
       // Only if nullmask to be supported in all LIGO_LW 
       // elements!!!   
       //if( elem->isNull( i ) == false )
       //{
       if( isprint( *buffer ) )
       {
          if( escape.find_first_of( *buffer ) != string::npos )
          {
             ss << bslashChar;
          }
          ss << *buffer;   
       }
       else
       {
          ss << '\\' << setw( 3 ) << setfill( '0' ) 
             << static_cast< INT_2S >( *buffer );
       }
       //}
    }

    return ss.str();   
}

template<>
const std::string LWDocument::getArrayData( 
   const ILwd::LdasArray< CHAR_U >* const elem, 
   const CHAR& delimiter )
{
    const CHAR_U* buffer = elem->getData(); 
    if( buffer == 0 )
    {
        return "";
    }

    const INT_4U num_of_elem( elem->getNData() ); 
   
    if( num_of_elem == 0 )
    {
       return "";
    }
    
    ostringstream ss;
    ss.setf( ios::oct, ios::basefield );

    // Output first data element
    ss << '\\' << setw( 3 ) << setfill( '0' ) 
       << static_cast< INT_2U >( *buffer );   
    ++buffer;   
   
    for( INT_4U i = 1; i < num_of_elem; ++i, ++buffer )
    {
       // Binary data will not have a data delimiter
       // Only if nullmask to be supported in all LIGO_LW 
       // documents!!!   
       // if( elem->isNull( i ) == false )
       //{
       ss << delimiter
          << '\\' << setw( 3 ) << setfill( '0' ) 
          << static_cast< INT_2U >( *buffer );   
       //}
    }

    return ss.str();   
}
   

//   
//!exc: out_of_range_index - Element index is out of range.
//   
template< class T >
const T LWDocument::getData( 
   const ILwd::LdasArray< T >* const elem, const size_t i ) 
{
   if( i >= elem->getNData() )
   {
      throw SWIGEXCEPTION( "LWDocument::getData< T >(): out of range data index" );
   }
   
   const T* buffer = elem->getData();  
   if( buffer == 0 )
   {
      return 0;
   }    
   return ( *( buffer + i ) );
}


//   
//!exc: out_of_range_index - Element index is out of range.
//      
const std::string LWDocument::getData( 
   const LdasString* const elem, const size_t i, const CHAR& s )
{
   if( elem == 0 )
   {
      throw runtime_error( "LWDocument::getData< LdasString >(): NULL element." );
   }
   
   if( i >= elem->size() )
   {
      throw SWIGEXCEPTION( "LWDocument::getData< LdasString >(): out of range data index" );
   }
    
   string temp( ( *elem )[ i ] );
   string::size_type pos( 0 ); 
   
   // Escape only stream data delimiter: 
   string escape( 1, s );
    
   size_t size( temp.size() - 2 );   
   
   while( ( pos = temp.find_first_of( escape, pos ) ) != string::npos )
   {
       temp.insert( pos, 1, bslashChar );
       if( pos <= size )
       {  
          // Skip over the escaped character
          pos += 2;
          ++size;
       }
       else
       {
          pos = string::npos;
       }
   }
 
   return temp;
}

  
//-----------------------------------------------------------------------------
//
//: Add name attribute to the element.
//
// This function adds Name attribute to the LIGO_LW element:
// <elem name="elem_name"..../>
// 
//!param: elem - LIGO_LW element to add name attribute to.
//!param: name - Name value.
//
//!return: Nothing.
//
//!exc: DOMException - XML format error occured.
//!exc: std::bad_alloc - Memory allocation failed.   
//    
void addName( DOMElement* elem, const std::string& name )
{
   if( name.empty() )
   {
      return;
   }
   
   elem->setAttribute( LIGO_LW::XML_STRING( LIGO_LW::nameAttName ), 
                       LIGO_LW::XML_STRING( name.c_str() ) );
   
   return;
}
   
   
//-----------------------------------------------------------------------------
//
//: Parse name attribute of the element.
//
// This function parses Name attribute of the ILWD format element for specified
// name field, and returns true if that name value was found, false otherwise.
// 
//!param: elem - A pointer to the ILWD element.
//!param: nameValue - Name value to search for.
//
//!return: True if name was found, false otherwise.
//
bool parseName( const LdasElement* const elem, const CHAR* const nameValue )
{
   for( INT_4U i = 0, num_names = elem->getNameSize(); i < num_names; ++i )
   {
      if( strcasecmp( nameValue, elem->getName( i ).c_str() ) == 0 )
      {
         return true;
      }
   }
   
   return false;
}


//-----------------------------------------------------------------------------
//
// Get size of the element.
//
// This function gets the size( number of elements ) of the ILWD format element.
// 
//!param: const LdasElement* elem - A pointer to the ILWD element.
//
//!return:  size_t - Element size.
//
size_t LWDocument::getSize( const ILwd::LdasElement* const elem )
{
   if( elem == 0 )
   {
      throw runtime_error( "LWDocument::getSize(): NULL element" );
   }

   
   size_t dim( 0 );
   
   switch( elem->getElementId() )
   {
      case(ID_LSTRING):
      {
         dim = dynamic_cast< const LdasString* >( elem )->size();
         break;
      }
      case(ID_ILWD):
      {
         dim = dynamic_cast< const LdasContainer* >( elem )->size();
         break;
      }
      case(ID_EXTERNAL):
      {
        break;
      }
      default:
      {
         dim = dynamic_cast< const LdasArrayBase* >( elem )->getNData();
         break;
      }
   }

   return dim;
}
   
   
