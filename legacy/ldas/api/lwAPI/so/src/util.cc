#include "config.h"

#include "util.hh"

#if defined( __SUNPRO_CC )
//-----------------------------------------------------------------------
// When Xerces is compiled with the Solaris compiler, it is missing
//   these definitions. We create them here to prevent unresolved symbols
//-----------------------------------------------------------------------
int
stricmp(const char*const s1, const char*const s2 )
{
  return( strcasecmp( s1, s2 ) );
}

int
strnicmp( const char*const s1, const char*const s2, const unsigned n)
{
  return( strncasecmp( s1, s2, n ) );
}
#endif /* defined( __SUNPRO_CC ) */
