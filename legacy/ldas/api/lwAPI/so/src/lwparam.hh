#ifndef LwParamHH
#define LwParamHH

// System Header Files
#include <string>
#include <deque>
#include <new>
#include <memory>   

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// ILWD Header Files
#include <ilwd/ldasarray.hh>

// XML Header Files
#include <xercesc/dom/DOMNode.hpp>   
   
// Local Header Files
#include "lwutil.hh"
#include "convertstr.hh"
#include "query.hh"
#include "lwconsts.hh"   

extern const std::string param_xml_name;
   

// array2ILwd: converts LW format Array into ILWD format array.
std::unique_ptr< ILwd::LdasElement > param2ILwd( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node, 
   std::deque< Query > dq );


//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: SwigException
//   
template< class T >
std::unique_ptr< ILwd::LdasElement > param2ILwd( 
   const XERCES_CPP_NAMESPACE::DOMNode* const node ) 
{
    // There is going to be only one data item
    INT_4U nDim( 1 );    

    std::string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
    if( name == LIGO_LW::detectorPrefix )
    {
       nDim = 2;  
    }
   
    std::string units( nodeAttribute( node, LIGO_LW::unitAttName ) );
    std::string unity( nodeAttribute( node, LIGO_LW::dataUnitAttName ) );   
    std::unique_ptr< REAL_8 > dx( nodeAttribute< REAL_8 >( node, 
                                                         LIGO_LW::scaleAttName,
                                                         nDim ) );
    if( dx.get() == 0 )
    {
       dx.reset( new REAL_8[ nDim ] );
       dx.get()[ 0 ] = defaultScaleX;
    }

    std::unique_ptr< REAL_8 > startx( nodeAttribute< REAL_8 >( node,
                                                             LIGO_LW::startAttName,
                                                             nDim ) );
    if( startx.get() == 0 )
    {
       startx.reset( new REAL_8[ nDim ] );
       startx.get()[ 0 ] = defaultStartX;
    }
   
    const CHAR delimiter = ',';
    std::string value( nodeValue( node ) );
    if( value.empty() )
    {
       throw SWIGEXCEPTION( "param_data_is_missing" );
    }

    std::unique_ptr< T > sData( string2Data< T >( value, nDim, delimiter ) );

    if( name.find( param_xml_name ) == std::string::npos )
    {
       name.append( param_xml_name );
    } 

    
    const bool allocate( false );
    const bool own( true );
   
    std::unique_ptr< ILwd::LdasElement > a( new ILwd::LdasArray< T >( 
                                                 sData.release(), nDim, 
                                                 name, units,
                                                 allocate, own,
                                                 dx.get()[ 0 ], 
                                                 startx.get()[ 0 ],
                                                 unity ) );
   
    return a;
}


#endif
