#ifndef LwStreamHH
#define LwStreamHH

#include <deque>
#include <string>

#include "query.hh"

namespace XERCES_CPP_NAMESPACE  
{   
   class DOMNode;
}

   
bool stream2File( 
   XERCES_CPP_NAMESPACE::DOMNode* const node,
   std::deque< Query > dq,
   const std::string& urlAddr, const std::string& urlDir ); 

bool file2Stream( XERCES_CPP_NAMESPACE::DOMNode* const node );


#endif
