#ifndef LwCmdHH
#define LwCmdHH

// System Header Files
#include <string>

// LDASHeader Files
#include <genericAPI/util.hh>
#include <genericAPI/threaddecl.hh>
   
   
// Local Header Files
#include "lwconsts.hh"   

// Forward declaration
namespace ILwd
{
   class LdasElement;
}
   
class LWDocument;   
#if HAVE_LIBOSPACE
class os_tcp_socket;
#endif /* HAVE_LIBOSPACE */

   
//-----------------------------------------------------------------------------
// Accessors
//-----------------------------------------------------------------------------
const std::string getLwAttribute( const LWDocument* doc, const char* command );
const std::string getLWDocument( LWDocument* doc );


//-----------------------------------------------------------------------------
// I/O
//-----------------------------------------------------------------------------
LWDocument* readLwFile( const char* filename );
CREATE_THREADED1_DECL( readLwFile, LWDocument*, const char* );

void writeLwFile( LWDocument* doc, const char* filename );
CREATE_THREADED2V_DECL( writeLwFile, LWDocument*, const char* );

#if HAVE_LIBOSPACE
void sendLWDocument( os_tcp_socket* socket, LWDocument* doc );
CREATE_THREADED2V_DECL( sendLWDocument, os_tcp_socket*, LWDocument* );
#endif /* HAVE_LIBOSPACE */

#if HAVE_LIBOSPACE
LWDocument* recvLWDocument( os_tcp_socket* socket );
CREATE_THREADED1_DECL( recvLWDocument, LWDocument*, os_tcp_socket* );
#endif /* HAVE_LIBOSPACE */

   
//----------------------------------------------------------------------------
// Data manipulation
//----------------------------------------------------------------------------
const std::string putLwData( LWDocument* doc, const char* command, 
                             const char* urlAddr, const char* urlDir );

   
//----------------------------------------------------------------------------
// Document utilities
//----------------------------------------------------------------------------
void setLwExtReferenceFlag( const char* value );
const std::string getLwTOC( const LWDocument* doc, const char* command );


//-----------------------------------------------------------------------------
// Destruction
//-----------------------------------------------------------------------------

void destructLWDocument( LWDocument* doc );


//-----------------------------------------------------------------------------
// Conversion
//-----------------------------------------------------------------------------

LWDocument* ilwd2Lw( const ILwd::LdasElement* c, 
   const char* format = LIGO_LW::textEncodingAttValue );
CREATE_THREADED2_DECL( ilwd2Lw, LWDocument*, const ILwd::LdasElement*, 
   const char* );
   
LWDocument* ilwd2TimeSeries( const ILwd::LdasElement* c, 
   const char* format = LIGO_LW::textEncodingAttValue );
CREATE_THREADED2_DECL( ilwd2TimeSeries, LWDocument*, 
   const ILwd::LdasElement*, const char* );
   
std::string getLwData( LWDocument* doc, const char* command );
CREATE_THREADED2_DECL( getLwData, std::string, LWDocument*, const char* );

   
#endif // LwCmdHH
