#ifndef LwElementDataHH
#define LwElementDataHH

#include <memory>   
   
#include "general/Memory.hh"

// Forward declaration   
namespace ILwd
{
   class LdasElement;
}
   
   
// Local Header Files
#include "util.hh"

   
//------------------------------------------------------------------------------
//
//: ElementData functional.
//
// This recursive functional is designed for converting a specified LIGO_LW 
// element to the ILWD format.
//
class ElementData
{

private:

  std::unique_ptr< ILwd::LdasElement > find( 
     const std::vector< XERCES_CPP_NAMESPACE::DOMNode* >& nodeList, 
     std::deque< Query >& dq, std::vector< size_t >& start, const size_t index );

   
  //------------------------------------------------------------------------------
  //: Data structure.
  // 
  // This structure is designed to keep a conversion information for each of the 
  // LIGO_LW elements. This structure contains two elements: Element is a set of 
  // allowed child elements for the current element, and a convert - a pointer 
  // to the conversion function for the element.
  //
  struct data 
  {
     ElementSet Element;
     convFP Convert;     

     data(){}
     data( const ElementSet& e, convFP& f ) : Element( e ) { Convert = f; }
     data( const data& a ) : Element( a.Element ){ Convert = a.Convert; }
  };
  typedef struct data Data;
  typedef std::pair< const char*, Data > dataPair;
  typedef General::unordered_map< std::string, dataPair > LwData;

  static Data& initLigoLwData();
  static Data& initLwTableData();
  static Data& initLwArrayData();
  static Data& initLwDimData();
  static Data& initLwColumnData();
  static Data& initLwCommentData();
  static Data& initLwParamData();
  static Data& initLwFrameData();
  static Data& initLwAdcDataData();
  static Data& initLwDetectorData();
  static Data& initLwTimeData();
  static Data& initLwAdcIntervalData();   

  static LwData& initAllData();

  Data mData;
  static LwData& mAllData;

public:

  /* ( Default ) Constructor */
  ElementData();
  /* Constructor */
  ElementData( Data& a );
   
  //: Overloaded operator().
  //
  // This operator extracts data from specified LIGO_LW element and
  // converts it into ILWD format.
  //
  //!param: const DOMNode* const node - LIGO_LW element to search for specified data.
  //!param: std::deque< Query > dq - Tokenized command.
  //!param: std::vector< size_t > start - Vector to record element position in      +
  //!param:    the document tree.
  //!param: size_t index - Current index into position vector.
  //
  //!return: LdasElement* - Pointer to the extracted element in the ILWD format.
  //
  //!exc: not_valid_element - Specified element is not valid.
  //!exc: bad_query - Query is bad.
  //!exc: ElementData_unknown_error - Unknown error( should never happen ).
  //   
  std::unique_ptr< ILwd::LdasElement > operator() ( 
     XERCES_CPP_NAMESPACE::DOMNode* const node, 
     std::deque< Query > dq, 
     std::vector< size_t >& start, const size_t index );
};


#endif
