#include "config.h"

// System Header Files
#include <iomanip>
#include <limits>   

// General Header Files   
#include <general/util.hh>   
   
// Local Header Files
#include "lwconsts.hh"
#include "convertstr.hh"
   
using namespace std;
using namespace XERCES_CPP_NAMESPACE;  
   

// Constants   
const REAL_8 MIN_NEG_REAL_4( -numeric_limits< REAL_4 >::max() );      
const REAL_8 MAX_NEG_REAL_4( -numeric_limits< REAL_4 >::min() );         
   
const long double MIN_NEG_REAL_8( -numeric_limits< REAL_8 >::max() );      
const long double MAX_NEG_REAL_8( -numeric_limits< REAL_8 >::min() );      
   
   
//------------------------------------------------------------------------------
//
//: Erases escapes for the specified character. Backslash character is used as
//: an escape character in the LIGO_LW format.
//
//!param: std::string& source - A reference to the data string.
//!param: const char& escChar - A reference to the character which escapes     +
//!param:    should be erased.
//
//!return: Nothing.
//   
void eraseEscape( std::string& source, const CHAR& escChar )
{
   string::size_type qpos( 0 );
   while( ( qpos = source.find_first_of( escChar, qpos ) ) != string::npos )
   {
      if( ( qpos != 0 ) && ( source[ qpos - 1 ] == bslashChar ) )
      {
         if( ( escChar == bslashChar && source[ qpos - 1 ] == bslashChar ) ||
             ( escChar != bslashChar && isEscaped( source, qpos - 1 ) ) )
         {
            // If escChar is preceded by backslash erase that backslash
            source.erase( qpos - 1, 1 );

            // That was the very last character in the string: qpos will point to the one passed
	    // last element in the source string
            if( source.size() == qpos )
	    {
 	       return;
	    }

            // To handle escape for the backslash itself
            if( source[ qpos ] == bslashChar )
            {
               ++qpos;
            }   
         }
         else
         {
            ++qpos;
         }
      }
      else
      {
         ++qpos;
      }
   }
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Detects if character is escaped.
//   
// This utility determines if found character is escaped or not.
// It detects the number of preceding backslashes: if that number
// is even or zero - character is not escaped, otherwise - escaped.
//
//!param: const std::string& val - A reference to the data string.
//!param: std::string::size_type pos - A preceding position to the found
//+       character.
//
//!return: const bool - True if character is escaped, false otherwise.
//   
bool isEscaped( const std::string& val, INT_4S pos )
{
   INT_4U bs_num( 0 );
   
   if( val[ pos ] == bslashChar )
   {
      if( pos == 0 )
      {
         ++bs_num;
      }
      else
      {
         // Need to find out how many backslashes are preceding the
         // delimiter: if even number - it's a data delimiter, 
         // otherwise - just escaped character
         while( pos >= 0 && val[ pos ] == bslashChar )         
         {
            ++bs_num;
            --pos;
         }
      }
   }
   
   // There are preceding backslashes
   return( bs_num%2 == 1 );
}

   
//------------------------------------------------------------------------------
//
//: Formats and throws an exception appropriate for the input stream.
//
//!param: istringstream& s - A reference to the input stream with bad/fail state.
//!param: const CHAR* type - Data type name.
//!param: const size_t num_bytes - Number of bytes from input stream to include
//+          into the exception message.   
//
//!return: Nothing.
//      
static void throw_bad_stream_exception( istringstream& s, const CHAR* type,
   const size_t num_bytes )
{
   static const CHAR* what( "bad_data_type: " );
   // Clear stream state to good()
   s.clear();
   
   // Limit number of bytes to read from the stream
   s.width( num_bytes );
   
   string value;
   s >> value;
   
   string info( what );
   info += type;
   info += " (ascii=\"";
   info += value;
   info += "\")";
   
   throw SWIGEXCEPTION( info );   
}
   
   
//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<>
CHAR* string2Data< CHAR >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter )
{
   if( !destSize )
   {
      return 0;
   }
   
   // Erase escapes:
   eraseEscape( source, ',' );  

   if( delimiter != ',' )
   {
      eraseEscape( source, delimiter );   
   }

   // Create a copy of the data string
   string p_source = source;
   eraseEscape( p_source, bslashChar );
   bool allascii( p_source.size() == destSize ); 


   istringstream ss( source );
   ss.setf( ios::oct, ios::basefield );
   ArrayJanitor< CHAR > dest( new CHAR[ destSize ] );
   INT_8S tmp;
   INT_4S c;
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      tmp = ss.peek();
      if( tmp == bslashChar )
      {
         // Check if it is an octal representation of the value:
         ss.get(); // Get the backslash in any case
         if( ss.peek() != bslashChar )
         {
            // The entry is octal based value
           ss >> tmp;
           if( !ss )
           {
              // Bad data
              throw_bad_stream_exception( ss, "char octal value", sizeof( tmp ) );      
           }
           if( numeric_limits< CHAR >::max() < tmp ||
               tmp < numeric_limits< CHAR >::min() )
           {
              // Data is out of range
              throw SWIGEXCEPTION( "out_of_range: char" );
           }      
         }
         else // it is a backslash
         {
            ss.get();
         }
      }
      else
      {
         ss.get();
         if( !ss )
         {
            // Bad data
            throw_bad_stream_exception( ss, "char", sizeof( tmp ) );         
         }   
      }

      dest[ i ] = static_cast< CHAR >( tmp );
   
      if ( !allascii && ( i != ( destSize - 1 ) ) )
      {
         c = ss.peek();
            
         if( c == delimiter ) 
         {
            ss.get();
         }
      }
   }

   if( ( ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: char" );
   }

   return dest.release();
}


//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<>
CHAR_U* string2Data< CHAR_U >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter )
{
   if( !destSize )
   {
      return 0;
   }

   // Erase escapes
   eraseEscape( source, ',' );  
   if( delimiter != ',' )
   {
      eraseEscape( source, delimiter );   
   }

   // Create a copy of the data string
   string p_source = source;
   eraseEscape( p_source, bslashChar );
   bool allascii( p_source.size() == destSize ); 
   
   istringstream ss( source );
   ss.setf( ios::oct, ios::basefield );
   
   ArrayJanitor< CHAR_U > dest( new CHAR_U[ destSize ] );
   INT_8S tmp;
   INT_4S c; 
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      tmp = ss.peek();
      if( tmp == bslashChar )
      {
         // Check if it is an octal representation of the value:
         ss.get(); // Get the backslash in any case
         if( ss.peek() != bslashChar )
         {
            // The entry is octal based value
           ss >> tmp;
           if( !ss )
           {
              // Bad data
              throw_bad_stream_exception( ss, "char_u octal value", sizeof( tmp ) );            
           }
           if( tmp < 0 || 
               tmp > static_cast< INT_8S >( numeric_limits< CHAR_U >::max() ) )
           {
              // Data is out of range
              throw SWIGEXCEPTION( "out_of_range: char_u" );
           }      
         }
         else // it is a backslash
         {
            ss.get();
         }
      }
      else
      {
         ss.get();
         if( !ss )
         {
            // Bad data
            throw_bad_stream_exception( ss, "char_u", sizeof( tmp ) );               
         }   
      }
   
      dest[ i ] = static_cast< CHAR_U >( tmp );
      if ( !allascii && i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }        
      }
   }


   if( ( ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: char_u" );
   }
   
   return dest.release();
}

   
//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//   
template<> 
INT_2S* string2Data< INT_2S >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< INT_2S > dest( new INT_2S[ destSize ] );
   INT_8S tmp;
   INT_4S c;
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> tmp;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "int_2s", sizeof( tmp ) );                  
      }
      if( numeric_limits< INT_2S >::min() > tmp || 
          tmp > numeric_limits< INT_2S >::max() )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: int_2s" );
      }   
   
      dest[ i ] = static_cast< INT_2S >( tmp );
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }

   
   if( (ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: int_2s" );
   }
   
   return dest.release();
} 

   
//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
INT_2U* string2Data< INT_2U >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< INT_2U > dest( new INT_2U[ destSize ] );
   INT_8U tmp;
   INT_4S c;
   
   for ( INT_4U i = 0; i < destSize; ++i )
   {
      if( ss.peek() == '-' )
      {
         // Negative number
         throw SWIGEXCEPTION( "out_of_range: negative int_2u" );
      }   
      ss >> tmp;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "int_2u", sizeof( tmp ) );                     
      }
      if( tmp > numeric_limits< INT_2U >::max() )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: int_2u" );
      }   
   
      dest[ i ] = static_cast< INT_2U >( tmp );
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }


   if( ( ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: int_2u" );
   }
   
   return dest.release();
} 
   

//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
INT_4S* string2Data< INT_4S >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< INT_4S > dest( new INT_4S[ destSize ] );
   INT_8S tmp;
   INT_4S c;
   
   for ( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> tmp;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "int_4s", sizeof( tmp ) );                     
      }
      if( numeric_limits< INT_4S >::min() > tmp || 
          tmp > numeric_limits< INT_4S >::max() )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: int_4s" );
      }   
   
      dest[ i ] = static_cast< INT_4S >( tmp );
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }

   
   if( (ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: int_4s" );
   }
   
   return dest.release();
} 
   

//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
INT_4U* string2Data< INT_4U >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< INT_4U > dest( new INT_4U[ destSize ] );
   INT_8U tmp;
   INT_4S c;
   
   for ( INT_4U i = 0; i < destSize; ++i )
   {
      if( ss.peek() == '-' )
      {
         // Negative number
         throw SWIGEXCEPTION( "out_of_range: negative int_4u" );
      }   
      ss >> tmp;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "int_4u", sizeof( tmp ) );
      }
      if( tmp > numeric_limits< INT_4U >::max() )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: int_4u" );
      }   
   
      dest[ i ] = static_cast< INT_4U >( tmp );
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }

   
   if( ( ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: int_4u" );
   }
   
   return dest.release();
} 
   

//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
INT_8S* string2Data< INT_8S >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< INT_8S > dest( new INT_8S[ destSize ] );
   INT_4S c;

   for( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> dest[ i ];
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "int_8s", sizeof( INT_8S ) );   
      }
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }

   
   if( ( ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: int_8s" );
   }
   
   return dest.release();
} 
   

//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
INT_8U* string2Data< INT_8U >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< INT_8U > dest( new INT_8U[ destSize ] );
   INT_4S c;
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      if( ss.peek() == '-' )
      {
         // Negative number
         throw SWIGEXCEPTION( "out_of_range: negative int_8u" );
      }   
      ss >> dest[ i ];
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "int_8u", sizeof( INT_8U ) );      
      }
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }

   if( (ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: int_8u" );
   }
   
   return dest.release();
} 

   
//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
REAL_4* string2Data< REAL_4 >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< REAL_4 > dest( new REAL_4[ destSize ] );
   REAL_8 tmp;
   INT_4S c;
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> tmp;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "real_4", sizeof( tmp ) );         
      }
      if( tmp != 0.0 && 
          ( tmp < MIN_NEG_REAL_4 || tmp > numeric_limits< REAL_4 >::max() || 
            ( tmp > MAX_NEG_REAL_4 && tmp < numeric_limits< REAL_4 >::min() ) ) )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: real_4" );
      }   
   
      dest[ i ] = static_cast< REAL_4 >( tmp );
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }


   if( ( ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: real_4" );
   }
   
   return dest.release();
}   
   

//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
REAL_8* string2Data< REAL_8 >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< REAL_8 > dest( new REAL_8[ destSize ] );
   long double tmp;
   INT_4S c;
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> tmp;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "real_8", sizeof( tmp ) );            
      }
      if( tmp != 0.0 && 
          ( tmp < MIN_NEG_REAL_8 || tmp > numeric_limits< REAL_8 >::max() || 
            ( tmp > MAX_NEG_REAL_8 && tmp < numeric_limits< REAL_8 >::min() ) ) )   
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: real_8" );
      }   
   
      dest[ i ] = static_cast< REAL_8 >( tmp );
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }


   if( (ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: real_8" );
   }
   
   return dest.release();
}   

   
//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
COMPLEX_8* string2Data< COMPLEX_8 >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< COMPLEX_8 > dest( new COMPLEX_8[ destSize ] );
   REAL_8 tmp1, tmp2;
   INT_4S c;
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> tmp1;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "complex_8", sizeof( tmp1 ) );   
      }
      if( tmp1 != 0.0 && 
          ( tmp1 < MIN_NEG_REAL_4 || tmp1 > numeric_limits< REAL_4 >::max() || 
            ( tmp1 > MAX_NEG_REAL_4 && tmp1 < numeric_limits< REAL_4 >::min() ) ) )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: complex_8" );
      }   
   
      c = ss.peek();
      if( c == delimiter )
      {
         ss.get();
      }
   
      ss >> tmp2;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "complex_8", sizeof( tmp1 ) );   
      }
      if( tmp2 != 0.0 && 
          ( tmp2 < MIN_NEG_REAL_4 || tmp2 > numeric_limits< REAL_4 >::max() || 
            ( tmp2 > MAX_NEG_REAL_4 && tmp2 < numeric_limits< REAL_4 >::min() ) ) )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: complex_8" );
      }   
   
      dest[ i ] = COMPLEX_8( static_cast< REAL_4 >( tmp1 ),
                             static_cast< REAL_4 >( tmp2 ) );
   
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }

   
   if( (ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: complex_8" );
   }
   
   return dest.release();
}   

   
//: Function to convert std::string into data
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<> 
COMPLEX_16* string2Data< COMPLEX_16 >( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   istringstream ss( source );

   ArrayJanitor< COMPLEX_16 > dest( new COMPLEX_16[ destSize ] );
   long double tmp1, tmp2;
   INT_4S c;
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> tmp1;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "complex_16", sizeof( tmp1 ) );   
      }
      if( tmp1 != 0.0 && 
          ( tmp1 < MIN_NEG_REAL_8 || tmp1 > numeric_limits< REAL_8 >::max() || 
            ( tmp1 > MAX_NEG_REAL_8 && tmp1 < numeric_limits< REAL_8 >::min() ) ) )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: complex_16" );
      }   
   
      c = ss.peek();
      if( c == delimiter )
      {
         ss.get();
      }
   
      ss >> tmp2;
      if( !ss )
      {
         // Bad data
         throw_bad_stream_exception( ss, "complex_16", sizeof( tmp1 ) );      
      }
      if( tmp2 != 0.0 && 
          ( tmp2 < MIN_NEG_REAL_8 || tmp2 > numeric_limits< REAL_8 >::max() || 
            ( tmp2 > MAX_NEG_REAL_8 && tmp2 < numeric_limits< REAL_8 >::min() ) ) )
      {
         // Out of range
         throw SWIGEXCEPTION( "out_of_range: complex_16" );
      }   
   
      dest[ i ] = COMPLEX_16( static_cast< REAL_8 >( tmp1 ),
                              static_cast< REAL_8 >( tmp2 ) );
   
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }
      }
   }

   
   if( ( ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: complex_16" );
   }
   
   return dest.release();
}   
   

//: Function to convert std::string into array data
//
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<>
CHAR* string2ArrayData< CHAR >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter )
{
   if( !destSize )
   {
      return 0;
   }
   
   // Erase escapes:
   eraseEscape( source, delimiter );  

   istringstream ss( source );
   ss.setf( ios::oct, ios::basefield );
   ArrayJanitor< CHAR > dest( new CHAR[ destSize ] );
   INT_8S tmp;
   INT_4S c;

   for( INT_4U i = 0; i < destSize; ++i )
   {
      tmp = ss.peek();
      if( tmp == bslashChar )
      {
         // Check if it is an octal representation of the value:
         ss.get(); // Get the backslash in any case
         if( ss.peek() != bslashChar )
         {
            // The entry is octal based value
           ss >> tmp;
           if( !ss )
           {
              // Bad data
              throw_bad_stream_exception( ss, "char octal value", sizeof( tmp ) );      
           }
           if( numeric_limits< CHAR >::max() < tmp || 
               tmp < numeric_limits< CHAR >::min() )
           {
              // Data is out of range
              throw SWIGEXCEPTION( "out_of_range: char" );
           }      
         }
         else // it is a backslash
         {
            ss.get();
         }
      }
      else
      {
         ss.get();
         if( !ss )
         {
            // Bad data
            throw_bad_stream_exception( ss, "char", sizeof( tmp ) );         
         }   
      }

      dest[ i ] = static_cast< CHAR >( tmp );
   
      if ( i != ( destSize - 1 ) )
      {
         c = ss.peek();
         if( c == delimiter ) 
         {
            ss.get();
         }
      }
   }

   
   if( (ss.tellg( ) >= 0 ) &&
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: char" );
   }
   
   return dest.release();
}
   

//: Function to convert std::string into array data
//
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//      
template<>
CHAR_U* string2ArrayData< CHAR_U >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter )
{
   if( !destSize )
   {
      return 0;
   }

   // Erase escapes:
   eraseEscape( source, delimiter );   

   istringstream ss( source );
   ss.setf( ios::oct, ios::basefield );
   
   ArrayJanitor< CHAR_U > dest( new CHAR_U[ destSize ] );
   INT_8S tmp;
   INT_4S c; 
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      tmp = ss.peek();
      if( tmp == bslashChar )
      {
         // Check if it is an octal representation of the value:
         ss.get(); // Get the backslash in any case
         if( ss.peek() != bslashChar )
         {
            // The entry is octal based value
           ss >> tmp;
           if( !ss )
           {
              // Bad data
              throw_bad_stream_exception( ss, "char_u octal value", sizeof( tmp ) );            
           }
           if( tmp < 0 || 
               tmp > static_cast< INT_8S >( numeric_limits< CHAR_U >::max() ) )
           {
              // Data is out of range
              throw SWIGEXCEPTION( "out_of_range: char_u" );
           }      
         }
         else // it is a backslash
         {
            ss.get();
         }
      }
      else
      {
         ss.get();
         if( !ss )
         {
            // Bad data
            throw_bad_stream_exception( ss, "char_u", sizeof( tmp ) );               
         }   
      }
   
      dest[ i ] = static_cast< CHAR_U >( tmp );
      if ( i != ( destSize-1 ) )
      {
         c = ss.peek();
         if( c == delimiter )
         {
            ss.get();
         }        
      }
   }

   
   if( ( ss.tellg( ) >= 0 ) && 
       ( find_if( source.begin() + ss.tellg(), source.end(), not_space )
	 != source.end() ) )
   {
      throw SWIGEXCEPTION( "extra_data: char_u" );
   }
   
   return dest.release();
}
   
