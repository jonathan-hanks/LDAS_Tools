/* -*- c-basic-offset: 3; -*- */
#include "config.h"

// System Header Files
#include <sstream>
#include <fstream>   
#include <algorithm>   
#include <iterator>   
#include <memory>   

#include "general/Memory.hh"

// XML Header Files
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>

// General Header Files
#include "general/util.hh"
#include "general/objectregistry.hh"
#include "general/ErrorLog.hh"
   
// GenericAPI Header Files   
#include <genericAPI/util.hh>
#include <genericAPI/registry.hh>
#include <genericAPI/swigexception.hh>
#include <genericAPI/thread.hh>
#include <genericAPI/elementcmd.hh>
   
// ILWD Header Files
#include <ilwd/elementid.hh>
#include <ilwd/ldaselement.hh>      
using namespace ILwd;

#if HAVE_LIBOSPACE
// Ospace Header Files
#include <ospace/stream/tstream.h>
#endif /* HAVE_LIBOSPACE */

// Local Header Files
#include "lwcmd.hh"
#include "lwdocument.hh"
#include "lwdocattribute.hh"
#include "query.hh"
#include "errorhandler.hh"

using namespace std;   
using namespace XERCES_CPP_NAMESPACE;   

   
static XercesDOMParser::ValSchemes gValScheme( XercesDOMParser::Val_Auto );      
   
ObjectRegistry< LWDocument > lwRegistry;

   
//-----------------------------------------------------------------------------
//
//: Check the validity of a LWDocument pointer.
//
// True if the LwDocument object is valid.  A pointer is valid if it
// points to a current LwDoc object in memory.
//
//!param: LWDocument* doc - The pointer to check.
//
//!return: bool - true if it is valid, false otherwise.
//
bool isLwDocValid( const LWDocument* doc )
{
   static const char* func = "isLwDocValid";

   if ( General::StdErrLog.IsOpen( ) )
   {
      std::ostringstream	msg;

      msg << func << ": doc: " << (void*)doc << " value: " << lwRegistry.isRegistered( doc );
      General::StdErrLog( General::ErrorLog::DEBUG,
			  __FILE__, __LINE__,
			  msg.str( ) );
   }
   return lwRegistry.isRegistered( doc );
}


//-----------------------------------------------------------------------------
//
//: Set Expand External References flag.
//
// This command sets an Expand External References flag to true or false.
// If this internal flag is set to true, external references will be expanded
// when reading an XML document in.
//
//!usage: setLwExtReferenceFlag value
//
//!param: const char* value - Can be one of true | false. This is the value
//!param:                        to set the flag to.
//
//!return: Nothing.
//
void setLwExtReferenceFlag( const char* value )
{
   static const char* func = "setLwExtReferenceFlag";

   if ( General::StdErrLog.IsOpen( ) )
   {
      std::ostringstream	msg;

      msg << func << ": value: " << value;
      General::StdErrLog( General::ErrorLog::DEBUG,
			  __FILE__, __LINE__,
			  msg.str( ) );
   }
   return LWDocument::setExpandReferences( value );
}


//-----------------------------------------------------------------------------
//
//: Get an attribute of specified element in lightweight document.
//
//!param: LwDocument* doc     - pointer to the lightweight document.
//!param: const char* command - user command to specify an element in the LW 
//                              document
//
//!return: string - found attribute value, or "NULL" if it's empty, and
//                  an index position of the element in the LW document.
//
//!exc: SwigException -
//!exc:    - invalid_lw_doc
//!exc:    - bad_query
//!exc:    - not_valid_element 
//
const std::string getLwAttribute( const LWDocument* doc, const char* command )
{
    static const char* func = "isLwDocValid";
   
    if ( General::StdErrLog.IsOpen( ) )
    {
        std::ostringstream	msg;

	msg << func << ": doc: " << (void*)doc << " command: " << command;
	General::StdErrLog( General::ErrorLog::DEBUG,
			  __FILE__, __LINE__,
			  msg.str( ) );
    }
    if ( !isLwDocValid( doc ) )
    {
        throw SWIGEXCEPTION( "invalid_lw_doc" );
    }

    deque< Query > dq;
    tokenize( command, dq );

    vector< size_t > sarray; 
    ostringstream ss;
   
    ss << "{" << getAttribute( doc, dq, sarray, 0 ) << "} { ";

    copy( sarray.begin(), sarray.end(), 
          ostream_iterator< size_t >( ss, " " ) );

    ss << '}';

   
    if ( General::StdErrLog.IsOpen( ) )
    {
        std::ostringstream	msg;

	msg << func << ": retval: " << ss.str( );
	General::StdErrLog( General::ErrorLog::DEBUG,
			  __FILE__, __LINE__,
			  msg.str( ) );
    }
    return ss.str();
}


//-----------------------------------------------------------------------------
//
//: Get the content of specified LIGO_LW element in the document
//
//!param: LwDocument* doc     - pointer to lightweight document.
//!param: const char* command - user command to specify an element.
//
//!return: string - element content in the following format:
//                  type1 "name1" type2 "name2" ...
//
//!exc: SwigException -
//!exc:    - invalid_lw_doc
//!exc:    - bad_query
//!exc:    - not_valid_element 
//!exc:    - only_ligo_lw_allowed 
//
const std::string getLwTOC( const LWDocument* doc, const char* command )
{
    if ( !isLwDocValid( doc ) )
    {
       throw SWIGEXCEPTION( "invalid_lw_doc" );
    }

    deque< Query > dq;
    tokenize( command, dq );

    vector< size_t > sarray; 
    ostringstream ss;
   
    ss << "{" << getTOC( doc, dq, sarray, 0 ) << "}";
   
    if ( sarray.size() != 0 )
    {
       ss << "{ ";
   
       copy( sarray.begin(), sarray.end(), 
             ostream_iterator< size_t >( ss, " " ) );   

       ss << '}';
    }

    return ss.str();
}


//-----------------------------------------------------------------------------
//
//: Convert specified element from lightweight document into ILWD format.
//
//!param: LwDocument* doc     - a pointer to the lightweight document.
//!param: const char* command - user command to specify the element. 
//
//!return: string - a pointer to converted ILWD element,
//                  and index position of the converted lw element in the 
//                  document.
//
//!exc: SwigException -
//!exc:    - invalid_lw_doc
//!exc:    - bad_query
//!exc:    - not_valid_element 
//!exc:    - bad_array_element ( dimension elements are missing )
//!exc:    - stream_is_missing ( for now array should store its data
//!exc:      in internal "Stream" element )
//!exc:    - openFTP_failed 
//!exc:    - openHTTP_failed
//
std::string getLwData( LWDocument* doc, const char* command )
{
    if ( !isLwDocValid( doc ) )
    {
        throw SWIGEXCEPTION( "invalid_lw_doc" );
    }
    
    deque< Query > dq;
    tokenize( command, dq );
   
    vector< size_t > sarray;
    LdasElement* element( getData( doc, dq, sarray, 0 ).release() );
    Registry::elementRegistry.registerObject( element );
    
    ostringstream ss;
    ss << "{" << createElementPointer( element ) << "} { ";
   
    copy( sarray.begin(), sarray.end(), 
          ostream_iterator< size_t >( ss, " " ) ); 
   
    ss << '}';

    return ss.str();
}


//-----------------------------------------------------------------------------
//
//: Convert ILWD format element into LIGO_LW format TimeSeries.
//
//!param: LdasElement* c - A pointer to the LdasElement.
//!param: const std::string format - Format to use for Stream elements.   
//
//!return: string - a pointer to LIGO_LW document.
//
LWDocument* ilwd2TimeSeries( const LdasElement* c, const char* format )
{
    if ( !Registry::isElementValid( c ) )
    {
        throw SWIGEXCEPTION( "invalid_ilwd_element" );
    }

    if( ( c->getElementId() != ID_ILWD ) )
    {
      throw SWIGEXCEPTION( "only_ilwd_element_allowed" );
    }

   
    unique_ptr< LWDocument > doc( new LWDocument );
    doc->setStreamEncoding( format );   

    const bool is_root( true );
    doc->convertToTimeSeries( c, is_root );

   
    LWDocument* lw_doc( doc.release() );
    lwRegistry.registerObject( lw_doc );    
   
    return lw_doc;    
}


//-----------------------------------------------------------------------------
//
//: Put raw data of specified element in external file.
//
//!param: LwDocument* doc      - pointer to the lightweight document.
//!param: const char* command  - user command to specify the element. 
//!param: const char* urlAddr  - url address.
//!param: const char* urlDir   - url directory.
//
//!return: string - index position of the lw element.
//
//!exc: SwigException -
//!exc:    - invalid_lw_doc
//!exc:    - bad_query
//!exc:    - not_valid_element 
//!exc:    - remote_data 
//!exc:    - open_file_failed
//
const std::string putLwData( LWDocument* doc, 
   const char* command, const char* urlAddr, const char* urlDir )
{
    if ( !isLwDocValid( doc ) )
    {
       throw SWIGEXCEPTION( "invalid_lw_doc" );
    }

    deque< Query > dq;
    tokenize( command, dq );
   
    vector< size_t > sarray;
    if ( putData( doc, dq, sarray, 0, urlAddr, urlDir ) == true )
    {
       ostringstream ss;
       ss << '{';
   
       copy( sarray.begin(), sarray.end(), 
             ostream_iterator< size_t >( ss, " " ) ); 
   
       ss << '}';

       return ss.str();
    }
    else
    {
       throw SWIGEXCEPTION( "put_data_failed" );
    }

    return "";
}


//-----------------------------------------------------------------------------
//
//: Write LW document to a file.
//
//!param: const char* filename - name of the file to write document to. 
//!param                         This will be overwritten if file already exists.
//!param: LWDocument* doc      - pointer to the LW document.
//
//!exc: SwigException -
//!exc:   - invalid_lw_doc: The document pointer is invalid.
//!exc:   - open_file_failed
//!exc:   - write_file_failed
//
void writeLwFile( LWDocument* doc, const char* filename )
{
    if ( !isLwDocValid( doc ) )
    {
        throw SWIGEXCEPTION( "invalid_lw_doc" );
    }

    // We need to do permission checking here.
    ofstream out( filename );
    if ( out == 0 )
    {
       throw SWIGEXCEPTION( "can't_open_file" );
    }

    try
    {
       out.exceptions( ios::badbit | ios::failbit );
       doc->write( out );
    }
    catch( const ios::failure& exc )
    {
       if( out.bad() )
       {
          string msg = filename;
          msg += " has badbit set";
          out.exceptions( ios::goodbit );
          out.close();
          throw SWIGEXCEPTION( msg );
       }
       else if( out.fail() )
       {
          string msg = filename;
          msg += " has failbit set";
          out.exceptions( ios::goodbit );
          out.close();
          throw SWIGEXCEPTION( msg );
       }   

       out.exceptions( ios::goodbit );
       out.close();
       throw;
    }
    catch ( ... )
    {
       out.exceptions( ios::goodbit );   
       out.close();
       throw SWIGEXCEPTION( "write_lw_doc_failed" );
    }

    out.exceptions( ios::goodbit );   
    out.close();
    return;
}


//-----------------------------------------------------------------------------
//
//: Display LW Document.
//
//!usage: getLWDocument ptLwDoc
//
//!param: LwDocument* doc - pointer to the LW document.
//
//!exc: SwigException -
//!exc:   - invalid_lw_doc: The document pointer is invalid.
//!exc:   - write_file_failed
//
//!todo: Have this function return a char* and set-up SWIG to make sure that
//!todo:    it is properly copied and destructed.
//
const std::string getLWDocument( LWDocument* doc )
{
    if ( !isLwDocValid( doc ) )
    {
        throw SWIGEXCEPTION( "invalid_lw_doc" );
    }

    ostringstream out;
    try
    {
       doc->write( out );
    }
    catch( ... )
    {
       throw SWIGEXCEPTION( "write_lw_doc_failed" );
    }

    return out.str();
}


//-----------------------------------------------------------------------------
//
//: Parse LIGO_LW format file.
//
//!param: const char* filename - The file to read and parse.
//
//!return: LwDocument* - The LW document pointer.
//
//!exc: memory_allocation_failed - Error allocating the memory.   
//!exc: SwigException -
//!exc:   - permission_denied 
//!exc:   - file_not_found
//!exc:   - bad_alloc
//!exc:   - io_error
//!exc:   - error_parsing_file
//!exc:   - Error: message     
//!exc:   - Fatal error: message
//!exc: 
LWDocument* readLwFile( const char* filename )
{
    // Make sure file exists
    ifstream f( filename );
    if( !f )
    {
       string msg( "readLwFile: error opening file \"" );
       msg += filename;
       msg += '\"';
       throw SWIGEXCEPTION( msg );
    }
    
    f.close();


    unique_ptr< XercesDOMParser > parser( new XercesDOMParser() );
    parser->setValidationScheme( gValScheme );
   
    //  Attach an error handler to the parser. The parser will call back to
    //  methods of the ErrorHandler if it discovers errors during the course
    //  of parsing the XML document.
    //
    unique_ptr< ErrorHandler > errReporter( new ParserErrorReporter() );
    parser->setErrorHandler( errReporter.get() );
   
    parser->parse( filename );
   
    unique_ptr< LWDocument > doc( new LWDocument( parser->getDocument() ) );

    LWDocument* doc_ptr( doc.release() );
    lwRegistry.registerObject( doc_ptr );    
   
    return doc_ptr;
}


//-----------------------------------------------------------------------------
//
//: Deletes a LWDocument object in memory.
//
//!param: LWDocument* doc - The LW document to destruct.
//
//!exc: SwigException -
//!exc:   - invalid_lw_document
//
void destructLWDocument( LWDocument* doc )
{
    if ( !isLwDocValid( doc ) )
    {
        throw SWIGEXCEPTION( "invalid_lw_doc" );
    }

    if( lwRegistry.destructObject( doc ) == false )
    {
       ostringstream s;
       s << "Error destructing LWDocument object: \n" 
         <<  doc;
   
       throw SWIGEXCEPTION( s.str() );
    }
   
    return;
}


#if HAVE_LIBOSPACE
//-----------------------------------------------------------------------------
//
//: Send LW Document through the socket.
//
//!param: os_tcp_socket* socket - data socket.
//!param: LWDocument* doc       - The LW document to send.
//
//!exc: SwigException -
//!exc:   - invalid_socket
//!exc:   - invalid_lw_doc
//!exc:   - write_lw_doc_failed
//
void sendLWDocument( os_tcp_socket* socket, LWDocument* doc )
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw SWIGEXCEPTION( "invalid_socket" );
    }

    if ( !isLwDocValid( doc ) )
    {
        throw SWIGEXCEPTION( "invalid_lw_doc" );
    }
    
    // First we have to write the LW object to a memory buffer.
    ostringstream buffer;
    try
    {
       doc->write( buffer );
    }
    catch( ... )
    {
       throw SWIGEXCEPTION( "write_lw_doc_failed" );
    }

    
    // Now we create a text stream adaptor for the socket and write out the 
    // buffer size followed by the buffer.
    os_tstream out( *socket );
    out << size_t( buffer.str().size() );
    out.write( buffer.str().c_str(), buffer.str().size() );
   
    return;
}
#endif /* HAVE_LIBOSPACE */


#if HAVE_LIBOSPACE
//-----------------------------------------------------------------------------
//
//: Receive LW document object through the socket.
//
//!param: os_tcp_socket* socket - data socket.
//
//!return: LWDocument* - The LW document which was received.
//
//!exc: SwigException -
//!exc:   - invalid_socket
//!exc:   - invalid_lw_doc
//
LWDocument* recvLWDocument( os_tcp_socket* socket )
{
    if ( !Registry::isSocketValid( socket ) )
    {
        throw SWIGEXCEPTION( "invalid_socket" );
    }
    
    // Create a text stream adaptor for the socket and read in the size.
    os_tstream in( *socket );
    size_t size;
    in >> size;
   
    // Create a temporary file to write the lw data to.
    char filename[ 160 ];
    tmpnam( filename );
    ofstream out( filename );  
    if( !out )
    {
       throw SWIGEXCEPTION( "Couldn't open temp file for recvLWDocument." );
    }

    // Read the data from the socket into the file via a 1k buffer.
    int n( 0 );
    char buffer[ 1024 ];
    while( in.good() && size != 0 )
    {
        n = ( size > 1024 ) ? 1024 : size;
        
        in.read( buffer, n );
        if ( in.good() )
        {
            size -= n;
            out.write( buffer, n );
        }
    }
    out.close();

    // Now read in the temporary file and parse it
    unique_ptr< XercesDOMParser > parser( new XercesDOMParser() );
    parser->setValidationScheme( gValScheme );   
   
    //  Attach an error handler to the parser. The parser will call back to
    //  methods of the ErrorHandler if it discovers errors during the course
    //  of parsing the XML document.
    //
    unique_ptr< ErrorHandler > errReporter( new ParserErrorReporter() );
    parser->setErrorHandler( errReporter.get() );

    try
    {
       parser->parse( filename );
    }
    catch( const SwigException& )
    {
       remove( filename );
       throw;
    }
    catch( const exception& )
    {
       remove( filename );
       throw;
    }   
    catch(...)
    {
       remove( filename );
       throw SWIGEXCEPTION( "Unknown error parsing temporary file." );
    }

    remove( filename );

    
    unique_ptr< LWDocument > doc( new LWDocument( parser->getDocument() ) );

    // Register the new document
    LWDocument* doc_ptr( doc.release() );
    lwRegistry.registerObject( doc_ptr );

    
    return doc_ptr;
}
#endif /* HAVE_LIBOSPACE */
   

//-----------------------------------------------------------------------------
//
//: Convert element from ILWD to LIGO_LW format.
//
//!param: ILwd::Element* c - ILWD element to be converted into
//!param:                    LW document.
//!param: const std::string format - Format to use for Stream elements.
//
//!return: LWDocument* - The LW document which was created.
//
//!exc: SwigException -
//!exc:   - invalid_ilwd_element
//!exc:   - conversion_failed
//!exc:   - only_ilwd_element_allowed
//
LWDocument* ilwd2Lw( const LdasElement* c, const char* format )
{
    if ( !Registry::isElementValid( c ) )
    {
        throw SWIGEXCEPTION( "invalid_ilwd_element" );
    }
    if( c->getElementId() != ID_ILWD )
    {
      throw SWIGEXCEPTION( "only_ilwd_element_allowed" );
    }

    unique_ptr< LWDocument > doc( new LWDocument );
    doc->setStreamEncoding( format );
   
    const bool is_root( true );
    doc->convertToLW( c, is_root );

   
    // Register new document
    LWDocument* doc_ptr( doc.release() );
    lwRegistry.registerObject( doc_ptr );    
   
   
    return doc_ptr;    
}


#if HAVE_LIBOSPACE
CREATE_THREADED2V( sendLWDocument, os_tcp_socket*, LWDocument* )
CREATE_THREADED1( recvLWDocument, LWDocument*, os_tcp_socket* )
#endif /* HAVE_LIBOSPACE */

CREATE_THREADED2( ilwd2Lw, LWDocument*, const LdasElement*, const char* )
CREATE_THREADED2( ilwd2TimeSeries, LWDocument*, const LdasElement*, const char* )


// The following code is expanded and modided CREATE_THREADED2V() macro.
//
CREATE_THREADED1( readLwFile, LWDocument*, const char* )
CREATE_THREADED2V( writeLwFile, LWDocument*, const char* )
CREATE_THREADED2( getLwData, std::string, LWDocument*, const char* )

   
template class ObjectRegistry< LWDocument >;
