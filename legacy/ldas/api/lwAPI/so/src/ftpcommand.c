/*************************************/
/* Command interface for FTP server. */
/*************************************/

#include "config.h"

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ftpcommand.h"
#include "urlutil.h"


int get_reply_(FILE* fp, char *lastReplyString)
{
  char replyString[256];
  int  error_code = 0;
  char cont = ' ';
 
  for (;;) {
#if DEBUG_LWAPI_FTP_COMMAND
    int nscanned;
#endif /* DEBUG_LWAPI_FTP_COMMAND */
    if (fgets_fp(replyString, 256, fp) != 0)
      return 0;
#if DEBUG_LWAPI_FTP_COMMAND
    printf ("reply string=`%s'\n", replyString);
    nscanned =
#endif /* DEBUG_LWAPI_FTP_COMMAND */
      sscanf(replyString, "%d%c", &error_code, &cont);
#if DEBUG_LWAPI_FTP_COMMAND
    printf ("nscanned=%d; error_code=%d; contcahr=%d\n", nscanned, error_code, cont);
#endif /* DEBUG_LWAPI_FTP_COMMAND */
    /* This skips all lines until it gets something like
     * 220 foo.bar.edu FTP server
     * I.e. it skips continuation lines that look like
     * 220-*** /etc/motd.ftp ***
     * ...
     */
    if (isdigit((int)*replyString) && cont != '-')
	break;
  }
  if (lastReplyString)
	strcpy(lastReplyString, replyString);
  
  return error_code;
}


int get_reply_from_fd(int fd)
{
  char replyString[256];
  int  error_code = 0;
  char cont = ' ';
 
  for (;;) {
    fgets_fd(replyString, 256, fd);
    sscanf(replyString, "%d%c", &error_code, &cont);
    if (isdigit((int)*replyString) && cont != '-')
	break;
  }
  
  return error_code;
}


int command_(char* comm, char* comm_value, FILE* file_in, FILE* file_out, char* lastReplyString)
{
  char  logString[256];
  char *ftpString;
  int   strLen = 0; 
  int   reply  = 0;
  

  if (comm_value == NULL)
  {
     sprintf(logString, "%s", comm);
     strLen = strlen(comm) + 2;
  }
  else
  {
     sprintf(logString, "%s %s", comm, comm_value);
     /* To include space and '\0' into the string: */
     strLen = strlen(comm) + strlen(comm_value) + 2;
  }


  ftpString = (char*)malloc(strLen);

  if (ftpString == NULL)
  {
    /* printf("Error allocating memory for %s.\n", comm); */
     return -1;
  }
  
  strncpy(ftpString, logString, (strLen-1));
  ftpString[strLen-1] = '\0';

  /* printf("FTP Sending %s\n", ftpString); */
  fprintf(file_out, ftpString);   
  /*************************************************************/
  /* FTP documentation tells to pass "\n\r" at the end of each */
  /* program, but it wouldn't work correctly ===> pass only    */
  /* '\n' at the end.                                          */
  /*************************************************************/
  fprintf(file_out, "\n");

  (void) fflush(file_out);
  free(ftpString);

  reply = get_reply_(file_in, lastReplyString); 

  return reply;
}
