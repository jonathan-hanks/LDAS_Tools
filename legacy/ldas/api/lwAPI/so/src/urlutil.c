#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "urlutil.h"

char *fgets_fd(char* buf, int nmax, int fd)
{
    int n = 0;
    int nbyte;

    if(fd < 0) 
	return NULL;

    while(1)
    {
        nbyte = read(fd, &buf[n], 1);
   
        if(nbyte == 0) break;
   
        n++;
    
        if(buf[n-1] == '\n' || n+1 >= nmax) break;
    }
    
    buf[n] = '\0';
    
    return buf;
}


int fgets_fp(char* read_buffer, int nmax, FILE* fp)
{
    int n = 0;
    int read_blocks = 0;
  
    if(fp == NULL) 
	return 1;

    while(1)
    {
       read_blocks = fread(&read_buffer[n], 1, 1, fp);

       if (read_blocks != 1)
       {
	  /* printf("Error reading response from fp.\n"); */
          return 1;
          break;
       }
 
       n++;

       if ( (read_buffer[n-1] == '\n') || ((n+1) == nmax) )
	 break;

    }

    read_buffer[n] = '\0';

    return 0;
}


char* recv_file(FILE* file_in, int *error)
{
  char *new_buf;
  char *fileBuffer;
  int   nread = 0;
  int   bytes_read = 0;
  int   block_size = 4096;
  int   nblocks = 1;

  fileBuffer = (char*) malloc(block_size);
  if (!fileBuffer) {
     *error = URL_ERROR_MALLOC; 
     return 0;
  }

  for (;;) {
    if ((bytes_read = fread(fileBuffer+nread, 1, block_size, file_in))) {
	nblocks++;
	new_buf = (char*)realloc(fileBuffer, nblocks*block_size);
	if (!new_buf) {
		free (fileBuffer);
		*error = URL_ERROR_MALLOC;
		return 0;
	} else {
		fileBuffer = new_buf;
	}
	nread += bytes_read;
    } else
	break;
  }
  new_buf = (char*)realloc(fileBuffer, nread + 1);
  if (!new_buf) {
        free (fileBuffer);
        *error = URL_ERROR_MALLOC;
        return 0;
  } else
	fileBuffer = new_buf;
  fileBuffer[nread]=0; /* zero terminate string */
  /* printf("received one file; length=%d\n", strlen(fileBuffer)); */
  return fileBuffer;
}


/*********************************/
/* Create a URL copy of file_in. */
/*********************************/
int put_file( char *ftp_file, FILE* file_out, int* error )
{
  char    *fileBuffer;
  FILE    *new_fp;
  size_t  bytes_read = 0;
  size_t  block_size = 4096;

 
  /* Now create a local copy of the ftp file: */
  new_fp = fopen(ftp_file, "rb");

  if (new_fp == NULL)
  {
     *error = URL_ERROR_CREATE_LOCAL_FILE;
     return 1;
  }
               
  fileBuffer = (char*)malloc(block_size*sizeof(char));

  if (fileBuffer == NULL)
     {
        *error = URL_ERROR_MALLOC;        
        fclose(new_fp);
        return 1;
     }

  while (1)
  {
     bytes_read = fread(fileBuffer, 1, block_size, new_fp);

     if ( (bytes_read > 0) && (bytes_read <= block_size))
     {
       	if (fwrite(fileBuffer, 1, bytes_read, file_out) != bytes_read)
      	{
	   *error = URL_ERROR_WRITE_URL_FILE;
           free(fileBuffer);
           fclose(new_fp);
           return 1;
        }
     }
     else if (bytes_read == 0)
     {
	 break;
     }

  }                


  /******************************************************************/
  /* Client has to close the connection in order to give the server */
  /* the end-of-file notification.                                  */
  /******************************************************************/
  fclose(file_out);

  fclose(new_fp);
  free(fileBuffer);
  return 0;

}

