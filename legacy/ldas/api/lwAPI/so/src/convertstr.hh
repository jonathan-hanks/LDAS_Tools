#ifndef ConvertStringHH
#define ConvertStringHH

// System Header Files
#include <new>
#include <string>
#include <sstream>
#include <algorithm>   

// XML Header Files
#include <xercesc/util/Janitor.hpp>         
   
// General Header Files
#include <general/types.hh>
   
// GenericAPI Header Files   
#include <genericAPI/swigexception.hh>   


// Helper functions   
void eraseEscape( std::string& source, const CHAR& escChar );
bool isEscaped( const std::string& val, INT_4S pos );
   
   
//-------------------------------------------------------------------------------
//   
//: Function to convert ascii into numerical data.
//   
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//   
template< class T > 
T* string2Data( std::string& source, 
  const INT_4U& destSize, const CHAR& delimiter )
{
   std::istringstream ss( source );

   XERCES_CPP_NAMESPACE::ArrayJanitor< T > dest( new T[ destSize ] );
   
   for( INT_4U i = 0; i < destSize; ++i )
   {
      ss >> dest[ i ];
      if( !ss )
      {
         // Bad data
         throw SWIGEXCEPTION( "bad_data_type" );
      }   
      if ( i != ( destSize-1 ) )
      {
         int c = ss.peek();
         if( isspace( c ) || c == delimiter )
         {
            ss.get();
         }
      }
   }

   if( find_if( source.begin() + ss.tellg(), source.end(), not_space )
       != source.end() )
   {
      throw SWIGEXCEPTION( "extra_data" );
   }
   
   return dest.release();
}

   
// Template specializations      
template<>
CHAR* string2Data< CHAR >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter );

template<>
CHAR_U* string2Data< CHAR_U >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter );

template<> 
INT_2S* string2Data< INT_2S >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );

template<> 
INT_2U* string2Data< INT_2U >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );
   
template<> 
INT_4S* string2Data< INT_4S >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );

template<> 
INT_4U* string2Data< INT_4U >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );
   
template<> 
INT_8S* string2Data< INT_8S >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );
   
template<> 
INT_8U* string2Data< INT_8U >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );

template<> 
REAL_4* string2Data< REAL_4 >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );
   
template<> 
REAL_8* string2Data< REAL_8 >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );
   
template<> 
COMPLEX_8* string2Data< COMPLEX_8 >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );
   
template<> 
COMPLEX_16* string2Data< COMPLEX_16 >( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter );

   
//: Function to convert std::string into array data
//
//!exc: SwigException 
//!exc: bad_alloc - Memory allocation failed.
//   
template< class T > 
T* string2ArrayData( std::string& source, 
   const INT_4U& destSize, const CHAR& delimiter )
{
   return string2Data< T >( source, destSize, delimiter );
}
   
   
// Template specializations      
template<>
CHAR* string2ArrayData< CHAR >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter );

template<>
CHAR_U* string2ArrayData< CHAR_U >( std::string& source,
   const INT_4U& destSize, const CHAR& delimiter );

   
#endif
