#include "config.h"

// System Header Files
#include <new>
#include <sstream>   

#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/swigexception.hh>

// XML Header Files 
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/util/Janitor.hpp>   

// ILWD Header Files
#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>
using ILwd::LdasArray;
using ILwd::LdasString;
using ILwd::LdasElement;   

// Local Header Files
#include "lwtime.hh"
#include "lwutil.hh"
#include "query.hh"
#include "convertstr.hh"
#include "lwconsts.hh"   
   
using namespace std;
using namespace XERCES_CPP_NAMESPACE;  
   
   
static const char* xml_name = ":Time:XML";
   

//-----------------------------------------------------------------------
//: Convert LIGO_LW Time into ILWD format element.
//
//!param: const DOMNode* const node - Time element to convert.
//!param: std::deque< Query > dq - Tokenized command.
//
//!return: ILwd::LdasElement* - ILWD format element.
//
//!exc: SwigException
//!exc: bad_alloc - Memory allocation failed.
//      
unique_ptr< LdasElement > time2ILwd( 
   const DOMNode* const node, std::deque< Query > dq ) 
{
   if ( dq.size() != 0 )
   {
      throw SWIGEXCEPTION( "bad_query" );
   }

   string name( nodeAttribute( node, LIGO_LW::nameAttName ) );
   if( name.find( xml_name ) == string::npos )
   {
      name.append( xml_name );
   }

   string type( nodeAttribute( node, LIGO_LW::typeAttName ) );
   string value( nodeValue( node ) );

   
   if( type == "GPS" )
   {
      static const INT_4U dim( 1 );
      static const CHAR delimiter( ' ' );
      ArrayJanitor< REAL_8 > time_val( string2Data< REAL_8 >( value, dim, delimiter ) );
     
      REAL_8 time( *time_val.get() );

      if( time < 0.0f )
      {
         string msg( "Negative value for GPS Time element: name=" );
         msg += name;
         msg += " value=";
         msg += value;
         throw SWIGEXCEPTION( msg );   
      }
   

      static const INT_4U time_dim( 2 );   
      INT_4U time_data[ time_dim ];
      time_data[ 0 ] = static_cast< const INT_4U >( time );
      time_data[ 1 ] = static_cast< const INT_4U >( fmod( time, 1.0 ) * 1e9 );
   
      return unique_ptr< LdasElement >( new LdasArray< INT_4U >( time_data,
                                                               time_dim, name ) );
   }
   else if( type == "ISO-8601" )
   {
      return unique_ptr< LdasElement >( new LdasString( value, name ) );      
   }
   else if( type == "Unix" )
   {
      REAL_8 data;
      istringstream ss( value );
      ss >> data;
   
      if( !ss || 
          data < 0.0f ) 
      {
         string msg( "Bad value for Unix Time element: name=" );
         msg += name;
         msg += " value=";
         msg += value;
         throw SWIGEXCEPTION( msg );   
      }
   
      return unique_ptr< LdasElement >( new LdasArray< REAL_8 >( data, name ) );      
   }

   return unique_ptr< LdasElement >( 0 );
}
