#ifndef LwVectorUtilHH
#define LwVectorUtilHH

#include <cstddef>
#include <vector>

void unwind( std::vector< size_t >& v, size_t size );

#endif
