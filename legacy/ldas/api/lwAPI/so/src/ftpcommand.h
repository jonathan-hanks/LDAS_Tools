#ifndef FTP_COMMAND_H
#define FTP_COMMAND_H

int get_reply_from_fd(int fd);
#define get_reply(a,b) get_reply_(a,b,0)
int get_reply_(FILE* fpm, char* lastReplyString);
#define command(a,b,c,d) command_(a,b,c,d,0)
int command_(char* comm, char* comm_value, FILE* file_in, FILE* file_out, char* lastReplyString);


#endif
