#ifndef QueryHH
#define QueryHH

#include <general/regex.hh>


//------------------------------------------------------------------------------
//
//: Query class.
//
class Query
{
public:
    
    Query( const std::string& name, bool isQuery = false,
	   const std::string& query = "",
           const std::string& value = "" );
    Query( const char* start, const char** final );
    
    const std::string& getName() const;
    bool isQuery() const;
    const std::string& getQuery() const;
    const std::string& getValue() const;
    
private:
    
    std::string mName;
    bool mIsQuery;
    std::string mQuery;
    std::string mValue;

    static const Regex re_item;
    static const Regex re_iquery;
    static const Regex re_query1;
    static const Regex re_query2;    
};


//------------------------------------------------------------------------------
//
//: Constructor.
//
inline Query::Query(
    const std::string& name, bool isQuery,
    const std::string& query, const std::string& value )
        : mName( name ), mIsQuery( isQuery ), mQuery( query ), mValue( value )
{}


//------------------------------------------------------------------------------
//
//: Get Name of the query.
//
inline const std::string& Query::getName() const
{
    return mName;
}


//------------------------------------------------------------------------------
//
//: Is it Query?
//
// This method detects if object is a query. For example,
// this is a query if command was in one of the following formats:
// <p>table{0} 
// <p>table{name = "table1"}
//
inline bool Query::isQuery() const
{
    return mIsQuery;
}


//------------------------------------------------------------------------------
//
//: Get query.
//
// This method gets the query. For example, in the command format like
// name{0} query is 'index' and query value is '0'. If command has a format like
// table{name="table1"} then query is 'name' and query value is 'table1'.
//
inline const std::string& Query::getQuery() const
{
    return mQuery;
}


//------------------------------------------------------------------------------
//
//: Get query value.
//
inline const std::string& Query::getValue() const
{
    return mValue;
}


#endif // QueryHH
