#include "../src/config.h"

#include <iostream>
#include <fstream>   
#include <vector>   
   
// ILWD Header Files   
#include <ilwd/ldaselement.hh>
using ILwd::LdasElement;   
   
// LDAS Header Files
#include <general/util.hh>
#include <general/objectregistry.hh>
#include <general/unittest.h>   
#include <genericAPI/util.hh>
#include <genericAPI/registry.hh>
#include <genericAPI/swigexception.hh>
   
// Local Header Files
#include "lwcmd.hh"
#include "lwdocument.hh"
#include "lwutil.hh"   

// XML Header Files   
#include <xercesc/util/PlatformUtils.hpp>   

using namespace std;   
using namespace XERCES_CPP_NAMESPACE;

// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true    
General::UnitTest lwTest;

   
//   
// Typedef for the pair of two strings representing the file name path
// to the test XML document and exception the document is supposed to 
// raise.
//    
typedef pair< string, string > testData;
   
   
std::string
dataPath( const string& Filename )
{
  // attach source directory to file name
  string path;

  if (getenv("SRCDIR"))
  {
    path  = getenv("SRCDIR");
    path += "/";
  }
  path += Filename;

  return path;
}

int main( int argc, char** argv )
try   
{
   // Initialize test
   lwTest.Init( argc, argv );
   LWDocument* doc( 0 );

   
   vector< testData > testInfo;
   
   // Test#1: malformed xml document( XML parser error )   
   testInfo.push_back( testData( "../../../../api/test/lwAPI/malformed.xml",
                                 "Error parsing LIGO_LW document on line #79 column #1" ) );

   // Test#2: extra data for 'char' type of column.   
   testInfo.push_back( testData( "../../../../api/test/lwAPI/extra_char1.xml",
                                 "extra_data: char" ) );   
   
   // Test#3: extra data for 'char' type of column( octal ).   
   testInfo.push_back( testData( "../../../../api/test/lwAPI/extra_char2.xml",
                                 "extra_data: char" ) );   
   
   // Test#4: underflow value for real_4
   testInfo.push_back( testData( "../../../../api/test/lwAPI/bad_real1.xml",
                                 "out_of_range: real_4" ) );   
   
   // Test#5: invalid value for real_4
   testInfo.push_back( testData( "../../../../api/test/lwAPI/bad_real2.xml",
                                 "bad_data_type: real_4" ) );      
   
   // Test#6: bad data for int_4s type of column( string data )
   testInfo.push_back( testData( "../../../../api/test/lwAPI/bad_int4s_1.xml",
                                 "bad_data_type: int_4s" ) );      
   
   // Test#7: bad data for int_4s type of column( floating point data )
   testInfo.push_back( testData( "../../../../api/test/lwAPI/bad_int4s_2.xml",
                                 "extra_data: int_4s" ) );         
   
   // Test#8: no opening double quote for lstring type of column
   testInfo.push_back( testData( "../../../../api/test/lwAPI/no_open_quote.xml",
                                 "extra_data: before opening double quote" ) );            
   
   // Test#9: no closing double quote for lstring type of column
   testInfo.push_back( testData( "../../../../api/test/lwAPI/no_close_quote.xml",
                                 "malformed_stream_data: missing closing double quote" ) );               
   
   // Test#10: extra data in ilwd:char type of column
   testInfo.push_back( testData( "../../../../api/test/lwAPI/extra_ilwd_char1.xml",
                                 "extra_data: char" ) );                  

   
   vector< testData >::const_iterator iter( testInfo.begin() ),
                                      end_iter( testInfo.end() );
   string msg;
  
   
   try
   {
      XMLPlatformUtils::Initialize();
   }
   catch( const XMLException& exc )
   {
      LIGO_LW::XString msg( exc.getMessage() );
   
      lwTest.Check( false ) << "error initializing xml: "
                            << msg.charForm() << endl;
      lwTest.Exit();      
   }
   
   
   while( iter != end_iter )
   {
      try
      {
         doc = readLwFile( dataPath( iter->first ).c_str() );
         string elem( getLwData( doc, "full" ) );         
      }
      catch( SwigException& e )
      {
         msg = e.getResult();
         if( doc )
         {
            destructLWDocument( doc );
            doc = 0;
         }
   
#if defined( __GNUC__ ) && __GNUC__ < 3
         lwTest.Check( ( msg.compare( iter->second,
				      0, iter->second.size() ) == 0 ),
                        string( "caught exception: " + msg ) );
#else
         lwTest.Check( ( msg.compare( 0, iter->second.size(),
				      iter->second ) == 0 ) )
					<< "caught exception: " << msg
					<< "(Expected: " << iter->second
					<< ")"
					<< std::endl;
#endif
      }
   
      ++iter;
   }

   testInfo.erase( testInfo.begin(), testInfo.end() );

   // Cleanup xml
   XMLPlatformUtils::Terminate();   
   
   lwTest.Exit();
}
catch( bad_alloc& )
{
   XMLPlatformUtils::Terminate();      
   lwTest.Check( false ) << "memory allocation failed"
                         << endl << flush;
   lwTest.Exit();   
}
catch( ... )
{
   XMLPlatformUtils::Terminate();      
   lwTest.Check( false ) << "unknown exception"
                         << endl << flush;
   lwTest.Exit();   
}

   
