## ******************************************************** 
##  meta_toUrl.tcl Version 1.0
##
##  This test driver sends a cmd to operator socket
##  of ligolwAPI standalone process to convert 
##  ilwds to xmls. 
## 
## ******************************************************** 

;#barecode
array set ::${::LDAS_SYSTEM} { HTTPDIR /ldas_outgoing/jobs }
# ligolw host 
;##set port [ string trim $::ligolw(operator) ]
#set port [ string trim $::ligolw(emergency) ]
##set dataport [ string trim $::ligolw(data) ]
#puts "send to ligolwAPI @$host:$port"

set index 0
set jobcnt 0
set basedir "/ldas_usr/ldas/test/database"

set index 1
set jobid TEST1
set email $::options(-email)
puts "email $email"

;##set fname "file:$basedir/${table}${dims}.ilwd"
set data $::options(-data) 
set ext [ file extension $data ]
if  { [ string match "*.xml" $ext ] } {
    set outext .ilwd
} else {
    set outext .xml
}
if  { [ regexp {^http|ftp} $data ] } {
    set infile $data
    set outfile "file:[file tail $data]"
    regsub "$ext$" $outfile $outext outfile
} else {
    if  { ! [ regexp {^file:/} $data  ] } {
        set infile "file:$basedir/$data"
        set outfile "file:$data"
        regsub "$ext$" $outfile $outext outfile
    } else {
        set infile "file:$data"
        #set outfile "file:[ set ::${LDAS_SYSTEM}(HTTPDIR)]/TEST1/[ file tail $data ]"
        set outfile "file:[file tail $data]"
        regsub ${ext}$" $outfile $outext outfile
    }
}

if  {  ! [ string compare $::options(-type) "user" ] } {
        set cmd "ldasJob \{-name $user -password $password -email $email \} \{ILwd2Lw -ingestdata $infile\
        -returnprotocol $outfile -outputformat [ list $::options(-outputformat) ] -subject {test script for ILwd2Lw}\}"                 
} else {
        set cmd "foo TEST$index \{ILwd2Lw -jobid TEST$index -input $infile -returnprotocol $outfile\}" 
}  
puts "submitting job $index, cmd=$cmd\n"
sendCmd $cmd $index    
after 1000    

