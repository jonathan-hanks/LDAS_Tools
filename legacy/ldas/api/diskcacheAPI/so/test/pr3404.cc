// System Header Files

#include <sys/stat.h>
#include <unistd.h>

#include <list>
#include <memory>
#include <sstream>

// General Header Files   
#include "general/unittest.h"

#include "general/types.hh"
#include "general/Directory.hh"
#include "general/SharedPtr.hh"

#include "genericAPI/TCL.hh"

#include "Commands.hh"
#include "diskcache.hh"
#include "diskcachecmd.hh"
#include "TCL.cc"

using namespace diskCache::Commands;

using General::Directory;
using diskCache::Common::LogMessage;

//---------------------------------------------------------------
//---------------------------------------------------------------
General::UnitTest	Test;

typedef std::pair< INT_4U, INT_4U > range_type;
typedef std::list< range_type > ranges_type;
// typedef General::SharedPtr< DirectoryInfo > directory_info_ptr_type;

static void test_dir_cleanup( );
static const std::string& test_dir_name( );
static void test_dir_setup( INT_4U FrameDT,
			    const ranges_type& Ranges );
static void test_dir_populate( INT_4U FrameDT,
			       const ranges_type& Ranges );

static void test_multi_gap( );
typedef  MountPointManagerSingleton::mount_point_name_container_type
mount_point_container_type;

//---------------------------------------------------------------
//---------------------------------------------------------------
int
main( int argc, char **argv )
{
  Test.Init( argc, argv );

  diskCache::Initialize( );

  
  mount_point_container_type			mounts;

  mounts.push_back( test_dir_name( ) );

  {
    MountPointManagerSingleton::UpdateResults	status;

    updateMountPtList( status, mounts, false );
  }

  diskCache::Cache::SDGTx::file_extension_container_type
    extensions;
  extensions.push_back( ".gwf" );

  updateFileExtList( extensions );

  //-------------------------------------------------------------
  // Perform tests
  //-------------------------------------------------------------
  test_multi_gap( );

  //-------------------------------------------------------------
  // Cleanup and exit
  //-------------------------------------------------------------
  // test_dir_cleanup( );
  Test.Exit();
   
  return 0;   
}

//---------------------------------------------------------------
// 
//---------------------------------------------------------------

static void
test_dir_cleanup( )
{
  const std::string&	dir_name( test_dir_name( ) );
  //-------------------------------------------------------------
  // Open the directory and remove all of its contents.
  //-------------------------------------------------------------
  try
  {
    Directory	dir( dir_name );

    if ( dir.Fd( ) >= 0 )
    {
      while( dir.Next( ) )
      {
	std::string	filename( dir_name );
	if ( ( strcmp( dir.EntryName( ), "." ) == 0 ) ||
	     ( strcmp( dir.EntryName( ), ".." ) == 0 ) )
	{
	  // Cannot remove current or parent directory entries.
	  continue;
	}
	filename += "/";
	filename += dir.EntryName( );
	::unlink( filename.c_str( ) );
      }
      dir.Close( );
      ::rmdir( dir_name.c_str( ) );
    }
    else
    {
      ::unlink( dir_name.c_str( ) );
    }
  }
  catch( ... )
  {
  }
}

//---------------------------------------------------------------
// 
//---------------------------------------------------------------
static const std::string&
test_dir_name( )
{
  static std::string	dir;
  if ( dir.length( ) == 0 )
  {
    char cwd[2048];

    if ( ::getcwd( cwd, sizeof( cwd ) ) != (char*)NULL )
    {
      dir = cwd;
    }
    dir += "/pr3404.d";
  }
  return dir;
}

//---------------------------------------------------------------
// 
//---------------------------------------------------------------
static void
test_dir_populate( INT_4U FrameDT,
		const ranges_type& Ranges )
{
  //-------------------------------------------------------------
  // Insure there is nothing left from a previous run
  //-------------------------------------------------------------
  const std::string&	dir_name( test_dir_name( ) );


  for ( ranges_type::const_iterator
	  cur = Ranges.begin( ),
	  last = Ranges.end( );
	cur != last;
	++cur )
  {
    for ( INT_4U
	    start = cur->first;
	  start < cur->second;
	  start += FrameDT )
    {
      std::ostringstream	filename;

      filename << dir_name << "/"
	       << "Z-pr-" << start << "-" << FrameDT
	       << ".gwf";
      std::ofstream( filename.str( ).c_str( ) );
    }
  }
}


//---------------------------------------------------------------
// 
//---------------------------------------------------------------
static void
test_dir_setup( INT_4U FrameDT,
		const ranges_type& Ranges )
{
  //-------------------------------------------------------------
  // Insure there is nothing left from a previous run
  //-------------------------------------------------------------
  const std::string&	dir_name( test_dir_name( ) );

  test_dir_cleanup( );
  //-------------------------------------------------------------
  // Create the directory.
  //-------------------------------------------------------------
  mkdir( dir_name.c_str( ), 0755 );
  test_dir_populate( FrameDT, Ranges );
}


//---------------------------------------------------------------
// 
//---------------------------------------------------------------

static void
test_multi_gap( )
{
  const INT_4U dt( 10 );

  ranges_type	ranges;

#if NEEDED
  DirectoryHash::excluded_dirs_container_type	edirs;
#endif /* NEEDED */

  //-------------------------------------------------------------
  // Create a gappy directory
  //-------------------------------------------------------------
  ranges.push_back( range_type( 100, 120 ) );
  ranges.push_back( range_type( 140, 160 ) );
  ranges.push_back( range_type( 180, 200 ) );
  ranges.push_back( range_type( 220, 240 ) );

  test_dir_setup( dt, ranges );

  //-------------------------------------------------------------
  // Scan the directory, checking to see the correct number
  // of files were discovered.
  //-------------------------------------------------------------


  {
    MountPointManagerSingleton::ScanResults	results;
    std::string					results_string;

    MountPointManagerSingleton::Scan( test_dir_name( ), results );

    GenericAPI::TCL::Translate( results_string, results );
    std::cerr << "DEBUG: " << results_string
	      << std::endl
      ;
  }

  //-------------------------------------------------------------
  // Fill in the gaps
  //-------------------------------------------------------------
  ranges.erase( ranges.begin( ),
		ranges.end( ) );

  ranges.push_back( range_type( 120, 140 ) );
  ranges.push_back( range_type( 160, 180 ) );
  ranges.push_back( range_type( 200, 220 ) );
  ranges.push_back( range_type( 240, 260 ) );
  ranges.push_back( range_type( 300, 320 ) );
  test_dir_populate( dt, ranges );


  //-------------------------------------------------------------
  // Scan the directory, checking to see the correct number
  // of files were discovered.
  //-------------------------------------------------------------

  {
    MountPointManagerSingleton::ScanResults	results;
    std::string					results_string;

    MountPointManagerSingleton::Scan( test_dir_name( ), results );

    GenericAPI::TCL::Translate( results_string, results );
    std::cerr << "DEBUG: " << results_string
	      << std::endl
      ;
  }

  //---------------------------------------------------------------------
  // Conflicting data
  //---------------------------------------------------------------------
  ranges.erase( ranges.begin( ),
		ranges.end( ) );

  ranges.push_back( range_type( 90, 90 + ( dt * 2 ) * 40 ) );
  test_dir_populate( dt * 2, ranges );

  {
    MountPointManagerSingleton::ScanResults	results;
    std::string					results_string;

    MountPointManagerSingleton::Scan( test_dir_name( ), results );

    GenericAPI::TCL::Translate( results_string, results );
    std::cerr << "DEBUG: " << results_string
	      << std::endl
      ;
  }

  //---------------------------------------------------------------------
  // Second pass over conflicting data.
  //---------------------------------------------------------------------
  {
    //-------------------------------------------------------------------
    // Because data is in conflict, the directory should be re-scanned.
    //-------------------------------------------------------------------
    MountPointManagerSingleton::ScanResults	results;
    std::string					results_string;

    MountPointManagerSingleton::Scan( test_dir_name( ), results );

    GenericAPI::TCL::Translate( results_string, results );
    std::cerr << "DEBUG: " << results_string
	      << std::endl
      ;
  }

  // test_dir_cleanup( );
}
