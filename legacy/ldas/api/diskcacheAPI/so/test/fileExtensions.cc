// System Header Files
#include <stdexcept>
#include <sstream>   
#include <algorithm>   
#include <iterator>   
#include <string>   
#include <list>   
   

// General Header Files   
#include "general/unittest.h"
#include "general/types.hh"   

// API header files
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "Commands.hh"
#include "diskcachecmd.hh"

   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true       
   
General::UnitTest	Test;
   
using namespace std;
using namespace diskCache::Commands;
   
int main( int argc, char **argv )
{
  Test.Init( argc, argv );
   
  // MOUNT_PT list used on ldas-dev
  // const CHAR* mount_pt_dirs( "/data/ide1/LHO /data/ide1/LLO /data/ide2/LHO /data/ide2/LLO /data/ide3/LHO /data/ide3/LLO /data/ide4/LHO /data/ide4/LLO /data/ide5/LHO /data/ide5/LLO /data/ide6/LHO /data/ide6/LLO /data/ide7/LHO /data/ide7/LLO /data/ide8/LHO /data/ide8/LLO /data/ide9/LHO /data/ide9/LLO /data/ide10/LHO /data/ide10/LLO /data/ide11/LHO /data/ide11/LLO /data/ide12/LHO /data/ide12/LLO /data/ide15/LHO /data/ide15/LLO /data/ide16/LHO /data/ide16/LLO /data/ide17/LHO /data/ide17/LLO /data/ide1/S2/LHO" );

  Test.Message() << "Initializing file name extension list ..." << endl;   

  {
    diskCache::Cache::SDGTx::file_extension_container_type
      extensions;
    diskCache::Cache::SDGTx::file_extension_container_type
      extensions_read;

    extensions.push_back( ".gwf" );
    extensions.push_back( ".xyz" );
    extensions.push_back( ".abc" );

    updateFileExtList( extensions );

    extensions_read = FileExtList( );

    Test.Check( extensions == extensions_read ) << "Setting of file extension list"
						<< endl;
  }
  try
  {
    diskCache::Cache::SDGTx::file_extension_container_type
      extensions;
    Test.Message() << "Initializing file name extension list with empyt list"
		   << endl; 

    updateFileExtList( extensions );
    Test.Check( false ) << "updateFileExtList: empty list: No exception thrown" 
			  << endl;
  }
  catch( const exception& exc )
  {
     Test.Check( true ) << "updateFileExtList: caught exception: " 
                         << exc.what() << endl;
  }
  catch( ... )
  {
     Test.Check( false ) << "updateFileExtList: caught unknown exception"
			 << endl;
  }
   
  Test.Exit();
   
  return 0;   
}
