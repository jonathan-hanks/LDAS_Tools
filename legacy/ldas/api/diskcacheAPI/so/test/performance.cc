// System Header Files
#include <stdexcept>
#include <sstream>   
#include <algorithm>   
#include <iterator>   
#include <string>   
#include <list>   
   

// General Header Files   
#include "general/unittest.h"
#include "general/types.hh"   

#include "genericAPI/StatFork.hh"
#include "genericAPI/StatPool.hh"

// API header files
#include "TCL.hh"

#include "Commands.hh"
#include "diskcachecmd.hh"

   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true       
   
General::UnitTest	Test;
   
   
using namespace std;
using namespace diskCache::Commands;

using GenericAPI::StatFork;
using GenericAPI::StatPool;
   
   
namespace 
{
   //: Predicate used to check if directory name contains a specific string
   class HasKey : public unary_function< string, bool >
   {
   public:
      
      HasKey( const string& key ); 
      bool operator()( const string& name ) const;
   
   
   private:

      //: A key to match in directory name
      string mKey; 
   };
   
   
   //: Constructor
   //
   //!param: const string& key - Key to match.
   //
   HasKey::HasKey( const string& key )
      : mKey( key )
   {
      if( mKey.empty() )
      {
         throw runtime_error( "HasKey::HasKey(): Non-empty key must be specified for directory search." );
      }
   }
   
   
   //: Compare operator
   //
   //!param: const string& name - Directory name to search.
   //
   //!return: bool - True if directory contains mKey, false otherwise.
   //
   bool HasKey::operator()( const string& name ) const
   {
      return ( name.find( mKey ) != string::npos );
   }

   void
   lstat_pool_cb( StatPool::info_type Key )
   {
      const std::string lstat_command = "../src/lstat";

      StatFork*	flstat = NULL;

      if ( ( flstat =dynamic_cast< StatFork* >( Key ) ) )
      {
         flstat->SetCommand( lstat_command );
      }
   }
}
   
  
void slow_read_dir( const string& DirName )
{
  try
  {
    string subdir_list( getDirEnt( DirName.c_str() ) );
    
    Test.Message() << "->Identified subdirectories for \""
                   << DirName << "\": "
                   << subdir_list << endl << endl;
   
   
#ifdef DISPLAY_TCL_HASH   
    Test.Message() << "Getting Tcl formatted hash ..." << endl;
    string cache = getDirCache();                      
   
    Test.Check( true ) << "***TclCache: ===> " 
                       << cache 
                       << endl << endl;
#endif   
  }
  catch( const exception& exc )
  {
     Test.Check( false ) << "slow_read_dir: caught exception: " << exc.what() << endl;
  }
  catch( ... )
  {
     Test.Check( false ) << "slow_read_dir: caught unknown exception" << endl;
  }
   
  return;
}

   
void do_frame_query()
{
  const CHAR* const ifo( "H" );
  const CHAR* const type( "RDS_R_L3" );   
  const INT_4U start_time( 732200194 );
  
  const INT_4U stop_time( 732200207 );    // 1 frame
  //const INT_4U stop_time( 732201791 );    // 100 frames
  //const INT_4U stop_time( 732360191 );    // 10,000 frames
  //const INT_4U stop_time( 748200191 );    // 1,000,000 frames
   
  //const INT_4U stop_time( 732200511 );   // 20 frames
  //const INT_4U stop_time( 732203391 );   // 200 frames
  //const INT_4U stop_time( 732216191 );   // 1000 frames = 16000secs 
   
  static const bool gaps_allowed( false );

   
  try
  {
    diskCache::Cache::QueryAnswer	frames;

    getFrameFiles( frames,
		   ifo, type,
		   start_time, stop_time,
		   ".gwf",
		   gaps_allowed );
   
    Test.Message() << "->Frame query results for ifo=\"" << ifo 
                   << "\" type=\"" << type << "\" start_time=" << start_time
                   << " stop_time=" << stop_time
                   << ": "
		   << endl
      ;
  }
  catch( const exception& exc )
  {
     Test.Check( false ) << "do_frame_query: caught exception: " << exc.what() << endl;
  }
  catch( ... )
  {
     Test.Check( false ) << "do_frame_query: caught unknown exception" << endl;
  }

   
  return;
}

   
void do_rds_frame_query()
{
  const CHAR* const ifo( "H" );
  const CHAR* const type( "RDS_R_L3" );   
  const INT_4U start_time( 732200194 );
  
  const INT_4U stop_time( 732200207 );      // 1 frame
  //const INT_4U stop_time( 732201791 );    // 100 frames
  //const INT_4U stop_time( 732360191 );    // 10,000 frames
  //const INT_4U stop_time( 748200191 );    // 1,000,000 frames
   
  //const INT_4U stop_time( 732200511 );   // 20 frames
  //const INT_4U stop_time( 732203391 );   // 200 frames
  //const INT_4U stop_time( 732216191 );   // 1000 frames = 16000secs 
   
  static const bool will_resample( true );

   
  try
  {
    diskCache::Cache::QueryAnswer	answer;

    getRDSFrameFiles( answer,
		      ifo, type,
		      start_time, stop_time,
		      ".gwf",
		      will_resample );
    
    Test.Message() << "->Frame query results for ifo=\"" << ifo 
		   << "\" type=\"" << type << "\" start_time=" << start_time
		   << " stop_time=" << stop_time
		   << endl
      ;
   
   
    getRDSFrameFiles( answer, ifo, type,
		      start_time, stop_time,
		      ".gwf",
		      !will_resample );
   
    Test.Message() << "->Frame query results for ifo=\"" << ifo 
                   << "\" type=\"" << type << "\" start_time=" << start_time
                   << " stop_time=" << stop_time
                   << " don't resample: "
		   << endl
      ;
  }
  catch( const exception& exc )
  {
     Test.Check( false ) << "do_rds_frame_query: caught exception: " << exc.what() << endl;
  }
  catch( ... )
  {
     Test.Check( false ) << "do_rds_frame_query: caught unknown exception" << endl;
  }

   
  return;
}

int
main( int argc, char **argv )
{
  try
  {
    Test.Init( argc, argv );


    StatPool::UserInitCB( lstat_pool_cb );

    MountPointManagerSingleton::mount_point_name_container_type
      mount_pt_dirs;

#ifdef TEST_LDAS_DEV_MOUNT_PT_LIST   
   
    // MOUNT_PT list used on ldas-dev
    mount_pt_dirs.push_back( "/data/ide1/LHO" );
    mount_pt_dirs.push_back( "/data/ide1/LLO" );
    mount_pt_dirs.push_back( "/data/ide2/LHO" );
    mount_pt_dirs.push_back( "/data/ide2/LLO" );
    mount_pt_dirs.push_back( "/data/ide3/LHO" );
    mount_pt_dirs.push_back( "/data/ide3/LLO" );
    mount_pt_dirs.push_back( "/data/ide4/LHO" );
    mount_pt_dirs.push_back( "/data/ide4/LLO" );
    mount_pt_dirs.push_back( "/data/ide5/LHO" );
    mount_pt_dirs.push_back( "/data/ide5/LLO" );
    mount_pt_dirs.push_back( "/data/ide6/LHO" );
    mount_pt_dirs.push_back( "/data/ide6/LLO" );
    mount_pt_dirs.push_back( "/data/ide7/LHO" );
    mount_pt_dirs.push_back( "/data/ide7/LLO" );
    mount_pt_dirs.push_back( "/data/ide8/LHO" );
    mount_pt_dirs.push_back( "/data/ide8/LLO" );
    mount_pt_dirs.push_back( "/data/ide9/LHO" );
    mount_pt_dirs.push_back( "/data/ide9/LLO" );
    mount_pt_dirs.push_back( "/data/ide10/LHO" );
    mount_pt_dirs.push_back( "/data/ide10/LLO" );
    mount_pt_dirs.push_back( "/data/ide11/LHO" );
    mount_pt_dirs.push_back( "/data/ide11/LLO" );
    mount_pt_dirs.push_back( "/data/ide12/LHO" );
    mount_pt_dirs.push_back( "/data/ide12/LLO" );
    mount_pt_dirs.push_back( "/data/ide15/LHO" );
    mount_pt_dirs.push_back( "/data/ide15/LLO" );
    mount_pt_dirs.push_back( "/data/ide16/LHO" );
    mount_pt_dirs.push_back( "/data/ide16/LLO" );
    mount_pt_dirs.push_back( "/data/ide17/LHO" );
    mount_pt_dirs.push_back( "/data/ide17/LLO" );
    mount_pt_dirs.push_back( "/data/ide1/S2/LHO" );
   
#else

    mount_pt_dirs.push_back( getenv( "LDAS_TOP" ) );   
   
#endif   
    
   
    Test.Message() << "Initializing MOUNT_PT..." << endl;   
    MountPointManagerSingleton::UpdateResults
      mount_pts_status;

    updateMountPtList( mount_pts_status, mount_pt_dirs );

    std::string	msg;
    diskCache::TCL::Translate( msg, mount_pts_status );
   
    Test.Check( msg.empty( ) == false )
      << "MOUNT_PT list: "
      << msg
      << endl
      ;
   
    Test.Message() << "Initializing extension list..."
		   << endl
      ;
    diskCache::Cache::SDGTx::file_extension_container_type
      extensions;
    diskCache::Cache::SDGTx::file_extension_container_type
      extensions_results;

    extensions.push_back( ".gwf" );
    updateFileExtList( extensions );

    extensions_results = FileExtList( );
   
    Test.Check( extensions_results == extensions )
      << "extension list: "
      << endl
      ;
   
   
#ifdef TEST_EXCLUDED_DIRS   
    // Block  directories   
    static const CHAR* block_sdir( "rds" );   
   
    Test.Message() << "Setting excluded directory list: " << block_sdir << endl;            

    string removed_sdirs = excludedDirList( block_sdir );
   
    Test.Message() << "Removed subdirectories: "
		   << removed_sdirs
		   << endl << endl;         
#endif   
   

#if WORKING
    // Extract directories from the list
    list< string > dirs;
    istringstream dirs_stream( mount_pt_dirs );   
   
    copy( istream_iterator< string >( dirs_stream ),
	  istream_iterator< string >(), 
	  back_inserter( dirs ) );  

   
    const string GappyDir( dirs.back() );
    // Gappy data increases look ups ~ 50-90 times:
    // dirs.pop_back();
   
   
    //#define CREATE_FRAME_HASH   
#ifdef CREATE_FRAME_HASH   
    for( list< string >::const_iterator iter = dirs.begin(), end_iter = dirs.end();
	 iter != end_iter; ++iter )
    {
      Test.Message() << "Slow scan of " << (*iter) << endl;         
      slow_read_dir( *iter );       
    }
#endif   
   
   
    // Frame query
    do_frame_query();
   

   
#ifdef TEST_EXCLUDED_DIRS   
    Test.Message() << "Resetting excluded directory list to be empty..." << endl;               
    string empty = excludedDirList( "" );
   
    Test.Message() << "Removed subdirectories: "
		   << empty
		   << endl << endl;            
   
   
    // Rescan directories in "dirs" to pick up previously excluded directories
    for( list< string >::const_iterator iter = dirs.begin(), end_iter = dirs.end();
	 iter != end_iter; ++iter )
    {
      Test.Message() << "After resetting excluded list: rescan of " << (*iter) << endl;         
      slow_read_dir( *iter );       
    }      
#endif   

   
    // Test I/O
    try
    {
      static const CHAR* TestCacheFile( "test.cache" );
   
      // Write cache to the file   
      writeDirCache( TestCacheFile );   

      
      // Read cache from the file
      readDirCache( TestCacheFile );      
    }
    catch( const exception& exc )
    {
      Test.Check( false ) << "IO test: caught exception: "
			  << exc.what() << endl << endl;
    }
    catch( ... ) 
    {
      Test.Check( false ) << "IO test: caught unknown exception: " << endl << endl;
    }
   
   
   
    // Do the same frame query
    Test.Message() << "Frame query after readDirCache..." << endl;
    do_frame_query();   

   
    // Do RDS frame query
    Test.Message() << "RDS frame query after readDirCache..." << endl;
    do_rds_frame_query();   
   
   
#ifdef TEST_LDAS_DEV_MOUNT_PT_LIST      
    // Remove some directories from MOUNT_PT list 
    const string Key( "LHO" );  
    dirs.remove_if( HasKey( Key ) );
   
   
    // Keep gappy data
    if( GappyDir.find( Key ) != string::npos )
    {
      // Add /data/ide1/S2/LHO to the list
      dirs.push_back( GappyDir );
    }
   
   
    ostringstream updated_mount_pt;
    copy( dirs.begin(), dirs.end(),
	  ostream_iterator< string >( updated_mount_pt, " " ) );
   
  
    // Update MOUNT_PT list
    Test.Message() << "Removing \"" << Key << "\" directories from MOUNT_PT..." << endl;
    string removed_dirs = updateMountPtList( updated_mount_pt.str().c_str() );
   
    Test.Check( removed_dirs.empty() == false ) << "Removed subdirectories: "
						<< removed_dirs
						<< endl << endl;      
   

#ifdef DISPLAY_TCL_HASH_ONCE
    Test.Message() << "Getting Tcl formatted hash ..." << endl;
    string cache = getDirCache();                      
   
    Test.Check( true ) << "***TclCache: ===> " 
		       << cache 
		       << endl << endl;   
#endif   
   
   
  
    // Do the same frame query
    do_frame_query();   
   
#endif   
   

    // Rescan directories in "dirs"
    for( list< string >::const_iterator iter = dirs.begin(), end_iter = dirs.end();
	 iter != end_iter; ++iter )
    {
      Test.Message() << "Rescan of " << (*iter) << endl;
      slow_read_dir( *iter );       
    }   
    Test.Message() << "Rescan completed" << endl;         

   
    // Do the same frame query
    do_frame_query();   
   
   
#ifdef TEST_LDAS_DEV_MOUNT_PT_LIST      
   
    // Block "S2" directories   
    static const CHAR* dirs_to_block( "LLO" );   
   
    Test.Message() << "Setting excluded directory list: " << dirs_to_block << endl;            
    removed_dirs = excludedDirList( dirs_to_block );
   
    Test.Check( removed_dirs.empty() == false ) << "Removed subdirectories: "
						<< removed_dirs
						<< endl << endl;         

    // Do the same frame query
    do_frame_query();   
#endif   
#endif /* WORKING */
   
   
  }
  catch( const std::exception& Exception ) {
    Test.Check( false )
      << "Caught std::exception: " << Exception.what( )
      << std::endl;
  }
  catch( ... )
  {
    Test.Check( false ) << "Caught an unexpected exception" << std::endl;
    Test.Exit( );
  }

  Test.Exit();
   
  return 0;   
}
