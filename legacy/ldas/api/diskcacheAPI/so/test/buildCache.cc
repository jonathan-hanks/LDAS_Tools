// diskcacheAPI Header Files
#include "diskcachecmd.hh"
   
// System Header Files   
#include <iostream>   
#include <sstream>   
#include <list>   
#include <algorithm>   
#include <iterator>   

#include <general/types.hh>
   
using namespace std;

   
const string scanDirectory( const string& dir )   
{
   try
   {
      string subdir_list( getDirEnt( dir.c_str() ) );

      return subdir_list;
   }
   catch( const exception& exc )
   {
      string msg( "Caught exception: ");
      msg += exc.what();

      return msg;   
   }
   catch( ... )
   {
      string msg( "Caught unknown exception." );
      return msg;
   }

   
   return "";
}

   
const string writeCache()
{
   static const CHAR* TestCacheFile( "test.cache" );
   
   try
   {
      // Write cache to the file   
      writeDirCache( TestCacheFile );
   }
   catch( const exception& exc )
   {
      string msg( "Caught exception: " );
      msg += exc.what();
   
      return msg;
   }
   catch( ... ) 
   {
      string msg( "Caught unknown exception." );
      return msg;
   }

   
   return "";
}

   
const string do_frame_queries()
{
  const CHAR* const ifo( "H" );
  const CHAR* const type( "RDS_R_L3" );   
  const INT_4U start_time( 732200192 );
  
  //const INT_4U stop_time_1frame( 732200208 );  
  const INT_4U stop_time_10frames(  732200351 );
  const INT_4U stop_time_100frames( 732201791 );
  const INT_4U stop_time_1000frames( 732216191 );

  //const INT_4U stop_time( 732360192 );    // 10,000 frames
  //const INT_4U stop_time( 748200192 );    // 1,000,000 frames
  //const INT_4U stop_time( 732200512 );   // 20 frames
  //const INT_4U stop_time( 732203392 );   // 200 frames
  //const INT_4U stop_time( 732216192 );   // 1000 frames = 16000secs 
   
  static const bool gaps_allowed( false );

   
  try
  {
    string frames10( getFrameFiles( ifo, type, start_time, stop_time_10frames, gaps_allowed ) );
   
    cout << "->Frame query results for ifo=\"" << ifo 
         << "\" type=\"" << type << "\" start_time=" << start_time
         << " stop_time=" << stop_time_10frames
         << " (10 frames expected): " << frames10 << endl << endl;


    
    string frames100( getFrameFiles( ifo, type, start_time, stop_time_100frames, gaps_allowed ) );
   
    cout << "->Frame query results for ifo=\"" << ifo 
         << "\" type=\"" << type << "\" start_time=" << start_time
         << " stop_time=" << stop_time_100frames
         << " (100 frames expected): " << frames100 << endl << endl;


    string frames1000( getFrameFiles( ifo, type, start_time, stop_time_1000frames, gaps_allowed ) );
   
    cout << "->Frame query results for ifo=\"" << ifo 
         << "\" type=\"" << type << "\" start_time=" << start_time
         << " stop_time=" << stop_time_1000frames
         << " (1000 frames expected): " << frames1000 << endl << endl;

  }
  catch( const exception& exc )
  {
     string msg( "Caught exception: " );
     msg += exc.what();
   
     return msg;
  }
  catch( ... )
  {
     return "Caught unknown exception.";
  }

   
  return "";
}  
   
   
int main()
{
   // From LDAS-DEV
   const CHAR* mount_pt_list( "/ldas_outgoing/mirror/frames /ldas_outgoing/mirror/dmt /ldas_outgoing/frames /ldas_outgoing/test/frames /ldas_outgoing/test/test_uncompressed /data/ide1/LHO /data/ide1/LLO /data/ide2/LHO /data/ide2/LLO /data/ide3/LHO /data/ide3/LLO /data/ide4/LHO /data/ide4/LLO /data/ide5/LHO /data/ide5/LLO /data/ide6/LHO /data/ide6/LLO /data/ide7/LHO /data/ide7/LLO /data/ide8/LHO /data/ide8/LLO /data/ide9/LHO /data/ide9/LLO /data/ide10/LHO /data/ide10/LLO /data/ide11/LHO /data/ide11/LLO /data/ide12/LHO /data/ide12/LLO /data/ide15/LHO /data/ide15/LLO /data/ide16/LHO /data/ide16/LLO /data/ide17/LHO /data/ide17/LLO /archive/S3 /archive/LLO /archive/LHO /archive/GEO/full/S1" );

   
    // Only IDE's
    //const CHAR* mount_pt_list( "/data/ide1/LHO /data/ide1/LLO /data/ide2/LHO /data/ide2/LLO /data/ide3/LHO /data/ide3/LLO /data/ide4/LHO /data/ide4/LLO /data/ide5/LHO /data/ide5/LLO /data/ide6/LHO /data/ide6/LLO /data/ide7/LHO /data/ide7/LLO /data/ide8/LHO /data/ide8/LLO /data/ide9/LHO /data/ide9/LLO /data/ide10/LHO /data/ide10/LLO /data/ide11/LHO /data/ide11/LLO /data/ide12/LHO /data/ide12/LLO /data/ide15/LHO /data/ide15/LLO /data/ide16/LHO /data/ide16/LLO /data/ide17/LHO /data/ide17/LLO" );


   // Gappy data only
   //const CHAR* mount_pt_list( "/data/ide1/S2 /data/ide2/S2 /data/ide3/S2 /data/ide4/S2 /data/ide5/S2 /data/ide6/S2 /data/ide7/S2 /data/ide8/S2 /data/ide9/S2 /data/ide10/S2 /data/ide11/S2 /data/ide12/S2 /data/ide15/S2 /data/ide16/S2 /data/ide17/S2" );
   
   
   list< string > dirs;
   istringstream dirs_stream( mount_pt_list );   
   
   copy( istream_iterator< string >( dirs_stream ),
         istream_iterator< string >(), 
         back_inserter( dirs ) );  


   //copy( dirs.begin(), dirs.end(), ostream_iterator< string >( cout, " " ) );
   
   
   // Set MOUNT_PT list
   string mount_entries( updateMountPtList( mount_pt_list ) );
   cout << "Set MOUNT_PT list: " << mount_entries << endl;
   
   
   // Scan each entry in the list, write cache to the file after each scan
   for( list< string >::const_iterator iter = dirs.begin(), end_iter = dirs.end();
        iter != end_iter; ++iter )
   {
      cout << "Scanning \"" << (*iter) << "\"..." << endl;         
      const string scan_result = scanDirectory( *iter );       
      cout << "--->done: "<< scan_result << endl;
   

#ifdef WRITE_WITHIN_LOOP   
      // Write current cache to the file
      cout << "Writing cache to the file..." << endl;            
      const string write_result = writeCache();
      cout << "--->done: "<< write_result << endl;   
#endif   
   }   
   
   
   // Write current cache to the file
   cout << "Writing cache to the file..." << endl;            
   const string write_result = writeCache();
   cout << "--->done: "<< write_result << endl;   
   
   
   //do_frame_queries();
   
   return 0;
}
