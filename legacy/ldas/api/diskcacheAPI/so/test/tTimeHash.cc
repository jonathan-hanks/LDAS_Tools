// System Header Files
#include <memory>

// General Header Files   
#include "general/unittest.h"

// API header files
#include "timehash.hh"

   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true       
   
General::UnitTest	Test;
using diskCache::TimeHash;

typedef TimeHash::KnownIntervals::ranges_type ranges_type;
   
//---------------------------------------------------------------
//---------------------------------------------------------------
struct entry {
  INT_4U	s_start_time;
  INT_4U	s_stop_time;
  INT_4U	s_delta_t;
};

struct frame_entry {
  INT_4U	s_start_time;
  INT_4U	s_delta_t;

  inline
  frame_entry( INT_4U Start, INT_4U DT )
    : s_start_time( Start ),
      s_delta_t( DT )
  {
  }
};

inline std::auto_ptr< ranges_type >
create_range( const entry* Source, const INT_2U SourceSize )
{
  std::auto_ptr< ranges_type >	retval( new ranges_type );

  for ( INT_2U x = 0;
	x < SourceSize;
	++x )
  {
    (*retval)[ Source[ x ].s_start_time ]
      = diskCache::TimeHash::IntervalInfo( Source[ x ].s_stop_time,
					   Source[ x ].s_delta_t );
  }

  return retval;
}

inline void
extend_frame_entry_container( const ranges_type& Source, std::list< frame_entry >& Dest )
{
  for ( ranges_type::const_iterator
	  cur = Source.begin( ),
	  last = Source.end( );
	cur != last;
	++cur )
  {
    INT_4U	start_time = cur->first;
    INT_4U	dt = cur->second.mFileDt;
    for ( INT_4U
	    end_time = start_time + dt,
	    last_end_time = cur->second.mStopTime;
	  start_time < last_end_time;
	  start_time = end_time,
	    end_time += dt )
    {
      Dest.push_back( frame_entry( start_time, dt ) );
    }
  }
}


//---------------------------------------------------------------
//---------------------------------------------------------------
void
test_frame( const char* Func,
	    const ranges_type& Source, const ranges_type Extension,
	    const std::string& ExpectedAnswer )
{
  std::list< frame_entry >	data;
  TimeHash			th;

  
  extend_frame_entry_container( Source, data );
  extend_frame_entry_container( Extension, data );

  for ( std::list< frame_entry >::const_iterator
	  cur = data.begin( ),
	  last = data.end( );
	cur != last;
	++cur )
  {
    th.Add( cur->s_start_time,
	    cur->s_delta_t );
  }

  std::string record;
  std::string msg = th.getTclHash( record, 0 );

  Test.Check( ExpectedAnswer.compare( msg ) == 0 )
    << Func << ": frame: "
    << msg << " =?= " << ExpectedAnswer
    << std::endl
    ;
}

//---------------------------------------------------------------
//---------------------------------------------------------------
void
test( const char* Func,
      const ranges_type& Source, const ranges_type Extension,
      const std::string& ExpectedAnswer )
{
  test_frame( Func, Source, Extension, ExpectedAnswer );

  TimeHash			th;
  TimeHash::KnownIntervals	s_ki( Source );
  TimeHash::KnownIntervals	e_ki( Extension );

  th.Add( s_ki );
  th.Add( e_ki );

  std::string record;
  std::string msg = th.getTclHash( record, 0 );

  Test.Check( ExpectedAnswer.compare( msg ) == 0 )
    << Func << ": known_interval: "
    << msg << " =?= " << ExpectedAnswer
    << std::endl
    ;
}

//---------------------------------------------------------------
//---------------------------------------------------------------
void
gap_create( )
{
  static const char* func = "gap_create";

  static entry old_data[] =
    {
      { 10, 20, 10 },
      { 50, 60, 10 },
    };

  static entry new_data[] =
    {
      { 30, 40, 10 }
    };

  //-------------------------------------------------------------
  // Create time range data.
  //-------------------------------------------------------------
  std::auto_ptr< ranges_type >
    old_tr( create_range( old_data,
			  sizeof( old_data ) / sizeof( *old_data ) ) );
  std::auto_ptr< ranges_type >
    new_tr( create_range( new_data,
			  sizeof( new_data ) / sizeof( *new_data ) ) );

  test( func, *old_tr, *new_tr, "10 0 3 {10 20 30 40 50 60} " );
}

//---------------------------------------------------------------
//---------------------------------------------------------------
void
gap_create_begin( )
{
  static const char* func = "gap_create_begin";

  static entry old_data[] =
    {
      { 10, 20, 10 },
      { 50, 60, 10 },
    };

  static entry new_data[] =
    {
      { 20, 30, 10 }
    };

  //-------------------------------------------------------------
  // Create time range data.
  //-------------------------------------------------------------
  std::auto_ptr< ranges_type >
    old_tr( create_range( old_data,
			  sizeof( old_data ) / sizeof( *old_data ) ) );
  std::auto_ptr< ranges_type >
    new_tr( create_range( new_data,
			  sizeof( new_data ) / sizeof( *new_data ) ) );

  test( func, *old_tr, *new_tr, "10 0 3 {10 30 50 60} " );
}

//---------------------------------------------------------------
//---------------------------------------------------------------
void
gap_create_end( )
{
  static const char* func = "gap_create_end";

  static entry old_data[] =
    {
      { 10, 20, 10 },
      { 50, 60, 10 },
    };

  static entry new_data[] =
    {
      { 40, 50, 10 }
    };

  //-------------------------------------------------------------
  // Create time range data.
  //-------------------------------------------------------------
  std::auto_ptr< ranges_type >
    old_tr( create_range( old_data,
			  sizeof( old_data ) / sizeof( *old_data ) ) );
  std::auto_ptr< ranges_type >
    new_tr( create_range( new_data,
			  sizeof( new_data ) / sizeof( *new_data ) ) );

  test( func, *old_tr, *new_tr, "10 0 3 {10 20 40 60} " );
}

//---------------------------------------------------------------
//---------------------------------------------------------------
void
gap_fill( )
{
  static const char* func = "gap_fill";

  static entry gap_data[] =
    {
      { 10, 20, 10 },
      { 30, 40, 10 }
    };

  static entry filler_data[] =
    {
      { 20, 30, 10 }
    };

  //-------------------------------------------------------------
  // Create time range data.
  //-------------------------------------------------------------
  std::auto_ptr< ranges_type >
    gap_tr( create_range( gap_data,
			  sizeof( gap_data ) / sizeof( *gap_data ) ) );
  std::auto_ptr< ranges_type >
    filler_tr( create_range( filler_data,
			     sizeof( filler_data ) / sizeof( *filler_data ) ) );

  test( func, *gap_tr, *filler_tr, "10 0 3 {10 40} " );
}

//---------------------------------------------------------------
//---------------------------------------------------------------
void
new_beginning( )
{
  static const char* func = "new_beginning";

  static entry old_data[] =
    {
      { 10, 20, 10 },
    };

  static entry new_data[] =
    {
      { 0, 10, 10 }
    };

  //-------------------------------------------------------------
  // Create time range data.
  //-------------------------------------------------------------
  std::auto_ptr< ranges_type >
    old_tr( create_range( old_data,
			  sizeof( old_data ) / sizeof( *old_data ) ) );
  std::auto_ptr< ranges_type >
    new_tr( create_range( new_data,
			     sizeof( new_data ) / sizeof( *new_data ) ) );

  test( func, *old_tr, *new_tr, "10 0 2 {0 20} " );
}

//---------------------------------------------------------------
//---------------------------------------------------------------
void
new_ending( )
{
  static const char* func = "new_ending";

  static entry old_data[] =
    {
      { 10, 20, 10 },
    };

  static entry new_data[] =
    {
      { 20, 30, 10 }
    };

  //-------------------------------------------------------------
  // Create time range data.
  //-------------------------------------------------------------
  std::auto_ptr< ranges_type >
    old_tr( create_range( old_data,
			  sizeof( old_data ) / sizeof( *old_data ) ) );
  std::auto_ptr< ranges_type >
    new_tr( create_range( new_data,
			     sizeof( new_data ) / sizeof( *new_data ) ) );

  test( func, *old_tr, *new_tr, "10 0 2 {10 30} " );
}

//---------------------------------------------------------------
//---------------------------------------------------------------
int
main( int argc, char **argv )
{
  Test.Init( argc, argv );
  
  //-------------------------------------------------------------
  // Perform tests
  //-------------------------------------------------------------
  new_beginning( );
  gap_fill( );
  new_ending( );

  gap_create( );
  gap_create_begin( );
  gap_create_end( );

  Test.Exit();
   
  return 0;   
}
