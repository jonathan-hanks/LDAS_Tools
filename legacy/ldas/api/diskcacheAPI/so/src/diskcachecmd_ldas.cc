/* -*- mode: c++; c-basic-offset: 3; indent-tabs-mode: nil; -*- */

#ifdef DEBUG_TIME
// System Header Files   
#include <sys/time.h>   
#include <time.h>   
#endif /* DEBUG_TIME */

#include <iostream>
#include <iomanip>
#include <list>

#include "diskcache.hh"
#include "diskcachecmd.hh"

#include "general/AtExit.hh"
#include "general/ConditionalVariable.hh"
#include "general/ldasexception.hh"
#include "general/Task.hh"
#include "general/TaskThread.hh"
#include "general/ThreadPool.hh"

// GenericAPI Header Files
#include "genericAPI/Logging.hh"
#include "genericAPI/thread.hh"

// Local Header Files

#include "diskcacheAPI/Cache/Directory.hh"
#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/QueryParams.hh"
#include "diskcacheAPI/Cache/RegistrySingleton.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "DumpCacheDaemon.hh"
#include "DirectoryManagerSingleton.hh"
#include "MountPointScanner.hh"
#include "ScanMountPointsDaemon.hh"


using std::ios_base;
using std::istringstream;
using std::ostringstream;
using std::runtime_error;
using std::string;

using namespace diskCache;
using General::ifstream;
using GenericAPI::LogEntryGroup_type;
using GenericAPI::queueLogEntry;

#if 0
namespace {
   typedef diskCache::MountPointScanner::mount_point_container_type
   mount_point_container_type;

   //--------------------------------------------------------------------
   /// \brief Debug utility to track down execution time.   
   //--------------------------------------------------------------------
   class Timer
   {
   public:
    
      Timer();
      void getDelta( const std::string& method );
   
   private:
    
      struct timeval mTv;
      INT_4S mLastSec;
      INT_4S mLastUsec;   
   };
   

   Timer::Timer()
   {
#ifdef DEBUG_TIME   
      gettimeofday( &mTv, NULL );
      mLastSec = mTv.tv_sec;
      mLastUsec = mTv.tv_usec;      
#endif   
   }
   
   
   void Timer::
   getDelta( const std::string& method )
   {
#ifdef DEBUG_TIME      
      gettimeofday( &mTv, NULL );
   
      REAL_4 delta( mTv.tv_sec - mLastSec + ( mTv.tv_usec - mLastUsec ) * 1e-6 );
   
      cout << "--->DEBUG_TIME: clock time {" << method << "}: "
           << delta << endl;   
   
      mLastSec = mTv.tv_sec;
      mLastUsec = mTv.tv_usec;
#endif   
   
      return;
   } // method - Timer::getDelta
   
#if OLD
   void
   query_mount_points(  FrameQuery& query,
                        const char* ifo,
                        const char* type,
                        const unsigned int start_time,
                        const unsigned int stop_time )
   {
#if WORKING
      // Get current MOUNT_PT list
      const vector< string >
         mount_pt_list( MountPointHash::getMountPtList( ) );
   

      // Fix for problem report #2349:   
      // need to do a substring match for provided single character "ifo" - 
      // match any ifo that contains that single character
      if( strlen( ifo ) == 1 )
      {
         // Look up frame data under each MOUNT_PT entry
         for( vector< string >::const_iterator
                 iter = mount_pt_list.begin(),
                 end_iter = mount_pt_list.end();
              iter != end_iter;
              ++iter )
         {
            // FIX PR 2390: new query method: pass start,stop into search
            if( query( MountPointHash::
                       getIFOTypeHashes( *ifo,
                                         type,
                                         *iter,
                                         start_time, stop_time ) ) )
            {
               break;
            }
         }      
      }
      else
      {   
         const string ifo_type( query.getIFOType() );      
      
         // Look up frame data under each MOUNT_PT entry
         for( vector< string >::const_iterator
                 iter = mount_pt_list.begin(),
                 end_iter = mount_pt_list.end();
              iter != end_iter;
              ++iter )
         {
            // FIX PR 2390: new query method: pass start,stop into search
            if( query( MountPointHash::
                       getIFOTypeHashes( ifo_type,
                                         *iter,
                                         start_time, stop_time ) ) )
            {
               break;
            }
         }
      }
#endif /* WORKING */
   } // function - query_mount_points
#endif /* OLD */


   inline void
   is_first_scan_complete( const char* const File,
                           int Line )
   {
#if WORKING
      if ( diskCache::MountPointScanner::FirstScanComplete( )
           == false )
      {
         std::ostringstream     msg;

         msg << "The in memory database of the file systems is currently unavailable"
             << " due to a request to do a full resync."
            ;

         std::ostringstream     info;
         info << "There are several reasons this may occur."
              << " 1) When the diskcacheAPI is restarted."
              << " 2) When a request has been made to rebuild the cache."
              << " 3) When the list of mount points has been changed."
              << " In all of these cases, the system must process each entry"
              << " in the list of mount points once before resuming normal"
              << " system processing."
            ;
         throw LdasException( Library::DISKCACHEAPI, -1,
                              msg.str( ), info.str( ),
                              File, Line );
      }
#endif /* WORKING */
   }
      
   //====================================================================================
   //
   //====================================================================================
   std::string
   ms_format( const General::GPSTime& DT )
   {
      std::ostringstream	msg;

      INT_4U sec( DT.GetSeconds( ) );
      INT_4U hrs( sec / 3600 );
      sec %= 3600;
      INT_4U min( sec / 60 );
      sec %= 60;
      INT_4U ms = DT.GetNanoseconds( ) / 1000000;


      msg.fill( '0' );

      msg << std::setw( 2 ) << hrs
          << ":" << std::setw( 2 ) << min
          << ":" << std::setw( 2 ) << sec
          << "." << std::setw( 3 ) << ms
         ;

      return msg.str( );
   }

} // namespace - anonymous
   
   
namespace diskCache
{
   //--------------------------------------------------------------------
   //
   //: Obtain the list of file name extensions
   //
   //!usage: set ext_list [ getFileExtList ]
   // 
   //!param: none 
   // 
   //!return: std::string
   //
#if CORE_COMMANDS
   const Cache::SDGTx::file_extension_container_type&
   FileExtList()
   {
      return Cache::SDGTx::FileExtList( );
   }
#endif /* CORE_COMMANDS */
} // namespace - diskCache
   
//------------------------------------------------------------------------------//-----------------------------------------------------------------------
///
/// \brief Gets directory information that is listed in MOUNT_PT list.
///   
/// This method is used to introduce directory to the global hash map and to
/// rescan existing hash entry for any changes.
///   
/// \param[in] dirpath
///     Directory path to examine.
///
/// \return
/// A space separated list of all identified and scanned subdirectories.  
///
//-----------------------------------------------------------------------
#if CORE_COMMANDS
string
getDirEnt( const std::string dirpath )
{
   Timer tracker;
   
#if DEPRICATED_INTERFACE
   const string result = MountPointHash::addEntry( dirpath );         
#endif /* DEPRICATED_INTERFACE */
#if NEW_INTERFACE
#if NEW_DIRECTORY_INTERFACE
   DirectoryManagerSingleton::ScanResults      r;
   DirectoryManagerSingleton::Scan( dirpath, r );
#else /* NEW_DIRECTORY_INTERFACE */
   MountPointManagerSingleton::ScanResults      r;
   MountPointManagerSingleton::Scan( dirpath, r );
#endif /* NEW_DIRECTORY_INTERFACE */
   //--------------------------------------------------------------------
   // \todo
   // Convert the results stored in r into a string for backwards
   // compatability
   //--------------------------------------------------------------------
   const string result = "";
#endif /* NEW_INTERFACE */
   
   string msg( "getDirEnt( \"" );
   msg += dirpath;
   msg += "\" )";
   tracker.getDelta( msg );

   return result;
}
#endif /* CORE_COMMANDS */
  
#if defined( CREATE_THREADED1 )
CREATE_THREADED1( getDirEnt, std::string, const std::string )
#endif /* defined( CREATE_THREADED1 ) */

   
   
#if 0
//-----------------------------------------------------------------------
/// Update list of mount point directories (as they appear in
/// the resource file): initialize list if called at API startup time;
///                     otherwise, check if any MOUNT_PT directory
///                     has been pruned, and remove that directory from
///                     global hash.   
///
/// \param[in] dir_list
///       A list of directories as they appear
///       in MOUNT_PT list (API resource file variable).
/// \param[in] enable_global_check
///       A flag to indicate if API should check for data conflicts
///       across all MOUNT_PT entries (diskcacheAPI\@LDAS reports data
///       conflicts only under the same  MOUNT_PT).
///       This flag should be set to TRUE only when called by outside of 
///       LDAS utility, such as /ldas/bin/cacheCheck.   
///       Default is FALSE.   
///   
/// \return
///     Tcl list of all removed subdirectories.
//-----------------------------------------------------------------------
void
updateMountPtList( MountPointManagerSingleton::UpdateResults& update_status,
                   const MountPointManagerSingleton::mount_point_name_container_type&
                   Paths,
                   const bool enable_global_check
                   )
{
   /// \todo Need to have dir_list come in a C++ structure instead of TCL list
   Timer tracker;   
   
   MountPointManagerSingleton::Update( Paths, update_status );

   string msg( "updateMountPtList( \"" );
   for ( MountPointManagerSingleton::mount_point_name_container_type::const_iterator
            cur = Paths.begin( ),
            last = Paths.end( );
         cur != last;
         ++cur )
   {
      msg += *cur;
      msg += " ";
   }
   msg += "\" )";
   tracker.getDelta( msg );

   return;
}
#endif /* 0 */
   
//-----------------------------------------------------------------------
///   
/// \brief Get ASCII representation of the current frame hash
///
/// \param[out] Answer
///     A collection of files that match the criteria.
/// \param[in] Ifo
///     An ifo to look up. Default is "all".
/// \param[in] Type
///     A type to look up. Default is "all".
///   
//-----------------------------------------------------------------------
#if CORE_COMMANDS
void
getDirCache( Cache::QueryAnswer& Answer, const char* Ifo, const char* Type )
{
   is_first_scan_complete( __FILE__, __LINE__ );

   Timer tracker;   
   
   string data;
   //--------------------------------------------------------------------
   // Set the search criteria
   //--------------------------------------------------------------------
   Cache::QueryParams   q;

   q.AddParam( "index", Cache::SDGTx::AsciiId );

   q.AddParam( "site", Ifo );
   q.AddParam( "description", Type );

   try
   {
      Cache::RegistrySingleton::TranslateQuery( q, Answer );
      MountPointManagerSingleton::Find( Answer );
   }
   catch( const Cache::QueryParams::MissingVariableError& Error )
   {
   }
   
   tracker.getDelta( "getDirCache()" );
}
#endif /* CORE_COMMANDS */

//-----------------------------------------------------------------------
///
/// Get TCL formatted lists for each mount point with name or mount point,
/// number of directories and number of files for data matching the specified
/// ifo and type.  Return lists are of the form:
///   {mountpoint_name number_of_dirs number_of_files }
///
/// \param[in] ifo
///     An ifo to look up. Default is "all".
/// \param[in] type
///     A type to look up. Default is "all".
//
/// \return
///     A list for each mount point with name, number of
///     directories and number of files under that mount point:
///     {mountpoint_name number_of_dirs number_of files }
///
//-----------------------------------------------------------------------
#if CORE_COMMANDS
void
getHashNumbers( diskCache::Cache::QueryAnswer& Answer,
                const char* Ifo, const char* Type )
{
   is_first_scan_complete( __FILE__, __LINE__ );

   // Debug timer
   Timer tracker;

   /// \todo Need to get getHashNumber queries working
#if 0
   data = MountPointHash::getHashNumbers( ifo, type );
#endif /* 0 */

   tracker.getDelta( "getHashNumbers()" );   
}
#endif /* CORE_COMMANDS */

//------------------------------------------------------------------------------
/// Get TCL formatted lists of file names for data matching the specified
/// IFO and Type within the bounds of the query.
//------------------------------------------------------------------------------
#if CORE_COMMANDS
void
getFileNames( diskCache::Cache::QueryAnswer& Answer,
              const char* ifo_type_str,
              const INT_4U query_start,
              const INT_4U query_stop,
              const std::string& Extension )
{
   is_first_scan_complete( __FILE__, __LINE__ );

   Timer tracker;

   //--------------------------------------------------------------------
   // Set the search criteria
   //--------------------------------------------------------------------
   Cache::QueryParams   q;
   std::ostringstream   start_string;
   std::ostringstream   stop_string;

   start_string << query_start;
   stop_string << query_stop;

   q.AddParam( "index", Cache::SDGTx::AsciiId );

   q.AddParam( "site-description", ifo_type_str );
   q.AddParam( "start", start_string.str( ) );
   q.AddParam( "stop", stop_string.str( ) );
   q.AddParam( "extension", Extension );
   
   try
   {
      Cache::RegistrySingleton::TranslateQuery( q, Answer );
      MountPointManagerSingleton::Find( Answer );
   }
   catch( const Cache::QueryParams::MissingVariableError& Error )
   {
   }

   tracker.getDelta( "getFileNames()" );
}
#endif /* CORE_COMMANDS */

#endif /* 0 */

#if 0
//----------------------------------------------------------------------------
//   
//: Write content of global frame data hash to binary file.   
//
//!param: const char* filename - Name of the file to write frame hash to.
//+       Default is NULL (C++ will use default file name).      
//
//!return: Nothing.
//
#if CORE_COMMANDS
void
writeDirCache( const char* filename ) 
{
   MountPointScanner::scanner_sync_ro_type
      scanner_sync_lock( MountPointScanner::SyncRO( ) );

   // Debug timer
   Timer tracker;   
   
#if 0
   CacheFile    cf;

   cf.writeToFile( filename );
#endif /* 0 */

   tracker.getDelta( "writeDirCache()" );   
   
   return;
}
#endif /* CORE_COMMANDS */
   
   
#if defined( CREATE_THREADED1V )

CREATE_THREADED1V( writeDirCache, const char* )
   
#endif /* defined( CREATE_THREADED1V */
   
//----------------------------------------------------------------------------
//   
//: Write content of global frame data hash to ascii file.   
//
//!param: const char* filename - Name of the file to write frame hash to.
//+       Default is NULL (C++ will use default file name).      
//
//!return: Nothing.
//
#if CORE_COMMANDS
void
writeDirCacheAscii( const char* filename ) 
{
   /// \todo Use new Ascii writer to output data to ASCII stream
#if 0
#if 0
   MountPointScanner::SyncRequest sync_request;
#endif /* 0 */
   MountPointScanner::scanner_sync_ro_type
      scanner_sync_lock( MountPointScanner::SyncRO( ) );

   static const CHAR* method_name( "writeDirCacheAscii" );
   static const CHAR* const DefaultFileName( "frame_cache_dump2" );
   static const string TmpExtension( ".tmp" );
   static const string BackupExtension( ".bak" );   
   static MutexLock::lock_type key = MutexLock::Initialize( );

   // Debug timer
   Timer tracker;
   MutexLock    baton( key );
   std::string  fn( DefaultFileName );

   if ( filename && *filename )
   {
      fn = filename;
   }
   string fn_tmp( fn );
   fn_tmp += TmpExtension;
   
   CacheFile::stream_type cache_file;

   cache_file.open( fn_tmp.c_str(), ios_base::out | ios_base::trunc );

   try
   {
      std::string hash( getDirCache( ) );
      std::string::size_type end = 0;
      std::string::size_type start = 0;
      while ( end != std::string::npos )
      {
         end = hash.find( " /", start );
         const std::string::size_type len( ( end == std::string::npos )
                                           ? end
                                           : ( end - start ) );
         cache_file << hash.substr( start, len ) << std::endl;
         start = end + 1;
      }
      cache_file.close( );
   }
   catch( ... )
   {
      cache_file.clear( );              // Clear any errors
      cache_file.close( );              // Close the file
      unlink( fn_tmp.c_str( ) );        // Remove the file
      throw;                            // rethrow the exception
   }

   // Create backup copy of existing frame cache file
   cache_file.open( fn.c_str( ), ios_base::in );
   
   if( cache_file )
   {
      // There is existing cache file ---> need to save it
      cache_file.close();

   
      // Format name for backup file
      string backup_name( fn );
      backup_name += BackupExtension;

   
      // Check if there is a backup file from previous write:
      ifstream backup_file( backup_name.c_str() );
   
      if( backup_file )
      {
         backup_file.close();
   
         if( unlink( backup_name.c_str() ) )
         {
            // Could not remove old backup file
            const string errno_msg( strerror( errno ) );
      
            string msg( method_name );
            msg += "Could not remove old backup copy of cache file \'";
            msg += backup_name;
            msg += "\' (errno=";
            msg += errno_msg;
            msg += "). New cache file is stored as temporary \'";
            msg += fn_tmp;
            msg += '\'';
      
            throw runtime_error( msg.c_str() );   
         }
      }
   
   
      if( rename( fn.c_str(), backup_name.c_str() ) )
      {
         // Could not rename file
         const string errno_msg( strerror( errno ) );
   
         // Creating backup copy failed:
         string msg( method_name );
         msg += "Could not create backup copy of already existing cache file \'";
         msg += fn;
         msg += "\' (errno=";
         msg += errno_msg;
         msg += "). New cache file is stored as temporary \'";
         msg += fn_tmp;
         msg += '\'';
   
         throw runtime_error( msg.c_str() );
      }
   }

   
   // Move file to the requested destination
   if( rename( fn_tmp.c_str(), fn.c_str() ) )
   {
      // Could not rename file
      const string errno_msg( strerror( errno ) );
   
      // Creating backup copy failed:
      string msg( method_name );
      msg += "Could not move temporary copy of cache file \'";
      msg += fn_tmp;
      msg += "\' (errno=";
      msg += errno_msg;
      msg += ") to the destination \'";
      msg += fn;
      msg += '\'';
   
      throw runtime_error( msg.c_str() );   
   }

   tracker.getDelta( "writeDirCacheAscii()" );   
   
#endif /* 0 */
   return;
}
#endif /* CORE_COMMANDS */   
   
#if defined( CREATE_THREADED1V )

CREATE_THREADED1V( writeDirCacheAscii, const char* )
   
#endif /* define( CREATE_THREADED1V ) */
   
//-----------------------------------------------------------------------
///
/// \brief Write content of global frame data hash to ascii file.   
///
/// \param[in] bfilename
///     Name of the file to write the binary frame hash to.
///     Default is NULL (C++ will use default file name).      
///
/// \param[in] afilename
///     Name of the file to write the ascii frame hash to.
///     Default is NULL (C++ will use default file name).      
///
//-----------------------------------------------------------------------
#if CORE_COMMANDS
void
writeDirCacheFiles( const char* bfilename,
                    const char* afilename ) 
{
   // Debug timer
   Timer        tracker;
#if FOR_TCL
   tid*         binary_tid  = writeDirCache_t( bfilename,
                                               (Tcl_Interp*)NULL,
                                               "" );
   tid*         ascii_tid = writeDirCacheAscii_t( afilename,
                                                  (Tcl_Interp*)NULL,
                                                  "" );
   
   binary_tid->Join( ); // Syncronize point
   ascii_tid->Join( );  // Syncronize point
   
   writeDirCache_r( binary_tid );       // Cleanup
   writeDirCacheAscii_r( ascii_tid );   // Cleanup
#else
   writeDirCache( bfilename );
   writeDirCacheAscii( afilename );
#endif


   tracker.getDelta( "writeDirCacheFiles()" );   
   
   return;
}
#endif /* CORE_COMMANDS */
   
#if defined( CREATE_THREADED2V )

CREATE_THREADED2V( writeDirCacheFiles, const char*, const char* )
   
#endif /* defined( CREATE_THREADED2V ) */
   
//------------------------------------------------------------------------------
//   
//: Read content of global frame data hash from binary file.   
//
// Tcl layer can specify different files for read and write operations. 
// This function forces all read and write operations to be sequencial.
//
// ATTN: This function destroys existing hash before reading a new one from the
//       given file. Caller must assure there are no running threads that might
//       access global frame data hash at the time "readDirCache" is called.   
//    
//!param: const char* filename - Name of the file to read frame hash from. 
//+       Default is NULL (C++ will use default file name).   
//
//!return: Nothing.
//   
#if CORE_COMMANDS
void
readDirCache( const char* filename )
{
   // Debug timer
   Timer tracker;   

   //----------------------------------------------------------------------------
   // Enscure serializatioin of reads and writes
   //----------------------------------------------------------------------------
   DumpCacheDaemon::io_lock_type        lock( DumpCacheDaemon::IOLock( ) );

#if 0
#endif /* 0 */

   tracker.getDelta( "readDirCache()" );   
   
   return;
}
#endif /* CORE_COMMANDS */
   
#if ! FOR_PYTHON

CREATE_THREADED1V( readDirCache, const char* )
   
#endif /* ! FOR_PYTHON */
   
//----------------------------------------------------------------------------
//   
//: Update list of excluded directories (as they appear in
//: the resource file): check if existing data hash relies on such directories
//:                     already, remove those directories recursively from
//:                     global hash.   
//
//!usage: set dir_list [ excludedDirList dirs_to_exclude ]
//   
//!param: const CHAR* dir_list - A list of directories to exclude (as they 
//+                              appear in API resource file variable).
//   
//!return: string - Sorted Tcl list of all removed subdirectories, followed
//+        by error messages if any:
//+        {Directories excluded by pattern 'dir_list': dir1 dir2 ...} 
//+        {error1 error2 ...}
//         
#if CORE_COMMANDS
string
excludedDirList( const char* dir_list )
{
   // Debug timer
   Timer tracker;   
   
   /// \todo Seed with list of directories to exclude
#if WORKING
   const string result( MountPointHash::updateExcludedDirList( dir_list ) );
#else
   const string result;
#endif /* WORKING */

   tracker.getDelta( "excludedDirList()" );   
   
   
   return result;
}
#endif /* CORE_COMMANDS */


//----------------------------------------------------------------------------
//
//: Update list of file name extensions 
//
//!usage: updateFileExtList [ list ]
//  
//!param: const CHAR* ext_list - A list of file extensions
//  
//!return: nothing 
//
#if CORE_COMMANDS
void
updateFileExtList( const diskCache::Cache::SDGTx::file_extension_container_type&
                   Extensions )
{
   // Debug timer
   Timer tracker;

   diskCache::Cache::SDGTx::FileExtList( Extensions );

   tracker.getDelta( "updateFileExtList()" );

   return;
}
#endif /* CORE_COMMANDS */

//   
//: Delete global frame data hash.
//
// ATTN: This function destroys existing hash in memory!
//       Caller must assure there are no running threads that might
//       access global frame data hash at the time "deleteDirCache" is called.
//    
//!return: Nothing.
//
#if CORE_COMMANDS
void deleteDirCache()
{
   // Debug timer
   Timer tracker;   
   
   MountPointManagerSingleton::Reset( MountPointManagerSingleton::RESET_CACHE);

   tracker.getDelta( "deleteDirCache()" );   
   
   
   return;   
}
#endif /* CORE_COMMANDS */
   
#if defined( CREATE_THREADED0V )
CREATE_THREADED0V( deleteDirCache )
#endif /* defined( CREATE_THREADED0V ) */

#if FOR_TCL
//-----------------------------------------------------------------------
/// \brief Bind a Tcl Varialbe to the C++ variable for mount pt conflict checking
///
/// \param Interp
///     The TCL interpreter containing the Variable.
/// \param Variable
///     name of variable to be used within TCL to control if
///	new time hashes need to be verified agaist all existing
///	time hashes.
///   
/// \return 1 if binding took place, 0 otherwise.
///
/// \todo Remove this function as the back end has been removed.
bool
bindToCheckForMountPTConflictVariable( Tcl_Interp* Interp, char* Variable )
{
   bool retval = false;
   return retval;
}
#endif /* FOR_TCL */

#if CORE_COMMANDS
//-----------------------------------------------------------------------
/// This routine does a single pass of the listed mount points.
//-----------------------------------------------------------------------
void
ScanConcurrency( INT_4U Concurrency )
{
   MountPointScanner::Concurrency( Concurrency );
}
#endif /* CORE_COMMANDS */

//-----------------------------------------------------------------------
/// This routine does a single pass of the listed mount points.
//-----------------------------------------------------------------------
#if CORE_COMMANDS
void
ScanMountPointList( MountPointScanner::ScanResults& Answer )
{
   static const char caller[] = "ScanMountPointList";
   //--------------------------------------------------------------------
   /// Obtain the list of mount points.
   //--------------------------------------------------------------------
   General::GPSTime     start;
   start.Now( );

   //--------------------------------------------------------------------
   // Loop over the mount points
   //--------------------------------------------------------------------
   MountPointScanner::mount_point_container_type
      mount_points( diskCache::MountPointManagerSingleton::MountPoints( ).Var( ) );

   MountPointScanner::Scan( mount_points, Answer );

   General::GPSTime     end;
   end.Now( );
   std::ostringstream   msg;
   General::GPSTime     dt( General::GPSTime( ) + ( end - start ) );
   INT_4U ms = dt.GetNanoseconds( ) / 1000000;
   ms += dt.GetSeconds( ) * 1000;
   //--------------------------------------------------------------------
   /// The output needs to be in the following format
   /// 7 mount points, 0 directories, 0 files, scanned in 3010 ms  (00:00:03.0010).
   //--------------------------------------------------------------------

   msg
#if DEPRICATED_INTERFACE
      << diskCache::MountPointHash::ScannedMountPoints( ) << " mount points, "
      << diskCache::DirectoryHash::ScannedDirectories( ) << " directories, "
      << diskCache::ScannedFiles( ) << " files,"
#endif /* DEPRICATED_INTERFACE */
      << " scanned in " << ms << " ms "
      << "(" << ms_format( dt ) << ")."
      ;
   if ( General::AtExit::IsExiting( ) == false )
   {
      if ( Stream )
      {
         (*Stream) << msg.str( ) << std::endl;
      }
      else
      {
         GenericAPI::queueLogEntry( msg.str( ),
                                    GenericAPI::LogEntryGroup_type::MT_NOTE,
                                    0,
                                    caller,
                                    "SCAN_MOUNTPT" );
      }
   }

}
#endif /* CORE_COMMANDS */

#if CORE_COMMANDS
//-----------------------------------------------------------------------
/// Create an image of the cache file on the storage media.
//-----------------------------------------------------------------------
void
DumpCacheDaemonStart( std::ostream* Stream )
{
   using namespace diskCache;

   //--------------------------------------------------------------------
   /// \todo
   /// Protect from multiple scanner daemons from running.
   //--------------------------------------------------------------------
   //--------------------------------------------------------------------
   // Get a thread in which to run this task in the background
   //--------------------------------------------------------------------
   General::TaskThread*         thread = General::ThreadPool::Acquire( );
   DumpCacheDaemon*             task = new DumpCacheDaemon( Stream );
   //--------------------------------------------------------------------
   /// \todo Need to setup the task thread to be returned to the pool
   ///     once the task is complete.
   //--------------------------------------------------------------------
   //--------------------------------------------------------------------
   // Add the task to the thread and get things running
   //--------------------------------------------------------------------
   thread->AddTask( task );
}
#endif /* CORE_COMMANDS */

#if CORE_COMMANDS
//-----------------------------------------------------------------------
/// Scan the list of mount points in the background
//-----------------------------------------------------------------------
void
ScanMountPointListContinuously( std::ostream* Stream )
{
   //--------------------------------------------------------------------
   /// \todo
   /// Protect from multiple scanner daemons from running.
   //--------------------------------------------------------------------
   //--------------------------------------------------------------------
   // Get a thread in which to run this task in the background
   //--------------------------------------------------------------------
   General::TaskThread*         thread = General::ThreadPool::Acquire( );
   ScanMountPointsDaemon*       task = new ScanMountPointsDaemon( Stream );
   //--------------------------------------------------------------------
   /// \todo Need to setup the task thread to be returned to the pool
   ///     once the task is complete.
   //--------------------------------------------------------------------
   //--------------------------------------------------------------------
   // Add the task to the thread and get things running
   //--------------------------------------------------------------------
   thread->AddTask( task );
}
#endif /* CORE_COMMANDS */

// END 
#endif /* 0 */
