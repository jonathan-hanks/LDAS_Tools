#include "IO.hh"

#include "diskcacheAPI/Streams/Binary.hh"

#include "MountPointManagerSingleton.hh"
#include "DirectoryManagerSingleton.hh"

namespace diskCache
{
  template<>
  Streams::IBinary&
  Read( Streams::IBinary& Stream )
  {
    static const char method_name[] = "diskCache::Read";

    {
      std::ostringstream    msg;
	
      msg << "Entry"
	;
      GenericAPI::queueLogEntry( msg.str( ),
				 GenericAPI::LogHTML::MT_DEBUG,
				 30,
				 method_name,
				 "CXX" );
    }
    diskCache::MountPointManagerSingleton::Read( Stream );
    diskCache::DirectoryManagerSingleton::Read( Stream );
    {
      std::ostringstream    msg;
	
      msg << "Exit"
	;
      GenericAPI::queueLogEntry( msg.str( ),
				 GenericAPI::LogHTML::MT_DEBUG,
				 30,
				 method_name,
				 "CXX" );
    }
    return Stream;
  }
  
  template<>
  Streams::OBinary&
  Write( Streams::OBinary& Stream )
  {
    static const char method_name[] = "diskCache::Write";

    {
      std::ostringstream    msg;
	
      msg << "Entry"
	;
      GenericAPI::queueLogEntry( msg.str( ),
				 GenericAPI::LogHTML::MT_DEBUG,
				 30,
				 method_name,
				 "CXX" );
    }
    diskCache::MountPointManagerSingleton::Write( Stream );
    diskCache::DirectoryManagerSingleton::Write( Stream );
    {
      std::ostringstream    msg;
	
      msg << "Exit"
	;
      GenericAPI::queueLogEntry( msg.str( ),
				 GenericAPI::LogHTML::MT_DEBUG,
				 30,
				 method_name,
				 "CXX" );
    }
    return Stream;
  }
  
} // namespace - diskCache
