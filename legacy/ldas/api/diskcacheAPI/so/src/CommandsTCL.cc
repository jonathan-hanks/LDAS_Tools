#include "genericAPI/thread.hh"

#include "diskcacheAPI/Cache/QueryAnswer.hh"
#include "diskcacheAPI/Cache/SDGTx.hh"

#include "genericAPI/TCL.hh"

#include "Commands.hh"
#include "CommandsTCL.hh"

namespace diskCache
{
  namespace TCL
  {
    namespace Commands
    {
      //-----------------------------------------------------------------
      //-----------------------------------------------------------------
      std::string
      getDirCache( const char* ifo, const char* type )
      {
	std::string			retval;
	diskCache::Cache::QueryAnswer	answer;

	diskCache::Commands::getDirCache( answer, ifo, type );
	GenericAPI::TCL::Translate( retval, answer );

	return retval;
      }

      //-----------------------------------------------------------------
      //-----------------------------------------------------------------
      CREATE_THREADED2( getDirCache, std::string, const char*, const char* )

      //-----------------------------------------------------------------
      /// \brief Obtain the list of file name extensions
      ///
      /// \return list of file extensions
      ///
      //-----------------------------------------------------------------
      std::string
      getFileExtList( )
      {
	const diskCache::Cache::SDGTx::file_extension_container_type&
	  extensions( diskCache::Commands::FileExtList( ) );

	std::ostringstream results;
	
	GenericAPI::TCL::ListUnbounded<std::ostream>( results,
						      extensions.begin( ),
						      extensions.end( ) );
	return results.str( );
      }

      //-----------------------------------------------------------------
      /// \brief Get filename based on search criteria
      ///
      /// Get TCL formatted lists of file names for data matching the specified
      /// IFO and Type within the bounds of the query.
      ///
      /// \param[in] ifo_type_str
      ///     A space delimited list of IFO-Type strings
      /// \param[in] start_time
      ///     Query start time.
      /// \param[in] stop_time
      ///     Query stop time.
      /// \param[in] Extension
      ///     Query file extension
      ///
      /// \return
      ///     A list for each mount point with data file names:
      ///     { mntpt1 { /f1 /f2 ... /fN } mntpt2 { /f1 /f2 ... /fN } ... mntptN { } }  
      ///
      /// \example set string [ getFileNames ifo_type_str query_start query_stop ]
      //---------------------------------------------------------------------
      std::string
      getFileNames( const char* IfoType,
		    const unsigned int Start,
		    const unsigned int Stop,
		    const char* Extension )
      {
	std::string			retval;
	diskCache::Cache::QueryAnswer	answer;

	diskCache::Commands::getFileNames( answer, IfoType,
					   Start,
					   Stop,
					   Extension );
	GenericAPI::TCL::Translate( retval, answer );

	return retval;
      }

      CREATE_THREADED4( getFileNames, std::string, const char*,
			const unsigned int, const unsigned int, const char* )
      //-----------------------------------------------------------------
      /// This command finds frame files that satisfy createRDS user request. 
      /// If "will_resample" flag is set to TRUE, requested time range will be
      /// automatically expanded by frame dt at the beginning and at the end.         
      //-----------------------------------------------------------------
      std::string
      getFrameFiles( const char* Ifo, 
		     const char* Type,
		     const unsigned int Start,
		     const unsigned int Stop,
		     const char* Extension,
		     const bool AllowGaps )
      {
	std::string			retval;
	diskCache::Cache::QueryAnswer	answer;
	
	diskCache::Commands::getFrameFiles( answer,
					    Ifo, Type,
					    Start, Stop,
					    Extension,
					    AllowGaps );

	GenericAPI::TCL::Translate( retval, answer );

	return retval;
      }

      CREATE_THREADED6( getFrameFiles, std::string, const char*, const char*,
			const unsigned int, const unsigned int, const char*, const bool )

      //-----------------------------------------------------------------
      //
      //: Get TCL formatted lists of intervals for data matching the specified
      //: IFO and Type within the bounds of the query.
      //
      //!usage: set string [ getIntervalsList ifo_type_str query_start query_stop ]
      //
      //!param: const char* ifo_type_str - A space delimited list of IFO-Type strings.
      //!param: const unsigned int start_time - query start time.
      //!param: const unsigned int stop_time - query stop time.
      //
      //!return: string - A list for each IFO-Type pair with data intervals:
      //+  IFO-Type1 { i1_start i1_stop ... iN_start iN_stop } ... IFO-TypeN { ... }
      //
      //-----------------------------------------------------------------
      std::string
      getIntervalsList( const char* IfoType,
			const unsigned int Start,
			const unsigned int Stop,
			const char* Extension )
      {
	diskCache::Cache::QueryAnswer	answer;
	std::string			retval;
	
	diskCache::Commands::getIntervalsList( answer,
					       IfoType,
					       Start,
					       Stop,
					       Extension );
	GenericAPI::TCL::Translate( retval, answer );

	return retval;
      }

      CREATE_THREADED4( getIntervalsList, std::string,
			const char*,
			const unsigned int,
			const unsigned int,
			const char* )

      //-----------------------------------------------------------------
      /// This command finds frame files that satisfy createRDS user request. 
      /// If "will_resample" flag is set to TRUE, requested time range will be
      /// automatically expanded by frame dt at the beginning and at the end.         
      //-----------------------------------------------------------------
      std::string
      getRDSFrameFiles( const char* Ifo, 
			const char* Type,
			const unsigned int Start,
			const unsigned int Stop,
			const char* Extension,
			const bool WillResample )
      {
	std::string			retval;
	diskCache::Cache::QueryAnswer	answer;
	
	diskCache::Commands::getRDSFrameFiles( answer,
					       Ifo, Type,
					       Start, Stop,
					       Extension,
					       WillResample );

	GenericAPI::TCL::Translate( retval, answer );

	return retval;
      }

      CREATE_THREADED6( getRDSFrameFiles, std::string, const char*, const char*,
			const unsigned int, const unsigned int, const char*, const bool )

      //-----------------------------------------------------------------
      //-----------------------------------------------------------------
      void
      updateFileExtList( const char* Extensions )
      {
	diskCache::Cache::SDGTx::file_extension_container_type     native;
     
	GenericAPI::TCL::Parse( Extensions, native );
	diskCache::Commands::updateFileExtList( native );

	return;
      }

      //-----------------------------------------------------------------
      //-----------------------------------------------------------------
      std::string
      updateMountPtList( const char* dir_list,
			 const bool enable_global_check )
      {
	using namespace diskCache;

	MountPointManagerSingleton::UpdateResults    status;
	MountPointManagerSingleton::mount_point_name_container_type
	  paths;
	GenericAPI::TCL::Parse( dir_list, paths );

	diskCache::Commands::updateMountPtList( status, paths, enable_global_check );
	
	/// \todo Convert update_status to TCL string representation
	std::string	retval;

	GenericAPI::TCL::Translate( retval, status );

	return retval;
      }

    } // namespace - Commands
  } // namespace - TCL
} // namespace - diskCache
