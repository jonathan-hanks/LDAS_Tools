//=======================================================================
/*! \mainpage

\htmlonly
<H1>Extended TCL Commands</H1>

<a href="tcl/tclindex.html">Index of extended TCL commands</a>
\endhtmlonly

<H1>Introduction</H1>
The diskcache API is responsible for maintaining an index of frame
files.

The various cache formats are described \ref CacheFile "here".
*/
//=======================================================================
/*! \namespace diskCache

\brief Disk cache.

This namespace isolates routines associated with maintaining the in
memory and file versions of the frame cache.
*/
