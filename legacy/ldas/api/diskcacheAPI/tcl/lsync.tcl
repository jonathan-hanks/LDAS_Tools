#!/ldcg/bin/tclsh
## ******************************************************** 
## Name: lsync.tcl Version 1.0
##
## Description:
## This is a 'standalone' version of the LDAS diskcache
## API. It maintains a cache file like the normal LDAS
## diskcache API, but does NOT offer any mechanism for
## interaction with the running script.
##
##
## ******************************************************** 

;#barecode
set auto_path "/ldcg/lib/64 /ldcg/lib64 /ldcg/lib /ldas/lib $auto_path"
foreach lib [list log_mt diskscan] {
    if { [ file exists ./lib${lib}.so ] } {
	load ./lib${lib}.so
    } elseif { [ file exists /ldas/lib/diskcacheAPI/lib${lib}.so ] } {
	load /ldas/lib/diskcacheAPI/lib${lib}.so
    }
}

namespace eval lsync {}

if { ! [ file exists lsync.rsc ] } {
   set msg    "This program requires that a resource file be\n"
   append msg "found in the current working directory, which\n"
   append msg "appears to be [ pwd ].\n"
   append msg "Please look in the distribution archive where\n"
   append msg "you got this program for the file named\n"
   append msg "'lsync.rsc', and edit that file to reflect your\n"
   append msg "installation and runtime requirements."
   puts stderr $msg
   exit
}
;#end

proc lsync::wait4Threads {} {
puts stderr "[ clock seconds ] in wait4Threads"
	if	{ ! [ info exist ::CAUGHT_SIGNAL ] } {
		return
	}
	puts stderr "doing getThreadList"
	#set threads [ getThreadList ]
	set threads ""
	puts stderr "threads $threads regexp [regexp -noncase -- {RUNNING} $threads ] "
	if	{ [ regexp -noncase -- {RUNNING} $threads ] } {
		puts stderr "waiting for $threads to finish"
	} else {
		puts stderr "no threads, ok to exit"
		lsync::hashFile write
		catch { exec kill -9 [ pid ] }
		exit
	}
}

package require Signal

;## unable to do getThreadList within signal or in background 
;## write out the cache before exiting
proc handleInterrupt {} {
puts stderr "[ clock seconds ] in handleInterrupt"
	;## cancel bgLoop 
	set ::bg::jobs(updateexcl,run)   0
	set ::bg::jobs(cacheupdate,run)   0
	lsync::hashFile write
	puts stderr "[ clock seconds ] caught interrupt exiting"
	exit
}

proc handleAbort {} {
puts stderr "[ clock seconds ] in handleInterrupt"
	;## cancel bgLoop 
	set ::bg::jobs(updateexcl,run)   0
	set ::bg::jobs(cacheupdate,run)   0
	lsync::hashFile write
	puts stderr "[ clock seconds ] caught abort signal, exiting"
	exit
}


signal add SIGINT handleInterrupt
signal add SIGHUP handleInterrupt
signal add SIGTERM handleInterrupt
signal add SIGABRT handleAbort
puts "Has handlers for signals\n[ signal print ]"

## ******************************************************** 
##
## Name: lsync::init
##
## Description:
## The variable ::lsync::cache contains a metadata
## representation of all directories found under the
## directories listed by ::MOUNT_PT which contain
## frame files whose names are consistent with the
## proposed frame naming convention.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::init { args } {
     
     if { [ catch {
        set ::number_of_running_threads 0
        set ::last_reorder_dir        [ list ]
        set ::lsync::nextupdateindex 0
        set ::get_dir_cache_last_called 0
        set ::lsync::queuestart [ clock clicks -milliseconds ]

        if { ! [ info exist ::MOUNT_PT_LOOP_INTERVAL_MS ] || \
             $::MOUNT_PT_LOOP_INTERVAL_MS < 500 } {
         set ::MOUNT_PT_LOOP_INTERVAL_MS 500
        } 

        if { ! [ info exists ::DEBUG_SCAN_RATE ] } {
           set ::DEBUG_SCAN_RATE 0
        }
        
        ;## how long to wait, in seconds, between repeated
        ;## error reports on a given directory
        if { ! [ info exists ::ERROR_REPORT_INTERVAL ] } {
           set ::ERROR_REPORT_INTERVAL 3600
        }
        
        if { ! [ info exists ::DEBUG_CACHE_SYNCHRONIZE ] } {
           set ::DEBUG_CACHE_SYNCHRONIZE 0
        }
        
        if { ! [ info exists ::EXCLUDE_THESE_DIRS_FROM_UPDATES ] } {
           set ::EXCLUDE_THESE_DIRS_FROM_UPDATES [ list ]
        }
        
        if { ! [ info exists ::DISKCACHE_HASHFILE_NAME_BINARY ] || \
             ! [ string length [ string trim $::DISKCACHE_HASHFILE_NAME_BINARY ] ] } {
           set ::DISKCACHE_HASHFILE_NAME_BINARY lsync.cache
        }
        
        if { ! [ info exists ::DISKCACHE_HASHFILE_NAME_ASCII ] || \
             ! [ string length [ string trim $::DISKCACHE_HASHFILE_NAME_ASCII ] ] } {
           set ::DISKCACHE_HASHFILE_NAME_ASCII lsync.cache.txt
        }
        
        ;## warn about directories that take a really long time
        ;## to update.
        if { ! [ info exists ::DIR_SLOW_UPDATE_WARNING_THRESHHOLD ] } {
           set ::DIR_SLOW_UPDATE_WARNING_THRESHHOLD 60
        }
        
        ;## how long to wait for running threads to complete before
        ;## giving up on an attempt to rebuild the cache from scratch.
        if { ! [ info exists ::WAIT_N_SECONDS_FOR_THREADS_TO_COMPLETE ] } {
           set ::WAIT_N_SECONDS_FOR_THREADS_TO_COMPLETE 10
        }
       
        ;## how many directories should be updated simultaneously?
        ;## there may be a practical limit of about one thread per
        ;## available cpu when directories are actually changing.
        if { ! [ info exists ::NUMBER_OF_RUNNING_THREADS_PERMITTED ] } {
           set ::NUMBER_OF_RUNNING_THREADS_PERMITTED 4
        }
        
        ;## Should the mount point conflicts be checked for?
		if { ! [ info exists ::CHECK_FOR_MOUNT_PT_CONFLICT ] } {
	   	set ::CHECK_FOR_MOUNT_PT_CONFLICT 1
		}
		;## Make sure the TCL variable and the C++ variable stay synced
		bindToCheckForMountPTConflictVariable ::CHECK_FOR_MOUNT_PT_CONFLICT

        ;## the diskcache API was extended to allow it to operate on
        ;## filenames that do NOT end in .gwf
        updateFileExtList \
           [ lsync::extensionListMangler $::SCANNED_FILENAME_EXTENSIONS ]
        
        lsync::hashFile read
        lsync::afterBootlock
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::extensionListMangler
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::extensionListMangler { raw } {
     
     if { [ catch {
        set bads [ list ]
        set mangled [ list ]
        set raw [ lsort -dictionary -unique $raw ]
        set i [ lsearch $raw .gwf ]
        if { $i != -1 } {
           set raw [ lreplace $raw $i $i ]
        }
        foreach ext $raw {
           if { ! [ regexp {^\.} $ext ] } {
              lappend bads $ext
           } elseif { [ regsub -all {\.} $ext {} junk ] > 1 } {
              lappend bads $ext
           }
        }
        if { [ llength $bads ] } {
           set err    "Bad filename extensions detected in "
           append err "::SCANNED_FILENAME_EXTENSIONS list.\n"
           append err "extensions must begin with a '.' and\n"
           append err "have no additional '.'.\n"
           append err "Bad items in list:\n $bads"
           return -code error $err
        }
        set mangled [ concat .gwf $raw ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $mangled
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::updateMountPoint
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::updateMountPoint { args } {
     
     if { [ catch {      
        set seqpt [ list ]
        set rscfile lsync.rsc
        
        set mtpt $::MOUNT_PT
        if { [ file exists .mount.point ] } {
           set mtpt [ dumpFile .mount.point ]
        }
        set data [ dumpFile $rscfile ]
        
        set target [ lsync::multiLine $data ]

        if { [ string length $target ] } {
           eval $target
        }
        
        set ::MOUNT_PT [ lsync::uniqueNotSorted $::MOUNT_PT ]

        ;## heal bad lisitification
        set mtpt [ concat $mtpt ]
        set ::MOUNT_PT [ concat $::MOUNT_PT ]
        set seqpt "updateMountPtList($::MOUNT_PT):"
        set data [ updateMountPtList $::MOUNT_PT ]

        lsync::mountPointErrors $data
        set seqpt [ list ]
        
        if { ! [ string equal $::MOUNT_PT $mtpt ] } {
           set ::scan_state_vector [ list ]
           set subject "::MOUNT_PT updated"
           set body "::MOUNT_PT updated"
           append body " from ***-> '$mtpt'\n"
           append body "to ***-> '$::MOUNT_PT'"
           set msg "Subject: ${subject}; Body: $body"
           addLogEntry $msg email
           set fid [ open .mount.point.tmp w 0600 ]
           puts $fid $::MOUNT_PT
           file rename -force .mount.point.tmp .mount.point
           ::close $fid
        }

     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::multiLine
##
## Description:
## Handles multi-line ::MOUNT_PT entries formatted
## consistently with Tcl's line continuation syntax.
##
## Parameters:
##
## Usage:
## With the addition of this function, ::MOUNT_PT entries
## in the lsync.rsc file can be formatted like this:
##
##     set ::MOUNT_PT [ list \
##                      /some/dir1 \
##                      /some/dir2 /some/dir3 \
##                      /another/dir \
##                      ...
##                      /the/last/dir \
##                    ]
##
## Where there is at least one space between the last
## character in the directory name and the line
## continuation character '\', and NO spaces after the
## line continuation character.
##
## Comments:
##

proc lsync::multiLine { data } {
     
     if { [ catch { 
        
        foreach line [ split $data "\n" ] {
           set line [ string trim $line " \\" ]
           if { [ info exists target ] } {
              append target " $line"
              if { [ regexp {\]} $line ] } {
                 break
              }   
           } elseif { [ regexp {^\s*set\s+::MOUNT_PT} $line ] } {
              set target $line
              if { [ regexp {\]} $line ] } {
                 break
              }
           }
        }
        
        if { ! [ info exists target ] } {
           set target [ list ]
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $target
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::uniqueNotSorted
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::uniqueNotSorted { data } {
     
     if { [ catch {
        
        ;## uniquify without sorting
        set temp [ list ]
        foreach element $data {
           if { [ lsearch -exact $temp $element ] == -1 } {
              lappend temp $element
           }
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $temp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::mountPointErrors
##
## Description:
## If errors are detected under a mount point, email the
## supervisor, and rerun the updateMountPtList command.
##
## Make sure the error is not reported TOO often, but this
## one is pretty serious.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::mountPointErrors { data } {
     
     if { [ catch {
        set report 0
        set error_dirs [ list ]
        set now [ clock seconds ]
        
        foreach [ list dirs errors ] $data { break }
        
        if { [ string length [ lindex $errors 0 ] ] } {
           set report 1
        }

        if { $report } {
           set report 0
           foreach [ list dir err ] $errors {
              
              lappend error_dirs $dir
              
              ;## avoid reorting continuously on existing, known errors!
              if { [ info exists ::mtpt_errors($dir) ] } { 
                 foreach [ list time error ] \
                    $::mtpt_errors($dir) { break } 
                 
                 ;## if the error has changed, we do report it.
                 if { $now - $time <= $::ERROR_REPORT_INTERVAL && \
                    [ string equal $err $error ] } {
                    continue
                 }
              }
              
              set subdir_rx {subdirectory of existing}
              if { [ regexp -nocase -- $subdir_rx $err ] } {
                 set ::mtpt_errors($dir,SUB) 1
              }
              
              ;## update the timestamp and error message if
              ;## appropriate.
              set ::mtpt_errors($dir) [ list $now $err ]
              set report 1
           }

           foreach name [ array names ::mtpt_errors ] {
              if { [ regexp {SUB} $name ] } { continue }
              if { [ lsearch $error_dirs $name ] == -1 } {
                 ::unset ::mtpt_errors($name)
                 if { [ info exists ::mtpt_errors($name,SUB) ] } {
                    ::unset ::mtpt_errors($name,SUB)
                 }
              }
           }
           
           if { $report } {
              set subject "lsync errors!"
              set body "$subject\n\n"
              append body "these errors occurred while scanning "
              append body "the ::MOUNT_PT list:\n\n"
              append body $errors
              addLogEntry "Subject: $subject; Body: $body" email
           }
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::updateExcludedDirs
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::updateExcludedDirs { args } {
     
     if { [ catch {      
        set seqpt [ list ]
        set rscfile lsync.rsc
        
        set exdirs $::EXCLUDE_THESE_DIRS_FROM_UPDATES
        if { [ file exists excluded.dirs ] } {
           set exdirs [ dumpFile excluded.dirs ]
        }

        set interp [ interp create ]
        $interp eval source /ldas_outgoing/cntlmonAPI/cntlmon.state
        $interp eval source /ldas_outgoing/LDASapi.rsc
        $interp eval source $rscfile 
        
        set data [ $interp eval set ::EXCLUDE_THESE_DIRS_FROM_UPDATES ]
        
        eval "set ::EXCLUDE_THESE_DIRS_FROM_UPDATES [ list $data ]"
        interp delete $interp  
        
        ;## heal bad lisitification
        set exdirs [ concat $exdirs ]
        set ::EXCLUDE_THESE_DIRS_FROM_UPDATES [ concat $::EXCLUDE_THESE_DIRS_FROM_UPDATES ]
        set seqpt "excludedDirList($::EXCLUDE_THESE_DIRS_FROM_UPDATES):"
        set ::real_excluded_directories_list \
           [ excludedDirList $::EXCLUDE_THESE_DIRS_FROM_UPDATES ]
        set seqpt [ list ]
        lsync::syncExcluded $::real_excluded_directories_list
        lsync::synchronize
        
        if { ! [ string equal $::EXCLUDE_THESE_DIRS_FROM_UPDATES $exdirs ] } {
           set subject "::EXCLUDE_THESE_DIRS_FROM_UPDATES updated"
           set body "::EXCLUDE_THESE_DIRS_FROM_UPDATES updated"
           append body " from ***-> '$exdirs'\n"
           append body "to ***-> '$::EXCLUDE_THESE_DIRS_FROM_UPDATES'\n"
           append body "directories excluded:\n"
           append body "'$::real_excluded_directories_list'"
           set msg "Subject: ${subject}; Body: $body"
           addLogEntry $msg email
           set fid [ open excluded.dirs.tmp w 0600 ]
           puts $fid $::EXCLUDE_THESE_DIRS_FROM_UPDATES
           file rename -force excluded.dirs.tmp excluded.dirs
           ::close $fid
        }

     } err ] } {
        addLogEntry "$seqpt $err ($::errorInfo)" red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::syncExcluded
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::syncExcluded { data } {
     
     if { [ catch {
        set elements [ list ]
        set dirs [ lindex $data 0 ]
        set errs [ lindex $data 1 ]
        foreach dir $dirs {
           set dir [ string trim $dir ]
           if { ! [ string length $dir ] } { continue }
           foreach name [ array names ::lsync::cache $dir,* ] {
              unset ::lsync::cache($name)
              lappend elements $name
           }
        }
        if { [ llength $elements ] } {
           addLogEntry "unregistered '$elements'" blue
        }
        if { [ string length [ lindex $errs 0 ] ] } {
           set subject "::EXCLUDE_THESE_DIRS_FROM_UPDATES"
           append subject " ERRORS!"
           set body "$subject\n\n"
           append body $errs
           set msg "Subject: ${subject}; Body: $body"
           addLogEntry $msg email
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::updateDirInfo
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::updateDirInfo { dir now data } {
     
     if { [ catch {
        set new    0
        set report 0
        
        if { ! [ info exists ::lsync::cache(dirs) ] } {
           set ::lsync::cache(dirs) $dir
           set new 1
        } elseif { [ lsearch -exact $::lsync::cache(dirs) $dir ] == -1 } {
          lappend ::lsync::cache(dirs) $dir
          set new 1
        }
        
        foreach [ list dirs errors ] $data { break }

        if { [ string length [ lindex $errors 0 ] ] } {
           
           if { [ info exists ::known_errors($dir) ] } {
              foreach [ list time junk ] \
                 [ set ::known_errors($dir) ] { break }
              if { [ string equal $junk $errors ] } {
                 if { $now - $time > $::ERROR_REPORT_INTERVAL } {
                    set report 1
                 }
              } else {
                 set report 1
              }
           } else {
              set report 1
           }
        }

        if { $report } {
           
           set ::known_errors($dir) [ list $now $errors ]
           
           lsync::parseErrorStrings $dir $errors
           
           set subject "lsync errors!"
           set body "$subject\n\n"
           append body "while scanning ::MOUNT_PT entry ${dir}:\n\n"
           append body $errors
           addLogEntry "Subject: $subject; Body: $body" email
        }
        
        set ::lsync::cache($dir) $dirs
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $new
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::synchronize
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::synchronize { } {
     
     set now [ clock seconds ]
     if { ! [ info exists ::last_cache_synch ] } {
        set ::last_cache_synch 0
     }
     if { $now - $::last_cache_synch < 3 } {
        return {}
     }
     set ::last_cache_synch $now
     
     if { [ catch {
        
        set data [ getDirCache ]
        foreach [ list element mtime N times ] $data {
           foreach [ list dir ifo type n dt ] \
              [ split $element , ] { break }
           
           set name $dir,$ifo,$type,$dt
           set value [ list $mtime $N $times ]
           
           if { [ info exists ::lsync::cache($name) ] } {
              set old [ set ::lsync::cache($name) ]
              if { ! [ string equal $old $value ] } {
                 set msg "$dir UPDATED: "
                 append msg "${ifo}-${type}-NNNNNNNNNN-${dt}.gwf "
                 append msg "($times)"
                 addLogEntry $msg blue
              }
           } else {
              set msg "$dir ADDED: "
              append msg "${ifo}-${type}-NNNNNNNNNN-${dt}.gwf "
              append msg "($times)"
              addLogEntry $msg blue
           }
           
           set ::lsync::cache($dir,$ifo,$type,$dt) $value
           if { ! [ string equal 0 $::DEBUG_CACHE_SYNCHRONIZE ] } {
              addLogEntry "$dir $ifo $type $N" blue
           }
        }
        
        set mp_rx [ join $::MOUNT_PT | ]
        set mp_rx ^(dirs|$mp_rx)
        
        ;## unset removed array name entries!!
        ;## PR #'s 2352, 2353
        foreach name [ array names ::lsync::cache * ] {
           if { ! [ regexp $mp_rx $name ] } {
              unset ::lsync::cache($name)
           }
        }
        
        lsync::hashFile write
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::parseErrorStrings
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::parseErrorStrings { mtpt errors } {
     
     if { [ catch {
         
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::reportScanStats
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::reportScanStats { dir } {
     
     if { [ catch {
        if { ! [ info exists ::scan_state_vector ] } {
           set ::scan_state_vector [ list ]
        }
        if { [ lsearch $::scan_state_vector $dir ] == -1 } {
           lappend ::scan_state_vector $dir
        }
        if { [ llength $::scan_state_vector ] >= \
             [ llength $::MOUNT_PT ] } {
           unset ::scan_state_vector
           set end [ clock clicks -milliseconds ]
           if { [ info exists ::lsync::queuestart ] } {
              set length  0
              set dlength 0
              set flength 0
              set mtptcounts [ getHashNumbers ]
              set length [ llength $mtptcounts ]
              set dt [ expr { $end - $::lsync::queuestart } ]
              if { [ string equal 1 $::DEBUG_SCAN_RATE ] } {
                 set msg "$length mount points, "
                 foreach dir $mtptcounts {
                    incr dlength [ lindex $dir 1 ]
                    incr flength [ lindex $dir 2 ]
                 }
                 append msg "$dlength directories, "
                 append msg "$flength files, "
                 append msg "scanned in $dt ms."
                 addLogEntry $msg blue
              }
           }   
           set ::lsync::queuestart $end
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: lsync::hashFile
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::hashFile { { action NULL } } {
     
     if { [ catch {
        set seqpt [ list ]
        
        if { ! [ info exists ::cache_file_last_updated ] } {
           set ::cache_file_last_updated 0
        }
        
        set hashfile $::DISKCACHE_HASHFILE_NAME_BINARY
        set ahashfile $::DISKCACHE_HASHFILE_NAME_ASCII
        
        switch -exact $action {
              read {
                    set seqpt "readDirCache($hashfile): "
		    readDirCache $hashfile
                    set ::waiting_for_cache_read 0
                   }
             write {
                    set seqpt "writeDirCache($hashfile): "
                    writeDirCacheFiles $hashfile $ahashfile
                    lsync::diagnostic
                    set seqpt [ list ]
                   }
            delete {
                    if { [ file exists $hashfile ] } {
                       bak $hashfile
                       file delete -force $hashfile
                    }
                   }
           default {
                    set err "unknown action: '$action'"
                    return -code error $err
                   }   
        }
     } err ] } {
        if { [ regexp {readDirCache} $seqpt ] } {
           set ::waiting_for_cache_read 0
        }
        addLogEntry "$seqpt$err" red
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: lsync::diagnostic
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::diagnostic { { log 0 } } {
     
     if { [ catch {
        set data   [ list ]
        set ::text_formatted_cache [ list ]
        set names [ array names ::lsync::cache *,* ]
        set names [ lsort -dictionary $names ]
        
        foreach name $names {
            lappend data $name [ set ::lsync::cache($name) ]
        }
        
        if { ! [ string equal 0 $log ] } {
           addLogEntry $data blue
        }
        
        foreach [ list name value ] $data {
           append ::text_formatted_cache "$name $value\n"
        }
        
        set ::text_formatted_cache \
           [ string trim $::text_formatted_cache ]
        
     } err ] } {
        catch { { ::close $fid }  }
        addLogEntry $err red
     }
}
## ******************************************************** 

## ********************************************************
##
## Name: lsync::afterBootlock
##
## Description:
## Helper function that defers initialization steps until
## AFTER the hash file read is complete.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc lsync::afterBootlock { args } {
     if { [ string equal 0 $::waiting_for_cache_read ] } {
        after 2000 [ list bgLoop updateexcl lsync::updateExcludedDirs 50 ]
        after 4000 [ list bgLoop cacheupdate lsync::updateMountPoint 15 ]
        after 3000 lsync::synchronize
        after 5000 [ list ScanMountPointListContinuously ]
     } else {
        after 1000 lsync::afterBootlock
     }
}
## ********************************************************

## ******************************************************** 
##
## Name: myName
##
## Description:
## Returns the name of the proc that calls it.
##
## Usage:
##       set myname [ myName ]
##
## Comments:
## This is the canonical form for getting a procs name.
## The utility of this is that a proc may be dynamically
## named, and the name managed much more easily this way
## than by trying to stack up and unstack proc names.
##
## Note that level 0 is the context of myName,
## level -1 is the context of the caller.  In general,
## you will not mess with the level.  Though you could
## get the name of the proc that called the caller with
## -2, and so forth.

proc myName { { level "-1" } } { 
     
     if { $level > 0 } {
        return -code error "myName: called with level > 0 ($level)."
     }

     if { [ catch {
        set name [ lindex [ info level $level ] 0 ]
     } err ] } {
        set name $::argv0
     }
     set name
}
## ******************************************************** 


## ******************************************************** 
##
## Name: addLogEntry
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc addLogEntry { args } {
     
     if { [ catch {
        set msg    [ lrange $args 0 end-1 ]
        set color  [ lindex $args end     ]
        set caller [ uplevel myName       ]
        set time   [ clock format [ clock seconds ] ]
        switch -exact $color {
             email {
                    set color ****
                    set fid stderr
                   }
               red {
                    set color "*** "
                    set fid stderr
                   }
            orange {
                    set color "**  "
                    set fid stderr
                   }
           default {
                    set color "*   "
                    set fid stdout
                   }
        }
        puts $fid "$color $time ${caller}: $msg"
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: bgLoop 
##
## Description:
## Start asynchronous looping jobs.  Jobs are ended by
## setting ::bg::jobs($name,run) to 0.
##
## Usage:
##        start: bgLoop $name $code $delay
##         stop: set ::bg::jobs($name,run) 0
##
## Comments:
## Since multiple processes with the same name CAN be started,
## a unique name should be chosen for each process.
## Process name bookkeeping must be done by the caller.
## the job code MUST NOT BLOCK, and should never return
## explicitly.

proc bgLoop { { name NULL } { code "" } { delay 2 } } {
    
     if { ! [ llength [ namespace children :: bg ] ] } {
        namespace eval bg {}
        set ::bg::iterator 0
     }
     
     incr ::bg::iterator
     
     ;## register a new job if it has valid args
     if { ! [ string equal NULL $name ]      && \
            [ string length [ join $code ] ] } {
        set ::bg::jobs($name,run)   1
        set ::bg::jobs($name,code)  $code
        set ::bg::jobs($name,delay) $delay
        addLogEntry "Looping process $name started"
     }
     
     if { [ info exists ::bg::after ] && \
          [ lsearch [ after info ] $::bg::after ] != -1 } {
        after cancel $::bg::after
     }
     
     if { [ string equal NULL $name ] } {
        set dt 0
        foreach job [ array names ::bg::jobs *,run ] {
           set job [ lindex [ split $job , ] 0 ]
           if { [ string equal NULL $job ] } { continue }
           
           ;## if the run flag == 0, unregister the job
           if { [ string equal 0 $::bg::jobs($job,run) ] } {
              foreach item [ array names ::bg::jobs $job,* ] {
                 unset ::bg::jobs($item)
              }
              addLogEntry "Looping process $job terminated"
              continue
           }
           
           ;## otherwise, eval!
           if { ! ($::bg::iterator % $::bg::jobs($job,delay)) } {
              set ts [ clock clicks -milliseconds ]
              if { [ catch {
                 eval $::bg::jobs($job,code)
              } err ] } {
                 set ::bg::jobs($job,run) 0
                 addLogEntry "$err ($::bg::jobs($job,code))" email
              }
              set te [ clock clicks -milliseconds ]
              set td [ expr $te - $ts ]
              set dt [ expr $dt + $td ]
              lappend data [ list $job $td ]
           }
        }
        
        ;## produce a timing report if required
        if { ($dt > 1000) && [ info exists ::PROFILE_BGLOOP ] && \
             [ string equal 1 $::PROFILE_BGLOOP ] } {
           addLogEntry "runtime per iteration: $dt ms ($data)" blue  
        }
        
        set ::bg::after [ after 1000 bgLoop ]
     } else {
        ;## we are running the code block for the first time,
        ;## so we eval NOW.
        if { [ catch {
           set retval [ eval $::bg::jobs($name,code) ]
        } err ] } {
           if { [ info exists job ] } {
              set ::bg::jobs($job,run) 0
              addLogEntry "$err ($::bg::jobs($job,code))" email
           } else {
              addLogEntry "$err ('job' not defined)" email
           }
           set ::bg::after [ after 1000 bgLoop ]
           return -code error $err
        }
        set ::bg::after [ after 1000 bgLoop ]
        return $retval
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: bgerror 
##

## Description:
## Unhandled exception sequence
##
## Usage:
##        called internally by Tcl
##
## Comments:
## When a Tcl script has an event loop but the Tk package
## is not loaded, a command called bgerror must be provided
## to handle asynchronous errors from within the event loop.

proc bgerror { args } {
     ;## if a socket is hosed inside a bgerror it needs to be
     ;## cleaned up!!  parse the error message for the sock i.d.
     if { [ regexp {\"(sock\d+)\":\s+broken\s+pipe} $args -> sock ] } {
        catch { ::close $sock }
     }
     set trace {}
     catch { set trace $::errorInfo }
     set caller [ myName -2 ]
     set msg "$args (stack: '$trace')"
     addLogEntry "$caller $msg" email
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dumpFile
##
## Description:
## return the contents of a file in a form suitable for
## "more" or "less" etc.
##
## Usage:
##       set data [ dumpFile filename ]
##
## Comments:
## This is an efficient slurper of files.
proc dumpFile { { file "" } } {
     if { ! [ string length $file ] } {
        return {}
        }
     if { ! [ file exists $file ] } {
        return {}
        }
     if { [ catch { set fid [ open $file r ] } err ] } {
        return -code error $err
        }
     set size [ file size $file ]
     set data [ read $fid $size ]
     catch { ::close $fid }
     set data [ string trim $data ]
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: main
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

source lsync.rsc
::lsync::init
vwait eternally
## ******************************************************** 

