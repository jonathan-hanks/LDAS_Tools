## ********************************************************
##
## Name: lsync.rsc
##
## Description:
## This is the resource file for lsync.tcl, a standalone
## form of the LDAS diskcache API that is incapable of
## user interaction, and acts as a drone maintainer of
## the file lsync.cache.
##
## Note that changes to ::MOUNT_PT and
## ::EXCLUDE_THESE_DIRS_FROM_UPDATES in this file WILL BE
## DETECTED AUTOMATICALLY BY THE RUNNING PROGRAM!!
##
## ********************************************************



;## desc=list of toplevel directories where frame files are found
set ::MOUNT_PT [ list /some/dir1 some/dir2 /some/dir/etc ]

;## desc=list of active mount points that we don't want extra email about
set ::IGNORE_REMOVED_DIRS_UNDER_MTPT [ list /frames/full ]
                     
;## desc=how many seconds to allow in a sigle framequery
set ::FRAMELIMIT 86400

;## desc=set this to 0 to supress logging of diagnostic
set ::CACHE_DIAGNOSTIC_LOGGING 1

;## desc=how long to sleep (ms) before starting a new ::MOUNT_PT entry scan thread.
set ::DELAY_BETWEEN_THREAD_SPAWNS_MS 100

;## desc=if the thread updating a ::MOUNT_PT entry takes this long, log it
set ::DIR_SLOW_UPDATE_WARNING_THRESHHOLD 1800

;## desc=how many ::MOUNT_PT update threads can run simultaneously
set ::NUMBER_OF_RUNNING_THREADS_PERMITTED 10

;## desc=how long to wait for existing threads to complete before aborting rebuild
set ::WAIT_N_SECONDS_FOR_THREADS_TO_COMPLETE 10

;## desc=report progress scanning through ::MOUNT_PT
set ::DEBUG_SCAN_RATE 1

;## desc=list of directories to be excluded from update scans
set ::EXCLUDE_THESE_DIRS_FROM_UPDATES [ list A4 Burst-MDC E4 E6 minute-trend second-trend ]

;## desc=name that will be used for C++ binary hash file on disk
set ::DISKCACHE_HASHFILE_NAME_BINARY lsync.cache

;## desc=name that will be used for C++ binary hash file on disk
set ::DISKCACHE_HASHFILE_NAME_ASCII lsync.cache.txt

;## desc=list of filename extensions that should be included in hash
set ::SCANNED_FILENAME_EXTENSIONS [ list .gwf ]

;## desc=how many seconds to delay repeating email reports of cache problems
set ::ERROR_REPORT_INTERVAL 3600

;## The ::CHECK_FOR_MOUNT_PT_CONFLICT was added initially as a diagnostic
;## tool for PR2753. The conclusion is that looking up of duplicates is
;## not the source of the observed slow down. The default value of 1
;## should be used used unless experimenting with system performance.
;## desc=1 if system should check for mount point conflicts, 0 otherwise
set ::CHECK_FOR_MOUNT_PT_CONFLICT 1

;## desc=turn on debugging of c++ thread information
set ::DEBUG_THREADS 0

;## desc=virtual memory limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default datasize \
default  core default maxproc default \
descriptors  default memorylocked default \
filesize  default cputime default ]

;## desc=Length  of  time  in  seconds  for  (l)stat  calls  to  complete
set ::STAT_TIMEOUT 5

;## desc=Length of time in milliseconds to wait before retrying to acquire mutex lock
set ::RWLOCK_INTERVAL_MS 2000

;## desc=Length of time in milliseconds to try and acquire mutex lock
set ::RWLOCK_TIMEOUT_MS 0

;## desc=Establish the level of lock debugging
;## The debugging level has two components. The first is
;## 0 - Disabled
;## 1 - Keep track of the state of locks
;## 2 - Same as 1 plus keeps a memory version of the text string
;## 3 - Same as 2 plus keeps track of all historical locks used by a thread.
;## The Second component specifies where the log message is saved.
;## 0000 - Create a C++ string with the value.
;## 1000 - Same as 0000 plus create a C global buffer (CDeadLockInfo) with info.
;## 2000 - Same as 1000 plus write out to error log each time the state
;##        has changed.
;##
;## Example: The value 1001 keeps track of active locks and writes out to the
;##          global CDeadLockInfo.
set ::DEBUG_DEADLOCK_DETECTOR_LEVEL 1

set ::LOG_ALL_EMERGENCY_COMMANDS 1
set ::DEBUG_EMERGENCY_PORT 1

;## desc=install user defined signal handlers
set ::TRAP_SIGNAL 1

;## set delay for bgLoop to write cache to disk
set ::CACHE_WRITE_DELAY_SECS 60

;## set SECS to enable bgLoop again in case things are stuck
set ::CACHE_WRITE_RESUME_SECS 3600

;## Specify size of the stack to give each thread.
set ::THREAD_STACK_SIZE 131072
