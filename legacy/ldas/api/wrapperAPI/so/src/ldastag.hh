#ifndef WrapperAPILdasTagHH
#define WrapperAPILdasTagHH


// Communication tags   
enum LDAS_COMM_TAG
{
   NODE_INFO_TAG = 1010,
   NODE_DATA_TAG,
   NODE_INPUT_DATA_TAG,
   NODE_WARNING_TAG,
   NODE_FINALIZE_TAG,
   NODE_HOSTNAME_TAG
}; 

   
// Datatype generation tags   
enum LDAS_DATA_TAG
{
   STRUCT_INFO_TAG = 1020
};        
   
   
// Load balancing communication tags
enum LDAS_LB_TAG
{
   LB_INFO_TAG = 1030
};           
   
   
#endif   
