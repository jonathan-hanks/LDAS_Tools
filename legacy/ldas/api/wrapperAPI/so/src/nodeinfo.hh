#ifndef WrapperAPINodeHH
#define WrapperAPINodeHH

   
// Local Header Files
#include "ldaserror.hh"   

   
class DLErrorHandler;
   
   
//-------------------------------------------------------------------------------   
//         
//: Node information structure.  
// 
// This data structure contains neccessary information for the node involved into 
// the parallel job processing.
//   
struct lbNodeInfo
{
   //: Number of elements in the result array of outPut data structures.
   UINT4 numResults;
   
   //: Node rank in the current MPlusLBcomm communicator.
   INT4  rank;
   
   //: Time node spent in the applySearch() function.
   REAL8 deltaT;   
   
   //: Remaining fraction of processing job for the current node.
   REAL4 fractionRemaining;
   
   //: Flag to indicate that applySearch() is finished.
   BOOLEAN notFinished;

   //: Buffer to store error messages.
   CHAR* error;
   
   //: Buffer to store warning messages.
   CHAR* warning;

   //: MPI derived datatype to represent current node information.
   MPI::Datatype nodeType;
   
   lbNodeInfo();
   ~lbNodeInfo();
   
   void setMessages( const DLErrorHandler& eh );
   void resetMessages();
   
private:

   //: Buffer dimension to store error and warning messages.
   static const INT4 msgDim;
   
   void createMPIType();
   void cleanup();
};
   

#endif   

   
