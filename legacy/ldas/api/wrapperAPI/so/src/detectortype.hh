#ifndef WrapperApiDetectorHH
#define WrapperApiDetectorHH

#include <list>   
   
#include "ldasdatatype.hh"
   

//------------------------------------------------------------------------------
//
//: DetectorType class.
// 
// This class represents MPI derived datatype corresponding to the C detector 
// detGeom data structure.
//   
class DetectorType : public LdasDatatype
{
   
public:
   
   /* Constructor */
   DetectorType( const ILwd::LdasElement* pe, detGeom** data,
      const UINT4 num_detectors, const INT4 rank = -1,
      const MPI::Intracomm& comm = MPI::COMM_WORLD );
   
   DetectorType( detGeom** data, const UINT4 num_detectors,
      const MPI::Intracomm& comm = MPI::COMM_WORLD, const INT4 rank = -1 );
   
   /* Destructor */
   virtual ~DetectorType();

   static ILwd::LdasElement* toILwd( const detGeom* pe, 
      const UINT4 num_detectors, const MPI::Intracomm& comm );
   static const std::string& getILwdName();   
   static const size_t getILwdNamePosition();         
   
   static void freeMpiType();
   static const MPI::Datatype& getMpiType();
   
private:

   //: No default constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //            
   DetectorType( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
    
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //            
   DetectorType( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD, const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //            
   DetectorType( const DetectorType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   DetectorType& operator=( const DetectorType& re ); 
   
   /* Helper methods, called only by master node. */
   template< class T > const T getArrayValue( 
      const ILwd::LdasContainer& pe, const CHAR* name ) const;

   void parseILwd( const ILwd::LdasElement& re, detGeom* const data ) const;
   
   /* Methods called by all nodes in communicator */
   void allocateData( detGeom** data );

   // Build MPI datatype
   void createMpiType();   

   virtual const CHAR* getDatatypeName() const;   
   
   //: Name attribute field to use for conversion to ILWD.
   static const std::string mILwdName;

   //: Name field position to use for conversion to ILWD.
   static const size_t mILwdNamePosition;   
   
   //: Outter most ILWD format container name attribute.
   static const CHAR* mOutterContainerName;   
   
   //: Name attribute for detector ILWD format container.
   static const CHAR* mContainerName;      

   static const CHAR* mPrefixILwdName;               
   static const CHAR* mLongitudeILwdName;
   static const CHAR* mLatitudeILwdName;   
   static const CHAR* mElevationILwdName;   
   static const CHAR* mArmXazimuthILwdName;   
   static const CHAR* mArmYazimuthILwdName;      
   static const CHAR* mArmXaltitudeILwdName;      
   static const CHAR* mArmYaltitudeILwdName;         
   static const CHAR* mLocalTimeILwdName;            
   
   //: Number of MPI datatypes per detector structure.
   static const INT4 mTypeNum;
   
   //: Number of elements in the array of detGeom structures.
   UINT4 mNum;
   
   //: MPI datatype to represent detGeom data structure
   static MPI::Datatype mMpiType;
};

   
void storeDetector( detGeom** pd, const detGeom* ps,
   const UINT4 num, const MPI::Intracomm& comm, const bool p2p );

#endif   
