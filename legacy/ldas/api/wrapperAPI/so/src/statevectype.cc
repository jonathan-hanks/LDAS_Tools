#include "LDASConfig.h"

// System Header Files
#include <algorithm>   
#include <memory>   
   
// ILwd Header Files
#include <ilwd/ldascontainer.hh>   
   
// Local Header Files   
#include "statevectype.hh"   
#include "multidimdata.hh"   
#include "mpilaltypes.hh"   
#include "ldaserror.hh"   
#include "initvars.hh"   
#include "multidimdata.hh"

using namespace std;   
   
using ILwd::LdasContainer;   
using ILwd::LdasElement;    
using ILwd::ID_ILWD;

// Static data members initialization.   
const string StateVectorType::mILwdName( "stateVector" );
const size_t StateVectorType::mILwdNamePosition( 0 );   
const string StateVectorType::mILwdStateNameAtt( "stateName" );   
const UINT4 StateVectorType::mTypeNum( 2 );   
   
   
//------------------------------------------------------------------------------
//
//: Constructor.
//   
// This constructor generates MPI derived datatype to represent C stateVector
// linked list used as part of inPut data structure.
// <p>When called by master it initializes passed to it stateVector linked list
// with the data of ILWD format element representing the list.
// <p>All nodes using this C linked list for communication must construct this
// datatype.
//
//!param: ilwdConstVector& state_vec - A reference to the vector of ILWD        
//+       elements representing stateVector data.
//!param: stateVector** ps - A pointer to the stateVector linked list being   
//+       initialized by master.   
//!param: const MPIStructInfo* stateInfo - An array of metadata structures for
//+       stateVector->store multiDimData structures.      
//!param: const UINT4 num - Number of nodes in the stateVector linked list.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which StateVectorType object is defined. Default is 
//+       MPI::COMM_WORLD.   
//
//!exc: sequence is missing. - ILWD format element representing sequence data   
//+     is missing.   
//!exc: Undefined datatype. - Undefined datatype for ILWD element               
//+     representing the data was specified.     
//!exc: Undefined domain. - Undefined domain was specified.                      
//+     One of TIME | FREQ | BOTH.       
//!exc: data is missing. - ILWD format element representing the data is missing.         
//
//!exc: start_time is missing. - Could not find elements with "start_time"      
//+     name field.
//!exc: step_size is missing. - Could not find element with step_size name      
//+     field.   
//!exc: Malformed input format for gpsTimeInterval. - Invalid input format was  
//+     specified for gpsTimeInterval.   
//!exc: start_freq is missing. - Could not find element with start_freq name    
//+     field.   
//!exc: Malformed input format for frequencyInterval. - Invalid input format    
//+     was specified for frequencyInterval.      
//!exc: Malformed input format for timeFreqInterval. - Invalid input format     
//+     was specified for timeFreqInterval.
//!exc: Inconsistent number of samples for timeFreqInterval. - Number of time   
//+     and frequency samples is different.         
//   
//!exc: Error generating IntervalType. - Error generating MPI derived datatype  
//+     to represent C interval union.   
//!exc: Error generating MultiDimDataType. - Error generating MPI derived        
//+     datatype to represent an array of C multiDimData structures.            
//!exc: Error generating HistoryType. - Error generating MPI derived datatype    
//+     to represent history linked list.   
//!exc: Invalid number of history nodes specified. - Negative number of nodes    
//+     was specified.                 
//!exc: Error generating StateVectorType. - Error generating MPI derived         
//+     datatype representing the stateVector linked list.      
//!exc: Invalid number of stateVector nodes. - Negative number specified for        
//+     number of nodes in the linked list.      
//!exc: bad_alloc - Error allocating memory.
//!exc: bad_cast - Input ILWD data is malformed.   
//!exc: MPI::Exception - MPI library exception.      
//
StateVectorType::StateVectorType( ilwdConstVector& state_vec, 
   stateVector** ps, const MPIStructInfo* stateInfo, const UINT4 num, 
   const INT4 rank, const MPI::Intracomm& comm ) 
   : LdasDatatype( comm, rank ), mNum( num )
{
   // There is stateVector
   if( mNum > 0 )
   { 
      // All nodes: allocate memory for the linked list
      allocateListData( ps );
   
   
      // Master node extracts data information and content.
      if( inMaster() )
      {
         parseILwd( state_vec, *ps );
      }

   
      // Build MultiDimDataType to represent store of each node
      vector< MPI::Datatype > storeType( mNum );
      ilwdVector store_vec;
      stateVector* curr_node( *ps );

   
      for( UINT4 i = 0; i < mNum; ++i )
      {
         if( inMaster() )
         {
            store_vec.erase( store_vec.begin(), store_vec.end() );
            store_vec.push_back( state_vec[ i ] );
         }
   
   
         // No memory leak here since MPI copy is shallow
         storeType[ i ] = MultiDimDataType( store_vec, &( curr_node->store ),
                                            &stateInfo[ i ], 1, rank );

         curr_node = curr_node->next;
      }

   
      // All nodes: Create datatype for the stateVector linked list
      buildType( storeType, *ps );
   }
   else if( mNum == 0 )
   {
      // There is no stateVector data present in the ILWD element
      createNULLType();
   }
   else
   {
      node_error( "Invalid number of stateVector nodes.", 
                  comm, ( rank >= 0 ) );
   }   
}

   
//------------------------------------------------------------------------------
//
//: Constructor.
//   
// This constructor generates MPI derived datatype to represent a C stateVector 
// linked list used as part of outPut data structure. 
// This constructor to be called only by two nodes that will use created datatype 
// for communication: slave node containing the data and master node 
// collecting that data. 
//
//!param: stateVector** ps - A pointer to the stateVector linked list being      
//+       initialized by master, but containing the valid data for slave.   
//!param: const UINT4 num - Number of nodes in the <b>ps</b> linked list.   
//!param: const MPIStructInfo* stateInfo - An array of metadata structures for   
//+       stateVector->store multiDimData structures.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator  
//+       within which StateVectorType object is defined. Default is          
//+       MPI::COMM_WORLD.   
//!param: const INT4 rank - Rank of the node sending the data. Default is -1.   
//
//!exc: bad_alloc - Error allocating memory.
//!exc: Undefined domain. - Undefined domain was specified.                      
//+     One of TIME | FREQ | BOTH.      
//!exc: Undefined datatype. - Undefined datatype for ILWD element                
//+     representing the data was specified.     
//!exc: Invalid number of stateVector nodes. - Negative number specified for        
//+     number of nodes in the linked list.      
//!exc: Error generating StateVectorType. - Error generating MPI derived        
//+     datatype representing the stateVector linked list.   
//!exc: Invalid number of history nodes specified. - Negative number of nodes    
//+     was specified.         
//!exc: Error generating HistoryType. - Error generating MPI derived datatype    
//+     to represent history linked list.   
//!exc: Error generating IntervalType. - Error generating MPI derived datatype   
//+     to represent C interval union.      
//!exc: Invalid number of multiDimData. - Negative number of array elements is   
//+     specified.         
//!exc: Error generating MultiDimDataType. - Error generating MPI derived        
//+     datatype to represent an array of C multiDimData structures.         
//!exc: MPI::Exception - MPI library exception.      
//
StateVectorType::StateVectorType( stateVector** ps, const UINT4 num,
   const MPIStructInfo* stateInfo, const MPI::Intracomm& comm, const INT4 rank )
   : LdasDatatype( comm, rank ), mNum( num )
{
   if( mNum > 0 )
   {
      if( inMaster() )
      {
         // Master allocates memory for the linked list
         allocateListData( ps );
      }

   
      // Build MultiDimDataType to represent store of each node
      vector< MPI::Datatype > storeType( mNum );
      stateVector* curr_node( *ps );
   
      for( UINT4 i = 0; i < mNum; ++i )
      {
         // No memory leak here since MPI copy is shallow
         storeType[ i ] = MultiDimDataType( &( curr_node->store ), 
                             &stateInfo[ i ], comm, rank );
   
         curr_node = curr_node->next;
      }   

   
      // Create MPI derived datatype
      buildType( storeType, *ps );
   }
   else if( mNum == 0 )
   {
      createNULLType();
   }
   else
   {
      node_error( "Invalid number of stateVector nodes.", comm );
   }
}
   
   
//-----------------------------------------------------------------------------
//
//: Destructor.
//   
StateVectorType::~StateVectorType()   
{}

  
//------------------------------------------------------------------------------
//
//: Converts stateVector linked list to the ILWD format element. 
//       
//!param: const stateVector* pe - A pointer to the stateVector linked list.
//!param: const MPI::Intracomm& comm - A reference to the intracommunicator 
//+       within which datatype is defined.  
//   
//!return: LdasElement* - A pointer to the ILWD format element.
//
//!exc: bad_alloc - Memory allocation failed.   
//   
LdasElement* StateVectorType::toILwd( const stateVector* pe,
   const MPI::Intracomm& comm )
{
   auto_ptr< LdasContainer > c( new LdasContainer( mILwdName ) ); 
   const stateVector* curr_state( pe );   
   bool first_node( true );

   
   while( curr_state )
   {
      // Create store 
      auto_ptr< LdasElement > e( MultiDimDataType::toILwd( curr_state->store[ 0 ], 
                                                           comm ) );
   
      if( numDummyState && first_node )
      {
         e->setName( mPrimaryPos, "primary" );               
         first_node = false;
      }

      const string n( curr_state->stateName );
      e->setMetadata( mILwdStateNameAtt, n );

      c->push_back( e.release(),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );
      curr_state = curr_state->next;   
   }

   
   return c.release();
}
   
   
//------------------------------------------------------------------------------
//
//: Gets ILWD format name attribute for stateVector linked list.
//       
//!return: const std::string& - A reference to the name attribute.
//      
const std::string& StateVectorType::getILwdName()
{
   return mILwdName;
}

   
//------------------------------------------------------------------------------
//
//: Gets name field position of the ILWD format attribute.
//       
//!return: const size_t - A field position.
//      
const size_t StateVectorType::getILwdNamePosition()
{
   return mILwdNamePosition;
}
   
   
//-----------------------------------------------------------------------------
//   
//: Allocates memory for the C stateVector linked list.
//
//!param: stateVector** ps - A pointer to the linked list to allocate.
//    
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
void StateVectorType::allocateListData( stateVector** ps ) const
{
   stateVector* curr_node( 0 );
   stateVector* prev_node( 0 );
   bool first_node( true );
   
   
   for( UINT4 i = 0; i < mNum; ++i )
   {
      curr_node = new stateVector;
      memset( curr_node, 0, sizeof( stateVector ) );
   
      if( first_node )
      {
         *ps = curr_node;
         first_node = false;
      }
      else
      {
         curr_node->previous = prev_node;
         prev_node->next = curr_node;
      }

       prev_node = curr_node;
   }

   
   return;
}

   
//-----------------------------------------------------------------------------
//   
//: Parse ILWD element.
//   
// This method will be called only by the master node of the communicator! 
// The node will extract the data from passed to it ILWD elements and 
// initialize passed to it stateVector linked list with that data.
//
//!param: ilwdConstVector& re - A reference to the vector of ILWD format        
//+       elements containing the data that is used for linked list          
//+       initialization.   
//!param: stateVector* ps - A pointer to the stateVector linked list.   
//
//!return: Nothing.
//   
void StateVectorType::parseILwd( ilwdConstVector& re, stateVector* ps ) const
{
   stateVector* curr_node( ps ); // Pointer to the current node in the list
   size_t s_size( 0 );
   
   for( const_iterator iter( re.begin() ), iter_end( re.end() );
        iter != iter_end; ++iter )
   {
      // Get name of the state
      const string& name =
	( *iter )->getMetadata<std::string>( mILwdStateNameAtt );
      s_size = std::min( static_cast< INT4 >( name.size() ), 
                         maxStateName - 1 );

      std::copy( name.begin(), name.begin() + s_size,
                 curr_node->stateName );
      curr_node->stateName[ s_size ] = '\0';
   
   
      curr_node = curr_node->next;
   }

   
   return;
}


//-----------------------------------------------------------------------------
//
//: Build MPI derived datatype.
//
// This method should be called by all nodes in the communicator that use
// linked list for the communication.   
// It generates MPI derived datatype representing C stateVector linked 
// list.  This method will mark passed to it MPI datatype objects for 
// deallocation.
//   
//!param: MPI::Datatype sType[] - An array of MPI datatypes where each element   
//+       represents C multiDimData structure.   
//!param: stateVector* ps - A pointer to the stateVector linked list.   
//
//!return: Nothing.
//  
//!exc: Error generating StateVectorType. - Error creating MPI stateVector derived  
//+     datatype.   
//!exc: bad_alloc - Memory allocation failed.
//!exc: MPI::Exception - MPI library exception.      
//  
void StateVectorType::buildType( vector< MPI::Datatype >& sType, stateVector* ps )
{
   const INT4 num( mNum * mTypeNum );
   
   vector< MPI::Datatype > types( num );
   vector< INT4 >          blocks( num );
   vector< MPI::Aint >     disps( num );
   
   // Current node in the linked list
   stateVector* curr_node( ps );
   UINT4 i( 0 );
   INT4 i_num( 0 );   

   
   while( curr_node != 0 )
   {
      // Init stateName info   
      types[ i_num ]  = LDAS_MPI_CHAR;
      blocks[ i_num ] = maxStateName;
      disps[ i_num ]  = MPI::Get_address( curr_node->stateName ); 
   
   
      // Init store info
      ++i_num;
      types[ i_num ]  = sType[ i ];
      blocks[ i_num ] = 1;
      disps[ i_num ]  = MPI::Get_address( curr_node->store[ 0 ].name );    
   
   
      curr_node = curr_node->next;
      ++i;
      ++i_num;
   }

   
   createType( i_num, blocks, disps, types );   
   
   
   for( UINT4 i = 0; i < mNum; ++i )
   {
      sType[ i ].Free();
   }
   
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Gets name of the derived datatype.
//         
//!return: const CHAR* - Datatype name.
//   
const CHAR* StateVectorType::getDatatypeName() const
{
   return "StateVectorType";
}
   
   
   
   
