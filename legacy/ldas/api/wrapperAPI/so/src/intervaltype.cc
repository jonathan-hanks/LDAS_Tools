#include "LDASConfig.h"

// System Header Files
#include <string>
#include <cstring>   
#include <algorithm>
   
// ILwd Header Files
#include <ilwd/ldascontainer.hh>   
#include <ilwd/ldasarray.hh>   
using ILwd::LdasArrayBase; 
using ILwd::LdasArray;   
using ILwd::ID_INT_4U;      
using ILwd::ID_REAL_8;
using ILwd::ID_ILWD;
   
// Local Header Files   
#include "intervaltype.hh"
#include "ldaserror.hh"
#include "mpilaltypes.hh"   
#include "initvars.hh"   
#include "detectortype.hh"   

using namespace std;   
   
using ILwd::LdasContainer;      
using ILwd::LdasElement;   

   
const INT4 IntervalType::mTypeNum( 5 );   
const INT4 IntervalType::mInitChannelPos( 0 );   

//#define DEBUG_CHANNEL_NAME      
   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor generates MPI derived datatype that represents C interval 
// union. When called by master it initializes union with the data of passed to
// it ILWD element representing the union. All nodes using interval C union for 
// communication must create this MPI datatype.  
// <p>This constructor will be used if union is a part of inPut data structure.   
//   
//!param: const LdasElement* pe - A pointer to the ILWD element containing the 
//+       data for union initialization.
//!param: const domain& space - An enum type specifying the domain.   
//!param: interval& ri - A reference to the interval structure being initialized.   
//!param: const UINT4 num_samples - A number of samples in interval. This will be
//+       used only when stopTime/stopFreq is not provided in the inPut data.   
//!param: const UINT4 num_channels - Number of channel names.
//!param: const UINT4 channel_size - Buffer size to store channel names.  
//!param: const UINT4 num_detectors - Number of detector geometry structures.   
//!param: const INT4 rank - Rank of the node sending/receiving the data. Default
//+       is -1   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator
//+       within which IntervalType object is defined. Default is           
//+       MPI::COMM_WORLD.   
//   
//!exc: Undefined domain. - Undefined domain is specified.                     
//+     One of TIME | FREQ | BOTH | DATABASE        
//   
//!exc: start_time is missing. - Could not find elements with <i>start_time</i>
//+     name field.
//!exc: step_size is missing. - Could not find element with <i>step_size</i>
//+     name field.   
//!exc: Malformed input format for gpsTimeInterval. - Invalid input format was 
//+     specified for gpsTimeInterval.   
//!exc: start_freq is missing. - Could not find element with <i>start_freq</i>
//+     name field.   
//!exc: Malformed input format for frequencyInterval. - Invalid input format   
//+     was specified for frequencyInterval.      
//!exc: Malformed input format for timeFreqInterval. - Invalid input format    
//+     was specified for timeFreqInterval.
//!exc: Inconsistent number of samples for timeFreqInterval. - Number of time  
//+     and frequency samples is different.         
//   
//!exc: Error generating IntervalType. - Error generating MPI derived datatype 
//+     to represent C interval union.   
//!exc: bad_alloc - Error allocating the memory.   
//!exc: bad_cast - ILWD input data is malformed.   
//!exc: MPI::Exception - MPI library exception.      
//   
IntervalType::IntervalType( const ILwd::LdasElement* pe, const domain& space,
   interval& ri, const UINT4 num_samples, const UINT4 num_channels,
   const UINT4 channel_size, const UINT4 num_detectors,
   const INT4 rank, const MPI::Intracomm& comm ) 
   : LdasDatatype( comm, rank ), mNumberChannels( num_channels ),
     mChannelSize( channel_size ), mNumberDetectors( num_detectors ),
     mDomain( space )
{
   allocateData( ri );
   
   // If there's a detector geometry information, allocate the memory,
   // create datatype if necessary, master node parses its ILWD
   // representation
   if( num_detectors )
   {
      const LdasElement* detector_pe( 0 );
      if( inMaster() )
      {
         detector_pe = findElement( *pe, DetectorType::getILwdName(),
                                    DetectorType::getILwdNamePosition() );
         if( detector_pe == 0 )
         {
            // Something went really wrong!
            ostringstream s( "ILWD representation of detector geometry is "
               "missing when num_detectors=" );
            s << num_detectors;
            node_error( s.str().c_str(), comm, ( rank >= 0 ) );
         }
      }
   
      switch( space )
      {
         case(noneD):
         {
            DetectorType detType( detector_pe, &( ri.dNone.geometry ), 
                                  num_detectors, rank, comm );
            break;
         }   
         case(timeD):
         {
            DetectorType detType( detector_pe, &( ri.dTime.geometry ), 
                                  num_detectors, rank, comm );
            break;
         }
         case(freqD):
         {
            DetectorType detType( detector_pe, &( ri.dFreq.geometry ), 
                                  num_detectors, rank, comm );   
            break;
         }   
         case(bothD):
         {
            DetectorType detType( detector_pe, &( ri.dBoth.geometry ), 
                                  num_detectors, rank, comm );   
            break;
         }   
         case(databaseD):
         {
            node_error( "databaseD domain doesn't support detector geometry.",
                        comm, ( rank >= 0 ) );
         }
         default:   
         {
            node_error( "Undefined domain.", comm, ( rank >= 0 ) );
         }   
      }
   }
   
   
   if( inMaster() )
   {
      // Master parses channel names out
      parseChannelName( pe, ri );
   
      switch( space )
      {
         case(noneD):
         {
            initNoneInterval( ri.dNone, num_samples );
            break;
         }   
         case(timeD):
         {
            initTimeInterval( *pe, ri.dTime, num_samples );
            break;
         }
         case(freqD):
         {
            initFreqInterval( *pe, ri.dFreq, num_samples );
            break;
         }   
         case(bothD):
         {
            initBothInterval( *pe, ri.dBoth, num_samples );
            break;
         }   
         case(databaseD):
         {
            initDatabaseInterval( ri.dDatabase, num_samples, pe->getComment() );
            break;
         }
         default:   
         {
            node_error( "Undefined domain.", comm, ( rank >= 0 ) );
         }
      }   
   }


   // Build the type:
   buildType( ri );
}

   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// Nodes that instantiate this class build the derived datatype to represent
// <i>ri</i> union for data communication.   
// <p>This constructor will be used if union is a part of outPut data structure.      
//   
//!param: const domain& space - An enum type specifying the domain.   
//!param: interval& ri - A reference to the interval structure being allocated  
//+       by master node, and containing valid data on slave nodes.         
//!param: const UINT4 num_channels - Number of channel names.
//!param: const UINT4 channel_size - Buffer size to store channel names.     
//!param: const UINT4 num_detectors - Number of detector geometry structures.      
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which IntervalType object is defined. Default is            
//+       MPI::COMM_WORLD.   
//!param: const INT4 rank - Rank of the node sending the data. Default is -1.   
//   
//!exc: Error generating IntervalType. - Error generating MPI datatype to      
//+     represent C interval union.
//!exc: MPI::Exception - MPI library exception.      
//!exc: bad_alloc - Memory allocation failed.   
//   
IntervalType::IntervalType( const domain& space, interval& ri,
   const UINT4 num_channels, const UINT4 channel_size, 
   const UINT4 num_detectors, const MPI::Intracomm& comm, const INT4 rank ) 
   : LdasDatatype( comm, rank ),
     mNumberChannels( num_channels ), mChannelSize( channel_size ),
     mNumberDetectors( num_detectors ), mDomain( space )
{
   if( inMaster() )
   {
      allocateData( ri );   
   }
   
   // If there's a detector geometry information, allocate memory for it,
   // create MPI type if necessary
   if( num_detectors )
   {
      switch( space )
      {
         case(noneD):
         {
            DetectorType detType( &( ri.dNone.geometry ), 
                                  num_detectors, comm, rank );
            break;
         }   
         case(timeD):
         {
            DetectorType detType( &( ri.dTime.geometry ), 
                                  num_detectors, comm, rank );
            break;
         }
         case(freqD):
         {
            DetectorType detType( &( ri.dFreq.geometry ), 
                                  num_detectors, comm, rank );   
            break;
         }   
         case(bothD):
         {
            DetectorType detType( &( ri.dBoth.geometry ), 
                                  num_detectors, comm, rank );   
            break;
         }   
         case(databaseD):
         {
            node_error( "databaseD domain doesn't support detector geometry.",
                        comm );
         }
         default:   
         {
            node_error( "Undefined domain.", comm );
         }   
      }
   }
   
   
   // Build the type:
   buildType( ri );
}
   
   
//------------------------------------------------------------------------------
//
//: Destructor
//
IntervalType::~IntervalType()
{}
   

//------------------------------------------------------------------------------
//
//: Converts interval data structure to the ILWD format element. 
//       
//!param: const domain& space -  An enum type specifying the domain.   
//!param: interval& ri - A reference to the interval structure.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined.    
//!param: std::string& channel_name - A reference to the string containing 
//+       channel information.   
//!param: std::string& comment - A reference to the comment for the sequence.
//   
//!return: ilwdResultVector - A vector of ILWD format elements representing the
//+        interval structure.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Undefined domain. - Undefined domain specified.      
//   
LdasDatatype::ilwdResultVector IntervalType::toILwd( const domain& space,
   const interval& ri, const MPI::Intracomm& comm, 
   std::list< std::string >& channel_name, std::string& comment ) 
{
   UINT4 num_channels( 0 );
   CHAR* channels( 0 );
   
   ilwdResultVector result;
   switch( space )
   {
      case(noneD):
      {
         if( ri.dNone.numberDetectors )
         {
            result.push_back( DetectorType::toILwd( ri.dNone.geometry,
                                 ri.dNone.numberDetectors, comm ) );
         }
   
         num_channels = ri.dNone.numberChannels;
         if( num_channels )
         {
            channels = ri.dNone.channelName[ 0 ];
         }
   
         break;
      }   
      case(timeD):
      {
         result = timeToILwd( ri.dTime, comm );
         if( ri.dTime.numberDetectors )
         {
            result.push_back( DetectorType::toILwd( ri.dTime.geometry,
                                 ri.dTime.numberDetectors, comm ) );
         }
   
         num_channels = ri.dTime.numberChannels;
         if( num_channels )
         {
            channels = ri.dTime.channelName[ 0 ];
         }
   
         break;
      }
      case(freqD):
      {
         result = freqToILwd( ri.dFreq, comm );
         if( ri.dFreq.numberDetectors )
         {
            result.push_back( DetectorType::toILwd( ri.dFreq.geometry,
                                 ri.dFreq.numberDetectors, comm ) );
         }
   
         num_channels = ri.dFreq.numberChannels;
         if( num_channels )
         {
            channels = ri.dFreq.channelName[ 0 ];
         }
   
         break;
      }   
      case(bothD):
      {
         result = bothToILwd( ri.dBoth, comm );
         if( ri.dBoth.numberDetectors )
         {
            result.push_back( DetectorType::toILwd( ri.dBoth.geometry,
                                 ri.dBoth.numberDetectors, comm ) );
         }
   
         num_channels = ri.dBoth.numberChannels;
         if( num_channels )
         {
            channels = ri.dBoth.channelName[ 0 ];
         }
   
         break;
      }   
      case(databaseD):
      {
         comment = ri.dDatabase.sqlQuery;
         break;
      }      
      default:
      {
         node_error( "Undefined domain.", comm );
         break;
      }
   }

   
   // Whenever node receives the channel information, it doesn't set the
   // array of pointers to the names.
   // Array of pointers is allocated but only channelName[ 0 ] is set to
   // the beginning of the contiguous buffer that contains all channel names.
   // ===> CAN'T USE first dimension of channelName array to access separate
   // channel names :p
   UINT4 offset( 0 );
   for( UINT4 i = 0; i < num_channels; ++i )
   {
      channels += offset;
      channel_name.push_back( string( channels ) );
      offset = strlen( channels ) + 1;
   }
   
   return result;
}

   
//------------------------------------------------------------------------------
//
//: Gets channel names information.
//
// This method parses channel names from the name attribute of the ILWD
// format element and creates a metadata about extracted data.
//
//!param: const ILwd::LdasElement* pe - Pointer to the ILWD format element.
//!param: UINT4& size - A reference to the size of buffer that would keep
//+       all channel names.   
//
//!return: const UINT4 - A number of parsed out channel names.
//   
const UINT4 IntervalType::getChannelNameInfo( 
   const ILwd::LdasElement* pe, UINT4& size )
{
   ChannelNameLexer parser( pe->getName( mInitChannelPos ) );
   populateChannelParser( parser, pe );

   const channelMap& m( parser.GetChannelNames() );
   const UINT4 dim( m.size() );
   if( dim )
   {
      // Calculate the buffer size
      const_channel_iterator iter( m.begin() ), iter_end( m.end() );
   
      while( iter != iter_end )
      {
         size += iter->first.size() + 1;
         ++iter;
      }   
   }

   return dim;
}
   
   

//------------------------------------------------------------------------------
//
//: Populates channel parser with names.
//
// This method passes ILWD format element's name fields through the parser.
//
//!param: ChannelNameParser& parser - A reference to the channel parser.   
//!param: const ILwd::LdasElement* pe - Pointer to the ILWD format element.
//
//!return: Nothing.
//   
void IntervalType::populateChannelParser( ChannelNameLexer& parser,
   const ILwd::LdasElement* pe )
{
   // Size of name vector
   const INT4 name_size( pe->getNameSize() );

   for( INT4 i = mPrimaryPos + 1; i < name_size; ++i )
   {
      parser.Parse( pe->getName( i ) );
   }

   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Parses channel names.
//
// This method parses out channel names from the sequence's 'name' attribute.
// Called only by the master node of the communicator.   
//  
//!param: const ILwd::LdasElement* pe - A pointer to the ILWD format multiDimData.
//!param: interval& ri - A reference to the interval structure being initialized.      
//
//!return: Nothing.    
//   
void IntervalType::parseChannelName( const ILwd::LdasElement* pe, interval& ri )
{
   if( !mNumberChannels )
   {
      return;
   }

   CHAR* channel_name( 0 );
   switch( mDomain )
   {
      case(noneD):
      {
         channel_name = ri.dNone.channelName[ 0 ];
         break;
      }   
      case(timeD):
      {
         channel_name = ri.dTime.channelName[ 0 ];
         break;
      }
      case(freqD):
      {
         channel_name = ri.dFreq.channelName[ 0 ];
         break;
      }   
      case(bothD):
      {
         channel_name = ri.dBoth.channelName[ 0 ];
         break;
      }   
      case(databaseD):
      {
         break;
      }      
      default:
      {
         node_error( "Undefined domain.", getComm() );
         break;
      }
   }

#ifdef DEBUG_CHANNEL_NAME      
   cout << jobID << ": numberChannels( master )=" << mNumberChannels << endl;
#endif   

   // Get channel names
   ChannelNameLexer parser( pe->getName( mInitChannelPos ) );
   populateChannelParser( parser, pe );
   
   const channelMap& channel_map( parser.GetChannelNames() );  
   if( channel_map.size() != mNumberChannels )
   {
      // Something went really wrong during metadata broadcast
      node_error( "Mismatched number of channels and their count.",
                  getComm() );
   }

   
   UINT4 offset( 0 );

   for( const_channel_iterator iter = channel_map.begin(),
        end_iter = channel_map.end(); iter != end_iter; ++iter )
   {
      const string& name = iter->first;
      std::copy( name.begin(), name.end(), channel_name + offset );

      // Go to the end of name
      offset += name.size();

      // Insert C-style string terminator
      *( channel_name + offset ) = '\0';
      
#ifdef DEBUG_CHANNEL_NAME      
      cout << jobID << ": channelName( master )=" << ( channel_name + offset - name.size() ) << endl;
#endif   

      // step over string terminator
      ++offset;
   }


   return;
}

   
//------------------------------------------------------------------------------
//
//: initNoneInterval method.
//
// This method initializes None domain interval C data structure. This method is 
// called only by the master node of the communicator.   
//   
//!param: rawSequence& ri - A reference to the rawSequence structure   
//+       to be initialized. 
//!param: const UINT4 num_samples - A number of samples in interval.
//
void IntervalType::initNoneInterval( 
   rawSequence& ri, const UINT4 num_samples ) const
{
   ri.numberSamples = num_samples;

   return;
}
   
   
//------------------------------------------------------------------------------
//
//: initTimeInterval method.
//
// This method initializes Time domain interval C data structure. This method is 
// called only by the master node of the communicator.   
//   
//!param: const LdasElement& re - A reference to the ILWD element containing   
//+       the data for structure initialization.
//!param: gpsTimeInterval& ri - A reference to the gpsTimeInterval structure   
//+       to be initialized. 
//!param: const UINT4 num_samples - A number of samples in interval. This will be
//+       used only when stopTime is not provided in the inPut data.      
//
//!exc: start_time is missing. - Could not find elements with <i>start_time</i>
//+     name field.
//!exc: step_size is missing. - Could not find element with <i>step_size</i> 
//+     name field.
//!exc: Malformed input format for gpsTimeInterval. - Invalid input format was 
//+     specified for gpsTimeInterval.   
//!exc: bad_alloc - Error allocating the memory.   
//!exc: bad_cast - ILWD input data is malformed.      
//   
void IntervalType::initTimeInterval( const ILwd::LdasElement& re, gpsTimeInterval& ri,
   const UINT4 num_samples ) const
{
   const LdasContainer& pe( dynamic_cast< const LdasContainer& >( re ) );   
   ilwdConstVector start_time( findILwd( pe, "start_time" ) );

   const bool p2p( isP2P() );
   
   
   if( start_time.size() != 2 || 
       start_time[ 0 ]->getName( 0 ) != "gps_sec" || 
       start_time[ 1 ]->getName( 0 ) != "gps_nan" ||
       start_time[ 0 ]->getElementId() != ID_INT_4U || 
       start_time[ 1 ]->getElementId() != ID_INT_4U )
   {
      node_error( "Malformed input format for gpsTimeInterval: start_time", 
                  getComm(), p2p );
   }

   
   const LdasArray< INT_4U >* t_sec( dynamic_cast< const LdasArray< INT_4U >* >
      ( start_time[ 0 ] ) );
   const LdasArray< INT_4U >* t_nan( dynamic_cast< const LdasArray< INT_4U >* >
      ( start_time[ 1 ] ) );
   if( t_sec->getNData() != 1 || t_sec->getUnits( 0 ) != "sec" ||       
       t_nan->getNData() != 1 || t_nan->getUnits( 0 ) != "nanosec" )
   {
      node_error( "Malformed input format for gpsTimeInterval: start_time.",
                  getComm(), p2p );
   }

   
   ri.startSec = t_sec->getData()[ 0 ];   
   ri.startNan = t_nan->getData()[ 0 ];         
   
   
   const LdasElement* step( findElement( pe, "step_size", 1 ) );

   if( step->getElementId() != ID_REAL_8 )
   {
      node_error( "Malformed input format for gpsTimeInterval: step_size.",
                  getComm(), p2p );
   }   
   
   const LdasArray< REAL_8 >& st_size( 
      dynamic_cast< const LdasArray< REAL_8 >& >( *step ) );
   
   if( st_size.getNData() != 1 || st_size.getUnits( 0 ) != "sec" || 
       ( ri.timeStepSize = st_size.getData()[ 0 ] ) <= 0.0f )
   {
      node_error( "Malformed input format for gpsTimeInterval: step_size.",
                  getComm(), p2p );
   }      
   
   // Stop time is not a required data anymore.
   size_t pos( 1 );
   ilwdConstVector stop_time( findILwd( pe, "stop_time", pos, false ) );

   if( stop_time.size() )
   {
      if( stop_time.size() != 2 || 
          stop_time[ 0 ]->getName( 0 ) != "gps_sec" || 
          stop_time[ 1 ]->getName( 0 ) != "gps_nan" ||
          stop_time[ 0 ]->getElementId() != ID_INT_4U || 
          stop_time[ 1 ]->getElementId() != ID_INT_4U )
      {
         node_error( "Malformed input format for gpsTimeInterval: stop_time.",
                     getComm(), p2p );
      }

   
      t_sec = dynamic_cast< const LdasArray< INT_4U >* >( stop_time[ 0 ] );
      t_nan = dynamic_cast< const LdasArray< INT_4U >* >( stop_time[ 1 ] );
   
      if( t_sec->getNData() != 1 || t_sec->getUnits( 0 ) != "sec" ||       
          t_nan->getNData() != 1 || t_nan->getUnits( 0 ) != "nanosec" )
      {
         node_error( "Malformed input format for gpsTimeInterval: stop_time.",
                     getComm(), p2p );
      }

   
      ri.stopSec = t_sec->getData()[ 0 ];   
      ri.stopNan = t_nan->getData()[ 0 ];         
   }
   else
   {
      REAL8 all_stop_time( ( static_cast< INT8 >( num_samples ) - 1 ) * 
                           ri.timeStepSize +
                           ( static_cast< INT8 >( ri.startSec ) +
                             1e-9 * static_cast< INT8 >( ri.startNan ) ) );      
      ri.stopSec = static_cast< UINT4 >( all_stop_time );
      ri.stopNan = static_cast< UINT4 >( ( all_stop_time - ri.stopSec ) * 1e9 );
   }
   
   
   ri.numberSamples = num_samples;
   
   // These elements are not required data.
   ri.baseFreq = getREAL8Value( pe, "base_freq", "gpsTimeInterval" );   
   ri.phase = getREAL8Value( pe, "phase", "gpsTimeInterval" );      

   return;
}

   
//------------------------------------------------------------------------------
//
//: initFreqInterval method.
//
// This method initializes Frequency domain interval C data structure. This 
// method is called only by the master node of the communicator.
//   
//!param: const LdasElement& re - A pointer to the ILWD element containing the  
//+       data for structure initialization.
//!param: frequencyInterval& ri - A reference to the frequencyInterval          
//+       structure to be initialized. 
//!param: const UINT4 num_samples - A number of samples in interval. This will be
//+       used only when stopTime/stopFreq is not provided in the inPut data.      
//
//!exc: start_time is missing. - Could not find elements with </i>start_time</i>
//+     name field.
//!exc: start_freq is missing. - Could not find element with <i>start_freq</i> 
//+     field.   
//!exc: step_size is missing. - Could not find element with <i>step_size</i> 
//+     name field.      
//!exc: Malformed input format for frequencyInterval. - Invalid input format    
//+     was specified for frequencyInterval.   
//!exc: bad_alloc - Error allocating the memory.   
//!exc: bad_cast - ILWD input data is malformed.      
//   
void IntervalType::initFreqInterval( const ILwd::LdasElement& re, 
   frequencyInterval& ri, const UINT4 num_samples ) const
{
   const LdasContainer& pe( dynamic_cast< const LdasContainer& >( re ) );      
   ilwdConstVector start_time( findILwd( pe, "start_time" ) );
  
   const bool p2p( isP2P() );
   
   
   if( start_time.size() != 2 || 
       start_time[ 0 ]->getName( 0 ) != "gps_sec" || 
       start_time[ 1 ]->getName( 0 ) != "gps_nan" ||
       start_time[ 0 ]->getElementId() != ID_INT_4U || 
       start_time[ 1 ]->getElementId() != ID_INT_4U )
   {
      node_error( "Malformed input format for frequencyInterval: start_time.",
                  getComm(), p2p );
   }

   
   const LdasArray< INT_4U >* t_sec( dynamic_cast< const LdasArray< INT_4U >* >
      ( start_time[ 0 ] ) );
   
   const LdasArray< INT_4U >* t_nan( dynamic_cast< const LdasArray< INT_4U >* >
      ( start_time[ 1 ] ) );
   
   
   if( t_sec->getNData() != 1 || t_sec->getUnits( 0 ) != "sec" ||
       t_nan->getNData() != 1 || t_nan->getUnits( 0 ) != "nanosec" )
   {
      node_error( "Malformed input format for frequencyInterval: start_time.",
                  getComm(), p2p );
   }

   
   ri.gpsStartTimeSec = t_sec->getData()[ 0 ];   
   ri.gpsStartTimeNan = t_nan->getData()[ 0 ];    

   
   // Stop time is not a required data anymore.
   size_t pos( 1 );   
   ilwdConstVector stop_time( findILwd( pe, "stop_time", pos, false ) );

   if( stop_time.size() )
   {
      if( stop_time.size() != 2 || 
          stop_time[ 0 ]->getName( 0 ) != "gps_sec" || 
          stop_time[ 1 ]->getName( 0 ) != "gps_nan" ||
          stop_time[ 0 ]->getElementId() != ID_INT_4U || 
          stop_time[ 1 ]->getElementId() != ID_INT_4U )
      {
         node_error( "Malformed input format for frequencyInterval: stop_time.",
                     getComm(), p2p );
      }

   
      t_sec = dynamic_cast< const LdasArray< INT_4U >* >( stop_time[ 0 ] );
   
      t_nan = dynamic_cast< const LdasArray< INT_4U >* >( stop_time[ 1 ] );
   
   
      if( t_sec->getNData() != 1 || t_sec->getUnits( 0 ) != "sec" ||       
          t_nan->getNData() != 1 || t_nan->getUnits( 0 ) != "nanosec" )
      {
         node_error( "Malformed input format for frequencyInterval: stop_time.",
                     getComm(), p2p );
      }

   
      ri.gpsStopTimeSec = t_sec->getData()[ 0 ];   
      ri.gpsStopTimeNan = t_nan->getData()[ 0 ];            
   }
   else
   {
      // Since there's no timeStepSize in the data structure,
      // could calculate the stopTime but knowing what frequency is being 
      // used( Niquest,... ).
      // Search algorithm would know, let it calculate stopTime.
      ri.gpsStopTimeSec = 0;   
      ri.gpsStopTimeNan = 0;               
   }
   
   
   const LdasElement* elem( findElement( pe, "start_freq" ) );
   
   
   if( elem->getElementId() != ID_REAL_8 )
   {
      node_error( "Malformed input format for frequencyInterval: start_freq.",
                  getComm(), p2p );
   }   
   
   
   const LdasArray< REAL_8 >& start_freq( 
      dynamic_cast< const LdasArray< REAL_8 >& >( *elem ) );
   
   if( start_freq.getNData() != 1 || start_freq.getUnits( 0 ) != "hz" )
   {
      node_error( "Malformed input format for frequencyInterval: start_freq.",
                  getComm(), p2p );
   }      
   
   
   ri.startFreq = start_freq.getData()[ 0 ];      
   
   
   elem = findElement( pe, "step_size", 1 );
   
   
   if( elem->getElementId() != ID_REAL_8 )
   {
      node_error( "Malformed input format for frequencyInterval: step_size.",
                  getComm(), p2p );
   }   
   
   
   const LdasArray< REAL_8 >& step_size( 
      dynamic_cast< const LdasArray< REAL_8 >& >( *elem ) );
   
   if( step_size.getNData() != 1 || step_size.getUnits( 0 ) != "hz" || 
       ( ri.freqStepSize = step_size.getData()[ 0 ] ) <= 0.0f )
   {
      node_error( "Malformed input format for frequencyInterval: step_size.",
                  getComm(), p2p );
   }      
   
   
   // Stop frequency is not a required data anymore.
   pos = 0;
   ilwdConstVector stop_freq( findILwd( pe, "stop_freq", pos, false ) );

   if( stop_freq.size() )
   {
      if( stop_freq.size() != 1 ||
          stop_freq[ 0 ]->getElementId() != ID_REAL_8 )
      {
         node_error( "Malformed input format for frequencyInterval: stop_freq.",
                     getComm(), p2p );
      }   
   
   
      const LdasArray< REAL_8 >* freq( 
         dynamic_cast< const LdasArray< REAL_8 >* >( stop_freq[ 0 ] ) );
   
      if( freq->getNData() != 1 || freq->getUnits( 0 ) != "hz" )
      {
         node_error( "Malformed input format for frequencyInterval: stop_freq.",
                     getComm(), p2p );
      }      
   
   
      ri.stopFreq = freq->getData()[ 0 ];      
   }
   else
   {
      ri.stopFreq = ( static_cast< INT8 >( num_samples ) - 1 ) * 
                    ri.freqStepSize + ri.startFreq; 
   }
   
   
   ri.numberSamples = num_samples;

   // This element is not required data.
   ri.baseFreq = getREAL8Value( pe, "base_freq", "frequencyInterval" );   

   return;
}

   
//------------------------------------------------------------------------------
//
//: initBothInterval method.
//
// This method initializes Time and Frequency domain interval C data structure. 
// This method is called only by the master node of the communicator.   
//   
//!param: const LdasElement& re - A reference to the ILWD element containing   
//+       the data for structure initialization.
//!param: timeFreqInterval& ri - A reference to the timeFreqInterval structure 
//+       to be initialized. 
//!param: const UINT4 num_samples - A number of samples in interval. This will be
//+       used only when stopTime is not provided in the inPut data.      
//
//!exc: start_time is missing. - Could not find elements with <i>start_time</i>
//+     name field.
//!exc: start_freq is missing. - Could not find element with <i>start_freq</i>
//+     name field.   
//!exc: step_size is missing. - Could not find elements with <i>step_size</i> 
//+     name field.
//!exc: Malformed input format for timeFreqInterval. - Invalid input format    
//+     was specified for timeFreqInterval.
//!exc: Inconsistent number of samples for timeFreqInterval. - Number of time  
//+     and frequency samples is different.      
//!exc: bad_alloc - Error allocating the memory.   
//!exc: bad_cast - ILWD input data is malformed.      
//   
void IntervalType::initBothInterval( const ILwd::LdasElement& re,
   timeFreqInterval& ri, const UINT4 num_samples ) const
{
   const LdasContainer& pe( dynamic_cast< const LdasContainer& >( re ) );      
   
   ilwdConstVector start_time( findILwd( pe, "start_time" ) );
  
   const bool p2p( isP2P() );
   
   
   if( start_time.size() != 2 || 
       start_time[ 0 ]->getName( 0 ) != "gps_sec" || 
       start_time[ 1 ]->getName( 0 ) != "gps_nan" ||
       start_time[ 0 ]->getElementId() != ID_INT_4U || 
       start_time[ 1 ]->getElementId() != ID_INT_4U )
   {
      node_error( "Malformed input format for timeFreqInterval: start_time",
                  getComm(), p2p );
   }

   
   const LdasArray< INT_4U >* t_sec( dynamic_cast< const LdasArray< INT_4U >* >
      ( start_time[ 0 ] ) );
   
   const LdasArray< INT_4U >* t_nan( dynamic_cast< const LdasArray< INT_4U >* >
      ( start_time[ 1 ] ) );
   
   if( t_sec->getNData() != 1 || t_sec->getUnits( 0 ) != "sec" ||       
       t_nan->getNData() != 1 || t_nan->getUnits( 0 ) != "nanosec" )
   {
      node_error( "Malformed input format for timeFreqInterval: start_time.",
                  getComm(), p2p );
   }

   
   ri.gpsStartTimeSec = t_sec->getData()[ 0 ];   
   ri.gpsStartTimeNan = t_nan->getData()[ 0 ];         
   

   ilwdConstVector step_size( findILwd( pe, "step_size" ) );
   
   
   if( step_size.size() != 2 || 
       step_size[ 0 ]->getName( 0 ) != "time" || 
       step_size[ 1 ]->getName( 0 ) != "freq" ||
       step_size[ 0 ]->getElementId() != ID_REAL_8 || 
       step_size[ 1 ]->getElementId() != ID_REAL_8 )
   {
      node_error( "Malformed input format for timeFreqInterval: step_size.",
                  getComm(), p2p );
   }
   
   
   const LdasArray< REAL_8 >* t_step( dynamic_cast< const LdasArray< REAL_8 >* >
      ( step_size[ 0 ] ) );
   
   const LdasArray< REAL_8 >* f_step( dynamic_cast< const LdasArray< REAL_8 >* >
      ( step_size[ 1 ] ) );
   
   if( t_step->getNData( ) != 1 || t_step->getUnits( 0 ) != "sec" ||       
       ( ri.timeStepSize = t_step->getData()[ 0 ] ) <= 0.0f || 
       f_step->getNData() != 1 || f_step->getUnits( 0 ) != "hz" ||
       ( ri.freqStepSize = f_step->getData()[ 0 ] ) <= 0.0f )
   {
      node_error( "Malformed input format for timeFreqInterval: step_size.",
                  getComm(), p2p );
   } 
   
   // Stop time is not a required data anymore
   size_t pos( 1 );
   ilwdConstVector stop_time( findILwd( pe, "stop_time", pos, false ) );

   if( stop_time.size() )
   {
      if( stop_time.size() != 2 || 
          stop_time[ 0 ]->getName( 0 ) != "gps_sec" || 
          stop_time[ 1 ]->getName( 0 ) != "gps_nan" ||
          stop_time[ 0 ]->getElementId() != ID_INT_4U || 
          stop_time[ 1 ]->getElementId() != ID_INT_4U )
      {
         node_error( "Malformed input format for timeFreqInterval: stop_time.",
                     getComm(), p2p );
      }

   
      t_sec = dynamic_cast< const LdasArray< INT_4U >* >( stop_time[ 0 ] );
   
      t_nan = dynamic_cast< const LdasArray< INT_4U >* >( stop_time[ 1 ] );
   
   
      if( t_sec->getNData() != 1 || t_sec->getUnits( 0 ) != "sec" ||       
          t_nan->getNData() != 1 || t_nan->getUnits( 0 ) != "nanosec" )
      {
         node_error( "Malformed input format for timeFreqInterval: stop_time.",
                     getComm(), p2p );
      }

   
      ri.gpsStopTimeSec = t_sec->getData()[ 0 ];   
      ri.gpsStopTimeNan = t_nan->getData()[ 0 ];         
   }
   else
   {
      REAL8 all_stop_time( ( static_cast< INT8 >( num_samples ) - 1 ) *
                           ri.timeStepSize +
                           ( static_cast< INT8 >( ri.gpsStartTimeSec ) +
                             1e-9 * static_cast< INT8 >( ri.gpsStartTimeNan ) ) );      
      ri.gpsStopTimeSec = static_cast< UINT4 >( all_stop_time );
      ri.gpsStopTimeNan = static_cast< UINT4 >( 
                             ( all_stop_time - ri.gpsStopTimeSec ) * 1e9 );   
   }

   
   const LdasElement* elem( findElement( pe, "start_freq" ) );
   
   
   if( elem->getElementId() != ID_REAL_8 )
   {
      node_error( "Malformed input format for timeFreqInterval: start_freq.",
                  getComm(), p2p );
   }   
   
   
   const LdasArray< REAL_8 >& start_freq( 
      dynamic_cast< const LdasArray< REAL_8 >& >( *elem ) );
   
   if( start_freq.getNData() != 1 || start_freq.getUnits( 0 ) != "hz" )
   {
      node_error( "Malformed input format for timeFreqInterval: start_freq.",
                  getComm(), p2p );
   }      
   
   
   ri.startFreq = start_freq.getData()[ 0 ];      
   
   
   // Stop frequency is not a required data anymore.
   pos = 0;
   ilwdConstVector stop_freq( findILwd( pe, "stop_freq", pos, false ) );

   if( stop_freq.size() )
   {
      if( stop_freq.size() != 1 || 
          stop_freq[ 0 ]->getElementId() != ID_REAL_8 )
      {
         node_error( "Malformed input format for timeFreqInterval: stop_freq.",
                     getComm(), p2p );
      }   
   
   
      const LdasArray< REAL_8 >* freq( 
         dynamic_cast< const LdasArray< REAL_8 >* >( stop_freq[ 0 ] ) );
   
      if( freq->getNData() != 1 || freq->getUnits( 0 ) != "hz" )
      {
         node_error( "Malformed input format for timeFreqInterval: stop_freq.",
                     getComm(), p2p );
      }      
   
   
      ri.stopFreq = freq->getData()[ 0 ];      
   }
   else
   {
      ri.stopFreq = static_cast< INT8 >( num_samples - 1 ) * ri.freqStepSize +
                    ri.startFreq;    
   }

   
   // Calculate number of time samples in the interval:
   ri.numberSamples = num_samples;
   
   // Calculate number of frequency samples in the interval:
   UINT8 freqSamples = static_cast< UINT8 >( 
      ( ri.stopFreq - ri.startFreq ) / ri.freqStepSize ) + 1;
   
   
   if( ri.numberSamples != freqSamples )
   {
      node_error( "Inconsistent number of samples for "
                  "timeFreqInterval.", getComm(), p2p );
   }

   return;
}


//------------------------------------------------------------------------------
//
//: initDatabaseInterval method.
//
// This method initializes Database domain interval C data structure. 
// This method is called only by the master node of the communicator.   
//   
//!param: databaseInterval& ri - A reference to the databaeInterval structure 
//+       to be initialized. 
//!param: const UINT4 num_rows - A number of rows in the database query
//!param: const std::string& sql - SQL query used to get database data.   
//
//!return: Nothing.
//   
void IntervalType::initDatabaseInterval( databaseInterval& ri,
   const UINT4 num_rows, const std::string& sql )
{
   ri.numberRows = num_rows;
   
   size_t size( sql.size() );
   if( size )
   {
      if( size >= maxSQL )
      {
         size = maxSQL - 1;
      }

      std::copy( sql.begin(), sql.begin() + size, ri.sqlQuery );   
   }
   
   
   return;   
}
   

//------------------------------------------------------------------------------
//
//: Converts Time domain interval to the ILWD format element. 
//       
//!param: const gpsTimeInterval& re - A reference to the Time domain interval 
//+       data structure.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined.    
//   
//!return: ilwdVector - A vector of ILWD format elements representing the
//+        interval structure.
//
//!exc: bad_alloc - Memory allocation failed.   
//   
LdasDatatype::ilwdResultVector IntervalType::timeToILwd( const gpsTimeInterval& re,
   const MPI::Intracomm& comm )
{
   static const size_t numDims( 1 );
   ilwdResultVector result;

   try
   {
      // Start time   
      result.push_back(  new LdasArray< INT_4U >( &re.startSec, numDims,
                                   "gps_sec:start_time", "sec" ) );

      result.push_back( new LdasArray< INT_4U >( &re.startNan, numDims,
                               "gps_nan:start_time", "nanosec" ) );

      // Stop time
      result.push_back( new LdasArray< INT_4U >( &re.stopSec, numDims,
                               "gps_sec:stop_time", "sec" ) );   
   
      result.push_back( new LdasArray< INT_4U >( &re.stopNan, numDims,
                               "gps_nan:stop_time", "nanosec" ) );
   
      // Step size   
      result.push_back( new LdasArray< REAL_8 >( &re.timeStepSize, numDims,
                               "time:step_size", "sec" ) );
   
      // Base frequency
      result.push_back( new LdasArray< REAL_8 >( &re.baseFreq, numDims, "base_freq" ) );
   
      // Phase
      result.push_back( new LdasArray< REAL_8 >( &re.phase, numDims, "phase" ) );   
   }
   catch(...)
   {
      vectorCleanup( result );
      throw;
   }

   
   return result;
}
   

//------------------------------------------------------------------------------
//
//: Converts Frequency domain interval to the ILWD format element. 
//       
//!param: const frequencyInterval& re - A reference to the Frequency domain interval 
//+       data structure.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined.    
//   
//!return: ilwdVector - A vector of ILWD format elements representing the
//+        interval structure.
//
//!exc: bad_alloc - Memory allocation failed.   
//      
LdasDatatype::ilwdResultVector IntervalType::freqToILwd( const frequencyInterval& re,
   const MPI::Intracomm& comm )
{
   static const size_t numDims( 1 );   
   ilwdResultVector result;
   
   try
   {
      // Start time
      result.push_back( new LdasArray< INT_4U >( &re.gpsStartTimeSec,
                               numDims, "gps_sec:start_time", "sec" ) );

      result.push_back( new LdasArray< INT_4U >( &re.gpsStartTimeNan,
                               numDims, "gps_nan:start_time", "nanosec" ) );

      // Stop time
      result.push_back( new LdasArray< INT_4U >( &re.gpsStopTimeSec, 
                               numDims, "gps_sec:stop_time", "sec" ) );   
   
      result.push_back( new LdasArray< INT_4U >( &re.gpsStopTimeNan,
                               numDims, "gps_nan:stop_time", "nanosec" ) );
    
      // Start frequency
      result.push_back( new LdasArray< REAL_8 >( &re.startFreq, numDims,
                               "start_freq", "hz" ) );
   
      // Stop frequency
      result.push_back( new LdasArray< REAL_8 >( &re.stopFreq, numDims,
                               "stop_freq", "hz" ) );
   
      // Frequency step size
      result.push_back( new LdasArray< REAL_8 >( &re.freqStepSize, numDims,
                               "freq:step_size", "hz" ) );
   
      // Base frequency
      result.push_back( new LdasArray< REAL_8 >( &re.baseFreq, numDims, "base_freq" ) );
   }
   catch(...)
   {
      vectorCleanup( result );
      throw;
   }
   
   
   return result;
}
   

//------------------------------------------------------------------------------
//
//: Converts Both domain interval to the ILWD format element. 
//       
//!param: const timeFreqInterval& re - A reference to the Both domain interval 
//+       data structure.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined.    
//   
//!return: ilwdVector - A vector of ILWD format elements representing the
//+        interval structure.
//
//!exc: bad_alloc - Memory allocation failed.   
//         
LdasDatatype::ilwdResultVector IntervalType::bothToILwd( const timeFreqInterval& re,
   const MPI::Intracomm& comm )
{
   static const size_t numDims( 1 );   
   ilwdResultVector result;

   try
   {
      // Start time
      result.push_back( new LdasArray< INT_4U >( &re.gpsStartTimeSec,
                               numDims, "gps_sec:start_time", "sec" ) );
   
      result.push_back( new LdasArray< INT_4U >( &re.gpsStartTimeNan,
                               numDims, "gps_nan:start_time", "nanosec" ) );

      // Stop time
      result.push_back( new LdasArray< INT_4U >( &re.gpsStopTimeSec,
                               numDims, "gps_sec:stop_time", "sec" ) );   
   
      result.push_back( new LdasArray< INT_4U >( &re.gpsStopTimeNan,
                               numDims, "gps_nan:stop_time", "nanosec" ) );

      // Time step size
      result.push_back( new LdasArray< REAL_8 >( &re.timeStepSize, numDims,
                               "time:step_size", "sec" ) );
   
      // Start frequency
      result.push_back( new LdasArray< REAL_8 >( &re.startFreq, numDims,
                               "start_freq", "hz" ) );
   
      // Stop frequency
      result.push_back( new LdasArray< REAL_8 >( &re.stopFreq, numDims,
                               "stop_freq", "hz" ) );

      // Frequency step size
      result.push_back( new LdasArray< REAL_8 >( &re.freqStepSize, numDims,
                               "freq:step_size", "hz" ) );
   }
   catch(...)
   {
      vectorCleanup( result );
      throw;
   }
   
   return result;
}


//------------------------------------------------------------------------------
//
//: Gets value of specified ILWD REAL8 element.
//      
// This method finds ILWD format array of <b>REAL8</b> type in the passed to it
// ILWD format container and extracts its value.   
//    
//!param: const LdasContainer& pe - A reference to the ILWD format container.   
//!param: const CHAR* name - Name key of the element.   
//!param: const CHAR* domain_name - Domain name.   
//
//!return: REAL8 - Value of the element.   
//
//!exc: Malformed input format for <b>domain_name</b>: <b>name</b>. - Specified
//+     ILWD format element is malformed.   
//!exc: bad_alloc - Memory allocation failed.   
//    
const REAL8 IntervalType::getREAL8Value( const ILwd::LdasContainer& pe, 
   const CHAR* name, const CHAR* domain_name ) const
{   
   // element is not a required data.
   static const size_t pos( 0 );   
   static const bool check_empty( false );
   ilwdConstVector result( findILwd( pe, name, pos, check_empty ) );

   if( result.size() )
   {
      if( result.size() != 1 || 
          result[ 0 ]->getElementId() != ID_REAL_8 )
      {
         string msg( "Malformed input format for " );
         msg += domain_name;
         msg += ": ";
         msg += name;
         node_error( msg.c_str(), getComm(), isP2P() );
      }

      const LdasArray< REAL_8 >* elem( 
         dynamic_cast< const LdasArray< REAL_8 >* >( result[ 0 ] ) );
   
      if( elem->getNData() != 1 )
      {
         string msg( "Malformed input format for " );
         msg += domain_name;
         msg += ": ";
         msg += name;
         node_error( msg.c_str(), getComm(), isP2P() );
      }      
   
   
      return elem->getData()[ 0 ];         
   }

   
   return 0.0f;   
}

   
//------------------------------------------------------------------------------
//
//: Allocates memory for the structure variables.
//      
// This method allocates memory for the channel names if any.
// This method has to be called by all nodes in the
// communicator that will use the union for communication.
//    
//!param: interval& ri - Interval union.
//
//!exc: Undefined domain. - Undefined domain was specified.                     
//+     One of TIME | FREQ | BOTH | DATABASE. 
//!exc: bad_alloc - Memory allocation failed.   
//    
void IntervalType::allocateData( interval& ri )
{
   switch( mDomain )
   {
      case(noneD):
      {
         ri.dNone.numberChannels  = mNumberChannels;
         ri.dNone.numberDetectors = mNumberDetectors;   
         if( mNumberChannels )
         {
            ri.dNone.channelName = new CHAR*[ mNumberChannels ];
            memset( ri.dNone.channelName, 0 , sizeof( CHAR* ) * mNumberChannels );
            ri.dNone.channelName[ 0 ] = new CHAR[ mChannelSize ];
         }
   
         break;
      }   
      case(timeD):
      {
         ri.dTime.numberChannels  = mNumberChannels;
         ri.dTime.numberDetectors = mNumberDetectors;   
         if( mNumberChannels )
         {
            ri.dTime.channelName = new CHAR*[ mNumberChannels ];
            memset( ri.dTime.channelName, 0 , sizeof( CHAR* ) * mNumberChannels );
            ri.dTime.channelName[ 0 ] = new CHAR[ mChannelSize ];
         }
   
         break;
      }
      case(freqD):
      {
         ri.dFreq.numberChannels  = mNumberChannels;
         ri.dFreq.numberDetectors = mNumberDetectors;      
         if( mNumberChannels )
         {
            ri.dFreq.channelName = new CHAR*[ mNumberChannels ];
            memset( ri.dFreq.channelName, 0 , sizeof( CHAR* ) * mNumberChannels );
            ri.dFreq.channelName[ 0 ] = new CHAR[ mChannelSize ];
         }
   
         break;
      }   
      case(bothD):
      {
         ri.dBoth.numberChannels  = mNumberChannels;
         ri.dBoth.numberDetectors = mNumberDetectors;      
         if( mNumberChannels )
         {
            ri.dBoth.channelName = new CHAR*[ mNumberChannels ];
            memset( ri.dBoth.channelName, 0 , sizeof( CHAR* ) * mNumberChannels );
            ri.dBoth.channelName[ 0 ] = new CHAR[ mChannelSize ];
         }
   
         break;
      }   
      case(databaseD):
      {
         break;
      }
      default:   
      {
         node_error( "Undefined domain.", getComm(), isP2P() );
      }

   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: buildType method.
//      
// This method creates MPI derived datatype which represents C interval 
// union passed to it. This method has to be called by all nodes in the
// communicator that will use the union for communication.
//    
//!param: interval& ri - Interval union the datatype is generated for.
//
//!exc: Error generating IntervalType. - Error generating MPI datatype to      
//+     represent C interval union.
//!exc: Undefined domain. - Undefined domain was specified.                     
//+     One of TIME | FREQ | BOTH | DATABASE. 
//!exc: MPI::Exception - MPI library exception.           
//!exc: bad_alloc - Memory allocation failed.   
//    
void IntervalType::buildType( interval& ri )
{
   if( mDomain == databaseD )
   {
      const INT4 num( 2 );

      vector< MPI::Aint > disps( num );
      disps[ 0 ] = MPI::Get_address( &ri.dDatabase.numberRows );
      disps[ 1 ] = MPI::Get_address( ri.dDatabase.sqlQuery );

      vector< MPI::Datatype > types( num );
      types[ 0 ] = LDAS_MPI_UINT8;
      types[ 1 ] = LDAS_MPI_CHAR;

      vector< INT4 > blocks( num );
      blocks[ 0 ] = 1;
      blocks[ 1 ] = maxSQL;
   
      return createType( num, blocks, disps, types );
   }
   
   
   vector< MPI::Aint > disps( mTypeNum );
   
   // Don't fill out the very last array's element: 
   // if mNumberChannels is zero and mNumberDetectors is not, 
   // then 4th array elements will have detector information
   vector< MPI::Datatype > types( mTypeNum );

   // You really need to know mTypeNum value before you change
   // these assignments!
   types[ 0 ] = LDAS_MPI_UINT8;
   types[ 1 ] = LDAS_MPI_UINT4;
   types[ 2 ] = LDAS_MPI_REAL8;
   types[ 3 ] = LDAS_MPI_CHAR;
   types[ 4 ] = MPI::DATATYPE_NULL;


   vector< INT4 > blocks( mTypeNum );

   // You really need to know mTypeNum value before you change
   // these assignments!
   blocks[ 0 ] = 1;
   blocks[ 1 ] = 4;
   blocks[ 2 ] = 4;
   blocks[ 3 ] = mChannelSize;
   blocks[ 4 ] = 0;
   
   // At least 3 datatypes: 0 .. 2
   INT4 i_num( 2 );

   // A bit evil: make sure i_num never exceeds (mTypeNum - 1)   
   switch( mDomain )
   {
      case(noneD):
      {
         i_num = 0;
         disps[ i_num ] = MPI::Get_address( &ri.dNone.numberSamples );
   
         if( mNumberChannels )
         {
            ++i_num;
            types[ i_num ] = LDAS_MPI_CHAR;
            blocks[ i_num ] = mChannelSize;
            disps[ i_num ] = MPI::Get_address( ri.dNone.channelName[ 0 ] );   
         }
   
         if( mNumberDetectors )
         {
            ++i_num;
            types[ i_num ]  = DetectorType::getMpiType();
            blocks[ i_num ] = mNumberDetectors;      
            disps[ i_num ]  = MPI::Get_address( ri.dNone.geometry->name );   
         }   
   
         break;
      }   
      case(timeD):
      {
         disps[ 0 ] = MPI::Get_address( &ri.dTime.numberSamples );
         disps[ 1 ] = MPI::Get_address( &ri.dTime.startSec );   
         disps[ 2 ] = MPI::Get_address( &ri.dTime.timeStepSize );   
   
         blocks[ 2 ] = 3;   
   
         if( mNumberChannels )
         {
            ++i_num;
            disps[ i_num ] = MPI::Get_address( ri.dTime.channelName[ 0 ] );   
         }
   
         if( mNumberDetectors )
         {
            ++i_num;
            types[ i_num ]  = DetectorType::getMpiType();
            blocks[ i_num ] = mNumberDetectors;      
            disps[ i_num ]  = MPI::Get_address( ri.dTime.geometry->name );   
         }   
   
         break;
      }
      case(freqD):
      {
         disps[ 0 ] = MPI::Get_address( &ri.dFreq.numberSamples );
         disps[ 1 ] = MPI::Get_address( &ri.dFreq.gpsStartTimeSec );   
         disps[ 2 ] = MPI::Get_address( &ri.dFreq.startFreq );      

         if( mNumberChannels )
         {  
            ++i_num;
            disps[ i_num ] = MPI::Get_address( ri.dFreq.channelName[ 0 ] );   
         }

         if( mNumberDetectors )
         {
            ++i_num;
            types[ i_num ]  = DetectorType::getMpiType();
            blocks[ i_num ] = mNumberDetectors;         
            disps[ i_num ]  = MPI::Get_address( ri.dFreq.geometry->name );   
         }   
   
         break;
      }
      case(bothD):
      {
         disps[ 0 ] = MPI::Get_address( &ri.dBoth.numberSamples );
         disps[ 1 ] = MPI::Get_address( &ri.dBoth.gpsStartTimeSec );   
         disps[ 2 ] = MPI::Get_address( &ri.dBoth.startFreq );   

         if( mNumberChannels )
         {
            ++i_num;
            disps[ i_num ] = MPI::Get_address( ri.dBoth.channelName[ 0 ] );   
         }
   
         if( mNumberDetectors )
         {
            ++i_num;
            types[ i_num ]  = DetectorType::getMpiType();
            blocks[ i_num ] = mNumberDetectors;         
            disps[ i_num ]  = MPI::Get_address( ri.dBoth.geometry->name );   
         }   
   
         break;
      }
      default:
      {
         node_error( "Undefined domain.", getComm(), isP2P() );
         break;
      }
   }

   ++i_num;
   
   return createType( i_num, blocks, disps, types );
}


//------------------------------------------------------------------------------
//
//: Gets name of the derived datatype.
//         
//!return: const CHAR* - Datatype name.
//   
const CHAR* IntervalType::getDatatypeName() const
{
   return "IntervalType";
}
   

//------------------------------------------------------------------------------
//
//: Allocates the memory and copies the data for the domain interval
//: data structure.
//
//!param: const domain& d - An enum type specifying the domain.      
//!param: interval& pd - A reference to the destination interval structure
//+       being initialized.
//!param: const interval& ps - A reference to the source interval structure.
//!param: const Intracomm& comm - A reference to the intracommunicator within 
//+       which communication occures.
//!param: const bool p2p - Flag to indicate that method is called within p2p 
//+       communication.   
//
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Undefined domain. - Undefined domain was specified for the C interval   
//+     union.   
//   
void storeInterval( const domain& d, interval& pd, const interval& ps,
   const MPI::Intracomm& comm, const bool p2p )
{
   CHAR*** channelName( 0 );
   UINT4  numberChannels( 0 );
   const CHAR* s_channelName( 0 ); 
   
   switch( d )
   {
      case(noneD):
      {
         memcpy( &( pd.dNone ), &( ps.dNone ), sizeof( rawSequence ) );   
         storeDetector( &( pd.dNone.geometry ), ps.dNone.geometry,
                        pd.dNone.numberDetectors, comm, p2p );
   
         numberChannels = pd.dNone.numberChannels;
         // If there're any channel names
         if( numberChannels )
         {
            channelName  = &( pd.dNone.channelName );
            s_channelName = ps.dNone.channelName[ 0 ];
         }
   
         break;
      }   
      case(timeD):
      {
         memcpy( &( pd.dTime ), &( ps.dTime ), sizeof( gpsTimeInterval ) );   
         storeDetector( &( pd.dTime.geometry ), ps.dTime.geometry,
                        pd.dTime.numberDetectors, comm, p2p );
   
         numberChannels = pd.dTime.numberChannels;
         // If there're any channel names
         if( numberChannels )
         {
            channelName  = &( pd.dTime.channelName );
            s_channelName = ps.dTime.channelName[ 0 ];
         }
   
         break;
      }
      case(freqD):
      {
         memcpy( &( pd.dFreq ), &( ps.dFreq ), sizeof( frequencyInterval ) );
         storeDetector( &( pd.dFreq.geometry ), ps.dFreq.geometry,
                        pd.dFreq.numberDetectors, comm, p2p );
   
         numberChannels = pd.dFreq.numberChannels;
         // If there're any channel names
         if( numberChannels )
         {   
            channelName  = &( pd.dFreq.channelName );   
            s_channelName = ps.dFreq.channelName[ 0 ];   
         }
   
         break;
      }   
      case(bothD):
      {
         memcpy( &( pd.dBoth ), &( ps.dBoth ), sizeof( timeFreqInterval ) );
         storeDetector( &( pd.dBoth.geometry ), ps.dBoth.geometry,
                        pd.dBoth.numberDetectors, comm, p2p );
   
         numberChannels = pd.dBoth.numberChannels;   
         // If there're any channel names
         if( numberChannels )
         {      
            channelName  = &( pd.dBoth.channelName );   
            s_channelName = ps.dFreq.channelName[ 0 ];      
         }
         break;
      }   
      case(databaseD):
      {
         memcpy( &( pd.dDatabase ), &( ps.dDatabase ), 
                 sizeof( databaseInterval ) );
         break;
      }      
      default:   
      {
         node_error( "Undefined domain.", comm, p2p );
      }      
   }

   // All domains except databaseInterval
   if( numberChannels )
   {
      // Allocate array for pointers to channels
      *channelName = new CHAR*[ numberChannels ];   
      memset( *channelName, 0, sizeof( CHAR* ) * numberChannels );
   
      // Array of channel offsets into the buffer
      UINT4* channel_len = new UINT4[ numberChannels ];
      memset( channel_len, 0, sizeof( UINT4 ) * numberChannels );
      // channel_len[ 0 ] is always '0'
      UINT4 channelSize( strlen( s_channelName ) + 1 ); 
   
      // Get total size of source channels and position of each channel
      // into the buffer
      UINT4* channel( channel_len );
      for( UINT4 index = 1; index < numberChannels; ++index )
      {
         ++channel;   
         *channel = channelSize;
         channelSize += strlen( &( s_channelName[ *channel ] ) ) + 1;
      }
   
      try
      {
         // Allocate the memory for the whole buffer
         ( *channelName )[ 0 ] = new CHAR[ channelSize ];
         memcpy( ( *channelName )[ 0 ], s_channelName,
                 sizeof( CHAR ) * channelSize );
         channel = channel_len;
      
#ifdef DEBUG_CHANNEL_NAME   
         cout << jobID << ": Channel( slave )[0]: " << ( *channelName )[ 0 ] << endl;
#endif      
         // Set pointers to channels( 0 channel points to the buffer itself ),
         for( UINT4 index = 1; index < numberChannels; ++index )
         {
            ++channel;
            ( *channelName )[ index ] = ( *channelName )[ 0 ] + ( *channel );
   
#ifdef DEBUG_CHANNEL_NAME   
            cout << jobID << ": Channel( slave )[" << index << "]: " << ( *channelName )[ index ] << endl;
#endif   
         }
      }
      catch(...)
      {
         delete[] channel_len;
         channel_len = 0;
   
         delete[] channelName;         
         channelName = 0;
   
         throw;
      }
   
      delete[] channel_len;
      channel_len = 0;
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Frees dynamically allocated memory for the domain interval
//: data structure.
//
//!param: const domain& d - An enum type specifying the domain.      
//!param: interval& pd - A reference to the interval structure.
//!param: const MPI::Intracomm& comm - A reference to the intracommunicator 
//+       within which communication occures.
//!param: const bool p2p - Flag to indicate that method is called within p2p 
//+       communication. Default is true.   
//
//!return: Nothing.
//
//!exc: Undefined domain. - Undefined domain was specified for the C interval   
//+     union.   
//      
void deleteInterval( const domain& d, interval& pd, 
   const MPI::Intracomm& comm, const bool p2p )
{
   switch( d )
   {
      case(noneD):
      {
         if( pd.dNone.numberChannels )
         {
            delete[] pd.dNone.channelName[ 0 ];
            pd.dNone.channelName[ 0 ] = 0;
   
            delete[] pd.dNone.channelName;
            pd.dNone.channelName = 0;   
         }

         delete[] pd.dNone.geometry;
         pd.dNone.geometry = 0;
   
         break;
      }   
      case(timeD):
      {
         if( pd.dTime.numberChannels )
         {
            delete[] pd.dTime.channelName[ 0 ];
            pd.dTime.channelName[ 0 ] = 0;
   
            delete[] pd.dTime.channelName;
            pd.dTime.channelName = 0;   
         }

         delete[] pd.dTime.geometry;
         pd.dTime.geometry = 0;
   
         break;
      }
      case(freqD):
      {
         if( pd.dFreq.numberChannels )
         {   
            delete[] pd.dFreq.channelName[ 0 ];
            pd.dFreq.channelName[ 0 ] = 0;
   
            delete[] pd.dFreq.channelName;
            pd.dFreq.channelName = 0;   
         }
   
         delete[] pd.dFreq.geometry;
         pd.dFreq.geometry = 0;
   
         break;
      }   
      case(bothD):
      {
         if( pd.dBoth.numberChannels )
         {   
            delete[] pd.dBoth.channelName[ 0 ];
            pd.dBoth.channelName[ 0 ] = 0;
   
            delete[] pd.dBoth.channelName;
            pd.dBoth.channelName = 0;   
         }
   
         delete[] pd.dBoth.geometry;
         pd.dBoth.geometry = 0;
   
         break;
      }   
      case(databaseD):
      {
         break;
      }      
      default:   
      {
         node_error( "Undefined domain.", comm, p2p );
      }      
   }

   return;
}
