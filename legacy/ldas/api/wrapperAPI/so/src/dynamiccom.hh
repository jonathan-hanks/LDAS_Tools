#ifndef WrapperApiDynamicIntracomHH
#define WrapperApiDynamicIntracomHH

#include "LDASConfig.h"

// System Header Files   
#include <iostream>
#include <vector>
   
// MPI Header Files   
#if HAVE_MPI___H
#include <mpi++.h>
#elif HAVE_MPICXX_H
#include <mpicxx.h>
#endif
   
// Local Header Files
#include "LALAtomicDatatypes.h"

   
//------------------------------------------------------------------------------
//
//: DynamicIntracom class.
// 
// This class represents dynamic intracommunicator.  
// The object of this class is constructed by specifying processes from 
// the communicator domain within which new communicator will be created.
//
class DynamicIntracom : public MPI::Intracomm
{

public:

   //
   //: Default constructor.
   //   
   // This constructor generates MPI::COMM_NULL handle.
   //
   DynamicIntracom() : MPI::Intracomm(){}
   

   DynamicIntracom( const std::vector< INT4 >& ranks,
      const MPI::Intracomm& comm = MPI::COMM_WORLD );

   //
   //: Copy constructor.
   //
   // This copy constructor is handle-based( shallow ) copy.
   //
   //!param: const MPI::Intracom& c - A reference to the 
   //+       intracommunicator to copy.
   //
   //!exc: MPI::Exception - MPI library exception.
   //
   DynamicIntracom( const MPI::Intracomm& c ) 
      : MPI::Intracomm( c ){}

   /* Assignment operator */
   DynamicIntracom& operator=( const MPI::Intracomm& c );
   
   /* Equal comparison operators */
   bool operator==( const MPI::Intracomm& c ) const; 
   bool operator!=( const MPI::Intracomm& c ) const;

   /* Destructor */
  virtual ~DynamicIntracom();

private:


};

   
#endif


