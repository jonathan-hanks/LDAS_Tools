#include "LDASConfig.h"

// System Header Files
#include <new>
#include <sstream>   
#include <fstream>   
#include <iomanip>   
#include <sys/types.h>
#include <sys/stat.h> 
#include <sys/time.h>   
#include <unistd.h>   
#include <cerrno>   
#include <cstring>   
#include <memory>   

// General Header Files
#include <general/formatexception.hh>      
   
// ILWD Header Files
#include <ilwd/ldascontainer.hh>      
#include <ilwd/ldasstring.hh>   
#include <ilwd/util.hh>      
#include <ilwd/reader.hh>   
   
// GenericAPI Header Files
#include <genericAPI/socketcmd.hh>   
#include <genericAPI/elementcmd.hh>   
#include <genericAPI/ilwdfile.hh>
#include "genericAPI/OSpaceException.hh"        
   
// Ospace Header Files   
#include <ospace/socket/tcpsrvr.h>   

using ILwd::LdasArrayBase;   
using ILwd::LdasString;   
   
// Local Header Files
#include "ldaserror.hh"   
#include "inputdatatype.hh"
#include "multidimdata.hh"   
#include "statevectype.hh"   
#include "ldasdata.hh"   
#include "mpilaltypes.hh"   
#include "mpidatautil.hh"   
#include "ldastag.hh"   
#include "initvars.hh"   
#include "intervaltype.hh"   
#include "detectortype.hh"   
#include "msgsocket.hh"   
#include "initargs.hh"   

using namespace std;   
   
using ILwd::LdasElement;
using ILwd::LdasContainer;   
using ILwd::ID_ILWD;
using ILwd::ID_LSTRING;   
using ILwd::Format;
using ILwd::Compression;      

   
//------------------------------------------------------------------------------
//
//: Initializes domainHash unordered_map.
//
// This method initializes the domain unordered_map, where a key is of a string type
// and value is a corresponding enum type.
//   
//!return: const domainHash - unordered_map.
//
//!exc: bad_alloc - Memory allocation failed.
//   
const InputType::domainHash InputType::initDomainHash()
{
   domainHash h;
   
   h.insert( domainValueType( string( NoneDomainString ), noneD ) );   
   h.insert( domainValueType( string( TimeDomainString ), timeD ) );
   h.insert( domainValueType( string( FreqDomainString ), freqD ) ); 
   h.insert( domainValueType( string( BothDomainString ), bothD ) );
   h.insert( domainValueType( string( DatabaseDomainString ), databaseD ) );   
   
   return h;
}


const InputType::domainHash InputType::mDomainHash( initDomainHash() );
const UINT4 InputType::mTypeNum( 2 );    
REAL8 InputType::mCreateTime( 0.0f );    
REAL8 InputType::mReceiveILwdTime( 0.0f );   
   

//------------------------------------------------------------------------------
//
//: Constructor.
//   
// This constructor generates a MPI derived datatype to represent C inPut data
// structure. All nodes in the COMM_WORLD must create this datatype to use it 
// for data broadcast by master to all slaves in the communicator if data 
// distributor is wrapperAPI. If search master distributes the input data, only
// wrapperAPI master and search master need to construct this datatype.
//
//!param: const INT4 rank - A node rank to send data information to. Default   
//+       is -1.
//!param: const MPI::Intracomm& comm - A reference to the communicator within  
//+       which input data gets sent. Default is MPI::COMM_WORLD.   
//      
//!exc: Malformed ILWD input. - Input ILWD data is malformed: either           
//+     element is NULL, or it is not an ILWD container or container is empty.
//!exc: Memory allocation failed. - Error allocating memory.
//   
//!exc: sequence is missing. - Sequence data is missing for inPut data structure.      
//!exc: data is missing - ILWD format element representing the data is missing.   
//!exc: domain is missing - ILWD format element representing the domain is    
//+     missing.      
//!exc: Undefined domain. - Undefined domain was specified.                    
//+     One of TIME | FREQ | BOTH | DATABASE.      
//!exc: Undefined datatype. - Undefined datatype for ILWD element              
//+     representing the data was specified.   
//!exc: Invalid boolean datatype. - ILwd element representing boolean data     
//+     must be of char_u type.   
//!exc: Index exceeds the size of packing buffer. - The size of the packing    
//+     buffer is smaller than the data being packed into it. listMaxNodes 
//+     and multiDimDataMaxDims control the size of the buffer.   
//
//!exc: start_time is missing. - Could not find elements with <i>start_time</i>
//+     name field.
//!exc: step_size is missing. - Could not find element with <i>step_size</i>    
//+     name field.   
//!exc: Malformed input format for gpsTimeInterval. - Invalid input format was
//+     specified for gpsTimeInterval.   
//!exc: start_freq is missing. - Could not find element with <i>start_freq</i>
//+     name field.   
//!exc: Malformed input format for frequencyInterval. - Invalid input format  
//+     was specified for frequencyInterval.      
//!exc: Malformed input format for timeFreqInterval. - Invalid input format    
//+     was specified for timeFreqInterval.
//!exc: Inconsistent number of samples for timeFreqInterval. - Number of time  
//+     and frequency samples is different.            
//!exc: Invalid number of history nodes specified. - Negative number of nodes  
//+     was specified.         
//   
//!exc: Error generating InputType. - Error generating MPI datatype to         
//+     represent C inPut data structure.      
//!exc: Error generating HistoryType. - Error generating MPI derived datatype  
//+     to represent C dcHistory linked list.
//!exc: Error generating IntervalType. - Error generating MPI derived datatype 
//+     to represent C interval union.
//!exc: Error generating MultiDimDataType. - Error generating MPI derived      
//+     datatype to represent array of C multiDimData structures.
//!exc: Error generating StateVectorType. - Error generating MPI derived       
//+     datatype to represent C stateVector linked list.      
//!exc: Invalid number of stateVector nodes. - Negative number specified for   
//+     number of nodes in the linked list.  
//!exc: Invalid input data jobID. - Input ILWD format element's jobID doesn't
//+     correspond to the current jobID.   
//   
InputType::InputType( const INT4 rank, const MPI::Intracomm& comm )
   try : 
   LdasDatatype( comm, rank )
{
   mCreateTime -= MPI::Wtime();
   
   memset( &mInput, 0, sizeof( inPut ) );
   const LdasElement* pe( getILwdInput() );

   try
   {
      // Get metadata and broadcast it to slaves
      ilwdVector state_vec, sequence_vec;
      UINT4 numStates( 0 );
      MPIStructInfo* stateInfo( 0 ), *sequencesInfo( 0 );


      broadcastInfo( pe, numStates, 
                     &stateInfo, state_vec,
                     &sequencesInfo, sequence_vec );

   
      node_error.pass( comm, isP2P(), rank );

      
      // Build multiDimData datatype for inPut->sequences
      MultiDimDataType seqType( sequence_vec, &( mInput.sequences ),
                                sequencesInfo, mInput.numberSequences,
                                rank );
   
   
      // Build stateVector datatype for inPut->states
      StateVectorType stateType( state_vec, &( mInput.states ), 
                                 stateInfo, numStates, rank );


      MPIStructInfo* struct_elem( stateInfo ),
                    *struct_end( 0 );
   
   
      // Cleanup the memory
      if( numStates )
      {
         struct_elem = stateInfo;
         struct_end  = stateInfo + numStates - 1;
   
   
         while( struct_elem <= struct_end )
         {
            if( struct_elem->numberDimensions )
            {
               delete[] struct_elem->dimensions;
               struct_elem->dimensions = 0;
            }
          
            if( struct_elem->numHistory != 0 )
            {
               delete[] struct_elem->historyInfo;
               struct_elem->historyInfo = 0; 
            }
   
            ++struct_elem;
         }
   
         delete[] stateInfo;
         stateInfo = 0;
      }
   

      // At this point there is a guarantee that mInput.numberSequences 
      // is not zero:
      struct_elem = sequencesInfo;
      struct_end  = sequencesInfo + mInput.numberSequences - 1;
   
   
      while( struct_elem <= struct_end )
      {
         if( struct_elem->numberDimensions )
         {
            delete[] struct_elem->dimensions;
            struct_elem->dimensions = 0;   
         }
     
         if( struct_elem->numHistory )
         {
            delete[] struct_elem->historyInfo;
            struct_elem->historyInfo = 0;
         }

   
         ++struct_elem;
      }

      delete[] sequencesInfo;   
      sequencesInfo = 0;
   
   
      // seqType and stateType will be freed by buildType method
      buildType( stateType, seqType );
   }
   catch(...)
   {
      delete pe;
      pe = 0;
      
      throw;
   }
   
   mCreateTime += MPI::Wtime();   
}
catch( const bad_alloc& exc )   
{
   const bool p2p( isP2P() );
   cleanup( &mInput, p2p, getComm() );   
   
   node_error( exc, getComm(), p2p );
}
catch( const bad_cast& exc )
{
   const bool p2p( isP2P() );
   cleanup( &mInput, p2p, getComm() );   
   
   node_error( "Malformed ILWD input.", getComm(), p2p );
}
catch( const MPI::Exception& exc )
{
   const bool p2p( isP2P() );
   cleanup( &mInput, p2p, getComm() );      
   
   node_error( exc, getComm(), p2p );   
}
catch( const LdasException& exc )
{
   // Can only happen when receiving ILWD input 
   node_error( exc, getComm(), isP2P() );
}
catch( const SwigException& exc )
{
   // Can only happen when receiving ILWD input    
   node_error( exc, getComm(), isP2P() );   
}
   
   
//------------------------------------------------------------------------------
//
//: Destructor
//
InputType::~InputType()
{
   cleanup( &mInput, isP2P() );
   
   
   if( *this != MPI::DATATYPE_NULL )
   {
      Free();
   }
}

   
//------------------------------------------------------------------------------
//
//: Gets a pointer to the inPut data structure.
//
// This method makes a deep copy of the inPut data structure and returns a 
// pointer to that copy.   
//   
//!return: inPut* - A pointer to the inPut data structure.
//      
inPut* InputType::getInputStruct()
try   
{ 
   return copyData();
}
catch( const bad_alloc& exc )
{
   node_error( exc, getComm(), isP2P() );

   // To make compiler happy
   return 0;
}
   
   
//------------------------------------------------------------------------------
//   
//: Calculates data duration.   
// 
// This method extracts primary data sequence element with a name <i>primary</i>  
// in the third position of the name attribute. Primary sequence contains 
// significant values for the data duration that will be used for the progress
// report by master node in the communicator. 
// <p>This method gets called only by master node!
// 
//!return: double - Data duration time( in seconds ).
//
//!exc: Invalid data duration. - Invalid data interval is specified. The       
//+     value must be more than zero.
//!exc: Missing primary sequence. - inPut data structure is missing primary    
//+     sequence.   
//!exc: Only one primary sequence is allowed. - More than one primary sequence is 
//+     detected.   
//     
const REAL8 InputType::getDuration() const
{
   if( mInput.numberSequences != 0 )
   {
      static const string primary_name( "primary" );   

      REAL8 dt( 0.0f );
      bool found_primary( false );
   

      for( UINT4 i = 0; i < mInput.numberSequences; ++i )
      {
         // Create temporary ILWD format element with a given name string,
	 // it will do necessary name parsing for us:
         const LdasString name_element( "", mInput.sequences[ i ].name );
         const string name( name_element.getName( mPrimaryPos ) );
   
         if( name == primary_name )
	 {
            if( found_primary )
            {
               // Primary sequence was already found.
               node_error( "Only one primary sequence is allowed.", 
                           getComm(), isP2P() );                   
            }
   
            INT8 startSec( 0 ), startNan( 0 ), 
                 stopSec( 0 ), stopNan( 0 );
   
            switch( mInput.sequences[ i ].space )
            {
               case(noneD):
               {
                  node_error( "Sequence of noneD domain cannot be "
                              "specified as primary.",
                               getComm(), isP2P() );
                  break;
               }
               case(timeD):
               {
                  startSec = static_cast< INT8 >( 
                                mInput.sequences[ i ].range.dTime.startSec );
                  startNan = static_cast< INT8 >( 
                                mInput.sequences[ i ].range.dTime.startNan );
                  stopSec = static_cast< INT8 >(
                                mInput.sequences[ i ].range.dTime.stopSec );                        
                  stopNan = static_cast< INT8 >( 
                                mInput.sequences[ i ].range.dTime.stopNan );
                  break;
               }
               case(freqD):
               {
                  startSec = static_cast< INT8 >(
                                mInput.sequences[ i ].range.dFreq.gpsStartTimeSec );
                  startNan = static_cast< INT8 >(
                                mInput.sequences[ i ].range.dFreq.gpsStartTimeNan );
                  stopSec = static_cast< INT8 >(
                                mInput.sequences[ i ].range.dFreq.gpsStopTimeSec );                        
                  stopNan = static_cast< INT8 >(
                                mInput.sequences[ i ].range.dFreq.gpsStopTimeNan );                           
                  break;
               }   
               case(bothD):
               {
                  startSec = static_cast< INT8 >(
                                mInput.sequences[ i ].range.dBoth.gpsStartTimeSec );
                  startNan = static_cast< INT8 >(
                                mInput.sequences[ i ].range.dBoth.gpsStartTimeNan );
                  stopSec = static_cast< INT8 >(
                               mInput.sequences[ i ].range.dBoth.gpsStopTimeSec );                        
                  stopNan = static_cast< INT8 >(
                               mInput.sequences[ i ].range.dBoth.gpsStopTimeNan );                           
                  break;
               }      
               default:
               {
                  break;
               }
            }
   
            dt = stopSec - startSec + ( stopNan - startNan ) * 1e-9;    

            if( dt <= 0.0 )
            {
               node_error( "Invalid data duration.", 
                           getComm(), isP2P() );
            }
 
            //return dt;
            found_primary = true;               
	 }
      }
   
      if( found_primary )
      {
         return dt;
      }   
   }

   node_error( "Missing primary sequence.", getComm(), isP2P() );
   return 0.0f;   
}

   
//------------------------------------------------------------------------------
//
//: Gets total time spent in datatype generation.
//
// <font color='green'>For debugging purposes only!</font>   
//
//!return: REAL8 - Time value.
//                     
const REAL8 InputType::getCreateTime()
{
   return mCreateTime;
}

   
//------------------------------------------------------------------------------
//
//: Gets total time spent in input data receive.
//
// <font color='green'>For debugging purposes only!</font>   
//
//!return: REAL8 - Time value.
//                     
const REAL8 InputType::getRecvILwdTime()
{
   return mReceiveILwdTime;
}
   

//------------------------------------------------------------------------------
//
//: Broadcast data information.
//
// This method to be called by all nodes in the COMM_WORLD communicator that 
// construct this MPI derived datatype for inPut data communication.    
// When called by master it will parse passed to it ILWD element and send
// extracted metadata to all slave nodes involved in the communication, so all 
// slaves can allocate appropriate memory for receiving the data from master node.
//   
//!param: const LdasElement* pe - A pointer to the ILWD format element         
//+       representing C inPut data structure.
//!param: UINT4& numStates - A reference to the number of states in the inPut  
//+       data structure.
//!param: MPIStructInfo** statesInfo - A pointer to the array of of metadata   
//+       structures for states->store multiDimData structures.   
//!param: ilwdVector& state_vec - A reference to the vector of ILWD elements   
//+       representing inPut->states.   
//!param: MPIStructInfo** sequencesInfo - A pointer to the array of metadata   
//+       structures for sequences multiDimData structures.      
//!param: ilwdVector& sequence_vec - A reference to the vector of ILWD         
//+       elements representing inPut->sequences.   
//   
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: bad_cast - Malformed ILWD input data.
//!exc: sequence is missing. - ILWD format element representing the sequence   
//+     is missing for inPut.sequences or inPut.states->store.   
//!exc: data is missing - ILWD format element representing the data is missing.   
//!exc: domain is missing - ILWD format element representing the domain is     
//+     missing.      
//!exc: Undefined datatype. - Undefined datatype for ILWD element             
//+     representing the data was specified.
//!exc: Undefined domain. - Undefined domain value was specified.             
//+     One of TIME | FREQ | BOTH | DATABASE.   
//!exc: Invalid boolean datatype. - ILwd element representing boolean data    
//+     must be of char_u type.   
//!exc: Index exceeds the size of packing buffer. - The size of the packing    
//+     buffer is smaller that the data being packed into it. listMaxNodes 
//+     and multiDimDataMaxDims control the size of the buffer.   
//!exc: MPI::Exception - MPI library exception.   
//   
void InputType::broadcastInfo( const ILwd::LdasElement* pe, UINT4& numStates, 
   MPIStructInfo** statesInfo, ilwdVector& state_vec,
   MPIStructInfo** sequencesInfo, ilwdVector& sequence_vec ) 
{
   const MPI::Intracomm& comm( getComm() );
   
   // Get the size of dimension numbers
   INT4 buf_size( LDAS_MPI_UINT4.Pack_size( 2, comm ) );
   
   // The size of all history information
   buf_size += ( listMaxNodes << 1 ) *  
               LDAS_MPI_LIST_INFO.Pack_size( listMaxNodes, comm );
   
   // The size of states->store( struct ) and sequences( struct )
   buf_size += LDAS_MPI_STRUCT_INFO.Pack_size( listMaxNodes, comm ) << 1;
   
   // The size of multiDimData->dimensions
   buf_size += ( listMaxNodes << 1 ) * 
               LDAS_MPI_UINT4.Pack_size( multiDimDataMaxDims, comm );
   
   CHAR* buffer( new CHAR[ buf_size ] );

   INT4 buf_position( 0 );

   MPIStructInfo* metadata_elem( 0 );
   const MPIStructInfo* metadata_end( 0 );   
   
   const bool p2p( isP2P() );
   
   
   if( inMaster() )
   {
      const LdasContainer& c( static_cast< const LdasContainer& >( *pe ) );
   
      sequence_vec = findILwd( c, MultiDimDataType::getILwdName(), 
                               MultiDimDataType::getILwdNamePosition() );
      mInput.numberSequences = sequence_vec.size();
   
   
      const bool required( false );
      const LdasElement* state_elem( findElement( c, StateVectorType::getILwdName(), 
                                     StateVectorType::getILwdNamePosition(),
                                     required ) );
      if( state_elem )
      {
         const LdasContainer& state_cont( 
            dynamic_cast< const LdasContainer& >( *state_elem ) );
   
         numStates = state_cont.size();   
   
         // Set vector of state elements 
         for( LdasContainer::const_iterator citer = state_cont.begin(),
                end_citer = state_cont.end(); citer != end_citer; ++citer )
         {
            state_vec.push_back( *citer );
         }
      }
   
      size_t total_elements( mInput.numberSequences );
      total_elements += ( state_elem ? 1 : 0 );
   
      if( total_elements != c.size() )
      {
         throw bad_cast();
      }
   
      mPackDelta -= MPI::Wtime();
   
      LDAS_MPI_UINT4.Pack( &( mInput.numberSequences ), 1, 
                           buffer, buf_size, buf_position, comm );      
   
      LDAS_MPI_UINT4.Pack( &numStates, 1, 
                           buffer, buf_size, buf_position, comm );         
   
      mPackDelta += MPI::Wtime();   
      
   
      if( numStates )
      {
         *statesInfo = new MPIStructInfo[ numStates ];
         memset( *statesInfo, 0, sizeof( MPIStructInfo ) * numStates );   
   
   
         metadata_elem = *statesInfo;
   
    
         // Parse inPut->states
         ilwdVector store_vec; 

         iterator iter( state_vec.begin() ), 
                  iter_end( state_vec.end() );

   
         while( iter != iter_end )
         {
            store_vec.erase( store_vec.begin(), store_vec.end() );
            
            if( static_cast< const LdasContainer& >( **iter ).getName(
                   MultiDimDataType::getILwdNamePosition() ) != 
                MultiDimDataType::getILwdName() )
            {
               node_error( "stateVector node is missing multiDimData data.",
                           comm, p2p );
            }
            
            store_vec.push_back( *iter );
   
            parseILwd( store_vec, metadata_elem );         

          
            // Pack statesInfo data
            mPackDelta -= MPI::Wtime();   
   
            LDAS_MPI_STRUCT_INFO.Pack( metadata_elem, 1,
                                       buffer, buf_size, buf_position,
                                       comm );
   
         
            if( metadata_elem->numberDimensions )
            {
               LDAS_MPI_UINT4.Pack( metadata_elem->dimensions,
                                    metadata_elem->numberDimensions,
                                    buffer, buf_size, buf_position, comm );
            }
   
   
            if( metadata_elem->numHistory != 0 )
            {
               LDAS_MPI_LIST_INFO.Pack( metadata_elem->historyInfo,
                                        metadata_elem->numHistory,
                                        buffer, buf_size, buf_position,
                                        comm );
            }
   
            mPackDelta += MPI::Wtime();   
   
            if( buf_position > buf_size )
            {
               node_error( "Index exceeds the size of packing buffer", 
                           comm, p2p );
            }   

   
            ++iter;
            ++metadata_elem;
         }
      }
   

      *sequencesInfo = new MPIStructInfo[ mInput.numberSequences ];
      memset( *sequencesInfo, 0,
              sizeof( MPIStructInfo ) * mInput.numberSequences );         

      metadata_elem = *sequencesInfo;
   
      parseILwd( sequence_vec, metadata_elem );               

      metadata_end  = metadata_elem + mInput.numberSequences - 1;   

   
      while( metadata_elem <= metadata_end )
      {
         // Pack sequenceInfo data
         mPackDelta -= MPI::Wtime();   
   
         LDAS_MPI_STRUCT_INFO.Pack( metadata_elem, 1,
                                    buffer, buf_size, buf_position,
                                    comm );   
     

         if( metadata_elem->numberDimensions )
         {
            LDAS_MPI_UINT4.Pack( metadata_elem->dimensions,
                                 metadata_elem->numberDimensions,
                                 buffer, buf_size, buf_position, comm );
         }
   
   
         if( metadata_elem->numHistory )
         {
            LDAS_MPI_LIST_INFO.Pack( metadata_elem->historyInfo,
                                     metadata_elem->numHistory,
                                     buffer, buf_size, buf_position,
                                     comm );
         }
   
         mPackDelta += MPI::Wtime();
     
         if( buf_position > buf_size )
         {
            node_error( "Index exceeds the size of packing buffer",
                        comm, p2p );
         }   
   
   
         ++metadata_elem;
      }       
   
   
      if( buf_position + 16 >= buf_size )
      {
         buf_position = buf_size;
      }
      else
      {
         buf_position += 16;
      }
   
   
      if( dataDistributor )
      {
         node_error.pass( comm, p2p, getRank() );   
         mBcastDelta -= MPI::Wtime();   
   
         // Master node broadcasts metadata to all nodes in COMM_WORLD
   
         // if using buf_position instead of buf_size, MPI::ERR_TRUNCATE
         // type of MPI::Exception is raised when specifying -lamd option 
         // to mpirun( works with -c2c option though?! )
         //comm.Bcast( buffer, buf_position, MPI::PACKED, 0 );          
         comm.Bcast( buffer, buf_size, MPI::PACKED, 0 );             
   
         mBcastDelta += MPI::Wtime();      
      }
      else
      {
         mBcastDelta -= MPI::Wtime();   
   
         // Master node sends metadata to the search master 
         MPI::Request request( comm.Isend( buffer, buf_position, 
                                           MPI::PACKED, getRank(),
                                           STRUCT_INFO_TAG ) );
   
         node_error.pass( request, comm );
         mBcastDelta += MPI::Wtime();      
      }
   }
   else
   {
      if( dataDistributor )
      {
         node_error.pass( comm, p2p, getRank() );   
         mBcastDelta -= MPI::Wtime();   
   
         // All slaves receive the data
         comm.Bcast( buffer, buf_size, MPI::PACKED, 0 );             

         mBcastDelta += MPI::Wtime();      
      }
      else
      {
         mBcastDelta -= MPI::Wtime();   
   
         // Search master receives the data
         MPI::Request request( comm.Irecv( buffer, buf_size, 
                                           MPI::PACKED, getRank(),
                                           STRUCT_INFO_TAG ) );
   
         node_error.pass( request, comm );
         mBcastDelta += MPI::Wtime();      
      }
   
   
      // Unpack the data
      mPackDelta -= MPI::Wtime();
   
      LDAS_MPI_UINT4.Unpack( buffer, buf_size, 
                             &( mInput.numberSequences ), 1,
                             buf_position, comm );   
   
      mPackDelta += MPI::Wtime();
   
      if( !mInput.numberSequences )
      {
         // Something went wrong during communication,
         // since wrapperMaster has done this checking.
         throw bad_cast();
      }
   
      mPackDelta -= MPI::Wtime();   
   
      LDAS_MPI_UINT4.Unpack( buffer, buf_size, &numStates, 1,
                             buf_position, comm );         
   
      mPackDelta += MPI::Wtime();   
   
      if( numStates )
      {
         *statesInfo = new MPIStructInfo[ numStates ];
         memset( *statesInfo, 0, sizeof( MPIStructInfo ) * numStates );   
   
   
         metadata_elem = *statesInfo;
         metadata_end  = *statesInfo + numStates - 1;
   
   
         while( metadata_elem <= metadata_end )
         {
            mPackDelta -= MPI::Wtime();   
   
            LDAS_MPI_STRUCT_INFO.Unpack( buffer, buf_size, 
                                         metadata_elem, 1,
                                         buf_position, comm );         
   
            mPackDelta += MPI::Wtime();

            if( metadata_elem->numberDimensions )
            {
               metadata_elem->dimensions = 
                  new UINT4[ metadata_elem->numberDimensions ];
   
   
               mPackDelta -= MPI::Wtime();   
   
               LDAS_MPI_UINT4.Unpack( buffer, buf_size, 
                                      metadata_elem->dimensions,
                                      metadata_elem->numberDimensions,
                                      buf_position, comm );
   
               mPackDelta += MPI::Wtime();   
            }
   

            if( metadata_elem->numHistory )
            {
               metadata_elem->historyInfo = 
                  new MPIListInfo[ metadata_elem->numHistory ];

               mPackDelta -= MPI::Wtime();
   
               LDAS_MPI_LIST_INFO.Unpack( buffer, buf_size, 
                                          metadata_elem->historyInfo,
                                          metadata_elem->numHistory,
                                          buf_position, comm );

               mPackDelta += MPI::Wtime();   
            }

   
            if( buf_position > buf_size )
            {
               node_error( "Index exceeds the size of packing buffer",
                           comm, p2p );
            }   
   
    
            ++metadata_elem;
         }
      }   

      
      *sequencesInfo = new MPIStructInfo[ mInput.numberSequences ];
   
      memset( *sequencesInfo, 0,
              sizeof( MPIStructInfo ) * mInput.numberSequences );         

      metadata_elem = *sequencesInfo;
      metadata_end  = *sequencesInfo + mInput.numberSequences - 1;

   
      while( metadata_elem <= metadata_end )
      {
         mPackDelta -= MPI::Wtime();   
   
         LDAS_MPI_STRUCT_INFO.Unpack( buffer, buf_size, 
                                      metadata_elem, 1, 
                                      buf_position, comm );   
   
         mPackDelta += MPI::Wtime();   
  
         if( metadata_elem->numberDimensions )
         {
            metadata_elem->dimensions = 
               new UINT4[ metadata_elem->numberDimensions ];
   
            mPackDelta -= MPI::Wtime();   
   
            LDAS_MPI_UINT4.Unpack( buffer, buf_size, 
                                   metadata_elem->dimensions,
                                   metadata_elem->numberDimensions,
                                   buf_position, comm );
   
            mPackDelta += MPI::Wtime();   
         }
   

         if( metadata_elem->numHistory )
         {
            metadata_elem->historyInfo = 
               new MPIListInfo[ metadata_elem->numHistory ];   
   
            mPackDelta -= MPI::Wtime();   
   
            LDAS_MPI_LIST_INFO.Unpack( buffer, buf_size,
                                       metadata_elem->historyInfo,
                                       metadata_elem->numHistory,
                                       buf_position, comm );
   
            mPackDelta += MPI::Wtime();   
         }
   
   
         if( buf_position > buf_size )
         {
            node_error( "Index exceeds the size of packing buffer",
                        comm, p2p );
         }   
   
    
         ++metadata_elem;
      }       
   
   }
   
   // Memory cleanup
   delete[] buffer;

   return;
}

   
//-----------------------------------------------------------------------------
//   
//: Parse ILwd element.
//   
// This method initializes the array of metadata structures with the data of
// passed to it vector of ILWD format elements representing an array of 
// C multiDimData structures. This method is to be called only by the master 
// node of the communicator.
//   
//!param: ilwdConstVector& re - A reference to the vector of ILWD elements    
//+       representing an array of C multiDimData structures.
//!param: MPIStructInfo* dataInfo - An array of metadata structures for the   
//+       array of C multiDimData structures.   
//
//!return: Nothing.
//   
//!exc: Undefined datatype. - Undefined datatype for ILWD element             
//+     representing the data was specified.
//!exc: Undefined domain. - Undefined domain value was specified.             
//+     One of TIME | FREQ | BOTH | DATABASE.
//!exc: Invalid boolean datatype. - ILwd element representing boolean data    
//+     must be of char_u type.
//!exc: data is missing - ILWD format element representing the data is missing.   
//!exc: domain is missing - ILWD format element representing the domain enum  
//+     is missing.   
//!exc: bad_alloc - Error allocating memory.   
//!exc: bad_cast - Malformed ILWD input.   
//      
void InputType::parseILwd( ilwdConstVector& re, 
   MPIStructInfo* dataInfo ) const
{
   MPIStructInfo* info_elem( dataInfo );
   
   const_iterator iter( re.begin() ),
                  iter_end( re.end() );   
   
   ilwdVector history_vec;
   const LdasElement* data_elem( 0 );
   const LdasArrayBase* b( 0 );
   const LdasElement* detector( 0 );

   domainHash::const_iterator hash_iter, hash_end( mDomainHash.end() );
   string tmp;
   
   while( iter != iter_end )
   {
      // Check if sequence container is empty
      if( *iter == 0 ||
          ( *iter )->getElementId() != ID_ILWD ||
          dynamic_cast< const LdasContainer* >( *iter )->size() == 0 )
      {
         throw bad_cast();
      }   

      // Initialize domain   
      const LdasElement* domain( findElement( **iter, "domain", 1 ) );
   
      tmp = ( dynamic_cast< const LdasString& >( *domain ) ).getString();
   
      hash_iter = mDomainHash.find( tmp );
      if( hash_iter == hash_end )
      {
         string msg( "Undefined domain specified for ILWD input: " );
         msg += tmp;
         node_error( msg.c_str(), getComm(), isP2P() );   
      }
   
      info_elem->space = hash_iter->second;


      // Get detector information( if it exists )
      detector = findElement( **iter, 
         DetectorType::getILwdName(), DetectorType::getILwdNamePosition(),
         false );
      if( detector != 0 )
      {
         if( detector->getElementId() != ID_ILWD )
         {
            throw bad_cast();
         }
   
         info_elem->numberDetectors = 
            ( dynamic_cast< const LdasContainer* >( detector ) )->size();
   
         if( info_elem->numberDetectors == 0 )
         {
            throw bad_cast();
         }
      }
       
      // Get history data information
      history_vec.erase( history_vec.begin(), history_vec.end() );
      history_vec = getHistory( *iter );
      info_elem->numHistory = history_vec.size();
   
   
      if( info_elem->numHistory )
      {
         info_elem->historyInfo = 
            new MPIListInfo[ info_elem->numHistory ];   
         memset( info_elem->historyInfo, 0, 
                 info_elem->numHistory * sizeof( MPIListInfo ) );
   
   
         for( UINT4 index = 0; index < info_elem->numHistory; 
              ++index )
         {
            info_elem->historyInfo[ index ].numData = 
               getDim( history_vec[ index ] );
   
            info_elem->historyInfo[ index ].type = 
               getType( history_vec[ index ] );
      
         }
      }
   

      // Get data element
      data_elem = findElement( **iter, "data" );
   
   
      // Initialize datatype
      info_elem->type = getType( data_elem, domain->getName( 0 ) );


      // Get channel names information   
      info_elem->numberChannels = IntervalType::getChannelNameInfo( *iter,
                                     info_elem->channelSize );
   
   
      if( data_elem->getElementId() == ID_LSTRING )
      {
         // Will be converted to CHAR[]
         const LdasString* s( dynamic_cast< const LdasString* >( data_elem ) );
         info_elem->numberDimensions = 1;

         info_elem->dimensions = new UINT4[ info_elem->numberDimensions ];      
         info_elem->dimensions[ 0 ] = s->getSize();
      }
      else
      {
         // Do explicit test, since dynamic_cast of pointer sometimes 
         // throws bad_cast, sometimes doesn't.
         // Get number of data dimensions   
         b = dynamic_cast< const LdasArrayBase* >( data_elem );
         if( b == 0 )
         {
            throw bad_cast();
         }
   
         info_elem->numberDimensions = b->getNDim();

         // If there are any dimensions
         if( info_elem->numberDimensions )
         {
            info_elem->dimensions = new UINT4[ info_elem->numberDimensions ];   
    
            for( UINT4 index = 0; index < info_elem->numberDimensions; 
                 ++index )
            {
               info_elem->dimensions[ index ] = b->getDimension( index );
            }
         }
      }
   
   
      ++iter;
      ++info_elem;
   }

   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Is character escaped?
//      
// This is a helper method to determine if string character in the specified
// position is escaped( with backslash ).
//
//!param: const string& s - A reference to the string.
//!param: INT4 pos - A character position in the string.
//
//!return: const bool - True if not escaped, false otherwise.
//   
const bool InputType::isNotEscaped( const std::string& s, INT4 pos ) const
{
   if( pos == 0 )
   {
      return true;
   }

   static const char bslashChar( '\\' );      
   INT4 num( 0 );
   
   // Position of the previous char, that may be a bslashChar?
   --pos;
   
   while( pos >= 0 && s[ pos ] == bslashChar )         
   {
      ++num;
      --pos;
   }

   return ( num%2 == 0 );
}
   
   
//------------------------------------------------------------------------------
//
//: Build MPI derived datatype.
//      
// This method must be called by all nodes in the communicator that will use
// created datatype for the communication.   
// <p>It generates MPI derived datatype to represent C inPut data structure.
// Passed to this method MPI datatype objects will be marked for deallocation.   
//   
//!param: MPI::Datatype& stateType - A reference to the MPI datatype            
//+       representing C stateVector linked list.
//!param: MPI::Datatype& seqType - A reference to the MPI datatype representing 
//+       an array of C multiDimData structures.   
//
//!return: Nothing.
//
//!exc: Error generating InputType. - Error generating MPI datatype to      
//+     represent C inPut data structure.
//!exc: MPI::Exception - MPI library exception.   
//!exc: bad_alloc - Memory allocation failed.   
//   
void InputType::buildType( MPI::Datatype& stateType, MPI::Datatype& seqType )
{
   vector< MPI::Datatype > types( mTypeNum );
   vector< MPI::Aint > disps( mTypeNum );

   // You really need to know mTypeNum value before you change
   // these assignments! 
   disps[ 0 ] = 0;
   disps[ 1 ] = 0;

   vector< INT4 > blocks( mTypeNum );

   // You really need to know mTypeNum value before you change
   // these assignments! 
   blocks[ 0 ] = 0;
   blocks[ 1 ] = 0;   

   INT4 i_num( 0 );

   
   // Init states info
   if( stateType != MPI::DATATYPE_NULL )
   {
      types[ i_num ]  = stateType;
      blocks[ i_num ] = 1;
      disps[ i_num ]  = MPI::Get_address( mInput.states->stateName );
      ++i_num;
   }
   

   // Init sequences info
   types[ i_num ]  = seqType;
   blocks[ i_num ] = 1;
   disps[ i_num ]  = MPI::Get_address( mInput.sequences[ 0 ].name );
   ++i_num;


   createType( i_num, blocks, disps, types );   
   

   if( stateType != MPI::DATATYPE_NULL )
   {
      stateType.Free();
   }
   
   seqType.Free();

   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Gets name of the derived datatype.
//         
//!return: const CHAR* - Datatype name.
//   
const CHAR* InputType::getDatatypeName() const
{
   return "InputType";
}
   
   
//------------------------------------------------------------------------------
//
//: Creates a deep copy of mInput data structure.
//
//!return: inPut* - A pointer to the newly allocated data structure. 
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Undefined datatype. - Undefined datatype was specified for the data    
//+     array.      
//   
inPut* InputType::copyData() 
{
    inPut* input( new inPut );
    memset( input, 0, sizeof( inPut ) );

   
    // Handle mInput.sequences
    if( mInput.numberSequences )
    {
       input->numberSequences = mInput.numberSequences;
       input->sequences = new multiDimData[ mInput.numberSequences ];
   
   
       // Copy sequences data
       storeMultiDimData( input->sequences, mInput.sequences, 
          input->numberSequences, getComm(), isP2P() );
    }
   
   
    // Copy states data
    if( mInput.states )
    {
       stateVector* d_prev_node( 0 );
       stateVector* d_curr_node( 0 );
       bool first_node( true );
       const stateVector* s_curr_node( mInput.states );
   
   
       // Allocate memory and copy the data for input->states
       while( s_curr_node != 0 )
       {
          d_curr_node = new stateVector;
          memset( d_curr_node, 0, sizeof( stateVector ) );

          // Copy name and units
          memcpy( d_curr_node->stateName, s_curr_node->stateName, 
                  maxStateName * sizeof( CHAR ) );

          // Allocate memory for store and copy its data
          d_curr_node->store = new multiDimData[ 1 ];
          storeMultiDimData( d_curr_node->store, s_curr_node->store,
                             1, getComm(), isP2P() );
   
    
          if( first_node )
          {
             input->states = d_curr_node;
             first_node = false;
          }
          else
          {
             d_curr_node->previous = d_prev_node;
             d_prev_node->next = d_curr_node;
          }
   
   
          d_prev_node = d_curr_node;
          s_curr_node = s_curr_node->next;       
       }
    }
   

    return input;  
}
   

//------------------------------------------------------------------------------
//
//: Receives input ILWD format element from the socket.
//      
// This method receives intput ILWD format element through the data socket,
// or reads it from the file specified by -inputFile command line option.
// This method is used only by the master node of the communicator.   
// Reading from the file is used only in a standalone mode.   
//   
//!return: const ILwd::LdasElement* - A pointer to the ILWD format element.
//      
//!exc: Invalid input data jobID. - Input ILWD format element's jobID doesn't
//+     correspond to the current jobID.   
//   
ILwd::LdasElement* InputType::getILwdInput()
{
   if( inMaster() )
   {
      auto_ptr< LdasElement > elem;   
      mReceiveILwdTime -= MPI::Wtime();      
   
      if( inputFile.empty() )
      {
         elem.reset( readSocketData() );
      }
      else
      {
         elem.reset( readFileData() );
      }   

      mReceiveILwdTime += MPI::Wtime();         
   
      if( elem.get() == 0 || 
          ( dynamic_cast< const LdasContainer& >( *elem ) ).size() == 0 )
      {
         throw bad_cast();
      }


      UINT4 elem_jobID( elem->getJobId() );   
   
      // If input was received through data socket, do 
      // "ilwd attribute <-> command line" validation.
      if( inputFile.empty() && elem_jobID != jobID )
      {
         ostringstream s;
         s << "Invalid input data jobID, expected: " << jobID
           << " received: " << elem_jobID;
         node_error( s.str().c_str(), getComm(), isP2P() );         
      }
      else if( inputFile.empty() == false )
      {
         if( elem_jobID == 0 && jobID == 0 )
         {
            node_error( "jobID must be specified (either as a command"
                        " line argument or as an ilwd element attribute)");
         }
         // jobID is an attribute of ILWD element, 
         // but not a command line argument
         else if( elem_jobID && jobID == 0 )
         {
            jobID = elem_jobID;
         }
         // jobID is set as an ILWD attribute and specified as a command
         // line argument.
         else if( elem_jobID && jobID && elem_jobID != jobID )
         {
            ostringstream msg;
            msg << "Inconsistent jobID: ILWD attribute (" << elem_jobID
                << ") vs. command line argument (" << jobID << ")";
            node_error( msg.str().c_str(), getComm(), isP2P() );
         }
   
         // Make sure proper jobID gets into the process table
         try
         {
            initArguments.setProcessTableJobID( getComm(), isP2P() );
         }
         catch( const std::exception& exc )
         {
            node_error( "Error setting jobID for Process table.",
                        getComm(), isP2P() );
         }
      }


      InitArguments::setDumpDataDir();
      writeILwdInput( elem.get() );      
   
      return elem.release();
   }
   
   // Slave has input set to NULL
   return 0;
}

   
//------------------------------------------------------------------------------
//
//: Receives input ILWD format element from the socket.
//      
// This method receives intput ILWD format element through the data socket.
// This method is used only by the master node of the communicator.   
//   
//!return: const ILwd::LdasElement* - A pointer to the ILWD format element.   
//   
ILwd::LdasElement* const InputType::readSocketData()
{
   LdasElement* elem( 0 );
   try   
   {
   
   
     // Open data server on system available port:
     static const INT4 server_port( 0 ); 
     //os_ip_address ipAddress( INADDR_ANY );
     os_ip_address ipAddress( os_ip_address::my_address() );
        
     // Create a socket address with the designated IP address 
     // and system available port
     os_socket_address socketAddress( ipAddress, server_port );

     // Create the server and bind it to the address
     // throws system_call_failure, std::bad_alloc
     // Only one connection is allowed for the server:
     static const INT4 backlog( 1 );
     auto_ptr< os_tcp_connection_server > dataServer(    
						     new os_tcp_connection_server( socketAddress, backlog ) );

     static const bool deliver_flag( true );
     static const INT4 linger_sec( 5 );   
     dataServer->linger_information( deliver_flag, linger_sec );
     dataServer->auto_close( true );
   
     // Implement 'unix'-like handshake: send jobid, server host,
     // server port number to the dataconAPI emergency port:
     ostringstream handshake;
     handshake << jobID << ' ' 
	       << inet_ntoa( dataServer->socket_address().ip_address() ) << ' '
	       << dataServer->socket_address().port();

     if( enableLdasTrace )
     {
       cout << "Handshake to dataAPI (jobid server_ip server_port ): " << handshake.str() << endl;
     }
   
     const INT4 serverFD( dataServer->descriptor() );

     const bool is_error( false );
     const bool to_mpi( false );
     string handshake_msg( handshake.str() );
   
     auto_ptr< MsgSocket > dataAPISocket( new MsgSocket( dataAPI, is_error, to_mpi ) );   
     dataAPISocket->writeMessage( handshake_msg );
     dataAPISocket.reset(); // Force socket closure
   
     // Listen for incoming connection from datacon on 
     // server port:   
     fd_set read_fds;
     FD_ZERO( &read_fds );
     FD_SET( serverFD, &read_fds );

   
     // Set timeout: how long to poll for incoming connection
     struct timeval timeout = { 60, 0 }; // seconds, microseconds
     INT4 select_return( 0 );
   
     // Poll for connection request from dataconAPI
     select_return = select( serverFD + 1, 
			     &read_fds, 0, 0, &timeout );
   
     if( select_return )
     {
       if( select_return == -1 )
       {
         string msg( "InputType::readSocketData: cannot 'select': " );
         msg += strerror( errno );
   
         dataServer->close();   
         throw SwigException( msg.c_str() );
       }
     
       // There's incoming connection, accept:
       auto_ptr< os_tcp_socket > dataSocket( 
					    new os_tcp_socket( os_tcp_socket::unbound ) );
 
       // Listen for a connection
       if( !dataServer->accept( *dataSocket ) )
       {
         dataServer->close();   
         throw ERRACCEPTFAILURE();
       }

#ifdef DATA_SOCKET_SANITY_CHECK   
       // Identify the connecting socket:
       // get the peer's socket address
       os_socket_address dataSocketAddress( dataSocket->peer_address() );
       os_ip_address ipDataAddress( dataAPI.first );
   
       // check the address
       if( !( ipDataAddress == dataSocketAddress.ip_address() ) )
       {
         dataSocket->close();
         dataServer->close();   
         throw ERRILLEGALCONNECTION( dataSocketAddress );
       }
#endif   

       dataSocket->auto_close( true );
   
       // Receive input data
       if ( !( dataSocket->connected() ) )
       {
         dataSocket->close();
         dataServer->close();
         throw SWIGEXCEPTION( "InputType::readSocketData: unconnected data socket." );
       }
                 
       os_bstream data_stream( *dataSocket );
       data_stream >> elem;
   
       // Close data socket:
       dataSocket->close();
   
       // Close data server
       dataServer->close();
     }
     else 
     {
       dataServer->close();   
   
       ostringstream msg;
       msg << "Timed out waiting for connection from dataAPI: "
	   << dataAPI.first;
       throw SwigException( msg.str() );
     }
   }
   catch( const os_toolkit_error& e )
   {
      throw OSPACEEXCEPTION( e );
   }
   return elem;
   
}   


//------------------------------------------------------------------------------
//
//: Reads input ILWD format element from the file.
//      
// This method reads input data from the file specified by -inputFile 
// command line option.
// This method is used only by the master node of the communicator.   
// Reading from the file is used only in standalone mode.   
//   
//!return: ILwd::LdasElement* const  - A pointer to the ILWD format element.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: SwigException - 
//   
ILwd::LdasElement* const InputType::readFileData()
{
   // Read input from file
   if( LdasDebug )
   {
      cout << "Opening ILWD input file... ";
   }
   
   ifstream ilwdStream( inputFile.c_str() );
   if( !ilwdStream )
   {
      string s( "Error opening input file: " );
      s += inputFile;
      throw SWIGEXCEPTION( s );
   }

      
   // Read file header
   if( !ILwd::readHeader( ilwdStream ) )   
   {
      string s( "Not ILWD format file: " );
      s += inputFile;
      throw SWIGEXCEPTION( s );
   }
  
   ILwd::Reader r( ilwdStream );
   r.skipWhiteSpace();   

   
   if( LdasDebug )
   {
      cout << "Reading an ILWD element...";
   }
   
   LdasElement* elem( LdasElement::createElement( r ) );
 

   ilwdStream.close();   
   return elem;
}
   

//------------------------------------------------------------------------------
//
//: Writes input ILWD format element to the file.
//      
// <font color='red'>This method is used for debugging only!</font>   
// It writes input ILWD format element to the file for data verification.
// The full path to the file is: 
// $(mDumpDataDirValue)/$(mRunCode)_N/$(mRunCode)jobid/wrapperInput.ilwd.   
//   
//!param: const ILwd::LdasElement* e - A pointer to the ILWD format element
//+       to write to the file.
//
//!return: Nothing.
//   
void InputType::writeILwdInput( const ILwd::LdasElement* e )
{
   if( inMaster() && enableDumpInput )
   {
      Format format;
      if( numDummyState )
      {
         format = ILwd::BINARY;   
      }
      else   
      {
         format = ILwd::USER_FORMAT;
      }
   
      const Compression compression( ILwd::USER_COMPRESSION );
      string name_path( dumpDataDir );
      name_path += "wrapperInput.ilwd";
   
      const CHAR* name( name_path.c_str() );

      commILwdTime -= MPI::Wtime();   
   
      ofstream out( name );
      if( !out )
      {
         delete e;
         
         string msg( "Error opening output file for ILWD inPut: " );
         msg += name;
         node_error( msg.c_str(), getComm(), isP2P() );
      }

      ILwd::writeHeader( out );
      ( dynamic_cast< const LdasContainer* >( e ) )->write( 
              0, 4, out, format, compression );      
      out.close();  
      if( chmod( name, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ) == -1 )      
      {
         delete e;
   
         string msg( "Error changing permissions for: " );
         msg += name;
         node_error( msg.c_str(), getComm(), isP2P() );
      }   
   
      commILwdTime += MPI::Wtime();      
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Cleanup utility.
//      
// This method frees dynamically allocated memory for the array of C inPut data 
// structures.
//   
//!param: inPut* input - A pointer to the array of C inPut data structures     
//+       to deallocate.
//!param: const MPI::Intracomm& comm - A reference to the communicator.        
//+       Default is COMM_WORLD.   
//   
void InputType::cleanup( inPut* input, const bool p2p,
   const MPI::Intracomm& comm )
{
   if( input == 0 )
   {
      return;
   }
   
   
   if( input->states )
   {
      // Delete mInput.states
      stateVector* curr_node( input->states ); 
      stateVector* next_node( 0 );   
   
      
      // Delete curr_node->store for each node in the linked list
      while( curr_node != 0 )
      {
         // There will be only one multiDimData structure per node
         deleteMultiDimData( curr_node->store, 1, comm, p2p );         
   
         if( curr_node->store != 0 )
         {
            delete[] curr_node->store;
            curr_node->store = 0;
         }
   
         next_node = curr_node->next;
   
         delete curr_node;
         curr_node = next_node;
      }
   
      input->states = 0;
   }


   // Delete input->sequences data
   if( input->numberSequences && input->sequences )
   {
      deleteMultiDimData( input->sequences, input->numberSequences,
                          comm, p2p );
   
      delete[] input->sequences;
      input->sequences = 0;
   
      input->numberSequences = 0;
   }

   
   return;
}

   
