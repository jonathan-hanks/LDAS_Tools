#include "LDASConfig.h"

// System Header Files
#include <sstream>
#include <iomanip>   
#include <iostream>   

// For C_CLIENT only   
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>   
#include <sys/time.h>   
#include <unistd.h>
#include <fcntl.h>   
#include <arpa/inet.h>      
   
// General Header Files
#include <general/regexmatch.hh>      
   
// Local Header Files
#include "initvars.hh"   
#include "msgsocket.hh"   
#include "ldasrequest.hh"
#include "ldaserror.hh"   
   
using namespace std;

   
// perceps can't handle static data member initialization 
// properly ===> ignore it.   
//!ignore_begin:   

// Static data members initialization.
const string LdasRequest::mInitialize( ":initializing" );      
const string LdasRequest::mFinalize( ":finalizing" );   
INT4         LdasRequest::mRequest( 0 );   
const string LdasRequest::mAdd( ":request add " );
const string LdasRequest::mSub( ":request sub " );   
const string LdasRequest::mWarning( ":warning " );
const string LdasRequest::mError( ":error " );   
const string LdasRequest::mProgress( ":progress " );   
const string LdasRequest::mUsing( ":using " );   
const string LdasRequest::mRatio( ":projected_ratio " );   

   
/*
 * Regex details for the mpiAPI add command.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "("                 Remember beginning of the match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match   
 * \\.                 Dot
 * "("                 Remember beginning of the second match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match   
 * "[[:space:]]*"      Optional space
 * ":"                 ":" 
 * "[[:space:]]*"      Optional space      
 * "add"               "add"    
 * "[[:space:]]+"      At least one space
 * "("                 Remember the third match
 *    "[0-9]+"         At least one digit   
 * ")"                 End of the match
 * "[[:space:]]*"      Optional space  
 */
const Regex LdasRequest::re_add(
   "^[[:space:]]*([0-9]+)\\.([0-9]+)[[:space:]]*:"
   "[[:space:]]*add[[:space:]]+([0-9]+)[[:space:]]*" );

   
/*
 * Regex details for the mpiAPI subtract command.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "("                 Remember beginning of the match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match   
 * \\.                 Dot
 * "("                 Remember beginning of the second match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match      
 * "[[:space:]]*"      Optional space
 * ":"                 ":" 
 * "[[:space:]]*"      Optional space   
 * "sub"               "sub"    
 * "[[:space:]]+"      At least one space
 * "("                 Remember the third match
 *    "[0-9]+"         At least one digit   
 * ")"                 End of the match
 * "[[:space:]]*"      Optional space  
 */
const Regex LdasRequest::re_sub(
   "^[[:space:]]*([0-9]+)\\.([0-9]+)[[:space:]]*:"
   "[[:space:]]*sub[[:space:]]+([0-9]+)[[:space:]]*" );

   
/*
 * Regex details for the mpiAPI kill command.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "("                 Remember beginning of the match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match   
 * \\.                 Dot
 * "("                 Remember beginning of the second match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match         
 * "[[:space:]]*"      Optional space
 * ":"                 ":" 
 * "[[:space:]]*"      Optional space   
 * "kill"              "kill"    
 * "[[:space:]]*"      Optional space
 * "$"                 The end of the buffer
 */
const Regex LdasRequest::re_kill(
   "^[[:space:]]*([0-9]+)\\.([0-9]+)[[:space:]]*:"
   "[[:space:]]*kill[[:space:]]*$" );
   
   
/*
 * Regex details for the mpiAPI continue command.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "("                 Remember beginning of the match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match   
 * \\.                 Dot
 * "("                 Remember beginning of the second match   
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match      
 * "[[:space:]]*"      Optional space
 * ":"                 ":" 
 * "[[:space:]]*"      Optional space   
 * "cont"              "cont"    
 * "[[:space:]]*"      Optional space
 * "$"                 The end of the buffer
 */
const Regex LdasRequest::re_cont(
   "^[[:space:]]*([0-9]+)\\.([0-9]+)[[:space:]]*:"
   "[[:space:]]*cont[[:space:]]*$" );   

   
//!ignore_end:
   
   
//-------------------------------------------------------------------------------   
//   
//: Default constructor.
//   
LdasRequest::LdasRequest()
{}

   
//-------------------------------------------------------------------------------   
//   
//: Overloaded call operator.
//   
// This overloaded call operator submits the warning to the mpiAPI and parses
// the response message received from it. This operator is called only 
// the master node of the communicator( wrapperMaster ).
//
//!param: const CHAR* warning - Warning message to be sent to the mpiAPI.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent.     
//   
//!return: Nothing.
//
//!exc: Invalid mpiAPI response for wrapperAPI warning. - mpiAPI can respond only
//+     with KILL or CONTINUE command.      
//!exc: Unknown mpiAPI command. - Unknown command is received from mpiAPI.
//!exc: Inconsistent request ID. - Request ID for wrapperAPI and mpiAPI are     
//+     inconsistent.      
//   
void LdasRequest::operator()( const std::string& warning, const MPI::Intracomm& comm )
{
   if( warning.empty() == false )
   {
      ++mRequest;

      ostringstream s;
      addWarning( s, warning, false );
   
      string msg( s.str() );
   
      LDAS_LB_STATE mpi_state( LDAS_LB_CONTINUE );
      const vector< INT4 > nodes( processRequest( msg, mpi_state ) );
   
      if( mpi_state != LDAS_LB_CONTINUE && mpi_state != LDAS_LB_KILL )
      {
         node_error( "Invalid mpiAPI response for wrapperAPI warning.", comm );  
      }
   
      if( mpi_state == LDAS_LB_KILL )
      {
         // wrapperMaster aborts. Normally when wrapperMaster receives 
         // 'KILL' command from mpiAPI, it will pass the command to all 
         // slaves in the COMM_WORLD and will let one more call to the 
         // applySearch function before terminating the job.
         // In a case of the warning( will be reported only when 
         // wrapper master doesn't hear from any of the slaves for a long
         // time ) it won't be able to do a clean termination ===>
         // might as well just do the job abort.
         node_error( 0, MPI::COMM_WORLD, !loadBalance );
      }   
   }

   return;
}
   
  
//-------------------------------------------------------------------------------   
//   
//: Overloaded call operator.
//   
// This overloaded call operator submits the request to the mpiAPI and parses
// the response message received from it. This operator is called only 
// the master node of the communicator( wrapperMaster ).
//
//!param: const LDAS_LB_STATE lb_state - Load balancing state set by wrapperMaster.
//!param: const INT4 dim - Number of requested nodes to add/subtract. 
//!param: const REAL4 ratio - Current ratio of the projected time to completion  
//+       to the amount of data being analyzed.
//!param: const REAL4 progress - Percent complete for the parallel process job.   
//!param: LDAS_LB_STATE& mpi_state - Load balancing state set by mpiAPI.  
//!param: const string& war_msg - A reference to the optional warning message. 
//+       Default is empty message.      
//!param: const string& err_msg - A reference to the optional error message. 
//+       Default is empty message.         
//
//!return: vector< INT4 > - A vector of nodes to be added/removed to/from the    
//+        communicator.   
//   
//!exc: Undefined request. - Undefined format for request was specified.
//!exc: Unknown mpiAPI command. - Unknown command is received from mpiAPI.
//!exc: Inconsistent request ID. - Request ID for wrapperAPI and mpiAPI are     
//+     inconsistent.   
//!exc: Number of nodes in LB communicator will exceed the COMM_WORLD. - Number 
//+     of nodes in the LB communicator exceeds total number of nodes in the 
//+     COMM_WORLD.
//!exc: Number of nodes exceeds comm world. - Number of nodes to add/remove     
//+     to/from LB communicator exceeds total number of nodes in the comm    
//+     world.    
//!exc: Malformed mpiAPI command to add nodes.- mpiAPI command to add           
//+     nodes into LB communicator is malformed( number of nodes to add      
//+     differs from amount of specified nodes ).   
//!exc: Malformed mpiAPI command to subtract nodes.- mpiAPI command to          
//+     subtract nodes from LB communicator is malformed( number of nodes    
//+     to subtract differs from amount of specified nodes ).      
//!exc: Illegal mpiAPI command to subtract nodes. - Illegal mpiAPI command to   
//+     subtract nodes from the communicator. The number of nodes to         
//+     subtract exceeds or equal to the total number of nodes in the        
//+     communicator.   
//!exc: bad_alloc - Error allocating new memory.       
//   
std::vector< INT4 > LdasRequest::operator()( const LDAS_LB_STATE lb_state, 
   const INT4 dim, const REAL4 ratio, const REAL4 progress,
   LDAS_LB_STATE& mpi_state, const std::string& war_msg, 
   const std::string& err_msg )
{
   ++mRequest;
   
   ostringstream s;   
   switch(lb_state)
   {
      case(LDAS_LB_CONTINUE):
      {
     	 // Report used nodes information only if load balancing
         // is turned on: otherwise it's redundant information.
	 if( loadBalance )
	 {
            INT4 size( nodes.size() );   
   
            s << jobID << '.' << mRequest << mUsing  << '{' 
              << size << " (";
   
            --size;
   
            for( vector< INT4 >::const_iterator iter = nodes.begin(), 
                 end_iter = nodes.end(); iter != end_iter; )
            {
               s << *iter;
               ++iter;
               if( iter != end_iter )
               {
                  s << ',';
               }
            }
   
            s << ") nodes out of the " << totalNodes 
              << " available in comm world }";
	 }
   
         break;
      }
      case(LDAS_LB_ADD):
      {
         s << jobID << '.' << mRequest << mAdd << dim;
         break;
      }
      case(LDAS_LB_SUBTRACT):
      {
         s << jobID << '.' << mRequest << mSub << dim;
         break;
      }   
      default:
      {
         node_error( "Undefined request." );
         break;
      }   
   }     

   // Append progress report
   s.setf( ios::fixed, ios::floatfield );
   s << '\n' << jobID << '.' << mRequest << mProgress << setprecision( 2 ) 
             << progress << '%'
     << '\n' << jobID << '.' << mRequest 
             << setprecision( 5 ) << mRatio << ratio; 

   addWarning( s, war_msg );
   addError( s, err_msg );
   
   string s_str( s.str() );
   return processRequest( s_str, mpi_state );
}


//-------------------------------------------------------------------------------      
//   
//: Gets current request number for the job.
//
//!return: const INT4 - Request number.
//   
const INT4 LdasRequest::getRequestNum()
{
   return ++mRequest;
}

   
//-------------------------------------------------------------------------------      
//   
//: Gets current request number.
//
//!return: const INT4 - Request number.
//   
const INT4 LdasRequest::getCurrentRequestNum()
{
   return mRequest;
}

   
//-------------------------------------------------------------------------------      
//   
//: <font color='green'>For debugging only!!!</font>   
//   
const INT4 LdasRequest::getNextRequestNum()
{
   return ( mRequest + 1 );
}
   

//-------------------------------------------------------------------------------      
//   
//: Gets error message format.
//
// This method gets error message format used for sending the error message 
// with current request.
//
//!return: const string& - A reference to the error message format.
//   
const std::string& LdasRequest::getErrorString()
{
   return mError;
}   
   

   
//-----------------------------------------------------------------------------
//   
//: Processes request.
//
// This method sends request to the mpiAPI, and parses response message
// received from it.   
//
//!param: const string& msg - A reference to the request message to send to     
//+       mpiAPI.   
//   
//!return: vector< INT4 > - A vector of nodes to be added/subtracted to/from    
//+        LB communicator. This vector will be empty if load balancing is
//+        disabled.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
std::vector< INT4 > LdasRequest::processRequest( std::string& msg, 
   LDAS_LB_STATE& mpi_state ) const
{
   string mpi_msg;
   
   if( enableMpiApi )
   {
      // Send request to mpiAPI
      if( MsgSocket::canSendMessage( msg ) )
      {
         MsgSocket mpiSocket( mpiAPI );
   
         if( enableLdasTrace )
         {
            cout << "Master before sending message to mpiAPI: " << msg << endl;
         }
   
         // Write the message
         mpiSocket.writeMessage( msg );

         // Recieve response message from mpiAPI
         mpiSocket.readMessage( mpi_msg );      

         if( enableLdasTrace )
         {
            cout << "Master received message from mpiAPI: " << mpi_msg << endl;
         }
      }
      else
      {
         // Message to mpiAPI was buffered, set mpi_state to CONTINUE (only 
         // if it's not set to KILL by previous mpiAPI response)
         if( mpi_state != LDAS_LB_KILL )
         {
            mpi_state = LDAS_LB_CONTINUE;       
         }
   
         return vector< INT4 >();
      }
   }
   else
   {
      // MpiAPI simulation
      if( enableLdasTrace )
      {
         cout << "Master before sending message to mpiAPI( mpiAPI simulation ): "
              << msg << endl;
      }
   
      ostringstream s;
   
      if( loadBalance )
      {
         if( mRequest == 1 )
         {
            s << jobID << "." << mRequest << ":add 1 ( 3 )";
         }
         else if( mRequest == 4 )
         {
            s << jobID << "." << mRequest << ":sub 1 ( 2 )";   
   
            // Test kill in the middle of parallel job 
            //s << jobID << "." << mRequest << ":kill";      
         }
         //else if( mRequest == 2 )
         //{
           //s << jobID << "." << mRequest << ":sub 2 ( 1, 2 )";   
         //}   
         else 
         {
            s << jobID << "." << mRequest << ":cont";   
         }
      }
      else
      {
         // Test kill 
            //#define TEST_MPI_KILL      
#ifdef TEST_MPI_KILL   
            if( mRequest == 5 )
            {
               s << jobID << "." << mRequest << ":kill";         
            }
            else
#endif   
         {
            s << jobID << "." << mRequest << ":cont";      
         }
      }
   
   
      mpi_msg = s.str();
   
      if( enableLdasTrace )
      {
         cout << "Master received message from mpiAPI: " << mpi_msg << endl;
      }
   }

   
   // Parse mpiAPI response message
   return parseResponse( mpi_msg, mpi_state );   
}
   

//-----------------------------------------------------------------------------
//   
//: Parses response message sent by mpiAPI.
//
// This method parses message received from the mpiAPI in response to the
// request sent to it by master node.
//
//!param: const string& msg - A reference to the message sent by mpiAPI.   
//
//!return: vector< INT4 > - A vector of nodes to be added/subtracted to/from   
//+        LB communicator. This vector will be empty if load balancing is 
//+        disabled.
//   
//!exc: Unknown mpiAPI command. - Unknown command is received from mpiAPI.
//!exc: Inconsistent request ID. - Request ID for wrapperAPI and mpiAPI are     
//+     inconsistent.   
//!exc: Number of nodes in LB communicator will exceed the COMM_WORLD. - Number 
//+     of nodes in the LB communicator exceeds total number of nodes in the 
//+     COMM_WORLD.
//!exc: Number of nodes exceeds comm world. - Number of nodes to add/remove     
//+     to/from LB communicator exceeds total number of nodes in the comm    
//+     world.    
//!exc: Malformed mpiAPI command to add nodes.- mpiAPI command to add           
//+     nodes into LB communicator is malformed( number of nodes to add      
//+     differs from amount of specified nodes ).   
//!exc: Malformed mpiAPI command to subtract nodes.- mpiAPI command to          
//+     subtract nodes from LB communicator is malformed( number of nodes    
//+     to subtract differs from amount of specified nodes ).      
//!exc: Illegal mpiAPI command to subtract nodes. - Illegal mpiAPI command to   
//+     subtract nodes from the communicator. The number of nodes to         
//+     subtract exceeds or equal to the total number of nodes in the        
//+     communicator.   
//!exc: Memory allocation failed. - Error allocating new memory.    
//      
std::vector< INT4 > LdasRequest::parseResponse( const std::string& msg, 
   LDAS_LB_STATE& mpi_state ) const
try   
{
   vector< INT4 > lb_nodes;
   
   const CHAR* index( msg.c_str() );

   RegexMatch rm( 4 );

   INT4 nodes_size( nodes.size() );
   
   
   if( rm.match( re_add, index ) )
   {
      // Add command was issued 
      const INT4 num( atoi( rm.getSubString( 3 ).c_str() ) );
   
   
      if( num + nodes_size > totalNodes )
      {
         node_error( "Number of nodes in LB communicator "
                     "will exceed the COMM_WORLD." );
      }
   

      // Go to the end of message header
      index = rm.getSubEnd( 0 );
   
   
      NodeListArg parse_nodes;
      lb_nodes = parse_nodes( index );
   
   
      if( num != static_cast< INT4 >( lb_nodes.size() ) )
      {
         node_error( "Malformed mpiAPI command to add nodes." );
      }
   
   
      mpi_state = LDAS_LB_ADD;
   }
   else if( rm.match( re_sub, index ) )
   {
      // Subtract command was issued
      const INT4 num( atoi( rm.getSubString( 3 ).c_str() ) );   
   
   
      if( num >= nodes_size )
      {
         node_error( "Illegal mpiAPI command to subtract nodes." );
      }
   
   
      // Go to the end of message header   
      index = rm.getSubEnd( 0 );   
   
   
      NodeListArg parse_nodes;
      lb_nodes = parse_nodes( index );   

   
      if( num != static_cast< INT4 >( lb_nodes.size() ) )
      {
         node_error( "Malformed mpiAPI command to subtract nodes." );
      }   
   
   
      mpi_state = LDAS_LB_SUBTRACT;   
   }
   else if( rm.match( re_kill, index ) )
   {
      // Allow last call to applyFilters if job is not complete.
      mpi_state = LDAS_LB_KILL;
   }
   else if( rm.match( re_cont, index ) )
   {
      // Continue command was issued, do nothing
      // (preserve previous KILL command though)
      if( mpi_state != LDAS_LB_KILL )
      {
         mpi_state = LDAS_LB_CONTINUE;    
      }
   }
   else
   {
      node_error( "Unknown mpiAPI command." );
   }

   
   if( atoi( rm.getSubString( 1 ).c_str() ) != 
       static_cast< INT4 >( jobID ) )
   {
      ostringstream msg;
      msg << "Inconsistent jobID: current jobID = " << jobID 
          << ", received jobID = " << rm.getSubString( 1 );
   
      node_error( msg.str().c_str() );
   }
   
   
   if( atoi( rm.getSubString( 2 ).c_str() ) != mRequest )
   {
      ostringstream msg;
      msg << "Inconsistent request ID: current ID = " << mRequest 
          << ", received ID = " << rm.getSubString( 2 ); 
      node_error( msg.str().c_str() );
   }
   
   
//    cout << "LB_NODES: ";
//    for( int i = 0; i < lb_nodes.size(); ++i )
//    {
//      cout << lb_nodes[ i ] << " ";
//    }
//    cout << endl;
   
   
   return lb_nodes;
}
catch( const bad_alloc& exc )
{
   node_error( exc );
   
   // To make compiler happy
   return vector< INT4 >();
}


//-------------------------------------------------------------------------------   
//   
//: Adds warning message.
//      
// This method adds the warning to the mpiAPI report message.   
//
//!param: ostingrstream& s - A reference to the string stream representing 
//+       mpiAPI message.
//!param: const string& msg - Warning message.
//!param: const bool new_line - A flag to indicate if new line need to be 
//+       inserted. Default is true.
//   
//!return: Nothing.
//   
void LdasRequest::addWarning( std::ostringstream& s, const std::string& msg, 
   const bool new_line )   
{   
   if( msg.empty() == false )
   {
      if( new_line )
      {
         s << '\n';
      }
   
      s << jobID << '.' << mRequest << mWarning 
        << '{' << msg << '}';
   }

   return;
}   

   
//-------------------------------------------------------------------------------   
//   
//: Adds error message.
//      
// This method adds the error to the mpiAPI report message.
//
//!param: ostringstream& s - A reference to the string stream representing 
//+       mpiAPI message.
//!param: const string& msg - Error message.
//!param: const bool new_line - A flag to indicate if new line need to be 
//+       inserted. Default is true.
//   
//!return: Nothing.
//      
void LdasRequest::addError( std::ostringstream& s, const std::string& msg, 
   const bool new_line )   
{   
   if( msg.empty() == false )
   {
      if( new_line )
      {
         s << '\n';
      }
   
      s << jobID << '.' << mRequest << mError
        << '{' << msg << '}';
   }

   return;
}   
   
   
