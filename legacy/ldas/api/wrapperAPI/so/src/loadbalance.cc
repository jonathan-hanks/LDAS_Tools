#include "LDASConfig.h"

// System Header Files
#include <cmath>   
#include <algorithm>   
#include <iterator>   
#include <sstream>   
#include <stdlib.h>   
#include <unistd.h>   

// Local Header Files
#include "loadbalance.hh"   
#include "ldasrequest.hh"   
#include "initvars.hh"
#include "ldaserror.hh"   
#include "mpiutil.hh"   
#include "dynamiccom.hh"   
#include "mpilaltypes.hh"   
#include "ldastag.hh"   
   
using namespace std;

// Static data initialization   
bool LdasLoadBalance::mSynchronize( true );   
REAL8 LdasLoadBalance::mTimeSpent( 0.0f );      
   

//-------------------------------------------------------------------------------   
//   
//: Default constructor.
//
// This constructor initializes flags and states used for load balancing, and
// allocates the memory used for node communication.   
//   
//!exc: bad_alloc - Memory allocation failed.
//   
LdasLoadBalance::LdasLoadBalance() 
   : mState( LDAS_LB_SUBTRACT ), mMPIState( LDAS_LB_CONTINUE ), mNum( 0 ),
     mBufferSize( 0 ), mBuffer( 0 ), mKillState( IDLE )
{
   mBufferSize += LDAS_MPI_INT4.Pack_size( 1 + totalNodes, MPI::COMM_WORLD );
   mBufferSize += LDAS_MPI_UINT4.Pack_size( 1, MPI::COMM_WORLD );
   mBufferSize += LDAS_MPI_BOOLEAN.Pack_size( 1, MPI::COMM_WORLD );                  
   

   mBuffer = new CHAR[ mBufferSize ];   
}

   
//-------------------------------------------------------------------------------   
//   
//: Destructor.
//   
LdasLoadBalance::~LdasLoadBalance()
{
   delete[] mBuffer;
   mBuffer = 0;
}
   
   
//-------------------------------------------------------------------------------   
//   
//: Overloaded call operator.
//      
// This operator balances the load of the parallel processing job.
//
//!param: const REAL4 num - Calculated number of missing/extra nodes in the LB   
//+       communicator.
//!param: const REAL4 ratio - Current ratio of the projected time to completion  
//+       to the amount of data being analyzed.
//!param: const REAL4 progress - Percent complete for the parallel process job.   
//!param: MPI::Intracomm& lbcomm - A reference to the load balancing communicator.
//!param: MPI::Intracomm& master_lbcomm - A reference to the communicator         
//+       consisting of load balancing nodes and master node of COMM_WORLD.   
//!param: MPI_Comm* c_lbcomm - A pointer to the C type load balancing communicator.   
//!param: const string& war_msg - A reference to the optional warning message.    
//+       Default is empty string.         
//!param: const string& err_msg - A reference to the optional error message.      
//+       Default is empty string.            
//
//!return: const INT4 - Number of nodes added/subtructed to/from LB communicator.
//   
//!exc: Invalid MPI response message - Invalid response message is sent by        
//+     mpiAPI( the state of response message does not correspond to the       
//+     state of sent request message ).
//!exc: Rank is already in LB communicator. - Specified rank to be added to the   
//+     load balancing communicator already exists within the communicator.   
//!exc: Rank is not in LB communicator. - Specified rank to be deleted from the   
//+     load balancing communicator is not in the communicator.   
//!exc: bad_alloc - Error allocating memory.   
//!exc: Undefined request. - Undefined format for request was specified.   
//!exc: Unknown mpiAPI command. - Unknown command is received from mpiAPI.
//!exc: Inconsistent request ID. - Request ID for wrapperAPI and mpiAPI are     
//+     inconsistent.   
//!exc: Number of nodes exceeds COMM_WORLD. - Number of nodes to add/remove     
//+     to/from LB communicator exceeds total number of nodes in the comm    
//+     world.    
//!exc: Malformed mpiAPI command to add nodes.- mpiAPI command to add           
//+     nodes into LB communicator is malformed( number of nodes to add      
//+     differs from amount of specified nodes ).   
//!exc: Malformed mpiAPI command to subtract nodes.- mpiAPI command to          
//+     subtract nodes from LB communicator is malformed( number of nodes    
//+     to subtract differs from amount of specified nodes ).      
//!exc: Illegal mpiAPI command to subtract nodes. - Illegal mpiAPI command to   
//+     subtract nodes from the communicator. The number of nodes to         
//+     subtract exceeds or equal to the total number of nodes in the        
//+     communicator.      
//!exc: MPI::Exception - MPI library exception.
//   
const INT4 LdasLoadBalance::operator()( const REAL4 num, const REAL4 ratio, 
   const REAL4 progress, BOOLEAN& not_finished, MPI::Intracomm& lbcomm, 
   MPI::Intracomm& master_lbcomm, MPI_Comm* c_lbcomm, 
   const std::string& war_msg, const std::string& err_msg ) 
{
   mTimeSpent -= MPI::Wtime();
   
   if( loadBalance )
   {
      mNodes.erase( mNodes.begin(), mNodes.end() );
      mNum = 0;
   }
   

   const bool last_call( isLastCall() );
   const bool allowAsynchronous( isAsynchronous( not_finished ) );
   

   // Master node
   if( myNode == masterNode )
   {
      // Based on job progress, figure out what type of request
      // to send to mpiAPI
      mState = LDAS_LB_SUBTRACT;
   
      REAL4 precision( 1.0f / ( static_cast< REAL4 >( nodes.size() ) ) );
      //cout << "LB number is " << num << endl;
   
      INT4 dim( static_cast< INT4 >( num ) );
      REAL4 diff( num - static_cast< REAL4 >( dim ) );
   
   
      if( loadBalance == false || progress == 100.0f ||
          ( dim == 0 && fabs( diff ) < precision ) || last_call )
      {
         // In this state wrapperAPI only sends progress report
         // to the mpiAPI( no requests are made )
         mState = LDAS_LB_CONTINUE;
      }
   
   
      if( mState == LDAS_LB_SUBTRACT && dim > 0 )
      {
         if( diff >= precision )
         {
            ++dim;
         }
   
         mState = LDAS_LB_ADD;
      }
   
   
      // Asynchronous mpiAPI<->wrapperMaster and 
      // wrapperMaster<->searchMaster communications &&
      // LDAS_LB_KILL wasn't sent to the searchMaster yet
   
      // wrapperMaster need to deliver only "kill" MPIApi response to all slaves
      if( allowAsynchronous && mMPIState == LDAS_LB_KILL ) //mKillState == IDLE ) 
      {
         if( mKillState == IDLE )
         {
            // Master responses to the searchMaster with 'continue' 
            // right away, then reports to the mpiAPI
            // Previous mpiAPI response state will be reported 
            // to all slaves.
            //cout << "Sending KILL state to the slaves" << endl;
   
            packContinueMessage( not_finished );   
   
            // Send the message
            sendContinueMessage( master_lbcomm );
   
            mKillState = SEND;
          }
          // Second time around ("kill" was already sent to the slaves)
          else if( mKillState == SEND )
          {
             //cout << "wrapperMaster sets mKillState to KILL" << endl;

            // LDAS_LB_KILL was already sent to the searchMaster
            mKillState = KILL;
          }
      }
   
         //cout << "Master mKillState=" << mKillState << endl;
   
      // Report progress to the mpiAPI
      LdasRequest request;
      mNodes = request( mState, abs( dim ), ratio, progress, mMPIState, 
                        war_msg, err_msg );   
   

      if( ( mSynchronize && last_call ) ||
          mKillState == KILL )
      {
         // If asynchronous communication is enabled, there's no guarantee that
         // searchMaster/Slaves will be allowed to call applySearch() one more
         // time before job terminates (with current implementation)
         node_error( "Job is terminated by mpiAPI", MPI::COMM_WORLD, !loadBalance );
      }

   
      // This checking must be here, but for testing load balancing
      // without mpiAPI have to disable.
      if( enableMpiApi &&
          mState == LDAS_LB_CONTINUE && 
          ( mMPIState == LDAS_LB_ADD || 
            mMPIState == LDAS_LB_SUBTRACT ) )
      {
         node_error( "Invalid MPI response message" );
      }

   
      if( !allowAsynchronous )
      {
         // The very latest mpiAPI response state gets reported
         // to all slaves
         packContinueMessage( not_finished );
   
         // Send the message
         sendContinueMessage( master_lbcomm );
      }
   } // if masterNode
   else
   {
      receiveContinueMessage( master_lbcomm, lbcomm, not_finished ); 
   
      if( isAsynchronous( not_finished ) && mMPIState == LDAS_LB_KILL )
      {
         mKillState = SEND;
      }
   
         //cout << myNode << ": mKillState=" << mKillState << endl << endl;
   }
   
   
   // All nodes do actual load balancing
   rebuildCommunicators( lbcomm, master_lbcomm, c_lbcomm );

   
   mTimeSpent += MPI::Wtime();   
   return getMPIAction();
}

   
//-------------------------------------------------------------------------------   
//   
//: Sets synchronization flag.
//         
// This method sets a value for the synchronization between mpiAPI response
// and "continue/kill" instruction to the searchMaster of dso.
//   
//!param: const bool flag - Value to set flag to.
//
//!return: Nothing.
//   
void LdasLoadBalance::setSynchronize( const bool flag )
{
   mSynchronize = flag;
   return;
}
   
   
//-------------------------------------------------------------------------------   
//   
//: Get time spent in the functional.
//         
// This method gets a total time spent by the node in the functional.
// For diagnostic purposes only.   
//   
//!return: const REAL8 - Time spent in the functional.
//      
const REAL8 LdasLoadBalance::getTimeSpent()
{
   return mTimeSpent;
}
   

//-------------------------------------------------------------------------------   
//   
//: Gets mpiAPI instruction to search code.
//      
// This method returns positive number if some nodes have been added to the LB 
// communicator, negative - if some nodes have been subtracted from the 
// communicator, and zero if communicator is the same.   
//
//!return: const INT4 - Number of nodes added/subtructed to/from LB communicator.
//   
INT4 LdasLoadBalance::getMPIAction() const
{
   INT4 num( getNumNodes() );
   
   if( num && mMPIState == LDAS_LB_SUBTRACT )
   {
      num *= -1;
   }
   
   return num;
}


//-------------------------------------------------------------------------------   
//   
//: Rebuilds MPI communicators.
//         
// This method rebuilds all MPI communicators (only if load balancing is enabled).
//   
//!param: MPI::Intracomm& lbcomm - A reference to the load balancing communicator.
//!param: MPI::Intracomm& master_lbcomm - A reference to the communicator         
//+       consisting of load balancing nodes and master node of COMM_WORLD.   
//!param: MPI_Comm* c_lbcomm - A pointer to the C type load balancing communicator.   
//   
void LdasLoadBalance::rebuildCommunicators( MPI::Intracomm& lbcomm, 
   MPI::Intracomm& master_lbcomm, MPI_Comm* c_lbcomm )   
{
   if( loadBalance && mNum )
   {
#ifdef DEBUG_INFO   
      cout << "LB_NODES for " << myNode << ": ";
   
      copy( mNodes.begin(), mNodes.end(),
            ostream_iterator< INT4 >( cout, " " ) );
      cout << endl;
#endif   

   
      switch( mMPIState )
      {
         case(LDAS_LB_ADD):
         {
            copy( mNodes.begin(), mNodes.end(), back_inserter( nodes ) ); 
            break;
         }
         case(LDAS_LB_SUBTRACT):
         {
            for( vector< INT4 >::const_iterator iter = mNodes.begin(),
                 end_iter = mNodes.end(); iter != end_iter; ++iter )
            {
               nodes.erase( find( nodes.begin(), nodes.end(), *iter ) );
            }
   
            break;
         }   
         default:
         {
            break;
         }
      }

    
      // Recreate vector of nodes not in the LB communicator.
      createNotLBNodes( totalNodes );
   
   
      if( lbcomm != MPI::COMM_NULL )
      {
         lbcomm.Free();
      }
   
   
      if( master_lbcomm != MPI::COMM_NULL )
      {
         master_lbcomm.Free();
      }   
   
   
      // Create C++ type of communicators 
      lbcomm = DynamicIntracom( nodes );
   
      masterPlusNodes.erase( masterPlusNodes.begin(), masterPlusNodes.end() );
      masterPlusNodes = nodes;
      masterPlusNodes.insert( masterPlusNodes.begin(), masterNode );   
   
      master_lbcomm = DynamicIntracom( masterPlusNodes );
   
      // Create C type of communicator
      createComm( nodes, c_lbcomm );   
   }   
   
   return;
}


//------------------------------------------------------------------------------
//
//: isAsynchronous check.
//
// This method checks if communications between mpiAPI<->wrapperMaster and
// wrapperMaster<->searchMaster are asynchronous.
// 
//!param: const bool& not_finished - A flag to indicate if search code is done.
//   
//!return: const bool - True if asynchronous, false otherwise.
//       
const bool LdasLoadBalance::isAsynchronous( const bool& not_finished ) const
{
   return( !loadBalance && mSynchronize == false && not_finished );
}
   

//-------------------------------------------------------------------------------   
//   
//: Pack "continue" information.
//         
// This method is called only by master node. It packs "continue" information
// for the slave nodes.   
//   
//!param: const BOOLEAN& not_finished - Flag to indicate if search code is
//+       finished (for nodes outside of the load balancing communicator).
//   
//!return: Nothing.
//   
void LdasLoadBalance::packContinueMessage( const BOOLEAN& not_finished )
{
   if( myNode != masterNode )
   {
      return;
   }
   
   
   INT4  buf_position( 0 );      
   
   // Pack MPI state
   LDAS_MPI_INT4.Pack( &mMPIState, 1, mBuffer, mBufferSize, 
                       buf_position, MPI::COMM_WORLD );   
   
   // Pack not_finished flag (only for nodes outside of the communicator)
   LDAS_MPI_BOOLEAN.Pack( &not_finished, 1, mBuffer, mBufferSize, 
                          buf_position, MPI::COMM_WORLD );      
   
   
   // Pack nodes if loadBalancing 
   if( loadBalance )
   {
      mNum = mNodes.size();   
   
      // Pack number of nodes
      LDAS_MPI_UINT4.Pack( &mNum, 1, mBuffer, mBufferSize, 
                           buf_position, MPI::COMM_WORLD );      
   
   
      if( mNum )
      {
         // Validate new nodes to add/subtract
         switch( mMPIState )
         {
            case(LDAS_LB_ADD):
            {
               for( UINT4 i = 0; i < mNum; ++i )
               {
                  if( mNodes[ i ] >= totalNodes )
                  {
                     ostringstream s;
                     s << "mpiAPI requested node (for add) = " << mNodes[ i ]
                       << " exceeds the total number of nodes in COMM_WORLD.";
                     node_error( s.str().c_str() );
                  }
   
                  if( inRanks( nodes, mNodes[ i ] ) )
                  {
                     ostringstream s;
                     s << "mpiAPI requested node (for add) = " << mNodes[ i ] 
                       << " is already in LB communicator.";
                     node_error( s.str().c_str() );
                  }
               }
               break;
            }
            case(LDAS_LB_SUBTRACT):
            {
               vector< INT4 >::const_iterator nodes_begin( nodes.begin() ),
                                              nodes_end( nodes.end() ); 
   
               for( vector< INT4 >::const_iterator iter = mNodes.begin(),
                    end_iter = mNodes.end(); iter != end_iter; ++iter )
               {
                  if( find( nodes_begin, nodes_end, *iter ) == nodes_end )
                  {
                     ostringstream s;
   
                     s << "mpiAPI requested node (for subtract)= " << ( *iter )
                       << " is not in LB communicator.";
                     node_error( s.str().c_str() );
                  }
               }

               break;
            }   
            default:
            {
               break;
            }
         }
   
   
         for( UINT4 i = 0; i < mNum; ++i )
         {
            LDAS_MPI_INT4.Pack( &( mNodes[ i ] ), 1, mBuffer, 
                                mBufferSize, buf_position, MPI::COMM_WORLD );      
         }
      } // if mNum
   } // if loadBalance   
   
   return;
}  


//-------------------------------------------------------------------------------   
//   
//: Send "continue" information.
//         
// This method is called only by master node. It sends "continue" information
// to all slave nodes (if load balancing is enabled), or to the searchMaster.
//   
//!param: MPI::Intracomm& master_lbcomm - A reference to the communicator         
//+       consisting of load balancing nodes and master node of COMM_WORLD.   
//   
//!return: Nothing.
//      
void LdasLoadBalance::sendContinueMessage( const MPI::Intracomm& master_lbcomm )   
{   
   if( myNode != masterNode )
   {
      return;
   }
   
   
   if( loadBalance )
   {
      // If master is trying to terminate all processes they should 
      // receive this message right here.
      node_error.pass( MPI::COMM_WORLD, false );

   
      // Master broadcasts number of nodes, nodes( if loadBalancing )
      // and mpiAPI response message state to all nodes in COMM_WORLD, 
      // including itself.
      MPI::COMM_WORLD.Bcast( mBuffer, mBufferSize,
                             MPI::PACKED, masterNode );
      return;
   
   }

   
   // No load balancing
   if( LdasDebug )
   {
      cout << myNode << ": sending LB_INFO_TAG message to searchMaster"
           << endl;
   }
   
   // wrapperMaster sends the information only to the searchMaster and
   // nodes not in the LB communicator
   MPI::Request request( MPI::COMM_WORLD.Isend( mBuffer, mBufferSize, 
                            MPI::PACKED, nodes[ 0 ], LB_INFO_TAG ) ); 
   
   node_error.pass( request, master_lbcomm );   
   

   // Number of nodes not in LB communicator
   INT4 num( notNodes.size() );
   
   for( INT4 i = 0; i < num; ++i )
   {
      if( LdasDebug )
      {
         cout << myNode << ": sending LB_INFO_TAG message to notNodes ("
              << notNodes[ i ] << ")" << endl;
      }   
      request = MPI::COMM_WORLD.Isend( mBuffer, mBufferSize, 
                     MPI::PACKED, notNodes[ i ], LB_INFO_TAG ); 
      node_error.pass( request, master_lbcomm );            
   }
   
   return;
}
   

//-------------------------------------------------------------------------------   
//   
//: Receive "continue" information.
//         
// This method is called only by slave nodes of the COMM_WORLD. It receives
// "continue" information from wrapperAPI master node.
//   
//!param: const MPI::Intracomm& master_lbcomm - A reference to the communicator         
//+       consisting of load balancing nodes and master node of COMM_WORLD.   
//!param: const MPI::Intracomm& lbcomm - A reference to the load balancing 
//+       communicator.   
//!param: BOOLEAN& not_finished - Flag to indicate if search code is
//+       finished (for nodes outside of the load balancing communicator).   
//   
//!return: Nothing.
//      
void LdasLoadBalance::receiveContinueMessage( const MPI::Intracomm& master_lbcomm,
   const MPI::Intracomm& lbcomm, BOOLEAN& not_finished )   
{   
   if( myNode == masterNode )
   {
      return;
   }
   
   
   if( loadBalance )
   {
      // If master is trying to terminate all processes they should 
      // receive this message right here.
      node_error.pass( MPI::COMM_WORLD, false );

   
      // Master broadcasts number of nodes, nodes( if loadBalancing )
      // and mpiAPI response message state to all nodes in COMM_WORLD, 
      // including itself.
      MPI::COMM_WORLD.Bcast( mBuffer, mBufferSize,
                             MPI::PACKED, masterNode );
   }
   else
   if( myNode == nodes[ 0 ] || 
       inRanks( notNodes, myNode ) || 
       mMPIState == LDAS_LB_KILL ) // will guarantee not calling applySearch()
                                   // twice after 'kill' is received
   {
      // If "kill" was already received, terminate the process
      if( mMPIState == LDAS_LB_KILL )
      { 
         const bool is_p2p( true );
         node_error( "Job is terminated by mpiAPI", MPI::COMM_WORLD, is_p2p );         
      }
   
      // If wrapperMaster is not sending a "kill" message, just go on.   
      if( not_finished &&
          MPI::COMM_WORLD.Iprobe( masterNode, LB_INFO_TAG ) == false )
      {
         if( LdasDebug )
         {
            cout << myNode << " (LB): no pending LB_INFO_TAG message from wrapperMaster."
                 << endl;
         }
         return;
      }

   
      if( LdasDebug )
      {
         cout << myNode << " (LB): receiving LB_INFO_TAG message from wrapperMaster";
      }   
   
      REAL8 receiveTime( 0.0f );
      receiveTime -= MPI::Wtime();
      MPI::Request request( MPI::COMM_WORLD.Irecv( mBuffer, mBufferSize, 
                               MPI::PACKED, masterNode, LB_INFO_TAG ) );    

      node_error.pass( request,
                       ( myNode == nodes[ 0 ] ? master_lbcomm : 
                                                MPI::COMM_WORLD ) );   
      receiveTime += MPI::Wtime();
      if( LdasDebug )
      {
         cout << " (timeSpent=" << receiveTime << ")" << endl;
      }      
   }
   

   // Unpack the buffer
   if( loadBalance || // all slave nodes
       ( myNode == nodes[ 0 ] || inRanks( notNodes, myNode ) ) ) 
   {
      INT4  buf_position( 0 );      
   
      // Unpack MPI state
      LDAS_MPI_INT4.Unpack( mBuffer, mBufferSize, &mMPIState, 1, 
                            buf_position, MPI::COMM_WORLD );   
   
      BOOLEAN tmp;
   
      // Unpack not_finished flag
      LDAS_MPI_BOOLEAN.Unpack( mBuffer, mBufferSize, &tmp,
                               1, buf_position, MPI::COMM_WORLD );      
   
   
      // Node is not in the LB communicator, so it needs to know 
      // when to exit the while loop 
      if( lbcomm == MPI::COMM_NULL )
      {
         not_finished = tmp;
      }
    
       
      if( loadBalance )
      {
         // Unpack number of nodes if loadBalancing
         LDAS_MPI_UINT4.Unpack( mBuffer, mBufferSize, &mNum, 1, 
                                buf_position, MPI::COMM_WORLD );         
   
         if( mNum )
         {
            INT4 temp;
   
   
            for( UINT4 i = 0; i < mNum; ++i )
            {
               LDAS_MPI_INT4.Unpack( mBuffer, mBufferSize, &temp, 1,
                                     buf_position, MPI::COMM_WORLD );      
            
               mNodes.push_back( temp );
            }         
         } // if mNum
      } // if loadBalance
   }   

   return;
}
