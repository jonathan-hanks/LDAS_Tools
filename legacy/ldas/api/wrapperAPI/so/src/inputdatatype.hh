#ifndef WrapperApiInputTypeHH
#define WrapperApiInputTypeHH

// general header files
#include "general/unordered_map.hh"

// Local Header Files
#include "ldasdatatype.hh"   
#include "mpilaltypes.hh"   

   
//------------------------------------------------------------------------------
//
//: InputType class.
// 
// This class is a contaiment class for the inPut data structure to be passed as
// one of the arguments to the applyFilters processing functions. The 
// responsibility of this class differs based on the node which instantiates it.
// <p>When instantiated by the master node, it will parse ILWD format data and
// broadcast extracted metadata to the slaves, so that all slave nodes that will
// be receiving the input data will allocate the memory and construct MPI
// derived datatype to represent that ILWD formated data.   
// <p>This class only converts ILWD format data to the C data structures.
// OutputType class handles reverse conversion: from C data structures to
// the ILWD format elements.   
//   
class InputType : public LdasDatatype
{
   
public:
   
   /* Constructor */
   InputType( const INT4 rank = -1, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD );   

   /* Destructor */
   virtual ~InputType();

   /* Getter methods */  
   const REAL8 getDuration() const;
   const CHAR* getBottom() const; 
   inPut* getInputStruct();

   static void cleanup( inPut* input, const bool p2p = false,
      const MPI::Intracomm& comm = MPI::COMM_WORLD );

   static const REAL8 getCreateTime();
   static const REAL8 getRecvILwdTime();
   
private:   

   //: No default constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   InputType();
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   InputType( const MPI::Datatype& re );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   InputType( const InputType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //         
   InputType& operator=( const InputType& re ); 
   
   typedef General::unordered_map< std::string, domain, 
				   General::hash< std::string > >
   domainHash;   
   typedef domainHash::value_type domainValueType;     
   
   static const domainHash initDomainHash();
   
   /* Helper functions: called only by master node in communicator */   
   void parseILwd( ilwdConstVector& re, MPIStructInfo* dataInfo ) const; 
   ILwd::LdasElement* getILwdInput();
   static ILwd::LdasElement* const readSocketData();   
   static ILwd::LdasElement* const readFileData();   
   void writeILwdInput( const ILwd::LdasElement* e );
   
   const bool isNotEscaped( const std::string& s, INT4 pos ) const;
   
   /* Helper functions: called by all nodes in communicator */
   void buildType( MPI::Datatype& seqType, MPI::Datatype& stateType );

   void broadcastInfo( const ILwd::LdasElement* pe, UINT4& numStates, 
      MPIStructInfo** statesInfo, ilwdVector& state_vec,
      MPIStructInfo** sequencesInfo, ilwdVector& sequence_vec ); 
   
   virtual const CHAR* getDatatypeName() const;      
      
   inPut* copyData();

   //: Domain hash map
   static const domainHash mDomainHash;
   
   //: Number of MPI datatypes per inPut structure.
   static const UINT4 mTypeNum;
   
   //: Time spent in datatype generation.
   static REAL8 mCreateTime;
   
   //: Time spent receiving input data.
   static REAL8 mReceiveILwdTime;
  
   //: C inPut data structure to store the data
   inPut mInput;

};


//------------------------------------------------------------------------------
//
//: Gets an absolute address of the data.
//
// This method gets an absolute address of the inPut data structure.
//   
//!return: const CHAR* - An address "ZERO" of the mInput data structure. 
//      
inline const CHAR* InputType::getBottom() const
{
   if( mInput.states != 0 )
   {
      return mInput.states->stateName;
   }

   return mInput.sequences[ 0 ].name;
}

   
#endif // WrapperApiInputTypeHH  
