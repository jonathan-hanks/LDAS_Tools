#include "LDASConfig.h"

#include <stdlib.h>

#include <iostream>

// Local Header Files
#include "slavemanager.hh"
#include "mpicmdargs.hh"
#include "outputtype.hh"
#include "ldaserror.hh"
#include "ldastag.hh"   
#include "nodeinfo.hh"
#include "mpiutil.hh"   
#include "initvars.hh"   

   
//------------------------------------------------------------------------------
// 
//: Constructor.
//   
//!param: DLErrorHandler& dl_error - A reference to the dso error handler.
//!param: lbNodeInfo& lb_node - A reference to the object containing node       
//+       information.   
//!param: SearchOutput& search_output - A reference to the SearchOutput         
//+       structure containing the result data for the node.   
//   
SlaveDataManager::SlaveDataManager( DLErrorHandler& dl_error,
   lbNodeInfo& lb_node, SearchOutput& search_output )
   : NodeDataManager( dl_error, lb_node, search_output )
{}


//------------------------------------------------------------------------------
//
//: Overloaded call operator.
//
// This method is used to send result outPut data if any has been generated
// by applySearch() function of dso to the master by slave node. 
//
//!param: MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent( wrapperMaster + LBcommunicator ).
//!param: const UINT4 num_nodes - A number of nodes in LBcommunicator.          
//+       Default is 0.      
//
//!return: Nothing.
//
//!exc: Illegal number of outPut structures. - Illegal number of outPut          
//+     structures is specified: <= 0.
//!exc: Empty array of outPut structures when number of elements is not zero. -  
//+     Array of outPut data structures is empty even though number of        
//+     elements in it is not zero.
//!exc: Index exceeds the size of packing buffer. - Index into the binary        
//+     buffer used for storing the metadata exceeds the size of that buffer.      
//!exc: NULL store in stateVector. - Malformed outPut data structure, such as    
//+     one of the nodes in stateVector linked list has store set to NULL.      
//!exc: Inconsistent outPut data structure signature. - Sceleton for outPut      
//+     data structure differs from original signature. This error can only
//+     occure if communicateOutput=ONCE.   
//!exc: Undefined datatype. - Undefined datatype was specified.         
//!exc: Undefined domain. - Undefined domain was specified.                      
//+     One of TIME | FREQ | BOTH.         
//!exc: Invalid number of dataBase nodes. - Negative number of dataBase nodes   
//+     was specified.   
//!exc: Error generating DataBaseType. - Error generating a MPI derived         
//+     datatype to represent dataBase linked list.   
//!exc: Invalid number of stateVector nodes. - Negative number specified for        
//+     number of nodes in the linked list.      
//!exc: Error generating StateVectorType. - Error generating MPI derived        
//+     datatype representing the stateVector linked list.   
//!exc: Invalid number of history nodes specified. - Negative number of nodes    
//+     was specified.         
//!exc: Error generating HistoryType. - Error generating MPI derived datatype    
//+     to represent history linked list.   
//!exc: Error generating IntervalType. - Error generating MPI derived datatype   
//+     to represent C interval union.      
//!exc: Invalid number of multiDimData. - Negative number of array elements is   
//+     specified.         
//!exc: Error generating MultiDimDataType. - Error generating MPI derived        
//+     datatype to represent an array of C multiDimData structures.            
//!exc: Memory allocation failed for OutputType. - Error allocating memory. 
//!exc: Error generating OutputType. - Error generating MPI datatype to          
//+     represent an array of C outPut data structures.   
//      
void SlaveDataManager::operator()( const MPI::Intracomm& comm, 
   const UINT4 num_nodes )    
{
   // Update node information   
   mLBNode.rank = comm.Get_rank();
   mLBNode.numResults = mSearchOutput.numOutput;      
   mLBNode.fractionRemaining = mSearchOutput.fracRemaining;
   mLBNode.notFinished = mSearchOutput.notFinished;   
   
   // Concatenate any pending error/warning messages for the node
   mLBNode.setMessages( mDLError );
   
   mDLError.resetMessages();   
   commOutputTime -= MPI::Wtime();      
   
   // Send node information to the master( master node has always rank
   // 0, no matter which communicator ) 
   MPI::Request request( comm.Isend( &( mLBNode.numResults ), 1, 
                            mLBNode.nodeType, masterNode, NODE_INFO_TAG ) );   
   
   node_error.pass( request, comm );
   commOutputTime += MPI::Wtime();      

   // Construct OutputType MPI datatype and send the data
   if( mLBNode.numResults )
   {
      if( LdasDebug )
      {
         // Debugging information
         std::cout << "Before constructing OUTPUT( slave ): "
		   << myNode << std::endl; 
      }

   
      OutputType temp_output( &( mSearchOutput.result ), 
                              mLBNode.numResults, comm, masterNode );           
   
      // Send result to the master      
      if( communicateOutput )
      {
         commOutputTime -= MPI::Wtime();

         if( LdasDebug )
         {
            std::cout << "Before sending OUTPUT( slave ): " << myNode << std::endl;
         }
         request = comm.Isend( &( mSearchOutput.result[ 0 ].indexNumber ),
                               1, temp_output, masterNode, NODE_DATA_TAG );
      }
      else
      {
         if( OutputType::getInitState() == false )
         {
            setOutputType( temp_output );
            OutputType::setInitState( true );
         }
   
   
         commOutputTime -= MPI::Wtime();
   
         request = comm.Isend( &( mSearchOutput.result[ 0 ].indexNumber ),
                               1, getOutputType(), masterNode, NODE_DATA_TAG );              
      }

      node_error.pass( request, comm );
   
      commOutputTime += MPI::Wtime();   
   
   
      if( LdasDebug )
      {
         std::cout << mLBNode.rank << " ( COMM_WORLD=" << myNode 
		   << " ) after sending output to the master." << std::endl;
      }
   
   
      if( communicateOutput )   
      {
         temp_output.Free();              

         // Cleanup the result array of outPut data structures
         if( numDummyOutput )
         {
            // Memory cleanup
            OutputType::cleanup( mSearchOutput.result, 
                                 mSearchOutput.numOutput, comm );
   
            delete[] mSearchOutput.result;
            mSearchOutput.result = 0;
            mSearchOutput.numOutput = 0;
         }
         else
         {
            if( num_nodes )
            {
               CHAR* free_status( 0 );
               freeOutputTime -= MPI::Wtime();
   
               INT4 ret_code = mFreeOutput( &free_status, &mSearchOutput );           
   
               freeOutputTime += MPI::Wtime();   
               mDLError( "freeOutput", ret_code, free_status, comm );           
   
               if( free_status )
               {   
                  free( free_status );
                  free_status = 0;
               }
            }
            else
            {
               // This will be envoked only when numDummyState 
               // or numDummyMdd is not zero.
               OutputType::cleanup( mSearchOutput.result, 
                                    mSearchOutput.numOutput, comm );
   
               delete[] mSearchOutput.result;
               mSearchOutput.result = 0;
               mSearchOutput.numOutput = 0;            
            }
         }
      }
   }
   
   
   // Reset warning/error message for the node
   mLBNode.resetMessages();

   
   return;
}


//------------------------------------------------------------------------------
//
//: Finalize routine.
//
// This method is used to send finalizing message to the master node indicating
// that slave node is ready to finalize.
// This functionality is needed to provide proper job completion in case if
// master didn't receive all the data slaves were sending to it in the main 
// "while" loop of the job.   
//
//!param: const MPI::Intracomm& comm - A reference to the load balancing MPI 
//+       intracommunicator.       
//!param: CHAR* const msg - Finalizing message to send to the master.  
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.   
//   
void SlaveDataManager::finalizeMessage( const MPI::Intracomm& comm, 
   CHAR* const msg )
{
   // Node belongs to the lb communicator?
   const MPI::Intracomm& msg_comm( inRanks( nodes, myNode ) ? comm : 
                                                              MPI::COMM_WORLD );
   
   // Send message to the master that it's ready to finalize
   // ( master node has always rank 0, no matter which communicator ) 
   //std::cout << myNode << " before FINALIZE_TAG." << std::endl;
   MPI::Request request( msg_comm.Isend( msg, mMsgSize, LDAS_MPI_CHAR,
                                         masterNode, NODE_FINALIZE_TAG ) );   
   
   node_error.pass( request, msg_comm );   
   
   //std::cout << myNode << " after FINALIZE_TAG." << std::endl;   
   
   
   return deleteMPIDatatypes(); 
}
   

//------------------------------------------------------------------------------
//
//: Sends hostname information to the master.
//
// This method is used to send host name to the master node as part of 
// initialization process.
//
//!param: CHAR* const host - Host name of the node.   
//
//!return: Nothing.
//   
void SlaveDataManager::communicateHostName( CHAR* const host )
{  
   MPI::Request request( MPI::COMM_WORLD.Isend( host, MPI::MAX_PROCESSOR_NAME,
                            LDAS_MPI_CHAR, masterNode, NODE_HOSTNAME_TAG ) );
   
   node_error.pass( request );
   
   return;
}
