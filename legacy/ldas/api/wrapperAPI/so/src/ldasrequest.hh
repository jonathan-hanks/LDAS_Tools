#ifndef WrapperApiRequestHH
#define WrapperApiRequestHH

// System Header Files
#include <vector>   
#include <sstream>   

// LDAS header files
#include "general/util.hh"

// Local Header Files
#include "mpicmdargs.hh"   

// Forward declaration   
class Regex;
   
   
//-------------------------------------------------------------------------------   
//   
//: LDAS request functional.
//
// This functional is designed to submit requests to the mpiAPI and parse
// response messages by master node of the COMM_WORLD intracommunicator. 
// Master node is responsible to pass the mpiAPI response information to all 
// slaves in the communicator if load balancing is enabled. In a case when 
// load balancing is disabled, wrapperMaster passes response information only 
// to the searchMaster of the parallel job.   
//   
class LdasRequest
{
public:
   
   LdasRequest();

   void operator()( const std::string& warning, const MPI::Intracomm& comm );
   
   std::vector< INT4 > operator()( const LDAS_LB_STATE lb_state, 
      const INT4 num, const REAL4 ratio, const REAL4 progress, 
      LDAS_LB_STATE& mpi_state, const std::string& war_msg = "",
      const std::string& err_msg = "" ); 
   
   static const INT4 getRequestNum();
   static const INT4 getCurrentRequestNum();      
   static const INT4 getNextRequestNum();   
   static const std::string& getErrorString();

   //: Request to initialize.
   static const std::string mInitialize;
   
   //: Request to finalize.
   static const std::string mFinalize;
   
private:
   
   // Helper methods used for communication with mpiAPI
   std::vector< INT4 > processRequest( std::string& msg,
      LDAS_LB_STATE& mpi_state ) const;
   std::vector< INT4 > parseResponse( const std::string& msg,
      LDAS_LB_STATE& mpi_state ) const;
   
   void addWarning( std::ostringstream& s, const std::string& msg, 
      const bool new_line = true );
   void addError( std::ostringstream& s, const std::string& msg, 
      const bool new_line = true );
   
   //: Incrementing request number.
   static INT4 mRequest;
   
   //: Request to add nodes.
   static const std::string mAdd;
   
   //: Request to subtract nodes.
   static const std::string mSub;
   
   //: <b>warning</b> message format to send with request.
   static const std::string mWarning;
   
   //: <b>error</b> message format to send with request.   
   static const std::string mError;
   
   //: <b>progress</b> message format to send with request.  
   static const std::string mProgress;

   //: <b>using</b> message format to send with request.   
   static const std::string mUsing;
   
   //: <b>projected_ratio</b> message format to send with request.   
   static const std::string mRatio;
   
   //: Regular expression used for parsing mpiAPI <b>add</b> message.
   static const Regex re_add;
   
   //: Regular expression used for parsing mpiAPI <b>subtract</b> message.   
   static const Regex re_sub;
   
   //: Regular expression used for parsing mpiAPI <b>kill</b> message.   
   static const Regex re_kill;   
   
   //: Regular expression used for parsing mpiAPI <b>continue</b> message.   
   static const Regex re_cont;   
   
};
   
   
#endif   
