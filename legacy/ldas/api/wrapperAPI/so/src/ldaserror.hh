#ifndef WrapperAPILdasErrorHH
#define WrapperAPILdasErrorHH

#include "LDASConfig.h"

// MPI Header Files   
#if HAVE_MPI___H
#include <mpi++.h>
#elif HAVE_MPICXX_H
#include <mpicxx.h>
#endif

// System Header Files   
#include <string>   

// Local Header Files
#include "LALAtomicDatatypes.h"   
   
// Class declaration   
class std::bad_alloc;   
class LdasException;
class SwigException;   
   
   
//-----------------------------------------------------------------------------   
//   
//: NodeError functional.   
//   
// Utility functional to handle errors occured during the parallel job in
// collective and point-to-point communications.   
// <p>The functional has ability to send error messages by slaves to the master 
// node, and master node after receiving the message will notify other slaves
// in the communicator of an error.
//
class NodeError
{
public:
   
   NodeError();
  ~NodeError();

   void operator()( const CHAR* err_msg = 0, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true );
   
   void operator()( const std::bad_alloc& exc, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true );
   
   void operator()( const LdasException& exc, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true );
   
   void operator()( const SwigException& exc, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true );
   
   void operator()( const MPI::Exception& exc, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true );
   
   void pass( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true, const INT4 rank = MPI::ANY_SOURCE );
   void pass( MPI::Request& request, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD ); 
   void passLB( const MPI::Intracomm& comm ); 
   
   void reportError( const std::string& msg, 
      MPI::Intracomm& comm = MPI::COMM_WORLD ); 
   
   void wrapMasterMessage();
   void setNoMpiApi( const bool val );
   
private:

   //: Tags used for error handling communications.
   enum LDAS_ERROR_TAG
   {
      ERR_MESSAGE_TAG = 1040,
      CANCEL_TAG
   };
   
   enum 
   {
      LDAS_MAX_ERROR = 16384
   };

   void checkForFailures( const MPI::Intracomm& comm ); 
   
   void setMessage( const CHAR* msg ); 
   void updateMasterMessage();
   
   void terminate( const MPI::Intracomm& comm = MPI::COMM_WORLD );
   void masterTerminate();
   
#ifdef MASTER_TEMPLATE_FILTER_COMM      
   // Master and templateFilter communication method
   void probeWarnings() const;
#endif   

   //: Flag to indicate that mpiAPI is not available for error logging.
   static bool mNoMpiApi;
   
   //: How many errors occured if slave node,
   //: total number of errors reported if master node.
   static INT4 mErrorCode;
   
   //: Buffer used for sending/receiving error messages.
   CHAR mErrorMessage[ LDAS_MAX_ERROR ];
   
   //: All error messages accumulated by master node.
   std::string mMasterMessage;
   
};


//------------------------------------------------------------------------------
//
//: Sets state of mpiAPI availability.
//
// This method sets a flag within error handler if mpiAPI is not available for
// the communication to the wrapperAPI.   
//   
//!param: const bool val - Value to set the state to. True if mpiAPI is not
//+       available.   
//
//!return: Nothing.
//   
inline void NodeError::setNoMpiApi( const bool val ) 
{
   mNoMpiApi = val;
   
   return;
}
   
   
// Function declaration for error handler  
extern "C" {
void LdasErrorHandler(  MPI::Comm& c, INT4* ec, ... );
}

   
// Global object declaration
extern NodeError node_error;
   
   
#endif   
