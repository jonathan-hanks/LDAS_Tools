#include "LDASConfig.h"

// System Header Files
#include <new>
#include <algorithm>
#include <memory>   

// ILwd Header Files
#include <ilwd/ldascontainer.hh>      
   
// Local Header Files
#include "historytype.hh"   
#include "ldaserror.hh"
#include "mpidatautil.hh"   
#include "initvars.hh"   
   
using namespace std;   

using ILwd::LdasElement;   
using ILwd::LdasContainer;   
using ILwd::ID_ILWD;   

   
// Static data member initialization   
const string HistoryType::mILwdName( "LDAS_History" );
const UINT4 HistoryType::mTypeNum( 2 );   
const UINT4 HistoryType::mNameUnitsNum( maxHistoryName + maxHistoryUnits );   

   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor generates MPI derived datatype to represent C dcHistory 
// linked list.   
// <p>When called by master it initializes passed to it C dcHistory linked list 
// with the data of passed to it ILWD format element representing the linked 
// list.   
// <p>All nodes using C dcHistory structure for communication allocate the 
// memory to store history linked list and generate MPI derived datatype to 
// represent this linked list.   
//   
//!param: ilwdConstVector& history_vec - A reference to the vector of ILWD     
//+       elements representing history linked list. Only master node      
//+       contains valid data.   
//!param: const UINT4 num - A number of elements in the linked list.
//!param: dcHistory** h - A pointer to the history linked list being           
//+       initialized by master node.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator
//+       within which HistoryType object is defined. Default is MPI::COMM_WORLD.   
//   
//!exc: bad_cast - Input ILWD data is malformed.
//!exc: bad_alloc - Error allocating memory.
//!exc: Error generating HistoryType. - Error generating MPI derived datatype  
//+     to represent history.   
//!exc: Undefined datatype. - Undefined datatype for ILWD element              
//+     representing the data was specified.  
//!exc: Invalid number of history nodes specified. - Negative number of nodes 
//+     was specified.   
//!exc: MPI::Exception - MPI library exception.      
//   
HistoryType::HistoryType( ilwdConstVector& history_vec, 
   dcHistory** pe, const MPIListInfo* dataInfo, const UINT4 num,
   const INT4 rank, const MPI::Intracomm& comm ) 
   : LdasDatatype( comm, rank ), LdasData(), mNum( num )
{
   // There is history:
   if( mNum > 0 )
   {
      // All nodes: allocate memory for the linked list:
      allocateListData( pe, dataInfo );

   
      // Master node extracts data information and content.
      if( inMaster() )
      {
         parseILwd( history_vec, *pe );
      }

   
      // All nodes: create datatype for the history linked list
      buildType( *pe );
   }
   else if( mNum == 0 )
   {
      createNULLType();
   }
   else
   {
      node_error( "Invalid number of history nodes specified.", 
                  comm, isP2P() );
   }
}   

   
//------------------------------------------------------------------------------
//
//: Constructor.
//
// This constructor generates MPI derived datatype to represent C dcHistory 
// linked list.   
// This constructor will be called only by two nodes in the communicator using 
// created datatype for the communication: by slave node containing the data
// and master node collecting the data.
//   
//!param: dcHistory** pe - A pointer to the history linked list being           
//+       allocated by master, and containing valid data on slave nodes.
//!param: const MPIStructInfo& dataInfo - A reference to the metadata structure 
//+       for the history linked list.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which HistoryType object is defined. Default is             
//+       MPI::COMM_WORLD.   
//!param: const INT4 rank - Rank of the node sending the data. Default is -1.   
//   
//!exc: bad_alloc - Error allocating memory.
//!exc: Error generating HistoryType. - Error generating MPI derived datatype  
//+     to represent history linked list.   
//!exc: Undefined datatype. - Undefined datatype for ILWD element              
//+     representing the data was specified.  
//!exc: Invalid number of history nodes specified. - Negative number of nodes  
//+     was specified.   
//!exc: MPI::Exception - MPI library exception.      
//   
HistoryType::HistoryType( dcHistory** pe, const MPIStructInfo& dataInfo, 
   const MPI::Intracomm& comm, const INT4 rank ) 
   : LdasDatatype( comm, rank ), LdasData(), mNum( dataInfo.numHistory )
{
   if( mNum > 0 )
   {
      if( inMaster() )
      {
         // Master allocates memory for the linked list:
         allocateListData( pe, dataInfo.historyInfo );
      }
   
   
      // All nodes: Create datatype for the history linked list
      buildType( *pe );
   }
   else if( mNum == 0 )
   {
      createNULLType();
   }
   else
   {
      node_error( "Invalid number of history nodes specified.", comm );
   }
}   
   
   
//-----------------------------------------------------------------------------
//
//: Destructor.
//   
HistoryType::~HistoryType()   
{}
   

//-----------------------------------------------------------------------------
//   
//: Converts dcHistory linked list into ILWD format element.
//
// This method converts C linked list dcHistory into the corresponding ILWD
//   format element.
//
//!param: const dcHistory* pe - A pointer to the dcHistory linked list.
//!param: const MPI::Intracomm& comm - A reference to the intracommunicator 
//+       within which convertion occured.   
//   
//!return: LdasElement* const - A pointer to the ILWD format element.
//
//!exc: bad_alloc - Memory allocation failed.   
//!exc: Undefined datatype. - Undefined datatype was specified.   
//!exc: NULL dimensions array when number of dimensions != 0. - Dimensions
//+     array is null while number of dimensions is not 0.  
//!exc: The number of dimensions was zero or dims was null. - Illegal
//+     dimension values has been specified to generate ILWD format
//+     element.      
//   
LdasElement* const HistoryType::toILwd( const dcHistory* pe, 
      const MPI::Intracomm& comm )
{
   static const UINT4 numDims( 1 );
   const string name( "" );
   const dcHistory* curr( pe );
   
   auto_ptr< LdasContainer > c( new LdasContainer( mILwdName ) );

   
   while( curr != 0 )
   {
      auto_ptr< LdasContainer > c_elem( new LdasContainer( curr->name ) );
      c_elem->push_back( dataToILwd( curr->type, numDims,
                                     &( curr->numberValues ),
                                     curr->value, comm, 
                                     name, curr->units ),
                         ILwd::LdasContainer::NO_ALLOCATE,
                         ILwd::LdasContainer::OWN );    
      c->push_back( c_elem.release(),
                    ILwd::LdasContainer::NO_ALLOCATE,
                    ILwd::LdasContainer::OWN );


   
      curr = curr->next;
   }   

   
   return c.release();
}

   
//------------------------------------------------------------------------------
//
//: Gets name attribute for ILWD format element representing the dcHistory data.
//    
// This method gets name for the ILWD format element to represent the history.
//    
//!return: const string - Name.
//   
const string HistoryType::getILwdName()
{
   return mILwdName;
}
   
   
//-----------------------------------------------------------------------------
//   
//: Parse ILWD element.
//   
// This method to be called only by the master node! It initializes C dcHistory
// linked list with the data of passed to it ILWD format element.   
//
//!param: ilwdConstVector& re - A vector of ILwd Elements containing the data 
//+       that will be used for linked list initialization.   
//!param: dcHistory* ph - A pointer to the history linked list.   
//
//!return: Nothing.
//   
//!exc: Undefined datatype. - Undefined datatype for ILWD element             
//+     representing the data was specified.
//!exc: bad_alloc - Error allocating memory.   
//!exc: bad_cast - Malformed ILWD input.   
//   
void HistoryType::parseILwd( ilwdConstVector& re, dcHistory* ph ) const
{
   size_t s_size( 0 );
   dcHistory* curr_node( ph ); // Pointer to the current node in the list

   
   for( const_iterator iter = re.begin(), iter_end = re.end();
        iter != iter_end; ++iter )
   {
      // Get name of the data
      const string& name = ( *iter )->getNameString();
      s_size = std::min( static_cast< INT4 >( name.size() ), 
                         maxHistoryName - 1 );

      std::copy( name.begin(), name.begin() + s_size,
                 curr_node->name );
      curr_node->name[ s_size ] ='\0';
         
   
      // Get data units
      const string& units = getUnits( *iter );
      s_size = min( static_cast< INT4 >( units.size() ), 
                    maxHistoryUnits - 1 );      

      std::copy( units.begin(), units.begin() + s_size,
                 curr_node->units );
      curr_node->units[ s_size ] = '\0';
   
   
      // Get the data:
      setDataArray( *iter, &( curr_node->value ), curr_node->type,   
         curr_node->numberValues );
   
   
      curr_node = curr_node->next;
   }

   
   return;
}

   
//-----------------------------------------------------------------------------
//   
//: Allocates memory for the history linked list.
//
//!param: dcHistory** pe - A pointer to the dcHistory linked list to be 
//+       allocated.   
//   
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: Undefined datatype. - Undefined datatype for ILWD element             
//+     representing the data was specified.   
//   
void HistoryType::allocateListData( dcHistory** pe, 
   const MPIListInfo* historyInfo ) const
{
   dcHistory* curr_node( 0 );
   dcHistory* prev_node( 0 );
   bool first_node( true );
   

   for( UINT4 i = 0; i < mNum; ++i )
   {
      // Allocate memory for new node
      curr_node = new dcHistory;
      memset( curr_node, 0, sizeof( dcHistory ) );
   
      
      // Copy the data
      if( historyInfo != 0 )
      {
         curr_node->type = historyInfo[ i ].type;
         curr_node->numberValues = historyInfo[ i ].numData;
   
         // Allocate memory for data:
         allocateDataArray( &( curr_node->value ), curr_node->type,   
            curr_node->numberValues, getComm(), isP2P() );
      }
   
   
      if( first_node )
      {
         *pe = curr_node;
         first_node = false;
      }
      else
      {
         curr_node->previous = prev_node;
         prev_node->next = curr_node;
      }
   
      prev_node = curr_node;
   }

   
   return;
}
   

//-----------------------------------------------------------------------------
//
//: Build MPI derived datatype.
//
// This method should be called by all nodes using the created datatype for the 
// communication. It generates MPI derived datatype representing the dcHistory 
// linked list.
//   
//!param: dcHistory* pe - A pointer to the dcHistory linked list.   
//
//!exc: Error generating HistoryType. - Error creating MPI derived datatype to 
//+     represent history linked list.
//!exc: bad_alloc - Memory allocation failed.   
//!exc: MPI::Exception - MPI library exception.      
//   
void HistoryType::buildType( dcHistory* pe ) 
{
   const INT4 num( mNum * mTypeNum );
   
   vector< INT4 > blocks( num );
   vector< MPI::Datatype > types( num );
   vector< MPI::Aint > disps( num );


   // Current node in the linked list
   dcHistory* curr_node( pe );
   UINT4 i( 0 );
   INT4 i_num( 0 );   

   const MPI::Intracomm& comm( getComm() );
   const bool p2p( isP2P() );   
   
   
   // ONLY TO TEST ERROR HANDLER!!!
//    if( !inMaster() )
//    {
//       node_error( "Error generating history node datatype.", getComm(),
//                   collect );
//    }
//    node_error.pass( getComm(), collect, getRank() );   

   
   while( curr_node != 0 )
   {
      // Init name and units info 
      types[ i_num ]  = LDAS_MPI_CHAR;
      blocks[ i_num ] = mNameUnitsNum;
      disps[ i_num ]  = MPI::Get_address( curr_node->name );

   
      // Init value info   
      if( curr_node->numberValues )
      {
         ++i_num;
         types[ i_num ]  = dataArrayType( curr_node->type, comm, p2p ); 
         blocks[ i_num ] = curr_node->numberValues;
         disps[ i_num ]  = dataArrayDisplacement( curr_node->type, 
                              curr_node->value, comm, p2p );
      }
   
      curr_node = curr_node->next;
      ++i;
      ++i_num;
   }

   
   return createType( i_num, blocks, disps, types );   
}

   
//------------------------------------------------------------------------------
//
//: Gets name of the derived datatype.
//         
//!return: const CHAR* - Datatype name.
//   
const CHAR* HistoryType::getDatatypeName() const
{
   return "HistoryType";
}
   

   
   
