#include "LDASConfig.h"

// ILWD Header Files
#include <ilwd/ldasarray.hh>  
#include <ilwd/ldasstring.hh>   
#include <ilwd/ldascontainer.hh>   
   
using ILwd::ElementId;   
using ILwd::LdasElement; 
using ILwd::LdasArray; 
using ILwd::LdasArrayBase;      
using ILwd::LdasString;      
using ILwd::LdasContainer;         
using ILwd::ID_CHAR;
using ILwd::ID_CHAR_U; 
using ILwd::ID_INT_2S;
using ILwd::ID_INT_2U;
using ILwd::ID_INT_4S;
using ILwd::ID_INT_4U;
using ILwd::ID_INT_8S;
using ILwd::ID_INT_8U;
using ILwd::ID_REAL_4;
using ILwd::ID_REAL_8;
using ILwd::ID_COMPLEX_8;
using ILwd::ID_COMPLEX_16;   
using ILwd::ID_LSTRING;   
using ILwd::ID_EXTERNAL;
using ILwd::ID_ILWD;      

   
// System Header Files
#include <algorithm>
#include <functional>   
   
// Local Header Files
#include "mpidatautil.hh" 
#include "ldaserror.hh"      
#include "initvars.hh"   
#include "historytype.hh"   

using namespace std;   
using General::unordered_map;
   

typedef unordered_map< ElementId, datatype, ElementIdHash, ElementIdEql > ILwdHash;       
   
   
//------------------------------------------------------------------------------
//
//: Initialize ILwd element id hash map.
//   
ILwdHash initILwdHash() 
{
   ILwdHash h;
   
   h[ ID_CHAR       ] = char_s;
   h[ ID_CHAR_U     ] = char_u;
   h[ ID_INT_2S     ] = int_2s;
   h[ ID_INT_2U     ] = int_2u;
   h[ ID_INT_4S     ] = int_4s;
   h[ ID_INT_4U     ] = int_4u;
   h[ ID_INT_8S     ] = int_8s;
   h[ ID_INT_8U     ] = int_8u;
   h[ ID_REAL_4     ] = real_4;
   h[ ID_REAL_8     ] = real_8;
   h[ ID_COMPLEX_8  ] = complex_8;
   h[ ID_COMPLEX_16 ] = complex_16;
   h[ ID_LSTRING    ] = char_s;

   return h;
}
   

// Initialize static data 
static ILwdHash mILwdHash( initILwdHash() );
   
static bool appendHistoryName( false );   
   

//-----------------------------------------------------------------------------
//
//: Appends history elements to the compound history vector.
//       
// This method is used to extract history records from ILWD format element 
// and populate compound history vector with these records.
//
//!param: const LdasElement* e - A pointer to the history entry.
//!param: vector< const LdasElement* > * vp - A pointer to the compound history
//+       vector.
//
//!return: Nothing.
//   
//!exc: bad_cast - Malformed ILWD input data.
//!exc: bad_alloc - Memory allocation failed.
//   
static void appendHistory( const LdasElement* e,
   vector< const LdasElement* > * vp )
{
   if( e == 0 ||
       e->getElementId( ) != ID_ILWD )
   {
      throw bad_cast();
   }
   
   const LdasContainer* c( dynamic_cast< const LdasContainer* >( e ) );
   const string c_name( e->getNameString() );
   
   LdasContainer::const_iterator iter( c->begin() ),
                                 end_iter( c->end() );

   while( iter != end_iter )
   {
      if( *iter == 0 || 
          ( *iter )->getElementId() == ID_ILWD )
      {
         throw bad_cast();
      }

      if( appendHistoryName )
      {
         string name( c_name );
         string temp_name( ( *iter )->getNameString() );
         if( temp_name.empty() == false )
         {
            name += ':';
            name += temp_name;
         }
   
         if( name.empty() == false )
         {
            ( *iter )->setNameString( name );
         }
      }
   
      vp->push_back( *iter );
      ++iter;
   }
   
   return;
}
   
   
//-----------------------------------------------------------------------------
//
//: Gets units of the ILWD element.
//    
// This method gets units attribute of the ILWD data element.
//   
//!param: const LdasElement* re - A pointer to an ILwd Element representing   
//+       the data.   
//
//!return: const string& - Data units.
//  
//!exc: bad_cast - Input data is malformed. Only ILWD arrays and lstrings     
//+     have units attribute.  
//!exc: bad_alloc - Memory allocation failed.   
//   
const string getUnits( const LdasElement* pe )
{
   ElementId id( pe->getElementId() );
   
   string units;
   size_t i( 0 );
   
   
   switch( id )
   {
      case(ID_LSTRING):
      {
         const LdasString* s = dynamic_cast< const LdasString* >( pe );
         size_t dim( s->getNUnits() );
   
   
         while( dim-- != 0 )
         {
            units += s->getUnits( i );
            if( dim != 0 )
            {
               units += ':';
            }
            ++i;
         }
   
         break;
      }
      default:   
      {
         // All arrays:
         const LdasArrayBase* b = dynamic_cast< const LdasArrayBase* >( pe );

         // Dynamic cast on pointer sometimes throws bad_cast, sometimes doesn't.
         if( b == 0 )
         {
            throw bad_cast();
         }
   
         size_t dim( b->getNDim() );
   
   
         while( dim-- != 0 )
         {
            units += b->getUnits( i );
            if( dim != 0 )
            {
               units += ':';
            }
            ++i;
         }
   
         break;
      }
   }

   
   return units;
}
   
   
//-----------------------------------------------------------------------------
//
//: Gets size of the ILWD format element data.
//    
// This method gets dimension size of the ILWD data element.
//   
//!param: const LdasElement* re - A pointer to an ILwd Element representing   
//+       the data.   
//
//!return: const string& - Data units.
//  
//!exc: bad_cast - Input ILWD data is malformed.
// 
const size_t getDim( const LdasElement* pe )
{
   ElementId id( pe->getElementId() );
   size_t dim( 0 );
   
   
   switch( id )
   {
      case(ID_LSTRING):
      {
         const LdasString* s = dynamic_cast< const LdasString* >( pe );
         dim = s->getSize();
         break;
      }
      default:   
      {
         // If it's a container, will throw bad_cast
         // All arrays:
         const LdasArrayBase* b = dynamic_cast< const LdasArrayBase* >( pe );
   
         // Dynamic cast on pointer doesn't always throw bad_cast.   
         if( b == 0 )
         {
            throw bad_cast();
         }
   
         dim = b->getNData();
         break;
      }
   }

   
   return dim;
}
   

   
//------------------------------------------------------------------------------
//
//: Gets type of the ILWD format element data.
//    
// This method gets datatype of the passed to it ILWD element data.   
//   
//!param: const LdasElement* pe - A pointer to the ILWD Element representing   
//+       the data.   
//
//!return: datatype - Data type.
//  
//!exc: Undefined datatype. - Data of undefined type.
//!exc: Invalid boolean datatype. - ILwd element representing boolean data     
//+     must be of char_u type.   
//    
datatype getType( const LdasElement* pe, const string& elem_type )
{
   ILwdHash::const_iterator iter( mILwdHash.find( pe->getElementId() ) );

   
   if ( iter == mILwdHash.end() )
   {
      node_error( "Undefined datatype.", MPI::COMM_WORLD,
                  !dataDistributor );
   }

   
   datatype type( iter->second );   
   
   
   // Check if it's boolean datatype
   if( elem_type == "boolean" )
   {
      if( type != char_u )
      {
         node_error( "Invalid boolean datatype.", MPI::COMM_WORLD,
                     !dataDistributor );
      }
   
      type = boolean_lu;
   }
   
   
   return type;
}
   

//------------------------------------------------------------------------------
//
//: Extracts history.
//
// This method extracts elements representing dcHistory linked list from passed 
// to it ILWD container.
//   
//!param: const LdasElement* re - An ILWD element representing one data sequence.
//!param: const bool append_names - A flag to indicate if history names must
//+       be concatenated. Default is false.  
//   
//!return: vector< const LdasElement* > - A vector of ILWD elements             
//+        representing the history linked list.
//   
//!exc: bad_alloc - Error allocating memory.
//!exc: bad_cast - Malformed ILWD input data.   
//   
vector< const LdasElement* > getHistory( const LdasElement* pe,
   const bool append_names )
{
   if( pe == 0 || 
       pe->getElementId() != ID_ILWD )
   {
      throw bad_cast();
   }
   
   const LdasElement* h( dynamic_cast< const LdasContainer* >( pe )->find( 
                            HistoryType::getILwdName() ) );
   
   vector< const LdasElement* > temp;  
   if( h == 0 )
   {
      return temp;
   }

   appendHistoryName = append_names;
   
   const LdasContainer& c( dynamic_cast< const LdasContainer& >( *h ) );
   
   for_each( c.begin(), c.end(), 
             bind2nd( ptr_fun( appendHistory ), &temp ) );

   appendHistoryName = false;
   return temp;
}
   
