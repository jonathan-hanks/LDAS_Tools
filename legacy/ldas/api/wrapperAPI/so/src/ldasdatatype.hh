#ifndef WrapperApiLdasDatatypeHH
#define WrapperApiLdasDatatypeHH

// System Header Files
#include <vector>   
#include <exception>
   
// ILWD Header Files
namespace ILwd
{
   class LdasElement;
   class LdasContainer;
}

   
// Local Header Files   
#include "wrapperInterfaceDatatypes.h"  
   
   
//------------------------------------------------------------------------------
//
//: LdasDatatype class.
// 
// A virtual base class for LDAS MPI derived datatypes. 
//
class LdasDatatype : public MPI::Datatype
{
   
public:
   
   /* Default constructor */
   LdasDatatype( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   /* Destructor */
   virtual ~LdasDatatype();

   /* Assignment operator */
   LdasDatatype& operator=( const MPI::Datatype& re );         
   
   /* Equal comparison operator. */
   bool operator==( const MPI::Datatype& re ) const;
   bool operator!=( const MPI::Datatype& re ) const;
   
   /* Helper methods for optimization debugging */
   static REAL8 getPackDelta();   
   static REAL8 getBcastDelta();

   typedef std::vector< ILwd::LdasElement* > ilwdResultVector;            
   
protected:

   /* Typedefs */
   typedef const std::vector< const ILwd::LdasElement* > ilwdConstVector;   
   typedef ilwdConstVector::const_iterator const_iterator;
   typedef std::vector< const ILwd::LdasElement* > ilwdVector;      
   typedef ilwdVector::const_iterator iterator;
   typedef ilwdResultVector::const_iterator result_iterator;   

   
   /* Getter methods called by all nodes in communicator */
   const bool inMaster() const;
   const MPI::Intracomm& getComm() const;
   const INT4 getRank() const;
   const bool isP2P() const;
   
   /* Create MPI datatype, called by all nodes in communicator */
   void createType( const INT4& num, const std::vector< INT4 >& blocks, 
                    std::vector< MPI::Aint >& disp, 
                    const std::vector< MPI::Datatype >& types );
   void createNULLType();
   virtual const CHAR* getDatatypeName() const = 0;
   
   /* Search methods called by master node only. */
//!ignore_begin:   
// Can't use ilwdVector in return type
// because perceps won't create proper link for the documentation.
//!ignore_end:         
   std::vector< const ILwd::LdasElement* > findILwd( 
      const ILwd::LdasContainer& re, 
      const std::string& namekey, size_t i = 1, 
      const bool check_empty = true ) const; 
   
   const ILwd::LdasElement* findElement( const ILwd::LdasElement& re, 
      const std::string& name, size_t = 0, const bool required = true ) const;
   const ILwd::LdasElement* findContainerElement( 
      const ILwd::LdasContainer& re, const CHAR* name, size_t = 0 ) const;   
   
   static void vectorCleanup( ilwdResultVector& v );

   //: Name position for the 'primary' keyword.   
   static const INT4 mPrimaryPos;
   
   // Debugging data members
   //: Total time spent in packing metadata for LdasDatatype's
   static REAL8 mPackDelta;
   //: Not  used. 
   static REAL8 mBcastDelta;
   //: Total time spent in broadcasting metadata for OutputType
   static REAL8 mBcastOnlyInInfoDelta;   
   
private:   

   //
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference 
   // counting is implemented.</font>      
   //   
   LdasDatatype( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD, 
      const INT4 rank = -1 );
   
   //
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference 
   // counting is implemented.</font>   
   //      
   LdasDatatype( const LdasDatatype& );

   //
   //: No assignment operator.
   //      
   // <font color="red">This method should not be used unless reference 
   // counting is implemented.</font>      
   //
   LdasDatatype& operator=( const LdasDatatype& re ); 
   
   //: MPI intracommunicator within which datatype is defined
   const MPI::Intracomm& mComm;

   //: Rank of the node sending/receiving the data
   INT4 mRank;
   
   //: Flag if object belongs to the master node of communicator
   bool mMaster;   

};

   
//------------------------------------------------------------------------------
//
//: inMaster method.
//
// This method detects if process is run by the master node of the 
// communicator.   
//   
//!return: bool - True if it is a master node, false otherwise.
//   
inline const bool LdasDatatype::inMaster() const
{
   return mMaster;
}
   

//------------------------------------------------------------------------------
//
//: Gets intracommunicator.
//   
// This method gets the reference to the MPI intracommunicator within which
// datatype is defined.
//   
//!return: MPI::Intracomm& - A reference to the intracommunicator within which +
//!return:    datatype is defined.
//   
inline const MPI::Intracomm& LdasDatatype::getComm() const
{
   return mComm;
}

   
//------------------------------------------------------------------------------
//
//: Gets rank of node process is communicating to.
//
// This method gets the rank of the node that current node is communicating to.
//   
//!return: const INT4 - Rank of the node.
//  
inline const INT4 LdasDatatype::getRank() const
{
   return mRank;
}

   
//------------------------------------------------------------------------------
//
//: Gets rank of node process is communicating to.
//
// This method gets the rank of the node that current node is communicating to.
//   
//!return: const INT4 - Rank of the node.
//  
inline const bool LdasDatatype::isP2P() const
{
   return ( mRank >= 0 );
}
   
   
//------------------------------------------------------------------------------
//
//: Gets time spent in packing metadata for OutputType.
//
// <font color='green'>For debugging purposes only!!!</font>   
//
//!return: REAL8 - Time value.
//         
inline REAL8 LdasDatatype::getPackDelta()
{
   return mPackDelta;
}         
   
   
//------------------------------------------------------------------------------
//
//: Gets time spent in broadcasting metadata for LdasDatatype's.
//
// <font color='green'>For debugging purposes only!!!</font>   
//
//!return: REAL8 - Time value.
//            
inline REAL8 LdasDatatype::getBcastDelta()
{
   return mBcastDelta;
}            

   
#endif // WrapperApiLdasDatatypeHH  
