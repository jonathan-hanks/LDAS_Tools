#include "LDASConfig.h"

// Local Header Files
#include "dynamiccom.hh"
#include "ldaserror.hh"   

#include <general/autoarray.hh>
   
using namespace std;
   

//------------------------------------------------------------------------------
//
//: Constructor.
//
// Constructs new object of DynamicIntracom class by specifying 
// the ranks of the processes from <i>comm</i> communicator to be included 
// into the new one.
//!ignore_begin:   
// Use 'std' namespace in method arguments( even though we said
// 'using namespace std' ) because perceps won't create proper link 
// for the documentation if we don't use it.
//!ignore_end:      
//
//!param: const vector< INT4 >& ranks - Ranks of the processes in the          
//+       <i>comm</i> communicator to appear in a group for new communicator.
//!param: const MPI::Intracomm& comm - A reference to the intracommunicator    
//+       within which domain new communicator will be generated. Default   
//+       is MPI::COMM_WORLD.   
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: Empty node vector for DynamicIntracom communicator. - Empty node vector 
//+     is passed for dynamic load balancing communicator generation.
//!exc: MPI::Exception - MPI library exception.   
//
DynamicIntracom::DynamicIntracom( const std::vector< INT4 >& ranks, 
   const MPI::Intracomm& comm )
   : MPI::Intracomm()
{    
     if( ranks.size() == 0 )
     {
        node_error( "Empty node vector for DynamicIntracom communicator.",
                    comm, false );
     }
   
   
     // Get the group under comm communicator.
     MPI::Group worldGroup( comm.Get_group() );

   
     // Create new group.
     const UINT4 num( ranks.size() );
   
     General::AutoArray< INT4 > nodes( new INT4[ num ] );
   
   
     for( UINT4 i = 0; i < num; ++i )
     {
        nodes[ i ] = ranks[ i ];
     }
   
   
     MPI::Group newGroup( worldGroup.Incl( num, nodes.get() ) );
   
   
     // Create new communicator.
     // MPI::COMM_WORLD.Create() is a collective call:
     // the requirement is that all processes in the old group( 
     // worldGroup ) participate in the Greate() call.
     *this = comm.Create( newGroup );

   
     // Mark the new group for deallocation( this group will not be 
     // deallocated until all references to it have been freed: 
     // communicators contain internal references to their process 
     // groups ==> need to free the communicator in order to free
     // the group ).
     newGroup.Free();
   
   
     // Free a reference to the COMM_WORLD group.
     worldGroup.Free();
}

   
//------------------------------------------------------------------------------
//
//: Destructor.
//   
DynamicIntracom::~DynamicIntracom()
{}
   

//------------------------------------------------------------------------------
//
//: Assignment operator.
//
// MPI assignment is handle-based( shallow assignment ). It is possible but is
// not generally advised to create alliases of the communicators:
// comm1 = comm2( creates a possibility to have dangling references to the
// object ).
//   
//!param: const MPI::Intracomm& c - A reference to the MPI intracommunicator   
//+       to assign from.
//
//!return: DynamicIntracom& - A reference to this object.
//   
DynamicIntracom& DynamicIntracom::operator=( const MPI::Intracomm& c )
{
   if( this != &c )
   {
      // MPI::Intracomm assignment operator is handle-base( shallow )
      // assignment.
      MPI::Intracomm::operator=( c );
   }
   
   return *this;
}     
   

//------------------------------------------------------------------------------
//
//: Equal comparison operator.
//
//!param: const MPI::Intracomm& c - A reference to the MPI intracommunicator   
//+       compare to.
//
//!return: bool - True if intracommunicators are equal, false otherwise.
//      
bool DynamicIntracom::operator==( const MPI::Intracomm& c ) const
{
   if( this == &c )
   {
      return true;
   }
 
   return ( MPI::Intracomm::operator==( c ) );
}     
   

//------------------------------------------------------------------------------
//
//: Not equal comparison operator.
//   
//!param: const MPI::Intracomm& c - A reference to the MPI intracommunicator   
//+       to compare to.
//
//!return: bool - True if communicators are not equal, false otherwise.
//   
bool DynamicIntracom::operator!=( const MPI::Intracomm& c ) const 
{
   return !operator==( c );
}        

   
   
   



