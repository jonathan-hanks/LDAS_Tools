#ifndef MasterManagerHH
#define MasterManagerHH

// System Header Files
#include <string>
#include <vector>   
#include <sstream>   
   
// Local Header Files
#include "nodemanager.hh"   
   

namespace ILwd
{
   class LdasContainer;   
}
   
   
//------------------------------------------------------------------------------
//   
//: Functional for data management by wrapperAPI master node.
//      
class MasterDataManager : public NodeDataManager   
{
public:

   MasterDataManager( DLErrorHandler& dl_error, lbNodeInfo& lb_node,
      SearchOutput& search_output, const REAL8 start_time, 
      const std::string name = "output:wrapperAPI" );

   ~MasterDataManager();
   
   void operator()( const MPI::Intracomm& comm, const UINT4 num_nodes = 0 );

   static void setConnectNum( const UINT4 num );
   static void setConnectDelay( const UINT4 num );
   static void setOutputDelay( const UINT4 num );
   
private:

   REAL8 pollData( const MPI::Intracomm& comm, const UINT4 num_nodes );   
   void communicateHostName( CHAR* const host );         
   void finalizeMessage( const MPI::Intracomm& comm, CHAR* const msg );            
   void sendFinalInfo( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = false );      
   REAL8 receiveData( const MPI::Intracomm& comm, const INT4 source_rank );
   void sendProcessInfo( const MPI::Intracomm& comm, 
      const bool init = true );   
   void sendData( const MPI::Intracomm& comm );
   void sendILwdElement( ILwd::LdasContainer* const e, 
      const MPI::Intracomm& comm );
   void writeILwdElement( ILwd::LdasContainer* const e,
      const std::string& name, const MPI::Intracomm& comm );

   void initTimeoutHash( const bool init_all_slaves = false );
   void updateTimeoutHash( const MPI::Intracomm& comm, const INT4 rank );
   void checkTimeout( const MPI::Intracomm& comm );
   void totalWrapperTime( std::ostringstream& s );
   
   const bool canSendData() const;
   
   //: Warning message timeout.
   static const INT4 mWarningTimeout;
   
   //: Last time master was collecting data from slaves.
   static INT4 mLastTimeSeconds;
   
   //: Incrementing file counter for writing outPut data in ILWD format.
   static UINT4 mFileCounter; 
   
   //: File name used for writing outPut data in ILWD format.
   static const std::string mFileName;  
   
   //: File extension used for writing outPut data in ILWD format.
   static const std::string mFileExtension; 

   //: Search master rank.
   //
   // Search master always has rank = 1 in any masterPlusLB communicator.
   static const INT4 mSearchMaster;   

   //: Vector of slave nodes that sent their data to the master.
   static std::vector< INT4 > mReceivedSlaves;
   
   //: Number of attempts to connect to the resultAPI
   static UINT4 mConnectNum;
   
   //: Time delay in microseconds between connection attempts.
   static UINT4 mConnectDelay;
   
   //: Time delay in seconds between ILWD output sends.
   //: Master node will buffer all outPut data structures
   //: for so many seconds before sending them to the resultAPI.
   static UINT4 mOutputDelay;
   
   //: Metadata attribute for final process ilwd object
   static const std::string mProcessILwdMeta;
   
   //: Metadata attribute for output ilwd object
   static const std::string mOutputILwdMeta;   
   
   
   //: A pointer to the ILWD element representing array of C outPut 
   //: data structures.
   ILwd::LdasContainer* mResultILwd;

   //: Vector used to track each slave in COMM_WORLD
   std::vector< INT4 > mTimeout;
   
   //: Vector used to trace COMM_WORLD slave's timeout
   std::vector< INT4 > mTimeoutCounter;   

   //: Start time of the process
   REAL8 mStartTime;
   
   //: Time the very last ILWD format outPut has been sent to the 
   //: eventmonAPI.
   REAL8 mOutputSentTime;
   
   //: Flag to indicate if metadata attribute 'mOutputILwdMeta' is set
   bool mOutputILwdMetaSet;
   
};
   
   
#endif   
