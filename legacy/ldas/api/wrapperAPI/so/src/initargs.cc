#include "LDASConfig.h"

#define InitArgsCC

// System Header Files   
#include <algorithm>   
#include <ctime> 
#include <sstream>   
#include <fstream>   
#include <algorithm>   
#include <cstdlib>   
#include <iterator>   
   
// General Header Files
#include "general/regexmatch.hh"
#include "general/gpstime.hh"   

using General::GPSTime;     
   
// LDAS Header Files   
#include <ldas/ldasconfig.hh>   
   
// DBAccess Header Files
#include <dbaccess/dbaccess.h>
#include <dbaccess/Table.hh>   
#include <dbaccess/Process.hh>
#include <dbaccess/ProcessParams.hh>   
#include <dbaccess/Transaction.hh>   

using DB::Transaction;
using DB::Table;   
   
// ILWD Header Files
#include <ilwd/ldascontainer.hh>   

using ILwd::LdasContainer;   
   
// Local Header Files
#include "initargs.hh"
#include "ldaserror.hh"   
#include "mpiutil.hh"   
#include "mpicmdargs.hh"   
#include "mastermanager.hh"   
#include "msgsocket.hh"   
#include "loadbalance.hh"   

using namespace std;

   
//------------------------------------------------------------------------------
//
//: Detects if character is not a space character.
//
//!param: const CHAR& c - A reference to the character to examine.
//
//!return: const bool - True if character is not a space, false otherwise.
//   
static const bool notSpace( const CHAR& c )
{
   return !isspace( c );
}  

   
//------------------------------------------------------------------------------
//
//: Detects if character is a space character.
//
//!param: const CHAR& c - A reference to the character to examine.
//
//!return: const bool - True if character is a space, false otherwise.
//   
static const bool isSpace( const CHAR& c )
{
   return isspace( c );
}  
   
   
//!ignore_begin:   

// InitArguments static data initialization   
bool   InitArguments::mInitState( true );
string InitArguments::mErrorMsg;   

   
/*
 * Regex details for the -dynlib command line argument.
 *
 * "^"                     The beginning of the buffer
 * "[[:space:]]*"          Optional space
 * "="                     =
 * "[[:space:]]*"          Optional space
 * "("                     Remember the match
 *    "[-0-9a-zA-Z/_~.]*"  Any word( can be empty )
 * ")"                     End of the match
 * "[[:space:]]*"          Optional space
 * "$"                     End of the buffer
 */   
const Regex InitArguments::re_path( 
   "^[[:space:]]*([-0-9a-zA-Z/_~.]*)[[:space:]]*$" );   

   
/*
 * Regex details for the string command line argument.
 *
 * "^"                            The beginning of the buffer
 * "[[:space:]]*"                 Optional space
 * "("                            Remember the match
 *    "[-a-zA-Z0-9_/\\{}() .~]+"  String( at least one character )
 * ")"                            End of match
 * "[[:space:]]*"                 Optional space
 * "$"                            followed by the end of the buffer
 */   
const Regex InitArguments::re_string(
   "^[[:space:]]*"
   "([-:a-zA-Z0-9_/\\{}() .~]*)[[:space:]]*$" );   
  
   
/*
 * Regex details for the unsigned integer command line argument.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "="                 =
 * "[[:space:]]*"      Optional space
 * "("                 Remember the match
 *    "[0-9]+"         At least one digit
 * ")"                 End of the match
 * "[[:space:]]*"      Optional space
 * "$"                 End of the buffer
 */   
const Regex InitArguments::re_uint( 
   "^[[:space:]]*([0-9]+)[[:space:]]*$" );   
   
   
/*
 * Regex details for the -realTimeRatio command line argument.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "="                 =
 * "[[:space:]]*"      Optional space
 * "("                 Remember the match
 *    "[0-9]"          One digit( value should be more or equal to 1 )
 *    "."              followed by .
 *    "[0-9]+"         followed by any amount of digits( one is required )
 * ")"                 End of the match
 * "[[:space:]]*"      Optional space
 * "$"                 End of the buffer
 */   
const Regex InitArguments::re_float( 
   "^[[:space:]]*([0-9]+.[0-9]+)[[:space:]]*$" );      
   
   
/*
 * Regex details for the -doLoadBalance command line argument.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "="                 =
 * "[[:space:]]*"      Optional space
 * "("                 Remember the match
 *    "T"              T letter 
 *    "(RUE)?"         or TRUE word
 *    "|"              OR
 *    "F"              F letter 
 *    "(ALSE)?"        or FALSE word
 * ")"                 End of the match
 * "[[:space:]]*"      Optional space
 * "$"                 End of the buffer
 */   
const Regex InitArguments::re_bool( 
   "^[[:space:]]*(T(RUE)?|F(ALSE)?)[[:space:]]*$" );            

   
/*
 * Regex details for the -dataDistributor command line argument.
 *
 * "^"                  The beginning of the buffer
 * "[[:space:]]*"       Optional space
 * "="                  =
 * "[[:space:]]*"       Optional space
 * "("                  Remember the match
 *    "W"               W letter 
 *    "(RAPPER)?"       or WRAPPER word
 *    "|"               OR
 *    "S"               S letter 
 *    "(EARCHMASTER)?"  or SEARCHMASTER word
 * ")"                  End of the match
 * "[[:space:]]*"       Optional space
 * "$"                  End of the buffer
 */   
const Regex InitArguments::re_distributor( 
   "^[[:space:]]*(W(RAPPER)?|S(EARCHMASTER)?)[[:space:]]*$" );         
   

/*
 * Regex details for the -communicateOutput command line argument.
 *
 * "^"                  The beginning of the buffer
 * "[[:space:]]*"       Optional space
 * "="                  =
 * "[[:space:]]*"       Optional space
 * "("                  Remember the match
 *    "A"               A letter 
 *    "(LWAYS)?"        or ALWAYS word
 *    "|"               OR
 *    "O"               O letter 
 *    "(NCE)?"          or ONCE word
 * ")"                  End of the match
 * "[[:space:]]*"       Optional space
 * "$"                  End of the buffer
 */   
const Regex InitArguments::re_communicate( 
   "^[[:space:]]*(A(LWAYS)?|O(NCE)?)[[:space:]]*$" );         

   
const string InitArguments::mProgramName( "wrapperAPI" );      
   
const string InitArguments::mTotalNodes( "-np=" );               
const string InitArguments::mMpiAPI( "-mpiAPI=" );               
const string InitArguments::mNodeList( "-nodelist=" );                  
const string InitArguments::mDynlib( "-dynlib=" );                  
const string InitArguments::mDataAPI( "-dataAPI=" );               
const string InitArguments::mResultAPI( "-resultAPI=" );                  
const string InitArguments::mFilterParams( "-filterparams=" );                     
const string InitArguments::mRealTimeRatio( "-realTimeRatio=" );                        
const string InitArguments::mDoLoadBalance( "-doLoadBalance=" );                           
const string InitArguments::mDataDistributor( "-dataDistributor=" );                              
const string InitArguments::mJobID( "-jobID=" );                                 
const string InitArguments::mCommunicateOutput( "-communicateOutput=" );                                       
const string InitArguments::mMemoryUsageLimit( "-memoryUsageLimit=" );   
const string InitArguments::mUserTag( "-userTag=" );      
   
const string InitArguments::mConnectResultApiNum( "resultAPI_connection_attempt" );   
const string InitArguments::mConnectResultApiDelay( "resultAPI_connection_delay" );      
const string InitArguments::mSendILwdOutputDelay( "send_ilwd_output_delay" );         
const string InitArguments::mConnectMpiApiDelay( "mpiAPI_connection_delay" );         
const string InitArguments::mMpiApiResponseTimeout( "mpiAPI_response_timeout" );               
const string InitArguments::mSyncMpiApiDso( 
   "synchronize_mpiApi_dsoMaster" );            
   
const string InitArguments::mEnableMpiApi( "enable_mpiAPI" );
const string InitArguments::mEnableDumpInput( "enable_dump_input" );
const string InitArguments::mDumpDataDir( "dump_data_directory" );   
const string InitArguments::mRunCode( "run_code" );      
const string InitArguments::mEnableResultApi( "enable_resultAPI" );
const string InitArguments::mEnableDummyOutput( "enable_dummy_output" );
const string InitArguments::mEnableDummyState( "enable_dummy_state" );
const string InitArguments::mEnableDummyMdd( "enable_dummy_mdd" );
const string InitArguments::mEnableLoadDso( "enable_load_dso" );
const string InitArguments::mEnableSleepBeforeDso( "enable_sleep_before_dso" );
const string InitArguments::mEnableSleepAfterDso( "enable_sleep_after_dso" );
const string InitArguments::mEnableLdasTrace( "enable_ldas_trace" );
const string InitArguments::mEnableXmpiTrace( "enable_xmpi_trace" );
const string InitArguments::mEnableDumpArgs( "enable_dump_args" );   
const string InitArguments::mEnableTimeoutTrace( "enable_timeout_trace" );      
const string InitArguments::mEnableTimeInfo( "enable_time_info" );         

string InitArguments::mDumpDataDirValue( "/ldas_outgoing/jobs" );
string InitArguments::mRunCodeValue( "NORMAL" );   
   
   
string InitArguments::mDSOParams( "" );

  
//------------------------------------------------------------------------------
//
//: Initializes RscFileHash unorderd_map.
//
// This method initializes the resource file unorderd_map, where key is string
// representing the name of the argument in the resource file   
// and value is a member function to use for parsing argument's value.
//   
//!return: const RscFileHash - unorderd_map.
//
//!exc: bad_alloc - Memory allocation failed.
//      
const InitArguments::RscFileHash InitArguments::initRscFileHash()
{
   static const size_t start_pos( 1 );
   typedef RscFileHash::value_type rscValue;       
   
   RscFileHash h;   
   h.insert( rscValue( string(  mMpiAPI, start_pos, mMpiAPI.size() - 2 ),
                       &InitArguments::initMpiAPI ) );
   h.insert( rscValue( string(  mDataAPI, start_pos, mDataAPI.size() - 2 ),
                       &InitArguments::initDataAPI ) );
   h.insert( rscValue( string(  mResultAPI, start_pos, mResultAPI.size() - 2 ),
                       &InitArguments::initResultAPI ) );
   h.insert( rscValue( string(  mRealTimeRatio, start_pos, 
                                mRealTimeRatio.size() - 2 ),
                       &InitArguments::initRealTimeRatio ) );
   h.insert( rscValue( string(  mDoLoadBalance, start_pos, 
                                mDoLoadBalance.size() - 2 ),
                       &InitArguments::initDoLoadBalance ) );
   h.insert( rscValue( string(  mDataDistributor, start_pos, 
                                mDataDistributor.size() - 2 ),
                       &InitArguments::initDataDistributor ) );
   h.insert( rscValue( string(  mCommunicateOutput, start_pos, 
                                mCommunicateOutput.size() - 2 ),
                       &InitArguments::initCommunicateOutput ) );
   h.insert( rscValue( string(  mMemoryUsageLimit, start_pos, 
                                mMemoryUsageLimit.size() - 2 ),
                       &InitArguments::initMemoryUsageLimit ) );

   // Connect parameters
   h.insert( rscValue( mConnectResultApiNum, 
                       &InitArguments::initConnectResultApiNum ) );      
   h.insert( rscValue( mConnectResultApiDelay, 
                       &InitArguments::initConnectResultApiDelay ) );         
   h.insert( rscValue( mSendILwdOutputDelay, 
                       &InitArguments::initSendILwdOutputDelay ) );            
   h.insert( rscValue( mConnectMpiApiDelay, 
                       &InitArguments::initConnectMpiApiDelay ) );            
   h.insert( rscValue( mMpiApiResponseTimeout, 
                       &InitArguments::initMpiApiResponseTimeout ) );               
   h.insert( rscValue( mSyncMpiApiDso, 
                       &InitArguments::initRsyncMpiApiDso ) );               
   
   // Debug flags
   h.insert( rscValue( mEnableMpiApi, &InitArguments::initEnableMpiApi ) );
   h.insert( rscValue( mEnableDumpInput, 
                       &InitArguments::initEnableDumpInput ) );   
   h.insert( rscValue( mDumpDataDir, 
                       &InitArguments::initDumpDataDir ) );      
   h.insert( rscValue( mRunCode, 
                       &InitArguments::initRunCode ) );         
   h.insert( rscValue( mEnableResultApi, 
                       &InitArguments::initEnableResultApi ) );      
   h.insert( rscValue( mEnableDummyOutput, 
                       &InitArguments::initNumDummyOutput ) );      
   h.insert( rscValue( mEnableDummyState, 
                       &InitArguments::initNumDummyState ) );      
   h.insert( rscValue( mEnableDummyMdd, 
                       &InitArguments::initNumDummyMdd ) );         
   h.insert( rscValue( mEnableLoadDso, 
                       &InitArguments::initEnableLoadDso ) );         
   h.insert( rscValue( mEnableSleepBeforeDso, 
                       &InitArguments::initEnableSleepBeforeDso ) );         
   h.insert( rscValue( mEnableSleepAfterDso, 
                       &InitArguments::initEnableSleepAfterDso ) );         
   h.insert( rscValue( mEnableLdasTrace, 
                       &InitArguments::initEnableLdasTrace ) );         
   h.insert( rscValue( mEnableXmpiTrace, 
                       &InitArguments::initEnableXmpiTrace ) );         
   h.insert( rscValue( mEnableDumpArgs, 
                       &InitArguments::initEnableDumpArgs ) );            
   h.insert( rscValue( mEnableTimeoutTrace, 
                       &InitArguments::initEnableTimeoutTrace ) );               
   h.insert( rscValue( mEnableTimeInfo, 
                       &InitArguments::initEnableTimeInfo ) );                  
   
   return h;                 
}

   
const InitArguments::RscFileHash InitArguments::mRscFileHash( initRscFileHash() );   

   
// ProcessInfo static data initialization
   
// For the following const Regexes can't put the whole keyword into the
// definition since RCS seems to substitute any occurences of $keyword 
// with its current value.   
/*
 * Regex details for parsing the mRcsTime variable.
 *
 * "^"                       The beginning of the buffer
 * "\\$D[^:]*: "             "$date: " string.
 *    "("                    Remember the match   
 *        "[^$]*"            Anything but '$' character
 *    ")"                    End of match
 * " \\$"                    " $" string
 * "$"                       followed by the end of the buffer
 */      
const Regex InitArguments::ProcessInfo::re_rcs_time(
    "^\\$D[^:]*: ([^$]*)\\$$" );
   
   
/*
 * Regex details for parsing the mRcsSource variable.
 *
 * "^"                       The beginning of the buffer
 * "\\$S[^:]*: "             "$source: " string.
 *    "("                    Remember the match   
 *        "[^$]*"            Anything but '$' character
 *    ")"                    End of match
 *    ",v "                  ",v" string 
 * " \\$"                    " $" string
 * "$"                       followed by the end of the buffer
 */      
const Regex InitArguments::ProcessInfo::re_rcs_source(
    "^\\$S[^:]*: ([^$]*)/api/wrapperAPI/so/src/initargs\\.cc,v \\$$" );   
   

const string InitArguments::ProcessInfo::mVersion( LDAS_VERSION );   

// RCS specific keywords   
const string InitArguments::ProcessInfo::mRcsTime( LDAS_CVS_DATE );   
const string InitArguments::ProcessInfo::mRcsSource( 
   "$Source: /mnt/ldas/ldas/api/wrapperAPI/so/src/initargs.cc,v $" );       

//!ignore_end:
   
   
// ProcessParams static data initialization
   
//------------------------------------------------------------------------------
//
//: Initializes process parameter hash map.
//         
//!return: ParamsHash - Hash map used for process information generation.
//            
InitArguments::ProcessParamsInfo::ParamsHash 
   InitArguments::ProcessParamsInfo::initHash()
{
    ParamsHash h;
   
    h[ InitArguments::mTotalNodes ] =
       ppair( string( "int" ), 
              &ProcessParamsInfo::mTotalNodesValue );   
    h[ InitArguments::mMpiAPI     ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mMpiAPIValue );      
    h[ InitArguments::mNodeList   ] =
       ppair( string( "string" ),
              &ProcessParamsInfo::mNodeListValue );      
    h[ InitArguments::mDynlib     ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mDynlibValue );         
    h[ InitArguments::mDataAPI    ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mDataAPIValue );         
    h[ InitArguments::mResultAPI  ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mResultAPIValue );        
    h[ InitArguments::mFilterParams  ] =
       ppair( string( "string" ), 
              &ProcessParamsInfo::mFilterParamsValue );      
    h[ InitArguments::mRealTimeRatio ] =
       ppair( string( "real" ), 
              &ProcessParamsInfo::mRealTimeRatioValue );
    h[ InitArguments::mDoLoadBalance ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mDoLoadBalanceValue );
    h[ InitArguments::mDataDistributor ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mDataDistributorValue );
    h[ InitArguments::mJobID         ] = 
       ppair( string( "int" ), 
              &ProcessParamsInfo::mJobIDValue ); 
    h[ InitArguments::mCommunicateOutput ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mCommunicateOutputValue );
    h[ InitArguments::mMemoryUsageLimit ] = 
       ppair( string( "real" ), 
              &ProcessParamsInfo::mMemoryUsageLimitValue );   
    h[ InitArguments::mUserTag ] = 
       ppair( string( "string" ), 
              &ProcessParamsInfo::mUserTagValue );   

   
   return h;
}
   

//!ignore_begin:
   
InitArguments::ProcessParamsInfo::ParamsHash 
   InitArguments::ProcessParamsInfo::mParamsHash( ProcessParamsInfo::initHash() );      
   
//!ignore_end:   
   
   
//------------------------------------------------------------------------------
// 
//: Default constructor.
//
InitArguments::InitArguments() 
   : mInitialized( false ), mProcess( 0 ), mProcessParams( 0 ),
     mTransaction( 0 )
{}
   

//------------------------------------------------------------------------------
// 
//: Destructor.
//
InitArguments::~InitArguments()
{
   cleanup();
}
   
   
//------------------------------------------------------------------------------
//
//: Overloaded call operator.
//   
// This function parses wrapperAPI options.
//   
//!param: int argc - Number of command line arguments plus 1.
//!param: char* argv[] - Array of command line arguments.
//
//!return: Nothing.
//
//!exc: Bad node value. - Node value exceeds the total amount of nodes in the  
//+     global communicator COMM_WORLD.
//!exc: Bad node range. - Invalid range is specified( node value exceeds the   
//+     total amount of nodes in the global communicator or range limits      
//+     are invalid ).
//!exc: Multiple occurences of rank r in nodelist. - Rank r is specified      
//+     multiple number of times.   
//!exc: Bad nodelist format. - Invalid format specified for <b>-nodelist</b>  
//+     command line argument.      
//!exc: Number of nodes exceeds COMM_WORLD. - Number of nodes exceeds total    
//+     number of nodes in the global communicator COMM_WORLD.   
//!exc: Rank 0 cannot be included in nodelist, it is reserved for wrapperAPI  
//+     master. - Rank 0 is reserved for the wrapperAPI master.   
//   
//!exc: Bad (hostname,port) format. - Invalid format specified for host/port   
//+     command line argument.   
//   
//!exc: Bad dynlib format. - Invalid format specified for <b>-dynlib</b>      
//+     command line argument.
//   
//!exc: Bad filterparams format. - Invalid format specified for               
//+     <b>-filterparams</b> argument.
//   
//!exc: Bad realTimeRatio argument. - Invalid value specified for             
//+     <b>-realTimeRatio</b> command line argument.
//   
//!exc: Bad doLoadBalance argument.- Invalid value specified for              
//+     <b>loadBalance</b> command line argument. One of T|TRUE || F|FALSE.  
//   
//!exc: Bad dataDistributor argument.- Invalid value specified for            
//+     <b>dataDistributor</b> command line argument. One of W|WRAPPER ||   
//+     S|SEARCHMASTER.
//     
//!exc: Bad communicateOutput argument.- Invalid value specified for          
//+     <b>-communicateOutput</b> command line argument. One of A|ALWAYS || 
//+     O|ONCE.
//
//!exc: Bad jobID argument. - Invalid format specified for <b>-jobID</b>      
//+     command line argument.   
//
//!exc: Bad -memoryUsageLimit argument. - Invalid format specified for 
//+     <b>-memoryUsageLimit</b> command line argument.
//   
//!exc: Bad -inputFile format. - Invalid format specified for <b>-inputFile</b>
//+     command line argument.
//   
//!exc: Bad -userTag format. - Invalid format specified for               
//+     <b>-userTag</b> argument.
//      
//!exc: Could not parse RCS time variable. - Error parsing RCS time variable.   
//!exc: Could not parse RCS source directory variable. - Error parsing RCS     
//+     source directory variable.
//!exc: Error converting RCS entry time to seconds. - Malformed format is used
//+     for RCS time.
//!exc: Required db field 'hostname' is empty. - Hostname is not set.
//!exc: Required db field 'username' is empty. - Username is not set.    
//!exc: Required db field 'domainname' is empty. - Domain is not set.         
//           
//!exc: Unknown command line argument. - Unknown command line argument specified.
//!exc: nodelist must be specified. - <b>-nodelist</b> command line argument  
//+     must be specified.   
//!exc: dynlib must be specified. - <b>-dynlib</b> command line argument      
//+     must be specified.      
//!exc: jobID must be specified. - <b>-jobId</b> command line argument must   
//+     be specified.
//!exc: mpiAPI must be specified. - <b>-mpiAPI</b> command line argument  
//+     must be specified.      
//!exc: dataAPI must be specified. - <b>-dataAPI</b> command line argument  
//+     must be specified.         
//!exc: resultAPI must be specified. - <b>-resultAPI</b> command line argument  
//+     must be specified.            
//!exc: bad_alloc - Memory allocation failed.   
//   
//!exc: range_error( "unknown table requested: table_name" ) - Specified( process
//+     or process parameters ) table name is unknown in database.         
//!exc: Process table must be created before ProcessParameters table. - Process
//+     table is NULL.      
//!exc: bad_cast - Malformed db data.      
//   
void InitArguments::operator()( INT4 argc, CHAR* argv[] )
try   
{
   // If there was error parsing resource file
   if( mErrorMsg.empty() == false )
   {
      node_error( mErrorMsg.c_str(), MPI::COMM_WORLD, false );
   }      

   
   if( totalNodes < 2 )
   {
      appendErrorMsg( "Illegal number of nodes in COMM_WORLD." );
   }
   
   const CHAR* start( 0 );
   
   
   for ( INT4 i = 1; i < argc; ++i )
   { 
      if ( !strncmp( argv[ i ], mNodeList.c_str(), mNodeList.size() ) )
      {
         start = &argv[ i ][ mNodeList.size() ];
   
         NodeListArg nodesArg;
         nodes = nodesArg( start );

   
         // Make sure that node with a lowest rank( searchMaster )
         // is at position 0
         vector< INT4 >::iterator v_iter( 
            min_element( nodes.begin(), nodes.end() ) );
   
         if( v_iter != nodes.end() )
         {
            if( *v_iter == 0 )
            {
               appendErrorMsg( "Rank 0 cannot be included in -nodelist: "
                  "it is reserved for the master." );
            }
            else
            if( v_iter != nodes.begin() )
            {
               INT4 tmp_val( *v_iter );
               nodes.erase( v_iter );
               nodes.insert( nodes.begin(), tmp_val );
            }
         }
      }
      else if( !strncmp( argv[ i ], mDynlib.c_str(), mDynlib.size() ) )
      {
         start = &argv[ i ][ mDynlib.size() ];
         RegexMatch rm( 3 );
   
         if( rm.match( re_path, start ) )
         {
            dynlib = rm.getSubString( 1 );
         }
         else
         {
            appendErrorMsg( "Bad -dynlib format." );
         }
      }
      else if( !strncmp( argv[ i ], mMpiAPI.c_str(), mMpiAPI.size() ) )
      {
         initMpiAPI( &argv[ i ][ mMpiAPI.size() ] );
      } 
      else if( !strncmp( argv[ i ], mDataAPI.c_str(), mDataAPI.size() ) )
      {
         initDataAPI( &argv[ i ][ mDataAPI.size() ] );
      }
      else if( !strncmp( argv[ i ], mResultAPI.c_str(), mResultAPI.size() ) )
      {
         initResultAPI( &argv[ i ][ mResultAPI.size() ] );
      }
      else if( !strncmp( argv[ i ], mFilterParams.c_str(), 
                         mFilterParams.size() ) )
      {
          start = &argv[ i ][ mFilterParams.size() ];
          filterParams.push_back( "-filterparams" );   
          mDSOParams = start;
   
          ParameterArg pArg;
          pArg( start, filterParams );
      }   
      else if( !strncmp( argv[ i ], mRealTimeRatio.c_str(),
                         mRealTimeRatio.size() ) )
      {
         initRealTimeRatio( &argv[ i ][ mRealTimeRatio.size() ] );
      }      
      else if( !strncmp( argv[ i ], mDoLoadBalance.c_str(), 
                         mDoLoadBalance.size() ) )
      {
         initDoLoadBalance( &argv[ i ][ mDoLoadBalance.size() ] );
      }
      else if( !strncmp( argv[ i ], mDataDistributor.c_str(), 
                         mDataDistributor.size() ) )
      {
         initDataDistributor( &argv[ i ][ mDataDistributor.size() ] );
      }   
      else if( !strncmp( argv[ i ], mCommunicateOutput.c_str(), 
                         mCommunicateOutput.size() ) )
      {
         initCommunicateOutput( &argv[ i ][ mCommunicateOutput.size() ] );
      }   
      else if( !strncmp( argv[ i ], mJobID.c_str(), mJobID.size() ) )
      {
         jobID = parseUINT4Value( &argv[ i ][ mJobID.size() ], 
                                  mJobID );
      }      
      else if( strncmp( argv[ i ], "-inputFile=", 11 ) == 0 )
      {
         start = &argv[ i ][ 11 ];
         RegexMatch rm( 3 );

         if( rm.match( re_path, start ) )
         {
            inputFile = rm.getSubString( 1 );
         }
         else
         {
            appendErrorMsg( "Bad -inputFile format." );
         }
   
      }            
      else if( !strncmp( argv[ i ], mUserTag.c_str(), mUserTag.size() ) )
      {
         start = &argv[ i ][ mUserTag.size() ];
         RegexMatch rm( 3 );
   
         if( rm.match( re_string, start ) )
         {
            userTag = rm.getSubString( 1 );
         }
         else
         {
            string msg( "Bad -userTag format: " );
            msg += start;
            appendErrorMsg( msg );
         }
      }      
      else if( !strncmp( argv[ i ], mMemoryUsageLimit.c_str(),
                         mMemoryUsageLimit.size() ) )
      {
         initMemoryUsageLimit( &argv[ i ][ mMemoryUsageLimit.size() ] );
      }         
      else
      {
         string msg = "Unknown command line argument: ";
         msg += argv[ i ];
         
         appendErrorMsg( msg );
      }
   }


   if( nodes.empty() )
   {
      mErrorMsg += "-nodelist must be specified.";
   }
   
   
   if( dynlib.empty() )
   {
      appendErrorMsg( "-dynlib must be specified." );
   }   

   
   // jobID must be specified on a command line if
   // input data is sent through the socket
   if( inputFile.empty() && !jobID )
   {
      appendErrorMsg( "-jobID must be specified." );
   }
   
   
   if( enableMpiApi && mpiAPI.first.empty() )
   {
      appendErrorMsg( "-mpiAPI must be specified." );
   }

   
   if( inputFile.empty() && dataAPI.first.empty() )
   {
      appendErrorMsg( "-dataAPI or -inputFile must be specified." );
   }   
   

   if( enableResultApi && resultAPI.first.empty() )
   {
      appendErrorMsg( "-resultAPI must be specified." );
   }   


   initialize();   
   if( mErrorMsg.empty() && mInitialized )
   {
      ( *mProcess )();
      ( *mProcessParams )( mProcess->getTable() );   
   }
   
   if( mErrorMsg.empty() == false )
   {
      cleanup();
      node_error( mErrorMsg.c_str(), MPI::COMM_WORLD, false );
   }   
      
   masterPlusNodes = nodes;
   masterPlusNodes.insert( masterPlusNodes.begin(), masterNode );
   
   
   // Create a vector of "not nodes", meaning nodes that 
   // are not in the LB communicator
   createNotLBNodes( totalNodes );
   
   displayArguments();
   
   mInitState = false;
   return;
}
catch( const exception& exc )
{
   cleanup();
   appendErrorMsg( exc.what() );
   node_error( mErrorMsg.c_str(), MPI::COMM_WORLD, false );   
}
   
   
//------------------------------------------------------------------------------
//
//: Gets processInfo and processParamsInfo.
//
// This method combines process and process parameters information in one 
// ILWD format element. 
// The method returns new dynamically allocated object, which caller must take 
// care of( delete the object ).
//   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator
//+       within which request for the data is made.
//   
//!return: LdasContainer* - ILWD format element representing combined 
//+        process information.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: Process and ProcessParams tables must be generated before requesting
//+     their data. - Process and ProcessParams tables are not generated at the
//+     time when their ILWD representation is requested.   
//   
ILwd::LdasContainer* InitArguments::getDBDataCopy( const MPI::Intracomm& comm )
{
   // Prevent execution of this method by slaves.
   if( myNode == masterNode )
   {
      if( mTransaction == 0 )
      {
         mTransaction = new Transaction();
   
         if( !mInitialized )
         {
            cleanup();
            node_error( "Process and ProcessParams tables must "
                        "be generated before requesting their data", comm );
         }
   
         mTransaction->AppendTable( mProcess->getTable(), false );
         mTransaction->AppendTable( mProcessParams->getTable(), false );   
      }
      else
      {
         mProcess->setEndTime();
      }
   
      return mTransaction->GetILwd();
   }
   
   return 0;
}
   

//------------------------------------------------------------------------------
//
//: Gets the state of the class.
//
//!return: const bool - True if parameter initialization is in progress, false
//+        otherwise.   
//   
const bool InitArguments::getInitState()
{
   return mInitState;
}

   
//------------------------------------------------------------------------------
//
//: Appends error message to the class error string.
//
//!param: const CHAR* msg - A pointer to the error message to append.
//   
//!return: Nothing.
//   
void InitArguments::appendErrorMsg( const CHAR* msg )
{
   if( !mErrorMsg.empty() )
   {
      mErrorMsg += ' ';
   }
   
   mErrorMsg += msg;
   return;
}   
   
   
//------------------------------------------------------------------------------
//
//: Appends error message to the class error string.
//
//!param: const string& msg - A reference to the error message to append.
//   
//!return: Nothing.
//   
void InitArguments::appendErrorMsg( const std::string& msg )
{
   if( !mErrorMsg.empty() )
   {
      mErrorMsg += ' ';
   }
   
   mErrorMsg += msg;
   return;
}   

   
//------------------------------------------------------------------------------
//
//: Set full path for data directory.
//
// This method sets a full directory path for input/output data
// (based on the jobID).
//   
//!return: Nothing.
//      
void InitArguments::setDumpDataDir()
{
   if( enableDumpInput || 
       enableResultApi == false )
   {
      // Calculate subdirectory name( all jobs are sorted by N=jobID/10000:
      // $dump_data_dir$/$run_code$_N/$run_code$jobID )
      static const UINT4 dir_mod( 10000 );
      const UINT4 dir_num( jobID / dir_mod );
   
      ostringstream s;
      s << mDumpDataDirValue << '/' 
        << mRunCodeValue << '_' << dir_num << '/'
        << mRunCodeValue << jobID << '/';
   
      dumpDataDir = s.str();
   }   
   
   return;
}
   
  
//------------------------------------------------------------------------------
//
//: Parses wrapperAPI resource file.
//
// This method parses wrapperAPI resource file found in the same directory as 
// wrapperAPI executable and initializes corresponding arguments with values 
// found in the file.   
//    
//!return: Nothing.
//   
void InitArguments::parseResourceFile() const
try   
{
   // Default settings
   realTimeRatio      = 1.0;
   loadBalance        = true; // do load balancing
   dataDistributor    = true; // wrapperAPI distributes data
   communicateOutput  = true; // always parse outPut structure  
   jobID              = 0;
   memoryUsageLimit   = 1.0f; // set to 100% memory usage
   userTag            = "";

   // Default settings for debug flags
   enableMpiApi       = true;
   enableDumpInput    = false;
   enableResultApi    = true;   
   numDummyOutput     = 0;      
   numDummyState      = 0;         
   numDummyMdd        = 0;       
   enableLoadDso      = true;
   secSleepBeforeDso  = 0;
   secSleepAfterDso   = 0;
   enableLdasTrace    = false;
   enableXmpiTrace    = false;
   enableTimeoutTrace = false;
   enableDumpArgs     = false;
   LdasDebug          = false;      
   enableTimeInfo     = true;

   commOutputTime     = 0.0f;   
   commInputTime      = 0.0f;
   initSearchTime     = 0.0f;
   conditionDataTime  = 0.0f;
   applySearchTime    = 0.0f;
   freeOutputTime     = 0.0f;
   finalizeSearchTime = 0.0f;        
   commILwdTime       = 0.0f;
   lbTime             = 0.0f;
   initArgsTime       = 0.0f;
   nodeInitTime       = 0.0f;
   createLdasTypesTime = 0.0f;
   loadDSOTime        = 0.0f;
   nodeFinalizeTime   = 0.0f;   
   
   string rscFileString;
   const CHAR* env_var( "WRAPPER_RESOURCE_FILE" );
   const CHAR* env_var_value( getenv( env_var ) );
   if( env_var_value )
   {
      rscFileString += env_var_value;
   }
   else
   {
      rscFileString += RESOURCEFILE;
   }
  
   rscFileString += "/LDASwrapper.rsc";
   
   const CHAR* rscFile( rscFileString.c_str() );

   //cout << "enter parseResourceFile: wrapperAPI process=" << DB::GetOSProcessId()
   //     << endl;  
   ifstream rsc_file( rscFile );

   
   // Resource file exists
   if( rsc_file )
   {
      list< string > rsc_lines;   
      string rsc_line;
      typedef RscFileHash::const_iterator const_iterator;   
   
      try
      {
         // Read valid lines in( up to the newline )
         while( getline( rsc_file, rsc_line ) )
         {
            // All comments start with '#'
            if( rsc_line.empty() == false &&
                rsc_line[ 0 ] != '#' &&
                find_if( rsc_line.begin(), rsc_line.end(), notSpace )
                   != rsc_line.end() )
            {
               rsc_lines.push_back( rsc_line );
            }
         }      
      }
      catch(...)
      {
         rsc_file.close();         
         throw;
      }

      rsc_file.close();      
   

      // Parse lines
      list< string >::const_iterator list_iter( rsc_lines.begin() ),
                                     list_end( rsc_lines.end() );
   
      string tmp;
      string arg_name, arg_value;
      string::const_iterator string_iter, string_end, tmp_iter;
      const_iterator hash_end( mRscFileHash.end() );  
   
      while( list_iter != list_end )
      {
         string_iter = list_iter->begin();
         string_end = list_iter->end();
            
         // Ignore leading blanks
         string_iter = find_if( string_iter, string_end, notSpace );
         tmp_iter    = find_if( string_iter, string_end, isSpace );

         arg_name.assign( ( *list_iter ), string_iter - list_iter->begin(), 
                          tmp_iter - list_iter->begin() );

         // Reminder of the line is value
         arg_value.assign( *list_iter, tmp_iter - list_iter->begin(),
                           std::string::npos );
             
         if( find_if( arg_value.begin(), arg_value.end(), notSpace ) == 
             arg_value.end() )
         {
            string msg( rscFile );
            msg += ": value is missing for ";
            msg += arg_name;
            appendErrorMsg( msg );
         }
         else
         {
            const string arg_n( arg_name );
            const_iterator hash_iter( mRscFileHash.find( arg_n ) );            
          
            if( hash_iter == hash_end )
            {
               string msg( rscFile );
               msg += ": unknown argument - ";
               msg += arg_name;   
               appendErrorMsg( msg );
            }
            else
            {
               // Parse the value
               ( this->*( hash_iter->second ) )( arg_value.c_str() );
            }
         }

   
         ++list_iter;
      }
   }

   if( enableLdasTrace )
   {
      cout << "exit parseResourceFile: wrapperAPI process=" << DB::GetOSProcessId()
           << endl;  
   }
   return;
}
catch( const exception& e )
{
   cout << "Exception in parseResourceFile: " << e.what() << endl;

   ostringstream s;
   s << "Process " << DB::GetOSProcessId() << " reported " << e.what();
   
   appendErrorMsg( s.str() );
   return;
}
catch(...)
{
   appendErrorMsg( "Unknown exception parsing resource file." );
   return;
}   

   
//------------------------------------------------------------------------------
//
//: Sets jobID field for the ProcessInfo.
//      
// This method is called only by master node of the COMM_WORLD communicator.
// It sets jobID field of the database process information table to the 
// currently used value. 
// This method was introduced to fix standalone mode when jobID is not passed
// as a command line argument.   
//
//!param: const MPI::Intracomm& comm - A reference to the MPI Intracommunicator
//+       within which data is exchanged.
//!param: const bool p2p - Flag to indicate that method gets called within
//+       p2p communicator.
//
//!return: Nothing.   
//      
void InitArguments::setProcessTableJobID( const MPI::Intracomm& comm,
   const bool p2p )
{
   if( myNode == masterNode )
   {
      if( !mInitialized )
      {
         cleanup();
         node_error( "Process table must be generated before "
                     "accessing its data", comm );
      }   
   
      return mProcess->setJobID();
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Allocates object memory.
//
// Normally class constructor should do this, but since object of this class
// is used only as a global object, need to do these allocations within first 
// use of the object.   
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: range_error( "unknown table requested: table_name" ) - Specified( process
//+     or process parameters ) table name is unknown in database.      
   
void InitArguments::initialize()
{
   if( !mInitialized && myNode == masterNode )
   {
      // Only master node will use these objects
   
      // Generate process information
      mProcess = new ProcessInfo;
      
      // Generate process parameters information 
      mProcessParams = new ProcessParamsInfo; 
   
      mInitialized = true;
   }
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Cleanup utility.
//
// This method frees dynamically allocated memory.
//   
void InitArguments::cleanup()
{
   delete mProcess;
   mProcess = 0;
   
   delete mProcessParams;
   mProcessParams = 0;   

   mInitialized = false;

   delete mTransaction;
   mTransaction = 0;
   
   return;
}     


//------------------------------------------------------------------------------
//
//: Initializes mpiAPI argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//            
void InitArguments::initMpiAPI( const CHAR* value ) const
{
   HostPortArg hpArg;   
   mpiAPI = hpArg( value );   
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes dataAPI argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//               
void InitArguments::initDataAPI( const CHAR* value ) const
{
   HostPortArg hpArg;   
   dataAPI = hpArg( value );   
   
   return;   
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes resultAPI argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//               
void InitArguments::initResultAPI( const CHAR* value ) const
{
   HostPortArg hpArg;   
   resultAPI = hpArg( value );   
   
   return;   
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes realTimeRatio argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad -realTimeRatio argument: less or equal 0.0. - Negative number of
//+     zero is passed.
//!exc: Bad -realTimeRatio argument. - Malformed value.   
//               
void InitArguments::initRealTimeRatio( const CHAR* value ) const
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_float, value ) )
   {
      realTimeRatio = atof( rm.getSubString( 1 ).c_str() );
   
      if( realTimeRatio <= 0.0 )
      {
         mErrorMsg += "Bad -realTimeRatio argument: less of equal to 0.0";
      }
   
      return;
   }

   mErrorMsg += "Bad -realTimeRatio argument.";
   return;
}

   
//------------------------------------------------------------------------------
//
//: Initializes doLoadBalance argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad -doLoadBalance value. - Malformed value.   
//                  
void InitArguments::initDoLoadBalance( const CHAR* value ) const
{
   loadBalance = parseBoolValue( value, mDoLoadBalance );
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes dataDistributor argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad -dataDistributor value. - Malformed value.   
//                  
void InitArguments::initDataDistributor( const CHAR* value ) const
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_distributor, value ) )
   {
      dataDistributor = ( rm.getSubString( 1 )[ 0 ] == 'W' ? 
                          true : false );
      return;
   }

   string msg( "Bad " );
   msg += mDataDistributor;
   msg += " value: ";
   msg += value;
   appendErrorMsg( msg );

   return;
}

   
//------------------------------------------------------------------------------
//
//: Initializes communicateOutput argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad -communicateOutput value. - Malformed value.   
//               
void InitArguments::initCommunicateOutput( const CHAR* value ) const
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_communicate, value ) )
   {
      communicateOutput = ( rm.getSubString( 1 )[ 0 ] == 'A' ? 
                            true : false );
      return;
   }
   
   string msg( "Bad " );
   msg += mCommunicateOutput;
   msg += " value: ";
   msg += value;
   appendErrorMsg( msg );

   return;   
}

   
//------------------------------------------------------------------------------
//
//: Initializes memoryUsageLimit argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad -memoryUsageLimit argument: less or equal 0.0 - Invalid value is
//+     passed.   
//!exc: Bad -memoryUsageLimit argument. - Malformed value.   
//                  
void InitArguments::initMemoryUsageLimit( const CHAR* value ) const
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_float, value ) )
   {
      memoryUsageLimit = atof( rm.getSubString( 1 ).c_str() );
   
      if( memoryUsageLimit <= 0.0 )
      {
         mErrorMsg += "Bad -memoryUsageLimit argument: less or equal 0.0";
      }
   
      return;
   }

   mErrorMsg += "Bad -memoryUsageLimit argument.";
   return;
}

   
//------------------------------------------------------------------------------
//
//: Initializes connectNum argument of the MasterDataManager.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad number_connect_resultAPI argument. - Malformed value.   
//                  
void InitArguments::initConnectResultApiNum( const CHAR* value ) const
{
   return MasterDataManager::setConnectNum( parseUINT4Value( value,
                                               mConnectResultApiNum ) );   
}

   
//------------------------------------------------------------------------------
//
//: Initializes connectDelay argument of the MasterManager.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad delay_connect_resultAPI argument. - Malformed value.   
//                  
void InitArguments::initConnectResultApiDelay( const CHAR* value ) const
{
   // Allow maximum delay of 1 second
   static const UINT4 max_delay( 1000000 );
   
   UINT4 delay( parseUINT4Value( value, mConnectResultApiDelay ) );
   if( delay > max_delay )
   {
      delay = max_delay;
   }
   
   return MasterDataManager::setConnectDelay( delay );   
}

   
//------------------------------------------------------------------------------
//
//: Initializes sendOutputDelay argument of the MasterManager.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad send_ilwd_output_delay argument. - Malformed value.   
//                  
void InitArguments::initSendILwdOutputDelay( const CHAR* value ) const
{
   // Allow maximum delay of 300 seconds (5 minutes)
   static const UINT4 max_delay( 300 );
   
   UINT4 delay( parseUINT4Value( value, mSendILwdOutputDelay ) );
   if( delay > max_delay )
   {
      delay = max_delay;
   }
   
   return MasterDataManager::setOutputDelay( delay );   
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes mMpiApiCommDelay argument of the MsgSocket class.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad number_connect_resultAPI argument. - Malformed value.   
//                  
void InitArguments::initConnectMpiApiDelay( const CHAR* value ) const
{
   // Allow maximum delay of 5 seconds.
   static const UINT4 max_delay( 5 );
   UINT4 delay( parseUINT4Value( value, mConnectMpiApiDelay ) );
   if( delay > max_delay )
   {
      delay = max_delay;
   }
   
   return MsgSocket::setMpiCommunicationDelay( delay );   
}

   
//------------------------------------------------------------------------------
//
//: Initializes mTimeout argument of the MsgSocket class.
//         
//!return: Nothing.
//
//!exc: Bad number_connect_resultAPI argument. - Malformed value.   
//                  
void InitArguments::initMpiApiResponseTimeout( const CHAR* value ) const
{
   // Allow maximum delay of 5 seconds.
   static const UINT4 max_timeout( 600 );
   UINT4 timeout( parseUINT4Value( value, mMpiApiResponseTimeout ) );
   if( timeout > max_timeout )
   {
      timeout = max_timeout;
   }
   
   return MsgSocket::setMpiAPIResponseTimeout( timeout );   
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes mRsyncMpiApiDsoMaster argument of the LdasLoadBalance class.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad synchronize_mpiAPI_dsoMaster argument. - Malformed value.   
//                  
void InitArguments::initRsyncMpiApiDso( const CHAR* value ) const
{
   return LdasLoadBalance::setSynchronize( parseBoolValue( value, 
                                                           mSyncMpiApiDso ) );   
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes enableMpiApi argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_mpiAPI value. - Malformed value.   
//                     
void InitArguments::initEnableMpiApi( const CHAR* value ) const
{
   enableMpiApi = parseBoolValue( value, mEnableMpiApi );
   return;   
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes enableDumpInput argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_dump_input value. - Malformed value.   
//                        
void InitArguments::initEnableDumpInput( const CHAR* value ) const
{
   enableDumpInput = parseBoolValue( value, mEnableDumpInput );
   return;      
}
   

//------------------------------------------------------------------------------
//
//: Initializes dumpDataDir argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad dumpDataDir argument. - Malformed value.
//                  
void InitArguments::initDumpDataDir( const CHAR* value ) const
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_path, value ) )
   {
      // It's already guaranteed that value is not empty
      mDumpDataDirValue = rm.getSubString( 1 );
      return;      
   }

   string msg( "Bad " );
   msg += mDumpDataDir;
   msg += " argument";
   appendErrorMsg( msg );
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Initializes runCode argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad runCode argument. - Malformed value.
//                  
void InitArguments::initRunCode( const CHAR* value ) const
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_path, value ) )
   {
      // It's already guaranteed that value is not empty
      mRunCodeValue = rm.getSubString( 1 );
      return;      
   }

   string msg( "Bad " );
   msg += mRunCode;
   msg += " argument";
   appendErrorMsg( msg );
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes enableResultApi argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_resultAPI value. - Malformed value.   
//                              
void InitArguments::initEnableResultApi( const CHAR* value ) const
{
   enableResultApi = parseBoolValue( value, mEnableResultApi );
   return;            
}
   

//------------------------------------------------------------------------------
//
//: Initializes numDummyOutput argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_dummy_output value. - Malformed value.   
//                                 
void InitArguments::initNumDummyOutput( const CHAR* value ) const
{
   numDummyOutput = parseUINT4Value( value, mEnableDummyOutput );
   return;
}
   
  
//------------------------------------------------------------------------------
//
//: Initializes numDummyState argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_dummy_state value. - Malformed value.   
//   
void InitArguments::initNumDummyState( const CHAR* value ) const
{
   numDummyState = parseUINT4Value( value, mEnableDummyState );
   return;   
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes numDummyMdd argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_dummy_mdd value. - Malformed value.   
//   
void InitArguments::initNumDummyMdd( const CHAR* value ) const
{
   numDummyMdd = parseUINT4Value( value, mEnableDummyMdd );
   return;      
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes enableLoadDso argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_load_dso value. - Malformed value.   
//      
void InitArguments::initEnableLoadDso( const CHAR* value ) const
{
   enableLoadDso = parseBoolValue( value, mEnableLoadDso );
   return;               
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes secSleepBeforeDso argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_sleep_before_dso value. - Malformed value.   
//      
void InitArguments::initEnableSleepBeforeDso( const CHAR* value ) const
{
   secSleepBeforeDso = parseUINT4Value( value, mEnableSleepBeforeDso );
   return;      
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes secSleepAfterDso argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_sleep_after_dso value. - Malformed value.   
//         
void InitArguments::initEnableSleepAfterDso( const CHAR* value ) const
{
   secSleepBeforeDso = parseUINT4Value( value, mEnableSleepAfterDso );
   return;      
}   

   
//------------------------------------------------------------------------------
//
//: Initializes enableLdasTrace argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_ldas_trace value. - Malformed value.   
//            
void InitArguments::initEnableLdasTrace( const CHAR* value ) const
{
   enableLdasTrace = parseBoolValue( value, mEnableLdasTrace );
   return;               
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes enableXmpiTrace argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_xmpi_trace value. - Malformed value.   
//               
void InitArguments::initEnableXmpiTrace( const CHAR* value ) const
{
   enableXmpiTrace = parseBoolValue( value, mEnableXmpiTrace );
   return;                  
}   

   
//------------------------------------------------------------------------------
//
//: Initializes enableDumpArgs argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_dump_args value. - Malformed value.   
//               
void InitArguments::initEnableDumpArgs( const CHAR* value ) const
{
   enableDumpArgs = parseBoolValue( value, mEnableDumpArgs );
   return;                  
}   
   

//------------------------------------------------------------------------------
//
//: Initializes enableTimeoutTrace argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_timeout_trace value. - Malformed value.   
//            
void InitArguments::initEnableTimeoutTrace( const CHAR* value ) const
{
   enableTimeoutTrace = parseBoolValue( value, mEnableTimeInfo );
   return;               
}
   
   
//------------------------------------------------------------------------------
//
//: Initializes enableTimeInfo argument.
//         
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Bad enable_time_info value. - Malformed value.   
//            
void InitArguments::initEnableTimeInfo( const CHAR* value ) const
{
   enableTimeInfo = parseBoolValue( value, mEnableTimeoutTrace );
   return;               
}   
   
   
//------------------------------------------------------------------------------
// 
//: Parses boolean value.
//   
// This method is used to parse a boolean value.
//
//!param: const CHAR* value - A pointer to the string representing the value.
//!param: const string& arg_name - A reference to the string representing the
//+       name of the value.
//
//!return: const bool - A value.
//
//!exc: Bad <b>name</b> value: <b>value</b>. - Bad value is specified for 
//+     <b>name</b> argument.   
//   
const bool InitArguments::parseBoolValue( const CHAR* value, 
   const std::string& arg_name )
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_bool, value ) )
   {
      return ( rm.getSubString( 1 )[ 0 ] == 'T' ? true : false );
   }

   string msg( "Bad " );
   msg += arg_name;
   msg += " value: ";
   msg += value;
   
   appendErrorMsg( msg );
   return true;
}

   
//------------------------------------------------------------------------------
// 
//: Parses UINT4 value.
//   
// This method is used to parse 4 byte unsigned integer value.
//
//!param: const CHAR* value - A pointer to the string representing the value.
//!param: const string& arg_name - A reference to the string representing the
//+       name of the value.
//
//!return: const UINT4 - A value.
//
//!exc: Bad <b>name</b> value: <b>value</b>. - Bad value is specified for 
//+     <b>name</b> argument.   
//   
const UINT4 InitArguments::parseUINT4Value( const CHAR* value, 
   const std::string& arg_name )
{
   RegexMatch rm( 3 );
   
   if( rm.match( re_uint, value ) )
   {
      return atoi( rm.getSubString( 1 ).c_str() );
   }

   string msg( "Bad " );
   msg += arg_name;
   msg += " value: ";
   msg += value;
   
   appendErrorMsg( msg );
   return 0;
}

   
//-------------------------------------------------------------------------------
//   
//: Displays command line arguments.
//
// <i><font color="green">This function is for debugging only!</font></i> 
// Displays parsed resource file and command line arguments passed to the
// wrapperAPI. Command line arguments values overwrite the values from
// resource file.   
//
//!return: Nothing.
//   
void InitArguments::displayArguments()
{
   if( myNode == masterNode && enableDumpArgs )
   {
      cout << "-> dynlib: " << dynlib << endl;
   
      cout << "-> mpiAPI: " << mpiAPI.first << " "
           << mpiAPI.second << endl;   
   
      cout << "-> dataAPI: " << dataAPI.first << " "
           << dataAPI.second << endl;         
   
      cout << "-> resultAPI: " << resultAPI.first << " "
           << resultAPI.second << endl;         
   
      cout << "-> jobID: " << jobID << endl;   

      cout << "-> filterparams: ";  
      copy( filterParams.begin(), filterParams.end(), 
            ostream_iterator< string >( cout, " " ) ); 
      cout << endl;

      cout << "-> nodes: ";
      copy( nodes.begin(), nodes.end(), 
            ostream_iterator< INT4 >( cout, " " ) );    
      cout << endl;

      cout << "-> notNodes: ";
      copy( notNodes.begin(), notNodes.end(), 
            ostream_iterator< INT4 >( cout, " " ) );    
      cout << endl;
   
   
      cout << "-> realTimeRatio: " << realTimeRatio << endl;    

      cout << "-> doLoadBalance: " << loadBalance << endl; 
   
      cout << "-> dataDistributor: " << dataDistributor << endl;    
   
      cout << "-> communicateOutput: " << communicateOutput << endl;
   
      cout << "-> memoryUsageLimit: "  << memoryUsageLimit << endl;   

      // Debug flags
      cout << "-> " << mEnableMpiApi << ": " << enableMpiApi << endl;    
   
      cout << "-> " << mEnableDumpInput << ": "
           << enableDumpInput << endl;
   
      cout << "-> " << mEnableResultApi << ": "
           << enableResultApi << endl;             
   
      cout << "-> " << mEnableDummyOutput << ": "
           << numDummyOutput << endl;            
   
      cout << "-> " << mEnableDummyState << ": "
           << numDummyState << endl;             

      cout << "-> " << mEnableDummyMdd << ": "
           << numDummyMdd << endl;                
   
      cout << "-> " << mEnableLoadDso << ": "
           << enableLoadDso << endl;                
   
      cout << "-> " << mEnableSleepBeforeDso << ": "
           << secSleepBeforeDso << endl;                
   
      cout << "-> " << mEnableSleepAfterDso << ": "
           << secSleepAfterDso << endl;                   
   
      cout << "-> " << mEnableLdasTrace << ": "
           << enableLdasTrace << endl;                
   
      cout << "-> " << mEnableXmpiTrace << ": "
           << enableXmpiTrace << endl;                   
   
      cout << "-> " << mEnableDumpArgs << ": "
           << enableDumpArgs << endl;                   
   }
   
   return;
}

   
   
   
//------------------------------------------------------------------------------
// 
//: Default constructor.
//
// Creates process DB table.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: range_error( "unknown table requested: table_name" ) - Specified process
//+     table name is unknown in database.      
//    
InitArguments::ProcessInfo::ProcessInfo() 
  :  mTable( 0 )
{
   mTable = Table::CreateTable( DB::Process::TABLE_NAME );
}
   
   
//------------------------------------------------------------------------------
// 
//: Destructor.
//
InitArguments::ProcessInfo::~ProcessInfo()
{
   cleanup();
}

   
//------------------------------------------------------------------------------
//
//: Creates process information ILWD format element.
//      
// This method is called only by master node of the COMM_WORLD communicator.
// It creates ILWD format element that represents process information to be 
// send to the resultAPI.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Could not parse RCS source directory. - Malformed format for RCS 
//+     directory.
//!exc: Error converting RCS entry time to seconds. - Malformed format is used
//+     for RCS time.
//!exc: Could not parse RCS time variable. - Malformed RCS time.     
//!exc: Required db field 'hostname' is empty. - Hostname is not set.
//!exc: Required db field 'username' is empty. - Username is not set.    
//!exc: Required db field 'domainname' is empty. - Domain is not set.   
//   
void InitArguments::ProcessInfo::operator()()
try   
{
   // it is a slave 
   if( myNode != masterNode )
   {
      return;
   }

   // Create process information:
   // Process name
   mTable->AppendColumnEntry( DB::Process::PROGRAM,
			      InitArguments::mProgramName );

   // Version
   mTable->AppendColumnEntry( DB::Process::PROGRAM_VERSION, mVersion );   
   
   // RCS source
   RegexMatch rm( 2 );

   if( !rm.match( re_rcs_source, mRcsSource.c_str() ) )
   {
      InitArguments::mErrorMsg += "Could not parse RCS source directory.";
      return;
   }   
   
   const string rcs_source_s( rm.getSubString( 1 ) );   
   mTable->AppendColumnEntry( DB::Process::REPOSITORY, rcs_source_s );   

   // RCS entry time
   mTable->AppendColumnEntry< INT_4S >( DB::Process::REPOSITORY_ENTRY_TIME,
                                        getRCSTime() );   
   
   // User comment
   ostringstream s;
   s << "wrapperAPI is running on " << totalNodes << " nodes.";
   mTable->AppendColumnEntry( DB::Process::COMMENT, s.str() );   

   // Is online?
   mTable->AppendColumnEntry<INT_4S>( DB::Process::IS_ONLINE,
				      DB::IsOnline() );   

   // Node
   const string node_name( DB::GetNodeName() );
   if( node_name.empty() )
   {
      if( mTable->GetField( DB::Process::NODE ).IsRequired() )
      {
         InitArguments::mErrorMsg += "Required db field 'hostname' is empty.";
      }
   }
   else
   {
      mTable->AppendColumnEntry( DB::Process::NODE, node_name );   
   }

   // Username 
   const string user_name( DB::GetOSUser() );
   
   if( user_name.empty() )
   {
      if( mTable->GetField( DB::Process::USERNAME ).IsRequired() )   
      {
         InitArguments::mErrorMsg += "Required db field 'username' is empty.";
      }
   }
   else
   {
      mTable->AppendColumnEntry( DB::Process::USERNAME, user_name );   
   }
   
   // Process id
   mTable->AppendColumnEntry( DB::Process::OS_PROCESS_ID,
			      DB::GetOSProcessId() );   
   
   // Job id
   mTable->AppendColumnEntry< INT_4S >( DB::Process::JOB_ID, jobID );   
   
   // Domain
   const string domain( DB::GetOSDomain() );   
   if( domain.empty() )
   {
      if( enableResultApi && 
          mTable->GetField( DB::Process::DOMAIN_NAME ).IsRequired() )   
      {
         InitArguments::mErrorMsg += "Required db field 'domainname' is empty.";
      }
   }
   else
   {   
      mTable->AppendColumnEntry( DB::Process::DOMAIN_NAME, domain );   
   }

   // Start time
   mTable->AppendColumnEntry< INT_4S >( DB::Process::START_TIME,
					getGPSTime() );
   
   // End time( this value will be reset at the end of the job )
   mTable->AppendColumnEntry< INT_4S >( DB::Process::END_TIME,
					getGPSTime() );   

   return;   
}
catch( const exception& exc )
{
   InitArguments::mErrorMsg += "Process DB table: ";   
   InitArguments::mErrorMsg += exc.what();
}

   
//------------------------------------------------------------------------------
//
//: Sets endTime field for the ProcessInfo.
//      
// This method is called only by master node of the COMM_WORLD communicator.
// It sets endTime field of the database process information table to the 
// current value.   
//
//!exc: bad_alloc - Memory allocation failed.
//   
void InitArguments::ProcessInfo::setEndTime()
{
   INT_4S end_time( getGPSTime() );
   
   // Reset the END_TIME column in the process table   
   // There is only one-row process table( one wrapperAPI job )
   const INT4 row_counter( 0 );
   return mTable->ModifyEntry( DB::Process::END_TIME, row_counter, &end_time );
}
   
   
//------------------------------------------------------------------------------
//
//: Sets jobID field for the ProcessInfo.
//      
// This method is called only by master node of the COMM_WORLD communicator.
// It sets jobID field of the database process information table to the 
// currently used value. 
// This method was introduced to fix standalone mode when jobID is not passed
// as a command line argument.   
//
//!exc: bad_alloc - Memory allocation failed.
//      
void InitArguments::ProcessInfo::setJobID()
{
   // Reset the JOB_ID column in the process table   
   // There is only one-row process table( one wrapperAPI job )
   const INT4 row_counter( 0 );
   
   // dbaccess defines jobID of INT4 type
   INT4 tmp( jobID );
   return mTable->ModifyEntry( DB::Process::JOB_ID, row_counter, &tmp );   
}
   
   
  
//------------------------------------------------------------------------------
//
//: Cleanup utility.
//
// This method frees dynamically allocated memory.
//      
void InitArguments::ProcessInfo::cleanup()
{
   delete mTable;
   mTable = 0;
   
   return;
}


//------------------------------------------------------------------------------
//
//: Gets GPS representation of the RCS entry time.
//
//!return: INT4 - GPS time in seconds.
//    
//!exc: Could not parse RCS time variable. - Error parsing RCS time variable.   
//!exc: Error converting RCS entry time to seconds. - Could not convert time 
//+     string into UNIX seconds.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: Error converting RCS entry time to seconds. - Malformed format is used
//+     for RCS time.
//!exc: Could not parse RCS time variable. - Malformed RCS time.  
//   
INT4 InitArguments::ProcessInfo::getRCSTime()
{
   RegexMatch rm( 2 );
   
   // Parse RCS time 
   if( !rm.match( re_rcs_time, mRcsTime.c_str() ) )
   {
      InitArguments::mErrorMsg += "Could not parse RCS time variable.";
      return 0;
   }
   
   const struct tm* time_struct = new struct tm;
   memset( ( void* )time_struct, 0, sizeof( struct tm ) );

   // Convert string to time structure:
   strptime( rm.getSubString( 1 ).c_str(), "%Y/%m/%d %T", ( struct tm*)time_struct );

#if 0
   // Get UNIX seconds
   const INT4 dim( 256 );
   CHAR buf[ dim ];
   size_t val( strftime( buf, dim, "%s", time_struct ) );

   // No characters were inserted into time_sec
   if( val == 0 )
   {
      delete time_struct;   
      InitArguments::mErrorMsg += "Error converting RCS entry time to seconds.";
      return 0;
   }
   
   istringstream s( buf );
   
   time_t temp( 0 );
   s >> temp;
#else /* 0 */
   time_t temp = mktime( const_cast<struct tm*>(time_struct) );
   if ( temp == -1 )
   {
      delete time_struct;   
      InitArguments::mErrorMsg +=
	"Error converting RCS entry time to seconds.";

      return 0;
   }
#endif /* 0 */
   
   
   delete time_struct;
   
   return GPSTime( temp, 0, GPSTime::UTC ).GetSeconds();
}
   
   
//------------------------------------------------------------------------------
//
//: Gets current GPS time.
//
//!return: INT4 - GPS time in seconds.
//    
//!exc: Error getting GPS time. - Error getting time of the day.
//   
INT4 InitArguments::ProcessInfo::getGPSTime()
{  
   static GPSTime gps_time;
   gps_time.Now();
   
   return gps_time.GetSeconds();
}
   
   
//------------------------------------------------------------------------------
// 
//: Default constructor.
//
// Allocates necessary memory.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: range_error( "unknown table requested: table_name" ) - Specified process
//+     parameters table name is unknown in database.   
//   
InitArguments::ProcessParamsInfo::ProcessParamsInfo() 
  : mTable( 0 )
{
   mTable = Table::CreateTable( DB::ProcessParams::TABLE_NAME );
}
   
   
//------------------------------------------------------------------------------
// 
//: Destructor.
//
InitArguments::ProcessParamsInfo::~ProcessParamsInfo()
{
   cleanup();
}
   

//------------------------------------------------------------------------------
//
//: Creates ProcessParams DB table.
//      
// This method is called only by master node of the COMM_WORLD communicator.
// It creates DB Process table that represents process parameters 
// information to be send to the resultAPI.
//
//!param: DB::Table* process_table - A pointer to the DB process table to be
//+       associated with parameters.   
//
//!return: Nothing.
//    
//!exc: bad_alloc - Memory allocation failed.
//!exc: Process table must be created before ProcessParameters table. - Process
//+     table is NULL.   
//!exc: bad_cast - Malformed db data.   
//   
void InitArguments::ProcessParamsInfo::operator()( DB::Table* const process_table )
try   
{
   // it is a slave
   if( myNode != masterNode || 
       InitArguments::mErrorMsg.empty() == false )
   {
      return;
   }

   
   if( process_table == 0 )
   {
      InitArguments::mErrorMsg += "Process table must be created before "
                                  "ProcessParameters table.";
      return;
   }

   // Insert all command line arguments information 
   hash_iterator iter( mParamsHash.begin() ), 
                 hash_end( mParamsHash.end() );

   string name;
   DB::Table::rowoffset_type id( process_table->GetRowCount() - 1 );
   DB::Table::rowoffset_type row_count( 0 );   
   
   while( iter != hash_end )
   { 
      // Program name
      mTable->AppendColumnEntry( DB::ProcessParams::PROGRAM, 
                                 InitArguments::mProgramName );
   
      // Parameter name
      // ( don't include '=' sign )
      name.assign( iter->first, 0, iter->first.size() - 1 );
      mTable->AppendColumnEntry( DB::ProcessParams::PARAM, name );
   
      // Parameter type
      mTable->AppendColumnEntry( DB::ProcessParams::TYPE, 
                                 iter->second.first );

      // Parameter value
      mTable->AppendColumnEntry( DB::ProcessParams::VALUE,
                                 ( this->*( iter->second.second ) )() );

      // Associate the entry with Process table
      mTable->Associate( row_count, *process_table, id );

      ++row_count;
      ++iter;
   }   
   
   return;
}
catch( const std::exception& exc )
{
   InitArguments::mErrorMsg += "ProcessParams: ";
   InitArguments::mErrorMsg += exc.what();
}

   
//------------------------------------------------------------------------------
//
//: Cleanup utility.
//
// This method frees dynamically allocated memory.
//      
void InitArguments::ProcessParamsInfo::cleanup()
{
   delete mTable;
   mTable = 0;
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Gets total nodes value.
//         
// This method converts total nodes in COMM_WORLD to the string.
//
//!return: string - String representation of the parameter.
//
//!exc: bad_alloc - Memory allocation failed.
//   
std::string InitArguments::ProcessParamsInfo::mTotalNodesValue()
{
   ostringstream s;
   s << totalNodes;

   return string( s.str() );
}

   
//------------------------------------------------------------------------------
//
//: Gets mpiAPI value.
//         
// This method converts mpiAPI information to the string.
//
//!return: string - String representation of the parameter.   
//
//!exc: bad_alloc - Memory allocation failed.
//      
std::string InitArguments::ProcessParamsInfo::mMpiAPIValue()
{
   ostringstream s;
   s << mpiAPI.first << " " << mpiAPI.second;
   
   return string( s.str() );
}
   
   
//------------------------------------------------------------------------------
//
//: Gets list of node values.
//         
// This method converts list of node values to the string.
//
//!return: string - String representation of the parameter.   
//
//!exc: bad_alloc - Memory allocation failed.
//         
std::string InitArguments::ProcessParamsInfo::mNodeListValue()
{
   ostringstream s;

   for( vector< INT4 >::const_iterator iter = nodes.begin(), 
        end_iter = nodes.end(); iter != end_iter; )
   {
      s << *iter;
      ++iter;
      if( iter != end_iter )
      {
         s << ',';
      }
   }

   return s.str();   
}   

   
//------------------------------------------------------------------------------
//
//: Gets dynlib value.
//         
// This method converts path to the dynamically loaded shared object
// to the string.
//
//!return: string - String representation of the parameter.   
//         
std::string InitArguments::ProcessParamsInfo::mDynlibValue()
{
   return dynlib;   
}   

   
//------------------------------------------------------------------------------
//
//: Gets dataAPI value.
//         
// This method converts dataAPI information to the string.
//
//!return: string - String representation of the parameter.   
//
//!exc: bad_alloc - Memory allocation failed.
//         
std::string InitArguments::ProcessParamsInfo::mDataAPIValue()
{
   ostringstream s;
   s << dataAPI.first << " " << dataAPI.second;
   
   return s.str();
}
   

//------------------------------------------------------------------------------
//
//: Gets resultAPI value.
//         
// This method converts resultAPI information to the string.
//
//!return: string - String representation of the parameter.   
//
//!exc: bad_alloc - Memory allocation failed.
//         
std::string InitArguments::ProcessParamsInfo::mResultAPIValue()
{
   ostringstream s;
   s << resultAPI.first << " " << resultAPI.second;
   
   return s.str();   
}
   

//------------------------------------------------------------------------------
//
//: Gets filterParams information.
//         
// This method converts filter parameters passed to the dso to the string.
//
//!return: string - String representation of the parameter.   
//
std::string InitArguments::ProcessParamsInfo::mFilterParamsValue()
{
   return InitArguments::mDSOParams;
}   
   
   
//------------------------------------------------------------------------------
//
//: Gets realTimeRatio value.
//         
// This method converts realTimeRatio value to the string.
//
//!return: string - String representation of the parameter.   
//
//!exc: bad_alloc - Memory allocation failed.
//      
std::string InitArguments::ProcessParamsInfo::mRealTimeRatioValue()
{
   ostringstream s;
   s << realTimeRatio;
   
   return s.str();
}

   
//------------------------------------------------------------------------------
//
//: Gets loadBalance value.
//         
// This method converts loadBalance value to the string.
//
//!return: string - String representation of the parameter.   
//         
std::string InitArguments::ProcessParamsInfo::mDoLoadBalanceValue()
{
   return string( loadBalance == true ? "TRUE" : "FALSE" );
}


//------------------------------------------------------------------------------
//
//: Gets dataDistributor value.
//         
// This method converts dataDistributor value to the string.
//
//!return: string - String representation of the parameter.   
//         
std::string InitArguments::ProcessParamsInfo::mDataDistributorValue()
{
   return string( dataDistributor == true ? "WRAPPER" : "SEARCHMASTER" );
}
   

//------------------------------------------------------------------------------
//
//: Gets jobID value.
//         
// This method converts jobID value to the string.
//
//!return: string - String representation of the parameter.   
//
//!exc: bad_alloc - Memory allocation failed.
//         
std::string InitArguments::ProcessParamsInfo::mJobIDValue()
{
   ostringstream s;
   s << jobID;

   return s.str();
}
   

//------------------------------------------------------------------------------
//
//: Gets communicateOutput value.
//         
// This method converts communicateOutput value to the string.
//
//!return: string - String representation of the parameter.   
//         
std::string InitArguments::ProcessParamsInfo::mCommunicateOutputValue()
{
   return string( communicateOutput == true ? "ALWAYS" : "ONCE" );
}   
   

//------------------------------------------------------------------------------
//
//: Gets memoryUsageLimit value.
//         
// This method converts memoryUsageLimit value to the string.
//
//!return: string - String representation of the parameter.   
//
//!exc: bad_alloc - Memory allocation failed.
//         
std::string InitArguments::ProcessParamsInfo::mMemoryUsageLimitValue()
{
   ostringstream s;
   s << memoryUsageLimit;

   return s.str();
}
   

//------------------------------------------------------------------------------
//
//: Gets userTag value.
//         
// This method converts user specified tag to the string.
//
//!return: string - String representation of the parameter.   
//         
std::string InitArguments::ProcessParamsInfo::mUserTagValue()
{
   return userTag;   
}   
   
   
   
   
   
   
