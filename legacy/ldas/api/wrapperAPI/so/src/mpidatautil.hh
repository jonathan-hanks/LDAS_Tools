#ifndef WrapperApiMPIDataUtilHH
#define WrapperApiMPIDataUtilHH
   
// System Header Files   
#include <exception>
#include <string>   
   
// General Header Files
#include "general/unordered_map.hh"

// ILwd Header Files
#include <ilwd/elementid.hh> 
using ILwd::ElementId;      
   
namespace ILwd
{
   class LdasElement;
}


// Local Header Files   
#include "wrapperInterfaceDatatypes.h"   

   
//------------------------------------------------------------------------------
//
//: Hash functional for ElementId.
//
class ElementIdHash
{
   
public:   

   //: Hash function.
   //
   //!param: ElementId e - An element Id used for hash search.
   //   
   //!return: size_t - Value corresponding to the key.
   //
   size_t operator()( ElementId e ) const
   {
     General::hash< INT4 > h;
     return h( static_cast< INT4 >( e ) );
   }
   
};
   
   
//------------------------------------------------------------------------------
//
//: Comparison functional.
//
// This functional is used for ILwd element id comparison.
//   
class ElementIdEql{
   
public:

   //: Comparison function.
   //
   //!param: ElementId& e1 - 1st element id.
   //!param: ElementId& e2 - 2nd element id.   
   //   
   //!return: bool - true if ids are equal, false otherwise.
   //
   bool operator()( const ElementId& e1, const ElementId& e2 ) const 
   {
      return( e1 == e2 );
   }
   
};


// Helper functions declarations
const std::string getUnits( const ILwd::LdasElement* pe ); 
const size_t getDim( const ILwd::LdasElement* pe ); 
datatype getType( const ILwd::LdasElement* pe, const std::string& elem_type = "" ); 
std::vector< const ILwd::LdasElement* > getHistory( const ILwd::LdasElement* pe,
   const bool append_names = false );
   
   
#endif
   
