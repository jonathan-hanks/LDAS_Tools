#ifndef WrapperApiLdasDataHH
#define WrapperApiLdasDataHH

// System Header Files   
#include <vector>   
#include <exception>
#include <string>   
   
namespace ILwd
{
   class LdasElement;
}
   
// Local Header Files      
#include "wrapperInterfaceDatatypes.h"  
   
   
//------------------------------------------------------------------------------
//
//: LdasData functional.
// 
// A base functional for LDAS derived datatypes containing the data pointers.
// This functional is designed mainly to manage memory allocation/deallocation
// for the C data structures containing data pointers, and to represent this data
// MPI-wise for communication between master and slaves.   
//
class LdasData 
{
   
public:
   
   /* Default constructor */
   LdasData();
   
   // Helper methods called by all nodes in communicator
   
   static void allocateDataArray( dataPointer* data, 
      const datatype& type, const UINT4 num,
      const MPI::Intracomm& comm = MPI::COMM_WORLD, 
      const bool p2p = true );      
   
   static void setDataArray( dataPointer* ptr, const dataPointer& sptr, 
      const datatype& type, const UINT4 num,
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true );   
   
   static void deleteDataArray( dataPointer* data, const datatype& type,
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true );
   
   static const UINT4 getNDim( const UINT4 ndim, const UINT4* dim );
   static const UINT4 getBlobNDim( const UINT4 ndim, const UINT4* dim );
   
protected:
   

   // Helper methods called only by master node in COMM_WORLD.
   void setDataArray( const ILwd::LdasElement* data,
      dataPointer* ptr, const datatype& type, const UINT4 num ) const;

   // Helper methods called by all nodes in communicator.
   MPI::Datatype dataArrayType( const datatype& type,
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true ) const;
   
   MPI::Aint dataArrayDisplacement( const datatype& type, 
      const dataPointer& data,
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = true ) const;

   static ILwd::LdasElement* dataToILwd( const datatype& type, 
      const UINT4 ndim, const UINT4* dims, const dataPointer& data, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD, 
      const std::string& name = "", const std::string& units = "" );
   
private:   
   
};


#endif // WrapperApiLdasDatatypeHH  
