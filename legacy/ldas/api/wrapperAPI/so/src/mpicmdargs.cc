#include "LDASConfig.h"

#define MpiCmdArgumentsCC
   
// System Header Files
#include <sstream>   
#include <iomanip>   
#include <cstring>   
#include <dlfcn.h>   
#include <unistd.h>   

// ILWD Header Files
#include <ilwd/reader.hh>
#include <ilwd/ldascontainer.hh>   
#include <ilwd/util.hh>

using ILwd::ID_ILWD;
using ILwd::LdasElement;   
using ILwd::LdasContainer;      

#include <dbaccess/dbaccess.h>   

// Local Header Files 
#include "dynamiccom.hh"
#include "initargs.hh"   
#include "ldaserror.hh"   
#include "mpicmdargs.hh"
#include "mpiutil.hh"   
#include "mpilaltypes.hh"   
#include "inputdatatype.hh"   
#include "outputtype.hh"   
#include "ldastag.hh"      
#include "loadbalance.hh"   
#include "nodeinfo.hh"   
#include "wrapperInterface.h"   
#include "mastermanager.hh"   
#include "slavemanager.hh"
#include "ldasrequest.hh"   
   
   
using namespace std;   

   
INT4 temp_initSearch( CHAR** initStatus, InitParams* initParams );
   
INT4 temp_conditionData( CHAR** conditionStatus, inPut* data,
   SearchParams* searchParams );
   
INT4 temp_applySearch( CHAR** searchMessage, inPut* input, 
   SearchOutput* output, SearchParams* searchParams );
   
INT4 temp_finalizeSearch( CHAR** finalizeStatus );

INT4 temp_freeOutput( CHAR** freeStatus, SearchOutput* output );
   
   
// Initialize global variables
INT4       myNode( -1 );
INT4       totalNodes( 0 );
const INT4 masterNode( 0 );  
   
NodeError node_error;

   
template < class FuncType >
union symbol{
  void*	obj;
  FuncType func;
};

//------------------------------------------------------------------------------
//   
//: Main function for the wrapperAPI.   
//
// WrapperAPI is issued by <b>mpirun</b> command and allows the following 
// options: 
//<p>-nodelist - Specifies the subset of nodes to run indexed filter algorithm
//    on.
//<p>-dynlib - Specifies an absolute path and file name of the dynamically 
//    loaded shared object library.   
//<p>-mpiAPI - Hostname and port number of mpiAPI to connect with in case of 
//    error, warning, to submit state information, job progress.   
//<p>-dataAPI - Hostname and port number of the LDAS API providing an input data
//   in ILWD format.   
//<p>-resultAPI - Hostname and port number of the LDAS API receiving result data.
//<p>-dataDistributor - Defines the method for distributing input data.
//<p>-searchSlaveCycle - The number of nodes each slave returns to the 
//    wrapperAPI.
//<p>-communicateOutput - Specifies model of the output structure data object 
//    used by the filter algorithm.   
//<p>-filterparams - The list of parameters used to control the parallel filter
//    algorithm.    
//<p>-realTimeRatio - The desired ratio of the time required to process the data
//    to the time contained within the data segment.    
//<p>-doLoadBalance - Enables/disables load balancing of the parallel job.
//    
//!param: INT4 argc - Number of command line arguments.
//!param: CHAR* argv[] - An array of command line arguments passed to the      
//+       wrapperAPI.
//   
//!exc: Illegal number of nodes in COMM_WORLD. - Less than two nodes were      
//+     specified to run parallel job on.   
//!exc: bad_alloc - Memory allocation failed.   
//   
//!exc: See error specifications for initArguments().
//!exc: See error specifications for createLdasTypes().   
//   
//!exc: Error loading dynamic library: error_message. - Error loading dynamic  
//+     shared object library specified by -dynlib command line argument.   
//!exc: Error obtaining address of initSearch function: error_message. - Error 
//+     obtaining the address where initSearch is loaded.
//!exc: Error obtaining address of conditionData function: error_message. -    
//+     Error obtaining the address where conditionData is loaded.  
//!exc: Error obtaining address of applySearch function: error_message. -     
//+     Error obtaining the address where applySearch is loaded.   
//!exc: Error obtaining address of freeOutput function: error_message. - Error 
//+     obtaining the address where freeOutput is loaded.   
//!exc: Error obtaining address of finalizeSearch function: error_message. -   
//+     Error obtaining the address where finalizeSearch is loaded.   
//
// Parsing errors from dynamically loaded library:   
//!exc: Node r( func ) reported undefined error code: n. - Node with rank       
//+     <b>r</b> reported undefined error code: <b>n</b>. One of -1 | 0 | 1.
//   
// Debug mode error messages( read ILWD from the file ):
//!exc: Error opening input file: wrapper.ilwd. - Error opening ILWD format    
//+     file containing the input data.
//!exc: Memory allocation failed while reading wrapper.ilwd. - Memory          
//+     allocation failed while reading ILWD input file.   
//!exc: SwigException_error_message - SwigException occured.
//!exc: Unexpected exception in read ILWD. - Undefined error while reading     
//+     wrapper.ilwd.   
// End of debug mode error messages( read ILWD from the file ):   
//
//!exc: bind_failure - Unable to bind a socket to the specified address and    
//+     port.
//!exc: invalid_address - The address is not a valid IP address.
//!exc: invalid_host - The host to which the address refers can not be found.   
//!exc: unexpected_exception - Unexpected exception occured.   
//!exc: invalid_socket - The socket does not exist
//!exc: connect_failure - The socket was unable to connect to the designated  
//+     address.   
//!exc: unconnected_socket - Data socket is not connected.      
//   
//!exc: See error specifications for InputType::InputType().
//   
//!exc: Empty node vector for DynamicIntracom communicator. - Empty node vector  
//+     is passed for dynamic load balancing communicator generation.   
//   
//!exc: See error specifications for OutputType::OutputType().
//
// Load balancing specific errors:   
//!exc: Invalid MPI response message - Invalid response message is sent by      
//+     mpiAPI( the state of response message does not correspond to the     
//+     state of sent request message ).
//!exc: Rank is already in LB communicator. - Specified rank to be added to the 
//+     load balancing communicator already exists within the communicator.   
//!exc: Rank is not in LB communicator. - Specified rank to be deleted from the 
//+     load balancing communicator is not in the communicator.   
//!exc: Invalid MPI response message - Invalid response message is sent by       
//+     mpiAPI( the state of response message does not correspond to the      
//+     state of sent request message ).
//!exc: Rank is already in LB communicator. - Specified rank to be added to the  
//+     load balancing communicator already exists within the communicator.   
//!exc: Rank is not in LB communicator. - Specified rank to be deleted from the  
//+     load balancing communicator is not in the communicator.   
//!exc: Memory allocation failed for load balancing. - Error allocating memory.   
//!exc: Undefined request. - Undefined format for request was specified.   
//!exc: Unknown mpiAPI command. - Unknown command is received from mpiAPI.
//!exc: Inconsistent request ID. - Request ID for wrapperAPI and mpiAPI are      
//+     inconsistent.   
//!exc: Number of nodes exceeds COMM_WORLD. - Number of nodes to add/remove      
//+     to/from LB communicator exceeds total number of nodes in the comm     
//+     world.    
//!exc: Malformed mpiAPI command to add nodes.- mpiAPI command to add            
//+     nodes into LB communicator is malformed( number of nodes to add       
//+     differs from amount of specified nodes ).   
//!exc: Malformed mpiAPI command to subtract nodes.- mpiAPI command to           
//+     subtract nodes from LB communicator is malformed( number of nodes     
//+     to subtract differs from amount of specified nodes ).      
//!exc: Illegal mpiAPI command to subtract nodes. - Illegal mpiAPI command to    
//+     subtract nodes from the communicator. The number of nodes to          
//+     subtract exceeds or equal to the total number of nodes in the         
//+     communicator.         
//!exc: Error creating C type group of nodes. - Could not create new C type      
//+     group of nodes for communicator generation.   
//!exc: Error creating C type communicator. - Could not create C-type            
//+     communicator.      
//   
int main( INT4 argc, CHAR* argv[] )
try   
{
   MPI::Init( argc, argv );

#define THROW_MPI_EXCEPTIONS   
#ifdef THROW_MPI_EXCEPTIONS

   MPI::COMM_WORLD.Set_errhandler( MPI::ERRORS_THROW_EXCEPTIONS );   
   
#else   
   
   MPI::Errhandler eh( MPI::Comm::Create_errhandler( LdasErrorHandler ) );
   MPI::COMM_WORLD.Set_errhandler( eh );    
   
#endif   
   
   
   // This time is used in 'finalizing' message to the mpiAPI
   REAL8 wrapperMaster_time( MPI::Wtime() ); 
   
   REAL8 parseRFTime( 0.0f );
   parseRFTime -= MPI::Wtime();
   
   //cout << "enter main: wrapperAPI process=" << DB::GetOSProcessId()
   //      << endl;  
   initArguments.parseResourceFile();

   parseRFTime += MPI::Wtime();

   
   // Get the size of COMM_WORLD, rank of the node
   totalNodes = MPI::COMM_WORLD.Get_size();
   myNode = MPI::COMM_WORLD.Get_rank();

   if( enableXmpiTrace )
   {
      MPIL_Trace_on();   
      XMPI_Buoy( "initializing" );   
   }


   if( secSleepBeforeDso )
   {
      INT4 pid( getpid() );
      cout << myNode << ": has pid=" << pid << endl;
   
      sleep( secSleepBeforeDso );
   }
   
   
   // Timing of wrapperAPI
   REAL8 time_main( -1.0f * MPI::Wtime() );

   
   //#define LDAS_REDIRECT   
#ifdef LDAS_REDIRECT
   // Redirect standard error and output to the files 
   // ( for slaves only ).
   if( myNode != masterNode )
   {
      CHAR fname[64];
      sprintf (fname, "slave%03d.err", myNode);
      freopen (fname, "w", stderr);
      sprintf (fname, "slave%03d.out", myNode);
      freopen (fname, "w", stdout);
   }

#endif
   
   if( enableXmpiTrace )
   {
      XMPI_Buoy( "done" );      
      MPIL_Trace_off();   
   }
   

   initArgsTime -= MPI::Wtime();   
   
   // All nodes in COMM_WORLD parse command line arguments   
   initArguments( argc, argv );
   node_error.pass( MPI::COMM_WORLD, false );
   
   initArgsTime += MPI::Wtime();


   DLErrorHandler dlError;
   
   SearchOutput searchOutput;
   memset( &searchOutput, 0, sizeof( SearchOutput ) );
   
   searchOutput.fracRemaining = 1.0f;
   searchOutput.notFinished   = true;

   // Specific to all processing nodes   
   lbNodeInfo lbNode;      
   
   NodeDataManager* nodeManager( 0 );
   
   
   if( myNode == masterNode )
   {
      nodeManager = new MasterDataManager( dlError, lbNode, 
                                           searchOutput, wrapperMaster_time );
   }
   else
   {
      nodeManager = new SlaveDataManager( dlError, lbNode, searchOutput );
   }
   
   node_error.pass( MPI::COMM_WORLD, false );   
   

   nodeInitTime -= MPI::Wtime();
   
   // Send 'initializing' message to the mpiAPI
   nodeManager->initialize();
   node_error.pass( MPI::COMM_WORLD, false );   

   nodeInitTime += MPI::Wtime();
   
   
   // Create LDAS specific MPI datatypes
   createLdasTypesTime -= MPI::Wtime();
   
   createLdasTypes();
   node_error.pass( MPI::COMM_WORLD, false );   
   
   createLdasTypesTime += MPI::Wtime();   

   
   init_fptr initSearch( 0 );
   cond_fptr conditionData( 0 );
   appl_fptr applySearch( 0 );
   free_fptr freeOutput( 0 );
   fnlz_fptr finalizeSearch( 0 );
   
   void* dynLibHandle( 0 );   
   loadDSOTime -= MPI::Wtime();

   
   // Only slave nodes need to load dso
   if( enableLoadDso && myNode != masterNode)
   {
      // Load dynamic shared object on all nodes
      dynLibHandle = dlopen( dynlib.c_str(), RTLD_NOW );
   
      if( dynLibHandle == 0 )
      {
         string msg( "Error loading dynamic library: " );
         msg += dlerror();
         node_error( msg.c_str(), MPI::COMM_WORLD, false );
      }

   
      // Obtain addresses of the processing functions from the loaded
      // shared object
      symbol< init_fptr >	symbolInitSearch;
      symbolInitSearch.obj = dlsym( dynLibHandle, "initSearch" );
      initSearch = symbolInitSearch.func;
   
      if( initSearch == 0 ) 
      {
         string msg( "Error obtaining address of initSearch function: " );
         msg += dlerror();
         node_error( msg.c_str(), MPI::COMM_WORLD, false );   
      }
   
      symbol< cond_fptr >	symbolConditionData;
      symbolConditionData.obj = dlsym( dynLibHandle, "conditionData" );
      conditionData = symbolConditionData.func;
   
      if( conditionData == 0 )
      {
         string msg( "Error obtaining address of conditionData function: " );
         msg += dlerror();
         node_error( msg.c_str(), MPI::COMM_WORLD, false );   
      }   

   
      symbol< appl_fptr >	symbolApplySearch;
      symbolApplySearch.obj = dlsym( dynLibHandle, "applySearch" );  
      applySearch = symbolApplySearch.func;
   
      if( applySearch == 0 )
      {
         string msg( "Error obtaining address of applySearch function: " );
         msg += dlerror();
         node_error( msg.c_str(), MPI::COMM_WORLD, false );   
      }


      symbol< free_fptr >	symbolFreeOutput;
      symbolFreeOutput.obj = dlsym( dynLibHandle, "freeOutput" );
      freeOutput = symbolFreeOutput.func;
   
      if( freeOutput == 0 )
      {
         string msg( "Error obtaining address of freeOutput function: " );
         msg += dlerror();
         node_error( msg.c_str(), MPI::COMM_WORLD, false );   
      }   
   
      symbol< fnlz_fptr >	symbolFinalizeSearch;
      symbolFinalizeSearch.obj = dlsym( dynLibHandle, "finalizeSearch" );   
      finalizeSearch = symbolFinalizeSearch.func;
   
      if( finalizeSearch == 0 )
      {
         string msg( "Error obtaining address of finalizeSearch function: " );
         msg += dlerror();
         node_error( msg.c_str(), MPI::COMM_WORLD, false );   
      } 
   }
   else if( enableLoadDso == false )
   {
      // For running wrapperAPI without dso
      initSearch = &temp_initSearch;
      conditionData = &temp_conditionData;
      applySearch = &temp_applySearch;
      freeOutput = &temp_freeOutput;
      finalizeSearch = &temp_finalizeSearch;
   }

   
   node_error.pass( MPI::COMM_WORLD, false );   
   loadDSOTime += MPI::Wtime();   

   
   if( secSleepAfterDso )
   {
      INT4 a_pid( getpid() );
      cout << myNode << ": has pid=" << a_pid << endl;
   
      sleep( secSleepAfterDso );
   }
   
   
   if( enableXmpiTrace )
   {
      MPIL_Trace_on();      
      XMPI_Buoy( "distribute input" );   
   }
   
   
   nodeManager->setFreeOutput( freeOutput );

   
   REAL8 dt( 0.0f ), dtRatio( 0.0f );   
   inPut* data( 0 );
   

   // If dataDistributor=wrapperMaster then all slaves in COMM_WORLD get
   // the data from wrapperMaster;
   // If dataDistributor=searchMaster then only searchMaster( nodes[ 0 ] )
   // gets the data from wrapperMaster.
   if( dataDistributor ||
       ( myNode == masterNode || myNode == nodes[ 0 ] ) )
   {
   
      // Create MPI datatype to represent inPut data structure
      if( LdasDebug )
      {
         cout << "Creating MPI input datatype for " << myNode << "...";
      }
      
      INT4 comm_rank( -1 );
      if( !dataDistributor )
      {
         comm_rank = ( myNode == masterNode ? nodes[ 0 ] : masterNode );
      }

      
      InputType input( comm_rank ); 
   
      if( dataDistributor )
      {
         node_error.pass( MPI::COMM_WORLD, false );
   
   
         // Master node broadcasts ILWD input data to all slaves
         // in COMM_WORLD
         if( enableXmpiTrace )
         {
            XMPI_Buoy( "inPut bcast in" );   
         }

         commInputTime -= MPI::Wtime();
   
         MPI::COMM_WORLD.Bcast( ( void* )( input.getBottom() ), 1,
                                input, 0 );   

         commInputTime += MPI::Wtime();   
   
         if( enableXmpiTrace )
         {
            XMPI_Buoy( "inPut bcast out" );      
         }
      }
      else
      { 
         // Master node of COMM_WORLD sends input data to the 
         // searchMaster
         MPI::Request request;
    
         if( myNode == masterNode )
         {
            if( enableXmpiTrace )
            {
               XMPI_Buoy( "inPut isend in" );      
            }
   
            commInputTime -= MPI::Wtime();   
   
            request = MPI::COMM_WORLD.Isend( 
                         ( void* )( input.getBottom() ), 1, input,
                         comm_rank, NODE_INPUT_DATA_TAG );

            node_error.pass( request );                       
   
            commInputTime += MPI::Wtime();   
   
            if( enableXmpiTrace )
            {
               XMPI_Buoy( "inPut isend out" );      
            }
         }
         else
         {
            if( enableXmpiTrace )
            {
               XMPI_Buoy( "inPut irecv in" );      
            }
   
            commInputTime -= MPI::Wtime();   
   
            request = MPI::COMM_WORLD.Irecv( 
                         ( void* )( input.getBottom() ), 1, input,
                         comm_rank, NODE_INPUT_DATA_TAG);

            node_error.pass( request );                       
   
            commInputTime += MPI::Wtime();            
   
            if( enableXmpiTrace )
            {
               XMPI_Buoy( "inPut irecv out" );      
            }
         }   
      }
   

      if( LdasDebug )
      {   
         cout << "done" << endl;
      }
   
      // Only slaves need to have inPut data from now on:
      if( myNode != masterNode )
      {
         data = input.getInputStruct();
      }
   
      dt = input.getDuration();
      dtRatio = dt * realTimeRatio;

   
      if( LdasDebug && myNode == masterNode )
      {
        cout << "dt = " << dt << " dtRatio = " << dtRatio << endl;
      }
   }
   else
   {
      // If dataDistributor=searchMaster all searchSlaves must have 
      // empty inPut structure allocated, so they can append data
      // distributed by their searchMaster( in conditionData )
      data = new inPut;
      memset( data, 0, sizeof( inPut ) );
   }

   node_error.pass( MPI::COMM_WORLD, !dataDistributor );

   
   // Number of sequences in inPut data
   nodeManager->setNumSequences( myNode == masterNode ? 0 :
                                 data->numberSequences );
   
   // Number of data seconds to be analyzed
   nodeManager->setAnalyzedSeconds( dt );
   
   
   if( enableXmpiTrace )
   {
      XMPI_Buoy( "done" );         
      MPIL_Trace_off();   
   
      MPIL_Trace_on();      
      XMPI_Buoy( "initSrch+condData" );   
   }

   
   INT4 retCode;   

   // Initialize SearchParams for conditionData and applySearch()
   SearchParams searchParams;
   memset( &searchParams, 0, sizeof( SearchParams ) );


   // Create C-style communicator
   searchParams.comm = new MPI_Comm;
   *( searchParams.comm ) = MPI_COMM_NULL; 

   createComm( nodes, searchParams.comm );

   node_error.pass( MPI::COMM_WORLD, false );
   

   searchParams.action = new MPIapiAction;
   searchParams.action->add = 0;
   searchParams.action->mpiAPIio = true;   
   
   // Call initSearch on all slaves
   if( myNode != masterNode )
   {   
      // Initialize initParams for InitSearch()
      InitParams initParams;
      memset( &initParams, 0, sizeof( InitParams ) );

      if( myNode == nodes[ 0 ] )
      {
         initParams.nodeClass = 1;
      }

   
      initParams.argc = filterParams.size();
   

      // Allocate memory and set InitParams.argv
      initParams.argv = new CHAR*[ initParams.argc ];
   
      memset( initParams.argv, 0, 
              sizeof( CHAR* ) * initParams.argc );
   
      INT4 size( 0 );
   
   
      for( INT4 i = 0; i < initParams.argc; ++i )
      {
         size = filterParams[ i ].size();
   
         initParams.argv[ i ] = new CHAR[ size + 1 ];
         memcpy( initParams.argv[ i ], filterParams[ i ].c_str(), 
                 sizeof( CHAR ) * size );
   
         initParams.argv[ i ][ size ] = '\0';
      }

   
      // LAM time is UNIX time
      initParams.startTime = static_cast< INT8 >( MPI::Wtime() );
      initParams.dataDuration = static_cast< INT8 >( dt + 0.5f );
      initParams.realtimeRatio = realTimeRatio;
   
   
      if( LdasDebug )
      {
         cout << "Calling initSearch() for " << myNode << "...";
      }

   
      CHAR* initStatus( 0 );
      initSearchTime -= MPI::Wtime();   
   
      retCode = initSearch( &initStatus, &initParams );
   
      initSearchTime += MPI::Wtime();   
      dlError( "initSearch", retCode, initStatus );

   
      if( initStatus )
      {
         free( initStatus );
         initStatus = 0;
      }
   
   
      // Cleanup memory
      for( INT4 i = 0; i < initParams.argc; ++i )
      {
         delete[] initParams.argv[ i ];
         initParams.argv[ i ] = 0;
      }
   
      delete[] initParams.argv; 
      initParams.argv = 0;

   
      // All slave nodes call conditionData.
      // If dataDistributor is searchMaster, searchMaster distributes
      // data in the conditionData

      // If load balancing is disabled, slaves outside of the 
      // C communicator don't need to call conditionData   
      if( loadBalance || ( !loadBalance && inRanks( nodes, myNode ) ) )
      {
         if( LdasDebug )
         {
            cout << "Calling conditionData() for " << myNode << "...";
         }   
   
         CHAR* conditionStatus( 0 );      
         conditionDataTime -= MPI::Wtime();   
   
         retCode = conditionData( &conditionStatus, data, &searchParams );
     
         conditionDataTime += MPI::Wtime();      
         dlError( "conditionData", retCode, conditionStatus );
   
         if( conditionStatus )
         {   
            free( conditionStatus );
            conditionStatus = 0;
         }
      }
      //}
   
      if( LdasDebug )
      {
         cout << "done" << endl;
      }
   }
   
   node_error.pass( MPI::COMM_WORLD, false );

   if( enableXmpiTrace )
   {
      XMPI_Buoy( "done" );         
      MPIL_Trace_off();   
   
      MPIL_Trace_on();      
      XMPI_Buoy( "applySearch" );   
   }
   
   
   // Create dynamic communicator for applySearch()
   DynamicIntracom LBcomm( nodes );
   node_error.pass( MPI::COMM_WORLD, false );
   
   
   // Create communicator for master node and all nodes in load
   // balancing communicator.
   DynamicIntracom MPlusLBcomm( masterPlusNodes );   
   node_error.pass( MPI::COMM_WORLD, false );
   
   
   UINT4 numNodes( nodes.size() );
   
   REAL8  lb_nodes( 0 ),      // number of nodes missing for the processing
                              // to keep up the parallel job
          process_time( 0 ),  // parallel processing time on master node   
          theory_time( 0 ),   // estimated parallel processing time
          time_ratio( 0 );

   REAL4 progress( 0 ), ratio( 0 );
     
   CHAR* searchStatus( 0 );
   
   LdasLoadBalance load_balance;   
   
   // For simulating fractionRemaining
   INT4 fraction_counter( 0 );
   
   
   // This loop must be called by all nodes in the COMM_WORLD
   // communicator since LBcomm might be regenerated in a case 
   // of dynamic load balancing( then all nodes have to participate
   // in that call )    
   while( searchOutput.notFinished )
   try
   {
      if( loadBalance )
      {
         // Number of nodes in LBcommunicator
         numNodes = nodes.size();
      }
   

      // Reset applySearch() timing for the iteration
      lbNode.deltaT = 0.0f;

   
      if( !enableLoadDso )
      {
         // Hard-coded calculations to simulate dso   
         ++fraction_counter;
      }
   
   
      // Nodes in LBcomm
      if( inRanks( masterPlusNodes, myNode ) )
      {
         // This is search slave ===> call applySearch
         if( myNode != masterNode )
         {
            if( LdasDebug )
            {
               // Debugging information
               cout << myNode << "(COMM_WORLD)" 
                    << " calling applySearch()...";
            }
   
#ifdef TRACE_FRAC_REMAINING   
            cout << "Slave before applySearch( rank=" << myNode 
                 << " ) has fractionRemaining=" 
                 << searchOutput.fracRemaining << endl << flush;
#endif                 
   
            // Start applySearch timing
            lbNode.deltaT -= MPI::Wtime();
            applySearchTime -= MPI::Wtime();      
   
            // Call applySearch   
            retCode = applySearch( &searchStatus, data,
                                   &searchOutput, &searchParams );
   
            // Finish applySearch timing
            lbNode.deltaT += MPI::Wtime();   
            applySearchTime += MPI::Wtime();      
   
            dlError( "applySearch", retCode, searchStatus, 
                     MPlusLBcomm, true );        

#ifdef TRACE_FRAC_REMAINING      
            cout << "Slave after applySearch( rank=" << myNode 
                 << " ) has fractionRemaining=" 
                 << searchOutput.fracRemaining << endl << flush;
#endif                 
   
            // Memory cleanup
            if( searchStatus )
            {   
               free( searchStatus );
               searchStatus = 0;
            }
   
      
            if( LdasDebug )
            {
               cout << "done" << endl;
            }


            if( !enableLoadDso )
            {
               // Hard-coded calculations to simulate dso
               // This will cause the deadlock if load balancing is
               // enabled and node gets removed and later added back
               // to the communicator.
               // ++fraction_counter;
            
               searchOutput.notFinished = true;
   
               searchOutput.fracRemaining = 1.0 - fraction_counter * 0.1; 
     
               if( searchOutput.fracRemaining <= 0.001f )
               {
                  searchOutput.notFinished = false;
               }     
            }

   
            if( numDummyOutput )
            {
               searchOutput.numOutput = numDummyOutput;   
   
               if( communicateOutput || 
                   ( !communicateOutput && !( OutputType::getInitState() ) ) )
               {
                  // For now applyFilters does not generate any output,
                  // create dummy one for now.
                  try
                  {
                     buildOutputStruct( &( searchOutput.result ), 
                                        searchOutput.numOutput );
                  }
                  catch( const bad_alloc& exc )
                  {
                     node_error( exc, MPlusLBcomm );
                  }   
               }
            }
         } // slave
   

         // Create MPI datatype and based on what the node is 
         // send( if slave )/receive( if master ) outPut data   
         ( *nodeManager )( MPlusLBcomm, numNodes );
      }

   
      node_error.passLB( MPlusLBcomm );
   
   
      // All nodes: cleanup if this was the last call to applyFilters
      // ( mpiAPI issued 'kill' command )
      if( searchParams.action->mpiAPIio == false )
      { 
         nodeManager->cleanup( &data, searchParams,
                               dynLibHandle, finalizeSearch,
                               searchParams.action->mpiAPIio );
   
         delete nodeManager;
         nodeManager = 0;
      }   

   
      // Master calculates load
      if( myNode == masterNode )
      {
         process_time += lbNode.deltaT;

         // Progress ratio of parallel job
         progress = 1.0f - lbNode.fractionRemaining;

         // Expected processing time
         theory_time = progress * dtRatio;

         // Processed time ratio
         time_ratio = theory_time / process_time;
   
         // Number of missing / extra nodes to keep up 
         if( progress == 0.0f )
         {
            lb_nodes = totalNodes - masterPlusNodes.size();
         }
         else
         {
            lb_nodes = static_cast< REAL8 >( numNodes ) /
                       time_ratio - 
                       static_cast< REAL8 >( numNodes );      
         }
   
         if( LdasDebug )
         {
            cout << "Process time: " << process_time << endl;   
   
            cout << "Fraction remaining: " 
                 << lbNode.fractionRemaining << endl;            

            cout << "Progress: " << progress << endl;

            cout << "Theory_time: " << theory_time << endl;   

            cout << "Real time ratio: " << time_ratio << endl;         
   
            cout << "MAIN: missing/extra nodes are " 
                 << lb_nodes << endl;   
         }   
   
   
         // Convert to %
         progress *= 100.0f;
   
         ratio = process_time / dt;
      }

    
      lbTime -= MPI::Wtime();
   
      // All nodes in COMM_WORLD must do load balancing
      searchParams.action->add = load_balance( lb_nodes, ratio, progress,
                                    searchOutput.notFinished, LBcomm,
                                    MPlusLBcomm, searchParams.comm,
                                    warning_msg, error_msg );
      lbTime += MPI::Wtime();
   
      // Used to be: node_error.pass();   
      node_error.passLB( MPlusLBcomm );


      DLErrorHandler::resetMessages( warning_msg, error_msg );
   
   
      searchParams.action->mpiAPIio = !( load_balance.isLastCall() );
   }
   catch( const bad_alloc& e )
   {
      if( inRanks( masterPlusNodes, myNode ) )
      {
         node_error( e, MPlusLBcomm );
      }
   
      node_error( e, MPI::COMM_WORLD, false );
   }   
   catch( const MPI::Exception& exc )
   {
      if( inRanks( masterPlusNodes, myNode ) )
      {
         node_error( exc, MPlusLBcomm );
      }
   
      node_error( exc, MPI::COMM_WORLD, false );
   }
   catch(...)
   {
      throw;
   }

   node_error.passLB( MPlusLBcomm );

   
   if( enableXmpiTrace )
   {
      XMPI_Buoy( "done" );         
      MPIL_Trace_off();   
   
      MPIL_Trace_on();      
      XMPI_Buoy( "finalizing" );   
   }


   nodeManager->simulateData( MPlusLBcomm );

   
   // All nodes: cleanup
   nodeManager->cleanup( &data, searchParams,
                         dynLibHandle, finalizeSearch );


   // Free communicators
   if( LBcomm != MPI::COMM_NULL )
   {
      LBcomm.Free();
   }   


   nodeFinalizeTime -= MPI::Wtime();   
   
   nodeManager->finalize( MPlusLBcomm );
   node_error.passLB( MPlusLBcomm );   

   nodeFinalizeTime += MPI::Wtime();
   
   
   delete nodeManager;
   nodeManager = 0;
   

   if( MPlusLBcomm != MPI::COMM_NULL )
   {
      MPlusLBcomm.Free();
   }   
   
   
   // Time wrapperAPI
   time_main += MPI::Wtime();   
     
//#define DISPLAY_TIME_DETAILS      
   
   if( enableTimeInfo )
   {
#ifdef DISPLAY_TIME_DETAILS      
      cout << myNode << ": ILWD data = " << commILwdTime << endl;               
      cout << myNode << ": load balance = " << lbTime << endl;                  
      cout << myNode << ": parseRFile = " << parseRFTime << endl;                     
      cout << myNode << ": initArgs = " << initArgsTime << endl;                        
      cout << myNode << ": nodeInit = " << nodeInitTime << endl;                        
      cout << myNode << ": loadDSO = " << loadDSOTime << endl;                        
      cout << myNode << ": createLdasTypes = " << createLdasTypesTime << endl;                           
      cout << myNode << ": nodeFinalize = " << nodeFinalizeTime << endl;                        
   
      REAL8 dso_spent_time( initSearchTime );   
      dso_spent_time += conditionDataTime;
      dso_spent_time += applySearchTime;
      dso_spent_time += freeOutputTime;
      dso_spent_time += finalizeSearchTime;
   
      REAL8 comm_time( InputType::getCreateTime() );
      comm_time += commInputTime;
      comm_time += initSearchTime;
      comm_time += conditionDataTime;
      comm_time += applySearchTime;
      comm_time += freeOutputTime;
      comm_time += finalizeSearchTime;
      comm_time += OutputType::getCreateTime();
      comm_time += commOutputTime;
      comm_time += commILwdTime;
      comm_time += lbTime;   
      comm_time += parseRFTime;                     
      comm_time += initArgsTime;                        
      comm_time += nodeInitTime;                        
      comm_time += loadDSOTime;                        
      comm_time += nodeFinalizeTime;                        
      comm_time += createLdasTypesTime;
   
      cout << myNode << ": wrapperAPI main() time=" << time_main
           << " summ time=" << comm_time 
           << " dso time=" << dso_spent_time << endl << endl;          
#endif   
   }
   
   node_error.pass( MPI::COMM_WORLD, false );   

   if( enableXmpiTrace )
   {
      XMPI_Buoy( "done" );         
      MPIL_Trace_off();   
   }

   
   MPI::COMM_WORLD.Set_errhandler( MPI::ERRORS_RETURN );    
   MPI::Finalize();
   return 0;
}
catch( const bad_alloc& exc )
{
   node_error( exc );
}
catch( const exception& exc )
{
   node_error( exc.what() );
}
catch( const MPI::Exception& exc )
{
   node_error( exc );
}
catch( ... )
{
   node_error( "Unknown exception in main()." );
}
   

INT4 temp_initSearch( CHAR** initStatus, InitParams* initParams )
{
   cout << myNode << ": initSearch" << endl;   
   return 0;
}
   
INT4 temp_conditionData( CHAR** conditionStatus, inPut* data,
   SearchParams* searchParams )
{
   cout << myNode << ": conditionData" << endl;   
   return 0;
}
   
INT4 temp_applySearch( CHAR** searchMessage, inPut* input, 
   SearchOutput* output, SearchParams* searchParams )
{
   cout << myNode << ": applySearch" << endl;   
   return 0;
}
   
INT4 temp_finalizeSearch( CHAR** finalizeStatus )
{
   cout << myNode << ": finalizeSearch" << endl;   
   return 0;
}
   
INT4 temp_freeOutput( CHAR** freeStatus, SearchOutput* output )
{
   cout << myNode << ": freeOutput" << endl;
   return 0;
}
   
   
