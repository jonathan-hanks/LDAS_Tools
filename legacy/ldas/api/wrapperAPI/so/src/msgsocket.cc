#include "LDASConfig.h"

// System Header Files
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>   
#include <sys/time.h>   
#include <unistd.h>
#include <fcntl.h>   
#include <arpa/inet.h>   
#include <cerrno>
#include <sstream>   
#include <string>   
#include <iostream>   
#include <algorithm>

#include <general/autoarray.hh>
   
// Local Header Files
#include "msgsocket.hh"
#include "ldaserror.hh"   
#include "mpiargs.hh"   
#include "initvars.hh"   

using namespace std;   


// Static data initialization   
//: Timeout value( seconds )
INT4 MsgSocket::mTimeOut( 60 );
//: Buffer dimension used for message receiving.
const INT4 MsgSocket::mDim( 1024 );   
   
//: mpiAPI related data   
REAL8 MsgSocket::mLastWriteTime( 0.0f );   
UINT4 MsgSocket::mMpiApiCommDelay( 0 );
string MsgSocket::mMpiApiCommBuffer( "" );   

   
//-----------------------------------------------------------------------------
// 
//: Constructor.   
//
//!param: const HostPort& host_port - A reference to the host-port pair.
//!param: const bool error - Flag to indicate that socket will be used for     
//+       error reporting. Default is false.   
//!param: const bool mpi - Flag to indicate that socket will be used for     
//+       mpiAPI communication. Default is true.      
//   
//!exc: MsgSocket error: host name is missing. - Host name is missing.
//!exc: MsgSocket error: illegal port number. - Illegal port number is 
//+     specified.
//!exc: bad_alloc - Memory allocation failed.      
//   
MsgSocket::MsgSocket( const HostPort& host_port, const bool error,
   const bool mpi ) 
  : mHandle( -1 ), mConnected( false ), mIsError( error ), //, mMsgBytes( 0 )   
    mToMpi( mpi ), mWrittenMsg( "" ) 
{
   if( host_port.first.empty() )
   {
      const char* msg( "MsgSocket error: host name is missing." );
      if( !mToMpi )
      {
         node_error( msg );
      }
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );   
         node_error( msg );
      }
      else
      {
         node_error.reportError( msg );
      }
   }
   
   
   if( host_port.second < 0 )
   {
      const char* msg( "MsgSocket error: illegal port number." );
      if( !mToMpi )
      {
         node_error( msg );
      }   
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );   
         node_error( msg );
      }
      else
      {
         node_error.reportError( msg );
      }
   }
   
   
   connectSocket( host_port.first, host_port.second );   
}


//-----------------------------------------------------------------------------
// 
//: MsgSocket Constructor.   
//   
//!param: const INT4 handle - A socket handler to the open socket.
//!param: const bool error - Flag to indicate that socket will be used for     
//+       error reporting.
//!param: const bool mpi - Flag to indicate that socket will be used for     
//+       mpiAPI communication. Default is true.         
//
MsgSocket::MsgSocket( const INT4 handle, const bool error,
   const bool mpi )
  : mHandle( handle ), mConnected( true ), mIsError( error ), //, mMsgBytes( 0 )   
    mToMpi( mpi ), mWrittenMsg( "" )
{}
   
   
//-----------------------------------------------------------------------------
// 
//: Copy constructor.
//
//!param: const MsgSocket& sh - A reference to the object to copy.
//   
MsgSocket::MsgSocket( const MsgSocket& sh )
  : mHandle( sh.mHandle ), mConnected( sh.mConnected ), 
    mIsError( sh.mIsError ), mToMpi( sh.mToMpi ),
    mWrittenMsg( sh.mWrittenMsg ) //, mMsgBytes( sh.mMsgBytes )
{}
   

//-----------------------------------------------------------------------------
//
//: Destructor
//   
MsgSocket::~MsgSocket()
{
   if( mConnected )
   {
      disconnectSocket();
      mConnected = false;
   }
}
   

//-----------------------------------------------------------------------------
//
//: writeMessage method.
//
// This method writes passed string to the socket. The number of bytes to write
// to the socket is defined by the size of the passed in string.   
//
//!param: const string& msg - A string to write to the socket.
//   
//!return: Nothing.
//   
//!exc: MsgSocket error: Socket is disconnected for writeMessage. - Socket 
//+     is disconnected for sending the message.
//!exc: MsgSocket error: <i>error</i>: write. - <i>error</i> occured while 
//+     writing <i>msg</i> to the socket.  
//!exc: Memory allocation failed. - Error allocating new memory.     
//   
void MsgSocket::writeMessage( std::string& msg )
try   
{
   if( enableLdasTrace )
   {
      cout << "Master before sending message to "
           << ( mToMpi ? "mpiAPI: " : "dataAPI: ")
           << msg << endl;
   }
   
   if( !mConnected )
   {
      string err( "MsgSocket error: socket is disconnected for "
                  "writeMessage." );
      if( !mToMpi )
      {
         node_error( err.c_str() );
      }
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );
         node_error( err.c_str() );
      }
      else
      {
         node_error.reportError( err.c_str() );
      }
   }

   // Make sure message is not empty:
   if( msg.size() == 0 )
   {
      const CHAR* err( "wrapperMaster is about to send an empty message to mpiAPI!" );
      if( !mToMpi )
      {
         node_error( err );
      }   
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );   
         node_error( err );
      }
      else
      {
         node_error.reportError( err );
      }
   }

   msg += '\n';
   
   // Prepend accumulated messages if any (only for mpiAPI communication!!!):
   if( mToMpi == true &&
       mMpiApiCommBuffer.empty() == false )
   {
      mMpiApiCommBuffer += msg;
      msg = mMpiApiCommBuffer;
   
      // Reset buffer
      mMpiApiCommBuffer = "";
   }

 
   // Don't include terminating null for C string( mpiAPI has doubts about \0 )
   const INT4 tmp_size( msg.size() );
   
   General::AutoArray< CHAR > tmp( new CHAR[ tmp_size ] );
   std::copy( msg.begin(), msg.end(), tmp.get() );


   // Capture write time 
   if( mToMpi )
   {
      mLastWriteTime = MPI::Wtime();
      mWrittenMsg = msg;   
   }
   
   //size_t wroteBytes( write( mHandle, tmp, tmp_size ) );
   INT4 wroteBytes( send( mHandle, tmp.get(), tmp_size, 0 ) );


   if( enableLdasTrace )
   {
      cout << "Master wrote " << wroteBytes << " bytes out of "
           << tmp_size << " to the MsgSocket." 
           << endl;
   }
   
   
   if( wroteBytes >= 0 && wroteBytes != tmp_size )
   {
      string err( "MsgSocket error: message \"" );
      err += msg;
      err +="\" didn't go through the socket.";
   
      if( !mToMpi )
      {
         node_error( err.c_str() );
      }
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );   
         node_error( err.c_str() );
      }
      else
      {
         node_error.reportError( err.c_str() );
      }
   }
   
   
   if( wroteBytes < 0 )
   {
      string err( "MsgSocket error: " );
      err += parseError( "write" );

      if( !mToMpi )
      {
         node_error( err.c_str() );
      }   
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );   
         node_error( err.c_str() );
      }
      else
      {
         node_error.reportError( err.c_str() );
      }
   }

   
   return;
}
catch( bad_alloc& exc )
{
   if( !mToMpi )
   {
      node_error( exc );
   }   
   else if( !mIsError )
   {
      node_error.setNoMpiApi( true );   
      node_error( exc );
   }
   else
   {
      node_error.reportError( "MsgSocket::writeMessage() memory allocation failed." );   
   }

   return;   
}
   

//-----------------------------------------------------------------------------
//
//: Reads message from the socket.
//
// This method reads message from the previously opened socket. This method is
// non-blocking.   
//
//!param: string& recv_msg - A reference to the read message.
//   
//!return: Nothing.
//      
//!exc: MsgSocket error: Socket is disconnected for readMessage. - Socket
//+     is disconnected for reading the message.   
//!exc: MsgSocket error: <i>error</i>: read. - <i>error</i> occured while 
//+     reading <i>recv_msg</i> from the socket.     
//    
void MsgSocket::readMessage( std::string& recv_msg )
{
   if( !mConnected )
   {
      // Read only from mpiAPI communication socket
      node_error.setNoMpiApi( true );   
      node_error( "MsgSocket error: Socket is disconnected for readMessage." );
   }   

   
   // Receive as many bytes as were sent   
   //const INT4 dim( mMsgBytes );
   
   CHAR* buffer( new CHAR[ mDim ] );
   //memset( buffer, 0, sizeof( CHAR ) * mDim );
   
   INT4 readBytes( -1 );

   
   // Make socket non-blocking
   setNonBlocking();
   
   
   // Set up timeout
   struct timeval startTime, tmpTime;
   gettimeofday( &startTime, 0 );
   
   
   // For debugging only
   INT8 read_counter( 0 );
   
   
   // while not-EOF
   while( recv_msg.empty() && 
          ( readBytes = recv( mHandle, buffer, mDim, 0 ) ) )   
          //          ( readBytes = read( mHandle, buffer, mDim ) ) )
   {
      ++read_counter;
    
      if( readBytes >= mDim )
      {
         if( enableLdasTrace )
         {
            cout << "readBytes = " << readBytes << endl;
         }

         delete[] buffer;
         buffer = 0;
         node_error( "MsgSocket error: mpiAPI response message exceeds"
                     " read buffer size.",
                     MPI::COMM_WORLD, !loadBalance );           
      }
   
   
      if( readBytes > 0 )
      {
         buffer[ readBytes ] = '\0';
         recv_msg += buffer;

         if( enableLdasTrace )
         {
            cout << "Master read " << readBytes << " bytes from the mpiAPI socket "
                    " after " << read_counter << " reads: " 
                 << buffer << endl;
         }
   
         //memset( buffer, 0, sizeof( CHAR ) * mDim );         
         readBytes = 0;

      }
      else
      if( readBytes == -1 )
      {
         if( errno != EAGAIN )
         {
            ostringstream s;
            s << "MsgSocket error: " << parseError( "read" )
              << "[ BEGIN_EXTRA_INFO: written message=\'" << mWrittenMsg 
              << "\' at time=" << mLastWriteTime;
   
            REAL8 now_time( MPI::Wtime() );
   
            s << " now time=" << now_time
              << " END_EXTRA_INFO ]";

            delete[] buffer;
            buffer = 0;   
   
            node_error.setNoMpiApi( true );      
            node_error( s.str().c_str() );         
         }
         else
         {
            gettimeofday( &tmpTime, 0 );   
   
            if( tmpTime.tv_sec - startTime.tv_sec >= mTimeOut )
            {
               ostringstream s;
               s << "MsgSocket error: timeout reading mpiAPI response"
                 " message after " << read_counter << " reads[ written msg='"
                 << mWrittenMsg << "' ]";
   
               delete[] buffer;
               buffer = 0;   
   
               node_error( s.str().c_str(), MPI::COMM_WORLD, !loadBalance );      
            }
   
            usleep( 100000 );
         }
      }
   }
   
   return;
}


//-----------------------------------------------------------------------------
// 
//: Sets flag that socket is used for error reporting.   
//
// This method is used to set up the flag that indicates that socket will be 
// used for error reporting.
//   
//!param: const bool error - Flag to indicate that socket is to be used for 
//+       error reporting( if set to <i>true</i> ).
//   
void MsgSocket::setIsError( const bool error )
{
   mIsError = error;
   return;
}
   
   
//-----------------------------------------------------------------------------
// 
//: Connects socket.
//
// This method does actual connection to the server.
//
//!param: const string& host - Host name to connect to.
//!param: const int port - Port number to connect to.
//   
//!exc: MsgSocket error: <i>error</i>: socket. - <i>error</i> occured while 
//+     creating a socket.
//!exc: MsgSocket error: <i>error</i>: gethostbyname. - <i>error</i> occured 
//+     while extracting the host data.
//!exc: MsgSocket error: missing host name: <i>error</i>: gethostbyname. -
//+     <i>error</i> occured while extracting host name.
//!exc: MsgSocket error: <i>error</i>: connect. - <i>error</i> occured while 
//+     connecting the socket.   
//!exc: bad_alloc - Memory allocation failed.      
//     
void MsgSocket::connectSocket( const std::string& host, const INT4 port )
{
   if( mConnected )
   {
      disconnectSocket();
   }
   
   
   if( ( mHandle = socket( AF_INET, SOCK_STREAM, 0 ) ) < 0 )   
   { 
      string msg( "MsgSocket error: " );
      msg += parseError( "socket" );
   
      if( !mToMpi )
      {
         node_error( msg.c_str() );
      }
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );
         node_error( msg.c_str() );
      }
      else
      {
         node_error.reportError( msg.c_str() );
      }
   }


   const struct hostent* host_data( gethostbyname( host.c_str() ) );
   
   if( host_data == NULL )
   {
      string msg = "MsgSocket error: ";
      msg += parseError( "gethostbyname" );
      close( mHandle );
   
      if( !mToMpi )
      {
         node_error( msg.c_str() );
      }   
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );
         node_error( msg.c_str() );
      }
      else
      {
         node_error.reportError( msg.c_str() );
      }
   }
   

   // Get first name
   struct in_addr* addr = (struct in_addr*)(
                              host_data->h_addr_list[ 0 ] );
   
   string name;
   if( addr != 0 )
   {
      name = inet_ntoa( *addr );       
   }
   else
   {
      string msg = "MsgSocket error: missing host name: ";
      msg += parseError( "gethostbyname" );
      close( mHandle );

      if( !mToMpi )
      {
         node_error( msg.c_str() );
      }   
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );
         node_error( msg.c_str() );
      }
      else
      {
         node_error.reportError( msg.c_str() );
      }   
   }
   
   
   struct sockaddr_in host_addr;
   memset( &host_addr, 0, sizeof( struct sockaddr_in ) );   
   
   host_addr.sin_addr.s_addr = inet_addr( name.c_str() );   
   host_addr.sin_family = AF_INET;   
   host_addr.sin_port = htons( port );   
   
   
   if( connect( mHandle, ( struct sockaddr* )&host_addr, 
          sizeof( struct sockaddr ) ) == -1 )
   {
      string msg( "MsgSocket error: " );
      msg += parseError( "connect" );
      close( mHandle );
   
      if( !mToMpi )
      {
         node_error( msg.c_str() );
      }   
      else if( !mIsError )
      {
         node_error.setNoMpiApi( true );   
         node_error( msg.c_str() );
      }
      else
      {
         node_error.reportError( msg.c_str() );
      }
   }
   
   
   mConnected = true;
   
   
   return;
}


//-----------------------------------------------------------------------------
//
//: Disconnects the socket.
//   
// This method disconnects the client socket.
//   
//!exc: MsgSocket error: <i>error</i>: shutdown. - <i>error</i> occured while 
//+     shutting down the socket.   
//!exc: MsgSocket error: error closing the socket. - Error occured when 
//+     closing the socket.
//   
void MsgSocket::disconnectSocket()
{
   if( mConnected )
   {
#ifdef DISABLE_MPI_SOCKET_SHUTDOWN   
      // mpiAPI receives Null command every time socket is shutdown.   
      if( shutdown( mHandle, SHUT_RDWR ) )
      {
         string msg( "MsgSocket error: " );
         msg += parseError( "shutdown" );

         if( !mToMpi )
         {
            node_error( msg.c_str() );
         }   
         else if( !mIsError )
         {
            node_error.setNoMpiApi( true );   
            node_error( msg.c_str() );
         }
         else
         {
            node_error.reportError( msg.c_str() );
         }         
      }
#endif   
   
      if( close( mHandle ) )
      {
         node_error( "MsgSocket error: error closing the socket.",
                     MPI::COMM_WORLD, !loadBalance );                 
      }
   
   }
   
   
   mConnected = false;
   
   return;
}

  
//-----------------------------------------------------------------------------
//
//: Set non-blocking mode.
//   
// This method sets socket into non-blocking mode.
//
//!exc: MsgSocket error: <i>error</i>: fcntl. - <i>error</i> occured while 
//+     making a call to <i>fcntl</i>.
//   
void MsgSocket::setNonBlocking() const 
{
   // non-blocking read
   INT4 fd_flag( fcntl( mHandle, F_GETFL ) );
   
   if( fd_flag == -1 )
   {
      string msg( "MsgSocket error: " );
      msg += parseError( "fcntl" );

      node_error.setNoMpiApi( true );      
      node_error( msg.c_str() );   
   }
   
   
   fd_flag |= O_NONBLOCK;
   
   
   if( fcntl( mHandle, F_SETFL, fd_flag ) == -1 )
   {
      string msg( "MsgSocket error: " );
      msg += parseError( "fcntl" );

      node_error.setNoMpiApi( true );      
      node_error( msg.c_str() );      
   }

  
   return; 
}
   

//-----------------------------------------------------------------------------
//
//: Error parser.
//
//!param: const string& from - A reference to the string to indicate the      
//+       source of error.
//
//!return: const string - Error message corresponding to the error code.
//   
//!exc: bad_alloc - Memory allocation failed.   
//   
const std::string MsgSocket::parseError( const std::string& from )
{
   // Currently following functions are documented:
   // connect(), gethostbyname(), socket(), read(), and write()
   ostringstream s;
   
   switch( errno )
   {
      // gethostbyname()
      case HOST_NOT_FOUND:
      {
         s << "Host not found: ";
         break;
      }
      case NO_ADDRESS:
      {
         s << "The requested name is valid but does not have an IP "
              "address, or interrupt is received: ";
         break;
      }   
      case NO_RECOVERY:
      {
         s << "An unrecoverable error occured while querying nameserver: ";
         break;
      }
      case TRY_AGAIN:
      {
         s << "Temporary error resolving host. You can try again later: ";
         break;
      }   
      // socket()
      case EPROTONOSUPPORT:
      {
         s << "Specified protocol is not supported in this domain: ";
         break;
      }
      case ENFILE:
      {
         s << "Too many open files in system: ";
         break;
      }
      case EMFILE:
      {
         s << "Process file table overflow: ";
         break;
      }
      case EACCES:
      {
         s << "Permission  to  create  a  socket of the specified "
              "type and/or protocol is denied: ";
         break;
      }   
      case ENOMEM:
      {
         s << "Insufficient memory: ";
         break;
      }
      case EINVAL:
      {
         s << "Invalid argument( unknown protocol, or "
              "protocol family not supported, or object is "
              "unsuitable for writing/reading ): ";
         break;
      }   
      // shutdown() or connect()
      case(EBADF):
      {
         s << "Bad file descriptor or is not open for reading: ";
         break;
      }      
      case ENOTSOCK:
      {
         s << "Descriptor is not associated to a socket: ";
         break;
      }   
      case ENOTCONN:
      {
         s << "Socket is not connected: ";
         break;
      }      
      // connect() or read()
      case EFAULT:
      {   
         s << "The address is outside the user's "
              "address space: ";
         break;
      }      
      case EISCONN:
      {
         s << "The socket is already connected: ";
         break;
      }   
      case ECONNREFUSED:
      {
         s << "Connection refused at server: ";
         break;
      }
      case ECONNRESET:
      {
         s << "Connection reset by peer: ";
         break;
      }
      case ETIMEDOUT:
      {
         s << "Operation timed out: ";
         break;
      }
      case ENETUNREACH:
      {
         s << "Network is unreachable: ";
         break;
      }
      case EADDRINUSE:
      {
         s << "Address already in use: ";
         break;
      }   
      case EINPROGRESS:
      {
         s << "Operation in progress: ";
         break;
      }
      case EALREADY:
      {
         s << "Socket is non-blocking and a "
              "previous connection attempt has not completed yet: ";
         break;
      }   
      case EAFNOSUPPORT:
      {
         s << "Address didn't have sa_family set "
              "properly, try AF_INET: ";
         break;
      }   
      // write() or read()
      // case EINTR = case NO_ADDRESS
      case EPIPE:
      {
         s << "Broken pipe: ";
         break;
      }
      case ENOSPC:
      {
         s << "Can't write to device - no space left: ";
         break;
      }   
      case EROFS:
      {
         s << "Read only file system: ";
         break;
      }      
      case EIO:
      {
         s << "Low-level I/O error while modifying inode: ";
         break;
      }
      case(EAGAIN):
      {
         s << "Resource temporarily unavalable: ";
         break;
      }      
      // read()
      case EISDIR:
      {
         s << "Descriptor points to a directory: ";
         break;
      }   
      //
      case ENXIO:
      {
         s << "No such device or address: ";
         break;
      }         
      case(ENODEV):
      {
         s << "No such device: ";
         break;
      }
      case(E2BIG):
      {
         s << "Arg list too long: ";
         break;
      }   
      case(EBADMSG):
      {
         s << "Bad message: ";
         break;
      }   
      case(EBUSY):
      {
         s << "Resource busy: ";
         break;
      }   
      case(ECANCELED):
      {
         s << "Operation canceled: ";
         break;
      }      
      case(EDEADLK):
      {
         s << "Resource deadlock avoided: ";
         break;
      }         
      case(EDOM):
      {
         s << "Domain error: ";
         break;
      }         
      case(EEXIST):
      {
         s << "File exists: ";
         break;
      }            
      default:
      {
         s << "No information available: ";    
         break;
      }
   }
   
   s << from << "( errno = " << errno << " )";
   return s.str();
}
   

//-----------------------------------------------------------------------------
//
//: Sets delay for mpiAPI communication.
//
//!param: const UINT4 sec - Delay value.
//
//!return: Nothing.
//   
void MsgSocket::setMpiCommunicationDelay( const UINT4 sec )
{
   mMpiApiCommDelay = sec;
   return;
}

   
//-----------------------------------------------------------------------------
//
//: Sets timeout value to wait for mpiAPI response message.
//
//!param: const UINT4 sec - Timeout value.
//
//!return: Nothing.
//   
void MsgSocket::setMpiAPIResponseTimeout( const UINT4 sec )
{
   mTimeOut = sec;
   return;
}
   

//-----------------------------------------------------------------------------
//
//: Can message be sent through the socket?
//
// This method checks if specified message can be sent to mpiAPI. If not, the
// message will be buffered for the next send.
//   
//!param: const std::string& msg - A message to be sent.
//
//!return: const bool - True if message can be sent, false otherwise.
//      
const bool MsgSocket::canSendMessage( const std::string& msg )
{
   const REAL8 dt( MPI::Wtime() - mLastWriteTime );
   if( dt < mMpiApiCommDelay )
   {
      if( msg.empty() == false )
      {
         // Buffer the message for later send.
         mMpiApiCommBuffer += msg;
         mMpiApiCommBuffer += '\n';
      }

      return false;
   }
   
   return true;
}
