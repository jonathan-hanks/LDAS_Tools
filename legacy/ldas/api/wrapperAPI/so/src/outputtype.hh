#ifndef WrapperApiOutputTypeHH
#define WrapperApiOutputTypeHH

// General Header Files   

#include "general/unordered_map.hh"

// Local Header Files   
#include "ldasdatatype.hh"   
#include "ldasdata.hh"
#include "mpilaltypes.hh"   


//------------------------------------------------------------------------------
//
//: Hash functional for <i>catagory</i>.
//
class hashCatagory
{
   
public:   

   //: Hash function.
   //
   //!param: const catagory e - A catagory value used for hash search.
   //   
   //!return: size_t - Value corresponding to the key.
   //
   size_t operator()( const catagory e ) const
   {
     General::hash< INT4 > h;
     return h( static_cast< INT4 >( e ) );
   }
   
};
   
   
//------------------------------------------------------------------------------
//
//: OutputType class.
// 
// This class is a containment class for the array of C outPut data structures,
// which represents the data of the structures in a form of a MPI derived
// datatype.   
// <p> The behavior of this class will differ based on which node instantiates 
// the object. Slave node is responsible for passing the data structure 
// information to the master node in the communicator. Only master node and 
// one slave node specified by <b>rank</b> value, passed to the constructor,
// build the MPI derived datatype to represent contained array of outPut data
// structures for communication. 
// <p> This class is used only when master node collects result data from the
// slaves in the communicator. After master gathers the results,
// it converts the array of result structures to the ILWD format element and 
// passes it to the resultAPI.   
//
class OutputType : public LdasDatatype
{
   
public:
   
   /* Constructor */
   OutputType( outPut** re, const UINT4 num,
      const MPI::Intracomm& comm = MPI::COMM_WORLD, 
      const INT4 rank = -1 );
   
   /* Destructor */
   ~OutputType();

   /* Helper methods, called by all nodes. */
   static const bool getInitState();
   static void setInitState( const bool v );
   
   static void cleanupMetaData();
   static void cleanup( outPut* result, const UINT4 num,
      const MPI::Intracomm& comm = MPI::COMM_WORLD );

   
   /* Helper methods, called only by slave nodes. */
   INT8* getBottom();
   
   /* Helper methods called only by master node. */
   const ILwd::LdasContainer* toILwd( const UINT4 index ) const; 

   static const INT4 initRank( const MPI::Comm& comm, const INT4 rank );    
   static const ILwd::LdasContainer* resultToILwd( const outPut& pe,
      const MPI::Intracomm& comm );
   static const REAL8 getCreateTime();   
   
   // Only for testing the data!!!
   void write() const;

private:

   //: No default constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //               
   OutputType( const MPI::Intracomm& comm = MPI::COMM_WORLD, 
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //               
   OutputType( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //               
   OutputType( const OutputType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //               
   OutputType& operator=( const OutputType& re ); 
   
   /* Typedefs */
   typedef General::unordered_map< catagory, std::string, hashCatagory >
   CatagoryHash;   
   
   /* Helper methods: called by master node only */
   static ILwd::LdasContainer* outputToILwd( const outPut& re,
      const MPI::Intracomm& comm );
   
   /* Helper methods: called by all nodes in communicator */

   void allocateMetaData( UINT4** numStates, UINT4** numDB,
      UINT4** numOpt, MPIStructInfo*** states, MPIDataBaseInfo*** db, 
      MPIStructInfo*** opt );
   
   static void deleteMetaData( const UINT4 num, UINT4** numStates, 
      UINT4** numDB, UINT4** numOpt, MPIStructInfo*** states, 
      MPIDataBaseInfo*** db, MPIStructInfo*** opt );

   // Used only if communicateOutput=ONCE
   void setMetaData( const UINT4* numStates, const UINT4* numDB,
      const UINT4* numOpt, MPIStructInfo**const states,
      MPIDataBaseInfo**const db, MPIStructInfo**const opt );

   const bool compareMetaData( const UINT4* numStates, const UINT4* numDB, 
      const UINT4* numOpt, MPIStructInfo**const states, 
      MPIDataBaseInfo**const db, MPIStructInfo**const opt ) const;
   
   void broadcastInfo( const outPut* pe, 
      UINT4* numState, MPIStructInfo** stateInfo, 
      UINT4* numDB, MPIDataBaseInfo** dbInfo,
      UINT4* numOptional, MPIStructInfo** optionalInfo );
   
   static void setMultiDimMetadata( const multiDimData* source, 
      MPIStructInfo* const meta, const MPI::Intracomm& comm );
   
   void buildType( outPut* pe, std::vector< MPI::Datatype >& sType, 
      std::vector< MPI::Datatype >& dbType, std::vector< MPI::Datatype >& optType ); 
   
   virtual const CHAR* getDatatypeName() const;      
   
   void storeData( outPut* pe, const UINT4 num );

   static CatagoryHash initCatagoryHash();
   
   //: Catagory hash map.
   static const CatagoryHash mCatagoryHash;
   
   //!ignore_begin:
   // These static data members are only used when 
   // communicateOutput is set to ONCE.
   //!ignore_end:
   
   //: State of datatype generation.
   static bool   mInitState;
   
   //: Number of elements in outPut array first time
   //: datatype gets generated.
   static UINT4  mInitNum;
   
   //: Number of nodes in stateVector linked list for all
   //: elements in mOutput first time datatype gets generated.   
   static UINT4* mNumStates;
   
   //: Number of nodes in dataBase linked list for all
   //: elements in mOutput first time datatype gets generated.      
   static UINT4* mNumDataBase;
   
   //: Number of elements in array of multiDimData
   //: structures for all elements in mOutput first time 
   //: datatype gets generated.      
   static UINT4* mNumOptional;   
   
   //: Metadata for stateVector linked list for all elements in 
   //: mOutput first time datatype gets generated.
   static MPIStructInfo** mStatesInfo;
   
   //: Metadata for dataBase linked list for all elements in mOuput
   //: first time datatype gets generated.   
   static MPIDataBaseInfo**   mDataBaseInfo;   
   
   //: Metadata for array of multiDimData structures for all
   //: elements in mOuput first time datatype gets generated.   
   static MPIStructInfo** mOptionalInfo;   
   
   //: Vector of node ranks in COMM_WORLD for which datatype has
   //: been generated.
   static std::vector<INT4> mInitSlaves;

   //: Number of MPI datatypes per outPut structure.
   static const UINT4 mTypeNum;   

   //: Time spent in datatype generation.
   static REAL8 mCreateTime;
   
   //: Number of elements in mOutput array
   UINT4 mNum;
   
   //: Rank of the node in the COMM_WORLD.
   const INT4  mCommWorldRank;

   //: Pointer to the array of C outPut data structures to 
   //: store result data.
   outPut* mOutput;
  
};
   

//------------------------------------------------------------------------------
//
//: Gets an absolute address of the data.
//
// This method gets an absolute address of the array of outPut data structures.
//   
//!return: INT8* - An address "ZERO" of the mOutput array of data        
//+        structures. 
//    
inline INT8* OutputType::getBottom() 
{
   return &( mOutput[ 0 ].indexNumber );
}
   
   
#endif // WrapperApiOutputTypeHH  

   
