#include "LDASConfig.h"

// ILWD Header Files
#include <ilwd/ldascontainer.hh>   
   
// Local Header Files
#include "ldasdatatype.hh"   
#include "mpicmdargs.hh"   
#include "ldaserror.hh"   
#include "ldasdata.hh"   
   
using namespace std;   

using ILwd::LdasContainer;   
using ILwd::LdasElement;      

   
// perceps can't handle static data member initialization 
// properly ===> ignore it.   
//!ignore_begin:   

// Initialize static data members   
const INT4 LdasDatatype::mPrimaryPos( 3 );   
REAL8 LdasDatatype::mPackDelta( 0.0f );      
REAL8 LdasDatatype::mBcastDelta( 0.0f );         
   
//!ignore_end:
   
   
//------------------------------------------------------------------------------
//
//: Default constructor.
//   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator 
//+       within which datatype is defined. Default is MPI::COMM_WORLD.
//!param: const INT4 rank - Rank of the node sending the data. Default is -1.   
//   
LdasDatatype::LdasDatatype( const MPI::Intracomm& comm, const INT4 rank ) 
   : MPI::Datatype(), mComm( comm ), mRank( rank ), mMaster( false )
{
   // node value is in the comm intracommunicator domain
   if( mComm.Get_rank() == 0 )
   {
      mMaster = true;
   }
}   

   
//------------------------------------------------------------------------------
//
//: Destructor
//
LdasDatatype::~LdasDatatype()
{}

   
//------------------------------------------------------------------------------
//
//: Assignment operator.
//      
// For the convenience this method will mark the datatype being assigned to
// for the deallocation. MPI assignment operator is shallow( handle-based ).
// 
//!param: const MPI::Datatype& re - A reference to the datatype to assign from.
//
//!return: LdasDatatype& - A reference to this object.
//
LdasDatatype& LdasDatatype::operator=( const MPI::Datatype& re ) 
{
   if( this == &re )
   {
      return *this;
   }

   if( *this != MPI::DATATYPE_NULL )
   {
      Free();
   }
   
   // MPI::Datatype assignment operator is handle-based( shallow )
   // assignment.
   MPI::Datatype::operator=( re );

   
   return *this; 
}
   

//------------------------------------------------------------------------------
//
//: Equal comparison operator.
//  
//!param: const MPI::Datatype& re - A reference to the MPI datatype object to 
//+       compare to.
//
//!return: bool - True if objects are equal, false otherwise.
//   
bool LdasDatatype::operator==( const MPI::Datatype& re ) const
{
   return( MPI::Datatype::operator==( re ) );
}
   
   
//------------------------------------------------------------------------------
//
//: Not equal comparison operator.
//
//!param: const MPI::Datatype& re - A reference to the MPI datatype object to  
//+       compare to.
//
//!return: bool - True if objects are not equal, false otherwise.   
//   
bool LdasDatatype::operator!=( const MPI::Datatype& re ) const
{
   return !LdasDatatype::operator==( re );
}
   

//------------------------------------------------------------------------------
//
//: Creates MPI datatype.
//      
// This method creates MPI derived datatype constructed from basic MPI datatypes.
// All nodes in the communicator must participate in this call.
//    
//!param: const INT4& num - Number of datatype blocks.
//!param: const INT4* blocks - An array of number of elements in each block.
//!param: MPI::Aint* disp - An array of byte displacement of each block.
//!param: const MPI::Datatype* types - An array of type of elements in each 
//+       block.   
//
//!return: Nothing.   
//
//!exc: Error generating <b>type</b>. - Error generating MPI datatype.
//!exc: MPI::Exception - MPI library exception.           
//!exc: bad_alloc - Memory allocation failed.   
//       
void LdasDatatype::createType( const INT4& num, const std::vector< INT4 >& blocks, 
   std::vector< MPI::Aint >& disp, const std::vector< MPI::Datatype >& types )
{
   for( INT4 index = num - 1; index >= 0; --index )
   {
      disp[ index ] -= disp[ 0 ];
   }
   
   const INT4* blocks_array( num == 0 ? 0 : &( blocks[ 0 ] ) );
   const MPI::Aint* disp_array( num == 0 ? 0 : &( disp[ 0 ] ) );
   const MPI::Datatype* types_array( num == 0 ? 0 : &( types[ 0 ] ) );   

   *this = Create_struct( num, blocks_array, disp_array, types_array );
   
   
   if( *this == MPI::DATATYPE_NULL )
   {
      string msg( "Error generating " );
      msg += getDatatypeName();
      node_error( msg.c_str(), getComm(), isP2P() );
   }

   
   Commit();

   return;   
}

   
//------------------------------------------------------------------------------
//
//: Creates NULL MPI datatype.
//      
// This method sets current MPI derived datatype to the MPI::DATATYPE_NULL.
//    
//!return: Nothing.   
//
//!exc: MPI::Exception - MPI library exception.           
//!exc: bad_alloc - Memory allocation failed.   
//       
void LdasDatatype::createNULLType()
{
   *this == MPI::DATATYPE_NULL;
   
   return;   
}
   
   
//-----------------------------------------------------------------------------
//
//: Finds elements within ILwd container by namekey.
//    
// This method searches input container and returns vector of LdasElements 
// that contain the namekey in their name attribute in the specified name 
// field.
//   
//!param: const LdasContainer& c - A reference to an ILWD container. 
//!param: const string& namekey - Name value to search for. The default        
//+       value is "data".         
//!param: size_t i - The name field to search. The default is 1.   
//!param: const bool check_empty - A flag to indicate if exception should be       
//+       raised in case if no elements have been found. True if error      
//+       must be issued, false otherwise. Default is true.   
//
//!return: vector< const LdasElement* > - A vector of ILWD elements.
//  
//!exc: namekey is missing. - ILWD element containing the <i>namekey</i> in the    
//+     specified index position was not found.
//!exc: bad_alloc - Memory allocation failed.   
//   
std::vector< const ILwd::LdasElement* > LdasDatatype::findILwd( 
   const ILwd::LdasContainer& c, const std::string& namekey, size_t i,
   const bool check_empty ) const
{
    ilwdVector temp;
    LdasContainer::const_iterator end( c.end() );   
   
   
    for( LdasContainer::const_iterator iter = c.begin(); 
         iter != end; ++iter )
    {
       if( ( *iter )->getName( i ) == namekey )
       {
          temp.push_back( *iter );
       }
    }

   
    if( check_empty && temp.empty() )
    {
       string msg( namekey );
       msg += " is missing.";
       node_error( msg.c_str(), mComm, ( mRank >= 0 ) );
    }
   
   
    return temp;
}
   

//-----------------------------------------------------------------------------
//
//: Finds element within ILwd container by name.
//    
// This method searches input ILWD format container and returns LdasElement 
// that contains the namekey in its name attribute in the specified name field. 
// This method is called only be the master node.   
//   
//!param: const LdasElement& re - A reference to an ILwd Element. The default 
//+       value is "domain".   
//!param: const string& namekey - Name value to search for.   
//!param: size_t i - The name field to search. The default is 0.
//!param: const bool required - A flag to indicate if searched element is
//+       required : exception must be raised in case if no element has been 
//+       found. True if required, false otherwise. Default is true.      
//
//!return: const LdasElement* - An ILWD element.
//  
//!exc: bad_cast - ILWD element passed to the method is not an ILwd container.
//!exc: <i>namekey</i> is missing. - ILWD element containing the <i>namekey</i>       
//+     in the specified index position was not found.
//   
const ILwd::LdasElement* LdasDatatype::findElement( 
   const ILwd::LdasElement& re, const std::string& namekey, size_t i,
   const bool required ) const
{
    const LdasContainer& c( dynamic_cast< const LdasContainer& >( re ) );
   
    LdasContainer::const_iterator iter( c.begin() ),
                                  end_iter( c.end() );
   
   
    while( iter != end_iter )
    {
       if( ( *iter )->getName( i ) == namekey )
       {
          return *iter;
       }
   
       ++iter;
    }

    if( required )
    {
       string msg( "ilwd element \"" );
       msg += namekey;
       msg += "\" is missing.";
       node_error( msg.c_str(), mComm, ( mRank >= 0 ) );
    }
    
    return 0;   
}

   
//-----------------------------------------------------------------------------
//
//: Finds container element by name.
//    
// This method searches input ILWD format container and returns LdasElement 
// that contains the namekey in its name attribute in the specified name field. 
// This method is called only be the master node.   
//   
//!param: const LdasContainer& re - A reference to an ILwd format container.
//!param: const string& namekey - Name value to search for.
//!param: size_t i - The name field to search. The default is 0.   
//
//!return: const LdasElement* - An ILWD element.
//  
//!exc: <i>namekey</i> is missing. - ILWD element containing the <i>namekey</i>       
//+     in the specified index position was not found.
//   
const ILwd::LdasElement* LdasDatatype::findContainerElement( 
   const ILwd::LdasContainer& c, const CHAR* namekey, size_t i ) const
{
    LdasContainer::const_iterator iter( c.begin() ),
                                  end_iter( c.end() );
   
    while( iter != end_iter )
    {
       if( ( *iter )->getName( i ) == namekey )
       {
          return *iter;
       }
   
       ++iter;
    }

    string msg( "ilwd element \"" );
    msg += namekey;
    msg += "\" is missing.";
    node_error( msg.c_str(), mComm, ( mRank >= 0 ) );
    
    return 0;   
}
   
   
//-----------------------------------------------------------------------------
//
//: Destructs vector elements.
//    
// This method frees dynamically allocated memory for the vector elements.
//
//!param: ilwdResultVector& v - A reference to the vector of ILWD format 
//+       elements.
//
//!return: Nothing.
//   
void LdasDatatype::vectorCleanup( ilwdResultVector& v )
{
   ilwdResultVector::iterator iter( v.begin() ), 
                              end_iter( v.end() );
   
   while( iter != end_iter )
   {
      delete *iter;
      *iter = 0;
      ++iter;
   }
   
   return;
}


