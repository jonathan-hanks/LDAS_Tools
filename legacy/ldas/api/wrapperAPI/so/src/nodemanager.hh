#ifndef NodeManagerHH
#define NodeManagerHH
   
// Local Header Files
#include "dsoutil.hh"   
   
   
class DLErrorHandler;
class lbNodeInfo;

   
//------------------------------------------------------------------------------
//   
//: Pure virtual base functional for node data management.
//   
class NodeDataManager
{
public:
   
   NodeDataManager( DLErrorHandler& dl_error, lbNodeInfo& lb_node,
      SearchOutput& search_output );

   //
   //: Destructor.  
   //
   virtual ~NodeDataManager(){}
   
   //
   //: Pure virtual call operator.
   //
   // This method is implemented by derived classes.
   //  
   //!param: MPI::Intracomm& comm - A reference to the MPI intracommunicator      
   //+       within which data is sent.    
   //!param: const UINT4 num_nodes - A number of slave nodes in LB communicator.   
   //+       Default is 0.   
   //
   virtual void operator()( const MPI::Intracomm& comm,
      const UINT4 num_nodes = 0 ) = 0;

   void initialize();   
   void finalize( const MPI::Intracomm& comm );
   
   void setFreeOutput( free_fptr free_output );
   void setNumSequences( const UINT4 num );
   void setAnalyzedSeconds( const REAL8 dt );
   void cleanup( inPut** input, SearchParams& search_params,
      void* dl_handle, fnlz_fptr finalize_search, const bool finalize = true );
   
   void simulateData( const MPI::Intracomm& comm );
   
protected:

   //: Reference to the dso error handler.
   DLErrorHandler& mDLError;
   
   //: Reference to the node information structure.
   lbNodeInfo&     mLBNode;
   
   //: Reference to the SearchOutput parameters.
   SearchOutput&   mSearchOutput;
   
   //: Pointer to the freeOutput function in loaded dso.
   free_fptr       mFreeOutput;
   
   void deleteMPIDatatypes();   
   
   /* Getter methods */
   const MPI::Datatype& getOutputType() const;
   
   /* Mutators */
   void setOutputType( MPI::Datatype& output_type );   

   //
   //: Sends( slave )/receives( master ) host name information.
   //
   // This method is implemented by derived classes.
   //  
   virtual void communicateHostName( CHAR* const host ) = 0;      

   //
   //: Sends( slave )/receives( master ) finalizing message.
   //
   // This method is implemented by derived classes.
   //     
   virtual void finalizeMessage( const MPI::Intracomm& comm, 
      CHAR* const msg ) = 0;   
   
   //
   //: Sends final job information to the resultAPI.
   //
   // This method is implemented by derived classes.
   //  
   virtual void sendFinalInfo( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const bool p2p = false ) = 0;   
   
   static const UINT4 mMsgSize;
   
   static REAL8 mSendILwdTime;
   
   //: Number of data seconds analyzed by the job
   UINT8 mAnalyzedSeconds;   
   
private:

   //: MPI derived datatype to represent array of outPut structures
   //: if communicateOutput = ONCE.
   MPI::Datatype mOutputType;
   
   UINT4 mInputSequences;
  
};
   
   
#endif
   
