#ifndef WrapperAPILalTypesHH
#define WrapperAPILalTypesHH

// Local Header Files
#include "wrapperInterfaceDatatypes.h"

#define LDAS_MPI_CHAR MPI::CHAR
#define LDAS_MPI_UCHAR MPI::UNSIGNED_CHAR
#define LDAS_MPI_BOOLEAN MPI::UNSIGNED_CHAR   

   
#if SIZEOF_SHORT == 2
#define LDAS_MPI_INT2 MPI::SHORT
#define LDAS_MPI_UINT2 MPI::UNSIGNED_SHORT
#elif SIZEOF_INT == 2
#define LDAS_MPI_INT2 MPI::INT
#define LDAS_MPI_UINT2 MPI::UNSIGNED   
#else
#   error "Error: No 2 byte integer found"
#endif

   
#if SIZEOF_INT == 4
#define LDAS_MPI_INT4 MPI::INT
#define LDAS_MPI_UINT4 MPI::UNSIGNED
#elif SIZEOF_LONG == 4
#define LDAS_MPI_INT4 MPI::LONG
#define LDAS_MPI_UINT4 MPI::UNSIGNED_LONG
#else
#   error "Error: No 4 byte integer found"
#endif

   
#if SIZEOF_LONG != 8 && SIZEOF_LONG_LONG != 8
#   error "Error: No 8 byte integer found"
#endif

   
#if SIZEOF_FLOAT == 4
#define LDAS_MPI_REAL4 MPI::FLOAT   
#else
#   error "Error: No 4 byte real found"
#endif

   
#if SIZEOF_DOUBLE == 8
#define LDAS_MPI_REAL8 MPI::DOUBLE
#else
#   error "Error: No 8 byte real found"
#endif
   

#ifdef WrapperAPILalTypesCC
#define LAL_TYPES 
#else
#define LAL_TYPES extern
#endif
   
   
// Since MPICH doesn't have a datatype for 8-byte integer,
// have to generate that type ourself.   
LAL_TYPES MPI::Datatype LDAS_MPI_INT8;
LAL_TYPES MPI::Datatype LDAS_MPI_UINT8;

   
// MPI datatypes to handle complex datatypes   
LAL_TYPES MPI::Datatype LDAS_MPI_COMPLEX8;
LAL_TYPES MPI::Datatype LDAS_MPI_COMPLEX16;   
   
   
// Structures to represent data information used
// for communication    
   
//-------------------------------------------------------------------------------   
//   
//: Maximum number of linked list nodes( needed 
//  to allocate max memory buffer for metadata communication ).
//      
extern const INT4 listMaxNodes;
   

//-------------------------------------------------------------------------------   
//   
//: Structure to represent metadata of the linked list node.   
//   
// This structure is used only for metadata communication. Used by master node
// to send inPut data to the slave nodes, and by slave node to send outPut 
// result data to the master.
//   
struct MPIListInfo
{
   //: Type of the node data.
   datatype type;
   
   //: Number of data elements.
   UINT4    numData;
};  
   

//-------------------------------------------------------------------------------   
//   
//: Maximum number of row dimensions for the dataBase data( needed 
//  to allocate max memory buffer for metadata communication ).
//      
extern const UINT4 dbMaxDims;   
   
   
//-------------------------------------------------------------------------------   
//   
//: Structure to represent metadata of dataBase linked list node.
//   
// This structure is used only for metadata communication. Used by slave node
// to send outPut result data to the master.
//   
struct MPIDataBaseInfo
{
   //: Type of the node data.
   datatype type;
   
   //: Number of data elements.
   UINT4    numData;
   
   //: Data dimensions.
   //
   // This structure member was added to support blobs for char, char_u 
   // data types( before MPIListInfo would be enough ). This member will
   // be set to NULL for all other data types.
   UINT4*   dimensions;  
};  
      

//-------------------------------------------------------------------------------   
//   
//: Maximum number of data dimensions for the multiDimData data( needed 
//  to allocate max memory buffer for metadata communication ).
//   
extern const INT4 multiDimDataMaxDims;
   
   
//-------------------------------------------------------------------------------   
//   
//: Structure to represent metadata of the data structure.   
//      
// This structure is used only for metadata communication. Used by master node
// to send inPut data to the slave nodes, and by slave node to send outPut 
// result data to the master.
//   
struct MPIStructInfo   
{
   //: Domain type of the data.
   domain       space;
   
   //: Type of the data.
   datatype     type;
   
   //: Number of history nodes.
   UINT4        numHistory;
   
   //: Number of data dimensions.
   UINT4        numberDimensions;

   //: Number of channels
   UINT4        numberChannels;

   //: Buffer size to hold channel names.
   UINT4        channelSize;

   //: Number of detector structures 
   UINT4        numberDetectors;
   
   //: Data dimensions.
   UINT4*       dimensions;      
   
   //: History metadata.
   MPIListInfo* historyInfo;
};
   
   
// MPI datatypes to represent metadata information
// NOTE: LDAS_MPI_LIST_INFO is used to represent MPIListInfo and
// MPIDataBaseInfo data structures.   
LAL_TYPES MPI::Datatype LDAS_MPI_LIST_INFO; 
LAL_TYPES MPI::Datatype LDAS_MPI_STRUCT_INFO;   
   
   
// Function declarations.   
void createLdasTypes();
void deleteLdasTypes();
   
   
#endif   
