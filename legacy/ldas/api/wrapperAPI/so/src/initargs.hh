#ifndef WrapperApiInitArgsHH
#define WrapperApiInitArgsHH

// System Header Files 
   
#include <utility>   
   
#include "general/unordered_map.hh"

// Local Header Files
#include "mpiargs.hh"   
#include "initvars.hh"   

namespace MPI
{
   class Intracomm;
}
   
namespace ILwd
{
   class LdasContainer;
}
   
namespace DB
{
   class Transaction;
   class Table;
}
   

//------------------------------------------------------------------------------
//
//: Case-sensitive string comparison functional.
//   
class stringEqual
{
public:

//------------------------------------------------------------------------------
//
//: Overloaded string equality operator.
//         
//!param: const string& s1 - A reference to the 1st string.
//!param: const string& s2 - A reference to the 2nd string.
//   
//!return: bool - True if strings are equal, false otherwise.
//         
   bool operator()( const std::string& s1, const std::string& s2 ) const
   {
      return ( s1 == s2 );
   }
};
   
   
//------------------------------------------------------------------------------
//
//: String hash functional.
//      
class hashString
{
public:
   
//------------------------------------------------------------------------------
//
//: Hash function for the string.
//         
//!param: const string& str - A reference to the string representing
//+       the key.   
//   
//!return: size_t - An index into the hash.
//            
   size_t operator()( const std::string& str ) const 
   {
      General::hash< const char* > h;
      return ( h( str.c_str() ) );
   }
};

   
//------------------------------------------------------------------------------
//
//: InitArguments functional.
//
// This functional handles parsing of wrapperAPI command line arguments.
//   
class InitArguments
{
public:

   InitArguments();
   ~InitArguments();

   void operator()( INT4 argc, CHAR* argv[] );   
   ILwd::LdasContainer* getDBDataCopy( const MPI::Intracomm& comm );
   
   static const bool getInitState();
   static void appendErrorMsg( const CHAR* msg );   
   static void appendErrorMsg( const std::string& msg );
   static void setDumpDataDir();

   void parseResourceFile() const;
   void setProcessTableJobID( const MPI::Intracomm& comm, const bool p2p );

private:

   //: Flag to indicate that initialization of arguments is in progress.
   static bool mInitState;

   //: Collective error message. 
   static std::string mErrorMsg;   

   
   //: Process information class.
   //
   // This functional is used only by master node of the COMM_WORLD.
   // It generates DB Process table which wrapperMaster will
   // send to the resultAPI in ILWD format.
   //
   class ProcessInfo
   {
   public:
      ProcessInfo();
      ~ProcessInfo();

      //: Creates process information ILWD format element.
      //      
      // This method is called only by master node of the COMM_WORLD communicator.
      // It creates ILWD format element that represents process information to be 
      // sent to the resultAPI.
      // 
      //!exc: bad_alloc - Memory allocation failed.
      //!exc: Could not parse RCS source directory. - Malformed format for RCS 
      //+     directory.
      //!exc: Error converting RCS entry time to seconds. - Malformed format is used
      //+     for RCS time.
      //!exc: Could not parse RCS time variable. - Malformed RCS time.     
      //! exc: Required db field 'hostname' is empty. - Hostname is not set.
      //!exc: Required db field 'username' is empty. - Username is not set.    
      //!exc: Required db field 'domainname' is empty. - Domain is not set.   
      //   
      void operator()();
      DB::Table* getTable() const;
      void setEndTime();
      void setJobID();   
   
   private:

      // Helper methods
      INT4 getRCSTime();
      INT4 getGPSTime();      
      void cleanup();

      //: Regex to match RCS time.
      static const Regex re_rcs_time;   
   
      //: Regex to match RCS source.
      static const Regex re_rcs_source;   
   
      static const std::string mVersion;
      static const std::string mRcsTime;
      static const std::string mRcsSource;

      //: dbaccess process table   
      DB::Table* mTable;
   
   };
   friend class ProcessInfo;   

   
   //: Process parameters information class.
   //
   // This functional is used only by master node of the COMM_WORLD.
   // It generates DB ProcessParameters table, which wrapperMaster
   // will send to the resultAPI in ILWD format.
   //   
   class ProcessParamsInfo
   {
   
   public:
      ProcessParamsInfo();
      ~ProcessParamsInfo();

      //: Creates ProcessParams DB table.
      //      
      // This method is called only by master node of the COMM_WORLD communicator.
      // It creates DB Process table that represents process parameters 
      // information to be send to the resultAPI.
      //
      //!param: DB::Table* process_table - A pointer to the DB process table to be
      //+       associated with parameters.   
      //
      //!return: Nothing.
      //    
      //!exc: bad_alloc - Memory allocation failed.
      //!exc: Process table must be created before ProcessParameters table. - Process
      //+     table is NULL.   
      //!exc: bad_cast - Malformed db data.   
      //      
      void operator()( DB::Table* const process_table );   
      DB::Table* getTable() const;
   
   private:
   
      typedef std::string ( ProcessParamsInfo::* FPtr )();
      typedef std::pair< std::string, FPtr > ppair;
      typedef General::unordered_map< std::string, ppair,
				      hashString, stringEqual > ParamsHash;
      typedef ParamsHash::const_iterator hash_iterator;

      void cleanup();
   
      // Helper methods
      std::string mTotalNodesValue();
      std::string mMpiAPIValue();
      std::string mNodeListValue();   
      std::string mDynlibValue();   
      std::string mDataAPIValue();   
      std::string mResultAPIValue();   
      std::string mFilterParamsValue();   
      std::string mRealTimeRatioValue();      
      std::string mDoLoadBalanceValue();         
      std::string mDataDistributorValue();            
      std::string mJobIDValue();               
      std::string mCommunicateOutputValue();                     
      std::string mMemoryUsageLimitValue();                        
      std::string mUserTagValue();                           
   
      static ParamsHash initHash();      
   
      //: Parameter information hash map.
      static ParamsHash mParamsHash;         

      //: dbacces process parameters table.
      DB::Table* mTable;
   };
   friend class ProcessParamsInfo;

   void initialize();   
   void cleanup();

   //: Regex to match file path.
   static const Regex re_path;
   
   //: Regex to match string value..
   static const Regex re_string;   
   
   //: Regex to match unsigned integer value.
   static const Regex re_uint; 
   
   //: Regex to match positive floating point value.
   static const Regex re_float;    
   
   //: Regex to match boolean value.
   static const Regex re_bool;    
   
   //: Regex to match dataDistributor argument.
   static const Regex re_distributor;    
   
   //: Regex to match communicateOutput argument.
   static const Regex re_communicate;    
   
   //: Program name
   static const std::string mProgramName;   
   
   // Command line argument format 
   static const std::string mTotalNodes;            
   static const std::string mMpiAPI;                            
   static const std::string mNodeList;                               
   static const std::string mDynlib;                            
   static const std::string mDataAPI;                            
   static const std::string mResultAPI;                                  
   static const std::string mFilterParams;                                     
   static const std::string mRealTimeRatio;                                        
   static const std::string mDoLoadBalance;
   static const std::string mDataDistributor;   
   static const std::string mJobID;      
   static const std::string mCommunicateOutput;            
   static const std::string mMemoryUsageLimit;           
   static const std::string mUserTag;
   
   // Communication parameters
   static const std::string mConnectResultApiNum;   
   static const std::string mConnectResultApiDelay;      
   static const std::string mSendILwdOutputDelay;
   static const std::string mConnectMpiApiDelay;         
   static const std::string mMpiApiResponseTimeout;            
   static const std::string mSyncMpiApiDso;            
   
   // Debug flags
   static const std::string mEnableMpiApi;
   static const std::string mEnableDumpInput;
   static const std::string mDumpDataDir;   
   static const std::string mRunCode;   
   static const std::string mEnableResultApi;
   static const std::string mEnableDummyOutput;
   static const std::string mEnableDummyState;
   static const std::string mEnableDummyMdd;
   static const std::string mEnableLoadDso;
   static const std::string mEnableSleepBeforeDso;
   static const std::string mEnableSleepAfterDso;
   static const std::string mEnableLdasTrace;
   static const std::string mEnableXmpiTrace;
   static const std::string mEnableDumpArgs;   
   static const std::string mEnableTimeoutTrace;      
   static const std::string mEnableTimeInfo;         
   
   // Debug values
   static std::string mDumpDataDirValue;   
   static std::string mRunCodeValue;   
   
   // Keep not parsed dso parameters( to insert into db )
   //: Not parsed dso filter parameters
   static std::string mDSOParams;

   typedef void ( InitArguments::* FPtr )( const CHAR* ) const;
   typedef General::unordered_map< std::string, FPtr,
				   hashString, stringEqual > RscFileHash;

   static const RscFileHash initRscFileHash();
   static const RscFileHash mRscFileHash;

   void initMpiAPI( const CHAR* value ) const; 
   void initDataAPI( const CHAR* value ) const;   
   void initResultAPI( const CHAR* value ) const;      
   void initRealTimeRatio( const CHAR* value ) const;      
   void initDoLoadBalance( const CHAR* value ) const;      
   void initDataDistributor( const CHAR* value ) const;      
   void initCommunicateOutput( const CHAR* value ) const;      
   void initMemoryUsageLimit( const CHAR* value ) const;      
   
   void initConnectResultApiNum( const CHAR* value ) const;      
   void initConnectResultApiDelay( const CHAR* value ) const;         
   void initSendILwdOutputDelay( const CHAR* value ) const;   
   void initConnectMpiApiDelay( const CHAR* value ) const;            
   void initMpiApiResponseTimeout( const CHAR* value ) const;               
   void initRsyncMpiApiDso( const CHAR* value ) const;               
   
   void initEnableMpiApi( const CHAR* value ) const;
   void initEnableDumpInput( const CHAR* value ) const; 
   void initDumpDataDir( const CHAR* value ) const;         
   void initRunCode( const CHAR* value ) const;            
   void initEnableResultApi( const CHAR* value ) const;
   void initNumDummyOutput( const CHAR* value ) const;   
   void initNumDummyState( const CHAR* value ) const;   
   void initNumDummyMdd( const CHAR* value ) const;   
   void initEnableLoadDso( const CHAR* value ) const;   
   void initEnableSleepBeforeDso( const CHAR* value ) const;   
   void initEnableSleepAfterDso( const CHAR* value ) const;   
   void initEnableLdasTrace( const CHAR* value ) const;   
   void initEnableXmpiTrace( const CHAR* value ) const;   
   void initEnableDumpArgs( const CHAR* value ) const;      
   void initEnableTimeoutTrace( const CHAR* value ) const;         
   void initEnableTimeInfo( const CHAR* value ) const;            
   
   static const bool parseBoolValue( const CHAR* value, 
      const std::string& arg_name );
   static const UINT4 parseUINT4Value( const CHAR* value, 
      const std::string& arg_name );   
   static void displayArguments();
   
   //: Flag to indicate that memory was allocated and initialized.
   //: This flag can be set to true only on master node.
   bool mInitialized;   
   
   ProcessInfo* mProcess;
   ProcessParamsInfo* mProcessParams;
   
   //: dbaccess transaction object necessary to generate
   //: metadataAPI expected process and process parameters
   //: information.
   DB::Transaction* mTransaction;

};
   

//------------------------------------------------------------------------------
//
//: Gets Process DB table.
//   
//!return: DB::Table* - A pointer to the DB Process table.
//   
inline DB::Table* InitArguments::ProcessInfo::getTable()
   const
{
   return mTable;
}

   
//------------------------------------------------------------------------------
//
//: Gets ProcessParameter DB table.
//   
//!return: DB::Table* - A pointer to the DB ProcessParameter table.
//      
inline DB::Table* InitArguments::ProcessParamsInfo::getTable()
   const
{
   return mTable;
}   
   
   
INIT_ARGS InitArguments initArguments;   
   
#endif   
