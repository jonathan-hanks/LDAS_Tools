#ifndef WrapperApiHistoryHH
#define WrapperApiHistoryHH

// Local Header Files   
#include "ldasdatatype.hh"   
#include "ldasdata.hh"
#include "mpilaltypes.hh"   
#include "multidimdata.hh"
   
   
//------------------------------------------------------------------------------
//
//: HistoryType class.
// 
// This class represents MPI datatype corresponding to the C dcHistory linked
// list. This class does not really contain the data, it only represents 
// the data MPI-wise.   
//
class HistoryType : public LdasDatatype, private LdasData
{
   
public:
   
   /* Constructors */
   HistoryType( ilwdConstVector& re, dcHistory** pe,
      const MPIListInfo* dataInfo, const UINT4 num,
      const INT4 rank = -1, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD ); 
   
   HistoryType( dcHistory** pe, const MPIStructInfo& dataInfo,
      const MPI::Intracomm& comm = MPI::COMM_WORLD, 
      const INT4 rank = -1 ); 
   
   /* Destructor */
   virtual ~HistoryType();

   static ILwd::LdasElement* const toILwd( const dcHistory* pe, 
      const MPI::Intracomm& comm );

   static const std::string getILwdName();
   
private:
  friend MultiDimDataType::
  MultiDimDataType( ilwdConstVector&, multiDimData**, 
		    const MPIStructInfo*, const UINT4, 
		    const INT4 rank, const MPI::Intracomm& comm); 
  friend MultiDimDataType::
  MultiDimDataType( multiDimData**, const MPIStructInfo* info,
		    const MPI::Intracomm& comm, const INT4 rank,
		    const UINT4 num );


   //: No default constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //      
   HistoryType( const MPI::Intracomm& comm = MPI::COMM_WORLD,
      const int rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //      
   HistoryType( const MPI::Datatype& re, 
      const MPI::Intracomm& comm = MPI::COMM_WORLD, 
      const INT4 rank = -1 );
   
   //: No copy constructor.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //      
   HistoryType( const HistoryType& re );

   //: No assignment operator.
   //
   // <font color="red">This method should not be used unless reference counting is
   // implemented.</font>   
   //      
   HistoryType& operator=( const HistoryType& re );
   
   /* Helper methods: called only by master node */
   void parseILwd( ilwdConstVector& re, dcHistory* pe ) const; 
   
   /* Helper methods: called by all nodes in communicator */
   void allocateListData( dcHistory** pe, 
      const MPIListInfo* dataInfo ) const;
   
   void buildType( dcHistory* pe ); 

   virtual const CHAR* getDatatypeName() const;      
   
   //: Name used for conversion to ILWD.
   static const std::string mILwdName;
   
   //: Number of MPI datatypes per dcHistory node.
   static const UINT4 mTypeNum;
   
   //: Number of characters to store name and units.
   static const UINT4 mNameUnitsNum;
   
   //: Number of nodes in the dcHistory linked list
   UINT4 mNum;

};

   
#endif   
   
