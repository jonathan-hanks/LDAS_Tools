#include "LDASConfig.h"

// System Header Files
#include <cstdlib>   
#include <sstream>   

// General Header Files
#include <general/regexmatch.hh>   
   
// Local Header Files
#include "mpiargs.hh"
#include "initargs.hh"   
#include "ldaserror.hh"   
#include "mpicmdargs.hh" 

using namespace std;
   

// Local variable  
   
/*
 * Regex details for the opening a specification range.
 *
 * "^"                    The beginning of the buffer
 * "("                    Remember beginning of the match
 *    "[[:space:]]*"      Optional space
 *    "="                 =
 *    "[[:space:]]*"      Optional space
 *    "\\("               Open scope for range
 *    "[[:space:]]*"      Optional space
 * ")"                    End of the match
 */
static const Regex re_open_arg(
   "^([[:space:]]*\\([[:space:]]*)" );
   

// perceps can't handle static data member initialization 
// properly ===> ignore it.   
//!ignore_begin:
   
/*
 * Regex details for the range of nodes:
 *
 * "^"                    The beginning of the buffer
 * "[[:space:]]*"         Optional space
 * "("                    Remember the first value of the range (it starts here)
 *    "[0-9]"             One digit number
 *    "|[1-9][0-9]"       or two-digit number
 * ")"                    End of the first value of the range
 * "[[:space:]]*"         Optional space
 * "-"                    Range separator
 * "[[:space:]]*"         Optional space
 * "("                    Remember the second value of the range (it starts here)
 *    "[0-9]"             One digit number
 *    "|[1-9][0-9]"       or two-digit number
 * ")"                    End of the second value of the range   
 * "[[:space:]]*"         Optional space
 * "("                    Open OR scope
 *    "\\)"               Close scope for range followed by
 *    "[[:space:]]*"      optional space followed by 
 *    "$"                 the end of the buffer
 *    "|,"                or , separator
 * ")"                    Close OR scope
 */   
const Regex NodeListArg::re_range(
   "^[[:space:]]*"
    "([0-9]|[1-9][0-9]+)[[:space:]]*-[[:space:]]*"
    "([0-9]|[1-9][0-9]+)[[:space:]]*(\\)[[:space:]]*$|,)" );
   
   
/*   
 * Regex details for the list of nodes:
 *
 * "^"                    The beginning of the buffer
 * "[[:space:]]*"         Optional space   
 * "("                    Remember the value of the list (it starts here)
 *    "[0-9]"             One digit number 
 *    "|[1-9][0-9]"       or two digit number
 * ")"                    End of the value of the list
 * "[[:space:]]*"         Optional space
 * "("                    Open OR scope
 *    "\\)"               Close scope for the list
 *    "[[:space:]]*"      followed by optional space   
 *    "$"                 followed by the end of the buffer
 *    "|,"                or , separator
 * ")"                    Close OR scope
 */   
const Regex NodeListArg::re_list(
   "^[[:space:]]*"
   "([0-9]|[1-9][0-9]+)[[:space:]]*(\\)[[:space:]]*$|,)" );

   
//!ignore_end:
   

//------------------------------------------------------------------------------
//
//: Overloaded call operator.
//   
// This call operator parses mixed format of ranges and lists of node ranks.
//
//!param: const CHAR* pc - A string representing the list and/or range of nodes.
//
//!return: vector< INT4 > - A vector of nodes represented by <i>pc</i> string.
//
//!exc: Bad node value. - Node value exceeds the total amount of nodes in the  
//+     global communicator COMM_WORLD.
//!exc: Bad node range. - Invalid range is specified( node value               
//+     exceeds the total amount of nodes in the global communicator or     
//+     range limits are invalid ).
//!exc: Multiple occurences of rank r in node list. - Rank r is specified      
//+     multiple number of times.   
//!exc: Bad node list format: <i>format</i>. - Invalid format specified for    
//+     node list: <i>format</i>.
//!exc: Number of nodes exceeds COMM_WORLD. - Number of nodes exceeds total    
//+     number of nodes in COMM_WORLD.   
//!exc: bad_alloc - Memory allocation failed.   
//   
std::vector< INT4 > NodeListArg::operator()( const CHAR* pc ) const
{
   RegexMatch rm( 4 );
   
   const CHAR* end( pc + strlen( pc ) );
   const CHAR* index( pc );
   
   vector< INT4 > nodes;
   bool openArg( false );   
   
   const MPI::Intracomm& comm( MPI::COMM_WORLD );   
   const bool p2p( false);
   

   while( index < end )
   { 
      if( openArg )
      {
         if( rm.match( re_range, index ) )
         {
            INT4 minR( atoi( rm.getSubString( 1 ).c_str() ) );
   
            INT4 maxR( atoi( rm.getSubString( 2 ).c_str() ) );
   
   
            if( minR > maxR ||
                maxR >= totalNodes )
            {
               if( InitArguments::getInitState() )
               {
                  InitArguments::appendErrorMsg( "Bad node range." );  
                  return nodes;
               }
               else
               {
                  node_error( "Bad node range.", comm, p2p );
               }
   
               break;
            }
   
   
            for( INT4 r = minR; r <= maxR; ++r )
            {
               if( find( nodes.begin(), nodes.end(), r ) != nodes.end() )
               {
                  ostringstream s;                  
                  s << "Multiple occurences of rank " << r 
                    << " in node list.";
   
                  if( InitArguments::getInitState() )
                  {
                     InitArguments::appendErrorMsg( s.str() );
                     return nodes;
                  }
                  else
                  {
                     node_error( s.str().c_str(), comm, p2p );
                  }   
               }
   
   
               nodes.push_back( r );
            }
         }
         else if( rm.match( re_list, index ) )
         {
            INT4 node( atoi( rm.getSubString( 1 ).c_str() ) );
   
            if( node >= totalNodes )
            {
               if( InitArguments::getInitState() )
               {
                  InitArguments::appendErrorMsg( "Bad node value." );  
                  return nodes;
               }
               else
               {
                  node_error( "Bad node value.", comm, p2p );
               }   

   
               break;
            }

   
            if( find( nodes.begin(), nodes.end(), node ) != nodes.end() )
            {
               ostringstream s;                  
               s << "Multiple occurences of rank " << node 
                 << " in node list.";
   
               if( InitArguments::getInitState() )
               {
                  InitArguments::appendErrorMsg( s.str() );  
                  return nodes;
               }
               else
               {
                  node_error( s.str().c_str(), comm, p2p );
               }
            }
   
   
            nodes.push_back( node );
         }
         else
         {
            string msg = "Bad node list format: ";
            msg += index;

            if( InitArguments::getInitState() )
            {
               InitArguments::appendErrorMsg( msg.c_str() );  
               return nodes;
            }
            else
            {
               node_error( msg.c_str(), comm, p2p );
            }   

            break;
         }
      }
      else if( !openArg && rm.match( re_open_arg, index ) )
      {
         openArg = true;
      }
      else
      {
         if( InitArguments::getInitState() )
         {
            InitArguments::appendErrorMsg( "Bad node list format." );  
            return nodes;
         }
         else
         {
            node_error( "Bad node list format.", comm, p2p );
         }   
      }

      index = rm.getSubEnd( 0 );
   }   

   
   if( static_cast< INT4 >( nodes.size() ) > totalNodes )
   {
      if( InitArguments::getInitState() )
      {
         InitArguments::appendErrorMsg( "Number of nodes exceeds COMM_WORLD." );
         return nodes;
      }
      else
      {
         node_error( "Number of nodes exceeds COMM_WORLD.", comm, p2p );
      }
   }
   
   
   return nodes;
}


// perceps can't handle static data member initialization 
// properly ===> ignore it.   
//!ignore_begin:   
   
/*
 * Regex details for the (host,port) command line argument.
 *
 * "^"                    The beginning of the buffer
 * "[[:space:]]*"         Optional space
 * "\\("                  Open scope for values
 * "[[:space:]]*"         Optional space
 * "("                    Remember the first match
 *    "[-\\.a-zA-Z0-9_]+" Any word( at least one letter )
 * ")"                    End of the first match
 * "[[:space:]]*"         Optional space
 * ","                    'comma' separator
 * "[[:space:]]*"         Optional space
 * "("                    Remember the second value of the match
 *    "[0-9]+"            Any number( at least one digit )
 * ")"                    End of the second match
 * "[[:space:]]*"         Optional space
 * "\\)"                  Close scope for values
 * "[[:space:]]*"         Optional space
 * "$"                    End of the buffer
 */
const Regex HostPortArg::re_host_port(
   "^[[:space:]]*\\([[:space:]]*"
   "([-\\.a-zA-Z0-9_]+)[[:space:]]*,"
   "[[:space:]]*([0-9]+)[[:space:]]*\\)[[:space:]]*$" );
   
//!ignore_end:
   

//------------------------------------------------------------------------------
//
//: Overloaded call operator.
//   
// This call operator parses the command line argument of the
// format (hostname,port), where hostname is a string and port is an integer 
// value.   
//
//!param: const char* pc - A string representing host and port in the format:  
//+       (hostname,port).   
//   
//!return: HostPort - An object representing a pair of host and port.
//
//!exc: Bad (hostname,port) format: <i>format</i>. - Invalid format specified: 
//+     <i>format</i>.   
//!exc: bad_alloc - Memory allocation failed.   
//   
HostPort HostPortArg::operator()( const CHAR* pc ) const 
{
   RegexMatch rm( 4 );
   
   const CHAR* index( pc );

   if( rm.match( re_host_port, index ) )
   {
      const string host( rm.getSubString( 1 ) );
      INT4 port( atoi( rm.getSubString( 2 ).c_str() ) );
   
      return HostPort( host, port );
   }   
   
   
   string msg = "Bad (hostname,port) format: ";
   msg += index;
   
   if( InitArguments::getInitState() )
   {
      InitArguments::appendErrorMsg( msg.c_str() );
   }
   else
   {
      node_error( msg.c_str(), MPI::COMM_WORLD, false );
   }
   
   
   return HostPort();
}   

   
// perceps can't handle static data member initialization 
// properly ===> ignore it.   
//!ignore_begin:   
   
/*
 * Regex details for the -filterparams: long integer parameter.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "("                 Remember the match
 *    "-?"             Optional 'minus' sign
 *    "[0-9]+"         Any number( at least one digit )
 *    "L?"             Optional L( to write explicit long literal )
 * ")"                 End of the match
 * "[[:space:]]*"      Optional space
 * "("                 Open OR scope
 *    "\\)"            Close scope for the list
 *    "[[:space:]]*"   followed by optional space   
 *    "$"              followed by the end of the buffer
 *    "|,"             or , separator
 * ")"                 Close OR scope
 */
const Regex ParameterArg::re_long(
   "^[[:space:]]*"
   "(-?[0-9]+L?)[[:space:]]*(\\)[[:space:]]*$|,)" ); 
   
   
/*
 * Regex details for the -filterparams: double precision floating-point
 * parameter.
 *
 * "^"                 The beginning of the buffer
 * "[[:space:]]*"      Optional space
 * "("                 Remember the match
 *    "-?"             Optional 'minus' sign
 *    "[0-9]*"         Optional number
 *    ".?"             Optional 'dot'
 *    "[0-9]*"         Optional number
 *    "e?"             Optional 'e'
 *    "-?"             Optional 'minus'
 *    "[0-9]*"         Optional number
 *    "[fF]?"          Optional f or F( explicit floating-point literal )
 * ")"                 End of the match
 * "[[:space:]]*"      Optional space
 * "("                 Open OR scope
 *    "\\)"            Close scope for the list
 *    "[[:space:]]*"   followed by optional space   
 *    "$"              followed by the end of the buffer
 *    "|,"             or , separator
 * ")"                 Close OR scope
 */   
const Regex ParameterArg::re_double(
   "^[[:space:]]*"
   "(-?[0-9]*\\.?[0-9]*(e-?[0-9]+)?[fF]?)"
   "[[:space:]]*(\\)[[:space:]]*$|,)" );
   

/*
 * Regex details for the -filterparams: string parameter.
 *
 * "^"                       The beginning of the buffer
 * "[[:space:]]*"            Optional space
 * "("                       Remember the match
 *    "[-a-zA-Z0-9_/\\{}() .~]+"  String( at least one character )
 * ")"                       End of match
 * "[[:space:]]*"            Optional space
 * "("                       Open OR scope
 *    "\\)"                  Close scope for the list
 *    "[[:space:]]*"         followed by optional space   
 *    "$"                    followed by the end of the buffer
 *    "|,"                   or , separator
 * ")"                       Close OR scope
 */   
const Regex ParameterArg::re_string(
   "^[[:space:]]*"
   "((\\\\,|[-:a-zA-Z0-9_/\\{}() .~])*)[[:space:]]*(\\)[[:space:]]*$|,)" );   

   
/*
 * Regex details for the -filterparams: group parameter.
 *
 * "^"                       The beginning of the buffer
 * "[[:space:]]*"            Optional space
 *    "\\("                  Group opening
 *        "("                Remember the match   
 *           "[^()]"         Anything but parenthesis( no embedded paranthesis )
 *        ")"                End of match
 *    "\\)"                  Group closing   
 * "[[:space:]]*"            Optional space
 * "("                       Open OR scope
 *    "\\)"                  Close scope for the list
 *    "[[:space:]]*"         followed by optional space   
 *    "$"                    followed by the end of the buffer
 *    "|,"                   or , separator
 * ")"                       Close OR scope
 */   
const Regex ParameterArg::re_group(
   "^[[:space:]]*"
   "\\("
   "([^()]*)"
   "\\)[[:space:]]*(\\)[[:space:]]*$|,)" );   

   
//!ignore_end:
   
   
//------------------------------------------------------------------------------
//
//: Overloaded call operator.
//   
// This call operator parses the -filterparams command line argument.
//
//!param: const CHAR* pc - A string representing the argument.
//!param: vector< string >& vs - A vector of parameters( their string          
//+       representations ).   
//   
//!return: Nothing.
//   
//!exc: Bad -filterparams format: <i>format</i> - Invalid format specified     
//+     for -filterparams argument: <i>format</i>.
//!exc: bad_alloc - Memory allocation failed.   
//   
void ParameterArg::operator()( const CHAR* pc, std::vector< std::string >& vs )
   const
{
   RegexMatch rm( 4 );
   
   const CHAR* end( pc + strlen( pc ) );
   const CHAR* index( pc );
   bool openArg( false );   
   
   
   while( index < end )
   { 
      if( openArg )
      {
         if( rm.match( re_long, index ) )
         {
            vs.push_back( rm.getSubString( 1 ) );
         }
         else if( rm.match( re_double, index ) )
         {
            vs.push_back( rm.getSubString( 1 ) );
         }
         // Since string_re includes (), try group_re
         // first, otherwise group match will be swallowed by
         // string_re
         else if( rm.match( re_group, index ) )
         {
            vs.push_back( rm.getSubString( 1 ) ); 
         }   
         else if( rm.match( re_string, index ) )
         {
            vs.push_back( rm.getSubString( 1 ) ); 
         }   
         else
         {
            string msg = "Bad -filterparams format: ";
            msg += index;

            if( InitArguments::getInitState() )
            {
               InitArguments::appendErrorMsg( msg.c_str() );
               return;
            }
            else
            {
               node_error( msg.c_str(), MPI::COMM_WORLD, false );
            }
   
            break;
         }
      }
      else if( !openArg && rm.match( re_open_arg, index ) )
      {
         openArg = true;
      }
      else
      {
         string msg = "Bad -filterparams format: ";
         msg += index;   

         if( InitArguments::getInitState() )
         {
            InitArguments::appendErrorMsg( msg.c_str() );
            return;
         }
         else
         {
            node_error( msg.c_str(), MPI::COMM_WORLD, false );
         }
      }
   

      index = rm.getSubEnd( 0 );
   }   
   
   
   return;
}   
   

   

   
   
