#include "LDASConfig.h"

// System Header Files
#include <fstream>   
#include <unistd.h>   
#include <sys/time.h>   
#include <algorithm>   
#include <iostream>      
#include <sstream>   
#include <iterator>   
#include <sys/types.h>   
#include <sys/stat.h>   

// ILWD Header Files
#include <ilwd/style.hh>   
#include <ilwd/util.hh>      
#include <ilwd/ldascontainer.hh>   
   
// GenericAPI Header Files
#include <genericAPI/socketcmd.hh>   
#include <genericAPI/elementcmd.hh> 
#include "genericAPI/OSpaceException.hh"
#include <genericAPI/registry.hh>   
   
// Local Header Files
#include "mastermanager.hh"
#include "mpiutil.hh"   
#include "outputtype.hh"   
#include "ldaserror.hh"   
#include "initargs.hh"    
#include "nodeinfo.hh"   
#include "mpicmdargs.hh"   
#include "msgsocket.hh"      
#include "ldastag.hh"   
#include "ldasrequest.hh"   
#include "initvars.hh"   
   
   
using namespace std;
   
using ILwd::LdasContainer;
using ILwd::LdasElement;
using ILwd::Format;
using ILwd::Compression;   

using GenericAPI::ErrConnectFailure;




// perceps can't handle static data member initialization 
// properly ===> ignore it.   
//!ignore_begin:   
   
// Static data member initialization   
const INT4 MasterDataManager::mWarningTimeout( 300 );
INT4 MasterDataManager::mLastTimeSeconds( 0 );
   
UINT4 MasterDataManager::mFileCounter( 0 );    
const string MasterDataManager::mFileName( "output_" );   
const string MasterDataManager::mFileExtension( ".ilwd" );      
const INT4 MasterDataManager::mSearchMaster( 1 );      
vector< INT4 > MasterDataManager::mReceivedSlaves;
UINT4 MasterDataManager::mConnectNum( 1 );
UINT4 MasterDataManager::mConnectDelay( 250000 );
UINT4 MasterDataManager::mOutputDelay( 0 );   
const string MasterDataManager::mProcessILwdMeta( "wrapperApiObjects" );   
const string MasterDataManager::mOutputILwdMeta( "analyzedDataSeconds" );   
   
   
// For debugging only!!!
vector< INT_4U > traced_ranks;

static struct timeval currTime;   

//!ignore_end:
   
   
//------------------------------------------------------------------------------
// 
//: Constructor.
//   
//!param: DLErrorHandler& dl_error - A reference to the dso error handler.
//!param: lbNodeInfo& lb_node - A reference to the object containing node       
//+       information.   
//!param: SearchOutput& search_output - A reference to the SearchOutput         
//+       structure containing the result data for the node.   
//!param: const REAL8 start_time - Process's start time ( for 'finalizing' 
//+       message to the mpiAPI)   
//!param: const string name - Name for ILWD format element storing outPut data  
//+       converted to ILWD format. Default is "output:wrapperAPI".   
//   
//!exc: bad_alloc - Memory allocation failed.
//   
MasterDataManager::MasterDataManager( DLErrorHandler& dl_error,
   lbNodeInfo& lb_node, SearchOutput& search_output, const REAL8 start_time,  
   const std::string name )
   : NodeDataManager( dl_error, lb_node, search_output ),
     mResultILwd( 0 ), mTimeout( totalNodes, -1 ), mTimeoutCounter( totalNodes, 0 ),
     mStartTime( start_time ), mOutputSentTime( 0.0f ), mOutputILwdMetaSet( false )
{
   mResultILwd = new LdasContainer( name );
}
   

//------------------------------------------------------------------------------
//
//: Overloaded call operator.
//
// This method is used by master node in the communicator to receive
// result outPut data from the slaves. Master node converts each outPut
// data structure to the ILWD format element and sends it to the resultAPI.   
//
//!param: MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent( master + LBcommunicator ).
//!param: const UINT4 num_nodes - A number of slave nodes in LB communicator.    
//+       Default is 0.   
//   
//!return: Nothing.
//      
//!exc: Illegal number of outPut structures. - Illegal number of outPut          
//+     structures is specified: <= 0.
//!exc: Empty array of outPut structures when number of elements is not zero. -  
//+     Array of outPut data structures is empty even though number of elements        
//+     in it is not zero.
//!exc: Index exceeds the size of packing buffer. - Index into the binary        
//+     buffer used for storing the metadata exceeds the size of that       
//+     buffer.  
//!exc: NULL store in stateVector. - Malformed outPut data structure, such as    
//+     one of the nodes in stateVector linked list has store set to NULL.      
//!exc: Inconsistent outPut data structure signature. - Sceleton for outPut      
//+     data structure differs from original signature. This error may occure 
//+     only if communicateOutput=ONCE.   
//!exc: Undefined datatype. - Undefined datatype was specified.         
//!exc: Undefined domain. - Undefined domain was specified.                      
//+     One of TIME | FREQ | BOTH.         
//!exc: Invalid number of dataBase nodes. - Negative number of dataBase nodes   
//+     was specified.   
//!exc: Error generating DataBaseType. - Error generating a MPI derived         
//+     datatype to represent dataBase linked list.   
//!exc: Invalid number of stateVector nodes. - Negative number specified for        
//+     number of nodes in the linked list.      
//!exc: Error generating StateVectorType. - Error generating MPI derived        
//+     datatype representing the stateVector linked list.   
//!exc: Invalid number of history nodes specified. - Negative number of nodes    
//+     was specified.         
//!exc: Error generating HistoryType. - Error generating MPI derived datatype    
//+     to represent history linked list.   
//!exc: Error generating IntervalType. - Error generating MPI derived datatype   
//+     to represent C interval union.      
//!exc: Invalid number of multiDimData. - Negative number of array elements is   
//+     specified.         
//!exc: Error generating MultiDimDataType. - Error generating MPI derived        
//+     datatype to represent an array of C multiDimData structures.            
//!exc: Memory allocation failed. - Error allocating memory. 
//!exc: Error generating OutputType. - Error generating MPI datatype to          
//+     represent an array of C outPut data structures.
//!exc: MPI::Exception - MPI library exception.   
//      
void MasterDataManager::operator()( const MPI::Intracomm& comm, 
   const UINT4 num_nodes ) 
{   
   mReceivedSlaves.erase( mReceivedSlaves.begin(), 
                          mReceivedSlaves.end() ); 
   
   MPI::Request request;

   REAL8 delta_time( 0.0 );
   
   // These variable will store the value received from searchMaster
   // of the parallel job.
   REAL4 fraction_remaining( 0.0f );
   BOOLEAN not_finished( true );
   bool received_searchMaster( false );
   MPI::Status status;

   if( enableLdasTrace )
   {
      traced_ranks.erase( traced_ranks.begin(), traced_ranks.end() );
   }
   
   // Send process info to the resultAPI
   sendProcessInfo( comm );
   
   // Initialize node timing info
   initTimeoutHash();
   
   
   // Master polls for any incoming messages until it 
   // hears from searchMaster.   
   while( !received_searchMaster )
   {
      // In a case of LB, slave nodes of LB communicator will report 
      // result data only once( they are blocked at LB before they 
      // can do another applySearch ).
      // In a case of non LB, wrapperMaster receives results from
      // slaves( the same node can report results multiple number
      // of times ) until it hears from searchMaster.
      if( comm.Iprobe( MPI::ANY_SOURCE, NODE_INFO_TAG, status ) )
      {
         // Receive node information and result data
         delta_time += receiveData( comm, status.Get_source() );
   
         if( enableLdasTrace )
         {
            traced_ranks.push_back( mLBNode.rank );
         }

         // If this was the seachMaster, copy fraction_remaining,
         // reset the flags   
         if( mLBNode.rank == mSearchMaster )
         {
            fraction_remaining = mLBNode.fractionRemaining;
            not_finished = mLBNode.notFinished;
   
#ifdef MASTER_DISP_INFO    
            cout << "Master's fractionRemaining = " 
                 << fraction_remaining << endl << flush;
#endif   


            if( numDummyState || numDummyMdd )
            {   
               if( mLBNode.deltaT > 0.0f )
               {
                  // Negative time will be reset to zero
                  received_searchMaster = true;      
               }
            }
            else
            {
               received_searchMaster = true;   
            }
         }

         // Reset 'warning message' timeout for the slave
         updateTimeoutHash( comm, mLBNode.rank );
      }
   
      // To avoid tight CPU usage
      usleep( 10000 );

      checkTimeout( comm );
   
      // Check for error messages
      node_error.pass( comm );
   } 
   
   
   // wrapperMaster has heard from searchMaster, need to hear
   // from all slaves before doing a load balancing
   if( loadBalance &&
       mReceivedSlaves.size() != num_nodes )   
   {
      // LB: all nodes in LB communicator must sent the
      // data before doing LB   
      INT4 status_source( 0 );

      // This loop will guarantee that all slave nodes report to 
      // the wrapperMaster before actual load balancing is done(
      // blocking ).
      while( mReceivedSlaves.size() != num_nodes )
      {
         if( comm.Iprobe( MPI::ANY_SOURCE, NODE_INFO_TAG, status ) )
         {
            status_source = status.Get_source();
  
            // searchMaster is already in the mReceivedSlaves
            if( !inRanks( mReceivedSlaves, status_source ) )
            {
               // Receive node information and result data
               delta_time += receiveData( comm, status_source );
   
               if( enableLdasTrace )
               {
                  traced_ranks.push_back( mLBNode.rank );   
               }
   
               // Reset 'warning message' timeout for the slave
               updateTimeoutHash( comm, mLBNode.rank );   
            }
         }
   
         // To avoid tight CPU usage
         usleep( 10000 );
   
         checkTimeout( comm );   
   
         // Check for error messages
         node_error.pass( comm );   
      }
   }

   
   // Check for error messages
   node_error.pass( comm );   
   
   
   if( enableLdasTrace )
   {
      cout << "Master received data:";   
      copy( traced_ranks.begin(), traced_ranks.end(), 
            ostream_iterator< UINT4 >( cout, " " ) );       
      cout << endl;
   }

   
   mLBNode.deltaT = delta_time/mReceivedSlaves.size();

   mLBNode.fractionRemaining = fraction_remaining;   
   mLBNode.notFinished = not_finished;
   mSearchOutput.notFinished = not_finished;

   
   gettimeofday( &currTime, 0 );   
   mLastTimeSeconds = currTime.tv_sec;

   if( enableTimeoutTrace )
   {
      cout << "Exiting data poll at " << mLastTimeSeconds << endl;
   }

   mDLError( "masterDataManager", 0, 0, comm, true );
   return;
}

   
//------------------------------------------------------------------------------
//
//: Destructor.
//   
MasterDataManager::~MasterDataManager()
{
   delete mResultILwd;
   mResultILwd = 0;
   
   mTimeout.clear();
   mTimeoutCounter.clear();
}

   
//------------------------------------------------------------------------------
//
//: Sets mConnectNum static data member.
//
// This method is used to set a number of connection attempts to the resultAPI.
//
//!param: const UINT4 num - Number of attempts.
//
//!return: Nothing.
//   
void MasterDataManager::setConnectNum( const UINT4 num )
{
   mConnectNum = num;
   
   return;
}   

   
//------------------------------------------------------------------------------
//
//: Sets mConnectDelay static data member.
//
// This method is used to set a delay between connection attempts to the 
// resultAPI.
//
//!param: const UINT4 num - Number of attempts.
//
//!return: Nothing.
//   
void MasterDataManager::setConnectDelay( const UINT4 num )
{
   mConnectDelay = num;
   
   return;
}   

   
//------------------------------------------------------------------------------
//
//: Sets mOutputDelay static data member.
//
// This method is used to set a delay between connections to the 
// resultAPI to send output ILWD format data.
//
//!param: const UINT4  - Number of attempts.
//
//!return: Nothing.
//   
void MasterDataManager::setOutputDelay( const UINT4 num )
{
   mOutputDelay = num;
   
   return;
}   
   
   
//------------------------------------------------------------------------------
//
//: Finalize routine.
//
// This method is used to receive finalizing messages from all slaves in the
// COMM_WORLD.
// This functionality is needed to provide proper job completion in case if
// master didn't receive all the data slaves were sending to it in the main 
// "while" loop of the job.   
//
//!param: const MPI::Intracomm& comm - A reference to the load balancing MPI 
//+       intracommunicator.       
//!param: CHAR* const msg - A finalizing information for master node. The 
//+       same buffer is used to receive slave's finalizing messages.   
//
//!return: Nothing.
//   
void MasterDataManager::finalizeMessage( const MPI::Intracomm& comm, 
   CHAR* const msg  )
{
   // Master counts itself:
   INT4 recv_nodes( 1 );
   string finalize_msg( msg );
   
   REAL8 tmp_real;
   
   MPI::Status status;
   INT4 status_tag( 0 ),
        status_source( 0 );   
   
   // Set up 'warning message' timeout
   initTimeoutHash( true );

   //cout << "Master collecting FINALIZE_TAGs" << endl;
   
   while( recv_nodes < totalNodes )
   {
      // Check for incoming messages in LB communicator
      if( comm.Iprobe( MPI::ANY_SOURCE, MPI::ANY_TAG, status ) )
      {
         status_tag = status.Get_tag();
         status_source = status.Get_source();
   
         if( status_tag == NODE_INFO_TAG )
         {
            tmp_real = receiveData( comm, status_source );
   
            // Reset 'warning message' timeout for the slave
            updateTimeoutHash( comm, status_source );   
         }
         else if( status_tag == NODE_FINALIZE_TAG )
         {
            comm.Recv( msg, mMsgSize, LDAS_MPI_CHAR, 
                       status_source, NODE_FINALIZE_TAG );
            ++recv_nodes;
            finalize_msg += ' ';
            finalize_msg += msg;

            // Reset 'warning message' timeout for the slave
            updateTimeoutHash( comm, status_source );   
         }
      }
      
      node_error.pass( comm );
   

      // Check for incoming messages in COMM_WORLD
      if( MPI::COMM_WORLD.Iprobe( MPI::ANY_SOURCE, NODE_FINALIZE_TAG,
                                  status ) )
      {
         status_tag = status.Get_tag();
         status_source = status.Get_source();
   
         MPI::COMM_WORLD.Recv( msg, mMsgSize, LDAS_MPI_CHAR, 
                               status_source, NODE_FINALIZE_TAG );
         ++recv_nodes;
         finalize_msg += ' ';
         finalize_msg += msg;
   
         // Reset 'warning message' timeout for the slave
         updateTimeoutHash( MPI::COMM_WORLD, status_source );
      }
      
      // To avoid tight CPU usage
      usleep( 10000 );
   
      checkTimeout( comm );      
   
      node_error.pass( MPI::COMM_WORLD );   
   }
   
   
   deleteMPIDatatypes();
   
   const bool p2p( true );
   sendFinalInfo( comm, p2p );
   
   
   ostringstream s;
   s << jobID << '.' << LdasRequest::getRequestNum() 
     << LdasRequest::mFinalize << " {";
   totalWrapperTime( s );

   s << " sentObjects=" << mFileCounter << "; ";
   
   if( enableTimeInfo ) 
   {
      s << finalize_msg;
   }
   
   s << " }";

   string tmp( s.str() );
   
   if( enableMpiApi )
   {
      MsgSocket msgSocket( mpiAPI );
      msgSocket.writeMessage( tmp );
   }
   else
   if( enableLdasTrace )
   {   
      cout << "Master's message( mpiAPI's disabled ): " << tmp << endl;
   }   
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Master node probes for any pending data from the slaves.
//   
//!param: MPI::Intracomm& comm - A reference to the MPI intracommunicator      +
//!param:     within which data is sent.
//!param: const UINT4 num_nodes - A number of nodes to probe for the data.
//
//!return: REAL8 - Time spent in applySearch() function by slave nodes that    +
//!return:    sent data to the master node.   
//  
REAL8 MasterDataManager::pollData( const MPI::Intracomm& comm,
   const UINT4 num_nodes )
{
   REAL8 delta_time( 0.0 );
   MPI::Status status;   
   INT4 status_source( 0 );

   for( UINT4 i = 0; i < num_nodes; ++i )
   {
      if( comm.Iprobe( MPI::ANY_SOURCE, NODE_INFO_TAG, status ) )
      {
         status_source = status.Get_source();
         if( status_source != mSearchMaster )
         {
            // Receive node information and result data
            delta_time += receiveData( comm, status_source );
    
            if( enableLdasTrace )
            {
               traced_ranks.push_back( mLBNode.rank );   
            }
   
            // Reset 'warning message' timeout for the slave
            updateTimeoutHash( comm, status_source );      
         }
      }
   }   
   
   return delta_time;
}

   
//------------------------------------------------------------------------------
//
//: Receives hostname information from all slaves.
//
// This method is used to send initializing message to the mpiAPI. Master node
// receives host name from each slave in the COMM_WORLD and appends this host
// information to the message.   
//
//!param: CHAR* const host - Host name of the node.   
//
//!return: Nothing.
//   
void MasterDataManager::communicateHostName( CHAR* const host )
{
   ostringstream s;
   s << jobID << '.' << LdasRequest::getCurrentRequestNum() 
     << LdasRequest::mInitialize 
     << " { Master-Rank" << masterNode << "(" << host << ") "; 

   bool no_host_name( true );      
   MPI::Request request;
   
   // Initialize node timing info for all slaves
   initTimeoutHash( true );

   
   for( INT4 i = 1; i < totalNodes; ++i )
   {
      no_host_name = true;
   
      while( no_host_name )
      {
         if( MPI::COMM_WORLD.Iprobe( i, NODE_HOSTNAME_TAG ) )
         {
            request = MPI::COMM_WORLD.Irecv( host, 
                                             MPI::MAX_PROCESSOR_NAME,
                                             LDAS_MPI_CHAR, i, 
                                             NODE_HOSTNAME_TAG );
            node_error.pass( request );

            s << "Rank"<< i << "(" << host << ") ";
   
            updateTimeoutHash( MPI::COMM_WORLD, i );   
   
            no_host_name = false;
         }
  
         // To avoid tight CPU usage
         usleep( 10000 );   
   
         checkTimeout( MPI::COMM_WORLD );   
         node_error.pass();
      }
   }
   
   // Reset timeout info to their defaults for use by 'applySearch' loop
   mTimeout.assign( totalNodes, -1 );
   mTimeoutCounter.assign( totalNodes, 0 );

   s << '}';   
   string msg( s.str() );
   
   if( enableMpiApi )
   {
      MsgSocket msgSocket( mpiAPI );
      msgSocket.writeMessage( msg );
   }
   else
   if( enableLdasTrace )
   {         
      cout << "Master's message( mpiAPI's disabled ): " << msg << endl;
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Sends final process information to the resultAPI.
//
// This method sends final ILWD format process information to the result API 
// which is specified as a command line argument to the wrapperAPI.
//
//!param: const MPI::Intracomm& comm - A reference to the intracommunicator.
//+       Default is MPI::COMM_WORLD.
//!param: const bool p2p - Flag to indicate if it's point-to-point communication.   
//+       Default is false.   
//   
//!return: Nothing.
//   
void MasterDataManager::sendFinalInfo( const MPI::Intracomm& comm, 
   const bool p2p )
try
{
   const bool init_info( false );
   return sendProcessInfo( comm, init_info );
}
catch( const bad_alloc& exc ) 
{
   node_error( exc, comm, p2p );
}
catch( const LdasException& exc )
{ 
   node_error( exc, comm, p2p );
}         
   
   
//------------------------------------------------------------------------------
//
//: Receives data from slave.
//
// This method is used to receive node information and result outPut data if any
// from one slave node in LB communicator with a rank value specified by    
// <b>source_rank</b>.   
//
//!param: const MPI::Intracomm& comm - A reference to the communicator within   
//+       which communication takes place.   
//!param: INT4 source_rank - Rank of the slave in <b>comm</b> communicator      
//+       sending the data.
//   
//!return: REAL8 - Time spent in applySearch() function by slave node.
//   
REAL8 MasterDataManager::receiveData( const MPI::Intracomm& comm,
   const INT4 source_rank )
{
   MPI::Request request;
   commOutputTime -= MPI::Wtime();      
   
   // Receive node information from the slave
   request = comm.Irecv( &( mLBNode.numResults ), 1, mLBNode.nodeType,
                         source_rank, NODE_INFO_TAG );   
   
   node_error.pass( request, comm );      
   commOutputTime += MPI::Wtime();   
   
   
   // If LB then need to keep the track of which nodes have
   // sent the data so far, 
   // if not LB then just update the number of received slaves
   if( !loadBalance ||
       !inRanks( mReceivedSlaves, mLBNode.rank ) )
   {
      if( numDummyState || numDummyMdd )
      {
         // Only in these debugging cases time will be negative.
         if( mLBNode.deltaT == -1.0f )
         {
            mLBNode.deltaT = 0.0f;
         }
         else
         {
            mReceivedSlaves.push_back( mLBNode.rank );   
         }
      }
      else
      {
         mReceivedSlaves.push_back( mLBNode.rank );
      }
   }   
   
   
   // Update master messages with new information
   if( strlen( mLBNode.error ) )
   {
      if( error_msg.empty() == false )
      {
         error_msg += " ";
      }
   
      error_msg += mLBNode.error;
   }

   
   if( strlen( mLBNode.warning ) )
   {
      if( warning_msg.empty() == false )
      {
         warning_msg += " ";
      }

      warning_msg += mLBNode.warning;
   }
   
   
   // Reset node messages
   mLBNode.resetMessages();
   

   // There is outPut data
   if( mLBNode.numResults )   
   {
      if( LdasDebug )
      {
         // Debugging information
         cout << "Constructing OUTPUT( master ) for: "
              << mLBNode.rank << " with outPut["  
              << mLBNode.numResults << "]...";    
      }
    
   
      // Build datatype for receiving the data from that slave
      OutputType temp_output( &( mSearchOutput.result ), 
                              mLBNode.numResults, comm,
                              mLBNode.rank );
   
      node_error.pass( comm );


      if( LdasDebug )
      {
         // Debugging information
         cout << "done." << endl;
   
         cout << "Receiving outPut from " << mLBNode.rank << "...";
      }


      // Receive result data
      if( communicateOutput )
      {
         commOutputTime -= MPI::Wtime();   

   
         request = comm.Irecv( 
                      static_cast< void* >( temp_output.getBottom() ), 
                      1, temp_output, mLBNode.rank, NODE_DATA_TAG );
      }
      else
      {
         if( OutputType::getInitState() == false )
         {
            setOutputType( temp_output );
            OutputType::setInitState( true );
         }

   
         commOutputTime -= MPI::Wtime();   
   
   
         request = comm.Irecv( 
                      &( mSearchOutput.result[ 0 ].indexNumber ), 
                      1, getOutputType(), mLBNode.rank, NODE_DATA_TAG );    
      }

   
      node_error.pass( request, comm );  
               
      commOutputTime += MPI::Wtime();   

   
      if( LdasDebug )
      {
         cout << "done" << endl;
   
         cout << "Inserting received outPut into ILWD container for " 
              << mLBNode.rank << "...";
      }

   
      const LdasElement* elem( 0 );

      // Set metadata attribute only once
      if( !mOutputILwdMetaSet )
      {
         ostringstream meta;
         meta << mAnalyzedSeconds;
         mResultILwd->setMetadata( mOutputILwdMeta, meta.str() );
         mOutputILwdMetaSet = true;
      }
   
      // Convert outPut[] to the ILWD format element and add it 
      // to the result container.
      for( UINT4 pos = 0; pos < mLBNode.numResults; ++pos )
      {
         try
         {
            if( communicateOutput )
            {
               elem = temp_output.toILwd( pos );
            }
            else
            {
               elem = OutputType::resultToILwd( 
                         mSearchOutput.result[ pos ], comm );
            }
   
 
            if( elem == 0 )
            {
               ostringstream s;
               s << "NULL ILWD container for " << pos;
   
               node_error( s.str().c_str(), comm );
            }
            else
            {
               mResultILwd->push_back( const_cast< LdasElement* >( elem ),
				       ILwd::LdasContainer::NO_ALLOCATE,
				       ILwd::LdasContainer::OWN );
            }
         }
         catch( const bad_alloc& exc )
         {
            node_error( exc, comm );
         }
      } // convert to ILWD format and add to the result container


      if( LdasDebug )
      {
         cout << "done" << endl;
      }
   

      if( communicateOutput )
      {          
         temp_output.Free();
      }
   
   
      // At this point should send one slave's 
      // data to the resultAPI
      if( canSendData() )
      {
         sendData( comm );   
      }
   }
   

   return mLBNode.deltaT;
}

   
//------------------------------------------------------------------------------
//
//: Sends data to the resultAPI.
//
// This method is to send ILWD format result data to the result API which is 
// specified as a command line argument to the wrapperAPI.
//
//!param: MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent.
//   
//!return: Nothing.
//         
//!exc: bad_alloc - Error allocating memory.
//   
//!exc: bind_failure - Unable to bind a socket to the specified address and     
//+     port.
//!exc: invalid_address - The address is not a valid IP address.
//!exc: invalid_host - The host to which the address refers can not be found.   
//!exc: unexpected_exception - Unexpected exception occured.   
//!exc: connect_failure - The socket was unable to connect to the designated   
//+     address.      
//!exc: invalid_socket - Socket is invalid. 
//!exc: invalid_element - ILWD element is invalid.
//!exc: unconnected_socket - Socket is not connected.   
//      
void MasterDataManager::sendData( const MPI::Intracomm& comm )
{
   if( enableResultApi )
   {
      if( LdasDebug )
      {
         cout << "Sending result ILWD element to the resultAPI...";
      }
   
   
      // Send result outPut data to the resultAPI
      sendILwdElement( mResultILwd, comm );        
   }
   else
   {
      // Create file name for outPut data
      ostringstream s;
      s << mFileName << mFileCounter << mFileExtension;
   
      string file_name( s.str() );
   
      writeILwdElement( mResultILwd, file_name, comm );
   }   

   
   // After sending the data, erase all the elements of the 
   // container
   mResultILwd->erase( mResultILwd->begin(), mResultILwd->end() );   
   
  
   if( LdasDebug )
   {
      cout << "done." << endl;
   }
   
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Sends process information to the resultAPI.
//
// This method is to send ILWD format process information to the result API 
// which is specified as a command line argument to the wrapperAPI.
//
//!param: MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent.
//!param: const bool init - Flag to indicate that initial process information
//+       is being sent. Default is true.   
//   
//!return: Nothing.
//         
//!exc: bad_alloc - Error allocating memory.
//   
//!exc: bind_failure - Unable to bind a socket to the specified address and     
//+     port.
//!exc: invalid_address - The address is not a valid IP address.
//!exc: invalid_host - The host to which the address refers can not be found.   
//!exc: unexpected_exception - Unexpected exception occured.   
//!exc: connect_failure - The socket was unable to connect to the designated   
//+     address.      
//!exc: invalid_socket - Socket is invalid. 
//!exc: invalid_element - ILWD element is invalid.
//!exc: unconnected_socket - Socket is not connected.   
//!exc: Null ILWD element for writing to the file. - Null ILWD element is 
//+     specified for writing to the file.   
//      
void MasterDataManager::sendProcessInfo( const MPI::Intracomm& comm,
   const bool init )
{
   // Process information gets written only once per job
   if( mFileCounter && init )
   {
      return;
   }
   

   LdasContainer* c( initArguments.getDBDataCopy( comm ) );
   c->setNameString( "ligo:ldas:file" );
   
   // This is final process information;
   // set number of ILWD objects master's sent to the resultAPI for
   // the whole job.
   if( !init )
   {
      // If there's any buffered output data, send it now 
      if( mResultILwd->size() )
      {
         // Will increment mFileCounter ==> important to call before
         // setting mProcessILwdMeta
         sendData( comm );
      }
   
      ostringstream s;
      s << ( mFileCounter + 1 );
   
      c->setMetadata( mProcessILwdMeta, s.str() );
   }
   
   try
   {
      if( enableResultApi )
      {
         // Send process information to the resultAPI
         sendILwdElement( c, comm );         
      }
      else
      {
         // Write mProcessInfo to the file   
         const string elem_name( init ? "process_1.ilwd" : "process_2.ilwd" );
         writeILwdElement( c, elem_name, comm );
      }
   }
   catch(...)
   {
      delete c;
      throw;
   }

   delete c;      
   return;
}   


//------------------------------------------------------------------------------
//
//: Sends ILWD format element to the resultAPI.
//
// This method is to send ILWD format container to the result API 
// which is specified as a command line argument to the wrapperAPI.
//   
//!param: const LdasContainer* e - ILWD format container to send.
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent.   
//
//!exc: Null ILWD element for sending to the resultAPI. - Null ILWD element is 
//+     specified for sending to the resultAPI.
//    
void MasterDataManager::sendILwdElement( ILwd::LdasContainer* const e,
   const MPI::Intracomm& comm )
try   
{
   static const string connect_exception( "110:Connection timed out" );
   
   if( e == 0 )
   {
      node_error( "Null ILWD element for sending to the resultAPI.",
                  comm );
   }
   
   
   if( LdasDebug )
   {
      cout << "Sending ILWD element to the resultAPI...";
   }

   
   mSendILwdTime -= MPI::Wtime();      
   
   os_tcp_socket* proc_socket( createDataSocket() );
   
   UINT4 connect_counter( 0 );
   bool connected( false );

   try
   {
      while( !connected && 
             connect_counter < mConnectNum )
      {
         try
         {
            if( enableLdasTrace )
            {
               cout << "Job #" << jobID << ": master connecting to "
                       "resultAPI( counter = " << connect_counter << " )" << endl;  
            }
   
            connectDataSocket( proc_socket, 
                               resultAPI.first.c_str(), 
                               resultAPI.second );                 
            connected = true;
         }
         catch( ErrConnectFailure& exc )
         {
            if( exc.getResult().find( connect_exception ) != 
                string::npos )
            {
               if( ++connect_counter == mConnectNum )
               {
                  throw;
               }
               
               usleep( mConnectDelay );
            }   
            else
            {
               throw;
            }
         }
      }

      // Set jobID
      e->setJobId( jobID );           
   
      if( enableLdasTrace )
      {
         cout << jobID << ": Master sending " << e << " ILWD to ("
              << resultAPI.first << "," << resultAPI.second << ")... ";
      }
   
      // Need to register ILWD object before using sendElementObject,
      // otherwise will throw exception 'invalid_element'   
      Registry::elementRegistry.registerObject( e );   
      sendElementObject( proc_socket, e );
   }
   catch(...)
   {
      closeDataSocket( proc_socket );      
      throw;
   }
   
   closeDataSocket( proc_socket );   

   mSendILwdTime += MPI::Wtime();         
   
   if( enableLdasTrace )
   {
      cout << " done sending " << e << endl;
   }
   
   Registry::elementRegistry.removeObject( e );      

   // Update the time stamp
   mOutputSentTime = MPI::Wtime();
   
   ++mFileCounter;   
   return;
}
catch( const SwigException& exc )
{
   node_error( exc, comm );
}      


//------------------------------------------------------------------------------
//
//: Writes ILWD format element to the file.
//
// This method is to write ILWD format container to the file with specified 
// name.
//   
//!param: const LdasContainer* e - ILWD format container to write.
//!param: const string& name - A reference to the file name.   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent.   
//       
//!exc: Null ILWD element for writing to the file. - Null ILWD element is 
//+     specified for writing to the file.
//!exc: Error opening output file for name. - Could not open file for 
//+     writing.   
//   
void MasterDataManager::writeILwdElement( ILwd::LdasContainer* const e,
   const std::string& name, const MPI::Intracomm& comm )
{
  // Works fine if use this exception
   if( e == 0 )
   {
      node_error( "Null ILWD element for writing to the file.", comm );
   }
   
   
   // Write result data to the file
   static const Format format( ILwd::USER_FORMAT );
   static const Compression compression( ILwd::USER_COMPRESSION );
   
   mSendILwdTime -= MPI::Wtime();         
   
   // To support Griphyn: don't use job specific directory in
   // standalone mode, write output to the local directory.
   string name_path( dumpDataDir );
   if( inputFile.empty() )
   {
      name_path += name;
   }
   else
   {
      name_path = name;
   }
   
   ofstream out( name_path.c_str() );
   if( !out )
   {
      string msg( "Error opening output file for " );
      msg += name_path;
   
      node_error( msg.c_str(), comm );
   }

   
   if( LdasDebug )
   {
      cout << "Writing ILWD element to the file #" 
           << mFileCounter << "..." << endl;
   }

   
   ILwd::writeHeader( out );

   try
   {
      e->setJobId( jobID );           
      e->write( 0, 4, out, format, compression );   
      out.close();
   }
   catch( const LdasException& exc )
   {
      out.close();      
      node_error( exc, comm );
   }
   catch(...)
   {
      out.close();      
      node_error( "Unexpected exception for writeILwdElement.", comm );
   }
   
   out.close();  
   
   if( chmod( name_path.c_str(), S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH ) == -1 )      
   {
      string msg( "Error changing permissions for: " );
      msg += name_path;
      node_error( msg.c_str(), comm );
   }   
   
   mSendILwdTime += MPI::Wtime();         

   // Update the time stamp
   mOutputSentTime = MPI::Wtime();
   
   ++mFileCounter;
   
   return;
}   

   
//------------------------------------------------------------------------------
//
//: Initializes the timeout unordered_map.
//
// This method sets the start time for all nodes in the passed to it communicator
// to the current time, and to default value of -1 if node doesn't belong to the
// LB communicator.   
//   
//!param: const bool init_all_slaves - Flag to indicate if all slaves in COMM_WORLD
//+       should be initialized. Default is false.   
//
//!return: Nothing.   
//   
void MasterDataManager::initTimeoutHash( const bool init_all_slaves )
{
   gettimeofday( &currTime, 0 );
   INT4 start_sec( currTime.tv_sec );

   INT4 pause( 0 );
   if( mLastTimeSeconds != 0 )
   {
      // This is sequencial data collection, get the time spent outside
      // of data polling
      pause = start_sec - mLastTimeSeconds;
   }
   

   // Skip wrapperMaster
   for( INT4 pos = 1; pos < totalNodes; ++pos )
   {
      // Reset nodes not in LB communicator
      if( mLastTimeSeconds &&
          !init_all_slaves && 
          loadBalance &&
          inRanks( notNodes, pos ) )
      {
         mTimeout[ pos ] = -1;
         mTimeoutCounter[ pos ] = 0;
      }
   
      if( init_all_slaves || inRanks( nodes, pos ) )
      {   
         if( mTimeout[ pos ] == -1 )
         {
            // Node appears in the LB communicator first time
            mTimeout[ pos ] = start_sec;
         }
         else
         {
            // Node returns ---> add time spent outside of data collection to
            // bring the value up to the current time
            mTimeout[ pos ] += pause;
         }
      }
   }

   if( enableTimeoutTrace )
   {
      cout << "Master initTimeout( curr_sec=" << start_sec 
           << " pause=" << pause << " ):";   
      copy( mTimeout.begin(), mTimeout.end(), 
            ostream_iterator< INT4 >( cout, " " ) );       
      cout << endl;
   }
   
   return;
}


//------------------------------------------------------------------------------
//
//: Updates the timeout unordered_map entry.
//
// This method resets the start time for the specified node in 'comm' 
// communicator.
//   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent.        
//!param: const INT4 rank - Rank of the node in <b>comm</b> communicator.
//
//!return: Nothing.      
//   
void MasterDataManager::updateTimeoutHash( const MPI::Intracomm& comm,
   const INT4 rank )
{
   // COMM_WORLD rank of node
   const INT4 cw_rank( OutputType::initRank( comm, rank ) );
   
   gettimeofday( &currTime, 0 );      
   mTimeout[ cw_rank ] = currTime.tv_sec;
   mTimeoutCounter[ cw_rank ] = 0;

   if( enableTimeoutTrace )
   {
      cout << "Master updateTimeout( for rank = " << cw_rank << " ): ";   
      copy( mTimeout.begin(), mTimeout.end(), 
            ostream_iterator< INT4 >( cout, " " ) );       
      cout << endl;
   
      cout << "Master updateTimeout( counter for rank = " << cw_rank << " ): ";   
      copy( mTimeoutCounter.begin(), mTimeoutCounter.end(), 
            ostream_iterator< INT4 >( cout, " " ) );       
      cout << endl;   
   }
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Checks if wrapperAPI master should raise a warning.
//
// This method checks if wrapperMaster timed out waiting for the slaves 
// communications. If so, it should notify mpiAPI of the situation as a 
// warning condition.   
//   
//!param: const MPI::Intracomm& comm - A reference to the MPI intracommunicator       
//+       within which data is sent.     
//
//!return: Nothing.
//   
//!exc: wrapperAPI master hasn't heard from any of the slaves for XX seconds. - 
//+     Master node hasn't heard from any slave for XX seconds.
//!exc: Invalid mpiAPI response for wrapperAPI warning. - mpiAPI can respond only
//+     with KILL or CONTINUE command.   
//!exc: Unknown mpiAPI command. - Unknown command is received from mpiAPI.
//!exc: Inconsistent request ID. - Request ID for wrapperAPI and mpiAPI are     
//+     inconsistent.      
//!exc: bad_alloc - Memory allocation failed.   
//   
void MasterDataManager::checkTimeout( const MPI::Intracomm& comm )
{
   gettimeofday( &currTime, 0 );      

   INT4 curr_counter( 0 ),
        delta_timeout( 0 ),
        slave_time( 0 );
   
   ostringstream s;
   bool add_new_line( false );
   
   for( INT4 pos = 1; pos < totalNodes; ++pos )
   {
      slave_time = mTimeout[ pos ];
   
      if( slave_time != -1 )
      {
         delta_timeout = currTime.tv_sec - slave_time;
         curr_counter = delta_timeout / mWarningTimeout;
   
         if( curr_counter > mTimeoutCounter[ pos ] )
         {
            if( add_new_line )
            {
               s << '\n';
            }
            else
            {
               add_new_line = true;
            }
   
            s << "Node " << pos << " has not communicated for "
              << delta_timeout << " seconds.";
   
            mTimeoutCounter[ pos ] = curr_counter;
         }
      }
   }
         
   const string warning( s.str() );

   if( warning.empty() == false )
   {
      if( enableTimeoutTrace )
      {   
         cout << "Master checkTimeout( curr_time = " << currTime.tv_sec 
              << " ): ";   
         copy( mTimeout.begin(), mTimeout.end(), 
               ostream_iterator< INT4 >( cout, " " ) );    
         cout << endl;
   
         cout << "Master checkTimeout counter( curr_time = " << currTime.tv_sec 
              << " ): ";   
         copy( mTimeoutCounter.begin(), mTimeoutCounter.end(), 
               ostream_iterator< INT4 >( cout, " " ) );       
         cout << endl;
      }
   
   
      LdasRequest request;
      request( warning, comm );
   }   
   
   return;
}


//------------------------------------------------------------------------------
//
//: Output process's total time to the stream.
//   
//!param: std::ostringstream& s - A reference to the output stream.
//
//!return: Nothing.
//   
void MasterDataManager::totalWrapperTime( std::ostringstream& s )
{
   const REAL8 dt( MPI::Wtime() - mStartTime );
   s << " totalTime=" << dt << ' ';
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Checks if output ILWD format data can be sent to the resultAPI.
//   
//!return: const bool - True if data can be sent, false otherwise.
//      
const bool MasterDataManager::canSendData() const
{
   const REAL8 dt( MPI::Wtime() - mOutputSentTime );
      //bool flag( dt >= mOutputDelay );
      //cout << "dt=" << dt << " flag=" << flag << endl;
   
   return( dt >= mOutputDelay );
}
