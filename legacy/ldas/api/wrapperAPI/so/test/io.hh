#ifndef WrapperTestIOHH
#define WrapperTestIOHH

   
/* Local Header Files */
#include "wrapperInterfaceDatatypes.h"
   
   
#ifdef __cplusplus
extern "C"{
#endif   
   
   
/* Declarations for search functions */  
INT4 initSearch( CHAR** initStatus, InitParams* initParams );
INT4 conditionData( CHAR** conditionStatus, inPut* data , SearchParams* searchParams);
INT4 applySearch( CHAR** searchStatus, inPut* input, 
   SearchOutput* output, SearchParams* searchParams );   
INT4 freeOutput( CHAR** freeStatus, SearchOutput* output );
INT4 finalizeSearch( CHAR** finalizeStatus );   
   

#ifdef __cplusplus
}
#endif   
   
   
#endif   
