#ifndef WrapperTestConvertHH
#define WrapperTestConvertHH
   
// wrapperAPI header files
#include <wrapperInterfaceDatatypes.h>   
   
namespace IODso
{
   
class ConvertData   
{
public:   
   
   //: Constructor
   ConvertData( const INT4 num = 0 ) : mNum( num ){}

   //: Overloaded call operator
   void operator()( const inPut* const input, outPut** output );

   static void cleanup( const UINT4 num, outPut* output );

private:

   // Helper methods: allocators
   static void storeMultiDimData( multiDimData* const dest_mdd, 
      const multiDimData& src_mdd );

   static void storeStateData( outPut* const output, 
      const stateVector* const src_state );
   
   static void storeInterval( const domain& d, interval& pd, 
      const interval& ps );
   
   static void storeDataArray( dataPointer* data, const datatype& type,
      const UINT4 num, const dataPointer& source_data );
   
   static void storeDetector( detGeom** pd, const detGeom* ps,
      const UINT4 num );
   
   
   // Helper methods: delete dynamically allocated memory   
   static void deleteMultiDimData( multiDimData* pe );   
   
   static void deleteInterval( const domain& d, interval& pd );   
   
   static void deleteDataArray( dataPointer* data, const datatype& type );   

   
   // Helper methods
   static const UINT4 getNDim( const UINT4 ndim, const UINT4* dim );
   
   //: Number of multiDimData structures
   INT4 mNum;
};

}
   
   
#endif   
   
