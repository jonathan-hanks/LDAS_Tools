#ifndef WrapperTestStateHH
#define WrapperTestStateHH
   
// System Header Files   
#include "general/unordered_map.hh"

// wrapperAPI header files
#include "wrapperInterfaceDatatypes.h"
   
   
namespace IODso
{
   
class StateData
{
public:   
   
   //: Overloaded call operator
   void operator()( const inPut* const input, outPut** output );
   
   static const UINT4 mNumOutput;

private:
   
   class EqString
{
public:
    //
    //: Overloaded operator().
    // 
    //!param: const char* s1 - First string.
    //!param: const char* s2 - Second string.
    //
    //!return: bool - True if strings are equal, false otherwise.
    //
    bool operator()( const char* s1, const char* s2 ) const
    {
       return( strcmp( s1, s2 ) == 0 );
    }
};
   
   typedef General::unordered_map< std::string, datatype > TypeHash;   
   typedef TypeHash::value_type hashValueType;   
   
   
   // Helper methods:
   static const TypeHash initTypeHash();
   static void createState( outPut* const dest );
   static void validateState( const stateVector* const state,
      outPut* const output );
   
   //: Allocators
   static void createMultiDimData( stateVector* const state,
      const datatype& type, const char* const name );
   static void createArrayData( dataPointer* data, const datatype& type );
   static void validateArrayData( const multiDimData* const store,
      const CHAR* const name );   
   
   //: Number of nodes in stateVector linked list
   static const UINT4 mNumStates;
   
   //: Number of elements in data array of each node
   static const UINT4 mNumData;
   
   //: Number of data dimensions
   static const UINT4 mNumDimensions;
   
   //: Hash map of datatypes and corresponding names
   static const TypeHash mTypes;   
   
};

}
   
   
#endif   
   
