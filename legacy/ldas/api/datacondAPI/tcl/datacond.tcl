## ********************************************************
##
## Description: datacond.tcl Version 1.0 
## Provides Tcl functions to access the data conditioning
## API.
##
## Comments:
## When a user command is received by the data conditioning
## API, the following events occur:
## 1. a call to dc::run parses the user command options.
## 2. dc::ingestData is called by dc::run to ingest the
##    input data and register the call chain.
## 3. dc::aliasSubst is called to assign aliases for the
##    ingested channel names (defaults of the form ch0, ch1
##    etc are assigned in the absence of an aliases list).
## 4. the algorithms option is then parsed by dc::parseAlgorithm,
##    and formed into a list of CallChain_ function calls
##    by dc::forgeChain.
## 5. the "links" of the chain are then evaluated, causing
##    causing the actions to be registered for execution
##    in the c++ layer.
## 6. the call chain is executed in a thread.
## 7. the results are written to an ilwd text file, or sent to the
##    ligolw API for conversion to an XML document and written to
##    a file, OR sent through a data socket to the mpi API.
## ;#ol
##
## Setting ::DEBUG to 0xdb (219) will cause database table
## formatted results to be dumped to file.
##
## ********************************************************

;#barecode
set ::RCS_ID_datacondtcl {$Id: datacond.tcl,v 1.331 2006/07/19 17:47:40 mlei Exp $}
set ::RCS_ID_datacondtcl [ string trim $::RCS_ID_datacondtcl "\$" ]

package provide datacond 1.0

package require datacondAPI

namespace eval dc {
   set state(NULL) [ list ]
}

namespace eval datacond {
   set dataqueue [ list ]
}   
;#end


## ******************************************************** 
##
## Name: dc::init
##
## Description:
## Initialization and rationalization function for the
## data-conditioning API.
## Parameters:
##
## Usage:
##
## Comments:
##

proc datacond::init { } {
     ;## set this symbol to 1 to get full ilwd object dumping
     if { ! [ info exists ::DEBUG_DUMP_OBJECTS ] } {
        set ::DEBUG_DUMP_OBJECTS 0
     }

     ;## set this symbol to 1 to get diagnostic messages about
     ;## all CallChain actions
     if { ! [ info exists ::DEBUG_TRACE_CPP ] } {
        set ::DEBUG_TRACE_CPP 0
     }

     ;## set this symbol to 1 to reduce the number of threaded
     ;## calls by about 80%
     if { ! [ info exists ::DEBUG_NO_THREADS ] } {
        set ::DEBUG_NO_THREADS 0
     }

     ;## set this symbol to 1 to dump the table of contents
     ;## of the object which will be sent to the wrapper
     if { ! [ info exists ::DEBUG_SHOW_WRAPPER_DATA_TOC ] } {
        set ::DEBUG_SHOW_WRAPPER_DATA_TOC 0
     }

     ;## set this symbol to 1 to trace the destruction of
     ;## all ilwd objects in the datacond API
     if { ! [ info exists ::DEBUG_TRACE_DESTRUCT ] } {
        set ::DEBUG_TRACE_DESTRUCT 0
     }

     if { ! [ info exists ::FREEMEM_LOOP_DELAY_S ] } {
        set ::FREEMEM_LOOP_DELAY_S 2
     }
     
     if { ! [ info exists ::DC_WRAPPER_COMM_PORT_OFFSET ] } {
        set msg    "::DC_WRAPPER_COMM_PORT_OFFSET not defined\n"
        append msg "please set ::DC_WRAPPER_COMM_PORT_OFFSET in\n"
        append msg "in the LDASapi.rsc file"
        return -code error $msg
     }
     
     set port \
        [ expr { $::BASEPORT + $::DC_WRAPPER_COMM_PORT_OFFSET } ]
     
     set ::datacond(wrappercomm) $port
     openListenSock wrappercomm $port
     bgLoop waitforfreemem dc::waitForFreeMem $::FREEMEM_LOOP_DELAY_S
     bgLoop defunctjobs datacond::defunctJobs 300
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::parseOpts
##
## Description:
## Parse the option list for the user command and massage
## default values for some options.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::parseOpts { jobid } {
     
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     set options [ list ]
     if { [ catch {
     
        set opts [ array names ::$jobid * ]
        
        ;## to help people write macros without having
        ;## to be excessively rigorous.
        set required {
                      -dbquery
                      -dbspectrum
                      -dbntuple
                      -dbqualitychannel
                      -responsefunction
                      -responsefiles
                      -state
                     }
        
        ;## load the databucket from the inputprotocol if
        ;## there is anything there!
        set inputprotocol [ set ::${jobid}(-inputprotocol) ]
        if { [ string length $inputprotocol ] } {
           if { [ llength  $inputprotocol ] == 1 } {
              set inputprotocol [ lindex $inputprotocol 0 ]
           }
           ;## cannot put this in after as err will empty data bucket
           eval lappend ::${jobid}_DATABUCKET $inputprotocol
           addLogEntry "lappend inputprotocol '$inputprotocol' bucket '::${jobid}_DATABUCKET' has [ set ::${jobid}_DATABUCKET ]" purple
           set ::${jobid}(-inputprotocol) [ list ]
        }

        foreach opt $opts {
           set arg [ set ::${jobid}($opt) ]
           ;## sanitizer for badly formed args...
           regsub -all -- {([\"\}])([\)\,\;])} $arg {\1 \2} arg
           lappend options $opt $arg
        }

        
     } err ] } {
        after 5000 emptyDataBucket $jobid
        return -code error "[ myName ]:${jobid}: $err"
     }
     return $options
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::run
##
## Description:
## Main function for the data conditioning API.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::run { jobid } {
     
     ;## uplevel #1 is the operator socket handler
     set cid   [ uplevel #1 set cid   ]
     
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     
     if { [ catch {
        set args [ dc::parseOpts $jobid ]
     } err ] } {
        return -code error "error parsing argument list: '$err'"
     }
     
     ;## initialise the state array
     set ::dc::state($job,cid)       $cid
     set ::dc::state($job,end)       0
     set ::dc::state($job,abort)     0
     set ::dc::state($job,dropdata)  0
     set ::dc::state($job,cc)        [ list ]
     set ::dc::state($job,inputptrs) [ list ]
     set ::dc::state($job,resultptr) [ list ]
     
     set fname {}
     set seqpt {}
     set outputflag 0
     
     set opts {
              -inputprotocol    {}
              -times            {}
              -interferometers  {}
              -dataquery        {}
              -aliases          {}
              -algorithms       {}
              -dbquery          {}
              -dbspectrum       {}
              -dbntuple         {}
              -dbqualitychannel {}
              -setsingledc      {}
              -returnprotocol   {}
              -outputformat    ilwd
              -jobid            {}
              -state            {}
              -responsefunction {}
              -responsefiles    {}
              -dropdata         0
              -datacondtarget   {}
              }
     
     regsub -all {\\} $args {__bS} args
     
     if { [ catch { 
        ;## expandOpts is our getopts-like function
        foreach { opt val } [ expandOpts ] {
           set opt [ string trim $opt - ]
           set $opt $val
           set ::dc::state($job,$opt) $val
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }   
     
     ;## missing arguments must be handled explicitly
     if { ! [ string length $algorithms     ] && \
          ! [ string length $dbquery        ] && \
          ! [ string length $dbspectrum     ] && \
          ! [ string length $dbntuple       ] && \
          ! [ string length $dbqualitychannel ] } {
        return -code error "[ myName ]: no algorithms specified"
     }

     ;## handle the substitution the manager did on the
     ;## input semicolon delimiters and update the state
     ;## array
     regsub -all {\|} $algorithms {;}    algorithms
     regsub -all {\|} $aliases    {;}    aliases
     regsub -all {\,} $aliases    {__cOmMa} aliases
     set ::dc::state($job,algorithms) $algorithms
     set ::dc::state($job,aliases) $aliases
     
     if { [ catch {

        ;## handle datacondtarget of mpi, which is illegal
        set target $::dc::state($job,datacondtarget)
        if { [ regexp -nocase {mpi} $target ] } {
           set msg "\n***'mpi' is no longer an accepted value for the\n"
           append msg "-datacondtarget option.  please rewrite your\n"
           append msg "command scripts to use 'wrapper' instead.***\n"
           return -code error $msg
        }
        
        ;## get the objects to be ingested out of the data bucket
        dc::input $jobid
      
        if { [ llength $::dc::state($job,objptrs) ] } {
           dc::startCallback $jobid
        } else {  
           if	{ [ regexp {8.4} $::tcl_version ] } {
           		trace add variable  ::dc::state($job,objptrs) { write } \
              	[ list dc::startCallback $jobid ]
           } else {
           		trace variable ::dc::state($job,objptrs) w \
              	[ list dc::startCallback $jobid ]
           }
        }

        if { $::dc::state($job,end) == 1 } {
           dc::endCallback $jobid
        } else {
           if	{ [ regexp {8.4} $::tcl_version ] } {
           		trace add variable ::dc::state($job,end) { write } \
                [ list dc::endCallback $jobid ]
           } else {
           		trace variable ::dc::state($job,end) w \
              	[ list dc::endCallback $jobid ]
           }
        }

     ;## error, possibly while executing call chain!
     } err ] } {
        addLogEntry $err 2
        set ::$cid [ list 3 $err error! ]
        reattach $jobid $cid
        ;## there could be all sorts of garbage even
        ;## at a very early stage, since we can get
        ;## data asynchronously.
        dc::cleanup $jobid
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::input
##
## Description:
## Asynchronous input object handler to avoid pitfalls
## of default data bucket handling.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::input { jobid { i 0 } } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        if { ! [ info exists ::dc::state($job,objptrs) ] } {
           set ::dc::state($job,objptrs) [ list ]
        }
        
        ;## memory based throttling
        if { [ info exists ::DATACOND_MEMORY_LOCK ] } {
           set wait $::DATACOND_MEMORY_LOCK
        } else {
           set wait 0
        }
        
        ;## Stuart Anderson asked that we NOT do local
        ;## memory throttling :TODO:
        set wait 0
        
        ;## we are going to do non-busy waiting until data
        ;## comes in.  special user command macros can
        ;## populate the bucket for immediate processing.
        if { $wait == 0 && \
             [ info exists ::${jobid}_DATABUCKET ] && \
             [ llength [ set ::${jobid}_DATABUCKET ] ] } {
           set ptrs [ processDataBucket $jobid ] 
           dc::dataFromFile $jobid $ptrs
           return {}
        }

        ;## nothing showed up yet, so we go away for 1 second.
        if { $i >= $::DATABUCKET_TIMEOUT } {
           set ptrs [ dc::dataFromFile $jobid {} ]
           if { [ llength $ptrs ] == 0 } {
              set msg "timed out reading input data after $i seconds"
              if { $wait == 1 } {
                 set msg "timed out waiting for sufficient free"
                 append msg " memory after $i seconds"
              }
              set ::dc::state($job,err) $msg
              set ::dc::state($job,end) 1
           }
           return {}
        }
     } err ] } {
        ;## we can throw an error on our first pass, but
        ;## after that we bgerror (not too likely!)
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        } else {
           ;## short circuit and avoid another turn through the
           ;## event loop.
           return {}
        }
     }
     ;## and off we go into the event loop
     incr i 3
     after 3000 [ list dc::input $jobid $i ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::waitForFreeMem
##
## Description:
## Implements a throttling heuristic based on available
## free memory.
##
## The resource variable ::MINIMUM_FREE_MEM_K is set to some
## number of kilobytes, when available free memory falls
## below that value, new jobs will not be started, and will
## be delayed for up to ::DATABUCKET_TIMEOUT seconds while
## memory is unavailable.
##
## This throttle is pigybacked on the data receive timeout
## logic.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::waitForFreeMem { args } {
     
     if { [ catch {
        
        if { ! [ info exists ::MINIMUM_FREE_MEM_K ] } {
           set minfree 256000
        } else {
           set minfree $::MINIMUM_FREE_MEM_K
        }
        
        set free [ freeMemOnBox ]
        
        if { $free <= $minfree } {
           set wait 1
           if { [ info exists ::WAITING_FOR_FREE_MEM ] && \
                $::WAITING_FOR_FREE_MEM == 0 } {
              set ::WAITING_FOR_FREE_MEM [ clock seconds ]    
           } elseif { ! [ info exists ::WAITING_FOR_FREE_MEM ] } {
              set ::WAITING_FOR_FREE_MEM [ clock seconds ]
           }
        } else {
           set wait 0
           set ::WAITING_FOR_FREE_MEM 0
        }
        
        if { [ info exists ::DEBUG_MINIMUM_FREE_MEM_K ] && \
             [ string equal 1 $::DEBUG_MINIMUM_FREE_MEM_K ] && \
             $::WAITING_FOR_FREE_MEM == 0 } {
           set msg "flag: $wait free: $free k minfree: $minfree k"
           addLogEntry $msg blue
        }
        
        if { $::WAITING_FOR_FREE_MEM != 0 } {
           set msg "flag: $wait free: $free k minfree: $minfree k"
           addLogEntry $msg orange
        }   
        
        if { ! [ info exists ::DATACOND_MEMORY_LOCK ] || \
             ! [ string equal $wait $::DATACOND_MEMORY_LOCK ] } {
           set ::DATACOND_MEMORY_LOCK $wait
           set sid [ sock::open manager emergency ]
           puts $sid "orange NULL NULL set ::DATACOND_MEMORY_LOCK $wait"
           ::close $sid
        }
        
        if { $::WAITING_FOR_FREE_MEM != 0 } {
           set now [ clock seconds ]
           if { ($now - $::WAITING_FOR_FREE_MEM) > 900 } {
              set subject "LDAS $::LDAS_SYSTEM datacondAPI "
              append subject "INSUFFICIENT FREE MEMORY!"
              set msg "${subject}\n\n"
              append msg "Available memory on the datacond API host\n"
              append msg "machine $::DATACOND_API_HOST has fallen to\n"
              append msg "$free kilobytes.\n\n"
              append msg "The variable ::MINIMUM_FREE_MEM_K in the\n"
              append msg "LDASdatacond.rsc file is currently set to\n"
              append msg "${::MINIMUM_FREE_MEM_K}.\n\n"
              append msg "The LDAS manager API will suspend all user\n"
              append msg "commands which depend upon the datacond API\n"
              append msg "until sufficient free memory is available!"
              addLogEntry "Subject: ${subject}; Body: $msg " email
              set ::WAITING_FOR_FREE_MEM $now
           }
        }
        
     } err ] } {
        catch { ::close $sid }
        set wait 0
        set ::WAITING_FOR_FREE_MEM 0
        addLogEntry $err red
     }
     return $wait
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::dataFromFile
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## Long time coming fix for PR #1413 -
##
## it is now possible to have -framequery {} and use
## -inputprotocol for all input of data.
##

proc dc::dataFromFile { jobid ptrs } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        ;## get specified files assuming ilwd
        set ip [ set ::dc::state($job,inputprotocol) ]
        foreach filename $ip {
           lappend $ptrs [ ilwd::readFile $filename $jobid ]
        }

        ;## we trigger dc::startCallback by populating
        ;## ::dc::state($job,objptrs) ONLY if there are
        ;## any!!
        if { [ string length [ lindex $ptrs 0 ] ] } {
           set ::dc::state($job,objptrs) $ptrs
        } else {
           set ptrs [ list ]
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $ptrs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::startCallback 
##
## Description:
## Asynchronous handler for dc API entry point.
##
## Parameters:
##
## Usage:
##
## Comments:
## We throw an exception if we are called outside of the
## event loop, and set ::dc::state($job,end) if we are
## inside the event loop.
##
## If we are inside the event loop then dc::endCallback
## is also already registered.

proc dc::startCallback { jobid args } {
     
     set traced 0
     if { [ catch {
        
        regexp {\d+} $jobid job

        ;## were we called from a trace, or directly?
        if	{ [ regexp {8.4} $::tcl_version ] } {
        	foreach entry [ trace info variable ::dc::state($job,objptrs) ] {
            	foreach { oplist cmd } $entry { break }
            	trace remove variable ::dc::state($job,objptrs) $oplist $cmd 
                set traced 1
            }
        } else {
        	set traced [ trace vinfo ::dc::state($job,objptrs) ]
        	if { [ string length $traced ] } {
           		trace vdelete \
              	::dc::state($job,objptrs) w "dc::startCallback $jobid" 
           		set traced 1
        	} 
        }
        
        ;## handle -responsfiles option
        dc::ingestResponseFiles $jobid
        
        ;## and ingest other (frame, metadata) input data
        dc::ingestBucketData $jobid

        ;## do alias substitution, defaulting to ch[N] mapping
        dc::aliasSubst $jobid
        ;## parse the algorithms option
        dc::parseAlgorithm $jobid

        ;## and create a series of call chain links
        dc::forgeChain $jobid

        ;## and load the call chain
        dc::loadCallChain $jobid
       
        if { [ info exists ::DEBUG_SYMBOL_TABLE ] && \
           ! [ string equal 0 $::DEBUG_SYMBOL_TABLE ] } {
           set msg [ CallChain_DumpSymbolTable $::dc::state($job,cc) ]
           addLogEntry "Symbol Table: ($msg)" purple
        }
       
        if { $::DEBUG_NO_THREADS } {
           dc::execute_nothreads $jobid
        } else {
           dc::execute $jobid
        }

     } err ] } {
        if { $traced } {
           set ::dc::state($job,err) $err
           set ::dc::state($job,end) 1
        } else {
           return -code error "[ myName ]: $err"
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::expandAlgorithms
##
## Description:
## Based on a proposal to support Greg's SFT handling via
## an automatic alias and algorithm expansion.
##
## This is a null call unless the .datacond macro that
## is used by the command defines ::${jobid}(autoexpand)
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::expandAlgorithms { jobid elements alii } {
     
     ;## the alii are those returned by dc::ingestBucketData
     if { ! [ info exists ::${jobid}(-autoexpand) ] || \
          ! [ string length [ set ::${jobid}(-autoexpand) ] ] || \
          [ set ::${jobid}(-autoexpand) ] == 0 } {  
        return $alii
     }
     
     regexp {\d+} $jobid job
     
     set algorithms [ list ]
     set aliases    [ list ]
     
     if { [ catch {
        set protoalias $::dc::state($job,aliases)
        set protoalgos $::dc::state($job,algorithms)
        
        set chN _chN
        regsub -all {\s+} $protoalias {} protoalias
        set protoalias [ string trim $protoalias " ;" ]
        regexp {([^=]+)=([^\;]+)} $protoalias -> protoalias chN
        
        set chN "($protoalias|$chN)"
        
        set elements [ lsort -dictionary $elements ]
        
        set N 0
        foreach channel $elements {
           foreach protoalgo $protoalgos {
              regsub -all -- $chN $protoalgo \$channel protoalgo
              if { [ catch {
                 lappend algorithms [ subst $protoalgo ]
              } err ] } {
                 set err "$err while substituting -algorithms prototype"
                 return -code error $err
              }
           }
           lappend aliases $protoalias$N=$channel
           incr N
        }
        
        set alii [ concat $alii $aliases ]

        set ::dc::state($job,aliases)    $alii
        set ::dc::state($job,algorithms) $algorithms
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $aliases
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::endCallback
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::endCallback { jobid args } {
     if { [ catch { 
        set files {}
        set tails {}
        set outputflag 0 
        
        regexp {\d+} $jobid job
        
        ;## make sure we don't trigger again.
        ;## this cancels all traces.
        unset ::dc::state($job,end)
        set ::dc::state($job,end) 1
        
        set rp     $::dc::state($job,returnprotocol)
        set cid    $::dc::state($job,cid)
        set of     $::dc::state($job,outputformat)
        set targ   $::dc::state($job,datacondtarget)
        set result $::dc::state($job,resultptr)
        set drop   $::dc::state($job,dropdata)
        
        ;## default subject for gridftp interaction needs
        ;## to be grid_output per PR #2148
        set subj [ set ::${jobid}(-subject) ]
        if { ! [ string length $subj ] } {
           if { [ regexp -nocase {gridftp} $rp ] } {
              set subj grid_output
           } else {
              set subj results
           }
        }

        if { [ info exists ::dc::state($job,err) ] } {
           
           ;## prevent bad list formation from erroring
           regsub -all -- {([\"\}])(\S)} \
              $::dc::state($job,err) {\1 \2} ::dc::state($job,err)
              
           if { [ string length $::dc::state($job,err) ] > 3 } {
              return -code error $::dc::state($job,err)
           }
        }   
        
        set outputflag 1
        set files [ dc::output $jobid ]
        foreach file $files { lappend tails [ file tail $file ] }

        if { $drop } { set targ datacond }
        
        ;## if -datacondtarget is 'datacond' and NOTHING is
        ;## intended to go to the mpi API, we can short circuit
        ;## by sending a return code of 4 to the manager.
        set returncode [ dc::endCallbackDataApi $jobid ]
        if { $returncode == 4 } {
           set ::dc::state($job,dropdata) 1
           set drop 1
        }
        
        if { [ string equal frame $targ ] } {
           ;## I *think* this is the place where dropped files
           ;## were getting lost when frames were also produced
           if { [ string length [ lindex $tails 0 ] ] } {
              set jobdir [ jobDirectory ]
              regsub $::HTTPDIR $jobdir {} jobdir
              set msg    "Your results:\n${tails}\ncan be found at:\n"
              append msg "$::HTTPURL$jobdir"
              set ::$cid [ list 0 $msg 0 ]
           } else {
              set ::$cid [ list 0 0 0 ]
           }
        } elseif { [ regexp -nocase -- {LIGO_LW} $of ] || \
           ! [ string equal datacond $targ ] } {
           if { [ string length [ lindex $tails 0 ] ] } {
              set jobdir [ jobDirectory ]
              regsub $::HTTPDIR $jobdir {} jobdir
              set msg    "Your results:\n${tails}\ncan be found at:\n"
              append msg "$::HTTPURL$jobdir"
              set ::$cid [ list 0 $msg 0 ]
           } else {
              set ::$cid [ list 0 0 0 ]
           }
        } elseif { $drop } {
           set url [ lindex $files 0 ]
           set msg    "Your results:\n${tails}\ncan be found at:\n"
           append msg "[ join [ lrange [ split $url / ] 0 end-1 ] / ]"
           
           ;## for gridftp result reporting
           set url2 [ lindex $files 1 ]
           if { [ string length $url2 ] && \
                [ regexp {gridftp} $url2 ] } { 
              set msg "Your results:\n${url2}\n\n$msg"
           }
           set ::$cid [ list $returncode $msg $subj ]
        } else {
           set jobdir [ jobDirectory ]
           regsub $::HTTPDIR $jobdir {} jobdir
           set msg    "Your results:\n${tails}\ncan be found at:\n"
           append msg "$::HTTPURL$jobdir"
           set lvl $returncode
           if { ! [ regexp -nocase {datacond} $targ ] } {
              set lvl  2
              set subj 0
           }
           set ::$cid [ list $lvl $msg $subj ]
        }
        
        reattach $jobid $cid
        ;## if the -datacondtarget was datacond or frame
        ;## then we do not need to wait around for the
        ;## wrapper to pickup, so just clean up.
        if { ! [ regexp -nocase {wrapper} $targ ] } {
           dc::cleanup $jobid
        }
        
     ;## error, possibly while executing call chain!
     } err ] } {
        dc::endCallbackErr $jobid $rp $files $cid $outputflag $err
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::endCallbackDataApi
##
## Description:
##
## Fix for PR #2226
##
## if -datacondtarget is datacond, and -dataapi is datacond,
## and no output() action targets 'wrapper', then we short
## circuit the mpi API entry.
## Parameters:
##
## Usage:
##
## Comments:
## Returns 1 (abort) if nothing's going to the mpi API.
##

proc dc::endCallbackDataApi { jobid } {
     
     if { [ catch {
        set dataapi  UNKNOWN
        set dctarget UNKNOWN
        set wrapperoutput 0
        set returncode 0
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        if { [ info exists ::dc::state($job,dataapi) ] } {
           set dataapi    $::dc::state($job,dataapi) 
        }
        if { [ info exists ::dc::state($job,datacondtarget) ] } {
           set dctarget   $::dc::state($job,datacondtarget) 
        }
        ;## need some state variable corresponding to
        ;## wrapper output...
        if { [ info exists ::dc::state($job,dataready) ] } {
           set wrapperoutput 1
        }
        
        if { [ string equal datacond $dataapi ]  && \
             [ string equal datacond $dctarget ] && \
             $wrapperoutput == 0 } {
           set returncode 4
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $returncode
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::endCallbackErr
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::endCallbackErr { jobid rp files cid outputflag err } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set primary_msg    "(no primary specified, "
        append primary_msg "or specified primary was not "
        append primary_msg "of time, frequency, or time-frequency "
        append primary_msg "series type)"
        regsub -nocase {no primary specified} $err $primary_msg err
        set err2 {} 
        ;## check for a partial result
        set tmp $::dc::state($job,resultptr)
        if { [ regexp {_p_LdasContainer} $tmp ] && ! $outputflag } {
           set of [ list ilwd ascii ]
           
           ;## generate a sensible file name for the partial
           ;## result.
           set rproot [ file rootname [ file tail $rp ] ]
           if { [ string length $rproot ] } {
              set rproot ${rproot}_partial_result.ilwd
           } else {
              set rproot partial_result.ilwd
           }
           set rp http://$rproot
           catch { set files [ dc::output $jobid ] } err2
        }
        
        ;## but if we succeeded in creating a partial
        ;## output file, let the user know.
        if { [ string length $files ] } {
           set msg "[ myName ]: $err"
           append msg " (partial output returned as: '$files')"
           addLogEntry $msg red
           set ::$cid [ list 3 $msg error! ]
        } else {
           ;## unless dc::output failed.
           addLogEntry "$err $err2" red
           set ::$cid [ list 3 "[ myName ]: $err $err2" error! ]
        }
        reattach $jobid $cid
        after 0 datacond::killJob $jobid
 
     } err ] } {
        addLogEntry "OOPS! Can't get here!! : $err" email
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::ingestData
##
## Description:
## Instantiates a new call chain and ingests the input data
## to be processed.  Returns the name of the call chain and
## the list of data elements instantiated in the c++ layer
## 
## Parameters:
##
## Usage:
##
## Comments:
## Needs mods for parallel call chains from a single job.
##

proc dc::ingestData { jobid data } {
     set elements [ list ]
     set alii     [ list ]
     set qchans   [ list ]
     set seqpt {}
     if { [ catch {

        regexp {\d+} $jobid job
        set cc $::dc::state($job,cc)
        
        if { $::DEBUG_DUMP_OBJECTS } {
           foreach datap $data {
              dc::0xdb $jobid $datap http://${datap}_input_dump.ilwd
           }   
        }
        
        ;## instantiate new Call Chain
        if { ! [ string length $cc ] } {
           set seqpt "new_CallChain():"
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "--> start $seqpt"
           }   
           set cc [ new_CallChain ]
           set ::dc::state($job,cc) $cc
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "<-- end $seqpt"
           }
           
           set setsingle [ set ::dc::state($job,setsingledc) ]
           set seqpt "CallChain_SetSingleFrameMode($cc $setsingle):"
           CallChain_SetSingleFrameMode $cc $setsingle
           
           ;## and if we have a "target", set it so that output
           ;## is formatted properly.
           set targ [ set ::dc::state($job,datacondtarget) ]
           if { [ string length $targ ] } {
              set seqpt "CallChain_SetReturnTarget($cc $targ):"
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "--> start $seqpt"
              }
              ;## and set Return target as early as possible!
              CallChain_SetReturnTarget $cc $targ
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "<-- end $seqpt"
              }
           }
        }
        set seqpt {}
        
        if { [ info exists ::DEBUG_INGESTION_RATE ] && \
             [ string equal 1 $::DEBUG_INGESTION_RATE ] } {
           __t::start ingest$jobid
        }

        foreach datap $data {
           set alias [ list ]
           set elems [ list ]
           set pass_thrus 0

           if { [ catch {
              ;## if we catch here the object is already destructed
              ;## so just get along...
              set seqpt "getContainerSize($datap):"
              set size [ getContainerSize $datap ]
              set seqpt {}
           } err ] } {
              addLogEntry "already destructed object: '$datap'!" red
              continue
           }

          
           ;## defer quality channel creation until all other data
           ;## is ingested.
           set flag 0
           set qcs [ dc::testForQualityChannel $jobid $datap ]
           foreach { name pushpass ilwdp } $qcs {
              if { ! [ string length $ilwdp ] } { continue }
              if { [ string length $name ] } {
                 if { [ lsearch -regexp $qchans ^\\s*$name ] > -1 } {
                    set msg "quality channel name: '$name' duplicated!"
                    return -code error $msg
                 }
                 lappend qchans [ list $name $ilwdp ]
                 set flag 1
                 continue
              }
           }
           if { $flag } { continue }
          
           foreach { type pushpass alias } \
              [ dc::testForDBQuery $jobid $datap ] { break }
           
           ;## if the user got the alias and pushpass options
           ;## in the wrong order, the metadata api will pass
           ;## them in the ilwd comment in the wrong order!
           if { [ regexp {^(push|pass)$} $alias ] } {
              set tmp $alias
              set alias $pushpass
              set pushpass $tmp
           }
           
           if { [ string length $type ] } {
              foreach { alias elems } [ dc::ingestDBData \
                 $jobid $type $pushpass $alias $cc $datap ] { break }
              ;## associate the db query derived alias with the name
              if { [ string length $alias ] \
                && [ string length $elems ] } {
                 lappend alii ${alias}=$elems
              }
              continue
           }
           
           ;## this condition is unreachable!! :TODO:
           ;## what was it for??
           if { [ string equal _pass $alias ] } {
              set seqpt "CallChain_PassThroughILwd($cc $datap):"
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "--> start $seqpt"
              }   
              CallChain_PassThroughILwd $cc $datap
              incr pass_thrus 
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "<-- end $seqpt"
              }
              set alias [ list ]
           } else { 
              set seqpt "CallChain_IngestILwd($cc $datap):"
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "--> start $seqpt"
              }   
              set elems [ CallChain_IngestILwd $cc $datap ]
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "<-- end $seqpt"
              }
           }
   
           if { [ string length $elems ] > 3 } {
              debugPuts \
                 "[ llength $elems ] elements ingested from $datap: $elems"
           } elseif { $pass_thrus } {
              debugPuts \
                 "$pass_thrus elements passing through from $datap"
           } else {
              set elems [ list ]
              addLogEntry "$cc: no data elements found in '$datap'" 2
           }
           
           regsub -all {\\} $elems {__bS} elems
           regsub -all {\,} $elems {__cOmMa} elems
           eval lappend elements $elems
           
           ::dc::destructElement $jobid $datap
        }
        ::dc::qualityChannel $jobid $cc $qchans

     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     
     if { [ info exists ::DEBUG_INGESTION_RATE ] && \
          [ string equal 1 $::DEBUG_INGESTION_RATE ] } {
        __t::end "call chain ingested data in" ingest$jobid
        catch { __t::cancel ingest$jobid }
     }
     return [ list $elements $alii ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::testForDBQuery 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##


proc dc::testForDBQuery { jobid ilwdp } {
     
     if { [ catch {
        ;## the alias will be used when pushpass is "push"
        set dbop     [ list ]
        set pushpass [ list ]
        set alias    [ list ]
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        if { [ catch {
           set seqpt "getElementMetadata($ilwdp dboperation):"
           set dbop [ getElementMetadata $ilwdp dboperation ]
           if { [ string equal qualitychannel $dbop ] } {
              set dbop [ list ]
           }
        } err ] } {
           ;## oops, it's not a dbquery
        }
        
        if { [ catch {
           set seqpt "getElementMetadata($ilwdp pushpass):"
           set pushpass [ getElementMetadata $ilwdp pushpass ]
        } err ] } {
           ;## oops, it must be a pass
           set pushpass pass
        }

        if { [ catch {
           set seqpt "getElementMetadata($ilwdp alias):"
           set alias [ getElementMetadata $ilwdp alias ]
        } err ] } {
           ;## oops, it must be a pass
        }
        
        ;## if the user got the alias and pushpass options
        ;## in the wrong order, the metadata api will pass
        ;## them in the ilwd comment in the wrong order!
        if { [ regexp {^(push|pass)$} $alias ] } {
           set tmp $alias
           set alias $pushpass
           set pushpass $tmp
        }

     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return [ list $dbop $pushpass $alias ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::ingestDBData
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::ingestDBData { jobid type pushpass alias cc ilwdp } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set seqpt {}
        set methods \
           [ list IngestDBSpectrum IngestDBQuery IngestNTuples ]
        
        ;## pushpass defaults to pass
        if { ! [ string length $pushpass ] } {
           set pushpass pass
        }
        
        ;## pushing without an alias is nonsense
        if { ! [ string length $alias ] } {
           if { [ string equal push $pushpass ] } {
              return -code error "'push' specified, but no alias given!"
           }
        }
        
        ;## passing with an alias is nonsense
        if { [ string equal pass $pushpass ] } {
           ;## Ed says, "not at all!"
           #set alias [ list ]
        }

        regexp -nocase -- {db(\S+)} $type -> type
        
        foreach method $methods {
           if { [ regexp -nocase -- $type $method ] } { 
              break
           }
           set method none
        }
        
        if { [ string equal none $method ] } {
           return -code error "Cannot ingest database derived '$type'"
        }
        
        set datap [ dc::registerObject $jobid $ilwdp ]
        
        set seqpt "CallChain_${method}($cc $datap $alias $pushpass):"
        
        if { $::DEBUG_TRACE_CPP } {
           debugPuts "--> start $seqpt"
        }
        set elems [ CallChain_$method $cc $datap $alias $pushpass ] 
        if { $::DEBUG_TRACE_CPP } {
           debugPuts "<-- end $seqpt"
        }

        ::dc::destructElement $jobid $datap
        
        addLogEntry "${pushpass}ed objects ingested from $datap"
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return [ list $alias $elems ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::testForQualityChannel 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::testForQualityChannel { jobid ilwdp } {
     
     if { [ catch {
        set seqpt {}
        set dbop     [ list ]
        set pushpass [ list ]
        set alias    [ list ]
        set datap    [ list ]
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        set seqpt "getContainerSize($ilwdp):"
        set size [ getContainerSize $ilwdp ]
        
        if { [ catch {
           set seqpt "getElementMetadata($ilwdp dboperation):"
           set dbop [ getElementMetadata $ilwdp dboperation ]
           if { ! [ string equal qualitychannel $dbop ] } {
              return {}
           }
        } err ] } {
           ;## oops, it's not a dbquery
           return {}
        }
        
        if { [ catch {
           set seqpt "getElementMetadata($ilwdp pushpass):"
           set pushpass [ getElementMetadata $ilwdp pushpass ]
        } err ] } {
           ;## oops, it must be a pass
           set pushpass pass
        }

        if { [ catch {
           set seqpt "getElementMetadata($ilwdp alias):"
           set alias [ getElementMetadata $ilwdp alias ]
        } err ] } {
           ;## oops, it must be a pass
        }

        set seqpt {}
        
        ;## if the user got the alias and pushpass options
        ;## in the wrong order, the metadata api will pass
        ;## them in the ilwd comment in the wrong order!
        if { [ regexp {^(push|pass)$} $alias ] } {
           set tmp $alias
           set alias $pushpass
           set pushpass $tmp
        }
        
        ;## pushing without an alias is nonsense
        if { ! [ string length $alias ] } {
           if { [ string equal push $pushpass ] } {
              return -code error "'push' specified, but no alias given!"
           } elseif { [ string equal pass $pushpass ] } {
              set alias quality_channel
           } else {
              return -code error "invalid push/pass: '$pushpass'"
           }
        }
        
        set datap [ dc::registerObject $jobid $ilwdp ]
        
     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]:$seqpt $err"
        }
     }
     return [ list $alias $pushpass $datap ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::parseAlgorithm
##
## Description:
## Parse and tokenise a user request -algorithms option.
##
## Parameters:
##
## Usage:
##
##       given:
##    set algo {x=foo(1,2); y=foo.apply(out, in); z=x+y; x=y \
##             output(z,1,2,3,4)}
##
##     running:
##    ::dc::parseAlgorithm $algo
##
##     returns:
##    {foo (1,2)x} {foo.apply (out,in)y} {add (x,y)z} {y x} \
##    {output (z,1,2,3,4)}
##
## Comments:
##

proc dc::parseAlgorithm { jobid } {
     set atoms  [ list ]
     set atom   [ list ]
     set retval [ list ]
     set tmp    [ list ]
     regexp {\d+} $jobid job
     ;## sanitize input
     if { [ catch {
        set algorithms [ set ::dc::state($job,algorithms) ]
        set atoms [ dc::optPreProcess $jobid $algorithms ]
        set atoms [ split $atoms ";" ]
        ;## get methods and arguments
        foreach atom $atoms {
           if { ! [ string length $atom ] } { continue } 
           if { [ regexp {^\s*output\(} $atom ] } {
           } else {
              if { [ regexp {[\=]+} $atom ] } {
                 set atom [ dc::mathops $atom ]
                 set atom [ split $atom ";" ]
              }
           }   
           eval lappend tmp $atom
        }
        set atoms $tmp
        set tmp [ list ]
   
        foreach atom $atoms {
           if { ! [ string length $atom ] } { continue }
           set method    [ list ]
           set arguments [ list ]
           
           ;## leading and trailing whitespace should go
           string trim $atom
           
           ;## break methods and arguments with a space
           regsub -all {(\S)(\(\S+)} $atom {\1 \2} atom
           
           set method    [ string trim [ lindex $atom 0 ] ]
           set arguments [ lrange $atom 1 end ]
           
           ;## remove spaces from parenthesized arguments
           regsub -all {\s+} $arguments {} arguments
   
           ;## the return value will be a list of lists
           lappend retval [ list $method $arguments ]
        }
        set ::dc::state($job,atoms) $retval
     } err ] } {
        set err "[ myName ]: $err while handling atom: '$atom'"
        return -code error $err
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::mathops
##
## Description:
## Renders infix notation atomic math operations (x+y, or
## z=x+y) into a format which can be expanded into call
## chain sequences.
##
## Parameters:
##
## Usage:
##
## Comments:
## It is not possible to execute a complex math operation,
## i.e. z=a+b+c in a single action.  If this is wanted, it
## would have to be rendered in the -algorithms option as
## z=a+b; z=z+c.

proc dc::mathops { atom } {
     if { [ catch {   
        regsub -all {\s+} $atom {} atom
        regexp {([^\s]+)(=)([^\s]+)} $atom -> lhs op rhs
        switch -exact -- $op {
           + {
             set method add
             set atom ${method}($lhs,$rhs)
             return -code error "infix math op '+' not supported"
             }
           - {
             set method sub
             set atom ${method}($lhs,$rhs)
             return -code error "infix math op '-' not supported"
             }
           * {
             set method mul
             set atom ${method}($lhs,$rhs)
             return -code error "infix math op '*' not supported"
             }
           / {
             set method div
             set atom ${method}($lhs,$rhs)
             return -code error "infix math op '/' not supported"
             }
          ** {
             set method pow
             set atom ${method}($lhs,$rhs)
             return -code error "infix math op '**' not supported"
             }
           ^ {
             set method pow
             set atom ${method}($lhs,$rhs)
             return -code error "infix math op '^' not supported"
             }
           = {
             set atom [ list $rhs $lhs ]
             }
           default {}
        }
        
        ;## handle simple assignment based on simple math op
        ;## i.e. z=x+y becomes add(x,y)z, which is expanded
        ;## to {CallChain_AppendCallFunction _XXX_Chain_p add {x y} z}
        if { [ regexp {(.+)=(.+)} $lhs -> ret arg1 ] } {
           set atom ${method}($arg1,$rhs)$ret
        }   
     } err ] } {
        return -code error "[ myName ]:$err"
     }   
     return $atom
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::aliasSubst
##
## Description:
## Substitute aliases defined within the user command
##
## Parameters:
## 
## Usage:
##
## Comments:
## Default aliasing action is for channels in incoming data
## to be aliased according to their order in the incoming
## container to _ch0, _ch1, _ch2, ..., _chN.
##
## When this proc fails it is assumed that a syntactical
## anomaly has been detected and consequently a verbose
## report of the state of the variables at the time of
## failure is returned.

proc dc::aliasSubst { jobid } {
     set parms      [ list ]
     set aliases    [ list ]
     set algorithms [ list ]
     set symb       [ list ]
     set subst      [ list ]
     set act        [ list ]
     set seqpt "-->(state_dump: aliases: '\$aliases'
                             algorithms: '\$algorithms'
                                   symb: '\$symb' 
                                  subst: '\$subst'
                                 action: '\$act'
                                 params: '\$parms')"
     regsub -all {[\n\t\s]+} $seqpt { } seqpt
     
     if { [ catch {
        regexp {\d+} $jobid job
        set aliases    [ set ::dc::state($job,aliases) ]
        set elements   [ set ::dc::state($job,elements) ]
        set algorithms [ set ::dc::state($job,algorithms) ]
        ;## attempt to split the user supplied aliases
        set aliases [ split [ dc::optPreProcess $jobid $aliases ] ";" ]
        
        ;## these two lines are solely for debugging
        ;## sub backslashes and semicolons
        regsub -all {__bS} $aliases {\\} dbg
        regsub -all {__cOmMa} $dbg {,} dbg
        regsub -all {__sPaCe} $dbg { } dbg
        regsub -all {__sC} $dbg {;} dbg
        debugPuts $dbg
        
        foreach alias $aliases {
           ;## make sure user is not creating forbidden alias names
           ;## with a leading underscore.
           if { ! [ string length $alias ] } { continue }
           regexp {(.+)=(.+)} $alias -> symb subst
           if { [ regexp {^_} $symb ] } {
              return -code error "user supplied aliases may not begin with '_'"
           }
        }   
        
        set i -1
        while { [ incr i ] < [ llength $elements ] } {
           lappend aliases _ch$i=[ lindex $elements $i ]
        }
        
        ;## preprocess the algorithms list
        set algorithms \
           [ split [ dc::optPreProcess $jobid $algorithms ] ";" ]

        ;## and perform substitutions
        foreach alias $aliases {
           if { ! [ string length $alias ] } { continue }
           set symb  [ list ]
           set subst [ list ]
           set tmp   [ list ]
           regexp {(.+)=(.+)} $alias -> symb subst ]

           set subst [ dc::aliasMatchPattern $jobid $subst $elements ]
           
           ;## make certain that aliases do not map to the lhs
           ;## of an assignment.
           foreach algorithm $algorithms {
              if { ! [ string length $algorithm ] } { continue }
              set rhs [ list ]
              set lhs [ list ]
              
              set rhs $algorithm
              
              if { ! [ regexp output $algorithm ] } {
                 regexp {(.+)=(.+)} $algorithm -> lhs rhs
              }   
              
              ;## test for matches on lhs.  we wouldn't substitute
              ;## them anyway, but we are being s-t-r-i-c-t
              if { [ regexp ^${symb}\$ $lhs ] } {
                 return -code error \
               "[ myName ]: lhs aliases forbidden('$symb' matched '$lhs')"
              }

              ;## parse the right hand side into action and params
              set act   {}
              set parms [ list ]
              regexp {([^\(]+)\((.+)\)$} $rhs -> act parms

              ;## SUBSTITUTE
              set done 0
              ;## output actions do not get all their args substituted
              if { [ regexp output $act ] } {
                 lappend tmp \
                    [ dc::outputParmSub $jobid $subst $symb $rhs ]
                 set done 1
              ;## all other actions get their parms fully substituted
              } elseif { ! $done } {
                 lappend tmp \
                    [ dc::actionParmSub $jobid $parms $act $subst $symb $lhs $rhs ]
              } else {
                 lappend tmp $algorithm
              }
           } ;## end of foreach on algorithms
           ;## set algorithms to substituted list   
           set algorithms $tmp
        } ;## end of foreach on aliases/chnames
        
        set ::dc::state($job,algorithms) [ join $algorithms ";" ]
     
     } err ] } {
        return -code error "[ myName ]:[ subst -nocommands $seqpt ] $err"
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::aliasMatchPattern
##
## Description:
## User may supply a right hand argument to an alias
## assignment that is a regexp pattern to be matched
## to the ingested objects.
##
## If the pattern matches EXACTLY one ingested element
## name, the argument will be substituted with the full
## element name.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::aliasMatchPattern { jobid alias elements } {
     
     set matches [ list ]
     if { [ catch {
        
        if { [ regexp {^_ch\d+$} $alias ] } {
           set elements [ list ]
        }
        
        foreach element $elements {
           regsub -all {[\)\(]} $alias {\\&} alias_rx
           if { [ regexp -nocase -- $alias_rx $element ] } {
              lappend matches $element
           }
        }
        
        set msg    "alias: '$alias' matched [ llength $matches ] "
        append msg "ingested elements: ($matches)"
        
        if { [ llength $matches ] >  1 } {
           addLogEntry $msg red
           append msg ". alias must match uniquely!"
           return -code error $msg
        }
        if { [ llength $matches ] == 1 } {
           if { [ info exists ::DEBUG_ALIAS_PATTERN_MATCH ] } {
              if { [ string equal 1 $::DEBUG_ALIAS_PATTERN_MATCH ] } {
                 addLogEntry $msg green
              }
           }   
           set retval [ lindex $matches 0 ]
        }
        if { [ llength $matches ] == 0 } {
           if { ! [ regexp {^_ch\d+$} $alias ] } {
              if { [ info exists ::DEBUG_ALIAS_PATTERN_MATCH ] } {
                 if { [ string equal 1 $::DEBUG_ALIAS_PATTERN_MATCH ] } {
                    addLogEntry $msg blue
                 }
              }   
           }
           set retval $alias
        }

     } err ] } {
        return -code error "[ myName ]: $err alias: ($alias)"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::actionParmSub
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::actionParmSub { jobid parms action subst symbol lhs rhs } {
     
     if { [ catch {
        set retval [ list ]
        set parms [ split $parms "," ]
        set tmpparms [ list ]
        foreach parm $parms {
           set parm [ string trim $parm ]
           if { ! [ string length $parm ] } { set parm _null }
           regsub {^\s*_\s*$} $parm _null parm
           regsub ^${symbol}\$ $parm $subst parm
           lappend tmpparms $parm
        }
        set parms [ join $tmpparms "," ]
        set rhs ${action}($parms)
        if { [ string length $lhs ] } {
           set retval $lhs=$rhs
        } else {
           set retval $rhs
        }   
 
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::outputParmSub
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::outputParmSub { jobid subst symb rhs } {
     
     if { [ catch {
        
        foreach [ list symbol format protocol name comment ] \
                [ dc::outputAction $jobid $rhs ] { break }
                 
        ;## substitute aliases in the symbol parameter
        regsub ^${symb}\$ $symbol $subst symbol
                 
        if { [ regexp {^([^\(]+)\((.+)\)$} $comment -> a p ] } {
           set p [ string trim $p ]
           regsub -all -- " $symb " $p " $subst " ps
           regsub -all -- _${symb}_ $p _${subst}_ ps
           set comment ${a}($ps) 
        }
        set parms [ list $symbol $format $protocol $name $comment ]
        set parms [ join $parms "," ]
        set retval output($parms)
 
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::outputAction
##
## Description:
## Cause the variable on the top of the stack to be copied
## to the result stack.
## The ilwd object will be named according to the provided
## name parameter, and a comment will be included as provided
## in the comment parameter.
##
## Parameters:
##
## Usage:
## The algorithm action "output" has the syntax:
## output($symbol,$format,$protocol,$name,$comment)
##
## Comments:
## 

proc dc::outputAction { jobid action } {
     set params   {}
     set symbol   {}
     set format   {}
     set protocol {}
     set name     {}
     set comment  {}
     if { [ catch {   
        
        ;## pattern matches option list of output action
        set parm_rx {([^,]*),([^,]*),([^,]*),([^,]*),(.*)}
       
        ;## strip braces inside actions
        regsub -all {\\[\}\{]} $action {} action
        regsub -all {[\}\{]}   $action {} action
       
        regexp {output\s*\((.+)\)$} $action -> params
        
        if { ! [ string length $params ] } {
           set msg "no parameters received, possible missing ')'?"
           return -code error $msg
        }   
        
        if { ! [
           regexp $parm_rx $params -> symbol format protocol name comment 
        ] } {
           set msg "error parsing action parameters: '$action'"
           return -code error $msg
        }
        
        set symbol   [ string trim $symbol   ]
        set format   [ string trim $format   ]
        set protocol [ string trim $protocol ]
        set name     [ string trim $name     ]
        set comment  [ string trim $comment  ]
        
        ;## we now reject mpi as an output protocol
        if { [ regexp -nocase {mpi} $protocol ] } {
           set msg "\n***'mpi' is no longer an accepted output protocol\n"
           append msg "for the datacond API.  please rewrite your\n"
           append msg "scripts to use 'wrapper' instead.***\n"
           return -code error $msg
        }
        
        ;## do not allow spaces in user specified names
        regsub -all {\s+} $name {_} name
        
        ;## FORMAT MANGLING --
        ;## get the user supplied outputformat
        set of [ set ::${jobid}(-outputformat) ]
        ;## if the output action has '_' for the format
        ;## use the -outputformat value
        if { [ string equal _ $format ] } { set format $of }
        ;## strip out 'ilwd '
        regsub -all -nocase {ilwd\s*} $format {} format
        ;## trim off possible protected braces and spaces
        set format [ string trim $format " \}\{" ]
        ;## isolate ascii or binary
        regexp -nocase {(ascii|binary)} $format -> format
        ;## default is binary
        if { ! [ string length $format ] } {
           set format binary
        }
        
        if { ! [ regexp {(ascii|binary|LIGO_LW|frame)} $format ] } {
           set msg    "requested format '$format' is invalid.\n"
           append msg "valid formats include ilwd (ascii or binary)\n"
           append msg ", LIGO_LW (xml), or frame."
           return -code error $msg
        }
        
        if { [ regexp {\s+} $protocol ] } {
           set msg    "embedded space in protocol: '$protocol'.\n"
           append msg "spaces in output protocols are prohibited."
           return -code error $msg
        }
        
        regsub -all {[\t\n\s]+} $comment {__sPaCe} comment
        
     } err ] } {
        return -code error "[ myName ]:$err (action: '$action')"
     }
     
     if { ! [ string length $symbol   ] } { set symbol   _null }
     if { ! [ string length $format   ] } { set format   _null }
     if { ! [ string length $name     ] } { set name     _null }
     if { ! [ string length $protocol ] } { set protocol _null }
     if {   [ regexp {^_$}  $symbol   ] } { set symbol   _null }
     if {   [ regexp {^_$}  $format   ] } { set format   _null }
     if {   [ regexp {^_$}  $name     ] } { set name     _null }
     if {   [ regexp {^_$}  $protocol ] } { set protocol _null }

     return [ list $symbol $format $protocol $name $comment ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::handleOutputs
##
## Description:
## Handle each element in an output container
## according to it's format and protocol.
## This may involve conversion of ilwd to LIGO_LW or
## even to frame data (shudder!).  In it's simplest
## case, and the only one we support NOW, individual
## ilwd elements can be written to separate files, sent
## out individually by ftp, or sent to databuckets in
## other API's.
##
## The bare result pointer is returned to the dc::output
## proc for handling.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::handleOutputs { jobid ptr target } {
     set seqpt {}
     set urls [ list ]
     set delete [ list ]
     regexp {\d+} $jobid job

     set rp     $::dc::state($job,returnprotocol)
     set dctarg $::dc::state($job,datacondtarget)

     
     if { [ catch {
        ;## api's which might be targets for output objects
        set api_rx {(wrapper|frame|ligolw|metadata)}
        set seqpt "getContainerSize($ptr):"
        set size [ getContainerSize $ptr ]
        set seqpt {}
        set outputs [ set ::dc::state($job,outputs) ]
        set i 0
        set j 0

        if { $::DEBUG_DUMP_OBJECTS } {
           dc::0xdb $jobid $ptr http://${ptr}_full_output_dump.ilwd
        }
        
        while { $j < $size } {
           set im [ lindex $outputs $j ]
           set protocol [ list ]
           set name     [ list ]
           set comment  [ list ]
           foreach { symbol format protocol name comment } $im { break }
              
           set protocol [ string trim [ join $protocol ] ]
           
           ;## if protocol was NOT provided in the output, then
           ;## set protocol to -returnprotocol if we are dumping
           ;## to disk.
           if { [ regexp {^(|_|_null)$} $protocol ] } {
              if { [ string equal datacond $dctarg  ] && \
                 ! [ regexp -nocase {frame} $format ] } {
                 set protocol $rp
              } else {
                 set protocol $target
              }
           }
           
           ;## reference the ith the element
           set seqpt "refContainerElement($ptr $i):"
           set elementp [ refContainerElement $ptr $i ]
           
           ;## check for empty ilwd element with name '__ignore__'
           set seqpt "getElementAttribute($elementp name):"
           set test [ getElementAttribute $elementp name ]
           set seqp {}
           if { [ string equal __ignore__ $test ] } {
              set seqpt "deleteContainerElement($ptr $i):"
              deleteContainerElement $ptr $i
              incr j
              set seqpt {}
              continue
           }
           
           ;## for conditionData when output() action does not
           ;## have another API as the protocol argument
           if { [ string equal datacond $target ] && \
              ! [ regexp $api_rx $protocol ] } {
              set protocol \
                 [ dc::dcTargetProtocol $jobid $test $protocol ] 
           }
           
           if { ! [ string equal $target $protocol ] || \
                  [ string equal wrapper $protocol ] } {
              set contp [ ilwd::addElement $elementp ]
              ;## clip it off the result
              set seqpt "deleteContainerElement($ptr $i):"
              deleteContainerElement $ptr $i
              set seqpt {}
              incr i -1
              ;## attach the jobid to the outgoing container
              ilwd::setjob $contp $jobid
              ;## do something with it...
              ;## send it to another api if the protocol says so
              if { [ regexp $api_rx $protocol ] } {
                 if { $::DEBUG_DUMP_OBJECTS } {
                    dc::0xdb $jobid $contp \
                       http://${contp}_part_output_dump.ilwd
                 }
                       
                 ;## we don't push data to the wrapper, we wait for it
                 ;## to come and get it.
                 if { ! [ regexp -nocase {wrapper} $protocol ] } {
                     
                    ;## remove the object pointer from the destructor
                    ;## list!
                    set i [ lsearch $::dc::state($job,objptrs) $contp ]
                    set ::dc::state($job,objptrs) \
                    [ lreplace $::dc::state($job,objptrs) $i $i ]
                    
                    dataSendThread $jobid $protocol $contp
                 } else {
                    ;## all wrapper targeted results go together
                    set seqpt "refContainerElement($contp 0):"
                    set elementp [ refContainerElement $contp 0 ]
                    set seqpt {}
                    if { ! [ info exists wrapperp ] } {
                       set wrapperp [ ilwd::addElement $elementp ]
                    } else {
                       ilwd::addElement $elementp $wrapperp
                    }
                    ::dc::destructElement $jobid $contp
                 }
              } else { 
                 lappend files($protocol,$format) $contp
              }
           }
           incr i
           incr j 
        }
        
        ;## pack up outputs aimed at a common file in a single
        ;## ilwd container.
        if { [ array exists files ] } {
           lappend urls \
              [ dc::packFileOutput $jobid [ array get files ] ]
           set urls [ join $urls ]
        }
        
        ;## handle wrapper results correctly!
        if { [ info exists wrapperp ] } {
           
           set seqpt "getContainerSize($ptr):"
           if { [ getContainerSize $ptr ] } {
              set seqpt "refContainerElement($ptr 0):"
              set elementp [ refContainerElement $ptr 0 ]
              set seqpt {}
              ilwd::addElement $elementp $wrapperp
           }
           
           set seqpt {}
           lappend ::dc::state($job,objptrs) $ptr
           set ptr $wrapperp
           set ::dc::state($job,resultptr) $ptr
        }
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return [ list $ptr $urls ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::packFileOutput
##
## Description:
## Pack up outputs for common files into single ilwds
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::packFileOutput { jobid elements } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set seqpt {}
        set urls [ list ]
        array set output $elements
        foreach name [ array names output * ] {
           foreach [ list protocol format ] [ split $name , ] { break }
           set ptrs $output($name)
           if { [ llength $ptrs ] > 1 } {
              set contp [ lindex $ptrs 0 ]
              foreach ptr [ lrange $ptrs 1 end ] {
                 set seqpt "mergeContainer($contp $ptr):"
                 mergeContainer $contp $ptr
                 set seqpt "destructElement($ptr):"
                 ::destructElement $ptr
              }
           } else {
              set contp $ptrs
           }
           
           set dctarg $::dc::state($job,datacondtarget)
           
           if { [ string equal datacond $dctarg ] } {
              ;## attach pass-thrus to the result
              if { [ info exists ::dc::state($job,passfiles) ] } {
                 foreach file $::dc::state($job,passfiles) {
                    set ilwdp [ ilwd::readFile $file ]
                    ilwd::addElement $ilwdp $contp
                    ::dc::destructElement $jobid $ilwdp
                 }
                 unset ilwdp
                 unset ::dc::state($job,passfiles)
              }
           }
           
           lappend urls \
              [ dc::ilwdOut $jobid $protocol $contp $format ]
           ::dc::destructElement $jobid $contp   
        }

     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $urls
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::dcTargetProtocol
##
## Description:
## Determine a usable URI for an object whose target id the
## datacond API.
##
## Parameters:
## name - the name attribute from the object being handled
## protocol - the protocol from arg 3 of the output() action
##
## Usage:
##
## Comments:
## Returned protocol must be a URI

proc dc::dcTargetProtocol { jobid name protocol } {
     
     if { [ catch {
        
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        set dir [ jobDirectory ]
        regsub $::HTTPDIR $dir {} dir
        
        ;## peter shawhan likes to test with things like
        ;## http: and ftp:// and file:
        set name [ string trim $name ":/\\" ]
        set protocol [ string trim $protocol ":/\\" ]
        if { [ string equal -nocase file $protocol ] } {
           set protocol file:/result
        }
        
        ;## protocol is just set to datacond
        if { [ string equal datacond $protocol ] } {
           set name [ file rootname [ file tail $name ] ]
           if { ! [ string length $name ] } {
              set name result
           }
           set protocol $::HTTPURL$dir/${name}.ilwd
        ;## protocol is an http or ftp url
        ;## or the bare string http or ftp
        } elseif { [ regexp {^(http|gridftp|ftp)} $protocol -> type ] } {
           set type [ string toupper $type ]
           set protocol [ file rootname [ file tail $protocol ] ]
           if { ! [ string length $protocol ] } {
              if { [ string length $name ] } {
                 set protocol [ file rootname [ file tail $name ] ]
              } else {
                 set protocol result
              }
           }
           if { [ regexp {^(http|gridftp|ftp)$} $protocol ] } {
              set protocol result
           }
           set protocol [ set ::${type}URL ]$dir/${protocol}.ilwd
        ;## protocol is a file url
        } elseif { [ regexp {^file\:} $protocol ] } {
           set protocol [ file rootname [ file tail $protocol ] ]
           set protocol $::HTTPDIR$dir/${protocol}.ilwd
        ;## protocol is a filename ending in .ilwd
        } elseif { [ string equal .ilwd [ file extension $protocol ] ] } {
           set protocol [ file tail $protocol ]
           set protocol $::HTTPURL$dir/$protocol
        ;## protocol is a bare word
        } else {
          if { [ string length $name ] } {
             set name [ file rootname [ file tail $name ] ]
          } elseif { [ string length $protocol ] } {
             set name [ file rootname [ file tail $protocol ] ]
          } else {
             set name result
          }
          set protocol $::HTTPURL$dir/${name}.ilwd
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $protocol
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::optPreProcess
##
## Description:
## Convenience function for parsing complex option strings
##
## Parameters:
##
## Usage:
##
## Comments:
## This was really designed for the -algorithms option and
## should not be used on other options without careful
## consideration.

proc dc::optPreProcess { jobid options } {
     set retval [ list ]
     
     if { [ catch {        
        ;## remove newlines and tabs
        regsub -all {[\s\n\t]+} $options { } options
        ;## break up on semicolons, discarding possible trailing
        ;## semicolon
        set options [ string trim $options ";" ]
        
        ;## PR #1283: comments with embedded ";" will cause
        ;## strange _null output objects.
        if { [ regexp {output\(} $options ] } {
           set options [ dc::preProcessAlgorithms $options ]
        } else {
           set options [ split $options ";" ]
        }
        
        ;## force consistent tight formatting: action(x,y,z)
        foreach option $options {
           set option [ string trim $option " \"" ]
           if { ! [ string length $option ] } { continue }
           set lhs [ list ]
           set rhs [ list ]
           set act [ list ]
           set ini [ list ]
           
           ;## output handler
           if { [ regexp {output\s*\((.+)\)$} $option ] } {
              foreach [ list symb form prot name comm ] \
                 [ dc::outputAction $jobid $option ] { break }
              set ini [ list $symb $form $prot $name $comm ]
              set ini [ join $ini "," ]
              lappend retval output($ini)
              continue
           }   
           
           ;## handle all null argument lists for things
           ;## that are not intermediates
           regsub {\(\)$} $option (_null) option
           
           ;## if option is of the form y=x or y=f(x)
           if { [ regexp {(.+)=(.+)} $option -> lhs rhs ] } {
              set lhs [ string trim $lhs " \"" ]
              set rhs [ string trim $rhs " \"" ]
              ;## if rhs is of the form f(x) (note the regexp anchors)
              if { [ regexp {^([^\(]+)\((.+)\)$} $rhs -> act ini ] } {
                 set act [ string trim $act " \"" ]
                 set ini [ string trim $ini " \"" ]
                 set rhs ${act}($ini)
              }     
              lappend retval $lhs=$rhs
              continue
           }
           
           ;## if option is of the form f(x) (note the anchors
           ;## for the regexp)
           if { [ regexp {^([^\(]+)\((.+)\)$} $option -> act ini ] } {
              set act [ string trim $act " \"" ]
              set ini [ string trim $ini " \"" ]
              regsub -all {\s+} $ini {} ini
              lappend retval ${act}($ini)
              continue
           }
           lappend retval $option
        } ;## end of foreach on options
     
     } err ] } {
        return -code error "[ myName ]:$err"
     }
     return  [ join $retval ";" ]   
}          
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::preProcessAlgorithms
##
## Description:
## Algorithms option parser that satisifies
## requirements of PR #1283
##
## Can parse outputs with anything in the name/comment
## fields!
##
## Parameters:
##
## Usage:
##
## Test case:
##
## set opts \
## "ad(1,x);sub(x,1); ad(,);output(,,ad(1,x);sub(x,1); ad(,);,);ad(1)"
##
## yields:
##
## ad(1,x) sub(x,1) ad(,) {output(,,ad(1,x)__sCsub(x,1)__sC ad(,)__sC,)} ad(1)
##
## Comments:
##

proc dc::preProcessAlgorithms { options } {
     
     if { [ catch {
        
        if { [ regsub -all {\)} $options {} x ] != \
             [ regsub -all {\(} $options {} x ] } {
           set err "unbalanced parentheses in -algorithms option"
           return -code error $err
        }
        
        set i 0
        set option  [ list ]
        set Options [ list ]
        
        ;## break the whole thing into individual chars
        set options [ split $options {} ]

        ;## iterate over all the chars, only cast an option
        ;## when parens balance.
        foreach char $options {
           switch -exact -- $char {
                  ";" {
                       if { $i } {
                          append option __sC
                       }
                      }
                  " " {
                       if { $i } {
                          append option $char
                       }
                      }
                  "(" {
                       incr i 1
                       append option $char
                      }
                  ")" {
                       incr i -1
                       append option $char
                       if { $i == 0 } {
                          lappend Options $option
                          set option [ list ]
                       }
                      }
              default {
                       append option $char
                      }
           }
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $Options
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::forgeChain
##
## Description:
## Render the algorithm atoms into call chain sequences.
##
## Parameters:
##
## Usage:
##       running dc::forgeChain on the output sample from
##       dc::parseAlgorithm yields:
##         {CallChain_AppendCallFunction $cc foo {1 2} x}          \
##         {CallChain_AppendCallFunction $cc foo.apply {out in} y} \
##         {CallChain_AppendCallFunction $cc add {x y} z}          \
##         {CallChain_AppendCallFunction $cc value x y}            \
##         {CallChain_AppendIntermediateResult $cc 3 4}
##
## Comments:
##

proc dc::forgeChain { jobid } {
     set args [ list ]
     set primary [ list ]
     set retval {}
     set i 0
     regexp {\d+} $jobid job
     set atoms [ set ::dc::state($job,atoms) ]
     set ::dc::state($job,outputs) [ list ]
     
     if { [ catch {
        
        foreach atom $atoms {
           set method {}
           set argret {}
           
           foreach {method argret} $atom {
              if { [ string equal $method output ] } {
                 
                 set atom [ dc::outputAction $jobid $atom ]
                 set symbol   {}
                 set format   {}
                 set protocol {}
                 set name     {}
                 set comment  {}
                 
                 foreach [ list symbol format protocol name comment ] \
                    $atom { break }
                 
                 foreach [ list format mode level ] \
                    [ dc::parseFrameFormatCompression $format ] \
                    { break }
                 
                 if { [ regexp {(.*)\:primary$} $name -> name ] } {
                    set primary 1
                 } else {
                    set primary 0
                 }
                 
                 if { ! [ string length $name ] } { set name _none }
                 if { ! [ string length $comment ] } { set comment none }
                 
                 set link "CallChain_AppendOutputResult \$cc $symbol $name \{$comment\} $protocol $primary $mode $level"
                 lappend ::dc::state($job,outputs) $atom
              } else {
                 regexp {(\((.+)\))?(.+)?} $argret -> -> args retval
                 
                 set args [ split $args "," ]
              
                 ;## handler for a simple assignment like y=x
                 if { ! [ string length $args ] } {
                    set args $retval
                    set retval $method
                    set method value
                 }   
              
                 ;## test args for constant declarations
                 ;## dc::constantArgs will add links in an
                 ;## uplevel context if constant declarations
                 ;## were made
                 set args [ dc::constantDeclarations $args ]
                 
                 ;## default return value if none is provided
                 if { ! [ string length $retval ] } {
                    set retval "{}"
                 }   
                 
                 regsub -all {__bS} $args {\\\\} args
                 regsub -all {__cOmMa} $args {,} args
                 regsub -all {__sPaCe} $args { } args
                 set link "CallChain_AppendCallFunction \$cc $method [ list $args ] $retval"
              }
              lappend chain $link
           }  
        } ;## end of foreach atom
        
        ;## special call chain actions
        set targ       [ set ::dc::state($job,datacondtarget) ]
        set algorithms [ set ::dc::state($job,algorithms) ]
        
        regsub -all {\;(\s+)?} $algorithms {; } algos
        lappend chain "CallChain_SetActionList \$cc \{$algos\}"
        
        if { [ regexp -nocase {wrapper} $targ ] } {
           lappend chain "CallChain_EnsurePrimary \$cc"
        }
     } err ] } {
        return -code error "[ myName ]:$err (atoms: '$atoms')"
     }
     set ::dc::state($job,chain) $chain
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::parseFrameFormatCompression 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::parseFrameFormatCompression { format } {
     
     if { [ catch {
        set mode  gzip
        set level 1
        set format [ split $format : ]
        if { [ llength $format ] > 1 } {
           set mode [ lindex $format 1 ]
        }
        if { [ llength $format ] > 2 } {
           set level [ lindex $format 2 ]
        }
        set format [ lindex $format 0 ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $format $mode $level ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::constantDeclarations
##
## Description:
## Create links in an uplevel context to initialise constant
## declarations.
##
## Parameters:
##
## Usage:
##
## Comments:
## The regex for floats is pretty thoroughgoing! It says:
## The string I match consists of one or more digits,
## possibly preceded by + or - and/or
## zero or more digits followed by a ".";
## optionally followed by the letter "[eE]", itself followed
## by an integer with a possible sign.

proc dc::constantDeclarations { arguments } {
     set args [ list ]
     if { [ catch {
        foreach a $arguments {
           if { ! [ string length $a ] } { continue }
           set class {}
           switch -regexp -- $a {
            {^([\+\-]+)?[0-9]+$} { ;## integers
                                 set class integer
                                 }
    {^([\+\-])?([0-9]*\.)?[0-9]*([eE]([\+\-])?[0-9]+)?$} { ;## floats
                                 set class double
                                 }
                         default {                           
                                 }
           } ;## end of switch
           if { [ string length $class ] } {
              set i  [ uplevel set i  ]
              set link "CallChain_AppendCallFunction \$cc $class $a _tmp_$i"
              uplevel lappend chain [ list $link ]
              lappend args _tmp_$i
              uplevel incr i
           } else {
              lappend args $a
           }   
        } ;## end of foreach on arguments
     } err ] } {
        return -code error "[ myName ]: $err"
     }   
     return $args
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::responsefilesHandler
##
## Description:
## Needs to be the first thing that a job does to make sure
## the aliases get substituted promptly!
##
## Parameters:
## args - the -responsefiles argument
## Usage:
##
## Comments:
##

proc dc::responsefilesHandler { jobid } {
     
     if { [ catch {
        set ptrs    [ list ]
        set aliases [ list ]
        set data    [ list ]
        regexp {\d+} $jobid job
        set data [ dc::responsefilesFormat $jobid ]
        foreach element $data {
           ;## passes are filenames, pushes are ilwdp's
           ;## and aliases.
           switch -exact -- [ llength $element ] {
                    1 { 
                      lappend ::dc::state($job,passfiles) $element
                      }
                    2 {
                      lappend ptrs    [ lindex $element 0 ]
                      lappend aliases [ lindex $element 1 ]
                      }
              default {
                      return -code error "internal error: '$data'"
                      }
           }
        }
     } err ] } {
        ;## don't leak!
        set ptrs [ concat $data $ptrs  ]
        set ptrs [ lsort -unique $ptrs ]
        ::dc::destructElement $jobid $ptrs -nocomplain
        return -code error "[ myName ]: $err"
     }
     return [ list $ptrs $aliases ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::responsefilesFormat
##
## Description:
## Prepare the input objects and aliases or just pass along
## the filenames for response files as appropriate.
## The object files are designed so that when they are read
## the "name" attribute can be accessed and used to complete
## the alias assignment.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::responsefilesFormat { jobid } {
     
     if { [ catch {
        set retval [ list ]
        set seqpt {}
        set parsedlist [ dc::responsefilesParser $jobid ]
        foreach item $parsedlist {
           foreach { file action alias } $item { break }
           
           ;## if were going to push, add pointer and alias
           ;## to the return value, otherwise just add the
           ;## "filename".
           if { [ string equal push $action ] } {
              set ilwdp [ ilwd::readFile $file ]
              #set seqpt "getElementAttribute($ilwdp name):"
              #set name [ getElementAttribute $ilwdp name ]
              #set alias "${alias}=$name"
              lappend retval [ list $ilwdp $alias ]
           } else {
              lappend retval $file
           }
           
        }
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::responsfilesParser
##
## Description:
## Parses the -responsefiles option as proposed by Kent
## Blackburn on 04-26-2001
##
## Parameters:
##
## Usage:
##
## Comments:
## Does nothing but parse, so far.

proc dc::responsefilesParser { jobid } {
     
     if { [ catch {
        set retval [ list ]
        regexp {\d+} $jobid job
        set files [ set ::dc::state($job,responsefiles) ]
        foreach file $files {
           set filename [ list ]
           set action   [ list ]
           set alias    [ list ]
           foreach { filename action alias } \
              [ split $file "," ] { break }
           
           set type [ fileType $filename ]
           if { [ lsearch $type ilwd ] == -1 } {
              set err "responsefile '$file' is not ilwd "
              append err "(looks like $type)!"
              return -code error $err
           }
           
           ;## action is "pass" or "push"   
           if { ! [ string length $action ] } {
              set action pass
           } elseif { ! [ regexp -nocase -- {(pass|push)} $action ] } {
              return -code error "invalid action '$action' in $file"
           }
           
           ;## if action was push, an alias is required
           if { ! [ string length $alias  ] } {
              if { [ string equal $action push ] } {
                 set msg    "All push actions in the -responsefiles "
                 append msg "option must have an alias defined in "
                 append msg "the third place: file,push,alias. "
                 append msg "The file $filename has no alias specified."
                 return -code error $msg
              } else {
                 set alias _null
              }
           }
           ;## now we are parsed
           lappend retval [ list $filename $action $alias ]
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::ingestResponseFiles
##
## Description:
## Note that the responsefiles must contain exactly one
## ilwd container.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::ingestResponseFiles { jobid } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        foreach { ptrs alii } \
           [ dc::responsefilesHandler $jobid ] { break }
        
        if { [ llength $ptrs ] } {   
           foreach ptr $ptrs alias $alii {
              set elems [ lindex [ dc::ingestData $jobid $ptr ] 0 ]
              ;## if responsefile contains > 1 element, bail out.
              if { [ llength $elems ] > 1 } {
                 set msg "responsfile contained multiple elements: '$elems'"
                 ::dc::destructElement $jobid $ptr -nocomplain
                 return -code error $msg
              }
              
              lappend tmp $alias=$elems
           }
           
           ;## update aliases found in responsefiles
           set aliases [ set ::dc::state($job,aliases) ]
           set aliases [ string trim $aliases ";" ]
           set alii [ join $tmp ";" ]
           set aliases [ join [ list $aliases $alii ] ";" ]
           set ::dc::state($job,aliases) $aliases
        }   
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::ingestBucketData 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::ingestBucketData { jobid } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set objptrs [ set ::dc::state($job,objptrs) ]
        foreach { elements alii } \
           [ dc::ingestData $jobid $objptrs ] { break } 
        
        eval lappend ::dc::state($job,elements) $elements
        
        ;## if we are doing an expansion as for SFT handling
        set alii [ dc::expandAlgorithms $jobid $elements $alii ]
        
        ;## update aliases found in responsefiles
        set aliases [ set ::dc::state($job,aliases) ]
        set aliases [ string trim $aliases ";" ]
        set alii [ join $alii ";" ]
        set aliases [ join [ list $aliases $alii ] ";" ]
        set ::dc::state($job,aliases) $aliases

     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::loadCallChain 
##
## Description:
## Evaluate Call Chain
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::loadCallChain { jobid } {
     
     if { [ catch {
        set seqpt {}
        regexp {\d+} $jobid job
        set chain         [ set ::dc::state($job,chain)          ]
        set cc            [ set ::dc::state($job,cc)             ]
        set targ          [ set ::dc::state($job,datacondtarget) ]
        
        set dbg "call chain: '[ subst -nocommands $chain ]'"
        regsub -all {__bS} $dbg {\\} dbg
        regsub -all {__cOmMa} $dbg {,} dbg
        regsub -all {__sPaCe} $dbg { } dbg
        debugPuts $dbg

        foreach link $chain {
           ;## do _null, semicolon, and backslash substitution
           regsub -all {_null} $link "{}" link
           regsub -all {__bS} $link {\\\\} link
           regsub -all {__cOmMa} $link {,} link
           regsub -all {__sPaCe} $link { } link
           regsub -all {__sC} $link {;} link
           set seqpt "eval($link):"
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "--> start $seqpt"
           }
           eval $link
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "<-- end $seqpt"
           }
        }
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::execute
##
## Description:
## Execute the call chain in a thread.  Periodically
## check thread status, and lappend the results of the
## call chain to ::${jobid}_info when the thread returns.
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::execute { jobid { tid "" } { iter 0 } } {
     if { [ catch { 
        set seqpt {}
        regexp {\d+} $jobid job
        
        set cc [ set ::dc::state($job,cc) ]
        set seqpt "ExecuteCommands_t($cc):"
        if { ! [ string length $tid ] } {       
           __t::start exec$jobid
           set tid [ ExecuteCommands_t $cc ]
           set delay 100
        } else {
           set delay 1000
        }
         
        ;## reverse logic here is to avoid calling the _r in
        ;## a nested if context.
        set seqpt "getThreadStatus($tid):"
        if { ! [ regexp {FINISHED} [ getThreadStatus $tid ] ] && \
             ! [ string equal TARDY! $iter ] } {
           incr iter
           dc::tardyThread $jobid $tid $iter
           after $delay dc::execute $jobid $tid $iter
           return {}
        }     
        
        ;## here is where call chain exceptions get thrown
        set seqpt "ExecuteCommands_r($tid):"
        if { [ catch { 
           ExecuteCommands_r $tid
        } err1 ] } {
           set err "$seqpt $err1 (partial or null result likely)"
        }
        
        set seqpt "CallChain_GetResults($cc):"
        if { [ catch {
           set seqpt "CallChain_GetResults($cc):"
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "--> start $seqpt"
           }
           set resultp [ CallChain_GetResults $cc ]
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "<-- end $seqpt"
           }
           set seqpt {}
        } err2 ] } {
           set resultp {}
           lappend err "$seqpt $err2"
        }   
        
        __t::end "call chain executed in" exec$jobid
        catch { __t::cancel exec$jobid }
        
        if { [ info exists err ] } {
           set ::dc::state($job,err) $err
        }
        set ::dc::state($job,resultptr) $resultp
        set ::dc::state($job,end) 1
        
     } err ] } {
        ;## can get here if a job is killed while the looping
        ;## call is in the event loop
        if { [ string length $err ] } {
           dc::executeDefunct $jobid $seqpt $err $tid
        }
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::tardyThread
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::tardyThread { jobid tid iter } {
     
     regexp {\d+} $jobid job
     
     if { ! [ info exists ::BLINDLY_CALLBACK_TARDY_THREADS ] } {
        set ::BLINDLY_CALLBACK_TARDY_THREADS 0
     }
     
     if { ! [ info exists ::DC_LONG_RUNNING_THREAD_WARNING ] } {
        set ::DC_LONG_RUNNING_THREAD_WARNING 900
     }
     set timeout $::DC_LONG_RUNNING_THREAD_WARNING
     
     if { [ catch {
        if { $iter % $timeout == 0 } {
           set msg    "thread: $tid has been running for "
           append msg "$iter seconds without reaching state "
           append msg "FINISHED. current state is "
           append msg [ getThreadStatus $tid ]
           addLogEntry $msg orange
        }
        
        ;## this seems outrageous to me... there has to be a
        ;## way to make this safe via a C++ wrapped call.
        if { $iter == $timeout && \
             $::BLINDLY_CALLBACK_TARDY_THREADS == 1 } {
           uplevel set iter TARDY!
           set msg    "thread: $tid has been running for "
           append msg "$iter seconds without reaching state "
           append msg "FINISHED, *AND* the resource variable "
           append msg "::BLINDLY_CALLBACK_TARDY_THREADS is "
           append msg "set to '1', so I am going to run the "
           append msg "ExecuteCommands_r thread reaper on "
           append msg "this thread *NOW*! (this may cause the "
           append msg "datacond API to hang FOREVER!)"
           addLogEntry $msg orange
        }     
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::executeDefunct
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::executeDefunct { jobid seqpt err tid } {
     
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     
     if { [ catch {
        if { [ string length $err ] } {
           if { [ llength [ array names ::dc::state $job,* ] ] } {
              set ::dc::state($job,err) "dc::execute:$seqpt $err"
              set ::dc::state($job,end) 1
           }
        }
     } err ] } {
        addLogEntry $err email
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::execute_nothreads
##
## Description:
## Non-threading execute call for debugging of possible
## thread-safety or other thread related issues.
##
## Parameters:
##
## Usage:
##
## Comments:
## Exception handling is impaired with this call :TODO:

proc dc::execute_nothreads { jobid } {
     if { [ catch {
        regexp {\d+} $jobid job
        set cc [ set ::dc::state($job,cc) ]
        set seqpt "CallChain_Execute($cc):"
        if { $::DEBUG_TRACE_CPP } {
           debugPuts "--> start $seqpt"
        }
        CallChain_Execute $cc
        if { $::DEBUG_TRACE_CPP } {
           debugPuts "<-- end $seqpt"
        }
        set seqpt "CallChain_GetResults($cc):"
        if { $::DEBUG_TRACE_CPP } {
           debugPuts "--> start $seqpt"
        }
        set resultp [ CallChain_GetResults $cc ]
        if { $::DEBUG_TRACE_CPP } {
           debugPuts "<-- end $seqpt"
        }
        set seqpt {}
        lappend ::${jobid}_info $resultp
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::output
##
## Description:
## Send results through a data socket to the mpi API or
## write them to a file as ilwd text or LIGO_LW text.
## At this point the ::${jobid}_info list "end" element
## contains the name of the result object.
##
## The result object is always a container.  If intermediates
## were specified then they are elements 0-(n-1), and the
## result is in element n.  It is necessary to extract the
## reslt from the container before sending it to another
## API.  Intermediates are never forwarded to a subsequent
## API.
##
## Parameters:
##
## Usage:
##
## Comments:
## Output files can get overwritten until an autonaming
## scheme has been devised for this API.
##
## Setting ::DEBUG to [ expr 0xdb ] will cause a text
## dump of the output object in addition to all normal
## and expected behaviour.

proc dc::output { jobid } {
     if { [ catch { 
        set seqpt {}
        set urls [ list ]
        regexp {\d+} $jobid job
        set api_rx ^([ join "[ split $::API_LIST ] wrapper" | ])$
        set of     $::dc::state($job,outputformat)
        set rp     $::dc::state($job,returnprotocol)
        set target $::dc::state($job,datacondtarget)
        set state  $::dc::state($job,state)
        set resp   $::dc::state($job,responsefunction)
        set result $::dc::state($job,resultptr)
        
        if { ! [ regexp {^_[0-9a-f]+_p_} $result ] } {
           return -code error "no results returned for output"
        }
        
        ;## if the target for output is mpi or metadata we
        ;## are going to pass the result to the appropriate
        ;## api.
        if { [ string length $target ] && \
           ! [ string equal datacond $target ] } {
           if { [ regexp -nocase -- $api_rx $target ] } {
              set of [ list api $target ]
           } else {
              return -code error "unknown target api: $target"
           }
        }
        
        ;## dc::handleOutputs will always leave the actual
        ;## result pointer empty...
        set tmp $result
        foreach [ list result urls ] \
           [ dc::handleOutputs $jobid $result $target ] { break }
        if { ! [ string length $result ] } {
           set result $tmp
        }
        
        ;## attach pass-thrus to the result
        if { [ info exists ::dc::state($job,passfiles) ] } {
           foreach file $::dc::state($job,passfiles) {
              set ilwdp [ ilwd::readFile $file ]
              ilwd::addElement $ilwdp $result
              ::dc::destructElement $jobid $ilwdp
           }
           unset ilwdp
        }
        
        if { [ getContainerSize $result ] } {
        
           switch -regexp -- $of {
              {api} { 
                    set api [ lindex $of 1 ]
                    ilwd::setjob $result $jobid
                    if { $::DEBUG_DUMP_OBJECTS } {
                       dc::0xdb \
                          $jobid $result http://${result}_result_dump.ilwd
                    }
                    if { [ regexp -nocase {wrapper} $api ] } {
                       lappend urls \
                       [ dc::wrapperOutput $jobid $result $state $resp ]
                    } else {
                       ;## remove the object pointer from the destructor
                       ;## list!
                       set i [ lsearch $::dc::state($job,objptrs) $result ]
                       set ::dc::state($job,objptrs) \
                       [ lreplace $::dc::state($job,objptrs) $i $i ]
                       set i [ lsearch $::dc::state($job,resultptr) $result ]
                       set ::dc::state($job,resultptr) \
                       [ lreplace $::dc::state($job,resultptr) $i $i ]

                       set seqpt "dataSendThread($jobid $api $result):"
                       dataSendThread $jobid $api $result
                    }
                 }
          {ilwd} {
                 set rx (binary|ascii)
                 if { ! [ regexp -nocase $rx $of -> format ] } {
                    set format binary
                 }
                 lappend urls \
                   [ dc::ilwdOut $jobid $rp $result $format ]
                }
      {LIGO_LW} {
                set seqpt "dataSend($jobid ligolw $result):"
                dataSend $jobid ligolw $result
                }
        default {
                return -code error "unrecognised output format: $of"
                }
           } ;## end of switch
        } ;## end of if getContainerSize on result   
     } err ] } {
        if { [ info exists ilwdp ] } {
           ::dc::destructElement $jobid $ilwdp -nocomplain
        }
        sleep 1
        return -code error "[ myName ]:$seqpt $err"
     }
     return [ join $urls ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::ilwdOut
##
## Description:
## Apply -returnptrotocol as nearly as possible to output
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::ilwdOut { jobid rp result format } {
     
     if { [ catch {
        
        set bad_rx \[^\\\s\\\/\\\'\\\"\\\}\\\{\\\)\\\(\\\]\\\[\\\>\\\<\]+
        set sane_rx (file|ftp|http):\\\/+$bad_rx\(\\\/$bad_rx\)+
        
        set rp [ string trim [ join $rp ] ]
        if { ! [ string length [ join $rp ] ] } {
           set fn $jobid$result
        ;## a URL or filename not just a directory
        } elseif { ! [ regexp {\/$} $rp ] } {
           set fn [ file rootname [ file tail $rp ] ]
        ;## a URL
        } elseif { [ regexp {^(http|ftp):/*(\S+)} $rp -> ip name ] } {
           ;## a URL ending in a filename with extension
           if { [ string length [ file extension $name ] ] } {
              set fn [ file rootname [ file tail $name ] ]
           } else {
             ;## a URL ending in a directory or just a directory
             set fn [ file tail $name ]/$jobid$result
           }
        }
        
        set fn [ publicFile $jobid $fn $result $format ]
        set url $fn 
        
        if { [ regexp {(file|http|ftp):/*} $rp ] } {
           if { [ regexp $sane_rx $rp ] } {
              set url $rp
              outputUrls $jobid [ list $fn $url ]
           } else {
              regsub $::HTTPDIR $fn {} fn
              set url $::HTTPURL$fn
           }
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $url
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::wrapperOutput
##
## Description:
## Handler for wrapper output staging.
##
## Parameters:
##
## Usage:
##
## Comments:
## "state" is the jobid of the state source job.
## "resp" is the jobid of the response function.

proc dc::wrapperOutput { jobid ptr state resp } {
     
     if { [ catch {
        set seqpt {}
        
        if { ! [ regexp {\d+} $jobid N ] } {
           return -code error "malformed jobid: $jobid"
        }

        set drop [ set ::dc::state($N,dropdata) ]
        
        ;## handle the stateinfo from file
        if { [ string length $state ] } {
           if { [ regexp {^\d+$} $state ] } {
              set state $::RUNCODE$state
           }
           set statedir [ jobDirectory $state ]
           set state $statedir/${state}State.ilwd
           if { [ file exists $state ] } {
              set stateptr [ ilwd::readFile $state ]
              ilwd::addElement $stateptr $ptr
              ::dc::destructElement $jobid $stateptr
           } else {
              return -code error "state file not found: $state" 
           }
        }

        ;## handle the response function (full path to file)
        if { [ string length $resp ] } {
           if { [ file exists $resp ] } {
              set respptr [ ilwd::readFile $resp ]
              ilwd::addElement $respptr $ptr
              ::dc::destructElement $jobid $respptr
           } else {
              return -code error "response file not found: $resp" 
           }
        }
        
        if { $drop } {
           set url [ dc::dropWrapperData $jobid $ptr ]
        } else {
           set ::${ptr}_lock 1
           set ::dc::state($N,dataready) [ clock seconds ]
           set url [ list ]
        }
        
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $url
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::dropWrapperData
##
## Description:
## For interacting with GRID/GLOBUS we may have a special
## directory hierarchy to hold outgoing data in ilwd files.
##
## The special directory should be defined in the LDASapi.rsc
## file as ::OUTGOING_GRID_DATA_DIR.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::dropWrapperData { jobid ptr } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        ;## we now handle ascii or binary output
        set outputs [ set ::dc::state($job,outputs) ]
        set format [ lindex [ lindex $outputs end ] 1 ]
        set format [ string trim $format " \}\{" ]
        regexp -nocase {(ascii|binary)} $format -> format
        if { [ string equal -nocase ilwd $format ] } {
           set format binary
        }
        
        set proto $::dc::state($job,returnprotocol)

        ;## get rid of multiple slashes
        if { [ regexp {^gridftp} $proto ] } {
           regsub -all {/+} $proto / proto
        }
        ;## handle lefthand fragments of the grid ftp url toplevel
        if { [ string match ${proto}* ${::GRIDFTPURL}/ ] } {
           set proto gridftp
        }
        
        ;## something like gridftp:/usr1/grid/foo.ilwd
        ;## which is not a left handed fragment and which
        ;## would otherwise produce a confusing recursing
        ;## hierarchy.
        if { [ string equal [ file dirname $proto ] \
             [ file dirname $::GRIDFTPURL ] ] && \
             [ string length [ file tail $proto ] ] } {
           set proto ${::GRIDFTPURL}/[ file tail $proto ]
        }
        
        if { [ regexp {^gridftp:/+(\S+)/$} $proto -> gridsubdir ] } {
           set gridsubdir /$gridsubdir
           regsub ^.*$::GRIDFTPDIR $gridsubdir {} gridsubdir
           set url $::GRIDFTPURL$gridsubdir/result.ilwd
        } elseif { [ regexp {^gridftp:/+(\S+)/(\S+)([^/])$} $proto -> dir file oops ] } {
           set abspath [ file rootname /$dir/$file$oops ].ilwd
           regsub ^.*$::GRIDFTPDIR $abspath {} abspath
           set url $::GRIDFTPURL$abspath
        } elseif { [ regexp {^gridftp(:/{0,2})?$} $proto ] } { 
           set gridsubdir [ relativeDirectory $jobid ]
           set url $::GRIDFTPURL/$gridsubdir/result.ilwd
        } else {
           set url [ dc::dcTargetProtocol $jobid $proto $proto ]
        }
        
        ;## default filename for this command is special
        set def_rx {[\/]+(wrapperdata|result).ilwd$}
        regsub -nocase $def_rx $url "/${jobid}_wrapperdata.ilwd" url
        
        set dir [ jobDirectory ]
        set filename [ file tail $url ]
        
        set filename $dir/$filename
        
        ;## compression does NOT work.
        set comp none
        
        ilwd::write2disk $jobid $filename $ptr $format $comp 
        
        if { [ regexp {^gridftp} $url ] } {
           managerOutputUrl $jobid [ list $filename $url ]
        }
        
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $url
}
## ******************************************************** 

## ******************************************************** 
##
## Name: wrappercomm
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc wrappercomm { cid } {
     
     if { [ catch {
     
        set data [ list ]
        fileevent $cid readable {}
        set peerinfo [ fconfigure $cid -peername ]
        set data [ cmd::result $cid ]
        ::close $cid
        
        if { ! [ llength [ join $data ] ] == 3 } {
           set msg    "received malformed data from wrapper API. "
           append msg "(peerinfo: '$peerinfo') (data: '$data')"
           return -code error $msg
        }
        
        foreach [ list job host port ] $data { break }
        set jobid $::RUNCODE$job
        
        ;## it is possible that the wrapper will connect to a
        ;## newly restarted datacond API which no longer knows
        ;## about the job referenced by the wrapper
        if { ! [ llength [ array names ::dc::state $job,* ] ] } {
           if { [ catch {
              set msg    "$::MGRKEY set ::mpi::queue($job,ero) "
              append msg "{datacondAPI restarted, state for job lost "
              append msg "by datacond API. Job aborted.} ;"
              append msg "set ::mpi::queue($job,end) 1"
              set sid [ sock::open mpi emergency ]
              puts $sid $msg
              ::close $sid
           } err ] } {
              catch { ::close $sid }
           }
           set msg "no state array for job $jobid in datacondAPI.\n"
           append msg "this can mean that a wrapperAPI has requested\n"
           append msg "data from a recently restarted datacond API.\n"
           append msg "the LDAS system can recover from this condition,\n"
           append msg "but you should make certain that the datacond API\n"
           append msg "has recently restarted, and if NOT the logs\n"
           append msg "should be examined carefully to determine the\n"
           append msg "cause of the loss of state in the datacond API."
           return -code error $msg
        }
        
        set datap $::dc::state($job,resultptr)
       
        set sid [ datasocket -myaddr $::env(HOST) $host $port ]
        puts $sid $datap
        ::close $sid
        
        ::unset ::${datap}_lock
        
        set start $::dc::state($job,dataready)
        set now [ clock seconds ]
        set dt [ expr { $now - $start } ]
        debugPuts "$datap sent to wrapper API after $dt seconds" 
        dc::cleanup $jobid
     } err ] } {
        catch { ::close $cid }
        catch { ::close $sid }
        append err " (complete string recieved from wrapper: '$data')"
        after 0 datacond::killJob $jobid
        set subject "$::LDAS_SYSTEM datacondAPI error!" 
        addLogEntry "Subject: ${subject}; Body: $err " email
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::0xdb
##
## Description:
## Setting the level of ::DEBUG for this API to 0xdb will
## cause this function to be called to dump database table
## formatted output to an ilwd text file.
##
## Parameters:
##
## Usage:
##
## Comments:
## Set ::DEBUG to 0xdb by connecting to the emergency socket
## and sending: ::MGRKEY "set ::DEBUG 219"
##
## If the variable ::DEBUG_DUMP_OBJECTS is set to 0 then
## even if :DEBUG is set to a value which would otherwise
## produce dumps, dumping will be turned off.

proc dc::0xdb { jobid ptr { rp http://table_dump.ilwd } } {
     if { [ catch {
        if { [ info exists ::DEBUG_DUMP_OBJECTS ] } {
           if { ! $::DEBUG_DUMP_OBJECTS } {
              return {}
           }
        }
        foreach { url fn } [ url2file $jobid $rp ] { break }
        set fn [ ilwd::writeFile $jobid $ptr $fn ascii ]
        addLogEntry "table data dumped to $url" blue  
     } err ] } {
        if { [ string length $err ] } {
           addLogEntry $err red
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::cleanup
##
## Description:
## All objects in the global namespace are destructed, and
## errors are logged.
##
## Parameters:
##
## Usage:
##
## Comments:
## This is the only place where elements are destructed other
## than the destruction of the input ilwd objects done by
## dc::ingestData

proc dc::cleanup { jobid } {
     
     regexp -- {\d+} $jobid job
     set jobid $::RUNCODE$job
    
     if { [ catch { 
        emptyDataBucket $jobid
     } err ] } {
        addLogEntry $err 2
     }
     
     if { $::DEBUG_TRACE_CPP } {
        debugPuts "emptied data bucket, about to delete call chain"
     }
     
     ;## delete the call chain, if it exists
     if { [ catch { 
        if { [ info exists ::dc::state($job,cc) ] } {
           set cc [ set ::dc::state($job,cc) ]
           if { [ regexp {_p_CallChain} $cc ] } {
              set seqpt "delete_CallChain($cc):"
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "--> start $seqpt"
              }
              delete_CallChain $cc
              if { $::DEBUG_TRACE_CPP } {
                 debugPuts "<-- end $seqpt"
              }
              ;## unregister the cc swig command
              if    { [ string length [ info commands $cc ] ] } {
                    ;## debugPuts "rename $cc"
                    rename $cc {}
              }
              set seqpt {}
           } elseif { ! [ string length $cc ] } {
              ;## do nothing, we never got any input data!
           } else {
              addLogEntry "Expected _p_CallChain, got '$cc'" 2
           }
        }   
     } err ] } {
        addLogEntry "could not delete CallChain: $err" red
     }
     
     ;## destruct the resultptr
     if { [ info exists ::dc::state($job,resultptr) ] } {
        set resultp $::dc::state($job,resultptr)
        ::dc::destructElement $jobid $resultp -nocomplain
     }
     
     ;## destruct all ilwd elements registered to this
     ;## job i.d.
     if { [ catch {
        set errs [ list ]
        if { [ info exists ::dc::state($job,objptrs) ] } {
           set ptrs $::dc::state($job,objptrs)
           ::dc::destructElement $jobid $ptrs -nocomplain
        }
     } err ] } {
        addLogEntry "could not destruct Object(s): $err" red
     }

     if { [ catch { 
        ;## and get rid of all the evidence
        foreach name [ array names ::dc::state $job,* ] {
           ::unset ::dc::state($name)
        }
     } err ] } {
        addLogEntry "state array corrupt or nonexistant: $err" red
     }
 
     catch { unset ::$jobid }
     
}
## ********************************************************

## ******************************************************** 
##
## Name: dc::dumpState 
##
## Description:
## Dump some pretty html formatted state into the file
## $::PUBDIR/$jobid/dc_state_dump.html
##
## Parameters:
## pat - either a jobid or a *
##
## Usage:
##        dc::dumpState $jobid
##
##        dc::dumpState $jobid NORMAL1234
##
##        dc::dumpState $jobid 1234
##
##        dc::dumpState $jobid *
##
## Comments:
## Special function for the dumpDcState user command.

proc dc::dumpState { jobid { pat * } } {
     regexp -- {\d+} $jobid job
     set jobid $::RUNCODE$job
     set utc [ clock seconds ]
     set localtime [ clock format $utc -format "%x-%X %Z" ]

     set html    "<html><head>\n"
     append html "<title>data conditioning API state dump"
     append html " at $localtime</title>\n"
     append html "</head>\n"
     append html "<BODY BGCOLOR=\"#DDDDDD\" TEXT=\"#000000\">\n"
     append html "<h3>data conditioning API state dump"
     append html " at $localtime</h3>\n"
     
     ;## allow globbing patterns or job i.d.'s
     regexp {\d+} $pat pat
      
     if { [ catch {
        set names [ array names ::dc::state $pat,* ]
        set names [ lsort $names ]
        set old [ list ]
        foreach name $names {
           set data $::dc::state($name)
           foreach { id key } [ split $name "," ] { break }
           if { ! [ string equal $old $id ] } {
              append html "<h3><font color=red>$::RUNCODE$id</font></h3>\n"
           }
           append html "<font color=red>$id</font> "
           append html "<font color=green>$key</font> "
           append html "<tt>$data</tt>\n<br>\n"
           set old $id
        }
        
        append html "<!-- created by dc::dumpState -->\n"
        append html "</body></html>"
        set dumpdir [ jobDirectory ]
        set dumpfile [ file join $dumpdir dc_state_dump.html ]
        set fid [ open $dumpfile w ]
        puts $fid $html
        ::close $fid
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::destructElement
##
## Description:
## Wrapper for destructElement that incorporates the debug
## flag ::DEBUG_TRACE_DESTRUCT
##
## Parameters:
## jobid - job i.d.
## ptrs - a list of ilwd pointers
## nocomplain - flag to switch on/off exception throwing
##
## Usage:
##
## Comments:
## Verbose logging is enabled by setting the resource
## variable ::DEBUG_TRACE_DESTRUCT to 1

proc dc::destructElement { jobid ptrs { nocomplain "" } } {
     
     set seqpt [ list ]
     
     if { [ string length nocomplain ] } {
        set throw 0
     } else {
        set throw 1
     }
     
     regexp {\d+} $jobid job
     set jobid ${::RUNCODE}$job
     
     if { [ catch {
        set errs [ list ]
        foreach ptr $ptrs {
           if { ! [ regexp {^_[0-9a-f]+_p_} $ptr ] } {
              if { $::DEBUG_TRACE_DESTRUCT } {
                 addLogEntry "not an ilwd pointer: '$ptr'" purple
              }
              lappend errs "not an ilwd pointer: '$ptr'"
           } else {
              
              ;## ************************************************
              ;## temporary local version of pointer manager
              set destruct 1
              foreach item [ array names ::dc::state *,objptrs ] {
                 regexp {\d+} $item N
                 set i [ lsearch $::dc::state($item) $ptr ]
                 ;## if the pointer is registered with some job
                 if { $i != -1 } {
                    if { $N > $job } {
                       set destruct 0
                    } else {
                       set ::dc::state($item) \
                          [ lreplace $::dc::state($item) $i $i ]
                    }
                 }
              }
              if { ! $destruct } { continue }
              ;## ************************************************
              
              if { $::DEBUG_TRACE_DESTRUCT } {
                 addLogEntry "about to destruct '$ptr'" purple
              }
              set seqpt "destructElement($ptr):"
              if { [ catch {
                 ::destructElement $ptr
                 if { $::DEBUG_TRACE_DESTRUCT } {
                    addLogEntry "successfully destructed '$ptr'" purple
                 }
              } err ] } {
                 if { $::DEBUG_TRACE_DESTRUCT } {
                    addLogEntry "ERROR destructing '$ptr': $err" purple
                 }
                 lappend errs "$seqpt $ptr: $err"
              }
           }
        }
        if { [ string length $errs ] } {
           return -code error $errs
        }
     } err ] } {
        if { $throw } {
           return -code error "[ myName ]: $err"
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::qualityChannel 
##
## Description:
## Data quality channels should be ingested as late as
## possible so that the 'symbol table' of the call chain
## will be populated.
##
## The -dbqualitychannel option can produce multiple outputs.
## Results for more than one interferometer may be returned
## by a single query.
##
## :TODO:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::qualityChannel { jobid cc qchans } {
     
     set seqpt {}
     set name   [ list ]
     set objptr [ list ]
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     
     if { [ catch {
        
        set qclist [ dc::qualChanValid $jobid ]
        
        foreach element $qclist {
           foreach { _name opts } $element { break }
        
           ;## find the right ptr and go!
           foreach nameandptr $qchans {
              foreach { name objptr } $nameandptr { break }
              if { [ string equal $name $_name ] } {
                 ;## pop out of foreach nameandptr
                 break
              }
              set name   [ list ]
              set objptr [ list ]
           }
           
           ;## name but no ilwd object
           if { ! [ string length $name ] } { 
              if { [ string length $objptr ] } {
                 set msg    "ilwd object: '$objptr' looks like a "
                 append msg "quality channel, but there is no 'name' "
                 append msg "attribute that can be matched to it." 
                 return -code error $msg
              }
           }
           
           ;## ilwd object but no name attribute
           if { ! [ string length $objptr ] } { 
              if { [ string length $name ] } {
                 set msg    "name: '$name' looks like an attribute for"
                 append msg "a quality channel, but there is no ilwd "
                 append msg "object that can be matched to it." 
                 return -code error $msg
              }
           }
           
           foreach { sample_rate stimes stimen etimes \
                     etimen pushpass } $opts { break }
           
           ;## may be several of these...
           set seqdat    "$cc $sample_rate $stimes $stimen "
           append seqdat "$etimes $etimen $objptr $name $pushpass"
           set seqpt "CallChain_IngestQualityChannel($seqdat):"
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "--> start $seqpt"
           }
           CallChain_IngestQualityChannel $cc $sample_rate \
           $stimes $stimen $etimes $etimen $objptr $name $pushpass
           if { $::DEBUG_TRACE_CPP } {
              debugPuts "<-- end $seqpt"
           }

           set msg    "ingested quality channel: "
           append msg "(name: '$name' ilwdp: '$objptr' "
           append msg "push_or_pass: '$pushpass')"
           debugPuts $msg
           
           if { [ info exists ::DEBUG_SYMBOL_TABLE ] && \
                ! [ string equal 0 $::DEBUG_SYMBOL_TABLE ] } {
              set msg [ CallChain_DumpSymbolTable $cc ]
              addLogEntry "Symbol table: ($msg)" purple
           }
           
           ::dc::destructElement $jobid $objptr
        }
        
     } err ] } {
        if { [ string length $err ] } {
           foreach nameandptr $qchans {
              foreach { name objptr } $nameandptr { break }
           ::dc::destructElement $jobid $objptr -nocomplain
           }
        }
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::qualChanValid 
##
## Description:
## Validation routine for qualityChannel... should probably
## be in metadata API.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::qualChanValid { jobid } {
     
     regexp {\d+} $jobid job
     set jobid $::RUNCODE$job
     set retval [ list ]
     
     if { [ catch {
        
        if { [ info exists ::dc::state($job,dbqualitychannel) ] } {
           set qc $::dc::state($job,dbqualitychannel)
           set qc [ string trim $qc ]
        } else {
           set qc [ list ]
        }
        
        ;## null string, just return
        if { ! [ string length $qc ] } {
           return {}
        }
        
        ;## not enough arguments provided
        if { [ llength [ lindex $qc 0 ] ] != 8 } {
           if { [ llength $qc ] == 8 \
             && [ llength [ lindex $qc 0 ] ] == 1 } {
              set qc "\{${qc}\}"
           } else {
              set msg "-dbqualitychannel list length != 8: '$qc'"
              return -code error $msg
           }
        }
        
        ;## regex patterns for validating user option
        ;## -dbqualitychannel which is named '$qc' here.
        set sample_rate_rx {\d+}
        set stimes_rx      {\d+}
        set stimen_rx      {\d+}
        set etimes_rx      {\d+}
        set etimen_rx      {\d+}
        set sql_rx         {[:alpha:]+}
        set name_rx        {.*}
        set pushpass_rx    {(push|pass)}
        
        ;## validate the user options.  this is done after
        ;## the metadata API has finished processing, so is
        ;## extremely unlikely to catch anything!!
        set msg [ list ]
        foreach chan $qc {
           foreach { sample_rate stimes stimen etimes \
                     etimen sql name pushpass } $chan { break }
           
           ;## if the user got the alias and pushpass options
           ;## in the wrong order, the metadata api will pass
           ;## them in the ilwd comment in the wrong order!
           if { [ regexp -nocase -- {^(push|pass)$} $name ] } {
              set tmp $name
              set name $pushpass
              set pushpass $tmp
           }
           
           set pushpass [ string tolower $pushpass ]
        
           ;## check for malformed options
           set msg [ list ]
           foreach opt { sample_rate stimes stimen etimes \
                         etimen sql name pushpass } {
              if { ! [ regexp [ set ${opt}_rx ] [ set $opt ] ] } {
                 lappend msg \
                 "malformed quality channel option: $opt: '[ set $opt ]'"
              }
           }
           set opts \
           [ list $sample_rate $stimes $stimen $etimes $etimen $pushpass ]
           lappend retval [ list $name $opts ]
        }
        
        if { [ llength $msg ] } {
           return -code error $msg
        }

     } err ] } {
        if { [ string length $err ] } {
           return -code error "[ myName ]: $err"
        } 
     }
     return $retval
}
## ******************************************************** 

## ******************************************************** 
##
## Name: datacond::killJob
##
## Description:
## Procedure called via the emergency socket by the manager
## API for unregistering a job when requested or required.
## Parameters:
##
## Usage:
##
## Comments:
##

proc datacond::killJob { jobid } {
     
     if { [ catch {
        regexp {\d+} $jobid job
        set jobid $::RUNCODE$job
        
        ;## remove the extra lock from the resultptr if
        ;## it exists.
        if { [ info exists ::dc::state($job,resultptr) ] } {
           set resultptr [ set ::dc::state($job,resultptr) ]
           if { [ info exists ::${resultptr}_lock ] } {
              unset ::${resultptr}_lock
           }
        }
        
        ;## now we can cleanup!
        ::dc::cleanup $jobid 
     } err ] } {
        addLogEntry $err email
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: dc::registerObject
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc dc::registerObject { jobid contp } {
     
     if { [ catch {

        set seqpt {}
        set datap _DEADBEEF_p_LdasContainer
        regexp {\d+} $jobid job
        set seqpt "copyContainerElement($contp 0):"
        set datap [ copyContainerElement $contp 0 ]
        set seqpt {}
        lappend ::dc::state($job,objptrs) $datap
        set ::dc::state($job,objptrs) \
           [ lsort -unique [ set ::dc::state($job,objptrs) ] ]
        ::dc::destructElement $jobid $contp

     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return $datap
}
## ******************************************************** 

## ******************************************************** 
##
## Name: frame::defunctJobs
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc datacond::defunctJobs { args } {
     
     if { [ catch {
        if { [ info exists ::SORTED_LIVE_JOB_LIST_REPORT ] } {
           set oldest [ lindex $::SORTED_LIVE_JOB_LIST_REPORT 0 ]
           regexp {\d+} $oldest oldest
           set jobs [ info vars ::${::RUNCODE}* ]
           if { [ string length $oldest ] } {
              foreach jobid $jobs {
                 regexp {\d+} $jobid job
                 set jobid $::RUNCODE$job
                 if { $job + 10 < $oldest  } {
                 	addLogEntry "removing defunct jobid: $jobid" purple
                    datacond::killJob $job
                 }
              }
           } 
        }
     } err ] } {
        set subject "$::LDAS_SYSTEM $::API API error!"
        set report "[ myName ]: $err"
        addLogEntry "Subject: ${subject}; Body: $report" email
     }
}
## ********************************************************
