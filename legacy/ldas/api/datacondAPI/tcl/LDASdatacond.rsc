## ********************************************************
## 
## Name: LDASdatacond.rsc
##
## This is the datacond API specific resource file.  It contains
## resource information which is only used by the datacond API.
##
## If this file is not found there the datacond API and hence the
## datacond API will be non-functional.
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=HEAD;content-type=text%2Fplain
##
## ********************************************************
;#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## cmonClient MODIFIABLE RESOURCES

;## desc=max memory MB allowed
set ::MEMFLAG_MEGS 2048 

;## desc=path to lsof executable
set ::PATH_TO_LSOF /usr/sbin/lsof

;## desc=seconds timeout for data socket receive
set ::DATABUCKET_TIMEOUT 180

;## desc=minimum free memory required for API to start new job
set ::MINIMUM_FREE_MEM_K 250000

;## desc=should we risk lockup by calling back non-reporting threads? 
set ::BLINDLY_CALLBACK_TARDY_THREADS 0

;## desc=number of seconds a thread can run before we report tardiness
set ::DC_LONG_RUNNING_THREAD_WARNING 900

;## desc=set to '1' to cause free memory to be logged with a blue ball
set ::DEBUG_MINIMUM_FREE_MEM_K 0

;## desc=dump all ilwd objects to disk
set ::DEBUG_DUMP_OBJECTS 0

;## desc=log all calls to CallChain c++ commands
set ::DEBUG_TRACE_CPP 0

;## desc=reduce threaded calls to a minimum
set ::DEBUG_NO_THREADS 0

;## desc=log table of contents of object sent to wrapper
set ::DEBUG_SHOW_WRAPPER_DATA_TOC 0

;## desc=virtual resource limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default core default cputime default datasize default \
filesize default memorylocked default descriptors default maxproc default ]
