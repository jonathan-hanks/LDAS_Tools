#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: fftperf01d.tcl,v 1.7 2004/01/14 20:38:20 Philip.Charlton Exp $

;## fft performance test

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;## FFTPERF01

;## Build the user command string
set ssub ""
set Nlist [ list \
		1024 \
		2048 \
		3072 \
		4096 \
		5120 \
		6144 \
		8192 \
		9216 \
		10240 \
		12288 \
		15360 \
		16384 \
		18432 \
		20480 \
		24576 \
		25600 \
		27648 \
		30720 \
		32768 \
		36864 \
		40960 \
		46080 \
		49152 \
		51200 \
		55296 \
		61440 \
		65536 \
		73728 \
		76800 \
		81920 \
		82944 \
		92160 \
		98304 \
		102400 \
		110592 \
		122880 \
		128000 \
		131072 \
		138240 \
		147456 \
		153600 \
		163840 \
		165888 \
		184320 \
		196608 \
		204800 \
		221184 \
		230400 \
		245760 \
		248832 \
		256000 \
		262144 \
		276480 \
		294912 \
		307200 \
		327680 \
		331776 \
		368640 \
		384000 \
		393216 \
		409600 \
		414720 \
		442368 \
		460800 \
		491520 \
		497664 \
		512000 \
		524288 \
		552960 \
		589824 \
		614400 \
		640000 \
		655360 \
		663552 \
		691200 \
		737280 \
		746496 \
		768000 \
		786432 \
		819200 \
		829440 \
		884736 \
		921600 \
		983040 \
		995328 \
		1024000 \
		1048576 \
		1105920 \
		1152000 \
		1179648 \
		1228800 \
		1244160 \
		1280000 \
		1310720 \
		1327104 \
		1382400 \
		1474560 \
		1492992 \
		1536000 \
		1572864 \
		1638400 \
		1658880 \
		1769472 \
		1843200 \
		1920000 \
		1966080 \
		1990656 \
		2048000 \
		2073600 \
		2097152 \
		2211840 \
		2239488 \
	   ]

foreach N $Nlist {

    set sub "
      x = slice(data, 0, $N, 1);
      fft(x);
      ti = user_time();
      fft(x); fft(x); fft(x); fft(x); fft(x);
      fft(x); fft(x); fft(x); fft(x); fft(x);
      fft(x); fft(x); fft(x); fft(x); fft(x);
      fft(x); fft(x); fft(x); fft(x); fft(x);
      tf = user_time();
      t = sub(tf, ti);
      time = div(t, 20.0);
      output(time,_,_,time,Time to fft data for length ${N});"
    
    set ssub "$ssub\n$sub"
}

set ifilepre "N8388608"
set ofilepre "FFTPERF01"

;## First test set: correct values on sequence
;## for length 1024 and default fft length and overlap

set ifile ldas_outgoing/mdc/input/${ifilepre}C16.sim
set ppost DC16  ;## Need to change this for tests A, B, C
set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd

set cmd "ldasJob { -name $user -password $pwrd -email $email } 
  { conditionData 
    -inputprotocol file:/$ifile
    -returnprotocol file:/$ofile -outputformat { ilwd ascii }
    -aliases { data = mdc_input_data:chan_01:data } 
    -algorithms { 
       $ssub
   } 
} "

sendCmd $cmd
