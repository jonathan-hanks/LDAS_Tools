#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval fftapi {
	
    set dtypes { R4 R8 C8 C16 }
	set ifilepre N200
	set ofilepre FFTAPI01
	set letter A
	set length 0
    set cmdstem { conditionData
            -subject $subject
            -inputprotocol { file:/$ifile $efile }
            -returnprotocol file:/$ofile
            -outputformat { ilwd ascii }
            -aliases { idata = mdc_input_data:chan_01:data $extra_aliases }
            -algorithms { 
            	$algo
            } }
            
	set algo_fftapi01a {
   		x = slice(idata,0,${length},1);
        rx = real(x);
        n = size(rx);
        output(n,_,_,n,length of input data);
        y=fft(x);
        output(y,_,_,y,fft of input data);
    }
    set ifile_fftapi01b {ldas_outgoing/mdc/input/$ifilepre${dtype}FFT.sim}
    
    set algo_fftapi01b $algo_fftapi01a

	set	algo_fftapi02 {
            x = slice(idata,0,${length},1);
            n1 = size(x);
            output(n1,_,_,n1,length of input data before FFT);
            y = fft(x);

            dif1 = sub(y, exp);
            dif1 = abs(dif1);
            max1 = max(dif1);
            output(max1,_,_,max1,maximum of abs difference for FFT);
            
            # if max1 > tol, throw error
            $sgn_subst1
            sgn = floor(sgn);
            sgn = integer(sgn);
            slice(x, sgn, 1, 1);

            # Verify Parseval 
            xrms = abs(x);
            xrms = mul(xrms, xrms);
            xrms = sum(xrms);
            xrms = sqrt(xrms);

            yrms = abs(y);
            yrms = mul(yrms, yrms);
            yrms = sum(yrms);
            yrms = div(yrms, n1);
            yrms = sqrt(yrms);

            dif1 = sub(xrms, yrms);
            output(dif1,_,_,dif1,diff of rms of FFT output);

            # Verify IFFT matches x
            z = ifft(y);
            n2 = size(z);

            dif2 = sub(z,x);
            dif2 = abs(dif2);
            max2 = max(dif2);
            output(max2,_,_,max2,maximum of abs difference for IFFT);

            # if max2 > tol, throw error
            $sgn_subst2
            sgn = floor(sgn);
            sgn = integer(sgn);
            slice(x, sgn, 1, 1);

            output(n2,_,_,n2,length of output data after IFFT);
    }
    
    namespace export fftapi*
    
    proc fftapiTest { ifilepre length algoname { needPrefix 0 } { sgn_subst1 "" } { sgn_subst2 "" } } {
    
    	if	{ [ catch {
        	set name [ namespace current ]
			set procname [ info level -1 ]
            namespace import [ namespace parent $name ]::*           
			
            set ofilepre 	[ string toupper $procname ]
            set cmdstem [ namespace eval $name set cmdstem ]
            set dtypes [ namespace eval $name set dtypes ]
            
            set algo [ namespace eval $name set $algoname ]
            
            set rc [ namespace eval $name info exist ifile_$procname ]
            
			foreach dtype $dtypes {
				if	{ !$rc } {
            		set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
                } else {
                	set ifile [ namespace eval $name set ifile_${procname} ]
                }
                
				if	{ $needPrefix } {
            		set prefix $ofilepre$dtype
    				set efile file:/ldas_outgoing/mdc/input/$prefix.exp
                	set ofile ldas_outgoing/mdc/output/$prefix.ilwd
                    set extra_aliases "; exp = $prefix:fftout:data"
            	} else {
					if	{ [ regexp -nocase -- {01b} $procname ] } { 
						set ifile ldas_outgoing/mdc/input/$ifilepre${dtype}FFT.sim   
					} else {
                		set ifile ldas_outgoing/mdc/input/$ifilepre${dtype}.sim 
					}  	
            		set ofile ldas_outgoing/mdc/output/$ofilepre$dtype.ilwd    
                    set extra_aliases ""
                    set efile ""		
            	}
                set subject datacondMDC:conditionData:$procname:$dtype
                set algo [ subst $algo ]
                set cmd [ subst $cmdstem ]
                runTest $subject $cmd            
    		}
    	} err ] } {
    		return -code error $err
		}
	}
    
## test that taking FFTs of a sequence with invalid length (N=0)
;## raises an exception    

	proc fftapi01a {} {
       
		if	{ [ catch {
			set ifilepre N200
			set code A
			set length 0   	
			fftapiTest $ifilepre $length algo_fftapi01a
		} err ] } {
    		return -code error $err
		}
	}

	proc fftapi01b {} {

		if	{ [ catch {
			set ifilepre N8388609
			set code B
			set length 8388609
			fftapiTest $ifilepre $length algo_fftapi01b
		} err ] } {
    		return -code error $err
		}
	}

;## FFTAPI02
;##
;## test that FFT and IFFT return correct results for a valid sequence
;## of length N=1

	proc fftapi02a {} {

		if	{ [ catch {
			set ifilepre N200
			set code A
			set length 1
        	set needPrefix 1
          set sgn_subst1 "sgn = sub(1.0e-3, max1);"
        	set sgn_subst2 "sgn = sub(1.0e-3, max2);"
        	fftapiTest $ifilepre $length algo_fftapi02 $needPrefix $sgn_subst1 $sgn_subst2     
    	} err ] } {
    		return -code error $err
		}
	}

;## FFTAPI02
;##
;## test that FFT and IFFT return correct results for a valid sequence
;## of length N=1024

	proc fftapi02b {} {

		if	{ [ catch {
			set ifilepre N10
			set code B
			set length 1024
        	set sgn_subst1 "sgn = sub(1.0, max1);"
        	set sgn_subst2 "sgn = sub(1.0e-1, max2);"
        	set needPrefix 1
			fftapiTest $ifilepre $length algo_fftapi02 $needPrefix $sgn_subst1 $sgn_subst2
    	} err ] } {
    		return -code error $err
		}
	}

	proc fftapi02c {} {
    
		if	{ [ catch {
			set ifilepre N19
			set code C
			set length 122880
			set sgn_subst1 "sgn = sub(5.0, max1);"
        	set sgn_subst2 "sgn = sub(1.0e-2, max2);"
        	set needPrefix 1
			fftapiTest $ifilepre $length algo_fftapi02 $needPrefix $sgn_subst1 $sgn_subst2 
    	} err ] } {
    		return -code error $err
		}
	}

	proc fftapi02d {} {

		if	{ [ catch {
			set ifilepre N23
			set ofilepre FFTAPI02
			set code D
			set length 1843200
			set sgn_subst1 "sgn = sub(25.0, max1);"
        	set sgn_subst2 "sgn = sub(1.0e-1, max2);"
        	set needPrefix 1
			fftapiTest $ifilepre $length algo_fftapi02 $needPrefix $sgn_subst1 $sgn_subst2
    	} err ] } {
    		return -code error $err
		}
	}

	proc fftapi02e {} {

		if	{ [ catch {
			set ifilepre N8388608
			set ofilepre FFTAPI02
			set code E
			set length 8388608
			set index  4194304
			set sgn_subst1 "sgn = sub(55, max1);"
        	set sgn_subst2 "sgn = sub(1.0e-1, max2);"
        	set needPrefix 1
			fftapiTest $ifilepre $length algo_fftapi02 $needPrefix $sgn_subst1 $sgn_subst2		        
    	} err ] } {
    		return -code error $err
		}
	}
}
namespace import fftapi::*
