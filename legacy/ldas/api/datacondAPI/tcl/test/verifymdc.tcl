#! /ldcg/bin/tclsh

;## run this in email directory to
;## verify all emails for mdc has been received.

set auto_path "/ldas/lib $auto_path"
set output [ lindex $argv 0 ]
set fd [ open $output r ]
set data [ read $fd ]
close $fd

set data [ split $data \n ]

foreach line $data {
    if  { [ regexp {(.+).tcl} $line -> testmode ] } {
        if  { ! [ info exist test($testmode) ] } {
            set test($testmode) [ list ]
        }
        continue
    }
    if  { [ regexp {(LDAS-.+)} $line -> jobid ] } {
        lappend test($testmode) $jobid
    }
}

source  $::env(HOME)/.datacondAPI.rc

if	{ [ regexp {cit|dev|test|lho|llo|mit} $site ] } {
	set site ldas-$site
}
set maildir $::env(HOME)/Mail/mdctest/cur

set filelist [ glob $maildir/* ]
set filelist [ lsort -dictionary $filelist ]
set passed_error 0
set passed_result 0 
set failed 0
foreach entry [ lsort -dictionary [ array names test ] ] {
    if  { [ regexp {02|pipe01|realimagabsapi01} $entry ] } {
        set pass 1
    } else {
        set pass 0
    }
    foreach jobid $test($entry) {
        if  { $pass } {
            set pattern "$jobid results"
        } else {
            set pattern "$jobid error!"
        }
        set found 0
        set index 0
        foreach file $filelist {
            set cmd "exec grep \"$pattern\" $file"
            set rc [ catch { eval $cmd } err ]
            if  { !$rc } {
                set found 1
                if  { ! $pass } {
                    set cmd1 "exec grep \"St9bad_alloc\" $file"
                    set rc1 [ catch { eval $cmd1 } err1 ]
                    if  { $rc1 } {
                        puts "$entry: $jobid passed: $err"
                        incr passed_error
                    } else {
                        puts "$entry: $jobid failed: $err, $err1"
                        incr failed
                    }
                } else {
                    puts "$entry: $jobid passed: $err"
                    incr passed_result 
                }
                set filelist [ lreplace $filelist $index $index ]
                puts "files remaining [ llength $filelist ]"
                break
            } else {
                incr index 1
            }
        }
        if  { ! $found } {
            incr failed 1
            puts "$entry has no email: $jobid failed"
        }
    }
}

set total_emails [ expr $passed_error + $passed_result + $failed ]
puts "Total emails $total_emails, $passed_result expected to pass, \
$passed_error expected to fail, got $failed unexpected results"    
if  { $total_emails == 419 && $passed_result == 137 && $passed_error == 282 } {
    puts "TEST PASSED"
} else {
    puts "TEST FAILED, expected 419 emails, 137 passed and 282 failed"
}
