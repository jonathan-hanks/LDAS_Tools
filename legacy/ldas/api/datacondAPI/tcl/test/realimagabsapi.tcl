#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval realimagabsapi {

	namespace export realimagabsapi*
    
	proc realimagabsapi01 {} {

		if	{ [ catch {
        
        		set name [ namespace current ]
          	namespace import [ namespace parent $name ]::*  
            
    			set postfixes { "R4" "R8" "C8" "C16" }
			set ifilepre "N8388608"
			set ofilepre "REALIMAGABSAPI01"
			set procname [ info level 0 ]
    
			foreach post $postfixes {

    			set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    			set ppost A$post
    			set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd
				set subject datacondMDC:conditionData:$procname:$post
        
    			set cmd "conditionData
    			-subject $subject
        		-inputprotocol { file:/$ifile }
        		-returnprotocol file:/$ofile -outputformat { ilwd ascii }
        		-aliases { raw = mdc_input_data:chan_01:data }
        		-algorithms {

            	minusInt = sub(0,1);
            	minusDouble = sub(0.0,1.0);

            	ri = real(minusInt);
            	output(ri,_,_,ri,{real(-1)});
            	ii = imag(minusInt);
            	output(ii,_,_,ii,{imag(-1)});
            	ai = abs(minusInt);
            	output(ai,_,_,ai,{abs(-1)});

            	rd = real(minusDouble);
            	output(rd,_,_,rd,{real(-1.0)});
            	id = imag(minusDouble);
            	output(id,_,_,id,{imag(-1.0)});
            	ad = abs(minusDouble);
            	output(ad,_,_,ad,{abs(-1.0)});

            	v = slice(raw,0,10,1);
            	output(v,_,_,v,valarray);
            	rv = real(v);
            	output(rv,_,_,rv,{real(valarray)});
            	iv = imag(v);
            	output(iv,_,_,iv,{imag(valarray)});
            	av = abs(v);
            	output(av,_,_,av,{abs(valarray)});

            	f = fft(v);
            	output(f,_,_,f,fvalarray);
            	rf = real(f);
            	output(rf,_,_,rf,{real(fvalarray)});
            	if = imag(f);
            	output(if,_,_,if,{imag(fvalarray)});
            	af = abs(f);
            	output(af,_,_,af,{abs(fvalarray)});
    			} "
				runTest $subject $cmd
				after 1000
			}
    	} err ] } {
    		return -code error $err
		}  
	}

}
namespace import realimagabsapi::*
