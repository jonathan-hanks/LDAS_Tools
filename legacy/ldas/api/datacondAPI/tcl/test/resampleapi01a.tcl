#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: resampleapi01a.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

;## RESAMPLEAPI01
;## resample api tests
;## Resample exceptions: zero p
;## Command should throw an exception

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N200"
set ofilepre "RESAMPLEAPI01"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost A$post
    set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases { xlong = mdc_input_data:chan_01:data }
        -algorithms {
            x = slice(xlong,0,100,1);
            y = resample(x,0,1);

            output(y,_,_,y,resample output);
        }
    } "

    sendCmd $cmd
}
