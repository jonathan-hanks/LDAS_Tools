#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: mixerapi01b.tcl,v 1.10 2004/01/14 20:38:20 Philip.Charlton Exp $

;## MIXERAPI01
;## mixer api tests
;## exception test: invalid_argument

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N200"
set ofilepre "MIXERAPI01"
set idir ldas_outgoing/mdc/input
set odir ldas_outgoing/mdc/output

foreach post $postfixes {
    set ifile ${idir}/${ifilepre}${post}.sim
    set ofile ${odir}/${ofilepre}B${post}.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol file:/${ifile}
        -returnprotocol file:/${ofile}
        -outputformat { ilwd ascii }
        -aliases {raw=mdc_input_data:chan_01:data}
        -algorithms {
            x = slice(raw,0,0,1);
            phi = value(0.0);
            f = value(0.125);
            z = mix(phi,f,x);
            output(z,_,_,z,mix);
        }
    } "

    sendCmd $cmd
}
