#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

;## this test causes runtime job timed out in datacond.
namespace eval fftperf {
	;## common for all fftperf tests
	;## Build the user command string
	
	set Nlist [ list \
		1024 \
		2048 \
		3072 \
		4096 \
		5120 \
		6144 \
		8192 \
		9216 \
		10240 \
		12288 \
		15360 \
		16384 \
		18432 \
		20480 \
		24576 \
		25600 \
		27648 \
		30720 \
		32768 \
		36864 \
		40960 \
		46080 \
		49152 \
		51200 \
		55296 \
		61440 \
		65536 \
		73728 \
		76800 \
		81920 \
		82944 \
		92160 \
		98304 \
		102400 \
		110592 \
		122880 \
		128000 \
		131072 \
		138240 \
		147456 \
		153600 \
		163840 \
		165888 \
		184320 \
		196608 \
		204800 \
		221184 \
		230400 \
		245760 \
		248832 \
		256000 \
		262144 \
		276480 \
		294912 \
		307200 \
		327680 \
		331776 \
		368640 \
		384000 \
		393216 \
		409600 \
		414720 \
		442368 \
		460800 \
		491520 \
		497664 \
		512000 \
		524288 \
		552960 \
		589824 \
		614400 \
		640000 \
		655360 \
		663552 \
		691200 \
		737280 \
		746496 \
		768000 \
		786432 \
		819200 \
		829440 \
		884736 \
		921600 \
		983040 \
		995328 \
		1024000 \
		1048576 \
		1105920 \
		1152000 \
		1179648 \
		1228800 \
		1244160 \
		1280000 \
		1310720 \
		1327104 \
		1382400 \
		1474560 \
		1492992 \
		1536000 \
		1572864 \
		1638400 \
		1658880 \
		1769472 \
		1843200 \
		1920000 \
		1966080 \
		1990656 \
		2048000 \
		2073600 \
		2097152 \
		2211840 \
		2239488 \
		2304000 \
		2359296 \
		2457600 \
		2488320 \
		2560000 \
		2621440 \
		2654208 \
		2764800 \
		2949120 \
		2985984 \
		3072000 \
		3145728 \
		3200000 \
		3276800 \
		3317760 \
		3456000 \
		3538944 \
		3686400 \
		3732480 \
		3840000 \
		3932160 \
		3981312 \
		4096000 \
		4147200 \
		4194304 \
		4423680 \
		4478976 \
		4608000 \
		4718592 \
		4915200 \
		4976640 \
		5120000 \
		5242880 \
		5308416 \
		5529600 \
		5760000 \
		5898240 \
		5971968 \
		6144000 \
		6220800 \
		6291456 \
		6400000 \
		6553600 \
		6635520 \
		6912000 \
		7077888 \
		7372800 \
		7464960 \
		7680000 \
		7864320 \
		7962624 \
		8192000 \
		8294400 \
		8388608 \
	   ]

    set ifilepre "N8388608"
	set ofilepre "FFTPERF01"
	
    namespace export fftperf*
           
	proc fftperfTest { icode ppost Nlimit } {
		
	if	{ [ catch {
		set procname [ info level 0 ]
        set parent [ namespace current ]
        ;## can invoke parent procs
        namespace import [ namespace parent $parent ]::*
        
		set Nlist [ lrange [ set ${parent}::Nlist ] 0 $Nlimit ]
        set ssub ""
        
        foreach N $Nlist {

    		set sub "
      		x = slice(data, 0, $N, 1);
      		fft(x);
      		ti = user_time();
      		fft(x); fft(x); fft(x); fft(x); fft(x);
      		fft(x); fft(x); fft(x); fft(x); fft(x);
      		fft(x); fft(x); fft(x); fft(x); fft(x);
      		fft(x); fft(x); fft(x); fft(x); fft(x);
      		tf = user_time();
      		t = sub(tf, ti);
      		time = div(t, 20.0);
      		output(time,_,_,time,Time to fft data for length ${N});"
    
    		set ssub "$ssub\n$sub"
		}
	;## First test set: correct values on sequence
	;## for length 1024 and default fft length and overlap
		set ifile /ldas_outgoing/mdc/input/[ set ${parent}::ifilepre ]${icode}.sim
        #set ifile /ldas_outgoing/mdc/input/${ifilepre}${icode}.sim

		set ofile /ldas_outgoing/mdc/output/[ set ${parent}::ofilepre ]$ppost.ilwd
        #set ofile /ldas_outgoing/mdc/output/${ofilepre}${ppost}.ilwd
		set subject datacondMDC:conditionData:$procname:$ppost
    
		set cmd "conditionData 
    	-subject $subject
    	-inputprotocol file:/$ifile
    	-returnprotocol file:/$ofile -outputformat { ilwd ascii }
    	-aliases { data = mdc_input_data:chan_01:data } 
    	-algorithms { 
       	[ set ${parent}::ssub ]
		} "
		runTest $subject $cmd         
    } err ] } {
    	return -code error $err
	}
	}
    
    proc fftperf01a {} {
    	if	{ [ catch {
        	set parent [ namespace current ]
        	namespace import [ namespace parent $parent ]::*
    		fftperfTest R4 AR4 end
        } err ] } {
        	return -code error $err
        }
    }
    
    proc fftperf01b {} {
    	if	{ [ catch {
        	set parent [ namespace current ]
        	namespace import [ namespace parent $parent ]::*
    		fftperfTest R8 BR8 end
        } err ] } {
        	return -code error $err
        }
    }
    
    proc fftperf01c {} {
    	if	{ [ catch {
        	set parent [ namespace current ]
        	namespace import [ namespace parent $parent ]::*
    		fftperfTest C8 CC8 end
        } err ] } {
        	return -code error $err
        }
    }
    
    proc fftperf01d {} {
    	if	{ [ catch {
        	set parent [ namespace current ]
        	namespace import [ namespace parent $parent ]::*
    		fftperfTest C16 DC16 end
        } err ] } {
        	return -code error $err
        }
    }
    
}
namespace import fftperf::*
