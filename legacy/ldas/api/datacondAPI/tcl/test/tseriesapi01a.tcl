#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: tseriesapi01a.tcl,v 1.3 2004/01/14 20:38:20 Philip.Charlton Exp $

;## TSERIESAPI02
;##
;## test for tseries action
;## 

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set actions {
    "ts = tseries();"
    "ts = tseries(1.0);"
    "ts = tseries(1.0, 2048.0);"
    "ts = tseries(x, 2048);"
    "ts = tseries(x, x);"
    "ts = tseries(x, 2048.0, 6000000.0);"
    "ts = tseries(x, 2048.0, 6000000, 1000.0);"
    "ts = tseries(x, 2048.0, 6000000, 1000, 1);"
    "ts = tseries(x, 2048.0, 6000000, 1000, 1.0, 1);"
}
    
set testname "TSERIESAPI"
set testnum "01"
set ifilepre "N200"

set dtypes { "R4" "R8" "C8" "C16" }
set testcases { A B C D E F G H I J }

;## repeat for each data type
foreach action $actions testcase $testcases {
  set ofilepre $testname$testnum$testcase

  foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    set ofile ldas_outgoing/mdc/output/$ofilepre$dtype.ilwd

    set cmd "
    ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data; }
        -algorithms {
            x = slice(idata,0,128,1);

            $action

            output(ts,_,_,ts,time series);
        }
    } "

    sendCmd $cmd
  }
}
