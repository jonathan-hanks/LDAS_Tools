#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: pr351.tcl,v 1.8 2004/01/14 20:38:20 Philip.Charlton Exp $

;## MIXERAPI01
;## mixer api tests
;## domain_error test

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set ifile /ldas_outgoing/mdc/input/N8388608R4.sim
set ofile /ldas_outgoing/mdc/output/MIXERAPI01A.ilwd

;## invalid_argument

set cmd "ldasJob { -name $user -password $pwrd -email $email }
{ conditionData
    -inputprotocol file:$ifile
    -returnprotocol file:$ofile
    -outputformat { ilwd ascii }
    -aliases {ch01 = mdc_input_data:chan_01:data}
    -algorithms {
        phi = value(0.0);
	f = value(0.125);
	z = mix(phi,f,ch01);
        output(z,_,_,z,mix);
    }
} "

sendCmd $cmd
