#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: waveletapi01d.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## WAVELETAPI01
;## wavelet api tests
;## WaveletForward exception: invalid argument #1
;## Command should throw an exception

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" }
set ifilepre "N200"
set ofilepre "WAVELETAPI01"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost D$post
    set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases { in = mdc_input_data:chan_01:data }
        -algorithms {
            x = getSequence( in );
            b = Biorthogonal( 12, 0 );
            y = WaveletForward( x, b, 2 );
            output(y,_,_,y,wavelet forward output);
        }
    } "

    sendCmd $cmd
    exec sleep 1
}
