#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: statapi01.tcl,v 1.8 2004/01/14 20:38:20 Philip.Charlton Exp $

;## STATAPI01
;## statistics api tests
;## test exception: statistics of zero length valarray

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set ofile "STATAPI01"
set methods { "min" "max" "mean" "rms" "variance" "skewness" "kurtosis" "all" }
set dtypes { "R4" "R8" "I2" }

foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/N200$dtype.sim

    foreach method $methods {

        set action ${method}(data)
        set ofile ldas_outgoing/mdc/output/STATAPI01$dtype$method.ilwd

        set cmd "ldasJob {-name $user -password $pwrd -email $email }
        { conditionData
            -inputprotocol file:/$ifile
            -returnprotocol file:/$ofile -outputformat { ilwd ascii }
            -aliases { idata = mdc_input_data:chan_01:data }
            -algorithms {
                data = slice(idata,1,0,1);
                ISize = size(data);
                output(ISize,_,_,ISize,Input data size);
                y = ${action};
                output(y,_,_,y,${method});
            }
        } "

        sendCmd $cmd
    }
}
