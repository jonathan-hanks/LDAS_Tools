#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

;## this test causes runtime job timed out in datacond.
namespace eval arithapi {
	
    set ifilepre N200
    set precs { float double }
	set types { "real(idata)" "complex(idata,idata)" }
	set ops { add sub mul div }

    set dtype R4
    set letter A
    set ifilepre N200
    set length 200
    set N [ expr $length - 1 ]
     
    set cmdstem { conditionData
      -subject $subject 
      -inputprotocol { file:/$ifile }
      -returnprotocol { file:/$ofile }
      -outputformat { ilwd ascii }
      -aliases { idata = mdc_input_data:chan_01:data; }
      -algorithms { $algo } }
      
   	set algo_arithapi01a {
        	lhs = ${lhs_type};
        	lhs = ${lhs_prec}(lhs);
        	lhs = slice(lhs, 0, ${N}, 1);
        	rhs = ${rhs_type};
        	rhs = ${rhs_prec}(rhs);
        	res = ${op}(lhs, rhs);
        	output(res, _, _, res, output of $procname);
    }
    set algo_arithapi02a {
        	lhs = ${lhs_type};
        	lhs = ${lhs_prec}(lhs);
        	rhs = ${rhs_type};
        	rhs = ${rhs_prec}(rhs);
        	res = ${op}(lhs, rhs);
    }
    
    namespace export arithapi*
    
	proc arithapiTest {} {
	
    	if	{ [ catch {
        	set name [ namespace current ]
			set procname [ info level -1 ]
            namespace import [ namespace parent $name ]::*

            set ifilepre 	[ namespace eval $name set ifilepre ]
            set ofilepre 	[ string toupper $procname ]
        	set cmdstem [ namespace eval $name set cmdstem ]
            set dtype [ namespace eval $name set dtype ]
            set letter [ namespace eval $name set letter ]
            set N 	[ namespace eval $name set N ]
            set precs  	[ namespace eval $name set precs ]
            set ops  	[ namespace eval $name set ops ]
            set types  	[ namespace eval $name set types ]
            set algo [ namespace eval $name set algo_${procname} ]
   
            set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
			set prefix $ofilepre$letter$dtype
			set ofile ldas_outgoing/mdc/output/$prefix.ilwd
			set efile ldas_outgoing/mdc/input/$prefix.exp
                       
			foreach lhs_prec $precs {
  				foreach rhs_prec $precs {
    				foreach lhs_type $types {
      					foreach rhs_type $types {
        					foreach op $ops {
                            	set desc "$lhs_prec:$rhs_prec:$lhs_type:$rhs_type:$op"
                                set algo [ subst $algo ]
                                if	{ [ string equal arithapi01a $procname ] } {
                                	set subject datacondMDC:conditionData:$procname:$desc
            						set cmd [ subst $cmdstem ]               
            						runTest $subject $cmd    
            						after 1000 
                                } else {
                                	append algostr "\n$algo"
                                }
                            }
                         }
                    }
             	}
            }
            if	{ [ string equal arithapi02a $procname ] } {
            	append algostr " 
                output(res, _, _, res, output of arithapi02a);"
            	set algo $algostr
                set subject "datacondMDC:conditionData:$procname:all"
            	set cmd [ subst $cmdstem ]
                runTest $subject $cmd
				after 1000
            }
    	} err ] } {
    		return -code error $err
		}
	}
    
	proc arithapi01a {} {
	
		if	{ [ catch {
			arithapiTest
		} err ] } {
			return -code error $err
		}
	}


	proc arithapi02a {} {

		if	{ [ catch { 
			arithapiTest
    	} err ] } {
			return -code error $err
		}
	}
}
namespace import arithapi::*
