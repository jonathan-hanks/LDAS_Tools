#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: siggenapi02a.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## SigGen
;##
;## test for signal generators action
;## 

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set dtypes { R4 }

set ifilepre N200
set ofilepre SIGGENAPI02
set testcase A

;## repeat for each data type
foreach dtype $dtypes {

    set prefix $ofilepre$testcase$dtype
    set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    set ofile ldas_outgoing/mdc/output/$prefix.ilwd
    set efile ldas_outgoing/mdc/input/$prefix.exp

    set cmd "
    ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data; }
        -algorithms {
            n1 = value(113);
            n2 = value(64);
            f1 = value(0.2);
            f2 = value(-0.1);
            phi1 = value(2.0);
            phi2 = value(-1.0);
            A1 = value(1.5);
            A2 = value(-0.5);
            x1 = value(3.5);
            x2 = value(-2.1);

            s1 = sine(n1, f1, phi1, A1, x1);
            s1 = sine(n2, f2, phi2, A2, x2);

            s2 = cosine(n1, f1, phi1, A1, x1);
            s2 = cosine(n2, f2, phi2, A2, x2);

            s3 = sawtooth(n1, f1, phi1, A1, x1);
            s3 = sawtooth(n2, f2, phi2, A2, x2);

            s4 = square(n1, f1, phi1, A1, x1);
            s4 = square(n2, f2, phi2, A2, x2);

            s5 = triangle(n1, f1, phi1, A1, x1);
            s5 = triangle(n2, f2, phi2, A2, x2);

            output(s1,_,_,_,signal 1);
            output(s2,_,_,_,signal 2);
            output(s3,_,_,_,signal 3);
            output(s4,_,_,_,signal 4);
            output(s5,_,_,_,signal 5);
        }
    } "

    sendCmd $cmd
}
