#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval signumapi {
    	
     set postfixes { "R4" "R8" }
	set cmdstem { conditionData
        -subject $subject
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile 
        -outputformat { ilwd ascii }
        -aliases { x = mdc_input_data:chan_01:data }
        -algorithms { $algo }
    	}
    	set ifilepre N200
     
     set algo_signumapi01a {
     	y = signum();
          output(y,_,_,y,signum output);
     }
     
     set algo_signumapi01b {
    		y = signum(x, 1.);
         	output(y,_,_,y,signum output);
     }
     
     set algo_signumapi01c {
     	y = signum( x, 1 );
          output(y,_,_,y,signum output);
     }
     
     set algo_signumapi01d {
     	y = signum( x, 1., 2 );
          output(y,_,_,y,signum output);
     }
     
    
     namespace export signumapi*
     
	proc signumapiTest { postfixes } {
     
     	if	{ [ catch {
         		set name [ namespace current ]
			set procname [ info level -1 ]
          	namespace import [ namespace parent $name ]::*  
                               
            	set cmdstem [ namespace eval $name set cmdstem ]
               set ifilepre [ namespace eval $name set ifilepre ]
            	set algo [ namespace eval $name set algo_$procname ]	
          	set ofilepre [ string toupper $procname ]
               
               foreach post $postfixes {
            		set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    				set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd                    
                	set subject datacondMDC:conditionData:$procname:$post
                	set algo [ subst $algo ]
                	set cmd [ subst $cmdstem ]
                	runTest $subject $cmd 
                    after 1000
              	}           
    		} err ] } {
    			return -code error $err
		} 	
	}
     
	proc signumapi01a {} {

		if	{ [ catch {
          	set postfixes { "R4" "R8" }
			signumapiTest $postfixes
    		} err ] } {
    			return -code error $err
		}  
	}

	proc signumapi01b {} {

		if	{ [ catch {
          	set postfixes { "C8" "C16" }
          	signumapiTest $postfixes
    		} err ] } {
    			return -code error $err
		}  	
	}


	proc signumapi01c {} {

		if	{ [ catch {
          	set postfixes { "R4" "R8" }
 			signumapiTest $postfixes
    		} err ] } {
    			return -code error $err
		} 
	}

	proc signumapi01d {} {

		if	{ [ catch {
          	set postfixes { "R4" "R8" }
			signumapiTest $postfixes
    		} err ] } {
    			return -code error $err
		} 
	}

	proc signumapi02a {} {

		if	{ [ catch {
          	set procname [ info level 0 ]
    			set ofilepre [ string toupper $procname ]
        		set ifile ldas_outgoing/jobs/mdc/input/N200R8.sim
			set ofile ldas_outgoing/mdc/output/$ofilepre.ilwd

			set test "
            m1 = mean( y );
            z = abs( y );
            m2 = mean( z );
            nz = sub( 1. , m2 );
            nz = mul( nz, 100.);
		"
		set subject datacondMDC:conditionData:$procname:all
		set cmd "conditionData
        -subject $subject
        -inputprotocol file:/$ifile
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -algorithms {
            d = ramp( 100, 1., -25. );
            d = tseries(d, 2048., 6100000);

            y = signum( d );
            $test
            output(m1,_,_,m1, expected value 0.49 );
            output(nz,_,_,nz, expected number of zeroes 1 );

            y = signum( d, -10.5 );
            $test
            output(m1,_,_,m1, expected value 0.70 );
            output(nz,_,_,nz, expected number of zeroes 0 );

            y = signum( d, 10., 20. );
            $test
            output(m1,_,_,m1, expected value 0.19 );
            output(nz,_,_,nz, expected number of zeroes 11 );
    		} "
			runTest $subject $cmd
    		} err ] } {
    			return -code error $err
		} 
	}
}
namespace import signumapi::*
