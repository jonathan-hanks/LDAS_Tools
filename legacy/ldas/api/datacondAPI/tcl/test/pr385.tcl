#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: pr385.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

;## PR385: intermediate() action fails when comment field contains an
;## equals (=) sign

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;## Build the user command string
set ssub ""
set Nlist [ list 1024 ]

foreach N $Nlist {

    set sub "
        x = slice(data, 0, $N, 1);
        fft(x);
        ti = user_time();
        fft(x); fft(x); fft(x); fft(x); fft(x);
        fft(x); fft(x); fft(x); fft(x); fft(x);
        tf = user_time();
        t = sub(tf, ti);
        time = div(t, 10.0);
        output(time,_,_,time,Time to fft data for length = ${N});"

    set ssub "$ssub\n$sub"
}

set ifilepre "N10"
set ofilepre "PR385"

;## First test set: correct values on sequence
;## for length 1024 and default fft length and overlap

set ifile ldas_outgoing/mdc/input/${ifilepre}R4.sim
set ppost AR4  ;## Need to change this for tests A, B, C
set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd

set cmd "ldasJob { -name $user -password $pwrd -email $email }
{ conditionData
    -inputprotocol file:/$ifile
    -returnprotocol file:/$ofile -outputformat { ilwd ascii }
    -aliases { data = mdc_input_data:chan_01:data }
    -algorithms {
        $ssub
    }
} "

sendCmd $cmd
