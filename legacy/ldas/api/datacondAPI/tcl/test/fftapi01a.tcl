#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: fftapi01a.tcl,v 1.8 2004/01/14 20:38:20 Philip.Charlton Exp $

;## FFTAPI01
;##
;## test that taking FFTs of a sequence with invalid length (N=0)
;## raises an exception

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set dtypes { R4 R8 C8 C16 }

set ifilepre N200
set ofilepre FFTAPI01
set letter A
set length 0

;## repeat for each data type
foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/$ifilepre${dtype}.sim
    set ofile ldas_outgoing/mdc/output/$ofilepre$letter$dtype.ilwd

    set cmd "
        ldasJob {-name $user -password $pwrd -email $email }
        { conditionData
            -inputprotocol file:/$ifile
            -returnprotocol file:/$ofile
            -outputformat { ilwd ascii }
            -aliases { idata = mdc_input_data:chan_01:data }
            -algorithms {
                x = slice(idata,0,${length},1);
                rx = real(x);
                n = size(rx);
                output(n,_,_,n,length of input data);
                y=fft(x);
                output(y,_,_,y,fft of input data);
            }
        } "

    sendCmd $cmd
}

