#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: statapi02.tcl,v 1.8 2004/01/14 20:38:20 Philip.Charlton Exp $

;## STATAPI02
;## Test correctness:

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set methods { "min" "max" "mean" "rms" "variance" "skewness" "kurtosis" "all" }
set dtypes { "R4" "R8" "I2" }

foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/N200$dtype.sim
    #set ifile ldas_outgoing/mdc/input/sample.ilwd
    set ofile ldas_outgoing/mdc/output/STATAPI02$dtype.ilwd

    set cmd "ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol file:/$ifile
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data }
        -algorithms {
            data = slice(idata,0,100,1);
            size = size(data);
            output(size,_,_,size,size);
            min = min(data);
            output(min,_,_,min,min);
            max = max(data);
            output(max,_,_,max,max);
            mean = mean(data);
            output(mean,_,_,mean,mean);
            rms = rms(data);
            output(rms,_,_,rms,rms);
            variance = variance(data);
            output(variance,_,_,variance,variance);
            skewness = skewness(data);
            output(skewness,_,_,skewness,skewness);
            kurtosis = kurtosis(data);
            output(kurtosis,_,_,kurtosis,kurtosis);
            all = all(data);
            output(all,_,_,all,all);
        }
    }"

    sendCmd $cmd
}
