#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: coherenceapi01a.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## COHERENCEAPI01A
;## coherence api tests
;## exception tests, zero length sequence
;## First test set: correct exception thrown
;## for length 0 and default fft length and overlap

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;##set postfixes { "C8" }
set postfixes { "R4" "R8" "C8" "C16" }

set ifilepre "N10" ;## Use data file of size 2^10 = 1024
set ofilepre "COHERENCEAPI01"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost A$post  ;## Need to change this for tests A, B, C
    set prefix $ofilepre$ppost
    set ofile ldas_outgoing/mdc/output/${prefix}.ilwd
    set efile ldas_outgoing/mdc/input/${prefix}.exp

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases {
            data=mdc_input_data:chan_01:data;
            freqexp=${prefix}:freqout:data;
            specexp=${prefix}:specout:data
        }
        -algorithms {
            x = slice(data, 0, 0, 1);
            spec = coherence(x, x);
            output(spec,_,_,spec,spectrum data);
        }
    } "

    sendCmd $cmd
}
