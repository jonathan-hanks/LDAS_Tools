#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: pr452.tcl,v 1.8 2004/01/14 20:38:20 Philip.Charlton Exp $

;## PR452
;##
;## test memory leak

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set dtypes { C16 }

set ifilepre N19
set ofilepre PR452
set letter C
set length 524288
set index  4194304

;## repeat for each data type
foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/$ifilepre${dtype}.sim
    set ofile ldas_outgoing/mdc/output/$ofilepre$letter$dtype.ilwd

    set cmd "ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol file:/$ifile
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data }
        -algorithms {
            x1 = slice(idata,0,${length},1);
            x2 = slice(idata,0,${length},1);
            x3 = slice(idata,0,${length},1);
            x4 = slice(idata,0,${length},1);
            x5 = slice(idata,0,${length},1);
            x6 = slice(idata,0,${length},1);
            x7 = slice(idata,0,${length},1);
            x8 = slice(idata,0,${length},1);
            x9 = slice(idata,0,${length},1);
            x10 = slice(idata,0,${length},1);
            x11 = slice(idata,0,${length},1);
            x12 = slice(idata,0,${length},1);
            x13 = slice(idata,0,${length},1);
            x14 = slice(idata,0,${length},1);
            x15 = slice(idata,0,${length},1);
            x16 = slice(idata,0,${length},1);
            output(x16,_,_,x,output);
        }
    } "

    sendCmd $cmd
}
