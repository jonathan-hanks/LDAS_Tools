#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: psdapi01c.tcl,v 1.10 2004/01/14 20:38:20 Philip.Charlton Exp $

;## PSDAPI01C
;## psd api tests
;## exception tests, FFT length = 1024, overlap = 1024
;## First test set: correct exception thrown
;## for length 0 and default fft length and overlap

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

;##set postfixes { "C8" }
set postfixes { "R4" "R8" "C8" "C16" }

set ifilepre "N10" ;## Use data file of size 2^10 = 1024
set ofilepre "PSDAPI01"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost C$post  ;## Need to change this for tests A, B, C
    set prefix $ofilepre$ppost
    set ofile ldas_outgoing/mdc/output/${prefix}.ilwd
    set efile ldas_outgoing/mdc/input/${prefix}.exp

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases {
            data=mdc_input_data:chan_01:data;
            freqexp=${prefix}:freqout:data;
            specexp=${prefix}:specout:data
        }
        -algorithms {
            x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);

            rx = real(x);
            n = size(rx);
            output(n,_,_,size,size of input data);

            spec = psd(x, 1024, _, 1024);
            output(spec,_,_,spec,spectrum data);
        }
    } "

    sendCmd $cmd
}
