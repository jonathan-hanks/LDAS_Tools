#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: resampleapi02e.tcl,v 1.2 2004/01/14 20:38:20 Philip.Charlton Exp $

;## RESAMPLEAPI02
;##
;## resample api tests
;## correct downsampling on broken sequence, using a generated sine wave
;## to verify

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" }

;## Need some dummy data
set ifilepre "N200"
set ofilepre "RESAMPLEAPI02"
set idir ldas_outgoing/mdc/input
set odir ldas_outgoing/mdc/output

;## Input params
set p 1
set q 2
set n 16384
set m [ expr $n/2 ]
set srate 16384.0
set dt [ expr 1.0/$srate ]
set Nyq [ expr $srate/2.0 ]
set freq [ expr 0.125*$Nyq ]
set f [ expr $freq*$dt ]

;## Output values
set n2 [ expr $n*$p/$q ]
set srate2 [ expr $srate*$p/$q ]
set dt2 [ expr 1.0/$srate2 ]
set Nyq2 [ expr $srate2/2.0 ]
set f2 [ expr $freq*$dt2 ]

foreach post $postfixes {
    set ifile ${idir}/${ifilepre}${post}.sim
    set prefix ${ofilepre}E${post}
    set ofile ${odir}/${prefix}.ilwd

    set cmd " ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/${ifile} }
        -returnprotocol file:/${ofile}
        -outputformat { ilwd ascii }
        -aliases { raw = mdc_input_data:chan_01:data; }
        -algorithms {
            xraw = sine($n, $f);
            x = tseries(xraw, $srate, 10000);

            x1 = slice(x, 0, $m, 1);
            x2 = slice(x, $m, $m, 1);

            x1 = resample(x1, $p, $q, z);
            x2 = resample(x2, z);

            x = concat(x1, x2);

            start = sub($n2, 100);
            x = slice(x, start, 100, 1);

            d = getResampleDelay(z);
            d = integer(d);

            y = sine($n2, $f2);
            start = sub($n2, d);
            start = sub(start, 100);
            y = slice(y, start, 100, 1);

            diff = sub(x, y);
            diff = abs(diff);
            diff = max(diff);

            output(diff, _, _, diff, Max diff between resampled and actual);
        }
    } "

    sendCmd $cmd
}

exit 
