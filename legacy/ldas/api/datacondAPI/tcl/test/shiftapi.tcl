#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval shiftapi {
  	
  	set ifilepre "N200"
        
  	set postfixes { "R4" "R8" "C8" "C16" }
	
	set cmdstem { conditionData 
		-subject $subject
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -aliases {
          srate = mdc_input_data:chan_01:rate;
          data = mdc_input_data:chan_01:data;
          len = mdc_input_data:chan_01:length;
        }
        -algorithms {

          x = tseries(data, 1.0, 610000000, 0);
          $algo
          output(y,_,_,y,Shifted output);

        }
    	}
     
     namespace export shiftapi*
     
	proc shiftapiTest { testcase algo } {

		if	{ [ catch {
        	set name [ namespace current ]
			set procname [ info level -1 ]
          	namespace import [ namespace parent $name ]::*  
               
            set ifilepre [ namespace eval $name set ifilepre ]
          	set cmdstem [ namespace eval $name set cmdstem ]
          	set postfixes [ namespace eval $name set postfixes ]
			set procname [ info level -1 ]
        
  			set ofilepre $procname$testcase

  			foreach post $postfixes {

    				set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    				set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd
					set subject datacondMDC:conditionData:$procname:$testcase:$post
    				set cmd [ subst $cmdstem ]
        			runTest $subject $cmd 
        			after 1000
               }
    		} err ] } {
    			return -code error $err
		}  
	}

	proc shiftapi01a {} {

	if	{ [ catch {
    	
    	;## Too few arguments
		set algo "
          offset = value(0.1);
          order = value(2);
          y = shift();
		"
		shiftapiTest A $algo

		;## Too few arguments
		set algo "
          offset = value(0.1);
          order = value(2);
          y = shift(x);
		"
		shiftapiTest B $algo

		;## Too many arguments
		set algo "
          offset = value(0.1);
          order = value(2);
          x = slice(x, 0, 0, 1);
          y = shift(x, offset, order, z, x);
		"
		shiftapiTest C $algo

		;## Zero-length data
		set algo "
          offset = value(0.1);
          order = value(2);
          x = slice(x, 0, 0, 1);
          y = shift(x, offset, order);
		"
		shiftapiTest D $algo

		;## Wrong data type
		set algo "
          offset = value(0.1);
          order = value(2);
          x = value(1.0);
          y = shift(x, offset, order);
		"
		shiftapiTest E $algo

		;## Wrong data type for offset
		set algo "
          offset = value(-0.1);
          order = value(2);
          y = shift(x, x, order);
		"
		shiftapiTest F $algo

		;## Negative offset
		set algo "
          offset = value(-0.1);
          order = value(2);
          y = shift(x, offset, order);
		"
		shiftapiTest G $algo

		;## Negative order
		set algo "
	      offset = value(0.1);
          order = value(-2);
          y = shift(x, offset, order);
		"
		shiftapiTest H $algo

		;## Wrong data type for order
		set algo "
          offset = value(0.1);
          order = value(1.0);
          y = shift(x, offset, order);
		"
		shiftapiTest I $algo


		;## Bad continuity for state
		set algo "
          offset = value(0.1);
          order = value(1.0);
          y = shift(x, offset, order, z);
          y = shift(x, z);
		"
		shiftapiTest J $algo

		;## Bad data type for state
		set algo "
          offset = value(0.1);
          order = value(1.0);
          y = shift(x, offset, order, z);
          r = resample(x, 1, 2, z);
          y = shift(x, z);
		"
		shiftapiTest	K $algo

    	} err ] } {
    		return -code error $err
	}  
	}

	proc shiftapi02a {} {

	if	{ [ catch {
		set name [ namespace current ]
          namespace import [ namespace parent $name ]::*  
		set procname [ info level 0 ]
        
		set resample_ratios { { 1 8 } { 3 2 } { 5 16 } }
		set testcases { A B C }
          set ifilepre [ namespace eval $name set ifilepre ]
          set postfixes [ namespace eval $name set postfixes ]
          
          set testname "SHIFTAPI"
		set testnum "02"
		foreach resample_ratio $resample_ratios testcase $testcases {
  			set ofilepre $testname$testnum$testcase
  			foreach post $postfixes {
    			set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    			set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd
    			set p [ lindex $resample_ratio 0 ]
    			set q [ lindex $resample_ratio 1 ]
               ;## no blanks in subject
			set subject "datacondMDC:conditionData:$procname:resample([ join $resample_ratio :]):$testcase:$post"
                
    			set cmd "conditionData 
        -subject $subject
        -inputprotocol { file:/$ifile }
        -returnprotocol { file:/$ofile }
        -outputformat { ilwd ascii }
        -algorithms {

          dat = sine(8192, 0.01);
          xpre = tseries(dat, 16384.0, 6100000, 0);
          x = resample(xpre, $p, $q, 99, 15.0, z);

          offset = getResampleDelay(z);
          order = value(2);

          # Break up the input and shift each separately, then rejoin them
          len0 = size(x);
          len0 = div(len0, 2);

          x0 = slice(x, 0, len0, 1);
          x1 = slice(x, len0, len0, 1);

          y0 = shift(x0, offset, order, z);
          y1 = shift(x1, z);
          clear(z);

          y_conc = concat(y0, y1);

          # Now do the output in one step
          y = shift(x, offset, order);

          # Find the max absolute difference
          maxdiff = sub(y, y_conc);
          maxdiff = abs(maxdiff);
          maxdiff = max(maxdiff);
          
          output(maxdiff, _, _, maxdiff, max difference of broken vs continuous shift-should be small);
      	}"
			runTest $subject $cmd
            	after 1000
			}
    		}
    	} err ] } {
    		return -code error $err
	}  
	}

	proc shiftapi02b {} {

	if	{ [ catch {
		set testname "SHIFTAPI"
		set testnum "02"

		set shift_wholes { 0 1 2 3 }
		set testcases { D E F G }
		set alpha 0.1
          
		set procname [ info level 0 ]
        	set name [ namespace current ]
          namespace import [ namespace parent $name ]::*  
       
		set resample_ratios { { 1 8 } { 3 2 } { 5 16 } }
		set testcases { A B C }
          set ifilepre [ namespace eval $name set ifilepre ]
          set postfixes [ namespace eval $name set postfixes ]
          
		foreach shift_whole $shift_wholes testcase $testcases {
  			set ofilepre $testname$testnum$testcase

  			foreach post $postfixes {
    			set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    			set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd
			set subject "datacondMDC:conditionData:$procname:shift_whole${shift_whole}:$testcase:$post"
                
    			set cmd "conditionData 
        		-subject $subject
       	 	-inputprotocol { file:/$ifile }
        		-returnprotocol { file:/$ofile }
        		-outputformat { ilwd ascii }
        		-aliases {
          		srate = mdc_input_data:chan_01:rate;
          		data = mdc_input_data:chan_01:data;
          		len = mdc_input_data:chan_01:length;
        		}
        		-algorithms {

          		x = tseries(data, 1.0, 6100000, 0);

          		# Shift
          		offset = add($shift_whole, $alpha);
         		 	order = value(2);

          		len0 = div(len, 2);

          		# Break up the input and shift each separately, then rejoin
          		x0 = slice(x, 0, len0, 1);
         		 	x1 = slice(x, len0, len0, 1);

          		y0 = shift(x0, offset, order, z);
          		y1 = shift(x1, z);
          		y_conc = concat(y0, y1);

          		clear(z);

          		# Now do the output in one step and find the difference
          		y = shift(x, offset, order);
          		maxdiff = sub(y, y_conc);
          		maxdiff = abs(maxdiff);
          		maxdiff = max(maxdiff);
          
          		output(maxdiff, _, _, maxdiff, max difference of broken vs continuous shift-should be small);
    			}"
        		runTest $subject $cmd
               after 1000
			}
    		}
    	} err ] } {
    	return -code error $err
	}  
	}
}

namespace import shiftapi::*
