#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: pr321.tcl,v 1.7 2004/01/14 20:38:20 Philip.Charlton Exp $

;## test showing problem with statistics not throwing exceptions
;## problem report 321

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set dtypes { "C8" "C16" }

;## values

foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/N200$dtype.sim
    set ofile ldas_outgoing/mdc/output/pr321$dtype.ilwd

    set cmd "ldasJob {-name $user -password $pwrd -email $email }
    { conditionData -inputprotocol file:/$ifile
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data }
        -algorithms {
            data = slice(idata,0,5,1);
            size = size(data);
            size(size,_,_,size,size);
            min = min(data);
            output(min,_,_,min,min);
            max = max(data);
            output(max,_,_,max,max);
            mean = mean(data);
            output(mean,_,_,mean,mean);
            variance = variance(data);
            output(variance,_,_,variance,variance);
            rms = rms(data);
            output(rms,_,_,rms,rms);
            skewness = skewness(data);
            output(skewness,_,_,skewness,skewness);
            kurtosis = kurtosis(data);
            output(kurtosis,_,_,kurtosis,kurtosis);
            all = all(data);
            output(all,_,_,all,all);
        }
    }"

    sendCmd $cmd
}
