#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval psdapi {

	set postfixes { "R4" "R8" "C8" "C16" }
    
    set cmdstem { conditionData
    	-subject $subject 
        -inputprotocol { file:/$ifile $efile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases { $aliases }
        -algorithms { $algo } 
        }
        
    set aliases_psdapi01a {
        	data=mdc_input_data:chan_01:data;
            freqexp=${prefix}:freqout:data;
            specexp=${prefix}:specout:data
        }
        
    set aliases_psdapi01b $aliases_psdapi01a
    set aliases_psdapi01c $aliases_psdapi01a
    set aliases_psdapi01d $aliases_psdapi01a
        
   	set aliases_psdapi02a {
        	data=mdc_input_data:chan_01:data;
            specexp=${prefix}:specout:data;
        
   	}
        
    set aliases_psdapi02b $aliases_psdapi02a
    set aliases_psdapi02c $aliases_psdapi02a
        
    set aliases_psdapi02d {
        	idata = mdc_input_data:chan_01:data;
            espec = $prefix:specout:data;
    }
    
    set length 8192
    set fftlength 2048
    set overlaplength 1024
    
    set algo_psdapi01a {        	
            x = slice(data, 0, 0, 1);
            rx = real(x);
            n = size(rx);
            output(n,_,_,size,{size of input data});
            spec = psd(x);
            output(spec,_,_,spec,spectrum data);
    }
        
    set algo_psdapi01b {
        	x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);
            rx = real(x);
            n = size(rx);
            output(n,_,_,size,size of input data);
            spec = psd(x, 0);
            output(spec,_,_,spec,spectrum data);
    }
        
    set algo_psdapi01c {
            x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);
            rx = real(x);
            n = size(rx);
            output(n,_,_,size,size of input data);
            spec = psd(x, 1024, _, 1024);
            output(spec,_,_,spec,spectrum data);
   	}
        
    set algo_psdapi01d {
            x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);
            rx = real(x);
            n = size(rx);
            output(n,_,_,size,size of input data);
            spec = psd(x, 1024, _, 1025);
            output(spec,_,_,spec,spectrum data);
    }

        
    set algo_psdapi02a {
        	x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);
	        spec = psd(x);
            output(spec,_,_,spec,spectrum data);
            output(specexp,_,_,specexp,expected spectra);
            specdif = sub(spec, specexp);
            output(specdif,_,_,specdif,diff of spec and specexp);
            specmin = min(specdif);
            output(specmin,_,_,specmin,minimum of difference);
            specmax = max(specdif);
            output(specmax,_,_,specmax,maximum of difference);
            specrms = rms(specexp);
            output(specrms,_,_,specrms,rms of expected spectra);
    }
	
    set algo_psdapi02b {    		
            x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);
            spec = psd(x, 256, _, 0);
            output(spec,_,_,spec,spectrum data);
            output(specexp,_,_,specexp,expected spectra);
            specdif = sub(spec, specexp);
            output(specdif,_,_,specdif,diff of spec and specexp);
            specmin = min(specdif);
            output(specmin,_,_,specmin,minimum of difference);
            specmax = max(specdif);
            output(specmax,_,_,specmax,maximum of difference);
            specrms = rms(specexp);
            output(specrms,_,_,specrms,rms of expected spectra);
    }
    
    set algo_psdapi02c {
    		x = slice(data, 0, 1024, 1);
            output(x,_,_,data,slice of input data);
            spec = psd(x, 256, _, 128, _);
            output(spec,_,_,spec,spectrum data);
            output(specexp,_,_,specexp,expected spectra);
            specdif = sub(spec, specexp);
            output(specdif,_,_,specdif,diff of spec and specexp);
            specmin = min(specdif);
            output(specmin,_,_,specmin,minimum of difference);
            specmax = max(specdif);
            output(specmax,_,_,specmax,maximum of difference);
            specrms = rms(specexp);
            output(specrms,_,_,specrms,rms of expected result);
    }
        
    set algo_psdapi02d {
            x = slice(idata,0,${length},1);
            rx = real(x);
            n1 = size(rx);
            output(n1,_,_,n1,length of input data before psd);
            spec = psd(x,${fftlength},_,${overlaplength},_);
            difspec = sub(spec,espec);
            minspec = min(difspec);
            output(minspec,_,_,minspec,minimum of difference for spectrum);
            maxspec = max(difspec);
            output(maxspec,_,_,maxspec,maximum of difference for spectrum);
            rmsspec = rms(spec);
            output(rmsspec,_,_,rmsspec,rms of spectrum);
            output(spec,_,_,spec,computed values of spectrum);
   	}
	namespace export psdapi*
    
	proc psdapiTest { ifilepre code { needefile 0 } } {
    
    	if	{ [ catch {
        	set name [ namespace current ]
			set procname [ info level -1 ]
            namespace import [ namespace parent $name ]::*  
                               
            set cmdstem [ namespace eval $name set cmdstem ]
            set postfixes [ namespace eval $name set postfixes ]
            set algo [ namespace eval $name set algo_$procname ]
         
           	set ofilepre [ string toupper $procname ]
            if	{ [ string match psdapi02d $procname ] } {
            	set postfixes [ lrange $postfixes 2 end ]
            }
            
            set length [ namespace eval $name set length ]
            set fftlength [ namespace eval $name set fftlength ]
           	set overlaplength [ namespace eval $name set overlaplength ]

			foreach post $postfixes {
            	set prefix ${ofilepre}$post
                set aliases [ namespace eval $name set aliases_$procname ]
                set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
                set ofile ldas_outgoing/mdc/output/${prefix}.ilwd
				if	{ $needefile } {
    				set efile file:/ldas_outgoing/mdc/input/$prefix.exp
            	} else { 
                    set efile ""		
            	}
                set subject datacondMDC:conditionData:$procname:$post
                set aliases [ subst $aliases ]
                set algo [ subst $algo ]
                set cmd [ subst $cmdstem ]
                runTest $subject $cmd            
    		}
    	} err ] } {
    		return -code error $err
		}
	}
    
	proc psdapi01a {} {

		if	{ [ catch {
			set ifilepre "N10" ;## Use data file of size 2^10 = 1024              
			psdapiTest $ifilepre A 
		} err ] } {
    		return -code error $err
		}
	}

	proc psdapi01b {} {

		if	{ [ catch {
			set ifilepre "N10" ;## Use data file of size 2^10 = 1024
			psdapiTest $ifilepre B 
		} err ] } {
    		return -code error $err
		}    
	}

	proc psdapi01c {} {
	
    	if	{ [ catch {
			set ifilepre "N10" ;## Use data file of size 2^10 = 1024
        	psdapiTest $ifilepre C
    	} err ] } {
    		return -code error $err
		}   
	}
	
	proc psdapi01d {} {

		if	{ [ catch {
			set ifilepre "N10" ;## Use data file of size 2^10 = 1024
        	psdapiTest $ifilepre D
    	} err ] } {
    		return -code error $err
		}   
	}

	proc psdapi02a {} {

		if	{ [ catch {
			set ifilepre "N13" ;## Use data file of size 2^13 = 8192
			psdapiTest $ifilepre A 1 
    	} err ] } {
    		return -code error $err
		}   
	}

	proc psdapi02b {} {

		if	{ [ catch {
;## PSDAPI02B
;## psd api tests
;## First test set: correct values on sequence
;## for length 1024 and fft length = 256 and overlap = 0

			set ifilepre "N13" ;## Use data file of size 2^13 = 8192
			psdapiTest $ifilepre B 1
    	} err ] } {
    		return -code error $err
		}   
	}

	proc psdapi02c {} {

		if	{ [ catch { 
			set ifilepre "N13" ;## Use data file of size 2^13 = 8192
			psdapiTest $ifilepre C 1
    	} err ] } {
    		return -code error $err
		}   
	}  

	proc psdapi02d {} {

		if	{ [ catch {
			set ifilepre PSDAPI02D
			psdapiTest $ifilepre D 1
    	} err ] } {
    		return -code error $err
		}   
	}  
}

namespace import psdapi::*
 
