#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./datacondMDC.tcl --site dev -debug 0 -verbose lpse --enable-globus 0 --enable-gsi 0 --tests "fftapi01a" --qa-debug-level 1 --clearEmails 1
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval statapi {

	set methods { "min" "max" "mean" "rms" "variance" "skewness" "kurtosis" "all" }
	set dtypes { "R4" "R8" "I2" }
     
     namespace export statapi*
     
	proc statapi01 {} {

	if	{ [ catch {
    	
		set procname [ info level 0 ]
        	set ofilepre [ string toupper $procname ]
          set name [ namespace current ]
          namespace import [ namespace parent $name ]::*  
		set dtypes [ namespace eval $name set dtypes ]
          set methods [ namespace eval $name set methods ]
          
		foreach dtype $dtypes {

    			set ifile ldas_outgoing/mdc/input/N200$dtype.sim

    			foreach method $methods {
        			set action ${method}(data)
        			set ofile ldas_outgoing/mdc/output/$ofilepre$dtype$method.ilwd
				set subject datacondMDC:conditionData:$procname:$dtype:$method
        			set cmd "conditionData
            -subject $subject
            -inputprotocol file:/$ifile
            -returnprotocol file:/$ofile -outputformat { ilwd ascii }
            -aliases { idata = mdc_input_data:chan_01:data }
            -algorithms {
                data = slice(idata,1,0,1);
                ISize = size(data);
                output(ISize,_,_,ISize,Input data size);
                y = ${action};
                output(y,_,_,y,${method});
        		} "
        			runTest $subject $cmd
            		after 1000
    			}
        	}
    	} err ] } {
    		return -code error $err
	}  
	}

	proc statapi02 {} {

	if	{ [ catch {

		set procname [ info level 0 ]
        	set ofilepre [ string toupper $procname ]
          set name [ namespace current ]
         	set dtypes [ namespace eval $name set dtypes ]
          set methods [ namespace eval $name set methods ]
          
		foreach dtype $dtypes {

    		set ifile ldas_outgoing/mdc/input/N200$dtype.sim
    		#set ifile ldas_outgoing/mdc/input/sample.ilwd
    		set ofile ldas_outgoing/mdc/output/$ofilepre$dtype.ilwd
		set subject datacondMDC:conditionData:$procname:$dtype:all_methods
        
    		set cmd "conditionData
        -subject $subject
        -inputprotocol file:/$ifile
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data }
        -algorithms {
            data = slice(idata,0,100,1);
            size = size(data);
            output(size,_,_,size,size);
            min = min(data);
            output(min,_,_,min,min);
            max = max(data);
            output(max,_,_,max,max);
            mean = mean(data);
            output(mean,_,_,mean,mean);
            rms = rms(data);
            output(rms,_,_,rms,rms);
            variance = variance(data);
            output(variance,_,_,variance,variance);
            skewness = skewness(data);
            output(skewness,_,_,skewness,skewness);
            kurtosis = kurtosis(data);
            output(kurtosis,_,_,kurtosis,kurtosis);
            all = all(data);
            output(all,_,_,all,all);
    	}"
        runTest $subject $cmd
        after 1000
        	}
    	} err ] } {
    		return -code error $err
	}  
	}
}
namespace import statapi::*
