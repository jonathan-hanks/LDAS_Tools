#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: fftapi02b.tcl,v 1.9 2004/01/14 20:38:20 Philip.Charlton Exp $

;## FFTAPI02
;##
;## test that FFT and IFFT return correct results for a valid sequence
;## of length N=1024

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set dtypes { R4 R8 C8 C16 }

set ifilepre N10
set ofilepre FFTAPI02
set letter B
set length 1024

;## repeat for each data type
foreach dtype $dtypes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
    set prefix $ofilepre$letter$dtype
    set ofile ldas_outgoing/mdc/output/$prefix.ilwd
    set efile ldas_outgoing/mdc/input/$prefix.exp

    set cmd "
    ldasJob {-name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile file:/$efile }
        -returnprotocol file:/$ofile
        -outputformat { ilwd ascii }
        -aliases { idata = mdc_input_data:chan_01:data; exp = $prefix:fftout:data }
        -algorithms {
            x = slice(idata,0,${length},1);
            n1 = size(x);
            output(n1,_,_,n1,length of input data before FFT);
            y = fft(x);

            dif1 = sub(y, exp);
            dif1 = abs(dif1);
            max1 = max(dif1);
            output(max1,_,_,max1,maximum of abs difference for FFT);
            
            # if max1 > tol, throw error
            sgn = sub(1.0, max1);
            sgn = floor(sgn);
            sgn = integer(sgn);
            slice(x, sgn, 1, 1);

            # Verify Parseval 
            xrms = abs(x);
            xrms = mul(xrms, xrms);
            xrms = sum(xrms);
            xrms = sqrt(xrms);

            yrms = abs(y);
            yrms = mul(yrms, yrms);
            yrms = sum(yrms);
            yrms = div(yrms, n1);
            yrms = sqrt(yrms);

            dif1 = sub(xrms, yrms);
            output(dif1,_,_,dif1,diff of rms of FFT output);

            # Verify IFFT matches x
            z = ifft(y);
            n2 = size(z);

            dif2 = sub(z,x);
            dif2 = abs(dif2);
            max2 = max(dif2);
            output(max2,_,_,max2,maximum of abs difference for IFFT);

            # if max2 > tol, throw error
            sgn = sub(1.0e-1, max2);
            sgn = floor(sgn);
            sgn = integer(sgn);
            slice(x, sgn, 1, 1);

            output(n2,_,_,n2,length of output data after IFFT);
        }
    } "

    sendCmd $cmd
}
