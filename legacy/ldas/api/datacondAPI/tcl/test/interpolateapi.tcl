#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval interpolateapi {

	namespace export interpolateapi*
    
	proc interpolateapi02a {} {

	if	{ [ catch {
    	set name [ namespace current ]
		set procname [ info level 0]
        namespace import [ namespace parent $name ]::*    
        
		set ifilepre "N200" ;## dummy data
		set procname [ info level 0 ]
        
		set postfixes { "R4" "R8" "C8" "C16" }

		set ofilepre [ string toupper $procname ]

		foreach post $postfixes {

    		set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    		set ofile ldas_outgoing/mdc/output/$ofilepre$post.ilwd
			set subject datacondMDC:conditionData:$procname:$post:OK
            
   	 		set cmd "conditionData 
            -subject $subject
        	-inputprotocol { file:/$ifile }
        	-returnprotocol { file:/$ofile }
        	-outputformat { ilwd ascii }
        	-aliases {
          		srate = mdc_input_data:chan_01:rate;
          		data = mdc_input_data:chan_01:data;
          		len = mdc_input_data:chan_01:length;
        	}
        	-algorithms {

          	x = tseries(data, 16384.0, 6100000, 0);

          	alpha = value(0.1);
          	order = value(2);

          # Break up the input and interpolate each separately
          rx = real(x);
          len0 = size(rx);
          len0 = div(len0, 2);

          x0 = slice(x, 0, len0, 1);
          x1 = slice(x, len0, len0, 1);

          y0 = interpolate(x0, alpha, order, z);
          y1 = interpolate(x1, z);

          clear(z);

          # Now do the output in one step
          y = interpolate(x, alpha, order);

          # Break up the output
          ry0 = real(y0);
          iy0 = imag(y0);
          sizey0 = size(ry0);

          ry1 = real(y1);
          iy1 = imag(y1);
          sizey1 = size(ry1);

          y0ref = slice(y, 0, sizey0, 1);
          y1ref = slice(y, sizey0, sizey1, 1);

          ry0ref = real(y0ref);
          iy0ref = imag(y0ref);

          ry1ref = real(y1ref);
          iy1ref = imag(y1ref);

          a0 = sub(ry0ref, ry0);
          a0 = mul(a0, a0);

          a1 = sub(ry1ref, ry1);
          a1 = mul(a1, a1);

          b0 = sub(iy0ref, iy0);
          b0 = mul(b0, b0);
          b1 = sub(iy1ref, iy1);
          b1 = mul(b1, b1);

          z0 = add(a0, b0);
          z0 = sqrt(z0);

          z1 = add(a1, b1);
          z1 = sqrt(z1);

          max0 = max(z0);
          max1 = max(z1);
          
          output(x,_,_,xpre,raw data);
          output(y,_,_,y,interpolated data);

          output(max0,_,_,max0,max difference of broken vs continuous interpolate-should be small);
          output(max1,_,_,max1,max difference of broken vs continuous interpolate-should be small);

    }"
	runTest $subject $cmd
	}            
    } err ] } {
    	return -code error $err
	}
	}

}
namespace import interpolateapi::*
