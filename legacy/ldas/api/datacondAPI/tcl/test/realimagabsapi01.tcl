#!/bin/sh
# \
exec ${TCLSH:=tclsh} "$0" "$@" 

;## $Id: realimagabsapi01.tcl,v 1.8 2004/01/14 20:38:20 Philip.Charlton Exp $

;## REALIMAGABSAPI01
;## real, imag and abs action tests
;## First test set: correct values on sequence

proc sendCmd {cmd} {
    regsub -all -- {[\n\s\t]+} $cmd { } cmd
    set sid [ socket $::host $::port ]
    puts $sid $cmd
    flush $sid
    puts [ read $sid ]
    close $sid
}

;## Default settings
set user ""
set pwrd ""
set email ""
set host "ldas-dev.ligo.caltech.edu"
set port 10001

;## Source resource file
set rcfile "~/.datacondAPI.rc"
catch {source $rcfile} err

;## Process command-line arguments
set opt [list user pwrd email]
for {set idx 0} {$idx < $::argc} {incr idx} {
    if {$idx >= [llength $opt]} {break}
    set [lindex $opt $idx] [lindex $::argv $idx]
}

set postfixes { "R4" "R8" "C8" "C16" }
set ifilepre "N8388608"
set ofilepre "REALIMAGABSAPI01"

foreach post $postfixes {

    set ifile ldas_outgoing/mdc/input/$ifilepre$post.sim
    set ppost A$post
    set ofile ldas_outgoing/mdc/output/$ofilepre$ppost.ilwd

    set cmd "ldasJob { -name $user -password $pwrd -email $email }
    { conditionData
        -inputprotocol { file:/$ifile }
        -returnprotocol file:/$ofile -outputformat { ilwd ascii }
        -aliases { raw = mdc_input_data:chan_01:data }
        -algorithms {

            minusInt = sub(0,1);
            minusDouble = sub(0.0,1.0);

            ri = real(minusInt);
            output(ri,_,_,ri,{real(-1)});
            ii = imag(minusInt);
            output(ii,_,_,ii,{imag(-1)});
            ai = abs(minusInt);
            output(ai,_,_,ai,{abs(-1)});

            rd = real(minusDouble);
            output(rd,_,_,rd,{real(-1.0)});
            id = imag(minusDouble);
            output(id,_,_,id,{imag(-1.0)});
            ad = abs(minusDouble);
            output(ad,_,_,ad,{abs(-1.0)});

            v = slice(raw,0,10,1);
            output(v,_,_,v,valarray);
            rv = real(v);
            output(rv,_,_,rv,{real(valarray)});
            iv = imag(v);
            output(iv,_,_,iv,{imag(valarray)});
            av = abs(v);
            output(av,_,_,av,{abs(valarray)});

            f = fft(v);
            output(f,_,_,f,fvalarray);
            rf = real(f);
            output(rf,_,_,rf,{real(fvalarray)});
            if = imag(f);
            output(if,_,_,if,{imag(fvalarray)});
            af = abs(f);
            output(af,_,_,af,{abs(fvalarray)});

        }
    } "

    sendCmd $cmd
}
