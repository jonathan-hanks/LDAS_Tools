#!/bin/sh
#\
. /ldas/libexec/setup_tclsh.sh
# \
exec  ${TCLSH} "$0" ${1+"$@"}

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# These are tests for descMetaData user command which retrieves
# data from system tables
#
#
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%	
#------------------------------------------------------------------------
# Make sure everything will be found
#------------------------------------------------------------------------
set ::auto_path "/ldas/libexec /ldas/lib/test /ldas/lib/LJrun $auto_path"

#========================================================================
# Need to be very clever here. If there is a pkgIndex.tcl file in the
#   same directory from where this command started, then need to
#   add the directory to the autopath to pick up some of the other
#   packages
# example of running with -match
#./dbException.tcl --site dev -debug 0 -verbose lpse --enable-globus 0
# --enable-gsi 0 --database dev_1 -match "*dbquality*channel*"
# 
# cannot run with globus for now as this runs on metaserver which does not
# have an ldas service cert
#
#========================================================================

if [file exists [file join [file dirname $argv0] pkgIndex.tcl] ] {
    set ::auto_path "[file dirname $argv0] $::auto_path"
}

namespace eval complexapi {
	
   	set ifilepre N200
	set dtype R4
	set length 200
	set N [ expr $length - 1 ]
    set precs { float double }
    
    set cmdstem {
  	conditionData
    -subject $subject
    -inputprotocol { file:/$ifile }
    -returnprotocol { file:/$ofile }
    -outputformat { ilwd ascii }
    -aliases { idata = mdc_input_data:chan_01:data; }
    -algorithms { $algo }
    }
    
	namespace export complexapi*
    
    set algo_complexapi01a {
        lhs = ${prec}(idata);
        lhs = slice(lhs, 0, ${N}, 1);
        rhs = ${prec}(idata);
        res = complex(lhs, rhs);
        output(res, _, _, res, output of $procname);
	}
    
    set algo_complexapi01b {
        lhs = ${prec}(idata);
        lhs = slice(lhs, 0, 0, 1);
        rhs = ${prec}(idata);
        res = complex(lhs, rhs);
        output(res, _, _, res, output of $procname);
    }
    set algo_complexapi01b {
        lhs = ${prec}(idata);
        lhs = slice(lhs, 0, 0, 1);
        rhs = ${prec}(idata);
        res = complex(lhs, rhs);
        output(res, _, _, res, output of $procname);
    }
    
    set algo_complexapi01c {
    	lhs = ${prec}(idata);
        lhs = slice(lhs, 0, 0, 1);
        rhs = ${prec}(idata);
        lhs = slice(rhs, 0, 0, 1);
        res = complex(lhs, rhs);
        output(res, _, _, res, output of $procname;
	}
    
    set algo_complexapi02a {
      lhs = ${prec}(idata);
      rhs = ${prec}(idata);
      res = complex(lhs, rhs);      
      lhs2 = real(res);
      rhs2 = imag(res);
      ldiff = sub(lhs2, idata);
      ldiff = abs(ldiff);
      ldiff = max(ldiff);
      rdiff = sub(rhs2, idata);
      rdiff = abs(rdiff);
      rdiff = max(rdiff);
      output(ldiff, _, _, ldiff, output of $procname);
      output(rdiff, _, _, rdiff, output of $procname);       
    }
    
	proc complexapiTest { code } {   
     
       	if	{ [ catch {
        	set name [ namespace current ]
			set procname [ info level -1 ]
            namespace import [ namespace parent $name ]::*           

            set ifilepre 	[ namespace eval $name set ifilepre ]
            set ofilepre 	[ string toupper $procname ]
            set cmdstem [ namespace eval $name set cmdstem ]
            set dtype [ namespace eval $name set dtype ]
            set precs [ namespace eval $name set precs ]
            set N [ namespace eval $name set N ]

            set prefix $ofilepre$code$dtype
            set algostr ""
            set algo [ namespace eval $name set algo_${procname} ]
            
            set ifile ldas_outgoing/mdc/input/$ifilepre$dtype.sim
			set prefix $ofilepre$code$dtype
			set ofile ldas_outgoing/mdc/output/$prefix.ilwd
			set efile ldas_outgoing/mdc/input/$prefix.exp
            
            foreach prec $precs {
                set algo [ subst $algo ]                
                set subject datacondMDC:conditionData:$procname:$prec
                if	{ [ regexp {01} $procname ] } {
                	set cmd [ subst $cmdstem ]
                	runTest $subject $cmd
            		after 1000
                } else {
                	append algostr "\n$algo"
                }
            }
            if	{ [ regexp {02} $procname ] } {
            	set algo $algostr               
                set subject "datacondMDC:conditionData:$procname:all"
            	set cmd [ subst $cmdstem ]
                runTest $subject $cmd
				after 1000
            }
      	} err ] } {
    		return -code error $err
		}
	}
    
	proc complexapi01a {} {

		if	{ [ catch {
			complexapiTest A
  		} err ] } {
    		return -code error $err
		}
	}

	proc complexapi01b {} {

		if	{ [ catch {
 			complexapiTest B
		} err ] } {
    		return -code error $err
		}
	}	

	proc complexapi01c {} {

		if	{ [ catch {
			complexapiTest C
   		} err ] } {
    		return -code error $err
		}
	}

	proc complexapi02a {} {

		if	{ [ catch {
			complexapiTest A
    	} err ] }  {
    		return -code error $err
		}
	}

}

namespace import complexapi::*

