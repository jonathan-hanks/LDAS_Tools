RECURSIVE_TARGETS=$(PROJECT_RECURSIVE_TARGETS)

DVIFILES = \
	develHandbook.dvi

if HAVE_PS2PDF
PDFFILES = \
	develHandbook.pdf
endif

EXTRA_DIST = \
	$(develHandbook_TEXSOURCE)

develHandbook_TEXSOURCE = \
	develHandbook.tex \
	handbookActions.tex \
	handbookClasses.tex \
	handbookExceptions.tex \
	handbookMaint.tex \
	handbookOrganization.tex \
	handbookTestCode.tex \
	handbookTutorial.tex \
	handbookUDTsAndUMDs.tex \
	TestCodeRules.tex \
	tpageLIGO.tex

all-local : $(PDFFILES)

clean-local :
	@rm -f *.dvi *.aux *.toc *.log *.pdf *.ps

# If there's no aux file to begin with, you usually need
# to run LaTeX one additional time
develHandbook.aux: develHandbook.dvi
	env TEXINPUTS=$(srcdir): \
	latex $(srcdir)/develHandbook;
	result=$$?; \
	if [ $$result -ne 0 ]; then \
		echo "Error in LaTeX file"; \
		touch $(srcdir)/develHandbook.tex; \
		false; \
	fi

# Need to run LaTeX twice to make sure the cross-refs are ok
develHandbook.dvi: $(develHandbook_TEXSOURCE)
	env TEXINPUTS=$(srcdir): \
	latex $(srcdir)/develHandbook; \
	env TEXINPUTS=$(srcdir): \
	latex $(srcdir)/develHandbook; \
	result=$$?; \
	if [ $$result -ne 0 ]; then \
		echo "Error in LaTeX file"; \
		touch $(srcdir)/develHandbook.tex; \
		false; \
	fi

develHandbook.ps : develHandbook.dvi
	env TEXINPUTS=$(srcdir): \
	dvips develHandbook -o develHandbook.ps

if HAVE_PS2PDF
develHandbook.pdf : develHandbook.ps
	ps2pdf develHandbook.ps
endif

include $(ldas_top_srcdir)/build/make-macros/memory-debug.mk
