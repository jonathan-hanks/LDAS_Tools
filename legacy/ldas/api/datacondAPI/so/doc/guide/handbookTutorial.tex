% $Id: handbookTutorial.tex,v 1.1 2001/04/17 00:52:17 Philip.Charlton Exp $

% Tutorial

\subsection{The LDAS CVS repository}

Before beginning development of API code it is necessary to obtain
the LDAS source code and install the correct versions of all required packages.
The LDAS distribution must be built and installed using the instructions at
\begin{verbatim}
http://www.ldas-dev.ligo.caltech.edu/doc/INSTALL.html
\end{verbatim}
Note that this provides an end-user installation of LDAS -- it is not suitable
for development.

The development version of LDAS must be obtained from CIT via CVS.
It is assumed that developers have obtained an account for the LDAS CVS server.
First ensure that the Unix environment variable \verb+CVSROOT+ is set to
{\small
\begin{verbatim}
:pserver:Your.Username@ldas-sw.ligo.caltech.edu:/ldcg_server/common/repository
\end{verbatim}}
This can be done permanently by adding the lines
{\small
\begin{verbatim}
setenv CVSROOT \
:pserver:Your.Username@ldas-sw.ligo.caltech.edu:/ldcg_server/common/repository
\end{verbatim}}
to your {\tt .cshrc} (or local equivalent).
\noindent
The user can then check out a private development version of LDAS using the
commands
\begin{verbatim}
cvs login
cvs co ldas
\end{verbatim}
This will create a directory under the current directory called {\tt ldas},
containing the full source and configuration for LDAS. This must be kept
separate from the end-user installation of LDAS.

The development LDAS can then be built and verified using the commands
\begin{verbatim}
cd ldas
./build-ldas [ --prefix=/location/of/ldas ] [ other params ... ]
cd Linux-i686 [ or correct architecture directory... ]
make check
\end{verbatim}
Note that the name of the directory where the object files and libraries are
built depends on the operating system. Possible values are \verb+Linux-i686+,
\verb+Linux-alpha+ or \verb+SunOS-sun4u+.
We will assume \verb+Linux-i686+ from now on.
The initial build may take several hours to complete.
The command {\tt make check} builds the LDAS test code. Developers may check
that the build worked correctly by looking at the log files in
\verb+Linux-i686/build_logs+.

After a successful build there will be a directory tree resembling:
\begin{verbatim}
ldas                              LDAS root directory
|-- Linux-i686                    Root of build directory (name
|   |-- api                           depends on architecture)
|   |   |-- datacondAPI           Root for build of API
|   |   |   |-- so                Root for build of shared object
|   |   |   |   |-- doc           Docs get built here
|   |   |   |   |   `-- html
|   |   |   |   |       `-- tcl
|   |   |   |   |-- src           Object files get built here
|   |   |   |   `-- test          Test exes get built here
|   |   |   `-- tcl
|   |   `-- tcl_docs
|   |-- build_logs                Logs for 'make' and 'configure'
|   |-- include                   
|   `-- lib                       
|-- api                           Root for all API's
|   `-- datacondAPI               Root for datacondAPI
|       |-- so                    Root for shared object
|       |   |-- doc               LaTeX source
|       |   |   `-- html          HTML source
|       |   |-- src               Source code for API library
|       |   `-- test              Source code for API test code
|       `-- tcl                   Source code for Tcl
|           `-- test              Source code for Tcl tests
`-- lib
\end{verbatim}
New code can then be added and built in the following way:
\begin{enumerate}
\item Go to the appropriate source directory under \verb+ldas/api/datacondAPI+.
\item Edit or create the source file(s).
\item Edit \verb+Makefile.am+ and add the new .cc files to the list under\\
\verb+libdatacondAPI_la_SOURCES+. New {\tt .hh} files should be added
under \verb+noinst_HEADERS+.
\item Run \verb+make+ in \verb+ldas/Linux-i686/api/datacondAPI+ or an
appropriate subdirectory to verify that the code compiles.
\item If any test code exists for the new or modified code, run
\verb+make check+ in the appropriate test directory and verify that the
tests are passed.
\item Add the code to the repository using the commands
\begin{verbatim}
cvs add file1 file2 ...
cvs commit -m'A message' Makefile.am file1 file2 ... 
\end{verbatim}
\end{enumerate}
{\bf NOTE: Before committing code to the repository, verify that it
compiles with the most up-to-date version of the API and doesn't cause
any of the existing tests to fail.}

Since LDAS code is constantly being revised, developers will want to
update their local CVS repository and rebuild LDAS regularly. This generally
does not take as long as the initial build since only those components which
changed need to be rebuilt. To update, use the commands
\begin{verbatim}
cd ldas
cvs login
cvs update -d
cd Linux-i686
make
\end{verbatim}
The {\tt -d} option instructs CVS to add any new directories it finds to
your source tree. You may also want to {\tt make check} to verify the new
build. Very occasionally, a new build may fail because of changes to
configuration files in the repository. In that case it is best to remove
the entire {\tt Linux-i686} directory and rebuild using the {\tt build-ldas}
command.

\subsection{Test code}

Each API class must have a C++ test harness in the directory
{\tt datacondAPI/so/test}. The name of the test harness file will be
{\tt tClassName.cc}. The name of the executable {\tt tClassName} must
be added to the \verb+EXTRA_PROGRAMS+ entry in
{\tt datacondAPI/so/test/Makefile.am} along with the line
\begin{verbatim}
tClassName_SOURCES = tClassName.cc
\end{verbatim}

The class {\tt UnitTest} is to be used in all test harnesses. This class
provides a standard interface for performing tests and reporting the results.
The harness will test every function of the class under consideration for:
\begin{itemize}
\item Numerical accuracy (where applicable).
\item Correct behaviour for several cases of ``typical'' valid input.
\item Correct behaviour for ``marginal'' valid input eg.
\begin{itemize}
\item Input parameters that are ``off-by-one'' for any upper or lower limits
(especially array boundaries).
\item Input parameters that are exactly at an upper or lower limit.
\item Input data that has atypically large or small dynamic range.
\end{itemize}
\item Correct behaviour for each type of invalid input. This would usually
mean verifying that the correct exception is thrown.
\end{itemize}
