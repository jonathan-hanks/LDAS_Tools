\input Head

\section{\tt FFT}

% A short (preferably one-line) description of {\tt MyClass}.

{\tt FFT} --- class to calculate the discrete Fourier transform of a
series using the Fast Fourier Transform algorithm.

\subsection{Description}

% A longer description of what {\tt MyClass} is used for.

The class {\tt FFT} provides functions for calculating the
discrete Fourier transform (DFT) of a series using the Fast Fourier
Transform algorithm. The DFT of a series is another series
(usually complex) containing the same number of elements.
If the input is regarded as a time-series then the output can be regarded
as a series in the frequency domain.

If $\{ x_j \}$, $j = 0, 1,\ldots N-1$ represents a
series where each $x_j$ is a real or complex number, then
the discrete Fourier transform $\{ X_k \}$ of $\{ x_j \}$ 
is given by 
$$
X_k = \sum_{j\, =\, 0}^{N - 1} x_j \exp(-i 2 \pi k j/N)
$$
where the index $k$ takes the values $k = 0, 1, \ldots, N - 1$.
With these conventions the zero-frequency (DC) component of the Fourier
transform is $X_{0}$ (the first value of the sequence), and the 
Nyquist frequency component is $X_{\lfloor N/2 \rfloor}$. These
conventions are chosen to agree with the Matlab convention for the
\verb+fft+ operation.
Note that when $\{ x_j \}$ is a real
sequence the resulting DFT has the Hermitian symmetry property that
$X_{k} = X_{N-k}^*$, hence the DC component $X_0$ is always real. The
Nyquist component $X_{\lfloor N/2 \rfloor}$ is also real when $N$ is
even.

\subsection{Operating Instructions}

\subsubsection{Usage}
Speed and memory requirements of the DFT computation are greatly
affected by the type of DFT to be performed and the numerical
precision required. The DFT of a real sequence can be performed in half
the time and memory space used to perform a complex DFT of the same
length. Single precision calculations also use half the time
and memory required by the equivalent double precision DFT. 
For these
reasons four DFT \verb+apply()+ functions are provided for performing 
the different types of DFT (real single precision; real double precision;
complex single precision; complex double precision). They all have
the common form
\begin{verbatim}
  template<class T>
  void apply(      DFT<complex< P > >& out,
             const std::valarray< T >& in);
\end{verbatim}
where
\begin{itemize}
\item \verb+P+ is either \verb+float+ or \verb+double+
\item \verb+T+ is one of the types \verb+float+, \verb+double+
  (for real DFT) or \verb+complex<float>+, \verb+complex<double>+
  (for complex DFT).
  These need not be explicitly specified: since \verb+apply()+ is
  a template function the compiler automatically chooses the appropriate
  function based on the data types given as parameters.
\item \verb+in+ is an input parameter containing the series
  $\{ x_j \}$ where \verb+in[j]+ $= x_j$ and
  \verb+in.size()+ $= N$
\item \verb+out+ is an output parameter containing the DFT of
  $\{ x_j \}$ where \verb+out[j]+ $= X_j$ and
  \verb+out.size()+ $= N$. In addition, \verb+out.IsSymmetric()+
  is true if \verb+in+ is real, false if it is complex. An Hermitian symmetric
  \verb+DFT<complex< P > >+ has the property that
  \verb+out[k] = conj(out[N-k])+.

  Note that
  \verb+out+ need not be initialized to any particular size or
  symmetry, but the DFT will experience a speed penalty if this is not
  the case, since it must resize the output array before performing the
  DFT.
\end{itemize}

\subsubsection{UDT Usage}

The {\tt FFT} class also provides the following method for applying the FFT to
a UDT object:
\begin{verbatim}
  void apply(udt* out, const udt& in);
\end{verbatim}
where
\begin{itemize}
\item \verb+in+ is a UDT derived from \verb+Sequence<T>+
\item \verb+out+ is either the zero pointer or a pointer to
a \verb+DFT<complex<T>  >+. If {\tt out} is zero, a UDT of the appropriate
type to hold the result is created on the heap. This UDT may be re-used
in subsequent calls to {\tt apply()}. It is the users responsibility to
delete it when finished.

If {\tt out} is non-zero, it must point to a UDT of a type appropriate to
holding the result of the FFT. If the input type is (or is derived from)
\verb+Sequence<T>+, then the output type must be (or be derived from)
\verb+DFT<complex<T> >+.
\end{itemize}


\subsubsection{Example Usage}

% A verbatim block of example code showing proper usage of {\tt
% MyClass}.

\begin{verbatim}
#include "ifft.hh"

main()
{
    const size_t N = 1024;
    Sequence<float> in(N); 
    DFT<complex<float> > out(N);

    udt* udt_out_nonzero = &out;
    udt* udt_out_zero = 0;

    FFT fft;

    // Set series
    for (size_t j = 0; j < N; j++)
    {
        in[j] = sin(2.0*M_PI*j/N);
    }

    // Perform DFT using valarray method
    // A Sequence is also a valarray, by inheritance
    fft.apply(out, in);

    // Perform DFT using UDT method, without creating output
    fft.apply(udt_out_nonzero, in);

    // Perform DFT using UDT method, creating output
    fft.apply(udt_out_zero, in);

    for (size_t k = 0; k < out.size(); k++)
    {
        cout << "X[" << k << "] = " << out[k] << endl;
    }

    // Cast udt_out to DFT<complex<float> >
    DFT<complex<float> >& X
        = dynamic_cast<DFT<complex<float> >&>(*udt_out_zero);
  
    for (size_t k = 0; k < X.size(); k++)
    {
        cout << "X[" << k << "] = " << X[k] << endl;
    }

    // Remember to delete the output!
    delete udt_out_zero;
}
\end{verbatim}

\subsection{Algorithms}

% A discussion of the algorithms used by {\tt MyClass}.

{\tt FFT} uses the Fast Fourier Transform algorithm as implemented by
FFTW (``Fastest Fourier Transform in the West''). It is fastest when
the sequence length $N$ can be expressed in the form $2^k 3^l 5^m$,
in which case it is $O(N \log N)$.
When $N$ is not of this form the $O(N^2)$ discrete Fourier transform is
used.

\subsection{Accuracy}

% A discussion of issues related to the accuracy of numerical routines
% used by {\tt MyClass}---e.g., approximations,  argument ranges, etc.

The precision of the FFT is determined by the precision of the input
data.
If the input is in single precision, the FFT is performed in
single precision. A single precision FFT is guaranteed to agree
with the FFT as calculated by Matlab (which uses double precision)
to an absolute tolerance of $10^{-5}$.

If the input is in double precision, the FFT is performed in
double precision. A double precision FFT is guaranteed to agree
with the FFT as calculated by Matlab
to an absolute tolerance of $10^{-9}$.

\subsection{Performance}

% A discussion of issues related to the speed (e.g., $O(N\log N)$) of
% computationally demanding routines used by {\tt MyClass}.
When $N$ is of the form $2^k 3^l 5^m$,
the algorithm is of order $O(N \log N))$, otherwise it is
of order $O(N^2)$.

\subsection{Testing}

% A description of any test code/test classes that were used to test
% {\tt MyClass}. 

\begin{itemize}
\item {\tt tSfft.cc} -- checks accuracy of single-precision complex FFT.
\item {\tt tDfft.cc} -- checks accuracy of double-precision complex FFT.
\item {\tt tSrealfft.cc} -- checks accuracy of single-precision real FFT.
\item {\tt tDrealfft.cc} -- checks accuracy of double-precision real FFT.
\item {\tt tFFTPerf.cc} -- checks performance of the FFT.
\item {\tt tFFT\_thread.cc} -- checks running of the FFT in multiple
simultaneous threads.
\end{itemize}

\subsection{Notes}

% Miscellaneous notes about {\tt MyClass} that do not appear
% elsewhere, things that remain to be done, etc.  
None

\subsection{See Also}

% A list of derived classes, base classes, etc.\ closely related to
% {\tt MyClass}. 

\begin{itemize}
\item {\tt DFT}
\item {\tt IFFT}
\end{itemize}


% \subsection{References}

% A list of references for any algorithms, etc.\ used by {\tt
% MyClass}. 
\begin{thebibliography}{99}

\bibitem{FFTW}
The FFTW Home Page, {\tt www.fftw.org}

\end{thebibliography}

\input Tail


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
