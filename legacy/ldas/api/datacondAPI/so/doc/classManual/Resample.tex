\input Head

\section{\texttt{Resample}}

\texttt{Resample} --- Change sample rate by rational fraction

% A short (preferably one-line) description of \texttt{MyClass}.

\subsection{Description}

\texttt{Resample} changes the sample rate of a time series, with 
anti-aliasing. The sample rate can be changed by any rational
fraction $p/q$, for natural numbers $p$ and $q$. The anti-aliasing
filter is a Kaiser window design with user-specifiable $\beta$. 
 
\subsection{Operating Instructions}

\subsubsection{Usage}

\texttt{Resample} is initialized on construction. A sequence
\texttt{x} is resampled using the overloaded \texttt{apply}
method:
\begin{quote}
\footnotesize
\begin{verbatim}
//: Action of Resample
void apply(udt*& out, const udt& in)
  ; // throw(logic_error, invalid_argument);
//!param: const udt& in - data to be resampled
//!param: udt*& out - result of resampling the "in" data
//!exc: logic_error - Input length must be a multiple of q
    
//: Action of Resample
template<class T>
void apply(std::valarray<T>& out, const std::valarray<T>& in)
  ; // throw(logic_error);
 //!param: const valarray<T>& in - data to be resampled
 //!param: valarray<T>& out - result of resampling the "in" data
 //!exc: logic_error - Input length must be a multiple of q
\end{verbatim}
\end{quote}
The udt version of \texttt{apply} method can act on 
\texttt{Sequence} and \texttt{TimeSeries} arguments. Metadata is
interpreted. In all cases the result must be of the same type as
\texttt{x}.  

\subsubsection{Examples}

% A verbatim block of example code showing proper usage of {\tt
% MyClass}.

\begin{quote}
\footnotesize
\begin{verbatim}
int p;
int q;
int n;        // optional filter order parameter
double beta;  // optional Kaiser window parameter
.
.
.
const ResampleState rsState(;

// Construct with default filter parameters
Resample rs(p,q);
// Construct with user-specified filter order
Resample rs(p,q,n);
// Construct with user-specified filter order, Kaiser window parameter
Resample rs(p,q,n,beta);
// Construct from existing state
Resample rs(rsState);

std::valarray<complex<double> > x;   // input sequence
std::valarray<complex<double> > y;   // output, input sequences of same type
// Can also be applied to Sequence<>, TimeSeries<>, in which case
// metadata is interpreted

rs.apply(y,x); // y is resampled x

int nOrder = rs.getOrder(); // the actual order of the anti-aliasing filter
double delay = rs.getDelay(); 
// if x is an impulse (i.e., x(0) = 1, x(1:end) = 0) then the maximum 
// in the output (y) is at sample delay, which may be fractional. 

std::valarray<complex<double> > xa(x(slice(0,100,1))); // segmented input
std::valarray<complex<double> > xb(x(slice(0,100,1)));
std::valarray<complex<double> > ya;     // segmented output
std::valarray<complex<double> > yb;
std::valarray<complex<double> > ybb;

Resample rs(p,q);
Resample rs0(p,q);

rs.apply(y,x);
rs0.apply(ya,xa);
Resample rss(rs0.getState());
rs0.apply(yb,xb);
rss.apply(ybb,xb);
// ya is identical to y(slice(0,100,1))
// yb and ybb are identical y(slice(100,100,1));
\end{verbatim}
\end{quote}

\subsubsection{Options/Defaults}

% A list of options or defaults for public methods or member data of
% \texttt{MyClass}. 

Unless specified by the user, the anti-aliasing filter will have order
parameter 10 (corresponding to filter order $n\max(p,q)$) and $\beta$
parameter 5.  

\subsection{Exceptions}

\subsubsection{Exceptions Thrown}

% A list of exceptions thrown by \texttt{MyClass}.

\begin{itemize}
\item \texttt{Resample(int p, int q, int n, double beta)}
  \begin{itemize}
  \item \texttt{std::domain\_error} if $p$, $q$, $n$ or $\beta$ zero
  \item \texttt{std::invalid\_argument} ????
  \end{itemize}
\item \texttt{Resample(const udt\& state)}
  \begin{itemize}
  \item \texttt{std::invalid\_argument} if \texttt{state} not a
    \texttt{ResampleState} object
  \end{itemize}
\item \texttt{apply(udt$*$\& out, const udt\& in)}
  \begin{itemize}
  \item \texttt{unimplemented\_error} if to something other than
    \texttt{TimeSeries} or \texttt{Sequence}
  \item \texttt{std::logic\_error} if \texttt{in.size()} not a
    multiple of \texttt{q}
  \end{itemize}
\end{itemize}

\subsubsection{Exceptions Handled}

% A list of exceptions handled by \texttt{MyClass}.

None. 

\subsection{Algorithms}

% A discussion of the algorithms used by \texttt{MyClass}.

Conceptually, resampling as implemented here involves the following
steps:
\begin{itemize}
\item the input sequence length is increased by insertion
of $p-1$ zeros between successive points;
\item the new sequence is low-pass filtered, with pass-band edge at
  $q/p$ of the up-sampled rate;
\item the filtered sequence is reduced by keeping only every $q$
  points. 
\end{itemize}
\texttt{Resample} performs these operations using a polyphase
algorithm, which interleaves and re-orders many of the operations.  In
the remainder of this section we describe that implementation. 

Start by focusing on the upsample operation. From the input sequence
$x$ we define a new sequence $x_p$ by interleaving elements of $x$
with stretches of $0$'s: for $k\in[0,p-1]$, 
\begin{eqnarray}
  x_p[pj+k] &:=& \left\{\begin{array}{ll}
      x[j]&k == 0\\
      0&k \neq 0
    \end{array}\right.
\end{eqnarray}
Assuming that the anti-aliasing filter $b$ is zero-extended so that
its length is a multiple of $p$, we can write the output $y$ as
\begin{eqnarray}
  y[pk+l] &:=& \sum_{j=0}^{N_B} b[j]x_p[pk+l-j]\\
  &=& \sum_{m=0}^{p-1}\sum_{j=0}^{(N_{B}+1)/p-1}
  b[pj+m]x_p[p(k-j)+l-m]
\end{eqnarray}
Since $x_p[pj+k]$ vanishes for $k\neq0$ we can write
\begin{eqnarray}
y[pk+l] &=& \sum_{j=0}^{(N_{B}+1)/p-1} b_l[j]x[k-j]
\end{eqnarray}
where we've identified the phases $b_l$ of the anti-aliasing filter as
\begin{equation}
b_l[k] := b[pk+l].
\end{equation}
That is, the result is equal to the interleaving of the input
sequence, without zero insertion, filtered by the different phases of
the anti-aliasing filter.  When $q$ is unity, we implement the
upsampling operation in just this way: elements of the result $y$ are
the result of filtering $x$ by the different phases of the filter and
interleaving the results as appropriate. No zero insertion takes
place and the storage required is minimized.

Now consider downsampling. Again assume that the anti-aliasing filter
is zero-extended so that its length is now a multiple of $q$. Then
\begin{eqnarray}
  y[k] &:=& \sum_{j=0}^{N_B} b[j]x[qk-j]\\
  &=& \sum_{j=0}^{q-1}\sum_{n=0}^{(N_{B}+1)/q-1} b[qn+j]x[q(k-n)-j]\\
  &=& \sum_{j=0}^{q-1}\sum_{n=0}^{(N_{B}+1)/q-1} b_j[n]x_j[k-n]
\end{eqnarray}
where
\begin{eqnarray}
  b_j[k] &:=& b[qk+j]\\
  x_j[k] &:=& x[qk-j]. \label{eq:x_j}
\end{eqnarray}
In other words, the downsampling operation involves the sum of phases
of the input sequence filtered by phases of the anti-aliasing
filter. No elements of a sequence, later to be discarded, are ever
computed.

Notice from equation \ref{eq:x_j} that $y[0]$ depends on elements of
$x$ that are not available - elements that are prior to $x[0]$.  If 
$x$ is resampled all at once, these missing elements are treated as
zeroes.  If $x$ is resampled in segments, the missing elements are
treated as zeroes for the first segment only.  Subsequent segments are
resampled correctly by saving a partial result from the previous segment.


Resampling by a rational fraction proceeds by combining these two
algorithms: 
\begin{itemize}
\item the anti-aliasing filter $b$ is divided into phases modulo
$p$ ($b_j$, $j\in[0,p-1]$);
\item the input sequence $x$ is ``downsampled'' $p$ times, each time
  using a different $b_j$ as the ``anti-aliasing'' filter, to produce
  output sequences $y_j$;
\item the resulting output sequences $y_j$ are interleaved, as in
  upsampling, to produce the final resampled result. 
\end{itemize}

Resample designs its own anti-aliasing filter using a Kaiser
window. The filter order $N$ is 
\begin{equation}
N := 2n\max(p,q),
\end{equation}
where $n$ is set in the constructor and defaults to 10. The Kaiser
window $\beta$ parameter, which defaults to 5, is also set in the
constructor. Corresponding to the filter's even order is a center-tap,
which is associated with the delay of the filters maximum impulse
response. An additional delay is added to the filter so that the
product of $x[0]$ and the center tap contributes to the output. The
method \texttt{getDelay()} returns the filter delay, corresponding to
the (non-integer) number of samples between an impulse input and the
maximum filter output. 

\subsection{Accuracy}

% A discussion of issues related to the accuracy of numerical routines
% used by \texttt{MyClass}---e.g., approximations,  argument ranges, etc.

Calculations are performed in the type/precision of the input data
stream. Linear filtering is performed using \texttt{LinFilt} objects.

\subsection{Performance}

% A discussion of issues related to the speed (e.g., $O(N\log N)$) of
% computationally demanding routines used by \texttt{MyClass}.

The performance of \texttt{Resample} is limited by the performance of
the \texttt{LinFilt}.

\subsection{Testing}

% A description of any test code/test classes that were used to test
% \texttt{MyClass}. 

\texttt{Resample} is tested by the \texttt{tResample}
test-harness. Tests include 
\begin{itemize}
\item Exceptions
\item Correctness
\begin{itemize}
\item A $\sin$-wave at a frequency below the cut-off is resampled and
  the result compared to a $\sin$-wave of the same frequency, sampled
  at the new rate and with the appropriate filter delay
\item A sequence is resampled first in its entirety and then as two,
  consecutive sub-sequences. The results are compared for zero
  differences.
\item These tests are performed for different input types. 
\end{itemize}
\end{itemize}

\subsection{Notes}

% Miscellaneous notes about \texttt{MyClass} that do not appear
% elsewhere, things that remain to be done, etc.  

Test code needs to be cleaned-up.

\subsection{See Also}

% A list of derived classes, base classes, etc.\ closely related to
% \texttt{MyClass}. 

\begin{itemize}
\item LinFilt
\item ResampleState
\item Sequence
\item TimeSeries
\end{itemize}

\subsection{References}

% A list of references for any algorithms, etc.\ used by {\tt
% MyClass}. 

\input Tail


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
