\input Head

\section{\tt Statistics}

A class of methods for calculating summary statistics (size, min, max, 
mean, rms, variance, skewness, and kurtosis) for a real array of data.

\subsection{Description}

{\tt Statistics} is a class of methods that can be used to calculate 
summary statistics for a real array of integer, float, or double 
precision data.
The statistics currently implemented are:
{\tt size}, {\tt min}, {\tt max},  {\tt mean}, {\tt rms}, 
{\tt variance}, {\tt skewness}, and {\tt kurtosis}.
In addition, there is an {\tt all} method, which returns all of
the above statistics in a statistics universal data type 
({\texttt{Stats}}) variable. 
The {\texttt{Stats}} variable contains the quantities related 
to the first four moments of the data distribution 
(mean, variance, skewness and kurtosis) in an array of doubles. 
The other summary statistitics (size, min, max, and rms) are 
associated with the universal meta-data of this class 
({\texttt{StatsMetaData}}). 

Mean, rms, variance, skewness, kurtosis are defined as follows:

\begin{description}
%
\item[{\tt mean}:]
%
\begin{equation}
{\tt mean}\equiv
\bar x := {1\over N}\sum_{i=0}^{N-1} x[i]\ ,
\label{statse:mean}
\end{equation}
%
where $N$ denotes the length of the data, and $x[i]$ are the 
data values.
If we interpret the data as being samples drawn from a larger 
population, the sample mean calculated above is an unbiased 
estimator of the true mean $\mu$ of the population.
%
\item[{\tt rms}:]
%
\begin{equation}
{\tt rms}\equiv
\sqrt{{1\over N}\sum_{i=0}^{N-1} x[i]^2}\ .
\label{statse:rms}
\end{equation}
%
This is just the square-root of the mean of the squared data values.
%
\item[{\tt variance}:]
%
\begin{equation}
{\tt variance}\equiv
s^2 := {1\over N-1}\sum_{i=0}^{N-1} (x[i]-\bar x)^2\ ,
\label{statse:variance}
\end{equation}
%
where $\bar x$ is the sample mean of the data, defined by
Eq.~(\ref{statse:mean}).
The normalization chosen here is appropriate when we 
interpret the data as being samples drawn from a larger population.
The sample variance thus calculated is an unbiased estimator of the
true variance $\sigma^2$ of the population.
%
\item[{\tt skewness}:]
%
\begin{equation}
{\tt skewness}\equiv
{1\over N}\sum_{i=0}^{N-1} \left[{x[i]-\bar x\over s}\right]^3\ ,
\label{statse:skew}
\end{equation}
%
where $\bar x$ and $s^2$ are the sample mean and sample variance 
of the data, defined by Eq.~(\ref{statse:mean}) and (\ref{statse:variance}).
Again, the normalization chosen here is appropriate when we 
interpret the data as being samples drawn from a larger population.
This normalization convention differs from that used in 
Matlab\cite{statsmatlab}, but agrees with that used in Numerical Recipes 
in C\cite{statsNRC}. 
%
\item[{\tt kurtosis}:]
%
\begin{equation}
{\tt kurtosis}\equiv
{1\over N}\sum_{i=0}^{N-1} \left[{x[i]-\bar x\over s}\right]^4\ ,
\label{statse:kurt}
\end{equation}
%
where $\bar x$ and $s^2$ are the sample mean and sample variance 
of the data, defined by Eq.~(\ref{statse:mean}) and (\ref{statse:variance}).
As before, the normalization chosen is appropriate when we 
interpret the data as being samples drawn from a larger population.
This normalization convention differs from that used in 
Matlab, but agrees with that used in Numerical Recipes in C 
provided we subtract off a 3. 
According to our definition of kurtosis, a Gaussian distribution 
would a kurtosis value of 3.
%
\end{description}

Note that variance, skewness, and kurtosis are all undefined 
if the data has length 0 or 1.
If the variance of an input array is 0 (e.g., the data consists 
of all 1's), skewness and kurtosis are defined to be 0.

\subsection{Operating Instructions}

\subsubsection{Example Usage}

{\footnotesize 
\begin{verbatim}

  // valid floating point data
  //
  float in1[] = {0.1, 0.3, -1.1, 1.2, 1.2};
  Sequence<float> data1(in1,5);

  // calculate individual statistics and display on standard output
  //
  Statistics statsObj;

  cout << "size     = " << statsObj.size(data1) << endl;
  cout << "min      = " << statsObj.min(data1)  << endl;
  cout << "max      = " << statsObj.max(data1)  << endl;

  // ...

  cout << "kurtosis = " << stats.kurtosis(data1) << endl;

  // calculate all statistics at once and display on standard output
  //
  Stats allStats;
  udt* pAllStats = &allStats; 

  statsObj.all(pAllStats, data1);

  cout << "size     = " << allStats.GetSize() << endl;
  cout << "min      = " << allStats.GetMin()  << endl;
  cout << "max      = " << allStats.GetMax()  << endl;

  // ...

  cout << "skewness = " << allStats[2] << endl;
  cout << "kurtosis = " << allStats[3] << endl;

  // repeat for a new set of floating point data  
  //
  float in2[] = {-1.5, 0.3, 6.2, 1.1, -3.6, 4.7};
  Sequence<float> data2(in2,6);

  // it's okay to use the same Statistics object
  //
  cout << "size     = " << statsObj.size(data2)      << endl;
  cout << "min      = " << statsObj.min(data2)       << endl;
  cout << "max      = " << statsObj.max(data2)       << endl;

  // ...

\end{verbatim}}

\subsection{Algorithms}

The algorithms for {\tt size}, {\tt min}, and {\tt max} are 
inherited from the corresponding {\tt valarray<T>} methods.
The {\tt mean}, however, is not implemented as simply 
{\tt data.sum()/data.size()}, since the standard valarray 
{\tt sum} method could easily overflow if the data is of 
integer type.
Instead, the mean is calculated in double precision, 
explicitly summing over the elements in the data array,
as specified by Eq.~(\ref{statse:mean}).
Likewise, the {\tt rms} method is a straight-forward implementation 
of Eq.~(\ref{statse:rms}).

The {\tt variance} is implemented using the {\em corrected 
two-pass algorithm} \cite{statsNRC,statstwo-pass}.
This algorithm helps to minimize round-off error, especially for
large samples.
Basically, one calculates the variance according to
%
\begin{equation}
{\tt variance}\equiv
{1\over N-1}\left\{
\sum_{i=0}^{N-1} (x[i]-\bar x)^2
-{1\over N}\left[\sum_{i=0}^{N-1} (x[i]-\bar x)\right]^2\right\}\ ,
\end{equation}
%
where $\bar x$ is the sample mean of the data, defined by
Eq.~(\ref{statse:mean}).
The second sum is zero if the calculation of $\bar x$ is exact.
If it is non-zero, it helps reduces the round-off error in the first 
term.

The {\tt skewness} and {\tt kurtosis} methods are straight-forward
implementations of Eqs.~(\ref{statse:skew}) and (\ref{statse:kurt}), with
the variance $s$ calculated using the two-pass algorithm, described
above.

\subsection{Accuracy}

Calculation of the mean, rms, variance, skewness, and kurtosis
are carried out using double precision arithmetic and returned as 
{\tt double}, regardless of the input type.

\subsection{Performance}

Calculation of the individual statistics are $O(N)$, where $N$ is
the length of the input data.

\subsection{Testing}

{\tt Statistics} is tested using {\tt tStatistics.cc}.
The following tests are performed:
%
\begin{itemize}
\item
test that proper exceptions are thrown by the individual 
statistics and {\tt all} statistics methods when the input data 
has length 0.
\item
test that proper exceptions are thrown by the {\tt variance}, 
{\tt skewness}, {\tt kurtosis}, and {\tt all} methods when the 
input data has length 1.
\item
test that the individual statistics and {\tt all} statistics 
methods return correct results for trivial (i.e., $N=1$) and 
non-trivial input data having different types ({\tt int}, 
{\tt float}, and {\tt double}) and different lengths.
\item
test that time and name metadata is handled correctly.
\end{itemize}
%
When testing the accuracy of the statistics methods on valid 
data, we compare the output of the methods with results 
previously calculated with Matlab.
The ouput is said to be in agreement with the Matlab results 
if the absolute value of the difference between output and result 
is less than or equal to $10^{-5}$.
{\tt Statistics} passes all of the above tests.

\subsection{Notes}

None.

\subsection{See Also}

\begin{itemize}
\item 
class {\tt Stats}
\item
class {\tt StatsMetaData}
\end{itemize}

\begin{thebibliography}{99}

\bibitem{statsNRC}
W.H.\ Press, B.P.\ Flannery, S.A.\ Teukolsky, and W.T.\ Vetterling,
{\em Numerical Recipes in C}, Second edition,
(Cambridge University Press, Cambridge, 1992), Chapter 14.

\bibitem{statsmatlab} 
The MathWorks: {\tt http:\slash\slash www.mathworks.com}.

\bibitem{statstwo-pass}
P.R. Bevington,
{\em Data Reduction and Error Analysis in the Physical Sciences},
(New York, McGraw-Hill, 1969), Chapter 2.
\end{thebibliography}

\input Tail

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "datacondAPI"
%%% End: 
