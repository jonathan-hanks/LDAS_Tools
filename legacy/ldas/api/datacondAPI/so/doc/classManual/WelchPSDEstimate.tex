\documentclass{article}
\usepackage{epsfig}

\begin{document}

\section{\tt WelchEstimate<T>}

{\tt WelchEstimate<T>} is a template class for estimating
power spectra using Welch's method \cite{WelchP&W,WelchO&S}.

\subsection{User Interface}

The following is a list of public functions declared in 
{\tt WelchEstimate.hh}:

{\footnotesize 
\begin{verbatim}
  template<class T>
  class WelchEstimate: public PSDEstimate<T> {

  public:

    // destructor
    virtual ~WelchEstimate();

    // default constructor
    WelchEstimate();

    // constructor with arguments
    explicit WelchEstimate(const size_t       fftLength,
                           const size_t       overlapLength,
                           const Window<T>&   window,
                           const detrendFlag& dFlag = none)
      throw (std::invalid_argument);

    // copy constructor
    WelchEstimate(const WelchEstimate<T>& r);

    // copy assignment
    const WelchEstimate<T>& operator=(const WelchEstimate<T>& r);

    // set parameters
    //
    void set_fftLength(const size_t fftLength)
      throw (std::invalid_argument);

    void set_overlapLength(const size_t overlapLength)
      throw (std::invalid_argument);

    void set_window(const Window<T>& window);

    // accessors
    //
    string     what()          const;
    size_t     fftLength()     const;
    size_t     overlapLength() const;
    WindowInfo windowType()    const;

    // operations
    //
    void apply(valarray<T>&       psd,
               valarray<T>&       freqs,
               const valarray<T>& in)
      throw (std::invalid_argument);

    void apply(valarray<T>&                 psd,
               valarray<T>&                 freqs,
               const valarray<complex<T> >& in)
      throw (std::invalid_argument);

    void operator()(valarray<T>&       psd,
                    valarray<T>&       freqs,
                    const valarray<T>& in)
      throw (std::invalid_argument);

    void operator()(valarray<T>&                 psd,
                    valarray<T>&                 freqs,
                    const valarray<complex<T> >& in)
      throw (std::invalid_argument);

  };
\end{verbatim}}

\subsection{Description}
\label{Welchss:description}

{\tt WelchEstimate<T>} is a template class for estimating
power spectra using Welch's method \cite{WelchP&W,WelchO&S}.
It derives from the abstract base class {\tt PSDEstimate<T>}.
The template class {\tt T} should be either {\tt float} or
{\tt double}.
If you want to use {\tt WelchEstimate<T>} to estimate 
power spectra of integer data, you should 
first cast the data to either {\tt float} or {\tt double}.
 
In Welch's method, a power spectrum is estimated by averaging 
modified periodograms of (possibly) overlapping segments of 
the input data.
A Welch estimate is defined by an fft length, overlap length, 
a choice of window, and detrend option:
%
\begin{itemize}
\item{}
The fft length is an integer that must be greater than zero
and less than or equal to the length of the input data.
If the length of the input data is less than the fft length, 
the {\tt apply} and {\tt operator()} methods call the 
{\tt set\_fftLength} method to change fft length to equal 
the length of the data.
\item{}
The overlap length is an integer that must be greater than or
equal to zero and less than the fft length.
\item{}
One can use a rectangular, Hann, or Kaiser windows to
window individual data segments before calculating the
discrete Fourier transforms (DFTs).
Additional windows---e.g., Bartlett, Blackman, and 
Hamming windows---may be added in the future.
(See {\tt Window<T>} and the associated window classes
for more information.)
\item{}
The available detrend options are:
{\tt none}, {\tt mean}, or {\tt linear}.
These, respectively, do nothing, remove a mean, or remove 
a linear trend from the pre-windowed segments of data.
(See {\tt PSDEstimate<T>} for information about set and 
accessor methods for the {\tt detrendFlag} variable.)
\end{itemize}

In addition to the functionality defined in the abstract base 
class {\tt PSDEstimate<T>}, {\tt WelchEstimate<T>} defines the 
following public methods:
%
\begin{description}
%
\item[{\tt set\_fftLength}:]
Sets the value of the fft length.
%
\item[{\tt set\_overlapLength}:]
Sets the value of the overlap length.
%
\item[{\tt set\_window}:]
Sets the window function.
%
\item[{\tt what}:]
Returns a string containing the name of tbe power spectrum
estimate: {\tt "Welch Estimate"}.
%
\item[{\tt fftLength}:]
Returns the value of the current fft length.
%
\item[{\tt overlapLength}:]
Returns the value of the current overlap length.
%
\item[{\tt windowType}:]
Returns a {\tt WindowInfo} object containing information about
the current window---i.e., the name of the window and a parameter 
value (if any) needed to define the window.
(See {\tt WindowInfo} for details.)
%
\item[{\tt apply}:]
Returns the 2-sided power spectrum estimate and corresponding 
discrete frequency values for both real and complex input data.
The units and normalization of the power spectrum are chosen 
so that Parseval's theorem holds in the following form:
%
\begin{equation}
{1\over{\tt fftLength}}
\sum_{\tt i=0}^{\tt fftLength-1}{\tt psd[i]} =
{1\over{\tt dataLength}}
\sum_{\tt i=0}^{\tt dataLength-1}{\tt in[i]}^2\ .
\label{Welche:parseval}
\end{equation}
%
The rms value of the input data is just the square root of the 
above quantity.

The discrete frequency values are in units of the Nyquist 
critical frequency.
They are given by:
%
\begin{equation}
{\tt freqs[i]}=2.0*({\tt i}-\lfloor{\tt fftLength}/2\rfloor)/
{\tt fftLength}\ ,
\label{Welche:freq1}
\end{equation}
%
where
%
\begin{equation}
{\tt i}=0,1,\cdots,{\tt fftLength}-1\ .
\label{Welche:freq2}
\end{equation}
%
Note that {\tt psd[0]} and {\tt freqs[0]} correspond to the most
negative frequency value, not to zero frequency.
The values in the {\tt psd} and {\tt freqs} valarrays are arranged
in order of increasing frequency, starting with the most negative 
frequency value and increasing in steps of 
$2.0*{\tt i}/{\tt fftLength}$.
%
\item[{\tt operator()}:]
Another name for the {\tt apply} method.
\end{description}

\subsection{Operating Instructions}

\subsubsection{Example Usage}
\label{Welchss:usage}

{\footnotesize 
\begin{verbatim}

  // read in colored noise from a file (16384 samples)
  const size_t n = 16384;
  float data[n];
  ifstream inFile("WelchEstimateIn.dat");

  int i = 0;
  while (1) 
  {
    inFile >> data[i]; // must try to read in data before generating an
                       // end-of-file flag.  works ok if file is empty.  
    if (inFile.eof()) 
    {
      break;  
    }
    
    i++;
  }
  valarray<float> in(data, n);

  // construct welch estimate object to estimate the power spectrum
  // (fft length=1024, no overlap, kaiser window (beta=5), no detrending)
  KaiserWindow<float>  kw(5);
  WelchEstimate<float> welch(1024,0,kw,none);

  // valarrays to hold discrete frequency values and psd estimate
  valarray<float> psd;
  valarray<float> freqs;

  // calculate the psd estimate
  welch.apply(psd, freqs, in);

  // write discrete frequency values and psd estimate to a file 
  // (to be displayed later)
  ofstream outFile("WelchEstimateOut.dat");

  for (int i = 0; i < 1024; i++)
  {
    outFile << freqs[i] << "  " << psd[i] << endl;
  }
\end{verbatim}}

The Welch estimate power spectrum produced by the above code
is plotted in Fig.~\ref{f:WelchEstimateFig1}.
%
\begin{figure}[htb!]
\begin{center}
{\epsfig{file=WelchEstimateFig1.ps,
angle=-90,width = 4in,bbllx=25pt,bblly=50pt,bburx=590pt,bbury=740pt}}
\caption{\label{f:WelchEstimateFig1}
Welch method power spectrum estimate for colored noise.
The Welch estimate was defined by an fft length of 1024, no overlap, 
no detrending, and a Kaiser window with shape parameter $\beta = 5$.
The colored noise was produced by passing white noise through a low-pass
finite impulse response filter.
For more details and for comparison, see the Example section of 
{\tt psd} in the Matlab Signal Processing Toolbox \cite{Welchmatlab}.}
\end{center}
\end{figure}
%

\subsubsection{Options/Defaults}

A user can construct a {\tt WelchEstimate<T>} object in a number of
different ways:
%
\begin{itemize}
\item{}
The default constructor {\tt WelchEstimate()} constructs a 
Welch estimate object with an fft length equal to 
{\tt WelchEstimateDefaultFFTLength}, overlap length equal to zero,
a rectangular window, and no detrending.
The default fft length is defined in the header file
{\tt DataCondAPIConstants.hh}.
If the input data to the {\tt apply} or {\tt operator()} methods
has a length less than or equal to this default, the fft length is 
reduced to the length of the input data;
the resulting power spectrum estimate is then a periodogram.
%
\item{}
The constructor
{\footnotesize 
\begin{verbatim}
  explicit WelchEstimate(const size_t       fftLength,
                         const size_t       overlapLength,
                         const Window<T>&   window,
                         const detrendFlag& dFlag = none)
    throw (std::invalid_argument);
\end{verbatim}}
%
allows a user to construct a Welch estimate object with
specified values for the fft length, overlap length, window, and
detrend option.
To construct a valid object, the values of {\tt fftLength} and
{\tt overlapLength} are subject to the restrictions mentioned 
in Sec.~\ref{Welchss:description}.
(See also Sec.~\ref{Welchsss:exceptions_thrown} for information
about exceptions thrown.)
%
\item{}
As an alternative to using the Welch estimate constructor with 
arguments, a user can construct a default Welch estimate object
and then use the set methods ({\tt set\_fftLength}, 
{\tt set\_overlapLength}, {\tt set\_window}, and {\tt set\_dFlag}) 
to specify the fft length, overlap length, window, and detrend option.
As before, the values of the fft length and overlap length are 
subject to the restrictions mentioned in
Secs.~\ref{Welchss:description} and \ref{Welchsss:exceptions_thrown}.
\end{itemize}

\subsection{Exceptions}

\subsubsection{Exceptions Thrown}
\label{Welchsss:exceptions_thrown}

The only standard exception that {\tt WelchEstimate<T>} 
throws is {\tt invalid\_argument}.
An {\tt invalid\_argument} exception is thrown when:
%
\begin{itemize}
\item
the {\tt fftLength} argument of the {\tt WelchEstimate<T>} 
constructor is:
zero,
less than or equal to {\tt overlapLength},
greater than {\tt MaximumWindowLength}, or
greater than {\tt MaximumFFTLength},
where {\tt MaximumWindowLength} and {\tt MaximumFFTLength}
are the maximum allowed window and fft lengths defined in 
the header file {\tt DataCondAPIConstants.hh}.
The fft length should not exceed the maximum window length 
since a window automatically resizes itself to the length 
of a data segment (i.e., to the fft length) in the {\tt apply} 
and {\tt operator()} methods.
This implementation of Welch's method differs from that used 
in Matlab \cite{Welchmatlab}, where the only restriction on the 
window length is that it be less than or equal to the fft length.
%
\item
the argument of the {\tt set\_fftLength} method is:
zero,
less than or equal to the overlap length,
greater than {\tt MaximumWindowLength}, or
greater than {\tt MaximumFFTLength}.
(See the previous item for more details.)
%
\item
the argument of the {\tt set\_overlapLength} method is
greater than or equal to the fft length.
%
\item
the {\tt in} argument of the real or complex {\tt apply}
method has zero length (i.e., there is no input data).
%
\item
the {\tt in} argument of the real or complex {\tt operator()}
method has zero length (i.e., there is no input data).
%
\item
the {\tt in} argument of the real or complex {\tt apply}
method has length less than the fft length and less than or 
equal to the overlap length.  
This condition leads to an error exception because the fft 
length should be reduced to the length of input data, but this
new fft length is less than or equal to the overlap length.
%
\item
the {\tt in} argument of the real or complex {\tt operator()}
method has length less than the fft length and less than or 
equal to the overlap length.  
This condition leads to an error exception for the same 
reason as discussed above.
\end{itemize}

\subsubsection{Exceptions Caught}

None.

\subsection{Algorithms}
\label{ss:Welchalgorithms}

{\tt WelchEstimate<T>} estimates power spectra by averaging 
modified periodograms of (possibly) overlapping segments of the 
input data \cite{WelchP&W,WelchO&S}.
The basic algorithm is as follows:
%
\begin{enumerate}
\item
Apply the chosen window to each successive detrended segment 
of the input data.
(Note: the window automatically resizes itself to the length 
of the data segment, which is equal to the chosen fft length.)
%
\item
Take the DFT of the window data segment.
(See the {\tt FFT} class for details about discrete Fourier
transforms.)
%
\item
Form the periodogram of each windowed segment of data by 
scaling the absolute value squared of the resulting DFT with
the inverse of the sum of squares of the window.
%
\item
Average the periodograms of the overlapping segments to get
the power spectrum estimate.
\end{enumerate}
%
The number of sections averaged is
%
\begin{equation}
{\tt numSections} 
= {\tt floor}
\left({{\tt dataLength} - {\tt overlapLength}\over
       {\tt fftLength}  - {\tt overlapLength}}\right)\ .
\end{equation}
%
The normalization of the power spectrum estimate and the discrete 
frequency values are given by 
Eqs.~(\ref{Welche:parseval})-(\ref{Welche:freq2}).

\subsection{Accuracy}

The accuracy of the {\tt apply} and {\tt operator()} methods is 
determined by the template type {\tt T}:
If {\tt T} is {\tt float}, the accuracy is single precision.
If {\tt T} is {\tt double}, the accuracy is double precision.

\subsection{Performance}

The following performance statistics will be generated during
the datacondAPI Mock Data challenge:
%
\begin{itemize}
\item 
Figures or tables showing CPU time required to calculate 
a Welch method power spectrum estimate as a function of the 
length of the input data.
The length of the data will be in powers of 4 starting 
from $2^{10}$ samples and ranging to $2^{20}$ samples.
Times to be measured are average time at the Tcl layer and 
average time at the {\tt WelchEstimate.apply()\/} invocation.
The Welch method estimate will consist of splitting the input
data array into 7 equal length overlapping segments, calculating 
windowed periodograms for each detrended segment (mean removed), 
and then averaging the resulting periodograms.
A Hann window will be used.
\item 
Figures or tables showing CPU time required to calculate 
a Welch method power spectrum estimate as a function of the 
number of channels processed, for fixed length input data.
The number of channels will be in multiples of 5 starting from
1 channel and ranging to 30 channels.
The length of the input data will start at $2^{10}$ samples 
and range to $2^{20}$ samples, in powers of 4. 
Times to be measured are average time at the Tcl layer and 
average time at the {\tt WelchEstimate.apply()\/} invocation.
The Welch method estimate will be calculated as described 
above.
\end{itemize}

\subsection{Testing}

{\tt WelchEstimate<T>} was tested by the code contained in 
{\tt tPSDEstimate.cc}.
The following tests were performed:
%
\begin{itemize}
\item
test that an {\tt invalid\_argument} exception is thrown
by the Welch estimate constructor when the {\tt fftLength}
argument is:
zero,
less than or equal to {\tt overlapLength},
greater than {\tt MaximumWindowLength}, or
greater than {\tt MaximumFFTLength},
where {\tt MaximumWindowLength} and {\tt MaximumFFTLength}
are the maximum allowed window and fft lengths, defined in 
the header file {\tt DataCondAPIConstants.hh}.
%
\item
test that an {\tt invalid\_argument} exception is thrown by the 
{\tt set\_fftLength} method when the {\tt fftLength} argument is:
zero,
less than or equal to {\tt overlapLength},
greater than {\tt MaximumWindowLength}, or
greater than {\tt MaximumFFTLength}.
%
\item
test that an {\tt invalid\_argument} exception is thrown by the 
{\tt set\_overlapLength} method when the
{\tt overlapLength} argument is greater than or equal to the
fft length.
%
\item
test that an {\tt invalid\_argument} exception is thrown 
by the real and complex {\tt apply} methods when the {\tt in}
argument has zero length (i.e., there is no input data).
%
\item
test that an {\tt invalid\_argument} exception is thrown 
by the real and complex {\tt operator()} methods when the {\tt in}
argument has zero length (i.e., there is no input data).
%
\item
test that an {\tt invalid\_argument} exception is thrown 
by the real and complex {\tt apply} methods when the {\tt in}
argument has length less than the fft length and less than or 
equal to the overlap length.  
%
\item
test that an {\tt invalid\_argument} exception is thrown 
by the real and complex {\tt operator()} methods when the {\tt in}
argument has length less than the fft length and less than or 
equal to the overlap length.  
%
\item
test that the default constructor produces a valid Welch estimate 
object with the expected fft length, overlap length, window, and 
detrend option.
%
\item
test that the Welch estimate constructor with arguments can used 
to be used to 
produce valid Welch estimate objects with specified fft length, 
overlap length, window, and detrend option.
%
\item
test that one can use the {\tt set\_fftLength} method to modify
the fft length of a valid Welch estimate object.
%
\item
test that one can use the {\tt set\_overlapLength} method to modify
the overlap length of a valid Welch estimate object.
%
\item
test that one can use the {\tt set\_window} method to modify the 
window of a valid Welch estimate object.
%
\item
test that one can use the {\tt set\_dFlag} method (a public
method defined in {\tt PSDEstimate<T>}) to modify the detrend option
of a valid Welch estimate object.
%
\item
test that the {\tt apply} and {\tt operator()} methods produce 
expected results for the power spectrum estimate and discrete
frequency values when acting on real data of length 8.
The following Welch estimate objects were tested:
%
\begin{itemize}
\item
fft length = 8, overlap length = 0, rectangular window, all three 
types of detrending.
\item
fft length = 4, overlap length = 0, rectangular window, all three 
types of detrending.
\item
fft length = 4, overlap length = 2, rectangular window, all three 
types of detrending.
\item
fft length = 4, overlap length = 2, Hann window, all three 
types of detrending.
\item
fft length = 4, overlap length = 2, Kaiser window ($\beta = 3$), 
all three types of detrending.
\end{itemize}
%
\item
test that the {\tt apply} and {\tt operator()} methods produce 
expected results for the power spectrum estimate and discrete
frequency values when acting on complex data of length 8.
The following Welch estimate objects were tested:
%
\begin{itemize}
\item
fft length = 8, overlap length = 0, rectangular window, all three 
types of detrending.
\item
fft length = 5, overlap length = 0, rectangular window, all three 
types of detrending. 
(Note that fft length is odd.)
\item
fft length = 5, overlap length = 3, rectangular window, all three 
types of detrending.
(Note that fft length and overlap length are both odd.)
\item
fft length = 5, overlap length = 3, Hann window, all three 
types of detrending.
\item
fft length = 5, overlap length = 3, Kaiser window ($\beta = 3$), 
all three types of detrending.
\end{itemize}
%
\item
test that the {\tt apply} and {\tt operator()} methods produce 
expected results for the power spectrum estimate and discrete
frequency values when acting on real data of length 1024.
This data is read in from a file and consists of 1024 random
samples of a normal distribution (mean = 0, variance = 1) 
generated with Matlab.
The Welch estimate tested had fft length = 256, 
overlap length = 128, a Hann window, and mean detrending.
\end{itemize}
%
%
When testing {\tt WelchEstimate<T>}, we compared the output of the
{\tt apply} and {\tt operator()} methods with results previously 
calculated with Matlab.
The output of the {\tt WelchEstimate<T>} methods was said to be 
in agreement with the results produced by Matlab if the absolute 
value of the difference between output and result was less than
or equal to $10^{-4}$.
{\tt WelchEstimate<T>} passed all of the above tests.

\subsection{Notes}

\begin{itemize}
\item
Since Matlab's definition of a Hann window disagrees with the 
definition given in {\tt HannWindow<T>}, we had to write our own
Matlab code to generate the appropriate Hann window before we
could perform the tests that used a Hann window.
(See {\tt HannWindow<T>} for details.)
\end{itemize}

\subsection{See Also}

\begin{itemize}
\item 
{\tt class PSDEstimate<T>}
\item 
{\tt class Window<T>}
\item
{\tt class WindowInfo}
\item
{\tt class RectangularWindow<T>}
\item
{\tt class HannWindow<T>}
\item
{\tt class KaiserWindow<T>}
\item
{\tt class FFT}
\end{itemize}

\begin{thebibliography}{99}

\bibitem{WelchP&W}
D.B.\ Percival and A.T.\ Walden,
{\em Spectral Analysis for Physical Applications\/},
(Cambridge, Cambridge, 1993).

\bibitem{WelchO&S}
A.V.\ Oppenheim and R.W. Schafer,
{\em Discrete-Time Signal Processing\/},
(New Jersey, Prentice-Hall, 1999).

\bibitem{Welchmatlab} 
The MathWorks: {\tt http:\slash\slash www.mathworks.com}.

\end{thebibliography}

\end{document}
