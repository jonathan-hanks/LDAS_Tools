#include "datacondAPI/config.h"

#include <sstream>
   
#include "PrecisionConversion.hh"
#include "ScalarUDT.hh"

#include "FloatFunction.hh"

using namespace std;   
   
//
// FloatFunction class
//

namespace {

    const FloatFunction staticFloatFunction;

}

FloatFunction::FloatFunction()
  : CallChain::Function(FloatFunction::GetName())
{
}

const std::string& FloatFunction::GetName() const
{
  static const std::string name("float");

  return name;
}

void FloatFunction::Eval(CallChain* Chain,
                          const CallChain::Params& Params,
                          const std::string& Ret) const
{
  if (Params.size() != 1)
  {
    throw CallChain::BadArgumentCount(GetName(), 1, Params.size());
  }

  //
  // Existence-testing for symbol is turned off in case the argument
  // is a literal number
  datacondAPI::udt* in = Chain->GetSymbol(Params[0], false);
  datacondAPI::udt* out = 0;
    
  if (in == 0)
  {
    // The symbol doesn't exist - it might be a literal number
    // so attempt to convert it to a float
    istringstream istr(Params[0]);
    float value = 0;
    float next_value = 0;
 
    // Need to check that there's only one number to read in the
    // stream, so read the next one as well - should give 0
    // Note that comparing a stream to zero tells you whether
    // the stream is good or bad.
    if (((istr >> value) != 0) && ((istr >> next_value) == 0))
    {
      out = new datacondAPI::Scalar<float>(value);
    }
    else
    {
      throw CallChain::BadSymbol(Params[0]);
    }
  }
  else // the symbol exists - convert it
  {
      out = datacondAPI::convert_to_float(*in);
  }

  Chain->ResetOrAddSymbol(Ret, out);
}
