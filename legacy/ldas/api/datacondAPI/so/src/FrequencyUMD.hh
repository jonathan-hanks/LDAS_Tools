/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef FREQUENCY_UMD_HH
#define FREQUENCY_UMD_HH
// $Id: FrequencyUMD.hh,v 1.12 2006/02/16 16:54:58 emaros Exp $

#include "UDT.hh"

namespace ILwdFCS
{
  class FrProcData;
}

namespace datacondAPI
{
  
  //: metadata specifying the properties of a frequency sequence
  class FrequencyMetaData
  {
    
  public:
    
    // accessors

    //: Frequency associated with element i of series
    //!param: i - element number whose frequency we are interested in
    //!return: double - Frequency (Hz)
    double GetFrequency(const unsigned int i) const;

    //: Increment in frequency between successive elements of series
    //!return: double - frequency increment (Hz)
    double GetFrequencyDelta() const;
    
    //: Set frequency associated with first element of series
    //!param: f0 - frequency associated with first element of series (Hz)
    void SetFrequencyBase(const double& f0);

    //: Set frequency increment between successive elements of series
    //!param: df - frequency increment between 
    //+ successive elements of series (Hz)
    void SetFrequencyDelta(const double& df);
    
    // destructor

    //: Destructor
    virtual ~FrequencyMetaData();
    
    // output
    
    //: Convert *this to an appropriate ilwd
    //!param: Chain - CallChain *this is part of
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasElement* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasElement* 
    ConvertToIlwd( const CallChain& Chain, 
		   datacondAPI::udt::target_type Target 
		   = datacondAPI::udt::TARGET_GENERIC ) const;

  protected:
    
    // constructors
    
    //: Default constructor
    //!todo: Why protected?
    FrequencyMetaData();

    //: Construct from initial frequency and increment
    //!todo: Why protected?
    FrequencyMetaData(const double& f0, const double& df);

    //: Copy constructor
    //!todo: Why protected?
    FrequencyMetaData(const FrequencyMetaData& in);
    
    //: Store meta
    //!param: ILwd::LdasContainer& Storage - place to store values
    //!param: datacondAPI::udt::target_type Target - target type
    //!param: Number of data points
    void store( ILwd::LdasContainer& Storage,
		datacondAPI::udt::target_type Target,
		unsigned int Size ) const;
    
  private:
    
    // data
    
    double m_f0;
    double m_df;
    
  }; // class FrequencyMetaData
  
} // namespace datacondAPI

#endif // FREQUENCY_UMD_HH
