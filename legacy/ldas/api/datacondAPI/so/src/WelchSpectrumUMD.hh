/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef WELCH_SPECTRUM_UMD_HH
#define WELCH_SPECTRUM_UMD_HH
// $Id: WelchSpectrumUMD.hh,v 1.11 2006/02/16 16:54:58 emaros Exp $

#include "UDT.hh"
#include "WindowInfo.hh"

namespace datacondAPI
{
  
  //: Metadata associated with Welch estimated power spectral densities
  class WelchSpectrumUMD
  {
    
  public:
    
    // constructors         

    //: Default constructor
    WelchSpectrumUMD();
    
    //: Copy constructor
    //!param: value - object to construct copy from
    WelchSpectrumUMD(const WelchSpectrumUMD& value);
    
    // manipulators
    
    //: Channel name that this is a PSD of
    //!return: string - channel name
    std::string GetNameOfChannel() const;

    //: Set the name of the channel associated with this PSD
    //!param: Channel - name
    void SetNameOfChannel(const std::string& Channel);
    
    //: Base frequency of channel for which this is the PSD
    //!return: double - base frequency in Hz
    double GetBaseFrequency() const;

    //: Set the base frequency of channel for which this is the PSD
    //!param: frequency - base frequency of input channel in Hz
    void SetBaseFrequency(const double& frequency);
    
    //: Length of FFT used to make this estimate
    //!return: int - length 
    //!todo: This is inappropriate metadata and should be eliminated
    unsigned int GetFFTLength() const;

    //: Length of FFT used to make this estimate
    //!param: length - length of DFT used to make this estimate
    //!todo: This is inappropriate metadata and should be eliminated
    void SetFFTLength(const unsigned int& length);

    //: overlap used to generate this psd
    //!return: unsigned int - number of elements overlapped(?)
    //!todo: This is inappropriate metadata and should be eliminated
    unsigned int GetFFTOverlap() const;

    //: set the overlap used to generate this psd
    //!param: overlap - number of elements overlapped(?)
    //!todo: This is inappropriate metadata and should be eliminated
    void SetFFTOverlap(const unsigned int& overlap);
    
    //: The window used on the data from which this psd estimate was generated
    //!return: window - information on window used to generate estimate
    //!todo: This is inappropriate metadata and should be eliminated
    WindowInfo GetWindowInfo() const;

    //: Set info on window used on the data from which this psd 
    //+ estimate was generated
    //!param: window - information on window used to generate estimate
    //!todo: This is inappropriate metadata and should be eliminated
    void SetWindowInfo(const WindowInfo& window);
    
    //: Create ILWD representation of *this
    //!param: Chain - CallChain *this is part of
    //!param: Target - intended destination for ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual ILwd::LdasElement* 
    ConvertToIlwd( const CallChain& Chain, 
		   datacondAPI::udt::target_type Target 
		   = datacondAPI::udt::TARGET_GENERIC ) const;
    
  protected:
    
    //: destructor
    //!todo: why is destructor protected!?
    virtual ~WelchSpectrumUMD();
    
  private:

    // Information about the channel
    std::string  m_channel;
    // Frequency to which the input channel was mixed, in Hz
    double       m_frequency;

    unsigned int m_FFTLength;
    unsigned int m_FFTOverlap;
    WindowInfo m_WindowInfo;
    
  }; // class WelchSpectrumUMD
  
  inline std::string WelchSpectrumUMD::
  GetNameOfChannel() const
  {	
    return m_channel;
  }
  
  inline void WelchSpectrumUMD::
  SetNameOfChannel(const std::string& Channel)
  {
    m_channel = Channel;
  }
  
} // namespace datacondAPI

#endif // WELCH_SPECTRUM_UMD_HH
