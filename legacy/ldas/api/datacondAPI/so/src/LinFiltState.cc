/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "datacondAPI/config.h"

#include <string>
#include <complex>

#include "general/Memory.hh"

#include <filters/LinFilt.hh>
#include <filters/valarray_utils.hh>

#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>

#include "LinFiltState.hh"
#include "ExpandPoly.hh"


UDT_CLASS_INSTANTIATION(LinFiltState, UDT_DEFAULT_KEY )

namespace datacondAPI {

  void LinFiltState::checkAB() const
  {
    if (m_a.size() == 0)
    {
      throw std::invalid_argument("LinFiltState: a.size() == 0");
    }

    if (m_b.size() == 0)
    {
      throw std::invalid_argument("LinFiltState: b.size() == 0");
    }

    if (m_a[0] == 0)
    {
      throw std::invalid_argument("LinFiltState: a[0] == 0");
    }

    bool bZero = true;
    
    for (size_t k = 0; k < m_b.size(); ++k)
    {
      if (m_b[k] != 0)
      {
        bZero = false;
        break;
      }
    }

    if (bZero)
    {
      throw std::invalid_argument("LinFiltState: b == 0");
    }
  }

  LinFiltState::LinFiltState(const Sequence<double>& b, 
                             const Sequence<double>& a)
    : m_b(b), m_a(a), m_linfilt(0)
  {
    checkAB();
  }

  LinFiltState::LinFiltState(const Sequence<std::complex<double> >& zeroes, 
                             const Sequence<std::complex<double> >& poles,
                             const double& gain)
    : m_linfilt(0)
  {
    expandPoly(m_b, zeroes, gain);
    expandPoly(m_a, poles, 1.0);

    checkAB();
  }
  
  LinFiltState::LinFiltState(const udt& b, const udt& a)
    : m_b(0), m_a(0), m_linfilt(0)
  {
    if (udt::IsA<Sequence<double> >(b))
    {
      m_b = udt::Cast<Sequence<double> >(b);
    }
    else
    {
      std::string what = "LinFiltState: b must be a Sequence<double>"; 
      throw std::invalid_argument(what);
    }
    
    if (udt::IsA<Sequence<double> >(a))
    {
      m_a = udt::Cast<Sequence<double> >(a);
    }
    else
    {
      std::string what = "LinFiltState: a must be Sequence<double>";
      throw std::invalid_argument(what);
    }

    checkAB();
  }

  LinFiltState::LinFiltState(const LinFiltState& state)
    : State(state),
      m_b(state.m_b), m_a(state.m_a), m_linfilt(0)
  {
    if (state.m_linfilt.get() != 0)
    {
      m_linfilt.reset(state.m_linfilt->Clone());
    }
  }

  LinFiltState::LinFiltState(const udt& b_or_state)
    : m_b(1.0, 1), m_a(1.0, 1), m_linfilt(0)
  {
    if (udt::IsA<LinFiltState>(b_or_state))
    {
      const LinFiltState& state = udt::Cast<LinFiltState>(b_or_state);

      State::operator=(state);

      m_a = state.m_a;
      m_b = state.m_b;

      if (state.m_linfilt.get() != 0)
      {
        m_linfilt.reset(state.m_linfilt->Clone());
      }
      else
      {
        m_linfilt.reset(0);
      }
    }
    else if (udt::IsA<Sequence<double> >(b_or_state))
    {
      m_b = udt::Cast<Sequence<double> >(b_or_state);
    }
    else
    {
      std::string what = "LinFiltState:: bstate not a Sequence<double> "
        "or LinFiltState";
      throw std::invalid_argument(what);
    }
  }
  
  LinFiltState::~LinFiltState()
  {
  }
  
  LinFiltState& LinFiltState::operator=(const LinFiltState& state)
  {
    if (this != &state)
    {
      State::operator=(state);

      m_b = state.m_b;
      m_a = state.m_a;

      if (state.m_linfilt.get() != 0)
      {
        m_linfilt.reset(state.m_linfilt->Clone());
      }
      else
      {
        m_linfilt.reset(0);
      }
    }
    
    return *this;
  }

  void LinFiltState::getB(Sequence<double>& b) const
  {
    b = m_b;
  }

  void LinFiltState::getA(Sequence<double>& a) const
  {
    a = m_a;
  }

  void LinFiltState::getSize(int& aSize, int& bSize)
  {
    aSize = m_a.size();
    bSize = m_b.size();
  }
  
  LinFiltState* LinFiltState::Clone() const
  {
    return new LinFiltState(*this);
  }
  
  template<class T>
  void LinFiltState::apply(std::valarray<T>& x)
  {
    Filters::LinFilt<double, T>* p
      = dynamic_cast<Filters::LinFilt<double, T>*>(m_linfilt.get());

    // Do we have the right type of state?
    if (p == 0)
    {
      // If this is the first call, create the initial resampler
      if (m_linfilt.get() == 0)
      {
        p = new Filters::LinFilt<double, T>(m_b, m_a);
        m_linfilt.reset(p);
      }
      else
      {
        throw std::invalid_argument("LinFiltState::apply - "
                               "Attempt to filter data where LinFiltState "
                               "was initialized with a different data type");
      }
    }

    p->apply(x);
  }

  template<class TOut, class TIn>
  void LinFiltState::apply(std::valarray<TOut>& y,
                           const std::valarray<TIn>& x)
  {
    // Copy input to output
    Filters::valarray_copy(y, x);

    // Filter in-place
    apply(y);
  }

  ILwd::LdasElement* LinFiltState::
  ConvertToIlwd( const CallChain& Chain,
                 udt::target_type Target ) const
  {
    std::unique_ptr<ILwd::LdasContainer> container(new ILwd::LdasContainer);
    
    Sequence<double>& b = const_cast<Sequence<double>&>(m_b);
    Sequence<double>& a = const_cast<Sequence<double>&>(m_a);

    container->push_back(new ILwd::LdasArray<double>(&b[0], b.size(), "b"),
                         ILwd::LdasContainer::NO_ALLOCATE,
                         ILwd::LdasContainer::OWN);
    container->push_back(new ILwd::LdasArray<double>(&a[0], a.size(), "a"),
                         ILwd::LdasContainer::NO_ALLOCATE,
                         ILwd::LdasContainer::OWN);

    if (const Filters::LinFilt<double, float>* const p
        = dynamic_cast<Filters::LinFilt<double, float>*>(m_linfilt.get()))
    {
      Sequence<double> zTmp;
      p->getZ(zTmp);
      container->push_back(new ILwd::LdasArray<double>(&zTmp[0],
                                                       zTmp.size(),
                                                       "z"),
                           ILwd::LdasContainer::NO_ALLOCATE,
                           ILwd::LdasContainer::OWN);
    }
    else if (const Filters::LinFilt<double, double>* const p
           = dynamic_cast<Filters::LinFilt<double, double>*>(m_linfilt.get()))
    {
      Sequence<double> zTmp;
      p->getZ(zTmp);
      container->push_back(new ILwd::LdasArray<double>(&zTmp[0],
                                                       zTmp.size(),
                                                       "z"),
                           ILwd::LdasContainer::NO_ALLOCATE,
                           ILwd::LdasContainer::OWN);
    }
    else if (const Filters::LinFilt<double, std::complex<float> >* const p
             = dynamic_cast<Filters::LinFilt<double, std::complex<float> >*>(m_linfilt.get()))
    {
      Sequence<std::complex<double> > zTmp;
      p->getZ(zTmp);
      container->push_back(new ILwd::LdasArray<std::complex<double> >
			   (&zTmp[0],
			    zTmp.size(),
			    "z"),
                           ILwd::LdasContainer::NO_ALLOCATE,
                           ILwd::LdasContainer::OWN);
    }
    else if (const Filters::LinFilt<double, std::complex<double> >* const p
             = dynamic_cast<Filters::LinFilt<double, std::complex<double> >*>(m_linfilt.get()))
    {
      Sequence<std::complex<double> > zTmp;
      p->getZ(zTmp);
      container->push_back(new ILwd::LdasArray<std::complex<double> >
			   (&zTmp[0],
			    zTmp.size(),
			    "z"),
                           ILwd::LdasContainer::NO_ALLOCATE,
                           ILwd::LdasContainer::OWN);
    }

    return container.release();
  }

#undef INSTANTIATE
#define INSTANTIATE(OUT_TYPE) \
template void LinFiltState::apply(std::valarray< OUT_TYPE >&, \
                                  const std::valarray< OUT_TYPE >& )

  INSTANTIATE(float);
  INSTANTIATE(double);
  INSTANTIATE(std::complex<float>);
  INSTANTIATE(std::complex<double>);

} // namespace datacondAPI

