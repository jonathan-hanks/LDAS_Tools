#include "config.h"

#include <complex>
#include <sstream>
//#include <stdexcept>

#include "WaveletFunctions.hh"
#include "Wavelet.hh"
#include "Haar.hh"
#include "Biorthogonal.hh"
#include "Daubechies.hh"
#include "Symlet.hh"
#include "WaveletUDT.hh"
#include "ScalarUDT.hh"

//=======================================================================
// Wavelet w = Haar(int tree)
//=======================================================================

static HaarFunction _HaarFunction;

HaarFunction::
HaarFunction() : Function( HaarFunction::GetName() )
{
}

void HaarFunction::Eval(CallChain* Chain, 
			const CallChain::Params& Params,
			const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int tree = 0;
    
    switch(Params.size()){

    case 1:

      obj = Chain->GetSymbol(Params[0]);

      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  tree = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be an integer";
	  throw std::invalid_argument(oss.str());
	}
      
      break;
      
    case 0:
      tree = 0;
      break;

    default:
      tree = 0;
      throw CallChain::BadArgumentCount("Haar", 1, Params.size());
      break;
    }

    //: no matter what is the Haar<> template type: cast to the base class
    result = new wat::Haar<float>(tree);

    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& HaarFunction::
GetName(void) const
{
    static const std::string name("Haar");
    return name;
}


//=======================================================================
// Wavelet w = Biorthogonal(int order=4, 
//                          int tree=0, 
//                          enum BORDER border=B_POLYNOM)
// currently the border type can't be specified and set to default value
//=======================================================================

static BiorthogonalFunction _BiorthogonalFunction;

BiorthogonalFunction::
BiorthogonalFunction() : Function( BiorthogonalFunction::GetName() )
{
}

void BiorthogonalFunction::Eval(CallChain* Chain, 
			const CallChain::Params& Params,
			const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int order = 4;
    int tree = 0;
    enum wat::BORDER border = wat::B_POLYNOM;
    
    switch(Params.size()){

/*
    case 3:

      obj = Chain->GetSymbol(Params[2]);

      if(const enum wat::BORDER* const p = 
	 dynamic_cast<const enum wat::BORDER*>(obj)) 
	{
	  border = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be a border type";
	  throw std::invalid_argument(oss.str());
	}
*/
    case 2:

      obj = Chain->GetSymbol(Params[1]);

      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  tree = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be an integer";
	  throw std::invalid_argument(oss.str());
	}

    case 1:

      obj = Chain->GetSymbol(Params[0]);

      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  order = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be an integer";
	  throw std::invalid_argument(oss.str());
	}
      
      break;
      
    case 0:
      break;

    default:
      throw CallChain::BadArgumentCount("Biorthogonal", 3, Params.size());
      break;
    }

    //: no matter what is the Biorthogonal<> template type: cast to the base class
    result = new wat::Biorthogonal<float>(order,tree,border);

    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& BiorthogonalFunction::
GetName(void) const
{
    static const std::string name("Biorthogonal");
    return name;
}


//=======================================================================
// Wavelet w = Daubechies(int order=4, 
//                        int tree=0, 
//                        enum BORDER border=B_CYCLE)
// currently the border type can't be specified and set to default value 
//=======================================================================

static DaubechiesFunction _DaubechiesFunction;

DaubechiesFunction::
DaubechiesFunction() : Function( DaubechiesFunction::GetName() )
{
}

void DaubechiesFunction::Eval(CallChain* Chain, 
			const CallChain::Params& Params,
			const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int order = 4;
    int tree = 0;
    enum wat::BORDER border = wat::B_CYCLE;
    
    switch(Params.size()){

/*
    case 3:

      obj = Chain->GetSymbol(Params[2]);

      if(const enum wat::BORDER* const p = 
	 dynamic_cast<const enum wat::BORDER*>(obj)) 
	{
	  border = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be a border type";
	  throw std::invalid_argument(oss.str());
	}
*/
    case 2:

      obj = Chain->GetSymbol(Params[1]);

      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  tree = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be an integer";
	  throw std::invalid_argument(oss.str());
	}

    case 1:

      obj = Chain->GetSymbol(Params[0]);

      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  order = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be an integer";
	  throw std::invalid_argument(oss.str());
	}
      
      break;
      
    case 0:
      break;

    default:
      throw CallChain::BadArgumentCount("Daubechies", 2, Params.size());
      break;
    }

    //: no matter what is the Daubechies<> template type: cast to the base class
    result = new wat::Daubechies<float>(order,tree,border);

    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& DaubechiesFunction::
GetName(void) const
{
    static const std::string name("Daubechies");
    return name;
}


//=======================================================================
// Wavelet w = Symlet(int order=4, 
//                        int tree=0, 
//                        enum BORDER border=B_CYCLE)
// currently the border type can't be specified and set to default value 
//=======================================================================

static SymletFunction _SymletFunction;

SymletFunction::
SymletFunction() : Function( SymletFunction::GetName() )
{
}

void SymletFunction::Eval(CallChain* Chain, 
			const CallChain::Params& Params,
			const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int order = 4;
    int tree = 0;
    enum wat::BORDER border = wat::B_CYCLE;
    
    switch(Params.size()){

/*
    case 3:

      obj = Chain->GetSymbol(Params[2]);

      if(const enum wat::BORDER* const p = 
	 dynamic_cast<const enum wat::BORDER*>(obj)) 
	{
	  border = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be a border type";
	  throw std::invalid_argument(oss.str());
	}
*/
    case 2:

      obj = Chain->GetSymbol(Params[1]);

      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  tree = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be an integer";
	  throw std::invalid_argument(oss.str());
	}

    case 1:

      obj = Chain->GetSymbol(Params[0]);

      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  order = p->GetValue();
	}

      else 
	{
	  std::ostringstream oss;  
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be an integer";
	  throw std::invalid_argument(oss.str());
	}
      
      break;
      
    case 0:
      break;

    default:
      throw CallChain::BadArgumentCount("Symlet", 3, Params.size());
      break;
    }

    //: no matter what is the Symlet<> template type: cast to the base class
    result = new wat::Symlet<float>(order,tree,border);

    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& SymletFunction::
GetName(void) const
{
    static const std::string name("Symlet");
    return name;
}

// ********************************************************
// Wavelet Forward Transform action:
// y = WaveletForward(TimeSeries &ts, Wavelet &w, int n)
//  y - output WaveletUDT object
// ts - input time series
//  w - Wavelet transform
//  n - number of steps
// ********************************************************
 
static WaveletForwardFunction _WaveletForwardFunction;

WaveletForwardFunction::
WaveletForwardFunction() : Function( WaveletForwardFunction::GetName() )
{
}

void WaveletForwardFunction::Eval(CallChain* Chain, 
				  const CallChain::Params& Params,
				  const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int level = -1;
    wat::Wavelet* pwave;
    
    switch(Params.size()){

    case 3:

      obj = Chain->GetSymbol(Params[2]);
 
      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  level = p->GetValue();
	}

      else {
	
	std::ostringstream oss;  
	oss << "Illegal argument to " << GetName()
	    << " - argument #3 must be an integer";
	throw std::invalid_argument(oss.str());
      }
      
      // Fall through
      
    case 2:

// input Wavelet

      obj = Chain->GetSymbol(Params[1]);

      if(const wat::Wavelet* const pw = dynamic_cast<const wat::Wavelet*>(obj)) 
      {
	 pwave = const_cast<wat::Wavelet *>(pw);
      }
      else {
	
	std::ostringstream oss;
	oss << "Illegal argument to " << GetName()
	    << " - argument #2 must be a Wavelet";
	throw std::invalid_argument(oss.str());
      }
	
// input TimeSeries
	
      obj = Chain->GetSymbol(Params[0]);

      if (const TimeSeries<float>* const p
	  = dynamic_cast<const TimeSeries<float>*>(obj))
	{
	  WaveletUDT<float>* out = new WaveletUDT<float>(*p, *pwave);
	  out->Forward(level);
	  result = out;
	}
      
      else if (const TimeSeries<double>* const p
	       = dynamic_cast<const TimeSeries<double>*>(obj))
	{
	  WaveletUDT<double>* out = new WaveletUDT<double>(*p, *pwave);
	  out->Forward(level);
	  result = out;
	}
      
      else
        {
	  std::ostringstream oss;
	  oss << "Illegal argument to " << GetName()
	      << " - argument #1 must be a time series";
	  throw std::invalid_argument(oss.str());
        }

      break;
      
    default:
      throw CallChain::BadArgumentCount("WaveletForward", 2, Params.size());
      break;
    }
    
    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& WaveletForwardFunction::
GetName(void) const
{
    static const std::string name("WaveletForward");
    return name;
}


// ********************************************************
// Wavelet Inverse Transform action:
// y = WaveletInverse(WaveletUDT &x)
//  y - output TimeSeries object
//  x - input wavelet series
// ********************************************************
 
static WaveletInverseFunction _WaveletInverseFunction;

WaveletInverseFunction::
WaveletInverseFunction() : Function( WaveletInverseFunction::GetName() )
{
}

void WaveletInverseFunction::Eval(CallChain* Chain, 
				  const CallChain::Params& Params,
				  const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    switch(Params.size()){

    case 1:
	
// input WaveletUDT
	
      obj = Chain->GetSymbol(Params[0]);

      if (const WaveletUDT<float>* const p
	  = dynamic_cast<const WaveletUDT<float>*>(obj))
	{
	  WaveletUDT<float> *wd = new WaveletUDT<float>(*p);
	  wd->Inverse();

	  TimeSeries<float>* out = 
	    new TimeSeries<float>((TimeSeries<float>) *wd);

	  result = out;
	  delete wd;
	}
      
      else if (const WaveletUDT<double>* const p
	  = dynamic_cast<const WaveletUDT<double>*>(obj))
	{
// create working copy of WaveletUDT
	  WaveletUDT<double> *wd = new WaveletUDT<double>(*p);
	  wd->Inverse();

	  TimeSeries<double>* out = 
	    new TimeSeries<double>((TimeSeries<double>) *wd);

	  result = out;
	  delete wd;
	}

      else
        {
	  std::ostringstream oss;
	  oss << "Illegal argument to " << GetName()
	      << " - argument must be a WaveletUDT";
	  throw std::invalid_argument(oss.str());
        }

      break;
      
    default:
      throw CallChain::BadArgumentCount("WaveletInverse", 1, Params.size());
      break;
    }
    
    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& WaveletInverseFunction::
GetName(void) const
{
    static const std::string name("WaveletInverse");
    return name;
}


// ********************************************************
// get layer action:
// y = GetLayer(WaveletUDT &x, int n)
//  y - output Sequence series
//  x - input wavelet series
//  n - layer number (default: 0)
// ********************************************************
 
static GetLayerFunction _GetLayerFunction;

GetLayerFunction::
GetLayerFunction() : Function( GetLayerFunction::GetName() )
{
}

void GetLayerFunction::Eval(CallChain* Chain, 
				  const CallChain::Params& Params,
				  const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int layer = 0;

    switch(Params.size()){
    
    case 2:            // input layer number

      obj = Chain->GetSymbol(Params[1]);
 
      if(const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(obj)) 
	{
	  layer = p->GetValue();
	}

      else {
	
	std::ostringstream oss;  
	oss << "Illegal argument to " << GetName()
	    << " - argument #2 must be an integer";
	throw std::invalid_argument(oss.str());
      }
      
      // Fall through
      

    case 1:            // input WaveletUDT
	
      obj = Chain->GetSymbol(Params[0]);

      if (WaveletUDT<float>* p
	  = dynamic_cast<WaveletUDT<float>*>(obj))
	{
	  Sequence<float>* out = new Sequence<float>(1);
	  p->getLayer(*out, layer);

	  result = out;
	}
      
      else if (WaveletUDT<double>* p
	  = dynamic_cast<WaveletUDT<double>*>(obj))
	{
	  Sequence<double>* out = new Sequence<double>(1);
	  p->getLayer(*out, layer);

	  result = out;
	}

      else
        {
	  std::ostringstream oss;
	  oss << "Illegal argument to " << GetName()
	      << " - argument #1 must be a WaveletUDT";
	  throw std::invalid_argument(oss.str());
        }

      break;
      
    default:
      throw CallChain::BadArgumentCount("GetLayer", 2, Params.size());
      break;
    }
    
    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& GetLayerFunction::
GetName(void) const
{
    static const std::string name("GetLayer");
    return name;
}



















