#ifndef ARMODEL_HH
#define ARMODEL_HH

// $Id: ARModel.hh,v 1.9 2003/02/28 00:57:15 emaros Exp $

#include <valarray>
#include <stdexcept>

#include "StateUDT.hh"

namespace datacondAPI
{

    class ARModel : public State
    {

    public:

	// default overrides


	//: Default constructor
	ARModel();

	//: Copy constructor
	//!param: ar - Original
	ARModel(const ARModel& ar);

	//: Destructor
	~ARModel();

	//: Assignment operator
	//!param: ar - Original
	//!return: reference to self
	ARModel& operator=(const ARModel& ar);

	// UDT interface

	//: Deep copy (UDT functionality)
	//!return: pointer to deep copy
	ARModel* Clone() const;

	ILwd::LdasElement*
	    ConvertToIlwd(const CallChain& Chain,
			  udt::target_type Target = udt::TARGET_GENERIC) const;

	// custom constructors

	//: Construct model of a particular order
	//!param: n - model order
	explicit ARModel(const int& n);

	//: Construct a model from its coefficents
	//!param: theta - model coefficients
	template<typename type>
	explicit ARModel(const std::valarray<type>& theta);

	//: Construct a model from a UDT (either order or coefficents)
	//!param: n_or_theta - scalar order or array of coeffcients
	explicit ARModel(const udt& n_or_theta);

	//: Construct a model of a particular order fitted to given system output
	//!param: x - time series of model output
	//!param: n - desired model order
	template<typename type>
	ARModel(const std::valarray<type>& x, const int& n);

	//: Construct a model of a particular order fitted to given system output
	//!param: x - time series of model output
	//!param: n - desired model order
	ARModel(const udt& x, const udt& n);

	// accessors

	//: Get the model order
	//!return: model order
	int getOrder() const;

	//: Get the model order
	//!param: order - pointer to scalar integer (may be null on input)
	void getOrder(udt*& order) const;

	//: Get the model coefficents
	//!param: theta - reference to output array
	template<typename type>
	void getTheta(std::valarray<type>& theta) const;

	//: Get the model coefficents
	//!param: theta - pointer to output array (may be null on input)
	void getTheta(udt*& theta) const;

	//: Get the FIR filter coefficents corresponding to the model
	//!param: filter - pointer to output array (may be null on input)
	void getFilter(udt*& filter) const;

	// mutators

	//: Set the model order (destroys any existing model)
	//!param: n - new model order
	void setOrder(const int& n);
	
	//: Set the model order (destroys any existing model)
	//!param: n - new model order (scalar integer)
	void setOrder(const udt& n);

	//: Set the model coefficents (destroys any existing model)
	//!param: theta - new model coefficents
	template<typename type>
	void setTheta(const std::valarray<type>& theta);

	//: Set the model coefficents (destroys any existing model)
	//!param: theta - new model coefficents (array)
	void setTheta(const udt& theta);

	// functionality

	//: Apply the model to system output, producing a prediction.  A model must be estimated before calling.
	//!param: y - model prediction
	//!param: x - system output (model input)
	template<typename type>
	void apply(std::valarray<type>& y,
		   const std::valarray<type>& x) const;

	//: Apply the model to system output, producing a prediction.  A model must be estimated before calling.
	//!param: y - model prediction
	//!param: x - system output (model input)
	void apply(udt*& y,
		   const udt& x) const;

	//: (Re)fit the model to (additional) system output x.  A model order must be set before calling.
	//!param: x - system output (model input)
	template<typename type>
	void refine(const std::valarray<type>& x);

	//: (Re)fit the model to (additional) system output x.  A model order must be set before calling.
	//!param: x - system output (model input)
	void refine(const udt& x);

	//: Compute model's merit as the least-squares norm of the difference between the system output and prediction for given system output (model input).
	//!param: x - system output (model input)
	//!return: model merit
	template<typename type>
	type merit(const std::valarray<type>& x) const;

	//: Compute model's merit as the least-squares norm of the difference between the system output and prediction for given system output (model input).
	//!param: m - (scalar) model merit (may be null on input)
	//!param: x - system output (model input)
	void merit(udt*& m, const udt& x) const;

    private:

	class Abstraction;
	template<typename type> class Implementation;

	int m_order;
	mutable Abstraction* m_data;

    };

} // namespace datacondAPI

#endif // ARMODEL_HH
