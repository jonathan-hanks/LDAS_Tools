/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: Resample2Function.cc,v 1.5 2007/01/24 17:51:28 emaros Exp $
//
// "resample2" action
//
// [y = ] resample2(x, srate[, z])
// [y = ] resample2(x, srate, n[, z])
// [y = ] resample2(x, srate, n, beta[, z])
//
// or
//
// [y = ] resample2(x, z)
//
// srate:
//    target sampling rate
// x: TimeSeries<T> where T is float, double, complex<float>, complex<double>
//    input sequence
// y: TimeSeries<T> where T is float, double, complex<dloat>, complex<double>
//    output sequence
// z: any object or undefined on input
//    optional state, may be undefined on input
//
#include "datacondAPI/config.h"

#include <memory>
#include <string>   
   
#include "general/Memory.hh"

#include "Resample2Function.hh"
#include "Resample.hh"
#include "TimeSeries.hh"
#include "TypeInfo.hh"

using std::unique_ptr;   
using std::floor;
using std::string;   
   
namespace {
  
  // Find integers p, q such that p/q is close to r
  void estimateResamplingFactor(const double& newSrate,
                                const double& oldSrate,
                                int& p,
                                int& q)
  {
    static const double TOLERANCE = 1.0e-12;

    if ((newSrate <= 0) || (oldSrate <= 0))
    {
      throw std::runtime_error("estimateResamplingFactor: "
                               "sample rates must be strictly positive");
                               
    }

    // Initial guess of p and q
    p = 1;
    q = 1;

    double diff = std::abs(p*oldSrate/q - newSrate);
    
    // Run through combinations of p/q
    while (diff > TOLERANCE)
    {
      ++q;
      // as a guess for p, round to nearest integer
      p = (int) floor(q*newSrate/oldSrate + 0.5);
      diff = std::abs(p*oldSrate/q - newSrate);
    }

  }
  
  
  // Static instance
  Resample2Function staticResample2Function;

} // namespace

const std::string& Resample2Function::GetName() const
{
  static const std::string name("resample2");

  return name;
}

Resample2Function::Resample2Function()
  : Function(Resample2Function::GetName())
{
}

void Resample2Function::Eval(CallChain* chain,
                             const CallChain::Params& params,
                             const std::string& ret) const
{
  using namespace datacondAPI;

  const size_t nParams = params.size();

  if ((nParams < 2) || (nParams > 5))
  {
    throw CallChain::BadArgumentCount( GetName(), "2-5", nParams);
  }

  // Storage for the final result
  udt* yUdt = 0;

  // Arg0 must always be a TimeSeries, so we can get it's sampling rate
  const udt* const xUdt = chain->GetSymbol(params[0]);
  double oldSrate = 0;

  if (const TimeSeriesMetaData* tsMD
      = dynamic_cast<const TimeSeriesMetaData*>(xUdt))
  {
    oldSrate = tsMD->GetSampleRate();
  }
  else
  {
    throw CallChain::BadArgument(GetName(),
                                 0,
                                 TypeInfoTable.GetName(typeid(*xUdt)),
                                 "TimeSeries<T>");
  }
  
  // Now process arg1 - must be either a state or the srate
  udt* const arg1 = chain->GetSymbol(params[1]);
  ResampleState* const zPtr = dynamic_cast<ResampleState*>(arg1);

  if ((nParams == 2) && (zPtr != 0))
  {
      // it is the y = resample2(x, z) case
      Resample r(*zPtr);
      
      r.apply(yUdt, *xUdt);
      r.getState(*zPtr);
  }
  else 
  {
    // Either nParams > 2, or nParams == 2 but arg1 is not a state.
    // Must be the y = resample2(x, srate, ...) case, and we
    // need to construct a new ResampleState
    //
    // Note that when resample2() is invoked in this form we don't care what
    // the "state" parameter currently is, or whether it is defined. It is
    // an output-only parameter that will be created or reset.

    double newSrate = 0;
    if (const Scalar<double>* const sratePtr
        = dynamic_cast<const Scalar<double>*>(arg1))
    {
      newSrate = sratePtr->GetValue();
    }
    else
    {
      if (nParams == 2)
      {
        throw CallChain::BadArgument(GetName(),
                                     1,
                                     TypeInfoTable.GetName(typeid(*arg1)),
                                     "Scalar<double> or ResampleState");
      }
      else
      {
        throw CallChain::BadArgument(GetName(),
                                     1,
                                     TypeInfoTable.GetName(typeid(*arg1)),
                                     "Scalar<double>");
      }
    }

    // Storage for the calculated values of p and q
    int p = 0;
    int q = 0;
    
    // Storage for the constructed state variable
    unique_ptr<ResampleState> z(0);
  
    // If state output is requested, this is the symbol name used.
    // It also acts as the flag to tell us whether or not the state
    // should be saved - if it is non-null, the user requested that
    // the state be updated.
    string zSymbolName("");

    // This calculates the approximate p and q, where p/q = newSrate/oldSrate
    estimateResamplingFactor(newSrate, oldSrate, p, q);
    
    if (nParams == 2)
    {
      // y = resample2(x, srate)
      z.reset(new ResampleState(p, q));
    }
    else if (nParams == 3)
    {
      // y = resample2(x, srate, n) or y = resample2(x, srate, z)
      
      // Note - use "false" because z need not have already been defined
      // ie. the third parameter string might not have a UDT attached to it
      const udt* const arg2 = chain->GetSymbol(params[2], false);

      // n parameter
      if (const Scalar<int>* const nPtr
          = dynamic_cast<const Scalar<int>*>(arg2))
      {
        const int n = nPtr->GetValue();
        z.reset(new ResampleState(p, q, n));
      }
      else
      {
        // params[2] must have been the z param, use default n instead
        zSymbolName = params[2];
        z.reset(new ResampleState(p, q));
      }
    }
    else if (nParams == 4)
    {
      // y = resample2(x, srate, n, beta) or y = resample2(x, srate, n, z)
      const udt* const arg2 = chain->GetSymbol(params[2]);

      // n parameter, must be provided
      int n = 0;
      if (const Scalar<int>* nPtr = dynamic_cast<const Scalar<int>*>(arg2))
      {
        n = nPtr->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(),
                                     2,
                                     TypeInfoTable.GetName(typeid(*arg2)),
                                     "Scalar<int>");
      }

      // again, need not have a UDT attached to it
      const udt* const arg3 = chain->GetSymbol(params[3], false);

      // beta parameter
      if (const Scalar<double>* const betaPtr
          = dynamic_cast<const Scalar<double>*>(arg3))
      {
        const double beta = betaPtr->GetValue();
        z.reset(new ResampleState(p, q, n, beta));
      }
      else
      {
        // params[3] must have been the z param, use default beta instead
        zSymbolName = params[3];
        z.reset(new ResampleState(p, q, n));
      }
    }
    else // nParams == 5
    {
      // y = resample2(x, srate, n, beta, z)
      const udt* const arg2 = chain->GetSymbol(params[2]);

      int n = 0;
      double beta = 0;

      // n parameter, must be provided
      if (const Scalar<int>* const nPtr
          = dynamic_cast<const Scalar<int>*>(arg2))
      {
        n = nPtr->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(),
                                     2,
                                     TypeInfoTable.GetName(typeid(*arg2)),
                                     "Scalar<int>");
      }

      const udt* const arg3 = chain->GetSymbol(params[3]);

      // beta parameter
      if (const Scalar<double>* const betaPtr
          = dynamic_cast<const Scalar<double>*>(arg3))
      {
        beta = betaPtr->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(),
                                     3,
                                     TypeInfoTable.GetName(typeid(*arg3)),
                                     "Scalar<double>");
      }

      zSymbolName = params[4];
      z.reset(new ResampleState(p, q, n, beta));
    }

    Resample r(*z);

    r.apply(yUdt, *xUdt);

    if (zSymbolName != "")
    {
      r.getState(*z);
      chain->ResetOrAddSymbol( zSymbolName, z.release() );
    }
    
  }

  chain->ResetOrAddSymbol( ret, yUdt );
}

//-----------------------------------------------------------------------
// This method validates accessability of the parameters
//-----------------------------------------------------------------------
void Resample2Function::
ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		    const CallChain::Params& Params ) const
{
  //---------------------------------------------------------------------
  // Validation really needs to happen knowing the datatype. Since the
  //   datatype is not available, make the assumption that if the
  //   variable does not exist, then it is to be created.
  //---------------------------------------------------------------------
  switch( Params.size( ) )
  {
  case 2:
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    break;
  case 3:
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_WRITE );
    break;
  case 4:
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 3 ], PARAM_WRITE );
    break;
  case 5:
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 3 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 4 ], PARAM_WRITE );
    break;
  default:
    Function::ValidateParameters( SymbolTable, Params );
    break;
  }
}
