#ifndef FFTPLAN_H
#define FFTPLAN_H

#include <complex>

namespace datacondAPI {

    class PlanDBMgr {
    public:
	static size_t size();

	static void purge();
    };
    
    //
    // Single-precision real 1-D plan
    // 
    class RealFloatPlanRecord;

    class RealFloatFFTPlan {
    public:
	RealFloatFFTPlan(int n, bool isForward);

	~RealFloatFFTPlan();
	
	void apply(float* data) const;

    private:
	RealFloatFFTPlan(const RealFloatFFTPlan&);
	RealFloatFFTPlan& operator=(const RealFloatFFTPlan&);

	RealFloatPlanRecord* m_plan_record;
    };


    //
    // Single-precision complex 1-D plan
    // 
    class ComplexFloatPlanRecord;

    class ComplexFloatFFTPlan {
    public:
	ComplexFloatFFTPlan(int n, bool isForward);

	~ComplexFloatFFTPlan();
	
	void apply(std::complex<float>* data) const;

    private:
	ComplexFloatFFTPlan(const ComplexFloatFFTPlan&);
	ComplexFloatFFTPlan& operator=(const ComplexFloatFFTPlan&);

	ComplexFloatPlanRecord* m_plan_record;
    };


    //
    // Double-precision real 1-D plan
    // 
    class RealDoublePlanRecord;

    class RealDoubleFFTPlan {
    public:
	RealDoubleFFTPlan(int n, bool isForward);

	~RealDoubleFFTPlan();
	
	void apply(double* data) const;

    private:
	RealDoubleFFTPlan(const RealDoubleFFTPlan&);
	RealDoubleFFTPlan& operator=(const RealDoubleFFTPlan&);

	RealDoublePlanRecord* m_plan_record;
    };


    //
    // Double-precision complex 1-D plan
    // 
    class ComplexDoublePlanRecord;

    class ComplexDoubleFFTPlan {
    public:
	ComplexDoubleFFTPlan(int n, bool isForward);

	~ComplexDoubleFFTPlan();
	
	void apply(std::complex<double>* data) const;

    private:
	ComplexDoubleFFTPlan(const ComplexDoubleFFTPlan&);
	ComplexDoubleFFTPlan& operator=(const ComplexDoubleFFTPlan&);
	
	ComplexDoublePlanRecord* m_plan_record;
    };

}

#endif // FFTPLAN_H
