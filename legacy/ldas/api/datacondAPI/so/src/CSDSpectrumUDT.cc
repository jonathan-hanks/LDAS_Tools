/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "datacondAPI/config.h"

#include <memory>

#include "general/Memory.hh"

#include "CSDSpectrumUDT.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"

#include "WhenUMD.hh"

#include "CallChain.hh"

template<class T> datacondAPI::CSDSpectrum<T>::CSDSpectrum()
{
}

template<class T> datacondAPI::CSDSpectrum<T>::CSDSpectrum(const CSDSpectrum& value)
  : TimeBoundedFreqSequence<T>(value), CSDSpectrumUMD(value)
{
}
		
template<class T> datacondAPI::CSDSpectrum<T>::~CSDSpectrum()
{
}

template<class T>
datacondAPI::CSDSpectrum< T >* datacondAPI::CSDSpectrum<T>::
Clone() const
{
  return new CSDSpectrum< T >(*this);
}

template<class T>
ILwd::LdasContainer* datacondAPI::CSDSpectrum<T>::
ConvertToIlwd(const CallChain& Chain, datacondAPI::udt::target_type Target) const
{
  std::unique_ptr<ILwd::LdasContainer> container( new ILwd::LdasContainer );

  switch (Target)
  {
  case datacondAPI::udt::TARGET_FRAME:
  case datacondAPI::udt::TARGET_FRAME_FINAL_RESULT:
    {
      using namespace ILwdFCS;

      CSDSpectrum<T>& data = const_cast<CSDSpectrum<T>&>( *this );
      const std::string& lname( Chain.GetIntermediateName( *this,
							   this->name( ) ) );

      FrProcData proc_data;

      proc_data.SetName( lname );
      proc_data.SetComment( Chain.GetIntermediateComment( *this ) );
      proc_data.SetType( ILwdFCS::FrProcData::FREQUENCY_SERIES );
      proc_data.SetSubType( ILwdFCS::FrProcData::CROSS_SPECTRAL_DENSITY );

      //:NOTE: undefined field for this class
      // proc_data.SetTimeOffset( 0.0 );

      proc_data.SetTRange( this->GetEndTime() - this->GetStartTime() );

      //:NOTE: undefined for this class
      // proc_data.SetFShift(0.0);
      // proc_data.SetPhase(0.0);

      proc_data.SetFRange( this->GetFrequency( this->size( ) ) -
			   this->GetFrequency( 0 ) );

      //:NOTE: undefined for this class
      // proc_data.SetBW(0.0);

      //:NOTE: Add auxParams, aux data, tables here

      // Add data
      FrVect vect_data;
      vect_data.SetData( &data[0],
                         data.size(),
                         this->GetFrequency(0),
                         "Hertz",
                         this->GetFrequencyDelta() );
      vect_data.SetName( lname );

      proc_data.AppendData( vect_data );

      proc_data.AppendHistory( this->getHistory(), GPSTime::NowGPSTime() );

      TimeBoundedFreqSequenceMetaData::Store( proc_data );

      ILwd::LdasContainer* retval =
	const_cast<CallChain&>(Chain).FormatFrame( Target,
						   container.get(),
						   proc_data );
      container.reset( retval );

      // Store the metadata in an FrDetector struct
      GeometryMetaData::Store( const_cast<CallChain&>(Chain).
			       GetFrameHeaderTop() );
      
    }
    break;

  case datacondAPI::udt::TARGET_GENERIC:
    container->push_back( TimeBoundedFreqSequence<T>::ConvertToIlwd(Chain,
								    Target),
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
    container->push_back( CSDSpectrumUMD::ConvertToIlwd(Chain, Target),
			  ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
    break;

  case datacondAPI::udt::TARGET_WRAPPER:
    container.reset( TimeBoundedFreqSequence< T >::
		     ConvertToIlwd( Chain, Target ) );
    break;

  default:
    throw datacondAPI::udt::BadTargetConversion(Target,
						"datacondAPI::CSDSpectrum");
    break;
  }

  return container.release();
}
		
#undef CLASS_INSTANTIATION
#define CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::CSDSpectrum< class_ >; \
UDT_CLASS_INSTANTIATION(CSDSpectrum< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)

#if __SUNPRO_CC
#undef INIT_FUNC
#define INIT_FUNC init_CSDSpectrumUDT
MK_UDT_INIT_FUNC_4(INIT_FUNC,float,double,cfloat,cdouble);
#pragma init (INIT_FUNC)
#endif /* __SUNPRO_CC */
