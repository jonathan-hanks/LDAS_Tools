#ifndef MT19937_CC
#define MT19937_CC

#include "datacondAPI/config.h"

#include <general/toss.hh>

#include "mtstate.hh"

#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */   
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000

    
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)


namespace datacondAPI {

  double mt19937(mtstate& s)
  {
    unsigned long y(s.getMT());

    y ^= TEMPERING_SHIFT_U(y);
    y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
    y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
    y ^= TEMPERING_SHIFT_L(y);

    return ( (double) y * 2.3283064370807974e-10 ); 
  }

  void mt19937(std::valarray<double>& x, int nreq, mtstate& s)
  {
    if (nreq <= 0) 
      General::toss<std::logic_error>
	("mtstate::generate(valarray<double>&,int,mtstate&)",
	 __FILE__,__LINE__,"request <=0 random numbers?");

    x.resize(nreq);
    
    std::valarray<unsigned long> mt;
    
    s.getMT(mt,nreq);

    for (int i = 0; i < nreq; i++)
      {
	unsigned long y;
	y = mt[i];
	
	y ^= TEMPERING_SHIFT_U(y);
	y ^= TEMPERING_SHIFT_S(y) & TEMPERING_MASK_B;
	y ^= TEMPERING_SHIFT_T(y) & TEMPERING_MASK_C;
	y ^= TEMPERING_SHIFT_L(y);

	x[i] = ( (double) y * 2.3283064370807974e-10 ); 
      }
    
  }

}

#undef MATRIX_A 
#undef UPPER_MASK 
#undef LOWER_MASK 

#undef TEMPERING_MASK_B
#undef TEMPERING_MASK_C
    
#undef TEMPERING_SHIFT_U
#undef TEMPERING_SHIFT_S
#undef TEMPERING_SHIFT_T
#undef TEMPERING_SHIFT_L

#endif
