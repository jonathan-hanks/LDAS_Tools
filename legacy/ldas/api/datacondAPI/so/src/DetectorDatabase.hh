#ifndef DATACONDAPI__DETECTOR_DATABASE_HH
#define DATACONDAPI__DETECTOR_DATABASE_HH

#include <list>

namespace ILwd
{
  class TOC;
}

namespace datacondAPI
{
  class udt;
  class WhenMetaData;
  class ILwdMetaDataHelper;

  class DetectorDatabase
  {
  public:
    DetectorDatabase( );

    ~DetectorDatabase( );

    void AddDetector( const ILwd::TOC& Detector,
		      const ILwdMetaDataHelper& FrameHeader );

    void AddDetector( const ILwd::TOC& Detector );

    void AddDetectorInfo( datacondAPI::udt& Object ) const;

    void SetDetectorTime( const WhenMetaData& When );

  private:
    struct info_type;

    typedef std::list<info_type> detector_cache_type;

    detector_cache_type*	m_detectors;
  };
}

#endif	/* DATACONDAPI__DETECTOR_DATABASE_HH */
