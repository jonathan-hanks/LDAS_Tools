/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "datacondAPI/config.h"

#include <memory>
#include <sstream>

#include "general/Memory.hh"
#include "general/unordered_map.hh"
   
#include "filters/valarray_utils.hh"

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "DFTUDT.hh"
#include "util.hh"

#include "Real.hh"

using std::complex;
using std::unique_ptr;   
using General::unordered_map;   
using namespace datacondAPI;

namespace {

    //
    //
    // The trivial converters are used when the udt is real.
    // It is provided because the maps
    // can only contain non-member functions
    //
    udt* convert_trivial(const udt& udt_in)
    {
        return udt_in.Clone();
    }

    //
    // The following functions are the specific conversion routines
    // which are used once the exact dynamic type of an object is
    // determined. Note that derived classes have their own converters
    // but also use the converters of their ancestors.
    //

    //
    // Convert a Scalar
    //
    template<class T> inline
    void convert_specific(Scalar<T>& out,
                          const Scalar<complex<T> >& in)
    {
        out.SetValue(real(in.GetValue()));
    }

    //
    // Convert the Sequence part of an object
    //
    template<class T> inline
    void convert_specific(Sequence<T>& out,
                          const Sequence<complex<T> >& in)
    {
        // Do UDT part
        out.udt::operator=(in);

        // Do valarray part
        out = Filters::real(in);
    }

    template<class T> inline
    void convert_specific(TimeSeries<T>& out,
                          const TimeSeries<complex<T> >& in)
    {
        convert_specific(static_cast<Sequence<T>&>(out),
                         static_cast<const Sequence<complex<T> >&>(in));
        
        out.TimeSeriesMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(FrequencySequence<T>& out,
                          const FrequencySequence<complex<T> >& in)
    {
        convert_specific(static_cast<Sequence<T>&>(out),
                         static_cast<const Sequence<complex<T> >&>(in));
        
        out.FrequencyMetaData::operator=(in);
        out.GeometryMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(TimeBoundedFreqSequence<T>& out,
                          const TimeBoundedFreqSequence<complex<T> >& in)
    {
        convert_specific(static_cast<FrequencySequence<T>&>(out),
                       static_cast<const FrequencySequence<complex<T> >&>(in));
        
        out.TimeBoundedFreqSequenceMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(CSDSpectrum<T>& out,
                          const CSDSpectrum<complex<T> >& in)
    {
        convert_specific(static_cast<TimeBoundedFreqSequence<T>&>(out),
                 static_cast<const TimeBoundedFreqSequence<complex<T> >&>(in));
        
        out.CSDSpectrumUMD::operator=(in);
    }

    template<class T> inline
    void convert_specific(WelchSpectrum<T>& out,
                          const WelchSpectrum<complex<T> >& in)
    {
        convert_specific(static_cast<CSDSpectrum<T>&>(out),
                         static_cast<const CSDSpectrum<complex<T> >&>(in));
        
        out.WelchSpectrumUMD::operator=(in);
    }

    template<class T> inline
    void convert_specific(WelchCSDSpectrum<T>& out,
                          const WelchCSDSpectrum<complex<T> >& in)
    {
        convert_specific(static_cast<CSDSpectrum<T>&>(out),
                         static_cast<const CSDSpectrum<complex<T> >&>(in));
        
        out.WelchCSDSpectrumUMD::operator=(in);
    }
    
    //
    // This is the top level dispatcher - different versions of
    // this function are instantiated and inserted into a map
    // which takes type_info to an appropriate function for
    // converting TIn to TOut.
    //
    template<class TOut, class TIn>
    udt* convert(const udt& udt_in)
    {
        const TIn& in = dynamic_cast<const TIn&>(udt_in);
        unique_ptr<TOut> out(new TOut());
        
        convert_specific(*out, in);
        
        return out.release();
    }

    //
    // These are the prototypes that all functions contained in the map
    // must adhere to. Note that the function uses UDT arguments because
    // all the functions contained in the map must have the same signature,
    // so a templated function wouldn't work.
    //
    // All conversion functions take a single UDT argument and return a
    // pointer to a UDT at the new precision created on the heap
    //
    typedef udt* (*ConversionFn)(const udt& in);
    
    //
    // A ConversionFnMap is a unordered_map that uses a type_info* as the key and
    // contains ConversionFn's as its elements
    //
    typedef unordered_map<const std::type_info*, ConversionFn,
                          LookupHash, LookupEq> ConversionFnMap;
    
    //
    // A function that we can use to initialise the unordered_map when the
    // library is loaded
    //
    const ConversionFnMap& initRealFnMap()
    {
        static ConversionFnMap realFnMap;
        
        // Lets be ultra-paranoid
        if (realFnMap.size() != 0)
        {
            return realFnMap;
        }
        
        //
        // Now we insert the function pointers into the map by typeid
        //

        // Trivial conversions for things that are already real - 
        // they are "converted" via cloning

        // Trivial Scalar
        realFnMap[&typeid(Scalar<int>)] = convert_trivial;
        realFnMap[&typeid(Scalar<float>)] = convert_trivial;
        realFnMap[&typeid(Scalar<double>)] = convert_trivial;

        // Trivial Sequence
        realFnMap[&typeid(Sequence<float>)] = convert_trivial;
        realFnMap[&typeid(Sequence<double>)] = convert_trivial;

        // Trivial TimeSeries
        realFnMap[&typeid(TimeSeries<float>)] = convert_trivial;
        realFnMap[&typeid(TimeSeries<double>)] = convert_trivial;

        // Trivial FrequencySequence
        realFnMap[&typeid(FrequencySequence<float>)] = convert_trivial;
        realFnMap[&typeid(FrequencySequence<double>)] = convert_trivial;

        // Trivial TimeBoundedFreqSequence
        realFnMap[&typeid(TimeBoundedFreqSequence<float>)] = convert_trivial;
        realFnMap[&typeid(TimeBoundedFreqSequence<double>)] = convert_trivial;

        // Trivial CSDSpectrum
        realFnMap[&typeid(CSDSpectrum<float>)] = convert_trivial;
        realFnMap[&typeid(CSDSpectrum<double>)] = convert_trivial;

        // Trivial WelchSpectrum (real only)
        realFnMap[&typeid(WelchSpectrum<float>)] = convert_trivial;
        realFnMap[&typeid(WelchSpectrum<double>)] = convert_trivial;

        // Trivial WelchCSDSpectrum
        realFnMap[&typeid(WelchCSDSpectrum<float>)] = convert_trivial;
        realFnMap[&typeid(WelchCSDSpectrum<double>)] = convert_trivial;

        // Trivial DFT - none, there aren't any real ones

        // Non-trivial conversions

        // Scalar
        realFnMap[&typeid(Scalar<complex<float> >)]
            = convert<Scalar<float>, Scalar<complex<float> > >;
        realFnMap[&typeid(Scalar<complex<double> >)]
            = convert<Scalar<double>, Scalar<complex<double> > >;

        // Sequence
        realFnMap[&typeid(Sequence<complex<float> >)]
            = convert<Sequence<float>, Sequence<complex<float> > >;
        realFnMap[&typeid(Sequence<complex<double> >)]
            = convert<Sequence<double>, Sequence<complex<double> > >;
        
        // TimeSeries
        realFnMap[&typeid(TimeSeries<complex<float> >)]
         = convert<TimeSeries<float>, TimeSeries<complex<float> > >;
        realFnMap[&typeid(TimeSeries<complex<double> >)]
         = convert<TimeSeries<double>, TimeSeries<complex<double> > >;

        // FrequencySequence
        realFnMap[&typeid(FrequencySequence<complex<float> >)]
            = convert<FrequencySequence<float>,
                      FrequencySequence<complex<float> > >;
        realFnMap[&typeid(FrequencySequence<complex<double> >)]
            = convert<FrequencySequence<double>,
                      FrequencySequence<complex<double> > >;

        // TimeBoundedFreqSequence
        realFnMap[&typeid(TimeBoundedFreqSequence<complex<float> >)]
            = convert<TimeBoundedFreqSequence<float>,
                      TimeBoundedFreqSequence<complex<float> > >;
        realFnMap[&typeid(TimeBoundedFreqSequence<complex<double> >)]
            = convert<TimeBoundedFreqSequence<double>,
                      TimeBoundedFreqSequence<complex<double> > >;

        // CSDSpectrum
        realFnMap[&typeid(CSDSpectrum<complex<float> >)]
            = convert<CSDSpectrum<float>,
                      CSDSpectrum<complex<float> > >;
        realFnMap[&typeid(CSDSpectrum<complex<double> >)]
            = convert<CSDSpectrum<double>,
                      CSDSpectrum<complex<double> > >;

        // WelchSpectrum's are real only
        
        // WelchCSDSpectrum
        realFnMap[&typeid(WelchCSDSpectrum<complex<float> >)]
            = convert<WelchCSDSpectrum<float>,
                      WelchCSDSpectrum<complex<float> > >;
        realFnMap[&typeid(WelchCSDSpectrum<complex<double> >)]
            = convert<WelchCSDSpectrum<double>,
                      WelchCSDSpectrum<complex<double> > >;

        // DFT - they only come in complex, so we have to convert them
        // to a Sequence.
        realFnMap[&typeid(DFT<complex<float> >)]
            = convert<Sequence<float>, Sequence<complex<float> > >;
        realFnMap[&typeid(DFT<complex<double> >)]
            = convert<Sequence<double>, Sequence<complex<double> > >;
        
        return realFnMap;
    }

    //
    // Static objects
    //
    
    //
    // This is the object we use to look up conversion functions. It is a
    // const reference to a static unordered_maps contained inside the
    // initRealFnMap() function. It's about as threadsafe as possible
    // because a) it's a reference and so must be initialised or the compiler
    // will complain b) it's initialised when the library is loaded
    // and c) it's const
    //
    static const ConversionFnMap& theRealFnMap = initRealFnMap();

    //-------------------------------------------------------------------
    // Need to instantiate the function objects
    //-------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE(A,B) template udt* convert< A, B >(const udt& udt_in)

    INSTANTIATE( Scalar<float>, Scalar<complex<float> > );
    INSTANTIATE( Scalar<double>, Scalar<complex<double> > );

    INSTANTIATE( Sequence<float>, Sequence<complex<float> > );
    INSTANTIATE( Sequence<double>, Sequence<complex<double> > );
        
    INSTANTIATE( TimeSeries<float>, TimeSeries<complex<float> > );
    INSTANTIATE( TimeSeries<double>, TimeSeries<complex<double> > );

    INSTANTIATE( FrequencySequence<float>,
		 FrequencySequence<complex<float> > );
    INSTANTIATE( FrequencySequence<double>,
		 FrequencySequence<complex<double> > );

    INSTANTIATE( TimeBoundedFreqSequence<float>,
		 TimeBoundedFreqSequence<complex<float> > );
    INSTANTIATE( TimeBoundedFreqSequence<double>,
		 TimeBoundedFreqSequence<complex<double> > );

    INSTANTIATE( CSDSpectrum<float>,
		 CSDSpectrum<complex<float> > );
    INSTANTIATE( CSDSpectrum<double>,
		 CSDSpectrum<complex<double> > );

    INSTANTIATE( WelchCSDSpectrum<float>,
		 WelchCSDSpectrum<complex<float> > );
    INSTANTIATE( WelchCSDSpectrum<double>,
		 WelchCSDSpectrum<complex<double> > );

#undef INSTANTIATE
    
} // anonymous namespace

namespace datacondAPI {

    udt* Real(const udt& in)
    {
        //
        // Look up the corresponding conversion function in the function table
        //
        ConversionFnMap::const_iterator iter
            = theRealFnMap.find(&typeid(in));

        if (iter == theRealFnMap.end())
        {
            // There was no function in the table for an
            // object of this type - throw an exception
            std::ostringstream oss;
            oss << "No Real() operator for "
                << TypeInfoTable.GetName(typeid(in));

            throw std::invalid_argument(oss.str());
        }

        // Now call the function, which is the second element
        // of the pair obtained in the map lookup
        return (*iter).second(in);
    }

} // namespace datacondAPI

