/* -*- mode: c++; c-basic-offset: 2; -*- */
//
// $Id: DatabaseUDT.cc,v 1.29 2006/11/07 22:25:46 emaros Exp $

#include "datacondAPI/config.h"

#include <complex>
#include <stdexcept>
#include <memory>   

#include "general/Memory.hh"
#include "general/types.hh"

#include "ilwd/elementid.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrTable.hh"
#include "ilwdfcs/FrVect.hh"

#include "dbaccess/Field.hh"
#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldBlob.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/FieldUniqueId.hh"
#include "dbaccess/Table.hh"

#include "DatabaseUDT.hh"
#include "CallChain.hh"

using namespace datacondAPI;
using namespace std;   

#if 0
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl
#else
#define	AT()
#endif

//-----------------------------------------------------------------------
// Forward Declaration of Helper Functions
//-----------------------------------------------------------------------

//-----------------------------------------------------------------------
// Helper functions
//-----------------------------------------------------------------------
template<class T>
ILwd::LdasElement*
make_ilwd( const std::vector<T>& ColumnData, std::string Name );

template <>
ILwd::LdasElement*
make_ilwd<std::string>( const std::vector<std::string>& Seq,
			std::string Name )
{
  using namespace datacondAPI;
  using namespace ILwd;

  ILwd::LdasString*	s = new ILwd::LdasString( );

  s->resize( Seq.size() );

  for ( unsigned int x = 0; x < Seq.size(); x++ )
  {
    (*s)[x] = Seq[x];
  }

  s->setNameString( Name );

  return s;
}

template <>
ILwd::LdasElement*
make_ilwd< std::vector< unsigned char > >
( const std::vector< std::vector< unsigned char > >& Seq,
  std::string Name )
{
  using namespace datacondAPI;
  using namespace ILwd;

  ILwd::LdasContainer*	c = new ILwd::LdasContainer( );

  for ( unsigned int x = 0; x < Seq.size(); x++ )
  {
    c->push_back( new ILwd::LdasArray<CHAR_U>(&(Seq[x].operator[](0)),
					      Seq[x].size()
					      ),
		  ILwd::LdasContainer::NO_ALLOCATE,
		  ILwd::LdasContainer::OWN );
  }

  c->setNameString( Name );

  return c;
}

template<class DataType_>
ILwd::LdasElement*
make_ilwd( const std::vector<DataType_>& Seq,
	   std::string Name )
{
  using namespace datacondAPI;
  using namespace ILwd;

  LdasArray<DataType_>*	array;

  // :TRICKY: Throw away the constant value so data component can
  // :TRICKY:    be passed to ILwd::LdasyArray<T> constructor.
  std::vector<DataType_>&
    data(const_cast< std::vector<DataType_>& >( Seq ));
  DataType_*	data_ptr( ( data.size() )
			  ? &data.operator[](0) :
			  (DataType_*)NULL );

  array = new LdasArray<DataType_>( data_ptr,
				     data.size(),
				     Name );
  return array;
}

//-----------------------------------------------------------------------
//-----------------------------------------------------------------------

class Database::Column
{
public:
  Column( const DB::Field& Field );

  //: Copy Constructor
  Column( const Database::Column& Source );

  //: Destructor
  ~Column();

  //: Convert to an ILWD
  ILwd::LdasElement*
  ConvertToILwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target ) const;

  //: Convert to an FrVect, dynamically allocated on the heap
  //
  // This function converts the column to an FrVect object, which is
  // dynamically allocated on the heap and returned as a pointer. It is
  // the callers responsibility to delete it.
  //
  //!return: FrVect* - a dynamically-allocated FrVect object
  ILwdFCS::FrVect*
  ConvertToFrameColumn( int Offset ) const;

  bool IsSimple( Database::simple_type Check ) const;

private:
  std::string			m_name;
  ILwd::ElementId		m_type;
  union {
    std::vector< std::vector<CHAR_U> >*	m_blob;
    std::vector<std::string>*		m_lstring;
    std::vector<INT_2S>*		m_int_2s;
    std::vector<INT_2U>*		m_int_2u;
    std::vector<INT_4S>*		m_int_4s;
    std::vector<INT_4U>*		m_int_4u;
    std::vector<INT_8S>*		m_int_8s;
    std::vector<INT_8U>*		m_int_8u;
    std::vector<REAL_4>*		m_real_4;
    std::vector<REAL_8>*		m_real_8;
    std::vector<COMPLEX_8>*		m_complex_8;
    std::vector<COMPLEX_16>*		m_complex_16;
  };

  template < class DataType_> static std::vector< DataType_ >*
  array_field( const DB::FieldArray< DataType_ >& Data );

  std::string get_domain() const;

  ILwd::LdasElement* convert_to_ilwd( const std::string& Name ) const;
};

Database::Column::
Column( const DB::Field& Field )
  : m_name( Field.GetName() )
{
  switch ( Field.GetType() )
  {
  case DB::Field::FIELD_BLOB:
    {
      const DB::FieldBlob&
	fld( dynamic_cast< const DB::FieldBlob& >( Field ) );

      m_type = ILwd::ID_CHAR_U;
      m_blob = new std::vector< std::vector<CHAR_U> >( fld.Size() );
      for ( unsigned int x = 0; x < fld.Size(); x++ )
      {
	const CHAR_U*			data( fld[x]->getData() );

	(*m_blob)[x].resize( fld[x]->getDimension(0) );
	for ( unsigned int y = 0; y < fld[x]->getDimension(0); y++, data++ )
	{
	  m_blob->operator[](x).operator[](y) = *data;
	}
      }
      break;
    }
  case DB::Field::FIELD_STRING:
    {
      const DB::FieldString&
	data( dynamic_cast< const DB::FieldString& >( Field ) );

      m_type = ILwd::ID_LSTRING;
      m_lstring = new std::vector<std::string>( data.Size() );
      for ( unsigned int x = 0; x < data.Size(); x++ )
      {
	(*m_lstring)[x] = data[x];
      }
      break;
    }

#define	ARRAY_FIELD(lm_type,var) \
  case DB::Field::FIELD_ARRAY_##lm_type: \
    m_type = ILwd::ID_##lm_type; \
    var = array_field( dynamic_cast<const DB::FieldArray<lm_type>& >\
		       ( Field ) ); \
    break;

  ARRAY_FIELD(INT_2S, m_int_2s);
  ARRAY_FIELD(INT_2U, m_int_2u);
  ARRAY_FIELD(INT_4S, m_int_4s);
  ARRAY_FIELD(INT_4U, m_int_4u);
  ARRAY_FIELD(INT_8S, m_int_8s);
  ARRAY_FIELD(INT_8U, m_int_8u);
  ARRAY_FIELD(REAL_4, m_real_4);
  ARRAY_FIELD(REAL_8, m_real_8);

#undef ARRAY_FIELD

  default:
    throw std::bad_alloc();
  }
}

Database::Column::
Column( const Database::Column& Source )
  : m_name( Source.m_name),
    m_type( Source.m_type )
{
  switch( m_type )
  {
#define	INIT3(type,var,vect_type) \
case ILwd::ID_##type: var = new std::vector< vect_type >( *(Source.var) ); break
#define	INIT(type,var) INIT3(type,var,type)

    INIT3(CHAR_U,m_blob,std::vector<CHAR_U>);
    INIT3(LSTRING,m_lstring,std::string);
    INIT(INT_2S,m_int_2s);
    INIT(INT_2U,m_int_2u);
    INIT(INT_4S,m_int_4s);
    INIT(INT_4U,m_int_4u);
    INIT(INT_8S,m_int_8s);
    INIT(INT_8U,m_int_8u);
    INIT(REAL_4,m_real_4);
    INIT(REAL_8,m_real_8);
    INIT(COMPLEX_8,m_complex_8);
    INIT(COMPLEX_16,m_complex_16);

#undef INIT
#undef INIT3
  default:
    throw std::bad_alloc();
  }
}

Database::Column::
~Column()
{
  switch( m_type )
  {
#define DELETE(type,var) \
  case ILwd::ID_##type:	delete var;	break

    DELETE(LSTRING,m_lstring);
    DELETE(CHAR_U,m_blob);
    DELETE(INT_2S,m_int_2s);
    DELETE(INT_2U,m_int_2u);
    DELETE(INT_4S,m_int_4s);
    DELETE(INT_4U,m_int_4u);
    DELETE(INT_8S,m_int_8s);
    DELETE(INT_8U,m_int_8u);
    DELETE(REAL_4,m_real_4);
    DELETE(REAL_8,m_real_8);
    DELETE(COMPLEX_8,m_complex_8);
    DELETE(COMPLEX_16,m_complex_16);

#undef DELETE
  default:
    throw( std::runtime_error( "Column has memory leak" ) );
  }
}

ILwd::LdasElement* Database::Column::
ConvertToILwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  AT();
  ILwd::LdasContainer*	retval( new ILwd::LdasContainer() );
  std::string		name;

  AT();
  try
  {
    AT();
    switch ( Target )
    {
    case datacondAPI::udt::TARGET_WRAPPER:
      {
	AT();
	std::string	domain(":domain");

	domain.insert( 0, get_domain( ) );

	AT();
	retval->appendName(m_name);
	retval->appendName("database");
	retval->appendName("sequence");
	retval->appendName("");
	retval->push_back( new ILwd::LdasString("DATABASE", domain),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
	retval->push_back( convert_to_ilwd( "data" ),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
	break;
      }	// case
    case datacondAPI::udt::TARGET_GENERIC:
      retval->push_back( convert_to_ilwd( "" ),
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
      retval->appendName( m_name );
      retval->back()->appendName( m_name );
      break;
    default:
      break;
    } // switch
  } // try
  catch( ... )
  {
    AT();
    delete retval;
    throw;
  }
  AT();
  return retval;
}

ILwdFCS::FrVect* Database::Column::
ConvertToFrameColumn( int Offset ) const
{
  using namespace ILwdFCS;

  AT();

  // :NOTE: This function used to return a reference to an FrVect that
  // was created on the heap inside the function. That's a very bad thing
  // to do as it will usually lead to a memory leak - a caller would not
  // normally want to use delete on the address of a reference! The only
  // way I can see to fix it easily is to return a pointer instead and
  // require that the caller delete the pointer.

  // Create a new FrVect in an unique_ptr, to protect against exceptions
  unique_ptr<FrVect> vect(new FrVect());
#if WORKING
  vect->SetNameAttribute( m_name );

  unsigned int size = 1;
      
  switch( m_type )
  {
  case ILwd::ID_LSTRING:
    AT();
    if ( Offset < 0 )
    {
      throw std::runtime_error( "Unable to convert data" );
    }
    AT();
    vect->SetData( (*m_lstring)[Offset].c_str(),
                   (*m_lstring)[Offset].length() );
    AT();
    break;

  case ILwd::ID_CHAR_U:
    AT();
    if ( Offset < 0 )
    {
      throw std::runtime_error( "Unable to convert data" );
    }
    vect->SetData( &( m_blob->operator[]( Offset ).operator[]( 0 ) ),
                   m_blob->operator[]( Offset ).size() );
    break;
    
#define	CONVERT(lm_type, lm_var) \
    case ILwd::ID_##lm_type: \
      AT(); \
      if ( Offset < 0 ) \
      { \
	size = lm_var->size( ); \
	Offset = 0; \
      } \
      else \
      { \
	size = 1; \
      } \
      vect->SetData( &( lm_var->operator[]( Offset ) ), size ); \
      break

      CONVERT(INT_2S, m_int_2s);
      CONVERT(INT_2U, m_int_2u);
      CONVERT(INT_4S, m_int_4s);
      CONVERT(INT_4U, m_int_4u);
      CONVERT(INT_8S, m_int_8s);
      CONVERT(INT_8U, m_int_8u);
      CONVERT(REAL_4, m_real_4);
      CONVERT(REAL_8, m_real_8);
      CONVERT(COMPLEX_8, m_complex_8);
      CONVERT(COMPLEX_16, m_complex_16);

#undef CONVERT
      
  default:
    // Why is there no default behaviour or error??
    AT();
    break;
  }

  AT();
#endif /* WORKING */
  return vect.release();
}


bool Database::Column::
IsSimple( Database::simple_type Check ) const
{
  AT();
  switch( m_type )
  {
  case ILwd::ID_CHAR_U:
    AT();
    return false;
    break;
  case ILwd::ID_LSTRING:
  case ILwd::ID_CHAR:
    if ( Check == Database::FRAME )
    {
      AT();
      return false;
    }
    AT();
    return true;
    break;
  default:
    AT();
    return true;
    break;
  }
}

template < class DataType_> std::vector< DataType_ >* Database::Column::
array_field( const DB::FieldArray< DataType_ >& Data )
{
  std::vector< DataType_ >*	data( new std::vector< DataType_ >( Data.Size() ) );

  for ( unsigned int x = 0; x < Data.Size(); x++ )
  {
    (*data)[x] = Data[x];
  }
  return data;
}

std::string Database::Column::
get_domain( ) const
{
  switch ( m_type )
  {
  case ILwd::ID_CHAR:
  case ILwd::ID_CHAR_U:
    return "char";
  case ILwd::ID_LSTRING:
    return "string";
  case ILwd::ID_COMPLEX_8:
  case ILwd::ID_COMPLEX_16:
    return "complex";
  default:
    return "real";
  }
}

ILwd::LdasElement* Database::Column::
convert_to_ilwd( const std::string& Name ) const
{
  ILwd::LdasElement*	retval( (ILwd::LdasElement*)NULL );

  switch( m_type )
  {
  case ILwd::ID_LSTRING:
    AT();
    retval = make_ilwd<std::string>( *m_lstring, Name );
    break;
  case ILwd::ID_CHAR_U:
    AT();
    retval = make_ilwd<std::vector<CHAR_U> >( *m_blob, Name );
    break;

#define	ARRAY_FIELD(lm_type,lm_var) \
  case ILwd::ID_##lm_type: \
    AT(); \
    retval = make_ilwd<lm_type>( *lm_var, Name ); \
    break

    ARRAY_FIELD(INT_2S, m_int_2s);
    ARRAY_FIELD(INT_2U, m_int_2u);
    ARRAY_FIELD(INT_4S, m_int_4s);
    ARRAY_FIELD(INT_4U, m_int_4u);
    ARRAY_FIELD(INT_8S, m_int_8s);
    ARRAY_FIELD(INT_8U, m_int_8u);
    ARRAY_FIELD(REAL_4, m_real_4);
    ARRAY_FIELD(REAL_8, m_real_8);
    ARRAY_FIELD(COMPLEX_8, m_complex_8);
    ARRAY_FIELD(COMPLEX_16, m_complex_16);

#undef ARRAY_FIELD
  default:
    AT();
    throw std::runtime_error( "Unknown ILwd conversion for datacondAPI::Database::Column" );
    break;
  } // switch
  return retval;
}
//-----------------------------------------------------------------------
//
//-----------------------------------------------------------------------

Database::
Database( const DB::Table& Table,
	  CallChain::query_type QueryType,
	  const std::string& SQL,
	  bool IgnoreErrors )
  : m_query( SQL ),
    m_row_count( -1 ),
    m_query_type( QueryType )

{
  AT();
  for ( unsigned int x = 0; x < Table.GetFieldCount(); x++ )
  {
    try {
      AppendColumn( Table.GetField( x ) );
    }
    catch( ... )
    {
      if ( ! IgnoreErrors )
      {
	throw;
      }
    }
  }
}

Database::
Database( const Database& Source )
  : udt( Source ),
    m_query( Source.m_query ),
    m_row_count( Source.m_row_count ),
    m_query_type( Source.m_query_type )
{
  AT();
  for ( std::vector<Column*>::const_iterator r = Source.m_columns.begin();
	r != Source.m_columns.end();
	r++ )
  {
    m_columns.push_back( new Column( *(*r) ) );
  }
}

datacondAPI::Database::
~Database()
{
  AT();
  for( std::vector<Column*>::const_iterator i = m_columns.begin();
       i != m_columns.end();
       i++ )
  {
    delete *i;
  }
}

void Database::
APICheck( datacondAPI::udt::target_type API ) const
{
  using namespace datacondAPI;

  switch( API )
  {
  case udt::TARGET_WRAPPER:
    if ( m_query_type == CallChain::QUERY_GENERIC )
    {
      throw( CallChain::LogicError( "Result of -dbquery cannot be sent to MPI" ) );
    }
    if ( ! IsSimple( Database::DATA_TYPES ) )
    {
      throw CallChain::TableHasNonSimpleTypes( );
    }
    break;
  case udt::TARGET_GENERIC:
  case udt::TARGET_METADATA:
  case udt::TARGET_METADATA_FINAL_RESULT:
  case udt::TARGET_FRAME:
  case udt::TARGET_FRAME_FINAL_RESULT:
    break;
  }
}

void Database::
AppendColumn( const DB::Field& Field )
{
  AT();
  if ( m_row_count == -1 )
  {
    AT();
    m_row_count = Field.Size();
  }
  else
  {
    AT();
    if ( ( (unsigned int)m_row_count ) != Field.Size() )
    {
      AT();
      throw std::runtime_error( "Columns differ in number of rows" );
    }
  }
  AT();
  m_columns.push_back( new Column( Field ) );
}

datacondAPI::Database* datacondAPI::Database::
Clone() const
{
  AT();
  return new Database( *this );
}

ILwd::LdasElement* datacondAPI::Database::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  APICheck( Target );
  AT();
  ILwd::LdasContainer*	container( new ILwd::LdasContainer() );

  try
  {
    AT();
    switch (Target)
    {
#if WORKING
    case datacondAPI::udt::TARGET_FRAME:
    case datacondAPI::udt::TARGET_FRAME_FINAL_RESULT:
      {
	using namespace ILwdFCS;

	AT();

	unsigned int	size;
	bool		simple;

	AT();
	if ( IsSimple( FRAME ) )
	{
	  simple = true;
	  size = 1;
	}
	else
	{
	  AT();
	  simple = false;
	  if ( m_row_count <= 0 )
	  {
	    size = 1;
	  }
	  else
	  {
	    size = m_row_count;
	  }
	}
	
	AT();
	FrProcData	proc_data;
	AT();
	FrTable		table[ size ];

	AT();
	for ( unsigned int x = 0; x < size; x++ )
	{
	  for ( std::vector<Column*>::const_iterator r = m_columns.begin();
		r != m_columns.end();
		r++ )
	  {
	    int offset;
	    if ( simple )
	    {
	      offset = -1;
	    }
	    else
	    {
	      offset = (int)x;
	    }

            unique_ptr<FrVect> vect( (*r)->ConvertToFrameColumn( offset ) );
	    table[x].AppendRow( *vect );
	  }
	  table[x].SetComment( m_query );
	  proc_data.AppendTable( table[x] );
	}

	const std::string& lname( Chain.GetIntermediateName( *this, name( ) );

        proc_data.SetName( lname );

        //:NOTE: don't know if comment is needed
        // proc_data.SetComment( ? );

        proc_data.SetType( ILwdFCS::FrProcData::UNKNOWN_TYPE );

        //:NOTE: I don't know what other meta-data should go here,
        // so I'm leaving out the other fields

        proc_data.AppendHistory(getHistory(), GPSTime::NowGPSTime());

	ILwd::LdasContainer* retval =
	  const_cast<CallChain&>(Chain).FormatFrame( Target,
						     container,
						     proc_data );
	if ( retval != container )
	{
	  delete container;
	  container = retval;
	}
      }
      break;
#endif /* WORKING */
    case datacondAPI::udt::TARGET_WRAPPER:
    case datacondAPI::udt::TARGET_GENERIC:
      AT();
      ConvertToIlwd( Chain, Target, container );
      break;
    default:
      AT();
      throw datacondAPI::udt::BadTargetConversion(Target,
						  "datacondAPI::Database");
      break;
    }
  }
  catch( ... )
  {
    AT();
    delete container;
    throw;
  }
  return container;
}

void datacondAPI::Database::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target,
	       ILwd::LdasContainer* Container ) const
{
  APICheck( Target );
  switch (Target)
  {
  case datacondAPI::udt::TARGET_WRAPPER:
    AT();
    for ( std::vector<Column*>::const_iterator r = m_columns.begin();
	  r != m_columns.end();
	  r++ )
    {
      Container->push_back( (*r)->ConvertToILwd( Chain, Target ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      Container->back()->setComment( m_query );
    }
    break;
  case datacondAPI::udt::TARGET_GENERIC:
    AT();
    for ( std::vector<Column*>::const_iterator r = m_columns.begin();
	  r != m_columns.end();
	  r++ )
    {
      Container->push_back( (*r)->ConvertToILwd( Chain, Target ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
    }
    break;
  default:
    AT();
    throw datacondAPI::udt::BadTargetConversion( Target,
						 "datacondAPI::Database" );
    break;
  }
}

bool Database::
IsSimple( simple_type Check ) const
{
  std::vector<Column*>::const_iterator r = m_columns.begin();
  while ( (r != m_columns.end() ) && ( (*r)->IsSimple( Check ) ) )
  {
    r++;
  }
  if ( r != m_columns.end() )
  {
    return false;
  }
  return true;
}

bool datacondAPI::Database::
ReturnsMultipleILwds( datacondAPI::udt::target_type Target ) const
{
  bool	retval = false;

  if ( Target == datacondAPI::udt::TARGET_WRAPPER )
  {
    retval = true;
  }
  return retval;
}

///----------------------------------------------------------------------
/// Private methods
///----------------------------------------------------------------------

