#include "datacondAPI/config.h"

#include <iostream>
#include <sstream>

#include "CallChain.hh"
#include "DatabaseUDT.hh"

//!ignore_begin:
// Do not generate any documentation for these debugging macros
#if 0
#define AT() std::cerr << "DEBUG: AT: " << (void*)this << " " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#define AT_STATIC() std::cerr << "DEBUG: AT: " << " " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#define AT_STATIC()
#endif
//-----------------------------------------------------------------------
CallChain::Step::
Step()
  : m_lineno( -1 )
{
}

//-----------------------------------------------------------------------

CallChain::CallFunction::
CallFunction( const std::string& FunctionName,
	      const char** Params,
	      const std::string& Return)
  : m_function(FunctionName),
    m_return(Return)
{
  Function*	f(CallChain::GetFunction(FunctionName));
  
  if (!f)
  {
    throw CallChain::BadFunction( FunctionName );
  }

  for (int x = 0; Params[x]; x++)
  {
    m_params.push_back(Params[x]);
  }
}

void CallChain::CallFunction::
DryRun( sudo_symbol_table_type& SudoSymbolTable ) const
{
  //---------------------------------------------------------------------
  // Validate the parameters
  //---------------------------------------------------------------------
  Function*	f( CallChain::GetFunction( m_function ) );

  f->ValidateParameters( SudoSymbolTable, m_params );
  //---------------------------------------------------------------------
  // Stuff the left hand side into the sudo symbol table
  //---------------------------------------------------------------------
  if ( m_return.length( ) > 0 )
  {
    SudoSymbolTable[ m_return ] = m_return;
  }
}

void CallChain::CallFunction::
Eval(CallChain* Chain)
{
  Function*	f(Chain->GetFunction(m_function));
  
  if (f)
  {
    // Calculate the history record
    std::ostringstream oss;
    oss << f->GetName( )
	<< "(";
    bool first( true );
    for ( Params::const_iterator i = m_params.begin();
	  i != m_params.end();
	  i++ )
    {
      if ( !first )
      {
	oss << ", ";
      }
      else
      {
          first = false;
      }

      oss << *i;
    }
    oss << ")";

    f->Eval(Chain, m_params, m_return);

    datacondAPI::udt* answer( Chain->GetSymbol( m_return, false ) );
    if ( answer )
    {
      answer->AppendHistory( oss.str( ) );
    }
  }
  else
  {
    throw CallChain::BadFunction( m_function );
  }
}

