/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef ZPG2LINFILTFUNCTION_HH
#define ZPG2LINFILTFUNCTION_HH

// $Id: ZPG2LinFiltFunction.hh,v 1.1 2003/01/07 02:41:34 Philip.Charlton Exp $

#include "CallChain.hh"

//
//: Function support for zpg2linfilt action
//
// z = zpg2linfilt(zeroes, poles, gain);
//
class ZPG2LinFiltFunction : public CallChain::Function
{

public:

    ZPG2LinFiltFunction();

    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;

    virtual const std::string& GetName() const;

};

#endif // ZPG2LINFILTFUNCTION_HH
