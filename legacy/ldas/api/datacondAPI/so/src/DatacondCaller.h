#ifndef DATACONDAPI__DATACONDCALLER_H
#define DATACONDAPI__DATACONDCALLER_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

  enum {
    TRANSLATION_OK = 1,
    TRANSLATION_FAIL = 0
  };

  typedef enum {
    DATACOND_SYMBOL_INPUT,
    DATACOND_SYMBOL_OUTPUT
  } translation_direction;

  typedef enum {
    DATACOND_OK,
    DATACOND_PARSE_ALIASES_FAILURE,
    DATACOND_PARSE_FAILURE,
    DATACOND_EXEC_FAILURE,
    DATACOND_INGESTION_FAILURE
  } datacond_error_codes;

  typedef int (*translation_func)( translation_direction Direction,
				   void** UserData,
				   void** DatacondData,
				   void* AuxData );
  typedef struct {
    translation_direction	s_direction;
    translation_func		s_translator;
    const char*			s_symbol_name;
    void*			s_aux_data;
    void*			s_user_data;
  } datacond_symbol_type;

  int
  DatacondCaller( const char* Algorithm,
		  const char* Aliases,
		  datacond_symbol_type* Symbols,
		  int	NumberOfSymbols,
		  char** ErrorMessage );

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* DATACONDAPI__DATACONDCALLER_H */
