/* -*- mode: c++ -*- */
#ifndef CEIL_FUNCTION_HH
#define CEIL_FUNCTION_HH

#include "CallChain.hh"
#include "UDT.hh"

class CeilFunction: public CallChain::Function
{
public:

  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  CeilFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

#ifdef UNARY_FUNCTION_CC

#define	OPERATION "ceil"
#define UnaryClass CeilFunction

namespace {
  template< class T >
  inline T
  action( const T& Source )
  {
    return T( std::ceil( Source ) );
  }

  template<>
  inline int
  action( const int& Source )
  {
    return Source;
  }   
   
  template<>
  inline short
  action( const short& Source )
  {
    return Source;
  }      
}

#endif	/* UNARY_FUNCTION_CC */

#endif	/* CEIL_FUNCTION_HH */
