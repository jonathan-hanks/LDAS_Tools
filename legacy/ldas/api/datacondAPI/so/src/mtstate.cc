#ifndef MTSTATE_CC
#define MTSTATE_CC

#include "datacondAPI/config.h"

/* Period parameters */  
#define MATRIX_A 0x9908b0df   /* constant vector a */
#define UPPER_MASK 0x80000000 /* most significant w-r bits */
#define LOWER_MASK 0x7fffffff /* least significant r bits */

/* Tempering parameters */   
#define TEMPERING_MASK_B 0x9d2c5680
#define TEMPERING_MASK_C 0xefc60000
#define TEMPERING_SHIFT_U(y)  (y >> 11)
#define TEMPERING_SHIFT_S(y)  (y << 7)
#define TEMPERING_SHIFT_T(y)  (y << 15)
#define TEMPERING_SHIFT_L(y)  (y >> 18)

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

#include "mtstate.hh"

namespace datacondAPI {

  mtstate::mtstate()  : m_N(624), m_M(397)
  {
    seed(4357);              // default initial seed
  }

  mtstate::mtstate(unsigned long s)  : m_N(624), m_M(397)
  {
    seed(s);
  }

  mtstate::mtstate(const mtstate& s)
    : State( s ),
      m_N(624),
      m_M(397)
  {
    m_mti = s.m_mti;
    m_mt.resize(m_N);
    m_mt = s.m_mt;
  }

  mtstate::~mtstate() {}
  
  void mtstate::seed(unsigned long s)
  {
    // setting initial seeds to mt[N] using        
    // the generator Line 25 of Table 1 in         
    // [KNUTH 1981, The Art of Computer Programming
    //    Vol. 2 (2nd Ed.), pp102]                 
    if (s != 0)
      {
	m_mt.resize(m_N);
	m_mt[0]= s & 0xffffffff;
	for (m_mti=1; m_mti<m_N; m_mti++)
	  m_mt[m_mti] = (69069 * m_mt[m_mti-1]) & 0xffffffff;
      }
    else
      {
	General::toss<std::invalid_argument>("mtstate::seed",
					     __FILE__,__LINE__,
					     "seed == 0");
      }
  }
	    
  unsigned long mtstate::getMT()
  { 
    if (m_mti >= m_N)
      kernel();
    return m_mt[m_mti++];
  }

  void mtstate::getMT(std::valarray<unsigned long>& mt, int nreq)
  {
    if (nreq <= 0)
      General::toss<std::out_of_range>("mtstate::getMT(valarray<>&,int)",
				       __FILE__, __LINE__, 
				       "Requested <= 0 random numbers");

    if (m_mti >= m_N)
      kernel();

    // Allocate space for random numbers
    mt.resize(nreq);

    for(int ndx = 0; ndx<nreq; )
      {
	// number remaining to allocate
	int nrem = nreq-ndx;

	// number to allocate on this iteration
	int ngen = (nrem<(m_N-m_mti))?nrem:(m_N - m_mti); 

	// allocate
	if (m_mti == 0 && ngen == m_N)
	  {
	    mt[std::slice(ndx,ngen,1)] = m_mt;
	  }
	else
	  {
#if __SUNPRO_CC
	    std::valarray< unsigned long>
	      t( m_mt[ std::slice(m_mti,ngen,1) ] );

	    mt[std::slice(ndx,ngen,1)] = t;
#else
	    mt[std::slice(ndx,ngen,1)] =
	      static_cast<const std::valarray<unsigned long> >
	      (m_mt)[std::slice(m_mti,ngen,1)];
#endif
	  }
	ndx += ngen;

	// advance internal pointer
	m_mti += ngen;

	// replenish stock
	kernel();
      }

  }

  // replenishes random number stock
  void mtstate::kernel()
  {
    const unsigned long mag01[2]={0x0, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (m_mti >= m_N) 
      { 
	/* generate N words at one time */

	unsigned long y;

        int kk;

        for (kk=0;kk<m_N-m_M;kk++) {
            y = (m_mt[kk]&UPPER_MASK)|(m_mt[kk+1]&LOWER_MASK);
            m_mt[kk] = m_mt[kk+m_M] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        for (;kk<m_N-1;kk++) {
            y = (m_mt[kk]&UPPER_MASK)|(m_mt[kk+1]&LOWER_MASK);
            m_mt[kk] = m_mt[kk+(m_M-m_N)] ^ (y >> 1) ^ mag01[y & 0x1];
        }
        y = (m_mt[m_N-1]&UPPER_MASK)|(m_mt[0]&LOWER_MASK);
        m_mt[m_N-1] = m_mt[m_M-1] ^ (y >> 1) ^ mag01[y & 0x1];

        m_mti = 0;
      }
    
  }

  mtstate* mtstate::Clone() const
  {
    return new mtstate(*this);
  }

  ILwd::LdasElement* mtstate::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    // ILwd::LdasContainer* container = new ILwd::LdasContainer;
    //    container->push_back(new ILwd::LdasArray<int>(m_mti,"mti"), false);
    //    container->push_back(new ILwd::LdasArray<unsigned long>(&((const_cast<std::valarray<unsigned long>&>(m_mt))[0]),m_mt.size(),"mt"));
    General::toss<General::unimplemented_error>("ConvertToILWD",
						__FILE__,__LINE__,
						"unimplemented");

    return 0;
  }

}

/* Period parameters */  
#undef MATRIX_A
#undef UPPER_MASK
#undef LOWER_MASK

/* Tempering parameters */   
#undef TEMPERING_MASK_B
#undef TEMPERING_MASK_C
#undef TEMPERING_SHIFT_U
#undef TEMPERING_SHIFT_S
#undef TEMPERING_SHIFT_T
#undef TEMPERING_SHIFT_L

#endif
