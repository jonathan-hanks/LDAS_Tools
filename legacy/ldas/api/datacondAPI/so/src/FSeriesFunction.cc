#include "datacondAPI/config.h"

#include <complex>

#include "TimeBoundedFreqSequenceUDT.hh"
#include "ScalarUDT.hh"
#include "TypeInfo.hh"

#include "FSeriesFunction.hh"

namespace {

    const FSeriesFunction staticFSeriesFunction;

}

FSeriesFunction::FSeriesFunction()
    : Function(FSeriesFunction::GetName())
{ }

const std::string& FSeriesFunction::
GetName() const
{
    static const std::string name("fseries");

    return name;
}

void FSeriesFunction::Eval(CallChain* chain,
                           const CallChain::Params& params,
			   const std::string& ret) const
{
    using namespace datacondAPI;
    using General::GPSTime;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int arg = 0;

    unsigned int start_s = 0;
    unsigned int start_ns = 0;

    unsigned int stop_s = 0;
    unsigned int stop_ns = 0;

    double f0 = 0.0;
    double df = 0.0;
    
    if (params.size() != 7)
    {
        throw CallChain::BadArgumentCount(GetName(), 7, params.size());
    }

    // Stop time ns
    arg = 7;
    obj = chain->GetSymbol(params[arg - 1]);
    if (const Scalar<int>* const p =
        dynamic_cast<const Scalar<int>*>(obj))
    {
        stop_ns = p->GetValue();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     arg,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                   TypeInfoTable.GetName(typeid(Scalar<int>)));
    }

    // Stop time s
    arg = 6;
    obj = chain->GetSymbol(params[arg - 1]);
    if (const Scalar<int>* const p =
        dynamic_cast<const Scalar<int>*>(obj))
    {
        stop_s = p->GetValue();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     arg,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                   TypeInfoTable.GetName(typeid(Scalar<int>)));
    }

    // Start time ns
    arg = 5;
    obj = chain->GetSymbol(params[arg - 1]);
    if (const Scalar<int>* const p =
        dynamic_cast<const Scalar<int>*>(obj))
    {
        start_ns = p->GetValue();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     arg,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                   TypeInfoTable.GetName(typeid(Scalar<int>)));
    }

    // Start time s
    arg = 4;
    obj = chain->GetSymbol(params[arg - 1]);
    if (const Scalar<int>* const p =
        dynamic_cast<const Scalar<int>*>(obj))
    {
        start_s = p->GetValue();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     arg,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                   TypeInfoTable.GetName(typeid(Scalar<int>)));
    }

    // Frequency delta (df)
    arg = 3;
    obj = chain->GetSymbol(params[arg - 1]);
    if (const Scalar<double>* const p =
        dynamic_cast<const Scalar<double>*>(obj))
    {
        df = p->GetValue();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     arg,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                TypeInfoTable.GetName(typeid(Scalar<double>)));
    }

    // Start frequency (f0)
    arg = 2;
    obj = chain->GetSymbol(params[arg - 1]);
    if (const Scalar<double>* const p =
        dynamic_cast<const Scalar<double>*>(obj))
    {
        f0 = p->GetValue();
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     arg,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                TypeInfoTable.GetName(typeid(Scalar<double>)));
    }
    
    // Frequency data
    arg = 1;
    obj = chain->GetSymbol(params[arg - 1]);
    
    if (const Sequence<float>* const p
        = dynamic_cast<const Sequence<float>*>(obj))
    {
        result = new TimeBoundedFreqSequence<float>(*p, 
                                                    f0,
                                                    df,
                                                    GPSTime(start_s, start_ns),
                                                    GPSTime(stop_s, stop_ns));
    }
    else if (const Sequence<double>* const p
             = dynamic_cast<const Sequence<double>*>(obj))
    {
        result = new TimeBoundedFreqSequence<double>(*p,
                                                     f0,
                                                     df,
                                                    GPSTime(start_s, start_ns),
                                                    GPSTime(stop_s, stop_ns));
    }
    else if (const Sequence<std::complex<float> >* const p
             = dynamic_cast<const Sequence<std::complex<float> >*>(obj))
    {
        result = new TimeBoundedFreqSequence<std::complex<float> >(*p,
                                                              f0,
                                                              df,
                                                    GPSTime(start_s, start_ns),
                                                    GPSTime(stop_s, stop_ns));
    }
    else if (const Sequence<std::complex<double> >* const p
             = dynamic_cast<const Sequence<std::complex<double> >*>(obj))
    {
        result = new TimeBoundedFreqSequence<std::complex<double> >(*p,
                                                               f0,
                                                               df,
                                                    GPSTime(start_s, start_ns),
                                                    GPSTime(stop_s, stop_ns));
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     arg,
                                     TypeInfoTable.GetName(typeid(*obj)),
                                     "Object derived from Sequence");
    }

    chain->ResetOrAddSymbol(ret, result);
}
