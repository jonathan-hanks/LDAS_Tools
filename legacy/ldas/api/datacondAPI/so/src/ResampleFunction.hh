/* -*- mode: c++; c-basic-offset: 4 -*- */
#ifndef RESAMPLE_FUNCTION_HH
#define RESAMPLE_FUNCTION_HH

#include "CallChain.hh"

//-----------------------------------------------------------------------------
// Forward declarations
//-----------------------------------------------------------------------------

namespace datacondAPI {

    template <class DataType_> class Sequence;

}

//-----------------------------------------------------------------------------
//: Function support for resample action
//
// This class implements the "resample" function to be used in the action
// section of a Tcl command. The syntax, as it would appear in the action
// sequence is:
//
//     [y = ] resample(x, p, q, [ n ], [ beta ], [ z ])
//
// where y, x are valarray<T>, p, q are int and optional z will be set
// to a ResampleState on return.
//
class ResampleFunction : public CallChain::Function {
    
public:
    
    //-------------------------------------------------------------------------
    //: Constructor
    //
    // Construct a new ResampleFunction
    ResampleFunction();
    
    //-------------------------------------------------------------------------
    //: Evaluate the Resample Function
    //
    // This performs the resampling. The list of parameters must contain
    // doubles phase and carrier and a valarray of input data.
    //
    //!param: CallChain* Chain - A pointer to the CallChain
    //!param: const CallChain::Params& Params - A list of parameter names
    //!param: const std::string& Ret - The name of the return variable
    //
    virtual void Eval(CallChain* Chain,
                      const CallChain::Params& Params,
                      const std::string& Ret) const;
    
    //-------------------------------------------------------------------------
    //: Return the Name of the function ("resample")
    //
    //!return: const std::string& - Returns a reference to the name of
    //+the function
    virtual const std::string& GetName(void) const;

    //-------------------------------------------------------------------------
    //: Validate parameters
    virtual void
    ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
			const CallChain::Params& Params ) const;

private:

    //: test if argument is a Sequence<T> for T float, double, complex<float>, 
    //+ or complex<double>
    //!param: const udt& - object to test
    //!return: bool - true if of Sequence type
    bool isASequence(const datacondAPI::udt&) const;
    

    inline void
    validation_assumption( CallChain::Step::sudo_symbol_table_type&
			   SymbolTable,
			   const std::string& Symbol ) const
    {
	try
	{
	    validateParameter( SymbolTable, Symbol, PARAM_READ );
	}
	catch( ... )
	{
	    validateParameter( SymbolTable, Symbol, PARAM_WRITE );
	}
    }
};

//-----------------------------------------------------------------------------
//: Function support for an action that gets the resample delay
//
// This class implements the "getResampleDelay" function to be used in the
// action section of a Tvcl command. The syntax, as it would appear in the
// action sequence is:
//
// [delay = ] getResampleDelay(z)
//
// where z is a ResampleState.
//
class GetResampleDelayFunction : public CallChain::Function {
    
public:
    
    //-------------------------------------------------------------------------
    //: Constructor
    GetResampleDelayFunction();
    
    //-------------------------------------------------------------------------
    //: Evaluate the GetResampleDelay Function
    //
    // This retrieves the delay.
    //
    //!param: CallChain* Chain - A pointer to the CallChain
    //!param: const CallChain::Params& Params - A list of parameter names
    //!param: const std::string& Ret - The name of the return variable
    virtual void Eval(CallChain* Chain,
                      const CallChain::Params& Params,
                      const std::string& Ret) const;
    
    //-------------------------------------------------------------------------
    //: Return the Name of the function ("getResampleDelay")
    //
    //!return: const std::string& - Returns a reference to the name of
    //+the function
    virtual const std::string& GetName(void) const;

private:
};

#endif // RESAMPLE_FUNCTION_HH
