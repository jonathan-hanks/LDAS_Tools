#ifndef PRECISIONCONVERSION_HH
#define PRECISIONCONVERSION_HH

// $Id: PrecisionConversion.hh,v 1.1 2002/05/16 22:14:36 Philip.Charlton Exp $

//
// Functions for converting the precision (double/single) of datacon objects
//

namespace datacondAPI {

    // Forward declaration
    class udt;

    //
    //: Take a UDT of any precision and produce an equivalent single-precision
    //+representation
    //
    // Ths function takes a UDT and attempts to convert it to a UDT of the
    // same class but at single precision (ie. using floats instead of
    // doubles). The meta-data of the original UDT is preserved.
    //
    //!return: a pointer to a UDT allocated on the heap. The caller is
    //+responsible for deleting the object
    //
    //!exc: invalid_argument - Thrown if there is no method of converting
    //+the UDT
    udt* convert_to_float(const udt& in);
    
    //
    //: Take a UDT of any precision and produce an equivalent double-precision
    //+representation
    //
    // Ths function takes a UDT and attempts to convert it to a UDT of the
    // same class but at double precision (ie. using doubles instead of
    // floats). The meta-data of the original UDT is preserved.
    //
    //!return: a pointer to a UDT allocated on the heap. The caller is
    //+responsible for deleting the object
    //
    //!exc: invalid_argument - Thrown if there is no method of converting
    //+the UDT
    udt* convert_to_double(const udt& in);

} // namespace datacondAPI

#endif // PRECISIONCONVERSION_HH
