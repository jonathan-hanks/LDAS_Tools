#include "datacondAPI/config.h"

#include "HaarPSUFunction.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"
#include "HaarPSU.hh"

using namespace datacondAPI::psu;


const std::string& HaarPSUFunction::GetName(void) const
{
  static std::string name("HaarPSU");
  
  return name;
}

HaarPSUFunction::HaarPSUFunction() : Function(GetName())
{ }

static HaarPSUFunction _HaarPSUFunction;

void HaarPSUFunction::Eval(CallChain* Chain,
			const CallChain::Params& Params,
			const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;   

  switch(Params.size())
    {
    case 0:
      {
	Haar haarWavelet;
	returnedSymbol = new Haar(haarWavelet);
      

	break;
      }//end case 0
    case 1:
      {

	int order = 0;
	if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[0])))
	  order = datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[0])).GetValue();

	Haar haarWavelet(order);
	returnedSymbol = new Haar(haarWavelet);

	break;
      }//end case 1

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: HaarFunctionPSU");
	break;
      }//end default


    }//end switch
      
      if(!returnedSymbol)
	{
	  throw CallChain::NullResult( GetName() );
	}
      
      Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval

