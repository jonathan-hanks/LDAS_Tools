/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef TIMING_FUNCTIONS_HH
#define	TIMING_FUNCTIONS_HH

// $Id: TimingFunctions.hh,v 1.4 2001/03/31 23:46:56 LSF5 Exp $

#include "CallChain.hh"

//!ppp: {eval `cat $ENV{SRCDIR}/cc_rules.pl`}

//: Class of methods for the cpu_time action (which returns the 
//+ amount of time spent in kernel space)
class CpuTimeFunction: CallChain::Function
{
public:

  //: Default constructor
  CpuTimeFunction();

  //: method that evaluates to the kernel space time
  //!param: CallChain* Chain - pointer to the call chain
  //!param: const CallChain:Params& Params - a list of parameter 
  //+ (null for this action)
  //!param: const std::string& Ret - the name of the return variable
  void Eval(CallChain* Chain,
	    const CallChain::Params& Params,
	    const std::string& Ret) const;

  //: the name of the action (cpu_time for this action)
  //!return: std::string& - the name of the action (cpu_time for this action)
  virtual const std::string& GetName(void) const;
};

inline const std::string& CpuTimeFunction::
GetName(void) const
{
  static std::string name("cpu_time");
  return name;
}

//: Class of methods for the time action 
//+ (allowing evaluation of cpu, user, and wall times)
class TimeFunction: CallChain::Function
{
public:

  TimeFunction();

  //: method that evaluates to a Sequence 
  //+ containing the user, kernel, and wall time
  //!param: CallChain* Chain - pointer to the call chain
  //!param: const CallChain:Params& Params - a list of parameter 
  //+ (null for this action)
  //!param: const std::string& Ret - the name of the return variable
  void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;

  //: the name of the action (time for this action)
  //!return: std::string& - the name of the action (time for this action)
  virtual const std::string& GetName(void) const;
};

inline const std::string& TimeFunction::
GetName(void) const
{
  static std::string name("time");
  return name;
}

//: Class of methods for the user_time action (which returns the 
//+ amount of time spent in user space)
class UserTimeFunction: CallChain::Function
{
public:

  UserTimeFunction();
 
  //: method that evaluates to the user space time
  //!param: CallChain* Chain - pointer to the call chain
  //!param: const CallChain:Params& Params - a list of parameter 
  //+ (null for this action)
  //!param: const std::string& Ret - the name of the return variable
 virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;

  //: the name of the action (user_time for this action)
  //!return: std::string& - the name of the action (user_time for this action)
  virtual const std::string& GetName(void) const;
};

inline const std::string& UserTimeFunction::
GetName(void) const
{
  static std::string name("user_time");
  return name;
}

//: Class of methods for the user_time action (which returns the 
//+ amount of wall time spent)
class WallTimeFunction: CallChain::Function
{
public:

  WallTimeFunction();

  //: method that evaluates to the user space time
  //!param: CallChain* Chain - pointer to the call chain
  //!param: const CallChain:Params& Params - a list of parameter 
  //+ (null for this action)
  //!param: const std::string& Ret - the name of the return variable
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;

  //: the name of the action (wall_time for this action)
  //!return: std::string& - the name of the action (wall_time for this action)
  virtual const std::string& GetName(void) const;
};

inline const std::string& WallTimeFunction::
GetName(void) const
{
  static std::string name("wall_time");
  return name;
}

#endif /* TIMING_FUNCTIONS_HH */
