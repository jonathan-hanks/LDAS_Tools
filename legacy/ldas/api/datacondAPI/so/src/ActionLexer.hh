#ifndef DATACONDAPI__ACTION_LEXER_HH
#define DATACONDAPI__ACTION_LEXER_HH

#ifndef DATACOND_API__ACTION_LEXER_CC
#define yyFlexLexer ActionFlexLexer
#include <FlexLexer.h>
#undef yyFlexLexer
#endif /* DATACOND_API__ACTION_LEXER_CC */

#include "ActionParser.hh"

namespace datacondAPI
{
  class ActionLexer
    : public ActionFlexLexer
  {
  public:
    typedef ActionParser::semantic_type              semantic_type;
    typedef ActionParser::location_type              location_type;

    ActionLexer( std::istream* ArgYYIn = 0, std::ostream* ArgYYOut = 0);

    // This function is defined in ActionLexerLL.ll
    virtual int yylex();

    void Reset( );
    
    void SetYYLloc( location_type* YYLloc );
    void SetYYLval( semantic_type* YYLval );

  protected:
    int	lineno;

  private:
    enum {
      MODE_UNSET,
      MODE_OUTPUT
    };

    int			m_mode;
    int			m_param_count;
    semantic_type*      m_yylval;
    location_type*      m_yylloc;

    void mode( int Mode );

    bool next_option_is_string( ) const;

    void param_count_inc( );
    void param_count_reset( );
  };

  inline void ActionLexer::
  SetYYLloc( location_type* YYLloc )
  {
    m_yylloc = YYLloc;
  }

  inline void ActionLexer::
  SetYYLval( semantic_type* YYLval )
  {
    m_yylval = YYLval;
  }

  inline void ActionLexer::
  mode( int Mode )
  {
    m_mode = Mode;
  }

  inline void ActionLexer::
  param_count_inc( )
  {
    ++m_param_count;
  }

  inline void ActionLexer::
  param_count_reset( )
  {
    m_param_count = 0;
  }
} /* namespace - datacondAPI */

#endif /* DATACONDAPI__ACTION_LEXER_HH */
