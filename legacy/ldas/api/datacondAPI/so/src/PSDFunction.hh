#ifndef PSDFUNCTION_HH
#define PSDFUNCTION_HH

#include "CallChain.hh"

class PSDFunction : CallChain::Function
{
public:
    //: Default constructor - construction of dummy instance registers  
    //+ the Eval method as the handler for calls to the action named by 
    //+ the GetName method
    PSDFunction();
    
    //: Evaluate an action call
    //!param: Chain - environment of the action call
    //!param: Params - container of parameter names
    //!param: Ret - return value name
    virtual void Eval(CallChain* Chain,
		      const CallChain::Params& Params, 
                      const std::string& Ret) const;

    //: Get the name of the handled action
    //!return: The name of the handled action
    virtual const std::string& GetName() const;
};

#endif // PSDFUNCTION_HH
