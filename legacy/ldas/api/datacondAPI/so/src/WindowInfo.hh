#ifndef WINDOWINFO_HH
#define WINDOWINFO_HH

//!ppp: {eval `cat $ENV{SRCDIR}/cc_rules.pl`}

#include <string>

namespace datacondAPI 
{
	
    //
    //: A class for holding information about a window
    //
    // WindowInfo consists of data members (a string and a double
    // precision variable) that can hold the name of a window--e.g., 
    // "Rectangular Window", and a parameter value (if any) needed 
    // to define the window.
    //
    class WindowInfo {
    public:
	//
	//: Constructor
	//
	//!param: name - window name 
	//!param: param - parameter defining the window (if any)
	//
	WindowInfo(const std::string& name = "", 
		   const double param = 0);
		
	//
	//: Return the name of the window
	//
	//!return: std::string - window name
	//
	std::string name() const;
		
	//
	//: Return parameter value defining the window (if any)
	//
	//!return: double - parameter
	//
	double parameter() const;

	//
	//: Equality operator for WindowInfo objects
	//
	//!return: bool - true if lhs==rhs
	//
	bool operator==(const WindowInfo& rhs) const;

	//
	//: Inequality operator for WindowInfo objects
	//
	//!return: bool - true if lhs!=rhs
	//
	bool operator!=(const WindowInfo& rhs) const;

    private:

	//: window name
	std::string m_name;

	//: window parameter
	double m_parameter;
    };

    inline
    WindowInfo::WindowInfo(const std::string& name, 
			   const double param)
	: m_name(name), 
	  m_parameter(param) 
    { 
    }

    inline
    std::string WindowInfo::name() const 
    { 
	return m_name; 
    }
    
    inline
    double WindowInfo::parameter() const 
    { 
	return m_parameter; 
    }

    inline
    bool WindowInfo::operator==(const WindowInfo& rhs) const
    {
	return ((m_parameter == rhs.m_parameter) && (m_name == rhs.m_name));
    }

    inline
    bool WindowInfo::operator!=(const WindowInfo& rhs) const
    {
	return !(rhs == *this);
    }
    
} // end of datacondAPI namespace

#endif // WINDOWINFO_HH

