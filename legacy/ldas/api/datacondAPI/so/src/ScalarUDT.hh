/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef ScalarUDT_HH
#define	ScalarUDT_HH
// $Id: ScalarUDT.hh,v 1.17 2006/02/16 16:54:58 emaros Exp $
//-----------------------------------------------------------------------
// This class has information and methods for handing a UDT of type
//   scalar (a single number).
//-----------------------------------------------------------------------

#include "UDT.hh"

// Forward declaration of ILwd elements
namespace ILwd
{
  class LdasElement;
}

namespace datacondAPI
{
  //: This class implements all scalar types that are supported by
  //+ the dataconditioning API.
  template <class DataType_>
  class Scalar: public udt
  {
  public:
    //: Constructor
    explicit Scalar(DataType_ Value = DataType_());

    //: Destructor
    virtual ~Scalar();

    //: Make duplicate of *this on heap
    //!return: datacondAPI::Scalar<DataType_>* - duplicate of *this, 
    //+ allocated on heap
    virtual datacondAPI::Scalar< DataType_ >* Clone(void) const;

    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target 
		   = datacondAPI::udt::TARGET_GENERIC) const;

    //: Make it look like we have inherited from the Scalar Type
    //!return: DataType_ - value 
    inline const DataType_ GetValue(void) const;

    //: When implicit conversion fails
    inline void SetValue(const DataType_&);

    //: Make it look like we have inherited from the Scalar Type
    //!return: const DataType& - value
    operator const DataType_& () const;

    //: Make it look like we have inherited from the Scalar Type
    //!return: DataType_& - value
    operator DataType_&();

  private:
    DataType_	m_value;

  };	/* Scalar */

  template <class DataType_>
  const DataType_ Scalar<DataType_>::GetValue(void) const
  {
    return m_value;
  }

  template <class DataType_>
  void Scalar<DataType_>::SetValue(const DataType_& a)
  {
    m_value = a;
  }

  template <class DataType_>
  inline Scalar<DataType_>::
  operator const DataType_& () const
  {
    return m_value;
  }

  template <class DataType_>
  inline Scalar<DataType_>::
  operator DataType_&()
  {
    return m_value;
  }
}	/* namespace - datacondAPI */

#endif	/* ScalarUDT_HH */
