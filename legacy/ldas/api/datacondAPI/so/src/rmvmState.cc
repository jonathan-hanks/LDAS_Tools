#include "config.h"

#include "rmvmState.hh"

namespace datacondAPI
{
  rmvmState::rmvmState(Sequence<double>& freqs, Sequence<double>& Qs,
		       double& min_f, double& max_f, Matrix<double>& A,
		       Matrix<double>& C, Matrix<double>& W, Matrix<double>& V,
		       Sequence<double>& psi, Matrix<double>& P) : m_Kalman(A,C,W,V,psi,P)
  {
    if(freqs.size() != Qs.size())
      throw invalid_argument("Size of frequencies list must match size of qualities list");
    if(min_f<=0 || max_f<=0)
      throw invalid_argument("Min and Max frequencies must be greater than 0");
    if(min_f>=max_f)
      throw invalid_argument("Min frequency must be less than Max frequency");

    m_freqs = freqs;
    m_Qs = Qs;
    m_min_f = min_f;
    m_max_f = max_f;
  }

  
  rmvmState::rmvmState(udt& freqs, udt& Qs, udt& min_f, udt& max_f, udt& A,
		       udt& C, udt& W, udt& V, udt& psi, udt& P) 
    : m_Kalman(udt::Cast<Matrix<double> >(A),udt::Cast<Matrix<double> >(C),udt::Cast<Matrix<double> >(W),
	       udt::Cast<Matrix<double> >(V),udt::Cast<Sequence<double> >(psi),udt::Cast<Matrix<double> >(P))
  {
    if(udt::IsA<Sequence<double> >(freqs) && udt::IsA<Sequence<double> >(Qs) && udt::IsA<Scalar<double> >(min_f)
       && udt::IsA<Scalar<double> >(max_f))
      {


	if(udt::Cast<Sequence<double> >(freqs).size() != udt::Cast<Sequence<double> >(Qs).size())
	  throw invalid_argument("Size of frequencies list must match size of qualities list");
	if(udt::Cast<Scalar<double> > (min_f).GetValue()<=0 || udt::Cast<Scalar<double> > (max_f).GetValue()<=0)
	  throw invalid_argument("Min and Max frequencies must be greater than 0");
	if(udt::Cast<Scalar<double> > (min_f).GetValue()>=udt::Cast<Scalar<double> > (max_f).GetValue())
	  throw invalid_argument("Min frequency must be less than Max frequency");
	
	m_freqs = udt::Cast<Sequence<double> >(freqs);
	m_Qs = udt::Cast<Sequence<double> >(Qs);
	m_min_f = udt::Cast<Scalar<double> > (min_f).GetValue();
	m_max_f = udt::Cast<Scalar<double> > (max_f).GetValue();
  
      }
    else
      throw std::invalid_argument("Wrong UDT type for rmvmState");
  }

  rmvmState::rmvmState(const rmvmState& s) : m_Kalman(s.m_Kalman)
  {
    m_freqs = s.m_freqs;
    m_Qs = s.m_Qs;
    m_min_f = s.m_min_f;
    m_max_f = s.m_max_f;
  }

  void rmvmState::getFreq(Sequence<double>& freqs)
  {
    freqs.resize(m_freqs.size());
    freqs = m_freqs;
  }
  
  void rmvmState::getFreq(udt*& freqs)
  {
    if(freqs ==  NULL)
      {
	freqs = new Sequence<double> (m_freqs);
	return;
      }

    if(udt::IsA<Sequence<double> >(*freqs))
      {
	udt::Cast<Sequence<double> >(*freqs).resize(m_freqs.size());
	udt::Cast<Sequence<double> >(*freqs) = m_freqs;
      }
    else
      throw invalid_argument("Wrong udt type for getFreq");
  }
  
  void rmvmState::getQ(Sequence<double>& Qs)
  {
    Qs.resize(m_Qs.size());
    Qs = m_Qs;
  }


  void rmvmState::getQ(udt*& Qs)
  {
    if(Qs ==  NULL)
      {
	Qs = new Sequence<double> (m_Qs);
	return;
      }
 
    if(udt::IsA<Sequence<double> >(*Qs))
      {
	udt::Cast<Sequence<double> >(*Qs).resize(m_Qs.size());
	udt::Cast<Sequence<double> >(*Qs) = m_Qs;
      }
    else
      throw invalid_argument("Wrong udt type for getQs");
  }

  void rmvmState::getMinF(double& min_f)
  {
    min_f = m_min_f;
  }
  
  void rmvmState::getMinF(udt*& min_f)
  {
    if(min_f ==  NULL)
      {
	min_f = new Scalar<double> (m_min_f);
	return;
      }
 
    if(udt::IsA<Scalar<double> >(*min_f))
      {
	udt::Cast<Scalar<double> >(*min_f).SetValue(m_min_f);
      } 
    else
      throw invalid_argument("Wrong udt type for getMinF");
  }

  void rmvmState::getMaxF(double& max_f)
  {
    max_f = m_max_f;
  }
  
  void rmvmState::getMaxF(udt*& max_f)
  {
    if(max_f ==  NULL)
      {
	max_f = new Scalar<double> (m_max_f);
	return;
      }
 
    if(udt::IsA<Scalar<double> >(*max_f))
      {
	udt::Cast<Scalar<double> >(*max_f).SetValue(m_max_f);
      } 
    else
      throw invalid_argument("Wrong udt type for getMaxF");
  }
}
