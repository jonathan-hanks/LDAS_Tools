/* -*- mode: c++ -*- */
#ifndef SUBTRACTION_FUNCTION_HH
#define SUBTRACTION_FUNCTION_HH

#include <valarray>

#include "CallChain.hh"
#include "UDT.hh"

class SubtractionFunction: public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  SubtractionFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

#ifdef BASIC_MATH_FUNCTION_CC
#define	OP -
#define	OPEQ -=
#define	OPERATION "sub"
#define	CLASS SubtractionFunction
#endif

#endif	/* SUBTRACTION_FUNCTION_HH */
