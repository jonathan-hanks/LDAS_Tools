/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef CONCATFUNCTION_HH
#define CONCATFUNCTION_HH

// $Id: ConcatFunction.hh,v 1.1 2002/04/13 01:27:34 Philip.Charlton Exp $

#include "CallChain.hh"

//-----------------------------------------------------------------------------
//: Function support for concat action
//
// This class implements the "concat" function to be used in the action
// section of a user command. The syntax, as it would appear in the action
// sequence is one of:
//
//     y = concat(x1, x2, [ x3, ... ] );
//
class ConcatFunction : public CallChain::Function {
    
public:
    
  //-------------------------------------------------------------------------
  //: Constructor
  //
  // Construct a new ConcatFunction
  ConcatFunction();
    
  //-------------------------------------------------------------------------
  //: Evaluate the Concat Function
  //
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  //
  virtual void Eval(CallChain* chain,
                    const CallChain::Params& params,
                    const std::string& ret) const;
    
  //-------------------------------------------------------------------------
  //: Return the Name of the function ("concat")
  //
  //!return: const std::string& ptName - Returns a reference to the name of
  //!return: the function
  virtual const std::string& GetName() const;
        
};

#endif // CONCATFUNCTION_HH
