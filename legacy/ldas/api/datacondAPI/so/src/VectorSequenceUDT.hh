/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef VECTORSEQUENCE_UDT_HH
#define	VECTORSEQUENCE_UDT_HH
// $Id: VectorSequenceUDT.hh,v 1.17 2007/06/18 15:26:42 emaros Exp $

#include <valarray>
#include <general/unexpected_exception.hh>

#include "UDT.hh"

// Forward declaration of ILwd elements
namespace ILwd
{
  class LdasElement;
}

namespace datacondAPI
{

  template <class DataType_> class Sequence;
  template <class DataType_> class udt_slice_array;

  //---------------------------------------------------------------------
  //: Class for representing vector-valued sequence data 
  //---------------------------------------------------------------------
  template <class DataType_>
  class VectorSequence : public udt
  {
  public:
    //: Default constructor
    VectorSequence(void);
    
    //: Constructor that specifies dimensions
    //!param: vdim - vector dimension (dimension of each vector)
    //!param: sdim - sequence dimension (number vectors in sequence)
    //!exc: std::invalid_argument - when either of vdim, sdim <= 0
    VectorSequence(int vdim, int sdim);
    
    //: Constructor that specifies dimensions and datas
    //!param: data - the data, arranged as consequtive vectors
    //!param: vdim - the number of elements in each vector
    //!param: sdim - the number of vectors in this sequence
    //!exc: std::invalid_argument - when either of vdim, sdim <= 0
    //!exc: std::invalid_argument - when data.size() != vdim*sdim
    VectorSequence(const std::valarray<DataType_>& data, 
		   int vdim, 
		   int sdim);
    
    //: Copy constructor
    //!param: const VectorSequence<DataType_>& - object to copy from
    //!exc: std::invalid_argument - when either of vdim, sdim <= 0
    //!exc: std::invalid_argument - when data.size() != vdim*sdim
    VectorSequence(const VectorSequence<DataType_>&);
    
    //: Destructor
    virtual ~VectorSequence();
    
    //: Convert *this to an appropriate ilwd
    //!param: Chain - CallChain *this is part of
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasElement* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasElement*
    ConvertToIlwd(  const CallChain& Chain,
		    datacondAPI::udt::target_type Target 
		    = datacondAPI::udt::TARGET_GENERIC) const;

    //: Duplicate *this on heap
    virtual datacondAPI::VectorSequence< DataType_ >* Clone(void) const;

    // accessors 

    //: return the dimension of the vectors in this sequence
    //!return: const int - the dimension of the vectors in this sequence
    int vDim(void) const;

    //: return the dimension of the sequence
    //!return: const int - the number of (vector-valued) 
    //+ elements in this sequence
    int sDim(void) const;

    //: return the vector at a specified position in the sequence
    //!param: int - position in the sequence to retrieve vector from
    //!return: std::valarray<DataType_> - the vector
    const std::valarray<DataType_> vec(int) const; 

    //: return a sequence corresponding to a particular element of each vector
    //!param: int - element in vector
    //!return: Sequence<DataType_> - the sequence
    const Sequence<DataType_> seq(int) const;

    // mutators

    //: Change the dimensions of *this. Destroy current data contents
    //!param: vdim - new vector dimension
    //!param: sdim - new sequence dimension
    //!exc: std::invalid_argument - when either of vdim, sdim <= 0
    void resize(const int vdim, const int sdim); 
    
    //: Allow restricted reference, for arithmetic
    //+  operations, to a vector in the sequence
    //!param: int - vector to manipulate
    //!return: udt_slice_array<DataType_> - holds reference to vector
    udt_slice_array<DataType_> vec(int);

    //: Allow restricted reference, for arithmetic operations, 
    //+ to a sequence constructed from vector elements
    //!param: int - element of vectors to manipulate
    //!return: udt_slice_array<DataType_> - holds reference to sequence
    udt_slice_array<DataType_> seq(int);
 
    //: assignment operator allows setting all elements to same value
    //!param: const DataType_ - value to assign
    //!return: VectorSequence<DataType_>& - reference to *this
    VectorSequence<DataType_>& operator=(const DataType_);

  private:
 
    //: sequence dimension of *this
    int m_sdim;

    //: vector dimension of *this
    int m_vdim;

    //: actual data held by *this
    std::valarray<DataType_> m_data;

  };	/* VectorSequence<T> */

}	/* namespace - datacondapi<T> */

#include <complex>
#include <stdexcept>

#include "SequenceUDT.hh"
#include "udtslice_array.h"

template<class DataType_>
inline datacondAPI::VectorSequence<DataType_>::
VectorSequence()
  : m_sdim(0),
    m_vdim(0),
    m_data(0)
{
}

template<class DataType_>
inline datacondAPI::VectorSequence<DataType_>::
VectorSequence(int vdim, int sdim)
  : m_sdim(sdim),
    m_vdim(vdim)
{
  if (sdim <= 0) 
    throw std::invalid_argument("sdim <= 0");
  if (vdim <= 0) 
    throw std::invalid_argument("vdim <= 0");
  m_data.resize(sdim*vdim);
}

template<class DataType_>
inline datacondAPI::VectorSequence<DataType_>::
VectorSequence(const std::valarray<DataType_>& data,
	       int vdim, int sdim)
  : m_sdim(sdim),
    m_vdim(vdim),
    m_data(0)
{
  if (sdim <= 0) 
    throw std::invalid_argument("sdim <= 0");
  if (vdim <= 0) 
    throw std::invalid_argument("vdim <= 0");
  if (data.size() != (size_t)(vdim*sdim))
    throw std::invalid_argument("data.size() != vdim*sdim");
  m_data.resize(data.size());
  m_data = data;
}

template<class DataType_>
inline datacondAPI::VectorSequence<DataType_>::
VectorSequence(const VectorSequence<DataType_>& vs)
  : udt( vs )
{
  if ( (m_vdim = vs.vDim()) <= 0 ) 
    throw std::invalid_argument("vdim <= 0");
  if ( (m_sdim = vs.sDim()) <= 0 ) 
    throw std::invalid_argument("sdim <= 0");

  m_data.resize(m_sdim*m_vdim);

  for (int k = 0; k < m_vdim; k++)
    {
      std::valarray<DataType_> t(m_sdim);
      t = vs.seq(k);
      m_data[std::slice(k,m_sdim,m_vdim)] = t;
    }
}

template<class DataType_>
inline datacondAPI::VectorSequence<DataType_>::
~VectorSequence()
{
}

template <class DataType_>
inline datacondAPI::VectorSequence< DataType_ >* datacondAPI::
VectorSequence<DataType_>::
Clone(void) const
{
  return new VectorSequence<DataType_>(*this);
}

template <class DataType_>
inline int datacondAPI::VectorSequence<DataType_>::
vDim(void) const 
{
  return m_vdim;
}

template <class DataType_>
inline int datacondAPI::VectorSequence<DataType_>::
sDim(void) const 
{
  return m_sdim;
}

template <class DataType_>
inline void datacondAPI::VectorSequence<DataType_>::
resize(const int nvec, const int nseq)
{
  if (nvec <= 0)
    throw std::invalid_argument("nvec <= 0");
  if (nseq <= 0)
    throw std::invalid_argument("nseq <= 0");
  m_data.resize(nseq*nvec);
  m_vdim = nvec;
  m_sdim = nseq;
}

// accessors

template <class DataType_>
inline const std::valarray<DataType_> 
datacondAPI::VectorSequence<DataType_>::
vec(int k) const
{
  return m_data[std::slice(k*m_vdim,m_vdim,1)];
}

template <class DataType_>
inline const datacondAPI::Sequence<DataType_> 
datacondAPI::VectorSequence<DataType_>::
seq(int k) const 
{
  return Sequence<DataType_>(m_data[std::slice(k,m_sdim,m_vdim)]);
}

// mutators

template <class DataType_>
inline datacondAPI::udt_slice_array<DataType_>
datacondAPI::VectorSequence<DataType_>::
vec(int k)
{
  return udt_slice_array<DataType_>(m_data,std::slice(k*m_vdim,m_vdim,1));
}

template <class DataType_>
inline datacondAPI::udt_slice_array<DataType_> 
datacondAPI::VectorSequence<DataType_>::
seq(int k)
{
  return udt_slice_array<DataType_>(m_data, std::slice(k,m_sdim,m_vdim));
}

template <class DataType_>
inline datacondAPI::VectorSequence<DataType_>& 
datacondAPI::VectorSequence<DataType_>::
operator=(const DataType_ d) 
{
  m_data = d;
  return *this;
}

///----------------------------------------------------------------------
/// Private methods
///----------------------------------------------------------------------

#endif	/* VECTORSEQUENCE_UDT_HH */
