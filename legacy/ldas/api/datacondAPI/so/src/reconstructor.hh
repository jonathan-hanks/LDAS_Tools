//reconstructor.hh

#ifndef RECONSTRUCTOR_HH
#define RECONSTRUCTOR_HH

#include <valarray>
#include <memory>   

#include "general/Memory.hh"

#include "result.hh"
#include "UDT.hh"
#include "WaveletPSU.hh"

namespace datacondAPI{
namespace psu
{
  class Reconstructor
  {

  public:
  
    //: default constructor
    Reconstructor();

    //: default destructor
    ~Reconstructor();

    //: full reconstruction -- datacondAPI::udt
    void apply(datacondAPI::udt*&, const datacondAPI::udt&);
    
    //: generates entire original signal
    //!param: datacondAPI::Sequence<T> out - reconstructed signal
    //!param: const Result<T>& in - wavelet decomposition of original signal
    template<class T>
    void apply (datacondAPI::Sequence<T>&, const Result<T>&);

    //: partial reconstruction -- datacondAPI::udt
    void apply(datacondAPI::udt*&, const datacondAPI::udt&, const datacondAPI::udt&);
    
    //: generates signal using specified level
    //!param: datacondAPI::Sequence<T> out - reconstructed signal
    //!param: const Result<T>& in - wavelet decomposition of original signal
    //!param: int level - level from which signal is reconstructed
    template<class T>
    void apply (datacondAPI::Sequence<T>&, const Result<T>&, const int);

    //: Copy constructor
    //!param: const Reconstructor& rcstr - instance to be copied
    Reconstructor(const Reconstructor& rcstr);

  private:

    //: gets synthesis filter coefficients
    //!param: const Result<T>& in - Result object to be reconstructed
    //!param: Wavelet* - reconst_wavelet
    //!param: datacondAPI::Sequence<double>& sCoeff_high - highpass synthesis coefficients
    //!param: datacondAPI::Sequence<double>& sCoeff_low - lowpass synthesis coefficients
    template<class T>
    void getSynthesis(const Result<T>&, std::unique_ptr<Wavelet>&, datacondAPI::Sequence<double>&, datacondAPI::Sequence<double>&);

    //: recursive apply (total reconstruction)
    //!param: datacondAPI::<T>& out - reconstructed signal
    //!param: const Result<T>& in - wavelet decomposition of original signal
    //!param: datacondAPI::<T>& r - holds current reconstruction
    //!param: const int levelnumber - current level of reconstruction 
    template<class T>
    void apply (datacondAPI::Sequence<T>&, const Result<T>&, datacondAPI::Sequence<T>&, int);

  }; //class Reconstructor

} //namespace psu
} //namespace datacondAPI

#endif

