/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef INTERPOLATEFUNCTION_HH
#define INTERPOLATEFUNCTION_HH

// $Id: InterpolateFunction.hh,v 1.2 2007/01/24 17:51:27 emaros Exp $

#include "CallChain.hh"

//-----------------------------------------------------------------------------
//: Function support for Interpolate action
//
// This class implements the "interpolate" function to be used in the action
// section of a TCL command. The syntax, as it would appear in the action
// sequence is one of:
//
//     y = interpolate(x, alpha, order, [z]);
//
// or
//
//     y = interpolate(x, z);
//
// where x is a time-series, alpha is the fraction we want to shift by
// ( 0 < alpha < 1), order is the degree of the Lagrange polynomial to be used
// for interpolation, and z is the (optional) state variable, 
//
//
class InterpolateFunction: CallChain::Function
{
    
public:
    
  //-------------------------------------------------------------------------
  //: Constructor
  //
  // Construct a new InterpolateFunction
  InterpolateFunction();
    
  //-------------------------------------------------------------------------
  //: Evaluate the Interpolate Function
  //
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  //
  virtual void Eval(CallChain* chain,
                    const CallChain::Params& params,
                    const std::string& ret) const;
    
  //-------------------------------------------------------------------------
  //: Return the Name of the function ("interpolate")
  //
  //!return: const std::string& ptName - Returns a reference to the name of
  //+        the function
  virtual const std::string& GetName() const;
        
  //-------------------------------------------------------------------------
  //: Validate parameters
  virtual void
  ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		      const CallChain::Params& Params ) const;
};

#endif // INTERPOLATEFUNCTION_HH
