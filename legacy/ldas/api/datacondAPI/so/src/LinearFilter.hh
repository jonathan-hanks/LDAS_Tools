#ifndef LINEARFILTER_HH
#define LINEARFILTER_HH

#include <valarray>

#include "StateUDT.hh"

namespace datacondAPI
{
    class LinearFilter : public State
    {

    public:

        // override defaults

        LinearFilter();
        LinearFilter(const LinearFilter&);

        virtual ~LinearFilter();

        LinearFilter& operator=(const LinearFilter&);

        // UDT functionality

        virtual LinearFilter* Clone() const;

        ILwd::LdasElement* ConvertToIlwd(const CallChain& Chain, udt::target_type Target =
            udt::TARGET_GENERIC) const;

        // valarray interface

        template<typename type> void SetCoefficientsA(const std::valarray<type>&,
            const signed int& = 0);
        template<typename type> void SetCoefficientsB(const std::valarray<type>&,
            const signed int& = 0);

        template<typename in, typename out> void apply(std::valarray<out>&,
            const std::valarray<in>&);

        // UDT interface

        void SetCoefficientsA(const udt&);
        void SetCoefficientsA(const udt&, const udt&);
        void SetCoefficientsB(const udt&);
        void SetCoefficientsB(const udt&, const udt&);

        void apply(udt*&, const udt&);


        class AbstractCoefficients;
        class AbstractState;

    private:

        bool single() const; // are any coefficents single precision

        template<typename type> void imply(udt*&, const udt&);

        template<typename in, typename out, typename internal> void implementation(
            std::valarray<out>&, const std::valarray<in>&);

        AbstractCoefficients* a_coefficients;
        AbstractCoefficients* b_coefficients;
        AbstractState* state;

    };
}

#endif // LINEARFILTER_HH
