/* -*- mode: c++; c-basic-offset: 2; -*- */

%skeleton "lalr1.cc"
%require "2.1a"
%defines
%output="ActionParser.cc"
%name-prefix="datacondAPI"
%define "parser_class_name" "ActionParser"

%{
#include "datacondAPI/config.h"

#include <string>
#include <sstream>

   namespace yy
   {
   }

   namespace datacondAPI
   {
     using namespace yy;
     class ActionDriver;
   } // namespace - DatacondAPI
%}

// The parsing context.
%parse-param	{ ActionDriver& Driver }
%lex-param	{ ActionDriver& Driver }
%locations
//%initial-action
//{
  // Initialize the initial location.
//  @$.begin.filename = @$.end.filename = &driver.file;
//};
%debug
%error-verbose

// Symbols.
%union
{
  std::string*	sval;
};
%{
#include "ActionDriver.hh"
#include "AliasDriver.hh"


#define YY_DECL \
  static datacondAPI::ActionParser::token_type			\
  yylex( datacondAPI::ActionParser::semantic_type* YYLval,	\
	 datacondAPI::ActionParser::location_type* YYLloc,	\
	 datacondAPI::ActionDriver& Driver )

// Declare it for the parser's sake.
YY_DECL;
%}

%token        TKN_END 0
%token <sval> TKN_CONSTANT_NUMERIC
%destructor { delete $$; } TKN_CONSTANT_NUMERIC
%token <sval> TKN_IDENTIFIER
%destructor { delete $$; } TKN_IDENTIFIER
%token <sval> TKN_OUTPUT
%destructor { delete $$; } TKN_OUTPUT
%token <sval> TKN_STRING
%destructor { delete $$; } TKN_STRING

%start translation_unit
%%

translation_unit:
	| statement_seq
	;

statement_seq: statement
	| statement statement_seq
	;

statement: assignment function
	| function
	| TKN_OUTPUT '(' variable ','  variable ','  variable ','  variable ',' var_string ')' ';'
	{
	  Driver.m_func_name = *$1;
	  Driver.add_action( );
	  Driver.reset( );
	}
	;

function: TKN_IDENTIFIER '(' variables ')' ';'
        {
	  Driver.m_func_name = *$1;
	  Driver.add_action( );
	  Driver.reset( );
	}
	;

assignment: TKN_IDENTIFIER '='
	{
	  Driver.m_output_name = *$1;
	}
	;

variables: variable
	| variable ',' variables
	;

variable: /* empty */
	| TKN_CONSTANT_NUMERIC
        {
	  std::istringstream stream( *$1 );

	  INT_8S	len;
	  int i;

	  stream.clear( ); // Clear error state
	  stream.seekg( 0 );
	  stream >> i;
	  len = stream.tellg( );
	  if ( stream.eof( ) )
	  {
	    Driver.add_parameter( ActionDriver::var_type::INTEGER, *$1 );
	  }
	  else
	  {
	    stream.str( *$1 );
	    double dbl;
	    stream.clear( ); // Clear error state
	    stream.seekg( 0 );
	    stream >> dbl;
	    len = stream.tellg( );
	    if ( stream.eof( ) )
	    {
	      Driver.add_parameter( ActionDriver::var_type::REAL, *$1 );
	    }
	    else
	    {
	      Driver.add_parameter( ActionDriver::var_type::BAD, *$1 );
	    }
	  }
	}
	| TKN_IDENTIFIER
        {
	  if ( ( *($1->begin( )) == '_' ) && ( $1->length( ) != 1 ) )
	  {
	    Driver.add_parameter( ActionDriver::var_type::BAD, *$1 );
	  }
	  else
	  {
	    AliasDriver::alias_type::const_iterator
	      alias( Driver.m_aliases( ).find( *$1 ) );
	    if ( alias != Driver.m_aliases( ).end( ) )
	    {
	      Driver.add_parameter( ActionDriver::var_type::UNIVERSAL, alias->second.c_str( ) );
	    }
	    else
	    {
	      Driver.add_parameter( ActionDriver::var_type::UNIVERSAL, *$1 );
	    }
	  }
	}
	;
var_string:
	TKN_STRING
        {
	  Driver.add_parameter( ActionDriver::var_type::UNIVERSAL, *$1 );
	}
	;
%%

static datacondAPI::ActionParser::token_type
yylex( datacondAPI::ActionParser::semantic_type* YYLval,
       datacondAPI::ActionParser::location_type* YYLloc,
       datacondAPI::ActionDriver& Driver )
{
  return Driver.Lex( YYLval, YYLloc );
}

namespace datacondAPI
{
  void ActionParser::
  error( const location_type& YYLoc, const std::string& Message )
  {
    std::cerr << "CDBUG: ActionParser::error: Message: " << Message
	      << std::endl;
  }
} // namespace - datacondAPI
