#include "datacondAPI/config.h"

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "ran3state.hh"
#include "random.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

namespace datacondAPI{

  ran3state::ran3state():ma(56)
  {
    idum = -248673140;
    iff=0;
  }

  ran3state::ran3state(int seed):ma(56)
  {
    idum = seed;
    iff = 0;
  }

  ran3state::ran3state(const ran3state& g)
    : State( g ),
      ma(g.ma)
  {
    idum = g.idum;
    iff = g.iff;
    inext = g.inext;
    inextp = g.inextp;
  }

  //restest state with new seed s
  void ran3state::seed(int s)
  {
    idum = s;
    iff = 0;
    //removes previous values in iv so that state is reset
    ma.resize(56);
  }

  float ran3state::getran()
  {
    //static int inext,inextp;
    //static long ma[56];
    //static int iff=0;
    long mj,mk;
    int i,ii,k;
    
    if(idum<0||iff==0)
      {
	iff=1;
	mj=MSEED-(idum<0? -idum : idum);
	mj%=MBIG;
	ma[55]=mj;
	mk=1;
	for(i=1;i<=54;i++)
	  {
	    ii=(21*i)%55;
	    ma[ii]=mk;
	    mk=mj-mk;
	    if(mk<MZ)
	      mk+=MBIG;
	    mj=ma[ii];
	  }
	for(k=1;k<=4;k++)
	  for(i=1;i<=55;i++)
	    {
	      ma[i] -= ma[1+(i+30)%55];
	      if(ma[i]<MZ) 
		ma[i] += MBIG;
	    }
	inext=0;
	inextp=31;
	idum=1;
      }
    if(++inext == 56)
      inext=1;
    if(++inextp== 56)
      inextp=1;
    mj=ma[inext]-ma[inextp];
    if(mj < MZ)
      mj+=MBIG;
    ma[inext]=mj;
    return mj*FAC;
  }

  void ran3state::getran(std::valarray<float>& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = getran();
  }

  ran3state* ran3state::Clone() const
  {
    return new ran3state(*this);
  }

  ILwd::LdasElement* ran3state::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    General::toss<General::unimplemented_error>("ConvertToILWD",
						__FILE__,__LINE__,
						"unimplemented");
    return container;
  }

}
