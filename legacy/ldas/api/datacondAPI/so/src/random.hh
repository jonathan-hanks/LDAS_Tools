#ifndef RANDOM_HH
#define RANDOM_HH

#include <valarray>
#include "ran0state.hh"
#include "ran1state.hh"
#include "ran2state.hh"
#include "ran3state.hh"
#include "ran4state.hh"
#include "gasdevstate.hh"

namespace datacondAPI{

const long IA = 48271;
const long IM = 2147483647;
const double AM = 1.0/IM;
const long IQ = 44488;
const long IR = 3399;
const long MASK = 123459876;
const long NTAB = 32;
const double NDIV = (1.0+(IM-1.0)/double(NTAB));
const double EPS = 1.2e-7;
const double RNMX = (1.0-EPS); 
const long IM1 = 2147483563;
const long IM2 = 2147483399;
const double AM1 = (1.0/IM1);
const long IMM1 = (IM1 - 1);
const long IA1 = 40014;
const long IA2 = 40692;
const long IQ1 = 53668;
const long IQ2 = 52774;
const long IR1 = 12211;
const long IR2 = 3791;
const double NDIV1 = (1.0+double(IMM1)/NTAB);
const long MBIG = 1000000000;
const long MSEED = 161803398;
const long MZ = 0;
const double FAC = (1.0/MBIG);
const unsigned long NITER = 4;

  //: "Basic" Randomizing Function
  float ran0 (ran0state& s);
  //!param: ran0state& s - state object for random function
  //!return: float - a psuedo-random number between 0 and 1

  //: "Basic" Randomizing Function
  void ran0 (ran0state& s, std::valarray<float>& v, int size);
  //!param: ran0state& s - state objext for random function
  //!param: std::valarray<float>& v - array to hold random numbers
  //!param: int size - size of array upon return and the 
  // + quantity of random numbers in the array

  //: "Intermediate" Randomizing Function
  float ran1 (ran1state& s);
  //!param: ran1state& s - state object for random function
  //!return: float - a psuedo-random number between 0 and 1

  //: "Intermediate" Randomizing Function
  void ran1 (ran1state& s, std::valarray<float>& v, int size);
  //!param: ran1state& s - state objext for random function
  //!param: std::valarray<float>& v - array to hold random numbers
  //!param: int size - size of array upon return and the 
  // + quantity of random numbers in the array

  //: "Advanced" Randomizing Function
  float ran2 (ran2state& s);
  //!param: ran2state& s - state object for random function
  //!return: float - a psuedo-random number between 0 and 1

  //: "Advanced" Randomizing Function
  void ran2 (ran2state& s, std::valarray<float>& v, int size);
  //!param: ran2state& s - state objext for random function
  //!param: std::valarray<float>& v - array to hold random numbers
  //!param: int size - size of array upon return and the 
  // + quantity of random numbers in the array

  //: Subtractive Method Randomizing Function
  float ran3 (ran3state& s);
  //!param: ran3state& s - state object for random function
  //!return: float - a psuedo-random number between 0 and 1

  //: Subtractive Method Randomizing Function
  void ran3 (ran3state& s, std::valarray<float>& v, int size);
  //!param: ran3state& s - state objext for random function
  //!param: std::valarray<float>& v - array to hold random numbers
  //!param: int size - size of array upon return and the 
  // + quantity of random numbers in the array

  //: Randomizing Function using Hashing
  float ran4 (ran4state& s);
  //!param: ran4state& s - state object for random function
  //!return: float - a psuedo-random number between 0 and 1

  //: Randomizing Function using Hashing
  void ran4 (ran4state& s, std::valarray<float>& v, int size);
  //!param: ran4state& s - state objext for random function
  //!param: std::valarray<float>& v - array to hold random numbers
  //!param: int size - size of array upon return and the 
  // + quantity of random numbers in the array

  //: Function to Generate Random Normal Sequences
  float gasdev(gasdevstate& s);
  //!param: gasdevstate& s - state object for random function
  //!return: - returns a normally distributed deviate with zero mean
  // + and unit variance

  //: Function to Generate Random Normal Sequences
  template<typename type> void gasdev(gasdevstate& s, std::valarray<type>& v, int size);
  //!param: gasdevstate& s - state objext for random function
  //!param: std::valarray<type>& v - array to hold random numbers
  //!param: int size - size of array upon return and the 
  // + quantity of random numbers in the array

}//datacondAPI

#endif //RANDOM_HH







