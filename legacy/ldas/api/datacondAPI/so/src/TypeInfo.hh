#ifndef TYPE_INFO_HH
#define	TYPE_INFO_HH

#include <typeinfo>

#undef	USE_UNORDERED_MAP

#ifdef USE_UNORDERED_MAP
#include "general/unordered_map.hh"
#else	/* USE_UNORDERED_MAP */
#include <map>
#endif	/* USE_UNORDERED_MAP */

#include <string>
#include <iostream>

namespace datacondAPI
{
  //: Type information table
  //
  // This class is to be used to generate class names that are more
  // meaningful than what is supplied by typeid().
  //
  class TypeInfoTable_type
  {
  public:
    enum Dump_type {
      KEY_VALUE,
      KEY,
      VALUE
    };
    //: Constructor
    TypeInfoTable_type(void);

    //: Register a new key
    bool AddType( const std::type_info& Key, std::string Value );

    void DumpTable( std::ostream& Stream, Dump_type Mode );
      
    //: Retrieve readable name for a class
    std::string GetName(const std::type_info& Key) const;

  private:
#ifdef USE_UNORDERED_MAP
    //: Equality test
    struct TI_eq {
      bool operator () (const std::type_info* p, const std::type_info* q) const
      {
	return *p == *q;
      }
    };

    //: Hash function
    struct TI_hash {
      int operator() (const std::type_info* p) const;
    };
#else	/* USE_UNORDERED_MAP */
    //: Placement function
    struct TI_cmp {
      bool operator () (const std::type_info* p, const std::type_info* q) const
      {
	// Don't accept duplicates
	if (*p == *q) return false;
	return q->before(*p);
      }
    };
#endif	/* USE_UNORDERED_MAP */

    //: Information that is stored about each class
    class TypeInfo_type
    {
    public:
      //: Constructor
      TypeInfo_type(const std::string& Name);
      //: Name of class
      const std::string& GetName(void) const;
      
    private:
      //: Storage for the name of the class
      std::string	m_name;
    };

#ifdef USE_UNORDERED_MAP
    //: Used if the table base is a hash map
    typedef General::unordered_map<const std::type_info*, TypeInfo_type,
      struct TI_hash, struct TI_eq> Map_type;
#else	/* USE_UNORDERED_MAP */
    //: Used if the table base is a map
    typedef std::map<const std::type_info*, TypeInfo_type, struct TI_cmp> Map_type;
#endif	/* USE_UNORDERED_MAP */

    //: Table to hold type info
    Map_type	m_table;
  };

  TypeInfoTable_type& TheTypeInfoTable();

  // Provide backwards compatability
#define TypeInfoTable TheTypeInfoTable()

}

#endif	/* TYPE_INFO_HH */
