#include "datacondAPI/config.h"

#include <algorithm>
#include <climits> //:todo: <limits>

#include <general/unimplemented_error.hh>

#include "fft.hh"
#include "ifft.hh"
#include "LineRemover.hh"
#include "OEModel.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "Mixer.hh"
#include "Resample.hh"
#include "ShiftState.hh"

namespace datacondAPI
{

    template<typename T> struct complex_traits
    {
        typedef std::complex<T> complex_type;
        typedef T real_type;
    };

    template<typename T> struct complex_traits<std::complex<T> >
    {
        typedef typename complex_traits<T>::complex_type complex_type;
        typedef typename complex_traits<T>::real_type real_type;
    };

    class LineRemover::Abstraction
    {

    public:

        // override defaults

        virtual ~Abstraction();

        // interface

        virtual Abstraction* clone() const = 0;

        virtual void apply(udt*& w, const udt& u) = 0;

        virtual void getFilter(udt*& b) const = 0;

    protected:

        Abstraction();
        Abstraction(const Abstraction&);

        Abstraction& operator=(const Abstraction&);

    private:

    };

    class BandSelector
    {

    public:

        BandSelector(const double& frequency, const std::size_t factor)
            : m_downmixer(MixerState(0.0, -frequency))
            , m_downsampler(1, factor)
            , m_downshifter(0)
            , m_upsampler(factor, 1)
            , m_upshifter(0)
            , m_upmixer(MixerState(0.0, +frequency))
        {
        }

        BandSelector(const BandSelector& bs)
            : m_downmixer(bs.m_downmixer)
            , m_downsampler(bs.m_downsampler)
            , m_downshifter(bs.m_downshifter ? bs.m_downshifter->Clone() : 0)
            , m_upsampler(bs.m_upsampler)
            , m_upshifter(bs.m_upshifter ? bs.m_upshifter->Clone() : 0)
            , m_upmixer(bs.m_upmixer)
        {
        }

        ~BandSelector()
        {
            delete m_downshifter;
            delete m_upshifter;
        }

        BandSelector& operator=(const BandSelector& bs)
        { 
            if (&bs != this)
            {
                m_downmixer = bs.m_downmixer;
                m_downsampler = bs.m_downsampler;
                delete m_downshifter;
                m_downshifter = bs.m_downshifter ? bs.m_downshifter->Clone() : 0;
                m_upsampler = bs.m_upsampler;
                delete m_upshifter;
                m_upshifter = bs.m_upshifter ? bs.m_upshifter->Clone() : 0;
                m_upmixer = bs.m_upmixer;
            }
            return *this;
        }
   
        BandSelector* clone() const
        {
            return new BandSelector(*this);
        }

        template<typename out_, typename in_>
            void apply(std::valarray<out_>& out, const std::valarray<in_>& in)
        {
            std::valarray<out_> downmixed;
            m_downmixer.apply(downmixed, in);
            Sequence<out_> downsampled;
            m_downsampler.apply(downsampled, downmixed);
            if (!m_downshifter) m_downshifter = new ShiftState<out_>(m_downsampler.getDelay(), 1);
            dynamic_cast<ShiftState<out_>&>(*m_downshifter).apply(downsampled);
            out.resize(downsampled.size());
            out = downsampled;
        }

        template<typename out_, typename in_>
            void ylppa(std::valarray<out_>& out, const std::valarray<in_>& in)
        {
            Sequence<in_> upsampled;
            m_upsampler.apply(upsampled, in);
            if (!m_upshifter) m_upshifter = new ShiftState<in_>(m_upsampler.getDelay(), 1);
            dynamic_cast<ShiftState<in_>&>(*m_upshifter).apply(upsampled);
            m_upmixer.apply(out, upsampled);
        }

    private:

        BandSelector();

        Mixer m_downmixer;
        Resample m_downsampler;
        State* m_downshifter;
        Resample m_upsampler;
        State* m_upshifter;
        Mixer m_upmixer;

    };

    template<typename type>
        class LineRemover::Implementation : public Abstraction
    {

    public:

        // override defaults

        Implementation();
        Implementation(const Implementation&);

        virtual ~Implementation();

        Implementation& operator=(const Implementation&);

        // interface

        virtual Implementation* clone() const;

        void refine(const std::valarray<type>& y,
            const std::valarray<type>& u,
            const double& frequency,
            const std::size_t& factor,
            const std::size_t& order);

        virtual void apply(udt*& w,
            const udt& u);

        void apply(std::valarray<type>& w,
            const std::valarray<type>& u);

        virtual void getFilter(udt*& b) const;

    private:

        OEModel* m_model;

        BandSelector* m_refine_y;
        BandSelector* m_refine_u;

        BandSelector* m_apply_y;
        BandSelector* m_apply_u;

    };

    LineRemover::LineRemover(const double& frequency, const std::size_t& factor, const std::size_t& order)
        : m_frequency(frequency)
        , m_factor(factor)
        , m_order(order)
        , m_data(0)
    {
    }

    LineRemover::LineRemover(const udt& frequency, const udt& factor, const udt& order)
    try : m_frequency(dynamic_cast<const Scalar<double>&>(frequency).GetValue())
        , m_factor(dynamic_cast<const Scalar<int>&>(factor).GetValue())
        , m_order(dynamic_cast<const Scalar<int>&>(order).GetValue())
        , m_data(0)
    {
    }
    catch (const std::exception& x)
    {
        throw std::logic_error(std::string("LineRemover::LineRemover: intercepted exception \"") + x.what() + std::string("\""));
    }

    LineRemover::LineRemover(const LineRemover& lr)
      : State( lr ),
	m_frequency(lr.m_frequency),
	m_factor(lr.m_factor),
	m_order(lr.m_order),
	m_data(lr.m_data ? lr.m_data->clone() : 0)
    {
    }

    LineRemover::~LineRemover()
    {
        delete m_data;
    }

    LineRemover& LineRemover::operator=(const LineRemover& lr)
    {
        if (this != &lr)
        {
            m_frequency = lr.m_frequency;
            m_factor = lr.m_factor;
            m_order = lr.m_order;
            delete m_data;
            m_data = lr.m_data ? lr.m_data->clone() : 0;
        }
        return *this;
    }

    LineRemover* LineRemover::Clone() const
    {
        return new LineRemover(*this);
    }

    ILwd::LdasElement* LineRemover::ConvertToIlwd(const CallChain& Chain, udt::target_type Target) const
    {
        throw General::unimplemented_error("LineRemover::ConvertToIlwd is unimplemented");
    }

    void LineRemover::getFilter(udt*& b) const
    try
    {
        if (m_data)
        {
            m_data->getFilter(b);
        }
        else
        {
            throw std::logic_error("LineRemover::getFilter: no filter estimated yet\n");
        }
    }
    catch (const std::exception& x)
    {
        throw std::logic_error(std::string("LineRemover::getFilter: intercepted exception \"") + x.what() + std::string("\""));
    }

    template<typename type>
        void LineRemover::refine(const std::valarray<type>& y, const std::valarray<type>& u)
    try
    {
        if (!m_data)
        {
            m_data = new Implementation<type>;
        }
        dynamic_cast<Implementation<type>&>(*m_data).refine(y, u, m_frequency, m_factor, m_order);
    }
    catch (const std::exception& x)
    {
        throw std::logic_error(std::string("LineRemover::refine: intercepted exception \"") + x.what() + std::string("\""));
    }


    void LineRemover::refine(const udt& y, const udt& u)
    try
    {
        if (const std::valarray<float>* p = dynamic_cast<const std::valarray<float>*>(&y))
        {
            if (const std::valarray<float>* q = dynamic_cast<const std::valarray<float>*>(&u))
            {
                refine(*p, *q);
            }
            else
            {
                throw std::invalid_argument("LineRemover::refine: input types mismatch");
            }
        }
        else if (const std::valarray<double>* p = dynamic_cast<const std::valarray<double>*>(&y))
        {
            if (const std::valarray<double>* q = dynamic_cast<const std::valarray<double>*>(&u))
            {
                refine(*p, *q);
            }
            else
            {
                throw std::invalid_argument("LineRemover::refine: input types mismatch");
            }
        }
        else if (const std::valarray<std::complex<float> >* p = dynamic_cast<const std::valarray<std::complex<float> >*>(&y))
        {
            if (const std::valarray<std::complex<float> >* q = dynamic_cast<const std::valarray<std::complex<float> >*>(&u))
            {
            refine(*p, *q);
            }
            else
            {
            throw std::invalid_argument("LineRemover::refine: input types mismatch");
            }
        }
        else if (const std::valarray<std::complex<double> >* p = dynamic_cast<const std::valarray<std::complex<double> >*>(&y))
        {
            if (const std::valarray<std::complex<double> >* q = dynamic_cast<const std::valarray<std::complex<double> >*>(&u))
            {
            refine(*p, *q);
            }
            else
            {
            throw std::invalid_argument("LineRemover::refine: input types mismatch");
            }
        }
        else
        {
            throw std::invalid_argument("LineRemover::refine: unsupported types");
        }
    }
    catch (const std::exception& x)
    {
        throw std::logic_error(std::string("LineRemover::refine: intercepted exception \"") + x.what() + std::string("\""));
    }


    template<typename type>
        void LineRemover::apply(std::valarray<type>& w, const std::valarray<type>& u)
    try
    {
        if (Implementation<type>* p = dynamic_cast<Implementation<type>*>(m_data))
        {
            p->apply(w, u);
        }
        else
        {
            throw std::logic_error("LineRemover::apply: no model yet or type mismatch");
        }
    }
    catch (const std::exception& x)
    {
        throw std::logic_error(std::string("LineRemover::apply: intercepted exception \"") + x.what() + std::string("\""));
    }


    void LineRemover::apply(udt*& w, const udt& u)
    try
    {
        if (m_data)
        {
            m_data->apply(w, u);
        }
        else
        {
            throw std::logic_error("LineRemover::apply: must estimate models before applying");
        }
    }
    catch (const std::exception& x)
    {
        throw std::logic_error(std::string("LineRemover::apply: intercepted exception \"") + x.what() + std::string("\""));
    }


    LineRemover::Abstraction::Abstraction()
    {
    }

    LineRemover::Abstraction::Abstraction(const Abstraction& a)
    {
    }

    LineRemover::Abstraction::~Abstraction()
    {
    }

    LineRemover::Abstraction& LineRemover::Abstraction::operator=(const Abstraction& a)
    {
        if (this != & a)
        {
        }
        return *this;
    }

    template<typename type>
        LineRemover::Implementation<type>::Implementation()
        : m_model(0)
        , m_refine_y(0)
        , m_refine_u(0)
        , m_apply_y(0)
        , m_apply_u(0)
    {
    }

    template<typename type>
    LineRemover::Implementation<type>::Implementation(const Implementation<type>& lri)
      : Abstraction( lri ),
	m_model(lri.m_model ? new OEModel(*lri.m_model) : 0),
	m_refine_y(lri.m_refine_y ? lri.m_refine_y->clone() : 0),
	m_refine_u(lri.m_refine_u ? lri.m_refine_u->clone() : 0),
	m_apply_y(lri.m_apply_y ? lri.m_apply_y->clone() : 0),
	m_apply_u(lri.m_apply_u ? lri.m_apply_u->clone() : 0)
    {
    }

    template<typename type>
        LineRemover::Implementation<type>::~Implementation()
    {
        delete m_model;
        delete m_refine_y;
        delete m_refine_u;
        delete m_apply_y;
        delete m_apply_u;
    }

    template<typename type>
        LineRemover::Implementation<type>& LineRemover::Implementation<type>::operator=(const Implementation<type>& lri)
    {
        if (this != &lri)
        {
            m_model = lri.m_model ? new OEModel(*lri.m_model) : 0;
            m_refine_y = lri.m_refine_y ? lri.m_refine_y->clone() : 0;
            m_refine_u = lri.m_refine_u ? lri.m_refine_u->clone() : 0;
            m_apply_y = lri.m_apply_y ? lri.m_apply_y->clone() : 0;
            m_apply_u = lri.m_apply_u ? lri.m_apply_u->clone() : 0;
        }
        return *this;
    }

    template<typename type>
    LineRemover::Implementation<type>* LineRemover::Implementation<type>::clone() const
    {
        return new Implementation<type>(*this);
    }

    template<typename type>
        void LineRemover::Implementation<type>::getFilter(udt*& b) const
    {
        if (m_model)
        {
            m_model->getFilterB(b);
        }
        else
        {
            throw std::invalid_argument("LineRemover::Implementation<type>::getFilter: no filter yet");
        }
    }

    template<typename type> void
    LineRemover::Implementation<type>::refine(const std::valarray<type>& y,
                          const std::valarray<type>& u,
                          const double& frequency,
                          const std::size_t& factor,
                          const std::size_t& order)
    {

        // check input sanity

        if (y.size() != u.size())
        {
            throw std::invalid_argument("LineRemover::Implementation<type>::refine: model input and output must be the same size");
        }

        // construct buffers (will be sized on first call)

        std::valarray<typename complex_traits<type>::complex_type> banded_y;
        std::valarray<typename complex_traits<type>::complex_type> banded_u;

        if (!m_refine_y) // first call
        {
            m_refine_y = new BandSelector(frequency, factor);
            m_refine_u = new BandSelector(frequency, factor);
            m_apply_y = new BandSelector(frequency, factor);
            m_apply_u = new BandSelector(frequency, factor);
        }

        m_refine_y->apply(banded_y, y);
        m_refine_u->apply(banded_u, u);

        if (m_model)
        {
            m_model->refine(banded_y, banded_u);
        }
        else
        {
            m_model = new OEModel(banded_y, banded_u, order, 0);
        }
    }

    template<typename T, typename U>
        struct aggregator
    {
        void operator()(std::valarray<T>&, const std::valarray<U>&) const;
    };

    template<> void aggregator<float, std::complex<float> >::operator()(std::valarray<float>& out, const std::valarray<std::complex<float> >& in) const
    {
        for (std::size_t i = 0; i < out.size(); ++i)
        {
            out[i] += (in[i].real() * 2);
        }
    }

    template<> void aggregator<double, std::complex<double> >::operator()(std::valarray<double>& out, const std::valarray<std::complex<double> >& in) const
    {
        for (std::size_t i = 0; i < out.size(); ++i)
        {
            out[i] += (in[i].real() * 2);
        }
    }

    template<> void aggregator<std::complex<float>, std::complex<float> >::operator()(std::valarray<std::complex<float> >& out, const std::valarray<std::complex<float> >& in) const
    {
        out += in;
    }

    template<> void aggregator<std::complex<double>, std::complex<double> >::operator()(std::valarray<std::complex<double> >& out, const std::valarray<std::complex<double> >& in) const
    {
        out += in;
    }   


    template<typename type> void   
        LineRemover::Implementation<type>::apply(
        std::valarray<type>& w,
        const std::valarray<type>& u)
    {

        if (!m_apply_u || !m_model || !m_apply_y)
        {
            throw std::logic_error("LineRemover::Implementation::apply Attempted to apply before estimating(refine)");
        }

        std::valarray<typename complex_traits<type>::complex_type> banded_u;
        std::valarray<typename complex_traits<type>::complex_type> banded_y;
        std::valarray<typename complex_traits<type>::complex_type> y;

        aggregator<type, typename complex_traits<type>::complex_type> aggregate;

        m_apply_u->apply(banded_u, u);
        m_model->apply(banded_y, banded_u);
        m_apply_y->ylppa(y, banded_y);
        if (w.size() != y.size()) w.resize(y.size(), type());
        aggregate(w, y);
    }

    template<typename type>
        void LineRemover::Implementation<type>::apply(udt*& w, const udt& u)
    {
        if (const std::valarray<type>* p = dynamic_cast<const std::valarray<type>*>(&u))
        {
            if (w == 0)
            {
                w = new Sequence<type>();
            }
            if (std::valarray<type>* q = dynamic_cast<std::valarray<type>*>(w))
            {
                apply(*q, *p);
            }
            else
            {
                throw std::invalid_argument("LineRemover::Implemnentation<type>::apply: input type mismatch");
            }
        }
        else
        {
            throw std::invalid_argument("LineRemover::Implemnentation<type>::apply: input type mismatch");
        }
    }

#define INSTANTIATE(type) \
    \
    template void LineRemover::refine(const std::valarray< type >&, const std::valarray< type >&);\
    template void LineRemover::apply(std::valarray< type >&, const std::valarray< type >&);\
    template class LineRemover::Implementation< type >;

    INSTANTIATE(float)
    INSTANTIATE(double)
    INSTANTIATE(std::complex<float> )
    INSTANTIATE(std::complex<double> )

#undef INSTANTIATE

}

#if 0
UDT_CLASS_INSTANTIATION(LineRemover, UDT_EMPTY_ARG )
#else
UDT_CLASS_INSTANTIATION_CAST(LineRemover)
#endif
