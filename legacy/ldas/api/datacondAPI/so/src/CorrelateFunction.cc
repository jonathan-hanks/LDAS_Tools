// $Id: CorrelateFunction.cc,v 1.4 2005/12/01 22:54:58 emaros Exp $
//
// "xcorr" action
//
// xcorr has ?? input arguments and one return value. The format is
//
//     xcorr_out = xcorr(xcorr_a, xcorr_b, ... , [ detrendMethod ])
//
// xcorr_a, xcorr_b are Sequences or subtype
//

#include "datacondAPI/config.h"

#include "CorrelateFunction.hh"
#include "Correlate.hh"
#include "ScalarUDT.hh"

static CorrelateFunction _CorrelateFunction;

CorrelateFunction::CorrelateFunction()
    : Function( CorrelateFunction::GetName() )
{
}

const std::string& 
CorrelateFunction::GetName(void) const
{
    static std::string name("xcorr");
    return name;
}

void 
CorrelateFunction::Eval(CallChain* Chain,
                        const CallChain::Params& Params,
                        const std::string& Ret) const
{
/*
    using namespace datacondAPI;

    Correlate corr_obj;
    
    const size_t num_params = Params.size();

    switch(num_params)
    {
    case 3:
    case 2:
    case 1:
	// Nothing to do here, just use all defaults
        break;

    default:
        throw CallChain::BadArgumentCount("xcorr", 1, Params.size());
    }

    const udt& xcorr_a = *(Chain->GetSymbol(Params[0]));
    const udt& xcorr_b = *(Chain->GetSymbol(Params[1]));
    CallChain::Symbol* xcorr_out = 0;

    corr_obj.apply(xcorr_out, xcorr_a, xcorr_b);
    
    Chain->ResetOrAddSymbol( Ret, xcorr_out );
*/
}
