#define	BASIC_MATH_FUNCTION_CC

#include "datacondAPI/config.h"

#include <ios>

#include <memory>
#include <valarray>
#include <complex>   
#include <sstream>

#include TARGET_HH

#include "general/Memory.hh"

#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"
#include "TypeInfo.hh"

using namespace std;   
using namespace datacondAPI;


#define EVAL(LM_TYPE) \
  if (udt::IsA< LM_TYPE >(Unknown1)) \
  { \
    return eval2( udt::Cast< LM_TYPE >(Unknown1), Unknown2, Reverse ); \
  }

#define EVAL2(LM_TYPE) \
  if (udt::IsA< LM_TYPE >(Unknown)) \
  { \
    op( *ret, udt::Cast< LM_TYPE >(Unknown), Reverse ); \
  }

#define EVAL2C(LM_TYPE,LM_CAST) \
  if (udt::IsA< LM_TYPE >(Unknown)) \
  { \
    op( *ret, (LM_CAST)udt::Cast< LM_TYPE >(Unknown), Reverse ); \
  }
//=============================================================================
// Static Helper functions
//=============================================================================

static udt* eval( const udt& Unknown1,
		  const udt& Unknwon2,
		  bool Reverse );

static udt* eval2( const Sequence< complex< float> >& Known,
		   const udt& Unknown,
		   bool Reverse );
static udt* eval2( const Sequence< complex< double> >& Known,
		   const udt& Unknown,
		   bool Reverse );
static udt* eval2( const Sequence<float>& Known,
		   const udt& Unknown,
		   bool Reverse );
static udt* eval2( const Sequence<double>& Known,
		   const udt& Unknown,
		   bool Reverse );

static udt* eval2( const Scalar< complex< double > >& Known,
		   const udt& Unknown,
		   bool Reverse );
static udt* eval2( const Scalar< complex< float > >& Known,
		   const udt& Unknown,
		   bool Reverse );

static udt* eval2( const Scalar< double >& Known,
		   const udt& Unknown,
		   bool Reverse );
static udt* eval2( const Scalar< float >& Known,
		   const udt& Unknown,
		   bool Reverse );
static udt* eval2( const Scalar< int >& Known,
		   const udt& Unknown,
		   bool Reverse );

//=============================================================================
// Perform arithmatic on scalars
//=============================================================================

template< class Result_, class Parm1_ >
static inline
void op( Scalar< Result_ >& Result, const Scalar< Parm1_ >& Parm1,
	 bool Reverse )
{
#if !defined( ASSOCIATIVE )
  Result.SetValue( ( Reverse )
		   ? ( Parm1.GetValue() OP Result.GetValue() )
		   : ( Result.GetValue() OP Parm1.GetValue() ) );
#else
  Result.SetValue( Result.GetValue() OP Parm1.GetValue() );
#endif
}

template< class Result_, class Parm1_ >
static inline
void op( Scalar< complex< Result_ > >& Result, const Scalar< Parm1_ >& Parm1,
	 bool Reverse )
{
#if !defined( ASSOCIATIVE )
  const complex< Result_ > tmp_parm1( Parm1.GetValue() );
   
  Result.SetValue( ( Reverse )
		   ? ( tmp_parm1 OP Result.GetValue() )
		   : ( Result.GetValue() OP tmp_parm1 ) );
#else
  const complex< Result_ > tmp_parm1( Parm1.GetValue() );
   
  Result.SetValue( Result.GetValue() OP tmp_parm1 );
#endif
}
   
   
//-----------------------------------------------------------------------------
// Handle demotion rules
//-----------------------------------------------------------------------------

template<>
inline
void op< complex< float >, complex< double > >
( Scalar< complex< float > >& Result,
  const Scalar< complex< double > >& Unknown,
  bool Reverse )
{
  // Handle demotion case
  Result.SetValue( ( Reverse )
		   ? ( complex< float >( Unknown.GetValue().real(),
					 Unknown.GetValue().imag() ) OP
		      Result.GetValue( ) )
		   : ( Result.GetValue() OP
		       complex< float >( Unknown.GetValue().real(),
					 Unknown.GetValue().imag() ) ));
}

//=============================================================================
// Perform arithmatic on sequences with Scalars
//=============================================================================

template< class Result_, class Parm1_ >
static inline
void op( Sequence< Result_ >& Result, const Parm1_& Parm1, bool Reverse )
{
#if defined( ASSOCIATIVE )
  Result_ result_parm1( Parm1 );
  Result OPEQ result_parm1;
#else
  if ( Reverse )
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] = Parm1 OP Result[i];
    }
  }
  else
  {
    Result_ result_parm1( Parm1 );
    Result OPEQ result_parm1;
  }
#endif
}

//=============================================================================
// Perform arithmatic on sequences with sequences
//=============================================================================

template< class Result_, class Parm1_ >
inline
void metadata_check( Sequence< Result_ >& Result,
		     const Sequence<Parm1_>& Parm1 )
{
  // Validate the metadata for time series as being correct.
  TimeSeriesMetaData* rptr = dynamic_cast< TimeSeriesMetaData* >( &Result );
  const TimeSeriesMetaData* pptr =
    dynamic_cast< const TimeSeriesMetaData* >( &Parm1 );

  if ( rptr && pptr )
  {
    if ( ! rptr->IsCompatable( *pptr ) )
    {
      throw CallChain::LogicError( "Incompatable metadata between TimeSeries" );
    }
  }
}

template< class Result_, class Parm1_ >
static inline
void seq_check( Sequence< Result_ >& Result, const Sequence<Parm1_>& Parm1 )
{
  if ( Result.size() != Parm1.size() )
  {
    std::ostringstream	msg;
    msg << "Sequences differ in size"
	<< " (" << Result.name( ) << ":" << Result.size( )
	<< " vs. " << Parm1.name( ) <<":" << Parm1.size( );
    throw CallChain::LogicError( msg.str( ) );
  }
}

template< class Result_, class Parm1_ >
static inline
void op( Sequence< Result_ >& Result, const Sequence<Parm1_>& Parm1,
	 bool Reverse )
{
  seq_check( Result, Parm1 );

  metadata_check( Result, Parm1 );

  if ( Reverse )
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] = (Result_)( Parm1[i] ) OP Result[i];
    }
  }
  else
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] OPEQ (Result_)( Parm1[i] );
    }
  }
}

//-----------------------------------------------------------------------------
// Handle demotion rules
//-----------------------------------------------------------------------------

template<>
inline
void op< float, double >
( Sequence< float >& Result,
  const Sequence< double >& Parm1,
  bool Reverse )
{
  seq_check( Result, Parm1 );

  metadata_check( Result, Parm1 );

  // Handle demotion case
  if ( Reverse )
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] = (float)( Parm1[i] ) OP Result[i];
    }
  }
  else
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] OPEQ (float)( Parm1[i] );
    }
  }
}

template<>
inline
void op< complex< float >, double >
( Sequence< complex< float > >& Result,
  const Sequence< double >& Parm1,
  bool Reverse )
{
  seq_check( Result, Parm1 );

  metadata_check( Result, Parm1 );

  // Handle demotion case
  if ( Reverse )
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] = (float)( Parm1[i] ) OP Result[i];
    }
  }
  else
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] OPEQ (float)( Parm1[i] );
    }
  }
}

template<>
inline
void op< complex< float >, complex< double > >
( Sequence< complex< float > >& Result,
  const Sequence< complex< double > >& Parm1,
  bool Reverse )
{
  seq_check( Result, Parm1 );

  metadata_check( Result, Parm1 );

  // Handle demotion case
  if ( Reverse )
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] = complex<float>( (float)Parm1[i].real(),
				  (float)Parm1[i].imag() ) OP
	Result[i];
    }
  }
  else
  {
    for ( size_t i = 0; i < Result.size(); i++ )
    {
      Result[i] OPEQ complex<float>( (float)Parm1[i].real(),
				     (float)Parm1[i].imag() );
    }
  }
}

//=============================================================================
// Addition Function
//=============================================================================

CLASS::
CLASS()
  : Function( CLASS::GetName() )
{
  //---------------------------------------------------------------------------
  // The sole purpose of the constructor is to register the action name, from
  // GetName(), using the Function constructor.
  //---------------------------------------------------------------------------
}

void CLASS::
Eval( CallChain* Chain, const CallChain::Params& Params,
      const std::string& Ret )
const
{
  if (2 != Params.size())
  {
    throw CallChain::BadArgumentCount( GetName(),
				       2,
				       Params.size() );
  }
  for ( INT_4U x = 0; x < Params.size( ); x++ )
  {
    if ( Params[x].length( ) <= 0 ) 
    {
      throw CallChain::BadDefaultArgument( x );
    }
  }

  udt*	left(Chain->GetSymbol(Params[0]));
  udt*	right(Chain->GetSymbol(Params[1]));
  udt*	answer;

  if ( !( answer = eval( *left, *right, false ) ) &&
       !( answer = eval( *right, *left, true ) ) )
  {
    std::string msg("unable to " OPERATION " ");
    msg += TypeInfoTable.GetName(typeid(*left));
    msg += " and ";
    msg += TypeInfoTable.GetName(typeid(*right));
    throw std::runtime_error(msg);
  }

  Chain->ResetOrAddSymbol( Ret, answer );
}

const std::string& CLASS::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name( OPERATION );

  return name;
}

//-----------------------------------------------------------------------------
// We construct a static instance of the AdditionFunction class. The
// constructor will register the name "add" and enable AdditionFunction::Eval
// to be called whenever the name is encountered.
//-----------------------------------------------------------------------------

static CLASS _a_local_instance;

//=======================================================================
// Definitions of static functions
//=======================================================================

static udt*
eval( const udt& Unknown1,
      const udt& Unknown2,
      bool Reverse )
{
  EVAL( Sequence< complex< float> > )
  else EVAL( Sequence< complex< double > > )
  else EVAL( Sequence<float> )
  else EVAL( Sequence<double> )
  else EVAL( Scalar< complex< double > > )
  else EVAL( Scalar< complex< float > > )
  else EVAL( Scalar<int> )
  else EVAL( Scalar<float> )
  else EVAL( Scalar<double> )
  return (udt*)NULL;
}

static udt*
eval2( const Scalar< double >& Known, const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Scalar< double > >	ret( Known.Clone() );

  EVAL2( Scalar<int> )
  else EVAL2( Scalar<float> )
  else EVAL2( Scalar<double> )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static udt*
eval2( const Scalar< float >& Known, const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Scalar< float > >	ret( Known.Clone() );

  EVAL2( Scalar<float> )
  else EVAL2( Scalar<int> )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static udt*
eval2( const Scalar< complex< double > >& Known, const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Scalar< complex< double > > >	ret( Known.Clone() );

  EVAL2( Scalar< complex< double > > )
  else EVAL2( Scalar< double > )
  else EVAL2( Scalar< float > )
  else EVAL2( Scalar< int > )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }

  return ret.release();
}

static udt*
eval2( const Scalar< complex< float > >& Known, const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Scalar< complex< float > > >	ret( Known.Clone() );

  EVAL2( Scalar< complex< double > > )
  else EVAL2( Scalar< complex< float > > )
  else EVAL2( Scalar< double > )
  else EVAL2( Scalar< float > )
  else EVAL2( Scalar< int > )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }

  return ret.release();
}

static udt*
eval2( const Scalar< int >& Known, const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Scalar< int > >	ret( Known.Clone() );

  EVAL2( Scalar<int> )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static udt*
eval2( const Sequence<float>& Known, const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Sequence< float > >	ret( Known.Clone() );

  EVAL2( Sequence<double> )
  else EVAL2( Sequence<float> )
  else EVAL2C( Scalar<int>, float )
  else EVAL2( Scalar<float> )
  else EVAL2C( Scalar<double>, float )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static udt*
eval2( const Sequence<complex< float > >& Known,
       const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Sequence< complex< float > > >	ret( Known.Clone() );

  EVAL2( Sequence<complex< double > > )
  else EVAL2( Sequence< complex< float > > )
  else EVAL2( Sequence<double> )
  else EVAL2( Sequence<float> )
  else EVAL2C( Scalar< complex< float > >, complex< float > )
  else EVAL2C( Scalar< complex< double > >, complex< float > )
  else EVAL2C( Scalar<int>, float )
  else EVAL2C( Scalar<float>, float )
  else EVAL2C( Scalar<double>, float )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static udt*
eval2( const Sequence<complex< double > >& Known,
       const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Sequence< complex< double > > >	ret( Known.Clone() );

  EVAL2( Sequence<complex< double > > )
  else EVAL2( Sequence<double> )
  else EVAL2( Sequence<float> )
  else EVAL2C( Scalar< complex< float > >, complex< double > )
  else EVAL2C( Scalar< complex< double > >, complex< double > )
  else EVAL2C( Scalar<int>, double )
  else EVAL2C( Scalar<float>, double )
  else EVAL2C( Scalar<double>, double )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}

static udt*
eval2( const Sequence<double>& Known, const udt& Unknown,
       bool Reverse )
{
  std::unique_ptr< Sequence< double > >	ret( Known.Clone() );

  EVAL2( Sequence<double> )
  else EVAL2C( Scalar<int>, double)
  else EVAL2( Scalar<double> )
  else EVAL2C( Scalar<float>, double )
  else
  {
    // Cannot do the conversion.
    // Destroy the pointer via unique_ptr destruction
    return (udt*)NULL;
  }
  return ret.release();
}
