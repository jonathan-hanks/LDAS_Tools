#include "datacondAPI/config.h"

#include "HaarFunction.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"
#include "HaarPSU.hh"

namespace datacondAPI
{

const std::string& HaarFunction::GetName(void) const
{
  static std::string name("Haar");
  
  return name;
}

HaarFunction::HaarFunction() : Function( HaarFunction::GetName() )
{ }

static HaarFunction _HaarFunction;

void HaarFunction::Eval(CallChain* Chain,
			const CallChain::Params& Params,
			const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;   

  using namespace datacondAPI;

  switch(Params.size())
    {
    case 0:
      {
	Haar haarWavelet;
	returnedSymbol = new Haar(haarWavelet);
      

	break;
      }//end case 0
    case 1:
      {

	int order = 0;
	if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[0])))
	  order = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[0])).GetValue();

	Haar haarWavelet(order);
	returnedSymbol = new Haar(haarWavelet);

	break;
      }//end case 1

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: HaarFunction");
	break;
      }//end default


    }//end switch
      
      if(!returnedSymbol)
	{
	  throw CallChain::NullResult( GetName() );
	}
      
      Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval


}//end namespace datacondAPI











