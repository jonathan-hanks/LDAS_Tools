/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef DATABASE_UDT_HH
#define	DATABASE_UDT_HH
// $Id: DatabaseUDT.hh,v 1.6 2006/02/16 16:54:58 emaros Exp $
#include <string>
#include <vector>

#include "CallChain.hh"
#include "UDT.hh"

// Forward declaration of ILwd elements
namespace ILwd
{
  class LdasElement;
}

namespace DB
{
  class Table;
}

namespace datacondAPI
{
  //---------------------------------------------------------------------
  //: Base class for sequence data
  //---------------------------------------------------------------------

  class Database : public udt
  {
  public:
    //: Different types of simple
    typedef enum {
      // Each column of the table can be represented by a FrVect
      FRAME,
      // None of the columns are BLOBs or CLOBs
      DATA_TYPES
    } simple_type;

    //: Default constructor
    Database( const DB::Table& Table,
	      CallChain::query_type QueryType,
	      const std::string& SQL = "",
	      bool IgnoreErrors = false );
    
    //: Copy Constructor
    Database( const Database& Source );

    //; Destructor
    virtual ~Database();
    
    //: Check if the database table contains simple data
    //!param: datacondAPI::udt::target_type API - Check if data is appropriate
    //+		for the specified API.
    void
    APICheck( datacondAPI::udt::target_type API ) const;

    //: Add a column of data
    void AppendColumn( const DB::Field& Field );

#ifdef OLD
    //: Assignment operator. NOTE: Unlike vector, this copy assignment is 
    //+ safe to use even when the lhs, rhs have different sizes. 
    //!param: rhs - object to copy from
    //!param: Database& - reference to this after copy
    virtual Database& operator=(const Database& rhs);
#endif	/* OLD */

    //: Duplicate *this on heap 
    virtual Database* Clone() const;

    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const;

    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!param: ILwd::LdasContainer* Container - Container to receive each
    //+		Row of data;
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual void
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target,
		   ILwd::LdasContainer* Container ) const;

    //: Check if the database table contains simple data
    //!param: simple_type Check - Rules to apply for checking for simplicity
    //!return: true if the table is considered simple, false otherwise
    bool
    IsSimple( simple_type Check ) const;

    //: Checks if the udt will be returning multiple ILwd's
    //!param: datacondAPI::udt::target_type - numeric target type
    //!return: bool - true if Target is TARGET_WRAPPER,
    //+		false otherwise.
    virtual bool ReturnsMultipleILwds( datacondAPI::udt::target_type Target )
      const;

    //: Store the query that origionated this result
    //
    //!param: const std::string& Query - The origionating query
    void SetQuery( const std::string& Query );

  private:
    //: Query that origionated this result
    class Column;

    std::string			m_query;
    std::vector<Column*>	m_columns;
    int				m_row_count;
    CallChain::query_type		m_query_type;
  };	/* Database<T> */

  inline void Database::
  SetQuery( const std::string& Query )
  {
    m_query = Query;
  }
}	/* namespace - datacondAPI */

#endif	/* DATABASE_UDT_HH */
