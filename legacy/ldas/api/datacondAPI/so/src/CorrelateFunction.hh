#ifndef CORRELATEFUNCTION_HH
#define CORRELATEFUNCTION_HH

//!ppp: {eval `cat $ENV{SRCDIR}/cc_rules.pl`}

#include "CallChain.hh"

class CorrelateFunction : CallChain::Function
{
public:
    //: Default constructor - construction of dummy instance registers  
    //+ the Eval method as the handler for calls to the action named by 
    //+ the GetName method
    CorrelateFunction();

    //: Evaluate an action call
    //!param: Chain - environment of the action call
    //!param: Params - container of parameter names
    //!param: Ret - return value name
    virtual void Eval(CallChain* Chain,
		      const CallChain::Params& Params, 
                      const std::string& Ret) const;

    //: Get the name of the handled action
    //!return: The name of the handled action
    virtual const std::string& GetName() const;
};

#endif // CORRELATEFUNCTION_HH
