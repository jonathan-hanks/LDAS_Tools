#ifndef DATACONDAPICONSTANTS_HH
#define DATACONDAPICONSTANTS_HH

// $Id: DataCondAPIConstants.hh,v 1.12 2006/11/07 22:25:46 emaros Exp $

#include <cstddef>

// Constants that express limitations of the Data Conditioning API

// Note that ALL CONSTANTS SHOULD BE DEFINED AS 
//
// const <type> <var>
//
// DO NOT USE MACROS FOR CONSTANTS
// CONSTANTS ARE DECLARED IN THIS FILE BUT DEFINED IN DataCondAPIConstants.cc

namespace datacondAPI {

    // Class FFT

    // Maximum length allowed for a Fourier transform
    extern const std::size_t MaximumFFTLength;


    // Class Mixer


    // Class CSD

    extern const std::size_t WelchCSDEstimateDefaultFFTLength;

    // Class Statistics

} // namespace datacondAPI

#endif
