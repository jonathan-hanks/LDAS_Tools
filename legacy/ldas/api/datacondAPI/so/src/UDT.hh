/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef UDT_HH
#define	UDT_HH
// $Id: UDT.hh,v 1.38 2009/05/20 00:14:59 emaros Exp $

#include <stdexcept>

#include "TypeInfo.hh"
#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/LdasHistory.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

// Forward declaration of ILwd classes
namespace ILwd
{
  class LdasElement;
}

class CallChain;

namespace datacondAPI {

  //---------------------------------------------------------------------
  //: Universal Data Type (UDT)
  //
  // <p>
  // This class is the base for all datatypes used in the Data
  // Conditioning API. It is the fundamental base class which is
  // manipulated by actions in the datacon CallChain. That is, all
  // CallChain actions take arguments and return objects which are
  // derived from the UDT class. The specific behaviour of each action
  // is determined by the dynamic type (that is, the "real" type) of
  // the UDT.
  //
  // <p>
  // The UDT class provides some basic support for operations that are
  // required in all the subclasses:
  // <list>
  // <li> History information associated with the data structure
  // <li> Casting UDTs down to a derived class
  // <li> Copying meta-data between UDT's
  // <li> Converting UDT's to ILWD's
  // </list>
  //
  // <p>
  // To construct a new UDT-based datatype:
  // <list>
  // <li>
  // Create a new header file and source file for the UDT.
  // The header file will contain the class declaration. The new class
  // must be publicly derived from class udt (note that the actual UDT
  // class name is in lowercase).
  // <li>
  // In the source file, use the macro UDT_CLASS_INSTANTIATION() to add an
  // identifying key for the class to the UDT database. The key is a text
  // string that will be used to identify the class in LDAS diagnostic and
  // error messages. Typically, the key will just be the name of the class.
  // Example:
  // <ul>
  //     UDT_CLASS_INSTANTIATION(TimeSeries&lt;float&gt;, "TimeSeries&lt;float&gt;");
  // </ul>
  // <li>
  // The pure virtual functions Clone() and ConvertToIlwd() 
  // must be implemented in the derived class.
  // </list>
  //---------------------------------------------------------------------
  class udt
  {
  public:

    //: possible destinations for ILWD representations of UDT objects
    typedef enum {
      TARGET_GENERIC,	// Default target
      TARGET_METADATA,
      TARGET_METADATA_FINAL_RESULT,
      TARGET_WRAPPER,
      TARGET_FRAME,
      TARGET_FRAME_FINAL_RESULT
    } target_type;

    //: Exception class thrown when a bad conversion is requested
    // This class represents a bad target conversion exception.
    class BadTargetConversion : public std::runtime_error
    {
    public:
      //: Constructor
      //
     //!param: udt::target_type Target - Requested target type.
      //!param: std::string ClassName - Name of class generating the error.
      //
      BadTargetConversion(udt::target_type Target,
			  const std::string& ClassName);

    private:
      //: Generate message
      //
      // This convienence routine forms the message string
      //
      //!param: udt::target_type Target - Requested target type.
      //!param: std::string ClassName - Name of class generating the error.
      //
      //!return: std::string - Formatted error message
      static std::string form_message(udt::target_type Target,
				      const std::string& ClassName);
    };

    //: Default constructor
    udt(const std::string& name = "");
    
    //: Copy Constructor
    udt( const udt& Source );
    
    //: Destructor
    virtual ~udt();

    //: Get the name of the UDT
    //
    //!ret: The name of the UDT
    std::string name() const;

    //: Set the name of the UDT
    //
    //!param: name - The name to be assigned to this UDT
    void SetName(const std::string& name);

    //: Copy history from another history object
    void AppendHistory( const ILwd::LdasHistory& History );

    //: log history of what happened to this object
    void AppendHistory( const std::string& Action );

    //: log history of what happened to this object
    void AppendHistory( const std::string& Action,
			const udt* Data, ... );

    //: Attempt to cast the UDT In to a const T&
    //!param: const udt& In - object to cast
    //!return: const T& - a reference to In cast to a const T&
    //!exc: bad_cast - Thrown if In is not able to be cast to class T&
    template<class T> static const T&  Cast(const udt& In);

    //: Attempt to cast the UDT In to a (non-const) T&
    //!param: udt& In - object to cast
    //!return: T& - a reference to In cast to a T&
    //!exc: bad_cast - Thrown if In is not able to be cast to class T&
    template<class T> static T&  Cast(udt& In);

    //: Copy the metadata
    // Copies metadata from another UDT to this UDT, if possible.
    //
    //!param: const udt& MetaData - Source meta data.
    virtual void CopyMetaData( const udt& MetaData );

    //: Duplicate *this
    // This function is a "virtual" copy constructor, in that the
    // duplicate returned is of the same (dynamic) type as object.
    // The new UDT is created on the heap - releasing this memory is
    // is the caller's responsibility.
    //
    //!ret: udt* - pointer to a duplicate of *this
    virtual udt* Clone(void) const = 0;

#if HAVE_LDAS_PACKAGE_ILWD
    //: Convert the UDT to an appropriate ILWD type. 
    //
    // Returns a pointer to a representation of the UDT as an ILWD formatted
    // for the requested target. Target formats include: generic, database,
    // wrapper, frame. If no appropriate conversion from the source to the
    // target exists, a null pointer is returned.
    //
    //!param: const CallChain& Chain - CallChain in which UDT was created.
    //!param: target_type Target - Type of conversion to be performed.
    //!return: ILwd::LdasElement* - The converted type or NULL if no
    //+        conversion possible.
    virtual ILwd::LdasElement*
    ConvertToIlwd(const CallChain& Chain,
		  target_type Target = TARGET_GENERIC) const = 0;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
    //: Append the UDT-specific meta-data to a container
    //
    // This function appends the meta-data contained in the UDT class
    // to the provided LdasContainer. For the UDT bass class itself, 
    // this applies ONLY to the LdasHistory meta-data contained in the UDT.
    // If no history is present, the contents of the LdasContainer remains
    // unchanged.
    // Note that the function is virtual (but not pure). Derived classes
    // may override the default behaviour.
    //
    //!param: const CallChain& Chain - CallChain in which UDT was created.
    //!param: target_type Target - Type of conversion to be performed.
    //!param: ILwd::LdasContainer* - Container to receive the ilwd
    virtual void ConvertToIlwd(const CallChain& Chain,
			       target_type Target,
			       ILwd::LdasContainer* Container) const;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

    //: Converts the numeric value of Target to string representation
    //
    // This convenience routine maps the numeric representation of the
    // target type to the string representation of the target type.
    //
    //!param: udt::target_type Target - numeric target type
    //!return: std::string - String representation of Target type.
    static std::string GetTargetType(udt::target_type Target);

    //: Converts the string value of Target to the numeric representation
    //
    // This convience routine maps the string representation of the
    // target type to the numeric representation of the target type.
    //
    //!param: std::string Target - String representation of Target type.
    //!param: udt::target_type - numeric target type
    static udt::target_type GetTargetType(const std::string& Target);

    //: See if the UDT is an instance of class T
    //!param: const udt& - object to query
    //!return: bool - true if *this is derived from class T
    template<class T>
    static bool IsA(const udt& In);

#if HAVE_LDAS_PACKAGE_ILWD
    //: Checks if the udt will be returning multiple ILwd's
    //!param: udt::target_type - numeric target type
    //!return: bool - true if udt will generate multiple ILwds,
    //+		false otherwise.
    virtual bool ReturnsMultipleILwds( udt::target_type Target )
      const;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
    //: Returns a reference to the history
    //!return: const ILwd::LdasHistory& - the history of this UDT
    const ILwd::LdasHistory& getHistory() const;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

    //: Returns the number of history records
    unsigned int SizeOfHistory() const;
    
  private:
#if HAVE_LDAS_PACKAGE_ILWD
    // Keeps track of what has happened to this object.
    ILwd::LdasHistory m_history;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
    
    // name of the udt
    std::string m_name;

  }; // udt

  ///--------------------------------------------------------------------
  /// Inline function definitions
  ///--------------------------------------------------------------------

#if HAVE_LDAS_PACKAGE_ILWD
  inline void udt::
  AppendHistory( const ILwd::LdasHistory& History )
  {
    m_history.Append( History );
  }
#endif /* HAVE_LDAS_PACKAGE_ILWD */

  inline
  std::string udt::name() const { return m_name; }

  inline
  void udt::SetName(const std::string& name) { m_name = name; }

#if HAVE_LDAS_PACKAGE_ILWD
  inline
  const ILwd::LdasHistory& udt::getHistory() const
  {
    return m_history;
  }
#endif /* HAVE_LDAS_PACKAGE_ILWD */

  inline unsigned int udt::
  SizeOfHistory() const
  {
    return m_history.size();
  }

} // namespace datacondAPI

#define UDT_DEFAULT_KEY key

#if __SUNPRO_CC
#define MK_UDT_INIT_FUNC(name,func_list) \
void \
name( ) \
{ \
  func_list; \
}
#else
#define MK_UDT_INIT_FUNC(name,func_list)
#endif

#define MK_UDT_INIT_FUNC_1(name,func1) \
  MK_UDT_INIT_FUNC(name,a_##func1())
#define MK_UDT_INIT_FUNC_2(name,func1,func2) \
  MK_UDT_INIT_FUNC_1(name,func1(); a_##func2)
#define MK_UDT_INIT_FUNC_3(name,f1,f2,f3)	\
  MK_UDT_INIT_FUNC_2(name,f1,f2();a_##f3)
#define MK_UDT_INIT_FUNC_4(name,f1,f2,f3,f4)	\
  MK_UDT_INIT_FUNC_3(name,f1,f2,f3();a_##f4)

#if __SUNPRO_CC
#define ADD_TO_TYPE_INFO_TABLE(class_,key_)	\
  static void a_##key_( ) \
  { \
    using namespace datacondAPI; \
    (void)TheTypeInfoTable().AddType( typeid( class_ ), #class_ );	\
  }
#else
#define ADD_TO_TYPE_INFO_TABLE(class_,key_)			\
  namespace datacondAPI { \
    static bool a_##key_ = TheTypeInfoTable().AddType( typeid( class_ ), #class_ ); \
  } /* namespace - datacondAPI */
#endif /* __SUNPRO_CC */

#define	UDT_CLASS_INSTANTIATION_CAST(class_)	\
namespace datacondAPI { \
template<> const class_& udt::Cast< class_ >(const udt& In) \
{ \
  return dynamic_cast<const class_&>(In); \
} \
\
template<> class_& udt::Cast< class_ >(udt& In) \
{ \
  return dynamic_cast<class_&>(In); \
} \
\
template<> bool udt::IsA< class_ >(const udt& In) \
{ \
  return (dynamic_cast<const class_*>(&In) != (const class_*)NULL); \
} \
} /* namespace datacondAPI */

#define	UDT_CLASS_INSTANTIATION(class_, key_)	\
UDT_CLASS_INSTANTIATION_CAST(class_)	\
ADD_TO_TYPE_INFO_TABLE(class_,key_)

#endif // UDT_HH
