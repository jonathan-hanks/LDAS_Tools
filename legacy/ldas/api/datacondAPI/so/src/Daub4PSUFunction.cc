#include "datacondAPI/config.h"

#include "Daub4PSUFunction.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"
#include "HaarPSU.hh"

using namespace datacondAPI::psu;

const std::string& Daub4PSUFunction::GetName(void) const
{
  static std::string name("Daub4PSU");
  
  return name;
}

Daub4PSUFunction::Daub4PSUFunction() : Function(GetName())
{ }

static Daub4PSUFunction _Daub4PSUFunction;

void Daub4PSUFunction::Eval(CallChain* Chain,
			const CallChain::Params& Params,
			const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;   

  switch(Params.size())
    {
    case 0:
      {
	Daub4 daub4Wavelet;
	returnedSymbol = new Daub4(daub4Wavelet);
      

	break;
      }//end case 0
    case 1:
      {

	int order = 0;
	if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[0])))
	  order = datacondAPI::udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[0])).GetValue();

	Daub4 daub4Wavelet(order);
	returnedSymbol = new Daub4(daub4Wavelet);

	break;
      }//end case 1

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: Daub4FunctionPSU");
	break;
      }//end default


    }//end switch
      
      if(!returnedSymbol)
	{
	  throw CallChain::NullResult( GetName() );
	}
      
      Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval











