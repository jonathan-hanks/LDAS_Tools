// $Id: setElementFunction.cc,v 1.2 2005/12/01 22:54:59 emaros Exp $
//
// "setElement" action
//
// setElement(y, k, x);  <=> y[k] = x
//
// y: Sequence<T>
// k: Scalar<int>
// x: Scalar<T>

#include "config.h"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"

#include "setElementFunction.hh"

using namespace datacondAPI;
using namespace std;

namespace {

    setElementFunction static_setElementFunction;

}

const string& setElementFunction::GetName() const
{
  static const string name("setElement");

  return name;
}

setElementFunction::setElementFunction()
    : Function( setElementFunction::GetName() )
{
}

void setElementFunction::checkIndex(const size_t ySize, const int k) const
{
    if (k < 0)
    {
        throw invalid_argument("Illegal Argument: "
                               "action setElement(); "
                               "argument #1 index must be non-negative");
    }
    
    // avoids a compiler warning
    if (static_cast<size_t>(k) >= ySize)
    {
        throw invalid_argument("Illegal Argument: "
                               "action setElement(); "
                               "argument #1 index out of range");
    }
}

// lhs is real or complex, rhs is real
void setElementFunction::setRealElement(udt& yUdt,
                                        const int k,
                                        const double& x) const
{
    if (Sequence<float>* const y = dynamic_cast<Sequence<float>*>(&yUdt))
    {
        checkIndex(y->size(), k);
        (*y)[k] = x;
    }
    else if (Sequence<double>* const y =
             dynamic_cast<Sequence<double>*>(&yUdt))
    {
        checkIndex(y->size(), k);
        (*y)[k] = x;
    }
    else if (Sequence<complex<float> >* const y =
             dynamic_cast<Sequence<complex<float> >*>(&yUdt))
    {
        checkIndex(y->size(), k);
        (*y)[k] = x;
    }
    else if (Sequence<complex<double> >* const y =
             dynamic_cast<Sequence<complex<double> >*>(&yUdt))
    {
        checkIndex(y->size(), k);
        (*y)[k] = x;
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     0,
                                     TypeInfoTable.GetName(typeid(yUdt)),
                                     "object derived from Sequence<T>");
    }
}

// lhs is complex, rhs is complex
void setElementFunction::setComplexElement(udt& yUdt,
                                           const int k,
                                           const complex<double>& x) const
{
    if (Sequence<complex<float> >* const y =
        dynamic_cast<Sequence<complex<float> >*>(&yUdt))
    {
        // Need to construct complex<float> explicitly
        checkIndex(y->size(), k);
        (*y)[k] = complex<float>(x);
    }
    else if (Sequence<complex<double> >* const y =
             dynamic_cast<Sequence<complex<double> >*>(&yUdt))
    {
        checkIndex(y->size(), k);
        (*y)[k] = x;
    }
    else
    {
        throw CallChain::BadArgument(GetName(),
                                     0,
                                     TypeInfoTable.GetName(typeid(yUdt)),
                                  "object derived from Sequence<complex<T> >");
    }
}

void setElementFunction::Eval(CallChain* chain,
                              const CallChain::Params& params,
                              const string& ret) const
{
    if (ret != "")
    {
        throw invalid_argument("Illegal Argument: "
                               "action setElement(); no return value allowed");
    }

    if (params.size() == 3)
    {
        udt* const yUdt = chain->GetSymbol(params[0]);

        const udt* const kUdt = chain->GetSymbol(params[1]);
        const udt* const xUdt = chain->GetSymbol(params[2]);

        // Get k
        int k = 0;
        
        if (const Scalar<int>* const p
            = dynamic_cast<const Scalar<int>*>(kUdt))
        {
            k = p->GetValue();
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         1,
                                         TypeInfoTable.GetName(typeid(*kUdt)),
                                         "Scalar<int>");
        }

        // Ok - got k

        if (const Scalar<double>* const x
            = dynamic_cast<const Scalar<double>*>(xUdt))
        {
            setRealElement(*yUdt, k, x->GetValue());
        }
        else if (const Scalar<complex<double> >* const x
            = dynamic_cast<const Scalar<complex<double> >*>(xUdt))
        {
            setComplexElement(*yUdt, k, x->GetValue());
        }
        else if (const Scalar<int>* const x
            = dynamic_cast<const Scalar<int>*>(xUdt))
        {
            setRealElement(*yUdt, k, x->GetValue());
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         2,
                                         TypeInfoTable.GetName(typeid(*xUdt)),
                                         "Scalar<T>");
        }
    }
    else
    {
        throw CallChain::BadArgumentCount(GetName(), "3", params.size());
    }

    // Nothing to return - all done in-place
}
