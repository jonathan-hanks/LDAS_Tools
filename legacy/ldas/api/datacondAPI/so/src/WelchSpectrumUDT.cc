/* -*- mode: c++; c-basic-offset: 4; -*- */

#if __SUNPRO_CC
#pragma ident "$Id: WelchSpectrumUDT.cc,v 1.56 2006/11/27 21:32:14 emaros Exp $"
#else
const char rcsID[] = "$Id: WelchSpectrumUDT.cc,v 1.56 2006/11/27 21:32:14 emaros Exp $";
#endif

#include "config.h"

#include <memory>
#include <stdexcept>

#include "general/Memory.hh"

#include "SequenceUDT.hh"
#include "WelchSpectrumUDT.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"

#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldBlob.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/Table.hh"
#include "dbaccess/Transaction.hh"
#include "dbaccess/Process.hh"
#include "dbaccess/SummSpectrum.hh"

#include "CallChain.hh"
#include "UtilSpectrum.hh"

//-----------------------------------------------------------------------

static datacondAPI::udt*
create_from_mime( const std::string& Channel,
		  const Mime::Spectrum& Data );

template<class T>
static datacondAPI::WelchSpectrum<T>*
create_from_data( INT_4S StartTimeSeconds,
		  INT_4S Start_timeNanoseconds,
		  INT_4S EndTimeSeconds,
		  INT_4S EndTimeNanoseconds,
		  const std::string& Channel,
		  const T* Spectra,
		  INT_4S Length,
		  REAL_8 Base,
		  REAL_8 Delta );

//-----------------------------------------------------------------------

template<class T> datacondAPI::WelchSpectrum<T>::WelchSpectrum()
{
}

template<class T> datacondAPI::WelchSpectrum<T>::WelchSpectrum(const WelchSpectrum& value)
: CSDSpectrum<T>(value),
  WelchSpectrumUMD(value)
{
}
		
template<class T> datacondAPI::WelchSpectrum<T>::~WelchSpectrum()
{
}
		
template<class T>
datacondAPI::WelchSpectrum<T>* datacondAPI::WelchSpectrum<T>::
Clone() const
{
  return new WelchSpectrum<T>(*this);
}

template<class T>
ILwd::LdasContainer* datacondAPI::WelchSpectrum<T>::
ConvertToIlwd(  const CallChain& Chain,
		datacondAPI::udt::target_type Target ) const
{
  using namespace datacondAPI;

  std::unique_ptr< ILwd::LdasContainer > container( new ILwd::LdasContainer );

  switch ( Target )
  {
  case datacondAPI::udt::TARGET_FRAME:
  case datacondAPI::udt::TARGET_FRAME_FINAL_RESULT:
      {
	  using namespace ILwdFCS;

	  WelchSpectrum<T>& data = const_cast<WelchSpectrum<T>&>(*this);
	  const std::string& lname( Chain.GetIntermediateName( *this,
							       this->name( ) ) );
	  
	  FrProcData proc_data;

          proc_data.SetName( lname );
          proc_data.SetComment( Chain.GetIntermediateComment( *this ) );
          proc_data.SetType( ILwdFCS::FrProcData::FREQUENCY_SERIES );
          proc_data.SetSubType( ILwdFCS::FrProcData::POWER_SPECTRAL_DENSITY );

          //:NOTE: undefined for this class
          // proc_data.SetTimeOffset( 0.0 );
          // proc_data.SetTimeOffset( 0.0 );

          proc_data.SetTRange( this->GetEndTime() - this->GetStartTime() );
          
          //:NOTE: datacon has opposite sign convention to frame spec
          // for frequency shift
          proc_data.SetFShift( -GetBaseFrequency() );

          //:NOTE: Phase is unknown - leave undefined
          // proc_data.SetPhase(0.0);

          proc_data.SetFRange( this->GetFrequency( this->size( ) ) -
			       this->GetFrequency(0) );

          //:NOTE: Bandwidth is unknown - leave undefined (although we could
          // add this as metadata)
          // proc_data.SetBW(0.0);
          
          //:NOTE: Add auxParams, aux data, tables here

          // Add the data
	  FrVect vect_data;
	  vect_data.SetData( &data[0],
                             data.size(),
			     this->GetFrequency( 0 ),
			     "Hertz",
			     this->GetFrequencyDelta() );
          vect_data.SetName( lname );

	  proc_data.AppendData( vect_data );

          proc_data.AppendHistory( this->getHistory(), GPSTime::NowGPSTime() );

	  ArbitraryWhenMetaData::Store( proc_data );

	  container.reset( const_cast<CallChain&>(Chain).
			   FormatFrame( Target,
					container.get( ),
					proc_data ) );

	  // Store the metadata in an FrDetector struct
	  GeometryMetaData::Store( const_cast<CallChain&>(Chain).
				   GetFrameHeaderTop() );
	  
      }
      break;

  case datacondAPI::udt::TARGET_METADATA:
  case datacondAPI::udt::TARGET_METADATA_FINAL_RESULT:
      {
	  CallChain& chain( const_cast<CallChain&>(Chain) );
	  WelchSpectrum< T >&	seq(const_cast<WelchSpectrum< T >&>(*this));
	  const CHAR_U*		data((CHAR_U*)&(seq[0]));
	  
	  DB::Table*		spectrum =
	      chain.FindOrCreateTable( DB::SummSpectrum::TABLE_NAME );
	  
	  DB::Table*		process =
	      chain.FindOrCreateTable( DB::Process::TABLE_NAME );
	  
	  spectrum->Associate(spectrum->GetRowCount(), *process, 0);

	  std::string program(process->GetFieldString(process->HasField(DB::Process::PROGRAM)).operator[](0));
	  
	  spectrum->AppendColumnEntry(DB::SummSpectrum::PROGRAM,
				      program);
	      
#define	e(a,b) spectrum->AppendColumnEntry(DB::SummSpectrum:: a, b)
#define	e2(a,b,c) spectrum->AppendColumnEntry(DB::SummSpectrum:: a, b, c)

	  e(START_TIME_SECONDS,	(INT_4S)(this->GetStartTime().GetSeconds()));
	  e(START_TIME_NANOSECONDS,	(INT_4S)(this->GetStartTime().GetNanoseconds()));
	  e(END_TIME_SECONDS,	(INT_4S)(this->GetEndTime().GetSeconds()));
	  e(END_TIME_NANOSECONDS,	(INT_4S)(this->GetEndTime().GetNanoseconds()));
	  e(CHANNEL,		GetNameOfChannel());
	  e(TYPE,			"Welch");
	  e2(SPECTRUM,		data, this->size()*sizeof(T));
	  e(LENGTH,			(INT_4S)(this->size()));
	  e(START_FREQUENCY,       	this->GetFrequency( 0 ) );
	  e(DELTA_FREQUENCY,       	this->GetFrequencyDelta() );
	  e(MIMETYPE,		pGetMimeType<T>() );
#undef e
#undef e2
	  chain.FormatTransaction( Target, container.get( ) );
      }
      break;

  case datacondAPI::udt::TARGET_WRAPPER:
      {
          WelchSpectrum<T>& data = const_cast<WelchSpectrum<T>&>(*this);
          std::string domain( this->GetDomain() + ":domain");
          
          container->appendName( this->name() );
          container->appendName( "spectrum" );
          container->appendName( "sequence" );
          container->appendName( "" );        // Place holder for primary.
          container->push_back( new ILwd::LdasString("FREQ", domain),
                                ILwd::LdasContainer::NO_ALLOCATE,
                                ILwd::LdasContainer::OWN );
          // Store the meta data
          GeometryMetaData::Store( *container );
          FrequencyMetaData::store( *container, Target, this->size() );
          container->push_back(new ILwd::LdasArray<REAL_8>
                               (GetBaseFrequency(), "base_freq", "hz"),
                               ILwd::LdasContainer::NO_ALLOCATE,
                               ILwd::LdasContainer::OWN );
          // Store History
          udt::ConvertToIlwd( Chain, Target, container.get() );
          
          // Store data
          container->push_back(
                        new ILwd::LdasArray<T>(&data[0], data.size(), "data"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN );

          TimeBoundedFreqSequenceMetaData::store( *container.get(), Target );
          container->appendName( GetNameOfChannel( ) );
      }
      break;

  case datacondAPI::udt::TARGET_GENERIC:
      container->push_back( CSDSpectrum<T>::ConvertToIlwd(Chain, Target),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( WelchSpectrumUMD::ConvertToIlwd(Chain, Target),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( new ILwd::LdasString( this->name( ),
						  "NameMetaData" ),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      break;

  default:
      throw udt::BadTargetConversion(Target, "datacondAPI::WelchSpectrum");
      break;
  }
  
  return container.release();
}
	
namespace datacondAPI
{
    template<>
    void datacondAPI::WelchSpectrum<double>::
    CreateFromDBTable( const DB::Table& Table,
		       std::vector<datacondAPI::udt*>& Spectrum,
		       std::vector<std::string>& Names,
		       const bool IgnoreTableName )
    {
	using namespace DB::SummSpectrum;
	using namespace DB;

	if ( ( !IgnoreTableName ) && ( Table.GetName() != TABLE_NAME ) )
	{
	    std::string msg("Table is: ");
	    msg.append( Table.GetName() );
	    msg.append( " instead of: ");
	    msg.append( DB::SummSpectrum::TABLE_NAME );
		   
	    throw std::invalid_argument( msg );
	}

#define	F(type,var,col_name)						\
	type& var( Table.Get##type( Table.HasField(col_name) ) )

	DB::Table		no_table("__none__");
	DB::FieldString	no_mime_info("__none__", false, no_table);

	F(FieldArray<INT_4S>,start_time_sec,START_TIME_SECONDS);
	F(FieldArray<INT_4S>,start_time_nsec,START_TIME_NANOSECONDS);
	F(FieldArray<INT_4S>,end_time_sec,END_TIME_SECONDS);
	F(FieldArray<INT_4S>,end_time_nsec,END_TIME_NANOSECONDS);

	F(FieldArray<REAL_8>,base,START_FREQUENCY);
	F(FieldArray<REAL_8>,delta,DELTA_FREQUENCY);
	FieldString& mime_type( (Table.HasField(MIMETYPE) )
				? Table.GetFieldString( Table.HasField(MIMETYPE) )
				: no_mime_info );

	F(FieldString,channel,CHANNEL);

	F(FieldBlob,spectra,SPECTRUM);
	F(FieldArray<INT_4S>,length,LENGTH);
#undef F

	Mime::Spectrum::SpectrumTypeId	mime_id;

	for ( DB::Table::rowoffset_type x = 0; x < Table.GetRowCount(); x++ )
	{
	    try
	    {
		//-----------------------------------------------------------
		// Try to figure out what is being worked on
		//-----------------------------------------------------------

		if ( ( &mime_type == &no_mime_info ) || ( mime_type[x] == "" ) )
		{
		    mime_id = Mime::Spectrum::REAL_LITTLEENDIAN;
		}
		else
		{
		    mime_id = Mime::Spectrum::GetSpectrumId(mime_type[x]);
		}

		//-----------------------------------------------------------
		// Generate the mime type
		//-----------------------------------------------------------

		Mime::Spectrum	ms( mime_id,
				    start_time_sec[x],
				    start_time_nsec[x],
				    end_time_sec[x],
				    end_time_nsec[x],
				    spectra[x]->getData(),
				    spectra[x]->getDimension( 0 ),
				    length[x],
				    base[x],
				    delta[x] );
		Spectrum.push_back( create_from_mime( channel[x], ms ) );
	    }
	    catch( ... )
	    {
		continue;
	    }
	}
    }

    template<class T>
    void datacondAPI::WelchSpectrum<T>::
    CreateFromDBTable( const DB::Table& Table,
		       std::vector<datacondAPI::udt*>& Spectrum,
		       std::vector<std::string>& Names,
		       const bool IgnoreTableName )
    {
	datacondAPI::WelchSpectrum<double>::
	    CreateFromDBTable( Table, Spectrum, Names, IgnoreTableName );
    }

    template<>
    bool datacondAPI::WelchSpectrum<double>::
    ValidateDBTable( const DB::Table& Table )
    {
	using namespace DB::SummSpectrum;
	using namespace DB;

	try {
	    //---------------------------------------------------------------
	    // Table must have at least these fields.
	    //---------------------------------------------------------------
	    Table.HasField( START_TIME_SECONDS );
	    Table.HasField( START_TIME_NANOSECONDS );
	    Table.HasField( END_TIME_SECONDS );
	    Table.HasField( END_TIME_NANOSECONDS );

	    Table.HasField( START_FREQUENCY );
	    Table.HasField( DELTA_FREQUENCY );

	    Table.HasField( CHANNEL );

	    Table.HasField( SPECTRUM );
	    Table.HasField( LENGTH );
	    return true;
	}
	catch( ... )
	{
	}
	return false;
    }

    template<class T>
    bool datacondAPI::WelchSpectrum<T>::
    ValidateDBTable( const DB::Table& Table )
    {
	return datacondAPI::WelchSpectrum<double>::
	    ValidateDBTable( Table );
    }
} // namespace - datacondAPI

static datacondAPI::udt*
create_from_mime( const std::string& Channel, const Mime::Spectrum& Data )
{
    //-----------------------------------------------------------
    // Create UDT
    //-----------------------------------------------------------
#define CREATE(lm_type) \
    create_from_data< lm_type >( Data.GetStartSecond(), \
			         Data.GetStartNanosecond(), \
			         Data.GetStopSecond(), \
			         Data.GetStopNanosecond(), \
			         Channel, \
			         (lm_type*)(Data.GetSpectrum()), \
			         Data.GetLength(), \
			         Data.GetBase(), \
			         Data.GetDelta() )

    switch ( Data.GetSpectrumTypeId() )
    {
    case Mime::Spectrum::REAL_BIGENDIAN:
    case Mime::Spectrum::REAL_LITTLEENDIAN:
	switch ( Data.GetByteCount() / Data.GetLength() )
	{
	case sizeof(float):
	    return CREATE(float);
	    break;
	case sizeof(double):
	    return CREATE(double);
	}
	break;
    case Mime::Spectrum::COMPLEX_BIGENDIAN:
    case Mime::Spectrum::COMPLEX_LITTLEENDIAN:
	switch ( ( Data.GetByteCount() / 2 ) / Data.GetLength() )
	{
	case sizeof(float):
	    return CREATE(std::complex< float >);
	    break;
	case sizeof(double):
	    return CREATE(std::complex< double >);
	    break;
	}
	break;
    }
    throw std::runtime_error
	( "create_from_mime: Unable to WelchSpectrum spectrum");
    return (datacondAPI::udt*)NULL;
#undef CREATE
}

template<class T>
static datacondAPI::WelchSpectrum<T>*
create_from_data( INT_4S StartTimeSeconds,
		  INT_4S StartTimeNanoseconds,
		  INT_4S EndTimeSeconds,
		  INT_4S EndTimeNanoseconds,
		  const std::string& Channel,
		  const T* Spectra,
		  INT_4S Length,
		  REAL_8 Base,
		  REAL_8 Delta)
{
    //-------------------------------------------------------------------
    // Create space
    //-------------------------------------------------------------------
    datacondAPI::WelchSpectrum<T>* w( new datacondAPI::WelchSpectrum<T>() );
    //-------------------------------------------------------------------
    // Put in data
    //-------------------------------------------------------------------
    w->resize(Length);
    std::copy(Spectra, Spectra + Length, &( w->operator[](0) ) );
    w->SetFrequencyBase( Base );
    w->SetFrequencyDelta( Delta );
    //-------------------------------------------------------------------
    // Put in the start and end time
    //-------------------------------------------------------------------
    w->SetStartTime( General::GPSTime( StartTimeSeconds,
				       StartTimeNanoseconds ) );
    w->SetEndTime( General::GPSTime( EndTimeSeconds,
				     EndTimeNanoseconds ) );
    //-------------------------------------------------------------------
    // Put in channel name
    //-------------------------------------------------------------------
    w->SetNameOfChannel(Channel);
    //-------------------------------------------------------------------
    // Return the WelchSpectrum
    //-------------------------------------------------------------------
    return w;
}

#undef CLASS_INSTANTIATION
#define CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::WelchSpectrum< class_ >; \
UDT_CLASS_INSTANTIATION(WelchSpectrum< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
