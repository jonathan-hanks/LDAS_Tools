#include "datacondAPI/config.h"

#include "general/types.hh"

#include "ilwd/ilwdtoc.hh"
#include "ilwd/ldasarray.hh"

#include "ilwdfcs/FrDetector.hh"

#include "GeometryMetaData.hh"
#include "ILwdMetaDataHelper.hh"
#include "Ingester.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

using namespace datacondAPI;

#if 0
#define AT() cerr << "DEBUG: AT: " << (void*)this << " " << __FILE__ << " " << __LINE__ << endl << flush
#define AT_STATIC() cerr << "DEBUG: AT: " << " " << __FILE__ << " " << __LINE__ << endl << flush
#else
#define AT() 
#define AT_STATIC()
#endif

Ingester::
Ingester( CallChain& Chain, ingestion_type Ingestion, bool AllowDuplicates )
  : m_chain( Chain),
    m_ingestion( Ingestion ),
    m_allow_duplicates( AllowDuplicates )
{
  
}

std::string Ingester::
IngestILwd( const ILwd::LdasElement& Element )
{
  ingest_ilwd_element( &Element, "" );
  return m_symbols;
}

template<class DataType>
void Ingester::
add_ilwd_int_symbol(const std::string& Symbol,
		    const ILwd::LdasArray<DataType>* Data )
{

  AT();
  if (Data == 0)
  {
    AT();
    throw std::invalid_argument("Ingester::add_ilwd_int_symbol: "
			        "Input LdasArray pointer is NULL");
  }

  AT();
  if (Data->getNDim() != 1)
  {
    AT();
    throw std::invalid_argument("Ingester::add_ilwd_int_symbol: "
			        "Input LdasArray must be 1-dimensional");
  }

  AT();
  if (Data->getDimension(0) < 1)
  {
    AT();
    throw std::invalid_argument("Ingester::add_ilwd_int_symbol: "
			        "Input LdasArray must have length >= 1");
  }

  AT();
  CallChain::Symbol* obj = 0;

  AT();
  if (Data->getDimension(0) == 1)
  {
    AT();
    const DataType tmpData = Data->getData()[0];
    const int data = tmpData;

    // Check that the conversion from unsigned int to int worked.
    // Note that the second check must be <= (rather than <)
    if ((tmpData > 0) && (data <= 0))
    {
      throw std::invalid_argument("Ingester::add_ilwd_int_symbol: "
	         "conversion from unsigned to signed gave changed value");
    }

    AT();
    obj = new datacondAPI::Scalar<int>(data);
  }
  else
  {
    AT();
    obj = new datacondAPI::Sequence<float>(Data->getData(),
					   Data->getDimension(0));
  }

  AT();
  obj->SetName(Symbol);

  AT();
  m_chain.StoreUDT( Symbol, obj, m_ingestion, m_allow_duplicates );
  add_symbol_name( Symbol );
}

template<class DataType>
void Ingester::
add_ilwd_float_symbol(const std::string& Symbol,
		      const ILwd::LdasArray<DataType>* Data )
{
  if (Data == 0)
  {
    throw std::invalid_argument("Ingester::add_ilwd_float_symbol: "
			        "Input LdasArray pointer is NULL");
  }

  if (Data->getNDim() != 1)
  {
    throw std::invalid_argument("Ingester::add_ilwd_float_symbol: "
			        "Input LdasArray must be 1-dimensional");
  }

  if (Data->getDimension(0) < 1)
  {
    throw std::invalid_argument("Ingester::add_ilwd_float_symbol: "
			        "Input LdasArray must have length >= 1");
  }

  CallChain::Symbol* obj = 0;

  if (Data->getDimension(0) == 1)
  {
    obj = new datacondAPI::Scalar<DataType>(Data->getData()[0]);
  }
  else
  {
    obj = new datacondAPI::Sequence<DataType>(Data->getData(),
					      Data->getDimension(0));
  }

  obj->SetName(Symbol);

  m_chain.StoreUDT( Symbol, obj, m_ingestion, m_allow_duplicates );
  add_symbol_name( Symbol );
}

void Ingester::
add_symbol_name( const std::string& SymbolName )
{
  if ( SymbolName.length() )
  {
    if ( m_symbols.length() )
    {
      m_symbols += " ";
    }
    m_symbols += SymbolName;
  }
}

bool Ingester::
detector( const datacondAPI::ILwdMetaDataHelper& ILwdHelper )
{
  AT( );
  if ( ILwdHelper.GetContainer( ).getName( 2 ).compare( "Container(Detector)" ) != 0 )
  {
    AT( );
    return false;
  }
  AT( );
  for ( ILwd::LdasContainer::const_iterator
	  d( ILwdHelper.GetContainer().begin() );
	d != ILwdHelper.GetContainer().end();
	d++ )
  {
    AT( );
    ILwd::TOC	helper( *(*d), ILwdFCS::FrDetector::TOCKeys );

    AT( );
    m_detectors.AddDetector( helper );
    
  }
  AT( );
  return true;
}

bool Ingester::
detector( const ILwd::LdasContainer* Container )
{
  if ( Container )
  {
    ILwdMetaDataHelper	helper(*Container);

    return detector( helper );
  }
  return false;
}

void Ingester::
ingest_ilwd_element( const ILwd::LdasElement* Element,
		     const std::string& Prefix)
{
  std::string symbol = Prefix;

  if ( Element == (const ILwd::LdasElement*)NULL )
  {
    return;
  }

  if ( ( Element->getNameString().compare( "frame_group" ) != 0 ) &&
       ( Element->getNameString().compare( "frame_set" ) != 0 ) )
  {
    // Treat frame_group and frame_seet like the empty string for backwards
    //   compatability generated names.
    symbol.append(Element->getNameString());
  }

  AT();

  if ( ingest_udt( symbol, *Element ) )
  {
    AT();
    return;
  }

  AT( );
  if ( detector( dynamic_cast<const ILwd::LdasContainer*>( Element ) ) )
  {
    AT( );
    return;
  }
  if ( m_chain.IngestDatabaseData( symbol, *Element, m_ingestion,
				   CallChain::QUERY_GENERIC  ) )
  {
    AT();
    add_symbol_name( symbol );
    return;
  }
  switch(Element->getElementId())
  {
  case ILwd::ID_INT_2S:
    AT();
    add_ilwd_int_symbol<INT_2S>
      (symbol, dynamic_cast<const ILwd::LdasArray<INT_2S>*>(Element) );
    break;
  case ILwd::ID_INT_2U:
    AT();
    add_ilwd_int_symbol<INT_2U>
      (symbol,
       dynamic_cast<const ILwd::LdasArray<INT_2U>*>(Element) );
    break;
  case ILwd::ID_INT_4S:
    AT();
    add_ilwd_int_symbol<INT_4S>
      (symbol,
       dynamic_cast<const ILwd::LdasArray<INT_4S>*>(Element));
    break;
  case ILwd::ID_INT_4U:
    AT();
    add_ilwd_int_symbol<INT_4U>
      (symbol,
       dynamic_cast<const ILwd::LdasArray<INT_4U>*>(Element) );
    break;
  case ILwd::ID_REAL_4:
    AT();
    add_ilwd_float_symbol<REAL_4>
      (symbol,
       dynamic_cast<const ILwd::LdasArray<REAL_4>*>(Element) );
    break;
  case ILwd::ID_REAL_8:
    AT();
    add_ilwd_float_symbol<REAL_8>
      (symbol,
       dynamic_cast<const ILwd::LdasArray<REAL_8>*>(Element) );
    break;
  case ILwd::ID_COMPLEX_8:
    AT();
    add_ilwd_float_symbol<COMPLEX_8>
      (symbol,
       dynamic_cast<const ILwd::LdasArray<COMPLEX_8>*>(Element) );
    break;
  case ILwd::ID_COMPLEX_16:
    AT();
    add_ilwd_float_symbol<COMPLEX_16>
      (symbol,
       dynamic_cast<const ILwd::LdasArray<COMPLEX_16>*>(Element) );
    break;
  case ILwd::ID_ILWD:
    AT();
    ingest_ilwd_container( dynamic_cast<const ILwd::LdasContainer*>(Element),
			   Prefix);
    break;
  default:
    // Quietly ignore unhandled data types
    AT();
    return;
  }
  AT();
  return;
}

void Ingester::
ingest_ilwd_container( const ILwd::LdasContainer* Container,
		       const std::string& Prefix)
{
  std::string	prefix(Prefix);

  if ( ( Container->getNameString().length() > 0) &&
       ( Container->getNameString().compare( "frame_group" ) != 0 ) &&
       ( Container->getNameString().compare( "frame_set" ) != 0 ) )
  {
    // Append Name of this container.
    prefix.append(Container->getNameString());
    prefix.append(":");
  }
  for (ILwd::LdasContainer::const_iterator i = Container->begin();
       i != Container->end();
       i++)
  {
    if (*i)
    {
      ingest_ilwd_element( (*i), prefix );
      AT();
    }
  }
}

void Ingester::
store_symbol( std::string Name,
	      datacondAPI::udt* UDT,
	      CallChain::IntermediateResult::primary_type Primary )
{
  std::string names;

  m_chain.StoreUDT( Name, UDT, m_ingestion, m_allow_duplicates,
		    &names, Primary );
  names = names.substr(names.find_first_not_of(" "));
  add_symbol_name( names );
}

void Ingester::
store_symbol( std::string Name,
	      const std::vector<datacondAPI::udt*>& UDTS,
	      CallChain::IntermediateResult::primary_type Primary )
{
  std::string names;

  m_chain.StoreUDT( Name, UDTS, m_ingestion, m_allow_duplicates,
		    &names, Primary );
  names = names.substr(names.find_first_not_of(" "));
  add_symbol_name( names );
}
