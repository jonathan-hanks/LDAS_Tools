#include "datacondAPI/config.h"

#include "DataCondAPIConstants.hh"

// Maximum length allowed for a Fourier transform
// specified by MDC document section 4.7.4
const std::size_t datacondAPI::MaximumFFTLength = 8388608; // 2^23

// Class Mixer


// Class CSD

const std::size_t datacondAPI::WelchCSDEstimateDefaultFFTLength = 1024;

// Class Statistics

