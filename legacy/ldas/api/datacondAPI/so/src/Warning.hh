// $Id: Warning.hh,v 1.3 2001/03/19 02:24:55 ldas_psu Exp $

// Warning class. Based on std::exception

//!author="Lee Samuel Finn"

#ifndef WARNING_HH
#define WARNING_HH

#include <stdexcept>

namespace std { 

  //: Exception class that carries partial, 
  //+ incomplete or suspect results from calculations
  template <class T>
  class warning : public runtime_error { 
  public: 
    //: Constructor
    explicit warning(const string& what_arg, const T& val) : runtime_error(what_arg), m_val(val) {} 
    //!param: const string& what_arg - describes the nature of the exception
    //!param: const T val - returned value

    //: Constructor
    explicit warning(const char* what_arg, const T& val) : runtime_error(what_arg), m_val(val) {}
    //!param: const char* what_arg - describes the nature of the exception
    //!param: const T val - the returned value

    void returns(T& val) const {val = m_val;}
    //!param: T& - the partial, incomplete or suspect results whose proper 
    //+ interpretation allows the program to (optionally) recover and continue 
    //+ execution. The type T may be atomic (e.g., int, double, complex<>, 
    //+ valarray<>) if a single object is being returned; alternatively, it 
    //+ might be a container (e.g., a map or a list) if several objects need 
    //+ to be returned. The lifetime of val is the lifetime of the warning class.

  private:
    
    //: m_val - value returned
    T m_val;
    
  }; 

} 

#endif
