#include "config.h"

#include <sstream>

#include "SignumFunction.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

//=======================================================================
// Signum Action
//=======================================================================

static SignumFunction staticSignumFunction;

SignumFunction::
SignumFunction() : Function( SignumFunction::GetName() )
{
}

void SignumFunction::Eval(CallChain* Chain, const CallChain::Params& Params,
			   const std::string& Ret) const
{
    using namespace datacondAPI;

    CallChain::Symbol* result = 0;
    CallChain::Symbol* obj = 0;

    int arg = 0;

//number of thresholds : 2, 1 or 0 ( 0 is equivalent to th1=th2=0 )
    int nth = 0;
// thresholds = 0. by default
    double th1 = 0.;
    double th2 = 0.;

    int n;
    double v, th;

    switch(Params.size())
    {

    case 3:
	arg = 3;
        nth = 2;
	obj = Chain->GetSymbol(Params[arg - 1]);
	if (const Scalar<double>* const p =
	    dynamic_cast<const Scalar<double>*>(obj))
	{
	    th2 = p->GetValue();
	}
	else
	{
	    std::ostringstream oss;

	    oss << "Illegal argument to " << GetName()
		<< " - argument #" << arg
		<< " must be a double";

	    throw std::invalid_argument(oss.str());
	}

	// Fall through

    case 2:
        arg = 2;

        obj = Chain->GetSymbol(Params[arg - 1]);
        if (const Scalar<double>* const p =
            dynamic_cast<const Scalar<double>*>(obj))
        {
            th1 = p->GetValue();
        }
        else
        {
            std::ostringstream oss;

            oss << "Illegal argument to " << GetName()
                << " - argument #" << arg
                << " must be a double";

            throw std::invalid_argument(oss.str());
        }

        if (nth == 0) 
        {
          nth = 1;
          th2 = th1;
        }
        else if ( th1 > th2 ) // exchange thresholds so that th1 < th2
        {
          th = th1;
          th1 = th2;
          th2 = th;
        }

        // Fall through

    case 1:
	arg = 1;
        if (nth == 0) nth = 1;

	obj = Chain->GetSymbol(Params[arg - 1]);

	if (const Sequence<float>* const p
	    = dynamic_cast<const Sequence<float>*>(obj))
	{
	    Sequence<float>* out = p->Clone();
            n = out->size();

            for (int i=0; i<n; i++)
            {
              v = (*out)[i];
              if ( v < float(th1) ) (*out)[i] = -1.;
              else if ( v > float(th2) ) (*out)[i] = 1.;
              else (*out)[i] = 0.;
            }

	    result = out;
	}
	else if (const Sequence<double>* const p
		 = dynamic_cast<const Sequence<double>*>(obj))
	{
	    Sequence<double>* out = p->Clone();
            n = out->size();

            for (int i=0; i<n; i++)
            {
              v = (*out)[i];
              if ( v < th1 ) (*out)[i] = -1.;
              else if ( v > th2 ) (*out)[i] = 1.;
              else (*out)[i] = 0.;
            }

	    result = out;
	}
	else
        {
	    std::ostringstream oss;

	    oss << "Illegal argument to " << GetName()
		<< " - argument #" << arg
		<< " must be a sequence of float or double";

	    throw std::invalid_argument(oss.str());
        }

	break;

    default:
      throw CallChain::BadArgumentCount("signum", 2, Params.size());
      break;
    }

    if (result == 0)
    {
      throw CallChain::NullResult( GetName() );
    }

    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& SignumFunction::
GetName(void) const
{
    static const std::string name("signum");
    return name;
}
