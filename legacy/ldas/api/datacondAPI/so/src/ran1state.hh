#ifndef _RAN1STATE_HH_
#define _RAN1STATE_HH_

#include <cmath>
#include <valarray>

#include "StateUDT.hh"

namespace datacondAPI
{
  class ran1state : public State
  {
  public:
    
    //: default constructor creates state with default seed
    ran1state();

    //: Construct with particular seed
    //!param: int - seed, must be negative
    //!exc: std::invalid_argument - seed >= 0 
    ran1state(int);

    //: Copy Constructor
    //:!param: const ranstate& - state to copy from
    ran1state(const ran1state&);
    
    ~ran1state() {};

    //: reset state with new seed
    //!param: unsigned long - seed, must be negative
    //!exc: std::out_of_range - if seed >= 0 
    void seed(int);

    //: Generate random number
    //!return: returns a random number
    float getran();

    //: Generate a sequence of random numbers
    //!param: std::valarray<float>& - array to be filled with numbers
    //!param: int - size of array (and quantity of generated numbers)
    void getran(std::valarray<float>&, int);

    ran1state* Clone() const;
    
    ILwd::LdasElement* ConvertToIlwd( const CallChain&,
				      udt::target_type Target) const;

  private:

    int idum;
    long iy;
    std::valarray<long> iv;

  };
}

#endif //_RAN1STATE_HH_
