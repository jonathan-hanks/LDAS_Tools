#include "config.h"

#include "StatsUMD.hh"

#include "general/types.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"

// destructor

datacondAPI::StatsMetaData::~StatsMetaData()
{
}

// accessors

unsigned int 
datacondAPI::StatsMetaData::GetSize() const
{
  return m_size;
}

double 
datacondAPI::StatsMetaData::GetMin() const
{
  return m_min;
}

double 
datacondAPI::StatsMetaData::GetMax() const
{
  return m_max;
}

double 
datacondAPI::StatsMetaData::GetRMS() const
{
  return m_rms;
}

std::string
datacondAPI::StatsMetaData::GetStatisticsOfName() const
{
  return m_statistics_of_name;
}

// mutators

void 
datacondAPI::StatsMetaData::SetSize(const unsigned int& size)
{
  m_size = size;
}

void 
datacondAPI::StatsMetaData::SetMin(const double& min)
{
  m_min = min;
}

void 
datacondAPI::StatsMetaData::SetMax(const double& max)
{
  m_max = max;
}

void 
datacondAPI::StatsMetaData::SetRMS(const double& rms)
{
  m_rms = rms;
}

void
datacondAPI::StatsMetaData::SetStatisticsOfName(const std::string& statistics_of_name)
{
  m_statistics_of_name = statistics_of_name;
}

// output

ILwd::LdasElement* 
datacondAPI::StatsMetaData::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  ILwd::LdasContainer* container = new ILwd::LdasContainer;
  try {
    switch(Target)
    {
    case datacondAPI::udt::TARGET_GENERIC:
      container->push_back( new ILwd::LdasArray<double>(m_size, "StatsSize"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( new ILwd::LdasArray<double>(m_min,  "StatsMin"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( new ILwd::LdasArray<double>(m_max,  "StatsMax"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( new ILwd::LdasArray<double>(m_rms,  "StatsRMS"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( new ILwd::LdasString(m_statistics_of_name,
						 "StatsStatisticsOfName"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      break;
    default:
      throw datacondAPI::udt::
	BadTargetConversion(Target,
			    "datacondAPI::CSDSpectrumUMD");
      
    }
  }
  catch( ... )
  {
    delete container;
    throw;
  }
  return container;
}

// constructors

datacondAPI::StatsMetaData::StatsMetaData() : 
  m_size(0), 
  m_min(0), 
  m_max(0), 
  m_rms(0),
  m_statistics_of_name("")
{
}

datacondAPI::StatsMetaData::StatsMetaData(const unsigned int& size, 
					  const double&       min,
					  const double&       max,
					  const double&       rms) : 
  m_size(size), 
  m_min(min), 
  m_max(max), 
  m_rms(rms),
  m_statistics_of_name("")
{
}

// copy constructor

datacondAPI::StatsMetaData::StatsMetaData(const StatsMetaData& r) :
  m_size(r.m_size), 
  m_min(r.m_min),
  m_max(r.m_max), 
  m_rms(r.m_rms),
  m_statistics_of_name(r.m_statistics_of_name)
{
}

/*
// copy assignment

const datacondAPI::StatsMetaData&
datacondAPI::StatsMetaData::operator=(const StatsMetaData& r)
{

  if (this != &r)
  {
    UDTMetaData::operator=(r);

    m_size = r.m_size;
    m_min  = r.m_min;
    m_max  = r.m_max;
    m_rms  = r.m_rms;
    m_statistics_of_name = r.statistics_of_name;
  }

  return *this;
}
*/
