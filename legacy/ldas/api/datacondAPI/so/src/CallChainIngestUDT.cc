/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "datacondAPI/config.h"

#include <cctype>

#include <algorithm>   
#include <sstream>
#include <string>
#include <memory>   

#include "general/Memory.hh"
#include "general/unordered_map.hh"

#include "ilwd/ilwdtoc.hh"
#include "ilwd/ldaselement.hh"

#include "ilwdfcs/FrDetector.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrRawData.hh"

#include "genericAPI/ChannelNameLexer.hh"
#include "genericAPI/swiglogging.h"

#include "CallChainPrivate.hh"
#include "DFTUDT.hh"
#include "FrequencySequenceUDT.hh"
#include "ILwdMetaDataHelper.hh"
#include "Ingester.hh"
#include "GapUMD.hh"
#include "TimeSeries.hh"
#include "TimeBoundedFreqSequenceUDT.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"

using namespace datacondAPI;
using General::GPSTime;
using namespace std;   

#if 0
#define AT(a) std::cerr << __FILE__ << " " << __LINE__ << " " << a << std::endl
#else
#define	AT(a)
#endif

#define USING(LM_X) \
  const ILwdMetaDataHelper::meta_data_type \
	LM_X(ILwdMetaDataHelper::LM_X)

USING(FREQUENCY_SHIFT);
USING(DATA);
USING(DELTA_TIME);
USING(PHASE);
USING(PROC_DATA);
USING(SAMPLE_RATE);
USING(TIME_OFFSET);
USING(TRANGE);
USING(TYPE);
USING(SUBTYPE);

#undef USING

struct Result_type {
  udt*		s_data;
  std::string	s_symbol_name;
  bool		s_history_set;

  Result_type()
    : s_data(0), s_symbol_name(""), s_history_set(false) { }
};

typedef std::vector<Result_type>	result_set_type;

struct time_series_metadata_type {
  GPSTime			s_start;	// Start time of segment
  REAL_8                        s_offset;       // Offset of segment
  REAL_8			s_sample_rate;	// Sample Rate
  REAL_8			s_freqbase;	// Frequency base
  REAL_4			s_phase;	// Phase
  REAL_8			s_delta;	// Delta Time
  const ILwd::LdasArrayBase*	s_data;         // Data array

  time_series_metadata_type()
    : s_start(),
      s_offset(0),
      s_sample_rate(0),
      s_freqbase(0),
      s_phase(0),
      s_delta(0),
      s_data(0)
  {
  }

  time_series_metadata_type( const GPSTime& Start,
                          REAL_8 Offset,
			  REAL_8 SampleRate,
			  REAL_8 freqBase,
			  REAL_4 Phase,
			  REAL_8 Delta,
			  const ILwd::LdasElement* Data )
    : s_start( Start ),
      s_offset( Offset ),
      s_sample_rate( SampleRate ),
      s_freqbase( freqBase ),
      s_phase( Phase ),
      s_delta( Delta ),
      s_data( dynamic_cast<const ILwd::LdasArrayBase*>(Data ) )
  {
  }

  time_series_metadata_type( const time_series_metadata_type& Source )
    : s_start( Source.s_start ),
      s_offset( Source.s_offset ),
      s_sample_rate( Source.s_sample_rate ),
      s_freqbase( Source.s_freqbase ),
      s_phase( Source.s_phase ),
      s_delta( Source.s_delta ),
      s_data( Source.s_data )
  {
  }
};

struct channel_metadata_type {
  datacondAPI::udt*			s_result;
  std::vector<time_series_metadata_type>	s_time_series;

  channel_metadata_type()
    : s_result( (datacondAPI::udt*)NULL )
  {
  }

  channel_metadata_type( const channel_metadata_type& Source )
    : s_result( Source.s_result ),
      s_time_series( Source.s_time_series )
  {
  }
};

static inline const ILwd::LdasContainer*
find_detector( const ILwd::LdasContainer& Container )
{
  return dynamic_cast< const ILwd::LdasContainer *>
    ( Container.find( "Container(Detector)", 2 ) );
}

static inline const ILwd::LdasContainer*
find_history( const ILwd::LdasContainer& Container )
{
  return dynamic_cast< const ILwd::LdasContainer *>
    ( Container.find( "history", 1 ) );
}

static void
add_frame_data( const ILwd::LdasContainer* DataGroup,
		std::string& Symbol,
		result_set_type& Results,
		CallChain::CallChainPrivate& Private,
		ILwd::LdasHistory& History );

static void
add_history( ILwd::LdasHistory& History, const ILwd::LdasContainer& Source );

template<class ILwd_,class TimeSeries_>
static void
copy_timeseries( datacondAPI::TimeSeries<TimeSeries_>& TimeSeries,
		 const std::vector<time_series_metadata_type>& Metadata );

template<class T>
static datacondAPI::TimeSeries<T>*
create_timeseries( const GPSTime& Start,
		   REAL_8 DeltaTime,
		   REAL_8 SampleRate );

static void
fill_timeseries( datacondAPI::udt* Series,
		 const std::vector<time_series_metadata_type>& Metadata );

//: Creates an object from a frame structure
static bool frame( const ILwdMetaDataHelper& ILwdHelper,
		   std::string& Symbol,
		   result_set_type& Results,
		   CallChain::CallChainPrivate& Private );

//: Creates a Frequency Series if ILwd provides sufficient information
static udt* frequency_sequence( const ILwdMetaDataHelper& ILwdHelper,
				CallChain::CallChainPrivate& Private );

//: Creates a TimeSeriesUDT if ILwd provides sufficient information
static udt* time_series( const ILwdMetaDataHelper& ILwdHelper,
			 CallChain::CallChainPrivate& Private );

static bool
time_series_group( const ILwdMetaDataHelper& ILwdHelper,
                   std::string& Symbol,
                   result_set_type& Results,
                   CallChain::CallChainPrivate& Private );

static bool
ingest_offset_time_series_group( const ILwd::LdasContainer& Container,
                                 const std::string& Symbol,
                                 result_set_type& Results );

static bool
ingest_time_series_group( const ILwdMetaDataHelper& ILwdHelper,
                          std::string& Symbol,
                          result_set_type& Results,
                          CallChain::CallChainPrivate& Private );

static udt* specialized_sequence( const ILwdMetaDataHelper& ILwdHelper );

namespace
{
  template<class T>
  T get_metadata( const ILwd::LdasElement& Element,
		  const std::string& Variable );
  template<class T>
  T format_metadata( const std::string& Metadata );
}

static bool is_a_time_series( const datacondAPI::ILwdMetaDataHelper& ILwdHelper,
			      std::vector<time_series_metadata_type>& Values );

static bool is_history_container( const ILwd::LdasElement* Element );

static double get_offset( const ILwdMetaDataHelper& ILwdHelper );

static double
get_time_span( const ILwdMetaDataHelper& ILwdHelper );

namespace
{
  template<>
  GPSTime get_metadata<GPSTime>( const ILwd::LdasElement& Element,
				 const std::string& Variable )
  {
    INT_4U sec = 0;
    INT_4U nanosec = 0;

    std::istringstream ss( Element.getMetadata<std::string>(Variable) );

    bool have_left_brace = false;
    bool fail = false;
    char c = '\0';

    // Allowed formats are:    secs
    //                        {secs}
    //                        {secs nanosecs}
    
    // Consume the opening brace
    ss >> c;
    fail = fail || ss.fail();
    if (!fail)
    {
      if (c == '{')
      {
	have_left_brace = true;
      }
      else
      {
	ss.putback(c);
      }
    }
    
    // Read the first number (must be present!)
    ss >> sec;
    fail = fail || ss.fail();
    
    if (!fail && have_left_brace)
    {
      // Read the second number (if present, otherwise default to zero)
      // Note we DON'T need to check if the extraction is ok, since there
      // may be no second number inside the braces
      ss >> nanosec;
      ss.clear(); // Don't care if we fail here, so clear the state
      
      // Read the right brace for parse checking - this MUST be present
      ss >> c;
      fail = ss.fail() || (c != '}');
    }
    
    // Now there must be NO characters left in the string!
    fail = fail || (ss >> c);
    
    if (fail)
    {
      std::ostringstream oss;
      
      oss << "Unable to convert metadata variable "
	  << Variable
	  << " with value of \""
	  << Element.getMetadata<std::string>(Variable)
	  << "\" to requested type"
	  << std::ends;

      throw CallChain::LogicError( oss.str() );
    }

    return GPSTime(sec, nanosec);
  }

  inline std::string
  getMetadataAsString( const ILwd::LdasElement& Element,
		       const std::string& Variable )
  {
    return Element.getMetadata<std::string>(Variable);
  }

  template<class T>
  T get_metadata( const ILwd::LdasElement& Element,
		  const std::string& Variable )
  {
    std::istringstream  ss( getMetadataAsString( Element, Variable ) );

    ss.clear();

    T	retval;
    ss >> retval;
    if ( ss.bad() )
    {
      std::ostringstream oss;

      oss << "Unable to convert metadata variable "
	  << Variable
	  << " with value of "
	  << getMetadataAsString( Element, Variable )
	  << " to requested type"
	  << std::ends;
      
      throw CallChain::LogicError( oss.str() );
    }
    return retval;
  }

  template<class T>
  T get_metadata( const std::string& Metadata )
  {
    std::istringstream  ss( Metadata );

    ss.clear();

    T	retval;
    ss >> retval;
    if ( ss.bad() )
    {
      std::ostringstream oss;

      oss << "Unable to convert metadata ("
	  << Metadata
	  << ") to requested type"
	  << std::ends;
      
      throw CallChain::LogicError( oss.str() );
    }
    return retval;
  }

  //---------------------------------------------------------------------
  // Template instantiation
  //---------------------------------------------------------------------
  template double get_metadata< double >( const ILwd::LdasElement& Element,
					  const std::string& Variable );
} // namespace - anonymous

bool Ingester::
ingest_udt( std::string& Symbol,
	    const ILwd::LdasElement& Element )
{
  using namespace ILwd;

  udt*		result((datacondAPI::udt*)NULL);
  result_set_type	results;
  
  AT("");
  std::string msg("Trying to ingest ilwd as udt");
  // GenericAPI::AddLogMessage(msg, GenericAPI::MSG_DEBUG);

  try {
    const LdasContainer&	con( dynamic_cast< const LdasContainer& >( Element ) );
    ILwdMetaDataHelper	ilwd_helper( con );

    //-----------------------------------------------------------------
    // History information
    //-----------------------------------------------------------------

    const ILwd::LdasContainer*	hc( find_history( con ) );

    if ( hc )
    {
      add_history( *(m_chain.GetPrivateData( ).GetHistory( )), *hc );
    }

    if ( is_history_container( &(ilwd_helper.GetContainer( ) ) ))
    {
      // This is the history container that was previously parsed
      return true;
    }

    //-----------------------------------------------------------------
    // FrDetector
    //-----------------------------------------------------------------

    const ILwd::LdasContainer*	dc( find_detector( con ) );

    if ( dc )
    {
      for ( ILwd::LdasContainer::const_iterator c = dc->begin();
	    c != dc->end();
	    c++ )
      {
	ILwd::TOC	helper( *(*c), ILwdFCS::FrDetector::TOCKeys );

	m_chain.GetPrivateData().AddDetector( helper );
      }
    }

    if ( detector( ilwd_helper ) )
    {
      return true;
    }

    //-----------------------------------------------------------------
    // Look for other objects
    //-----------------------------------------------------------------

    if ( ( frame( ilwd_helper, Symbol, results, m_chain.GetPrivateData() ) ) ||
	 ( result = frequency_sequence( ilwd_helper, m_chain.GetPrivateData() ) ) ||
	 ( time_series_group( ilwd_helper, Symbol, results,
			      m_chain.GetPrivateData() ) ) ||
	 ( result = time_series( ilwd_helper, m_chain.GetPrivateData() ) ) ||
	 ( result = specialized_sequence( ilwd_helper ) ) )
    {
      udt*	r = result;

      if ( ( ! result ) && ( results.size() > 0 ) )
      {
	r = results[0].s_data;
      }
      WhenMetaData*	when( dynamic_cast<WhenMetaData*>( r ) );
      if ( when )
      {
	m_chain.GetPrivateData().SetDetectorTime( *when );
      }
    }
  }
  catch( const std::bad_cast& e )
  {
  }
  AT("store_udt");
  if ( result )
  {
    AT("store_symbol: single result");
    if ( result->SizeOfHistory( ) == 0 )
    {
      result->AppendHistory( *m_chain.GetPrivateData().GetHistory( ) );
    }
    store_symbol( Symbol, result );
  }
  else if ( results.size() > 0 )
  {
    AT("store_symbol: multiple result");
    for ( result_set_type::const_iterator r = results.begin();
	  r != results.end();
	  r++ )
    {
      if ( ( (*r).s_data->SizeOfHistory( ) == 0 ) &&
	   ( (*r).s_history_set == false ) )
      {
	(*r).s_data->AppendHistory( *m_chain.GetPrivateData().GetHistory( ) );
      }
      store_symbol( (*r).s_symbol_name, (*r).s_data );
    }
  }
  else
  {
    AT("store_symbol: no results");
    return false;
  }
  AT(true);
  return true;
}

static void
add_frame_data( const ILwd::LdasContainer* DataGroup,
		std::string& Symbol,
		result_set_type& Results,
		CallChain::CallChainPrivate& Private,
		ILwd::LdasHistory& History )
{
  AT( "" );
  if ( DataGroup )
  {
    AT( "" );
    Result_type	result;
    
    AT( "" );
    for ( ILwd::LdasContainer::const_iterator c = DataGroup->begin();
	  c != DataGroup->end();
	  c++ )
    {
      AT( "" );
      if ( ! *c )
      {
	AT( "" );
	continue;
      }
      AT( "" );
      ILwdMetaDataHelper	helper( *(*c) );
      
      AT( "" );
      if ( ( result.s_data = frequency_sequence( helper, Private ) ) ||
	   ( result.s_data = time_series( helper, Private ) ) )
      {
	AT( "" );
	CallChain	cmd;
	
	result.s_data->AppendHistory( History );
	result.s_symbol_name = Symbol;
	result.s_history_set = true;
	if ( Symbol.length() > 0 )
	{
	  result.s_symbol_name += ":";
	}
	AT( "" );
	result.s_symbol_name += (*c)->getNameString();
	Results.push_back( result );
      }
    }
  }
}

static void
add_history( ILwd::LdasHistory& History, const ILwd::LdasContainer& Source )
{
  if ( is_history_container( &Source ) )
  {
    for ( ILwd::LdasContainer::const_iterator i = Source.begin();
	  i != Source.end();
	  i++ )
    {
      if ( is_history_container( *i ) )
      {
	// Flatten history containers
	add_history( History,
		     dynamic_cast<const ILwd::LdasContainer&>( *(*i) ) );
      }
      else
      {
	ILwd::LdasHistoryRecord	hr( dynamic_cast<const ILwd::LdasContainer*>
				    ( *i ) );
      
	History.AppendRecord( hr );
      }
    }
  }
}

typedef enum {
  TRANSFER_FUNCTION,
  UNKNOWN
} sequence_type;

typedef General::unordered_map<std::string, sequence_type> sequence_map_type;

const sequence_map_type&
mk_seq_map(void)
{
  static sequence_map_type	map;

  map["transfer_function"] = TRANSFER_FUNCTION;
  
  return map;
}

static const sequence_map_type&	seq_map = mk_seq_map();

udt*
specialized_sequence( const ILwdMetaDataHelper& ILwdHelper )
{
  AT("Looking for specialized sequence");
  udt*					result((datacondAPI::udt*)NULL);
  AT("");
  const ILwd::LdasContainer&		container( ILwdHelper.GetContainer() );
  AT("");
  sequence_map_type::const_iterator	seq = seq_map.end( );
  

  AT("Doing if");
  if ( ( container.getNameSize() == 3 ) &&
       ( container.getName(2) == "sequence" ) &&
       ( ( seq = seq_map.find( container.getName( 1 ) ) ) != seq_map.end() ) )
  {
    AT("");
    // It is now known that it is a container and the name has three fields
    // and it is a sequence of a known type
    AT("");
    switch( (*seq).second )
    {
    case TRANSFER_FUNCTION:
      {
	AT("TRANSFER_FUNCTION");
	switch( ILwdHelper.GetILwd( DATA )->getElementId() )
	{
	case ILwd::ID_REAL_8:
	  {
	    AT("ILwd::ID_REAL_8");
	    const ILwd::LdasArray<REAL_8>* data =
	      dynamic_cast<const ILwd::LdasArray<REAL_8>* >
	      ( ILwdHelper.GetILwd( DATA ) );
	    AT("Have ILwd::LdasArray");
	    if ( data->getNDim() > 1 )
	    {
	      AT("data->getNDim() > 1");
	      break;
	    }
	    AT("");
	    result =
	      new datacondAPI::Sequence<REAL_8>( data->getData(),
						 data->getDimension(0) );
	  }
	  break;
	default:
	  AT("");
	  break;
	}
	AT("");
      }
      break;
    default:
      AT("");
      break;
    }
  }
  if ( result )
  {
    AT("Have Results of a specialized se");
    std::string msg("Ingested specialized sequence: ");
    msg += container.getNameString();
    // GenericAPI::AddLogMessage(msg, GenericAPI::MSG_DEBUG);
    result->SetName( container.getNameString() );
  }
  AT("Finished with specialized sequence");
  return result;
}


#if CARRY_GEOMETRY_META_DATA
static void
add_geometry( udt& Result, const GeometryMetaData& Detectors )
try
{
  using namespace genericAPI;

  GeometryMetaData&		g( dynamic_cast<GeometryMetaData&>( Result ) );
  const ChannelNameLexer&	channel_name_list( Result.name() );

  for ( ChannelNameLexer::data_list_type::const_iterator ch =
	  channel_name_list.GetChannelNames().begin();
	ch != channel_name_list.GetChannelNames().end();
	ch++ )
  {
    // Lookup the channel prefix in list of geometry info
      
    try
    {
      g.AddDetector( Detectors, (*ch).first.substr(0,2) );
    }
    catch( ... )
    {
      //:TODO: Add log entry in tcl logs warning of this condition
      // Quietly ignore lookup failure
    }
  }
  g.AddUnmappedDetectors( Detectors );
}	
catch( ... )
{
  throw CallChain::LogicError( "Cannot add geometry for unsupported udt type" );
}
#endif /* CARRY_GEOMETRY_META_DATA */

template<class ILwd_,class TimeSeries_>
static void
copy_timeseries( datacondAPI::TimeSeries<TimeSeries_>& TimeSeries,
		 const std::vector<time_series_metadata_type>& Metadata )
{
  AT("");
  for ( std::vector<time_series_metadata_type>::const_iterator tmd =
	  Metadata.begin();
	tmd != Metadata.end();
	tmd++ )
  {
    AT("");
    GPSTime	start( (*tmd).s_start + (*tmd).s_offset );
    GPSTime	end( start + (*tmd).s_delta );

    //-------------------------------------------------------------------
    // Sanity checks on data
    //-------------------------------------------------------------------
    
    
    //-------------------------------------------------------------------
    // Make sure the start time does not happen before the start of
    //   the time series
    //-------------------------------------------------------------------
    if ( start < TimeSeries.GetStartTime() )
    {
      std::ostringstream oss;

      oss << "Start time of frame data ("
	  << ( start - GPSTime( ) )
	  << ") occurs before time series start time ("
	  << ( TimeSeries.GetStartTime( ) - GPSTime( ) )
	  << ")"
	;
      throw CallChain::LogicError( oss.str( ).c_str( ) );
    }
    //-------------------------------------------------------------------
    // Calculate the first point
    //-------------------------------------------------------------------
    INT_4U	offset( (INT_4U)(floor( ( start - TimeSeries.GetStartTime() )
					* TimeSeries.GetSampleRate() ) ) );
    const ILwd::LdasArray<ILwd_>*	data_ilwd
      ( dynamic_cast<const ILwd::LdasArray<ILwd_>*>( (*tmd).s_data ) );
    //-------------------------------------------------------------------
    // Make sure the End point is not beyond the size of the timeseries
    //-------------------------------------------------------------------
    
    if ( ( offset + data_ilwd->getDimension(0) ) > TimeSeries.size() )
    {
      std::ostringstream	msg;

      msg << "End time of frame data ("
	  << end.GetSeconds() << "." << end.GetNanoseconds()
	  << ") occurs after time series end time ("
	  << TimeSeries.GetEndTime().GetSeconds()
	  << "." << TimeSeries.GetEndTime().GetNanoseconds()
	  << ")";
      throw CallChain::LogicError( msg.str() );
    }

    //-------------------------------------------------------------------
    // Continue on since all looks good.
    //-------------------------------------------------------------------
    AT( " start: " << start << " end: " << end );
    TimeSeries.GapAppendDataRange( start, end );

    AT("");
    std::copy( data_ilwd->getData(),
	       data_ilwd->getData() + data_ilwd->getDimension(0),
	       &(TimeSeries[offset]) );
  }
}

template<class T>
static datacondAPI::TimeSeries<T>*
create_timeseries( const GPSTime& Start,
		   REAL_8 DeltaTime,
		   REAL_8 SampleRate )
{
  AT("create_timeseries");
  datacondAPI::TimeSeries<T>* retval( new datacondAPI::TimeSeries<T>() );
  AT("create_timeseries");
  retval->resize( static_cast< unsigned int >( ceil( DeltaTime * SampleRate ) ) );
  AT("create_timeseries");
  retval->SetSampleRate( SampleRate );
  AT("create_timeseries");
  retval->SetStartTime( Start );
  AT("create_timeseries");
  retval->GapReset( true );	// Flag Time series as all gap
  AT("create_timeseries");
  return retval;
}

static void
fill_timeseries( datacondAPI::udt* Series,
		 const std::vector<time_series_metadata_type>& Metadata )
{
  AT("begin: fill timeseries");
  if ( udt::IsA<TimeSeries<REAL_4> >( *Series ) )
  {
    AT("");
    TimeSeries<REAL_4>& ts( udt::Cast<TimeSeries<REAL_4> >( *Series ) );

    switch( Metadata.front().s_data->getElementId() )
    {
    case ILwd::ID_CHAR:
      AT("");
      copy_timeseries<CHAR>( ts, Metadata );
      break;
    case ILwd::ID_CHAR_U:
      AT("");
      copy_timeseries<CHAR_U>( ts, Metadata );
      break;
    case ILwd::ID_INT_2U:
      AT("");
      copy_timeseries<INT_2U>( ts, Metadata );
      break;
    case ILwd::ID_INT_2S:
      AT("");
      copy_timeseries<INT_2S>( ts, Metadata );
      break;
    case ILwd::ID_INT_4U:
      AT("");
      copy_timeseries<INT_4U>( ts, Metadata );
      break;
    case ILwd::ID_INT_4S:
      AT("");
      copy_timeseries<INT_4S>( ts, Metadata );
      break;
    case ILwd::ID_REAL_4:
      AT("");
      copy_timeseries<REAL_4>( ts, Metadata );
      break;
    default:
      // :TODO: throw BadIngestion
      AT("end: fill timeseries");
      return;
    }
  }
  else if ( udt::IsA<TimeSeries<REAL_8> >( *Series ) )
  {
    AT("");
    TimeSeries<REAL_8>& ts( udt::Cast<TimeSeries<REAL_8> >( *Series ) );

    switch( Metadata.front().s_data->getElementId() )
    {
    case ILwd::ID_INT_8U:
      AT("");
      copy_timeseries<INT_8U>( ts, Metadata );
      break;
    case ILwd::ID_INT_8S:
      AT("");
      copy_timeseries<INT_8S>( ts, Metadata );
      break;
    case ILwd::ID_REAL_8:
      AT("");
      copy_timeseries<REAL_8>( ts, Metadata );
      break;
    default:
      // :TODO: throw BadIngestion
      AT("end: fill timeseries");
      return;
    }
  }
  else if ( udt::IsA<TimeSeries<COMPLEX_8> >( *Series ) )
  {
    AT("");
    TimeSeries<COMPLEX_8>& ts( udt::Cast<TimeSeries<COMPLEX_8> >( *Series ) );

    switch( Metadata.front().s_data->getElementId() )
    {
    case ILwd::ID_COMPLEX_8:
      AT("");
      copy_timeseries<COMPLEX_8>( ts, Metadata );
      break;
    default:
      // :TODO: throw BadIngestion
      AT("end: fill timeseries");
      return;
    }
  }
  else if ( udt::IsA<TimeSeries<COMPLEX_16> >( *Series ) )
  {
    AT("");
    TimeSeries<COMPLEX_16>&
      ts( udt::Cast<TimeSeries<COMPLEX_16> >( *Series ) );

    switch( Metadata.front().s_data->getElementId() )
    {
    case ILwd::ID_COMPLEX_16:
      AT("");
      copy_timeseries<COMPLEX_16>( ts, Metadata );
      break;
    default:
      // :TODO: throw BadIngestion
      AT("end: fill timeseries");
      return;
    }
  }
  AT("end: fill timeseries");
}

bool
frame( const datacondAPI::ILwdMetaDataHelper& ILwdHelper,
       std::string& Symbol,
       result_set_type& Results,
       CallChain::CallChainPrivate& Private )
{
  using ILwdFCS::FrameH;
  using ILwdFCS::FrRawData;
  using ILwd::TOC;
  using ILwd::LdasContainer;

  AT("begin: frame");
  try
  {
    ILwd::LdasHistory	history;

    //-------------------------------------------------------------------
    // History information
    //-------------------------------------------------------------------

    const ILwd::LdasContainer*	hc( find_history( ILwdHelper.GetContainer( ) ) );

    if ( hc )
    {
      add_history( history, *hc );
    }

    //-------------------------------------------------------------------
    // FrDetector
    //-------------------------------------------------------------------

    AT("begin: frame::FrDetector");
    const ILwd::LdasContainer*	dc( find_detector( ILwdHelper.GetContainer( ) ) );

    AT("");
    if ( dc )
    {
      AT("");
      for ( ILwd::LdasContainer::const_iterator c = dc->begin();
	    c != dc->end();
	    c++ )
      {
	AT("");
	ILwd::TOC	helper( *(*c), ILwdFCS::FrDetector::TOCKeys );

	Private.AddDetector( helper, ILwdHelper );
      }
    }

    AT("end: fame::FrDetector");

    //-------------------------------------------------------------------
    // Use new TOC class
    //-------------------------------------------------------------------

    TOC	frame_toc( ILwdHelper.GetContainer(), FrameH::TOCKeys );

    //-------------------------------------------------------------------
    // FrProcData
    //-------------------------------------------------------------------

    AT("begin: frame::FrProcData");
    try
    {
      add_frame_data( &frame_toc.GetILwd<ILwd::LdasContainer>
		      ( FrameH::PROC_DATA ),
		      Symbol,
		      Results,
		      Private,
		      history );
    }
    catch ( const CallChain::LogicError& e )
    {
      AT( "" );
      // Must rethrow this error
      throw;
    }
    catch( ... )
    {
      AT( "" );
    }
    AT("end: frame: FrProcData");

    //-------------------------------------------------------------------
    // FrRawData
    //-------------------------------------------------------------------

    AT( "begin: frame:FrRawData" );
    try {
      AT( "" );
      LdasContainer& c = frame_toc.GetILwd<ILwd::LdasContainer>
	( FrameH::RAW_DATA );

      AT( "" );
      TOC	raw_data_toc( c, FrRawData::TOCKeys );
      
      try
      {
	AT( "" );
	add_frame_data( &raw_data_toc.GetILwd<ILwd::LdasContainer>
			( FrRawData::SERIAL_DATA ),
			Symbol,
			Results,
			Private,
			history );
	AT( "" );
      }
      catch ( const CallChain::LogicError& e )
      {
	AT( "" );
	// Must rethrow this error
	throw;
      }
      catch( ... )
      {
	AT( "" );
      }
      try
      {
	AT( "" );
	add_frame_data( &raw_data_toc.GetILwd<ILwd::LdasContainer>
			( FrRawData::ADC_DATA ),
			Symbol,
			Results,
			Private,
			history );
	AT( "" );
      }
      catch ( const CallChain::LogicError& e )
      {
	AT( "" );
	// Must rethrow this error
	throw;
      }
      catch( ... )
      {
	AT( "" );
      }
    }
    catch ( const CallChain::LogicError& e )
    {
      AT( "" );
      // Must rethrow this error
      throw;
    }
    catch( ... )
    {
      AT( "" );
      // Ignore if no raw data present
    }
    AT( "end: frame:FrRawData" );

  }
  catch ( const CallChain::LogicError& e )
  {
    AT("end: frame: FrProcData (catch: LogicError)");
    for ( result_set_type::const_iterator r = Results.begin();
	  r != Results.end();
	  r++ )
    {
      delete (*r).s_data;
    }
    Results.erase( Results.begin(), Results.end() );
    // Need to propigate this error up the chain.
    AT( "" );
    throw;
  }
  catch( ... )
  {
    AT("end: frame: FrProcData (catch: ...)");
    for ( result_set_type::const_iterator r = Results.begin();
	  r != Results.end();
	  r++ )
    {
      delete (*r).s_data;
    }
    Results.erase( Results.begin(), Results.end() );
    AT( "" );
    return false;
  }
  AT( "end: frame" );
  return ( Results.size() > 0 ) ? true : false;
}

//
//: Attempt to create a TimeBoundedFreqSequence<T>
//
// Provided for use by create_frequency_sequence.
//
// Returns a pointer to a TimeBoundedFreqSequence<T> allocated on the heap,
// otherwise 0 if unable to
//
template<class T>
datacondAPI::TimeBoundedFreqSequence<T>*
createTimeBoundedFreqSequence(const T* const data,
                              const size_t dataSize,
                              const double& f0,
                              const double& df,
                              const GPSTime& startTime,
                              const GPSTime& endTime)
{
  std::unique_ptr<TimeBoundedFreqSequence<T> > p(new TimeBoundedFreqSequence<T>());

  // Set Sequence data
  p->resize(dataSize);
  copy(data, data + dataSize, &(*p)[0]);

  // Set FrequencyMetaData
  p->SetFrequencyBase(f0);
  p->SetFrequencyDelta(df);

  // Set TimeBoundedFreqSequenceMetaData
  p->SetStartTime(startTime);
  p->SetEndTime(endTime);

  return p.release();
}

//: Attempt to create a WelchSpectrum of the requested type T
//
// Provided for use by create_frequency_sequence.
//
// :NOTE: Only real WelchSpectra are allowed, and
// we need to avoid attempting to instantiate complex WelchSpectra in
// the functions that create frequency series, otherwise
// we get a a linker error (ie. symbol not found). They are returned
// as CSDSpectra because this is the nearest ancestor class which
// is allowed to be complex.
//
// By default, all attempts to create a WelchSpectrum return 0, except
// for float and double.
template<class T>
datacondAPI::CSDSpectrum<T>*
createWelchSpectrum(const T* const data,
                    const size_t dataSize,
                    const double& freqBase,
                    const double& f0,
                    const double& df,
                    const GPSTime& startTime,
                    const GPSTime& endTime)
{
  return 0;
}

//: Attempt create a WelchSpectrum<float>
//
// Provided for use by create_frequency_sequence.
//
template<>
datacondAPI::CSDSpectrum<float>*
createWelchSpectrum<float>(const float* const data,
                           const size_t dataSize,
                           const double& freqBase,
                           const double& f0,
                           const double& df,
                           const GPSTime& startTime,
                           const GPSTime& endTime)
{
  std::unique_ptr<WelchSpectrum<float> > p(new WelchSpectrum<float>());

  // Set Sequence data
  p->resize(dataSize);
  copy(data, data + dataSize, &(*p)[0]);

  // Set WelchSpectrum meta-data
  p->SetBaseFrequency(freqBase);

  //:NOTE: several other things could be set here, if they were available
  // eg. source channel name, FFT length, overlap, window information.
  // These would need to be provided in the frame as auxiliary parameters.

  // Set FrequencyMetaData
  p->SetFrequencyBase(f0);
  p->SetFrequencyDelta(df);

  // Set TimeBoundedFreqSequenceMetaData
  p->SetStartTime(startTime);
  p->SetEndTime(endTime);

  return p.release();
}

//: Attempt create a WelchSpectrum<double>
//
// Provided for use by create_frequency_sequence.
//
template<>
datacondAPI::CSDSpectrum<double>*
createWelchSpectrum<double>(const double* const data,
                            const size_t dataSize,
                            const double& freqBase,
                            const double& f0,
                            const double& df,
                            const GPSTime& startTime,
                            const GPSTime& endTime)
{
  std::unique_ptr<WelchSpectrum<double> > p(new WelchSpectrum<double>());

  // Set Sequence data
  p->resize(dataSize);
  copy(data, data + dataSize, &(*p)[0]);

  // Set WelchSpectrum meta-data
  p->SetBaseFrequency(freqBase);

  //:NOTE: several other things could be set here, if they were available
  // eg. source channel name, FFT length, overlap, window information.
  // These would need to be provided in the frame as auxiliary parameters.

  // Set FrequencyMetaData
  p->SetFrequencyBase(f0);
  p->SetFrequencyDelta(df);

  // Set TimeBoundedFreqSequenceMetaData
  p->SetStartTime(startTime);
  p->SetEndTime(endTime);

  return p.release();;
}

//: Attempt create a WelchCSDSpectrum<T>
//
// Provided for use by create_frequency_sequence.
//
template<class T>
datacondAPI::WelchCSDSpectrum<T>*
createWelchCSDSpectrum(const T* const data,
                       const size_t dataSize,
                       const double& f0,
                       const double& df,
                       const GPSTime& startTime,
                       const GPSTime& endTime)
{
  std::unique_ptr<WelchCSDSpectrum<T> > p(new WelchCSDSpectrum<T>());

  //:NOTE: no freqBase for this class

  //:NOTE: several other things could be set here, if they were available
  // eg. source channel name, FFT length, overlap, window information.
  // These would need to be provided in the frame as auxiliary parameters.

  // Set Sequence data
  p->resize(dataSize);
  copy(data, data + dataSize, &(*p)[0]);

  // Set FrequencyMetaData
  p->SetFrequencyBase(f0);
  p->SetFrequencyDelta(df);

  // Set TimeBoundedFreqSequenceMetaData
  p->SetStartTime(startTime);
  p->SetEndTime(endTime);

  return p.release();
}

//: Attempt to create a DFT
//
// Note that only complex DFT's may be instantiated, so again
// we provide a general function that "fails" ie. returns a null
// pointer, and two specialisations that succeed for single and
// double precision complex DFT's
//
template<class T>
datacondAPI::DFT<T>*
createDFT(const T* data, const size_t dataSize)
{
  return 0;
}

template<>
datacondAPI::DFT<complex<float> >*
createDFT<complex<float> >(const complex<float>* const data,
                           const size_t dataSize)
{
  return new DFT<complex<float> >(data, dataSize);
}

template<>
datacondAPI::DFT<complex<double> >*
createDFT<complex<double> >(const complex<double>* const data,
                            const size_t dataSize)
{
  return new DFT<complex<double> >(data, dataSize);
}

//
//: Create a frequency sequence of the indicated type
//
// Returns a pointer to a TimeBoundedFreqSequence<T> created on the heap,
// of a type specified by subType
//
//!arg: subType - type of frequency sequence created
//!arg: elt - an LdasElement containing the data
//!arg: freqBase - frequency by which original time-series was mixed
//!arg: startTime - start time of interval that f-series is valid for
//!arg: endTime - end time of interval that f-series is valid for
//
//!return: TimeBoundedFreqSequence<T>* - Pointer to frequency sequence
//+allocated on the heap, or zero if unsuccessful
template<class T>
datacondAPI::Sequence<T>*
create_frequency_sequence(const ILwdFCS::FrProcData::subType_type& subType,
                          const ILwd::LdasElement& elt,
                          const REAL_8& freqBase,
                          const GPSTime& startTime,
                          const GPSTime& endTime)
{
  AT("Entering create_frequency_sequence");

  const ILwd::LdasArray<T>& arrayData =
    dynamic_cast<const ILwd::LdasArray<T>&>(elt);
  const T* const data = arrayData.getData();
  const size_t dataSize = arrayData.getDimension(0);
  const double f0 = arrayData.getStartX(0);
  const double df = arrayData.getDx(0);

  // All of the returned clasess are based on Sequence
  std::unique_ptr<Sequence<T> > p(0);

  switch(subType)
  {
  case ILwdFCS::FrProcData::UNKNOWN_SUB_TYPE:
  case ILwdFCS::FrProcData::AMPLITUDE_SPECTRAL_DENSITY:
  case ILwdFCS::FrProcData::COHERENCE:
  case ILwdFCS::FrProcData::TRANSFER_FUNCTION:
    p.reset(createTimeBoundedFreqSequence<T>(data,
                                             dataSize,
                                             f0,
                                             df,
                                             startTime,
                                             endTime));
    break;

  case ILwdFCS::FrProcData::DFT:
    p.reset(createDFT<T>(data, dataSize));
    break;

  case ILwdFCS::FrProcData::POWER_SPECTRAL_DENSITY:
    //:NOTE: returns 0 if T is a complex type
    p.reset(createWelchSpectrum<T>(data,
                                   dataSize,
                                   freqBase,
                                   f0,
                                   df,
                                   startTime,
                                   endTime));
    break;

  case ILwdFCS::FrProcData::CROSS_SPECTRAL_DENSITY:
    p.reset(createWelchCSDSpectrum<T>(data,
                                      dataSize,
                                      f0,
                                      df,
                                      startTime,
                                      endTime));
    break;

  default:
    p.reset(0);
    break;
  }

  AT("Leaving create_frequency_sequence");

  return p.release();
}

//
//: Attempt to ingest UDT as some sort of frequency series
//
//!return: udt* - pointer to UDT representation of frequency series,
//+ or zero if invalid
udt*
frequency_sequence( const datacondAPI::ILwdMetaDataHelper& ILwdHelper,
		    CallChain::CallChainPrivate& Private )
{
  AT((string("Entering frequency_sequence() for: ")
     + ILwdHelper.GetContainer().getNameString()));

  std::unique_ptr<udt> result(0);

  try
  {
    // If any of these fields aren't found an exception will be thrown
    // causing 0 to be returned

    const INT_2U type = ILwdHelper.Get<INT_2U>(TYPE);

    // Ensure type is frequency series
    if (type != ILwdFCS::FrProcData::FREQUENCY_SERIES)
    {
      return 0;
    }

    const INT_2U subTypeInt = ILwdHelper.Get<INT_2U>(SUBTYPE);
    const ILwdFCS::FrProcData::subType_type subType
      = static_cast<ILwdFCS::FrProcData::subType_type>(subTypeInt);
    const REAL_8 timeOffset = ILwdHelper.Get<REAL_8>(TIME_OFFSET);
    const REAL_8 tRange = ILwdHelper.Get<REAL_8>(TRANGE);
    
    //:NOTE: Frame spec has opposite for frequency shift than datacon
    const REAL_8 freqBase = -ILwdHelper.Get<REAL_8>(FREQUENCY_SHIFT);

    // Other fields like phase, fRange, BW etc are not used

    // Construct the time-span
    const GPSTime startTime
      = GPSTime(atoi( ILwdHelper.GetContainer().getName(3).c_str() ),
                atoi( ILwdHelper.GetContainer().getName(4).c_str() ) )
      + timeOffset;
    
    const GPSTime endTime = startTime + tRange;

    //
    // Construct appropriate UDT now that it is know all the pieces do
    // exist.
    //

    AT("");
    const ILwd::LdasContainer& data_container
      = dynamic_cast<const ILwd::LdasContainer&>(*ILwdHelper.GetILwd( DATA ));
    
    if (data_container.size() <= 0)
    {
      // Oops! something went badly wrong - return a zero
      return 0;
    }

    switch( data_container[0]->getElementId() )
    {
    case ILwd::ID_REAL_4:
      result.reset(create_frequency_sequence<REAL_4>(subType,
                                                     *data_container[0],
                                                     freqBase,
                                                     startTime,
                                                     endTime ) );
      break;

    case ILwd::ID_REAL_8:
      result.reset(create_frequency_sequence<REAL_8>(subType,
                                                     *data_container[0],
                                                     freqBase,
                                                     startTime,
                                                     endTime ) );
      break;

    case ILwd::ID_COMPLEX_8:
      result.reset(create_frequency_sequence<COMPLEX_8>(subType,
                                                        *data_container[0],
                                                        freqBase,
                                                        startTime,
                                                        endTime ) );
      break;

    case ILwd::ID_COMPLEX_16:
      result.reset(create_frequency_sequence<COMPLEX_16>(subType,
                                                         *data_container[0],
                                                         freqBase,
                                                         startTime,
                                                         endTime ) );
      break;

    default:
      // For anything else but these types return 0 ie. failure
      result.reset(0);
      break;
    }
    //-------------------------------------------------------------------
    // Assign the udt name
    //-------------------------------------------------------------------

    AT("frequency_sequence");
    result->SetName( ILwdHelper.GetContainer().getName( 0 ) );

  }
  catch ( const CallChain::LogicError& e )
  {
    AT("");
    // Need to propagate this error up the chain.
    throw;
  }
  catch( const std::exception& e )
  {
    // For debugging
    AT((string("Caught exception with frequency sequence: ") + e.what()));
  }
  catch( ... )
  {
    // For debugging
    AT("Caught unknown exception with frequency sequence");
  }

  return result.release();
}

//!param: const ILwd::LdasElement& Element - ILwd to ingest
//!return: datacondAPI::udt* - If ILwd specified enough information to
//+        generate a TimeSeries UDT, then a pointer to that UDT,
//+        NULL otherwise.
//!param: std::string& Symbol - How to reference this object in symbol table
datacondAPI::udt*
time_series( const ILwdMetaDataHelper& ILwdHelper,
	     CallChain::CallChainPrivate& Private )
try
{
  std::unique_ptr<udt>	result( (udt*)NULL );

  REAL_8		delta( -1.0 );
  GPSTime		start;

  // Try the old way to fill pieces

  AT( "time_series" );
  delta = get_time_span( ILwdHelper );
  
  AT( "time_series" );
  if ( delta > 0 )
  {
    AT( "time_series" );
    start = GPSTime( atoi( ILwdHelper.GetContainer().getName(3).c_str() ),
		     atoi( ILwdHelper.GetContainer().getName(4).c_str() ) );
    
    AT( "time_series" );
    start += get_offset( ILwdHelper );
  }

  AT("time_series");
  if ( delta <= 0 )
  {
    throw CallChain::LogicError( "Delta time is less than or equal to 0" );
  }

  //---------------------------------------------------------------------
  // Continue with normal processing
  //---------------------------------------------------------------------

  AT("time_series");
  std::vector<time_series_metadata_type>	values;

  if ( ! is_a_time_series( ILwdHelper, values ) )
  {
    AT("time_series");
    //:TODO: Throw malformed_ingestion
    //:TRICKY: just get us out of here...NOW!
    goto end;
  }

  //---------------------------------------------------------------------
  // Ingested all the pieces. Now it is time to construct a time series
  //---------------------------------------------------------------------
  // Create space for the array.
  AT("time_series");
  switch( values.front().s_data->getElementId() )
  {
  case ILwd::ID_CHAR:
  case ILwd::ID_CHAR_U:
  case ILwd::ID_INT_2U:
  case ILwd::ID_INT_2S:
  case ILwd::ID_INT_4U:
  case ILwd::ID_INT_4S:
  case ILwd::ID_REAL_4:
    AT("time_series");
    result.reset (create_timeseries<REAL_4>( start, delta,
					     values.front().s_sample_rate ) );
    dynamic_cast< TimeSeries<REAL_4>* >
      ( result.get( ) )->SetBaseFrequency( values.front().s_freqbase );
    dynamic_cast< TimeSeries<REAL_4>* >
      ( result.get( ) )->SetPhase( values.front().s_phase );
    break;
  case ILwd::ID_INT_8U:
  case ILwd::ID_INT_8S:
  case ILwd::ID_REAL_8:
    AT("time_series");
    result.reset( create_timeseries<REAL_8>( start, delta,
					     values.front().s_sample_rate ) );
    dynamic_cast< TimeSeries<REAL_8>* >
      ( result.get( ) )->SetBaseFrequency( values.front().s_freqbase );
    dynamic_cast< TimeSeries<REAL_8>* >
      ( result.get( ) )->SetPhase( values.front().s_phase );
    break;
  case ILwd::ID_COMPLEX_8:
    AT("time_series");
    result.reset( create_timeseries<COMPLEX_8>( start, delta,
						values.front().s_sample_rate ) );
    dynamic_cast< TimeSeries<COMPLEX_8>* >
      ( result.get( ) )->SetBaseFrequency( values.front().s_freqbase );
    dynamic_cast< TimeSeries<COMPLEX_8>* >
      ( result.get( ) )->SetPhase( values.front().s_phase );
    break;
  case ILwd::ID_COMPLEX_16:
    AT("time_series");
    result.reset( create_timeseries<COMPLEX_16>( start, delta,
						 values.front().s_sample_rate ) );
    dynamic_cast< TimeSeries<COMPLEX_16>* >
      ( result.get( ) )->SetBaseFrequency( values.front().s_freqbase );
    dynamic_cast< TimeSeries<COMPLEX_16>* >
      ( result.get( ) )->SetPhase( values.front().s_phase );
    break;
  default:
    //:TODO: throw malformed_ingestion
    AT("time_series");
    goto end;
  }
  //---------------------------------------------------------------------
  // Assign the udt name
  //---------------------------------------------------------------------

  AT("time_series");
  result->SetName( ILwdHelper.GetContainer().getName
		   ( ILwdHelper.GetContainer().getNameSize() - 6 ) );

  //-------------------------------------------------------------------
  // Add appropriate geometry information
  //-------------------------------------------------------------------

  // Private.MapChannelToDetector( result->name() );

  //---------------------------------------------------------------------
  // Time to fill the time series with the pieces that are known.
  //---------------------------------------------------------------------

  AT("time_series");
  fill_timeseries( result.get(), values );

  //---------------------------------------------------------------------
  // Make sure the gaps are zero filled
  //---------------------------------------------------------------------
  {
    AT("time_series");
    GapFill gfz(GapFill::ZERO);
    udt* out( result.get() );
      
    gfz.apply( out, *( result.get() ) );
  }

 end:
  AT("time_series");
  return result.release();
}
catch( const CallChain::LogicError& e )
{
  throw;
}
catch( ... )
{
  return (datacondAPI::udt*)NULL;
}

//!param: const ILwd::LdasElement& Element - ILwd to ingest
//!return: datacondAPI::udt* - If ILwd specified enough information to
//+        generate a TimeSeries UDT, then a pointer to that UDT,
//+        NULL otherwise.
//!param: std::string& Symbol - How to reference this object in symbol table
bool
time_series_group( const ILwdMetaDataHelper& ILwdHelper,
		   std::string& Symbol,
		   result_set_type& Results,
		   CallChain::CallChainPrivate& Private )
{
  AT("Entering time_series_group()");

  //
  // The old time_series_group() function has been replaced by a new function
  // which decides whether or not to use the original time_series_group
  // (now called ingest_time_series_group), or a new version of
  // time_series_group which can handle data with non-zero offsets.
  //
  // Hopefully, when data with no offsets is read in, the old behaviour will
  // be replicated exactly. This is done so that the original ingestion
  // routines don't become any more broken than they already were.
  //
  bool ingested = false;

  // First see if it can be done as data with a non-zero offset (which
  // might happen for eg. resampled data)
  ingested = ingest_offset_time_series_group(ILwdHelper.GetContainer(),
                                             Symbol, Results);

  // Then try it as data with no offset (this is the old behaviour)
  //:NOTE: this is the old behaviour, which only works when the frame
  // group contains nothing but time-series. Need to add new code
  // to read more general frame groups that contain frequency
  // series data or other non-time-series data.
  if (!ingested)
  {
    ingested = ingest_time_series_group(ILwdHelper, Symbol,
                                        Results, Private);
  }

  AT("Leaving time_series_group()");

  // No other possibilities to try - return whatever actually happened
  return ingested;
}

//
//: Attempt to ingest a frame group which has a non-zero offset
//
// This function examines the data in Container to try to determine
// if it represents a frame group in which one or more channels has
// a non-zero offset from the start of the frame. If it does, and no
// errors are found, the data in Container is converted to a set of
// TimeSeries UDTs and returned in Results, and the function returns
// a value of "true" to indicate that the data was successfully ingested.
//
// If the data is valid but no non-zero offsets are found, the fuction
// returns an empty Results set and "false" as a return value.
//
// If the data is invalid, an exception is thrown.
//
// :KLUDGE: The whole function is a big kludge to temporarily allow us
// to read downsampled reduced frames
//
//!param: Container - data to be ingested. Must represent a frame group
//!param: Symbol - the base symbol name
//!param: Results - the set of UDTs resulting from the ingestion
//
//!exc: CallChain::LogicError - Thrown if Container is empty, or has
//+ a delta_t <= 0
//
bool
ingest_offset_time_series_group(const ILwd::LdasContainer& Container,
                                const std::string& Symbol,
                                result_set_type& Results)
{
  using namespace std;

  AT("Looking for: time series group with offset");

  //
  // First determine (as well as possible) if this data came from an
  // frame group with a non-zero offset.
  //
  // The constraints I make are:
  //
  //   - the data must be a frame group ie. it must have a start_time
  //   and a delta_t
  //
  //   - the frame group must not be empty
  //
  //   - the frame group must contain no gaps, hence there must be
  //   only one segment of data associated with each channel name
  //
  //   - the start-time of each channel must coincide with with the start-time
  //   of the frame group
  //
  //   - at least one channel must have non-zero offset
  //

  if (Container.size() <= 0)
  {
    throw CallChain::LogicError("Container has no data");
  }

  // Storage for frame group meta-data
  GPSTime start_time;
  REAL_8 delta_t = 0;

  // Check if it's really a frame group ie. has the right meta-data.
  // Only way to check is to try it and catch exception if it fails...
  
  try {
    start_time = Container.getMetadata<GPSTime>("start_time");
    delta_t = Container.getMetadata<REAL_8>("delta_t");
  }
  catch(...)
  {
    // One of the meta-data was not present - this is not a frame group
    return false;
  }

  if (delta_t <= 0)
  {
    throw CallChain::LogicError("Frame group has delta_t <= 0");
  }

  AT("");

  // Put a try-catch block around here - if any exceptions that get out,
  // regard it as a failure to ingest and return false
  try
  {

    // Initially, assume all offsets are zero
    bool all_offsets_zero = true;
    General::unordered_map<string, channel_metadata_type> channels;
    
    for (ILwd::LdasContainer::const_iterator c = Container.begin();
         c != Container.end();
         ++c)
    {
      ILwdMetaDataHelper helper = *(*c);
      const string name
        = helper.GetContainer().getName(helper.GetContainer().getNameSize() - 6);
      channel_metadata_type& ci = channels[name];
    
      // :NOTE: this function returns true if helper refers to time-series
      // data, and also populates the data into ci
      if (!is_a_time_series(helper, ci.s_time_series))
      {
        // If the first element of the container isn't a time-series,
        // just return false so we can try the next possibility
        if (c == Container.begin())
        {
          return false;
        }
        else
        {
          AT("");
          // Something went wrong - the first element was a time-series but
          // some subsequent element wasn't - inconsistent data
          throw CallChain::BadIngestion("not all data is TimeSeries data");
        }
      }
    
      // Check that there is only one segment in the channel meta-data
      if (ci.s_time_series.size() > 1)
      {
        // There are gaps, we can't ingest this way
        return false;
      }

      // Check that the start-time is the same as the frame group start-time
      if (ci.s_time_series[0].s_start != start_time)
      {
        return false;
      }

      // See if the offset is non-zero
      if (ci.s_time_series[0].s_offset != 0)
      {
        // Good - at least one offset is non-zero
        all_offsets_zero = false;
      }
    }

    // All offsets are zero - return and try ingesting the old way
    if (all_offsets_zero)
    {
      return false;
    }

    //
    // All is well with the data - now create the results
    //
    Results.resize(0);

    for (General::unordered_map<string, channel_metadata_type>::iterator
	   c = channels.begin();
         c != channels.end();
         ++c)
    {
      channel_metadata_type& ch = (*c).second;

      // There is only one time-series component to ingest
      time_series_metadata_type& ts = ch.s_time_series[0];
      const size_t size = ts.s_data->getDimension(0);

      AT("");
      switch(ts.s_data->getElementId())
      {
      case ILwd::ID_INT_2S:
        {
          AT("");
          const ILwd::LdasArray<INT_2S>& data
            = dynamic_cast<const ILwd::LdasArray<INT_2S>&>(*ts.s_data);
          TimeSeries<REAL_4>* const p = new TimeSeries<REAL_4>();

          p->resize(size);
          copy(data.getData(), data.getData() + size, &(*p)[0]);

          p->SetStartTime(ts.s_start + ts.s_offset);
          p->SetSampleRate(ts.s_sample_rate);

          ch.s_result = p;
        }
        break;

      case ILwd::ID_REAL_4:
        {
          AT("");
          const ILwd::LdasArray<REAL_4>& data
            = dynamic_cast<const ILwd::LdasArray<REAL_4>&>(*ts.s_data);
          TimeSeries<REAL_4>* const p = new TimeSeries<REAL_4>();

          p->resize(size);
          copy(data.getData(), data.getData() + size, &(*p)[0]);

          p->SetStartTime(ts.s_start + ts.s_offset);
          p->SetSampleRate(ts.s_sample_rate);

          ch.s_result = p;
        }
        break;

      case ILwd::ID_REAL_8:
        {
          AT("");
          const ILwd::LdasArray<REAL_8>& data
            = dynamic_cast<const ILwd::LdasArray<REAL_8>&>(*ts.s_data);
          TimeSeries<REAL_8>* const p = new TimeSeries<REAL_8>();

          p->resize(size);
          copy(data.getData(), data.getData() + size, &(*p)[0]);

          p->SetStartTime(ts.s_start + ts.s_offset);
          p->SetSampleRate(ts.s_sample_rate);

          ch.s_result = p;
        }
        break;

      default:
        //:TODO: throw malformed_ingestion
        AT("");
        throw CallChain::BadIngestion("Unable to create TimeSeries "
                                      "for this data type");
        break;
      }

      // Now set name meta-data
      AT("");
      ch.s_result->SetName((*c).first);
    
#if CARRY_GEOMETRY_META_DATA
      // Add appropriate geometry information
      add_geometry(*(ch.s_result), Detectors);
#endif // CARRY_GEOMETRY_META_DATA

      AT("");

      // Create the symbol name that this object will be known as,
      // using a dummy object so it is easy to manipulate the name.
      ILwd::LdasContainer dc(Container.front()->getNameString());
      
      ostringstream sec;
      ostringstream nanosec;
      string name(Symbol);
    
      if (name.length() > 0)
      {
        name += ":";
      }
      name += (*c).first;
    
      sec << start_time.GetSeconds();
      nanosec << start_time.GetNanoseconds();
    
      dc.setName(0, name, true);
      dc.setName(3, sec.str(), true);
      dc.setName(4, nanosec.str(), true);
    
      Result_type r;
    
      r.s_data = ch.s_result;
      r.s_symbol_name = dc.getNameString();
      r.s_history_set = false;
    
      Results.push_back(r);
    } // end for

  } // end try
  catch (...)
  {
    // Failed to ingest as time-series group with offset - clean up
    // and return false
    
    // Try and clean up memory
    for (std::vector<Result_type>::iterator iter = Results.begin();
         iter != Results.end();
         ++iter)
    {
      delete iter->s_data;
    }

    Results.resize(0);

    return false;
  }

  AT("");

  // Successfully ingested
  return true;
}

//!param: const ILwd::LdasElement& Element - ILwd to ingest
//!return: datacondAPI::udt* - If ILwd specified enough information to
//+        generate a TimeSeries UDT, then a pointer to that UDT,
//+        NULL otherwise.
//!param: std::string& Symbol - How to reference this object in symbol table
bool
ingest_time_series_group(const ILwdMetaDataHelper& ILwdHelper,
                         std::string& Symbol,
                         result_set_type& Results,
                         CallChain::CallChainPrivate& Private )
try
{
  //
  // :NOTE: This function replicates the behaviour of the old
  // time_series_group() function. The new time_series_group() has hacks
  // to try and determine if the data has non-zero offset.
  // If no offset is present been done, it
  // uses ingest_time_series_group() so as to try and replicate the
  // old behaviour.
  //
  Results.resize( 0 );

  AT("Looking for: time series group");
  GPSTime	start( get_metadata<GPSTime>( ILwdHelper.GetContainer(),
					      "start_time" ) );
  REAL_8	delta = get_metadata<REAL_8>( ILwdHelper.GetContainer(),
					      "delta_t" );

  AT("");
  if ( delta <= 0 )
  {
    throw CallChain::LogicError( "Delta time is less than or equal to 0" );
  }

  //---------------------------------------------------------------------
  // Make sure there is some data
  //---------------------------------------------------------------------

  AT("");
  if ( ILwdHelper.GetContainer().size() == 0 )
  {
    throw CallChain::LogicError( "Timeseries contains no data" );
  }

  //---------------------------------------------------------------------
  // Continue with normal processing
  //---------------------------------------------------------------------

  AT("");
  General::unordered_map<std::string, channel_metadata_type>	channels;
    
  AT("");
  for ( ILwd::LdasContainer::const_iterator c =
	  ILwdHelper.GetContainer().begin();
	c != ILwdHelper.GetContainer().end();
	c++ )
  {
    datacondAPI::ILwdMetaDataHelper	helper( *(*c) );
    std::string				name( helper.GetContainer().getName
					      ( helper.GetContainer().
						getNameSize() - 6 ) );
    channel_metadata_type&			ci( channels[ name ] );
    
    if ( ! is_a_time_series( helper, ci.s_time_series ) )
    {
      if ( c == ILwdHelper.GetContainer().begin() )
      {
	return false;
      }
      else
      {
	AT("");
	//:TODO: Throw malformed_ingestion
	//:TRICKY: just get us out of here...NOW!
	throw CallChain::BadIngestion( "not all data is timeseries data" );
      }
    }
  }

  for ( General::unordered_map<std::string, channel_metadata_type>::iterator c =
	  channels.begin();
	c != channels.end();
	c++ )
  {
    channel_metadata_type&				ch( (*c).second );
    std::vector<time_series_metadata_type>&	ts( ch.s_time_series );

    //-------------------------------------------------------------------
    // Ingested all the pieces. Now it is time to construct a time series
    //-------------------------------------------------------------------
    // Create space for the array.
    AT("");
    switch( ts.front().s_data->getElementId() )
    {
    case ILwd::ID_CHAR:
    case ILwd::ID_CHAR_U:
    case ILwd::ID_INT_2U:
    case ILwd::ID_INT_2S:
    case ILwd::ID_INT_4U:
    case ILwd::ID_INT_4S:
    case ILwd::ID_REAL_4:
      AT("");
      ch.s_result = create_timeseries<REAL_4>( start, delta,
					       ts.front().s_sample_rate );
      dynamic_cast< TimeSeries<REAL_4>* >
	( ch.s_result )->SetBaseFrequency( ts.front().s_freqbase );
      dynamic_cast< TimeSeries<REAL_4>* >
	( ch.s_result )->SetPhase( ts.front().s_phase );
      break;
    case ILwd::ID_INT_8U:
    case ILwd::ID_INT_8S:
    case ILwd::ID_REAL_8:
      AT("");
      ch.s_result = create_timeseries<REAL_8>( start, delta,
					       ts.front().s_sample_rate );
      dynamic_cast< TimeSeries<REAL_8>* >
	( ch.s_result )->SetBaseFrequency( ts.front().s_freqbase );
      dynamic_cast< TimeSeries<REAL_8>* >
	( ch.s_result )->SetPhase( ts.front().s_phase );
      break;
    case ILwd::ID_COMPLEX_8:
      AT("");
      ch.s_result = create_timeseries<COMPLEX_8>( start, delta,
						  ts.front().s_sample_rate );
      dynamic_cast< TimeSeries<COMPLEX_8>* >
	( ch.s_result )->SetBaseFrequency( ts.front().s_freqbase );
      dynamic_cast< TimeSeries<COMPLEX_8>* >
	( ch.s_result )->SetPhase( ts.front().s_phase );
      break;
    case ILwd::ID_COMPLEX_16:
      AT("");
      ch.s_result = create_timeseries<COMPLEX_16>( start, delta,
						   ts.front().s_sample_rate );
      dynamic_cast< TimeSeries<COMPLEX_16>* >
	( ch.s_result )->SetBaseFrequency( ts.front().s_freqbase );
      dynamic_cast< TimeSeries<COMPLEX_16>* >
	( ch.s_result )->SetPhase( ts.front().s_phase );
      break;
    default:
      //:TODO: throw malformed_ingestion
      AT("");
      throw CallChain::BadIngestion( "Unable to create timeseries for data type" );
    }
  }

  try
  {
    for ( General::unordered_map<std::string, channel_metadata_type>::iterator c =
	    channels.begin();
	  c != channels.end();
	  c++ )
    {
      channel_metadata_type&			ch( (*c).second );
      std::vector<time_series_metadata_type>&	ts( ch.s_time_series );

      //-----------------------------------------------------------------
      // Assign the udt name
      //-----------------------------------------------------------------
      AT("");
      ch.s_result->SetName( (*c).first );
    
#if CARRY_GEOMETRY_META_DATA
      //-----------------------------------------------------------------
      // Add appropriate geometry information
      //-----------------------------------------------------------------

      add_geometry( *(ch.s_result), Detectors );
#endif	/* CARRY_GEOMETRY_META_DATA */

      //-----------------------------------------------------------------
      // Time to fill the time series with the pieces that are known.
      //-----------------------------------------------------------------
      AT("");
      fill_timeseries( ch.s_result, ts );

      //-----------------------------------------------------------------
      // Make sure the gaps are zero filled
      //-----------------------------------------------------------------

      AT("");
      GapFill gfz(GapFill::ZERO);
      
      gfz.apply( ch.s_result, *(ch.s_result) );

      //-----------------------------------------------------------------
      // Create the symbol name that this object will be known as
      //-----------------------------------------------------------------

      AT("");
      {
	// :TRICKY: Create dummy object so it is easy to manipulate the
	// :TRICKY:   name
	ILwd::LdasContainer dc( ILwdHelper.GetContainer().front()
				->getNameString() );

	GPSTime	t0( get_metadata<GPSTime>( ILwdHelper.GetContainer(),
					   "start_time" ) );

	std::ostringstream	sec;
	std::ostringstream	nanosec;
	std::string		name( Symbol );

	if ( name.length() > 0 )
	{
	  name += ":";
	}
	name += (*c).first;

	sec << t0.GetSeconds();
	nanosec << t0.GetNanoseconds();

	dc.setName( 0, name, true );
	dc.setName( 3, sec.str(), true );
	dc.setName( 4, nanosec.str(), true );

	Result_type	r;

	r.s_data = ch.s_result;
	r.s_symbol_name = dc.getNameString();
	r.s_history_set = false;
	
	Results.push_back( r );
      }
    }
  }
  catch( ... )
  {
    Results.resize( 0 );
    for ( General::unordered_map<std::string, channel_metadata_type>::iterator c =
	    channels.begin();
	  c != channels.end();
	  c++ )
    {
      channel_metadata_type&			ch( (*c).second );

      if ( ch.s_result )
      {
	delete ch.s_result;
      }
    }
    throw;
  }

  AT("");

  return true;
}
catch( const CallChain::LogicError& e )
{
  Results.resize( 0 );
  throw;
}
catch( ... )
{
  Results.resize( 0 );
  return false;
}

static bool
is_a_time_series( const datacondAPI::ILwdMetaDataHelper& ILwdHelper,
		  std::vector<time_series_metadata_type>& Values )
{
  AT("Entering is_a_time_series()");

  const ILwd::LdasContainer&	c( ILwdHelper.GetContainer() );

  const ILwd::LdasContainer* time_series_data_container = 0;

  AT( "" );
  try
  {
    AT( "" );
    // Try to use the new frame stucture
    ILwdFCS::FrProcData proc( &( ILwdHelper.GetContainer( ) ) );

    if ( proc.GetType( ) != ILwdFCS::FrProcData::TIME_SERIES )
    {
      AT( "" );
      throw std::bad_cast( );
    }

    //-------------------------------------------------------------------
    // Yes, this is a timeseries object.
    //-------------------------------------------------------------------

    // For a time-series, *always* use the span, never the tRange
    // contained in the FrProc
    const REAL_8 tRange = proc.GetSpan();

    //:NOTE: Frame spec uses opposite sign convention to datacon
    const REAL_8 freqBase = -proc.GetFShift();
    const REAL_8 phase = -proc.GetPhase();

    //-------------------------------------------------------------------
    // Now transfer the data from the ilwd
    // to the udt.
    //-------------------------------------------------------------------

    AT( " start: " << proc.GetStartTime( ) << " timeOffset: " << proc.GetTimeOffset( ) << " dt: " << tRange );
    Values.push_back( time_series_metadata_type
		      ( proc.GetStartTime( ),
                        proc.GetTimeOffset( ),
			proc.GetSampleRate( ),
			freqBase,
			phase,
			tRange,
			proc.RefDataContainer( )[0]
			)
		      );
  }
  catch( ... )
  {
    //-------------------------------------------------------------------
    // It better have some data, or else it's not a time-series.
    // Unfortunately, exception-handling is the only way to detect it.
    //-------------------------------------------------------------------
    AT( "" );
    try {
      time_series_data_container
	= dynamic_cast<const ILwd::LdasContainer*>(ILwdHelper.GetILwd(DATA));
    }
    catch(...)
    {
      return false;
    }
    //-------------------------------------------------------------------
    //-------------------------------------------------------------------
    if ( ( time_series_data_container ) &&
	 ( time_series_data_container->size() == 1 ) )
    {
      GPSTime	start( atoi( c.getName(3).c_str() ),
		       atoi( c.getName(4).c_str() ) );
      REAL_8	tRange( get_time_span( ILwdHelper ) );

      //:NOTE: Frame spec uses opposite sign convention to datacon
      // for phase and frequency shift
      AT( " start: " << start << " timeOffset: " << get_offset( ILwdHelper ) << " dt: " << tRange );
      Values.push_back( time_series_metadata_type
			( start,
			  get_offset( ILwdHelper ),
			  ILwdHelper.Get<REAL_8>( SAMPLE_RATE ),
			  -ILwdHelper.Get<REAL_8>( FREQUENCY_SHIFT ),
			  -ILwdHelper.Get<REAL_4>( PHASE, 0.0 ),
			  tRange,
			  (*time_series_data_container)[0] ) );
    }
    else
    {
      return false;
    }
  }

  AT( " Size: " << Values.size( ) );
  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  if ( Values.size() > 1 )
  {
    //-------------------------------------------------------------------
    // Verify that all segments are sampled at the same properties
    //-------------------------------------------------------------------
    if ( ( Values.front().s_sample_rate != Values.back().s_sample_rate ) ||
	 ( Values.front().s_freqbase != Values.back().s_freqbase ) ||
	 ( Values.front().s_phase != Values.back().s_phase ) ||
	 ( Values.front().s_data->getElementId() !=
	   Values.back().s_data->getElementId() )
#if 0
	 // :TODO: These tests should be added
	 ||

	 ( Values.front().s_data->getDx( 0 ) !=
	   Values.back().s_data->getDx( 0 ) ) ||
	 ( Values.front().s_data->getUnits() !=
	   Values.back().s_data->getUnits() ) ||
	 ( Values.front().s_data->getDataValueUnit() !=
	   Values.back().s_data->getDataValueUnit() )
#endif
	   )
    {
      //:TODO: throw malformed_ingestion;
      return false;
    }
  }
  else
  {
    if ( Values.front().s_sample_rate <= 0 )
    {
      // Time series should never have 0 or fewer samples per second
      return false;
    }
  }

  AT("Leaving is_a_time_series()");

  return true;
}

static bool is_history_container( const ILwd::LdasElement* Element )
{
  return ( ( Element->getName( 2 ) == "Container(History)" ) &&
	   ( Element->getName( 3 ) == "Frame" ) &&
	   ( dynamic_cast< const ILwd::LdasContainer*>( Element ) )
	   );
}

static double
get_offset( const ILwdMetaDataHelper& ILwdHelper )
{
  using namespace ILwd;

  try
  {
    return ILwdHelper.Get<REAL_8>( TIME_OFFSET );
  }
  catch( ... )
  {
  }
  double	retval = 0.0;

  const ILwd::LdasArray<INT_4U>*
    gps_array( ILwdHelper.GetILwd<LdasArray<INT_4U> >( TIME_OFFSET, false ) );

  if ( gps_array &&
       ( gps_array->getNDim() == 1) &&
       ( gps_array->getDimension( 0 ) > 0 ) &&
       ( gps_array->getDimension( 0 ) <= 2 ) )
  {
    GPSTime	zero;
    
    INT_4U	seconds( gps_array->getData()[0] );
    INT_4U	nanoseconds( 0 );
    
    if ( gps_array->getDimension( 0 ) == 2 )
    {
      nanoseconds = gps_array->getData()[1];
    }
    
    GPSTime	offset( seconds, nanoseconds );
    retval = offset - zero;
  }
  return retval;
}
    
static double
get_time_span( const ILwdMetaDataHelper& ILwdHelper )
{
  try
  {
    return ILwdHelper.Get<REAL_8>( DELTA_TIME );
  }
  catch( ... )
  {
  }

  const ILwd::LdasContainer* dc( ILwdHelper.GetILwd<ILwd::LdasContainer>( DATA ) );
  const ILwd::LdasArrayBase* d( (const ILwd::LdasArrayBase*)NULL );
  if ( dc )
  {
    d = dynamic_cast<const ILwd::LdasArrayBase*>((*dc)[0]);
  }
  else
  {
    d = ILwdHelper.GetILwd<ILwd::LdasArrayBase>( DATA );
  }

  double ret;
  try
  {
    ret = ( 1.0 / ILwdHelper.Get<REAL_8>( SAMPLE_RATE ) );
  }
  catch( ... )
  {
    ret = d->getDx( 0 );
  }
  ret *= d->getDimension( 0 );
  return ret;
}
