/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef BASICFUNCTIONS_HH
#define BASICFUNCTIONS_HH

#include "CallChain.hh"

class ClearFunction : public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  ClearFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

class ValueFunction : public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  ValueFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

class RealFunction : public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  RealFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

class ImagFunction : public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  ImagFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

class AbsFunction : public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  AbsFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

class ArgFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  ArgFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

class ConjFunction : public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  ConjFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

class ReverseFunction : public CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  ReverseFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

void sqrt_apply(datacondAPI::udt*& out, const datacondAPI::udt& in);

#endif // BASICFUNCTIONS_HH
