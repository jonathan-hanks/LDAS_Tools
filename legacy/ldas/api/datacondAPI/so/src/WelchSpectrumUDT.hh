/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef WELCH_SPECTRUM_UDT_HH
#define WELCH_SPECTRUM_UDT_HH
// $Id: WelchSpectrumUDT.hh,v 1.18 2006/02/16 16:54:58 emaros Exp $

#include <vector>
#include <string>

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "CSDSpectrumUDT.hh"
#include "WelchSpectrumUMD.hh"

namespace DB
{
  class Table;
}

namespace datacondAPI
{
  
  template<class T> class WelchSpectrum :
    public CSDSpectrum<T>,
    public WelchSpectrumUMD
  {
    
  public:
    
    // constructors
    
    //: Default constructor
    WelchSpectrum();
    
    //: Copy constructor
    //!param: const WelchSpectrum& - object to copy from
    WelchSpectrum(const WelchSpectrum& value);
    
    // destructor
    //: Destructor
    virtual ~WelchSpectrum();
    
    // udt
    
    //: Duplicate *this, allocating storage on heap
    virtual WelchSpectrum< T >* Clone() const;
    
#if HAVE_LDAS_PACKAGE_ILWD
    //: Convert this object to an ILWD
    //!param: Chain -
    //!param: Target - flag indicating API this ILWD is destined for 
    //+ (e.g., metadataAPI or wrapperAPI)
    //!return: ILwd::LdasContainer* - ILWD representation of *this,
    //+ appropriate for transmission to API associated with Target
    virtual ILwd::LdasContainer*
    ConvertToIlwd( const CallChain& Chain,
		   udt::target_type Target = udt::TARGET_GENERIC ) const;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

    //: Create WelchSpectrum from database information.

    static void CreateFromDBTable( const DB::Table& Table,
				   std::vector<datacondAPI::udt*>& Spectrum,
				   std::vector<std::string>& Names,
				   const bool IgnoreTableName = false );
    
    //: Validate if the table supplied is sufficient to create a
    //: WelchSpectrum

    static bool ValidateDBTable( const DB::Table& Table );

  }; // class WelchSpectrum
  
} // namespace datacondAPI

#endif // WELCH_SPECTRUM_UDT_HH
