/* -*- mode: c++ -*- */
#ifndef SIGNUMFUNCTION_HH
#define SIGNUMFUNCTION_HH

#include "CallChain.hh"

//=======================================================================
// Signum Action 
//=======================================================================

class SignumFunction: CallChain::Function
{
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  SignumFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
                    const CallChain::Params& Params,
                    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};


#endif  /* SIGNUMFUNCTION_HH */

