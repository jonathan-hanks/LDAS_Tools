// $Id: MixFunction.cc,v 1.22 2007/01/24 17:51:27 emaros Exp $
//
// "mixer" action
//
// [y = ]mix(p, f, x[, z])
// [y = ]mix(x, z)
//
// p: double (or int)
//    phase (updated double on return)
// f: double (or int)
//    frequency
// x: valarray<T> where T is float, double, complex<float>, complex<double>
//    input sequence
// y: valarray<T> where T is complex<float>, complex<double>
//    output sequence

#include "datacondAPI/config.h"

#include <complex>
#include <valarray>

#include "MixFunction.hh"

#include "Mixer.hh"
#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

//=============================================================================
// MixFunction
//=============================================================================

//-----------------------------------------------------------------------------
// MixFunction::GetName
//-----------------------------------------------------------------------------

const std::string& MixFunction::GetName(void) const
{
  
  //-------------------------------------------------------------------------
  // The GetName function returns the string "mix", the name of the action as
  // presented to the user. The name is stored as a static variable and
  // returned as a const reference for performance reasons.
  //-------------------------------------------------------------------------
  
  static std::string name("mix");
  
  return name;
  
}

//-----------------------------------------------------------------------------
// MixFunction::MixFunction
//-----------------------------------------------------------------------------

MixFunction::MixFunction() : Function( MixFunction::GetName() )
{
  
  //-------------------------------------------------------------------------
  // The sole task of the MixFunction constructor is to pass the action name,
  // defined by GetName, to the Function constructor.
  //-------------------------------------------------------------------------

}

//-----------------------------------------------------------------------------
// We construct a static, global instance of a LinfiltFunction so that the
// constructor is called when the API is loaded. The constructor passes the
// action name on to the Function constructor, thus registering it as a valid
// action.
//-----------------------------------------------------------------------------

static MixFunction _MixFunction;

//-----------------------------------------------------------------------------
// MixFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "mix". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//-----------------------------------------------------------------------------

void MixFunction::Eval(CallChain* Chain,
                       const CallChain::Params& Params,
                       const std::string& Ret) const
{
  
  //-------------------------------------------------------------------------
  // A variable for later use.
  //-------------------------------------------------------------------------
  
  CallChain::Symbol* out = 0;

  
  //-------------------------------------------------------------------------
  // We use a switch statement to handle the various number of Params we are
  // given. The mix action has three mandatory arguments so we handle case 3
  // and throw an exception for any other number of arguments.
  //-------------------------------------------------------------------------

  switch (Params.size())
  {
  case 4:
  case 3:
    
    //---------------------------------------------------------------------
    // Case 3 arguments.
    //---------------------------------------------------------------------
        
    {
      //-----------------------------------------------------------------
      // We extract pointers to the Symbols in the CallChain associated
      // with the symbol names for the first three parameters.
      //-----------------------------------------------------------------
            
      const datacondAPI::udt& phase(*(Chain->GetSymbol(Params[0])));
      const datacondAPI::udt& frequency(*(Chain->GetSymbol(Params[1])));
      const datacondAPI::udt& in(*(Chain->GetSymbol(Params[2])));

      datacondAPI::Mixer mixer(datacondAPI::MixerState(phase, frequency));
      mixer.apply(out, in);
      	
      Chain->AddSymbol(Params[0], new datacondAPI::Scalar<double>(mixer.getState().GetPhase()));
    
      //------------------------------------------------------------------
      // If 4 arguments, set the fourth to the Mixer State
      //------------------------------------------------------------------
  
      if(Params.size() == 4)
      {
        Chain->AddSymbol(Params[3], new datacondAPI::MixerState(mixer.getState()));
      }
    }
    
    //---------------------------------------------------------------------
    // End case 3.
    //---------------------------------------------------------------------
    
    break;

  case 2:

    //---------------------------------------------------------------------
    // Case 2 arguments.
    //---------------------------------------------------------------------
        
    {
      //-----------------------------------------------------------------
      // We extract pointers to the Symbols in the CallChain associated
      // with the symbol names for the first three parameters.
      //-----------------------------------------------------------------
            
      const datacondAPI::udt& in(*(Chain->GetSymbol(Params[0])));
      const datacondAPI::udt& state(*(Chain->GetSymbol(Params[1])));

      // avoid strange compiler error...

      datacondAPI::MixerState temp(state);

      datacondAPI::Mixer mixer(temp);
      mixer.apply(out, in);

      Chain->AddSymbol(Params[1], new datacondAPI::MixerState(mixer.getState()));
    
    }
    
    //---------------------------------------------------------------------
    // End case 2.
    //---------------------------------------------------------------------

    break;
        
  default:
    
    //-----------------------------------------------------------------
    // Unsupported number of arguments. We throw an exception
    //-----------------------------------------------------------------
    
    throw CallChain::BadArgumentCount( GetName(), "2-4", Params.size() );
    
    //-----------------------------------------------------------------
    // End case default - unreachable code!
    //-----------------------------------------------------------------
          
    break;
  }
  
  if (!out)
  {

    //-----------------------------------------------------------------
    // A null pointer was returned by apply. We throw an exception.
    //-----------------------------------------------------------------
    
    throw CallChain::NullResult( GetName() );

  }
  
  //---------------------------------------------------------------------
  // Set the symbol name Ret with the calculated result,
  // replacing any previous value of Ret.
  //---------------------------------------------------------------------
  
  Chain->ResetOrAddSymbol( Ret, out );
  
}


//-----------------------------------------------------------------------
// This method validates accessability of the parameters
//-----------------------------------------------------------------------
void MixFunction::
ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		    const CallChain::Params& Params ) const
{
  switch( Params.size( ) )
  {
  case 4:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    //-------------------------------------------------------------------
    // Validate output parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 3 ], PARAM_WRITE );
    break;
  default:
    Function::ValidateParameters( SymbolTable, Params );
    break;
  }
}

