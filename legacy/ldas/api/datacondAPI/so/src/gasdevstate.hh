#ifndef _GASDEVSTATE_HH_
#define _GASDEVSTATE_HH_

#include <cmath>
#include <valarray>

#include "ran4state.hh"
#include "StateUDT.hh"

namespace datacondAPI
{
  class gasdevstate : public State
  {
  public:

    //: default constructor creates state with default seed
    gasdevstate();

    //: Construct with particular seed
    //!param: int - seed, must be negative
    //!exc: std::invalid_argument - seed >= 0
    gasdevstate(const int& s);

    //: Copy Constructor
    //:!param: const ranstate& - state to copy from
    gasdevstate(const gasdevstate&);

    ~gasdevstate() {};

    //: Reset State with new seed
    //!param: int sd - seed, must be negative
    //!exc: std::invalid_argument - seed >=0
    void seed(int sd);

    //: Generate random number
    //!return: returns a random number
    float getgasdev();

    //: Generate a sequence of random numbers
    //!param: std::valarray<type>& - array to be filled with numbers
    //!param: int - size of array (and quantity of generated numbers)
    template<typename type> void getgasdev(std::valarray<type>&, int);

    gasdevstate* Clone() const;

    ILwd::LdasElement* ConvertToIlwd( const CallChain&,
                                      udt::target_type Target) const;

  private:

    ran4state s;
    int iset;
    float gset;

  };
}

#endif //_GASDEVSTATE_HH_

