#include <complex>

#include "general/types.hh"

#include "mime/spectrum.hh"

#include "UtilSpectrum.hh"

namespace datacondAPI
{
  template <>
  std::string
  pGetMimeType<float>( void )
  {
    Mime::Spectrum	s;
#ifdef WORDS_BIGENDIAN
    return s.GetSpectrumMimeType(Mime::Spectrum::REAL_BIGENDIAN);
#else /* WORDS_BIGENDIAN */
    return s.GetSpectrumMimeType(Mime::Spectrum::REAL_LITTLEENDIAN);
#endif /* WORDS_BIGENDIAN */
  }

  template <>
  std::string
  pGetMimeType<double>( void )
  {
    Mime::Spectrum	s;
#ifdef WORDS_BIGENDIAN
    return s.GetSpectrumMimeType(Mime::Spectrum::REAL_BIGENDIAN);
#else /* WORDS_BIGENDIAN */
    return s.GetSpectrumMimeType(Mime::Spectrum::REAL_LITTLEENDIAN);
#endif /* WORDS_BIGENDIAN */
  }

  template <>
  std::string
  pGetMimeType< std::complex<float> >( void )
  {
    Mime::Spectrum	s;
#ifdef WORDS_BIGENDIAN
    return s.GetSpectrumMimeType(Mime::Spectrum::COMPLEX_BIGENDIAN);
#else /* WORDS_BIGENDIAN */
    return s.GetSpectrumMimeType(Mime::Spectrum::COMPLEX_LITTLEENDIAN);
#endif /* WORDS_BIGENDIAN */
  }

  template <>
  std::string
  pGetMimeType< std::complex<double> >( void )
  {
    Mime::Spectrum	s;
#ifdef WORDS_BIGENDIAN
    return s.GetSpectrumMimeType(Mime::Spectrum::COMPLEX_BIGENDIAN);
#else /* WORDS_BIGENDIAN */
    return s.GetSpectrumMimeType(Mime::Spectrum::COMPLEX_LITTLEENDIAN);
#endif /* WORDS_BIGENDIAN */
  }
} // namespace - datacondAPI

