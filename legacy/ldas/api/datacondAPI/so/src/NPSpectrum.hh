/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef NPSPECTRUM_HH
#define NPSPECTRUM_HH

#include <valarray>

#include <general/unexpected_exception.hh>
#include <general/unimplemented_error.hh>

#include "UDT.hh"
#include "ScalarUDT.hh"

// Forward declaration of ILwd elements
namespace ILwd
{
  class LdasElement;
}

//: A NPSpectrum represents a Non-Parametric Spectrum 
//+ estimate. It is a multi-dimensional data array, indexed
//+ by non-negative integers. Associated with the indices 
//+ in each dimension are unit-ed values.

namespace datacondAPI {

  template <class T>
  class NPSpectrum : public udt
  {
  public:
    
    //: Accept default default, copy constructors
    
    //: default constructor
    NPSpectrum();

    // Specialty constructors go here
    
    //: Construct from dimensions, data, axis values
    //!param: dims - the dimension bounds of the axes
    //!param: data - the spectrum data in column-major order
    //!param: axesLB - value associated with index 0 on each axis
    //!param: axesUB - value associated with index upper bound on each axis
    //!exc: length_error - 
    //!exc: out_of_range -
    //!exc: bad_exception - 
    NPSpectrum(const std::valarray<size_t>& dims, 
	       const std::valarray<T>& data, 
	       const std::valarray<double>& axesLB,
	       const std::valarray<double>& axesUB)
      throw (std::length_error,
	     std::out_of_range,
	     General::unexpected_exception);
    
    //: destructor
    ~NPSpectrum();
    
    //: Accessors
    
    //: number of dimensions
    //!return: number of dimensions of this spectrum
    size_t getNDims(void) const;
    
    //: dimension bounds
    //!param: dims - the upper index bound on each dimension
    void getDims(std::valarray<size_t>& dims) const;
    
    //: Get the values associated with upper and lower index bounds
    //+ on the different axes. 
    //+ The relation between indices and values is (required to be) 
    //+ described entirely by the index and the value associated with 
    //+ the index at its upper and lower bounds. (The relationship
    //+ itself is determined by the user.) 
    //!param: min - value associated with index 0
    //!param: max - value associated with upper bound index
    //!param: k    - dimension selector
    //!exc: length_error - k out-of-bounds
    //!exc: bad_exception - unexpected exception
    void getAxis(Scalar<double>& min,
		 Scalar<double>& max,
		 size_t k) const
      throw (std::length_error,
	     General::unexpected_exception);
    
    void getAxis(double& min,
		 double& max,
		 size_t k) const
      throw (std::length_error,
	     General::unexpected_exception);
    
    //: Get the values associated with upper and lower index bounds
    //+ on the different axes. 
    //+ The relation between indices and values is (required to be) 
    //+ described entirely by the index and the value associated with 
    //+ the index at its upper and lower bounds. (The relationship
    //+ itself is determined by the user.) 
    //!param: min - values associated with index 0
    //!param: max - values associated with upper bound index
    void getAxes(std::valarray<double>& min,
		 std::valarray<double>& max) const;
    
    //: return the spectrum value at a point
    //!param: ndx - the point as an integer n-tuple
    //!return: T - the value
    //!exc: out_of_range -
    //!exc: bad_exception -
    const T operator[] (const std::valarray<size_t>& ndx) const
      throw (std::out_of_range,
	     General::unexpected_exception);
    
    //: Select a "sub-spectrum": i.e., return a NPSpectrum object whose 
    //+ axes and data are a subset of the axes and data. To transform 
    //+ in place see slice(std::valarray<size_t>&)
    //!param: spec - the result
    //!param: mask - sub-spectrum specification. 
    //+ The length of mask is equal to the number of dimensions. 
    //+ If mask[k] >= 0, then the corresponding index is held constant 
    //+ with the value mask[k]; otherwise, the corresponding index runs over
    //+ the allowed values for that dimension. 
    //!exc: invalid_argument -
    //!exc: bad_exception -
    //!exc: length_error -
    //!exc: out_of_range -
    //!exc: bad_alloc -
    //!exc: bad_exception - unexpected exception 
    void slice(udt*& spec, const std::valarray<int>& mask) const
      throw (std::invalid_argument,
	     std::length_error,
	     std::out_of_range,
	     std::bad_alloc,
	     General::unexpected_exception);
    
    //: Get the spectrum data
    //!param: data - the spectrum data, stored column-major
    void getData(std::valarray<T>& data) const;
    
    //: Get the subset of spectrum data
    //!param: data - subset of spectrum data, stored in column-major order
    //!param: mask - data specification.
    //+ The length of mask is equal to the number of dimensions. 
    //+ If mask[k] >= 0, then the corresponding index is held constant 
    //+ with the value mask[k]; otherwise, the corresponding index runs over
    //+ the allowed values for that dimension. 
    //!exc: invalid_argument -
    //!exc: length_error -
    //!exc: bad_exception -
    void getData(std::valarray<T>& data, 
		 const std::valarray<int>& mask) const
      throw (std::invalid_argument,
	     std::length_error,
	     General::unexpected_exception);
    
    //: Mutators
    
    //: (re)set the dimension bounds. Any data currently held is lost.
    //!param: dims - (new) upper index bounds on each dimension. 
    //!param: data - (new) spectrum data in column-major order
    //!param: axesLB - (new) values associated with index 0 on each axis
    //!param: axesUB - (new) values associated with index upper bound 
    //+ on each axis
    //!exc: out_of_range -
    //!exc: invalid_argument -
    //!exc: length_error -
    //!exc: bad_exception -
    void resize(const std::valarray<size_t>& dims,
		const std::valarray<T>& data,
		const std::valarray<double>& axesLB,
		const std::valarray<double>& axesUB)
      throw (std::out_of_range,
	     std::invalid_argument,
	     std::length_error,
	     General::unexpected_exception);
    
    //: (re)set the dimension bounds. Any data currently held is lost.
    //!param: dims - (new) upper index bounds on each dimension. 
    //!param: data - (new) spectrum data in column-major order
    //+ on each axis
    //!exc: out_of_range -
    //!exc: bad_exception -
    void resize(const std::valarray<size_t>& dims)
      throw (std::out_of_range,
	     General::unexpected_exception);
    
    //: Set the values associated with upper and lower index bounds
    //+ on the different axes. 
    //+ The relation between indices and values is (required to be) 
    //+ described entirely by the index and the value associated with 
    //+ the index at its upper and lower bounds. (The relationship
    //+ itself is determined by the user.) 
    //!param: min - value associated with index 0
    //!param: max - value associated with upper bound index
    //!param: k    - dimension selector
    //!exc: out_of_range -
    //!exc: bad_exception -
    void setAxis(double min, double max, int k)
      throw (std::out_of_range,
	     General::unexpected_exception);
    
    //: Set the values associated with upper and lower index bounds
    //+ on the different axes. 
    //+ The relation between indices and values is (required to be) 
    //+ described entirely by the index and the value associated with 
    //+ the index at its upper and lower bounds. (The relationship
    //+ itself is determined by the user.) 
    //!param: min - value associated with index 0
    //!param: max - value associated with upper bound index
    void setAxes(const std::valarray<double>& min,
		 const std::valarray<double>& max);
    
    //: Transform to represent a "sub-spectrum": i.e., transform by 
    //+ projecting out certain dimensions of *this
    //!param: mask - axes to be masked from the result are marked with 0; 
    //+ The length of mask is equal to the number of dimensions. 
    //+ If mask[k] >= 0, then the corresponding index is held constant 
    //+ with the value mask[k]; otherwise, the corresponding index runs over
    //+ the allowed values for that dimension. 
    void slice(const std::valarray<int>& mask);
    
    //: Set the spectrum data
    //!param: data - the spectrum data, stored column-major
    //!exc: out_of_range -
    //!exc: invalid_argument -
    //!exc: bad_exception -
    void setData(const std::valarray<T>& data)
      throw (std::out_of_range,
	     std::invalid_argument,
	     General::unexpected_exception);
    
    //: Set a subset of spectrum data
    //!param: data - subset of spectrum data, corresponding to mask, 
    //+ stored in column-major order
    //!param: mask - identify the slice to assign the data to. 
    //+ The length of mask is equal to the number of dimensions. 
    //+ If mask[k] >= 0, then the corresponding index is held constant 
    //+ with the value mask[k]; otherwise, the corresponding index runs over
    //+ the allowed values for that dimension. 
    //!exc: unimplemented_error -
    //!exc: bad_exception -
    void setData(const std::valarray<T>& data, 
		 const std::valarray<int>& mask)
      throw (General::unimplemented_error,
	     General::unexpected_exception);

    // UDT classes

    //: duplicate *this
    //!return: NPSpectrum<T>* - pointer to a copy of *this, allocated 
    //+ on the heap
    //!exc: bad_alloc -
    //!exc: bad_exception -
    NPSpectrum<T>* Clone(void) const
      throw (std::bad_alloc,
	     General::unexpected_exception);

    //: Convert the data to an appropriate ilwd type. 
    //+ If no conversion is possible, return a null pointer
    //!param: const CallChain& Chain - CallChain in which UDT was created.
    //!param: target_type Target - Type of conversion to be performed.
    //!return: ILwd::LdasElement* - The converted type or NULL if no
    //+        conversion possible.
    //!exc: unimplemented_error -
    //!exc: bad_exception -
    ILwd::LdasElement*
    ConvertToIlwd(const CallChain& Chain,
		  target_type Target = TARGET_GENERIC) const
      throw (General::unimplemented_error,
	     General::unexpected_exception);
    
  private:
    
    //: the spectrum data
    std::valarray<T> m_data;
    
    //: integer bounds on the axis dimensions
    std::valarray<size_t> m_dims;
    
    //: values for upper, lower index bounds
    std::valarray<double> m_axesLB;
    std::valarray<double> m_axesUB;

    //: verify dims are positive and return required storage
    //!exc: out_of_range -
    //!exc: bad_exception -
    size_t checkDims() const
      throw (std::out_of_range,
	     General::unexpected_exception);

    //: get gslice and dimensions that corresponds to a projection
    //!param: gs   - gslice corresponding to projection
    //!param: dims - dimensions of projection
    //!param: mask - identify the slice to assign the data to. 
    //+ The length of mask is equal to the number of dimensions. 
    //+ If mask[k] >= 0, then the corresponding index is held constant 
    //+ with the value mask[k]; otherwise, the corresponding index runs over
    //+ the allowed values for that dimension. 
    //!exc: invalid_argument -
    //!exc: bad_exception -
    void getGSlice(std::gslice& gs, 
		 std::valarray<size_t>& dims, 
		 const std::valarray<int>& mask) const
      throw (std::invalid_argument,
	     General::unexpected_exception);
  };
  
}


#endif // NPSPECTRUMUDT_HH
