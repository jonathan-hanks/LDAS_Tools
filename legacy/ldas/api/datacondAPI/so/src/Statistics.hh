#ifndef STATISTICS_HH
#define STATISTICS_HH

#include <complex>

namespace datacondAPI {

  // Forward declarations
  class Stats;
  template<class T> class Sequence;

  //: Trait definitions for Statistics
  //
  // These traits are provided so that certain scalar values such as sum,
  // min, max etc can be returned as double-precision values, either real
  // or complex. Generally, we promote single-precision to double-precision
  // but leave the rest alone.
  //
  // Default for most things is real double
  template<class TIn>
  class StatisticsTraits {
  public:
    typedef double TOut;
  };

  // Specialisations for complex<float> and complex<double>
  template<>
  class StatisticsTraits<std::complex<float> > {
  public:
    typedef std::complex<double> TOut;
  };

  template<>
  class StatisticsTraits<std::complex<double> > {
  public:
    typedef std::complex<double> TOut;
  };

  //
  //: A class for calculating summary statistics for a real array of data
  // 
  // The statistics currently implemented are: size, min, max, mean, rms, 
  // variance, skewness, and kurtosis.  In addition, there is an all method, 
  // which returns all of the above statistics in a structure allStats whose 
  // fields are the names of the statistics.
  //
  class Statistics {

  public:
    //
    //: Return size of data
    //
    //!param: data - input data
    //!return: unsigned int - length of data
    //!exc: std::invalid_argument - thrown if no input data
    //
    template<class T>
    unsigned int
    size(const Sequence<T>& data) const;

    //
    //: Return sum of the data
    //
    //!param: data - input data
    //!return: T - sum of the data
    //!exc: std::invalid_argument - thrown if no input data
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    sum(const Sequence<T>& data) const;

    //
    //: Return min value
    //
    //!param: data - input data
    //!return: double - min value
    //!exc: std::invalid_argument - thrown if no input data
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    min( const Sequence<T>& data ) const;

    //
    //: Return max value
    //
    //!param: data - input data
    //!return: double - max value
    //!exc: std::invalid_argument - thrown if no input data
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    max(const Sequence<T>& data) const;

    //
    //: Calculate sample mean
    //
    //!param: data - input data
    //!return: double - mean value
    //!exc: std::invalid_argument - thrown if no input data
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    mean( const Sequence<T>& data ) const;

    //
    //: Calculate root-mean-square (rms) value
    //
    // This is just the square root of the mean of the squared data values.
    //
    //!param: data - input data
    //!return: double - rms value
    //!exc: std::invalid_argument - thrown if no input data
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    rms( const Sequence<T>& data ) const;

    //
    //: Calculate sample variance 
    //
    // NOTE: the normalization chosen here is appropriate when we interpret 
    // the data as being a sample drawn from a larger population.  
    // The sample variance thus calculated is the unbiased estimator of the
    // true variance of the population.
    //
    // Uses the two-pass algorithm for calculating the variance as described 
    // in "Numerical Recipes in C", p.613.
    //
    //!param: data - input data
    //!return: double - variance
    //!exc: std::invalid_argument - thrown if data has length 0 or 1
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    variance( const Sequence<T>& data ) const;

    //	
    //: Calculate sample skewness 
    //
    // NOTE: the normalization chosen here is appropriate when we interpret 
    // the data as being a sample drawn from a larger population.  
    // This convention differs from that used in Matlab, but agrees with the 
    // "Numerical Recipes in C" convention.
    //
    // Uses the two-pass algorithm for calculating the variance as described 
    // in "Numerical Recipes in C", p.613.
    //
    //!param: data - input data
    //!return: double - skewness
    //!exc: std::invalid_argument - thrown if data has length 0 or 1
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    skewness( const Sequence<T>& data ) const;

    //
    //: Calculate sample kurtosis 
    //
    // NOTE: the normalization chosen here is appropriate when we interpret 
    // the data as being a sample drawn from a larger population.  
    // This convention differs from that used in Matlab, but agrees with the 
    // "Numerical Recipes in C" convention provided we subtract off a 3.  
    // According to our definition of kurtosis, a Gaussian distribution 
    // would have a kurtosis value of 3.
    //
    // Uses the two-pass algorithm for calculating the variance as described
    // in "Numerical Recipes in C", p.613.
    //
    //!param: data - input data
    //!return: double - kurtosis
    //!exc: std::invalid_argument - thrown if data has length 0 or 1
    //
    template<class T>
    typename StatisticsTraits<T>::TOut
    kurtosis( const Sequence<T>& data ) const;

    //
    //: Calculate all statistics
    //
    // Calculates the size, min, max, mean, rms, variance, skewness, 
    // and kurtosis of the input data.
    // See the definitions of the individual statistics for choice 
    // of normalizations, etc.
    //
    //!param: out - a class that holds the values of all the 
    //+ statistics
    //!param: in - input data
    //!exc: std::invalid_argument - thrown if data has length 0 or 1
    //
    template<class T>
    void 
    all(Stats& out, const Sequence<T>& in) const;

  };

} // end of datacondAPI namespace


#endif // STATISTICS_HH
