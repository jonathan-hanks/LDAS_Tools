/* -*- mode: c++; c-basic-offset: 2; -*- */

#if __SUNPRO_CC
#pragma ident "$Id: TimeBoundedFreqSequenceUDT.cc,v 1.13 2006/11/27 21:32:14 emaros Exp $"
#else
const char rcsID[] = "$Id: TimeBoundedFreqSequenceUDT.cc,v 1.13 2006/11/27 21:32:14 emaros Exp $";
#endif

#include "config.h"

#include <memory>

#include "general/Memory.hh"

#include <ilwd/ldasarray.hh>
#include <ilwd/ldasstring.hh>

#include <ilwdfcs/FrameH.hh>
#include <ilwdfcs/FrProcData.hh>
#include <ilwdfcs/FrVect.hh>

#include "TimeBoundedFreqSequenceUDT.hh"

#include "CallChain.hh"

using General::GPSTime;
using datacondAPI::TimeBoundedFreqSequence;

template<class DataType_>
TimeBoundedFreqSequence<DataType_>::
TimeBoundedFreqSequence( )
{
}

template<class DataType_>
TimeBoundedFreqSequence<DataType_>::
TimeBoundedFreqSequence( const Sequence<DataType_>& DataPoints, 
			 const double& f0, 
			 const double& df,
			 const GPSTime& Start,
			 const GPSTime& End)
  : FrequencySequence< DataType_ >( DataPoints, f0, df ),
    TimeBoundedFreqSequenceMetaData( Start, End )
{
}

template<class DataType_>
TimeBoundedFreqSequence<DataType_>::
TimeBoundedFreqSequence( const TimeBoundedFreqSequence< DataType_ >& value )
  : FrequencySequence< DataType_ >( value ),
    TimeBoundedFreqSequenceMetaData( value )
{
}

template<class T>
TimeBoundedFreqSequence< T >*
TimeBoundedFreqSequence<T>::
Clone() const
{
  return new TimeBoundedFreqSequence<T>( *this );
}

template<class DataType_>
ILwd::LdasContainer* TimeBoundedFreqSequence<DataType_>::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
    using namespace datacondAPI;

    std::unique_ptr<ILwd::LdasContainer> container( new ILwd::LdasContainer );

    switch (Target)
    {
    case udt::TARGET_FRAME:
    case udt::TARGET_FRAME_FINAL_RESULT:
      {
	using namespace ILwdFCS;

	TimeBoundedFreqSequence<DataType_>& data =
          const_cast<TimeBoundedFreqSequence<DataType_>&>(*this);
	const std::string& lname( Chain.GetIntermediateName( *this,
							     this->name( ) ) );
	
	FrProcData proc_data;

	proc_data.SetName( lname );
	proc_data.SetComment( Chain.GetIntermediateComment( *this ) );
        proc_data.SetType( ILwdFCS::FrProcData::FREQUENCY_SERIES );
        proc_data.SetSubType( ILwdFCS::FrProcData::UNKNOWN_SUB_TYPE );
        
        //:NOTE: undefined for this class
        // proc_data.SetTimeOffset( 0.0 );

        proc_data.SetTRange( GetEndTime() - GetStartTime() );

        //:NOTE: undefined for this class
        // proc_data.SetFShift( 0.0 );
        // proc_data.SetPhase(0.0);

        proc_data.SetFRange( this->GetFrequency(this->size()) -
			     this->GetFrequency(0) );
                
        //:NOTE: Undefined field
        // proc_data.SetBW(0.0);
          
        //:NOTE: Add auxParams, aux data, tables here
                
        // Add the data
	FrVect vect_data;
	vect_data.SetData( &data[0],
                           data.size(),
			   this->GetFrequency( 0 ),
			   "Hertz",
			   this->GetFrequencyDelta() );
        vect_data.SetName( lname );

	proc_data.AppendData( vect_data );

        proc_data.AppendHistory( this->getHistory(), GPSTime::NowGPSTime());

	TimeBoundedFreqSequenceMetaData::Store( proc_data );

	ILwd::LdasContainer* retval =
	  const_cast<CallChain&>(Chain).FormatFrame( Target,
						     container.get( ),
						     proc_data );
	// Store the metadata in an FrDetector struct
	GeometryMetaData::Store( const_cast<CallChain&>(Chain).
				 GetFrameHeaderTop() );
	if ( retval != container.get( ) )
	{
	  container.reset( retval );
	}
      }
      break;

    case udt::TARGET_GENERIC:
      container->push_back( FrequencySequence<DataType_>::
			    ConvertToIlwd(Chain, Target),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( TimeBoundedFreqSequenceMetaData::
			    ConvertToIlwd(Chain, Target),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      break;

    case udt::TARGET_WRAPPER:
      container.reset( FrequencySequence< DataType_ >::
		       ConvertToIlwd( Chain, Target ) );
      TimeBoundedFreqSequenceMetaData::store( *container.get(), Target );
      break;

    default:
      throw udt::
	BadTargetConversion(Target,
			    "datacondAPI::FrequencySequence");
      break;
    }

    return container.release();
}

#undef CLASS_INSTANTIATION
#define CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::TimeBoundedFreqSequence< class_ >; \
UDT_CLASS_INSTANTIATION(TimeBoundedFreqSequence< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)
