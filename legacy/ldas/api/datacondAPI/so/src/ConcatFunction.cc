/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "datacondAPI/config.h"

#include <memory>
#include <sstream>
#include <vector>

#include "general/Memory.hh"

#include "ConcatFunction.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

using namespace std;
using namespace datacondAPI;

namespace {

  const char* const rcsId = "#(@) $Id: ConcatFunction.cc,v 1.4 2006/11/27 21:32:14 emaros Exp $";

  const ConcatFunction staticConcatFunction;

  //
  // Apply concat to a bunch of Sequences
  //
  template<class T>
  void dispatch(const CallChain& chain,
                const CallChain::Params& params,
                const Sequence<T>& first,
                unique_ptr<udt>& yUdt)
  {
    // Storage for the pointers to Sequence
    vector<const Sequence<T>*> argPtr(params.size());
    
    // Keep track of how big a final result we need
    size_t totalSize = first.size();

    argPtr[0] = &first;

    for (size_t k = 1; k < params.size(); ++k)
    {
      const CallChain::Symbol* const argUdt = chain.GetSymbol(params[k]);
      
      if (const Sequence<T>* const p
             = dynamic_cast<const Sequence<T>*>(argUdt))
      {
        argPtr[k] = p;
        totalSize += p->size();
      }
      else
      {
        ostringstream oss;
        
        oss << "Illegal Argument: action concat(); argument #"
            << (k + 1)
            << "; expected a "
            << TypeInfoTable.GetName(typeid(first))
            << ", received a "
            << TypeInfoTable.GetName(typeid(*argUdt));

        throw std::invalid_argument(oss.str());
      }
    }

    // Everything's ok - create the output (preserving all metadata!)
    
    Sequence<T>* const y = first.Clone();
    y->resize(totalSize);
    yUdt.reset(y);

    size_t start = 0;
    for (size_t k = 0; k < argPtr.size(); ++k)
    {
      const size_t size = (*argPtr[k]).size();

      (*y)[slice(start, size, 1)] = *argPtr[k];

      start += size;
    }

    // Build the name
    ostringstream oss;

    oss << "concat(";
    oss << first.name();

    for (size_t k = 1; k < argPtr.size(); ++k)
    {
      oss << "," << (*argPtr[k]).name();
    }

    oss << ")";

    y->SetName(oss.str());

  }
  
  //---------------------------------------------------------------------
  // Template instantiation
  //---------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE( T ) \
  template \
  void dispatch(const CallChain& chain, \
                const CallChain::Params& params, \
                const Sequence< T >& first, \
                unique_ptr<udt>& yUdt)

  INSTANTIATE( float );
  INSTANTIATE( double );
  INSTANTIATE( complex< float > );
  INSTANTIATE( complex< double > );

#undef INSTANTIATE
} // anonymous namespace

const string& ConcatFunction::GetName() const
{
  const static string name("concat");
  
  return name;
}

ConcatFunction::ConcatFunction()
  : Function(ConcatFunction::GetName())
{ }

void ConcatFunction::Eval(CallChain* chain,
                          const CallChain::Params& params,
                          const string& ret) const
{
  // Storage for final result
  unique_ptr<udt> yUdt(0);

  // Allow 2 or more arguments
  if (params.size() >= 2)
  {
    // The first argument determines what type all the others must be
    const CallChain::Symbol* const first = chain->GetSymbol(params[0]);

    if (const Sequence<float>* const firstPtr
        = dynamic_cast<const Sequence<float>*>(first))
    {
      dispatch<float>(*chain, params, *firstPtr, yUdt);
    }
    else if (const Sequence<double>* const firstPtr
        = dynamic_cast<const Sequence<double>*>(first))
    {
      dispatch<double>(*chain, params, *firstPtr, yUdt);
    }
    else if (const Sequence<complex<float> >* const firstPtr
        = dynamic_cast<const Sequence<complex<float> >*>(first))
    {
      dispatch<complex<float> >(*chain, params, *firstPtr, yUdt);
    }
    else if (const Sequence<complex<double> >* const firstPtr
        = dynamic_cast<const Sequence<complex<double> >*>(first))
    {
      dispatch<complex<double> >(*chain, params, *firstPtr, yUdt);
    }
    else
    {
        throw std::invalid_argument("Illegal Argument: action concat(); "
                                    "argument #1; expected a Sequence");
    }

  }
  else // Not enough args
  {
    throw CallChain::BadArgumentCount(GetName(), "2+", params.size());
  }

  chain->ResetOrAddSymbol(ret, yUdt.release());
}
