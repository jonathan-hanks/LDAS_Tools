// -*-Mode: C++;-*-


//!ignore_begin:
%{

#include <stdexcept>
#include <sstream>

#include "ilwd/ldasstring.hh"
#include "genericAPI/registry.hh"
#include "genericAPI/thread.hh"
#include "genericAPI/threaddecl.hh"
#include "genericAPI/util.hh"

#include "CallChain.hh"

  using namespace datacondAPI;
  using ILwd::LdasContainer;
  using ILwd::LdasElement;

  typedef CallChain::Step		Step;

  //=====================================================================
  // Forward declaration of functions that are defined in this file.
  //=====================================================================

  extern "C"
  {
    bool ExecuteCommands( CallChain* Chain )
    {
      return Chain->Execute( true );
    }

    CREATE_THREADED1_DECL( ExecuteCommands, bool, CallChain* );
    CREATE_THREADED1( ExecuteCommands, bool, CallChain* )
  }

%}

%include "genericAPI/ilwd.swig"

//!ignore_end:

//: A chain of data conditioning commands to be executed
//
// This class maintains information necessary for executing a series of
// command.
// It manages a list of commands to be executed.
// It is also manipulated by the underlying classes.
//
class CallChain
{
public:
  //: Constructor
  //!return: mangled name of a CallChain pointer (_XXXXXX_CallChain_p)
  //!usage: set cc [ new_CallChain ]
  //!usage_ooi: CallChain	cc
  CallChain();

  //: Destructor
  //
  //!usage: set cc [ delete_CallChain ]
  //!usage_ooi: delete	cc
  ~CallChain();

  //: Add a "call function" step to the execution flow.
  //
  // Append to the list of steps a command a single step.
  //
  //!param: tcl_string Function - Name of method
  //!param: tcl_list Params - a list of parameters to pass the Method
  //!param: tcl_string Return - Optional variable to hold return value
  //
  //!return: Nothing
  //
  //!usage: CallChain_AppendCallFunction $cc $Function $Params $Return
  //!usage_ooi: cc AppendCallFunction $Function $Params $Return

  void AppendCallFunction(const char* Function,
			  const char** Params,
			  const char* Return);

  //: Ensure that a primary has been specified
  //
  //!return: Nothing
  //
  //!usage: CallChain_EnsurePrimary $cc
  //!usage_ooi: cc EnsurePrimary
  //
  void EnsurePrimary(void);

  //: Ingest an ilwd of channels
  //
  // This function takes an ilwd of channels and populates the symbol
  // table.
  //
  //!return: Nothing
  //!param: tcl_string Channels - mangled name of an ldas container
  //
  //!usage: CallChain_IngestChannels $cc $Channels
  //!usage_ooi: cc IngestChannels $Channels
  //
  // string IngestChannels(const LdasContainer& Channels);

  // This stores the actions to be perfomed within the CallChain.
  // It is useful for updating the database tables with the "parameters"
  // that were used to create the results.
  //
  //!param: tcl_list ActionList - a list of actions to be executed
  //
  //!usage: CallChain_SetActionList $cc $ActionList
  //!usage_ooi: cc SetActionList $ActionList

  void SetActionList( const char** ActionList);

  //: Specify the target for the final result
  //
  //!return: Nothing
  //!param: tcl_string Target - Either the name of an api (metadata
  //+       or wrapper) or the empty string (no api).
  //
  //!usage: CallChain_SetReturnTarget $cc target
  //!usage_ooi: cc SetReturnTarget target
  //

  void SetReturnTarget(const char* Target);

  //: Establish frame return mode.
  //
  //!param: bool Mode - True if all frame output is to go into a single frame,
  //+	false otherwise.
  //
  //!usage: CallChain_SetSingleFrameMode $cc 1
  //!usage_ooi: cc SetSingleFrameMode 1
  //
  void SetSingleFrameMode( bool Mode );
    
  //!ignore_begin:
#if defined( SWIGNEW )
  %extend {
#else /* defined( SWIGNEW ) */
  %addmethods {
#endif /* defined( SWIGNEW ) */
    //!ignore_end:
    //: Add an "intermediate result" step to the execution flow.
    //
    // Append to the list of steps a command an "intermediate result" step.
    //
    //!param: tcl_string Alias - The name to appear in the ilwd for the
    //+		element.
    //!param: tcl_string Comment - The comment to appear in the ilwd for the
    //+		element.
    //!param: tcl_string Target - Target specifier for destination.
    //+       (metadata, mpi, generic).
    //!param: tcl_bool Primary - True if intermediate is to be
    //+		the primary array for wrapper, false otherwise.
    //!param: tcl_string CompressionMethod - Method of compression to be
    //+		used. Is not used by all output targets.
    //!param: tcl_int CompressionLevel - Level of compression to be used.
    //+		Is not used by all CompressionMethods.
    //
    //!return: Nothing
    //
    //!usage: CallChain_AppendCallMethodImplied $cc name "intermediate result" $Primary
    //!usage_ooi: cc AppendCallMethodImplied name "intermediate result" $Primary
    void AppendIntermediateResult(const char* Alias,
				  const char* Comment,
				  const char* Target,
				  bool Primary,
				  const char* CompressionMethod = "gzip",
				  int CompressionLevel = 1 )
      {
	CallChain::IntermediateResult::primary_type
	  p( ( Primary )
	     ? CallChain::IntermediateResult::PRIMARY
	     : CallChain::IntermediateResult::MAYBE_PRIMARY );
	ILwd::LdasString s( Alias );

	if ( s.getName( s.getNameSize( ) - 1 ) == "primary" )
	{
	  s.deleteName( s.getNameSize( ) - 1 );
	  Alias = s.getNameString().c_str( );
	  p = CallChain::IntermediateResult::PRIMARY;
	}
	self->AppendIntermediateResult( "", Alias, Comment, Target, p,
					CompressionMethod,
					CompressionLevel );
      }

    //!ignore_end:
    //: Add an "output result" step to the execution flow.
    //
    // Append to the list of steps a command an "intermediate result" step.
    //
    //!param: tcl_string Symbol - The name of a symbol in the symbol table.
    //!param: tcl_string Alias - The name to appear in the ilwd for the
    //+		element.
    //!param: tcl_string Comment - The comment to appear in the ilwd for the
    //+		element.
    //!param: tcl_string Target - Target specifier for destination.
    //+       (metadata, mpi, generic).
    //!param: tcl_bool Primary - True if intermediate is to be
    //+		the primary array for wrapper, false otherwise.
    //!param: tcl_string CompressionMethod - Method of compression to be
    //+		used. Is not used by all output targets.
    //!param: tcl_int CompressionLevel - Level of compression to be used.
    //+		Is not used by all CompressionMethods.
    //
    //!return: Nothing
    //
    //!usage: CallChain_AppendCallMethodImplied $cc $x name "output result" $Primary
    //!usage_ooi: cc AppendCallMethodImplied $x name "output result" $Primary
    void AppendOutputResult( const char* Symbol,
			     const char* Alias,
			     const char* Comment,
			     const char* Target,
			     bool Primary,
			     const char* CompressionMethod = "gzip",
			     int CompressionLevel = 1 )
      {
	CallChain::
	IntermediateResult::primary_type
	  p( ( Primary )
	     ? CallChain::IntermediateResult::PRIMARY
	     : CallChain::IntermediateResult::MAYBE_PRIMARY );
	ILwd::LdasString s( Symbol );

	if ( s.getName( s.getNameSize( ) - 1 ) == "primary" )
	{
	  s.deleteName( s.getNameSize( ) - 1 );
	  Symbol = s.getNameString().c_str( );
	  p = CallChain::IntermediateResult::PRIMARY;
	}
	self->AppendIntermediateResult( Symbol, Alias, Comment, Target, p,
					CompressionMethod, CompressionLevel );
      }

    //: Do basic algorithm checking
    //
    // This method will verify basic correctness of the algoritm section
    //   of the call chain. If this function returns without throwing an
    //   error, then the algrorithm is ready to be executed
    //   (see Execute( ) ).
    //
    //!usage: CallChain_DryRun $cc
    //!usage_ooi: cc DryRun
    //
    void DryRun( ) const
    {
      self->DryRun( );
    }


    //: Show symbols in the symbol table
    //
    //!usage: CallChain_DumpSymbolTable $cc
    //!usage_ooi: cc DumpSymbolTable
    //
    string DumpSymbolTable( )
      {
	std::ostringstream	oss;

	self->Dump( oss );
	return oss.str();
      }

    //: Execute the accumuated series of steps.
    //
    // Take the list of accumulated steps and execute each one.
    //
    //!return: Nothing
    //
    //!usage: CallChain_Execute $cc
    //!usage_ooi: cc Execute
    //
    void Execute(void)
      {
	self->Execute( true );
      }

    /*
    tid* ExecuteCommands_t( Tcl_Interp* interp = 0,
			    const char* flag = "")
      {
	ThreadedException*	e = new ThreadedException(self);
	tid* t = new tid1<ThreadedException*,ThreadedException*>
	  ( &ExecuteCommands,
	    "ExecuteCommands",
	    e );
	Registry::threadRegistry.registerObject( t );
	t->setAlert( interp, flag );
	t->run();
	return t;
      }

    bool ExecuteCommands_r( tid* t )
      {
	if ( !Registry::threadRegistry.isRegistered( t ) )
	  throw SWIGEXCEPTION( "invalid_tid" );
	if ( t->getFunction() != "ExecuteCommands" )
	  throw SWIGEXCEPTION( "invalid_tid" );
	ThreadedException* thread( reinterpret_cast< tid1< ThreadedException*, ThreadedException* >* >
				   ( t )->getReturn() );
	Registry::threadRegistry.destructObject( t );
	bool retval(thread->retval());
	if (thread->ExceptionThrown())
	{
	  std::string result(thread->what());
	  std::string info(thread->info());
	  
	  delete thread;
	  throw SwigException(result, info,
			      __FILE__, __LINE__);
	}
	delete thread;
	return retval;
      }
    */

    //: Retrieve the results
    //
    // This function returns in a single ilwd all intermediate results
    // along with the final result.
    //	
    //!return: ptResults - a pointer to an element
    //
    //!usage: set ptResults [ CallChain_GetResults $cc ]
    //!usage_ooi: ptResults [ cc GetResults ]
    //
    string GetResults()
      {
	return createElementPointer(self->GetResults());
      }

    //: Ingest an ilwd of database information
    //
    // This function takes an ilwd of database information and creates
    //   objects in the CallChain based on the data supplied.
    //
    //!param: tcl_string Query - mangled name of an ldas container
    //!param: tcl_string BaseName - Name to be associated with the object.
    //+		If more than one object is created from the Query,
    //+		then the name of the objects will be BaseName followed
    //+		by "_" {count} where {count} starts at 1.
    //!param: tcl_string PushPass - Either "push" for pushing the
    //+		resulting objects into the CallChain's symbol table, or
    //+		"pass" to cause the resulting objects to forwarded to the
    //+		target API.
    //
    //!usage: CallChain_IngestDBQuery $cc $Query "db_data" "push"
    //!usage_ooi: cc IngestDBQuery $Query "db_data" "push"
    //
    string IngestDBQuery( const LdasContainer& Query,
			  char* BaseName,
			  char* PushPass )
      {
	if (!Registry::isElementValid(&Query))
	{
	  throw SWIGEXCEPTION( "Query is an invalid_element" );
	}
	if ( ( strcmp(PushPass, "push" ) != 0 ) &&
	     ( strcmp(PushPass, "pass" ) != 0 ) )
	{	
	  throw SWIGEXCEPTION( "Bad value for PushPass argument" );
	}
	CallChain::ingestion_type i( CallChain::SYMBOL );
	if ( strcmp( PushPass, "pass" ) == 0 )
	{
	  i = CallChain::INTERMEDIATE;
	}
	std::string symbols;
	self->IngestDatabaseData( BaseName, Query, i,
				  CallChain::QUERY_GENERIC,
				  &symbols );
	return symbols;
      }

    //: Ingest general ilwd
    //
    // This function takes an ilwd and populates the symbol
    // table.
    //
    //!return: Nothing
    //!param: tcl_string ILwd - mangled name of an ldas element
    //
    //!usage: CallChain_IngestILwd $cc $ILwd
    //!usage_ooi: cc IngestILwd $ILwd
    //
    string IngestILwd(const LdasElement& ILwd)
      {
	if (!Registry::isElementValid(&ILwd))
	{
	  throw SWIGEXCEPTION( "ILwd is an invalid_element" );
	}
	return self->IngestILwd( ILwd );
      }

    //: Ingest an ilwd of database information
    //
    // This function only creates DatabaseUDTs that are appropriate
    //	for the wrapperAPI.
    //
    //!param: tcl_string Query - mangled name of an ldas container
    //!param: tcl_string BaseName - Name to be associated with the object.
    //+		If more than one object is created from the Query,
    //+		then the name of the objects will be BaseName followed
    //+		by "_" {count} where {count} starts at 1.
    //!param: tcl_string PushPass - Either "push" for pushing the
    //+		resulting objects into the CallChain's symbol table, or
    //+		"pass" to cause the resulting objects to forwarded to the
    //+		target API.
    //
    //!usage: CallChain_IngestNTuples $cc $Query "db_data" "push"
    //!usage_ooi: cc IngestNTuples $Query "db_data" "push"
    //
    string IngestNTuples( const LdasContainer& Query,
			  char* BaseName,
			  char* PushPass )
      {
	if (!Registry::isElementValid(&Query))
	{
	  throw SWIGEXCEPTION( "Query is an invalid_element" );
	}
	if ( ( strcmp(PushPass, "push" ) != 0 ) &&
	     ( strcmp(PushPass, "pass" ) != 0 ) )
	{	
	  throw SWIGEXCEPTION( "Bad value for PushPass argument" );
	}
	CallChain::ingestion_type i( CallChain::SYMBOL );
	if ( strcmp( PushPass, "pass" ) == 0 )
	{
	  i = CallChain::INTERMEDIATE;
	}
	std::string symbols;
	self->IngestDatabaseData( BaseName, Query, i,
				  CallChain::QUERY_NTUPLE,
				  &symbols );
	return symbols;
      }

    //: Ingest an ilwd of channels
    //
    // This function takes an ilwd of channels and populates the symbol
    //	 table.
    //
    //!return: list of symbols generated by action
    //
    //!param: tcl_number SampleRate - Number of samples per second
    //!param: tcl_number StartTimeSeconds - The second component of the start
    //+	time of the quality channel.
    //!param: tcl_number StartTimeNanoSeconds - The nanosecond component of the
    //+	start time of the quality channel.
    //!param: tcl_number EndTimeSeconds - The second component of the end
    //+	time of the quality channel.
    //!param: tcl_number EndTimeNanoSeconds - The nanosecond component of the
    //+	end time of the quality channel.
    //!param: tcl_string Channels - mangled name of an ldas container
    //!param: tcl_string Name - Name to be associated with the quality channel.
    //+		If the data for the quality channel spans multiple
    //+		multiple interferometers, then the Name becomes a prefix
    //+		specifier and the name of the channel will be Name followed
    //+		by "_" <interferometer>.
    //!param: tcl_string PushPass - Either "push" for pushing the data
    //+		quality channel into the CallChain's symbol table, or
    //+		"pass" to cause the data to forwarded to the target
    //+		API.
    //
    //!usage: CallChain_IngestQualityChannel $cc
    //+		1024 686868046 0 686868047 0 $Channel "qc" "push"
    //!usage_ooi: cc IngestQualityChannel
    //+		1024 686868046 0 686868047 0 $Channel "qc" "push"
    //
    string IngestQualityChannel( unsigned int SampleRate,
				 unsigned int StartTimeSeconds,
				 unsigned int StartTimeNanoSeconds,
				 unsigned int EndTimeSeconds,
				 unsigned int EndTimeNanoSeconds,
				 const LdasContainer& Query,
				 char* Alias,
				 char* PushPass )
      {
	if (!Registry::isElementValid(&Query))
	{
	  throw SWIGEXCEPTION( "Query is an invalid_element" );
	}
	if ( ( strcmp(PushPass, "push" ) != 0 ) &&
	     ( strcmp(PushPass, "pass" ) != 0 ) )
	{
	  throw SWIGEXCEPTION( "Bad value for PushPass argument" );
	}
	CallChain::ingestion_type i( CallChain::SYMBOL );
	if ( strcmp( PushPass, "pass" ) == 0 )
	{
	  i = CallChain::INTERMEDIATE;
	}
	return self->IngestQualityChannel( Query,
					   StartTimeSeconds,
					   StartTimeNanoSeconds,
					   EndTimeSeconds,
					   EndTimeNanoSeconds,
					   SampleRate,
					   Alias,
					   i );
      }

    //: Ingest an ilwd of database information
    //
    // This function only creates DatabaseUDTs that are appropriate
    //	for the wrapperAPI.
    //
    //!param: tcl_string Query - mangled name of an ldas container
    //!param: tcl_string BaseName - Name to be associated with the object.
    //+		If more than one object is created from the Query,
    //+		then the name of the objects will be BaseName followed
    //+		by "_" {count} where {count} starts at 1.
    //!param: tcl_string PushPass - Either "push" for pushing the
    //+		resulting objects into the CallChain's symbol table, or
    //+		"pass" to cause the resulting objects to forwarded to the
    //+		target API.
    //
    //!usage: CallChain_IngestNTuples $cc $Query "db_data" "push"
    //!usage_ooi: cc IngestNTuples $Query "db_data" "push"
    //
    string IngestDBSpectrum( const LdasContainer& Query,
			     char* BaseName,
			     char* PushPass )
      {
	if (!Registry::isElementValid(&Query))
	{
	  throw SWIGEXCEPTION( "Query is an invalid_element" );
	}
	if ( ( strcmp(PushPass, "push" ) != 0 ) &&
	     ( strcmp(PushPass, "pass" ) != 0 ) )
	{	
	  throw SWIGEXCEPTION( "Bad value for PushPass argument" );
	}
	CallChain::ingestion_type i( CallChain::SYMBOL );
	if ( strcmp( PushPass, "pass" ) == 0 )
	{
	  i = CallChain::INTERMEDIATE;
	}
	std::vector<datacondAPI::udt*>	data;
	std::vector<std::string>	names;
	std::string			symbols;
	std::string*			s( ( i == CallChain::SYMBOL )
					   ? &symbols
					   : (std::string*)NULL );

	DB::Table*	table( DB::Table::MakeTableFromILwd( Query, true ) );

	self->IngestDatabaseSpectrumData( *table,
					  data,
					  names,
					  BaseName,
					  CallChain::EXACTLY_ONE,
					  s );
	return symbols;
      }

    //: Pass through general ilwd
    //
    // This function takes an ilwd and passes it through
    //
    //!return: Nothing
    //!param: tcl_string ILwd - mangled name of an ldas element
    //
    //!usage: CallChain_PassThroughILwd $cc $ILwd
    //!usage_ooi: cc PassThroughILwd $ILwd
    //
    void PassThroughILwd( const LdasElement& ILwd )
      {
	self->IngestILwd( ILwd, CallChain::INTERMEDIATE );
      }
    
    //!ignore_begin:
  } // %addmethods
  //!ignore_end:
};

//-------------------------------------------------------------------
//
//: Executing a list of commands - Threaded
//
//!return: tid* - A Pointer to a Thread Id
//
//!usage: set tid [ ExecuteCommands_t cc ]
//
tid* ExecuteCommands_t( CallChain* p1,
			Tcl_Interp* interp = 0,
			const char* flag = "" );

//-------------------------------------------------------------------
//
//: Executing a list of commands - Thread return value
//
// Blocks until the thread finishes executing, returning any thrown
// exception. This will also destruct the thread object.
//
//!usage: ExecuteCommands_r tid
//
//!param: tid - A pointer to a ExecuteCommands Thread.
//
//!return: none
//
//!usage: set tid [ ExecuteCommands_t cc ]
//
//!exec: refer to CallChain::Execute()
//
bool ExecuteCommands_r( tid* t );

#ifdef SWIGTCL8
%typemap(in) char **;
%typemap(freearg) char **;
#endif /* SWIGTCL8 */


