/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "config.h"

#include "general/types.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldasarray.hh"

#include "VectorSequenceUDT.hh"

template<class DataType_>
ILwd::LdasElement* datacondAPI::VectorSequence<DataType_>::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  switch ( Target )
  {
  default:
    throw datacondAPI::udt::BadTargetConversion(Target,
						"datacondAPI::VectorSequence");
  }
}

///----------------------------------------------------------------------
/// Instantiations of the class
///----------------------------------------------------------------------

#define VSCLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::VectorSequence< class_ >; \
UDT_CLASS_INSTANTIATION(VectorSequence< class_ >,key_)

VSCLASS_INSTANTIATION(float,float)
VSCLASS_INSTANTIATION(double,double)
VSCLASS_INSTANTIATION(std::complex<float>,cfloat)
VSCLASS_INSTANTIATION(std::complex<double>,cdouble)

#if __SUNPRO_CC
#undef INIT_FUNC
#define INIT_FUNC init_VectorSequenceUDT
MK_UDT_INIT_FUNC_4(INIT_FUNC,float,double,cfloat,cdouble);
#pragma init (INIT_FUNC)
#endif /* __SUNPRO_CC */

#undef VSCLASS_INSTANTIATION

