#include "datacondAPI/config.h"

#include <complex>

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "gasdevstate.hh"
#include "random.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

using std::log;
using std::sqrt;

namespace datacondAPI{

  gasdevstate::gasdevstate()
  {
    iset=0;
  }

  gasdevstate::gasdevstate(const int& seed)
    : s(seed)
  {
    iset=0;
  }

  gasdevstate::gasdevstate(const gasdevstate& g)
    : State( g ),
      s(g.s)
  {
    iset = g.iset;
    gset = g.gset;
  }

  //restest state with new seed s
  void gasdevstate::seed(int sd)
  {
    s.seed(sd);
    iset=0;
  }

  float gasdevstate::getgasdev()
  {
  
    float fac,rsq,v1,v2;

        if(iset==0)
        {
                do{
                        v1=2.0*ran4(s)-1.0;
                        v2=2.0*ran4(s)-1.0;
                        rsq=v1*v1+v2*v2;
                }while (rsq >=1.0 || rsq ==0.0);
                fac=sqrt(-2.0*log(rsq)/rsq);
                gset=v1*fac;
                iset=1;
                return v2*fac;
        }
        else
        {
                iset=0;
                return gset;
        }


  }

  template<> void gasdevstate::getgasdev(std::valarray<float>& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = getgasdev();
  }

  template<> void gasdevstate::getgasdev(std::valarray<double>& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = double(getgasdev());
  }

  template<> void gasdevstate::getgasdev(std::valarray<std::complex<float> >& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = std::complex<float>(getgasdev(), getgasdev());
  }

  template<> void gasdevstate::getgasdev(std::valarray<std::complex<double> >& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = std::complex<double>(double(getgasdev()), double(getgasdev()));
  }

  gasdevstate* gasdevstate::Clone() const
  {
    return new gasdevstate(*this);
  }

  ILwd::LdasElement* gasdevstate::
  ConvertToIlwd( const CallChain& Chain,
                 datacondAPI::udt::target_type Target) const
  {
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    General::toss<General::unimplemented_error>("ConvertToILWD",
						__FILE__,__LINE__,
						"unimplemented");
    return container;
  }

}

