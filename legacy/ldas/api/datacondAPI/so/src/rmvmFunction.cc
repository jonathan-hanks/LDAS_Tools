#include "config.h"

#include "ShiftState.hh"
#include <filters/LDASConstants.hh>
#include <general/toss.hh>
#include "Kalman.hh"
#include "SequenceUDT.hh"
#include "random.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"
#include "rmvmFunction.hh"
#include "udtslice_array.h"
#include "Mixer.hh"
#include "MixerState.hh"
#include "Resample.hh"
#include "Matrix.hh"

using std::log10;
using std::pow;
using std::sqrt;

double factorial(int number)
{
	double product = 1;

	for ( ; number > 0 ; number--)
		product *= number;

	return product;
}
namespace datacondAPI{

double inf_norm(Matrix<double> &A){
  Sequence<double> rowsum(A.getNRows());
  for(unsigned int i=0; i<rowsum.size(); i++){
    double sum =0;
    for(unsigned int j=0; j< A.getNCols(); j++){
      if(A[i][j] < 0)
	sum -= A[i][j];
      else
	sum += A[i][j];
    }
    rowsum[i] = sum;
  }
  double max = 0;
  for (unsigned int i=0; i<rowsum.size(); i++){
    if( max < rowsum[i])
      max = rowsum[i];
  }
  return max;
}

void expm(Matrix<double> &A, Matrix<double> &B){
  double j = 1 + int(log10(inf_norm(A))/(log10(2.0)));
  if(j < 0)
    j = 0.0;
  A = A / pow(2.0,j);
  int q = 0;
  double epsilon = 8;
  double eps_fact = 1;
  const double delta = 1e-10;
  while (epsilon > delta){
    q++;
    eps_fact = eps_fact / (4*(2*q-1)*(2*q+1));
    epsilon = pow(2.0, (3-(2*q)))* eps_fact;
  }
  Matrix<double> D(A.getNRows(),A.getNRows(),0.0);
  Matrix<double> N(A.getNRows(),A.getNRows(),0.0);
  Matrix<double> X(A.getNRows(),A.getNRows(),0.0);
  double c = 1;
  for(unsigned int i=0; i<A.getNRows(); i++){
    D[i][i] = 1.0;
    N[i][i] = 1.0;
    X[i][i] = 1.0;
  }
  
  for(int k = 1; k<= q; k++){
    c = c*(q - k + 1)/((2*q-k+1)*k);
    X = A*X;
    N = N + c*X;
    D = D + pow(-1.0,k)*c*X;
  }

  LinearAlgebra::SV(D,B,N);

  for(int k=1; k<=j;k++){
    B = B*B;
  }
  return;

}

void calc_A(Sequence<double>& f0s, Sequence<double>& Qs,double df, double fc, Matrix<double> &A)
{
  //double w0 = LDAS_TWOPI*f0;
  //double wc = LDAS_TWOPI*df/2 + LDAS_TWOPI*fc;
  for(int i=0; i<int(f0s.size()); i++){
    datacondAPI::Matrix<double> temp(2,2);
    temp[0][0] = temp[1][1] = 1;
    temp[0][1] = temp[1][0] = 0;
    datacondAPI::Matrix<double> temp1(2,2);
    temp1[0][0] = temp1[1][1] = -1*f0s[i]*LDAS_TWOPI/(2*Qs[i]*df);
    temp1[0][1] = (fc*LDAS_TWOPI-f0s[i]*LDAS_TWOPI*sqrt(1-1/(4*Qs[i]*Qs[i])))/df;
    temp1[1][0] = -1*temp1[0][1];
    expm(temp1,temp);
    
    /*for(int j=1;j<20;j++)
      {
	temp = temp + 1/(factorial(j))*temp1;
	temp1 = temp1*temp1;
	}*/
    A[2*i][2*i] = temp[0][0];
    A[2*i][2*i+1] = temp[0][1];
    A[2*i+1][2*i] = temp[1][0];
    A[2*i+1][2*i+1] = temp[1][1];
  }
  //A.resize(2,2);
  //A = temp;
  return;
   
}
}//namespace

const std::string& rmvmFunction::GetName(void) const
{
  static std::string name("rmvm");

  return name;
}

rmvmFunction::rmvmFunction() : Function( rmvmFunction::GetName() )
{}

static rmvmFunction _rmvmFunction;

void rmvmFunction::Eval(CallChain* Chain,
			const CallChain::Params& Params,
			const std::string& Ret) const
{
  //double fs = 16384.0;
  CallChain::Symbol* returnedSymbol = 0;   
  using namespace datacondAPI;

  switch (Params.size())
  {
  case 9:
    {
      //rmvm(x,freqs,Qs,fc,deltaf,W,V,down,fs)
      //step 0: read in inputs and make initial checks
     
      Sequence<double> data;
      if(udt::IsA<TimeSeries<double> >(*Chain->GetSymbol(Params[0]))){
	data.resize(udt::Cast<TimeSeries<double> >(*Chain->GetSymbol(Params[0])).size());
	data = udt::Cast<TimeSeries<double> >(*Chain->GetSymbol(Params[0]));
      }
      else if(udt::IsA<TimeSeries<float> >(*Chain->GetSymbol(Params[0]))){
	data.resize(udt::Cast<TimeSeries<float> >(*Chain->GetSymbol(Params[0])).size());
	for(int i=0; i<int(data.size()); i++){
	  data[i] = double(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[0]))[i]);
	}
      }
      else if(udt::IsA<Sequence<double> >(*Chain->GetSymbol(Params[0]))){
	data.resize(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[0])).size());
	data = (udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[0])));
      }
      else if(udt::IsA<Sequence<float> >(*Chain->GetSymbol(Params[0]))){
	data.resize(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[0])).size());
	for(int i=0; i<int(data.size()); i++){
	  data[i] = double(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[0]))[i]);
	}
      }
      else
	throw std::invalid_argument("Argument 1 (data) is not of the correct type");

      Sequence<double> f0s;
      if(udt::IsA<Sequence<double> >(*Chain->GetSymbol(Params[1]))){
	f0s.resize(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[1])).size());
	f0s = udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[1]));
      }
      else if(udt::IsA<Sequence<float> >(*Chain->GetSymbol(Params[1]))){
	f0s.resize(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[1])).size());
	for(int i=0; i<int(f0s.size()); i++){
	  f0s[i] = double(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[1]))[i]);
	}
      }
      else if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[1]))){
	f0s.resize(1);
	f0s[0] = double(udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[1])).GetValue());
      }
      else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[1]))){
	f0s.resize(1);
	f0s[0] = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[1])).GetValue());
      }
      else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[1]))){
	f0s.resize(1);
	f0s[0] = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[1])).GetValue());
      }
      else
	throw std::invalid_argument("Argument 2 (frequencies) is not of the correct type");
      
      Sequence<double> Qs;
      if(udt::IsA<Sequence<double> >(*Chain->GetSymbol(Params[2]))){
	Qs.resize(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[2])).size());
	Qs = udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[2]));
      }
      else if(udt::IsA<Sequence<float> >(*Chain->GetSymbol(Params[2]))){
	Qs.resize(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[2])).size());
	for(int i=0; i<int(Qs.size()); i++){
	  Qs[i] = double(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[2]))[i]);
	}
      }
      else if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[2]))){
	Qs.resize(1);
	Qs[0] = double(udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[2])).GetValue());
      }
      else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[2]))){
	Qs.resize(1);
	Qs[0] = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[2])).GetValue());
      }
      else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[2]))){
	Qs.resize(1);
	Qs[0] = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[2])).GetValue());
      }
      else
	throw std::invalid_argument("Argument 3 (qualities) is not of the correct type");

      double fc;
      if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[3]))){
	fc = double(udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[3])).GetValue());
      }
      else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[3]))){
	fc = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[3])).GetValue());
      }
      else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[3]))){
	fc = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[3])).GetValue());
      }
      else
	throw std::invalid_argument("Argument 4 (central frequency) is not of the correct type");

      double deltaf;
      if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[4]))){
	deltaf = double(udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[4])).GetValue());
      }
      else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[4]))){
	deltaf = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[4])).GetValue());
      }
      else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[4]))){
	deltaf = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[4])).GetValue());
      }
      else
	throw std::invalid_argument("Argument 5 (deltaf) is not of the correct type");

      Sequence<double> Ws;
      if(udt::IsA<Sequence<double> >(*Chain->GetSymbol(Params[5]))){
	Ws.resize(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[5])).size());
	Ws = udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[5]));
      }
      else if(udt::IsA<Sequence<float> >(*Chain->GetSymbol(Params[5]))){
	Ws.resize(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[5])).size());
	for(int i=0; i<int(Ws.size()); i++){
	  Ws[i] = double(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[5]))[i]);
	}
      }
      else if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[5]))){
	Ws.resize(1);
	Ws[0] = double(udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[5])).GetValue());
      }
      else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[5]))){
	Ws.resize(1);
	Ws[0] = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[5])).GetValue());
      }
      else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[5]))){
	Ws.resize(1);
	Ws[0] = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[5])).GetValue());
      }
      else
	throw std::invalid_argument("Argument 6 (Process Noise) is not of the correct type");

      double Vs;
      if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[6]))){
	Vs = double(udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[6])).GetValue());
      }
      else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[6]))){
	Vs = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[6])).GetValue());
      }
      else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[6]))){
	Vs = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[6])).GetValue());
      }
      else
	throw std::invalid_argument("Argument 7 (Measurement Noise) is not of the correct type");

      double fs;
      if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[8]))){
	fs = double(udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[8])).GetValue());
      }
      else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[8]))){
	fs = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[8])).GetValue());
      }
      else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[8]))){
	fs = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[8])).GetValue());
      }
      else
	throw std::invalid_argument("Argument 9 (Sampling Frequency) is not of the correct type");

      int down;
      if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[7]))){
	down = udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[7])).GetValue();
      }
      else
	throw std::invalid_argument("Argument 8 (Downsample ratio) is not of the correct type");
      
      int test1 = data.size()/down;
      if(test1 != double(data.size())/double(down))
	throw std::invalid_argument("Length of data must be a multiple of the downsample ratio");
      if(!(f0s.size() == Qs.size() && Qs.size() == Ws.size()))
	throw std::invalid_argument("freqs, Qs, and W must all be of the same size");
      for(int i=0; i<int(f0s.size()); i++){
	if(f0s[i] < fc - deltaf/2. || f0s[i] > fc + deltaf/2.)
	  throw std::invalid_argument("Not all frequencies fall within deltaf of fc");
      }


      Sequence<double> new_data (0.,int(data.size() + down*20));
      for(unsigned int i=0; i<data.size(); i++){
	new_data[i] = data[i];
      }
      
      //step 1: Use BandSelector to transform to the z-domain (fc -> 0 Hz) and bandlimit by downsampling
      BandSelector bs(2*fc/fs, down);
      Sequence<std::complex<double> > z;
      bs.apply(z,new_data);

      //step 2: Convert data to the proper format for the Kalman Filter
      Sequence<double> tmp_z(2*z.size());
      for(int i=0; i < int(tmp_z.size()); i+=2){
	tmp_z[i] = z[i/2].real();
	tmp_z[i+1] = z[i/2].imag();
      }
      VectorSequence<double> z_in(tmp_z,2,tmp_z.size()/2);

      //step 3: Form arguments for Kalman Filter (A,C,W,V,psi,P)
      int num_nodes = f0s.size();
      Matrix<double> A(2*num_nodes, 2*num_nodes, 0.);
      Matrix<double> C(2, 2*num_nodes, 0.);
      Matrix<double> W(2*num_nodes, 2*num_nodes, 0.);
      Matrix<double> V(2, 2, 0.); 
      Sequence<double> psi(0., 2*num_nodes);

      //form A
      calc_A(f0s,Qs,deltaf,fc,A);
      
      //form C
      for(int i=0; i<int(2*num_nodes); i++){
	if(i%2 == 0){
	  C[0][i] = 1;
	}
	else{
	  C[1][i] = 1;
	}
      }

      //form W
      for(int i=0; i<int(2*num_nodes); i+=2){
	W[i][i] = Ws[i/2];
	W[i+1][i+1] = W[i][i];
      }

      //form V
      V[0][0] = Vs;
      V[1][1] = Vs;

      //form P
      Matrix<double> P(W);

      //step 4: create Kalman Filter and apply it to the data
      Kalman filter(A,W,C,V,psi,P);
      udt* asdf = new VectorSequence<double> (2,tmp_z.size()/2);
      filter.apply(asdf,z_in);
      VectorSequence<double> VS_z_out(udt::Cast<VectorSequence<double> >(*asdf));
      delete asdf;
      asdf = NULL;

      //change the data to a more useful format
      Sequence<std::complex<double> >z_out(tmp_z.size()/2);
      for(int i=0; i<int(z_out.size()); i++){
	std::valarray<double> vect = VS_z_out.vec(i);
	std::complex<double> comp(vect[0],vect[1]);
	z_out[i] = comp;
      }

      //step 5: transform data back from z-domain
      Sequence<std::complex<double> >Gv;
      bs.ylppa(Gv,z_out);

      //step 6: find the prediction of the contribution of the v-modes
      Sequence<double> almostdone(Gv.size());
      for(int i=0; i<int(Gv.size()); i++){
	almostdone[i] = 2*Gv[i].real();
	almostdone[i] = data[i] - almostdone[i];
      }

      if(udt::IsA<TimeSeries<float> >(*Chain->GetSymbol(Params[0]))){
	TimeSeries<double> temp(almostdone,udt::Cast<TimeSeries<float> >(*Chain->GetSymbol(Params[0])).GetSampleRate());
	temp.CopyMetaData(udt::Cast<TimeSeries<float> >(*Chain->GetSymbol(Params[0])));
	returnedSymbol = new TimeSeries<double> (temp);
      }
      else if(udt::IsA<TimeSeries<double> >(*Chain->GetSymbol(Params[0]))){
	TimeSeries<double> temp(almostdone,udt::Cast<TimeSeries<double> >(*Chain->GetSymbol(Params[0])).GetSampleRate());
	temp.CopyMetaData(udt::Cast<TimeSeries<double> >(*Chain->GetSymbol(Params[0])));
	returnedSymbol = new TimeSeries<double> (temp);
      }
      else
      returnedSymbol = new Sequence<double> (almostdone);
      /*Sequence<double> A_t(A.getNCols()*A.getNRows());
      for(int i=0; i< A.getNRows(); i++){
	for(int j=0; j < A.getNCols(); j++){
	  A_t[2*j + i%2] = A[i][j];
	}
      }

      returnedSymbol = new Sequence<double> (A_t);*/
    }
    break;

  default:
    throw CallChain::BadArgumentCount( GetName(),
				       "9",
				       Params.size() );
    break;
  
   }

  if (!returnedSymbol)
  {
    throw CallChain::NullResult( GetName() );
  }
  
  Chain->ResetOrAddSymbol( Ret, returnedSymbol );  
}
