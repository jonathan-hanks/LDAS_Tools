/* -*- mode: c++ -*- */
#ifndef GETSEQUENCEFUNCTION_HH
#define GETSEQUENCEFUNCTION_HH

#include "CallChain.hh"

//
// Extract the Sequence component of an object:
//
//   y = getSequence(x)
//
class GetSequenceFunction : public CallChain::Function {
public:
  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  GetSequenceFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* chain,
                    const CallChain::Params& params,
                    const std::string& ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName() const;
};

#endif // GETSEQUENCEFUNCTION_HH

