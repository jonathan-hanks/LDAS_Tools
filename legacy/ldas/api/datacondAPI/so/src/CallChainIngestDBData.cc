#include "datacondAPI/config.h"

#include <cmath>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

#include "CallChain.hh"

#include "general/Memory.hh"
#include "general/unordered_map.hh"

#include "ilwd/ldaselement.hh"
#include "ilwd/ldascontainer.hh"

#include "dbaccess/Table.hh"
#include "dbaccess/FieldArray.hh"
#include "dbaccess/FieldString.hh"
#include "dbaccess/SummCSD.hh"
#include "dbaccess/SummSpectrum.hh"
#include "dbaccess/SummStatistics.hh"

#include "DatabaseUDT.hh"
#include "StatsUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"

using std::ceil;
using std::floor;

#if 0
#define	AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define	AT()
#endif

//: Structure to hold information on how to validate table.
typedef struct {
  bool				s_simple;
  bool				s_allow_nulls;
  CallChain::db_ingestion_type	s_row_rule;
  CallChain::query_type		s_query_type;
  CallChain::ingestion_type	s_ingestion;
  datacondAPI::udt::target_type	s_target;
} check_table_type;

//: Checks to see if the table is a simple table
static void check_table( const DB::Table& Table, const check_table_type& Check );

const DB::FieldArray< INT_4S >&
validate_column_int4s( const DB::Table& DBTable,
		       const std::string& Column )
try
{
  return DBTable.GetFieldArray<INT_4S>( DBTable.HasField( Column ) );
}
catch( std::bad_cast )
{
  std::string msg = "Column type is not appropriate for '";
  msg += Column;
  msg += "' column";
  throw CallChain::LogicError( msg );
}

const DB::FieldString&
validate_column_string( const DB::Table& DBTable,
		       const std::string& Column )
try
{
  return DBTable.GetFieldString( DBTable.HasField( Column ) );
}
catch( std::bad_cast )
{
  std::string msg = "Column type is not appropriate for '";
  msg += Column;
  msg += "' column";
  throw CallChain::LogicError( msg );
}

// A quality channel has several components.
std::string CallChain::
IngestQualityChannel( const ILwd::LdasContainer& Channel,
		      INT_4U StartSec, INT_4U StartNanoSec,
		      INT_4U EndSec, INT_4U EndNanoSec,
		      INT_4U SampleRate,
		      const std::string& Alias,
		      CallChain::ingestion_type Ingestion )
{
  using namespace DB;
  using namespace datacondAPI;
  using General::GPSTime;

  typedef General::unordered_map<std::string,unsigned int> qc_map_type;
  typedef float						qc_data_type;
  typedef TimeSeries<qc_data_type>				qc_type;

  static const qc_data_type	qc_typerue((qc_data_type)1);
  static const qc_data_type	qc_false((qc_data_type)0);

  std::string		retval;
  qc_map_type		qc_map;
  std::vector< qc_type* >	qc;	// Quality Channel
  std::string		ifo;

  AT();
  GPSTime		channel_start( StartSec, StartNanoSec );
  GPSTime		channel_stop( EndSec, EndNanoSec );
  INT_4U		channel_len( (INT_4U)( ceil( ( channel_stop -
						       channel_start )
						     * SampleRate ) ) + 1U);
  AT();
  double		channel_dt( ( channel_stop - channel_start ) /
				    channel_len );


  AT();
  std::unique_ptr< Table >
    table( Table::MakeTableFromILwd( Channel, true ) );

  AT();
  const FieldArray<INT_4S>&	col_st_sec
    ( validate_column_int4s( *table, "StartSec" ) );
  const FieldArray<INT_4S>&	col_st_nsec
    ( validate_column_int4s( *table, "StartNanoSec" ) );
  const FieldArray<INT_4S>&	col_et_sec
    ( validate_column_int4s( *table, "StopSec" ) );
  const FieldArray<INT_4S>&	col_et_nsec
    ( validate_column_int4s( *table, "StartNanoSec" ) );

  try {
    table->HasField( "IFO" );
  } catch ( const std::runtime_error& msg )
  {
    // IFO column was not supplied. Generate an IFO column and populate it
    //   with the empty string.
    table->AddField( new FieldString("IFO", false, *table) );
    table->AppendNCopies( "IFO", table->GetRowCount(), "", 0);
  }

  AT();
  const FieldString&	col_ifo( validate_column_string( *table, "IFO" ) );

  AT();
  if ( table->GetRowCount() == 0 )
  {
    AT();
    Sequence<qc_data_type>	data( qc_false, channel_len );
    
    qc.push_back( new qc_type( data, SampleRate, channel_start ) );
    qc_map[ "" ] = 0;
  }
  for ( unsigned int r = 0; r < table->GetRowCount(); r++ )
  {
    //-------------------------------------------------------------------
    // Find the appropriate time series
    //-------------------------------------------------------------------

    AT();
    qc_map_type::const_iterator	i = qc_map.find( col_ifo[r] );
    qc_type*			channel;

    AT();
    if ( i == qc_map.end() )
    {
      //-----------------------------------------------------------------
      // No appropriate time series was found. Create a new one
      //-----------------------------------------------------------------
      AT();
      Sequence<qc_data_type>	data( qc_false, channel_len );

      size_t	o = qc.size();
      
      channel = new qc_type( data, SampleRate, channel_start );

      qc.push_back( channel );
      qc_map[ col_ifo[r] ] = o;
      
    }
    else
    {
      //-----------------------------------------------------------------
      // Use previously created channel.
      //-----------------------------------------------------------------
      AT();
      channel = qc[ i->second ];
    }

    //-------------------------------------------------------------------
    // Populate the quality channel
    //-------------------------------------------------------------------

    AT();
    GPSTime	start(col_st_sec[r], col_st_nsec[r]);
    GPSTime	stop(col_et_sec[r], col_et_nsec[r]);

    AT();
    if ( start < channel_start )
    {
      AT();
      start = channel_start;
    }
    if (stop > channel_stop )
    {
      AT();
      stop = channel_stop;
    }
    if ( ( stop - start ) >= 0 )
    {
      AT();

      INT_4U begin = (INT_4U)( ceil( ( start - channel_start) / channel_dt ) );
      INT_4U end = ( INT_4U)( floor( ( stop - channel_start) / channel_dt ) );
      qc_data_type* data( &(channel->operator[](begin) ) );

      AT();
      for ( INT_4U x = begin; x <= end; x++, data++ )
      {
	*data = qc_typerue;
      }
    }
  }

  AT();
  //---------------------------------------------------------------------
  // Store the results
  //---------------------------------------------------------------------
  register unsigned int qc_map_size( qc_map.size() );
  for ( qc_map_type::iterator i = qc_map.begin();
	i != qc_map.end();
	i++ )
  {
    AT();
    std::string	name(Alias);
    if ( i != qc_map.begin() )
    {
      retval += " ";
    }
    if ( ( i->first != "" ) && ( qc_map_size > 1 ) )
    {
      name += "_";
      name += i->first;
    }
    AT();
    retval += name;
    qc[ i->second ]->SetName( name );
    StoreUDT( name, qc[ i->second ], Ingestion, true,
	      (std::string*)NULL,
	      IntermediateResult::NEVER_PRIMARY );
  }

  AT();
  return retval;
}

bool CallChain::
IngestDatabaseData( const std::string& Symbol,
		    const ILwd::LdasElement& Element,
		    CallChain::ingestion_type Ingestion,
		    CallChain::query_type QueryType,
		    std::string* Results )
{
  bool		convert( true );
  check_table_type	check;

  check.s_simple = false;
  check.s_allow_nulls = true;
  check.s_row_rule = ZERO_OR_MORE;
  check.s_query_type = QueryType;
  check.s_ingestion = Ingestion;
  check.s_target = m_final_result_target;

  switch( QueryType )
  {
  case QUERY_GENERIC:
    convert = false;
    check.s_simple = false;
    check.s_row_rule = ZERO_OR_MORE;
    //-------------------------------------------------------------------
    // Check if arbitrary query is acceptable
    //-------------------------------------------------------------------

    if ( ( check.s_ingestion == CallChain::INTERMEDIATE ) &&
	 ( check.s_target == datacondAPI::udt::TARGET_WRAPPER ) )
    {
      std::ostringstream	msg;
      
      msg << "Cannot send arbitrary query results to target ("
	  << datacondAPI::udt::GetTargetType( check.s_target )
	  << ")";
      throw CallChain::BadIngestion( msg.str() );
    }

    break;
  case QUERY_NTUPLE:
    convert = false;
    check.s_simple = true;
    check.s_row_rule = ZERO_OR_MORE;
    check.s_allow_nulls = false;
    break;
  case QUERY_SPECTRUM:
    convert = true;
    check.s_simple = false;
    check.s_row_rule = ZERO_OR_MORE;
    break;
  }

  try {
    const ILwd::LdasContainer&	c
      ( dynamic_cast<const ILwd::LdasContainer&>(Element) );
  
    std::string msg("Trying to ingest ilwd as database table");
    // GenericAPI::AddLogMessage(msg, GenericAPI::MSG_DEBUG);

    std::vector<datacondAPI::udt*> data;
    std::vector<std::string> names;
    
    AT();
    try {
      std::unique_ptr< DB::Table >
	table( DB::Table::MakeTableFromILwd( c ) );
      
      AT();
      if ( ( convert ) && ( table->GetRowCount() == 1 ) )
      {
	try
	{
	  AT();
	  std::string*	results( ( Ingestion == CallChain::SYMBOL )
				 ? Results
				 : ( std::string* )NULL );

	  if ( IngestDatabaseSpectrumData( *table,
					   data,
					   names,
					   Symbol,
					   check.s_row_rule,
					   results ) )
	  {
	    return true;
	  }
	}
	catch ( const CallChain::LogicError& e )
	{
	  throw;
	}
	catch( ... )
	{
	}
	//---------------------------------------------------------------
	// Table is not appropriate as Spectrum data.
	//---------------------------------------------------------------
	AT();
	check_table( *table, check );
	try
	{
	  AT();
	  data.push_back( new datacondAPI::Database( *table,
						     check.s_query_type,
						     Element.getComment(),
						     true ) );
	}
	catch ( ... )
	{
	  AT();
	  // Do Nothing
	}
      }
      else
      {
	//---------------------------------------------------------------
	// Verify that the table meets other requirements
	//---------------------------------------------------------------
	AT();
	check_table( *table, check );
	try
	{
	  AT();
	  data.push_back( new datacondAPI::Database( *table,
						     check.s_query_type,
						     Element.getComment(),
						     true ) );
	}
	catch ( ... )
	{
	  // Do Nothing
	}
      }
    }
    catch ( const CallChain::LogicError& e )
    {
      throw;
    }
    catch ( ... )
    {
      // Not well defined table. Try generic table
      AT();
      std::unique_ptr< DB::Table >
	table( DB::Table::MakeTableFromILwd( c, true ) );
      std::string	table_name( table->GetName() );
      
      AT();
      if ( ( convert ) && ( table->GetRowCount() == 1 ) )
      {
	AT();
	try
	{
	  std::string*	results( ( Ingestion == CallChain::SYMBOL )
				 ? Results
				 : ( std::string* )NULL );
	  if ( IngestDatabaseSpectrumData( *table,
					   data,
					   names,
					   Symbol,
					   check.s_row_rule,
					   results ) )
	  {
	    return true;
	  }
	}
	catch ( const CallChain::LogicError& e )
	{
	  throw;
	}
	catch( ... )
	{
	}
	//---------------------------------------------------------------
	// Table is not appropriate as Spectrum data.
	//---------------------------------------------------------------
	AT();
	check_table( *table, check );
	try
	{
	  AT();
	  data.push_back( new datacondAPI::Database( *table,
						     check.s_query_type,
						     Element.getComment(),
						     true ) );
	}
	catch ( ... )
	{
	  AT();
	  // Do Nothing
	}
      }
      else // convert
      {
	AT();
	check_table( *table, check );
	try
	{
	  AT();
	  data.push_back( new datacondAPI::Database( *table,
						     check.s_query_type,
						     Element.getComment(),
						     true ) );
	}	// try
	catch ( ... )
	{
	  AT();
	  // Do Nothing
	}	// catch
      }	// convert
    }	// catch
    AT();
    StoreUDT( Symbol, data, Ingestion, true, Results,
	      IntermediateResult::NEVER_PRIMARY );
    AT();
    return true;
  }
  catch( const CallChain::LogicError& e )
  {
    // Rethrow this error as it means data input is bad
    throw;
  }
  catch( ... )
  {
    AT();
    return false;
  }
}


bool CallChain::
IngestDatabaseSpectrumData( const DB::Table& Table,
			    std::vector<datacondAPI::udt*>& Data,
			    std::vector<std::string>& Names,
			    const std::string& BaseName,
			    CallChain::db_ingestion_type RowRule,
			    std::string* Symbols )
{
  std::string	table_name( Table.GetName() );
  
  switch( RowRule )
  {
  case CallChain::EXACTLY_ONE:
    if ( Table.GetRowCount() != 1 )
    {
      throw WrongNumberOfRows( RowRule );
    }
    break;
  case CallChain::MORE_THAN_ZERO:
    if ( Table.GetRowCount() <= 0 )
    {
      throw WrongNumberOfRows( RowRule );
    }
    break;
  case ZERO_OR_MORE:
    // Accept anything
    break;
  }

  AT();
  if ( table_name == DB::SummSpectrum::TABLE_NAME )
  {
    AT();
    datacondAPI::WelchSpectrum<double>::CreateFromDBTable( Table,
							   Data,
							   Names );
  }
  else if ( table_name == DB::SummCSD::TABLE_NAME )
  {
    AT();
    datacondAPI::WelchCSDSpectrum<double>::CreateFromDBTable( Table,
							      Data,
							      Names );
  }
  else if ( datacondAPI::WelchCSDSpectrum<double>::ValidateDBTable( Table ) )
  {
    AT();
    datacondAPI::WelchCSDSpectrum<double>::CreateFromDBTable( Table,
							      Data,
							      Names,
							      true );
  }
  else if ( datacondAPI::WelchSpectrum<double>::
	    ValidateDBTable( Table ) )
  {
    AT();
    datacondAPI::WelchSpectrum<double>::CreateFromDBTable( Table,
							   Data,
							   Names,
							   true);
  }
  else
  {
    AT();
    return false;
  }
  if ( Table.GetRowCount() != Data.size() )
  {
    throw CallChain::LogicError( "Unable to convert all rows to spectrum" );
  }
  if ( Symbols )
  {
    AT();
    StoreUDT( BaseName, Data, CallChain::SYMBOL, true, Symbols );
  }
  else
  {
    std::string symbols;

    StoreUDT( BaseName, Data, CallChain::INTERMEDIATE, true,
	      &symbols,
	      IntermediateResult::NEVER_PRIMARY );
  }
  AT();
  return true;
}

static void
check_table( const DB::Table& Table, const check_table_type& Check )
{
  //---------------------------------------------------------------------
  // Check if arbitrary query is acceptable
  //---------------------------------------------------------------------

  if ( ( Check.s_query_type == CallChain::QUERY_GENERIC ) &&
       ( Check.s_ingestion == CallChain::INTERMEDIATE ) &&
       ( Check.s_target == datacondAPI::udt::TARGET_WRAPPER ) )
  {
    std::ostringstream	msg;
	  
    msg << "Cannot send arbitrary query results to target ("
	<< datacondAPI::udt::GetTargetType( Check.s_target )
	<< ")";
    throw CallChain::BadIngestion( msg.str() );
  }

  //---------------------------------------------------------------------
  // Check if only simple data types are allowed
  //---------------------------------------------------------------------
  if ( Check.s_simple )
  {
    unsigned int	stop( Table.GetFieldCount( ) );
    unsigned int	cnt = 0;

    while ( cnt < stop )
    {
      try
      {
	Table.GetFieldBlob( cnt++ );
	break;
      }
      catch( ... )
      {
	continue;
      }
    }
    if ( cnt != stop )
    {
      throw CallChain::TableHasNonSimpleTypes( );
    }
  }
  if ( ! Check.s_allow_nulls )
  {
    AT();
    for ( unsigned int	col_index( 0 );
	  col_index < Table.GetFieldCount( );
	  col_index++ )
    {
      AT();
      const DB::Field& column( Table.GetField( col_index ) );
      for ( DB::Table::rowoffset_type row_index( 0 );
	    row_index < column.Size( );
	    row_index++ )
      {
	AT();
	if ( column.IsNull( row_index ) )
	{
	  AT();
	  throw CallChain::NullFieldInDatabaseTable();
	}
      }
    }
  }
}
