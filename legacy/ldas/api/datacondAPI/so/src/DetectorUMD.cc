/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "datacondAPI/config.h"

#include "ilwdfcs/FrDetector.hh"

#include "DetectorUMD.hh"

using namespace datacondAPI;

void DetectorMetaData::
Read( const ILwdFCS::FrDetector& Source )
{
}

void DetectorMetaData::
Store( ILwdFCS::FrDetector& Storage ) const
{
}
