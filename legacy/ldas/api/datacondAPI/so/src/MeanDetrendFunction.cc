// $Id: MeanDetrendFunction.cc,v 1.4 2005/12/01 22:54:58 emaros Exp $
//
// "meandetrend" action
//
// [y = ] meandetrend(x)
//
// x: Sequence<T> or TimeSeries<T> where T is float, double,
//    complex<float>, complex<double> input sequence
// y: Sequence<T> or TimeSeries<T> where T is float, double
//    output sequence
//

#include "datacondAPI/config.h"

#include "MeanDetrendFunction.hh"
#include "Detrend.hh"

namespace {
    MeanDetrendFunction staticMeanDetrendFunction;
}

MeanDetrendFunction::MeanDetrendFunction()
    : Function( MeanDetrendFunction::GetName() )
{ }

//
// MeanDetrendFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "meandetrend". The Eval function recieves a pointer to the
// calling CallChain, and a list of symbol names (Params) and a return value
// symbol name (Ret).
//
void 
MeanDetrendFunction::Eval(      CallChain* Chain,
			  const CallChain::Params& Params,
                          const std::string& Ret) const
{
    if (Params.size() != 1)
    {
        throw CallChain::BadArgumentCount("meandetrend", 1, Params.size());
    }

    const CallChain::Symbol& in = *(Chain->GetSymbol(Params[0]));

    CallChain::Symbol* out_ptr = 0;
    datacondAPI::Detrend detrend(datacondAPI::Detrend::mean);

    detrend.apply(out_ptr, in);
    
    Chain->ResetOrAddSymbol( Ret, out_ptr );
}

const std::string&
MeanDetrendFunction::GetName() const
{
    static const std::string name("meandetrend");

    return name;
}
