#include "datacondAPI/config.h"

#include <stdexcept>
#include <memory>
#include <complex>

#include "general/Memory.hh"

#include "fft.hh"
#include "DataCondAPIConstants.hh"
#include "SequenceUDT.hh"
#include "DFTUDT.hh"
#include "FFTPlan.hh"

template<class TOut, class TIn>
void datacondAPI::FFT::dispatch(udt*& out, const Sequence<TIn>& in)
{
    // Create an unique_ptr in case we need a dynamic out, protects us
    // against exceptions thrown in apply()
    std::unique_ptr<DFT<TOut> > tmp(0);
    udt* tmp_out = out;

    if (tmp_out == 0)
    {
        tmp.reset(new DFT<TOut>);
        tmp_out = tmp.get();
    }
    else if (!udt::IsA<DFT<TOut> >(*tmp_out))
    {
        // Can't delete the output if it's the wrong type,
        // have to throw an exception instead
        throw std::invalid_argument( "FFT::apply() "
				     "output UDT must be a DFT" );
    }

    apply(udt::Cast<DFT<TOut> >(*tmp_out), in);

    // Caution - only alter 'out' if we created it
    if (out == 0)
    {
        // transfer ownership from the unique_ptr to 'out' so that the
        // DFT isn't deleted when the unique_ptr goes out of scope
        out = tmp.release();
    }
}

void datacondAPI::FFT::apply(udt*& out, const udt& in)
{
    if (udt::IsA<Sequence<float> >(in))
    {
        dispatch<std::complex<float>, float>(out,
            udt::Cast<Sequence<float> >(in));
    }
    else if (udt::IsA<Sequence<double> >(in))
    {
        dispatch<std::complex<double>, double>(out,
            udt::Cast<Sequence<double> >(in));
    }
    else if (udt::IsA<Sequence<std::complex<float> > >(in))
    {
        dispatch<std::complex<float>, std::complex<float> >(out,
            udt::Cast<Sequence<std::complex<float> > >(in));
    }
    else if (udt::IsA<Sequence<std::complex<double> > >(in))
    {
        dispatch<std::complex<double>, std::complex<double> >(out,
            udt::Cast<Sequence<std::complex<double> > >(in));
    }
    else
    {
        throw std::invalid_argument("FFT::apply() "
                                    "input UDT must be a Sequence");
    }
}

void datacondAPI::
FFT::apply(std::valarray<std::complex<float> >& out,
           const std::valarray<float>& in)
{
    //
    // Apply the Fast Fourier Transform to the *real* data in, putting
    // the result in out.
    //

    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take FFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take FFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
        out.resize(n);
    }

    float* const out_ptr = (float*)(&out[0]);

    const RealFloatFFTPlan plan(n, true);
    
    size_t i = 0;

    for (i = 0; i < n; ++i)
    {
        out_ptr[i] = in[i];
    }

    plan.apply(out_ptr);

    for (i = n/2 + 1; i < n; ++i)
    {
        out[i] = std::complex<float>(out_ptr[n-i], -out_ptr[i]);
    }
    
    if (n % 2 == 0)
    {
        out[n/2] = std::complex<float>(out_ptr[n/2], 0.0);
    }

    for (i = 1; i < (n+1)/2; ++i)
    {
        out[i] = conj(out[n-i]);
    }

    out[0] = std::complex<float>(out_ptr[0], 0.0);
}

void datacondAPI::
FFT::apply(std::valarray<std::complex<double> >& out,
           const std::valarray<double>&  in)
{
    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take FFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take FFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
        out.resize(n);
    }

    double* const out_ptr = (double*)(&out[0]);

    const RealDoubleFFTPlan plan(n, true);
    
    size_t i = 0;

    for (i = 0; i < n; ++i)
    {
        out_ptr[i] = in[i];
    }

    plan.apply(out_ptr);

    for (i = n/2 + 1; i < n; ++i)
    {
        out[i] = std::complex<double>(out_ptr[n-i], -out_ptr[i]);
    }
    
    if (n % 2 == 0)
    {
        out[n/2] = std::complex<double>(out_ptr[n/2], 0.0);
    }

    for (i = 1; i < (n+1)/2; ++i)
    {
        out[i] = conj(out[n-i]);
    }

    out[0] = std::complex<double>(out_ptr[0], 0.0);
}

void datacondAPI::
FFT::apply(std::valarray<std::complex<float> >&        out,
           const std::valarray< std::complex<float> >& in)
{
    //
    // Apply the Fast Fourier Transform to the *complex* data in, putting
    // the result in out.
    //

    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take FFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take FFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
        out.resize(n);
    }

    const ComplexFloatFFTPlan plan(n, true);

    out = in;

    plan.apply(&out[0]);
}

void datacondAPI::
FFT::apply(std::valarray<std::complex<double> >&              out,
           const std::valarray< std::complex<double> >& in)
{
    const size_t n = in.size();

    if (n <= 0)
    {
        throw std::invalid_argument("Attempt to take FFT of zero-length sequence");
    }
    else if (n > datacondAPI::MaximumFFTLength)
    {
        throw std::invalid_argument("Attempt to take FFT of sequence longer "
                                    "than MaximumFFTLength");
    }

    if (out.size() != n)
    {
        out.resize(n);
    }

    const ComplexDoubleFFTPlan plan(n, true);

    out = in;

    plan.apply(&out[0]);
}
