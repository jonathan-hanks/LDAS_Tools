/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef STATSUDT_HH
#define STATSUDT_HH
// $Id: StatsUDT.hh,v 1.19 2006/02/16 16:54:58 emaros Exp $

#include "SequenceUDT.hh"
#include "StatsUMD.hh"
#include "WhenUMD.hh"

namespace DB
{
  class Table;
}

namespace datacondAPI
{
  //: Object that holds statistics on a sequence
  class Stats : public Sequence<double>, 
		public StatsMetaData, 
		public ArbitraryWhenMetaData {

  public:
        
    //: Offsets of moments
    enum {
      MEAN,
      VARIANCE,
      SKEWNESS,
      KURTOSIS,
      MOMENTS_MAX
    };

    //: Default constructor
    Stats();

    //: Specify everything constructor
    //!param: size - the size of the Sequence from which *this was created
    //!param: min - value of the minimum element in the Sequence from which
    //+ this was created
    //!param: max - value of the maximum element in the Sequence from which
    //+ this was created
    //!param: rms - the root-mean-square of the Sequence from which *this
    //+ was created
    //!param: moments - first several moments of the sequence, must have
    //+ size MOMENTS_MAX
    Stats(const unsigned int&          size,
	  const double&                min,
	  const double&                max,
	  const double&                rms,
	  const std::valarray<double>& moments);

    //: destructor
    virtual ~Stats();

    // UDT required members

    //: Duplicate this on heap
    //!return: Stats* - duplicate of *this allocated on heap
    virtual Stats* Clone() const;
    
    //: Convert object to ILWD
    //!param: Chain - CallChain *this is part of 
    //!param: Target - destination this ILWD is meant for
    //!return: ILwd::LdasElement* - ILWD allocated on heap, or null pointer if 
    //+ can't convert
    virtual ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target 
		   = datacondAPI::udt::TARGET_GENERIC ) const;

    virtual void ConvertToIlwd(const CallChain& Chain,
			       target_type Target,
			       ILwd::LdasContainer* Container) const;
  }; // class Stats
    
  inline void Stats::
  ConvertToIlwd(const CallChain& Chain,
		target_type Target,
		ILwd::LdasContainer* Container) const
  {
    Sequence<double>::ConvertToIlwd( Chain, Target, Container );
  }
} // namespace datacondAPI

#endif // STATSUDT_HH
