#include "datacondAPI/config.h"

#include "general/types.hh"

#include "general/gpstime.hh"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwdfcs/FrHistory.hh"

#include "GapUMD.hh"

#if 0
#define AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define	AT()
#endif

using namespace datacondAPI;
using General::GPSTime;

using std::ceil;
using std::floor;

template<class Unit_type>
GapMetaData<Unit_type>::Gap_type::
Gap_type( )
  : start( ),
    end( )
{
}

template<class Unit_type>
GapMetaData<Unit_type>::Gap_type::
Gap_type( const Gap_type& Source )
  : start( Source.start ),
    end( Source.end )
{
}

template<class Unit_type>
GapMetaData<Unit_type>::Gap_type::
Gap_type( const Unit_type& Start, const Unit_type& End )
  : start( Start ),
    end( End )
{
}

template<class Unit_type>
GapMetaData<Unit_type>::GapInterval_type::
GapInterval_type( )
  : start( ),
    stop( )
{
}

template<class Unit_type>
GapMetaData<Unit_type>::GapInterval_type::
GapInterval_type( const GapInterval_type& Source )
  : start( Source.start ),
    stop( Source.stop )
{
}

template<class Unit_type>
GapMetaData<Unit_type>::GapInterval_type::
GapInterval_type( const INT_4U Start, const INT_4U Stop )
  : start( Start ),
    stop( Stop )
{
}

template<class Unit_type>
GapMetaData<Unit_type>::
GapMetaData()
{
    m_fill_method = GapFill::ZERO;
}

template<class Unit_type>
GapMetaData<Unit_type>::
~GapMetaData()
{
}

template<class Unit_type>
void
GapMetaData<Unit_type>::
GapSetFillMethod(const GapFill::FillMethod_type fill_method)
{
  m_fill_method = fill_method;
}

template<class Unit_type>
GapFill::FillMethod_type
GapMetaData<Unit_type>::
GapGetFillMethod() const
{
  return m_fill_method;
}

template<class Unit_type>
void GapMetaData<Unit_type>::
GapAppendDataRange( const Unit_type& Start,
		  const Unit_type& Stop )
{
  AT();
  if ( Start > Stop )
  {
    AT();
    throw std::invalid_argument( "Stop occurs before Start" );
  }
  Unit_type	start(Start);
  Unit_type	stop(Stop);

  AT();
  if ( Start == GapGetDataBegin() )
  {
    AT();
    start -= GetStepSize();
  }
  AT();
  if ( Stop == GapGetDataEnd() )
  {
    AT();
    stop += GetStepSize();
  }

  iterator g = m_gaps.begin();
  while ( g != m_gaps.end() )
  {
    AT();
    if ( ( (*g).start >= start ) && ( (*g).end <= stop ) )
    {
      // Case where gap is contained in the data set (remove gap)
      AT();
      g = m_gaps.erase(g);
      if ( g == m_gaps.end() )
      {
	// Removed the gap at the end. Need to terminate the loop
	//   so we do not extend beyond the end.
	break;
      }
    }
    else if ( ( (*g).start < start ) && ( (*g).end > stop ) )
    {
      // case where data is contained in the gap (Split the gap in two).
      AT();
      g = m_gaps.insert( g, Gap_type( (*g).start, start ) );
      AT();
      g++;
      AT();
      (*g).start = stop;
    }
    else if ( ( (*g).start >= start ) && ( (*g).start <= stop ) )
    {
      // case of data overlapping start of gap ( Shift start of gap to
      //  be end of data )
      AT();
      (*g).start = stop;
    }
    else if ( ( (*g).end >= start ) && ( (*g).start <= stop ) )
    {
      // case of data overlapping end of gap ( Shift end of gap to
      //  be start of data )
      AT();
      (*g).end = start;
    }
    AT();
    g++;
  }
  AT();
}

template<class Unit_type>
void GapMetaData<Unit_type>::
GapAppendGapRange( const Unit_type& Start,
		   const Unit_type& Stop )
{
  if ( Start > Stop )
  {
    throw std::invalid_argument( "Stop occurs before Start" );
  }

  Unit_type	start(Start);
  Unit_type	stop(Stop);

  start -= GetStepSize();
  stop += GetStepSize();

  iterator g = m_gaps.begin();
  while ( g != m_gaps.end() )
  {
    if ( ( (*g).end ) == start )
    {
      // Expand 
      (*g).end = Stop;	
      return;
    }
    if ( (*g).start == ( stop ) )
    {
      // Expand 
      (*g).start = start;
      return;
    }
    if ( stop < (*g).start )
    {
      break;
    }
    g++;
  }
  g = m_gaps.insert( g, Gap_type(start, stop) );
}

template<class Unit_type>
void GapMetaData<Unit_type>::
CalcTransitions( Transitions_type& Gap ) const
{
  const Unit_type	t_start( GapGetDataBegin() );
  double	t_dt( GetStepSize() );
  GapInterval_type	transition;

  Gap.resize(0);
  for ( const_iterator
	  i = m_gaps.begin( ),
	  last = m_gaps.end( );
	i != last;
	++i )
  {
    double start = (*i).start + t_dt - t_start;

    if ( start < 0 )
    {
      start = 0;
    }
    transition.start = 
      (INT_4U)( floor( start / t_dt ) );
    transition.stop = ( INT_4U)( ceil( ( (*i).end - t_start ) / t_dt ) );
    if ( transition.stop > GapGetDataSize() )
    {
      transition.stop = GapGetDataSize();
    }
    Gap.push_back( transition );
  }
}

template<class Unit_type>
void GapMetaData<Unit_type>::
CalcTransitionsOfData( Transitions_type& Data ) const
{
  const Unit_type	t_start( GapGetDataBegin() );
  double	t_dt( GetStepSize() );
  GapInterval_type	transition;

  Data.resize(0);
  transition.start = 0;

  if ( (m_gaps.size() > 0) &&
       ( ( m_gaps.front().start + t_dt - t_start ) <= 0 ) )
  {
    transition.start = ( INT_4U)( ceil( ( m_gaps.front().end - t_start )
					/ t_dt ) );
    if ( transition.start > GapGetDataSize() )
    {
      transition.start = GapGetDataSize();
    }
  }
  for ( const_iterator
	  i = m_gaps.begin( ),
	  last = m_gaps.end( );
	i != last;
	++i )
  {
    double start = (*i).start + t_dt - t_start;

    INT_4U	gap_start, gap_stop;

    if ( start < 0 )
    {
      start = 0;
    }
    transition.stop = gap_start =
      (INT_4U)( floor( start / t_dt ) );
    if ( transition.stop != 0 )
    {
      Data.push_back( transition );
    }
    transition.start = gap_stop = ( INT_4U)( ceil( ( (*i).end - t_start ) / t_dt ) );
    if ( transition.start > GapGetDataSize() )
    {
      transition.start = GapGetDataSize();
    }
  }
  if ( transition.start != GapGetDataSize() )
  {
    transition.stop = GapGetDataSize();
    Data.push_back( transition );
  }
}

template<class Unit_type>
void GapMetaData<Unit_type>::
GapReset( bool AllGap )
{
  AT();
  m_gaps.resize(0);
  AT();
  if ( AllGap )
  {
    AT();
    m_gaps.push_back( Gap_type( GapGetDataBegin() - GetStepSize(),
			     GapGetDataEnd() + GetStepSize() ) );
    AT();
  }
}

namespace datacondAPI
{
  template<>
  void GapMetaData<GPSTime>::
  Store( ILwd::LdasContainer& Container ) const
  {
    if ( m_gaps.size() <= 0 )
    {
      // No need to generate any history since there are no gaps.
      return;
    }
    ILwd::LdasContainer*	history_record = new ILwd::LdasContainer( "gap_info" );
    INT_4U	t[4];

    history_record->push_back( new ILwd::LdasArray<INT_4U>
			       ( m_fill_method , "fill_method"),
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::OWN );
    for ( const_iterator g = m_gaps.begin(); g != m_gaps.end(); g++ )
    {
      t[0] = (*g).start.GetSeconds();
      t[1] = (*g).start.GetNanoseconds();
      t[2] = (*g).end.GetSeconds();
      t[3] = (*g).end.GetNanoseconds();

      history_record->push_back( new ILwd::LdasArray<INT_4U>
				 ( t, 4, "time_range"),
				 ILwd::LdasContainer::NO_ALLOCATE,
				 ILwd::LdasContainer::OWN );
    }

    ILwd::LdasContainer*	history = new ILwd::LdasContainer( "LDAS_History" );

    history->push_back( history_record,
			ILwd::LdasContainer::NO_ALLOCATE,
			ILwd::LdasContainer::OWN );
    Container.push_back( history,
			 ILwd::LdasContainer::NO_ALLOCATE,
			 ILwd::LdasContainer::OWN );
  }

  template<>
  void GapMetaData<GPSTime>::
  Store( ILwdFCS::FrHistory& History ) const
  {
  }

#undef CLASS_INSTANTIATION
#define	INSTANTIATE(lm_units) \
template class GapMetaData<lm_units>

  INSTANTIATE(GPSTime);
} // namespace - datacondAPI

