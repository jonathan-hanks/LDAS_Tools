// Wavelet Analysis Tool
//$Id: Wavelet.hh,v 1.5 2006/02/16 16:54:58 emaros Exp $
#ifndef WAVELET_HH
#define WAVELET_HH

#include "UDT.hh"

namespace datacondAPI {
namespace wat {

#define PI 3.141592653589793

//: constants which rule the boundary processing
enum BORDER {B_PAD_ZERO, 
	     B_CYCLE, 
	     B_MIRROR, 
	     B_PAD_EDGE, 
	     B_POLYNOM};


//: wavelet types
enum WAVETYPE {HAAR, 
	       BIORTHOGONAL,
	       DAUBECHIES, 
	       SYMLET, 
	       DMEYER};

class Wavelet : public udt 			// Wavelet base class
  //class Wavelet 			// Wavelet base class
{
   public:

// constructors

      //: constructor
      Wavelet(int mH=1, int mL=1, int tree=0, enum BORDER border=B_CYCLE);

      //: copy constructor
      Wavelet(const Wavelet &);

      //: Virtual destructor
      virtual ~Wavelet();

      //: duplicate on heap
      //!return: Wavelet* - duplicate of *this, allocated on heap
      virtual Wavelet* Clone() const;

      //: Convert *this to an ILWD
      //!param: Chain - CallChain *this is part of
      //!param: Target - intended destination of ILWD
      //!return: ILwd::LdasElement* - ILWD allocated on heap or null pointer
      virtual ILwd::LdasElement*
      ConvertToIlwd( const CallChain& Chain,
		     datacondAPI::udt::target_type 
		     Target = datacondAPI::udt::TARGET_GENERIC) const;

// access functions

      //: get array index of the first sample for (level,layer)
      virtual int getOffset(int,int);             
      //: get frequency index for (level,layer)
      virtual int convertL2F(int,int);
      //: get layer index for (level,frequency index)
      virtual int convertF2L(int,int);

// mutators

      //: set level
      inline virtual void reset() { m_Level = 0; }
      inline virtual void setLevel(int level) { m_Level = level; };
      inline virtual  int getMaxLevel(int);

      //: check type of wavelet tree
      inline bool BinaryTree(){ return (m_TreeType) ? true : false; }

// data members

      //: wavelet type
      enum WAVETYPE m_WaveType;

      //: borders handling: see BORDER constants definitions above
      enum BORDER m_Border;

      //: wavelet tree type: 0-diadic, 1-binary tree
      int m_TreeType;

      //: current level of decomposition
      int m_Level;              

      //: number of highpass wavelet filter coefficients
      int m_H;

      //: number of lowpass wavelet filter coefficients
      int m_L;

}; // class Wavelet

// inlines
inline int Wavelet::getMaxLevel(int n)
{
   int maxLevel = 0;
   for(; (n>=2*m_H) && (n>=2*m_L) && !(n&1); n/=2) maxLevel++;
   return maxLevel;
}

// WAT functions

// Calculates polynomial interpolation coefficient using Lagrange formula.
inline double Lagrange(const int n, const int i, const double x)
{
    double c = 1.;
    double xn = x+n/2.-0.5;	// shift to the center of interpolation interval

    for(int j=0; j<n; j++) 
       if(j!=i) c *= (xn-j)/(i-j);

    return c;
}

// Nevill's polynomial interpolation.
// x - polynom argument (x stride is allways 1) 
// n - number of interpolation samples
// p - pointer to a sample with x=0.
// q - double array of length n
template<class DataType_>
inline double Nevill(const double x0,
		    int n,   
		    DataType_* p,
		    double* q)
{
   register int i;
   register double x = x0;
   register double xm = 0.5;

   n--;
   *q = *p;

   for(i=0; i<n; i++)
      q[i] = p[i] + (x--)*(p[i+1]-p[i]);

   while(--n >= 1){
      x = x0;

      q[0] += xm*(x--)*(q[1]-q[0]);
      if(n == 1) goto M0;
      q[1] += xm*(x--)*(q[2]-q[1]);
      if(n == 2) goto M0;
      q[2] += xm*(x--)*(q[3]-q[2]);
      if(n == 3) goto M0;
      q[3] += xm*(x--)*(q[4]-q[3]);
      if(n == 4) goto M0;
      q[4] += xm*(x--)*(q[5]-q[4]);
      if(n == 5) goto M0;
      q[5] += xm*(x--)*(q[6]-q[5]);
      if(n == 6) goto M0;

      for(i=6; i<n; i++)
	 q[i] += xm*(x--)*(q[i+1]-q[i]);

M0:   xm /= (1.+xm);
   }

   return *q;
}

} // end namespace wat
} // end namespace datacondAPI

#endif // WAVELET_HH
