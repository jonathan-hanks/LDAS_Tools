#ifndef _RAN0FUNCTION_HH_
#define _RAN0FUNCTION_HH_

#include "CallChain.hh"

class ran0Function : public CallChain::Function
{
public:
  ran0Function();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //_RAN0FUNCTION_HH_
