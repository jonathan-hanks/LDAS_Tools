/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "datacondAPI/config.h"

#include <memory>
#include <sstream>

#include "general/Memory.hh"
#include "general/unordered_map.hh"

#include "filters/valarray_utils.hh"

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "DFTUDT.hh"
#include "util.hh"

#include "Abs.hh"

using std::complex;
using std::unique_ptr;   
using General::unordered_map;   
using namespace datacondAPI;

namespace {

    //
    //
    // The trivial converter is used when the udt is real.
    //
    template<class T>
    udt* convertRealScalar(const udt& udt_in)
    {
        const Scalar<T>& in = dynamic_cast<const Scalar<T>&>(udt_in);
        unique_ptr<Scalar<T> > p(in.Clone());
        
        p->SetValue(std::abs(in.GetValue()));
        
        return p.release();
    }

    template<class T>
    udt* convertRealSequence(const udt& udt_in)
    {
        const Sequence<T>& in = dynamic_cast<const Sequence<T>&>(udt_in);
        unique_ptr<Sequence<T> > p(in.Clone());
        
        *p = std::abs(in);
        
        return p.release();
    }

    //
    // The following functions are the specific conversion routines
    // which are used once the exact dynamic type of an object is
    // determined. Note that derived classes have their own converters
    // but also use the converters of their ancestors.
    //

    //
    // Convert a complex Scalar
    //
    template<class T> inline
    void convert_specific(Scalar<T>& out,
                          const Scalar<complex<T> >& in)
    {
        out.SetValue(std::abs(in.GetValue()));
    }

    //
    // Convert the Sequence part of an object
    //
    template<class T> inline
    void convert_specific(Sequence<T>& out,
                          const Sequence<complex<T> >& in)
    {
        // Do UDT part
        out.udt::operator=(in);

        // Do valarray part.
        out = Filters::abs(in);
    }

    template<class T> inline
    void convert_specific(TimeSeries<T>& out,
                          const TimeSeries<complex<T> >& in)
    {
        convert_specific(static_cast<Sequence<T>&>(out),
                         static_cast<const Sequence<complex<T> >&>(in));
        
        out.TimeSeriesMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(FrequencySequence<T>& out,
                          const FrequencySequence<complex<T> >& in)
    {
        convert_specific(static_cast<Sequence<T>&>(out),
                         static_cast<const Sequence<complex<T> >&>(in));
        
        out.FrequencyMetaData::operator=(in);
        out.GeometryMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(TimeBoundedFreqSequence<T>& out,
                          const TimeBoundedFreqSequence<complex<T> >& in)
    {
        convert_specific(static_cast<FrequencySequence<T>&>(out),
                       static_cast<const FrequencySequence<complex<T> >&>(in));
        
        out.TimeBoundedFreqSequenceMetaData::operator=(in);
    }

    template<class T> inline
    void convert_specific(CSDSpectrum<T>& out,
                          const CSDSpectrum<complex<T> >& in)
    {
        convert_specific(static_cast<TimeBoundedFreqSequence<T>&>(out),
                 static_cast<const TimeBoundedFreqSequence<complex<T> >&>(in));
        
        out.CSDSpectrumUMD::operator=(in);
    }

    template<class T> inline
    void convert_specific(WelchSpectrum<T>& out,
                          const WelchSpectrum<complex<T> >& in)
    {
        convert_specific(static_cast<CSDSpectrum<T>&>(out),
                         static_cast<const CSDSpectrum<complex<T> >&>(in));
        
        out.WelchSpectrumUMD::operator=(in);
    }

    template<class T> inline
    void convert_specific(WelchCSDSpectrum<T>& out,
                          const WelchCSDSpectrum<complex<T> >& in)
    {
        convert_specific(static_cast<CSDSpectrum<T>&>(out),
                         static_cast<const CSDSpectrum<complex<T> >&>(in));
        
        out.WelchCSDSpectrumUMD::operator=(in);
    }
    
    //
    // This is the top level dispatcher - different versions of
    // this function are instantiated and inserted into a map
    // which takes type_info to an appropriate function for
    // converting TIn to TOut.
    //
    template<class TOut, class TIn>
    udt* convert(const udt& udt_in)
    {
        const TIn& in = dynamic_cast<const TIn&>(udt_in);
        unique_ptr<TOut> out(new TOut());
        
        convert_specific(*out, in);
        
        return out.release();
    }

    //
    // These are the prototypes that all functions contained in the map
    // must adhere to. Note that the function uses UDT arguments because
    // all the functions contained in the map must have the same signature,
    // so a templated function wouldn't work.
    //
    // All conversion functions take a single UDT argument and return a
    // pointer to a UDT at the new precision created on the heap
    //
    typedef udt* (*ConversionFn)(const udt& in);
    
    //
    // A ConversionFnMap is a unordered_map that uses a type_info*
    // as the key and contains ConversionFn's as its elements
    //
    typedef unordered_map< const std::type_info*, ConversionFn,
			   LookupHash, LookupEq > ConversionFnMap;
    
    //
    // A function that we can use to initialise the unordered_map when the
    // library is loaded
    //
    const ConversionFnMap& initAbsFnMap()
    {
        static ConversionFnMap absFnMap;
        
        // Lets be ultra-paranoid
        if (absFnMap.size() != 0)
        {
            return absFnMap;
        }
        
        //
        // Now we insert the function pointers into the map by typeid
        //

        // Trivial conversions for things that are already real - 
        // they are "converted" via cloning

        // Trivial Scalar
        absFnMap[&typeid(Scalar<int>)] = convertRealScalar<int>;
        absFnMap[&typeid(Scalar<float>)] = convertRealScalar<float>;
        absFnMap[&typeid(Scalar<double>)] = convertRealScalar<double>;

        // Trivial Sequence
        absFnMap[&typeid(Sequence<float>)] = convertRealSequence<float>;
        absFnMap[&typeid(Sequence<double>)] = convertRealSequence<double>;

        // Trivial TimeSeries
        absFnMap[&typeid(TimeSeries<float>)] = convertRealSequence<float>;
        absFnMap[&typeid(TimeSeries<double>)] = convertRealSequence<double>;

        // Trivial FrequencySequence
        absFnMap[&typeid(FrequencySequence<float>)]
            = convertRealSequence<float>;
        absFnMap[&typeid(FrequencySequence<double>)]
            = convertRealSequence<double>;;

        // Trivial TimeBoundedFreqSequence
        absFnMap[&typeid(TimeBoundedFreqSequence<float>)]
            = convertRealSequence<float>;
        absFnMap[&typeid(TimeBoundedFreqSequence<double>)]
            = convertRealSequence<double>;

        // Trivial CSDSpectrum
        absFnMap[&typeid(CSDSpectrum<float>)] = convertRealSequence<float>;
        absFnMap[&typeid(CSDSpectrum<double>)] = convertRealSequence<double>;

        // Trivial WelchSpectrum (real only)
        absFnMap[&typeid(WelchSpectrum<float>)]
            = convertRealSequence<float>;
        absFnMap[&typeid(WelchSpectrum<double>)]
            = convertRealSequence<double>;

        // Trivial WelchCSDSpectrum
        absFnMap[&typeid(WelchCSDSpectrum<float>)]
            = convertRealSequence<float>;
        absFnMap[&typeid(WelchCSDSpectrum<double>)]
            = convertRealSequence<double>;

        // Trivial DFT - none, there aren't any real ones

        // Non-trivial conversions

        // Scalar
        absFnMap[&typeid(Scalar<complex<float> >)]
            = convert<Scalar<float>, Scalar<complex<float> > >;
        absFnMap[&typeid(Scalar<complex<double> >)]
            = convert<Scalar<double>, Scalar<complex<double> > >;

        // Sequence
        absFnMap[&typeid(Sequence<complex<float> >)]
            = convert<Sequence<float>, Sequence<complex<float> > >;
        absFnMap[&typeid(Sequence<complex<double> >)]
            = convert<Sequence<double>, Sequence<complex<double> > >;
        
        // TimeSeries
        absFnMap[&typeid(TimeSeries<complex<float> >)]
         = convert<TimeSeries<float>, TimeSeries<complex<float> > >;
        absFnMap[&typeid(TimeSeries<complex<double> >)]
         = convert<TimeSeries<double>, TimeSeries<complex<double> > >;

        // FrequencySequence
        absFnMap[&typeid(FrequencySequence<complex<float> >)]
            = convert<FrequencySequence<float>,
                      FrequencySequence<complex<float> > >;
        absFnMap[&typeid(FrequencySequence<complex<double> >)]
            = convert<FrequencySequence<double>,
                      FrequencySequence<complex<double> > >;

        // TimeBoundedFreqSequence
        absFnMap[&typeid(TimeBoundedFreqSequence<complex<float> >)]
            = convert<TimeBoundedFreqSequence<float>,
                      TimeBoundedFreqSequence<complex<float> > >;
        absFnMap[&typeid(TimeBoundedFreqSequence<complex<double> >)]
            = convert<TimeBoundedFreqSequence<double>,
                      TimeBoundedFreqSequence<complex<double> > >;

        // CSDSpectrum
        absFnMap[&typeid(CSDSpectrum<complex<float> >)]
            = convert<CSDSpectrum<float>,
                      CSDSpectrum<complex<float> > >;
        absFnMap[&typeid(CSDSpectrum<complex<double> >)]
            = convert<CSDSpectrum<double>,
                      CSDSpectrum<complex<double> > >;

        // WelchSpectrum's are real only
        
        // WelchCSDSpectrum
        absFnMap[&typeid(WelchCSDSpectrum<complex<float> >)]
            = convert<WelchCSDSpectrum<float>,
                      WelchCSDSpectrum<complex<float> > >;
        absFnMap[&typeid(WelchCSDSpectrum<complex<double> >)]
            = convert<WelchCSDSpectrum<double>,
                      WelchCSDSpectrum<complex<double> > >;

        // DFT - they only come in complex, so we have to convert them
        // to a Sequence.
        absFnMap[&typeid(DFT<complex<float> >)]
            = convert<Sequence<float>, Sequence<complex<float> > >;
        absFnMap[&typeid(DFT<complex<double> >)]
            = convert<Sequence<double>, Sequence<complex<double> > >;
        
        return absFnMap;
    }

    //
    // Static objects
    //
    
    //
    // This is the object we use to look up conversion functions. It is a
    // const reference to a static unordered_maps contained inside the
    // initAbsFnMap() function. It's about as threadsafe as possible
    // because a) it's a reference and so must be initialised or the compiler
    // will complain b) it's initialised when the library is loaded
    // and c) it's const
    //
    static const ConversionFnMap& theAbsFnMap = initAbsFnMap();
    
#if __SUNPRO_CC
    //-------------------------------------------------------------------
    // Need to instantiate the function objects
    //-------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE(A) \
    template udt* convertRealScalar< A >(const udt& udt_in)

    INSTANTIATE(int);
    INSTANTIATE(float);
    INSTANTIATE(double);

#undef INSTANTIATE

#define INSTANTIATE(A) \
    template udt* convertRealSequence< A >(const udt& udt_in)

    INSTANTIATE(float);
    INSTANTIATE(double);

#undef INSTANTIATE
#define INSTANTIATE(A,B) template udt* convert< A, B >(const udt& udt_in)

    INSTANTIATE(Scalar<float>, Scalar<complex<float> >);
    INSTANTIATE(Scalar<double>, Scalar<complex<double> >);
    INSTANTIATE(Sequence<float>, Sequence<complex<float> >);
    INSTANTIATE(Sequence<double>, Sequence<complex<double> > );
    INSTANTIATE(TimeSeries<float>, TimeSeries<complex<float> >);
    INSTANTIATE(TimeSeries<double>, TimeSeries<complex<double> >);
    INSTANTIATE(FrequencySequence<float>,
		FrequencySequence<complex<float> >);
    INSTANTIATE(FrequencySequence<double>,
		FrequencySequence<complex<double> >);
    INSTANTIATE(TimeBoundedFreqSequence<float>,
		TimeBoundedFreqSequence<complex<float> >);
    INSTANTIATE(TimeBoundedFreqSequence<double>,
		TimeBoundedFreqSequence<complex<double> >);
    INSTANTIATE(CSDSpectrum<float>, CSDSpectrum<complex<float> >);
    INSTANTIATE(CSDSpectrum<double>,
                      CSDSpectrum<complex<double> >);
    INSTANTIATE(WelchCSDSpectrum<float>,
                      WelchCSDSpectrum<complex<float> >);
    INSTANTIATE(WelchCSDSpectrum<double>,
                      WelchCSDSpectrum<complex<double> >);
    INSTANTIATE(Sequence<float>, Sequence<complex<float> >);
    INSTANTIATE(Sequence<double>, Sequence<complex<double> >);

#undef INSTANTIATE
#endif
    
} // anonymous namespace

namespace datacondAPI {

    udt* Abs(const udt& in)
    {
        //
        // Look up the corresponding conversion function in the function table
        //
        ConversionFnMap::const_iterator iter
            = theAbsFnMap.find(&typeid(in));

        if (iter == theAbsFnMap.end())
        {
            // There was no function in the table for an
            // object of this type - throw an exception
            std::ostringstream oss;
            oss << "No Abs() operator for "
                << TypeInfoTable.GetName(typeid(in));

            throw std::invalid_argument(oss.str());
        }

        // Now call the function, which is the second element
        // of the pair obtained in the map lookup
        return (*iter).second(in);
    }

} // namespace datacondAPI

