/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "datacondAPI/config.h"

#include "general/Memory.hh"

#include "InterpolateFunction.hh"
#include "InterpolateState.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "TypeInfo.hh"

//=============================================================================
// InterpolateFunction
//=============================================================================

using namespace std;
using namespace datacondAPI;

namespace {

  const char* const rcsId = "#(@) $Id: InterpolateFunction.cc,v 1.6 2007/01/24 17:51:27 emaros Exp $";

  const InterpolateFunction staticInterpolateFunction;

  template<class T>
  bool dispatchTimeSeries(const udt* const xUdt,
                          const double alpha,
                          const size_t order,
                          unique_ptr<udt>& yUdt,
                          unique_ptr<udt>& zUdt)
  {
    if (const TimeSeries<T>* const p
        = dynamic_cast<const TimeSeries<T>*>(xUdt))
    {
      unique_ptr<InterpolateState<T> > z(new InterpolateState<T>(alpha, order));
      unique_ptr<TimeSeries<T> > y(p->Clone());
    
      z->apply(*y);

      yUdt.reset( y.release( ) );
      zUdt.reset( z.release( ) );

      return true;
    }
    else
    {
      return false;
    }
  }

  template<class T>
  bool dispatchSequence(const udt* const xUdt,
                        const double alpha,
                        const size_t order,
                        unique_ptr<udt>& yUdt,
                        unique_ptr<udt>& zUdt)
  {
    if (const Sequence<T>* const p = dynamic_cast<const Sequence<T>*>(xUdt))
    {
      unique_ptr<InterpolateState<T> > z(new InterpolateState<T>(alpha, order));
      unique_ptr<Sequence<T> > y(p->Clone());
    
      z->apply(*y);

      yUdt.reset( y.release( ) );
      zUdt.reset( z.release( ) );

      return true;
    }
    else
    {
      return false;
    }
  }

  template<class T>
  bool dispatchTimeSeries(const udt* const xUdt,
                          udt* const zUdt,
                          unique_ptr<udt>& yUdt)
  {
    if (const TimeSeries<T>* const p
        = dynamic_cast<const TimeSeries<T>*>(xUdt))
    {
      if (InterpolateState<T>* const z
          = dynamic_cast<InterpolateState<T>*>(zUdt))
      {
        unique_ptr<TimeSeries<T> > y(p->Clone());
        
        z->apply(*y);

        yUdt.reset( y.release( ) );

        return true;
      }
      else
      {
        throw CallChain::BadArgument("interpolate", 2,
                        TypeInfoTable.GetName(typeid(*zUdt)),
                        TypeInfoTable.GetName(typeid(InterpolateState<T>)));
      }
    }
    else
    {
      return false;
    }
  }

  template<class T>
  bool dispatchSequence(const udt* const xUdt,
                        udt* const zUdt,
                        unique_ptr<udt>& yUdt)
  {
    if (const Sequence<T>* const p
        = dynamic_cast<const Sequence<T>*>(xUdt))
    {
      if (InterpolateState<T>* const z
          = dynamic_cast<InterpolateState<T>*>(zUdt))
      {
        unique_ptr<Sequence<T> > y(p->Clone());
        
        z->apply(*y);

        yUdt.reset( y.release( ) );

        return true;
      }
      else
      {
        throw CallChain::BadArgument("interpolate", 2,
                        TypeInfoTable.GetName(typeid(*zUdt)),
                        TypeInfoTable.GetName(typeid(InterpolateState<T>)));
      }
    }
    else
    {
      return false;
    }
  }
  
    //-------------------------------------------------------------------
    // Need to instantiate the function objects
    //-------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE(T) \
  template \
  bool dispatchTimeSeries< T >( const udt* const xUdt, \
				const double alpha, \
				const size_t order, \
				unique_ptr<udt>& yUdt, \
				unique_ptr<udt>& zUdt )
  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );
				  

#undef INSTANTIATE
#define INSTANTIATE(T) \
  template \
  bool dispatchSequence< T >( const udt* const xUdt, \
			      const double alpha, \
			      const size_t order, \
			      unique_ptr<udt>& yUdt, \
			      unique_ptr<udt>& zUdt )

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
#define INSTANTIATE(T) \
  template \
  bool dispatchSequence< T >( const udt* const xUdt, \
			      udt* const zUdt, \
			      unique_ptr<udt>& yUdt )

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
#define INSTANTIATE(T) \
  template \
  bool dispatchTimeSeries< T >( const udt* const xUdt, \
				udt* const zUdt, \
				unique_ptr<udt>& yUdt )

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
} // anonymous namespace

const string& InterpolateFunction::GetName() const
{
  const static string name("interpolate");
  
  return name;
}

InterpolateFunction::InterpolateFunction()
  : Function(InterpolateFunction::GetName())
{ }

//
// InterpolateFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "interpolate". The Eval function recieves a pointer to the
// calling CallChain, and a list of symbol names (params) and a return value
// symbol name (Ret).
//
void InterpolateFunction::Eval(CallChain* chain,
                               const CallChain::Params& params,
                               const string& ret) const
{
  bool ok = false;
  unique_ptr<udt> y;

  switch(params.size())
  {
  case 4:
  case 3:
    {
      const udt* const orderUdt = chain->GetSymbol(params[2]);
      const udt* const alphaUdt = chain->GetSymbol(params[1]);
      const udt* const xUdt = chain->GetSymbol(params[0]);
      
      unique_ptr<udt> z;

      double alpha = 0.0;
      int order = 0;
    
      if (const Scalar<double>* const p
          = dynamic_cast<const Scalar<double>*>(alphaUdt))
      {
        alpha = p->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(), 1,
                               TypeInfoTable.GetName(typeid(*alphaUdt)),
                               TypeInfoTable.GetName(typeid(Scalar<double>)));
      }

      if (const Scalar<int>* const p
          = dynamic_cast<const Scalar<int>*>(orderUdt))
      {
        order = p->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(), 2,
                               TypeInfoTable.GetName(typeid(*orderUdt)),
                               TypeInfoTable.GetName(typeid(Scalar<int>)));
      }

      // Since we convert order to an unsigned here, we need to check
      // that its non-negative first
      if (order < 0)
      {
        throw invalid_argument("Illegal Argument: action interpolate(); "
                               "argument #3 must be a non-negative integer");
      }

      ok = dispatchTimeSeries<float>(xUdt, alpha, order, y, z)
        || dispatchTimeSeries<double>(xUdt, alpha, order, y, z)
        || dispatchTimeSeries<complex<float> >(xUdt, alpha, order, y, z)
        || dispatchTimeSeries<complex<double> >(xUdt, alpha, order, y, z)
        || dispatchSequence<float>(xUdt, alpha, order, y, z)
        || dispatchSequence<double>(xUdt, alpha, order, y, z)
        || dispatchSequence<complex<float> >(xUdt, alpha, order, y, z)
        || dispatchSequence<complex<double> >(xUdt, alpha, order, y, z);

      if (ok && (params.size() == 4))
      {
        chain->AddSymbol(params[3], z.release());
      }

    }
    break;

  case 2:
    {
      udt* const zUdt = chain->GetSymbol(params[1]);
      const udt* const xUdt = chain->GetSymbol(params[0]);

      ok = dispatchTimeSeries<float>(xUdt, zUdt, y)
        || dispatchTimeSeries<double>(xUdt, zUdt, y)
        || dispatchTimeSeries<complex<float> >(xUdt, zUdt, y)
        || dispatchTimeSeries<complex<double> >(xUdt, zUdt, y)
        || dispatchSequence<float>(xUdt, zUdt, y)
        || dispatchSequence<double>(xUdt, zUdt, y)
        || dispatchSequence<complex<float> >(xUdt, zUdt, y)
        || dispatchSequence<complex<double> >(xUdt, zUdt, y);

    }
    break;

  default:
    throw CallChain::BadArgumentCount(GetName(), "2-4", params.size());
    break;
  }

  if (!ok)
  {
    throw std::invalid_argument("Illegal Argument: action interpolate(); "
                                "argument #1; "
                                "expected a TimeSeries or Sequence");
  }

  //---------------------------------------------------------------------
  // Set the symbol name Ret with the calculated result,
  // replacing any previous value of Ret.
  //---------------------------------------------------------------------
  
  chain->ResetOrAddSymbol( ret, y.release() );
}

//-----------------------------------------------------------------------
// This method validates accessability of the parameters
//-----------------------------------------------------------------------
void InterpolateFunction::
ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		    const CallChain::Params& Params ) const
{
  switch( Params.size( ) )
  {
  case 4:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    //-------------------------------------------------------------------
    // Validate output parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 3 ], PARAM_WRITE );
    break;
  default:
    Function::ValidateParameters( SymbolTable, Params );
    break;
  }
}
