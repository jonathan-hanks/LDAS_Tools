/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef RESAMPLE_HH
#define RESAMPLE_HH

// $Id: Resample.hh,v 1.41 2007/06/18 15:26:42 emaros Exp $

#include <general/unexpected_exception.hh>
#include <general/unimplemented_error.hh>

#include "ScalarUDT.hh"
#include "ResampleState.hh"

namespace datacondAPI {
  
  template<class T> class Sequence;
  
  class Resample {
  public:
    //-------------------------------------------------------------------- 
    //: General purpose constructor.  
    Resample(int p, int q, int n = 10, double beta = 5);
    //!param: int p - upsampling ratio
    //!param: int q - downsampling ratio
    //!param: int n = 10 - 2*n*max(p,q) is the length of the filter
    //!param: double beta = 5 - the beta parameter of the Kaiser Window
    //!exc: invalid_argument - if p or q or n <1 or beta < 0
    
    //-------------------------------------------------------------------- 
    //: General purpose constructor.  
    Resample(const udt& p,
	     const udt& q,
	     const udt& n = Scalar<int>(10),
	     const udt& beta = Scalar<double>(5.0));
    //!param: int p - upsampling ratio
    //!param: int q - downsampling ratio
    //!param: int n = 10 - 2*n*max(p,q) is the length of the filter
    //!param: double beta = 5 - the beta parameter of the Kaiser Window
    //!exc: std::invalid_argument - if p or q or n <1 or beta < 0
    //!exc: std::invalid_argument - p, q, n not Scalar<int>, or beta not
    //+ Scalar<double>
    
    //---------------------------------------------------------------------
    //: Alternate Constructor for specifying state. 
    //+ only way the state can be set is with this constructor
    Resample(const udt& state);
    //!param: const udt& state - sets the initial state of the class
    //!exc: std::invalid_argument - state not a ResampleState

    //---------------------------------------------------------------------
    //: Constructor that takes a filter as a parameter
    Resample(int p, int q, const Sequence<double>& b);

    //---------------------------------------------------------------------
    //: Copy constructor
    Resample(const Resample& rsmpl);
    //!param: const Resample& rsmpl - instance to be copied

    //---------------------------------------------------------------------
    //: Default destructor
    ~Resample();
    
    //---------------------------------------------------------------------
    //: Synonym for apply()
    void operator() (udt*& out, const udt& in);
    //!param: const udt& in - data to be resampled
    //!param: udt& out - result of resampling the "in" data
    //!exc: logic_error - Input length must be a multiple of q
    
    //---------------------------------------------------------------------
    //: Synonym for apply()
    template<class T>
    void operator() (std::valarray<T>& out, const std::valarray<T>& in);
    //!param: const udt& in - data to be resampled
    //!param: udt& out - result of resampling the "in" data
    //!exc: logic_error - Input length must be a multiple of q
    
    //---------------------------------------------------------------------
    //: Action of Resample
    void apply(udt*& out, const udt& in);
    //!param: const udt& in - data to be resampled
    //!param: udt*& out - result of resampling the "in" data
    //!exc: logic_error - Input length must be a multiple of q
    //!exc: General::unimplemented_error - in not Sequence or TimeSeries
    //!exc: std::invalid_argument - *out, in not of same type
    
    //---------------------------------------------------------------------
    //: Action of Resample
    template<class T>
    void apply(std::valarray<T>& out, const std::valarray<T>& in);
    //!param: const std::valarray<T>& in - data to be resampled
    //!param: std::valarray<T>& out - result of resampling the "in" data
    //!exc: logic_error - Input length must be a multiple of q
    
    //---------------------------------------------------------------------
    //: Gets current state of Resample instance
    ResampleState getState();
    void getState(ResampleState& state) const;
    void getState(udt*& state);
    template<class T> void getState(Sequence<T>& state);
    //!param: udt*& state - container for the current state of the instance

    //---------------------------------------------------------------------
    //: Set current state of Resample instance
    //:!param: const ResampleState& state - new state
    void setState(const ResampleState& state);
    
    //---------------------------------------------------------------------
    //: Gets the resample fraction
    void getPQ(int& p, int& q);
    //!param: int& p - upsample factor
    //!param: int& q - downsample factor
    
    //---------------------------------------------------------------------
    //: Gets anti-aliasing filter parameters
    void getNBeta(int& n, double& beta);
    //!param: int& n - filter length param
    //!param: double& beta - Kaiser Window beta parameter
    
    //---------------------------------------------------------------------
    //: Gets the delay due to the filtering action
    void getDelay(double& delay);
    //!param: double& delay - group delay of the impulse due to filtering
    
    //---------------------------------------------------------------------
    //: Gets the delay due to the filtering action
    double getDelay(void);
    //!return: returns the group delay of the impulse due to filtering
    
  private:

    //---------------------------------------------------------------------
    //: Helper class for apply(udt*&, udt&). Attempts to apply filter 
    //+ interpreting data as a TimeSeries<>. Sets metadata as appropriate.
    //!param: in - data on which to apply filter
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ on heap.
    //!return: bool - true if successful interpreting as TimeSeries<>
    bool asTimeSeries(udt*& out, const udt& in);

    //---------------------------------------------------------------------
    //: Helper class for apply(udt*&, udt&). Attempts to apply filter
    //+ interpreting data as a Sequence<>. Sets metadata as appropriate.
    //!param: in - data on which to apply filter
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ on heap.
    //!return: bool - true if successful interpreting as Sequence<>
      bool asSequence(udt*& out, const udt& in);

    //---------------------------------------------------------------------
    //: Helper class for apply(udt*&, const udt&). Attempts to apply filter
    //+ on objects of type T
    //!param: out - points to result of filter acting on in. May be null 
    //+ on entry, in which case appropriate output object is allocated 
    //+ on heap.
    //!param: in - data on which to attempt to apply filter
    //!return: bool - true if successful applying filter to in
    template <class T>
    bool applyAs(udt*& out, const udt& in);

    void sequenceMetadata(udt*& out, const udt& in);
    void timeSeriesMetadata(udt*& out, const udt& in);

    ResampleState m_state;
    
  };
  
} //end datacondAPI

#endif // RESAMPLE_HH
