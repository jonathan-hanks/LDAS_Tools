#ifndef _GETRESULTPSUFUNCTION_HH_
#define _GETRESULTPSUFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI{
namespace psu
{

class getResultPSUFunction : public CallChain::Function
{
public:
  getResultPSUFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain, const CallChain::Params& Params,
		    const std::string& Ret) const;

};

}//end namespace psu
}//end namespace datacondAPI

#endif //_GETRESULTPSUFUNCTION_HH_
