// $Id: WaveletUDT.cc,v 1.13 2009/01/20 17:22:39 emaros Exp $

#define WAVELETUDT_CC

#include "config.h"

#include <iostream>
#include <memory>
#include <sstream>   
#include <stdexcept>

#include "general/Memory.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

#include "WaveletUDT.hh"

// WAT headers
#include "Wavelet.hh"
#include "Haar.hh"
#include "Biorthogonal.hh"
#include "Daubechies.hh"
#include "Symlet.hh"
//#include "Dmeyer.hh"
#include "WaveDWT.hh"

namespace datacondAPI
{

// constructors

template<class DataType_>
WaveletUDT<DataType_>::WaveletUDT()
{
   pWavelet = NULL;
   wat::Wavelet *p = new wat::Wavelet();
   setWavelet(*p);
   delete p;
}

template<class DataType_>
WaveletUDT<DataType_>::WaveletUDT(const wat::Wavelet &w) 
{ 
   pWavelet = NULL;
   setWavelet(w);
}  

template<class DataType_>
WaveletUDT<DataType_>::
WaveletUDT(const Sequence<DataType_>& value, const wat::Wavelet &w) : 
TimeSeries<DataType_>(value, 1.)
{   
   pWavelet = NULL;
   setWavelet(w);
}

template<class DataType_>
WaveletUDT<DataType_>::
WaveletUDT(const TimeSeries<DataType_>& value, const wat::Wavelet &w) : 
TimeSeries<DataType_>(value)
{   
   pWavelet = NULL;
   setWavelet(w);
}

template<class DataType_>
WaveletUDT<DataType_>::
WaveletUDT(const WaveletUDT<DataType_>& value) : 
TimeSeries<DataType_>(value)
{
/*
   pWavelet = NULL;
   setWavelet((wat::Wavelet) *(value.pWavelet));
*/

   pWavelet = value.pWavelet->Clone();
   pWavelet->release();

   std::valarray<DataType_>* pv=this;
   DataType_* p= &(pv->operator[](0));

   pWavelet->allocate(pv->size(),p);
}

// destructor

template<class DataType_>
WaveletUDT<DataType_>::~WaveletUDT()
{
   if(pWavelet){
     pWavelet->release();
     delete pWavelet;
   }
}

// UDT required methods

template<class DataType_>
WaveletUDT<DataType_>* WaveletUDT<DataType_>::Clone() const
{
  return new WaveletUDT<DataType_>(*this);
}

template<class DataType_>
ILwd::LdasElement* WaveletUDT<DataType_>::
ConvertToIlwd( const CallChain& Chain,
               datacondAPI::udt::target_type Target) const
{
   std::unique_ptr<ILwd::LdasContainer> container(new ILwd::LdasContainer);
   
   container->push_back(TimeSeries<DataType_>::ConvertToIlwd(Chain, Target),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (int(pWavelet->m_WaveType), "WaveletType"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (pWavelet->m_TreeType, "WaveletTreeType"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (pWavelet->m_Level, "WaveletLevel"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (int(pWavelet->m_Border), "WaveletBorder"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN); 
   
   container->push_back(new ILwd::LdasArray<int>
                        (pWavelet->m_H, "WaveletHPFilterLength"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   container->push_back(new ILwd::LdasArray<int>
                        (pWavelet->m_L, "WaveletLPFilterLength"),
                        ILwd::LdasContainer::NO_ALLOCATE,
                        ILwd::LdasContainer::OWN);
   
   return container.release();
}


// accessors


// metadata methods

template<class DataType_>
int WaveletUDT<DataType_>::
getMaxLevel()
{ 
   int maxlevel = 0;

   if(pWavelet->allocate())
      maxlevel = pWavelet->getMaxLevel();

   return maxlevel;
}

// Accessors

// access to data in wavelet domain
// get wavelet coefficients from a layer with specified frequency index
// if index<0 - get coefficients from the layer = |index|
template<class DataType_>
size_t WaveletUDT<DataType_>::
getLayer(Sequence<DataType_> &value, int index=0)
{
   const std::valarray<DataType_> *w = (const std::valarray<DataType_>*) this;
   std::slice s = pWavelet->getSlice(index);
   size_t last = s.start()+(s.size()-1)*s.stride();

   if(last < w->size()){
      value.resize(s.size());
      value = (*w)[s];               // get slice of wavelet valarray
      return s.size();
   }
   else{
      throw std::invalid_argument
      ("WaveletUDT::getLayer(): data length mismatch");
      return 0;
   }
}

// access to data in wavelet domain
// put wavelet coefficients into layer with specified frequency index
// if index<0 - put coefficients into layer = |index|
template<class DataType_>
void WaveletUDT<DataType_>::
putLayer(const Sequence<DataType_> &value, int index=0)
{
   std::valarray<DataType_> *w = (std::valarray<DataType_>*) this;
   std::slice s = pWavelet->getSlice(index);
   size_t last = s.start()+(s.size()-1)*s.stride();

   if( (s.size() < value.size()) || (last >= w->size()) ){
      std::ostringstream oss;
      oss << "WaveDWT::putLayer(): invalid array size. "
	  << "Expected size: " << s.size() << std::endl;
  
      throw std::invalid_argument(oss.str());
   }      
   else{
      (*w)[s] = value;    // put slice into wavelet valarray
   }
}

// mutators

template<class DataType_>
void WaveletUDT<DataType_>::
setWavelet(const wat::Wavelet &w)
{
   if(pWavelet){              // delete old wavelet object
      pWavelet->release();
      delete pWavelet; 
   } 

//   pWavelet = (wat::WaveDWT<DataType_> *)w.Clone();

// new wavelet object will inherit wavelet parameters from
// prototype 'w' with any data type, but pointer
// to data must correspond to data type in *this WaveletUDT

   switch (w.m_WaveType)
   {
     case wat::BIORTHOGONAL :
       pWavelet = new wat::Biorthogonal<DataType_>(w);
       break;
     case wat::DAUBECHIES :
       pWavelet = new wat::Daubechies<DataType_>(w);
       break;
     case wat::SYMLET :
       pWavelet = new wat::Symlet<DataType_>(w);
       break;
//     case wat::DMEYER :
//       pWavelet = new wat::Dmeyer<DataType_>(w);
//       break;

     default: 
     case wat::HAAR :
       pWavelet = new wat::Haar<DataType_>(w);
       break;
   }

   pWavelet->release();

   std::valarray<DataType_>* pv;
   pv=this;
   DataType_* p=&(pv->operator[](0));

   pWavelet->allocate(pv->size(),p);
}

template<class DataType_>
void WaveletUDT<DataType_>::
Forward(int k)
{
   if(pWavelet->allocate()){
      pWavelet->t2w(k);
   }
   else{
      throw std::invalid_argument
      ("WaveletUDT::Forward(): data is not allocated");
   }
}

template<class DataType_>
void WaveletUDT<DataType_>::
Inverse(int k)
{ 
   if(pWavelet->allocate()){
      pWavelet->w2t(k); 
   }
   else{
      throw std::invalid_argument
      ("WaveletUDT::Inverse(): data is not allocated");
   }
}

} // namespace datacondAPI


// instantiations

#define CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::WaveletUDT< class_ >; \
UDT_CLASS_INSTANTIATION(WaveletUDT< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
//CLASS_INSTANTIATION(std::complex<float>)
//CLASS_INSTANTIATION(std::complex<double>)

#undef CLASS_INSTANTIATION
