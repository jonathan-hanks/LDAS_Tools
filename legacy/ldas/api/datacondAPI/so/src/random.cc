#include "datacondAPI/config.h"

#include "random.hh"
#include <cmath>
#include <complex>

namespace datacondAPI{

float ran0(ran0state& s)
{
  return s.getran();
}

void ran0 (ran0state& s, std::valarray<float>& v, int size)
{
  s.getran(v,size);
}

float ran1(ran1state& s)
{
  return s.getran();
}

void ran1 (ran1state& s, std::valarray<float>& v, int size)
{
  s.getran(v,size);
}

float ran2(ran2state& s)
{
  return s.getran();
}

void ran2 (ran2state& s, std::valarray<float>& v, int size)
{
  s.getran(v,size);
}

float ran3(ran3state& s)
{
  return s.getran();
}

void ran3 (ran3state& s, std::valarray<float>& v, int size)
{
  s.getran(v,size);
}

float ran4(ran4state& s)
{
  return s.getran();
}

void ran4 (ran4state& s, std::valarray<float>& v, int size)
{
  s.getran(v,size);
}

float gasdev(gasdevstate& s)
{
  return s.getgasdev();
}

template<typename type> void gasdev (gasdevstate& s, std::valarray<type>& v, int size)
{
  s.getgasdev(v,size);
}

template void gasdev(gasdevstate&, std::valarray<float>&, int);
template void gasdev(gasdevstate&, std::valarray<double>&, int);
template void gasdev(gasdevstate&, std::valarray<std::complex<float> >&, int);
template void gasdev(gasdevstate&, std::valarray<std::complex<double> >&, int);

}//datacondAPI

