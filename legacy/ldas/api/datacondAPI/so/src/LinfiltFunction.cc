// $Id: LinfiltFunction.cc,v 1.25 2007/01/24 17:51:27 emaros Exp $
//
// "linfilt" action
//
// [y = ]linfilt(x, z)
// [y = ]linfilt(b, x[, z])
// [y = ]linfilt(b, a, x[, z])
//
// b: Sequence<double>
//    FIR transfer function
// a: Sequence<double>
//    IIR transfer function
// x: Sequence<T> where T is float, double, complex<float>, complex<double>
//    input sequence
// y: Sequence<T> where T is float, double, complex<float>, complex<double>
//    output sequence
// z: any (valarray<T> on output)
//    optional state, may be undefined on input

#include "datacondAPI/config.h"

#include "LinfiltFunction.hh"

#include "LinFilt.hh"
#include "SequenceUDT.hh"

//#include <iostream> // for debugging output

//=============================================================================
// LinfiltFunction
//=============================================================================

//-----------------------------------------------------------------------------
// LinfiltFunction::GetName
//-----------------------------------------------------------------------------

const std::string& LinfiltFunction::GetName(void) const
{
    //-------------------------------------------------------------------------
    // The GetName function returns the string "linfilt", the name of the
    // action as presented to the user. The name is stored as a static variable
    // and returned as a const reference for performance reasons.
    //-------------------------------------------------------------------------
  
    static std::string name("linfilt");

    return name;
}

//-----------------------------------------------------------------------------
// LinfiltFunction::LinfiltFunction
//-----------------------------------------------------------------------------

LinfiltFunction::LinfiltFunction() : Function( LinfiltFunction::GetName() )
{
    //-------------------------------------------------------------------------
    // The sole task of the LinfiltFunction constructor is to pass the action
    // name, defined by GetName, to the Function constructor.
    //-------------------------------------------------------------------------
}

//-----------------------------------------------------------------------------
// We construct a static, global instance of a LinfiltFunction so that the
// constructor is called when the API is loaded. The constructor passes the
// action name on to the Function constructor, thus registering it as a valid
// action.
//-----------------------------------------------------------------------------

static LinfiltFunction _LinfiltFunction;

//-----------------------------------------------------------------------------
// LinfiltFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "linfilt". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//-----------------------------------------------------------------------------

void LinfiltFunction::Eval(CallChain* Chain,
                           const CallChain::Params& Params,
                           const std::string& Ret) const
{

  //std::cout << "entering Eval\n";

    using namespace datacondAPI;
  
    //-------------------------------------------------------------------------
    // Some variables for later use
    //-------------------------------------------------------------------------
  
    CallChain::Symbol* returnedSymbol = 0;
  
    switch (Params.size())
    {
    case 2:
      {
	//std::cout << "case 2\n";
	CallChain::Symbol* obj0 = Chain->GetSymbol(Params[0]);
	CallChain::Symbol* obj1 = Chain->GetSymbol(Params[1]);
	if(udt::IsA<State>(*obj1))
	{
	  // [y = ]linfilt(x, z)
	  //std::cout << "linfilt(x, z)\n";
	  LinFilt lf = LinFiltState(*obj1);
	  lf.apply(returnedSymbol,*obj0);
	  lf.getState(obj1);
	}
	else
	{
	  // [y = ]linfilt(b, x)
	  //std::cout << "linfilt(b, x)\n";
	  LinFilt lf = LinFiltState(*obj0);
	  lf.apply(returnedSymbol,*obj1);
	}
      }
      break;

    case 3:
      {
	//std::cout << "case 3\n";
	CallChain::Symbol* obj0 = Chain->GetSymbol(Params[0]);
	CallChain::Symbol* obj1 = Chain->GetSymbol(Params[1]);
	CallChain::Symbol* obj2 = Chain->GetSymbol(Params[2], false);
	if((obj2 == 0) || udt::IsA<State>(*obj2))
	{
	  // [y = ]linfilt(b, x, z)
	  LinFilt lf = LinFiltState(*obj0);
	  lf.apply(returnedSymbol,*obj1);
	  Chain->AddSymbol(Params[2], new datacondAPI::LinFiltState(lf.getState()));
	}
	else
	{
	  // [y = ]linfilt(b, a, x)
	  LinFilt lf(LinFiltState(*obj0, *obj1));
	  lf.apply(returnedSymbol,*obj2);
	}
      }
      break;

    case 4:
      {
	//std::cout << "case 4\n";
	CallChain::Symbol* obj0 = Chain->GetSymbol(Params[0]);
	CallChain::Symbol* obj1 = Chain->GetSymbol(Params[1]);
	CallChain::Symbol* obj2 = Chain->GetSymbol(Params[2]);
	// [y = ]linfilt(b, a, x, z)
	LinFilt lf(LinFiltState(*obj0, *obj1));
	lf.apply(returnedSymbol, *obj2);
	Chain->AddSymbol(Params[3], new datacondAPI::LinFiltState(lf.getState()));
      }	
      break;
    
    default:
      //std::cout << "default\n";
    
        //---------------------------------------------------------------------
        // Unsupported number of arguments. We throw an exception
        //---------------------------------------------------------------------
    
        throw CallChain::BadArgumentCount("linfilt", 4, Params.size());
    
        //---------------------------------------------------------------------
        // End case default - unreachable code!
        //---------------------------------------------------------------------
    
        break;
    }
    
    //std::cout << "break\n";
  
    if (!returnedSymbol)
    {

        //---------------------------------------------------------------------
        // A null pointer was returned by apply. We throw an exception.
        //---------------------------------------------------------------------
    
        throw CallChain::NullResult( GetName() );
    }
  
  
    //-------------------------------------------------------------------
    // Set the symbol name Ret with the calculated result,
    // replacing any previous value of Ret.
    //-------------------------------------------------------------------
  
    Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}

//-----------------------------------------------------------------------
// This method validates accessability of the parameters
//-----------------------------------------------------------------------
void LinfiltFunction::
ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		    const CallChain::Params& Params ) const
{
  switch( Params.size( ) )
  {
  case 3:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    try
    {
      validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    }
    catch( ... )
    {
      validateParameter( SymbolTable, Params[ 2 ], PARAM_WRITE );
    }
    break;
  case 4:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    //-------------------------------------------------------------------
    // Validate output parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 3 ], PARAM_WRITE );
    break;
  default:
    Function::ValidateParameters( SymbolTable, Params );
    break;
  }
}
