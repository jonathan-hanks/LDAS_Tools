#ifndef CSDESTIMATE_HH
#define CSDESTIMATE_HH

//!ppp: {eval `cat $ENV{SRCDIR}/cc_rules.pl`}

#include <string>
#include <general/unexpected_exception.hh>

namespace datacondAPI {

    // Forward declarations
    class udt;
    template<class T> class Sequence;

    //
    //: An abstract base class for spectral density estimation
    //
    class CSDEstimate {

    public:
	//
	//: An enum type for holding the detrend option
	//
	enum DetrendMethod {
	    none,
	    mean,
	    linear
	};

	//
	//: Constructor
	//
	//!param: detrendMethod - detrend method
	//
	explicit
	CSDEstimate(const DetrendMethod detrendMethod = none);

	//
	//: Destructor
	//
	virtual 
	~CSDEstimate();

	//
	//: Set detrend method
	//
	// Detrend options are: none, mean, linear
	//
	//!param: detrendMethod - detrend method
	//
	void 
	set_detrendMethod(const DetrendMethod detrendMethod);

	//
	//: Set detrend method (udt input)
	//
	//!param: detrendMethod - detrend method
	//
	//!exc: std::invalid_argument - thrown if the udt input is not
	//+ a Scalar&lt int&gt
	//
	void 
	set_detrendMethod(const udt& detrendMethod);

	//
	//: Return name of spectral density estimate
	//
	//!ret: string - name of spectral density estimate
	//
	virtual 
	std::string 
	what() const = 0;

	//
	//: Return detrend method
	//
	//!ret: DetrendMethod - detrend method
	//
	DetrendMethod 
	detrendMethod() const;
    
	// 
	//: Apply method for cross-spectral density estimate S_xy
	//+ (udt input and output)
	//
	//!param: out - output CSD estimate: S_xy
	//!param: xIn - input data: x
	//!param: yIn - input data: y
	//
	virtual 
	void 
	apply(udt*& out, const udt& xIn, const udt& yIn) = 0;

	// 
	//: Apply method for power spectral density estimate S_xx
	//+ (udt input and output)
	//
	//!param: out - output CSD estimate: S_xx
	//!param: in - input data: x
	//
	virtual 
	void 
	apply(udt*& out, const udt& in) = 0;

	//
	//: Remove mean or linear trend from real data in-place
	//
	// Depending on the value of the detrend flag, this routine does 
	// one of the following:
	//
	// <ul>
	// <li>does nothing</li>
	// <li>removes a mean</li>
	// <li>removes a linear trend</li>
	// </ul>
	//
	//!param: x - input and output data
	//
	//!exc: std::invalid_argument - thrown if no input data
	//
	template<class T>
	void
	detrend(Sequence<T>& x) const;

    private:
	//: detrend method
	DetrendMethod m_detrendMethod;
    };
} // end of datacondAPI namespace 

#endif // CSDESTIMATE_HH

