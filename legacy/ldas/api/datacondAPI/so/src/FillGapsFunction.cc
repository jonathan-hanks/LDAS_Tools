// $Id: FillGapsFunction.cc,v 1.6 2005/12/01 22:54:58 emaros Exp $
//
// "fillgaps" action
//
// fillgaps has 2 input arguments and one return value. The format is
//
//     out = fillgaps(in, [ fill_method ])
//
// out and in are Sequences.
// fill_method is an integer, default is zero-fill
//

#include "datacondAPI/config.h"

#include "FillGapsFunction.hh"
#include "GapFill.hh"
#include "TypeInfo.hh"
#include "ScalarUDT.hh"

namespace {
    FillGapsFunction staticFillGapsFunction;
}

FillGapsFunction::FillGapsFunction()
    : Function( FillGapsFunction::GetName() )
{
}

const std::string& FillGapsFunction::GetName(void) const
{
    const static std::string name("fillgaps");
    return name;
}

void FillGapsFunction::Eval(CallChain* Chain,
			    const CallChain::Params& Params,
			    const std::string& Ret) const
{
    using namespace datacondAPI;

    GapFill::FillMethod_type fill_method = GapFill::ZERO;

    switch(Params.size())
    {
    case 2:
	if (Params[1] != "")
	{
	    const CallChain::Symbol* const sym = Chain->GetSymbol(Params[1]);
	    if (const Scalar<int>* const p
		= dynamic_cast<const Scalar<int>*>(sym))
	    {
		const int method = p->GetValue();
		switch(method)
		{
		case 0:
		    fill_method = GapFill::ZERO;
		    break;
		case 1:
		    fill_method = GapFill::AVERAGE;
		    break;
		case 2:
		    fill_method = GapFill::LINEAR;
		    break;
		default:
		    throw CallChain::BadArgument("fillgaps", 2,
						 "",
						 "");  
		    break;
		}
	    }
	    else
	    {
		throw CallChain::BadArgument("fillgaps", 1,
				  TypeInfoTable.GetName(typeid(*sym)),
                                  TypeInfoTable.GetName(typeid(Scalar<int>)));
 		
	    }
	}
	
	// Fall through
    case 1:
	// Nothing to do here
        break;

    default:
        throw CallChain::BadArgumentCount("fillgaps", 1, Params.size());
    }

    const udt& in = *(Chain->GetSymbol(Params[0]));
    CallChain::Symbol* out = 0;

    const GapFill fillGaps(fill_method);

    fillGaps.apply(out, in);
    
    Chain->ResetOrAddSymbol( Ret, out);
}
