/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef LINFILTSTATE_HH
#define LINFILTSTATE_HH

#include <memory>

#include "general/Memory.hh"
#include <general/unexpected_exception.hh>

#include "StateUDT.hh"
#include "SequenceUDT.hh"

namespace std {
  template<class T> class complex;
}

namespace Filters {
  class LinFiltBase;
}

namespace datacondAPI {

  class LinFiltState : public State {

  public:

    //: Construct IIR filter state from transfer function
    //!param: b - FIR transfer function coefficients
    //!param: a - IIR transfer function coefficients
    LinFiltState(const Sequence<double>& b, 
                 const Sequence<double>& a = Sequence<double>(1.0, 1));

    //: Construct IIR filter state from zeroes, poles and gain
    //
    // In this constructor, the filter is specified in zeroes-poles-gain
    // (ZPG) form. A purely real (ie. imag(r[k]) == 0) number in the array
    // of zeroes (or poles) leads to a linear factor in the transfer function
    // numerator (or denominator) ie. (z - r[k]). On the other hand, when a
    // non-real (ie. imag(r[k]) != 0)
    // number occurs in the array this is taken to represent one member of
    // a complex-conjugate pair of roots for the final polynomial. The other
    // member of the pair should not be present in the array.
    // Consequently, a non-real number r[k] contributes the quadratic factor
    // (x - r[k])(x - conj(r[k]) to the resulting polynomial.
    // Finally, the numerator is multiplied by the (real) gain.
    //
    // This scheme is chosen to guarantee the reality of the transfer function.
    //
    //!param: zeroes - zeroes of the transfer function
    //!param: poles - poles of the transfer function
    //!param: gain - gain of the transfer function
    LinFiltState(const Sequence<std::complex<double> >& zeroes,
                 const Sequence<std::complex<double> >& poles,
                 const double& gain);

    //: Copy constructor. Required to be CopyConstructible (C++ spec 20.1.3).
    //!param: state - LinFiltState object to copy from
    LinFiltState(const LinFiltState& state);

    //: Construct IIR filter state from transfer function
    //!param: b - FIR transfer function coefficients
    //!param: a - IIR transfer function coefficients
    //!exc: std::invalid_argument - one or both of a, b not Sequence<double>
    LinFiltState(const udt& b, const udt& a);

    //: Construct FIR filter state from transfer function
    //!param: b_or_state - FIR transfer function coefficients or LinFiltState
    //!exc: std::invalid_argument - bstate not either Sequence<double> 
    //+ or LinFiltState object
    LinFiltState(const udt& b_or_state);
    
    //: Destructor
    virtual ~LinFiltState();
    
    //: Assignment operator. Required to be Assignable (C++ spec 23.1.3, 4)
    //!param: state - LinFiltState to assign from
    LinFiltState& operator=(const LinFiltState& state);
    
    //: retrieve FIR filter coefficients as valarray<T>
    //!param: a - FIR filter coefficients. 
    void getB(Sequence<double>& b) const;

    //: retrieve IIR filter coefficients as valarray<T>
    //!param: a - IIR filter coefficients. 
    void getA(Sequence<double>& a) const;

    //: return number of coefficients in FIR, IIR transfer functions
    //!param: a - number of IIR transfer function coefficient
    //!param: b - number of FIR transfer function coefficient
    void getSize(int& aSize, int& bSize);
    
    //: return duplicate copy of object, constructed on heap
    //!return: datacondAPI::LinFiltState* - copy of *this allocated on heap 
    virtual LinFiltState* Clone() const;

    //: use the linear filter defined by the state on the input data
    //!param: x - the input data. On input, replaced by the result of
    //+filtering x
    template<class T>
    void apply(std::valarray<T>& x);
    
    //: use the linear filter defined by the state on the input data
    //!param: x - the input data.
    //!param: y - the output data generate by filtering x
    template<class TOut, class TIn>
    void apply(std::valarray<TOut>& yout, const std::valarray<TIn>& yin);
      
    //: Convert this state to an ILWD object
    //!todo: Need to finish documenting this method
    //!param: Chain - 
    //!param: Target - 
    //!return: 
    virtual
    ILwd::LdasElement* 
    ConvertToIlwd(const CallChain& Chain, 
                  datacondAPI::udt::target_type Target = 
                  datacondAPI::udt::TARGET_GENERIC ) const;
    
  private:
    //: Default constructor prohibited - no canonical default
    LinFiltState();

    void checkAB() const;

    //: member data

    Sequence<double> m_b;
    Sequence<double> m_a;
    
    std::unique_ptr<Filters::LinFiltBase> m_linfilt;

  }; // class LinFiltState

} // namespace datacondAPI

#endif // LINFILTSTATE_HH
