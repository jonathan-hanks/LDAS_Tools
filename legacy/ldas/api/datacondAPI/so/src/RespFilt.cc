#include "config.h"

#if HAVE_IEEEFP_H
#include <ieeefp.h>
#endif

#include <filters/valarray_utils.hh>
#include <ilwd/ldascontainer.hh>

#include "fft.hh"
#include "ifft.hh"
#include "RespFilt.hh"

using std::complex;
using std::invalid_argument;
using std::valarray;
using std:: floor;
using std::ostringstream;

namespace datacondAPI {

    template<class T>
    void respFilt(TimeSeries<T>& ts,
                  const FrequencySequence<std::complex<float> >& R,
                  const FrequencySequence<std::complex<float> >& C,
                  const TimeSeries<std::complex<float> >& alphas,
                  const TimeSeries<std::complex<float> >& gammas,
                  const RespFiltDirection& direction)
    {
        // Tolerance for what we regard as "zero" in the calibration
        const float CAL_FAC_TOLERANCE = 1.0e-12;

        // R and C must have the same start frequency, step-size and size
        const double dfIn = R.GetFrequencyDelta();
        const double f0In = R.GetFrequency(0);

        if (C.GetFrequency(0) != f0In)
        {
            throw invalid_argument("respFilt() - "
                                   "R and C have different start frequency");
        }

        if (C.GetFrequencyDelta() != dfIn)
        {
            throw invalid_argument("respFilt() - "
                                "R and C have different frequency step-size");
        }

        if (C.size() != R.size())
        {
            throw invalid_argument("respFilt() - "
                                   "R and C have different size");
        }

        if (R.size() < 2)
        {
            throw invalid_argument("respFilt() - "
                                   "R and C have size < 2");
        }

        if (ts.size() <= 0)
        {
            throw invalid_argument("respFilt() - "
                                   "Input time-series has zero size");
        }

        if (alphas.size() <= 0)
        {
            throw invalid_argument("respFilt() - "
                                   "Alpha time-series has zero size");
        }

        if (gammas.size() <= 0)
        {
            throw invalid_argument("respFilt() - "
                                   "Gamma time-series has zero size");
        }

        // Get the value of alpha
   double tmpIdx
     = floor((ts.GetStartTime() - alphas.GetStartTime())/alphas.GetStepSize());
        
        if ((tmpIdx < 0) || (tmpIdx >= alphas.size()))
        {
            ostringstream oss;
            
            oss << "respFilt() - Start-time of input time-series ("
                << ts.GetStartTime()
                << ") is not in the range spanned by alphas ("
                << alphas.GetStartTime()
                << " - "
                << alphas.GetEndTime()
                << ")";
            
            throw invalid_argument(oss.str());
        }

        // We know it's positive, so this cast is safe
        size_t idx = (size_t) tmpIdx;
        
        const complex<double> alpha = alphas[idx];

        if (abs(alpha) < CAL_FAC_TOLERANCE)
        {
            ostringstream oss;
            
            oss << "respFilt() - Zero alpha factor at time "
                << ts.GetStartTime();
            
            throw invalid_argument(oss.str());
        }

        // Get the value of gamma
   tmpIdx
     = floor((ts.GetStartTime() - gammas.GetStartTime())/gammas.GetStepSize());
        
        if ((tmpIdx < 0) || (tmpIdx >= gammas.size()))
        {
            ostringstream oss;
            
            oss << "respFilt() - Start-time of input time-series ("
                << ts.GetStartTime()
                << ") is not in the range spanned by gammas ("
                << gammas.GetStartTime()
                << " - "
                << gammas.GetEndTime()
                << ")";
            
            throw invalid_argument(oss.str());
        }

        // We know it's positive, so this cast is safe
        idx = (size_t) tmpIdx;
        
        const complex<double> gamma = gammas[idx];

        if (abs(gamma) < CAL_FAC_TOLERANCE)
        {
            ostringstream oss;
            
            oss << "respFilt() - Zero gamma factor at time "
                << ts.GetStartTime();
            
            throw invalid_argument(oss.str());
        }

        // Make the transfer function (use double precision)
        FrequencySequence<complex<double> > fs1(C.size());
        Filters::valarray_copy(fs1, C);
        fs1.FrequencyMetaData::operator=(C);  // fs1 = C

        FrequencySequence<complex<double> > fs2(R.size());
        Filters::valarray_copy(fs2, R);
        fs2.FrequencyMetaData::operator=(R); // fs2 = R

        fs2 *= fs1;                  // fs2 = C*R
        fs2 -= complex<double>(1);   // fs2 = H = C*R - 1
        fs2 *= gamma;                // fs2 = gamma*H
        fs2 += complex<double>(1);   // fs2 = 1 + gamma*H

        fs1 *= alpha;                // fs1 = alpha*C

        if (direction == RESPFILT_FORWARD)
        {
            fs1 /= fs2;                  // fs1 = alpha*C/(1 + gamma*H)
        }
        else
        {
            fs2 /= fs1;                  // fs2 = (1 + gamma*H)/(alpha*C)
            fs1 = fs2;                   // fs1 = (1 + gamma*H)/(alpha*C)
        }

        // Check the final result for finite-ness
        
        for (size_t k = 0; k < fs1.size(); ++k)
        {
            if (!finite(abs(fs1[k])))
            {
                ostringstream oss;
                
                oss << "respFilt() - Transfer function is infinite at index "
                    << k
                    << " ("
                    << fs1.GetFrequency(k)
                    << " Hz)";
                
                throw invalid_argument(oss.str());
            }
        }
        
        // At this point, fs1 contains the transfer function,
        // at the resolution of R and C.
        // Now we need to interpolate it. We're done with fs2, so we can
        // use it to store the interpolated transfer function
        const double dfOut = ts.GetSampleRate()/ts.size();

        fs2.resize(ts.size()/2 + 1, 0.0);
        fs2.SetFrequencyDelta(dfOut);
        
        for (size_t k = 0; k < fs2.size(); ++k)
        {
            // Target frequency (assuming f0 = 0)
            const double f = k*dfOut;

            // Index nearest to but <= f in TF
            const double jTmp = floor((f - f0In)/dfIn);

            // If outside bandwidth of TF, set to zero
            if ((jTmp < 0) || (jTmp > fs1.size() - 2))
            {
                fs2[k] = 0;
            }
            else
            {
                const size_t j = (size_t) jTmp;
                const double lambda = (f - fs1.GetFrequency(j))/dfIn;

                fs2[k] = (1 - lambda)*fs1[j] + lambda*fs1[j + 1];
            }
        }

        // At this point, fs2 contains the interpolated transfer
        // function and we're done with fs1. Can reuse it to
        // store the 2-sided transfer function.

        fs1.resize(ts.size(), 0.0);

        for (size_t k = 0; k < fs2.size() - 1; ++k)
        {
            fs1[k] = fs2[k];
        }

        // Nyquist component, faked to make it real
        fs1[fs2.size() - 1] = abs(fs2[fs2.size() - 1]);

        for (size_t k = fs2.size(); k < fs1.size(); ++k)
        {
            fs1[k] = conj(fs2[fs1.size() - k]);
        }

        // fs1 now contains the 2-sided transfer function

        FFT fft;
        IFFT ifft;

        // We're done with fs2, so we can use it for part of the FFT,
        // but we still need to complexify the input time-series
        valarray<complex<double> > fftIn(ts.size());

        Filters::valarray_copy(fftIn, ts);

        fft.apply(fs2, fftIn);
        
        // Apply the 2-side transfer fucntion to the data
        fs2 *= fs1;
        
        ifft.apply(fftIn, fs2);
        
        for (size_t k = 0; k < ts.size(); ++k)
        {
            ts[k] = (T) fftIn[k].real();
        }
    }

    
    template
    void respFilt<float>(TimeSeries<float>& ts,
                         const FrequencySequence<std::complex<float> >& R,
                         const FrequencySequence<std::complex<float> >& C,
                         const TimeSeries<std::complex<float> >& alphas,
                         const TimeSeries<std::complex<float> >& gammas,
                         const RespFiltDirection&);

    template
    void respFilt<double>(TimeSeries<double>& ts,
                          const FrequencySequence<std::complex<float> >& R,
                          const FrequencySequence<std::complex<float> >& C,
                          const TimeSeries<std::complex<float> >& alphas,
                          const TimeSeries<std::complex<float> >& gammas,
                          const RespFiltDirection&);

}
