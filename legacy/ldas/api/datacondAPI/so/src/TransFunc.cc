#include "config.h"

#include <fstream>
#include <memory>

#include "general/Memory.hh"

#include "ilwd/ldaselement.hh"
#include "ilwd/reader.hh"
#include "ilwd/util.hh"

#include "TransFunc.h"

#include "ILwdToUDT.hh"
#include "UDT.hh"

int
TranslateILwdFile( translation_direction Direction,
		   void** UserData,
		   void** DatacondData,
		   void* AuxData )
{
  using namespace datacondAPI;

  if ( ( UserData == (void**)NULL ) || ( DatacondData == (void**)NULL ) )
  {
    // One of the pointers cannot be dereferenced
    return TRANSLATION_FAIL;
  }
  switch ( Direction )
  {
  case DATACOND_SYMBOL_INPUT:
    {
      const char* user = reinterpret_cast< const char* >( *UserData );
      udt**	dc( reinterpret_cast< udt** >( DatacondData ) );

      if ( user == ( const char*)NULL )
      {
	return TRANSLATION_FAIL;
      }
      //-----------------------------------------------------------------
      // Read in the ILwd File
      //-----------------------------------------------------------------
      std::ifstream ilwd_stream( user );
      if ( ! ilwd_stream.good( ) )
      {
	return TRANSLATION_FAIL;
      }
      std::unique_ptr< ILwd::LdasElement > ilwd;
      try
      {
	ILwd::Reader r( ilwd_stream );
	ILwd::readHeader( ilwd_stream );

	ilwd.reset( ILwd::LdasElement::createElement( r ) );
	//---------------------------------------------------------------
	//:TODO: Convert the ILwd to UDT
	//---------------------------------------------------------------
	ILwdToUDT results( *ilwd );
	switch( results.Size( ) )
	{
	case 0:
	  return TRANSLATION_FAIL;
	case 1:
	  // Set the result to a single UDT
	  *dc = results[ 0 ];
	  results.Release( );
	  break;
	default:
	  //:TODO: Multiple outputs
	  return TRANSLATION_FAIL;
	  break;
	}
      }
      catch( ... )
      {
	ilwd_stream.close( );
	return TRANSLATION_FAIL;
      }
      ilwd_stream.close( );
    }
    break;
  case DATACOND_SYMBOL_OUTPUT:
    return TRANSLATION_FAIL;
    break;
  }

  return TRANSLATION_OK;
}

int
TranslateUDT( translation_direction Direction,
	      void** UserData,
	      void** DatacondData,
	      void* AuxData )
{
  using namespace datacondAPI;

  udt**	user( reinterpret_cast< udt** >( UserData ) );
  udt**	dc( reinterpret_cast< udt** >( DatacondData ) );

  if ( ( user == (udt**)NULL ) || ( dc == (udt**)NULL ) )
  {
    // One of the pointers cannot be dereferenced
    return TRANSLATION_FAIL;
  }
  switch ( Direction )
  {
  case DATACOND_SYMBOL_INPUT:
    if ( *user == (udt*)NULL )
    {
      return TRANSLATION_FAIL;
    }
    *dc = (*user)->Clone( );
    break;
  case DATACOND_SYMBOL_OUTPUT:
    if ( *dc == (udt*)NULL )
    {
      return TRANSLATION_FAIL;
    }
    *user = (*dc)->Clone( );
    break;
  }

  return TRANSLATION_OK;
}
