#include "datacondAPI/config.h"

#include <general/unimplemented_error.hh>
#include <general/toss.hh>

#include "ran4state.hh"
#include "random.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

namespace datacondAPI{

  ran4state::ran4state()
    : c1(NITER),
      c2(NITER)
  {
    idum = INT_4S(-248673140);
    idums = INT_4S(0);
    jflone = INT_4U(0x3f800000);
    jflmsk = INT_4U(0x007fffff);
    c1[0] = INT_4U(0xbaa96887);
    c1[1] = INT_4U(0x1e17b32c);
    c1[2] = INT_4U(0x02bcdc3c);
    c1[3] = INT_4U(0x0f33d1b2);
    c2[0] = INT_4U(0x4b0f3b58);
    c2[1] = INT_4U(0xe874f0c3);
    c2[2] = INT_4U(0x6955c5a6);
    c2[3] = INT_4U(0x55a7ca46);
  }

  ran4state::ran4state(INT_4S seed)
    : c1(NITER),
      c2(NITER)
  {
    idum = seed;
    idums = INT_4S(0);
    jflone = INT_4U(0x3f800000);
    jflmsk = INT_4U(0x007fffff);
    c1[0] = INT_4U(0xbaa96887);
    c1[1] = INT_4U(0x1e17b32c);
    c1[2] = INT_4U(0x02bcdc3c);
    c1[3] = INT_4U(0x0f33d1b2);
    c2[0] = INT_4U(0x4b0f3b58);
    c2[1] = INT_4U(0xe874f0c3);
    c2[2] = INT_4U(0x6955c5a6);
    c2[3] = INT_4U(0x55a7ca46);
  }

  ran4state::ran4state(const ran4state& g)
    : State( g ),
      c1(g.c1), c2(g.c2)
  {
    idum = g.idum;
    idums = g.idums;
    jflone = g.jflone;
    jflmsk = g.jflmsk;
  }

  //restest state with new seed s
  void ran4state::seed(INT_4S s)
  {
    idum = s;
    idums = INT_4S(0);
    jflone = INT_4U(0x3f800000);
    jflmsk = INT_4U(0x007fffff);
    c1[0] = INT_4U(0xbaa96887);
    c1[1] = INT_4U(0x1e17b32c);
    c1[2] = INT_4U(0x02bcdc3c);
    c1[3] = INT_4U(0x0f33d1b2);
    c2[0] = INT_4U(0x4b0f3b58);
    c2[1] = INT_4U(0xe874f0c3);
    c2[2] = INT_4U(0x6955c5a6);
    c2[3] = INT_4U(0x55a7ca46);
  }

  float ran4state::getran()
  {
    INT_4U irword,lword;
    union {
      INT_4U	i;
      REAL_4	r;
    } retval;

    //static long idums =0;
    //static INT_4U jflone = 0x3f800000;
    //static INT_4U jflmsk = 0x007fffff;
    if(idum<0)
      {
	idums = -(idum);
	idum=1;
      }
    irword=idum;
    lword=idums;
    psdes(lword,irword);
    retval.i = jflone | (jflmsk&irword);
    ++idum;
    retval.r -= 1.0;
    return retval.r;
  }

  void ran4state::psdes(INT_4U& lword, INT_4U& irword)
  {
    	INT_4U i,ia,ib,iswap,itmph=0,itmpl=0;
	//static INT_4U c1[NITER]={
	//	0xbaa96887L, 0x1e17b32cL, 0x02bcdc3cL, 0x0f33d1b2L};
	//static INT_4U c2[NITER]={
	//	0x4b0f3b58L, 0xe874f0c3L, 0x6955c5a6L, 0x55a7ca46L};
	
	for(i=0;i<NITER;i++)
	{
		ia=(iswap=(irword)) ^ c1[i];
		itmpl = ia & 0xffff;
		itmph = ia >> 16;
		ib=itmpl*itmpl+ ~(itmph*itmph);
		irword=lword^ (((ia=(ib>>16) | ((ib & 0xffff) << 16)) ^ c2[i])+itmpl*itmph);
		lword=iswap;
	}
  }

  void ran4state::getran(std::valarray<float>& vals, int size)
  {
    vals.resize(size);
    for(int i =0; i<size;i++)
      vals[i] = getran();
  }

  ran4state* ran4state::Clone() const
  {
    return new ran4state(*this);
  }

  ILwd::LdasElement* ran4state::
  ConvertToIlwd( const CallChain& Chain,
		 datacondAPI::udt::target_type Target) const
  {
    ILwd::LdasContainer* container = new ILwd::LdasContainer;
    General::toss<General::unimplemented_error>("ConvertToILWD",
						__FILE__,__LINE__,
						"unimplemented");
    return container;
  }

}
