/* -*- mode: c++; c-basic-offset: 4; -*- */
#include "datacondAPI/config.h"

#include "CallChain.hh"
#include "ARModel.hh"

// arls - AutoRegression Least Squares
//
// arls uses the ARModel class to least-squares fit an autoregressive
// linear filter to data, and to apply that filter to data
//
// b = arls(x, n)
//   x - (input) sequence data
//   n - (input) integer model order
//   b - (output) autoregressive linear filter coefficients
//
// b = arls(x, n, z)
//   x - (input) sequence data
//   n - (input) integer model order
//   z - (output) ar model state
//   b - (output) autoregressive linear filter coefficients
//
// b = arls(x, z)
//   x - (input) sequence data
//   z - (input/output) ar model state to be refined
//   b - (output) refined autoregressive linear filter coefficients


namespace datacondAPI
{

    class ARModelFunction : public CallChain::Function
    {

    public:

	ARModelFunction();

	virtual const std::string& GetName() const;

	virtual void Eval(CallChain* Chain,
			  const CallChain::Params& Params,
			  const std::string& Ret) const;

	//---------------------------------------------------------------
	//: Validate parameters
	virtual void
	ValidateParameters( CallChain::Step::sudo_symbol_table_type&
			    SymbolTable,
			    const CallChain::Params& Params ) const;
    private:

    };

    static ARModelFunction _ARModelFunction; // registering instantiation

    ARModelFunction::ARModelFunction()
	: CallChain::Function( ARModelFunction::GetName() )
    {
    }

    const std::string& ARModelFunction::GetName() const
    {
	static const std::string name("arls");
	return name;
    }

    void ARModelFunction::Eval(CallChain* call_chain,
	      const CallChain::Params& parameter_list,
	      const std::string& return_name) const
    {
	udt* b = 0;
	switch(parameter_list.size())
	{
	case 2:
	    // b = arls(x, n) or b = arls(x, z)
	    {
		udt& x = *(call_chain->GetSymbol(parameter_list[0]));
		udt& n_or_z = *(call_chain->GetSymbol(parameter_list[1]));
		if (udt::IsA<ARModel>(n_or_z))
		{
		    // b = arls(x, z)
		    udt::Cast<ARModel>(n_or_z).refine(x);
		    udt::Cast<ARModel>(n_or_z).getFilter(b);
		}
		else
		{
		    // b = arls(x, n)
		    ARModel ar(x, n_or_z);
		    ar.getFilter(b);
		}
	    }
	    break;
	case 3:
	    // b = arls(x, n, z)
	    {
		udt& x = *(call_chain->GetSymbol(parameter_list[0]));
		udt& n = *(call_chain->GetSymbol(parameter_list[1]));
		udt* z = new ARModel(x, n);
		udt::Cast<ARModel>(*z).getFilter(b);
		call_chain->AddSymbol(parameter_list[2], z);
	    }
	    break;
	default:
	    throw CallChain::BadArgumentCount( GetName(),
					       "2 or 3",
					       parameter_list.size() );
	}
	call_chain->ResetOrAddSymbol( return_name, b );
    }

    //-------------------------------------------------------------------
    // This method validates accessability of the parameters
    //-------------------------------------------------------------------
    void ARModelFunction::
    ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
			const CallChain::Params& Params ) const
    {
	switch( Params.size( ) )
	{
	case 3:
	    //-----------------------------------------------------------
	    // Validate input parameters
	    //-----------------------------------------------------------
	    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
	    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
	    //-----------------------------------------------------------
	    // Validate output parameters
	    //-----------------------------------------------------------
	    validateParameter( SymbolTable, Params[ 2 ], PARAM_WRITE );
	    break;
	default:
	    Function::ValidateParameters( SymbolTable, Params );
	    break;
	}
    }
}
