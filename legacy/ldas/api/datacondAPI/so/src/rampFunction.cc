#include "datacondAPI/config.h"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "TypeInfo.hh"

#include "rampFunction.hh"

//
// Syntax is
//
//   y = ramp(n, dy [, y0]);
//
// where
//
//   y[k] = y0 + k*dy, k = 0, 1, ..., n - 1
//
namespace {

  const rampFunction staticRampFunction;

}

const std::string& rampFunction::GetName() const
{
  const static std::string name("ramp");
  
  return name;
}

rampFunction::rampFunction()
  : Function(rampFunction::GetName())
{}

void rampFunction::Eval(CallChain* Chain,
			const CallChain::Params& Params,
			const std::string& Ret) const
{
  // Default values
  int n = 1;
  double dy = 0;
  double y0 = 0;
  
  using namespace datacondAPI;

  switch(Params.size())
  {
  case 3: // y = ramp(n, dy, y0);
    {
      const CallChain::Symbol* const y0_udt = Chain->GetSymbol(Params[2]);
      
      if (const Scalar<int>* const y0ptr
          = dynamic_cast<const Scalar<int>*>(y0_udt))
      {
        y0 = y0ptr->GetValue();
      }
      else if (const Scalar<float>* const y0ptr
               = dynamic_cast<const Scalar<float>*>(y0_udt))
      {
        y0 = y0ptr->GetValue();
      }
      else if (const Scalar<double>* const y0ptr
               = dynamic_cast<const Scalar<double>*>(y0_udt))
      {
        y0 = y0ptr->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(),
                                     3,
                               "Scalar<int>, Scalar<float> or Scalar<double>",
                                     TypeInfoTable.GetName(typeid(*y0_udt)));
      }
    }

    // Fall through
    
  case 2:  // y = ramp(n, dy);
    {
      const CallChain::Symbol* const dy_udt = Chain->GetSymbol(Params[1]);
      const CallChain::Symbol* const n_udt = Chain->GetSymbol(Params[0]);
      
      if (const Scalar<int>* const dy_ptr
          = dynamic_cast<const Scalar<int>*>(dy_udt))
      {
        dy = dy_ptr->GetValue();
      }
      else if (const Scalar<float>* const dy_ptr
               = dynamic_cast<const Scalar<float>*>(dy_udt))
      {
        dy = dy_ptr->GetValue();
      }
      else if (const Scalar<double>* const dy_ptr
               = dynamic_cast<const Scalar<double>*>(dy_udt))
      {
        dy = dy_ptr->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(),
                                     2,
                               "Scalar<int>, Scalar<float> or Scalar<double>",
                                     TypeInfoTable.GetName(typeid(*dy_udt)));
      }

      if (const Scalar<int>* const n_ptr
          = dynamic_cast<const Scalar<int>*>(n_udt))
      {
        n = n_ptr->GetValue();
      }
      else
      {
        throw CallChain::BadArgument(GetName(),
                                     1,
                                     "Scalar<int>",
                                     TypeInfoTable.GetName(typeid(*n_udt)));
      }
      
      if (n <= 0)
      {
        throw CallChain::BadArgument(GetName(), 1,
                                     "n <= 0", "n must be a positive integer");
      }
      
    }

    break;

  default:
    throw CallChain::BadArgumentCount(GetName(), "2-3", Params.size());
    break;
  }

  Sequence<double>* const seq = new Sequence<double>(n);

  for (size_t k = 0; k < seq->size(); ++k)
  {
    (*seq)[k] = k*dy + y0;
  }

  Chain->ResetOrAddSymbol(Ret, seq);  
}

