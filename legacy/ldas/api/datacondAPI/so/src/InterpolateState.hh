#ifndef INTERPOLATESTATE_HH
#define INTERPOLATESTATE_HH

#include <filters/Interpolate.hh>

#include "StateUDT.hh"

namespace datacondAPI {

    template<class T> class Sequence;
    template<class T> class TimeSeries;

    template<class T>
    class InterpolateState : public State, public Filters::Interpolate<T> {
    public:
        //: Constructor
        InterpolateState(const double alpha, const size_t order);

        //: Virtual constructor
        virtual InterpolateState* Clone() const;

        //: Apply to a Sequence in-place
        void apply(Sequence<T>& x);

        //: Apply to a TimeSeries in-place
        void apply(TimeSeries<T>& x);

        //: Convert this to an LdasElement
        //!exc: unimplemented_error - thrown if called
        virtual ILwd::LdasElement*
        ConvertToIlwd(const CallChain& Chain,
                      target_type Target = TARGET_GENERIC) const;

    private:
    };

}

#endif // INTERPOLATESTATE_HH
