#ifndef DATACONDAPI__TRANS_FUNC_H
#define DATACONDAPI__TRANS_FUNC_H

#include "DatacondCaller.h"

#ifdef __cplusplus
extern "C" {
#endif

  //---------------------------------------------------------------------
  // Translates a ILwd File to a UDT
  //---------------------------------------------------------------------
  int TranslateILwdFile( translation_direction Direction,
			 void** UserData,
			 void** DatacondData,
			 void* AuxData );
  //---------------------------------------------------------------------
  // Translates a UDT to a UDT
  //---------------------------------------------------------------------
  int TranslateUDT( translation_direction Direction,
		    void** UserData,
		    void** DatacondData,
		    void* AuxData );

#ifdef __cplusplus
}
#endif

#endif /* DATACONDAPI__TRANS_FUNC_H */
