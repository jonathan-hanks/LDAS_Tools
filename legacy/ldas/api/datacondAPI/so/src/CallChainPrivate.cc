#include "datacondAPI/config.h"

#include "ilwd/LdasHistory.hh"

#include "CallChainPrivate.hh"

CallChain::CallChainPrivate::
CallChainPrivate( )
  : m_current_history( (ILwd::LdasHistory*)NULL )
{
}

CallChain::CallChainPrivate::
~CallChainPrivate( )
{
  ResetHistory();
}

ILwd::LdasHistory* CallChain::CallChainPrivate::
GetHistory( ) const
{
  if ( m_current_history == (ILwd::LdasHistory*)NULL )
  {
    m_current_history = new ILwd::LdasHistory;
  }
  return m_current_history;
}

void CallChain::CallChainPrivate::
ResetHistory( )
{
  delete m_current_history;
  m_current_history = (ILwd::LdasHistory*)NULL;
}
