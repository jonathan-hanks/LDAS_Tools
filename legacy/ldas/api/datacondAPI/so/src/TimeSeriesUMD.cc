/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "config.h"

#include <general/types.hh>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarray.hh>
#include <filters/LDASConstants.hh>

#include "TimeSeriesUMD.hh"

using namespace datacondAPI;
using General::GPSTime;

datacondAPI::TimeSeriesMetaData::~TimeSeriesMetaData()
{
}

ILwd::LdasElement* datacondAPI::TimeSeriesMetaData::
ConvertToIlwd( const CallChain& Chain,
               datacondAPI::udt::target_type Target ) const
{
  ILwd::LdasContainer* container = new ILwd::LdasContainer;
  try {
    switch ( Target )
    {
    case datacondAPI::udt::TARGET_GENERIC:
      container->push_back( new ILwd::LdasArray<double>(m_fs, "SampleRate"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( new ILwd::LdasArray<double>(m_frequency,
							"BaseFrequency"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      container->push_back( new ILwd::LdasArray<double>(m_phase, "Phase"),
			    ILwd::LdasContainer::NO_ALLOCATE,
			    ILwd::LdasContainer::OWN );
      break;
    default:
      throw datacondAPI::udt::
        BadTargetConversion(Target,
                            "datacondAPI::TimeSeriesMetaData");
      break;
    }
  }
  catch ( ... )
  {
    delete container;
    throw;
  }
  return container;
}

datacondAPI::TimeSeriesMetaData::TimeSeriesMetaData(const double& fs,
						    const GPSTime& StartTime,
						    const double& frequency,
						    const double& phase)
  : WhenMetaData( StartTime )
{
  SetSampleRate(fs);
  
  // Note that correct setting of base frequency depends on sample rate
  SetBaseFrequency(frequency);

  SetPhase(phase);
}

datacondAPI::TimeSeriesMetaData::
TimeSeriesMetaData(const TimeSeriesMetaData& in)
    : WhenMetaData( in ),
      GeometryMetaData( in ),
      GapMetaData<General::GPSTime>( in ),
      m_fs(in.m_fs),
      m_frequency(in.m_frequency),
      m_phase(in.m_phase)
{
}

double datacondAPI::TimeSeriesMetaData::GetSampleRate() const 
{
  return m_fs;
}

void datacondAPI::TimeSeriesMetaData::SetSampleRate(const double& fs)
{
  if (fs <= 0.0)
  {
    throw std::invalid_argument(
        "TimeSeriesMetaData::SetSampleRate: "
	"sample rate fs is non-positive");
  }

  m_fs = fs;
}

double datacondAPI::TimeSeriesMetaData::GetStepSize() const
{
    return (1.0L/m_fs);
}

double datacondAPI::TimeSeriesMetaData::GetBaseFrequency() const 
{
  return m_frequency;
}

void datacondAPI::TimeSeriesMetaData::SetBaseFrequency(const double& frequency)
{
  // Ignore the possibility of base frequency being greater than Nyquist,
  // since this may cause resampling to fail
  m_frequency = frequency;
}

double datacondAPI::TimeSeriesMetaData::GetPhase() const 
{
  return m_phase;
}

void datacondAPI::TimeSeriesMetaData::SetPhase(const double& phase)
{
  if ((phase < 0) || (phase >= LDAS_TWOPI))
  {
    throw std::invalid_argument(
      "TimeSeriesMetaData::SetPhase: "
      "Phase outside [0, 2pi)");
  }

  m_phase = phase;
}

TimeSeriesMetaData& TimeSeriesMetaData::
operator=( const TimeSeriesMetaData& MetaData )
{
  if ( &MetaData != this )
  {
    static_cast<WhenMetaData& >(*this) = MetaData;
    static_cast<GeometryMetaData& >(*this) = MetaData;
    static_cast<GapMetaData<GPSTime>& >(*this) = MetaData;

    m_fs = MetaData.m_fs;
    m_frequency = MetaData.m_frequency;
    m_phase = MetaData.m_phase;
  }
  return *this;
}

bool TimeSeriesMetaData::
IsCompatable( const TimeSeriesMetaData& MetaData ) const
{
  if ( &MetaData != this )
  {
    if ( ( m_fs != MetaData.m_fs ) ||
	 ( GetStartTime() != MetaData.GetStartTime() ) )
    {
      return false;
    }
  }
  return true;
}
