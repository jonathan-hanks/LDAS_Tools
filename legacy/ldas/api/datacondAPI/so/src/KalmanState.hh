/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef KALMANSTATE_HH
#define KALMANSTATE_HH

// $Id: KalmanState.hh,v 1.9 2003/02/28 00:57:17 emaros Exp $

#include <general/unexpected_exception.hh>
#include "StateUDT.hh"

#include "SequenceUDT.hh"
#include "Matrix.hh"

//#include "Kalman.hh"

namespace datacondAPI 
{
  
  class KalmanState : public State
  {
  public: 
    friend class Kalman;
    
    //: Default constructor
    KalmanState();
    
    //: Specify-everything constructor
    //!param: A - operator that advances state in time
    //!param: W - process error covariance matrix
    //!param: C - operator that relates state to measurement
    //!param: V - measurement error covariance matrix
    //!param: psi - state vector
    //!param: P - state error covariance matrix
    KalmanState(const Matrix<double>& A, 
		const Matrix<double>& W,
		const Matrix<double>& C, 
		const Matrix<double>& V,
		const Sequence<double>& psi, 
		const Matrix<double>& P);

    //: Specify-everything constructor
    //!param: A - operator that advances state in time
    //!param: W - process error covariance matrix
    //!param: C - operator that relates state to measurement
    //!param: V - measurement error covariance matrix
    //!param: psi - state vector
    //!param: P - state error covariance matrix
    KalmanState(Matrix<float>& A, 
		Matrix<float>& W,
		Matrix<float>& C, 
		Matrix<float>& V,
		Sequence<float>& psi, 
		Matrix<float>& P);

    //: UDT constructor
    //!param: A - operator that advances state in time
    //!param: W - process error covariance matrix
    //!param: C - operator that relates state to measurement
    //!param: V - measurement error covariance matrix
    //!param: psi - state vector
    //!param: P - state error covariance matrix
    KalmanState(udt& A,
                udt& W,
                udt& C,
                udt& V,
                udt& psi,
                udt& P);


    //: Copy constructor
    //!param: const KalmanState& - object to copy from
    KalmanState(const KalmanState&);
    
    //: Destructor
    ~KalmanState();
    
    //: get copy of state evolution matrix
    //!param: Matrix<double>& - on return copy of state evolution matrix
    void getA(Matrix<double>& A) const;

    //: get copy of process error covariance matrix
    //!param: Matrix<double>& - on return copy of 
    //+ process error covariance matrix
    void getW(Matrix<double>& W) const;

    //: get copy of measurement error covariance matrix
    //!param: Matrix<double>& - on return copy of 
    //+ measurement error covariance matrix
    void getV(Matrix<double>& V) const;

    //: get copy of measurement operator
    //!param: Matrix<double>& - on return copy of 
    //+ measurement operator
    void getC(Matrix<double>& C) const;

    //: get copy of state vector
    //!param: Matrix<double>& - on return copy of current state vector
    void getPsi(Sequence<double>& psi) const;

    //: get copy of state error covariance matrix
    //!param: Matrix<double>& - on return copy of 
    //+ state error covariance matrix 
    void getP(Matrix<double>& P) const;

        //: get copy of state evolution matrix
    //!param: Matrix<double>& - on return copy of state evolution matrix
    void getA(Matrix<float>& A);

    //: get copy of process error covariance matrix
    //!param: Matrix<double>& - on return copy of 
    //+ process error covariance matrix
    void getW(Matrix<float>& W);

    //: get copy of measurement error covariance matrix
    //!param: Matrix<double>& - on return copy of 
    //+ measurement error covariance matrix
    void getV(Matrix<float>& V);

    //: get copy of measurement operator
    //!param: Matrix<double>& - on return copy of 
    //+ measurement operator
    void getC(Matrix<float>& C);

    //: get copy of state vector
    //!param: Matrix<double>& - on return copy of current state vector
    void getPsi(Sequence<float>& psi);

    //: get copy of state error covariance matrix
    //!param: Matrix<double>& - on return copy of 
    //+ state error covariance matrix 
    void getP(Matrix<float>& P);

    //: get copy of state evolution matrix
    //!param: A - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if A needs to be allocated and new fails
    //!exc: std::invalid_argument - if A not an appropriate pointer type
    void getA(udt*& A) throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception);

    //: get copy of process error covariance matrix
    //!param: W - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if W needs to be allocated and new fails
    //!exc: std::invalid_argument - if W not an appropriate pointer type
    void getW(udt*& W) throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception);

    //: get copy of measurement error covariance matrix
    //!param: V - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if V needs to be allocated and new fails
    //!exc: std::invalid_argument - if V not an appropriate pointer type
    void getV(udt*& V) throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception);

    //: get copy of measurement matrix
    //!param: C - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if C needs to be allocated and new fails
    //!exc: std::invalid_argument - if C not an appropriate pointer type
    void getC(udt*& C) throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception);

    //: get copy of current state vector prediction
    //!param: A - pointer to Sequence<double>. May be null, in which case
    //+ Sequence<double> will be allocated on heap
    //!exc: std::bad_alloc - if psi needs to be allocated and new fails
    //!exc: std::invalid_argument - if psi not an appropriate pointer type
    void getPsi(udt*& psi) throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception);

    //: get copy of state vector error covariance matrix
    //!param: P - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if P needs to be allocated and new fails
    //!exc: std::invalid_argument - if P not an appropriate pointer type
    void getP(udt*& P) throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception);
    
    //: get dimensions of state and sample
    //!param: M - will return with dimension of state
    //!param: N - will return with dimension of sample
    void getMN(int& M, int& N);

    //: Duplicate *this
    //!return: KalmanState* - copy of *this, allocated on heap
    KalmanState* Clone() const throw (std::bad_alloc,General::unexpected_exception);

    //: convert *this to an ILWD
    //!param: Chain - call chain *this belongs to
    //!param: Target - intended destination of ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    ILwd::LdasElement* 
    ConvertToIlwd( const CallChain& Chain, 
		   udt::target_type Target 
		   = udt::TARGET_GENERIC ) const; 

  private:

    //: Reset state vector. Generally used only by Kalman
    //!param: psi - new state vector
    void setPsi(const Sequence<double>& psi)
      throw (std::invalid_argument,General::unexpected_exception);

    //: Reset state vector error covariance matrix. 
    //+ Generally used only by Kalman
    //!param: P - new state vector error covariance matrix
    void setP(const Matrix<double>& P)
      throw (std::invalid_argument,General::unexpected_exception);

     //: Reset state vector. Generally used only by Kalman
    //!param: psi - new state vector
    void setPsi(const Sequence<float>& psi)
      throw (std::invalid_argument,General::unexpected_exception);

    //: Reset state vector error covariance matrix. 
    //+ Generally used only by Kalman
    //!param: P - new state vector error covariance matrix
    void setP(const Matrix<float>& P)
      throw (std::invalid_argument,General::unexpected_exception);
    
    Matrix<double> m_A;
    Matrix<double> m_W;
    Matrix<double> m_V;
    Matrix<double> m_C;
    Matrix<double> m_P;
    Sequence<double> m_psi;
  };

}
#endif // KALMANSTATE_HH
