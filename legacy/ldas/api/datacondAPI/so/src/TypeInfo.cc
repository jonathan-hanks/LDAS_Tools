#define	TYPE_INFO_CC

#include "config.h"

#include <complex>

#include "general/mutexlock.hh"

#include "TypeInfo.hh"

#include "DFTUDT.hh"
#include "result.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "StateUDT.hh"
#include "StatsUDT.hh"
#include "WaveletPSU.hh"
#include "VectorSequenceUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "WelchSpectrumUDT.hh"
#include "NPSpectrum.hh"
#include "InterpolateState.hh"

#ifdef HAVE_MATRIX
#include "Matrix.hh"
#endif	/* HAVE_MATRIX */

datacondAPI::TypeInfoTable_type::
TypeInfoTable_type(void)
{
}

bool datacondAPI::TypeInfoTable_type::
AddType( const std::type_info& Key, std::string Value )
{
  return m_table.insert(std::make_pair(&Key, TypeInfo_type( Value ) ) ).second;
}

void datacondAPI::TypeInfoTable_type::
DumpTable( std::ostream& Stream, Dump_type Mode )
{
  Stream << "Begin TypeInfo Table" << std::endl;
  for ( Map_type::const_iterator i = m_table.begin(); i != m_table.end(); i++ )
  {
    if ( Mode == KEY_VALUE )
    {
      Stream << "  Key: ";
    }
    if ( ( Mode == KEY_VALUE ) || ( Mode == KEY ) )
    {
      Stream << ( (*i).first )->name();
    }
    if ( Mode == KEY_VALUE )
    {
      Stream << " Value: ";
    }
    if ( ( Mode == KEY_VALUE ) || ( Mode == VALUE ) )
    {
      Stream << (*i).second.GetName();
    }
    Stream << std::endl;
  }
  Stream << "End TypeInfo Table" << std::endl;
}

std::string datacondAPI::TypeInfoTable_type::
GetName(const std::type_info& Key) const
{
  const Map_type::const_iterator
    p(m_table.find(&Key));

  if (p != m_table.end())
  {
    return p->second.GetName();
  }
  std::string msg("Unknown(");
  msg += Key.name();
  msg += ")";
  return msg;
}

datacondAPI::TypeInfoTable_type::TypeInfo_type::
TypeInfo_type(const std::string& Name)
  : m_name(Name)
{
}

const std::string& datacondAPI::TypeInfoTable_type::TypeInfo_type::
GetName(void) const
{
  return m_name;
}

datacondAPI::TypeInfoTable_type&
datacondAPI::TheTypeInfoTable( )
{
  static datacondAPI::TypeInfoTable_type	ti;

  return ti;
}
