#include "config.h"

#include <algorithm>
#include <cmath>
#include <valarray>
#include <memory>

#define UNARY_FUNCTION_CC

#include TARGET_HH

#include "general/Memory.hh"

#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"
#include "TypeInfo.hh"

using std::complex;
using namespace datacondAPI;


//=============================================================================
// Templated/ Helper functions
//=============================================================================

namespace {
  template < class T>
  inline T
  unary( const T& Source )
  {
    return action( Source );
  }

  template <>
  inline complex< float >
  unary( const complex< float >& Source )
  {
    return complex< float >( action ( Source.real( ) ),
			     action ( Source.imag( ) ) );
  }

  template <>
  inline complex< double >
  unary( const complex< double >& Source )
  {
    return complex< double >( action ( Source.real( ) ),
			      action ( Source.imag( ) ) );
  }

  template< class T>
  inline Scalar< T >*
  eval_scalar( udt* Source )
  {
    if ( udt::IsA< Scalar< T > >( *Source ) )
    {
      std::unique_ptr< Scalar< T > >
	a( (udt::Cast< Scalar< T > >( *Source ) ).Clone( ) );
      
      a->SetValue( unary( a->GetValue( ) ) );

      return a.release( );
    }
    return ( Scalar< T >*)NULL;
  }

  template< class T >
  inline Sequence< T >*
  eval_sequence( udt* Source )
  try
  {
    if ( udt::IsA< Sequence< T > >( *Source ) )
    {
      std::unique_ptr< Sequence< T > >
	a( (udt::Cast< Sequence< T > >( *Source ) ).Clone( ) );
      
    
      std::transform
	( &( (*a)[ 0 ] ),
	  &( (*a)[ (*a).size() ] ),
	  &( (*a)[ 0 ] ),
	  unary<T> );

      return a.release( );
    }
    return ( Sequence< T >*)NULL;
  }
  catch( ... )
  {
    return ( Sequence< T >*)NULL;
  }
}

//=============================================================================
// Unary Function
//=============================================================================

UnaryClass::
UnaryClass()
  : Function( UnaryClass::GetName() )
{
  //---------------------------------------------------------------------------
  // The sole purpose of the constructor is to register the action name, from
  // GetName(), using the Function constructor.
  //---------------------------------------------------------------------------
}

void UnaryClass::
Eval( CallChain* Chain, const CallChain::Params& Params,
      const std::string& Ret )
const
{
  if ( 1 != Params.size() )
  {
    throw CallChain::BadArgumentCount( GetName(),
				       1,
				       Params.size() );
  }

  udt*	operand(Chain->GetSymbol(Params[0]));
  udt*	answer( (udt*)NULL );

  if ( ( answer = eval_sequence< complex< double > >( operand ) ) ||
       ( answer = eval_sequence< complex< float > >( operand ) ) ||
       ( answer = eval_sequence< double >( operand ) ) ||
       ( answer = eval_sequence< float >( operand ) ) ||
       ( answer = eval_scalar< complex< double > >( operand ) ) ||
       ( answer = eval_scalar< complex< float > >( operand ) ) ||
       ( answer = eval_scalar< double >( operand ) ) ||
       ( answer = eval_scalar< float >( operand ) ) ||
       ( answer = eval_scalar< int >( operand ) ) ||
       ( answer = eval_scalar< short >( operand ) ) )
  {
    // Add history Record
    answer->AppendHistory( GetName(), operand, (udt*)NULL );
  }
  else
  {
    std::string msg("unable to " OPERATION " type ");
    msg += TypeInfoTable.GetName(typeid(*operand));
    throw std::runtime_error(msg);
  }

  Chain->ResetOrAddSymbol( Ret, answer );
}

const std::string& UnaryClass::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name( OPERATION );

  return name;
}

//-----------------------------------------------------------------------------
// We construct a static instance of the Function class. The
// constructor will register the name and enable Function::Eval
// to be called whenever the name is encountered.
//-----------------------------------------------------------------------------

static UnaryClass _a_local_instance;
