#include "datacondAPI/config.h"

#include "ResolvePSUFunction.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TimeSeries.hh"
#include "UDT.hh"

#include "resolver.hh"
#include "result.hh"
#include "WaveletPSU.hh"
#include "HaarPSU.hh"

using namespace datacondAPI::psu;

const std::string& ResolvePSUFunction::GetName(void) const
{
  static std::string name("ResolvePSU");

  return name;
}

ResolvePSUFunction::ResolvePSUFunction() : Function( ResolvePSUFunction::GetName() )
{}

static ResolvePSUFunction _ResolvePSUFunction;

void ResolvePSUFunction::Eval(CallChain* Chain, const CallChain::Params& Params,
			   const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;

  switch(Params.size())
    {
    case 3:
      {
	//ResolvePSU(wavelet, levels, Sequence_of_data/TimeSeries_of_data)

	datacondAPI::udt *inW( Chain->GetSymbol(Params[0]));
	
	if(datacondAPI::udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	  {
	    Resolver r(*inW, *Chain->GetSymbol(Params[1]));

	    if (datacondAPI::udt::IsA<datacondAPI::TimeSeries<double> >( *Chain->GetSymbol(Params[2])))
	      {
		datacondAPI::TimeSeries<double> in(datacondAPI::udt::Cast<datacondAPI::TimeSeries<double> >(*Chain->GetSymbol(Params[2])));
		Result<double> out;
		datacondAPI::udt* pout = &out;
		r.apply(pout, in);
		returnedSymbol = new Result<double> (out);
	      }

	    else if(datacondAPI::udt::IsA<datacondAPI::TimeSeries<float> >( *Chain->GetSymbol(Params[2])))
	      {
		datacondAPI::TimeSeries<float> in(datacondAPI::udt::Cast<datacondAPI::TimeSeries<float> >(*Chain->GetSymbol(Params[2])));
	        Result<float> out;
		datacondAPI::udt* pout = &out;
		r.apply(pout, in);
		returnedSymbol = new Result<float> (datacondAPI::udt::Cast<Result<float> >(out));
	      }

	    else if(datacondAPI::udt::IsA<datacondAPI::Sequence<double> >( *Chain->GetSymbol(Params[2])))
	      {
		datacondAPI::Sequence<double> in(datacondAPI::udt::Cast<datacondAPI::Sequence<double> >(*Chain->GetSymbol(Params[2])));
		Result<double> out;
		datacondAPI::udt* pout = &out;
		r.apply(pout, in);
		returnedSymbol = new Result<double> (out);
	      }
	    else if(datacondAPI::udt::IsA<datacondAPI::Sequence<float> >( *Chain->GetSymbol(Params[2])))
	      {
		datacondAPI::Sequence<float> in(datacondAPI::udt::Cast<datacondAPI::Sequence<float> >(*Chain->GetSymbol(Params[2])));
		Result<float> out;
		datacondAPI::udt* pout = &out;
		r.apply(pout, in);
		returnedSymbol = new Result<float> (out);
	      }
	    else
	      {
		throw CallChain::BadArgument(GetName(), 0, "Unknown parameter type", "Usage: ResolvePSUFunction(wavelet, levels, data)");
	      }
	  }

	else
	  throw CallChain::BadArgument(GetName(), 0, "Unknown parameter type", "Usage: ResolvePSUFunction(wavelet, levels, data)");
	

	
        break;
      }//end case 3

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: ResolvePSUFunction(wavelet, levels, sequence_of_data)");
	break;
      }//end default

   }//end switch

  if(!returnedSymbol)
  { 
    throw CallChain::NullResult( GetName() );
  }

  Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval

