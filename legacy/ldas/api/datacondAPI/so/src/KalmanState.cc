/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: KalmanState.cc,v 1.14 2006/02/16 16:54:58 emaros Exp $
#include "datacondAPI/config.h"

#include "KalmanState.hh"

namespace 
{
  const char rcsID[] = "@(#) $Id: KalmanState.cc,v 1.14 2006/02/16 16:54:58 emaros Exp $";
}

datacondAPI::KalmanState::KalmanState(){}

datacondAPI::KalmanState::KalmanState(const Matrix<double>& A, 
				      const Matrix<double>& W,
				      const Matrix<double>& C, 
				      const Matrix<double>& V,
				      const Sequence<double>& psi, 
				      const Matrix<double>& P):m_A(A) 
{
  int m = A.getNRows();
  int n = V.getNCols();

  if(int(A.getNCols()) != m || int(W.getNRows()) != m || int(W.getNCols()) != m || int(C.getNRows()) !=n || int(C.getNCols()) != m || int(V.getNRows()) != n || int(psi.size()) != m || int(P.getNCols()) != m || int(P.getNRows())!= m)
    {
      throw std::invalid_argument ("Invalid dimensions for Kalman input");
    }

  m_W = W;
  m_V = V;
  m_C = C;
  m_P = P;
  m_psi = psi;
}

datacondAPI::KalmanState::KalmanState(Matrix<float>& A, 
				      Matrix<float>& W,
				      Matrix<float>& C, 
				      Matrix<float>& V,
				      Sequence<float>& psi, 
				      Matrix<float>& P)
{
  int m = A.getNRows();
  int n = V.getNCols();

  if(int(A.getNCols()) != m || int(W.getNRows()) != m || int(W.getNCols()) != m || int(C.getNRows()) !=n || 
     int(C.getNCols()) != m || int(V.getNRows()) != n || int(psi.size()) != m || int(P.getNCols()) != m 
     || int(P.getNRows())!= m)
    {
      throw std::invalid_argument ("Invalid dimensions for Kalman input");
    }

  {
      std::valarray<float> mA;
      A.getData(mA);
      std::valarray<double> dmA(mA.size());
      for(size_t i = 0;i<mA.size();i++)
	{
	  dmA[i]=mA[i];
	}
      Matrix<double> temp(dmA,A.getNRows(),A.getNCols());
      m_A=temp;
      }
      
      {
      std::valarray<float> mW;
      W.getData(mW);
      std::valarray<double> dmW(mW.size());
      for(size_t i = 0;i<mW.size();i++)
	{
	  dmW[i]=mW[i];
	}
      Matrix<double> temp(dmW,W.getNRows(),W.getNCols());
      m_W=temp;
      }

       {
      std::valarray<float> mC;
      C.getData(mC);
      std::valarray<double> dmC(mC.size());
      for(size_t i = 0;i<mC.size();i++)
	{
	  dmC[i]=mC[i];
	}
      Matrix<double> temp(dmC,C.getNRows(),C.getNCols());
      m_C=temp;
       }

       {
      std::valarray<float> mV;
      V.getData(mV);
      std::valarray<double> dmV(mV.size());
      for(size_t i = 0;i<mV.size();i++)
	{
	  dmV[i]=mV[i];
	}
      Matrix<double> temp(dmV,V.getNRows(),V.getNCols());
      m_V=temp;
       }

        {
      std::valarray<float> mP;
      P.getData(mP);
      std::valarray<double> dmP(mP.size());
      for(size_t i = 0;i<mP.size();i++)
	{
	  dmP[i]=mP[i];
	}
      Matrix<double> temp(dmP,P.getNRows(),P.getNCols());
      m_P=temp;
      }
      
     {
      std::valarray<double> dmpsi(psi.size());
      for(size_t i = 0;i<dmpsi.size();i++)
	{
	  dmpsi[i]=psi[i];
	}
      m_psi=dmpsi;
     }
}

datacondAPI::KalmanState::KalmanState(udt& A,
				      udt& W,
				      udt& C,
				      udt& V,
				      udt& psi,
				      udt& P)
{
  if((!udt::IsA<Matrix<double> >(A)||!udt::IsA<Matrix<double> >(W)||
     !udt::IsA<Matrix<double> >(C)||!udt::IsA<Matrix<double> >(V)||
     !udt::IsA<Sequence<double> >(psi)||!udt::IsA<Matrix<double> >(P))&&
     (!udt::IsA<Matrix<float> >(A)||!udt::IsA<Matrix<float> >(W)||
     !udt::IsA<Matrix<float> >(C)||!udt::IsA<Matrix<float> >(V)||
      !udt::IsA<Sequence<float> >(psi)||!udt::IsA<Matrix<float> >(P)))
    {
      throw std::invalid_argument("Wrong UDT type for Kalman arguments");
    }
  if(udt::IsA<Matrix<double> >(A))
    {
      m_A = udt::Cast<Matrix<double> >(A);
      m_W = udt::Cast<Matrix<double> >(W);
      m_C = udt::Cast<Matrix<double> >(C);
      m_V = udt::Cast<Matrix<double> >(V);
      m_psi = udt::Cast<Sequence<double> >(psi);
      m_P = udt::Cast<Matrix<double> >(P);
    }
  else
    {
      
      {
      std::valarray<float> mA;
      udt::Cast<Matrix<float> >(A).getData(mA);
      std::valarray<double> dmA(mA.size());
      for(size_t i = 0;i<mA.size();i++)
	{
	  dmA[i]=mA[i];
	}
      Matrix<double> temp(dmA,udt::Cast<Matrix<float> >(A).getNRows(),udt::Cast<Matrix<float> >(A).getNCols());
      m_A=temp;
      }
      
      {
      std::valarray<float> mW;
      udt::Cast<Matrix<float> >(W).getData(mW);
      std::valarray<double> dmW(mW.size());
      for(size_t i = 0;i<mW.size();i++)
	{
	  dmW[i]=mW[i];
	}
      Matrix<double> temp(dmW,udt::Cast<Matrix<float> >(W).getNRows(),udt::Cast<Matrix<float> >(W).getNCols());
      m_W=temp;
      }

       {
      std::valarray<float> mC;
      udt::Cast<Matrix<float> >(C).getData(mC);
      std::valarray<double> dmC(mC.size());
      for(size_t i = 0;i<mC.size();i++)
	{
	  dmC[i]=mC[i];
	}
      Matrix<double> temp(dmC,udt::Cast<Matrix<float> >(C).getNRows(),udt::Cast<Matrix<float> >(C).getNCols());
      m_C=temp;
       }

       {
      std::valarray<float> mV;
      udt::Cast<Matrix<float> >(V).getData(mV);
      std::valarray<double> dmV(mV.size());
      for(size_t i = 0;i<mV.size();i++)
	{
	  dmV[i]=mV[i];
	}
      Matrix<double> temp(dmV,udt::Cast<Matrix<float> >(V).getNRows(),udt::Cast<Matrix<float> >(V).getNCols());
      m_V=temp;
       }

        {
      std::valarray<float> mP;
      udt::Cast<Matrix<float> >(P).getData(mP);
      std::valarray<double> dmP(mP.size());
      for(size_t i = 0;i<mP.size();i++)
	{
	  dmP[i]=mP[i];
	}
      Matrix<double> temp(dmP,udt::Cast<Matrix<float> >(P).getNRows(),udt::Cast<Matrix<float> >(P).getNCols());
      m_P=temp;
      }
      
     {
      std::valarray<double> dmpsi(udt::Cast<Sequence<float> >(psi).size());
      for(size_t i = 0;i<dmpsi.size();i++)
	{
	  dmpsi[i]=udt::Cast<Sequence<float> >(psi)[i];
	}
      m_psi=dmpsi;
     }
       
    }
  int m = m_A.getNRows();
  int n = m_V.getNCols();

  if(int(m_A.getNCols()) != m || int(m_W.getNRows()) != m || int(m_W.getNCols()) != m || int(m_C.getNRows()) !=n ||
     int(m_C.getNCols()) != m || int(m_V.getNRows()) != n || int(m_psi.size()) != m || int(m_P.getNCols()) != m
     || int(m_P.getNRows()) != m)
    {
      throw std::invalid_argument ("Invalid dimensions for Kalman input");
    }

}


datacondAPI::KalmanState::
KalmanState(const KalmanState& state)
  : State( state ),
    m_A(state.m_A)
{
  m_W = state.m_W;
  m_V = state.m_V;
  m_C = state.m_C;
  m_P = state.m_P;
  m_psi = state.m_psi;
}

datacondAPI::KalmanState::
~KalmanState(){}

datacondAPI::KalmanState* 
datacondAPI::KalmanState::
Clone() const throw (std::bad_alloc,General::unexpected_exception)
{
  // Cloning not supported
  throw std::bad_alloc();
}

void datacondAPI::KalmanState::
getA(Matrix<double>& A) const {A=m_A;}

void datacondAPI::KalmanState::
getW(Matrix<double>& W) const {W=m_W;}

void datacondAPI::KalmanState::
getV(Matrix<double>& V) const {V=m_V;}

void datacondAPI::KalmanState::
getC(Matrix<double>& C) const {C=m_C;}

void datacondAPI::KalmanState::
getPsi(Sequence<double>& psi) const {psi=m_psi;}

void datacondAPI::KalmanState::
getP(Matrix<double>& P) const {P=m_P;}

void datacondAPI::KalmanState::
getA(Matrix<float>& A)
{
  std::valarray<double> temp;
  m_A.getData(temp);
  std::valarray<float> help(temp.size());
  for(size_t i=0;i<help.size();i++)
    help[i]=temp[i];
  Matrix<float> argh(help,m_A.getNRows(),m_A.getNCols());
  A=argh;
}

void datacondAPI::KalmanState::
getW(Matrix<float>& W) 
{
 std::valarray<double> temp;
  m_W.getData(temp);
  std::valarray<float> help(temp.size());
  for(size_t i=0;i<help.size();i++)
    help[i]=temp[i];
  Matrix<float> argh(help,m_W.getNRows(),m_W.getNCols());
  W=argh;
}

void datacondAPI::KalmanState::
getV(Matrix<float>& V) 
{
 std::valarray<double> temp;
  m_V.getData(temp);
  std::valarray<float> help(temp.size());
  for(size_t i=0;i<help.size();i++)
    help[i]=temp[i];
  Matrix<float> argh(help,m_V.getNRows(),m_V.getNCols());
  V=argh;
}

void datacondAPI::KalmanState::
getC(Matrix<float>& C)
{
 std::valarray<double> temp;
  m_C.getData(temp);
  std::valarray<float> help(temp.size());
  for(size_t i=0;i<help.size();i++)
    help[i]=temp[i];
  Matrix<float> argh(help,m_C.getNRows(),m_C.getNCols());
  C=argh;
}

void datacondAPI::KalmanState::
getPsi(Sequence<float>& psi) 
{
  std::valarray<float> help(psi.size());
  for(size_t i=0;i<help.size();i++)
    help[i]=psi[i];
  psi=help;
}

void datacondAPI::KalmanState::
getP(Matrix<float>& P) 
{
 std::valarray<double> temp;
  m_P.getData(temp);
  std::valarray<float> help(temp.size());
  for(size_t i=0;i<help.size();i++)
    help[i]=temp[i];
  Matrix<float> argh(help,m_P.getNRows(),m_P.getNCols());
  P=argh;
}

void datacondAPI::KalmanState::
getMN(int& M, int& N) 
{
  M = m_A.getNRows();
  N = m_V.getNCols();
}

void datacondAPI::KalmanState::getA(datacondAPI::udt*& A)
  throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception)
{
  if(A==NULL)
    {
      Matrix<double> *a = (Matrix<double>*)NULL;
      try{
	a = new Matrix<double>;
	*a=m_A;
	A=a;
      }
      catch(...)
        {
	  delete a;
	  throw;
        }
    }
  else if (udt::IsA<Matrix<double> >(*A))
    {
      Matrix<double>& tmp = udt::Cast<Matrix<double> >(*A);
      tmp=m_A;
    }
  else
    throw std::invalid_argument("Wrong UDT type for KalmanState.getA");
}

void datacondAPI::KalmanState::getW(datacondAPI::udt*& W)
  throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception)
{
  if(W==NULL)
    {
      Matrix<double> *a = (Matrix<double>*)NULL;
      try{
	a = new Matrix<double>;
	*a=m_W;
	W=a;
      }
      catch(...)
        {
	  delete a;
	  throw;
        }
    }
  else if (udt::IsA<Matrix<double> >(*W))
    {
      Matrix<double>& tmp = udt::Cast<Matrix<double> >(*W);
      tmp=m_W;
    }
  else
    throw std::invalid_argument("Wrong UDT type for KalmanState.getW");
}

void datacondAPI::KalmanState::getV(datacondAPI::udt*& V)
  throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception)
{
  if(V==NULL)
    {
      Matrix<double> *a = (Matrix<double>*)NULL;
      try{
	a = new Matrix<double>;
	*a=m_V;
	V=a;
      }
      catch(...)
        {
	  delete a;
	  throw;
        }
    }
  else if (udt::IsA<Matrix<double> >(*V))
    {
      Matrix<double>& tmp = udt::Cast<Matrix<double> >(*V);
      tmp=m_V;
    }
  else
    throw std::invalid_argument("Wrong UDT type for KalmanState.getV");
}

void datacondAPI::KalmanState::getC(datacondAPI::udt*& C)
  throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception)
{
  if(C==NULL)
    {
      Matrix<double> *a = (Matrix<double>*)NULL;
      try{
	a = new Matrix<double>;
	*a=m_C;
	C=a;
      }
      catch(...)
        {
	  delete a;
	  throw;
        }
    }
  else if (udt::IsA<Matrix<double> >(*C))
    {
      Matrix<double>& tmp = udt::Cast<Matrix<double> >(*C);
      tmp=m_C;
    }
  else
    throw std::invalid_argument("Wrong UDT type for KalmanState.getC");
}

void datacondAPI::KalmanState::getPsi(datacondAPI::udt*& psi)
  throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception)
{
  if(psi==NULL)
    {
      Sequence<double> *a = (Sequence<double>*)NULL;
      try{
	a = new Sequence<double>;
	*a=m_psi;
	psi=a;
      }
      catch(...)
        {
	  delete a;
	  throw;
        }
    }
  else if (udt::IsA<Sequence<double> >(*psi))
    {
      Sequence<double>& tmp = udt::Cast<Sequence<double> >(*psi);
      tmp=m_psi;
    }
  else
    throw std::invalid_argument("Wrong UDT type for KalmanState.getPsi");
}

void datacondAPI::KalmanState::getP(datacondAPI::udt*& P)
  throw (std::bad_alloc,std::invalid_argument,General::unexpected_exception)
{
  if(P==NULL)
    {
      Matrix<double> *a = (Matrix<double>*)NULL;
      try{
	a = new Matrix<double>;
	*a=m_P;
	P=a;
      }
      catch(...)
        {
	  delete a;
	  throw;
        }
    }
  else if (udt::IsA<Matrix<double> >(*P))
    {
      Matrix<double>& tmp = udt::Cast<Matrix<double> >(*P);
      tmp=m_P;
    }
  else
    throw std::invalid_argument("Wrong UDT type for KalmanState.getP");
}


void datacondAPI::KalmanState::
setPsi(const Sequence<double>& psi) throw (std::invalid_argument,General::unexpected_exception) {}

void datacondAPI::KalmanState::
setP(const Matrix<double>& P) throw (std::invalid_argument,General::unexpected_exception) {}

void datacondAPI::KalmanState::
setPsi(const Sequence<float>& psi) throw (std::invalid_argument,General::unexpected_exception) {}

void datacondAPI::KalmanState::
setP(const Matrix<float>& P) throw (std::invalid_argument,General::unexpected_exception) {}

ILwd::LdasElement* 
datacondAPI::KalmanState::
ConvertToIlwd( const CallChain& Chain, 
	       datacondAPI::udt::target_type Target ) const
{
  //:TODO: Use toss<> instead
  throw datacondAPI::udt::BadTargetConversion(Target,
					      "datacondAPI::KalmanState");
}
