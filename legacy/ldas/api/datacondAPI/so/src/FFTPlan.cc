#include "datacondAPI/config.h"

#include <time.h>
#include <stdexcept>
#include <memory>
#include <valarray>

#include <fftw3.h>

#include "general/unordered_map.hh"
#include "general/mutexlock.hh"

#include "FFTPlan.hh"

#define DEBUG_FFTPLAN

using namespace datacondAPI;
using std::complex;
using std::bad_alloc;

namespace {

    // A mutex is needed around plan creation and destruction
    pthread_mutex_t fftw_mutex = PTHREAD_MUTEX_INITIALIZER;

}

namespace datacondAPI {

    // A key for 1-D plans
    class PlanKey {
    public:
        PlanKey(int n, bool isForward)
            : m_n(n), m_isForward(isForward) { }

	bool operator==(const PlanKey& rhs) const
	{
	    return ((m_n == rhs.m_n) && (m_isForward == rhs.m_isForward));
	}

        int  m_n;
        bool m_isForward;
    };

    const int DEFAULT_FLAGS = FFTW_ESTIMATE;

    //
    // Class for real, single precision 1-D plans
    //
    class RealFloatPlanRecord {
    public:
        RealFloatPlanRecord(int n, bool isForward);

	~RealFloatPlanRecord();

        time_t m_creation_time;
        int    m_use_count;
	int    m_n;
	bool   m_isForward;

        fftwf_plan m_plan;

    private:
        RealFloatPlanRecord(const RealFloatPlanRecord&);
        const RealFloatPlanRecord& operator=(const RealFloatPlanRecord&);
    };

    RealFloatPlanRecord::RealFloatPlanRecord(int n, bool isForward)
        : m_creation_time(time(0)), m_use_count(0),
	  m_n(n), m_isForward(isForward), m_plan(0)
    {
        const fftw_r2r_kind kind = (isForward ? FFTW_R2HC : FFTW_HC2R);
        std::valarray<float> in(n);

        const MutexLock lock(fftw_mutex);

        m_plan = fftwf_plan_r2r_1d(n, &in[0], &in[0], kind, DEFAULT_FLAGS);
        
        if (m_plan == 0)
        {
            throw bad_alloc();
        }
    }

    RealFloatPlanRecord::~RealFloatPlanRecord()
    {
        fftwf_destroy_plan(m_plan);
	m_plan = 0;
    }


    //
    // Class for complex, single precision 1-D plans
    //
    class ComplexFloatPlanRecord {
    public:
        ComplexFloatPlanRecord(int n, bool isForward);

	~ComplexFloatPlanRecord();

        time_t m_creation_time;
        int    m_use_count;
	int    m_n;
	bool   m_isForward;

        fftwf_plan m_plan;

    private:
        ComplexFloatPlanRecord(const ComplexFloatPlanRecord&);
        const ComplexFloatPlanRecord& operator=(const ComplexFloatPlanRecord&);
    };

    ComplexFloatPlanRecord::ComplexFloatPlanRecord(int n, bool isForward)
        : m_creation_time(time(0)), m_use_count(0),
	  m_n(n), m_isForward(isForward), m_plan(0)
    {
        const int kind = (isForward ? FFTW_FORWARD : FFTW_BACKWARD);
        std::valarray<complex<float> > in(n);
        fftwf_complex* const in_ptr = reinterpret_cast<fftwf_complex*>(&in[0]);

        const MutexLock lock(fftw_mutex);

        m_plan = fftwf_plan_dft_1d(n, in_ptr, in_ptr, kind, DEFAULT_FLAGS);
        
        if (m_plan == 0)
        {
            throw bad_alloc();
        }
    }

    ComplexFloatPlanRecord::~ComplexFloatPlanRecord()
    {
        fftwf_destroy_plan(m_plan);
	m_plan = 0;
    }

    //
    // Class for real, double precision 1-D plans
    //
    class RealDoublePlanRecord {
    public:
        RealDoublePlanRecord(int n, bool isForward);

	~RealDoublePlanRecord();

        time_t m_creation_time;
        int    m_use_count;
	int    m_n;
	bool   m_isForward;

        fftw_plan m_plan;

    private:
        RealDoublePlanRecord(const RealDoublePlanRecord&);
        RealDoublePlanRecord& operator=(const RealDoublePlanRecord&);
    };

    RealDoublePlanRecord::RealDoublePlanRecord(int n, bool isForward)
        : m_creation_time(time(0)), m_use_count(0),
	  m_n(n), m_isForward(isForward), m_plan(0)
    {
        const fftw_r2r_kind kind = (isForward ? FFTW_R2HC : FFTW_HC2R);
        std::valarray<double> in(n);

        const MutexLock lock(fftw_mutex);

        m_plan = fftw_plan_r2r_1d(n, &in[0], &in[0], kind, DEFAULT_FLAGS);
        
        if (m_plan == 0)
        {
            throw bad_alloc();
        }
    }

    RealDoublePlanRecord::~RealDoublePlanRecord()
    {
        const MutexLock lock(fftw_mutex);
        fftw_destroy_plan(m_plan);
	m_plan = 0;
    }


    //
    // Class for complex, double precision 1-D plans
    //
    class ComplexDoublePlanRecord {
    public:
        ComplexDoublePlanRecord(int n, bool isForward);

	~ComplexDoublePlanRecord();

        time_t m_creation_time;
        int    m_use_count;
	int    m_n;
	bool   m_isForward;

        fftw_plan m_plan;

    private:
        ComplexDoublePlanRecord(const ComplexDoublePlanRecord&);
        ComplexDoublePlanRecord& operator=(const ComplexDoublePlanRecord&);
    };

    ComplexDoublePlanRecord::ComplexDoublePlanRecord(int n, bool isForward)
        : m_creation_time(time(0)), m_use_count(0),
	  m_n(n), m_isForward(isForward), m_plan(0)
    {
        const int kind = (isForward ? FFTW_FORWARD : FFTW_BACKWARD);
        std::valarray<complex<double> > in(n);
        fftw_complex* const in_ptr = reinterpret_cast<fftw_complex*>(&in[0]);

        const MutexLock lock(fftw_mutex);

        m_plan = fftw_plan_dft_1d(n, in_ptr, in_ptr, kind, DEFAULT_FLAGS);
        
        if (m_plan == 0)
        {
            throw bad_alloc();
        }
    }

    ComplexDoublePlanRecord::~ComplexDoublePlanRecord()
    {
        const MutexLock lock(fftw_mutex);
        fftw_destroy_plan(m_plan);
	m_plan = 0;
    }

}

HASH_NAMESPACE_BEGIN
{
  template<>
  class hash<PlanKey>
  {
  public:
    // Hash function for plans
    size_t
    operator() (const PlanKey& key) const
    {
	return (key.m_n + key.m_isForward);
    }
  };
}
HASH_NAMESPACE_END

namespace datacondAPI {

    using General::unordered_map;
   
    size_t PlanDB_map_max_size = 32;

    // Mutex for locking the PlanDB_map
    pthread_mutex_t PlanDB_mutex = PTHREAD_MUTEX_INITIALIZER;
    
    typedef unordered_map<PlanKey, RealFloatPlanRecord*> RealFloatPlanMap;
    typedef unordered_map<PlanKey, ComplexFloatPlanRecord*> ComplexFloatPlanMap;

    typedef unordered_map<PlanKey, RealDoublePlanRecord*> RealDoublePlanMap;
    typedef unordered_map<PlanKey, ComplexDoublePlanRecord*> ComplexDoublePlanMap;

    RealFloatPlanMap RFPlanDB_map;
    ComplexFloatPlanMap CFPlanDB_map;

    RealDoublePlanMap RDPlanDB_map;
    ComplexDoublePlanMap CDPlanDB_map;

    size_t PlanDBMgr::size()
    {
	return (RFPlanDB_map.size() + CFPlanDB_map.size() 
		+ RDPlanDB_map.size() + CDPlanDB_map.size());
    }

    void PlanDBMgr::purge()
    {
	{
	    RealFloatPlanMap::iterator iter = RFPlanDB_map.begin();
	    while (iter != RFPlanDB_map.end())
	    {
		RealFloatPlanMap::iterator pred = iter++;
		if (pred->second->m_use_count == 0)
		{
		    delete pred->second;
		    RFPlanDB_map.erase(pred);
		}
	    }
	}
	
	{
	    ComplexFloatPlanMap::iterator iter = CFPlanDB_map.begin();
	    while (iter != CFPlanDB_map.end())
	    {
		ComplexFloatPlanMap::iterator pred = iter++;
		if (pred->second->m_use_count == 0)
		{
		    delete pred->second;
		    CFPlanDB_map.erase(pred);
		}
	    }
	}
	
	{
	    RealDoublePlanMap::iterator iter = RDPlanDB_map.begin();
	    while (iter != RDPlanDB_map.end())
	    {
		RealDoublePlanMap::iterator pred = iter++;
		if (pred->second->m_use_count == 0)
		{
		    delete pred->second;
		    RDPlanDB_map.erase(pred);
		}
	    }
	}

	{
	    ComplexDoublePlanMap::iterator iter = CDPlanDB_map.begin();
	    while (iter != CDPlanDB_map.end())
	    {
		ComplexDoublePlanMap::iterator pred = iter++;
		if (pred->second->m_use_count == 0)
		{
		    delete pred->second;
		    CDPlanDB_map.erase(pred);
		}
	    }
	}
    }

    bool PlanDB_has_space()
    {
	bool has_space = false;

	if (PlanDBMgr::size() < PlanDB_map_max_size)
	{
	    has_space = true;
	}
	else
	{
	    PlanDBMgr::purge();
	    if (PlanDBMgr::size() < PlanDB_map_max_size)
	    {
		has_space = true;
	    }
	    else
	    {
		has_space = false;
	    }
	}

	return has_space;
    }


} // anonymous namespace

namespace datacondAPI {

    //
    // Single-precision real 1-D plan
    //
    RealFloatFFTPlan::RealFloatFFTPlan(int n, bool isForward)
	: m_plan_record(0)
    {
        const MutexLock lock(PlanDB_mutex);
        
	const PlanKey key(n, isForward);
 
	typedef RealFloatPlanMap::iterator iterator;
	
        iterator plan_position = RFPlanDB_map.find(key);

        if (plan_position == RFPlanDB_map.end())
        {
            //
            // The plan wasn't there, so we construct one.
            //
	    if (PlanDB_has_space())
	    {
		m_plan_record = new RealFloatPlanRecord(n, isForward);
		RFPlanDB_map[key] = m_plan_record;
	    }
	    else
	    {
	      throw std::runtime_error("RealFloatFFTPlan::RealFloatFFTPlan() "
				       "too many FFT plans in use");
	    }
        }
        else
        {
            // The plan was found in the database, so we can get it from 
            // the iterator (recall that dereferencing the iterator
            // plan_position gives us an STL pair<Key, Value>)
            m_plan_record = plan_position->second;
        }
        
        ++(m_plan_record->m_use_count);
    }

    RealFloatFFTPlan::~RealFloatFFTPlan()
    {
        const MutexLock lock(PlanDB_mutex);
        
        --(m_plan_record->m_use_count);
    }
    
    void
    RealFloatFFTPlan::apply(float* data) const
    {
	fftwf_execute_r2r(m_plan_record->m_plan, data, data);
    }


    //
    // Single-precision complex 1-D plan
    //
    ComplexFloatFFTPlan::ComplexFloatFFTPlan(int n, bool isForward)
	: m_plan_record(0)
    {
        const MutexLock lock(PlanDB_mutex);
        
	const PlanKey key(n, isForward);
 
	typedef ComplexFloatPlanMap::iterator iterator;
	
        iterator plan_position = CFPlanDB_map.find(key);

        if (plan_position == CFPlanDB_map.end())
        {
            //
            // The plan wasn't there, so we construct one.
            //
	    if (PlanDB_has_space())
	    {
		m_plan_record = new ComplexFloatPlanRecord(n, isForward);
		CFPlanDB_map[key] = m_plan_record;
	    }
	    else
	    {
	 throw std::runtime_error("ComplexFloatFFTPlan::ComplexFloatFFTPlan() "
				  "too many FFT plans in use");
	    }
        }
        else
        {
            // The plan was found in the database, so we can get it from 
            // the iterator (recall that dereferencing the iterator
            // plan_position gives us an STL pair<Key, Value>)
            m_plan_record = plan_position->second;
        }
        
        ++(m_plan_record->m_use_count);
    }

    ComplexFloatFFTPlan::~ComplexFloatFFTPlan()
    {
        const MutexLock lock(PlanDB_mutex);
        
        --(m_plan_record->m_use_count);
    }
    
    void
    ComplexFloatFFTPlan::apply(std::complex<float>* data) const
    {
	fftwf_execute_dft(m_plan_record->m_plan,
                          (fftwf_complex*) data, (fftwf_complex*) data);
    }

    //
    // Double-precision real 1-D plan
    //
    RealDoubleFFTPlan::RealDoubleFFTPlan(int n, bool isForward)
	: m_plan_record(0)
    {
        const MutexLock lock(PlanDB_mutex);
        
	const PlanKey key(n, isForward);
 
	typedef RealDoublePlanMap::iterator iterator;
	
        iterator plan_position = RDPlanDB_map.find(key);

        if (plan_position == RDPlanDB_map.end())
        {
            //
            // The plan wasn't there, so we construct one.
            //
	    if (PlanDB_has_space())
	    {
		m_plan_record = new RealDoublePlanRecord(n, isForward);
		RDPlanDB_map[key] = m_plan_record;
	    }
	    else
	    {
	 throw std::runtime_error("RealDoubleFFTPlan::RealDoubleFFTPlan() "
				  "too many FFT plans in use");
	    }
        }
        else
        {
            // The plan was found in the database, so we can get it from 
            // the iterator (recall that dereferencing the iterator
            // plan_position gives us an STL pair<Key, Value>)
            m_plan_record = plan_position->second;
        }
        
        ++(m_plan_record->m_use_count);
    }

    RealDoubleFFTPlan::~RealDoubleFFTPlan()
    {
        const MutexLock lock(PlanDB_mutex);
        
        --(m_plan_record->m_use_count);
    }
    
    void
    RealDoubleFFTPlan::apply(double* data) const
    {
	fftw_execute_r2r(m_plan_record->m_plan, data, data);
    }


    //
    // Double-precision complex 1-D plan
    //
    ComplexDoubleFFTPlan::ComplexDoubleFFTPlan(int n, bool isForward)
	: m_plan_record(0)
    {
        const MutexLock lock(PlanDB_mutex);
        
	const PlanKey key(n, isForward);
 
	typedef ComplexDoublePlanMap::iterator iterator;
	
        iterator plan_position = CDPlanDB_map.find(key);

        if (plan_position == CDPlanDB_map.end())
        {
            //
            // The plan wasn't there, so we construct one.
            //
	    if (PlanDB_has_space())
	    {
		m_plan_record = new ComplexDoublePlanRecord(n, isForward);
		CDPlanDB_map[key] = m_plan_record;
	    }
	    else
	    {
       throw std::runtime_error("ComplexDoubleFFTPlan::ComplexDoubleFFTPlan() "
				"too many FFT plans in use");
	    }
        }
        else
        {
            // The plan was found in the database, so we can get it from 
            // the iterator (recall that dereferencing the iterator
            // plan_position gives us an STL pair<Key, Value>)
            m_plan_record = plan_position->second;
        }
        
        ++(m_plan_record->m_use_count);
    }

    ComplexDoubleFFTPlan::~ComplexDoubleFFTPlan()
    {
        const MutexLock lock(PlanDB_mutex);
        
        --(m_plan_record->m_use_count);
    }
    
    void
    ComplexDoubleFFTPlan::apply(std::complex<double>* data) const
    {
	fftw_execute_dft(m_plan_record->m_plan,
                         (fftw_complex*) data, (fftw_complex*) data);
    }

} // namespace datacondAPI
