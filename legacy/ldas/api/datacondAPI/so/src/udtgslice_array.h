// $Id: udtgslice_array.h,v 1.2 2001/04/01 00:30:58 LSF5 Exp $

#ifndef UDT_GSLICE_ARRAY_HH
#define UDT_GSLICE_ARRAY_HH

namespace datacondAPI {

  //: provide gslice operations on VectorSequence.
  //+ A udt_gslice_array holds a reference to a an std::valarray and a copy
  //+ of a gslice object
  template<typename Tp> class udt_gslice_array
  {
  public:
    typedef Tp value_type;

    //: Return a copy of the data indicated by the gslice
    operator std::valarray<Tp>() const;

    //: Assign to gslice
    //!param: std::valarray<Tp>& - data to assign from
    void operator=  (const valarray<Tp>&) const;

    //: Multiply current gslice element by element
    //!param: const std::valarray<Tp>& - data to multiply by
    void operator*= (const valarray<Tp>&) const;

    //: divide current gslice element by element
    //!param: const std::valarray<Tp>& - data to divide by
    void operator/= (const valarray<Tp>&) const;

    //: Integer divide current gslice element by element
    //!param: const std::valarray<Tp>& - data to divide by. 
    void operator%= (const valarray<Tp>&) const;

    //: Add to current gslice element by element
    //!param: const std::valarray<Tp>& - data to add
    void operator+= (const valarray<Tp>&) const;

    //: Subtract from current gslice element by element
    //!param: const std::valarray<Tp>& - data to subtract
    void operator-= (const valarray<Tp>&) const;

    //: Exclusive-or with current gslice element by element
    //!param: const std::valarray<Tp>& - data to exclusive-or with
    void operator^= (const valarray<Tp>&) const;

    //: And with current gslice element by element
    //!param: const std::valarray<Tp>& - data to and with
    void operator&= (const valarray<Tp>&) const;

    //: Or with current gslice element by element
    //!param: const std::valarray<Tp>& - data to or by
    void operator|= (const valarray<Tp>&) const;

    //: left-shift data in current gslice element by element
    //!param: const std::valarray<Tp>& - amount to shift by
    void operator<<=(const valarray<Tp>&) const;

    //: Right-shift data in current gslice element by element
    //!param: const std::valarray<Tp>& - amount to shift by
    void operator>>=(const valarray<Tp>&) const;

    //: Assign to every element in gslice
    //!param: const Tp - data to assign 
    void operator=(const Tp&);

  protected:
    // udt and descendants have access to these protected methods

    //: VectorSequence<Tp> granted friendship
    friend class VectorSequence<Tp>;

    //: Protected constructor from valarray<Tp> and gslice
    //!param: std::valarray<Tp>& - valarray to construct from
    //!param: std::gslice& - gslice of std::valarray we are interested in
    udt_gslice_array(valarray<Tp>&, const gslice&);

    //: Copy constructor
    //!param: const udt_gslice_array& - object to copy from
    udt_gslice_array(const udt_gslice_array&);

private:

    //: gslice of interest
    const gslice m_gslice;

    //: reference to data of interest
    valarray<Tp>& m_data;
    
    //: Default constrctor not implemented
    udt_gslice_array ();

    //: Assignment operator not implemented
    udt_gslice_array& operator= (const udt_gslice_array&);
};


  template<typename Tp>
  inline udt_gslice_array<Tp>::
  operator std::valarray<Tp>() const 
  { return m_data[m_gslice]; }
  
  template<typename Tp>
  inline udt_gslice_array<Tp>::
  udt_gslice_array (valarray<Tp>& v, const slice& s)
    : m_gslice(s), m_data(v) {}
  
  template<typename Tp>
  inline udt_gslice_array<Tp>::
  udt_gslice_array(const udt_gslice_array<Tp>& usa)
    : m_gslice(usa.m_gslice), m_data(usa.m_data) {}
  
  template<typename Tp>
  inline void
  udt_gslice_array<Tp>::operator= (const Tp& v) 
  { m_data[m_gslice] = v; }
  
  template<typename Tp>
  inline void
  udt_gslice_array<Tp>::operator= (const valarray<Tp>& v)
  { m_data[m_gslice] = v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator*= (const valarray<Tp>& v) const
  { m_data[m_gslice] *= v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator/= (const valarray<Tp>& v) const
  { m_data[m_gslice] /= v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator%= (const valarray<Tp>& v) const
  { m_data[m_gslice] %= v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator+= (const valarray<Tp>& v) const
  { m_data[m_gslice] += v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator-= (const valarray<Tp>& v) const
  { m_data[m_gslice] -= v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator^= (const valarray<Tp>& v) const
  { m_data[m_gslice] ^= v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator&= (const valarray<Tp>& v) const
  { m_data[m_gslice] &= v; }
  
  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator|= (const valarray<Tp>& v) const
  { m_data[m_gslice] |= v; }

  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator<<= (const valarray<Tp>& v) const
  { m_data[m_gslice] <<= v; }

  template<typename Tp>
  inline void		
  udt_gslice_array<Tp>::operator>>= (const valarray<Tp>& v) const
  { m_data[m_gslice] >>= v; }

} // extern "C++"

    
#endif // __GSLICE_ARRAY__

// Local Variables:
// mode:c++
// End:
