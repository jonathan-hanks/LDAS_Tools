/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef FREQUENCY_SEQUENCE_UDT_HH
#define FREQUENCY_SEQUENCE_UDT_HH

// $Id: FrequencySequenceUDT.hh,v 1.17 2006/11/27 21:32:14 emaros Exp $

#include "GeometryMetaData.hh"
#include "FrequencyUMD.hh"
#include "SequenceUDT.hh"

namespace datacondAPI
{

  //: UDT representation of a sequence of data whose elements are tied to 
  //+ frequency
  template<class DataType_>
  class FrequencySequence : public Sequence< DataType_ >,
			    public FrequencyMetaData,
			    public GeometryMetaData
  {
    
  public:
    // constructors

    //: Default constructor
    FrequencySequence();
    
    //: Construct allowing specification of size, frequencies
    //!param: size - number of elements in series
    //!param: f0 = 0.0 - frequency of first element in series
    //!param: df = 0.0 - increment in frequency between consequtive elements
    FrequencySequence(unsigned int size, 
		      const double& f0 = 0.0, 
		      const double& df = 0.0);

    //: Construct allowing specification of data, frequencies
    //!param: std::valarray<DataType_> - elements of series
    //!param: f0 = 0.0 - frequency of first element in series
    //!param: df = 0.0 - increment in frequency between consequtive elements
    FrequencySequence(const std::valarray<DataType_>& value, 
		      const double& f0 = 0.0, 
		      const double& df = 0.0);

    //: Construct allowing specification of data, frequencies
    //!param: Sequence<DataType_> - elements of series
    //!param: f0 = 0.0 - frequency of first element in series
    //!param: df = 0.0 - increment in frequency between consequtive elements
    FrequencySequence(const Sequence<DataType_>& value, 
		      const double& f0 = 0.0, 
		      const double& df = 0.0);

    //: Copy constructor
    //!param: value - object to copy from
    FrequencySequence(const FrequencySequence<DataType_>& value);
    
    //: Constructor from an array of data
    //!param: Data - pointer to an array of type ArrayData_, of length size, 
    //+ that can be implicity converted to type DataType_
    //!param: size - length of FrequencySeries to create from Data
    //!param: f0 = 0 - Frequency associated with first element of 
    //+ FrequencySeries
    //!param: df = 0 - Increment between successive values of elements of 
    //+ series. 
    template<class ArrayData_>
    FrequencySequence(const ArrayData_* Data, 
		      unsigned int size, 
		      const double& f0 = 0.0, 
		      const double& df = 0.0);
    
    //: destructor
    virtual ~FrequencySequence();
    
    // UDT required members
    
    //: duplicate this on heap
    //!return: FrequencySequence<DataType_>* - duplicate of *this, allocated
    //+ on heap
    virtual FrequencySequence< DataType_ >* Clone() const;
        
    //: Make ILWD representation of *this
    //!param: Chain - CallChain *this belongs to
    //!param: Type - intended destination of ILWD representation of *this
    //!return: ILwd::LdasElement* - ILWD representation of this
    virtual ILwd::LdasContainer* 
    ConvertToIlwd( const CallChain& Chain, 
		   datacondAPI::udt::target_type Type 
		   = datacondAPI::udt::TARGET_GENERIC ) const;
    
  }; // class FrequencySequence<DataType_>
  
} // namespace datacondAPI

#endif // FREQUENCY_SEQUENCE_UDT_HH
