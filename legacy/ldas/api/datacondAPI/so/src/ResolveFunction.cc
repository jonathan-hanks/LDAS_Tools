#include "datacondAPI/config.h"

#include "ResolveFunction.hh"
#include "SequenceUDT.hh"
#include "UDT.hh"
#include "resolver.hh"
#include "result.hh"
#include "WaveletPSU.hh"
#include "HaarPSU.hh"

namespace datacondAPI
{

const std::string& ResolveFunction::GetName(void) const
{
  static std::string name("Resolve");

  return name;
}

ResolveFunction::ResolveFunction() : Function( ResolveFunction::GetName() )
{}

static ResolveFunction _ResolveFunction;

void ResolveFunction::Eval(CallChain* Chain, const CallChain::Params& Params,
			   const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;
  using namespace datacondAPI;

  switch(Params.size())
    {
    case 3:
      {
	//Resolve(wavelet, levels, Sequence_of_data)

	udt *inW( Chain->GetSymbol(Params[0]));
	if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	  {
	    Resolver::Resolver r(*inW, *Chain->GetSymbol(Params[1]));

	    if(udt::IsA<Sequence<double> >( *Chain->GetSymbol(Params[2])))
	      {
		Sequence<double> in(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[2])));
		Result<double> out;
		r.applyFilter(out, in);
		returnedSymbol = new Result<double> (out);
		
	      }
	    else if(udt::IsA<Sequence<float> >( *Chain->GetSymbol(Params[2])))
	      {
		Sequence<float> in(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[2])));
		Result<float> out;
		r.applyFilter(out, in);
		returnedSymbol = new Result<float> (out);
		
	      }
	  }
	

	
        break;
      }//end case 3

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: ResolveFunction(wavelet, levels, sequence_of_data)");
	break;
      }//end default

   }//end switch

  if(!returnedSymbol)
  { 
    throw CallChain::NullResult( GetName() );
  }

  Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval

}//end namespace datacondAPI
