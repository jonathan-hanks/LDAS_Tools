#include "datacondAPI/config.h"

#include <stdio.h>

#include "DivisionFunction.hh"
#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

//=============================================================================
// Static Helper functions
//=============================================================================

static datacondAPI::udt* eval(const datacondAPI::udt& Unknown1,
			      const datacondAPI::udt& Unknwon2,
			      bool Reverse);
static datacondAPI::udt* eval2(const double Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const float Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const int Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const std::valarray<float>& Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);
static datacondAPI::udt* eval2(const std::valarray<double>& Known,
			       const datacondAPI::udt& Unknown,
			       bool Reverse);

template<class Out, class In1, class In2>
inline datacondAPI::Scalar<Out>* divide(const In1 Value1,
					const In2 Value2,
					bool Reverse)
{
  return new datacondAPI::Scalar<Out>
    ((Reverse ? (Value2 / Value1) : (Value1 / Value2)));
}

template<class Out, class In1, class In2>
datacondAPI::Sequence<Out>* divide_sequence(const In1 Value1,
					    const In2 Value2,
					    bool Reverse)
{
  datacondAPI::Sequence<Out>* ret;
  if (Reverse)
  {
#if !old && 0
    ret = (datacondAPI::Sequence<Out>*)NULL;
#else	/* !old */
    ret = new datacondAPI::Sequence<Out>(Value2 / Value1);
    // (*ret) = Value1 / (*ret);
    // (*ret) *= Value1;
#endif	/* !old */
  }
  else
  {
    ret = new datacondAPI::Sequence<Out>(Value1);
    (*ret) /= Value2;
  }
  return ret;
}

//=============================================================================
// Division Function
//=============================================================================

DivisionFunction::
DivisionFunction()
: Function( DivisionFunction::GetName() )
{
}

void DivisionFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
const
{
  if (2 != Params.size())
  {
    throw CallChain::BadArgumentCount( GetName(), 2, Params.size() );
  }

  datacondAPI::udt*	left(Chain->GetSymbol(Params[0]));
  datacondAPI::udt*	right(Chain->GetSymbol(Params[1]));
  datacondAPI::udt*	answer;

  if (!(answer = eval(*left, *right, false)) &&
      !(answer = eval(*right, *left, true)))
  {
    std::string msg("unable to divide type ");
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*left));
    msg += " by type ";
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*right));
    throw std::runtime_error(msg);
  }

  Chain->ResetOrAddSymbol( Ret, answer );
}

const std::string& DivisionFunction::
GetName(void) const
{
  static std::string name("div");
  return name;
}


static DivisionFunction  _DivisionFunction;

//=============================================================================
// Static Functions
//=============================================================================

static datacondAPI::udt*
eval(const datacondAPI::udt& Unknown1, const datacondAPI::udt& Unknown2,
     bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<int> >(Unknown1), Unknown2,
		 Reverse);
  }
  else if (udt::IsA< Scalar<float> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<float> >(Unknown1), Unknown2,
		 Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<double> >(Unknown1), Unknown2,
		 Reverse);
  }
  else if (udt::IsA< Sequence<float> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<float> >(Unknown1), Unknown2,
		 Reverse);
  }
  else if (udt::IsA< Sequence<double> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<double> >(Unknown1), Unknown2,
		 Reverse);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const double Known, const datacondAPI::udt& Unknown,
     bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    return divide<double>(Known, udt::Cast< Scalar<int> >(Unknown),
			  Reverse);
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    return divide<float>(Known, udt::Cast< Scalar<float> >(Unknown),
			 Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    return divide<double>(Known, udt::Cast< Scalar<double> >(Unknown),
			  Reverse);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const float Known, const datacondAPI::udt& Unknown,
     bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // float = float / int
    return divide<float>(Known, udt::Cast< Scalar<int> >(Unknown),
			 Reverse);
  }	
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // float = float / float
    return divide<float>(Known, udt::Cast< Scalar<float> >(Unknown),
			 Reverse);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const int Known, const datacondAPI::udt& Unknown,
     bool Reverse)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    return divide<int>(Known, udt::Cast< Scalar<int> >(Unknown),
		       Reverse);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const std::valarray<float>& Known, const datacondAPI::udt& Unknown,
      bool Reverse)
{
  using namespace datacondAPI;

  datacondAPI::Sequence<float>* ret((datacondAPI::Sequence<float>*)NULL);
  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<float> += int
    ret = divide_sequence<float>(Known,
				 (float)udt::Cast< Scalar<int> >(Unknown),
				 Reverse);
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // sequence<float> += float
    ret = divide_sequence<float>(Known,
				 (float)udt::Cast< Scalar<float> >(Unknown),
				 Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<float> += double
    ret = divide_sequence<float>(Known,
				 (float)udt::Cast< Scalar<double> >(Unknown),
				 Reverse);
  }
  else if (udt::IsA< Sequence<float> >(Unknown))
  {
    // sequence<float> += sequence<float>
    ret = divide_sequence<float>(Known,
				 udt::Cast< Sequence<float> >(Unknown),
				 Reverse);
  }
  return ret;
}

static datacondAPI::udt*
eval2(const std::valarray<double>& Known, const datacondAPI::udt& Unknown,
      bool Reverse)
{
  using namespace datacondAPI;

  datacondAPI::Sequence<double>* ret((datacondAPI::Sequence<double>*)NULL);
  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<double> += int
    ret = divide_sequence<double>(Known,
				  (double)(udt::Cast< Scalar<int> >(Unknown)),
				  Reverse);
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<double> += double
    ret = divide_sequence<double>(Known,
				  (double)(udt::Cast< Scalar<double> >(Unknown)),
				  Reverse);
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // sequence<double> += float
    ret = divide_sequence<double>(Known,
				 (double)(udt::Cast< Scalar<float> >(Unknown)),
				  Reverse);
  }
  else if (udt::IsA< Sequence<double> >(Unknown))
  {
    // sequence<double> += sequence<double>
    ret = divide_sequence<double>(Known,
				  udt::Cast< Sequence<double> >(Unknown),
				  Reverse);
  }
  return ret;
}
