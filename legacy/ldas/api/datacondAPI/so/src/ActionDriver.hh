#ifndef DATACONDAPI__ACTION_DRIVER_HH
#define DATACONDAPI__ACTION_DRIVER_HH

#include <list>

namespace datacondAPI
{
  class ActionDriver;
  class ActionLexer;
  class AliasDriver;
}

#include "ActionLexer.hh"
#include "ActionParser.hh"
#include "CallChain.hh"

namespace datacondAPI
{
  class ActionDriver
  {
  public:
    // typedef AliasDriver::alias_type alias_type
    typedef ActionParser::token_type token_type;
    
    ActionDriver( const AliasDriver& Aliases, CallChain& Execution);
    ~ActionDriver( );
    std::string Messages( ) const;
    bool Parse( std::istream& ActionStream );
    token_type Lex( datacondAPI::ActionParser::semantic_type* YYLval,
		    datacondAPI::ActionParser::location_type* YYLloc );


  private:
    friend class ActionParser;

    struct var_type {
      enum data_type {
	BAD,
	REAL,
	INTEGER,
	UNIVERSAL
      };

      inline var_type( data_type Type, const std::string& TextValue )
	: s_symbol_name( TextValue ),
	  s_type( Type ),
	  s_text_value( TextValue )
      {
      }

      inline const std::string& TextValue( ) const
      {
	return s_text_value;
      }

      inline data_type Type( ) const
      {
	return s_type;
      }

      inline const std::string& GetSymbolName( ) const
      {
	return s_symbol_name;
      }

      inline void SetSymbolName( const std::string& SymbolName )
      {
	s_symbol_name = SymbolName;
      }
    private:
      std::string	s_symbol_name;
      data_type		s_type;
      const std::string	s_text_value;
    };

    typedef std::list< var_type > vars_type;

#if 0
    location_type	m_yylloc;
    semantic_type	m_yylval;
#endif /* 0 */

    const AliasDriver& m_aliases;

    CallChain&	m_call_chain;
    //: Function name
    std::string	m_func_name;
    //: Count of temporary variables
    int		m_tmp_count;
    //: Name of output variable to assign
    std::string	m_output_name;
    //: List of variables to pass to the fuction
    vars_type	m_vars;

    bool	m_ok;

    std::unique_ptr< ActionLexer >     m_scanner;

    std::ostringstream	m_messages;

    void add_action( );

    void add_function( );

    void add_intermediate( );

    void add_parameter( var_type::data_type Type, const std::string& TextValue );

    std::ostream& error( );

    void reset( );
  };
} /* namespace - datacondAPI */

#endif /* DATACONDAPI__ACTION_DRIVER_HH */
