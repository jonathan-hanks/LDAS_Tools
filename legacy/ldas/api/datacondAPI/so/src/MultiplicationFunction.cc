#include "datacondAPI/config.h"

#include <stdio.h>

#include "MultiplicationFunction.hh"
#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

//=============================================================================
// Static Helper functions
//=============================================================================
static datacondAPI::udt* eval(const datacondAPI::udt& Unknown1,
			      const datacondAPI::udt& Unknwon2);
static datacondAPI::udt* eval2(const double Known,
			      const datacondAPI::udt& Unknown);
static datacondAPI::udt* eval2(const float Known,
			      const datacondAPI::udt& Unknown);
static datacondAPI::udt* eval2(const int Known,
			      const datacondAPI::udt& Unknown);
static datacondAPI::udt* eval2(const std::valarray<float>& Known,
			       const datacondAPI::udt& Unknown);
static datacondAPI::udt* eval2(const std::valarray<double>& Known,
			       const datacondAPI::udt& Unknown);

template<class Out, class In1, class In2>
inline datacondAPI::Scalar<Out>* multiply(const In1 Value1,
					  const In2 Value2)
{
  return new datacondAPI::Scalar<Out>(Value1 * Value2);
}

template<class Out, class In1, class In2>
datacondAPI::Sequence<Out>* multiply_sequence(const In1 Value1,
					      const In2 Value2)
{
  datacondAPI::Sequence<Out>* ret(new datacondAPI::Sequence<Out>(Value1));
  (*ret) *= Value2;
  return ret;
}

//=============================================================================
// Multiplication Function
//=============================================================================

MultiplicationFunction::
MultiplicationFunction()
: Function( MultiplicationFunction::GetName() )
{
}

void MultiplicationFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
const
{
  if (2 != Params.size())
  {
    throw CallChain::BadArgumentCount( GetName(), 2, Params.size() );
  }

  datacondAPI::udt*	left(Chain->GetSymbol(Params[0]));
  datacondAPI::udt*	right(Chain->GetSymbol(Params[1]));
  datacondAPI::udt*	answer;

  if (!(answer = eval(*left, *right)) &&
      !(answer = eval(*right, *left)))
  {
    std::string msg("unable to multiply type ");
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*left));
    msg += " by type ";
    msg += datacondAPI::TypeInfoTable.GetName(typeid(*right));
    throw std::runtime_error(msg);
  }

  Chain->ResetOrAddSymbol( Ret, answer );
}

const std::string& MultiplicationFunction::
GetName(void) const
{
  static std::string name("mul");
  return name;
}


static MultiplicationFunction  _MultiplicationFunction;

//=============================================================================
// Static Helper functions
//=============================================================================

static datacondAPI::udt*
eval(const datacondAPI::udt& Unknown1, const datacondAPI::udt& Unknown2)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<int> >(Unknown1), Unknown2);
  }
  else if (udt::IsA< Scalar<float> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<float> >(Unknown1), Unknown2);
  }
  else if (udt::IsA< Scalar<double> >(Unknown1))
  {
    return eval2(udt::Cast< Scalar<double> >(Unknown1), Unknown2);
  }
  else if (udt::IsA< Sequence<float> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<float> >(Unknown1), Unknown2);
  }
  else if (udt::IsA< Sequence<double> >(Unknown1))
  {
    return eval2(udt::Cast< Sequence<double> >(Unknown1), Unknown2);
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const double Known, const datacondAPI::udt& Unknown)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // double = double + int
    return multiply<double>(Known, udt::Cast< Scalar<int> >(Unknown));
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // float = double + float
    return multiply<float>(Known, udt::Cast< Scalar<float> >(Unknown));
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // double = double + double
    return multiply<double>(Known, udt::Cast< Scalar<double> >(Unknown));
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const float Known, const datacondAPI::udt& Unknown)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // float = float - int
    return multiply<float>(Known, udt::Cast< Scalar<int> >(Unknown));
  }	
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // float = float - float
    return multiply<float>(Known, udt::Cast< Scalar<float> >(Unknown));
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const int Known, const datacondAPI::udt& Unknown)
{
  using namespace datacondAPI;

  if (udt::IsA< Scalar<int> >(Unknown))
  {
    return multiply<int>(Known, udt::Cast< Scalar<int> >(Unknown));
  }
  return (datacondAPI::udt*)NULL;
}

static datacondAPI::udt*
eval2(const std::valarray<float>& Known, const datacondAPI::udt& Unknown)
{
  using namespace datacondAPI;

  datacondAPI::Sequence<float>* ret((datacondAPI::Sequence<float>*)NULL);
  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<float> += int
    ret = multiply_sequence<float>(Known,
				   udt::Cast< Scalar<int> >(Unknown));
  }
  else if (udt::IsA< Scalar<float> >(Unknown))
  {
    // sequence<float> += float
    ret = multiply_sequence<float>(Known,
				   udt::Cast< Scalar<float> >(Unknown));
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<float> += double
    ret = multiply_sequence<float>(Known,
				   udt::Cast< Scalar<double> >(Unknown));
  }
  else if (udt::IsA< Sequence<float> >(Unknown))
  {
    // sequence<float> += sequence<float>
    ret = multiply_sequence<float>(Known,
				   udt::Cast< Sequence<float> >(Unknown));
  }
  return ret;
}

static datacondAPI::udt*
eval2(const std::valarray<double>& Known, const datacondAPI::udt& Unknown)
{
  using namespace datacondAPI;

  datacondAPI::Sequence<double>* ret((datacondAPI::Sequence<double>*)NULL);
  if (udt::IsA< Scalar<int> >(Unknown))
  {
    // sequence<double> += int
    ret = multiply_sequence<double>(Known,
				    udt::Cast< Scalar<int> >(Unknown));
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<double> += double
    ret = multiply_sequence<double>(Known,
				    udt::Cast< Scalar<double> >(Unknown));
  }
  else if (udt::IsA< Scalar<double> >(Unknown))
  {
    // sequence<double> += double
    ret = multiply_sequence<double>(Known,
				    udt::Cast< Scalar<double> >(Unknown));
  }
  else if (udt::IsA< Sequence<double> >(Unknown))
  {
    // sequence<double> += sequence<double>
    ret = multiply_sequence<double>(Known,
				    udt::Cast< Sequence<double> >(Unknown));
  }
  return ret;
}
