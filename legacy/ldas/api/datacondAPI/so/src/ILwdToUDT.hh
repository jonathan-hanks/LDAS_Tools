#ifndef DATACOND_API__ILWD_TO_UDT_HH
#define DATACOND_API__ILWD_TO_UDT_HH

#include <vector>

namespace ILwd
{
  class LdasElement;
  class LdasContainer;
}

namespace datacondAPI
{
  class udt;

  class ILwdToUDT
  {
  public:
    typedef unsigned int size_type;

    ILwdToUDT( const ILwd::LdasElement& Element );
    ~ILwdToUDT( );

    size_type Size( ) const;
    void Release( );
    datacondAPI::udt* operator[]( size_type Index ) const;
  private:
    typedef std::vector< datacondAPI::udt* >	result_set_type;

    bool		m_owns;
    result_set_type	m_results;

    void parse_container( const ILwd::LdasContainer& Container );
    void parse_element( const ILwd::LdasElement& Element );
  };

  inline void ILwdToUDT::
  Release( )
  {
    m_owns = false;
  }

  inline ILwdToUDT::size_type ILwdToUDT::
  Size( ) const
  {
    return m_results.size( );
  }

}

#endif /* DATACOND_API__ILWD_TO_UDT_HH */
