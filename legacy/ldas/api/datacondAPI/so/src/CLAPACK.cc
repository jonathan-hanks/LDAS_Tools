#include "datacondAPI/config.h"

#if HAVE_LIBCLAPACK
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#if SIZEOF_INTEGER == 0
#include <cblas.h>
#include <clapack.h>
#endif /* SIZEOF_INTEGER == 0 */

#include <stdexcept>
#include <vector>
#include <complex>
#include <algorithm>

#include "Matrix.hh"
#include "SequenceUDT.hh"

#include "CLAPACK.hh"
#include "CLAPACK_Atlas.hh"
#include "CLAPACK_Default.hh"

static char* get_libraries(void);

namespace
{
#ifdef ATLAS_ORDER
  enum CBLAS_TRANSPOSE
  trans( char Arg )
  {
    if ( ( Arg == 'T' )
	 || ( Arg == 't' ) )
      {
	return CblasTrans;
      }
    return CblasNoTrans;
  }
#endif /* ATLAS_ORDER */

  template <class T>
  T so_lookup( void* Handle, const char* Func )
  {
    union {
      void*	symbol;
      T		func;
    } p;

    p.symbol = dlsym(Handle, Func);

    if ( p.func == (T)NULL )
    {
      std::string msg(dlerror());
      throw std::runtime_error(msg);
    }
    return p.func;
  }

}

// Create the one instance of the class with handle for 4 libraries
datacondAPI::CLAPACKSoHandle datacondAPI::
TheCLAPACKSoHandle(4);

datacondAPI::CLAPACKSoHandle::
CLAPACKSoHandle(int Instances)
  : SoThreader(get_libraries(), Instances, false)
{
}

//: DGESV computes the solution to a real system of linear equations   
//+    A * X = B,   
//+    where A is an N-by-N matrix and X and B are N-by-NRHS matrices.   
//
//    The LU decomposition with partial pivoting and row interchanges is   
//    used to factor A as   
//       A = P * L * U,   
//    where P is a permutation matrix, L is unit lower triangular, and U is   
//    upper triangular.  The factored form of A is then used to solve the   
//    system of equations A * X = B.   
//
//!param: int N - The number of linear equations, i.e., the order of the   
//+       matrix A.  N >= 0.   
//!param: int NRHS - The number of right hand sides, i.e., the number of
//+       columns of the matrix B.  NRHS >= 0.   
//!param: double*  A - (input/output) REAL array, dimension (LDA,N)   
//+       On entry, the N-by-N coefficient matrix A.   
//+       On exit, the factors L and U from the factorization   
//+       A = P*L*U; the unit diagonal elements of L are not stored.   
//!param: int LDA - (input) INTEGER   
//+       The leading dimension of the array A.  LDA >= max(1,N).   
//!param: int* IPIV    (output) INTEGER array, dimension (N)   
//+       The pivot indices that define the permutation matrix P;   
//+       row i of the matrix was interchanged with row IPIV(i).   
//!param: double* B - (input/output) REAL array, dimension (LDB,NRHS)   
//+       On entry, the N-by-NRHS matrix of right hand side matrix B.   
//+       On exit, if INFO = 0, the N-by-NRHS solution matrix X.   
//!param: int LDB - (input) INTEGER   
//+       The leading dimension of the array B.  LDB >= max(1,N).   
//!param: int* INFO - (output) INTEGER   
//+       = 0:  successful exit   
//+       < 0:  if INFO = -i, the i-th argument had an illegal value   
//+       > 0:  if INFO = i, U(i,i) is exactly zero.  The factorization   
//+             has been completed, but the factor U is exactly   
//+             singular, so the solution could not be computed.   
void datacondAPI::CLAPACKSoHandle::
DGESV( int N, int NRHS, double* A, int LDA, int* IPIV,
       double* B, int LDB,
       int* INFO )
{
  //---------------------------------------------------------------------
  // Handle to shared object. Resource is released when object goes
  //   out of scope.
  //---------------------------------------------------------------------
  handle_manager	handle( this );

  //---------------------------------------------------------------------
  // Obtain a pointer to the function
  //---------------------------------------------------------------------
  func_dgesv_ f = so_lookup<func_dgesv_>( handle(), HAVE_DGESV );
  //---------------------------------------------------------------------
  // Load variables that will be passed to the function
  //---------------------------------------------------------------------
  integer	n = N;
  integer	nrhs = NRHS;
  doublereal	a = *A;
  integer	lda = LDA;
  integer	ipiv = *IPIV;
  doublereal	b = *B;
  integer	ldb = LDB;
  integer	info = *INFO;
  //---------------------------------------------------------------------
  // Exec the function
  //---------------------------------------------------------------------
  GESV_( f, n, nrhs,
	 &a, lda, &ipiv,
	 &b, ldb,
	 info );

  //---------------------------------------------------------------------
  // Store the changed values
  //---------------------------------------------------------------------
  *A = a;
  *IPIV = ipiv;
  *B = b;
  *INFO = info;
}

void datacondAPI::CLAPACKSoHandle::
SV( const Matrix<double>& A,
    Matrix<double>& X,
    const Matrix<double>& B )
{
  // validate arguments

  integer ARG_N = A.getNCols();
  if (ARG_N != static_cast<integer>(A.getNRows()))
  {
    throw std::invalid_argument("CLAPACKSoHandle::SV: A is not square");
  }

  integer ARG_NRHS = B.getNCols();
  if (ARG_N != static_cast<integer>(B.getNRows()))
  {
    throw std::invalid_argument("CLAPACKSoHandle::SV: Matrix size mismatch");
  }

  integer ARG_LDA = ARG_N;
  integer ARG_LDB = ARG_N;
  integer ARG_INFO = 0;

  // vector argument

  std::valarray<integer> ipiv(ARG_N); // exception safe
  integer* ARG_IPIV = &(ipiv[0]);

  // matrix arguments

  Matrix<doublereal> scrAtch(A); // DGESV overwrites input so we pass a copy
  doublereal* ARG_A = &(scrAtch.m_data[0]); // friendship with Matrix

  X = B; // DGESV takes input from output buffer
  doublereal* ARG_B = &(X.m_data[0]); // frindship with Matrix

  {
    // monopolise CLAPACK as briefly as possible
    handle_manager	handle( this );
    // extract function pointer to dgesv_
    func_dgesv_ f = so_lookup<func_dgesv_>( handle(), HAVE_DGESV );

    GESV_( f, ARG_N, ARG_NRHS,
	   ARG_A, ARG_LDA, ARG_IPIV,
	   ARG_B, ARG_LDB,
	   ARG_INFO);
  }


  // have released CLAPACK, examine output

  if(ARG_INFO < 0)
  {
    throw std::invalid_argument("CLAPACKSoHandle::SV: DGESV was passed an invalid argument");
  }
  if(ARG_INFO > 0)
  {
    throw std::invalid_argument("CLAPACKSoHandle::SV: Singular");
  }

  // everything is fine?

}

void datacondAPI::CLAPACKSoHandle::SV(const Matrix<float>& A,
				      Matrix<float>& X,
				      const Matrix<float>& B)
{

    // validate arguments

    integer ARG_N = A.getNCols();
    if (ARG_N != static_cast<int>(A.getNRows()))
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: A is not square");
    }

    integer ARG_NRHS = B.getNCols();
    if (ARG_N != static_cast<int>(B.getNRows()))
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: Matrix size mismatch");
    }

    integer ARG_LDA = ARG_N;
    integer ARG_LDB = ARG_N;
    integer ARG_INFO = 0;

    // vector argument

    std::valarray<integer> ipiv(ARG_N); // exception safe
    integer* ARG_IPIV = &(ipiv[0]);

    // matrix arguments

    Matrix<float> scrAtch(A); // SGESV overwrites input so we pass a copy
    float* ARG_A = &(scrAtch.m_data[0]); // friendship with Matrix

    X = B; // SGESV takes input from output buffer
    float* ARG_B = &(X.m_data[0]); // frindship with Matrix

    // monopolise CLAPACK as briefly as possible

    {
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      // extract function pointer to dgesv_
      func_sgesv_ f = so_lookup<func_sgesv_>( handle( ), HAVE_SGESV );
      GESV_( f, ARG_N, ARG_NRHS,
	     ARG_A, ARG_LDA, ARG_IPIV,
	     ARG_B, ARG_LDB,
	     ARG_INFO );
    }
    // have released CLAPACK, examine output

    if(ARG_INFO < 0)
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: SGESV was passed an invalid argument");
    }
    if(ARG_INFO > 0)
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: Singular");
    }

    // everything is fine?

}





void datacondAPI::CLAPACKSoHandle::SV(const Matrix<std::complex<double> >& A,
				      Matrix<std::complex<double> >& X,
				      const Matrix<std::complex<double> >& B)
{

    // validate arguments

    integer ARG_N = A.getNCols();
    if (ARG_N != static_cast<int>(A.getNRows()))
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: A is not square");
    }

    integer ARG_NRHS = B.getNCols();
    if (ARG_N != static_cast<int>(B.getNRows()))
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: Matrix size mismatch");
    }

    integer ARG_LDA = ARG_N;
    integer ARG_LDB = ARG_N;
    integer ARG_INFO = 0;

    // vector argument

    std::valarray<integer> ipiv(ARG_N); // exception safe
    integer* ARG_IPIV = &(ipiv[0]);

    // matrix arguments

    Matrix<std::complex<double> > scrAtch(A); // DGESV overwrites input so we pass a copy
    std::complex<double>* ARG_A = &(scrAtch.m_data[0]); // friendship with Matrix

    X = B; // DGESV takes input from output buffer
    std::complex<double>* ARG_B = &(X.m_data[0]); // frindship with Matrix

    {
      // monopolise CLAPACK as briefly as possible
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      // extract function pointer to zgesv_
      func_zgesv_ f = so_lookup<func_zgesv_>( handle( ), HAVE_ZGESV );
      GESV_CPLX_( f, ARG_N, ARG_NRHS,
		  ARG_A, ARG_LDA, ARG_IPIV,
		  ARG_B, ARG_LDB,
		  ARG_INFO );

    }

    if(ARG_INFO < 0)
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: ZGESV was passed an invalid argument");
    }
    if(ARG_INFO > 0)
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: Singular");
    }

    // everything is fine?

}




void datacondAPI::CLAPACKSoHandle::SV(const Matrix<std::complex<float> >& A,
				      Matrix<std::complex<float> >& X,
				      const Matrix<std::complex<float> >& B)
{

    // validate arguments

    integer ARG_N = A.getNCols();
    if (ARG_N != static_cast<int>(A.getNRows()))
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: A is not square");
    }

    integer ARG_NRHS = B.getNCols();
    if (ARG_N != static_cast<int>(B.getNRows()))
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: Matrix size mismatch");
    }

    integer ARG_LDA = ARG_N;
    integer ARG_LDB = ARG_N;
    integer ARG_INFO = 0;

    // vector argument

    std::valarray<integer> ipiv(ARG_N); // exception safe
    integer* ARG_IPIV = &(ipiv[0]);

    // matrix arguments

    Matrix<std::complex<float> > scrAtch(A); // DGESV overwrites input so we pass a copy
    std::complex<float>* ARG_A = &(scrAtch.m_data[0]); // friendship with Matrix

    X = B; // CGESV takes input from output buffer
    std::complex<float>* ARG_B = &(X.m_data[0]); // frindship with Matrix

    {
      // monopolise CLAPACK as briefly as possible
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      // extract function pointer to cgesv_
      func_cgesv_ f = so_lookup<func_cgesv_>( handle( ), HAVE_CGESV );

      GESV_CPLX_( f, ARG_N, ARG_NRHS,
		  ARG_A, ARG_LDA, ARG_IPIV,
		  ARG_B, ARG_LDB, ARG_INFO );
    }

    if(ARG_INFO < 0)
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: CGESV was passed an invalid argument");
    }
    if(ARG_INFO > 0)
    {
	throw std::invalid_argument("CLAPACKSoHandle::SV: Singular");
    }

    // everything is fine?

}

void datacondAPI::CLAPACKSoHandle::MV(Sequence<float>& y,
		     const Matrix<float>& A,
		     const Sequence<float>& x)
{
  if(x.size() != A.getNCols())
  {
    throw std::invalid_argument("CLAPACKSoHandle::MV: Size mismatch");
  }
  y.resize(A.getNRows(), float());

  //---------------------------------------------------------------------
  // Some variables
  //---------------------------------------------------------------------
  char ARG_TRANS = 'n';
  integer ARG_M = A.getNRows();
  integer ARG_N = A.getNCols();
  float ARG_ALPHA = 1.0f;
  integer ARG_INCX = 1;
  float ARG_BETA = 0.0f;
  
  //---------------------------------------------------------------------
  // Handle to shared object. Resource is released when object goes
  //   out of scope.
  //---------------------------------------------------------------------
  handle_manager	handle( this );

  //---------------------------------------------------------------------
  // Obtain a pointer to the function
  //---------------------------------------------------------------------
  //---------------------------------------------------------------------
  // Obtain a pointer to the function
  //---------------------------------------------------------------------
  func_cblas_sgemv f
    = so_lookup<func_cblas_sgemv>( handle( ) , HAVE_SGEMV );
  //---------------------------------------------------------------------
  // Exec the function
  //---------------------------------------------------------------------
  GEMV_( f, ARG_TRANS, ARG_M, ARG_N,
	 ARG_ALPHA, &(A.m_data[0]), ARG_M,
	 &(const_cast<Sequence<float>&>(x)[0]), ARG_INCX,
	 ARG_BETA, &y[0], ARG_INCX );
}

void datacondAPI::CLAPACKSoHandle::MV(Sequence<std::complex<float> >& y,
		     const Matrix<std::complex<float> >& A,
		     const Sequence<std::complex<float> >& x)
{

    if(x.size() != A.getNCols())
    {
	throw std::invalid_argument("CLAPACKSoHandle::MV: Size mismatch");
    }
    y.resize(A.getNRows(), float());

    char ARG_TRANS = 'n';

    integer ARG_M = A.getNRows();

    integer ARG_N = A.getNCols();

    std::complex<float> ARG_ALPHA = 1.0f;

    integer ARG_LDA = std::max(integer( 1 ), ARG_M);

    integer ARG_INCX = 1;

    std::complex<float> ARG_BETA = 0.0f;

    integer ARG_INCY = 1;

    {
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      func_cblas_cgemv f = so_lookup<func_cblas_cgemv>( handle( ), HAVE_CGEMV );

      GEMV_CPLX_( f, ARG_TRANS, ARG_M, ARG_N,
		  &ARG_ALPHA, &(A.m_data[0]), ARG_LDA,
		  &(const_cast<Sequence<scomplex>&>(x)[0]), ARG_INCX,
		  &ARG_BETA, &y[0], ARG_INCY);
    }
}

void datacondAPI::CLAPACKSoHandle::MV(Sequence<double>& y,
		     const Matrix<double>& A,
		     const Sequence<double>& x)
{

    if( x.size() != A.getNCols() )
    {
	throw std::invalid_argument("CLAPACKSoHandle::MV: Size mismatch");
    }
    y.resize(A.getNRows(), float());

    char ARG_TRANS = 'n';

    integer ARG_M = A.getNRows();

    integer ARG_N = A.getNCols();

    double ARG_ALPHA = 1.0;

    integer ARG_INCX = 1;

    double ARG_BETA = 0.0;

    {
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      // extract function pointer to dgesv_
      func_cblas_dgemv f = so_lookup<func_cblas_dgemv>( handle( ), HAVE_DGEMV );
    
      GEMV_( f, ARG_TRANS, ARG_M, ARG_N,
	     ARG_ALPHA, &(A.m_data[0]), ARG_M,
	     &(const_cast<Sequence<double>&>(x)[0]), ARG_INCX,
	     ARG_BETA, &y[0], ARG_INCX );
    }
}

void datacondAPI::CLAPACKSoHandle::MV(Sequence<std::complex<double> >& y,
		     const Matrix<std::complex<double> >& A,
		     const Sequence<std::complex<double> >& x)
{

    if( x.size() != A.getNCols() )
    {
	    throw std::invalid_argument("CLAPACKSoHandle::MV: Size mismatch");
    }
    y.resize(A.getNRows(), float());

    char ARG_TRANS = 'n';

    integer ARG_M = A.getNRows();

    integer ARG_N = A.getNCols();

    std::complex<double> ARG_ALPHA = 1.0;

    integer ARG_INCX = 1;

    std::complex<double> ARG_BETA = 0.0;

    //---------------------------------------------------------------------
    // Handle to shared object. Resource is released when object goes
    //   out of scope.
    //---------------------------------------------------------------------
    handle_manager	handle( this );
    
    func_cblas_zgemv f = so_lookup<func_cblas_zgemv>( handle( ), HAVE_ZGEMV );

    GEMV_CPLX_( f, ARG_TRANS, ARG_M, ARG_N,
		&ARG_ALPHA, &(A.m_data[0]), ARG_M,
		&(const_cast<Sequence<dcomplex>&>(x)[0]), ARG_INCX,
		&ARG_BETA, &y[0], ARG_INCX );
}

void datacondAPI::CLAPACKSoHandle::MM(Matrix<float>& C,
				      const Matrix<float>& A,
				      const Matrix<float>& B,
				      const float& alpha,
				      const float& beta,
				      const bool& transposeA,
				      const bool& transposeB)				      
{
    char ARG_TRANSA = transposeA ? 't' : 'n';
    char ARG_TRANSB = transposeB ? 't' : 'n';

    integer ARG_M = transposeA ? A.getNCols() : A.getNRows();
    integer ARG_N = transposeB ? B.getNRows() : B.getNCols();
    integer ARG_K = transposeA ? A.getNRows() : A.getNCols();

    if ((transposeB ? static_cast<int>(B.getNCols()) : static_cast<int>(B.getNRows())) != ARG_K)
    {
        throw std::invalid_argument("CLAPACKSoHandle::MM: A (or A') and B(or B') have columns (rows) != rows (columns)");
    }
   
    if (static_cast<int>(C.getNRows()) != ARG_M || static_cast<int>(C.getNCols()) != ARG_N)
      {
	if (beta == 0)
	  {
	    C.resize(ARG_M, ARG_N);
	  }
	else
	  {
	    throw std::invalid_argument("CLAPACKSoHandle::MM: C is the wrong size AND beta != 0.0");
	  }
      } 
    
    float ARG_ALPHA = alpha;
    float* ARG_A = &A.m_data[0];
    integer ARG_LDA = std::max(integer( 1 ), transposeA ? ARG_K : ARG_M);

    float* ARG_B = &B.m_data[0];
    integer ARG_LDB = std::max(integer( 1 ), transposeB ? ARG_N : ARG_K);

    float ARG_BETA = beta;

    float* ARG_C = &C.m_data[0];
    integer ARG_LDC = std::max(integer( 1 ), ARG_M);
    
    {
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      func_cblas_sgemm f = so_lookup<func_cblas_sgemm>( handle( ), HAVE_SGEMM );
    
      GEMM_( f, ARG_TRANSA, ARG_TRANSB,
	     ARG_M, ARG_N, ARG_K,
	     ARG_ALPHA, ARG_A, ARG_LDA,
	     ARG_B, ARG_LDB,
	     ARG_BETA, ARG_C, ARG_LDC );
    }
}

void datacondAPI::CLAPACKSoHandle::MM(Matrix<double>& C,
				      const Matrix<double>& A,
				      const Matrix<double>& B,
				      const double& alpha,
				      const double& beta,
				      const bool& transposeA,
				      const bool& transposeB)
{
    char ARG_TRANSA = transposeA ? 't' : 'n';
    char ARG_TRANSB = transposeB ? 't' : 'n';

    integer ARG_M = transposeA ? A.getNCols() : A.getNRows();
    integer ARG_N = transposeB ? B.getNRows() : B.getNCols();
    integer ARG_K = transposeA ? A.getNRows() : A.getNCols();

    if ((transposeB ? static_cast<int>(B.getNCols()) : static_cast<int>(B.getNRows())) != ARG_K)
    {
        throw std::invalid_argument("CLAPACKSoHandle::MM: A (or A') and B(or B') have columns (rows) != rows (columns)");
    }

    if (static_cast<int>(C.getNRows()) != ARG_M || static_cast<int>(C.getNCols()) != ARG_N)
      {
	if (beta == 0)
	  {
	    C.resize(ARG_M, ARG_N);
	  }
	else
	  {
	    throw std::invalid_argument("CLAPACKSoHandle::MM: C is the wrong size AND beta != 0.0");
	  }
      }

    double ARG_ALPHA = alpha;
    double* ARG_A = &A.m_data[0];
    integer ARG_LDA = std::max(integer( 1 ), transposeA ? ARG_K : ARG_M);

    double* ARG_B = &B.m_data[0];
    integer ARG_LDB = std::max(integer( 1 ), transposeB ? ARG_N : ARG_K);

    double ARG_BETA = beta;

    double* ARG_C = &C.m_data[0];
    integer ARG_LDC = std::max(integer( 1 ), ARG_M);
    
    {
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      func_cblas_dgemm f = so_lookup<func_cblas_dgemm>( handle( ), HAVE_DGEMM );
    
      GEMM_( f, ARG_TRANSA, ARG_TRANSB, ARG_M, ARG_N, ARG_K,
	     ARG_ALPHA, ARG_A, ARG_LDA,
	     ARG_B, ARG_LDB,
	     ARG_BETA, ARG_C, ARG_LDC );
    }
}

//
//
//

void datacondAPI::CLAPACKSoHandle::MM(Matrix<std::complex<float> >& C,
				      const Matrix<std::complex<float> >& A,
				      const Matrix<std::complex<float> >& B,
				      const std::complex<float> & alpha,
				      const std::complex<float> & beta,
				      const bool& transposeA,
				      const bool& transposeB)				      
{
    char ARG_TRANSA = transposeA ? 't' : 'n';
    char ARG_TRANSB = transposeB ? 't' : 'n';

    integer ARG_M = transposeA ? A.getNCols() : A.getNRows();
    integer ARG_N = transposeB ? B.getNRows() : B.getNCols();
    integer ARG_K = transposeA ? A.getNRows() : A.getNCols();

    if ((transposeB ? static_cast<int>(B.getNCols()) : static_cast<int>(B.getNRows())) != ARG_K)
    {
        throw std::invalid_argument("CLAPACKSoHandle::MM: A (or A') and B(or B') have columns (rows) != rows (columns)");
    }
   
    if (static_cast<int>(C.getNRows()) != ARG_M || static_cast<int>(C.getNCols()) != ARG_N)
      {
        static const std::complex<float> zero( 0.0, 0.0 );
   
	if (beta == zero)
	  {
	    C.resize(ARG_M, ARG_N);
	  }
	else
	  {
	    throw std::invalid_argument("CLAPACKSoHandle::MM: C is the wrong size AND beta != 0.0");
	  }
      } 
    
    std::complex<float>  ARG_ALPHA = alpha;
    std::complex<float> * ARG_A = &A.m_data[0];
    integer ARG_LDA = std::max(integer( 1 ), transposeA ? ARG_K : ARG_M);

    std::complex<float> * ARG_B = &B.m_data[0];
    integer ARG_LDB = std::max(integer( 1 ), transposeB ? ARG_N : ARG_K);

    std::complex<float>  ARG_BETA = beta;

    std::complex<float> * ARG_C = &C.m_data[0];
    integer ARG_LDC = std::max(integer( 1 ), ARG_M);
    
    {
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      func_cblas_cgemm f = so_lookup<func_cblas_cgemm>( handle( ), HAVE_CGEMM );
    
      GEMM_CPLX_( f, ARG_TRANSA, ARG_TRANSB, ARG_M, ARG_N, ARG_K,
		  &ARG_ALPHA, ARG_A, ARG_LDA,
		  ARG_B, ARG_LDB,
		  &ARG_BETA, ARG_C, ARG_LDC );
    }
}

void datacondAPI::CLAPACKSoHandle::MM(Matrix<std::complex<double> >& C,
				      const Matrix<std::complex<double> >& A,
				      const Matrix<std::complex<double> >& B,
				      const std::complex<double> & alpha,
				      const std::complex<double> & beta,
				      const bool& transposeA,
				      const bool& transposeB)
{
    char ARG_TRANSA = transposeA ? 't' : 'n';
    char ARG_TRANSB = transposeB ? 't' : 'n';

    integer ARG_M = transposeA ? A.getNCols() : A.getNRows();
    integer ARG_N = transposeB ? B.getNRows() : B.getNCols();
    integer ARG_K = transposeA ? A.getNRows() : A.getNCols();

    if ((transposeB ? static_cast<int>(B.getNCols()) : static_cast<int>(B.getNRows())) != ARG_K)
    {
        throw std::invalid_argument("CLAPACKSoHandle::MM: A (or A') and B(or B') have columns (rows) != rows (columns)");
    }

    if (static_cast<int>(C.getNRows()) != ARG_M || static_cast<int>(C.getNCols()) != ARG_N)
      {
         static const std::complex<double> zero( 0.0, 0.0 );
	if (beta == zero )
	  {
	    C.resize(ARG_M, ARG_N);
	  }
	else
	  {
	    throw std::invalid_argument("CLAPACKSoHandle::MM: C is the wrong size AND beta != 0.0");
	  }
      }

    std::complex<double>  ARG_ALPHA = alpha;
    std::complex<double> * ARG_A = &A.m_data[0];
    integer ARG_LDA = std::max(integer( 1 ), transposeA ? ARG_K : ARG_M);

    std::complex<double> * ARG_B = &B.m_data[0];
    integer ARG_LDB = std::max(integer( 1 ), transposeB ? ARG_N : ARG_K);

    std::complex<double>  ARG_BETA = beta;

    std::complex<double> * ARG_C = &C.m_data[0];
    integer ARG_LDC = std::max(integer( 1 ), ARG_M);
    
    {
      //-------------------------------------------------------------------
      // Handle to shared object. Resource is released when object goes
      //   out of scope.
      //-------------------------------------------------------------------
      handle_manager	handle( this );

      func_cblas_zgemm f = so_lookup<func_cblas_zgemm>( handle( ), HAVE_ZGEMM );
       
      GEMM_CPLX_(f, ARG_TRANSA, ARG_TRANSB,
		 ARG_M, ARG_N, ARG_K,
		 &ARG_ALPHA, ARG_A, ARG_LDA,
		 ARG_B, ARG_LDB,
		 &ARG_BETA, ARG_C, ARG_LDC );
    }
}


//: SGESV computes the solution to a real system of linear equations   
//+    A * X = B,   
//+    where A is an N-by-N matrix and X and B are N-by-NRHS matrices.   
//
//    The LU decomposition with partial pivoting and row interchanges is   
//    used to factor A as   
//       A = P * L * U,   
//    where P is a permutation matrix, L is unit lower triangular, and U is   
//    upper triangular.  The factored form of A is then used to solve the   
//    system of equations A * X = B.   
//
//!param: int N - The number of linear equations, i.e., the order of the   
//+       matrix A.  N >= 0.   
//!param: int NRHS - The number of right hand sides, i.e., the number of
//+       columns of the matrix B.  NRHS >= 0.   
//!param: float*  A - (input/output) REAL array, dimension (LDA,N)   
//+       On entry, the N-by-N coefficient matrix A.   
//+       On exit, the factors L and U from the factorization   
//+       A = P*L*U; the unit diagonal elements of L are not stored.   
//!param: int LDA - (input) INTEGER   
//+       The leading dimension of the array A.  LDA >= max(1,N).   
//!param: int* IPIV    (output) INTEGER array, dimension (N)   
//+       The pivot indices that define the permutation matrix P;   
//+       row i of the matrix was interchanged with row IPIV(i).   
//!param: float* B - (input/output) REAL array, dimension (LDB,NRHS)   
//+       On entry, the N-by-NRHS matrix of right hand side matrix B.   
//+       On exit, if INFO = 0, the N-by-NRHS solution matrix X.   
//!param: int LDB - (input) INTEGER   
//+       The leading dimension of the array B.  LDB >= max(1,N).   
//!param: int* INFO - (output) INTEGER   
//+       = 0:  successful exit   
//+       < 0:  if INFO = -i, the i-th argument had an illegal value   
//+       > 0:  if INFO = i, U(i,i) is exactly zero.  The factorization   
//+             has been completed, but the factor U is exactly   
//+             singular, so the solution could not be computed.   
void datacondAPI::CLAPACKSoHandle::
SGESV( int N, int NRHS, float* A, int LDA, int* IPIV,
       float* B, int LDB,
       int* INFO )
{
  //---------------------------------------------------------------------
  // Handle to shared object. Resource is released when object goes
  //   out of scope.
  //---------------------------------------------------------------------
  handle_manager	handle( this );

#if SIZEOF_INT != SIZEOF_INTEGER
  integer INFO_DATA = *INFO;

  integer N_ = N;
  integer NRHS_ = NRHS;
  integer LDA_ = LDA;
  integer LDB_ = LDB;
  integer* INFO_ = &INFO_DATA;
  std::valarray<integer> IPIV_DATA( N );
  integer* IPIV_ = &(IPIV_DATA[0]);

  std::copy( IPIV, &IPIV[ N ], IPIV_ );
#else
#define N_ N
#define NRHS_ NRHS
#define LDA_ LDA
#define LDB_ LDB
#define IPIV_ IPIV
#define INFO_ INFO
#endif
  func_sgesv_ f = so_lookup<func_sgesv_>( handle( ), HAVE_SGESV );
  GESV_( f, N_, NRHS_,
	 A, LDA_, IPIV_,
	 B, LDB_,
	 *INFO_ );
#if SIZEOF_INT != SIZEOF_INTEGER
  std::copy( IPIV_, &IPIV_[ N ], IPIV );
  *INFO = INFO_DATA;
#endif
}

static char*
get_libraries(void)
{
#if defined(CLAPACK_LIB_DIR)
  static char libraries[] = CLAPACK_LIB_DIR "/libclapack.so";
#else
  static char libraries[] = "libclapack%d.so";
#endif /* defined(CLAPACK_LIB_DIR) */
  return libraries;
}

datacondAPI::CLAPACKSoHandle::handle_manager::
handle_manager( datacondAPI::CLAPACKSoHandle* SoHandle )
  : m_so_handle( SoHandle )
{
  m_handle = m_so_handle->getHandle();
}

datacondAPI::CLAPACKSoHandle::handle_manager::
~handle_manager( )
{
  m_so_handle->giveHandle( m_handle );
}
#endif /* HAVE_LIBCLAPACK */
