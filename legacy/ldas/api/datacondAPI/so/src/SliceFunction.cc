/* -*- mode: c++; c-basic-offset: 2; -*- */

#include "config.h"

#include "general/Memory.hh"
#include "general/unordered_map.hh"
   
#include "filters/LDASConstants.hh"
#include "filters/valarray_utils.hh"

#include "TypeInfo.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "DFTUDT.hh"

#include "SliceFunction.hh"

using std::fmod;
using std::complex;


namespace {

  using namespace datacondAPI;
  using std::slice;

  //
  // Attempt to extract an unsigned integer from a CallChain object.
  // An exception will be thrown if the object is not a Scalar<int>
  // or if the value is a negative integer.
  //
  // No attempt is made to convert floating point Scalar<T>'s to int.
  //
  unsigned int getUintArg(const CallChain::Symbol* sym, unsigned int argnum)
  {
    unsigned int val = 0;

    if (const Scalar<int>* const p = dynamic_cast<const Scalar<int>*>(sym))
    {
      const int k = p->GetValue();

      if (k < 0)
      {
	std::ostringstream oss;

	oss << "Illegal Argument: action slice(); argument #"
	    << argnum << " must be a non-negative integer";

	throw std::invalid_argument(oss.str());
      }
      else
      {
	val = k;
      }
    }
    else
    {
      // The object is of an invalid type
      throw CallChain::BadArgument("slice", argnum,
				   TypeInfoTable.GetName(typeid(*sym)),
				   TypeInfoTable.GetName(typeid(Scalar<int>)));
    }
    
    return val;
  }

  //
  // void do_specific_slice(Sequence& out, Sequence& in)
  //     - slice specific classes derived from Sequence
  //
  // The following functions each act on a particular class derived from
  // Sequence eg. Sequence itself, TimeSeries, FrequencySequence and so on.
  // The function for the Sequence base class is the one that does the
  // real slicing on the Sequence component of each derived classes.
  // The do_specific_slice() for the derived classes call the Sequence
  // version of do_specific_slice() and also set meta-data where necessary.
  //
  // :TRICKY: In the following functions non-const reference is used for
  // the input array because slicing a const valarray takes about 4 times
  // longer than slicing a non-const valarray. However, the input array
  // should never be modified. Reasonably safe as these are "internal"
  // functions hidden in the anonymous namespace
  //

  //
  // The function that does the real slicing on Sequences
  //
  template<class T> inline
  void do_specific_slice(Sequence<T>& out, Sequence<T>& in, const slice& s)
  {
    const size_t slice_size = s.size();

    // Check that the index of the last element of the slice doesn't
    // exceed the index of the last element of the input sequence.
    //
    // The last element of the slice has index
    //
    //     s.start() + (slice_size - 1)*s.stride()
    //
    // however to avoid problems with mixing signed and unsigned arithmetic
    // we move the negative term over to the RHS.
    if (s.start() >= in.size())
    {
      throw std::invalid_argument("Illegal Argument: action slice(); "
				  "slice starts beyond end of sequence");
    }
    else if ( (s.start() + slice_size*s.stride()) >= (in.size() + s.stride()) )
    {
      throw std::invalid_argument("Illegal Argument: action slice(); "
				  "slice extends beyond end of sequence");
    }

    // Assign the components of the Sequence class, from the bottom up
    
    // UDT part is identical except for the name and history.
    // Assignment is used because we don't know what things may
    // be added to UDT in the future. May need to be removed if
    // we don't want to duplicate history
    out.udt::operator=(in);

    // Set the name
    std::ostringstream oss;
    oss << "slice(" << in.name() << ","
	<< s.start() << "," << slice_size << ","
	<< s.stride() << ")";

    out.SetName(oss.str());

    // :TODO: What do we do about the "history" component?
    
    // Now assign the valarray component.
    //
    // :TRICKY: Because 'in' is non-const, in[s] returns a slice_array
    // rather than a Sequence. We have to resize.
    if (out.size() != slice_size)
    {
      out.resize(slice_size);
    }

    Filters::valarray_copy_slice(s, in, out);
  }

  //
  // Slicing on TimeSeries
  //
  template<class T> inline
  void do_specific_slice(TimeSeries<T>& out, TimeSeries<T>& in, const slice& s)
  {
    // Assign the components of the TimeSeries class, from the bottom up

    // Do the Sequence part. We have to static_cast down to Sequence
    // so that the correct version of do_specific_slice() is called
    do_specific_slice(static_cast<Sequence<T>&>(out),
		      static_cast<Sequence<T>&>(in),
		      s);

    // First, just assign the whole meta-data from the LHS to the RHS.
    // We *have* to do this, because we don't know what meta-data will
    // be added in the future
    out.TimeSeriesMetaData::operator=(in);
    
    // Then change only the parts altered by slice
    // Base frequency stays the same, since its in Hz and this is just a
    // cheap way to to resample by decimation

    // :TODO: How do we handle gaps??

    // Set TimeSeriesMetaData part
    out.SetSampleRate(in.GetSampleRate()/s.stride());

    // Phase of the first sample needs to be adjusted.
    // :NOTE: Do this using the sampling rate of the INPUT TimeSeries, and
    // don't forget to convert to Nyquist
    
    // New phase = phase of input time-series + s.start()*pi*fc
    // Calculate frequency shift as fraction of Nyquist
    const double fc = 2.0*in.GetBaseFrequency()/in.GetSampleRate();
    double new_phase = fmod(in.GetPhase() + s.start()*LDAS_PI*fc,
			    LDAS_TWOPI);
    if (new_phase < 0.0)
    {
      new_phase += LDAS_TWOPI;
    }
    out.SetPhase(new_phase);

    // Set WhenMetaData
    out.SetStartTime(in.GetStartTime() + s.start()*in.GetStepSize());
  }

  //
  // Slicing on FrequencySequence
  //
  template<class T> inline
  void do_specific_slice(FrequencySequence<T>& out,
                         FrequencySequence<T>& in,
                         const slice& s)
  {
    // Do the Sequence part. We have to static_cast down to Sequence
    // so that the correct version of do_specific_slice() is called
    do_specific_slice(static_cast<Sequence<T>&>(out),
		      static_cast<Sequence<T>&>(in),
		      s);

    // Assign meta-data
    out.FrequencyMetaData::operator=(in);
    out.GeometryMetaData::operator=(in);
    
    // Then change the parts altered by slice
    out.SetFrequencyBase(in.GetFrequency(0)
                         + s.start()*in.GetFrequencyDelta());

    out.SetFrequencyDelta(s.stride()*in.GetFrequencyDelta());
  }

  //
  // Slicing on TimeBoundedFreqSequence
  //
  template<class T> inline
  void do_specific_slice(TimeBoundedFreqSequence<T>& out,
                         TimeBoundedFreqSequence<T>& in,
                         const slice& s)
  {
    do_specific_slice(static_cast<FrequencySequence<T>&>(out),
		      static_cast<FrequencySequence<T>&>(in),
		      s);

    out.TimeBoundedFreqSequenceMetaData::operator=(in);
  }

  //
  // Slicing on CSDSpectrum
  //
  template<class T> inline
  void do_specific_slice(CSDSpectrum<T>& out,
                         CSDSpectrum<T>& in,
                         const slice& s)
  {
    do_specific_slice(static_cast<TimeBoundedFreqSequence<T>&>(out),
		      static_cast<TimeBoundedFreqSequence<T>&>(in),
		      s);

    out.CSDSpectrumUMD::operator=(in);
  }

  //
  // Slicing on WelchSpectrum
  //
  template<class T> inline
  void do_specific_slice(WelchSpectrum<T>& out,
                         WelchSpectrum<T>& in,
                         const slice& s)
  {
    do_specific_slice(static_cast<CSDSpectrum<T>&>(out),
		      static_cast<CSDSpectrum<T>&>(in),
		      s);

    out.WelchSpectrumUMD::operator=(in);
  }

  //
  // Slicing on WelchCSDSpectrum
  //
  template<class T> inline
  void do_specific_slice(WelchCSDSpectrum<T>& out,
                         WelchCSDSpectrum<T>& in,
                         const slice& s)
  {
    do_specific_slice(static_cast<CSDSpectrum<T>&>(out),
		      static_cast<CSDSpectrum<T>&>(in),
		      s);

    out.WelchCSDSpectrumUMD::operator=(in);
  }

  //
  // Set up the mapping from UDT types to slice functions
  //

  //
  // This is the prototype that all functions contained in the map
  // must adhere to. Note that the function uses UDT arguments because
  // all the functions contained in the map must have the same signature,
  // so a templated function wouldn't work.
  //
  typedef udt* (*SliceFn)(udt& in, const slice& s);

  //
  // These are the functions that will be inserted into the map.
  // The template parameter must be a class derived from Sequence
  //
  template<class SeqBased_type>
  udt* do_slice(udt& udt_in, const slice& s)
  {
    // Convert the UDT to its real type, which we know because of
    // the way it was inserted into the map
    SeqBased_type& in = dynamic_cast<SeqBased_type&>(udt_in);

    // Create the object to hold the result, using an unique_ptr
    // to prevent memory leaks
    std::unique_ptr<SeqBased_type> out_ptr(new SeqBased_type());

    //
    // The correct version of do_specific_slice() will be called here
    // because we know that 'in' is of type SeqBased_type
    //
    do_specific_slice(*out_ptr, in, s);

    //
    // Release the unique_ptr and return the result
    //
    return out_ptr.release();
  }

  //
  // A SliceFnMap is a unordered_map that uses a type_info* as the key and
  // contains SliceFn's as its elements
  //
  typedef General::unordered_map< const std::type_info*, SliceFn,
				  LookupHash, LookupEq > SliceFnMap;

  //
  // A function that we can use to initialise the unordered_map when the
  // library is loaded.
  //
  const SliceFnMap& initSliceFnMap()
  {
    static SliceFnMap sliceFnMap;

    // Lets be ultra-paranoid
    if (sliceFnMap.size() != 0)
    {
      return sliceFnMap;
    }

    //
    // Now we insert the function pointers into the map by typeid
    //

    // Sequence
    sliceFnMap[&typeid(Sequence<float>)] = do_slice<Sequence<float> >;

    sliceFnMap[&typeid(Sequence<double>)] = do_slice<Sequence<double> >;

    sliceFnMap[&typeid(Sequence<complex<float> >)]
      = do_slice<Sequence<complex<float> > >;

    sliceFnMap[&typeid(Sequence<complex<double> >)]
      = do_slice<Sequence<complex<double> > >;

    // TimeSeries
    sliceFnMap[&typeid(TimeSeries<float>)] = do_slice<TimeSeries<float> >;

    sliceFnMap[&typeid(TimeSeries<double>)] = do_slice<TimeSeries<double> >;

    sliceFnMap[&typeid(TimeSeries<complex<float> >)]
      = do_slice<TimeSeries<complex<float> > >;

    sliceFnMap[&typeid(TimeSeries<complex<double> >)]
      = do_slice<TimeSeries<complex<double> > >;

    // FrequencySequence
    sliceFnMap[&typeid(FrequencySequence<float>)]
      = do_slice<FrequencySequence<float> >;

    sliceFnMap[&typeid(FrequencySequence<double>)]
      = do_slice<FrequencySequence<double> >;

    sliceFnMap[&typeid(FrequencySequence<complex<float> >)]
      = do_slice<FrequencySequence<complex<float> > >;

    sliceFnMap[&typeid(FrequencySequence<complex<double> >)]
      = do_slice<FrequencySequence<complex<double> > >;

    // TimeBoundedFreqSequence
    sliceFnMap[&typeid(TimeBoundedFreqSequence<float>)]
      = do_slice<TimeBoundedFreqSequence<float> >;

    sliceFnMap[&typeid(TimeBoundedFreqSequence<double>)]
      = do_slice<TimeBoundedFreqSequence<double> >;

    sliceFnMap[&typeid(TimeBoundedFreqSequence<complex<float> >)]
      = do_slice<TimeBoundedFreqSequence<complex<float> > >;

    sliceFnMap[&typeid(TimeBoundedFreqSequence<complex<double> >)]
      = do_slice<TimeBoundedFreqSequence<complex<double> > >;

    // CSDSpectrum
    sliceFnMap[&typeid(CSDSpectrum<float>)]
      = do_slice<CSDSpectrum<float> >;

    sliceFnMap[&typeid(CSDSpectrum<double>)]
      = do_slice<CSDSpectrum<double> >;

    sliceFnMap[&typeid(CSDSpectrum<complex<float> >)]
      = do_slice<CSDSpectrum<complex<float> > >;

    sliceFnMap[&typeid(CSDSpectrum<complex<double> >)]
      = do_slice<CSDSpectrum<complex<double> > >;

    // WelchSpectrum (real only)
    sliceFnMap[&typeid(WelchSpectrum<float>)]
      = do_slice<WelchSpectrum<float> >;

    sliceFnMap[&typeid(WelchSpectrum<double>)]
      = do_slice<WelchSpectrum<double> >;

    // WelchCSDSpectrum
    sliceFnMap[&typeid(WelchCSDSpectrum<float>)]
      = do_slice<WelchCSDSpectrum<float> >;

    sliceFnMap[&typeid(WelchCSDSpectrum<double>)]
      = do_slice<WelchCSDSpectrum<double> >;

    sliceFnMap[&typeid(WelchCSDSpectrum<complex<float> >)]
      = do_slice<WelchCSDSpectrum<complex<float> > >;

    sliceFnMap[&typeid(WelchCSDSpectrum<complex<double> >)]
      = do_slice<WelchCSDSpectrum<complex<double> > >;

    // DFT - complex only. Uses Sequence slicing
    sliceFnMap[&typeid(DFT<complex<float> >)]
      = do_slice<Sequence<complex<float> > >;

    sliceFnMap[&typeid(DFT<complex<double> >)]
      = do_slice<Sequence<complex<double> > >;

    return sliceFnMap;
  }

  //
  // Static objects
  //

  //
  // This is the object we use to look up slice functions. This is
  // a const reference to the static unordered_map contained inside the
  // initSliceFnMap() function. It's about as threadsafe as possible
  // because a) it's a reference and so must be initialised or the compiler
  // will complain b) it's initialised when the library is loaded
  // and c) it's const
  //
  const SliceFnMap& theSliceFnMap = initSliceFnMap();

  const SliceFunction theSliceFunction;

#undef INSTANTIATE
#define INSTANTIATE(SeqBased_type) \
  template udt* do_slice< SeqBased_type >(udt& udt_in, const slice& s)

  INSTANTIATE( Sequence<float> );
  INSTANTIATE( Sequence<double> );
  INSTANTIATE( Sequence<complex<float> > );
  INSTANTIATE( Sequence<complex<double> > );

  INSTANTIATE( TimeSeries<float> );
  INSTANTIATE( TimeSeries<double> );
  INSTANTIATE( TimeSeries<complex<float> > );
  INSTANTIATE( TimeSeries<complex<double> > );

  INSTANTIATE( FrequencySequence<float> );
  INSTANTIATE( FrequencySequence<double> );
  INSTANTIATE( FrequencySequence<complex<float> > );
  INSTANTIATE( FrequencySequence<complex<double> > );

  INSTANTIATE( TimeBoundedFreqSequence<float> );
  INSTANTIATE( TimeBoundedFreqSequence<double> );

  INSTANTIATE( TimeBoundedFreqSequence<complex<float> > );
  INSTANTIATE( TimeBoundedFreqSequence<complex<double> > );

  INSTANTIATE( CSDSpectrum<float> );
  INSTANTIATE( CSDSpectrum<double> );
  INSTANTIATE( CSDSpectrum<complex<float> > );
  INSTANTIATE( CSDSpectrum<complex<double> > );

  INSTANTIATE( WelchSpectrum<float> );
  INSTANTIATE( WelchSpectrum<double> );

  INSTANTIATE( WelchCSDSpectrum<float> );
  INSTANTIATE( WelchCSDSpectrum<double> );
  INSTANTIATE( WelchCSDSpectrum<complex<float> > );
  INSTANTIATE( WelchCSDSpectrum<complex<double> > );
} // anonymous namespace


//
// SliceFunction::SliceFunction
//
SliceFunction::SliceFunction()
  : Function( SliceFunction::GetName() )
{ }


//
// SliceFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "slice". The Eval function receives a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//
void SliceFunction::Eval(CallChain* Chain,
			 const CallChain::Params& Params,
			 const std::string& Ret) const
{
  // Slice function has four mandatory arguments. Since it's so simple
  // we won't bother with a switch statement
  if (Params.size() != 4)
  {
    throw CallChain::BadArgumentCount("slice", 4, Params.size());
  }

  // :CHANGE: We used to prevent self-slicing but it should not be
  // a problem anymore
  
  //
  // Extract integer arguments.
  // An exception will be thrown if any of the symbols don't exist or if
  // the parameters are not Scalar<int> or if they are negative
  //
  const size_t start  = getUintArg(Chain->GetSymbol(Params[1]), 2);
  const size_t size   = getUintArg(Chain->GetSymbol(Params[2]), 3);
  const size_t stride = getUintArg(Chain->GetSymbol(Params[3]), 4);
  
  //
  // Obtain the object associated with the first input parameter name
  //
  CallChain::Symbol* const sym = Chain->GetSymbol(Params[0]);

  //
  // Look up the corresponding slice function in the "virtual" function table
  //
  SliceFnMap::const_iterator iter = theSliceFnMap.find(&typeid(*sym));

  if (iter == theSliceFnMap.end())
  {
    // There was no slice function in the table corresponding to an
    // object of this type - throw an exception
    throw CallChain::BadArgument("slice", 1,
				 TypeInfoTable.GetName(typeid(*sym)));
  }

  // Now call the slice function, which is the second element
  // of the pair obtained in the map lookup
  CallChain::Symbol* const res = (*iter).second(*sym,
						slice(start, size, stride));
  
  // We set the symbol name Ret with the calculated result,
  // replacing any previous value of Ret.
  Chain->ResetOrAddSymbol(Ret, res);
}

//
// SliceFunction::GetName
//
const std::string& SliceFunction::GetName() const
{
  static const std::string name("slice");

  return name;
}
