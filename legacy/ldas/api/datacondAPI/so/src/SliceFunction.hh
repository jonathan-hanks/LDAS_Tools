/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef SLICE_FUNCTION_HH
#define	SLICE_FUNCTION_HH

// $Id: SliceFunction.hh,v 1.3 2001/05/25 04:21:54 Philip.Charlton Exp $

#include "CallChain.hh"

//: Class of methods for the slice() action
class SliceFunction: CallChain::Function
{
public:

  //: Default constructor
  SliceFunction();

  //: Evaluates to the slice of the input data
  //!param: CallChain* Chain - A pointer to the CallChain
  //!param: const CallChain::Params& Params - A list of parameter names
  //!param: const std::string& Ret - The name of the return variable
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;

  //: The name of the action (slice for this action)
  //!return: std::string& - the name of the action (slice for this action)
  const std::string& GetName() const;
};

#endif	/* SLICE_FUNCTION_HH */
