/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef RESPFILTFUNCTION_HH
#define RESPFILTFUNCTION_HH

// $Id: RespFiltFunction.hh,v 1.2 2003/01/31 02:14:46 emaros Exp $

#include "CallChain.hh"

//
//: Function support for respfilt action
//
// y = respfilt(x, R, C, alphas, gammas [, direction ]);
//
// x: real TimeSeries
// R: response function as a FrequencySequence<complex<float> >
// C: sensing function as a FrequencySequence<complex<float> >
// alphas: TimeSeries<complex<float> > of measured alpha values
// gammas: TimeSeries<complex<float> > of measured gamma values
// direction: 0 indicates transform strain to ADC counts, 1 indicates
// transform ADC counts to strain.
class RespFiltFunction : public CallChain::Function {

public:

    RespFiltFunction();

    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;

    virtual const std::string& GetName() const;

};

#endif // RESPFILTFUNCTION_HH
