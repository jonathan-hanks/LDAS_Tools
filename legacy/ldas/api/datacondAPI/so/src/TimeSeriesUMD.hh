/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef TIMESERIESUMD_HH
#define TIMESERIESUMD_HH
// $Id: TimeSeriesUMD.hh,v 1.18 2006/02/16 16:54:58 emaros Exp $

#include "general/gpstime.hh"

#include "GapUMD.hh"
#include "GeometryMetaData.hh"
#include "WhenUMD.hh"

namespace datacondAPI 
{
  //: metadata specifying the properties of a TimeSeries
  class TimeSeriesMetaData : public WhenMetaData,
			     public GeometryMetaData,
			     public GapMetaData<General::GPSTime>
  {
    
  public:
    
    // accessors
    
     //: TimeSeries sample rate (Hz)
    //!return: double - sample rate in Hz
    double GetSampleRate() const;
    
    //: Set TimeSeries sample rate (Hz)
    //!param: fs - sample rate in Hz
    void SetSampleRate(const double& fs);
    
    //: TimeSeries step size (sec)
    //!return: double - step size in seconds
    double GetStepSize() const;

    //: TimeSeries base frequency (Hz)
    //!return: double - base frequency in Hz
    double GetBaseFrequency() const;

    //: Set TimeSeries base frequency (Hz) - default is 0.0
    //!param: double - base frequency in Hz
    void SetBaseFrequency(const double& frequency);

    //: TimeSeries phase
    //!return: double - phase in radians
    double GetPhase() const;

    //: Set TimeSeries phase - default is 0.0
    //!param: double - phase in radians
    void SetPhase(const double& phase);
    
    //: destructor
    virtual ~TimeSeriesMetaData();
    
    // output
    
    //: Convert *this to an appropriate ilwd
    //!param: Chain - CallChain owning *this
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasElement* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasElement* 
    ConvertToIlwd( const CallChain& Chain, 
		   datacondAPI::udt::target_type Target 
		   = datacondAPI::udt::TARGET_GENERIC ) const;
    
    //: Assignment from single value
    TimeSeriesMetaData& operator=( const TimeSeriesMetaData& d );

    //: Test to see if two timeseries are compatable
    //
    // This test ensures compatability between two time series
    // for such operations as addition, subtraction, multiplication,
    // and division.
    //
    //!param: const TimeSeriesMetaData& MetaData - Time series for
    //+		comparison.
    //
    //!ret: bool - true if the two time series are compatable, false otherwise
    bool IsCompatable( const TimeSeriesMetaData& RHS ) const;

  protected:
    
    // constructors
    
    //: Construct with sample rate
    //!param: fs - sample rate in Hz
    TimeSeriesMetaData(const double& fs,
		       const General::GPSTime& StartTime = General::GPSTime(),
		       const double& frequency = 0.0,
		       const double& phase = 0.0);
    
    //: copy constructor
    //!param: data - object to construct copy of
    TimeSeriesMetaData(const TimeSeriesMetaData& data);
    
  private:
    
    // Data members
    
    //: Sampling rate in Hz
    double m_fs;
    
    //: For a TimeSeries heterodyned using the Mixer class, base frequency
    //+ in Hz
    double m_frequency;

    //: For a TimeSeries heterodyned using the Mixer class, phase in radians
    double m_phase;
   
  }; // class TimeSeriesMetaData

} // namespace datacondAPI

#endif // TIMESERIESUMD_HH
