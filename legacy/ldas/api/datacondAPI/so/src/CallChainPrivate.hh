#ifndef CALL_CHAIN_PRIVATE_HH
#define	CALL_CHAIN_PRIVATE_HH

#include <vector>

#include "general/unordered_map.hh"
#include "CallChain.hh"
#include "GeometryMetaData.hh"
#include "DetectorDatabase.hh"

class CallChain::CallChainPrivate
  : public datacondAPI::DetectorDatabase
{
public:
  CallChainPrivate();
  ~CallChainPrivate();

  ILwd::LdasHistory* GetHistory( ) const;

  void ResetHistory( );

private:
  mutable ILwd::LdasHistory*	m_current_history;
};

#endif	/* CALL_CHAIN_PRIVATE_HH */
