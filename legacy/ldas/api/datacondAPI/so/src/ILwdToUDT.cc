#include "config.h"

#include <algorithm>
#include <memory>

#include "general/gpstime.hh"
#include "general/Memory.hh"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldaselement.hh"

#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"

#include "DFTUDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TimeBoundedFreqSequenceUDT.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"

#include "ILwdToUDT.hh"

using General::GPSTime;
using ILwd::LdasContainer;
using ILwd::LdasElement;
using namespace datacondAPI;

#if 0
#define AT(a) std::cerr << __FILE__ << " " << __LINE__ << " " << a << std::endl
#else
#define	AT(a)
#endif

namespace
{
  typedef std::vector< udt* > udt_results_type;

  static const char* TAG_HISTORY_STR = "history";
  static const int TAG_HISTORY_POS = 1;

  template<class Type_>
  Type_ get_data( const LdasContainer& Container,
		  const std::string& name )
  {
    const ILwd::LdasArray< Type_ >* e
      ( dynamic_cast<const ILwd::LdasArray< Type_ >*>
	( Container.find( name ) ) );
    if ( e )
    {
      return e->getData()[0];
    }
    else
    {
      throw std::domain_error( "Cannot cast to requested type" );
    }
  }

  //: Return an ilwd cast to the requested type
  //
  // It is very important to note that Type_ is assumed to be
  //   some type of reference
  template<class Type_>
  Type_ get_ilwd( const LdasContainer& Container,
		  const std::string& name )
  {
    const LdasElement* e = Container.find( name );
    if ( e )
    {
      return dynamic_cast< const Type_& >( *e );
    }
    else
    {
      throw std::domain_error( "Cannot cast to requested type" );
    }
  }

  template<class DataType>
  std::unique_ptr< udt >
  udt_int( const ILwd::LdasArray<DataType>& Data )
  {
    
    AT();
    if (Data.getNDim() != 1)
    {
      AT();
      throw std::invalid_argument( "Ingester::add_ilwd_int_symbol: "
				   "Input LdasArray must be 1-dimensional" );
    }

    AT();
    if (Data.getDimension(0) < 1)
    {
      AT();
      throw std::invalid_argument( "Ingester::add_ilwd_int_symbol: "
				   "Input LdasArray must have length >= 1");
    }

    AT( );
    std::unique_ptr< udt > obj;

    AT();
    if (Data.getDimension(0) == 1)
    {
      AT();
      const DataType tmpData = Data.getData()[0];
      const int data = tmpData;

      // Check that the conversion from unsigned int to int worked.
      // Note that the second check must be <= (rather than <)
      if ((tmpData > 0) && (data <= 0))
      {
	throw std::invalid_argument( "Ingester::add_ilwd_int_symbol: "
				     "conversion from unsigned to signed gave changed value");
      }
      
      AT();
      obj.reset( new datacondAPI::Scalar<int>(data) );
    }
    else
    {
      AT();
      obj.reset( new datacondAPI::Sequence<float>( Data.getData(),
						   Data.getDimension( 0 ) ) );
    }
    return obj;
  }

  template<class DataType>
  std::unique_ptr< udt >
  udt_float( const ILwd::LdasArray<DataType>& Data )
  {
    std::string	leader( __FILE__ ": udt_float: " );

    if (Data.getNDim() != 1)
    {
      std::ostringstream msg;

      msg << leader << "Input LdasArray must be 1-dimensional";
      throw std::invalid_argument( msg.str( ) );
    }

    if (Data.getDimension(0) < 1)
    {
      std::ostringstream msg;

      msg << leader << "Input LdasArray must have length >= 1";
      throw std::invalid_argument( msg.str( ) );
    }

    AT( );
    std::unique_ptr< udt > obj;

    if ( Data.getDimension(0) == 1 )
    {
      obj.reset( new datacondAPI::Scalar<DataType>( Data.getData()[0] ) );
    }
    else
    {
      obj.reset( new datacondAPI::Sequence<DataType>
		 ( Data.getData(),
		   Data.getDimension( 0 ) ) );
    }
    return obj;
}

  //: This routine tries to convert an LdasContainer to a frequency sequence
  bool c2frequency_sequence( const LdasContainer& Container,
			     udt_results_type& Results );

  inline const ILwd::LdasContainer*
  find_history( const ILwd::LdasContainer& Container )
  {
    return dynamic_cast< const ILwd::LdasContainer *>
      ( Container.find( TAG_HISTORY_STR, TAG_HISTORY_POS ) );
  }

  //---------------------------------------------------------------------
  // Template instantiation
  //---------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE( DataType ) \
  template \
  std::unique_ptr< udt > \
  udt_int( const ILwd::LdasArray< DataType >& Data )

  INSTANTIATE( INT_2S );
  INSTANTIATE( INT_2U );
  INSTANTIATE( INT_4S );
  INSTANTIATE( INT_4U );

#undef INSTANTIATE
#define INSTANTIATE( DataType ) \
  template \
  std::unique_ptr< udt > \
  udt_float( const ILwd::LdasArray< DataType >& Data )

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
  
} // namespace - anonymous

using ILwd::LdasContainer;

//=======================================================================
// Public
//=======================================================================
datacondAPI::ILwdToUDT::
ILwdToUDT( const ILwd::LdasElement& Element )
  : m_owns( true )
{
  parse_element( Element );
}

datacondAPI::ILwdToUDT::
~ILwdToUDT( )
{
  if ( m_owns )
  {
    General::Purge( m_results );
  }
  m_results.resize( 0 );
}

datacondAPI::udt* ILwdToUDT::
operator[]( size_type Index ) const
{
  if ( Index >= m_results.size( ) )
  {
    std::ostringstream	msg;
    msg << "Index of " << Index
	<< " exceeds the number of results (" << m_results.size( ) << ")";
    throw std::range_error( msg.str( ) );
  }
  return m_results[ Index ];
}

//=======================================================================
// Private
//=======================================================================
void datacondAPI::ILwdToUDT::
parse_container( const ILwd::LdasContainer& Container )
{
  const ILwd::LdasContainer*	pos;

  //---------------------------------------------------------------------
  // History info
  //---------------------------------------------------------------------
  pos = find_history( Container );
  if ( pos != (const ILwd::LdasContainer*)NULL )
  {
    //:TODO: Need put history information where it can be used later
    return;
  }
  //---------------------------------------------------------------------
  // Handle complex structures
  //---------------------------------------------------------------------
  if ( c2frequency_sequence( Container, m_results ) )
  {
    //:TODO: Need to add history information
    return;
  }
  //---------------------------------------------------------------------
  // Parse everything else
  //---------------------------------------------------------------------
  for ( ILwd::LdasContainer::const_iterator
	  cur = Container.begin( ),
	  end = Container.end( );
	cur != end;
	++cur )
  {
    parse_element( *(*cur) );
  }
}

void datacondAPI::ILwdToUDT::
parse_element( const ILwd::LdasElement& Element )
{
  std::unique_ptr< udt >	result;

  switch( Element.getElementId( ) )
  {
  case ILwd::ID_INT_2S:
    AT();
    result = udt_int( dynamic_cast<const ILwd::LdasArray<INT_2S>&>(Element) );
    break;
  case ILwd::ID_INT_2U:
    AT();
    result = udt_int( dynamic_cast<const ILwd::LdasArray<INT_2U>&>(Element) );
    break;
  case ILwd::ID_INT_4S:
    AT();
    result = udt_int( dynamic_cast<const ILwd::LdasArray<INT_4S>&>(Element) );
    break;
  case ILwd::ID_INT_4U:
    AT();
    result = udt_int( dynamic_cast<const ILwd::LdasArray<INT_4U>&>(Element) );
    break;
  case ILwd::ID_REAL_4:
    AT();
    result =
      udt_float( dynamic_cast<const ILwd::LdasArray<REAL_4>&>(Element) );
    break;
  case ILwd::ID_REAL_8:
    AT();
    result =
      udt_float( dynamic_cast<const ILwd::LdasArray<REAL_8>&>(Element) );
    break;
  case ILwd::ID_COMPLEX_8:
    AT();
    result =
      udt_float( dynamic_cast<const ILwd::LdasArray<COMPLEX_8>&>(Element) );
    break;
  case ILwd::ID_COMPLEX_16:
    AT();
    result =
      udt_float( dynamic_cast<const ILwd::LdasArray<COMPLEX_16>&>(Element) );
    break;
  case ILwd::ID_ILWD:
    try
    {
      parse_container( dynamic_cast< const LdasContainer& >( Element ) );
    }	
    catch( ... )
    {
    }
    break;
  case ILwd::ID_CHAR:
  case ILwd::ID_CHAR_U:
  case ILwd::ID_INT_8S:
  case ILwd::ID_INT_8U:
  case ILwd::ID_LSTRING:
  case ILwd::ID_EXTERNAL:
    // Quietly ignore these cases
    break;
  }
  if ( result.get( ) != (udt*)NULL )
  {
    m_results.push_back( result.release( ) );
  }
}

namespace
{
  //: Attempt to create a DFT
  //
  // Note that only complex DFT's may be instantiated, so again
  // we provide a general function that "fails" ie. returns a null
  // pointer, and two specialisations that succeed for single and
  // double precision complex DFT's
  //
  template<class T>
  datacondAPI::DFT<T>*
  createDFT(const T* data, const size_t dataSize)
  {
    return 0;
  }

  //
  //: Attempt to create a TimeBoundedFreqSequence<T>
  //
  // Provided for use by create_frequency_sequence.
  //
  // Returns a pointer to a TimeBoundedFreqSequence<T> allocated on the heap,
  // otherwise 0 if unable to
  //
  template<class T>
  datacondAPI::TimeBoundedFreqSequence<T>*
  createTimeBoundedFreqSequence( const T* const data,
				 const size_t dataSize,
				 const double f0,
				 const double df,
				 const GPSTime& startTime,
				 const GPSTime& endTime )
  {
    std::unique_ptr<TimeBoundedFreqSequence<T> >
      p( new TimeBoundedFreqSequence<T>( ) );

    // Set Sequence data
    p->resize(dataSize);
    std::copy(data, data + dataSize, &(*p)[0]);

    // Set FrequencyMetaData
    p->SetFrequencyBase(f0);
    p->SetFrequencyDelta(df);

    // Set TimeBoundedFreqSequenceMetaData
    p->SetStartTime(startTime);
    p->SetEndTime(endTime);

    return p.release();
  }

  //: Attempt to create a WelchSpectrum of the requested type T
  //
  // Provided for use by create_frequency_sequence.
  //
  // :NOTE: Only real WelchSpectra are allowed, and
  // we need to avoid attempting to instantiate complex WelchSpectra in
  // the functions that create frequency series, otherwise
  // we get a a linker error (ie. symbol not found). They are returned
  // as CSDSpectra because this is the nearest ancestor class which
  // is allowed to be complex.
  //
  // By default, all attempts to create a WelchSpectrum return 0, except
  // for float and double.
  template<class T>
  datacondAPI::CSDSpectrum<T>*
  createWelchSpectrum( const T* const data,
		       const size_t dataSize,
		       const double freqBase,
		       const double f0,
		       const double df,
		       const GPSTime& startTime,
		       const GPSTime& endTime)
  {
    return 0;
  }

  //: Attempt create a WelchCSDSpectrum<T>
  //
  // Provided for use by create_frequency_sequence.
  //
  template<class T>
  datacondAPI::WelchCSDSpectrum<T>*
  createWelchCSDSpectrum( const T* const data,
			  const size_t dataSize,
			  const double f0,
			  const double df,
			  const GPSTime& startTime,
			  const GPSTime& endTime)
  {
    std::unique_ptr<WelchCSDSpectrum<T> > p(new WelchCSDSpectrum<T>());
    
    //:NOTE: no freqBase for this class

    //:NOTE: several other things could be set here, if they were available
    // eg. source channel name, FFT length, overlap, window information.
    // These would need to be provided in the frame as auxiliary parameters.

    // Set Sequence data
    p->resize(dataSize);
    std::copy(data, data + dataSize, &(*p)[0]);

    // Set FrequencyMetaData
    p->SetFrequencyBase(f0);
    p->SetFrequencyDelta(df);

    // Set TimeBoundedFreqSequenceMetaData
    p->SetStartTime(startTime);
    p->SetEndTime(endTime);

    return p.release();
  }

  //: Create a frequency sequence of the indicated type
  //
  // Returns a pointer to a TimeBoundedFreqSequence<T> created on the heap,
  // of a type specified by subType
  //
  //!arg: subType - type of frequency sequence created
  //!arg: elt - an LdasElement containing the data
  //!arg: freqBase - frequency by which original time-series was mixed
  //!arg: startTime - start time of interval that f-series is valid for
  //!arg: endTime - end time of interval that f-series is valid for
  //
  //!return: TimeBoundedFreqSequence<T>* - Pointer to frequency sequence
  //+allocated on the heap, or zero if unsuccessful
  template<class T>
  datacondAPI::Sequence<T>*
  create_frequency_sequence( const INT_2U subType,
			     const ILwd::LdasElement& elt,
			     const REAL_8 freqBase,
			     const GPSTime startTime,
			     const GPSTime endTime)
  {
    AT("Entering create_frequency_sequence");

    const ILwd::LdasArray<T>& arrayData =
      dynamic_cast<const ILwd::LdasArray<T>&>(elt);
    const T* const data = arrayData.getData();
    const size_t dataSize = arrayData.getDimension(0);
    const double f0 = arrayData.getStartX(0);
    const double df = arrayData.getDx(0);

    // All of the returned clasess are based on Sequence
    std::unique_ptr<Sequence<T> > p(0);

    switch(subType)
    {
    case ILwdFCS::FrProcData::UNKNOWN_SUB_TYPE:
    case ILwdFCS::FrProcData::AMPLITUDE_SPECTRAL_DENSITY:
    case ILwdFCS::FrProcData::COHERENCE:
    case ILwdFCS::FrProcData::TRANSFER_FUNCTION:
      p.reset( createTimeBoundedFreqSequence<T>( data,
						 dataSize,
						 f0,
						 df,
						 startTime,
						 endTime ) );
      break;
    case ILwdFCS::FrProcData::DFT:
      p.reset( createDFT<T>( data, dataSize ) );
      break;
    case ILwdFCS::FrProcData::POWER_SPECTRAL_DENSITY:
      //:NOTE: returns 0 if T is a complex type
      p.reset( createWelchSpectrum<T>( data,
				       dataSize,
				       freqBase,
				       f0,
				       df,
				       startTime,
				       endTime ) );
      break;
    case ILwdFCS::FrProcData::CROSS_SPECTRAL_DENSITY:
      p.reset( createWelchCSDSpectrum<T>( data,
					 dataSize,
					 f0,
					 df,
					 startTime,
					 endTime ) );
      break;
    default:
      p.reset(0);
      break;
    }

    AT("Leaving create_frequency_sequence");

    return p.release();
  }

  bool c2frequency_sequence( const LdasContainer& Container,
			     udt_results_type& Results )
  {
    Results.resize( 0 );

    try
    {
      ILwdFCS::FrProcData	fr_proc_data( &Container );

      const ILwdFCS::FrProcData::data_type&
	data( fr_proc_data.RefData( ) );

      //-----------------------------------------------------------------
      // Have all of the pieces. It is now time to create the udt
      //-----------------------------------------------------------------
      std::unique_ptr< udt >	result;

      switch( data[ 0 ]->GetArrayBase( )->getElementId( ) )
      {
      case ILwd::ID_REAL_4:
	result.reset( create_frequency_sequence<REAL_4>
		      ( fr_proc_data.GetSubType( ),
			*(data[ 0 ]->GetArrayBase( )),
			fr_proc_data.GetFShift( ),
			fr_proc_data.GetStartTime( ),
			fr_proc_data.GetStopTime( ) ) );
	break;
      case ILwd::ID_REAL_8:
	result.reset( create_frequency_sequence<REAL_8>
		      ( fr_proc_data.GetSubType( ),
			*(data[ 0 ]->GetArrayBase( )),
			fr_proc_data.GetFShift( ),
			fr_proc_data.GetStartTime( ),
			fr_proc_data.GetStopTime( ) ) );
	break;
      case ILwd::ID_COMPLEX_8:
	result.reset( create_frequency_sequence<COMPLEX_8>
		      ( fr_proc_data.GetSubType( ),
			*(data[ 0 ]->GetArrayBase( )),
			fr_proc_data.GetFShift( ),
			fr_proc_data.GetStartTime( ),
			fr_proc_data.GetStopTime( ) ) );
	break;
      case ILwd::ID_COMPLEX_16:
	result.reset( create_frequency_sequence<COMPLEX_16>
		      ( fr_proc_data.GetSubType( ),
			*(data[ 0 ]->GetArrayBase( )),
			fr_proc_data.GetFShift( ),
			fr_proc_data.GetStartTime( ),
			fr_proc_data.GetStopTime( ) ) );
	break;
      default:
	// For anything else but these types return failure
	break;
      } // switch
    }
    catch( ... )
    {
    }

    return ( Results.size( ) > 0 ) ? true : false;
  } // c2frequency_sequence

  //---------------------------------------------------------------------
  // Template instantiation
  //---------------------------------------------------------------------
#undef INSTANTIATE
#define INSTANTIATE( T ) \
  template \
  datacondAPI::DFT< T >* \
  createDFT< T > (const T* data, const size_t dataSize)

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
#define INSTANTIATE( T ) \
  template \
  datacondAPI::TimeBoundedFreqSequence< T >* \
  createTimeBoundedFreqSequence< T >( const T* const data, \
				 const size_t dataSize, \
				 const double f0, \
				 const double df, \
				 const GPSTime& startTime, \
				 const GPSTime& endTime )

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
#define INSTANTIATE( T ) \
  template \
  datacondAPI::CSDSpectrum< T >* \
  createWelchSpectrum< T > ( const T* const data, \
			     const size_t dataSize,	\
			     const double freqBase,	\
			     const double f0,		\
			     const double df,		\
			     const GPSTime& startTime,	\
			     const GPSTime& endTime)

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
#define INSTANTIATE( T ) \
  template \
  datacondAPI::WelchCSDSpectrum< T >* \
  createWelchCSDSpectrum< T >( const T* const data, \
			       const size_t dataSize, \
			       const double f0,	\
			       const double df,	\
			       const GPSTime& startTime, \
			       const GPSTime& endTime)

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE
#define INSTANTIATE( T ) \
  template \
  datacondAPI::Sequence< T >* \
  create_frequency_sequence< T >( const INT_2U subType, \
				  const ILwd::LdasElement& elt, \
				  const REAL_8 freqBase,	\
				  const GPSTime startTime,	\
				  const GPSTime endTime)

  INSTANTIATE( REAL_4 );
  INSTANTIATE( REAL_8 );
  INSTANTIATE( COMPLEX_8 );
  INSTANTIATE( COMPLEX_16 );

#undef INSTANTIATE

} // namespace - anonymous
