// $Id: getSampleRateFunction.cc,v 1.2 2005/12/01 22:54:58 emaros Exp $
//
// "getSampleRate" action
//
// srate = getSampleRate(ts);
//
// ts: TimeSeries
//
#include "datacondAPI/config.h"

#include "TimeSeriesUMD.hh"
#include "ScalarUDT.hh"

#include "getSampleRateFunction.hh"

using namespace datacondAPI;

namespace {

    getSampleRateFunction static_getSampleRateFunction;
}

const std::string& getSampleRateFunction::GetName() const
{
  static const std::string name("getSampleRate");

  return name;
}

getSampleRateFunction::getSampleRateFunction()
    : Function( getSampleRateFunction::GetName() )
{
}

void getSampleRateFunction::Eval(CallChain* chain,
                                 const CallChain::Params& params,
                                 const std::string& ret) const
{
    double srate = 0;

    if (params.size() == 1)
    {
        const udt* const tsUdt = chain->GetSymbol(params[0]);

        if (const TimeSeriesMetaData* const p
            = dynamic_cast<const TimeSeriesMetaData*>(tsUdt))
        {
            srate = p->GetSampleRate();
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         0,
                                         TypeInfoTable.GetName(typeid(*tsUdt)),
                                         "TimeSeries<T>");
        }
    }
    else
    {
        throw CallChain::BadArgumentCount(GetName(), "1", params.size());
    }
    
    chain->ResetOrAddSymbol( ret, new Scalar<double>(srate) );
}
