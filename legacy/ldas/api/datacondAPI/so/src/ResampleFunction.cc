// $Id: ResampleFunction.cc,v 1.27 2007/01/24 17:51:28 emaros Exp $
//
// "resample" action
//
// [y = ] resample(x, z)
// [y = ] resample(x, p, q[, z])
// [y = ] resample(x, p, q, n[, z])
// [y = ] resample(x, p, q, n, beta[, z])
//
// p: int
//    upsampling ratio
// q: int
//    downsampling ratio
// x: valarray<T> where T is float, double, complex<float>, complex<double>
//    input sequence
// y: valarray<T> where T is float, double, complex<dloat>, complex<double>
//    output sequence
// z: any object or undefined on input
//    optional state, may be undefined on input
//
// "getResampleDelay" action
//
// [delay = ] getResampleDelay(z)
//
// z: ResampleState
//

#include "datacondAPI/config.h"

#include <complex>

#include "ResampleFunction.hh"
#include "Resample.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

//=============================================================================
// ResampleFunction
//=============================================================================

//-----------------------------------------------------------------------------
// ResampleFunction::GetName
//-----------------------------------------------------------------------------

const std::string& ResampleFunction::GetName(void) const
{
  //-------------------------------------------------------------------------
  // The GetName function returns the string "resample", the name of the
  // action as presented to the user. The name is stored as a static variable
  // and returned as a const reference for performance reasons
  //-------------------------------------------------------------------------

  static std::string name("resample");
  
  return name;
}

//-----------------------------------------------------------------------------
// ResampleFunction::ResampleFunction
//-----------------------------------------------------------------------------

ResampleFunction::ResampleFunction()
    : Function(ResampleFunction::GetName())
{
  //-------------------------------------------------------------------------
  // The sole task of the ResampleFunction constructor is to pass the action
  // name, defined by GetName, to the Function constructor.
  //-------------------------------------------------------------------------
}

//-----------------------------------------------------------------------------
// We construct a static, global instance of a ResampleFunction so that the
// constructor is called when the API is loaded. The constructor passes the
// action name on to the Function constructor, thus registering it as a valid
// action.
//-----------------------------------------------------------------------------

namespace {

    ResampleFunction staticResampleFunction;

}

//-----------------------------------------------------------------------------
// ResampleFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "resample". The Eval function recieves a pointer to the calling
// CallChain, and a list of symbol names (Params) and a return value symbol
// name (Ret).
//-----------------------------------------------------------------------------

void ResampleFunction::Eval(CallChain* Chain,
                            const CallChain::Params& Params,
                            const std::string& Ret) const
{
  //-------------------------------------------------------------------------
  // Some variables for later use
  //-------------------------------------------------------------------------
  
  CallChain::Symbol* returnedSymbol = 0;   

  //-------------------------------------------------------------------------
  // We use a switch statement to handle the various number of Params we are
  // given. The resample action has three mandatory arguments and one optional
  // argument, so we handle cases 3 and 4 and throw exceptions for any other
  // number of arguments.
  //-------------------------------------------------------------------------

  using namespace datacondAPI;

  switch (Params.size())
  {

  case 2: // resample(x, z)
    {
      CallChain::Symbol* z = Chain->GetSymbol(Params[1]);
      Resample rs = ResampleState(*z);
      rs.apply(returnedSymbol, *Chain->GetSymbol(Params[0]));
      rs.getState(z);
    }
    break;

  case 3: // resample(x, p, q) or resample(d, x, z)
    {
      if (isASequence(*Chain->GetSymbol(Params[1])))
	{
	  // resample(d, x, z)
	  CallChain::Symbol* z = Chain->GetSymbol(Params[2]);
	  Resample rs(*z);
	  rs.apply(returnedSymbol, *Chain->GetSymbol(Params[1]));
	  rs.getState(z);
	  Chain->AddSymbol(Params[0], new Scalar<double>(rs.getDelay()));
	}
      else
	{
	  // resample(x, p, q) 
	  Resample rs = ResampleState(*Chain->GetSymbol(Params[1]), 
				      *Chain->GetSymbol(Params[2]));
	  rs.apply(returnedSymbol, *Chain->GetSymbol(Params[0]));
	}
    }
    break;

  case 4: // resample(x, p, q, z) or resample(x, p, q, n) 
          //or resample(d, x, p, q)
    {
      CallChain::Symbol* z_or_n = Chain->GetSymbol(Params[3], false);
      if((z_or_n == 0) || udt::IsA<State>(*z_or_n))
	{ 
	  // resample(x, p, q, z)
	  Resample rs = ResampleState(*Chain->GetSymbol(Params[1]), 
				      *Chain->GetSymbol(Params[2]));
	  rs.apply(returnedSymbol, *Chain->GetSymbol(Params[0]));
	  Chain->AddSymbol(Params[3], new ResampleState(rs.getState()));
	}
      else //Params[3] not a state
	if (isASequence(*Chain->GetSymbol(Params[1])))
	  {
	    // resample(d, x, p, q)
	    Resample rs = ResampleState(*Chain->GetSymbol(Params[2]), 
					*Chain->GetSymbol(Params[3]));
	    rs.apply(returnedSymbol, *Chain->GetSymbol(Params[1]));
	    Chain->AddSymbol(Params[0], new Scalar<double>(rs.getDelay()));
	  }
      else 
	{
	  // resample(x, p, q, n)
	  Resample rs = ResampleState(*Chain->GetSymbol(Params[1]), 
				      *Chain->GetSymbol(Params[2]), 
				      *Chain->GetSymbol(Params[3]));
	  rs.apply(returnedSymbol, *Chain->GetSymbol(Params[0]));
	}
    }
    break;

  case 5: // resample(x, p, q, n, z) or resample(x, p, q, n, beta)
    // or resample(d, x, p, q, z) or resample(d, x, p, q, n)
    {
      CallChain::Symbol* z_or_n = Chain->GetSymbol(Params[4], false);
      if((z_or_n == 0) || udt::IsA<State>(*z_or_n))
	{ 
	  // resample(x, p, q, n, z) or resample(d, x, p, q, z)
	  if (isASequence(*Chain->GetSymbol(Params[1])))
	    {
	      //  resample(d, x, p, q, z)
	      Resample rs = ResampleState(*Chain->GetSymbol(Params[2]), 
					  *Chain->GetSymbol(Params[3]));
	      rs.apply(returnedSymbol, *Chain->GetSymbol(Params[1]));
	      Chain->AddSymbol(Params[4], new ResampleState(rs.getState()));
	      Chain->AddSymbol(Params[0], new Scalar<double>(rs.getDelay()));
	    }
	  else
	    {
	      // resample(x, p, q, n, z)
	      Resample rs = ResampleState(*Chain->GetSymbol(Params[1]), 
					  *Chain->GetSymbol(Params[2]), 
					  *Chain->GetSymbol(Params[3]));
	      rs.apply(returnedSymbol, *Chain->GetSymbol(Params[0]));
	      Chain->AddSymbol(Params[4], new ResampleState(rs.getState()));
	    }
	}
      else
	{
	  // resample(x, p, q, n, beta) or resample(d, x, p, q, n)
	  if (isASequence(*Chain->GetSymbol(Params[1])))
	    {
	      // resample(d, x, p, q, n)
	      Resample rs = ResampleState(*Chain->GetSymbol(Params[2]),
					  *Chain->GetSymbol(Params[3]),
					  *Chain->GetSymbol(Params[4]));
	      rs.apply(returnedSymbol, *Chain->GetSymbol(Params[1]));
	      Chain->AddSymbol(Params[0], new Scalar<double>(rs.getDelay()));
	    }
	  else
	    {
	      // resample(x, p, q, n, beta)
	      Resample rs = ResampleState(*Chain->GetSymbol(Params[1]), 
					  *Chain->GetSymbol(Params[2]), 
					  *Chain->GetSymbol(Params[3]), 
					  *Chain->GetSymbol(Params[4]));
	      rs.apply(returnedSymbol, *Chain->GetSymbol(Params[0]));
	    }
	}
    }
    break;

  case 6: // resample(x, p, q, n, beta, z) 
    // or resample(d, x, p, q, n, z) or resample(d, x, p, q, n, beta)
    {
      if (isASequence(*Chain->GetSymbol(Params[0])))
	{
	  // resample(x, p, q, n, beta, z) 
	  Resample rs = ResampleState(*Chain->GetSymbol(Params[1]), 
				      *Chain->GetSymbol(Params[2]), 
				      *Chain->GetSymbol(Params[3]), 
				      *Chain->GetSymbol(Params[4]));
	  rs.apply(returnedSymbol, *Chain->GetSymbol(Params[0]));
	  Chain->AddSymbol(Params[5], new ResampleState(rs.getState()));
	}
      else if (udt::IsA<State>(*Chain->GetSymbol(Params[5])))
	{
	  // resample(d, x, p, q, n, z) 
	  Resample rs = ResampleState(*Chain->GetSymbol(Params[2]), 
				      *Chain->GetSymbol(Params[3]), 
				      *Chain->GetSymbol(Params[4]));
	  rs.apply(returnedSymbol, *Chain->GetSymbol(Params[1]));
	  Chain->AddSymbol(Params[5], new ResampleState(rs.getState()));
	  Chain->AddSymbol(Params[0], new Scalar<double>(rs.getDelay()));
	}
      else
	{
	  // resample(d, x, p, q, n, beta)
	  Resample rs = ResampleState(*Chain->GetSymbol(Params[2]), 
				      *Chain->GetSymbol(Params[3]), 
				      *Chain->GetSymbol(Params[4]),
				      *Chain->GetSymbol(Params[5]));
	  rs.apply(returnedSymbol, *Chain->GetSymbol(Params[1]));
	  Chain->AddSymbol(Params[0], new Scalar<double>(rs.getDelay()));
	}
    }
    break;

  case 7: // resample(d, x, p, q, n, beta, z)
    {
	Resample rs = ResampleState(*Chain->GetSymbol(Params[2]), 
				    *Chain->GetSymbol(Params[3]), 
				    *Chain->GetSymbol(Params[4]), 
				    *Chain->GetSymbol(Params[5]));
	rs.apply(returnedSymbol, *Chain->GetSymbol(Params[1]));
        Chain->AddSymbol(Params[6], new ResampleState(rs.getState()));
	Chain->AddSymbol(Params[0], new Scalar<double>(rs.getDelay()));
    }
    break;

  default:
    
    //-----------------------------------------------------------------
    // Unsupported number of arguments. We throw an exception
    //-----------------------------------------------------------------
    
    throw CallChain::BadArgumentCount( GetName(), "2-7", Params.size() );
    
    //-----------------------------------------------------------------
    // End case default - unreachable code!
    //-----------------------------------------------------------------
    
    break;
  
   }

  //-------------------------------------------------------------------------
  // Store the result
  //-------------------------------------------------------------------------
    
  if (!returnedSymbol)
  {

    //-----------------------------------------------------------------
    // A null pointer was returned by apply. We throw an exception.
    //-----------------------------------------------------------------

    throw CallChain::NullResult( GetName() );
  }
  
  //---------------------------------------------------------------------
  // Set the symbol name Ret with the calculated result,
  // replacing any previous value of Ret.
  //---------------------------------------------------------------------
  
  Chain->ResetOrAddSymbol( Ret, returnedSymbol );
    
}


//-----------------------------------------------------------------------
// This method validates accessability of the parameters
//-----------------------------------------------------------------------
void ResampleFunction::
ValidateParameters( CallChain::Step::sudo_symbol_table_type& SymbolTable,
		    const CallChain::Params& Params ) const
{
  //---------------------------------------------------------------------
  // Validation really needs to happen knowing the datatype. Since the
  //   datatype is not available, make the assumption that if the
  //   variable does not exist, then it is to be created.
  //---------------------------------------------------------------------
  switch( Params.size( ) )
  {
  case 2:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 0 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    break;
  case 3:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    //-------------------------------------------------------------------
    // :TRICKY: Dangerous assumption!!
    //-------------------------------------------------------------------
    validation_assumption( SymbolTable, Params[ 0 ] );
    break;
  case 4:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    //-------------------------------------------------------------------
    // :TRICKY: Dangerous assumption!!
    //-------------------------------------------------------------------
    validation_assumption( SymbolTable, Params[ 0 ] );
    validation_assumption( SymbolTable, Params[ 3 ] );
    break;
  case 5:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 3 ], PARAM_READ );
    //-------------------------------------------------------------------
    // :TRICKY: Dangerous assumption!!
    //-------------------------------------------------------------------
    validation_assumption( SymbolTable, Params[ 0 ] );
    validation_assumption( SymbolTable, Params[ 4 ] );
    break;
  case 6:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 3 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 4 ], PARAM_READ );
    //-------------------------------------------------------------------
    // :TRICKY: Dangerous assumption!!
    //-------------------------------------------------------------------
    validation_assumption( SymbolTable, Params[ 0 ] );
    validation_assumption( SymbolTable, Params[ 5 ] );
    break;
  case 7:
    //-------------------------------------------------------------------
    // Validate input parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 1 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 2 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 3 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 4 ], PARAM_READ );
    validateParameter( SymbolTable, Params[ 5 ], PARAM_READ );
    //-------------------------------------------------------------------
    // Validate output parameters
    //-------------------------------------------------------------------
    validateParameter( SymbolTable, Params[ 0 ], PARAM_WRITE );
    validateParameter( SymbolTable, Params[ 6 ], PARAM_WRITE );
    break;
  default:
    Function::ValidateParameters( SymbolTable, Params );
    break;
  }
}

bool 
ResampleFunction::isASequence(const datacondAPI::udt& arg) const
{
  bool is = datacondAPI::udt::IsA<datacondAPI::Sequence<float> >(arg);
  if (!is) 
    is = datacondAPI::udt::IsA<datacondAPI::Sequence<double> >(arg);
  if (!is) 
    is = datacondAPI::udt::IsA<datacondAPI::Sequence<std::complex<float> > >(arg);
  if (!is) 
    is = datacondAPI::udt::IsA<datacondAPI::Sequence<std::complex<double> > >(arg);
  return is;
}


//=============================================================================
// GetResampleDelayFunction
//=============================================================================

namespace {

    GetResampleDelayFunction staticGetResampleDelayFunction;

}

const std::string& GetResampleDelayFunction::GetName(void) const
{
  static const std::string name("getResampleDelay");
  
  return name;
}

GetResampleDelayFunction::GetResampleDelayFunction()
    : Function(GetResampleDelayFunction::GetName())
{
}

void GetResampleDelayFunction::Eval(CallChain* Chain,
                                    const CallChain::Params& Params,
                                    const std::string& Ret) const
{
  using namespace datacondAPI;

  CallChain::Symbol* result = 0;   

  if (Params.size() != 1)
  {
      throw CallChain::BadArgumentCount(GetName(), "1", Params.size());
  }

  const udt* const stateUDT = Chain->GetSymbol(Params[0]);

  if (const ResampleState* const p
      = dynamic_cast<const ResampleState*>(stateUDT))
  {
      const double delay = p->getDelay();
      result = new Scalar<double>(delay);
  }
  else
  {
      // The object is of an invalid type
      throw CallChain::BadArgument(GetName(), 1,
                                TypeInfoTable.GetName(typeid(*stateUDT)),
                                TypeInfoTable.GetName(typeid(ResampleState)));
  }
  
  Chain->ResetOrAddSymbol(Ret, result);
}


