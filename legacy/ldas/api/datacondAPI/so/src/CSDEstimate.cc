#include "datacondAPI/config.h"

#include <complex>

#include "CSDEstimate.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "Detrend.hh"

datacondAPI::CSDEstimate::
CSDEstimate(const DetrendMethod detrendMethod)
    : m_detrendMethod(detrendMethod)
{
}

datacondAPI::CSDEstimate::
~CSDEstimate()
{
}

void 
datacondAPI::CSDEstimate::
set_detrendMethod(const DetrendMethod detrendMethod)
{
    m_detrendMethod = detrendMethod;
}

void 
datacondAPI::CSDEstimate::
set_detrendMethod(const udt& detrendMethod)
{
    if (udt::IsA<Scalar<int> >(detrendMethod))
    {
	// Static cast is necessary to force an int to be an enum.
	// Sucks of course, but what else to do?
	const int tmp = udt::Cast<Scalar<int> >(detrendMethod).GetValue();
	set_detrendMethod(static_cast<const DetrendMethod>(tmp));
    }
    else
    {
	throw std::invalid_argument("CSDEstimate::set_detrendMethod() "
				    "argument must be Scalar<int>");
    }
}


datacondAPI::CSDEstimate::DetrendMethod
datacondAPI::CSDEstimate::detrendMethod() const
{
    return m_detrendMethod;
}	

template<class T>
void 
datacondAPI::CSDEstimate::
detrend(Sequence<T>& x) const
{
    switch(m_detrendMethod)
    {
    case none:
	// Do nothing
	break;

    case mean:
	{
	    Detrend detrender(Detrend::mean);
	    detrender.apply(x, x);
	}
	break;

    case linear:
	{
	    Detrend detrender(Detrend::linear);
	    detrender.apply(x, x);
	}
	break;

    default:
	throw std::invalid_argument("CSDEstimate::detrend() "
				    "invalid detrend method");
	break;
    }
}

template void 
datacondAPI::CSDEstimate::
detrend(Sequence<float>&) const;

template void 
datacondAPI::CSDEstimate::
detrend(Sequence<double>&) const;

template void 
datacondAPI::CSDEstimate::
detrend(Sequence<std::complex<float> >&) const;

template void 
datacondAPI::CSDEstimate::
detrend(Sequence<std::complex<double> >&) const;

