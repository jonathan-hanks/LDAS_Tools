/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "config.h"

#include <memory>

#include "general/Memory.hh"

#include <ilwd/ldascontainer.hh>

#include "TimeBoundedFreqSequenceUMD.hh"

using namespace datacondAPI;
using General::GPSTime;

TimeBoundedFreqSequenceMetaData::
~TimeBoundedFreqSequenceMetaData()
{
}

ILwd::LdasContainer* TimeBoundedFreqSequenceMetaData::
ConvertToIlwd( const CallChain& Chain,
               udt::target_type Target ) const
{
  std::unique_ptr<ILwd::LdasContainer> container( new ILwd::LdasContainer );

  switch ( Target )
  {
  case udt::TARGET_GENERIC:
    container->push_back( ArbitraryWhenMetaData::
			  ConvertToIlwd( Chain, Target ),
                          ILwd::LdasContainer::NO_ALLOCATE,
			  ILwd::LdasContainer::OWN );
    break;
  default:
    throw udt::
      BadTargetConversion(Target,
			  "TimeBoundedFreqSequenceMetaData");
    break;
  }
  return container.release( );
}

TimeBoundedFreqSequenceMetaData::
TimeBoundedFreqSequenceMetaData( const GPSTime& StartTime,
		    const GPSTime& EndTime )
  : ArbitraryWhenMetaData( StartTime, EndTime )
{
}

TimeBoundedFreqSequenceMetaData::
TimeBoundedFreqSequenceMetaData( const TimeBoundedFreqSequenceMetaData& in )
    : ArbitraryWhenMetaData( in )
{
}

TimeBoundedFreqSequenceMetaData& TimeBoundedFreqSequenceMetaData::
operator=( const TimeBoundedFreqSequenceMetaData& MetaData )
{
  if ( &MetaData != this )
  {
    static_cast<ArbitraryWhenMetaData& >(*this) = MetaData;
  }
  return *this;
}

void TimeBoundedFreqSequenceMetaData::
store( ILwdFCS::FrProcData& Storage ) const
{
  WhenMetaData::Store( Storage );
}

void TimeBoundedFreqSequenceMetaData::
store( ILwd::LdasContainer& Storage,
       udt::target_type Target ) const
{
  WhenMetaData::store( Storage, Target );
}

