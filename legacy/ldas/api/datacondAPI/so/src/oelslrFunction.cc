#include "config.h"

#include "CallChain.hh"
#include "LineRemover.hh"
#include "TimeSeries.hh"
#include "ScalarUDT.hh"

// oelslr - Output Error Least Squares Line Removal
//
// oelslr estimates a model of how an input sequence interacts with an output
// sequence and can can apply that model to a (sliced) input sequence to
// produce a prediction of the output sequence which can then be subtracted
// away
//
// model = oelslr(output, input, frequency, factor, order)
//     model      - output-error model describing contamination
//     output     - sequence contaminated by input
//     input      - sequence contaminating output
//     frequency  - centre of band of interest (nyquist units)
//     factor     - width of band of interest (downsample ratio)
//     order      - fir order of model
//
// model2 = oelslr(output, input, model)
//     model2     - refined output-error model describing contamination
//     output     - next sequence contaminated by input
//     input      - next sequence contaminating output
//     model      - existing output-error model describing contamination
//
// prediction = oelslr(input, model)
//     prediction - (offset) prediction of output contamination
//     input      - sequence contaimating output
//     model      - output-error model describing contamination

#include "datacondAPI/config.h"

namespace datacondAPI
{

    class oelslrFunction : public CallChain::Function
    {

    public:

	oelslrFunction();

	virtual const std::string& GetName() const;

	virtual void Eval(CallChain* Chain,
			  const CallChain::Params& Params,
			  const std::string& Ret) const;

    private:

    };

    static oelslrFunction _oelslrFunction; // registering instantiation

    oelslrFunction::oelslrFunction()
	: CallChain::Function( oelslrFunction::GetName() )
    {
    }

    const std::string& oelslrFunction::GetName() const
    {
	static const std::string name("oelslr");
	return name;
    }

    void oelslrFunction::Eval(CallChain* chain,
	      const CallChain::Params& parameter,
	      const std::string& result) const
    {

	switch(parameter.size())
	{
	case 5: // m = oelslr(y, u, f, r, n)
	    {
		const udt& y = *(chain->GetSymbol(parameter[0]));
		const udt& u = *(chain->GetSymbol(parameter[1]));
		const udt& f = *(chain->GetSymbol(parameter[2]));
		const udt& r = *(chain->GetSymbol(parameter[3]));
		const udt& n = *(chain->GetSymbol(parameter[4]));

                try {
                    LineRemover lr(f, r, n);
                    lr.refine(y, u);
                    chain->ResetOrAddSymbol(result, lr.Clone());
                }
                catch(const std::logic_error& e)
                {
                    if (!dynamic_cast<const Scalar<double>*>(&f))
                    {
                        throw CallChain::BadArgument(GetName(),
                                                     3,
                               TypeInfoTable.GetName(typeid(f)),
                               TypeInfoTable.GetName(typeid(Scalar<double>)));
                    }
                    else if (!dynamic_cast<const Scalar<int>*>(&r))
                    {
                        throw CallChain::BadArgument(GetName(),
                                                     4,
                               TypeInfoTable.GetName(typeid(r)),
                               TypeInfoTable.GetName(typeid(Scalar<int>)));
                    }
                    else if (!dynamic_cast<const Scalar<int>*>(&n))
                    {
                        throw CallChain::BadArgument(GetName(),
                                                     5,
                               TypeInfoTable.GetName(typeid(n)),
                               TypeInfoTable.GetName(typeid(Scalar<int>)));
                    }
                    else
                    {
                        throw;
                    }
                }
                catch(...)
                {
                    // Rethrow any other error
                    throw;
                }

	    }
	    break;
        case 3: // m2 = oelslr(y, u, m)
            {
                const udt& y = *(chain->GetSymbol(parameter[0]));
                const udt& u = *(chain->GetSymbol(parameter[1]));
                const udt& m = *(chain->GetSymbol(parameter[2]));

                LineRemover lr(dynamic_cast<const LineRemover&>(m));
                lr.refine(y, u);

                chain->ResetOrAddSymbol(result, lr.Clone());
            }
            break;
        case 2: // w = oelslr(u, m)
            {
                 const udt& u = *(chain->GetSymbol(parameter[0]));
                 udt& m = *(chain->GetSymbol(parameter[1]));
   
                 LineRemover& lr = dynamic_cast<LineRemover&>(m);
                 
                 udt* w = 0;
                 lr.apply(w, u);

                 chain->ResetOrAddSymbol(result, w);
            }
            break;
	default:
            throw CallChain::BadArgumentCount(GetName(),  "2, 3 or 5", parameter.size());
	}
    }
}
