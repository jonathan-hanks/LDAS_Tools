#include "datacondAPI/config.h"

#include "Matrix.hh"

#define INSTANTIATE(TYPE,KEY) \
UDT_CLASS_INSTANTIATION(Matrix< TYPE >,KEY)
  
INSTANTIATE(float,float)
INSTANTIATE(double,double)
INSTANTIATE(std::complex<float>,cfloat)
INSTANTIATE(std::complex<double>,cdouble)
