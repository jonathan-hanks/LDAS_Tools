#include "datacondAPI/config.h"

#include <memory>

#include "general/Memory.hh"

#include <filters/LDASConstants.hh>

#include "ScalarUDT.hh"
#include "MixerState.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

UDT_CLASS_INSTANTIATION(MixerState,UDT_DEFAULT_KEY)

namespace datacondAPI
{
    MixerState::MixerState(const double& phase, const double& frequency)
    {
        SetPhase(phase);
        SetFrequency(frequency);
    }

    MixerState::MixerState(const udt& phase, const udt& frequency)
    {
        SetPhase(phase);
        SetFrequency(frequency);
    }

    MixerState::MixerState(const MixerState& state)
	: State(state), m_phase(state.m_phase), m_frequency(state.m_frequency)
    {
    }

    MixerState::MixerState(const udt& state)
    {
        if (udt::IsA<MixerState>(state))
	{
	    const MixerState& reference = udt::Cast<MixerState>(state);
            m_phase = reference.m_phase;
            m_frequency = reference.m_frequency;
	}
        else
        {
	    throw std::invalid_argument("MixerState::MixerState(const udt& "
					"state): input must be a MixerState");
	}  
    }

    MixerState::~MixerState()
    {
    }

    MixerState& MixerState::operator=(const MixerState& state)
    {
	if (&state != this)
	{
	    State::operator=(state);
	    m_phase = state.m_phase;
	    m_frequency = state.m_frequency;
	}
        return *this;
    }

    double MixerState::GetPhase() const
    {
        return m_phase;
    }

    double MixerState::GetFrequency() const
    {
        return m_frequency;
    }

    void MixerState::SetPhase(const double& phase)
    {
        if ((phase < 0) || (phase >= LDAS_TWOPI))
        {
            throw std::domain_error("datacondAPI::MixerState::SetPhase: "
				    "Phase outside [0, 2pi)");
        }
        m_phase = phase;
    }

    void MixerState::SetFrequency(const double& frequency)
    {
        if ((frequency < -1) || (frequency > 1))
        {
            throw std::domain_error("datacondAPI::MixerState::SetFrequency: "
				    "|frequency| >  Nyquist frequency");
        }
        m_frequency = frequency;
    }

    void MixerState::SetPhase(const udt& phase)
    {
        if (udt::IsA<Scalar<double> >(phase))
        {
            SetPhase(udt::Cast<Scalar<double> >(phase).GetValue());
        }
        else if (udt::IsA<Scalar<float> >(phase))
        {
            SetPhase(udt::Cast<Scalar<float> >(phase).GetValue());
        }
        else if (udt::IsA<Scalar<int> >(phase))
        {
            SetPhase(udt::Cast<Scalar<int> >(phase).GetValue());
        }
        else
        {
            throw std::invalid_argument("MixerState::SetPhase: invalid input");
        }
    }

    void MixerState::SetFrequency(const udt& frequency)
    {
        if (udt::IsA<Scalar<double> >(frequency))
        {
            SetFrequency(udt::Cast<Scalar<double> >(frequency).GetValue());
        }
        else if (udt::IsA<Scalar<float> >(frequency))
        {
            SetFrequency(udt::Cast<Scalar<float> >(frequency).GetValue());
        }
        else if (udt::IsA<Scalar<int> >(frequency))
        {
            SetFrequency(udt::Cast<Scalar<int> >(frequency).GetValue());
        }
        else
        {
            throw std::invalid_argument("MixerState::SetPhase: invalid input");
        }
    }

    MixerState* MixerState::Clone() const
    {
        return new MixerState(*this);
    }

    ILwd::LdasElement* MixerState::
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type TARGET ) const
    {
        std::unique_ptr<ILwd::LdasContainer> container(new ILwd::LdasContainer);

	container->push_back(new ILwd::LdasArray<double>(m_frequency,
							 "Frequency"),
                             ILwd::LdasContainer::NO_ALLOCATE,
                             ILwd::LdasContainer::OWN);
	container->push_back(new ILwd::LdasArray<double>(m_phase, "Phase"),
                             ILwd::LdasContainer::NO_ALLOCATE,
                             ILwd::LdasContainer::OWN);

        return container.release();
    }

    MixerState::MixerState() : m_phase(0), m_frequency(0)
    {
    }

} // namespace datacondAPI
