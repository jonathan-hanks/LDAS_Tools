#include "datacondAPI/config.h"

#include <stdexcept>

#include <complex>

#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

#include "LinearFilter.hh"

namespace datacondAPI
{

    // AbstractCoefficents

    class LinearFilter::AbstractCoefficients
    {

    public:

        virtual ~AbstractCoefficients();

        virtual AbstractCoefficients* Clone() const = 0;

        signed int GetDelay() const;
        void SetDelay(const signed int&);

        virtual void Promote(std::valarray<float>&) const = 0;
        virtual void Promote(std::valarray<double>&) const = 0;
        virtual void Promote(std::valarray<std::complex<float> >&) const = 0;
        virtual void Promote(std::valarray<std::complex<double> >&) const = 0;

    protected:

        AbstractCoefficients();
        AbstractCoefficients(const AbstractCoefficients&);
        AbstractCoefficients(const signed int&);

    private:

        AbstractCoefficients& operator=(const AbstractCoefficients&);

        signed int delay;

    };

    LinearFilter::AbstractCoefficients::~AbstractCoefficients()
    {

    }

    signed int LinearFilter::AbstractCoefficients::GetDelay() const
    {
        return delay;
    }

    void LinearFilter::AbstractCoefficients::SetDelay(const signed int& a)
    {
        if (delay >= 0)
        {
            delay = a;
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::AbstractCoefficients::SetDelay(const signed int&): negative delay");
        }
    }

    LinearFilter::AbstractCoefficients::AbstractCoefficients()
        : delay(0)
    {
    }

    LinearFilter::AbstractCoefficients::AbstractCoefficients(const signed int& a)
        : delay(a)
    {
        if (a < 0)
        {
                throw std::invalid_argument(
"LinearFilter::AbstractCoefficients::AbstractCoefficients(const signed int&): negative delay");
        }
    }

    LinearFilter::AbstractCoefficients::AbstractCoefficients(const AbstractCoefficients& a)
        : delay(a.delay)
    {
    }

    // ConcreteCoeffcients<type>

    template<typename type> class ConcreteCoefficients : public
        LinearFilter::AbstractCoefficients
    {

    public:

        ConcreteCoefficients(const std::valarray<type>&);
        ConcreteCoefficients(const std::valarray<type>&, const signed int&);

        virtual ~ConcreteCoefficients();

        virtual ConcreteCoefficients* Clone() const;

        virtual void Promote(std::valarray<float>&) const;
        virtual void Promote(std::valarray<double>&) const;
        virtual void Promote(std::valarray<std::complex<float> >&) const;
        virtual void Promote(std::valarray<std::complex<double> >&) const;

    private:

        ConcreteCoefficients();
        ConcreteCoefficients(const ConcreteCoefficients&);
        ConcreteCoefficients& operator=(const ConcreteCoefficients&);

        std::valarray<type> coefficients;

    };

    template<typename type> ConcreteCoefficients<type>::ConcreteCoefficients(const
        std::valarray<type>& a) : coefficients(a)
    {
    }

    template<typename type> ConcreteCoefficients<type>::ConcreteCoefficients(const
        std::valarray<type>& a, const signed int& b)
        : LinearFilter::AbstractCoefficients(b), coefficients(a)
    {
    }

    template<typename type> ConcreteCoefficients<type>::~ConcreteCoefficients()
    {
    }

    template<typename type> ConcreteCoefficients<type>*
        ConcreteCoefficients<type>::Clone() const
    {
        return new ConcreteCoefficients<type>(*this);
    }

    template<typename type> ConcreteCoefficients<type>::ConcreteCoefficients(const
        ConcreteCoefficients<type>& a) : LinearFilter::AbstractCoefficients(a),
        coefficients(a.coefficients)
    {
    }

    // float conversions

    template<> void ConcreteCoefficients<float>::Promote(std::valarray<float>& x) const
    {
        x.resize(coefficients.size());
        x = coefficients;
    }

    template<> void ConcreteCoefficients<float>::Promote(std::valarray<double>& x) const
    {
        x.resize(coefficients.size());
        for (unsigned int i = 0; i < coefficients.size(); ++i)
        {
            x[i] = coefficients[i];
        }
    }

    template<> void ConcreteCoefficients<float>::Promote(std::valarray<std::complex<float> >& x)
        const
    {
        x.resize(coefficients.size());
        for (unsigned int i = 0; i < coefficients.size(); ++i)
        {
            x[i] = coefficients[i];
        }
    }

    template<> void ConcreteCoefficients<float>::Promote(std::valarray<std::complex<double> >& x)
        const
    {
        x.resize(coefficients.size());
        for (unsigned int i = 0; i < coefficients.size(); ++i)
        {
            x[i] = coefficients[i];
        }
    }

    // double conversions

    template<> void ConcreteCoefficients<double>::Promote(std::valarray<float>& x) const
    {
        throw std::logic_error(
"ConcreteCoefficients<double>::Promote(const std::valarray<float>&): Demotion"
            );
    }

    template<> void ConcreteCoefficients<double>::Promote(std::valarray<double>& x) const
    {
        x.resize(coefficients.size());
        x = coefficients;
    }

    template<> void ConcreteCoefficients<double>::Promote(std::valarray<std::complex<float> >& x)
        const
    {
        throw std::logic_error(
"ConcreteCoefficients<double>::Promote(const std::valarray<std::complex<float> >&): Demotion"
            );
    }

    template<> void ConcreteCoefficients<double>::Promote(std::valarray<std::complex<double> >& x)
        const
    {
        x.resize(coefficients.size());
        for (unsigned int i = 0; i < coefficients.size(); ++i)
        {
            x[i] = coefficients[i];
        }
    }

    // std::complex<float> conversions

    template<> void ConcreteCoefficients<std::complex<float> >::Promote(std::valarray<float>& x)
        const
    {
        throw std::logic_error(
"ConcreteCoefficients<std::complex<float> >::Promote(const std::valarray<float>&): Demotion"
            );
    }

    template<> void ConcreteCoefficients<std::complex<float> >::Promote(
        std::valarray<double>& x) const
    {
        throw std::logic_error(
"ConcreteCoefficients<std::complex<float> >::Promote(const std::valarray<double>&): Demotion"
            );
    }

    template<> void ConcreteCoefficients<std::complex<float> >::Promote(
        std::valarray<std::complex<float> >& x) const
    {
        x.resize(coefficients.size());
        x = coefficients;
    }

    template<> void ConcreteCoefficients<std::complex<float> >::Promote(
        std::valarray<std::complex<double> >& x) const
    {
        x.resize(coefficients.size());
        for (unsigned int i = 0; i < coefficients.size(); ++i)
        {
            x[i] = coefficients[i];
        }
    }

    // std::complex<double> conversions

    template<> void ConcreteCoefficients<std::complex<double> >::Promote(
        std::valarray<float>& x) const
    {
        throw std::logic_error(
"ConcreteCoefficients<std::complex<double> >::Promote(const std::valarray<float>&): Demotion"
            );
    }

    template<> void ConcreteCoefficients<std::complex<double> >::Promote(
        std::valarray<double>& x) const
    {
        throw std::logic_error(
"ConcreteCoefficients<std::complex<double> >::Promote(const std::valarray<double>&): Demotion"
            );
    }

    template<> void ConcreteCoefficients<std::complex<double> >::Promote(
        std::valarray<std::complex<float> >& x) const
    {
        throw std::logic_error(
"ConcreteCoefficients<std::complex<double> >::Promote(const std::valarray<std::complex<float> >&): Demotion"
            );
    }

    template<> void ConcreteCoefficients<std::complex<double> >::Promote(
        std::valarray<std::complex<double> >& x) const
    {
        x.resize(coefficients.size());
        x = coefficients;
    }

    // AbstractState

    class LinearFilter::AbstractState
    {

    public:

        virtual ~AbstractState();

        virtual AbstractState* Clone() const = 0;

    protected:

        AbstractState();
        AbstractState(const signed int&);
        AbstractState(const AbstractState&);

    private:

        AbstractState& operator=(const AbstractState&);

        signed int delay;

    };

    LinearFilter::AbstractState::~AbstractState()
    {

    }

    LinearFilter::AbstractState::AbstractState()
    {
    }

    LinearFilter::AbstractState::AbstractState(const AbstractState&)
    {
    }

    // ConcreteState<type>

    template<typename type> class ConcreteState : public
        LinearFilter::AbstractState
    {

        friend class LinearFilter;

    public:

        ConcreteState();
        ConcreteState(const std::valarray<type>&);
        ConcreteState(const std::valarray<type>&, const signed int&);

        virtual ~ConcreteState();

        virtual ConcreteState* Clone() const;

    private:

        ConcreteState(const ConcreteState&);
        ConcreteState& operator=(const ConcreteState&);

        std::valarray<type> state;

    };

    template<typename type> ConcreteState<type>::ConcreteState() : state()
    {
    }

    template<typename type> ConcreteState<type>::ConcreteState(const
        std::valarray<type>& a) : state(a)
    {
    }

    template<typename type> ConcreteState<type>::ConcreteState(const
        std::valarray<type>& a, const signed int& b) : state(a)
    {
    }

    template<typename type> ConcreteState<type>::~ConcreteState()
    {
    }

    template<typename type> ConcreteState<type>*
        ConcreteState<type>::Clone() const
    {
        return new ConcreteState<type>(*this);
    }

    template<typename type> ConcreteState<type>::ConcreteState(const
        ConcreteState<type>& a) : LinearFilter::AbstractState(a),
        state(a.state)
    {
    }

    // LinearFilter

    LinearFilter::LinearFilter() : a_coefficients(0), b_coefficients(0), state(0)
    {
    }

    LinearFilter::LinearFilter(const LinearFilter& z)
      : State( z ),
	a_coefficients(z.a_coefficients ? z.a_coefficients->Clone() : 0),
	b_coefficients(z.b_coefficients ? z.b_coefficients->Clone() : 0),
	state(z.state ? z.state->Clone() : 0)
    {
    }

    LinearFilter::~LinearFilter()
    {
        delete a_coefficients;
        delete b_coefficients;
        delete state;
    }

    LinearFilter& LinearFilter::operator=(const LinearFilter& z)
    {
        if (this != &z)
        {
            delete a_coefficients;
            a_coefficients = z.a_coefficients ? z.a_coefficients->Clone() : 0;
            delete b_coefficients;
            b_coefficients = z.b_coefficients ? z.b_coefficients->Clone() : 0;
            delete state;
            state = z.state ? z.state->Clone() : 0;
        }
        return *this;
    }

    LinearFilter* LinearFilter::Clone() const
    {
        return new LinearFilter(*this);
    }

   ILwd::LdasElement* LinearFilter::ConvertToIlwd(const CallChain& Chain, udt::target_type Target )
       const
    {
        throw std::logic_error("LinearFilter::ConvertToIlwd: unimplemented");
    }

    template<typename type> void LinearFilter::SetCoefficientsA(const std::valarray<type>& a, const
        signed int& d)
    {
        delete a_coefficients;
        a_coefficients = new ConcreteCoefficients<type>(a, d);
    }

    template<typename type> void LinearFilter::SetCoefficientsB(const std::valarray<type>& b, const
        signed int& d)
    {
        delete b_coefficients;
        b_coefficients = new ConcreteCoefficients<type>(b, d);
    }

    // float output

    template<> void LinearFilter::apply(std::valarray<float>& x, const std::valarray<float>& y)
    {
        try
        {
            implementation<float, float, float>(x, y);
        }
        catch(...)
        {
            implementation<float, float, double>(x, y);
        }
    }

    template<> void LinearFilter::apply(std::valarray<float>& x, const std::valarray<double>& y)
    {
        implementation<double, float, double>(x, y);
    }

    template<> void LinearFilter::apply(std::valarray<float>& x, const
        std::valarray<std::complex<float> >& y)
    {
        throw std::logic_error(
"LinearFilter::apply(std::valarray<float>&, const std::valarray<std::complex<float> >&): Demotion"
            );
    }

    template<> void LinearFilter::apply(std::valarray<float>& x, const
        std::valarray<std::complex<double> >& y)
    {
        throw std::logic_error(
"LinearFilter::apply(std::valarray<float>&, const std::valarray<std::complex<float> >&): Demotion"
            );
    }

    // double output

    template<> void LinearFilter::apply(std::valarray<double>& x, const std::valarray<float>& y)
    {
        throw std::logic_error(
"LinearFilter::apply(std::valarray<double>&, const std::valarray<std::complex<float> >&): Promotion"
            );
    }

    template<> void LinearFilter::apply(std::valarray<double>& x, const std::valarray<double>& y)
    {
        if (single())
        {
            throw std::logic_error(
"LinearFilter::apply(std::valarray<std::complex<double> >&, const std::valarray<double>&): Promotion"
                );
        }
        else
        {
            implementation<double, double, double>(x, y);
        }
    }

    template<> void LinearFilter::apply(std::valarray<double>& x, const
        std::valarray<std::complex<float> >& y)
    {
        throw std::logic_error(
"LinearFilter::apply(std::valarray<double>&, const std::valarray<std::complex<float> >&): Demotion"
            );
    }

    template<> void LinearFilter::apply(std::valarray<double>& x, const
        std::valarray<std::complex<double> >& y)
    {
        throw std::logic_error(
"LinearFilter::apply(std::valarray<double>&, const std::valarray<std::complex<float> >&): Demotion"
            );
    }

    // std::complex<float> output

    template<> void LinearFilter::apply(std::valarray<std::complex<float> >& x, const
        std::valarray<float>& y)
    {
        try
        {
            implementation<float, std::complex<float>, std::complex<float> >(x, y);
        }
        catch(...)
        {
            implementation<float, std::complex<float>, std::complex<double> >(x, y);
        }
    }

    template<> void LinearFilter::apply(std::valarray<std::complex<float> >& x, const
        std::valarray<double>& y)
    {
        implementation<double, std::complex<float>, std::complex<double> >(x, y);
    }

    template<> void LinearFilter::apply(std::valarray<std::complex<float> >& x, const
        std::valarray<std::complex<float> >& y)
    {
        try
        {
            implementation<std::complex<float>, std::complex<float>, std::complex<float> >(x, y);
        }
        catch(...)
        {
            implementation<std::complex<float>, std::complex<float>, std::complex<double> >(x, y);
        }
    }

    template<> void LinearFilter::apply(std::valarray<std::complex<float> >& x, const
        std::valarray<std::complex<double> >& y)
    {
        implementation<std::complex<double>, std::complex<float>, std::complex<double> >(x, y);
    }

    // std::complex<double> output

    template<> void LinearFilter::apply(std::valarray<std::complex<double> >& x, const
        std::valarray<float>& y)
    {
        throw std::logic_error(
"LinearFilter::apply(std::valarray<std::complex<double> >&, const std::valarray<float>&): Promotion"
            );
    }

    template<> void LinearFilter::apply(std::valarray<std::complex<double> >& x, const
        std::valarray<double>& y)
    {
        if (single())
        {
            throw std::logic_error(
"LinearFilter::apply(std::valarray<std::complex<double> >&, const std::valarray<double>&): Promotion"
                );
        }
        else
        {
            implementation<double, std::complex<double>, std::complex<double> >(x, y);
        }
    }

    template<> void LinearFilter::apply(std::valarray<std::complex<double> >& x, const
        std::valarray<std::complex<float> >& y)
    {
        throw std::logic_error(
"LinearFilter::apply(std::valarray<std::complex<double> >&, const std::valarray<std::complex<float> >&): Promotion"
            );
    }

    template<> void LinearFilter::apply(std::valarray<std::complex<double> >& x, const
        std::valarray<std::complex<double> >& y)
    {
        if (single())
        {
            throw std::logic_error(
"LinearFilter::apply(std::valarray<std::complex<double> >&, const std::valarray<double>&): Promotion"
                );
        }
        else
        {
            implementation<std::complex<double>, std::complex<double>, std::complex<double> >(x, y);
        }
    }

    // UDT interface

    void LinearFilter::SetCoefficientsA(const udt& x)
    {
        if (udt::IsA<Sequence<float> >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<float> >(x));
        }
        else if (udt::IsA<Sequence<double> >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<double> >(x));
        }
        else if (udt::IsA<Sequence<std::complex<float> > >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<std::complex<float> > >(x));
        }
        else if (udt::IsA<Sequence<std::complex<double> > >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<std::complex<double> > >(x));
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::SetCoefficientsA(const udt&): argument must be an array"
                );
        }
    }

    void LinearFilter::SetCoefficientsA(const udt& x, const udt& y)
    {
        signed int delay = 0;
        if (udt::IsA<Scalar<int> >(y))
        {
            delay = udt::Cast<Scalar<int> >(y).GetValue();
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::SetCoefficientsA(const udt&, const udt&): second argument must be an integer"
                );
        }
        if (udt::IsA<Sequence<float> >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<float> >(x), delay);
        }
        else if (udt::IsA<Sequence<double> >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<double> >(x), delay);
        }
        else if (udt::IsA<Sequence<std::complex<float> > >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<std::complex<float> > >(x), delay);
        }
        else if (udt::IsA<Sequence<std::complex<double> > >(x))
        {
            SetCoefficientsA(udt::Cast<Sequence<std::complex<double> > >(x), delay);
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::SetCoefficientsA(const udt&, const udt&): first argument must be an array"
                );
        }
    }

    void LinearFilter::SetCoefficientsB(const udt& x)
    {
        if (udt::IsA<Sequence<float> >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<float> >(x));
        }
        else if (udt::IsA<Sequence<double> >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<double> >(x));
        }
        else if (udt::IsA<Sequence<std::complex<float> > >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<std::complex<float> > >(x));
        }
        else if (udt::IsA<Sequence<std::complex<double> > >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<std::complex<double> > >(x));
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::SetCoefficientsA(const udt&): argument must be an array"
                );
        }
    }

    void LinearFilter::SetCoefficientsB(const udt& x, const udt& y)
    {
        signed int delay = 0;
        if (udt::IsA<Scalar<int> >(y))
        {
            delay = udt::Cast<Scalar<int> >(y);
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::SetCoefficientsB(const udt&, const udt&): second argument must be an integer"
                );
        }
        if (udt::IsA<Sequence<float> >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<float> >(x), delay);
        }
        else if (udt::IsA<Sequence<double> >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<double> >(x), delay);
        }
        else if (udt::IsA<Sequence<std::complex<float> > >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<std::complex<float> > >(x), delay);
        }
        else if (udt::IsA<Sequence<std::complex<double> > >(x))
        {
            SetCoefficientsB(udt::Cast<Sequence<std::complex<double> > >(x), delay);
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::SetCoefficientsA(const udt&, const udt&): first argument must be an array"
                );
        }
    }

    void LinearFilter::apply(udt*& x, const udt& y)
    {
        if (dynamic_cast<const std::valarray<float>*>(&y))
        {
            imply<float>(x, y);
        }
        else if (dynamic_cast<const std::valarray<double>*>(&y))
        {
            imply<double>(x, y);
        }
        else if (dynamic_cast<const std::valarray<std::complex<float> >*>(&y))
        {
            imply<std::complex<float> >(x, y);
        }
        else if (dynamic_cast<const std::valarray<std::complex<double> >*>(&y))
        {
            imply<std::complex<double> >(x, y);
        }
        else
        {
            throw std::invalid_argument(
"LinearFilter::apply(udt*&, const udt&): arguments must be arrays");
        }
    }

    // private methods

    bool LinearFilter::single() const
    {
        if (dynamic_cast<ConcreteCoefficients<float>*>(b_coefficients))
        {
            return true;
        }
        if (dynamic_cast<ConcreteCoefficients<std::complex<float> >*>(b_coefficients))
        {
            return true;
        }
        if (dynamic_cast<ConcreteCoefficients<float>*>(a_coefficients))
        {
            return true;
        }
        if (dynamic_cast<ConcreteCoefficients<std::complex<float> >*>(a_coefficients))
        {
            return true;
        }
        return false;
    }

    template<typename type> void LinearFilter::imply(udt*& x, const udt& y)
    {
        // y is-a std::valarray<type>
        if (x)
        {
            if (dynamic_cast<const std::valarray<float>*>(x))
            {
                apply(dynamic_cast<std::valarray<float>&>(*x),
                    dynamic_cast<const std::valarray<type>&>(y));
            }
            else if (dynamic_cast<const std::valarray<double>*>(x))
            {
                apply(dynamic_cast<std::valarray<double>&>(*x),
                    dynamic_cast<const std::valarray<type>&>(y));
            }
            else if (dynamic_cast<const std::valarray<std::complex<float> >*>(x))
            {
                apply(dynamic_cast<std::valarray<std::complex<float> >&>(*x),
                    dynamic_cast<const std::valarray<type>&>(y));
            }
            else if (dynamic_cast<const std::valarray<std::complex<double> >*>(x))
            {
                apply(dynamic_cast<std::valarray<std::complex<double> >&>(*x),
                    dynamic_cast<const std::valarray<type>&>(y));
            }
            else
            {
                throw std::invalid_argument(
"LinearFilter::apply(udt*&, const udt&): arguments must be arrays"
                    );
            }
        }
        else
        {
            x = y.Clone();
            apply(dynamic_cast<std::valarray<type>&>(*x),
                dynamic_cast<const std::valarray<type>&>(y));
        }
    }

    // implementation

    template<typename in, typename out, typename internal> void LinearFilter::implementation(
        std::valarray<out>& x, const std::valarray<in>& y)
    {
        x.resize(y.size());

        if (!dynamic_cast<ConcreteState<internal>*>(state))
        {
            // no useable state
            delete state;
            state = new ConcreteState<internal>();
        }
        // std::valarray<internal>& z = dynamic_cast<ConcreteState<internal>&>(*state).state;

        if (a_coefficients)
        {
            std::valarray<internal> a;
            a_coefficients->Promote(a);
            if(b_coefficients)
            {
                std::valarray<internal> b;
                b_coefficients->Promote(b);

                // general filter
            }
            else
            {
                // iir filter
            }
        }
        else
        {
            if (b_coefficients)
            {
                std::valarray<internal> b;
                b_coefficients->Promote(b);

                // fir filter
            }
            else
            {
                // trvial filter
            }
        }
    }

}
