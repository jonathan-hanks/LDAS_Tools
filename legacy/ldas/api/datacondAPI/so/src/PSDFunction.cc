// $Id: PSDFunction.cc,v 1.28 2005/12/01 22:54:58 emaros Exp $
//
// "psd" action
//
// psd has 5 input arguments and one return value. The format is
//
//     psd_out = psd(psd_in, [ fftLength ],
//                   [ window ], [ overlap ], [ detrendMethod ])
//
// psd_out, freqs_out and psd_in are Sequences.
// fftLength, overlap and detrendMethod are integers
// window is a Window
//

#include "datacondAPI/config.h"

#include "PSDFunction.hh"
#include "WelchCSDEstimate.hh"
#include "ScalarUDT.hh"

static PSDFunction _PSDFunction;

PSDFunction::PSDFunction()
    : Function( PSDFunction::GetName() )
{
}

const std::string& PSDFunction::GetName(void) const
{
    static std::string name("psd");
    return name;
}

void PSDFunction::Eval(CallChain* Chain,
                       const CallChain::Params& Params,
                       const std::string& Ret) const
{
    using namespace datacondAPI;

    WelchCSDEstimate we;
    
    const size_t num_params = Params.size();

    switch(num_params)
    {

    case 5:
	// Detrend
	if (Params[4] != "")
	{
	    we.set_detrendMethod(*(Chain->GetSymbol(Params[4])));
	}

    case 4:
	// :TRICKY: Setting overlap is deferred to case 2
	// because it must be set *after* fft length
       
    case 3:
	// Set window
	if (Params[2] != "")
	{
	    we.set_window(*(Chain->GetSymbol(Params[2])));
	}

    case 2:
	// FFT length *must* be set first
	if (Params[1] != "")
	{
	    we.set_fftLength(*(Chain->GetSymbol(Params[1])));
	}
	
	// Set overlap length if present
	if ((num_params >= 4) && (Params[3] != ""))
	{
	    we.set_overlapLength(*(Chain->GetSymbol(Params[3])));
	}

    case 1:
	// Nothing to do here, just use all defaults
        break;

    default:
        throw CallChain::BadArgumentCount("psd", 1, Params.size());
    }

    const udt& psd_in = *(Chain->GetSymbol(Params[0]));
    CallChain::Symbol* psd_out = 0;

    we.apply(psd_out, psd_in);
    
    Chain->ResetOrAddSymbol( Ret, psd_out );
}
