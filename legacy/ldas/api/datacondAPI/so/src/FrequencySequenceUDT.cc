/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "datacondAPI/config.h"

#include <complex>

#include "general/Memory.hh"

#include "FrequencySequenceUDT.hh"
#include "WhenUMD.hh"

#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"

#include "CallChain.hh"

template<class DataType_>
datacondAPI::FrequencySequence<DataType_>::FrequencySequence()
{
}

template<class DataType_>
datacondAPI::FrequencySequence<DataType_>::FrequencySequence(unsigned int size, const double& f0, const double& df)
: Sequence<DataType_>(size), FrequencyMetaData(f0, df)
{
}

template<class DataType_>
datacondAPI::FrequencySequence<DataType_>::FrequencySequence(const std::valarray<DataType_>& value, const double& f0, const double& df)
: Sequence<DataType_>(value),  FrequencyMetaData(f0, df)
{
}

template<class DataType_>
datacondAPI::FrequencySequence<DataType_>::FrequencySequence(const Sequence<DataType_>& value, const double& f0, const double& df)
: Sequence<DataType_>(value),  FrequencyMetaData(f0, df)
{
}

template<class DataType_>
datacondAPI::FrequencySequence<DataType_>::FrequencySequence(const FrequencySequence<DataType_>& value)
    : Sequence<DataType_>(value), FrequencyMetaData(value),
      GeometryMetaData( value )
{
}

template<class DataType_> template<class ArrayData_>
datacondAPI::FrequencySequence<DataType_>::FrequencySequence(const ArrayData_* data, unsigned int size, const double& f0, const double& df)
: Sequence<DataType_>(data, size), FrequencyMetaData(f0, df)
{
}
       
template<class DataType_>
datacondAPI::FrequencySequence<DataType_>::~FrequencySequence()
{
}

template<class DataType_>
datacondAPI::FrequencySequence< DataType_ >*
datacondAPI::FrequencySequence<DataType_>::
Clone() const
{
	return new FrequencySequence<DataType_>(*this);
}

template<class DataType_>
ILwd::LdasContainer* datacondAPI::FrequencySequence<DataType_>::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
    using namespace datacondAPI;

    std::unique_ptr<ILwd::LdasContainer> container(new ILwd::LdasContainer);

    switch (Target)
    {
    case udt::TARGET_FRAME:
    case udt::TARGET_FRAME_FINAL_RESULT:
        {
            using namespace ILwdFCS;
            
            FrequencySequence<DataType_>& data =
                const_cast<FrequencySequence<DataType_>&>(*this);
	    const std::string&
		lname( Chain.GetIntermediateName( *this,
						  this->name( ) ) );
            
            FrProcData proc_data;
            
            proc_data.SetName( lname );
            proc_data.SetComment( Chain.GetIntermediateComment( *this ) );
            proc_data.SetType( ILwdFCS::FrProcData::FREQUENCY_SERIES );
            proc_data.SetSubType( ILwdFCS::FrProcData::UNKNOWN_SUB_TYPE );
            
            //:NOTE: All these are undefined for FrequencySeries
            // proc_data.SetTimeOffset( 0.0 );
            // proc_data.SetTRange( 0.0 );
            // proc_data.SetFShift( 0.0 );
            // proc_data.SetPhase(0.0);
            
            proc_data.SetFRange( GetFrequency( this->size( )) -
				 GetFrequency(0));
            
            //:NOTE: Bandwidth is unknown - leave undefined
            // proc_data.SetBW(0.0);
            
            //:NOTE: Add auxParams, aux data, tables here
            
            // Add the data
            FrVect vect_data;
            vect_data.SetData( &data[0],
                               data.size(),
                               GetFrequency( 0 ),
                               "Hertz",
                               GetFrequencyDelta() );
            vect_data.SetName( lname );

            proc_data.AppendData( vect_data );
            
            proc_data.AppendHistory( this->getHistory( ),
				     GPSTime::NowGPSTime() );
            
            ArbitraryWhenMetaData when;
            when.Store( proc_data );
            
            ILwd::LdasContainer* retval =
                const_cast<CallChain&>(Chain).FormatFrame( Target,
                                                           container.get(),
                                                           proc_data );

            container.reset(retval);

            // Store the metadata in an FrDetector struct
            GeometryMetaData::Store( const_cast<CallChain&>(Chain).
                                     GetFrameHeaderTop() );
        }
        break;

    case udt::TARGET_GENERIC:
        container->push_back( Sequence<DataType_>::
                              ConvertToIlwd(Chain, Target),
                              ILwd::LdasContainer::NO_ALLOCATE,
                              ILwd::LdasContainer::OWN );
        container->push_back( FrequencyMetaData::
                              ConvertToIlwd(Chain, Target),
                              ILwd::LdasContainer::NO_ALLOCATE,
                              ILwd::LdasContainer::OWN );
        break;

    case udt::TARGET_WRAPPER:
        {
            // :TRICKY: Throw away the constant value so data component can
            // :TRICKY: be passed to ILwd::LdasyArray<DataType_> constructor.
            FrequencySequence<DataType_>& data =
                const_cast<FrequencySequence<DataType_>&>(*this);
            std::string	domain( this->GetDomain() );
            domain += ":domain";
            
            container->appendName( this->name( ) );
            container->appendName( "spectrum" );
            container->appendName( "sequence" );
            container->appendName( "" );	// Place holder for primary.
            container->push_back( new ILwd::LdasString("FREQ", domain),
                                  ILwd::LdasContainer::NO_ALLOCATE,
                                  ILwd::LdasContainer::OWN );
            // Store the meta data
            GeometryMetaData::Store( *container );
            FrequencyMetaData::store( *container, Target, this->size( ) );
            
            // Store History
            udt::ConvertToIlwd( Chain, Target, container.get() );
            
            // Store data
            container->push_back( new ILwd::LdasArray<DataType_>
                                  ( &data[0],
                                    data.size(),
                                    "data"),
                                  ILwd::LdasContainer::NO_ALLOCATE,
                                  ILwd::LdasContainer::OWN );
        }
        break;

    default:
        throw udt::
            BadTargetConversion(Target,
                                "datacondAPI::FrequencySequence");
        break;
    }

    return container.release();
}


// instantiations

#undef CLASS_INSTANTIATION
#define CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::FrequencySequence< class_ >; \
UDT_CLASS_INSTANTIATION(FrequencySequence< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)

