#ifndef _DAUB4PSUFUNCTION_HH_
#define _DAUB4PSUFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI {
namespace psu
{

class Daub4PSUFunction : public CallChain::Function
{
public:
  Daub4PSUFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

}//end namespace psu
}//end namespace datacondAPI

#endif //_DAUB4PSUFUNCTION_HH_  


