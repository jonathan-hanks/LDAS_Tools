#ifndef COHERENCE_HH
#define COHERENCE_HH

#include "SequenceUDT.hh"
#include "WelchCSDEstimate.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "BasicFunctions.hh"

template<typename T> struct traits
{
  typedef std::complex<T> type;
  typedef T real_type;
};

template<typename T> struct traits<std::complex<T> >
{
  typedef std::complex<T> type;
  typedef T real_type;
};

namespace datacondAPI
{
  class Coherence
  {
  public:
    explicit Coherence(const WelchCSDEstimate& csd = WelchCSDEstimate());
    Coherence(const Coherence& c);

    virtual ~Coherence();

    Coherence& operator=(const Coherence &);

    void apply(datacondAPI::udt*& out, const datacondAPI::udt& in1, const datacondAPI::udt& in2);

    template <typename T>
    void apply(std::valarray<typename traits<T>::type>& out, const std::valarray<T>& in1, const std::valarray<T>& in2);

  private:
    WelchCSDEstimate m_csd;    
  };
}

#endif // COHERENCE_HH
