// $Id: LinearDetrendFunction.cc,v 1.4 2005/12/01 22:54:58 emaros Exp $
//
// "lineardetrend" action
//
// [y = ] lineardetrend(x)
//
// x: Sequence<T> or TimeSeries<T> where T is float, double,
//    complex<float>, complex<double> input sequence
// y: Sequence<T> or TimeSeries<T> where T is float, double
//    output sequence
//

#include "datacondAPI/config.h"

#include "LinearDetrendFunction.hh"
#include "Detrend.hh"

namespace {
    LinearDetrendFunction staticLinearDetrendFunction;
}

LinearDetrendFunction::LinearDetrendFunction()
    : Function( LinearDetrendFunction::GetName() )
{ }

//
// LinearDetrendFunction::Eval
//
// This virtual function is called whenever a CallChain attempts to process an
// action named "lineardetrend". The Eval function recieves a pointer to the
// calling CallChain, and a list of symbol names (Params) and a return value
// symbol name (Ret).
//
void 
LinearDetrendFunction::Eval(      CallChain* Chain,
			  const CallChain::Params& Params,
                          const std::string& Ret) const
{
    if (Params.size() != 1)
    {
        throw CallChain::BadArgumentCount("lineardetrend", 1, Params.size());
    }

    const CallChain::Symbol& in = *(Chain->GetSymbol(Params[0]));

    CallChain::Symbol* out_ptr = 0;
    datacondAPI::Detrend detrend(datacondAPI::Detrend::linear);

    detrend.apply(out_ptr, in);
    
    Chain->ResetOrAddSymbol( Ret, out_ptr );
}

const std::string&
LinearDetrendFunction::GetName() const
{
    static const std::string name("lineardetrend");

    return name;
}
