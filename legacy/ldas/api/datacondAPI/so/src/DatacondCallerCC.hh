#ifndef DATACONDAPI__DATACONDCALLERCC_HH
#define DATACONDAPI__DATACONDCALLERCC_HH

#include "DatacondCaller.h"

class CallChain;

int
DatacondCallerCC( const char* Algorithm,
		  const char* Aliases,
		  datacond_symbol_type* Symbols,
		  int NumberOfSymbols,
		  char** ErrorMessage,
		  CallChain& Executioner );

#endif /* DATACONDAPI__DATACONDCALLERCC_HH */
