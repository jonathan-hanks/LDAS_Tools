/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "datacondAPI/config.h"

#include <complex>

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"

#include "CallChain.hh"
#include "WhenUMD.hh"
#include "DFTUDT.hh"

// constructors

template<class DataType_>
datacondAPI::DFT<DataType_>::DFT()
{
}

template<class DataType_>
datacondAPI::DFT<DataType_>::DFT(unsigned int size)
  : Sequence<DataType_>(size)
{
}

template<class DataType_>
datacondAPI::DFT<DataType_>::DFT(const std::valarray<DataType_>& value)
  : Sequence<DataType_>(value)
{
}

template<class DataType_>
datacondAPI::DFT<DataType_>::DFT(const Sequence<DataType_>& value)
  : Sequence<DataType_>(value)
{
}

template<class DataType_>
datacondAPI::DFT<DataType_>::DFT(const DFT& value)
  : Sequence<DataType_>(value)
{
}

template<class DataType_>
datacondAPI::DFT<DataType_>::DFT(const DataType_* const data,
                                  const size_t size)
  : Sequence<DataType_>(data, size)
{
}

// destructor

template<class DataType_>
datacondAPI::DFT<DataType_>::~DFT()
{
}

// UDT required methods

template<class DataType_>
datacondAPI::DFT<DataType_>* datacondAPI::DFT<DataType_>::Clone() const
{
  return new DFT<DataType_>(*this);
}

template<class DataType_>
ILwd::LdasElement* datacondAPI::DFT<DataType_>::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  ILwd::LdasContainer* container = new ILwd::LdasContainer;

  switch(Target)
  {
  case udt::TARGET_FRAME:
  case udt::TARGET_FRAME_FINAL_RESULT:
    {
      using namespace ILwdFCS;

      DFT<DataType_>& data = const_cast<DFT<DataType_>&>(*this);
      const std::string& lname( Chain.GetIntermediateName( *this,
							   this->name( ) ) );
      FrProcData proc_data;

      proc_data.SetName( lname );
      proc_data.SetComment( Chain.GetIntermediateComment( *this ) );

      // In the datacon DFT's aren't derived from FrequencySeries,
      // but they should be tagged this way for output to frames
      proc_data.SetType( ILwdFCS::FrProcData::FREQUENCY_SERIES );
      proc_data.SetSubType( ILwdFCS::FrProcData::DFT );

      //:NOTE: undefined for this class
      // proc_data.SetTimeOffset( 0.0 );
      // proc_data.SetTRange( 0.0 );
      // proc_data.SetFShift( 0.0 );
      // proc_data.SetPhase( 0.0 );
      // proc_data.SetFRange( 0.0 );
      // proc_data.SetBW( 0.0 );
      
      //:NOTE: Add auxParams, aux data, tables here
      
      // Add the data
      FrVect vect_data;
      vect_data.SetData( &data[0], this->size() );
      vect_data.SetName( lname );

      proc_data.AppendData( vect_data );

      proc_data.AppendHistory( this->getHistory(), GPSTime::NowGPSTime() );

      ArbitraryWhenMetaData when;
      when.Store( proc_data );

      ILwd::LdasContainer* retval =
	const_cast<CallChain&>(Chain).FormatFrame( Target,
						   container,
						   proc_data );

      if ( retval != container )
      {
	delete container;
	container = retval;
      }

    }
    break;

  case udt::TARGET_WRAPPER:
    {
      DFT<DataType_>& data = const_cast<DFT<DataType_>&>(*this);
      std::string domain(":domain");
	
      domain.insert( 0, this->GetDomain( ) );
      
      container->appendName( this->name( ) );
      container->appendName("channel");
      container->appendName("sequence");

      // This function in the UDT base class is the only way to get
      // the UDT history into the container.
      udt::ConvertToIlwd(Chain, udt::TARGET_WRAPPER, container);

      container->push_back(new ILwd::LdasString("NONE", domain),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );

      // Push the time-series data
      container->push_back(new ILwd::LdasArray<DataType_>
			   (&data[0], data.size(), "data"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
    }
    break;

  case udt::TARGET_GENERIC:
    {
      DFT<DataType_>& data = const_cast<DFT<DataType_>&>(*this);
      
      delete container;

      return new ILwd::LdasArray<DataType_>(&data[0], data.size());
    }
    break;

  default:
    throw udt::BadTargetConversion(Target, "datacondAPI::Sequence");
    break;
  }

  return container;
}

// metadata methods

template<class DataType_>
bool datacondAPI::DFT<DataType_>::IsSymmetric() const
{
  const unsigned int n = this->size( );
  
  if(n) // if we have data
    {
      if( this->operator[](0).imag() != 0.0) // if [0] is not real
	{
	  return false;
	}
      // index of (negative) nyquist frequency
      const unsigned int nyquist = (n + 1) / 2; 
      // if nyquist frequency of even sequence is not real
      if(!(n & 1) && ( this->operator[](nyquist).imag() != 0.0)) 
	{
	  return false;			
	}
      for(unsigned int i = 1; i < nyquist; ++i)
	{
	  if( this->operator[](i) != std::conj(this->operator[](n - i)))
	    {
	      return false;
	    }
	}
    }
  return true;
}

template<class DataType_>
void datacondAPI::DFT<DataType_>::SetSymmetric()
{
  if(!IsSymmetric())
    {
      this->operator[](0) = DataType_(this->operator[](0).real(), 0);
      const unsigned int n = this->size();
      // index of (negative) nyquist frequency
      const unsigned int nyquist = (n + 1) / 2; 
      if(!(n & 1))
	{
	  this->operator[](nyquist) = DataType_(this->operator[](nyquist).real(), 0);
	}
      for(unsigned int i = 0; i < nyquist; ++i)
	{
	  this->operator[](n - i) =
	    std::conj(this->operator[](i) = (this->operator[](i) +
				  std::conj(this->operator[](n - i))) / (DataType_)2);
	}
    }
}

// instantiations

#undef CLASS_INSTANTIATION
#define CLASS_INSTANTIATION(class_,key_) \
    template class datacondAPI::DFT< class_ >; \
UDT_CLASS_INSTANTIATION(DFT< class_ >,key_)

CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)

#if __SUNPRO_CC
#undef INIT_FUNC
#define INIT_FUNC init_DFTUDT
MK_UDT_INIT_FUNC_2(INIT_FUNC,cfloat,cdouble);
#pragma init (INIT_FUNC)
#endif /* __SUNPRO_CC */
