#include "datacondAPI/config.h"

#include <memory>
#include <numeric>

#include "general/Memory.hh"

#include "Detrend.hh"
#include "TimeSeries.hh"

using General::GPSTime;

#define DETREND_DEBUG 0

namespace datacondAPI {

    Detrend::Detrend(const DetrendMethod method)
	: m_method(method)
    {
	if (m_method < mean || m_method > linear)
	{
	    throw std::invalid_argument("Detrend::Detrend() - "
					"invalid detrend method");
	}
    }

    void Detrend::apply(udt*& out, const udt& in) const
    {
#define IF_TIME_SERIES(TYPE) \
        if (const TimeSeries< TYPE >* const p = \
            dynamic_cast<const TimeSeries< TYPE >* >(&in)) \
        { \
            dispatch<TimeSeries< TYPE > >(out, *p); \
        }

#define IF_SEQUENCE(TYPE) \
        if (const Sequence< TYPE >* const p = \
            dynamic_cast<const Sequence< TYPE >* >(&in)) \
        { \
            dispatch<Sequence< TYPE > >(out, *p); \
        }

	IF_TIME_SERIES(float)
	else IF_TIME_SERIES(double)
	else IF_TIME_SERIES( std::complex<float> )
	else IF_TIME_SERIES( std::complex<double> )
	else IF_SEQUENCE(float)
	else IF_SEQUENCE(double)
	else IF_SEQUENCE( std::complex<float> )
	else IF_SEQUENCE( std::complex<double> )
        else
        {
            throw std::invalid_argument("Detrend::apply() "
              "input UDT must be a TimeSeries<float> or TimeSeries<double>");
        }

#undef IF_SEQUENCE
#undef IF_TIME_SERIES

    }

    template<class T>
    void Detrend::dispatch(udt*& out, const T& in) const
    {
	std::unique_ptr<T > tmp(0);
	udt* tmp_out = out;

	if (tmp_out == 0)
	{
	    tmp.reset(new T());
	    tmp_out = tmp.get();
	}
	else if (!udt::IsA<T>(*tmp_out))
	{
	    // Can't delete the output if it's the wrong type,
	    // have to throw an exception instead
	    throw std::invalid_argument("Detrend::dispatch() - "
              "output UDT must be a Sequence or TimeSeries");
	}

	apply(udt::Cast<T>(*tmp_out), in);

	// Caution - only alter 'out' if we created it
	if (out == 0)
	{
	    // transfer ownership from the unique_ptr to 'out' so that
	    // 'out' isn't deleted when the unique_ptr goes out of scope
	    out = tmp.release();
	}
    }

    template<class T>
    void Detrend::meanDetrend(TimeSeries<T>& in) const
    {
	// The number of non-gap points
	size_t M = 0;

	// The mean of the data not in gaps
	T mean = 0.0;

	// Get the non-gaps
	GapMetaData<GPSTime>::Transitions_type trans;
	in.CalcTransitionsOfData(trans);

	GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
        while (i != trans.end())
        {
	    const size_t size = ((*i).stop - (*i).start);
            M += size;

	    // "mean" is really the sum at this point
	    // accumulate() takes the input (mean) and returns
	    // mean + sum of the values
	    mean = std::accumulate(&in[(*i).start], &in[(*i).stop], mean);

            ++i;
        }

	mean /= M;
	
	// Subtract the mean out of the non-gaps
	i = trans.begin();
        while (i != trans.end())
        {
	    // By the C++ standard, the following is meant to work
	    // but, annoyingly, doesn't in GCC
	    //   const slice s((*i).start, (*i).stop - (*i).start, 1);
	    //   in[s] -= T(mean);
	    
	    for (size_t k = (*i).start; k < (*i).stop; ++k)
	    {
		in[k] -= mean;
	    }

            ++i;
        }
    }

    template<class T>
    void Detrend::linearDetrend(TimeSeries<T>& in) const
    {
	// The linear detrending is based on Numerical Recipes sec. 15.2
	// where the weighting (sigma_k) is taken to be uniform (sigma_k = 1)

	// Get the non-gaps
	GapMetaData<GPSTime>::Transitions_type trans;
	in.CalcTransitionsOfData(trans);

	// The number of non-gap points
	size_t M = 0;

	double sum_x = 0.0;
	T sum_y = 0.0;

	GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
        while (i != trans.end())
        {
	    const size_t start = (*i).start;
	    const size_t stop  = (*i).stop;
	    const size_t size = stop - start;

	    M += size;

	    // Sum the "x" values (sum of an algebraic series)
	    sum_x += size*(2*start + size - 1)/2.0;

	    // Sum the "y" values
	    sum_y = std::accumulate(&in[start], &in[stop], sum_y);

            ++i;
        }

	const double sum_x_on_M = sum_x/M;
	
	T b = 0.0;
	double norm = 0.0;

	i = trans.begin();
        while (i != trans.end())
        {
	    for (size_t k = (*i).start; k < (*i).stop; ++k)
	    {
		const double dev = k - sum_x_on_M;

		norm += dev*dev;
		b    += T(dev)*in[k];
	    }

            ++i;
        }

	b /= norm;

	const T a = (sum_y - T(sum_x)*b)/T(M);

	// Subtract the linear trend out of the non-gaps
	i = trans.begin();
        while (i != trans.end())
        {
	    for (size_t k = (*i).start; k < (*i).stop; ++k)
	    {
		in[k] -= (a + b* T(k));
		//:TODO: With this print statement, the code appear to work
		//:TODO: properly.
		// std::cerr << "DEBUG: linear detrend: in[" << k << "] := " << in[k] << std::endl;
	    }

            ++i;
	    //:TODO: With this print statement, the code appear to work
	    //:TODO: properly.
	    //std::cerr << "DEBUG: sum_y: " << sum_y << std::endl;
	    //:TODO: With this print statement, the code appear to work
	    //:TODO: properly.
	    //std::cerr << "DEBUG: b: " << b << std::endl;

        }
    }

    template<class T>
    void Detrend::apply(Sequence<T>& out, const Sequence<T>& in) const
    {
	// check for valid data
	//
	const size_t n = in.size();
	
	if (n == 0) 
	{
	    throw std::invalid_argument("Detrend::apply() - no input data");
	}
	
	// Copy input to output
	// No size checking needed - Sequence assignment is safe
	out = in;
	
	switch (m_method)
	{
	case mean:

	    // remove mean from input data
	    out -= (in.sum()/T(n));
	    break;

	case linear:
	
	    // remove linear trend (y = a + b x) from input data
	    {
		// calculate "b" coeff of least square fit
		//
		double norm = 0.0;

		T sum_y = 0.0;
		T b     = 0.0;
		
		for (size_t k = 0; k < n; ++k)
		{
		    const double dev = k - (n - 1)/2.0;

		    norm  += dev*dev;
		    b     += T(dev)*in[k];
		    sum_y += in[k];
		}

		b /= norm;
		
		// calculate "a" coefficient of least square fit
		//
		const T a = sum_y/T(n) - b*T((n - 1)/2.0);
		
		// subtract off fitted line from original data
		//
		for (size_t k = 0; k < n; ++k)
		{
		    out[k] -= (a + b*T(k));
		}
	    }
	    break;
	    
	default:
	    // This ought never happen - if it does, it's a bug in the
	    // class, NOT a user error
	    throw std::invalid_argument("Detrend::apply() - "
					"invalid detrend method");
	    break;
	}
    }

    template<class T>
    void Detrend::apply(TimeSeries<T>& out, const TimeSeries<T>& in) const
    {
	// check for valid data
	//
	const size_t n = in.size();
	
	if (n == 0) 
	{
	    throw std::invalid_argument("Detrend::apply() - no input data");
	}
	
	// Copy input to output
	// No size checking needed - Sequence assignment is safe
	out = in;
	
	switch (m_method)
	{
	case mean:
	    meanDetrend(out);
	    break;

	case linear:
#if DETREND_DEBUG
	    std::cerr << "DEBUG: before linearDetrend: out[7]: " << out[7] << std::endl;
#endif /* DETREND_DEBUG */
	    linearDetrend(out);
#if DETREND_DEBUG
	    std::cerr << "DEBUG: after linearDetrend: out[7]: " << out[7] << std::endl;
#endif /* DETREND_DEBUG */
	    break;
	    
	default:
	    // This ouught never happen - if it does, it's a bug in the
	    // class, NOT a user error
	    throw std::invalid_argument("Detrend::apply() - "
					"invalid detrend method");
	    break;
	}
    }

    // Detrend class instantiations
#define INSTANTIATE(TYPE) \
    template void Detrend::apply(TimeSeries<TYPE>& out, \
			    const TimeSeries<TYPE>& in) const; \
    template void Detrend::apply(TimeSeries<std::complex<TYPE> >& out, \
			    const TimeSeries<std::complex<TYPE> >& in) const

    INSTANTIATE(float);
    INSTANTIATE(double);

#undef INSTANTIATE

} // namespace datacondAPI
