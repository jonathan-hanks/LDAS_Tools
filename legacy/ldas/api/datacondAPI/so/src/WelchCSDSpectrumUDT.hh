/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef WELCHCSDSPECTRUMUDT_HH
#define WELCHCSDSPECTRUMUDT_HH
// $Id: WelchCSDSpectrumUDT.hh,v 1.16 2006/02/16 16:54:58 emaros Exp $

#if HAVE_LDAS_PACKAGE_ILWD
#include "ilwd/ldascontainer.hh"
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "CSDSpectrumUDT.hh"
#include "WelchCSDSpectrumUMD.hh"

namespace DB
{
  class Table;
}

namespace datacondAPI
{
  
  //: Cross-spectral density estimated by Welch method
  template<class T> 
  class WelchCSDSpectrum : public CSDSpectrum<T>, 
			   public WelchCSDSpectrumUMD
  {
    
  public:
    
    //: destructor
    virtual ~WelchCSDSpectrum();
    
    // udt methods

    //: Create duplicate of *this on heap
    //!return: WelchCSDSpectrum<T>* - duplicate of *this, allocated on heap
    virtual WelchCSDSpectrum< T >* Clone() const;
    
    //: Convert *this to an appropriate ilwd
    //!param: Chain - 
    //!param: Target - flag specifying destination API for ILWD
    //!return: ILwd::LdasContainer* - pointer to ILWD representing *this, 
    //+ appropriate for transmitting to destination specified by Target
    virtual ILwd::LdasContainer*
    ConvertToIlwd(const CallChain& Chain,
		  udt::target_type Target = udt::TARGET_GENERIC ) const;
    
    //: Create WelchCSDSpectrum from database information.

    static void CreateFromDBTable( const DB::Table& Table,
				   std::vector<datacondAPI::udt*>& Spectrum,
				   std::vector<std::string>& Names,
				   const bool IgnoreTableName = false );

    //: Validate if the table supplied is sufficient to create a
    //: WelchSpectrum

    static bool ValidateDBTable( const DB::Table& Table );

  }; // class WelchCSDSpectrum
  
} // namespace datacondAPI

#endif // WELCHCSDSPECTRUMUDT_HH
