#ifndef _SYNTHESIZEFUNCTION_HH_
#define _SYNTHESIZEFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI
{

class SynthesizeFunction : public CallChain::Function
{
public:
  SynthesizeFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain, const CallChain::Params& Params,
		    const std::string& Ret) const;

};

}//end namespace datacondAPI

#endif //_SYNTHESIZEFUNCTION_HH_
