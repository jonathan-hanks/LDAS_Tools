#include "datacondAPI/config.h"

#include <memory>

#include "general/types.hh"
#include "general/gpstime.hh"

#include "ilwd/ilwdtoc.hh"
#include "ilwd/ldasarray.hh"

#include "genericAPI/ChannelNameLexer.hh"

#include "DetectorDatabase.hh"
#include "GeometryMetaData.hh"
#include "ILwdMetaDataHelper.hh"
#include "TimeSeries.hh"
#include "TypeInfo.hh"
#include "UDT.hh"
#include "WhenUMD.hh"

//!ignore_begin:
// Do not generate any documentation for these debugging macros
#if 0
#define AT() std::cerr << "DEBUG: AT: " << (void*)this << " " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#define AT_STATIC() std::cerr << "DEBUG: AT: " << " " << __FILE__ << " " << __LINE__ << std::endl << std::flush
#else
#define AT() 
#define AT_STATIC()
#endif

using namespace datacondAPI;

template<class T>
static T get_metadata( const ILwd::LdasElement& Element,
		       const std::string& Variable );

struct DetectorDatabase::info_type
{
  info_type( const ILwd::TOC& Detector );
  info_type( const General::GPSTime& Start, const General::GPSTime& End,
	  const ILwd::TOC& Detector );
  info_type( const info_type& Source );

  info_type& operator=( const info_type& Source );
	  
  bool				time_set;
  ArbitraryWhenMetaData		time_range;
  GeometryMetaData		detector;
};

DetectorDatabase::info_type::
info_type( const ILwd::TOC& Detector )
  : time_set( false )
{
  detector.AddDetector( Detector );
}

DetectorDatabase::info_type::
info_type( const General::GPSTime& Start, const General::GPSTime& End,
	const ILwd::TOC& Detector )
  : time_set( true ),
    time_range( Start, End )
{
  detector.AddDetector( Detector );
}

DetectorDatabase::info_type::
info_type( const DetectorDatabase::info_type& Source )
{
  if ( &Source != this )
  {
    *this = Source;
  }
}

DetectorDatabase::info_type& DetectorDatabase::info_type::
operator=( const DetectorDatabase::info_type& Source )
{
  if ( &Source != this )
  {
    time_set = Source.time_set;
    time_range = Source.time_range;
    detector = Source.detector;
  }
  return *this;
}

DetectorDatabase::
DetectorDatabase( )
  : m_detectors( new detector_cache_type )
{
}

DetectorDatabase::
~DetectorDatabase( )
{
  delete m_detectors;
}

void DetectorDatabase::
AddDetector( const ILwd::TOC& Detector,
	     const ILwdMetaDataHelper& FrameHeader )
{
  
  const ILwd::LdasArray<INT_4U>* istart = dynamic_cast<const ILwd::LdasArray<INT_4U>*>
    ( FrameHeader.GetILwd( ILwdMetaDataHelper::START_TIME ) );
  const REAL_8 delta =
    FrameHeader.Get<REAL_8>( ILwdMetaDataHelper::DELTA_TIME );
  General::GPSTime	frame_start( (istart->getData())[0],
				     (istart->getData())[1] );

  info_type		info( frame_start, frame_start + delta,
			      Detector );

  ArbitraryWhenMetaData	frame_time( frame_start, frame_start + delta );

  for ( detector_cache_type::const_iterator i = m_detectors->begin();
	i != m_detectors->end();
	i++ )
  {
    if ( ( (*i).time_range.GetStartTime( ) == frame_time.GetStartTime( ) ) &&
	 ( (*i).time_range.GetEndTime() == frame_time.GetEndTime( ) ) &&
	 ( (*i).detector == info.detector ) )
    {
      return;
    }
  }
  m_detectors->push_back( info );
}

void DetectorDatabase::
AddDetector( const ILwd::TOC& Detector )
{
  m_detectors->push_back( info_type( Detector ) );
}

void DetectorDatabase::
AddDetectorInfo( udt& Object ) const
try
{
  AT( );
  using namespace datacondAPI;

  static const double	fudge_factor( 1 );

  WhenMetaData&		when( dynamic_cast<WhenMetaData&>( Object ) );
  GeometryMetaData&	detectors( dynamic_cast<GeometryMetaData&>( Object ) );

  General::GPSTime	start( when.GetStartTime() );
  General::GPSTime	end( when.GetEndTime() );
  double		dt( fudge_factor );

  AT( );
#if 0
  //---------------------------------------------------------------------
  // If this is timeseries data, then calculate a bounding region for
  //    resample and other actions that may hvae modified the start
  //    and/or end time.
  //---------------------------------------------------------------------

  if ( dynamic_cast< TimeSeriesMetaData* >( &Object ) )
  {
    dt = fudge_factor *
      dynamic_cast< TimeSeriesMetaData& >( Object ).GetStepSize();
  }
#endif	/* 0 */

  //---------------------------------------------------------------------
  // This object needs detector information
  //---------------------------------------------------------------------

  //---------------------------------------------------------------------
  // Get a list of channel names
  //---------------------------------------------------------------------
  AT( );
  GenericAPI::ChannelNameLexer	channel_lexer( Object.name() );
  const GenericAPI::ChannelNameLexer::data_list_type&
    channel_names( channel_lexer.GetChannelNames() );

  //---------------------------------------------------------------------
  // Find interferometers that match the channel names
  //---------------------------------------------------------------------

  AT( );
  for ( GenericAPI::ChannelNameLexer::data_list_type::const_iterator
	  n( channel_names.begin() );
	n != channel_names.end();
	n++ )
  {
    std::string	prefix( (*n).first.substr( 0, 2 ) );

    for ( detector_cache_type::const_iterator i = m_detectors->begin();
	  i != m_detectors->end();
	  i++ )
    {
      if ( (*i).time_set &&
	   ( ( ( start >= (*i).time_range.GetStartTime() ) &&
	       ( end <= (*i).time_range.GetEndTime() ) ) ||
	     ( ( start >= ( (*i).time_range.GetStartTime() - dt ) ) &&
	       ( end <= ( (*i).time_range.GetEndTime() + dt ) ) ) ) )
      {
	if ( detectors.AddDetector( (*i).detector, prefix ) )
	{
	  break;
	}
      }
    }
  }

  //---------------------------------------------------------------------
  // Put in all unknown interferometers for the given time range
  //---------------------------------------------------------------------

  AT( );
  for ( detector_cache_type::const_iterator i = m_detectors->begin();
	i != m_detectors->end();
	i++ )
  {
    if ( (*i).time_set &&
	 ( ( ( start >= (*i).time_range.GetStartTime() ) &&
	     ( end <= (*i).time_range.GetEndTime() ) ) ||
	   ( ( start >= ( (*i).time_range.GetStartTime() - dt ) ) &&
	     ( end <= ( (*i).time_range.GetEndTime() + dt ) ) ) ) )
    {
      detectors.AddUnmappedDetectors( (*i).detector );
    }
  }
}
catch( std::bad_cast& e )
{
  AT( );
}

void DetectorDatabase::
SetDetectorTime( const WhenMetaData& When )
{
  for ( detector_cache_type::iterator i = m_detectors->begin();
	i != m_detectors->end(); )
  {
    // Flag if m_detectors element has been erased using iterator "i": 
    // iterator is already reset to the next element in the list --->
    // no need to increment
    bool erased( false );
   
    if ( (*i).time_set == false )
    {
      bool duplicate( false );
      for ( detector_cache_type::iterator d = m_detectors->begin();
	    d != m_detectors->end();
	    d++ )
      {
	if ( ( (*d).time_set == true ) &&
	     ( (*d).time_range.GetStartTime() <= When.GetStartTime() ) &&
	     ( (*d).time_range.GetEndTime() >= When.GetEndTime() ) &&
	     ( (*d).detector == (*i).detector ) )
	{
	  duplicate = true;
	  i = m_detectors->erase(i);
          erased = true;
	  break;
	} else if ( ( (*d).time_set == true ) &&
		    ( (*d).time_range.GetStartTime() >= When.GetStartTime() ) &&
		    ( (*d).time_range.GetEndTime() <= When.GetEndTime() ) &&
		    ( (*d).detector == (*i).detector ) )
	{
          if( d == i )
          {
             // d and i iterators point to the same list element: will invalidate 
             // iterator i
             i = m_detectors->erase( d );
             erased = true;
          }
          else
          {
  	     m_detectors->erase(d);
          }
   
	  break;
	}
      } // if time_set == false
   
      if ( !duplicate )
      {
	(*i).time_range = When;
	(*i).time_set = true;
      }
    }
   
    if( erased == false )
    {
       ++i;
    }
  }
}

