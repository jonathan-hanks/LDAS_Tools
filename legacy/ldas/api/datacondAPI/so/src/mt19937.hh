#ifndef MT19937_HH
#define MT19937_HH

#include "mtstate.hh"

namespace datacondAPI {

  double mt19937(mtstate& s);

  void mt19937(std::valarray<double>& x, int nreq, mtstate& s);
}

#endif
