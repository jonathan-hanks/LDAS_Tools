#include "config.h"

#include "SynthesizeFunction.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"
#include "reconstructor.hh"
#include "result.hh"

namespace datacondAPI
{

const std::string& SynthesizeFunction::GetName(void) const
{
  static std::string name("Synthesize");
  return name;
}

SynthesizeFunction::SynthesizeFunction()
  : Function( SynthesizeFunction::GetName() )
{}

static SynthesizeFunction _SynthesizeFunction;

void SynthesizeFunction::Eval(CallChain* Chain, const CallChain::Params& Params,
			       const std::string& Ret) const
{
  CallChain::Symbol* returnedSymbol = 0;
  using namespace datacondAPI;

  switch(Params.size())
    {      
    case 1:
      //Synthesize(Result)
      {	
			
	if(udt::IsA<Result<double> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<double> in(udt::Cast<Result<double> >(*Chain->GetSymbol(Params[0])));
	    Sequence<double> out;
	    Reconstructor::Reconstructor reconstruction;
	    reconstruction.apply(out, in);
	    returnedSymbol = new Sequence<double> (out);

	
	  }

	else if (udt::IsA<Result<float> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<float> in( udt::Cast<Result<float> >(*Chain->GetSymbol(Params[0])));
	    Sequence<float> out;
	    Reconstructor::Reconstructor reconstruction;
	    reconstruction.apply(out, in);
	    returnedSymbol = new Sequence<float> (out);

	  }


	break;
      }//end case 1

    case 2:
      //Synthesize(Result, level)
      {

	if(udt::IsA<Result<double> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<double> in(udt::Cast<Result<double> >(*Chain->GetSymbol(Params[0])));
	    Sequence<double> out;

	    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	      {
		int level = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue();
		Reconstructor::Reconstructor reconstruction;
		reconstruction.apply(out, in, level);
		returnedSymbol = new Sequence<double> (out); 

	      }

	  }

	else if (udt::IsA<Result<float> >( *Chain->GetSymbol(Params[0])))
	  {
	    Result<float> in( udt::Cast<Result<float> >(*Chain->GetSymbol(Params[0])));
	    Sequence<float> out;

	    if(udt::IsA<Scalar<int> >( *Chain->GetSymbol(Params[1])))
	      {
		int level = udt::Cast<Scalar<int> > (*Chain->GetSymbol(Params[1])).GetValue();
		Reconstructor::Reconstructor reconstruction;
		reconstruction.apply(out, in, level);
		returnedSymbol = new Sequence<float> (out);
	      }

	  }


	break;
      }//end case 2

    default:
      {
	throw CallChain::BadArgument(GetName(), 0, "Wrong number of parameters", "Usage: Synthesize(in, level)");
	break;
      }

    }//end switch

  if(!returnedSymbol)
    {
      throw CallChain::NullResult( GetName() );
    }

  Chain->ResetOrAddSymbol( Ret, returnedSymbol );

}//end Eval

}//end namespace datacondAPI
