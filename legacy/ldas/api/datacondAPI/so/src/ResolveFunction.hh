#ifndef _RESOLVEFUNCTION_HH_
#define _RESOLVEFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI
{

class ResolveFunction : public CallChain::Function
{
public:
  ResolveFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain, const CallChain::Params& Params,
		    const std::string& Ret) const;

};

}//end namespace datacondAPI

#endif //_RESOLVEFUNCTION_HH_
