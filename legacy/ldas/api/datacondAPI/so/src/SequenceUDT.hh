/* -*- mode: c++; c-basic-offset: 2; -*- */

#ifndef SEQUENCE_UDT_HH
#define	SEQUENCE_UDT_HH
// $Id: SequenceUDT.hh,v 1.18 2006/02/16 16:54:58 emaros Exp $
#include <valarray>
#include "UDT.hh"

// Forward declaration of ILwd elements
namespace ILwd
{
  class LdasElement;
}

namespace datacondAPI
{
  //---------------------------------------------------------------------
  //: Base class for sequence data
  //---------------------------------------------------------------------

  template <class DataType_>
  class Sequence : public udt, public std::valarray<DataType_>
  {
  public:
    //: Default constructor
    Sequence();
    
    //: Constructor allowing specification of object size
    //!param: Size - pre-allocate data using default constructor for DataType_
    Sequence(size_t Size);

    //: Constructor allowing specification of value, size
    //!param: Value - data to assign to elements of Sequence
    //!param: Size - number of elements to allocate
    Sequence(const DataType_& Value, size_t Size);
    
    //: Construct from a std::valarray<DataType_>
    //!param: Value - object to construct from
    Sequence(const std::valarray<DataType_>& Value);
    
    //: Copy constructor
    //!param: Value - object to create copy of 
    Sequence(const Sequence& Value);
    
    //: Construct from an array of data. Assume that the pointer 
    //+ addresses at least Size consecutive objects of type DataType_, 
    //+ which will be assigned to the consecutive elements of the Sequence
    //!param: Data - pointer to DataType_
    //!param: Size - number of elements to pre-allocate and assign to. 
    Sequence(const DataType_* Data, size_t Size);

    //: Construct from an array of data, not of type DataType_.  Assume that 
    //+ the pointer addresses at least Size consecutive objects, which can be 
    //+ implicitly converted to type DataType_. These will be assigned to the
    //+ consecutive elements of the Sequence
    //!param: Data - pointer to ArrayData_
    //!param: Size - number of elements to pre-allocate and assign to. 
    template<class ArrayData_>
    Sequence(const ArrayData_* Data, size_t Size);

    //; Destructor
    virtual ~Sequence();
    
    //: Assignment operator. NOTE: Unlike valarray, this copy assignment is 
    //+ safe to use even when the lhs, rhs have different sizes. 
    //!param: rhs - object to copy from
    //!param: Sequence& - reference to this after copy
    virtual Sequence& operator=(const Sequence& rhs);

    // forwarded methods

    //: Assign from a valarray
    //!param: const valarray<DataType_>& - valarray to assign from
    //!return: valarray<DataType_>& - reference to this after assignment
    virtual std::valarray<DataType_>& operator=(const std::valarray<DataType_>&);

    //: Assign to every element of *this 
    //!param: const DataType_& - value to assign from
    //!return: std::valarray<DataType_>& - reference to this after assignment
    virtual std::valarray<DataType_>& operator=(const DataType_&);

    //: Assign from a slice of a valarray
    //!param: const slice_array<DataType_>& - slice of valarray to assign from
    //!return: std::valarray<DataType_>& - reference to this after assignment
    virtual std::valarray<DataType_>& operator=(const std::slice_array<DataType_>&);

    //: Assign from a gslice of a valarray
    //!param: const gslice_array<DataType_>& - 
    //+ gslice of valarray to assign from
    //!return: std::valarray<DataType_>& - reference to this after assignment
    virtual std::valarray<DataType_>& operator=(const std::gslice_array<DataType_>&);

    //: Assign from a mask of a valarray
    //!param: const mask_array<DataType_>& - 
    //+ mask of valarray to assign from
    //!return: std::valarray<DataType_>& - reference to this after assignment
    virtual std::valarray<DataType_>& operator=(const std::mask_array<DataType_>&);

    //: Assign from a indirect_array
    //!param: const indirect_array<DataType_>& - indirect_array
    //+ reference to valarray elements to assign from
    //!return: std::valarray<DataType_>& - reference to this after assignment
    virtual std::valarray<DataType_>& operator=(const std::indirect_array<DataType_>&);

    //: Duplicate *this on heap 
    virtual Sequence* Clone() const;

    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const;
    virtual void ConvertToIlwd(const CallChain& Chain,
			       target_type Target,
			       ILwd::LdasContainer* Container) const;

    //: Return the domain of the data
    std::string GetDomain( ) const;
  };	/* Sequence<T> */

  template< class T > inline void Sequence<T>::
  ConvertToIlwd(const CallChain& Chain,
		target_type Target,
		ILwd::LdasContainer* Container) const
  {
    udt::ConvertToIlwd( Chain, Target, Container );
  }
}	/* namespace - datacondAPI */

#endif	/* SEQUENCE_UDT_HH */
