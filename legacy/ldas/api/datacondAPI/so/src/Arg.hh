#ifndef ARG_HH
#define ARG_HH

// $Id: Arg.hh,v 1.1 2002/08/16 01:20:19 Philip.Charlton Exp $

//
// Functions for converting the type (complex to real) of datacon objects
//

namespace datacondAPI {

    // Forward declaration
    class udt;

    //
    //: Return the imaginary part of a UDT
    //
    // Ths function takes a UDT and attempts to convert it to a UDT of the
    // same class with values taken from the arg of the input.
    // The meta-data of the original UDT is preserved.
    //
    //!return: a pointer to a UDT allocated on the heap. The caller is
    //+responsible for deleting the object
    //
    //!exc: invalid_argument - Thrown if there is no method of converting
    //+the UDT
    udt* Arg(const udt& in);
    
} // namespace datacondAPI

#endif // ARG_HH
