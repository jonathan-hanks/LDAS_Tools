#include "datacondAPI/config.h"

#include "general/autoarray.hh"

#include "util.hh"
#include "CallChain.hh"
#include "ActionDriver.hh"
#include "AliasDriver.hh"

namespace datacondAPI
{
  ActionDriver::
  ActionDriver( const AliasDriver& Aliases, CallChain& CC )
    : m_aliases( Aliases ),
      m_call_chain( CC ),
      m_tmp_count( 0 ),
      m_ok( true ),
      m_scanner( new ActionLexer )
  {
    reset( );
  }

  ActionDriver::
  ~ActionDriver( )
  {
  }

  std::string ActionDriver::
  Messages( ) const
  {
    return m_messages.str( );
  }

  bool ActionDriver::
  Parse( std::istream& ActionStream )
  {
    ActionParser	parser( *this );
    m_scanner->yyrestart( &ActionStream );
    bool		retval( parser.parse( ) == 0 );

    return retval;
  }

  ActionDriver::token_type ActionDriver::
  Lex( ActionParser::semantic_type* YYLval,
       ActionParser::location_type* YYLloc )
  {
    m_scanner->SetYYLval( YYLval );
    m_scanner->SetYYLloc( YYLloc );

    token_type  retval = token_type( m_scanner->yylex( ) );

    return retval;
  }

  void ActionDriver::
  add_action( )
  {
    if ( m_func_name.compare( "output" ) == 0 )
    {
      add_intermediate( );
    }
    else
    {
      add_function( );
    }
  }

  void ActionDriver::
  add_function( )
  {
#if 1 // :TODO: Need to get the actual line number
    const int lineno = -1;
#endif /* 0 */
    //---------------------------------------------------------------------
    // Look for context errors
    //---------------------------------------------------------------------

    AliasDriver::alias_type::const_iterator
      alias( m_aliases( ).find( m_output_name ) );
    if ( alias != m_aliases( ).end( ) )
    {
      error( )
	<< "Alias (" << m_output_name << ") on left side of assigment operator"
	<< std::endl;
    }

    //---------------------------------------------------------------------
    // Loop over all parameters and create temporaries for numeric
    //   constants.
    //---------------------------------------------------------------------
    for ( vars_type::iterator
	    cur = m_vars.begin( ),
	    end = m_vars.end( );
	  cur != end;
	  ++cur )
    {
      switch( cur->Type( ) )
      {
      case var_type::INTEGER:
	{
	  std::ostringstream name;
	  const char* params[2];

	  name << "_tmp_" << m_tmp_count++;
	  params[ 0 ] = cur->TextValue( ).c_str( );
	  params[ 1 ] = (char*)NULL;
	  m_call_chain.AppendCallFunction( "integer", params,
					   name.str( ),
					   lineno );
	  cur->SetSymbolName( name.str( ) );
	}
	break;
      case var_type::REAL:
	{
	  std::ostringstream name;
	  const char* params[2];

	  name << "_tmp_" << m_tmp_count++;
	  params[ 0 ] = cur->TextValue( ).c_str( );
	  params[ 1 ] = (char*)NULL;
	  m_call_chain.AppendCallFunction( "double",
					   params,
					   name.str( ),
					   lineno );
	  cur->SetSymbolName( name.str( ) );
	}
	break;
      default:
	// Ignore the rest for this section
	break;
      }
    }
    //---------------------------------------------------------------------
    // Build parameter list for Function Call
    //---------------------------------------------------------------------
    unsigned int vars_size = m_vars.size( );
    General::AutoArray< const char* > params( new const char*[ vars_size + 1 ] );
    unsigned int params_offset = 0;

    params[ vars_size ] = (const char*)NULL;
    for ( vars_type::const_iterator
	    cur = m_vars.begin( ),
	    end = m_vars.end( );
	  cur != end;
	  ++cur )
    {
      params[ params_offset++ ] = cur->GetSymbolName( ).c_str( );
    }  
    //---------------------------------------------------------------------
    // Add function call to CallChain
    //---------------------------------------------------------------------
    m_call_chain.AppendCallFunction( m_func_name.c_str( ),
				     params.get( ),
				     m_output_name.c_str( ),
				     lineno );
  }

  void ActionDriver::
  add_intermediate( )
  {
#if 1 // :TODO: Need to get the actual line number
    const int lineno = -1;
#endif /* 0 */
    if ( m_vars.size( ) != 5 )
    {
      //:TODO: Need to append to error string proper usage of output
      return;
    }

    CallChain::IntermediateResult::primary_type
      primary = CallChain::IntermediateResult::MAYBE_PRIMARY;
  
    vars_type::const_iterator cur = m_vars.begin( );
    std::string variable( cur->GetSymbolName( ) );
    //:TODO: Need to parse format info for compression characteristics
    std::string format( ( (++cur)->GetSymbolName( ) ) );
    //:TODO: Need to add handling of :primary syntax
    std::string protocol( ( (++cur)->GetSymbolName( ) ) );
    std::string name( ( (++cur)->GetSymbolName( ) ) );
    std::string comment( ( (++cur)->GetSymbolName( ) ) );
    std::string compression_method = DEFAULT_COMPRESSION_METHOD;
    int compression_level =  DEFAULT_COMPRESSION_LEVEL;

    //---------------------------------------------------------------------
    // Add intermediate result
    //---------------------------------------------------------------------
    m_call_chain.AppendIntermediateResult( variable.c_str( ), // SymbolName
					   name.c_str( ), // Alias
					   comment.c_str( ), // Comment
					   protocol.c_str( ), // Target
					   primary,
					   compression_method.c_str( ),
					   compression_level,
					   lineno );
  }

  void ActionDriver::
  add_parameter( var_type::data_type Type, const std::string& TextValue )
  {
    m_vars.push_back( var_type( Type, TextValue ) );
  }

  std::ostream& ActionDriver::
  error( )
  {
#if 1 // :TODO: Need to get the actual line number
    const int lineno = -1;
#endif /* 0 */
    m_ok = false;
    m_messages << "ERROR: Line: " << lineno << ": ";
    return m_messages;
  }

  void ActionDriver::
  reset( )
  {
    // Clear the names
    m_func_name = "";
    m_output_name = "";
    // Erase all variables
    m_vars.clear( );
    // Reset the base class
    m_scanner->Reset( );
  }
} // namespace - datacondAPI
