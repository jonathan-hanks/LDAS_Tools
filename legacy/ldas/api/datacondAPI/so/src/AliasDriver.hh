#ifndef DATACONDAPI__ALIAS_DRIVER_HH
#define DATACONDAPI__ALIAS_DRIVER_HH

#include <list>
#include <memory>

#include "general/Memory.hh"
#include "general/unordered_map.hh"

namespace datacondAPI
{
  class AliasDriver;
  class AliasLexer;
}

#include "CallChain.hh"

#include "AliasDriver.hh"
#include "AliasLexer.hh"
#include "AliasParser.hh"

namespace datacondAPI
{
  class AliasDriver
  {
  public:
    typedef General::unordered_map< std::string, std::string > alias_type;
    typedef AliasParser::token_type token_type;

    AliasDriver( );
    ~AliasDriver( );
    void AppendAlias( const std::string& LHS, const std::string& RHS );

    const alias_type& operator()( ) const;

    token_type Lex( datacondAPI::AliasParser::semantic_type* YYLval,
		    datacondAPI::AliasParser::location_type* YYLloc );
    bool Parse( std::istream& Stream );

  private:
    std::unique_ptr< AliasLexer >     m_scanner;

    int		yynerrs;
    int		yychar;
    alias_type	m_alias;
  };

  inline void AliasDriver::
  AppendAlias( const std::string& LHS, const std::string& RHS )
  {
    m_alias[ LHS ] = RHS;
  }

  inline const AliasDriver::alias_type& AliasDriver::
  operator()( ) const
  {
    return m_alias;
  }
} /* namespace - datacondAPI */

#endif /* DATACONDAPI__ACTION_DRIVER_HH */
