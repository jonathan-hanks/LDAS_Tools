#include "config.h"

#include <complex>
#include <sstream>

#include <general/unimplemented_error.hh>

#include "ShiftState.hh"
#include "TimeSeries.hh"

using std::complex;   
   
namespace datacondAPI {

    template<class T>
    ShiftState<T>::ShiftState(const double shift, const size_t order)
        : State(),
          m_shift_whole(static_cast<size_t>(std::floor(shift))),
          m_order(order),
          m_expected_time(),
          m_expected_sample_rate(0),
          m_apply_called(false),
          m_interp(0)
    {
        if (shift < 0)
        {
            throw std::invalid_argument("ShiftState<T>::ShiftState<T>() - "
                                        "shift must be >= 0");
        }

        // Need to break the shift into whole number and fractional part
        const double alpha = shift - m_shift_whole;
        
        // We need an interpolator if alpha > 0
        if (alpha > 0)
        {
            m_interp.reset(new Filters::Interpolate<T>(alpha, m_order));
        }
    }

    template<class T>
    ShiftState<T>::ShiftState(const ShiftState<T>& rhs)
        : State(rhs),
          m_shift_whole(rhs.m_shift_whole),
          m_order(rhs.m_order),
          m_expected_time(rhs.m_expected_time),
          m_expected_sample_rate(rhs.m_expected_sample_rate),
          m_apply_called(rhs.m_apply_called),
          m_interp(0)
    {
        if (rhs.m_interp.get() != 0)
        {
            m_interp.reset(new Filters::Interpolate<T>(*rhs.m_interp));
        }
    }
    
    template<class T>
    const ShiftState<T>&
    ShiftState<T>::operator=(const ShiftState<T>& rhs)
    {
        if (&rhs != this)
        {
            State::operator=(rhs);
            
            m_shift_whole = rhs.m_shift_whole;
            m_order = rhs.m_order;
            m_expected_time = rhs.m_expected_time;
            m_expected_sample_rate = rhs.m_expected_sample_rate;
            m_apply_called = rhs.m_apply_called;

            if (rhs.m_interp.get() != 0)
            {
                m_interp.reset(new Filters::Interpolate<T>(*rhs.m_interp));
            }
        }

        return *this;
    }

    template<class T>
    ShiftState<T>* ShiftState<T>::Clone() const
    {
        return new ShiftState<T>(*this);
    }

    template<class T>
    void ShiftState<T>::apply(Sequence<T>& x)
    {
        //
        // The first time we call apply, we need to remove some samples
        // from the beginning of the sequence, but we need to know
        // if the sequence has enough samples before we call apply.
        //
        // This is the number of samples to remove from beginning
        size_t offset = 0;

        // Easier to branch on whether on not we need to interpolate
        if (m_interp.get() != 0)
        {
            if (!m_apply_called)
            {
                // Check that we have enough data
                offset = m_shift_whole + (getOrder() + 1)/2;
                
                if (x.size() <= offset)
                {
                    std::ostringstream oss;

                    oss << "ShiftState<T>::apply() - "
                        "initial input data must have "
                        "size > floor(shift) + (order + 1)/2 "
                        "(" << offset << ")"
                        << std::ends;

                    throw std::invalid_argument(oss.str());
                }
            }

            m_interp->apply(x);
        }
        else // No interpolation, only whole-number shift
        {
            if (!m_apply_called)
            {
                // Check that we have enough data
                offset = m_shift_whole;
                
                if (x.size() <= offset)
                {
                    std::ostringstream oss;

                    oss << "ShiftState<T>::apply() - "
                        "initial input data must have "
                        "size > floor(shift) "
                        "(" << offset << ")"
                        << std::ends;

                    throw std::invalid_argument(oss.str());
                }
            }
            
            // Don't need to do anything else for whole-number shift
        }

        //
        // The first time we call apply, need to remove some samples
        // from the beginning of the array
        //
        if (!m_apply_called)
        {
            const std::valarray<T> temp
	        = static_cast< const std::valarray<T> >
	        (x)[std::slice(offset, x.size() - offset, 1)];

            x.resize(temp.size());
            x = temp;

            m_apply_called = true;
        }

        // Set the name
        std::ostringstream oss;
        
        oss << "shift("
            << x.name() << ","
            << getShift() << ","
            << getOrder() << ")";

        x.SetName(oss.str());
    }
    
    template<class T>
    void ShiftState<T>::apply(TimeSeries<T>& x)
    {
        double offset = 0;
        
        //
        // The first time through, the start-time of the time-series
        // is just the old start-time plus the shift. On subsequent
        // calls to apply(), we need to account for the delay due to
        // interpolation if needed.
        //
        // Also need to check for continuity of time-series, and valid
        // sample rate.
        //
        if (!m_apply_called)
        {
            // This is the first call to apply(), don't bother checking
            // for continuity
            offset = getShift();
        }
        else // apply() has been called at least once
        {
            // Allow a 2-nanosecond tolerance. Probably this is not enough
            // as errors accumulate
            if (std::abs(x.GetStartTime() -  m_expected_time) > 2.0e-09)
            {
                std::ostringstream oss;
                
                oss << "ShiftState<T>::apply() - "
                    "input time-series is not contiguous with previous "
                    "time-series (expected start-time "
                    << m_expected_time
                    << ", received start-time "
                    << x.GetStartTime() 
                    << ")"
                    << std::ends;
            }

            if (x.GetSampleRate() != m_expected_sample_rate)
            {
                std::ostringstream oss;
                
                oss << "ShiftState<T>::apply() - "
                    "input time-series does not have same sample rate as "
                    "previous time-series (expected rate "
                    << m_expected_sample_rate
                    << ", received rate "
                    << x.GetSampleRate() 
                    << ")"
                    << std::ends;
            }

            if (m_interp.get() != 0)
            {
                offset += (m_interp->getAlpha() - (getOrder() + 1)/2);
            }
        }

        // Apply as a Sequence<T> - also sets Sequence metaddata
        apply(static_cast<Sequence<T>&>(x));

        x.SetStartTime(x.GetStartTime() + offset*x.GetStepSize());

        // Remember the time one sample beyond the end - this is what we
        // expect on the next call to apply()
        m_expected_time = x.GetStartTime() + x.size()*x.GetStepSize();
        m_expected_sample_rate = x.GetSampleRate();
    }
    
    template<class T>
    ILwd::LdasElement*
    ShiftState<T>::ConvertToIlwd(const CallChain& Chain,
                                       target_type Target) const
    {
        throw General::unimplemented_error(
                            "ShiftState<T>::ConvertToIlwd() - unimplemented");
    }

    template class ShiftState<float>;
    template class ShiftState<double>;

    template class ShiftState<complex<float> >;
    template class ShiftState<complex<double> >;
}
