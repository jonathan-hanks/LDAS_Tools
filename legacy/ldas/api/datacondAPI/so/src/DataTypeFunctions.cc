/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: DataTypeFunctions.cc,v 1.15 2006/11/07 22:25:46 emaros Exp $

#include "datacondAPI/config.h"

#include <stdio.h>
#include <complex>
#include <valarray>

#include "DataTypeFunctions.hh"
#include "UDT.hh"
#include "DFTUDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"

namespace 
{
  const char rcsID[] = "@(#) $Id: DataTypeFunctions.cc,v 1.15 2006/11/07 22:25:46 emaros Exp $:";
}

//!ignore_begin:
template<class ReturnType_>
static ReturnType_ cast_param_to(const datacondAPI::udt& Param);

inline int get_integer(const datacondAPI::udt& Param)
{
  return datacondAPI::udt::Cast< datacondAPI::Scalar<int> >(Param).GetValue();
}

inline float get_float(const datacondAPI::udt& Param)
{
   return datacondAPI::udt::Cast< datacondAPI::Scalar<float> >(Param).GetValue();
}

inline double get_double(const datacondAPI::udt& Param)
{
   return datacondAPI::udt::Cast< datacondAPI::Scalar<double> >(Param).GetValue();
}
//!ignore_end:

//=======================================================================
// Helper functions
//=======================================================================

//!ignore_begin:
template<>
double cast_param_to<double>(const datacondAPI::udt& Param)
{
  double	ret(0.0);

  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(Param))
  {
    ret = (double)(get_integer(Param));
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(Param))
  {
    ret = (double)(get_float(Param));
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(Param))
  {
    ret = (double)(get_double(Param));
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(Param).name();
    msg += " to double";
    throw std::runtime_error(msg);
  }
  return ret;
}

template<>
float cast_param_to<float>(const datacondAPI::udt& Param)
{
  float ret(0.0);

  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(Param))
  {
    ret = (float)(get_integer(Param));
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(Param))
  {
    ret = (float)(get_float(Param));
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(Param))
  {
    ret = (float)(get_double(Param));
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(Param).name();
    msg += " to float";
    throw std::runtime_error(msg);
  }
  return ret;
}

template<>
int cast_param_to<int>(const datacondAPI::udt& Param)
{
  int	ret(0);

  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(Param))
  {
    ret = (int)(get_integer(Param));
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(Param))
  {
    ret = (int)(get_float(Param));
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(Param))
  {
    ret = (int)(get_double(Param));
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(Param).name();
    msg += " to int";
    throw std::runtime_error(msg);
  }
  return ret;
}

template<>
size_t cast_param_to<size_t>(const datacondAPI::udt& Param)
{
  size_t	ret(0);

  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(Param))
  {
    ret = (size_t)(get_integer(Param));
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(Param))
  {
    ret = (size_t)get_float(Param);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(Param))
  {
    ret = (size_t)get_double(Param);
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(Param).name();
    msg += " to size_t";
    throw std::runtime_error(msg);
  }
  return ret;
}
//!ignore_end:

//-----------------------------------------------------------------------
// Instantiation of helper functions
//-----------------------------------------------------------------------

// template int cast_param_to<int>(const datacondAPI::udt& Param);

//=======================================================================
//  Complex function
//=======================================================================

template<class DataType_>
ComplexFunction<DataType_>::
ComplexFunction(void)
  : Function( ComplexFunction::GetName() )
{
}


// The evaluation function takes one or two parameters. The first parameter
// is always the real componenent of the complex number. The optional second
// parameter is the imaginary component. If not supplied, it defaults to
// zero.
//
//!exc: CallChain::BadArgumentCount - thrown when the number of parameters
//+     passed is not equal to one.
//
template<class DataType_>
void ComplexFunction<DataType_>::
Eval(CallChain* Chain,
     const CallChain::Params& Params,
     const std::string& Ret) const
{
  DataType_	arg1(0);
  DataType_	arg2(0);

  switch(Params.size())
  {
  case 2:
      try
      {
	datacondAPI::udt*	symbol(dynamic_cast<CallChain::Symbol*>
				       (Chain->GetSymbol(Params[1])));

	arg2 = cast_param_to<DataType_>(*symbol);
      }
      catch(const CallChain::BadSymbol& e)
      {
	double tmp;
	sscanf(Params[1].c_str(), "%lg", &tmp);
	arg2 = (DataType_)tmp;
      }
    // FALL THROUGH
  case 1:
      try
      {
	datacondAPI::udt*	symbol(dynamic_cast<CallChain::Symbol*>
				       (Chain->GetSymbol(Params[0])));
	arg1 = cast_param_to<DataType_>(*symbol);
      }
      catch(const CallChain::BadSymbol& e)
      {
	double tmp;
	sscanf(Params[0].c_str(), "%lg", &tmp);
	arg1 = (DataType_)tmp;
      }
    break;
  default:
    throw CallChain::BadArgumentCount(GetName(), 2, Params.size());
    break;
  }
  std::complex<DataType_>	value(arg1,arg2);

  Chain->ResetOrAddSymbol( Ret,
			   new datacondAPI::Scalar<std::complex<DataType_> >
			   (value));
}

template<>
const std::string& ComplexFunction<double>::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("dcomplex");

  return name;
}

template<>
const std::string& ComplexFunction<float>::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("scomplex");

  return name;
}

static ComplexFunction<float> _ComplexFunctionFloat;
static ComplexFunction<double> _ComplexFunctionDouble;

//=======================================================================
//                  D F T   F U N C T I O N
//=======================================================================

template<class DataType_>
DFTFunction<DataType_>::
DFTFunction(void)
  : Function( DFTFunction::GetName() )
{
}


// The evaluation function takes two parameters. The first parameter
// is the base for the DFT. The second is the length of the DFT to be
// created.
//
//!exc: CallChain::BadArgumentCount - thrown when the number of parameters
//+     passed is not equal to one.
//
template<class DataType_>
void DFTFunction<DataType_>::
Eval(CallChain* Chain,
     const CallChain::Params& Params,
     const std::string& Ret) const
{
  datacondAPI::DFT<DataType_>*	out;
  out = new datacondAPI::DFT<DataType_>();

  switch(Params.size())
  {
  case 2:
    {
      DataType_	base(get_base(Chain->GetSymbol(Params[0])));
      size_t		len(get_len(Chain->GetSymbol(Params[1])));

      out->resize(len, base);
    }
    break;
  default:
    throw CallChain::BadArgumentCount(GetName(), 2, Params.size());
    break;
  }
  Chain->ResetOrAddSymbol( Ret, out );
}

template<>
const std::string& DFTFunction< std::complex<double> >::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("dfvalarray");

  return name;
}

template<>
const std::string& DFTFunction< std::complex<float> >::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("sfvalarray");

  return name;
}

template<class DataType_>
DataType_ DFTFunction<DataType_>::
get_base(const CallChain::Symbol* Symbol) const
{
  return cast_param_to<DataType_>(*Symbol);
}
  
template<>
std::complex<float> DFTFunction<std::complex<float> >::
get_base(const CallChain::Symbol* Symbol) const
{
  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(*Symbol))
  {
    return std::complex<float>(datacondAPI::udt::Cast< datacondAPI::Scalar<int> >(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(*Symbol))
  {
    return std::complex<float>(get_float(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(*Symbol))
  {
    return std::complex<float>(get_double(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<std::complex<float> > >(*Symbol))
  {
      return datacondAPI::udt::Cast< datacondAPI::Scalar<std::complex<float> > >(*Symbol);
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(*Symbol).name();
    msg += " to complex<double>";
    throw std::runtime_error(msg);
  }
}
  
template<>
std::complex<double> DFTFunction<std::complex<double> >::
get_base(const CallChain::Symbol* Symbol) const
{
  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(*Symbol))
  {
    return std::complex<double>(datacondAPI::udt::Cast< datacondAPI::Scalar<int> >(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(*Symbol))
  {
    return std::complex<double>(get_float(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(*Symbol))
  {
    return std::complex<double>(get_double(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<std::complex<float> > >(*Symbol))
  {
    std::complex<float>	c(datacondAPI::udt::Cast< datacondAPI::Scalar<std::complex<float> > >(*Symbol));

    return std::complex<double>(c.real(), c.imag());
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<std::complex<double> > >(*Symbol))
  {
    return datacondAPI::udt::Cast< datacondAPI::Scalar<std::complex<double> > >(*Symbol);
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(*Symbol).name();
    msg += " to complex<double>";
    throw std::runtime_error(msg);
  }
}
  
template<class DataType_>
size_t DFTFunction<DataType_>::
get_len(const CallChain::Symbol* Symbol) const
{
  return cast_param_to<size_t>(*Symbol);
}

static DFTFunction< std::complex<float> > _DFTFunctionComplexFloat;
static DFTFunction< std::complex<double> > _DFTFunctionComplexDouble;

//=======================================================================
//             I N T E G E R   F U N C T I O N
//=======================================================================

IntegerFunction::
IntegerFunction(void)
  : Function( IntegerFunction::GetName() )
{
}

// The evaluation function takes a single parameter and converts it to
// an integer. The parameter may either be a numeric constant or a variable.
//
//!exc: CallChain::BadArgumentCount - thrown when the number of parameters
//+     passed is not equal to one.
//
void IntegerFunction::
Eval(CallChain* Chain,
     const CallChain::Params& Params,
     const std::string& Ret) const
{
  int	value = 0;

  switch(Params.size())
  {
  case 1:
    {
      try {
	CallChain::Symbol*	c(dynamic_cast<CallChain::Symbol*>
				  (Chain->GetSymbol(Params[0])));
	if (c)
	{
	  value = cast_param_to<int>(*c);
	}
	else
	{
	  throw (CallChain::BadSymbol(""));
	}
      }
      catch(const CallChain::BadSymbol& e)
      {
	double	tmp;
	sscanf(Params[0].c_str(), "%lg", &tmp);
	value = (int)tmp;
      }
      break;
    }
  default:
    throw CallChain::BadArgumentCount(GetName(), 1, Params.size());
    break;
  }
  Chain->ResetOrAddSymbol( Ret,
			   new datacondAPI::Scalar<int>(value) );
}

// The name of this action in the CallChain is "integer"
const std::string& IntegerFunction::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("integer");

  return name;
}

static IntegerFunction _IntegerFunction;

//=======================================================================
//  Sequence function
//=======================================================================

template<class DataType_>
SequenceFunction<DataType_>::
SequenceFunction(void)
  : Function( SequenceFunction::GetName() )
{
}

// The evaluation function takes one or two parameters. The first parameter
// is always the real componenent of the complex number. The optional second
// parameter is the imaginary component. If not supplied, it defaults to
// zero.
//
//!exc: CallChain::BadArgumentCount - thrown when the number of parameters
//+     passed is not equal to one.
//
template<class DataType_>
void SequenceFunction<DataType_>::
Eval(CallChain* Chain,
     const CallChain::Params& Params,
     const std::string& Ret) const
{
  datacondAPI::Sequence<DataType_>*	sequence;
  sequence = new datacondAPI::Sequence<DataType_>();

  switch(Params.size())
  {
  case 2:
    {
      DataType_	base(get_base(Chain->GetSymbol(Params[0])));
      size_t		len(get_len(Chain->GetSymbol(Params[1])));

      sequence->resize(len, base);
    }
    break;
  default:
    throw CallChain::BadArgumentCount(GetName(), 2, Params.size());
    break;
  }
  Chain->ResetOrAddSymbol( Ret, sequence );
}

template<>
const std::string& SequenceFunction<std::complex<double> >::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("zvalarray");

  return name;
}

template<>
const std::string& SequenceFunction<std::complex<float> >::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("cvalarray");

  return name;
}

template<>
const std::string& SequenceFunction<double>::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("dvalarray");

  return name;
}

template<>
const std::string& SequenceFunction<float>::
GetName(void) const
{
  //---------------------------------------------------------------------------
  // We set a static standard string to the name of the action, and return a
  // refernce to it. This is used by the constructor to register the action
  // name.
  //---------------------------------------------------------------------------

  static std::string name("svalarray");

  return name;
}

template<class DataType_>
DataType_ SequenceFunction<DataType_>::
get_base(const CallChain::Symbol* Symbol) const
{
  return cast_param_to<DataType_>(*Symbol);
}
  
template<>
std::complex<float> SequenceFunction<std::complex<float> >::
get_base(const CallChain::Symbol* Symbol) const
{
  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(*Symbol))
  {
    return std::complex<float>(datacondAPI::udt::Cast< datacondAPI::Scalar<int> >(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(*Symbol))
  {
    return std::complex<float>(get_float(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(*Symbol))
  {
    return std::complex<float>(get_double(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<std::complex<float> > >(*Symbol))
  {
      return datacondAPI::udt::Cast< datacondAPI::Scalar<std::complex<float> > >(*Symbol);
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(*Symbol).name();
    msg += " to complex<double>";
    throw std::runtime_error(msg);
  }
}
  
template<>
std::complex<double> SequenceFunction<std::complex<double> >::
get_base(const CallChain::Symbol* Symbol) const
{
  if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(*Symbol))
  {
    return std::complex<double>(datacondAPI::udt::Cast< datacondAPI::Scalar<int> >(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(*Symbol))
  {
    return std::complex<double>(get_float(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(*Symbol))
  {
    return std::complex<double>(get_double(*Symbol), 0);
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<std::complex<float> > >(*Symbol))
  {
    std::complex<float>	c(datacondAPI::udt::Cast< datacondAPI::Scalar<std::complex<float> > >(*Symbol));

    return std::complex<double>(c.real(), c.imag());
  }
  else if (datacondAPI::udt::IsA< datacondAPI::Scalar<std::complex<double> > >(*Symbol))
  {
    return datacondAPI::udt::Cast< datacondAPI::Scalar<std::complex<double> > >(*Symbol);
  }
  else
  {
    std::string msg("Unable to convert symbol of type ");
    msg += typeid(*Symbol).name();
    msg += " to complex<double>";
    throw std::runtime_error(msg);
  }
}
  
template<class DataType_>
size_t SequenceFunction<DataType_>::
get_len(const CallChain::Symbol* Symbol) const
{
  return cast_param_to<size_t>(*Symbol);
}

static SequenceFunction<std::complex<float> > _SequenceFunctionComplexFloat;
static SequenceFunction<std::complex<double> > _SequenceFunctionComplexDouble;
static SequenceFunction<float> _SequenceFunctionFloat;
static SequenceFunction<double> _SequenceFunctionDouble;

