/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "config.h"

#include <memory>

#include "general/Memory.hh"

#include "WhenUMD.hh"

#include "general/types.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarray.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrProcData.hh"

namespace datacondAPI
{

  using General::GPSTime;

  WhenMetaData::WhenMetaData()
    : m_start_time()
  {
  }

  WhenMetaData::WhenMetaData(const GPSTime& start_time)
    : m_start_time(start_time)
  {
  }

  const WhenMetaData& WhenMetaData::operator=(const WhenMetaData& rhs)
  {
    if (&rhs != this)
    {
      m_start_time = rhs.m_start_time;
    }

    return *this;
  }

  GPSTime WhenMetaData::GetStartTime() const
  {
    return m_start_time;
  }

  void WhenMetaData::SetStartTime(const GPSTime& start_time)
  {
    m_start_time = start_time;
  }

  WhenMetaData::~WhenMetaData()
  {
  }

  ILwd::LdasElement* WhenMetaData::
  ConvertToIlwd(const CallChain& Chain,
                udt::target_type Target) const
  {
    std::unique_ptr<ILwd::LdasContainer> container( new ILwd::LdasContainer );
    ConvertToIlwd(Chain, Target, *container);
    return container.release( );
  }

  void WhenMetaData::
  ConvertToIlwd( const CallChain& Chain,
		 udt::target_type Target,
		 ILwd::LdasContainer& Container) const
  {
    switch ( Target )
    {
    case datacondAPI::udt::TARGET_GENERIC:
    case datacondAPI::udt::TARGET_WRAPPER:
      {
        static const char* names[][2] = {
          {"GPSStartS", "gps_sec:start_time"},
          {"GPSStartNS", "gps_nan:start_time"},
          {"GPSEndS", "gps_sec:stop_time"},
          {"GPSEndNS", "gps_nan:stop_time"},
        };

        const unsigned int data[] = {m_start_time.GetSeconds(),
                                     m_start_time.GetNanoseconds(),
                                     GetEndTime().GetSeconds(),
                                     GetEndTime().GetNanoseconds()};
        std::string units[] = {"sec", "nanosec", "sec", "nanosec"};
        
        int offset = 0;
        if (Target == datacondAPI::udt::TARGET_WRAPPER)
        {
          offset = 1;
        }

        for (unsigned int row = 0; row < sizeof(data)/sizeof(*data); row++)
        {
          Container.push_back( new ILwd::LdasArray<INT_4U>
			       ((INT_4U)(data[row]),
				names[row][offset],
				units[row]),
			       ILwd::LdasContainer::NO_ALLOCATE,
			       ILwd::LdasContainer::OWN );
        }
        break;
      }
    default:
      throw datacondAPI::udt::
        BadTargetConversion(Target,
                            "datacondAPI::WhenMetaData");
    }
  }

  void WhenMetaData::
  Store( ILwdFCS::FrProcData& Storage ) const
  {
    Storage.SetStartTime( GetStartTime() );
    Storage.SetStopTime( GetEndTime() );
  }

  void WhenMetaData::
  Store( ILwdFCS::FrameH& Storage ) const
  {
    Storage.SetStartTime( GetStartTime() );
    Storage.SetDeltaTime( GetEndTime() - GetStartTime() );
  }

  void WhenMetaData::
  store( ILwd::LdasContainer& Storage,
	 udt::target_type Target ) const
  {
    int naming_convention;

    switch( Target )
    {
    case udt::TARGET_GENERIC:
      naming_convention = 0;
      break;
    case udt::TARGET_WRAPPER:
      naming_convention = 1;
      break;
    default:
      naming_convention = -1;
      break;
    }
    if ( naming_convention >= 0 )
    {
      static const char* names[][2] = {
	{"GPSStartS", "gps_sec:start_time"},
	{"GPSStartNS", "gps_nan:start_time"},
	{"GPSEndS", "gps_sec:stop_time"},
	{"GPSEndNS", "gps_nan:stop_time"},
      };

      static const char* units[] = {"sec", "nanosec", "sec", "nanosec"};

      const unsigned int data[] = {m_start_time.GetSeconds(),
				   m_start_time.GetNanoseconds(),
				   GetEndTime().GetSeconds(),
				   GetEndTime().GetNanoseconds()};
      for (unsigned int i = 0; i < sizeof(data)/sizeof(*data); i++)
      {
	Storage.push_back( new ILwd::LdasArray<INT_4U>
			   ((INT_4U)(data[i]),
			    names[i][naming_convention],
			    units[i]),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
      }
    }
  }

  ArbitraryWhenMetaData::ArbitraryWhenMetaData()
    : WhenMetaData(), m_end_time()
  {
  }

  ArbitraryWhenMetaData::ArbitraryWhenMetaData(const GPSTime& start_time,
                                               const GPSTime& end_time)
    : WhenMetaData(start_time), m_end_time(end_time)
  {
  }

  const ArbitraryWhenMetaData&
  ArbitraryWhenMetaData::operator=(const WhenMetaData& rhs)
  {
    if (&rhs != this)
    {
      WhenMetaData::operator=(rhs);
      m_end_time = rhs.GetEndTime();
    }

    return *this;
  }

  GPSTime ArbitraryWhenMetaData::GetEndTime() const
  {
    return m_end_time;
  }

  void ArbitraryWhenMetaData::SetEndTime(const GPSTime& end_time)
  {
    m_end_time = end_time;
  }


  void ArbitraryWhenMetaData::
  Store( ILwdFCS::FrProcData& Storage ) const
  {
    WhenMetaData::Store( Storage );
  }

  void ArbitraryWhenMetaData::
  Store( ILwdFCS::FrameH& Storage ) const
  {
    WhenMetaData::Store( Storage );
  }

} // namespace datacondAPI
