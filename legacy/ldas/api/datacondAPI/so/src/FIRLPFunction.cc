// $Id: FIRLPFunction.cc,v 1.4 2005/12/01 22:54:58 emaros Exp $
//
// "firlp" action for constructing a FIR low-pass filter
//
// firlp has 3 input arguments and one return value. The format is
//
//     b = firlp(fc, order[, window ] )
//
// b - returned filter coefficients
// fc - cut-off frequency as a fraction of Nyquist
// window - window to use when constructing filter. Default is a Kaiser window
//          with beta = 5.0, to match the default Resample filter
//

#include "datacondAPI/config.h"

#include <memory>   
   
#include "general/Memory.hh"

#include <filters/FIRLP.hh>
#include <filters/KaiserWindow.hh>

#include "FIRLPFunction.hh"
#include "WindowUDT.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"

using namespace std;   
   
namespace {

    FIRLPFunction staticFIRLPFunction;

}

FIRLPFunction::FIRLPFunction()
    : Function(FIRLPFunction::GetName())
{
}

const std::string& FIRLPFunction::GetName() const
{
    static const std::string name("firlp");

    return name;
}

void FIRLPFunction::Eval(CallChain* chain,
                         const CallChain::Params& params,
                         const std::string& ret) const
{
    using namespace datacondAPI;

    // Create a placeholder for the result
    unique_ptr<Sequence<double> > b(new Sequence<double>());

    const size_t num_params = params.size();

    if (num_params == 2 || num_params == 3)
    {
        // Get the fc and order
        
        const udt* const fcUDT = chain->GetSymbol(params[0]);
        const udt* const orderUDT = chain->GetSymbol(params[1]);

        double fc = 0;
        int order = 0;

        if (const Scalar<double>* const p
                = dynamic_cast<const Scalar<double>*>(fcUDT))
        {
            fc = p->GetValue();
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         1,
                                TypeInfoTable.GetName(typeid(*fcUDT)),
                                TypeInfoTable.GetName(typeid(Scalar<double>)));
        }

        if (const Scalar<int>* const p
                = dynamic_cast<const Scalar<int>*>(orderUDT))
        {
            order = p->GetValue();
        }
        else
        {
            throw CallChain::BadArgument(GetName(),
                                         2,
                                TypeInfoTable.GetName(typeid(*orderUDT)),
                                TypeInfoTable.GetName(typeid(Scalar<int>)));
        }

        // In case a window is specified
        if (num_params == 3)
        {
            const udt* const wUDT = chain->GetSymbol(params[2]);
            
            if (const WindowUDT* const p
                    = dynamic_cast<const WindowUDT*>(wUDT))
            {
                // Now we can finally construct FIRLP
                Filters::FIRLP firlp(fc, order, p->getWindow());

                firlp.apply(*b);
            }
            else
            {
                throw CallChain::BadArgument(GetName(),
                                             1,
                                TypeInfoTable.GetName(typeid(*wUDT)),
                                TypeInfoTable.GetName(typeid(WindowUDT)));
            }

        }
        else // num_params == 2 (use default window)
        {
            const double beta = 5.0;
            Filters::FIRLP firlp(fc, order, Filters::KaiserWindow(beta));
            
            firlp.apply(*b);
        }

    }
    else
    {
        throw CallChain::BadArgumentCount(GetName(), 2, params.size());
    }

    chain->ResetOrAddSymbol(ret, b.release());
}
