#ifndef _SINEFUNCTION_HH_
#define _SINEFUNCTION_HH_

#include "CallChain.hh"

class sineFunction : public CallChain::Function
{
public:
  sineFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //_SINEFUNCTION_HH_
