/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef GETSAMPLERATEFUNCTION_HH
#define GETSAMPLERATEFUNCTION_HH

// $Id: getSampleRateFunction.hh,v 1.1 2002/11/30 01:14:05 Philip.Charlton Exp $

#include "CallChain.hh"

//
//: Function support for getSampleRate action
//
// srate = getSampleRate(ts);
//
class getSampleRateFunction : public CallChain::Function
{

public:

    getSampleRateFunction();

    virtual void Eval(CallChain* chain,
                      const CallChain::Params& params,
                      const std::string& ret) const;

    virtual const std::string& GetName() const;

};

#endif // GETSAMPLERATEFUNCTION_HH
