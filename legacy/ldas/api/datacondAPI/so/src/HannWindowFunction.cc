// $Id: HannWindowFunction.cc,v 1.5 2005/12/01 22:54:58 emaros Exp $
//
// HannWindow action
//
// HannWindow has no input arguments and one return value. The format is
//
//     window = HannWindow();
//
// window is a Window
//

#include "datacondAPI/config.h"

#include "HannWindowFunction.hh"
#include "WindowUDT.hh"
#include "ScalarUDT.hh"

static HannWindowFunction _HannWindowFunction;

HannWindowFunction::HannWindowFunction()
    : Function( HannWindowFunction::GetName() )
{
}

const std::string& HannWindowFunction::GetName(void) const
{
    static std::string name("HannWindow");

    return name;
}

void HannWindowFunction::Eval(CallChain* Chain,
			      const CallChain::Params& Params,
			      const std::string& Ret) const
{
    switch(Params.size())
    {
    case 0:
	// Nothing to do here
        break;

    default:
        throw CallChain::BadArgumentCount("HannWindow", 0, Params.size());
    }

    CallChain::Symbol* window = new datacondAPI::HannWindowUDT();

    Chain->ResetOrAddSymbol( Ret, window );
}
