#ifndef CORRELATE_HH
#define CORRELATE_HH

//!ppp: {eval `cat $ENV{SRCDIR}/cc_rules.pl`}

#include <stdexcept>

// Forward declarations
namespace datacondAPI {
    class udt;
    template<class T> class Sequence;
}

namespace datacondAPI
{

    //
    //: A class for estimating correlation sequences
    //
    class Correlate {

    public:
	    
	//
	//: Default constructor
	//
	Correlate();

	//
	//: Copy constructor
	//
	//!param: r - Correlate object that is copied
	//
	Correlate(const Correlate& r);

	//
	//: Destructor
	//
	virtual 
	~Correlate();

	//
	//: Copy assignment
	//
	//!param: r - Correlate object that is copied
	//
	//!return: const Correlate&
	//
	const Correlate& 
	operator=(const Correlate& r);

	//
	//: Apply method for estimating cross-correlation sequences
	//
	//!param: out - pointer to udt output
	//!param: xIn - input data: x
	//!param: yIn - input data: y
	//
	//!exc: std::invalid_argument - thrown if the input udts are not ...
	//
	void 
	apply(udt*& out, 
	      const udt& xIn, 
	      const udt& yIn)
	    throw (std::invalid_argument);

	//
	//: Apply method for estimating auto-correlation sequences
	//
	//!param: out - pointer to udt output
	//!param: in - input data: x
	//
	//!exc: std::invalid_argument - thrown if the input udt is not ...
	//
	void 
	apply(udt*& out, 
	      const udt& in)
	    throw (std::invalid_argument);

    private:
	//
	//: Private apply method for estimating the cross-correlation
	//+ sequences 
	//
	//!param: out - output cross-correlation estimate
	//!param: xIn - input data: x
	//!param: yIn - input data: y
	//
	//!exc: std::invalid_argument - thrown if 
	// 
	template<class T>
	void 
	apply(Sequence<T>& out, 
       	      const Sequence<T>& xIn, 
	      const Sequence<T>& yIn)
	    throw (std::invalid_argument);

	//
	//: Private apply method for estimating the auto-correlation 
	//+ sequences
	//
	//!param: out - output auto-correlation estimate
	//!param: in - input data: x
	//
	//!exc: std::invalid_argument - thrown if ... 
	//
	template<class T>
	void 
	apply(Sequence<T>& out, 
	      const Sequence<T>& in)
	    throw (std::invalid_argument);

	//
	//: Dispatch method for cross-correlation estimation
	//
	// Private method for checking (or constructing) the proper udt
	// output type for the cross-correlation apply method.
	//
	//!param: out - pointer to udt output
	//!param: xIn - input data: x
	//!param: yIn - input data: y
	//
	//!exc: std::invalid_argument - thrown if the output udt is not ...
	//
	template<class T>
	void
	dispatch(udt*& out, 
	         const Sequence<T>& xIn, 
		 const Sequence<T>& yIn)
	    throw (std::invalid_argument);
	
	//
	//: Dispatch method for auto-correlation estimation 
	//
	// Private method for checking (or constructing) the proper udt
	// output type for the auto-correlation apply method.
	//
	//!param: out - pointer to udt output
	//!param: in - input data: x
	//
	//!exc: std::invalid_argument - thrown if the output udt is not ...
	//
	template<class T>
	void
	dispatch(udt*& out, 
		 const Sequence<T>& in)
	    throw (std::invalid_argument);
	
    };

} // end of datacondAPI namespace 

#endif // CORRELATE_HH
