#include "datacondAPI/config.h"

#include <sstream>
   
#include "PrecisionConversion.hh"
#include "ScalarUDT.hh"

#include "DoubleFunction.hh"

using namespace std;   
   
//
// DoubleFunction class
//
   
namespace {

    const DoubleFunction staticDoubleFunction;

}

DoubleFunction::DoubleFunction()
  : CallChain::Function(DoubleFunction::GetName())
{
}

const std::string& DoubleFunction::GetName() const
{
  static const std::string name("double");

  return name;
}

void DoubleFunction::Eval(CallChain* Chain,
                          const CallChain::Params& Params,
                          const std::string& Ret) const
{
  if (Params.size() != 1)
  {
    throw CallChain::BadArgumentCount(GetName(), 1, Params.size());
  }

  //
  // Existence-testing for symbol is turned off in case the argument
  // is a literal number
  datacondAPI::udt* in = Chain->GetSymbol(Params[0], false);
  datacondAPI::udt* out = 0;
    
  if (in == 0)
  {
    // The symbol doesn't exist - it might be a literal number
    // so attempt to convert it to a double
    istringstream istr(Params[0]);
    double value = 0;
    double next_value = 0;
 
    // Need to check that there's only one number to read in the
    // stream, so read the next one as well - should give 0
    // Note that comparing a stream to zero tells you whether
    // the stream is good or bad.
    if (((istr >> value) != 0) && ((istr >> next_value) == 0))
    {
      out = new datacondAPI::Scalar<double>(value);
    }
    else
    {
      throw CallChain::BadSymbol(Params[0]);
    }
  }
  else // the symbol exists - convert it
  {
      out = datacondAPI::convert_to_double(*in);
  }

  Chain->ResetOrAddSymbol(Ret, out);
}
