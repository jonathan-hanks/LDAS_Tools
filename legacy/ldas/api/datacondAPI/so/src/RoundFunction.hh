/* -*- mode: c++ -*- */
#ifndef ROUND_FUNCTION_HH
#define ROUND_FUNCTION_HH

#include "CallChain.hh"
#include "UDT.hh"

class RoundFunction: public CallChain::Function
{
public:

  //: Default constructor - construction of dummy instance registers the Eval
  //+ method as the handler for calls to the action named by the GetName method
  RoundFunction();

  //: Evaluate an action call
  //!param: Chain - environment of the action call
  //!param: Params - container of parameter names
  //!param: Ret - return value name
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;

  //: Get the name of the handled action
  //!return: The name of the handled action
  virtual const std::string& GetName(void) const;
};

#ifdef UNARY_FUNCTION_CC

#define	OPERATION "round"
#define UnaryClass RoundFunction

namespace {
  template< class T >
  inline T
  action( const T& Source )
  {
    return T( std::floor( Source + 0.5 ) );
  }
}

#endif	/* UNARY_FUNCTION_CC */

#endif	/* ROUND_FUNCTION_HH */
