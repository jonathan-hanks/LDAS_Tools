#ifndef WINDOWUDT_HH
#define WINDOWUDT_HH

#include <filters/HannWindow.hh>
#include <filters/KaiserWindow.hh>
#include <filters/RectangularWindow.hh>

#include "WindowInfo.hh"
#include "UDT.hh"

namespace datacondAPI {

    template<class T> class Sequence;

    //: Wrapper class to provide a UDT implementation of Filters::Window
    class WindowUDT : public udt {
    public:
        
        //: Destructor
        virtual ~WindowUDT();

        //: Return window length
        //
        //!return: size_t - length of window
        size_t size() const;

        //: Return mean value of window
        //
        //!return: double - mean value of window elements
        double mean() const;
        
        //: Return RMS value of window
        //
        //!return: double - root mean square value of window elements
        double rms() const;

        //: Return window info
        //
        //!return: WindowInfo - window info
        WindowInfo what() const;

        //: Change the size of the window
        //
        //!param: n - desired length of window
        //
        //!exc: std::length_error - thrown if desired window length is 
        //+ greater than maximum allowed value 
        //
        //!exc: std::bad_alloc - thrown if not able to allocate memory for
        //+ the window 
        void resize(const size_t n);
      
        //: Apply a window to the data in-place
        //
        //!param: x  - input/output sequence
        //
        //!exc: std::length_error - thrown if desired window length is 
        //+ greater than maximum allowed value 
        //
        //!exc: std::bad_alloc - thrown if not able to allocate memory for 
        //+ window
        template<class T>
        void apply(Sequence<T>& x); 

        //: Apply a window to the data out-of-place
        //
        //!param: out - windowed sequence
        //!param: in  - input sequence
        //
        //!exc: std::length_error - thrown if desired window length is 
        //+ greater than maximum allowed value 
        //
        //!exc: std::bad_alloc - thrown if not able to allocate memory for 
        //+ window
        template<class TOut, class TIn>
        void apply(Sequence<TOut>& out, const Sequence<TIn>& in);

        //: Synonym for apply
        //
        //!param: out - windowed sequence
        //!param: in  - input sequence
        //
        //!exc: std::length_error - thrown if desired window length is 
        //+ greater than maximum allowed value 
        //
        //!exc: std::bad_alloc - thrown if not able to allocate memory for 
        //+ window
        template<class TOut, class TIn>
        void operator()(Sequence<TOut>& out, const Sequence<TIn>& in);

        //: Clone a window
        //
        //!return: WindowUDT* - copy of current window
        virtual WindowUDT* Clone() const = 0;

        //
        //: Convert the window to an ilwd
        //
        // NOTE: This function is NOT currently implemented.
        //   
        virtual
        ILwd::LdasElement* ConvertToIlwd(const CallChain& Chain,
                             udt::target_type Target = udt::TARGET_GENERIC) const;

        //: Pure virtual function to obtain the underlying Filters::Window.
        //+Must be defined in derived classes
        virtual const Filters::Window& getWindow() const = 0;

    private:
        //: Non-const version of getWindow() is private
        virtual Filters::Window& getWindow() = 0;
        
    };

    //: UDT Hanning window
    class HannWindowUDT : public WindowUDT {
    public:
        //: Clone a window
        //
        //!return: HannWindowUDT* - copy of current window
        virtual HannWindowUDT* Clone() const;

        //: Virtual function to return the underlying Filters::Window.
        //+Must be defined in derived classes
        virtual const Filters::HannWindow& getWindow() const;
        
    private:
        //: Non-const version of getWindow() is private
        virtual Filters::HannWindow& getWindow();
        
        Filters::HannWindow m_window;
    };

    //: UDT Kaiser window
    class KaiserWindowUDT : public WindowUDT {
    public:
        
        //: Default constructor, uses beta defined in FilterConstants.hh
        KaiserWindowUDT();

        //: Construct Kaiser window given beta parameter
        //
        //!param: beta - Kaiser window parameter
        explicit
        KaiserWindowUDT(const double beta);

        //: Get the beta parameter
        double beta() const;

        //: Set the beta parameter
        void set_beta(const double& beta);

        //: Clone a window
        //
        //!return: KaiserWindowUDT* - copy of current window
        virtual KaiserWindowUDT* Clone() const;

        //: Virtual function to return the underlying Filters::Window.
        //+Must be defined in derived classes
        virtual const Filters::KaiserWindow& getWindow() const;
        
    private:
        //: Non-const version of getWindow() is private
        virtual Filters::KaiserWindow& getWindow();
        
        Filters::KaiserWindow m_window;
    };

    //: UDT rectangular window
    class RectangularWindowUDT : public WindowUDT {
    public:
        //: Clone a window
        //
        //!return: RectangularWindowUDT* - copy of current window
        virtual RectangularWindowUDT* Clone() const;

        //: Virtual function to return the underlying Filters::Window.
        //+Must be defined in derived classes
        virtual const Filters::RectangularWindow& getWindow() const;
        
    private:
        //: Non-const version of getWindow() is private
        virtual Filters::RectangularWindow& getWindow();
        
        Filters::RectangularWindow m_window;
    };


} // namespace datacondAPI

#endif // WINDOWUDT_HH
