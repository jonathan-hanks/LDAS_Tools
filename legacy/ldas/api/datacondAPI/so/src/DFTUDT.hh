/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef DFT_UDT_HH
#define DFT_UDT_HH
// $Id: DFTUDT.hh,v 1.10 2006/02/16 16:54:58 emaros Exp $
#include "SequenceUDT.hh"

namespace datacondAPI
{
  
  //: UDT Representation of a DFT
  template<class DataType_>
  class DFT : public Sequence<DataType_>
  {
    
  public:
    
    // constructors
    
    //: Default constructor
    DFT();
    
    //: Copy constructor
    //!param: value - object to copy from 
    DFT(const DFT& value);

    //: Construct DFT of specific dimension
    //!param: n - number of elements to pre-allocated using DataType_ 
    //+ default constructor
    explicit DFT(unsigned int n);

    //: Construct from valarray
    //!param: value - data to convert to DFT object
    explicit DFT(const std::valarray<DataType_>& value);

    //: Construct from Sequence
    //!param: value - data to convert to DFT object
    explicit DFT(const Sequence<DataType_>& value);
    
    //: Construct from array
    //!param: data - pointer to size elements of consecutive storage of 
    //+ type ArrayData_ objects, which can be implicitly converted to 
    //+ DataType_ objects
    //!param: size - size of DFT
    DFT(const DataType_* const data, const size_t n);
    
    //: destructor
    virtual ~DFT();
    
    // UDT required members
    
    //: Duplicate *this on heap
    //!return: DFT* - duplicate of *this, allocated on heap
    virtual DFT* Clone() const;

    //: Convert *this to an appropriate ilwd type
    //!param: Chain - CallChain *this belongs to
    //!param: Target - intended destination of this ILWD
    //!return: ILwd::LdasElement* - ILWD representation of *this
    virtual ILwd::LdasElement*
    ConvertToIlwd( const CallChain& Chain,
		   datacondAPI::udt::target_type Target) const;

    // metadata methods
    
    //: Check if this is the dft of a real sequence
    //!return: bool - true if dft of a real sequence
    bool IsSymmetric() const;

    //: Enforce symmetry associated with dft of a real sequence:
    //+ the positive frequency is set equal to the average of the
    //+ current positive frequency and the conjugate of the corresponding 
    //+ negative frequency, and the negative frequency is set equal to the 
    //+ conjugate of the positive frequency.
    void SetSymmetric();
    
  }; // class DFT<DataType_>
  
} // namespace datacondAPI

#endif // DFT_UDT_HH

