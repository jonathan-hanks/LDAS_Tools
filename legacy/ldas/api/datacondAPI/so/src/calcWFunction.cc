#include "datacondAPI/config.h"

#include <filters/LDASConstants.hh>
#include <general/toss.hh>
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "UDT.hh"
#include "fft.hh"
#include "DFTUDT.hh"
#include "ShiftState.hh"
#include "calcWFunction.hh"
#include "rmvmFunction.hh"
#include <math.h>

//for input, will need list of Qs, f0s, fc, fs ,deltaf, x, mode_width


const std::string& calcWFunction::GetName(void) const{
  static std::string name("calcW");
  
  return name;
}

calcWFunction::calcWFunction() : Function ( calcWFunction::GetName() )
{}

static calcWFunction _calcWFunction;

void calcWFunction::Eval(CallChain* Chain,
			 const CallChain::Params& Params,
			 const std::string& Ret) const{
  using namespace datacondAPI;
  CallChain::Symbol* returnedSymbol = 0;
  
  switch(Params.size()){
  case 7:{
    //calcW(x,f0s,Qs,fc,fs,deltaf,mode_width)
    Sequence<double> x;
    if(udt::IsA<Sequence<double> >(*Chain->GetSymbol(Params[0]))){
      x.resize(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[0])).size());
      x = udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[0]));
    }
    else if(udt::IsA<Sequence<float> >(*Chain->GetSymbol(Params[0]))){
      x.resize(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[0])).size());
      for(int i=0; i< int(x.size()); i++){
	x[i] = udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[0]))[i];
      }
    }
    else
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Argument 0 (input data) is of the wrong type");
    
    Sequence<double> f0s;
    if(udt::IsA<Sequence<double> >(*Chain->GetSymbol(Params[1]))){
      f0s.resize(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[1])).size());
      f0s = udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[1]));
    }
    else if(udt::IsA<Sequence<float> >(*Chain->GetSymbol(Params[1]))){
      f0s.resize(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[1])).size());
      for(int i=0;i<int(f0s.size());i++){
	f0s[i] = udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[1]))[i];
      }
    }
    else if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[1]))){
      f0s.resize(1);
      f0s = udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[1])).GetValue();
    }
    else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[1]))){
      f0s.resize(1);
      f0s = double(udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[1])).GetValue());
    }
    else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[1]))){
      f0s.resize(1);
      f0s = double(udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[1])).GetValue());
    }
    else
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Argument 1 (frequencies) is of the wrong type");
    
    Sequence<double> Qs;
    if(udt::IsA<Sequence<double> >(*Chain->GetSymbol(Params[2]))){
      Qs.resize(udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[2])).size());
      Qs = udt::Cast<Sequence<double> >(*Chain->GetSymbol(Params[2]));
    }
    else if(udt::IsA<Sequence<float> >(*Chain->GetSymbol(Params[2]))){
      Qs.resize(udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[2])).size());
      for(int i=0;i<int(f0s.size());i++){
	Qs[i] = udt::Cast<Sequence<float> >(*Chain->GetSymbol(Params[2]))[i];
      }
    }
    else if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[2]))){
      Qs.resize(1);
      Qs = udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[2])).GetValue();
    }
    else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[2]))){
      Qs.resize(1);
      Qs = udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[2])).GetValue();
    }
    else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[2]))){
      Qs.resize(1);
      Qs = udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[2])).GetValue();
    }
    else
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Argument 2 (qualities) is of the wrong type");
    
    if(f0s.size() != Qs.size())
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Arguments 1 and 2 must be of the same length");
    
    double fc=0;
    if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[3]))){
      fc = udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[3])).GetValue();
    }
    else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[3]))){
      fc = udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[3])).GetValue();
    }
    else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[3]))){
      fc = udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[3])).GetValue();
    }
    else
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Argument 3 (fc) is of the wrong type");
    
    double fs =0;
    if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[4]))){
      fs = udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[4])).GetValue();
    }
    else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[4]))){
      fs = udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[4])).GetValue();
    }
    else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[4]))){
      fs = udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[4])).GetValue();
    }
    else
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Argument 4 (fs) is of the wrong type");

    double deltaf =0;
    if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[5]))){
      deltaf = udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[5])).GetValue();
    }
    else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[5]))){
      deltaf = udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[5])).GetValue();
    }
    else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[5]))){
      deltaf = udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[5])).GetValue();
    }
    else
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Argument 5 (deltaf) is of the wrong type");
    
    double mode_width =0;
    if(udt::IsA<Scalar<double> >(*Chain->GetSymbol(Params[6]))){
	mode_width = udt::Cast<Scalar<double> >(*Chain->GetSymbol(Params[6])).GetValue();
    }
    else if(udt::IsA<Scalar<float> >(*Chain->GetSymbol(Params[6]))){
      mode_width = udt::Cast<Scalar<float> >(*Chain->GetSymbol(Params[6])).GetValue();
    }
    else if(udt::IsA<Scalar<int> >(*Chain->GetSymbol(Params[6]))){
      mode_width = udt::Cast<Scalar<int> >(*Chain->GetSymbol(Params[6])).GetValue();
    }
    else
      General::toss<std::invalid_argument>("calcW Function",__FILE__,__LINE__,"Argument 6 (mode_width) is of the wrong type");
    
    int num_nodes = f0s.size();
    Sequence<double> w0s(num_nodes);
    for(int i=0; i<num_nodes; i++){
      w0s[i] = LDAS_TWOPI * f0s[i];
    }
    
    //double wc = LDAS_TWOPI * fc;
    
    //mix down so fc = 0?
    //downsample by deltaf/fs?
    int down = int(fs/deltaf);
    Sequence<std::complex<double> > z1;
    BandSelector bands(2*(fc)/fs, down);
    bands.apply(z1, x);
    
    Sequence<double> real_z(z1.size());
    Sequence<double> imag_z(z1.size());
    
    for(int i=0; i < int(z1.size()); i++){
      real_z[i] = z1[i].real();
      imag_z[i] = z1[i].imag();
    }
    
    Sequence<double> z_f0s(num_nodes);
    
    for(int i=0; i<num_nodes; i++){
      z_f0s[i] = f0s[i] - fc;
      if(z_f0s[1] < 0)
	z_f0s[1]*=-1.0;
    }
    
    //create hanning window of length nfft
    int nfft = 0;
    
    if(real_z.size()/5 > 8192)
      nfft = 8192; 
    else
      nfft = real_z.size()/5;
    
    Sequence<double> hannw(nfft);
    for(int i=0; i < nfft; i++){
      hannw[i] = 0.5 - 0.5 * cos(LDAS_TWOPI*i/(nfft + 1));
    }
    
    //calculate PSD of real and imaginary parts of z
    int iter = 0;
    int overlap = int(nfft/2);
    Sequence<double> pz_re(0.,nfft);
    Sequence<double> pz_im(0.,nfft);
    while(nfft + overlap*iter < int(real_z.size())){
      Sequence<double> temp1(nfft);
      Sequence<double> temp2(nfft);
      for(int i=0; i< nfft; i++){
	temp1[i] = hannw[i]*real_z[i+overlap*iter];
	temp2[i] = hannw[i]*imag_z[i+overlap*iter];
      }
      FFT fft1;
      FFT fft2;
      udt* asdf;
      fft1.apply(asdf,temp1);
      DFT<std::complex<double> > r_out(udt::Cast<DFT<std::complex<double> > >(*asdf));
      delete asdf;
      asdf = NULL;
      fft2.apply(asdf,temp2);
      DFT<std::complex<double> > i_out(udt::Cast<DFT<std::complex<double> > >(*asdf));
      delete asdf;
      asdf = NULL;
      
      for(int i=0; i < int(r_out.size()); i++){
	pz_re[i] += r_out[i].real()*r_out[i].real() + r_out[i].imag()*r_out[i].imag();
	pz_im[i] += i_out[i].real()*i_out[i].real() + i_out[i].imag()*i_out[i].imag();
      }
      iter++;
    }

    double norm = 0; 
    double wsum = 0;
    for(int i=0; i < nfft; i++){
      norm += hannw[i]*hannw[i];
      wsum += hannw[i];
    }
    norm = sqrt(norm);
    pz_re = pz_re/(norm*norm*(iter+1));
    pz_im = pz_im/(norm*norm*(iter+1));
    
    /*for(int i=0; i<int(pz_re.size()); i++){
      pz_re[i] = 10*log10(pz_re[i]);
      pz_im[i] = 10*log10(pz_im[i]);
      }*/
    Sequence<double> f(0.,nfft);
    for(int i=0; i<int(f.size()); i++){
      f[i] = i/nfft * deltaf;
    }
    
    Sequence<double> w(0.,2*num_nodes);
    Sequence<double> z_w0s(0.,z_f0s.size());
    //    throw std::invalid_argument("before returnedSymbol");
    //calculate process noise
    for(int i=0; i<num_nodes; i++){
      z_w0s[i] = LDAS_TWOPI*z_f0s[i]/deltaf;
      int start = 0;
      int end = 0;
      //pull out the part of the psd which contains the mode peak
      
      for(int j=0; j < int(f.size()); j++){
	if( ((f[j]-z_f0s[i]> 0 && f[j]-z_f0s[i]< mode_width) || (-(f[j]-z_f0s[i]) < mode_width)) && start == 0){
	  start = j;
	}
	else
	  end = j;
      }
      Sequence<double> w_proc(0.,end - start);
      Sequence<double> pz_re_proc (0.,end - start);
      Sequence<double> pz_im_proc (0.,end - start);
      for(int j=start; j<end; j++){
	w_proc[j-start] = f[j]*LDAS_TWOPI/deltaf;
	pz_re_proc[j-start] = pz_re[j]*norm*norm/(wsum*wsum);
	pz_im_proc[j-start] = pz_im[j]*norm*norm/(wsum*wsum);
      }
      //sum the re/im average to find the power under the peak
      double pz =0;
      for(int j=0; j<int(pz_re_proc.size()); j++){
	pz += pz_re_proc[j] + pz_im_proc[j];
      }
      
      //calculate the quantity you need to divide Pz by to get trW
      double normf=0;
      for(int j=0; j < int(w_proc.size()); j++){
	normf += (4*Qs[i]*Qs[i])/(z_w0s[i]*z_w0s[i] + pow((2*Qs[i]*w_proc[j] - z_w0s[i]*sqrt(4*Qs[i]*Qs[i] -1)),2));
      }
      
      
      //find the trW and calculate W
      double trW = pz/normf;
      w[2*i] = w[2*i+1] = trW;
      
    }//end of for loop
    
    returnedSymbol = new Sequence<double> (w);
    //throw std::invalid_argument("after returnedSymbol");
    }//end of case statement
    break;
  default:
    throw CallChain::BadArgumentCount( GetName(),
				       "7",
				       Params.size() );
    break;
  }//end of switch
  
  if(!returnedSymbol){
    throw CallChain::NullResult( GetName() );
  }
  
  Chain->ResetOrAddSymbol( Ret, returnedSymbol );
}
