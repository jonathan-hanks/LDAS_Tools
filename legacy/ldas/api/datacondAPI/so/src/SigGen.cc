#include "config.h"

#include <cmath>

#include "SigGen.hh"
#include "UDT.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

//=======================================================================
// Signal Generator
//=======================================================================

static SigGenSinFunction staticSigGenSinFunction;

SigGenSinFunction::
SigGenSinFunction()
: Function( SigGenSinFunction::GetName() )
{
}

void SigGenSinFunction::
Eval(CallChain* Chain, const CallChain::Params& Params, const std::string& Ret)
const
{
    using namespace datacondAPI;
    using std::complex;

    CallChain::Symbol* result = 0;
    
    switch(Params.size())
    {
    case 3:
        {

      //-----------------------------------------------------------------
      // We extract a pointer to the Object in the CallChain associated
      // with the symbol name of the parameter.
      //-----------------------------------------------------------------
      
      const CallChain::Symbol&  nsamp(*(Chain->GetSymbol(Params[0])));
      const CallChain::Symbol&  freq(*(Chain->GetSymbol(Params[1])));
      const CallChain::Symbol&  phi(*(Chain->GetSymbol(Params[2])));
      
      //-----------------------------------------------------------------
      // We must check that the Object contains a supported data type,
      // using dynamic_casts.
      //-----------------------------------------------------------------


     int n_samples;
     if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(nsamp))
       {
	 n_samples = datacondAPI::udt::Cast< datacondAPI::Scalar<int>>(nsamp);
       }
     else
       {
         throw std::invalid_argument("siggensin() - input argument nsamp must "
                                            "be int.");
	 break;
       }


     double frequency;
     if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(freq))
       {
	 frequency = datacondAPI::udt::Cast< datacondAPI::Scalar<int> >(freq);
       }
     else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(freq))
       {
	 frequency = datacondAPI::udt::Cast< datacondAPI::Scalar<float> >(freq);
       }
     else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(freq))
       {
	 frequency = datacondAPI::udt::Cast< datacondAPI::Scalar<double> >(freq)
       }
     else
       {
         throw std::invalid_argument("siggensin() - input argument freq must "
                                            "be int, float or double.");
	 break;
       }


     double phase;
     if (datacondAPI::udt::IsA< datacondAPI::Scalar<int> >(phi))
       {
	 phase = datacondAPI::udt::Cast< datacondAPI::Scalar<int> >(phi);
       }
     else if (datacondAPI::udt::IsA< datacondAPI::Scalar<float> >(phi))
       {
	 phase = datacondAPI::udt::Cast< datacondAPI::Scalar<float> >(phi);
       }
     else if (datacondAPI::udt::IsA< datacondAPI::Scalar<double> >(phi))
       {
	 phase = datacondAPI::udt::Cast< datacondAPI::Scalar<double> >(phi);
       }
     else
       {
         throw std::invalid_argument("siggensin() - input argument phi must "
                                            "be int, float or double.");
	 break;
       }

    if (fabs(frequency) >= 1)
      {
	throw std:invalid_argument("siggensin() - invalid frequency");
	break;
      }
      std::valarray<double>& out
      for (size_t k = 0; k < n_samples; ++k)
	{
	  out[k] = sin((LDAS_PI*frequency*k)) + phase);
	}
 
      result = new datacondAPI::Sequence<double>(out);
      }
    break;
          
    default:
        throw CallChain::BadArgumentCount("siggensin", 3, Params.size());
        break;
    }
    
    if (result == 0)
    {
        throw CallChain::NullResult( GetName() );
    }
    
    Chain->ResetOrAddSymbol( Ret, result );
}

const std::string& SigGenSinFunction::
GetName(void) const
{
    const static std::string name("siggensin");
    return name;
}

static SigGenSinFunction _SigGenSinFunction;
