/* -*- mode: c++; c-basic-offset: 2; -*- */
//
// $Id: SequenceUDT.cc,v 1.34 2006/11/27 21:32:14 emaros Exp $

#include "config.h"

#include <complex>

#include "general/types.hh"

#include "ilwd/ldascontainer.hh"
#include "ilwd/ldaselement.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"

#include "ilwdfcs/FrameH.hh"
#include "ilwdfcs/FrProcData.hh"
#include "ilwdfcs/FrVect.hh"

#include "CallChain.hh"
#include "SequenceUDT.hh"
#include "WhenUMD.hh"

template<class DataType_>
datacondAPI::Sequence<DataType_>::
Sequence()
{
}

template<class DataType_>
datacondAPI::Sequence<DataType_>::
Sequence(size_t Size)
  : std::valarray<DataType_>(Size)
{
}

template<class DataType_>
datacondAPI::Sequence<DataType_>::
Sequence(const DataType_& Value, size_t Size)
  : std::valarray<DataType_>(Value, Size)
{
}

template<class DataType_>
datacondAPI::Sequence<DataType_>::
Sequence(const std::valarray<DataType_>& Value)
  : std::valarray<DataType_>(Value)
{
}

template<class DataType_>
datacondAPI::Sequence<DataType_>::
Sequence(const Sequence<DataType_>& Data)
  : datacondAPI::udt(Data), std::valarray<DataType_>(Data)
{
}

template<class DataType_>
datacondAPI::Sequence<DataType_>::
Sequence(const DataType_* Data, size_t Size)
  : std::valarray<DataType_>(Data, Size)
{
}

template<class DataType_>
  template<class ArrayData_>
datacondAPI::Sequence<DataType_>::
Sequence(const ArrayData_* Data, size_t Size)
  : std::valarray<DataType_>(Size)
{
    std::copy(Data, Data + Size, &(*this)[0]);
}
    
template<class DataType_>
datacondAPI::Sequence<DataType_>::
~Sequence()
{
}

template <class DataType_>
datacondAPI::Sequence<DataType_>* datacondAPI::Sequence<DataType_>::
Clone() const
{
  return new Sequence<DataType_>(*this);
}

template<class DataType_>
ILwd::LdasElement* datacondAPI::Sequence<DataType_>::
ConvertToIlwd( const CallChain& Chain,
	       datacondAPI::udt::target_type Target ) const
{
  ILwd::LdasContainer* container = new ILwd::LdasContainer;

  switch(Target)
  {
  case udt::TARGET_FRAME:
  case udt::TARGET_FRAME_FINAL_RESULT:
    {
      using namespace ILwdFCS;

      Sequence<DataType_>& data = const_cast<Sequence<DataType_>&>(*this);
      const std::string& lname( Chain.GetIntermediateName( *this, name( ) ) );

      FrProcData proc_data;

      proc_data.SetName( lname );
      proc_data.SetComment( Chain.GetIntermediateComment( *this ) );
      proc_data.SetType( ILwdFCS::FrProcData::UNKNOWN_TYPE );

      //:NOTE: undefined for this class
      // proc_data.SetSubType( 0 );
      // proc_data.SetTimeOffset( 0.0 );
      // proc_data.SetTRange( 0.0 );
      // proc_data.SetFShift( 0.0 );
      // proc_data.SetPhase( 0.0 );
      // proc_data.SetFRange( 0.0 );
      // proc_data.SetBW( 0.0 );
      
      //:NOTE: Add auxParams, aux data, tables here
      
      // Add the data
      FrVect vect_data;
      vect_data.SetData( &data[0], this->size() );
      vect_data.SetName( lname );

      proc_data.AppendData( vect_data );

      proc_data.AppendHistory(getHistory(), GPSTime::NowGPSTime());

      ArbitraryWhenMetaData when;
      when.Store( proc_data );

      ILwd::LdasContainer* retval =
	const_cast<CallChain&>(Chain).FormatFrame( Target,
						   container,
						   proc_data );
      if ( retval != container )
      {
	delete container;
	container = retval;
      }
    }
    break;

  case udt::TARGET_WRAPPER:
    {
      Sequence<DataType_>& data = const_cast<Sequence<DataType_>&>(*this);
      std::string domain(":domain");
	
      domain.insert( 0, GetDomain( ) );
      
      container->appendName( name() );
      container->appendName("channel");
      container->appendName("sequence");

      // This function in the UDT base class is the only way to get
      // the UDT history into the container.
      udt::ConvertToIlwd(Chain, udt::TARGET_WRAPPER, container);

      container->push_back(new ILwd::LdasString("NONE", domain),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );

      // Push the time-series data
      container->push_back(new ILwd::LdasArray<DataType_>
			   (&data[0], data.size(), "data"),
			   ILwd::LdasContainer::NO_ALLOCATE,
			   ILwd::LdasContainer::OWN );
    }
    break;

  case udt::TARGET_GENERIC:
    {
      Sequence<DataType_>& data = const_cast<Sequence<DataType_>&>(*this);
      
      delete container;

      return new ILwd::LdasArray<DataType_>(&data[0], data.size());
    }
    break;

  default:
    throw udt::BadTargetConversion(Target, "datacondAPI::Sequence");
    break;
  }

  return container;
}

template <class DataType_>
datacondAPI::Sequence<DataType_>& 
datacondAPI::Sequence<DataType_>::
operator=(const Sequence<DataType_>& rhs)
{
    if (&rhs != this)
    {
	datacondAPI::udt::operator=(rhs);

	if (this->size() != rhs.size())
	{
	    resize(rhs.size());
	}

	std::valarray<DataType_>::operator=(rhs);
    }
    return *this;
}

// forwarded methods
template <class DataType_>
std::valarray<DataType_>& 
datacondAPI::Sequence<DataType_>::
operator=(const std::valarray<DataType_>& v)
{
    if (this->size() != v.size())
    {
	resize(v.size());
    }
    
    return std::valarray<DataType_>::operator=(v);
}

template <class DataType_>
std::valarray<DataType_>& 
datacondAPI::Sequence<DataType_>::
operator=(const DataType_& v)
{
  return std::valarray<DataType_>::operator=(v);
}

template <class DataType_>
std::valarray<DataType_>& 
datacondAPI::Sequence<DataType_>::
operator=(const std::slice_array<DataType_>& v)
{
  return std::valarray<DataType_>::operator=(v);
}


template <class DataType_>
std::valarray<DataType_>& 
datacondAPI::Sequence<DataType_>::
operator=(const std::gslice_array<DataType_>& v)
{
  return std::valarray<DataType_>::operator=(v);
}

template <class DataType_>
std::valarray<DataType_>& 
datacondAPI::Sequence<DataType_>::
operator=(const std::mask_array<DataType_>& v)
{
  return std::valarray<DataType_>::operator=(v);
}

template <class DataType_>
std::valarray<DataType_>& 
datacondAPI::Sequence<DataType_>::
operator=(const std::indirect_array<DataType_>& v)
{
  return std::valarray<DataType_>::operator=(v);
}

namespace datacondAPI
{
  template<class T>
  std::string datacondAPI::Sequence<T>::
  GetDomain() const
  {
    return "real";
  }

  template< >
  std::string datacondAPI::Sequence< std::complex< float > >::
  GetDomain() const
  {
    return "complex";
  }

  template< >
  std::string datacondAPI::Sequence< std::complex< double > >::
  GetDomain() const
  {
    return "complex";
  }
} // namespace - datacondAPI

///----------------------------------------------------------------------
/// Private methods
///----------------------------------------------------------------------

///----------------------------------------------------------------------
/// Instantiation of the class
///----------------------------------------------------------------------

#undef CLASS_INSTANTIATION
#define	CLASS_INSTANTIATION(class_,key_) \
template class datacondAPI::Sequence< class_ >; \
UDT_CLASS_INSTANTIATION(Sequence< class_ >,key_)

CLASS_INSTANTIATION(float,float)
CLASS_INSTANTIATION(double,double)
CLASS_INSTANTIATION(std::complex<float>,cfloat)
CLASS_INSTANTIATION(std::complex<double>,cdouble)

#if __SUNPRO_CC
#undef INIT_FUNC
#define INIT_FUNC init_SequenceUDT
MK_UDT_INIT_FUNC_4(INIT_FUNC,float,double,cfloat,cdouble);
#pragma init (INIT_FUNC)
#endif /* __SUNPRO_CC */

#undef CLASS_INSTANTIATION

///----------------------------------------------------------------------
/// Sequence(const DataType_*, size_t) instantiation
///----------------------------------------------------------------------

template
datacondAPI::Sequence<float>::
Sequence(const short* Data, size_t Size);

template
datacondAPI::Sequence<float>::
Sequence(const unsigned short* Data, size_t Size);

template
datacondAPI::Sequence<float>::
Sequence(const int* Data, size_t Size);

template
datacondAPI::Sequence<float>::
Sequence(const unsigned int* Data, size_t Size);

template
datacondAPI::Sequence<float>::
Sequence(const long* Data, size_t Size);

template
datacondAPI::Sequence<float>::
Sequence(const unsigned long* Data, size_t Size);

template
datacondAPI::Sequence<double>::
Sequence(const float* Data, size_t Size);

template
datacondAPI::Sequence<std::complex<double> >::
Sequence(const std::complex<float>* Data, size_t Size);
