#ifndef _RAN1FUNCTION_HH_
#define _RAN1FUNCTION_HH_

#include "CallChain.hh"

class ran1Function : public CallChain::Function
{
public:
  ran1Function();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

#endif //_RAN1FUNCTION_HH_
