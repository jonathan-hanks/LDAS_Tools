/* -*- mode: c++; c-basic-offset: 2; -*- */
#ifndef KALMAN_HH
#define KALMAN_HH
// $Id: Kalman.hh,v 1.10 2007/06/18 15:26:42 emaros Exp $

//preliminary interface for the Kalman filter class

//includes:
#include <vector>

#include <general/unexpected_exception.hh>

#include "KalmanState.hh"
#include "Matrix.hh"
#include "VectorSequenceUDT.hh"

namespace datacondAPI {
  
  class Kalman
  {
  public:
    
    //======================================================================
    //: Specify-everything constructor
    //!param: A - state evolution matrix (MxM)
    //!param: W - process error covariance matrix (MxM)
    //!param: C - measurement matrix (NxM)
    //!param: V - measurement error covariance matrix (NxN)
    //!param: psi - initial estimate state of state vector (Mx1)
    //!param: P - initial estimate state vector covariance matrix (MxM)
    //!exc: std::invalid_argument - matrix or state vector dimensions are not
    //! + consistent
    Kalman(const Matrix<double>& A, 
	   const Matrix<double>& W, 
	   const Matrix<double>& C, 
	   const Matrix<double>& V, 
	   const Sequence<double>& psi, 
	   const Matrix<double>& P);

    //: Construct from existing KalmanState object
    //!param: state - KalmanState to initialize from
    Kalman(const KalmanState& state);
    

    //: Construct form UDTs
    //!param: A - state evolution matrix (MxM)
    //!param: W - process error covariance matrix (MxM)
    //!param: C - measurement matrix (NxM)
    //!param: V - measurement error covariance matrix (NxN)
    //!param: psi - initial estimate state of state vector (Mx1)
    //!param: P - initial estimate state vector covariance matrix (MxM)
    //!exc: std::invalid_argument - matrix or state vector dimensions are not
    //! + consistent or argument is of the wrong type
    Kalman(udt& A,
           udt& W,
           udt& C,
           udt& V,
           udt& psi,
           udt& P);

    //: Copy constructor
    //!param: const Kalman& - object to duplicate
    Kalman(const Kalman&);
    
    //: Default constructor
    Kalman();

    //: Destructor
    ~Kalman();
    
    
    //======================================================================
    //mutators
    
    //======================================================================
    //accessors
    //: get copy of state evolution matrix
    //!param: A - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if A needs to be allocated and new fails
    //!exc: std::invalid_argument - if A not an appropriate pointer type
    void getA(udt*& A);

    //: get copy of process error covariance matrix
    //!param: W - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if W needs to be allocated and new fails
    //!exc: std::invalid_argument - if W not an appropriate pointer type
    void getW(udt*& W);

    //: get copy of measurement error covariance matrix
    //!param: V - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if V needs to be allocated and new fails
    //!exc: std::invalid_argument - if V not an appropriate pointer type
    void getV(udt*& V);

    //: get copy of measurement matrix
    //!param: C - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if C needs to be allocated and new fails
    //!exc: std::invalid_argument - if C not an appropriate pointer type
    void getC(udt*& C);

    //: get copy of current state vector prediction
    //!param: A - pointer to Sequence<double>. May be null, in which case
    //+ Sequence<double> will be allocated on heap
    //!exc: std::bad_alloc - if psi needs to be allocated and new fails
    //!exc: std::invalid_argument - if psi not an appropriate pointer type
    void getPsi(udt*& psi);

    //: get copy of state vector error covariance matrix
    //!param: P - pointer to Matrix<double>. May be null, in which case
    //+ Matrix<double> will be allocated on heap
    //!exc: std::bad_alloc - if P needs to be allocated and new fails
    //!exc: std::invalid_argument - if P not an appropriate pointer type
    void getP(udt*& P);

    //: get copy of KalmanState object
    //!param: state - pointer to KalmanState. May be null, in which case
    //+ KalmanState will be allocated on heap
    //!exc: std::bad_alloc - if state needs to be allocated and new fails
    //!exc: std::invalid_argument - if state not an appropriate pointer type
    void getState(KalmanState*& state);
    
    //: get copy of state evolution matrix
    //!param: Matrix<double>& - on return copy of state evolution matrix
    void getA(Matrix<double>& A) const;

    //: get copy of process error covariance matrix
    //!param: Matrix<double>& - on return copy of
    //+ process error covariance matrix
    void getW(Matrix<double>& W) const;

    //: get copy of measurement error covariance matrix
    //!param: Matrix<double>& - on return copy of
    //+ measurement error covariance matrix
    void getV(Matrix<double>& V) const;

    //: get copy of measurement operator
    //!param: Matrix<double>& - on return copy of
    //+ measurement operator
    void getC(Matrix<double>& C) const;

    //: get copy of state vector
    //!param: Matrix<double>& - on return copy of current state vector
    void getPsi(Sequence<double>& psi) const;

    //: get copy of state error covariance matrix
    //!param: Matrix<double>& - on return copy of
    //+ state error covariance matrix
    void getP(Matrix<double>& P) const;

    //======================================================================
    //actions
    
    //: Basic filter action
    //!param: z_out - Prediction of filter based on input. Existing contents destroyed.
    //!param: in - input data for filter.
    //!param: psi_out - Output of State vector.
    //!param: P_out - Predicted state vector error convariance.
    //!exc: std::invalid_argument - in.size() == 0
    template <class out_type, class in_type>
    void apply(VectorSequence<out_type>& z_out, const VectorSequence<in_type>& in, 
	VectorSequence<out_type>& psi_out, Matrix<out_type>& P_out);

    //: apply to measurement, get measurement prediction
    //!param: out - pointer to measurement prediction. May be null on input,
    //+ in which case result is allocated on heap
    //!param: in - (noisy) input measurements
    //!exc: std::invalid_argument - if out is not of correct type for use 
    //+ with in
    //!exc: std::bad_alloc - if *out needs to be allocated and new fails
    void apply(udt*& out, const udt& in);
    
    //: apply to measurement, get measurement prediction and, optionally, 
    //+ state vector and prediction error covariance matrix predictions
    //!param: z_out - pointer to measurement prediction. May be null on input,
    //+ in which case result is allocated on heap
    //!param: in - (noisy) input measurements
    //!param: psi_out - predicted state vector. Ignored if null.
    //!param: P_out - predicted state vector error covariance. Ignored if null.
    //!exc: std::invalid_argument - if out, psi_out or P_out are not of 
    //+ correct type for use with in
    //!exc: std::bad_alloc - if *z_out needs to be allocated and new fails
    void apply(udt*& z_out, const udt& in, 
	       udt*& psi_out, udt*& P_out);
    
  private:
    
    KalmanState m_state;
  };


}

#endif
