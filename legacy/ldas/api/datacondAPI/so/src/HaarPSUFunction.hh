#ifndef _HAARPSUFUNCTION_HH_
#define _HAARPSUFUNCTION_HH_

#include "CallChain.hh"

namespace datacondAPI {
namespace psu
{

class HaarPSUFunction : public CallChain::Function
{
public:
  HaarPSUFunction();
  virtual const std::string& GetName() const;
  virtual void Eval(CallChain* Chain,
		    const CallChain::Params& Params,
		    const std::string& Ret) const;
};

}//end namespace psu
}//end namespace datacondAPI

#endif //_HAARPSUFUNCTION_HH_


