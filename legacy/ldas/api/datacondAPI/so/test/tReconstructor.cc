//tReconstructor.cc

#include "datacondAPI/config.h"

#include <iostream>
#include <string>   

#include "general/unittest.h"

#include "WaveletPSU.hh"
#include "HaarPSU.hh"
#include "resolver.hh"
#include "result.hh"
#include "reconstructor.hh"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"

using namespace datacondAPI::psu;
using namespace std;   

void EasyTest();
void RTest();
void PartReconstruct();
void ExceptionTest();

General::UnitTest Test;

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  EasyTest();

  RTest();

  PartReconstruct();

  ExceptionTest();

  Test.Check(true) << "End of test harness reached." << endl; 

  Test.Exit();

} //end main


void EasyTest()
{
  Test.Message() << "Executing EasyTest." << endl;

  datacondAPI::Sequence<double> data(0.0, 4);
  data[0] = 1.0;

  Haar haarwavelet;
  Result<double> easyResult; 
  datacondAPI::udt* peasyResult = &easyResult;

  Resolver easyTest(haarwavelet, 1);

  Test.Message() << "Performing decomposition (EasyTest)" << endl;
  easyTest.apply(peasyResult, data);
  
  Reconstructor ercst;
  datacondAPI::Sequence<double> reconstruction;

  Test.Message() << "Performing reconstruction (EasyTest)." << endl;

  string what = "Reconstructor::apply";
  try
    {
      ercst.apply(reconstruction, easyResult);
    }
  catch(std::domain_error& e)
    {
      Test.Check(false) << e.what() << endl;
    }
  catch(...)
    {
      Test.Check(false) << "Caught unexpected exception.";
    }

  /*
  //display reconstructed signal
  Test.Message() << "Reconstructed signal: \n";
  for(unsigned int i = 0; i < reconstruction.size(); i++)
    Test.Message() << reconstruction[i] << endl;

  //check reconstructed signal
  Test.Message() << "Expected reconstructed signal: \n";
  for(unsigned int pqr = 0; pqr < data.size(); pqr++)
    Test.Message() << data[pqr] << endl;
  */

  //difference between original and reconstructed signals
  datacondAPI::Sequence<double> difference(data - reconstruction);
  Test.Message() << "Difference between original and reconstructed:" << endl;
  for (unsigned int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(data == reconstruction);

  bool pass = true;
  for (unsigned int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  Test.Check(pass) << "Reconstructed signal.\n";
}

void RTest()
{
  Test.Message() << "Executing RTest." << endl;
  Test.Message() << "DECOMPOSITION for full reconstruction:" << endl;
  datacondAPI::Sequence<double> data(0.0, 48);
  data[4] = 1.0;

  Haar haarWavelet;
  Result<double> testResult;
  datacondAPI::udt* ptestResult = &testResult;
  Resolver test(haarWavelet, 4);

  //  Test.Message()<< "Applying filter." << endl;
  try{
    test.apply(ptestResult, data);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //Reconstruct the signal
  Test.Message() << "RECONSTRUCTION:" << endl;

  Reconstructor rcst;
  datacondAPI::Sequence<double> reconstruction;
  datacondAPI::udt* preconstruction = &reconstruction;

  //Test.Message() << "Applying synthesis filters for reconstruction." << endl;
  string what = "Reconstructor::apply method";
  try
    {
      rcst.apply(preconstruction, testResult);
    }
  catch(std::domain_error& d)
    {
      Test.Check(false) << what << " ( " << d.what() << " )"<< endl;
    }

  //check reconstructed signal
  datacondAPI::Sequence<double> original(0.0, 48);
  original[4] = 1.0;

  //difference between original and reconstructed signals
  datacondAPI::Sequence<double> difference(original - reconstruction);
  Test.Message() << "Difference between original and reconstructed:" << endl;
  for (unsigned int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(original == reconstruction);

  bool pass = true;
  for (unsigned int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  Test.Check(pass) << "Reconstructed signal.\n";

}

void PartReconstruct()
{
  Test.Message() << "DECOMPOSITION for partial reconstruction:" << endl;
  datacondAPI::Sequence<double> original(0.0, 24);
  original[3] = 1.0;

  Haar haarWavelet2;
  Result<double> testResult2;
  datacondAPI::udt* ptestResult2 = &testResult2;
  Resolver test2(haarWavelet2, 3);

  //Test.Message()<< "Applying filter." << endl;
  try{
    test2.apply(ptestResult2, original);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  Reconstructor rcst;
  datacondAPI::Sequence<double> reconstruction;
  datacondAPI::udt* preconstruction = &reconstruction;
  datacondAPI::Scalar<int> level(3);

  Test.Message() << "PARTIAL RECONSTRUCTION from level " << (int&)(level) << endl;

  string what = "Overloaded Reconstructor::apply method";
  try
    {
      rcst.apply(preconstruction, testResult2, level);
    }
  catch(std::invalid_argument& a)
    {
      Test.Check(false) << what << " (" << a.what() << " )" << endl;
    }
  catch(...)
    {
      Test.Check(false) << what << "(Caught unexpected exception.)" << endl;
    }

  /*
  //display partially reconstructed signal
  Test.Message() << "Partially reconstructed signal: " << endl;
  for(unsigned int t = 0; t < reconstruction.size(); t++)
    Test.Message() << reconstruction[t] << endl;
  */

  //expected signals from different levels of reconstruction:
  /*
  //check partially reconstructed signal -> level = 0
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[3] = 0.5;
  expected[4] = -0.5;
  */

  /*  
  //check partially reconstructed signal -> level = 1
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[1] = expected[2] = -0.25;
  expected[3] = expected[4] = 0.25;
  */

  /*
  //check partially reconstructed signal -> level = 2
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[1] = expected[2] = expected[3] = expected[4] = 0.125;
  expected[5] = expected[6] = expected[7] = expected[8] = -0.125; 
  */

 
  //check partially reconstructed signal -> level = 3
  datacondAPI::Sequence<double> expected(0.0, 24);
  expected[1] = expected[2] = expected[3] = expected[4] = 0.125;
  expected[5] = expected[6] = expected[7] = expected[8] = 0.125; 
 

   //difference between expected and reconstructed signals
  datacondAPI::Sequence<double> difference(expected - reconstruction);
  Test.Message() << "Difference = (expected - reconstructed):" << endl;
  for (unsigned int k = 0; k < difference.size(); k++)
    Test.Message() << difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check(expected == reconstruction);

  bool pass = true;
  for (unsigned int i = 0; i < check.size(); i++)
    pass = pass && check[i];

  Test.Check(pass) << "Partially reconstructed signal.\n";

}


void ExceptionTest()
{
  //tests exceptions in Reconstructor class

  Result<double> exceptionresult;
  datacondAPI::Sequence<float> excep;
  Reconstructor exceptionreconstructor;
  datacondAPI::Sequence<double> out2;
  datacondAPI::Scalar<int> level(0);
  datacondAPI::Scalar<int> badlevel(-4);

  std::string what1 = "unsupported type used in udt apply(out, in)";
  std::string what2 = "unsupported type used in udt apply(out, in, level)";
  std::string what3 = "illegal levels value";

  try
    {
      datacondAPI::udt* out;
      exceptionreconstructor.apply(out, excep);
      Test.Check(false) << what1 << endl;
    }
  catch(exception& e1)
    {
      Test.Check(true) << what1 << " (" << e1.what() << ")" << endl;
    }

  try
    {
      datacondAPI::udt* out;
      exceptionreconstructor.apply(out, excep, level);
      Test.Check(false) << what2 << endl;
    }
  catch(exception& e2)
    {
      Test.Check(true) << what2 << " (" << e2.what() << ")" << endl;
    }

  try
    {
      exceptionreconstructor.apply(out2, exceptionresult, (int&)(badlevel));
      Test.Check(false) << what3 << endl;
    }
  catch(exception& e3)
    {
      Test.Check(true) << what3 << " (" << e3.what() << ")" << endl;
    }
}
