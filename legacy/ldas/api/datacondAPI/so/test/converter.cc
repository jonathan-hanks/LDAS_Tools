#include "datacondAPI/config.h"

#include <cstdarg>

#include <iostream>
#include <map>
#include <vector>
#include <sstream>   

#include "general/unittest.h"
#include "general/unordered_map.hh"

#include "UDT.hh"
#include "CallChain.hh"
#include "token.hh"

using General::unordered_map;  
using namespace datacondAPI;
using namespace DB;

enum dboption_type {
  DB_NONE,
  DB_SPECTRUM
};

enum pushpass_type {
  PUSH,
  PASS
};

static void convert( char* Filename, udt::target_type Format,
		     dboption_type DBOption, pushpass_type PushPass );
static std::vector<std::string> get_symbol_names( std::string Symbols );
static std::vector<CallChain::Symbol*>
get_objects( const CallChain& Chain,
	     std::string Symbols );

//-----------------------------------------------------------------------
// Declaration of templates
//-----------------------------------------------------------------------

template <class T> T*
create( const std::string& Input )
{
  std::istringstream is( Input );
  ILwd::Reader 	reader(is);

  return dynamic_cast<T*>( ILwd::LdasElement::createElement( reader ) );
}

//-----------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------

General::UnitTest Test;

//-----------------------------------------------------------------------
// Entry Point
//-----------------------------------------------------------------------

int
main( int ArgC, char** ArgV )
try
{
  Test.Init(ArgC, ArgV);

  typedef unordered_map<std::string,udt::target_type>	map_target_t;
  map_target_t					map_target;
  unordered_map<std::string,dboption_type>	map_dboption;
  unordered_map<std::string,pushpass_type>	map_push_pass;

  map_target["-generic"] = udt::TARGET_GENERIC;
  map_target["-metadata"] = udt::TARGET_METADATA;
  map_target["-wrapper"] = udt::TARGET_WRAPPER;


  map_target["-frame"] = udt::TARGET_FRAME;

  map_dboption["-db-none"] = DB_NONE;
  map_dboption["-dbspectrum"] = DB_SPECTRUM;

  map_push_pass["-push"] = PUSH;
  map_push_pass["-pass"] = PASS;

  if ( ArgC < 3 )
  {
    std::cerr << "Usage: " << ArgV[0];
    for( map_target_t::const_iterator i = map_target.begin();
	 i != map_target.end();
	 i ++ )
    {
      if ( i == map_target.begin() )
      {
	std::cerr << " ";
      }
      else
      {
	std::cerr << "|";
      }
      std::cerr << (*i).first;
    }
    std::cerr << " <file> [ <file> ...]"
	      << std::endl;
    exit (1);
  }

  udt::target_type	target( udt::TARGET_GENERIC );
  dboption_type	dboption( DB_NONE );
  pushpass_type	push_pass( PUSH );

  ArgV++;
  for( ; *ArgV; ArgV++ )
  {
    if ( *(*ArgV) == '-' )
    {
      if ( (*ArgV)[1] == '-' )
      {
	continue;
      }
      if ( map_target.find(*ArgV) != map_target.end() )
      {
	target = map_target[*ArgV];
	continue;
      }
      else if ( map_dboption.find(*ArgV) != map_dboption.end() )
      {
	dboption = map_dboption[*ArgV];
	continue;
      }
      else if ( map_push_pass.find(*ArgV) != map_push_pass.end() )
      {
	push_pass = map_push_pass[*ArgV];
	continue;
      }
    }
    convert( *ArgV, target, dboption, push_pass);
  }
  Test.Exit();
}
catch( std::exception& e )
{
  Test.Check(false) << "Caught exception: " << e.what() << std::endl;
  Test.Exit();
}
catch( ... )
{
  Test.Check(false) << "Caught unknown exception" << std::endl;
  Test.Exit();
}

static void
convert( char* Filename, udt::target_type Format,
	 dboption_type DBOption,  pushpass_type PushPass )
{
  Test.Message() << "Working on: " << Filename << std::endl;
  const ILwd::LdasElement*	ilwd( read_ilwd( Filename, "" ) );

  if ( ilwd == (const ILwd::LdasElement*)NULL )
  {
    Test.Message() << "Unable to create ilwd from file: "
		   << Filename
		   << std::endl;
    return;
  }

  CallChain	chain;
  std::string	symbols;

  chain.Reset();

  switch( DBOption )
  {
  case DB_NONE:
    symbols = chain.IngestILwd( *ilwd );
    break;
  case DB_SPECTRUM:
    {
      Test.Message( ) << "DB_SPECTRUM: " << __FILE__ << " " << __LINE__
		      << std::endl;

      const ILwd::LdasContainer& tbl_ilwd =
	dynamic_cast<const ILwd::LdasContainer&>(*ilwd);
      Test.Message( ) << "DB_SPECTRUM: " << __FILE__ << " " << __LINE__
		      << std::endl;
      Table* table( DB::Table::MakeTableFromILwd( tbl_ilwd, true ) );
      Test.Message( ) << "DB_SPECTRUM: " << __FILE__ << " " << __LINE__
		      << std::endl;

      std::vector<udt*>		data;
      std::vector<std::string>	names;

      tbl_ilwd.write( 2, 2, Test.Message( false ), ILwd::ASCII );

      chain.IngestDatabaseSpectrumData( *table,
					data,
					names,
					"query",
					CallChain::EXACTLY_ONE,
					&symbols );
      for ( std::vector<std::string>::const_iterator s = names.begin();
	    s != names.end();
	    s++, symbols += " " )
      {
	symbols += (*s);
      }
    }
    break;
  }

  Test.Message() << "Ingested symbols:" << symbols << std::endl;

  std::vector<CallChain::Symbol*> obj( get_objects( chain, symbols ) );
  std::vector<std::string>	sn( get_symbol_names( symbols ) );

  for ( std::vector<std::string>::const_iterator sym( sn.begin() );
	sym != sn.end();
	sym++ )
  {
    chain.AppendIntermediateResult( (*sym).c_str(),
				    (*sym).c_str(), "result_data",
				    udt::GetTargetType( Format ).c_str() );
  }

  chain.Execute();

  Test.Message() << "Results: ";
  chain.GetResults()->write( 2, 2, Test.Message( false ), ILwd::ASCII );
  Test.Message( false ) << std::endl;
}

static std::vector<std::string>
get_symbol_names( std::string Symbols )
{
  std::vector<std::string>	retval;

  std::string::size_type	start(0);
  std::string::size_type	end(Symbols.find(' '));

  while (1)
  {
    std::string		symbol_name(Symbols.substr(start,
						   (end == std::string::npos)
						   ? end
						   : end - start));

    retval.push_back( symbol_name );
    
    if (end == std::string::npos)
    {
      break;
    }
    start = Symbols.find_first_not_of(' ', end);
    end = Symbols.find(' ', start);
  }
  return retval;
}

static std::vector<CallChain::Symbol*>
get_objects( const CallChain& Chain, std::string Symbols )
{
  std::vector<CallChain::Symbol*>	retval;

  std::string::size_type	start(0);
  std::string::size_type	end(Symbols.find(' '));

  while (1)
  {
    std::string		symbol_name(Symbols.substr(start,
						   (end == std::string::npos)
						   ? end
						   : end - start));

    retval.push_back( Chain.GetSymbol(symbol_name) );
    
    if (end == std::string::npos)
    {
      break;
    }
    start = Symbols.find_first_not_of(' ', end);
    end = Symbols.find(' ', start);
  }
  return retval;
}
