/* -*- mode: c++; c-basic-offset: 2; -*- */
#include "datacondAPI/config.h"

#include <general/unittest.h>
#include <filters/valarray_utils.hh>

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "DFTUDT.hh"

#include "Abs.hh"
#include "Arg.hh"
#include "Imag.hh"
#include "Real.hh"

using namespace std;   
using namespace datacondAPI;

General::UnitTest Test;

// T = float or double
template<class T>
class Trait {
public:
    typedef T ResultType;
};

// Specialisations
template<>
class Trait<complex<float> > {
public:
    typedef float ResultType;
};

template<>
class Trait<complex<double> > {
public:
    typedef double ResultType;
};

template<class T>
string typeName()
{
    throw std::runtime_error("No type specified");

    return string("");
}

template<>
string typeName<float>()
{
    return string("float");
}

template<>
string typeName<double>()
{
    return string("double");
}

template<>
string typeName<complex<float> >()
{
    return string("complex<float>");
}

template<>
string typeName<complex<double> >()
{
    return string("complex<double>");
}

template<class T>
void fillSequence(Sequence<T>& x)
{
    const size_t n = 50;

    x.resize(n);
    for (size_t k = 0; k < x.size(); ++k)
    {
        x[k] = k - x.size()/2;
    }
}

template<class T>
void fillSequence(Sequence<complex<T> >& x)
{
    const size_t n = 50;

    x.resize(n);
    for (size_t k = 0; k < x.size(); ++k)
    {
        x[k] = complex<T>(k - x.size()/2, -k + x.size()/2);
    }
}

template<class TOut, class TIn>
void testReal_SequenceValues(const Sequence<TOut>& out,
                             const Sequence<TIn>& in)
{
    const Sequence<TOut> check = Filters::real(in);
    bool pass = true;

    for (size_t k = 0; k < in.size(); ++k)
    {
        pass = pass && (out[k] == check[k]);
    }

    Test.Check(pass) << "Sequence values are correct" << endl;
}

template<class TOut, class TIn>
void testImag_SequenceValues(const Sequence<TOut>& out,
                             const Sequence<TIn>& in)
{
    const Sequence<TOut> check = Filters::imag(in);
    bool pass = true;

    for (size_t k = 0; k < in.size(); ++k)
    {
        pass = pass && (out[k] == check[k]);
    }

    Test.Check(pass) << "Sequence values are correct" << endl;
}

template<class TOut, class TIn>
void testAbs_SequenceValues(const Sequence<TOut>& out,
                            const Sequence<TIn>& in)
{
    valarray<TOut> check(in.size());

    for (size_t k = 0; k < in.size(); ++k)
    {
        check[k] = std::abs(in[k]);
    }

    bool pass = true;

    for (size_t k = 0; k < in.size(); ++k)
    {
        pass = pass && (out[k] == check[k]);
    }

    Test.Check(pass) << "Sequence values are correct" << endl;
}

template<class TOut, class TIn>
void testArg_SequenceValues(const Sequence<TOut>& out,
                            const Sequence<TIn>& in)
{
    bool pass = true;

    for (size_t k = 0; k < in.size(); ++k)
    {
        pass = pass && (out[k] == 0);
    }

    Test.Check(pass) << "Sequence values are correct" << endl;
}

template<class T>
void testArg_SequenceValues(const Sequence<T>& out,
                            const Sequence<complex<T> >& in)
{
    valarray<T> check(in.size());

    for (size_t k = 0; k < in.size(); ++k)
    {
        check[k] = std::arg(in[k]);
    }

    bool pass = true;

    for (size_t k = 0; k < in.size(); ++k)
    {
        pass = pass && (out[k] == check[k]);
    }

    Test.Check(pass) << "Sequence values are correct" << endl;
}
                        
template<class T>
void testReal_Sequence2()
{
    typedef typename Trait<T>::ResultType TOut;
    
    Test.Message() << "Input type \"" << typeName<T>()
                   << "\", output type \"" << typeName<TOut>()
                   << "\""
                   << endl;

    Sequence<T> in;

    fillSequence(in);
    in.SetName("Sequence");
   
    udt* p = datacondAPI::Real(in);

    Test.Check(typeid(*p) == typeid(Sequence<TOut>))
        << "Dynamic type is correct"
        << endl;
    
    const Sequence<TOut>& out = dynamic_cast<Sequence<TOut>&>(*p);

    testReal_SequenceValues(out, in);
}

void
testReal_Sequence()
{
    Test.Message() << "Testing Real() on Sequence<T>" << endl;

    testReal_Sequence2<float>();
    testReal_Sequence2<double>();
    testReal_Sequence2<complex<float> >();
    testReal_Sequence2<complex<double> >();
}


template<class T>
void testReal_TimeSeries2()
{
    typedef typename Trait<T>::ResultType TOut;
    
    Test.Message() << "Input type \"" << typeName<T>()
                   << "\", output type \"" << typeName<TOut>()
                   << "\""
                   << endl;

    TimeSeries<T> in;

    fillSequence(in);
   
    in.SetSampleRate(16384);
    in.SetStartTime(General::GPSTime(60000,100));
    in.SetName("TimeSeries");

    udt* p = datacondAPI::Real(in);

    Test.Check(typeid(*p) == typeid(TimeSeries<TOut>))
        << "Dynamic type is correct"
        << endl;
    
    TimeSeries<TOut>& out = dynamic_cast<TimeSeries<TOut>&>(*p);

    testReal_SequenceValues(out, in);

    Test.Check(static_cast<TimeSeriesMetaData&>(out) == static_cast<TimeSeriesMetaData&>(in)) << "TimeSeriesMetaData is correct" << endl;
}


void
testReal_TimeSeries()
{
    Test.Message() << "Testing Real() on TimeSeries<T>" << endl;

    testReal_TimeSeries2<float>();
    testReal_TimeSeries2<double>();
    testReal_TimeSeries2<complex<float> >();
    testReal_TimeSeries2<complex<double> >();
}

template<class TOut, class TIn>
void testReal_Generic()
{
    TIn in;

    fillSequence(in);
    
    udt* p = datacondAPI::Real(in);

    Test.Check(typeid(*p) == typeid(TOut)) << "Dynamic type is correct"
                                           << endl;
    
    const TOut& out = dynamic_cast<TOut&>(*p);
    testReal_SequenceValues(out, in);
}

template<class TOut, class TIn>
void testImag_Generic()
{
    TIn in;

    fillSequence(in);
    
    udt* p = datacondAPI::Imag(in);

    Test.Check(typeid(*p) == typeid(TOut)) << "Dynamic type is correct"
                                           << endl;
    
    const TOut& out = dynamic_cast<TOut&>(*p);
    testImag_SequenceValues(out, in);
}

template<class TOut, class TIn>
void testAbs_Generic()
{
    TIn in;

    fillSequence(in);
    
    udt* p = datacondAPI::Abs(in);

    Test.Check(typeid(*p) == typeid(TOut)) << "Dynamic type is correct"
                                           << endl;
    
    const TOut& out = dynamic_cast<TOut&>(*p);
    testAbs_SequenceValues(out, in);
}

template<class TOut, class TIn>
void testArg_Generic()
{
    TIn in;

    fillSequence(in);
    
    udt* p = datacondAPI::Arg(in);

    Test.Check(typeid(*p) == typeid(TOut)) << "Dynamic type is correct"
                                           << endl;
    
    const TOut& out = dynamic_cast<TOut&>(*p);
    testArg_SequenceValues(out, in);
}

int
main(int ArgC, char** ArgV) 
{
  Test.Init(ArgC, ArgV);

  if (Test.IsVerbose())
  {
    cout << "$Id: tRealImagAbs.cc,v 1.4 2005/12/01 22:55:03 emaros Exp $" << endl;
  }

  try {

      //
      // Real tests
      //

      testReal_Sequence();
      testReal_TimeSeries();

      Test.Message() << "Testing Real() on FrequencySequence<T>" << endl;
      testReal_Generic<FrequencySequence<float>,
          FrequencySequence<float> >();
      testReal_Generic<FrequencySequence<float>,
          FrequencySequence<complex<float> > >();
      testReal_Generic<FrequencySequence<double>,
          FrequencySequence<double> >();
      testReal_Generic<FrequencySequence<double>,
          FrequencySequence<complex<double> > >();

      Test.Message() << "Testing Real() on TimeBoundedFreqSequence<T>" << endl;
      testReal_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<float> >();
      testReal_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<complex<float> > >();
      testReal_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<double> >();
      testReal_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<complex<double> > >();

      Test.Message() << "Testing Real() on CSDSpectrum<T>" << endl;
      testReal_Generic<CSDSpectrum<float>,
          CSDSpectrum<float> >();
      testReal_Generic<CSDSpectrum<float>,
          CSDSpectrum<complex<float> > >();
      testReal_Generic<CSDSpectrum<double>,
          CSDSpectrum<double> >();
      testReal_Generic<CSDSpectrum<double>,
          CSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Real() on WelchSpectrum<T>" << endl;
      testReal_Generic<WelchSpectrum<float>,
          WelchSpectrum<float> >();
      testReal_Generic<WelchSpectrum<double>,
          WelchSpectrum<double> >();

      Test.Message() << "Testing Real() on WelchCSDSpectrum<T>" << endl;
      testReal_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<float> >();
      testReal_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<complex<float> > >();
      testReal_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<double> >();
      testReal_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Real() on DFT<T>" << endl;
      testReal_Generic<Sequence<float>,
          DFT<complex<float> > >();
      testReal_Generic<Sequence<double>,
          DFT<complex<double> > >();

      //
      // Imag tests
      //

      Test.Message() << "Testing Imag() on Sequence<T>" << endl;
      testImag_Generic<Sequence<float>,
          Sequence<float> >();
      testImag_Generic<Sequence<float>,
          Sequence<complex<float> > >();
      testImag_Generic<Sequence<double>,
          Sequence<double> >();
      testImag_Generic<Sequence<double>,
          Sequence<complex<double> > >();

      Test.Message() << "Testing Imag() on TimeSeries<T>" << endl;
      testImag_Generic<TimeSeries<float>,
          TimeSeries<float> >();
      testImag_Generic<TimeSeries<float>,
          TimeSeries<complex<float> > >();
      testImag_Generic<TimeSeries<double>,
          TimeSeries<double> >();
      testImag_Generic<TimeSeries<double>,
          TimeSeries<complex<double> > >();

      Test.Message() << "Testing Imag() on FrequencySequence<T>" << endl;
      testImag_Generic<FrequencySequence<float>,
          FrequencySequence<float> >();
      testImag_Generic<FrequencySequence<float>,
          FrequencySequence<complex<float> > >();
      testImag_Generic<FrequencySequence<double>,
          FrequencySequence<double> >();
      testImag_Generic<FrequencySequence<double>,
          FrequencySequence<complex<double> > >();

      Test.Message() << "Testing Imag() on TimeBoundedFreqSequence<T>" << endl;
      testImag_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<float> >();
      testImag_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<complex<float> > >();
      testImag_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<double> >();
      testImag_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<complex<double> > >();

      Test.Message() << "Testing Imag() on CSDSpectrum<T>" << endl;
      testImag_Generic<CSDSpectrum<float>,
          CSDSpectrum<float> >();
      testImag_Generic<CSDSpectrum<float>,
          CSDSpectrum<complex<float> > >();
      testImag_Generic<CSDSpectrum<double>,
          CSDSpectrum<double> >();
      testImag_Generic<CSDSpectrum<double>,
          CSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Imag() on WelchSpectrum<T>" << endl;
      testImag_Generic<WelchSpectrum<float>,
          WelchSpectrum<float> >();
      testImag_Generic<WelchSpectrum<double>,
          WelchSpectrum<double> >();

      Test.Message() << "Testing Imag() on WelchCSDSpectrum<T>" << endl;
      testImag_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<float> >();
      testImag_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<complex<float> > >();
      testImag_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<double> >();
      testImag_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Imag() on DFT<T>" << endl;
      testImag_Generic<Sequence<float>,
          DFT<complex<float> > >();
      testImag_Generic<Sequence<double>,
          DFT<complex<double> > >();

      //
      // Abs tests
      //

      Test.Message() << "Testing Abs() on Sequence<T>" << endl;
      testAbs_Generic<Sequence<float>,
          Sequence<float> >();
      testAbs_Generic<Sequence<float>,
          Sequence<complex<float> > >();
      testAbs_Generic<Sequence<double>,
          Sequence<double> >();
      testAbs_Generic<Sequence<double>,
          Sequence<complex<double> > >();

      Test.Message() << "Testing Abs() on TimeSeries<T>" << endl;
      testAbs_Generic<TimeSeries<float>,
          TimeSeries<float> >();
      testAbs_Generic<TimeSeries<float>,
          TimeSeries<complex<float> > >();
      testAbs_Generic<TimeSeries<double>,
          TimeSeries<double> >();
      testAbs_Generic<TimeSeries<double>,
          TimeSeries<complex<double> > >();

      Test.Message() << "Testing Abs() on FrequencySequence<T>" << endl;
      testAbs_Generic<FrequencySequence<float>,
          FrequencySequence<float> >();
      testAbs_Generic<FrequencySequence<float>,
          FrequencySequence<complex<float> > >();
      testAbs_Generic<FrequencySequence<double>,
          FrequencySequence<double> >();
      testAbs_Generic<FrequencySequence<double>,
          FrequencySequence<complex<double> > >();

      Test.Message() << "Testing Abs() on TimeBoundedFreqSequence<T>" << endl;
      testAbs_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<float> >();
      testAbs_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<complex<float> > >();
      testAbs_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<double> >();
      testAbs_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<complex<double> > >();

      Test.Message() << "Testing Abs() on CSDSpectrum<T>" << endl;
      testAbs_Generic<CSDSpectrum<float>,
          CSDSpectrum<float> >();
      testAbs_Generic<CSDSpectrum<float>,
          CSDSpectrum<complex<float> > >();
      testAbs_Generic<CSDSpectrum<double>,
          CSDSpectrum<double> >();
      testAbs_Generic<CSDSpectrum<double>,
          CSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Abs() on WelchSpectrum<T>" << endl;
      testAbs_Generic<WelchSpectrum<float>,
          WelchSpectrum<float> >();
      testAbs_Generic<WelchSpectrum<double>,
          WelchSpectrum<double> >();

      Test.Message() << "Testing Abs() on WelchCSDSpectrum<T>" << endl;
      testAbs_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<float> >();
      testAbs_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<complex<float> > >();
      testAbs_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<double> >();
      testAbs_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Abs() on DFT<T>" << endl;
      testAbs_Generic<Sequence<float>,
          DFT<complex<float> > >();
      testAbs_Generic<Sequence<double>,
          DFT<complex<double> > >();

      //
      // Arg tests
      //

      Test.Message() << "Testing Arg() on Sequence<T>" << endl;
      testArg_Generic<Sequence<float>,
          Sequence<float> >();
      testArg_Generic<Sequence<float>,
          Sequence<complex<float> > >();
      testArg_Generic<Sequence<double>,
          Sequence<double> >();
      testArg_Generic<Sequence<double>,
          Sequence<complex<double> > >();

      Test.Message() << "Testing Arg() on TimeSeries<T>" << endl;
      testArg_Generic<TimeSeries<float>,
          TimeSeries<float> >();
      testArg_Generic<TimeSeries<float>,
          TimeSeries<complex<float> > >();
      testArg_Generic<TimeSeries<double>,
          TimeSeries<double> >();
      testArg_Generic<TimeSeries<double>,
          TimeSeries<complex<double> > >();

      Test.Message() << "Testing Arg() on FrequencySequence<T>" << endl;
      testArg_Generic<FrequencySequence<float>,
          FrequencySequence<float> >();
      testArg_Generic<FrequencySequence<float>,
          FrequencySequence<complex<float> > >();
      testArg_Generic<FrequencySequence<double>,
          FrequencySequence<double> >();
      testArg_Generic<FrequencySequence<double>,
          FrequencySequence<complex<double> > >();

      Test.Message() << "Testing Arg() on TimeBoundedFreqSequence<T>" << endl;
      testArg_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<float> >();
      testArg_Generic<TimeBoundedFreqSequence<float>,
          TimeBoundedFreqSequence<complex<float> > >();
      testArg_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<double> >();
      testArg_Generic<TimeBoundedFreqSequence<double>,
          TimeBoundedFreqSequence<complex<double> > >();

      Test.Message() << "Testing Arg() on CSDSpectrum<T>" << endl;
      testArg_Generic<CSDSpectrum<float>,
          CSDSpectrum<float> >();
      testArg_Generic<CSDSpectrum<float>,
          CSDSpectrum<complex<float> > >();
      testArg_Generic<CSDSpectrum<double>,
          CSDSpectrum<double> >();
      testArg_Generic<CSDSpectrum<double>,
          CSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Arg() on WelchSpectrum<T>" << endl;
      testArg_Generic<WelchSpectrum<float>,
          WelchSpectrum<float> >();
      testArg_Generic<WelchSpectrum<double>,
          WelchSpectrum<double> >();

      Test.Message() << "Testing Arg() on WelchCSDSpectrum<T>" << endl;
      testArg_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<float> >();
      testArg_Generic<WelchCSDSpectrum<float>,
          WelchCSDSpectrum<complex<float> > >();
      testArg_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<double> >();
      testArg_Generic<WelchCSDSpectrum<double>,
          WelchCSDSpectrum<complex<double> > >();

      Test.Message() << "Testing Arg() on DFT<T>" << endl;
      testArg_Generic<Sequence<float>,
          DFT<complex<float> > >();
      testArg_Generic<Sequence<double>,
          DFT<complex<double> > >();

  }
  catch (const std::exception& e)
  {
    Test.Check(false) << "Unexpected exception: " << e.what() << endl;
  }
  catch(...)
  {
    Test.Check(false) << "Unknown exception" << endl;
  }
  
  Test.Exit();
}
