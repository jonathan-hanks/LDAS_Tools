#include "datacondAPI/config.h"

#include <unistd.h>
#include <valarray>
#include <stdexcept>
#include <complex>

#include "general/unittest.h"

#include "fft.hh"
#include "ifft.hh"

#include "SequenceUDT.hh"
#include "DFTUDT.hh"

#include "fft_check.hh"

using std::complex;
using namespace datacondAPI;

// This may be too small
const double TOLERANCE = 1E-05;

General::UnitTest Test;

void
TestCaseA001()
{
    std::string what("A001: FFT constructor");

    FFT fft;

    Test.Check( true ) << "Construction: " << &fft << std::endl;
}

void
TestCaseA002()
{
    std::string what("A002: FFT::apply() - FFT of real 1-d data, n = 1");
    
    const size_t n = 1;
    const double tdata[n] = {
	0.5
    };

    complex<double> fdata[n] = {
        complex<double>(0.5, 0.0)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseA003()
{
    std::string what("A003: FFT::apply() - FFT of real 1-d data, n = 2");
    
    const size_t n = 2;
    const double tdata[n] = {
        0.0000000000000,
        -1.6225424859374
    };

    complex<double> fdata[n] = {
        complex<double>(1.6225424859374, 0.0000000000000),
        complex<double>(-1.6225424859374, 0.0000000000000)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseA004()
{
    std::string what("A004: FFT::apply() - FFT of real 1-d data, n = 3");
    
    const size_t n = 3;
    const double tdata[n] = {
        0.0000000000000,
        0.3420567567975,
        -0.1273784400710
    };

    complex<double> fdata[n] = {
        complex<double>(-0.1073391583632, 0.4065428059186),
        complex<double>(0.2146783167265, 0.0000000000000),
        complex<double>(-0.1073391583632, -0.4065428059186)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseA005()
{
    std::string what("A005: FFT::apply() - FFT of real 1-d data, n = 8");

    const size_t n = 8;
    const double tdata[n] = {
        0.0000000000000,
        0.9249743361524,
        1.9989505464822,
        -0.8718239319649,
        -1.6225424859374,
        0.0831024961533,
        -0.1778578459743,
        1.0020836236942
    };

    complex<double> fdata[n] = {
        complex<double>(-0.9397863094646, 0.0000000000000),
        complex<double>(-0.2978035409393, -2.9065678454264),
        complex<double>(-3.4436351864452, 0.8778171405764),
        complex<double>(3.5428885128141, 1.4470489394866),
        complex<double>(1.3368867386055, 0.0000000000000),
        complex<double>(3.5428885128141, -1.4470489394866),
        complex<double>(-3.4436351864452, -0.8778171405764),
        complex<double>(-0.2978035409393, 2.9065678454264)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseA006()
{
    std::string what("A006: FFT::apply() - FFT of real 1-d data, n = 13");

    const size_t n = 13;
    const double tdata[n] = {
        0.0000000000000,
        0.3889378004670,
        0.2037230188996,
        2.3663592709158,
        0.1831046214343,
        -0.8863487432987,
        0.1490192261770,
        -1.0608146709072,
        -0.4740926420061,
        -1.4249953813559,
        -0.2042575726337,
        1.9966318958862,
        -0.0210059012588
    };

    complex<double> fdata[n] = {
        complex<double>(-0.3602393010549, -0.2739728293039),
        complex<double>(1.5425560193023, 5.1052114848906),
        complex<double>(0.7902434170799, 1.0769580147805),
        complex<double>(-4.0487288957951, -2.7100181151349),
        complex<double>(-2.7122916943733, -1.9431880963616),
        complex<double>(4.1803299936814, 2.7866048746098),
        complex<double>(1.2162609223195, 0.0000000000000),
        complex<double>(4.1803299936814, -2.7866048746098),
        complex<double>(-2.7122916943733, 1.9431880963616),
        complex<double>(-4.0487288957951, 2.7100181151349),
        complex<double>(0.7902434170799, -1.0769580147805),
        complex<double>(1.5425560193023, -5.1052114848906),
        complex<double>(-0.3602393010549, 0.2739728293039)
    };

    TestFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB002()
{
    std::string what("B002: IFFT::apply() - IFFT of real 1-d data, n = 1");
    
    const size_t n = 1;
    const double tdata[n] = {
	0.5
    };

    complex<double> fdata[n] = {
        complex<double>(0.5, 0.0)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB003()
{
    std::string what("B003: IFFT::apply() - IFFT of real 1-d data, n = 2");
    
    const size_t n = 2;
    const double tdata[n] = {
        0.0000000000000,
        -1.6225424859374
    };

    complex<double> fdata[n] = {
        complex<double>(1.6225424859374, 0.0000000000000),
        complex<double>(-1.6225424859374, 0.0000000000000)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB004()
{
    std::string what("B004: IFFT::apply() - IFFT of real 1-d data, n = 3");
    
    const size_t n = 3;
    const double tdata[n] = {
        0.0000000000000,
        0.3420567567975,
        -0.1273784400710
    };

    complex<double> fdata[n] = {
        complex<double>(-0.1073391583632, 0.4065428059186),
        complex<double>(0.2146783167265, 0.0000000000000),
        complex<double>(-0.1073391583632, -0.4065428059186)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB005()
{
    std::string what("B005: IFFT::apply() - IFFT of real 1-d data, n = 8");

    const size_t n = 8;
    const double tdata[n] = {
        0.0000000000000,
        0.9249743361524,
        1.9989505464822,
        -0.8718239319649,
        -1.6225424859374,
        0.0831024961533,
        -0.1778578459743,
        1.0020836236942
    };

    complex<double> fdata[n] = {
        complex<double>(-0.9397863094646, 0.0000000000000),
        complex<double>(-0.2978035409393, -2.9065678454264),
        complex<double>(-3.4436351864452, 0.8778171405764),
        complex<double>(3.5428885128141, 1.4470489394866),
        complex<double>(1.3368867386055, 0.0000000000000),
        complex<double>(3.5428885128141, -1.4470489394866),
        complex<double>(-3.4436351864452, -0.8778171405764),
        complex<double>(-0.2978035409393, 2.9065678454264)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseB006()
{
    std::string what("B006: IFFT::apply() - IFFT of real 1-d data, n = 13");

    const size_t n = 13;
    const double tdata[n] = {
        0.0000000000000,
        0.3889378004670,
        0.2037230188996,
        2.3663592709158,
        0.1831046214343,
        -0.8863487432987,
        0.1490192261770,
        -1.0608146709072,
        -0.4740926420061,
        -1.4249953813559,
        -0.2042575726337,
        1.9966318958862,
        -0.0210059012588
    };

    complex<double> fdata[n] = {
        complex<double>(-0.3602393010549, -0.2739728293039),
        complex<double>(1.5425560193023, 5.1052114848906),
        complex<double>(0.7902434170799, 1.0769580147805),
        complex<double>(-4.0487288957951, -2.7100181151349),
        complex<double>(-2.7122916943733, -1.9431880963616),
        complex<double>(4.1803299936814, 2.7866048746098),
        complex<double>(1.2162609223195, 0.0000000000000),
        complex<double>(4.1803299936814, -2.7866048746098),
        complex<double>(-2.7122916943733, 1.9431880963616),
        complex<double>(-4.0487288957951, 2.7100181151349),
        complex<double>(0.7902434170799, -1.0769580147805),
        complex<double>(1.5425560193023, -5.1052114848906),
        complex<double>(-0.3602393010549, 0.2739728293039)
    };

    TestIFFT(tdata, fdata, n, TOLERANCE, Test);
}

void
TestCaseC002()
{
    std::string what("C002: (I)FFT::apply() - FFT/IFFT of real 1-d data, n = 1");
    
    const size_t n = 1;
    const double tdata[n] = {
	0.5
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseC003()
{
    std::string what("C003: (I)FFT::apply() - FFT/IFFT of real 1-d data, n = 2");
    
    const size_t n = 2;
    const double tdata[n] = {
        0.0000000000000,
        -1.6225424859374
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseC004()
{
    std::string what("C004: (I)FFT::apply() - FFT/IFFT of real 1-d data, n = 3");
    
    const size_t n = 3;
    const double tdata[n] = {
        0.0000000000000,
        0.3420567567975,
        -0.1273784400710
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseC005()
{
    std::string what("C005: (I)FFT::apply() - FFT/IFFT of real 1-d data, n = 8");

    const size_t n = 8;
    const double tdata[n] = {
        0.0000000000000,
        0.9249743361524,
        1.9989505464822,
        -0.8718239319649,
        -1.6225424859374,
        0.0831024961533,
        -0.1778578459743,
        1.0020836236942
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseC006()
{
    std::string what("C006: (I)FFT::apply() - FFT/IFFT of real 1-d data, n = 13");

    const size_t n = 13;
    const double tdata[n] = {
        0.0000000000000,
        0.3889378004670,
        0.2037230188996,
        2.3663592709158,
        0.1831046214343,
        -0.8863487432987,
        0.1490192261770,
        -1.0608146709072,
        -0.4740926420061,
        -1.4249953813559,
        -0.2042575726337,
        1.9966318958862,
        -0.0210059012588
    };

    TestFFTandIFFT(tdata, n, TOLERANCE, Test);
}

void
TestCaseE001()
{
    std::string what("E001: FFT apply exceptions - size 0 input array");

    FFT				fft;
    Sequence<complex<double> >	fft_in;
    udt*			fft_out = 0;

    try
    {
	fft.apply(fft_out, fft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception: "
			 << e.what() << std::endl;
	return;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;
}

void
TestCaseE002()
{
    std::string what("E002: FFT apply exceptions - size 0 input array");

    FFT			fft;
    Sequence<double >	fft_in;
    udt*		fft_out = 0;

    try
    {
	fft.apply(fft_out, fft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception: "
			 << e.what() << std::endl;
	return;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;
}

void
TestCaseE003()
{
    std::string what("E003: IFFT apply exceptions - size 0 input array");

    IFFT			ifft;
    DFT<complex<double> >	ifft_in;
    udt* 			ifft_out = 0;

    try
    {
	ifft.apply(ifft_out, ifft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception: "
			 << e.what() << std::endl;
	return;
    }

    Test.Check(false) << "Didn't catch exception" << std::endl;

}

void
TestCaseE004()
{
    std::string what("E004: IFFT apply exceptions - size 0 input array");

    const size_t		n(7);
    IFFT			ifft;
    DFT<complex<double> >	ifft_in;
    Sequence<double >		ifft_out(n);
    udt*			p_ifft_out = 0;

    try
    {
	ifft.apply(p_ifft_out, ifft_in);
    }
    catch(std::invalid_argument& e)
    {
        Test.Check(true) << "Caught exception: "
			 << e.what() << std::endl;
	return;
    }
    Test.Check(false) << "Didn't catch exception" << std::endl;
}

int
TestCaseF001()
{
    int result = 0;

#if 0
    std::string what("F001: purging of used plans");

    PlanDB planDB;

    planDB.purge_plans(planDB.size());

    Test.Check(planDB.size() == 0) << "Plan database is empty" << std::endl;

    planDB.set_max_size(4);

    Test.Check(planDB.max_size() == 4) << "Plan database max size is 4"
				       << std::endl;

    FFT fft;
    
    Sequence<double> fft_in(8);
    udt* fft_out = 0;
    
    fft.apply(fft_out, fft_in);

    fft_in.resize(8);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 1) << "Plan database has 1 element" << std::endl;

    sleep(1);

    fft_in.resize(7);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 2) << "Plan database has 2 elements" << std::endl;

    sleep(1);

    fft_in.resize(6);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 3) << "Plan database has 3 elements" << std::endl;

    sleep(1);

    // Re-use the 8-plan to update it's use time
    fft_in.resize(8);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 3) << "Plan database has 3 elements" << std::endl;

    sleep(1);

    fft_in.resize(5);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() == 4) << "Plan database has 4 elements" << std::endl;

    sleep(1);

    fft_in.resize(4);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() <= planDB.max_size())
	<< "Plan db size is smaller than max_size" << std::endl;

    sleep(1);

    fft_in.resize(3);
    fft.apply(fft_out, fft_in);
    Test.Check(planDB.size() <= planDB.max_size())
	<< "Plan db size is smaller than max_size" << std::endl;

    // Oldest plans (7 and 6) should have aged out by now
    size_t count = planDB.count(std::valarray<size_t>(7,1),
				planType<DRealFwdPlanH>(),
				FFTW_ESTIMATE);

    Test.Check(count == 0) << "Plan 7 is gone" << std::endl;

    count = planDB.count(std::valarray<size_t>(6,1),
			 planType<DRealFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 0) << "Plan 6 is gone" << std::endl;

    count = planDB.count(std::valarray<size_t>(8,1),
			 planType<DRealFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 8 is left" << std::endl;

    count = planDB.count(std::valarray<size_t>(5,1), planType<DRealFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 5 is left" << std::endl;

    count = planDB.count(std::valarray<size_t>(4,1), planType<DRealFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 4 is left" << std::endl;

    count = planDB.count(std::valarray<size_t>(3,1), planType<DRealFwdPlanH>(),
			 FFTW_ESTIMATE);

    Test.Check(count == 1) << "Plan 3 is left" << std::endl;

    planDB.purge_plans(planDB.size());

    Test.Check(planDB.size() == 0) << "All plans removed" << std::endl;

    delete fft_out;
#endif
    return result;
}

int
main(int argc, char** argv)
{
    Test.Init(argc, argv);

    try
    {
	TestCaseA001();
	TestCaseA002();
	TestCaseA003();
	TestCaseA004();
	TestCaseA005();
	TestCaseA006();
	
	TestCaseB002();
	TestCaseB003();
	TestCaseB004();
	TestCaseB005();
	TestCaseB006();

	TestCaseC002();
	TestCaseC003();
	TestCaseC004();
	TestCaseC005();
	TestCaseC006();

	TestCaseE001();
	TestCaseE002();
	TestCaseE003();
	TestCaseE004();

	TestCaseF001();
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Unhandled exception: " 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception" << std::endl;
    }

    //PlanDB().purge_plans(PlanDB().size());

    Test.Exit();
}
