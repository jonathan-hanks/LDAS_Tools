//! author="Edward Maros"
// $Id: pr535.cc,v 1.3 2005/12/01 22:55:02 emaros Exp $

/*
=========================================================================
 Number:        351 
 Category:      data_conditioning_api 
 Synopsis:      mixer unable to handle large data sources without
 	        slicing first. 
 Confidential:  no 
 Severity:      critical 
 Priority:      high 
 Responsible:   dataConditioningAPI 
 State:	        closed
 Class:         sw-bug 
 Submitter-Id:  ldas 
 Arrival-Date:  Thu Aug 03 15:49:00 PDT 2000 
 Closed-Date:   Tue Sep 19 19:33:13 PDT 2000 
 Last-Modified: Tue Sep 19 19:34:00 PDT 2000 
 Originator:    emaros@ligo.caltech.edu 
 Release:       ldas-i.j.k 
 Organization:
 Environment:

 Description:
                 The script described in the How-To-Repeat section causes an
                 ABORT. If there is a line above mix which takes a
                 slice of ch01, "x = slice(ch01, 0, 8388608, 1);", and use
                 x for ch01 in the mix command, the job completes
                 successfully.
=========================================================================
*/

#include "datacondAPI/config.h"

#include "general/unittest.h"
#include "general/util.hh"

#include "genericAPI/registry.hh"

#include "token.hh"

#include "CallChain.hh"

#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

General::UnitTest	Test;

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */
    int main(int argc, char** argv);
#ifdef __cplusplus
}
#endif	/* __cplusplus */

int
main(int argc, char** argv)
{
  Test.Init(argc, argv);
  Test.Message()
    << "$Id: pr535.cc,v 1.3 2005/12/01 22:55:02 emaros Exp $"
    << endl << flush;

  ILwd::LdasArray<INT_4U>	data((INT_4U)(2147483647), "data");

  CallChain	cmds;
  Parameters	args;

  data.setComment("Max Signed Int");

  try
  {
    datacondAPI::Scalar<INT_4U>*	r;
    INT_4U				expect(2147483648U);

    cmds.Reset();
    cmds.IngestILwd(data);
    cmds.AppendCallFunction("integer", args.set(1, "1"), "one");
    cmds.AppendCallFunction("add", args.set(2, "one", "data"), "result");
    cmds.Execute();

    r = dynamic_cast<datacondAPI::Scalar<INT_4U>*>(cmds.GetSymbol("data"));
    Test.Message() << datacondAPI::TypeInfoTable.GetName(typeid(*(cmds.GetSymbol("data"))))
		   << endl << flush;
    Test.Check(r) << "data" << endl << flush;
    if (r)
    {
      Test.Check(r->GetValue() == data.getData()[0])
	<< "result: " << r->GetValue()
	<< " expected: " << data.getData()[0]
      << endl << flush;
    }

    r = dynamic_cast<datacondAPI::Scalar<INT_4U>*>(cmds.GetSymbol("result"));
    Test.Check(r) << "result" << endl << flush;
    if (r)
    {
      Test.Check(r->GetValue() == expect)
	<< "result: " << r->GetValue()
	<< " expected: " << expect
      << endl << flush;
    }
  }
  CATCH(Test);

    

  Test.Exit();
}
