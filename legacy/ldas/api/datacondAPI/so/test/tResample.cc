// $Id: tResample.cc,v 1.50 2006/10/13 19:32:40 emaros Exp $

#include "datacondAPI/config.h"

#include <cmath>
#include <string>
#include <iostream>
#include <iomanip>
#include <stdexcept>

#include "general/unittest.h"

#include "SequenceUDT.hh"
#include "Resample.hh"
#include <filters/LDASConstants.hh>
#include <filters/FIRLP.hh>
#include <filters/KaiserWindow.hh>
#include <filters/gcd.hh>
#include "TimeSeries.hh"

using namespace datacondAPI;
using General::GPSTime;
using namespace std;   

General::UnitTest Test;

// Forward declarations

void mkpath(std::string&);
void Nominal(void);
void Exceptions(void);
void Accessors(void);
void Copy(void);
template <class T>
void Results(void);
template <class T>
void StateTest(void);
template <class T>
void ImpulseResponse(int, int, int, double);
void MetaDataTest();
void FilterTest();

//New Test Declarations
template <class T>
void CXStateTest(void);
template <class T>
void CXImpulseResponse(int, int, int, double);
template <class T>
void CXResults(void);
template <class T>
void GeneralTest(void);

template<class T>
void TestGetB();

template<class T>
double tolerance()
{
    return 1.0e-12;
}

template<>
double tolerance<float>()
{
    return 1.0e-6;
}

int main( int ArgC, char** ArgV)
{
  try{
      std::string id("$Id: tResample.cc,v 1.50 2006/10/13 19:32:40 emaros Exp $");
      Test.Init ( ArgC, ArgV );
      if (Test.IsVerbose())
	Test.Message() << id << std::endl;
      
      Test.Message() << "------- Nominal -------" << std::endl;
      Nominal();
      Test.Message() << "------- Exceptions -------" << std::endl;
      Exceptions();
      Test.Message() << "------- Accessors -------" << std::endl;
      Accessors();
      Test.Message() << "------- Copy -------" << std::endl;
      Copy();
      Test.Message() << "------- StateTest<float> -------" << std::endl;
      StateTest<float>();
      Test.Message() << "------- StateTest<double> -------" << std::endl;
      StateTest<double>();

      Test.Message() << "------- getB<float> -------" << std::endl;
      TestGetB<float>();
      Test.Message() << "------- getB<double> -------" << std::endl;
      TestGetB<double>();
      Test.Message() << "------- getB<complex<float> > -------" << std::endl;
      TestGetB<complex<float> >();
      Test.Message() << "------- getB<complex<double> > -------" << std::endl;
      TestGetB<complex<double> >();

      Test.Message() << "------- CXStateTest<float> -------" << std::endl;
      CXStateTest<float>();
      Test.Message() << "------- CXStateTest<double> -------" << std::endl;
      CXStateTest<double>();
      Test.Message() << "------- ImpulseResponse<double> -------" << std::endl;
      ImpulseResponse<double>(3,1,10,5);
      Test.Message() << "------- ImpulseResponse<double> -------" << std::endl;
      ImpulseResponse<double>(1,3,10,5);
      Test.Message() << "------- ImpulseResponse<double> -------" << std::endl;
      ImpulseResponse<double>(5,3,31,5);
      Test.Message() << "------- CXImpulseResponse<double> -------" << std::endl;
      CXImpulseResponse<double>(3,1,10,5);
      Test.Message() << "------- CXImpulseResponse<double> -------" << std::endl;
      CXImpulseResponse<double>(1,3,10,5);
      Test.Message() << "------- CXImpulseResponse<double> -------" << std::endl;
      CXImpulseResponse<double>(7,5,30,5);
      Test.Message() << "------- Results<float> -------" << std::endl;
      Results<float>();
      Test.Message() << "------- Results<double> -------" << std::endl;
      Results<double>();  
      Test.Message() << "------- CXResults<float> -------" << std::endl;
      CXResults<float>();
      Test.Message() << "------- CXResults<double> -------" << std::endl;
      CXResults<double>();
      Test.Message() << "------- General<float> -------" << std::endl;
      GeneralTest<float>();
      Test.Message() << "------- MetaDataTest -------" << std::endl;
      MetaDataTest();
      Test.Message() << "------- FilterTest -------" << std::endl;
      FilterTest();

      Test.Check(true) << "reached end of test harness" << std::endl;
    } 
  catch(std::exception& x)
    {
      Test.Check(false) << "Caught std::exception " << x.what() << std::endl;
    }
  catch(...)
    {
      Test.Check(false) << "Caught unknown exception" << std::endl;
    }
   
  Test.Exit();

}


void mkpath(std::string& path)
{
  char* srcdir = getenv("DATADIR");
  if (srcdir != 0) path = srcdir;
  else path = ".";
}

void Nominal()
{
  std::string what = "Resample: Initialize no-sample";
  try{
      Resample up(1,1,20,3);
      Test.Check(true, what);
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Check(false) << what << " caught unexpected exception" << std::endl;
    }

  what = "Resample: Initialize upsample";
  try{
      Resample up(3,1,20,3);
      Test.Check(true, what);
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Check(false) << what << " caught unexpected exception" << std::endl;
    }
  
  what = "Resample: Initialize downsample";
  try{
      Resample down(1,2,10,3);
      Test.Check(true, what);
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Check(false) << what << " caught unexpected exception" << std::endl;
    }

  what = "Resample: Initialize general resample";
  try{
      Resample gen(3,2,15,3);
      Test.Check(true, what);
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Check(false) << what << " caught unexpected exception" << std::endl;
    }
  
  what = "Resample: Initialize trivial Resample from ResampleState";
  try{
    ResampleState rstate(2,2,15,3);
    Resample res(rstate);
    Test.Check(true, what);
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Check(false) << what << " caught unexpected exception" << std::endl;
    }

  what = "Resample: Initialize Resample from ResampleState";
  try{
    ResampleState rstate(3,5,15,3);
    Resample res(rstate);
    Test.Check(true, what);
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Check(false) << what << " caught unexpected exception" << std::endl;
    }

  Sequence<double> data(100);
  for(size_t i=0; i<data.size(); i++) data[i] = 0.01 * i;
  
  what = "Resample: Apply nominal values for trivial resampling ";
  Test.Message() << "Reached: " << what << std::endl;
  try{
      Sequence<double> results(1);
      Resample none(1,1,21,3);
      none.apply(results, data);
      what = "Resample: Length of upsampled results match expected value ";
      Test.Check( results.size() == data.size() , what);
      what = "Resample: filter delay is 0";
      Test.Check(none.getDelay() == 0) << what << std::endl;

      Sequence<double> b;
      none.getState().getB(b);
      Test.Check(b.size() == 1) << "Filter for trivial resample has correct size"
                                << endl;
      Test.Check(b[0] == 1) << "Filter for trivial resample has correct values"
                            << endl;
      
      bool pass = true;
      for (size_t k = 0; k < data.size(); ++k)
      {
          if (results[k] != data[k])
          {
              pass = false;
          }
      }
      Test.Check(pass) << "Trivially resampled output values are correct"
                       << endl;
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Message() << "Unexpected exception caught.\n";
    }

  what = "Resample: Apply nominal values for upsampling ";
  Test.Message() << "Reached: " << what << std::endl;
  try{
      Sequence<double> results(1);
      Resample up(3,1,21,3);
      up.apply(results, data);
      what = "Resample: Length of upsampled results match expected value ";
      Test.Check( results.size() == 3 * data.size() , what);
      what = "Resample: filter delay is location of center tap";
      Test.Check(up.getDelay() == 63)
	<< what + "(63 == " <<up.getDelay() << ")" << std::endl;
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Message() << "Unexpected exception caught.\n";
    }
  
  what = "Resample: Apply nominal values for downsampling ";
  Test.Message() << "Reached: " << what << std::endl;  
  try{
      Sequence<double> results(1);
      Resample down(1,2,10,3);
      down.apply(results, data);
      what = "Resample: Length of downsampled results match expected value ";
      Test.Check( results.size() == data.size() / 2 , what );
      what = "Resample: Delay of downsampled data matches expected value ";
      Test.Check( down.getDelay() == 10)
	<< what + "(10 == " <<down.getDelay() << ")" << std::endl;
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Message() << "Unexpected exception caught.\n";
    }
  
  what = "Resample: Apply nominal values for general resampling ";
  Test.Message() << "Reached: " << what << std::endl;  
  try{
    int p = 7;
    int q = 5;
    int n = 29;
    Sequence<double> results(1);
    Resample gen(p,q,n,3);
    gen.apply(results, data);
    what = "Resample: Length of general resampled results match expected value ";
    Test.Check( results.size() == data.size() * p / q , what );
    what = "Resample: Delay of general resampled data matches expected value ";
    double delay = (double)n*std::max(p,q)/q;
    Test.Check( gen.getDelay() == delay)
      << what + "(" << delay << "==" <<gen.getDelay() << ")" << std::endl;
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Message() << "Unexpected exception caught.\n";
    }

  what = "Resample: Apply nominal values from ResampleState ";
  Test.Message() << "Reached: " << what << std::endl;  
  try{
    int p = 2;
    int q = 5;
    int n = 9;
    Sequence<double> results(1);
    ResampleState rstate(p,q,n,3);
    Resample res(rstate);
    res.apply(results, data);
    what = "Resample: Length of general resampled results match expected value ";
    Test.Check( results.size() == data.size() * p / q , what );
    what = "Resample: Delay of general resampled data matches expected value ";
    double delay = (double)n*std::max(p,q)/q;
    Test.Check( res.getDelay() == delay)
      << what + "(" << delay << "==" <<res.getDelay() << ")" << std::endl;
    }
  catch(std::exception& e)
    {
      Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    }
  catch(...)
    {
      Test.Message() << "Unexpected exception caught.\n";
    }
}

void Exceptions()
{
  std::string what = " P < 1 test";
  try
  {
    Resample test(0,2,20,3);
    Test.Check(false, what);
  }
  catch (std::invalid_argument& u)
    {
      Test.Check(true) << what << "(" << u.what() << ")" << std::endl;
    }
  catch (std::exception& u)
    {
      Test.Check(false) << what << "caught unexpected std::exception (" 
        << u.what() << ")" << std::endl;
    }
  catch (...)
    {
      Test.Check(false) << what << "caught unexpected exception" << std::endl;
    }
  
  what = "Resample: Q cannot be less than 1 ";
  try{
    Resample test(2,0,20,3);
    Test.Check(false, what);
  }
  catch(std::invalid_argument& u)
    {
      Test.Check(true) << what << "(" << u.what() << ")" << std::endl;
    }
  catch (std::exception& u)
    {
      Test.Check(false) << what << "caught unexpected std::exception (" 
        << u.what() << ")" << std::endl;
    }
  catch (...)
    {
      Test.Check(false) << what << "caught unexpected exception"                       
        << std::endl;
    }

  what = "Resample: N cannot be less than 1 ";
  try{
    int zero = 0;
    Resample test(2,1,zero,3);
    Test.Check(false, what);
  }
  catch(std::invalid_argument& u)
    {
      Test.Check(true) << what << "(" << u.what() << ")" << std::endl;
    }
    catch (std::exception& u)
    {
        Test.Check(false) << what << "caught unexpected std::exception (" 
	  << u.what() << ")" << std::endl;
    }
     catch (...)
    {
        Test.Check(false) << what << "caught unexpected exception"
	  << std::endl;
    }
  
  what = "Resample: Beta cannot be less than 0 ";
  try{
    Resample test(2,1,20,-3);
    Test.Check(false, what);
  }
  catch (std::invalid_argument& u)
    {
      Test.Check(true) << what << "(" << u.what() << ")" << std::endl;
    }
  catch (std::exception& u)
    {
      Test.Check(false) << what << "caught unexpected std::exception (" 
        << u.what() << ")" << std::endl;
    }
  catch (...)
    {
      Test.Check(false) << what << "caught unexpected exception" << std::endl;
    }

  what = "input divisible by q ";
  try{
    Sequence<double> data(1.0, 20);
    Resample test(1,3,50,4);
    try{
      Sequence<double> results;
      test.apply(results, data);
      Test.Check(false, what);
    }
    catch(std::logic_error& u)
      {
	Test.Check(true) << what << "(" << u.what() << ")" << std::endl;
      }
  }
  catch(std::invalid_argument& u)
    {
      Test.Check(false) << what << "(" << u.what() << ")" << std::endl;
    }
    catch (std::exception& u)
    {
        Test.Check(false) << what << "caught unexpected std::exception (" 
	  << u.what() << ")" << std::endl;
    }
     catch (...)
    {
        Test.Check(false) << what << "caught unexpected exception" << std::endl;
    }

  what = "apply method input";
  try{
    Sequence<double> b(0.5, 3);
    Sequence<double> out;
    Sequence<float> out2;
    Sequence<double> in(0.0, 30);
    in[10] = 1.5;
    Sequence<float> in2(0.0, 30);
    in2[10] = 3.6;

    Resample exper(1, 3, b);
    exper.apply(out, in);
    exper.apply(out2, in2);

  }
  catch(std::invalid_argument& ue)
    {
      Test.Check(true) << what << "(" << ue.what() << ")" << std::endl;
    }

  catch(...)
    {
      Test.Check(false) << what << "Caught unexpected exception." << std::endl;
    }

}

template <class T>
void StateTest()
{
  std::string basename("tResample100.dat");
  std::string path;
  mkpath(path);
  if (path.size() != 0) 
    path+="/";
  path += basename;
  std::ifstream in(path.c_str());

  Sequence<T> data(100);
  for(int i=0; in; i++) in >> data[i];
  
  const double eps = tolerance<T>();

  std::valarray<T> r0, r1, r2;
  
    {
        const Sequence<T>&
	  const_data( static_cast< const Sequence<T>& >( data ) );
        Resample up(3,1,20,7);
        Resample up2(up);

        up.apply(r0,data);
        up2.apply(r1,std::valarray<T>(const_data[std::slice(0,50,1)]));
        up2.apply(r2,std::valarray<T>(const_data[std::slice(50,50,1)]));
        
        Test.Check((r1.size() + r2.size()) == r0.size(),
                   "Upsample split input size check");

        Test.Check(r0.size() == 300,
                   "Upsample output size check");
        
        r0[std::slice(0,150,1)] -= r1;
        r0[std::slice(150,150,1)] -= r2;
        
        bool test = true;
        for ( int k=0; k<300; k++ ) test = test && abs(r0[k]) < eps;

        Test.Check(test,"Upsample split input value test");
    }
            
    {
      const Sequence<T>&
	const_data( static_cast< const Sequence<T>& >( data ) );
      unsigned int Q = 10;
      Resample dn(1,Q,20,7);
      Resample dn2(dn);
      
      dn.apply(r0,data);
      dn2.apply(r1,std::valarray<T>(const_data[std::slice(0,50,1)]));
      dn2.apply(r2,std::valarray<T>(const_data[std::slice(50,50,1)]));
      
      Test.Check((r1.size() + r2.size()) == r0.size(),
		 "Downsample split input size check");
      
      Test.Check( r0.size() == 100/Q,
		 "Downsample output size check");
      
      r0[std::slice(0,50/Q,1)] -= r1;
      r0[std::slice(50/Q,50/Q,1)] -= r2;
      
      bool test = true;
      for ( unsigned int k = 0; k < 100/Q; k++ )
        {
	  //	  Test.Message() << r0[k] << std::endl; 
	  test = test && abs(r0[k]) <= eps;
        }
      Test.Check(test,"Downsample split input value test");
    }

    {
      const Sequence<T>&
	const_data( static_cast< const Sequence<T>& >( data ) );
      unsigned int Q = 10;
      Resample gen(3,Q,20,7);
      Resample gen2(gen);
      
      gen.apply(r0,data);
      gen2.apply(r1,std::valarray<T>(const_data[std::slice(0,50,1)]));
      gen2.apply(r2,std::valarray<T>(const_data[std::slice(50,50,1)]));
      
      Test.Check((r1.size() + r2.size()) == r0.size(),
		 "General resample Split input size check");
      
      Test.Check( r0.size() == 300/Q,
		 "General resample output size check");
      
      r0[std::slice(0,150/Q,1)] -= r1;
      r0[std::slice(150/Q,150/Q,1)] -= r2;
      
      bool test = true;
      for ( unsigned int k = 0; k < 300/Q; k++ )
        {
	  //	  Test.Message() << r0[k] << std::endl; 
	  test = test && abs(r0[k]) <= eps;
        }
      Test.Check(test,"General resample split input value test");
    }
}

template <class T>
void CXStateTest()
{
  std::string basename("tResample100.dat");
  std::string path;
  mkpath(path);
  if (path.size() != 0) 
    path+="/";
  path += basename;
  std::ifstream in(path.c_str());

  Sequence<T> data0(100);
  const Sequence<T>&
    const_data0( static_cast< const Sequence<T>& >( data0 ) );
  for(int i=0; in; i++) in >> data0[i];
  Sequence<std::complex<T> > data(50);
  for(int i=0; i<50; i++) data[i] = std::complex<T>(const_data0[i],data0[i+50]);
  
  double eps = tolerance<T>();

  std::valarray<std::complex<T> > r0, r1, r2;
  
    {
        const Sequence<std::complex<T> >&
	  const_data( static_cast< const Sequence<std::complex<T> >& >( data ) );
        Resample up(3,1,20,7);
        Resample up2(up);

        up.apply(r0,data);
        up2.apply(r1,std::valarray<std::complex<T> >(const_data[std::slice(0,25,1)]));
        up2.apply(r2,std::valarray<std::complex<T> >(const_data[std::slice(25,25,1)]));
        
        Test.Check((r1.size() + r2.size()) == r0.size(),
                   "(complex) Upsample split input size check");

        Test.Check(r0.size() == 150,
                   "(complex) Upsample output size check");
        
        r0[std::slice(0,75,1)] -= r1;
        r0[std::slice(75,75,1)] -= r2;
        
        bool test = true;
        for ( int k=0; k<150; k++ ) test = test && abs(r0[k]) < eps;

        Test.Check(test,"(complex) Upsample split input value test");
    }
            
    {
      const Sequence<std::complex<T> >&
	const_data( static_cast< const Sequence<std::complex<T> >& >( data ) );
      unsigned int Q = 5;
      Resample dn(1,Q,20,7);
      Resample dn2(dn);
      
      dn.apply(r0,data);
      dn2.apply(r1,std::valarray<std::complex<T> >(const_data[std::slice(0,25,1)]));
      dn2.apply(r2,std::valarray<std::complex<T> >(const_data[std::slice(25,25,1)]));
      
      Test.Check((r1.size() + r2.size()) == r0.size(),
		 "(complex) Downsample split input size check ");
      
      Test.Check(r0.size() == 50/Q,
		 "(complex) Downsample output size check");
      
      r0[std::slice(0,25/Q,1)] -= r1;
      r0[std::slice(25/Q,25/Q,1)] -= r2;
      
      bool test = true;
      for ( unsigned int k = 0; k < 50/Q; k++ )
        {
	  //	  Test.Message() << r0[k] << std::endl; 
	  test = test && abs(r0[k]) <= eps;
        }
      
      Test.Check(test,"(complex) Downsample split input value test");
    }

    {
      const Sequence<std::complex<T> >&
	const_data( static_cast< const Sequence<std::complex<T> >& >( data ) );
      unsigned int Q = 5;
      Resample gen(6,Q,20,7);
      Resample gen2(gen);
      
      gen.apply(r0,data);
      gen2.apply(r1,std::valarray<std::complex<T> >(const_data[std::slice(0,25,1)]));
      gen2.apply(r2,std::valarray<std::complex<T> >(const_data[std::slice(25,25,1)]));
      
      Test.Check((r1.size() + r2.size()) == r0.size(),
		 "(complex) General resample split input size check ");
      
      Test.Check(r0.size() == 300/Q,
		 "(complex) General resample output size check");
      
      r0[std::slice(0,150/Q,1)] -= r1;
      r0[std::slice(150/Q,150/Q,1)] -= r2;
      
      bool test = true;
      for ( unsigned int k = 0; k < 300/Q; k++ )
        {
	  //	  Test.Message() << r0[k] << std::endl; 
	  test = test && abs(r0[k]) <= eps;
        }
      
      Test.Check(test,"(complex) General resample split input value test");
    }
}

void Accessors()
{
  Test.Message() << "Accessor tests ..." << std::endl; 
  std::string what;
  try
    {
      Resample a(2,7,13,3);
      int p=0, q=0;
      a.getPQ(p,q);
      what = "getPQ()";
      Test.Check(p==2) << what << " p = " << p << std::endl;
      Test.Check(q==7) << what << " q = " << q << std::endl;
      
      int n;
      double beta;
      a.getNBeta(n,beta);
      what = "getNBeta()";
      Test.Check(n == 13) << what << " n = " << n << std::endl;
      Test.Check(beta == 3.0) << what << "beta = " << beta << std::endl;
    }
  catch(std::exception& e)
  {
    Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
    what = "Resample: Get Q of upsample object";
    Test.Check(false) << what << "(" << e.what() << ")" << std::endl;
  }
  catch (...)
  {
    Test.Check(false) << what << "caught unexpected exception" << std::endl;
  }
}

void Copy()
{
  std::string what = "Copy object ";
  
  std::string basename("tResample100.dat");
  std::string path;
  mkpath(path);
  if (path.size() != 0)  path += "/";
  path += basename;

  std::ifstream in(path.c_str());
  Sequence<double> data(100);
  int i=0;
  while(in)
    {
      in >> data[i];
      i++;
    }
  
  try{
    Resample a(2,1,20,3);
    Resample b(a);
    
    Sequence<double> out1, out2;
    
    a.apply(out1, data);
    b.apply(out2, data);
    
    Test.Check(out1.size() == out2.size(), what + "result size check");
    
    bool test = true;
    for(size_t i=0; i<out1.size() && test; i++) test = (out1[i] == out2[i]);
    Test.Check(test, what + "equality check");
  }
  catch(std::exception& e)
  {
    Test.Check(false) <<  what << "(" << e.what() << ")" << std::endl;
  }
  catch (...)
  {
    Test.Check(false) << what << "caught unexpected exception"
		      << std::endl;
  }
}

template <class T>
void ImpulseResponse(int p, int q, int n, double beta)
{
    try
    {
        Test.Message() <<"("<<p<<", "<<q<<", "<<n<<", "<<beta<<")"<< std::endl;
    
        std::valarray<T> x(0.0,2*n*p*q);
        x[0] = 1.0;
        Resample rsmpl(p,q,n,beta);
        std::valarray<T> y;
        rsmpl.apply(y,x);
	Test.Message() << "Filter delay: " << rsmpl.getDelay() << std::endl;
        for (size_t k = (int)(rsmpl.getDelay()-3); k < rsmpl.getDelay()+4; k++)
        {
	  Test.Message() <<  k << "     " << y[k] << std::endl;
        }
    }
    catch (std::exception& r)
    {
        Test.Check(false) << "ImpulseResponse: " << r.what() << std::endl;
    }
    catch (...)
    {
        Test.Check(false) << "ImpulseResponse: unknown exception" << std::endl;
    }
}

template <class T>
void CXImpulseResponse(int p, int q, int n, double beta)
{
    try
    {
        Test.Message() <<"("<<p<<", "<<q<<", "<<n<<", "<<beta<<")"<<std::endl;
    
        std::valarray<std::complex<T> > x(0.0,2*n*std::max(p,q));
        x[0] = std::complex<T>(1.0,1.0);
	x[0] = x[0]/abs(x[0]);
	Test.Message() << "x[0] = " << x[0] << std::endl;
        Resample rsmpl(p,q,n,beta);
        std::valarray<std::complex<T> > y;
        rsmpl.apply(y,x);
	Test.Message() << "Filter delay: " << rsmpl.getDelay() << std::endl;
        for (size_t k = (int)(rsmpl.getDelay()-3); k < rsmpl.getDelay()+4; k++)
        {
	  Test.Message() <<  k << "     " << y[k] << std::endl;
        }
    }
    catch (std::exception& r)
    {
        Test.Check(false) << "COMPLEX ImpulseResponse: " << r.what() << std::endl;
    }
    catch (...)
    {
        Test.Check(false) << "COMPLEX ImpulseResponse: unknown exception" << std::endl;
    }
}

template <class T>
void Results()
{
    const int n(300), PQ(9), t(13);
    Resample up(PQ,1,21,15);
    Resample down(1,PQ,21,15);

    Sequence<T> a(n);
    for (int i=0; i<n; i++) a[i] = sin(LDAS_TWOPI*i/t);

    Sequence<T> r0, r1;
    up.apply(r0, a);
    down.apply(r1, r0);

    double delay = (up.getDelay())/PQ + down.getDelay();
    Test.Message() << "UpDelay = " << up.getDelay() << std::endl;
    Test.Message() << "DownDelay = " << down.getDelay() << std::endl;
    Test.Message() << "Delay = " << delay << std::endl;
    int d = ((int) delay) % t;

    a = a.cshift(-d);

    bool test = true;
    for(unsigned int i=2*(int)delay; i<r1.size(); i++)
    {
      //Test.Message() << a[i] << '\t' << r1[i] << std::endl;
      test = test && (abs(a[i] - r1[i]) < 1e-6);
    }
    std::string what = "Resample: Up then downsampled data matches the original";
    Test.Check(test, what);
}

template <class T>
void CXResults()
{
    const int n(300), PQ(9), t(13);
    Resample up(PQ,1,21,20);
    Resample down(1,PQ,21,20);

    Sequence<std::complex<T> > z(n);
    T rhomax = 0;
    for (int i=0; i<n; i++)
      {
	T a = sin(LDAS_TWOPI*i/t);
	T b = 2*sin(1+LDAS_TWOPI*i/t);
	z[i] = std::complex<T>(a,b);
	if ( abs(z[i]) > rhomax ) rhomax = abs(z[i]);
      }

    Sequence<std::complex<T> > r0, r1;
    up.apply(r0, z);
    down.apply(r1, r0);

    double delay = (up.getDelay())/PQ + down.getDelay();
    Test.Message() << "UpDelay = " << up.getDelay() << std::endl;
    Test.Message() << "DownDelay = " << down.getDelay() << std::endl;
    Test.Message() << "Delay = " << delay << std::endl;
    int d = ((int)delay) % t;

    z = z.cshift(-d);

    bool test = true;
    for(unsigned int i=2*(int)delay; i<r1.size(); i++)
    {
      //Test.Message() << z[i] << '\t' << r1[i] << '\t'
      //  << abs(z[i] - r1[i]) << std::endl;
      test = test && (abs(z[i] - r1[i]) < rhomax*1e-6);
    }
    std::string what = "Resample: Up then downsampled COMPLEX data matches the original";
    Test.Check(test, what);
}

void MetaDataTest()
{
  const int p_up = 2;
  const int q_up = 1;

  Resample up(p_up, q_up);

  const double srate = 2048.0;
  const GPSTime upStartTime(10000, 0);
  const double up_time_offset = up.getDelay()*q_up/p_up/srate;
  const GPSTime upExpStartTime = upStartTime - up_time_offset;

  TimeSeries<double> in(Sequence<double>(100), 1.0 / 100.0);
  in.SetName("upsamplee");
  in.SetStartTime(upStartTime);
  in.SetSampleRate(srate);

  const std::string upOutName("resample(upsamplee,2,1)");

  udt* pout = 0;

  up.apply(pout, in);

  TimeSeries<double>& out = dynamic_cast<TimeSeries<double>&>(*pout);

  Test.Check(out.name() == upOutName, "Upsample Name");
  Test.Check(out.GetStartTime() == upExpStartTime, "Upsample StartTime");

  Test.Check(out.GetEndTime() == (out.GetStartTime() + out.size()*out.GetStepSize()), "Upsample EndTime");

  Test.Check(out.GetSampleRate() == in.GetSampleRate() * 2.0, "Upsample SampleRate");

  const int p_do = 1;
  const int q_do = 2;

  Resample down(p_do, q_do);

  const GPSTime downStartTime(20000, 0);
  const double down_time_offset = down.getDelay()*q_do/p_do/srate;
  const GPSTime downExpStartTime = downStartTime - down_time_offset;

  in.SetName("downsamplee");
  in.SetStartTime(downStartTime);
  in.SetSampleRate(srate);

  const std::string downOutName("resample(downsamplee,1,2)");

  down.apply(pout, in);

  Test.Check(out.name() == downOutName, "Downsample Name");
  Test.Check(out.GetStartTime() == downExpStartTime, "Downsample StartTime");
  Test.Check(out.GetEndTime() == (out.GetStartTime() + out.size()*out.GetStepSize()), "Downsample EndTime");

  Test.Check(out.GetSampleRate() == in.GetSampleRate() / 2.0, "Downsample SampleRate");

  delete pout;
}

void FilterTest()
{

  //tests the initFilter overloaded function through the 
  //overloaded ResampleState and Resample constructors

  //create an impulse {0,0,1,0,0,0,0,0}
  Sequence<double> impulse(0.0,8);
  impulse[2] = 1.0;

  //input filter {1/2, 1/2}; Haar highpass
  Sequence<double> b(0.5, 2);

  //upsample factor
  int p = 1;

  //downsample factor
  int q = 2;
  
  Resample test(p, q, b);

  //create an output 
  Sequence<double> out(4);
  udt* pout = &out;

  //apply the filter to the impulse and downsample
  test.apply(pout, impulse);

  //output 
  if (Test.IsVerbose())
    {
      Test.Message() << "The impulse is:  ";
      for(size_t i = 0; i < impulse.size(); i++)
	Test.Message( false ) << impulse[i] << " ";
      Test.Message( false ) << std::endl;

      Test.Message() << "The filter is:  ";
      for(size_t i = 0; i < b.size(); i++)
	Test.Message( false ) << b[i] << " ";
      Test.Message( false ) << std::endl;

      Test.Message() << "The response is:  ";
      for(size_t i = 0; i < out.size(); i++)
	Test.Message( false ) << out[i] << " ";
      Test.Message( false ) << std::endl; 
    }
 
  //expected filtered output
  Sequence<double> expected(0.0,4);
  expected[1] = 0.5;

  //find and display difference between expected and actual
  std::valarray<double> difference = expected - out;

  if (Test.IsVerbose())
    {
      Test.Message() << "The difference between expected and actual is:  ";
      for (size_t i = 0; i < difference.size(); i++)
	Test.Message( false ) << difference[i] << " ";
      Test.Message( false ) << std::endl;
    }

  //test to see if output is correct
  std::valarray<bool> check(expected == out);

  bool pass = true;
  for (size_t i = 0; i < check.size(); i++)
    pass = pass && check[i];
     
  Test.Check(pass) << "FilterTest filtered output" << std::endl;

}// end FilterTest

template <class T>
void GeneralTest()
{
  const int p(11), q(10), n(250), t(60);

  Sequence<T> a0(0.0, n);
  for (int i = 0; i < n; i++)
    a0[i] = sin(LDAS_TWOPI*i/t);

  Sequence<T> b;
  Resample gen(p,q,99,15);
  gen.apply(b,a0);
  double delay = gen.getDelay();

  Test.Message() << "Delay = " << delay << std::endl;
  Test.Check(b.size() == n * p/q, "Output has correct size");

  Sequence<T> a1(0.0, n*p/q);
  for (int i = 0; i < n*p/q; i++)
    a1[i] = sin(LDAS_TWOPI*(i-delay)*q/p/t);

  bool test = true;
  for (unsigned int i=(unsigned int)2*(int)delay; i < b.size(); i++)
    {
      test = test && ( abs(b[i]-a1[i]) < 1.0e-05 );
      //Test.Message() << a1[i] << "    " << b[i] << std::endl;
    }
  Test.Check(test, "Resampled data matches stretched/compressed original");
}

template<class T>
void TestGetB(int p, int q, const size_t n, const double beta)
{
  Test.Message()
    << "p = " << p << " "
    << "q = " << q << " "
    << "n = " << n
    << endl;

  const int GCD = Filters::gcd(p, q);
  
  p /= GCD;
  q /= GCD;

  const double fc = 1.0/std::max(p, q);
  const int nOrder = 2*n*std::max(p, q);
  valarray<double> b;

  Filters::FIRLP(fc, nOrder, Filters::KaiserWindow(beta)).apply(b);

  ResampleState r(p, q, n, beta);

  // Make sure everything is the right type
  valarray<T> in(10*p*q);
  valarray<T> out;;
  r.apply(out, in);

  // Now retrieve the filter coeffs
  Sequence<double> b_out;
  r.getB(b_out);

  // Array may have been padded
  Test.Check(b_out.size() >= b.size()) << "Output filter size is correct"
                                       << endl;

  bool pass = true;
  for (size_t k = 0; k < b.size(); ++k)
  {
    // We allow a very small tolerance, since the complex filter coeffs
    // end up being slightly different
    const double diff = abs(b_out[k] - b[k]);
    pass = pass && (diff < 1.0e-12);
  }

  // Padded values must be zero
  for (size_t k = b.size(); k < b_out.size(); ++k)
  {
    pass = pass && (b_out[k] == 0);
  }
  
  Test.Check(pass) << "Output filter values are correct" << endl;
}

template<class T>
void TestGetB()
{
  for (int p = 1; p < 5; ++p)
  {
    for (int q = 1; q < 5; ++q)
    {
      if (p == q)
      {
        break;
      }

      TestGetB<T>(p, q, 10, 5.0);
      TestGetB<T>(p, q, 20, 3.0);

    }
  }

}
