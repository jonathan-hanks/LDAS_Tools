#include "datacondAPI/config.h"

#include <iomanip>

#include <general/unittest.h>

#include "Resample.hh"
#include "TimeSeries.hh"
#include "ShiftState.hh"

#include "token.hh"

using namespace std;      
using namespace General;
using namespace datacondAPI;

UnitTest Test;

namespace
{
  template < class T >
  class ArrayOut
  {
  public:
    ArrayOut( std::ostream& Stream, const std::string& Leader )
      : m_leader( Leader ),
	m_stream( Stream ),
	m_counter( 0 )
    {
    };

      
    void operator()( const T& Data )
    {
      m_stream << m_leader << "[" << m_counter++ << "] = " << Data << std::endl;
    }
  private:
    const std::string	m_leader;
    std::ostream&	m_stream;
    int			m_counter;
  };
}

//
// Tests that for alpha << 1, samples with the same time stamp have the
// the same value
//
template<class T>
void TestTimeStamps(const size_t order, const double offset)
{
    Test.Message() << "Time-stamp test for order = "
                   << order
                   << ", offset = "
                   << offset
                   << endl;

    bool valPass = true;
    bool tsPass = true;

    const int offset_whole = static_cast<int>(std::floor(offset));
    const double alpha = offset - offset_whole;
    Test.Check(alpha < 1.0e-09) << "alpha is small enough" << endl;

    ShiftState<T> state(offset, order);

    const size_t n = 100;
    const double fs = 1;
    const GPSTime initialTime(600000, 0);
    const GPSTime finalTime = initialTime + offset/fs;
    
    const double freq = 1.0;

    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);

    TimeSeries<T> x1(x);
    x1.resize(n/2);
    x1.valarray<T>::operator=(x[slice(0, x1.size(), 1)]);

    TimeSeries<T> x2(x);
    x2.resize(n/2);
    x2.valarray<T>::operator=(x[slice(x1.size(), x2.size(), 1)]);
    x2.SetStartTime(x1.GetStartTime() + x1.size()*x1.GetStepSize());

    TimeSeries<T> y1(x1);
    TimeSeries<T> y2(x2);

    state.apply(y1);
    state.apply(y2);

    size_t expected_y1Size = 0;
    if (alpha == 0)
    {
        expected_y1Size = n/2 - offset_whole;
    }
    else
    {
        expected_y1Size = n/2 - (order + 1)/2 - offset_whole;
    }

    Test.Check(y1.size() == expected_y1Size)
        << "First part of output has correct size"
        << endl;

    Test.Check(y2.size() == n/2)
        << "Second part of output has correct size"
        << endl;

    Test.Check(y2.GetStartTime()
               == (y1.GetStartTime() + y1.size()*y1.GetStepSize()))
               << "Parts of output are continuous in time"
               << endl;

    TimeSeries<T> y(y1);

    y.resize(y1.size() + y2.size());
    y[slice(0, y1.size(), 1)] = y1;
    y[slice(y1.size(), y2.size(), 1)] = y2;

    for (size_t k = 0; k < y.size(); ++k)
    {
        const int xIdx = k + offset_whole;

        const GPSTime tx = x.GetStartTime() + xIdx*x.GetStepSize();

        const GPSTime ty = y.GetStartTime() + k*y.GetStepSize();

        if (tx != ty)
        {
            tsPass = false;
        }

        const double Abs = abs(y[k] - x[xIdx]);

        if (Abs > 1.0e-09)
        {
	    // cerr << "Bad data at k = " << k << "Abs: " << Abs << endl;
            valPass = false;
        }

#if 0
        cerr
            << tx
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << ty
            << "  "
            << "y[" << k << "] = " << setw(5) << y[k]
            << "  "
            << "Abs = " << Abs
            << endl;
#endif

    }
    
    Test.Check(tsPass) << "Time-stamps are correct"
                       << endl;

    Test.Check(valPass) << "Values are correct"
                        << endl;
}

//
// Tests order 1 (linear interpolation), the values are correct
//
template<class T>
void TestLinear(const double offset)
{
    Test.Message() << "Linear interpolation test for offset = "
                   << offset
                   << endl;

    bool valPass = true;

    const size_t order = 1;
    const int offset_whole = static_cast<int>(std::floor(offset));
    const double alpha = offset - offset_whole;

    ShiftState<T> state(offset, order);

    const size_t n = 100;
    const double fs = 16384.0;
    const GPSTime initialTime(600000, 0);
    const GPSTime finalTime = initialTime + offset/fs;
    
    const double freq = 1.0;

    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);

    TimeSeries<T> x1(x);
    x1.resize(n/2);
    x1.valarray<T>::operator=(x[slice(0, x1.size(), 1)]);

    TimeSeries<T> x2(x);
    x2.resize(n/2);
    x2.valarray<T>::operator=(x[slice(x1.size(), x2.size(), 1)]);
    x2.SetStartTime(x1.GetStartTime() + x1.size()*x1.GetStepSize());

    TimeSeries<T> y1(x1);
    TimeSeries<T> y2(x2);

    state.apply(y1);
    state.apply(y2);

    size_t expected_y1Size = 0;
    if (alpha == 0)
    {
        expected_y1Size = n/2 - offset_whole;
    }
    else
    {
        expected_y1Size = n/2 - (order + 1)/2 - offset_whole;
    }

    Test.Check(y1.size() == expected_y1Size)
        << "First part of output has correct size"
        << endl;

    Test.Check(y2.size() == n/2)
        << "Second part of output has correct size"
        << endl;

    Test.Check(y1.GetStartTime() == (x.GetStartTime() + offset*x.GetStepSize()))
               << "Output has correct start time"
               << endl;

    // Allow a 5 nanosecond tolerance for rounding
    Test.Check(abs(y2.GetStartTime() -
               (y1.GetStartTime() + y1.size()*y1.GetStepSize())) < 5.0e-09)
               << "Parts of output are continuous in time"
               << endl;

    TimeSeries<T> y(y1);

    y.resize(y1.size() + y2.size());
    y[slice(0, y1.size(), 1)] = y1;
    y[slice(y1.size(), y2.size(), 1)] = y2;

    for (size_t k = 0; k < y.size(); ++k)
    {
        const int xIdx = k + offset_whole;
        const T xval = ( alpha == 0.0 )
	  ? static_cast< T >( x[xIdx] )
	  : static_cast<T>(1 - alpha)*x[xIdx]
	    + static_cast<T>(alpha)*x[xIdx + 1];
        const double Abs = std::abs(y[k] - xval);

        if (Abs > 1.0e-07)
        {
	  cerr << "Bad data at k = " << k << " Abs: " << Abs << endl;
            valPass = false;
        }

#if 0
        cerr
            << (x.GetStartTime() + (xIdx + alpha)*x.GetStepSize())
            << "  "
            << "xval[" << xIdx << "] = " << setw(5) << xval
            << "   "
            << (y.GetStartTime() + k*y.GetStepSize())
            << "  "
            << "y[" << k << "] = " << setw(5) << y[k]
            << "  "
            << "Abs = " << Abs
            << endl;
#endif

    }
    
    Test.Check(valPass) << "Values are correct"
                        << endl;
}

//
// Tests that shifting of split data gives same results as whole data
//
template<class T>
void TestSplitShift(const size_t order, const double offset)
{
    Test.Message() << "Split data test for order = "
                   << order
                   << ", offset = "
                   << offset
                   << endl;

    bool valPass = true;
    bool tsPass = true;

    const int offset_whole = static_cast<int>(std::floor(offset));
    const double alpha = offset - offset_whole;

    const size_t nn = 50;
    const size_t n = 3*nn;
    const double fs = 2048.0;
    const GPSTime initialTime(600000, 0);
    const GPSTime finalTime = initialTime + offset/fs;
    
    const double freq = 1.0;

    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);

    TimeSeries<T> x1(x);
    x1.resize(nn);
    x1.valarray<T>::operator=(x[slice(0, x1.size(), 1)]);

    TimeSeries<T> x2(x);
    x2.resize(nn);
    x2.valarray<T>::operator=(x[slice(x1.size(), x2.size(), 1)]);
    x2.SetStartTime(x1.GetStartTime() + x1.size()*x1.GetStepSize());

    TimeSeries<T> x3(x);
    x3.resize(nn);
    x3.valarray<T>::operator=(x[slice(x1.size() + x2.size(), x3.size(), 1)]);
    x3.SetStartTime(x2.GetStartTime() + x2.size()*x2.GetStepSize());

    TimeSeries<T> y1(x1);
    TimeSeries<T> y2(x2);
    TimeSeries<T> y3(x3);

    {
        ShiftState<T> state(offset, order);

        state.apply(y1);
        state.apply(y2);
        state.apply(y3);
    }

    size_t expected_y1Size = 0;
    if (alpha == 0)
    {
        expected_y1Size = nn - offset_whole;
    }
    else
    {
        expected_y1Size = nn - (order + 1)/2 - offset_whole;
    }

    Test.Check(y1.size() == expected_y1Size)
        << "First part of output has correct size"
        << endl;

    Test.Check(y2.size() == nn)
        << "Second part of output has correct size"
        << endl;

    Test.Check(y3.size() == nn)
        << "Third part of output has correct size"
        << endl;

    // Allow a 5 nanosecond tolerance for rounding errors
    Test.Check(abs(y2.GetStartTime()
               - (y1.GetStartTime() + y1.size()*y1.GetStepSize())) < 5.0e-09)
               << "Second part of output is continuous in time"
               << endl;

    Test.Check(abs(y3.GetStartTime()
               - (y2.GetStartTime() + y2.size()*y2.GetStepSize())) < 2.0e-09)
               << "Third part of output is continuous in time"
               << endl;

    TimeSeries<T> y(y1);

    y.resize(y1.size() + y2.size() + y3.size());
    y[slice(0, y1.size(), 1)] = y1;
    y[slice(y1.size(), y2.size(), 1)] = y2;
    y[slice(y1.size() + y2.size(), y3.size(), 1)] = y3;

    TimeSeries<T> yFull(x);

    {
        ShiftState<T> state(offset, order);

        state.apply(yFull);
    }

    Test.Check(yFull.size() == y.size())
        << "Full output has same size as split output"
        << endl;

    for (size_t k = 0; k < y.size(); ++k)
    {
        const GPSTime ty = y.GetStartTime() + k*y.GetStepSize();
        const GPSTime tyFull = yFull.GetStartTime() + k*yFull.GetStepSize();

        if (ty != tyFull)
        {
            tsPass = false;
        }

        const double Abs = abs(y[k] - yFull[k]);

        if (Abs > 1.0e-07)
        {
            // cerr << "Bad data at k = " << k << endl;
            valPass = false;
        }

#if 0
        cerr
            << ty
            << "  "
            << "y[" << k << "] = " << setw(5) << y[k]
            << "   "
            << tyFull
            << "  "
            << "yFull[" << k << "] = " << setw(5) << yFull[k]
            << "  "
            << "Abs = " << Abs
            << endl;
#endif

    }
    
    Test.Check(tsPass) << "Time-stamps are correct"
                       << endl;

    Test.Check(valPass) << "Values are correct"
                        << endl;
}

//
// This tests that we have a valid method for correcting fractional delays.
// We upsample by 9/8, giving delay = 90/8 = 11.25, and then attempt
// to correct for the fractional delay of 0.25  so that
//
// a) the tickmarks of the resulting time-series are aligned with the
// tick-marks of a reference time-series with sampling rate original*4/3
//
// b) the input data (a low-frequency sine wave) lines up in time with the
// orignal data
//
template<class T>
void TestResampleDelay(const size_t order)
{
    const int p = 9;
    const int q = 8;
    const int N = 10;

    Resample res(p, q, N);

    const double delay = res.getDelay();
    const double offset = (delay - floor(delay));

    const size_t n = 100*q;
    const double fs = double(q)/double(p);
    const GPSTime initialTime(6000000, 0);
    
    const double resFs = (fs*q)/p;
    const GPSTime finalTime = initialTime
        + (offset - order/2 - 1 - delay)/resFs;

    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);
    TimeSeries<T> y;

    const udt& xUdt = x;
    udt* yUdt = &y;

    res.apply(yUdt, xUdt);

    Test.Check(abs(y.GetSampleRate() - ((x.GetSampleRate()*p)/q)) < 1.0e-6)
        << "Resampled data sample rate is correct" << endl;

    Test.Check((y.GetStartTime() + delay*y.GetStepSize()) == x.GetStartTime())
        << "Resampled data start-time is correct" << endl;

    ShiftState<T> state(offset, order);

    state.apply(y);

#if 0
    Test.Check(y.GetStartTime() == finalTime)
        << "output has correct start-time for order = " << order
        << endl;

    for (size_t k = 0; k < y.size(); ++k)
    {
        const GPSTime tk = x.GetStartTime() + k*x.GetStepSize();
        const int yIdx = int((tk - y.GetStartTime())/y.GetStepSize() + 0.5);

        const double Abs = abs(y[yIdx] - x[k]);

        if (Abs > 1.0e-09)
        {
            pass = false;
        }

        cerr
            << (x.GetStartTime() + k*x.GetStepSize())
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << (y.GetStartTime() + yIdx*y.GetStepSize())
            << "  "
            << "y[" << yIdx << "] = " << setw(5) << y[yIdx]
            << endl;

    }
#endif

#if 0
    for (size_t k = 0; k < min(x.size(), y.size()); ++k)
    {
        cerr
            << (x.GetStartTime() + k*x.GetStepSize())
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << (y.GetStartTime() + k*y.GetStepSize())
            << "  "
            << "y[" << k << "] = " << setw(5) << y[k]
            << endl;
    }
    
    Test.Check(pass) << "Time-stamps are correct for order = " << order
                     << endl;
#endif
}

template<class T>
void TestValues()
{
    const double offset = .1;
    const size_t order = 1;

    ShiftState<T> state(offset, order);

    const size_t n = 25;
    const double fs = 1;
    const GPSTime initialTime(6000000, 0);
    const GPSTime finalTime = initialTime + offset/fs;
    
    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x(data, fs, initialTime);
    TimeSeries<T> y(x);

    state.apply(y);

    Test.Check(y.GetStartTime() == finalTime)
        << "output has correct start-time"
        << endl;

    Test.Message( ) << "y.GetStartTime() = " << y.GetStartTime() << endl;
    Test.Message( ) << "finalTime = " << finalTime << endl;

#if 0
    for (size_t k = 0; k < n; ++k)
    {
        cerr
            << (x.GetStartTime() + k*x.GetStepSize())
            << "  "
            << "x[" << k << "] = " << setw(5) << x[k]
            << "   "
            << (y.GetStartTime() + k*y.GetStepSize())
            << "  "
            << "y[" << k << "] = " << setw(5) << y[k]
            << endl;
    }
#endif /* 0 */

}

template<class T>
void TestShiftState()
{
#if 1

    const size_t maxOrder = 6;
    const size_t maxOffset = 6;

    for (size_t order = 1; order <= maxOrder; ++order)
    {
        for (size_t offset = 0; offset <= maxOffset; ++offset)
        {
            TestTimeStamps<T>(order, offset);
            TestTimeStamps<T>(order, offset + 1.0e-12);

            TestSplitShift<T>(order, offset);
            TestSplitShift<T>(order, offset + 0.01);

            TestSplitShift<T>(order, offset + 0.1);
            TestSplitShift<T>(order, offset + 0.2);

            TestSplitShift<T>(order, offset + 0.5);

            TestSplitShift<T>(order, offset + 0.7);
            TestSplitShift<T>(order, offset + 0.8);

            TestSplitShift<T>(order, offset + 0.99);
        }
    }

    for (size_t offset = 0; offset <= maxOffset; ++offset)
    {
        TestLinear<T>(offset);

        TestLinear<T>(offset + 0.001);

        TestLinear<T>(offset + 0.1);
        TestLinear<T>(offset + 0.2);
        TestLinear<T>(offset + 0.5);
        TestLinear<T>(offset + 0.7);
        TestLinear<T>(offset + 0.9);

        TestLinear<T>(offset + 0.999);
    }

    for (size_t order = 1; order <= 1; ++order)
    {
        TestResampleDelay<T>(order);
    }

#endif
}

template<class T>
void TestShiftAction(const double alpha, const int order)
{
    const size_t n = 50;
    const double fs = 16384;
    const GPSTime initialTime(6000000, 0);
    const GPSTime finalTime = initialTime + alpha/fs;
    
    const int size = n/2;

    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x0(data, fs, initialTime);

    TimeSeries<T>* x = x0.Clone();
    x->SetName("H1:LSC-AS_Q");

    Scalar<double>* Alpha = new Scalar<double>(alpha);
    Scalar<int>* Order = new Scalar<int>(order);

    Scalar<int>* Zero = new Scalar<int>(0);
    Scalar<int>* One = new Scalar<int>(1);
    Scalar<int>* Size = new Scalar<int>(size);

    CallChain chain;
    Parameters args;

    chain.Reset();
    
    // for interpolator
    chain.AddSymbol("x", x);
    chain.AddSymbol("alpha", Alpha);
    chain.AddSymbol("order", Order);
    
    // for slices
    chain.AddSymbol("zero", Zero);
    chain.AddSymbol("one", One);
    chain.AddSymbol("size", Size);

    // y = shift(x, alpha, order);
    chain.AppendCallFunction("shift",
                             args.set(3, "x", "alpha", "order"),
                             "y");

    // x0 = slice(x, 0, size, 1);
    chain.AppendCallFunction("slice",
                             args.set(4, "x", "zero", "size", "one"),
                             "x0");

    // x1 = slice(x, size, size, 1);
    chain.AppendCallFunction("slice",
                             args.set(4, "x", "size", "size", "one"),
                             "x1");

    // y0 = shift(x0, alpha, order, z);
    chain.AppendCallFunction("shift",
                             args.set(4, "x0", "alpha", "order", "z"),
                             "y0");

    // y1 = shift(x1, z);
    chain.AppendCallFunction("shift",
                             args.set(2, "x1", "z"),
                             "y1");

    // Needed so doesn't throw exception
    chain.AppendIntermediateResult("y", "result", "Final Result", "" );

    chain.Execute();

    {
        const udt* const result = chain.GetSymbol("y");
        
        const ILwd::LdasElement* const ilwd
            = result->ConvertToIlwd(chain, datacondAPI::udt::TARGET_WRAPPER);
        
        dynamic_cast<const ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                      Test.Message() << endl,
                                                      ILwd::ASCII);
        Test.Message(false) << endl;

        delete ilwd;
    }

    {
        const udt* const result = chain.GetSymbol("y0");
        
        const ILwd::LdasElement* const ilwd
            = result->ConvertToIlwd(chain, datacondAPI::udt::TARGET_WRAPPER);
        
        dynamic_cast<const ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                      Test.Message() << endl,
                                                      ILwd::ASCII);
        Test.Message(false) << endl;

        delete ilwd;
    }

    {
        const udt* const result = chain.GetSymbol("y1");
        
        const ILwd::LdasElement* const ilwd
            = result->ConvertToIlwd(chain, datacondAPI::udt::TARGET_WRAPPER);
        
        dynamic_cast<const ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                      Test.Message() << endl,
                                                      ILwd::ASCII);
        Test.Message(false) << endl;

        delete ilwd;
    }

    // Don't need to delete anything else, obj. registry does it
}

template<class T>
void TestShiftAction()
{
    TestShiftAction<T>(0.1, 2);
}

int main(int argc, char** argv)
{
    Test.Init(argc, argv);
    Test.Message() << "$Id: tShiftState.cc,v 1.5 2006/10/13 19:32:40 emaros Exp $" << endl;

    try {
        TestShiftState<float>();
        TestShiftState<double>();
        TestShiftState<complex<float> >();
        TestShiftState<complex<double> >();
        
        TestShiftAction<float>();
        TestShiftAction<double>();
        TestShiftAction<complex<float> >();
        TestShiftAction<complex<double> >();

        //TestValues<float>();
    }
    catch(exception& e)
    {
        Test.Check(false) << "Exception: " << e.what() << endl;
    }
    catch( ... )
    {
        Test.Check(false) << "Unknown exception" << endl;
    }

    Test.Exit();
}
