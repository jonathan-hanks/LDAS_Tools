#include "datacondAPI/config.h"

#include <sstream>
   
#include "general/unittest.h"

#include "token.hh"

#include "ScalarUDT.hh"

using namespace std;   
   
   
General::UnitTest	Test;

template<class Answer_, class lhs_, class rhs_>
Answer_ calc_expected(const std::string& OP,
		       lhs_	Left, rhs_ Right)
{
    Answer_		expected((const Answer_)Left);

    if (OP == "add")
    {
      expected += Right;
    }
    else if (OP == "sub")
    {
      expected -= Right;
    }
    else if (OP == "mul")
    {
      expected *= Right;
    }
    else if (OP == "div")
    {
      expected /= Right;
    }

    return expected;
}

template<int,int,int>
int calc_expected(const std::string& OP,
		  int	Left, int Right)
{
    int		expected((const int)Left);

    if (OP == "add")
    {
      expected += Right;
    }
    else if (OP == "sub")
    {
      expected -= Right;
    }
    else if (OP == "mul")
    {
      expected *= Right;
    }
    else if (OP == "div")
    {
      expected /= Right;
    }
    else if (OP == "mod")
    {
      expected /= Right;
    }

    return expected;
}

template<class T>
void
append_call( T Value, CallChain& Cmds, std::string TypeStr, std::string Side)
{


  Parameters		args;
  ostringstream	        value;

  value << Value;
  Cmds.AppendCallFunction( TypeStr,
			   args.set(1, value.str().c_str()),
			   Side);
  return;
}

template<>
void
append_call( complex<float> Value,
	     CallChain& Cmds, std::string TypeStr, std::string Side)
{
  Parameters		args;
  ostringstream	r;
  ostringstream	i;

  r << Value.real();
  i << Value.imag();
  Cmds.AppendCallFunction( "float",
			   args.set(1, r.str().c_str() ),
			   "r");
  Cmds.AppendCallFunction( "float",
			   args.set(1, i.str().c_str() ),
			   "i");
  Cmds.AppendCallFunction( TypeStr,
			   args.set(2, "r", "i"),
			   Side);
  return;
}

template<>
void
append_call( complex<double> Value,
	     CallChain& Cmds, std::string TypeStr, std::string Side)
{
  Parameters		args;
  ostringstream	r;
  ostringstream	i;

  r << Value.real();
  i << Value.imag();
  Cmds.AppendCallFunction( "double",
			   args.set(1, r.str().c_str() ),
			   "r");
  Cmds.AppendCallFunction( "double",
			   args.set(1, i.str().c_str() ),
			   "i");
  Cmds.AppendCallFunction( TypeStr,
			   args.set(2, "r", "i"),
			   Side);
  return;
}

template<class LeftSideType, class RightSideType,
  class AnswerType, class AnswerTokenType>
void
basic_math_template(const std::string& OP,
		    const std::string& LeftSideTypeStr,
		    const LeftSideType Left,
		    const std::string& RightSideTypeStr,
		    const RightSideType Right)
{
  try {
    //-------------------------------------------------------------------
    // Calculate the expected value
    //-------------------------------------------------------------------
    Parameters		args;
    AnswerType		expected(calc_expected<AnswerType,
				               LeftSideType,
				               RightSideType>
				 (OP, Left, Right));

    //-------------------------------------------------------------------
    // Calculate value using Call Chain.
    //-------------------------------------------------------------------

    CallChain		cmds;
    ostringstream	test_name;

    test_name << "basic_math: "
	      << OP << "(" << Left << ", " << Right << "):";

    cmds.Reset();
    append_call( Left, cmds, LeftSideTypeStr, "left" );
    append_call( Right, cmds, RightSideTypeStr, "right" );
    cmds.AppendCallFunction(OP, args.set(2, "left", "right"), "result");

    cmds.AppendIntermediateResult( "result", "result", "Final Resullt", "" );

    cmds.Execute();

    //-------------------------------------------------------------------
    // Comparet the results to the exepected value.
    //-------------------------------------------------------------------
    const AnswerTokenType&	symbol =
      dynamic_cast<const AnswerTokenType&>(*(cmds.GetSymbol("result")));
    AnswerType	result = symbol;
    Test.Check(expected == result) << test_name.str() << " "
				   << expected << " =?= " << result
				   << " " << (expected - result)
				   << endl;
    Test.Message( );
    symbol.ConvertToIlwd( cmds )->write( Test.Message( false ), ILwd::ASCII );
    Test.Message( false ) << std::endl;
  }
  CATCH(Test);
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_basic_math.cc,v 1.10 2005/12/01 22:55:03 emaros Exp $"
		 << endl;

  basic_math_template<int,int,int,datacondAPI::Scalar<int> >
    ("add", "integer", 34, "integer", 5);
  basic_math_template<int,int,int,datacondAPI::Scalar<int> >
    ("sub", "integer", 34, "integer", 5);
  basic_math_template<int,int,int,datacondAPI::Scalar<int> >
    ("mul", "integer", 34, "integer", 5);
  basic_math_template<int,int,int,datacondAPI::Scalar<int> >
    ("div", "integer", 34, "integer", 5);
#if old
  basic_math_template<double,int,int,datacondAPI::Scalar<int> >
    ("mod", "integer", 34, "integer", 5);
#endif /* old */

  basic_math_template<double,int,double,datacondAPI::Scalar<double> >
    ("add", "double", 5.3, "integer", 5);
  basic_math_template<double,int,double,datacondAPI::Scalar<double> >
    ("sub", "double", 5.3, "integer", 5);
  basic_math_template<double,int,double,datacondAPI::Scalar<double> >
    ("mul", "double", 5.3, "integer", 5);
  basic_math_template<double,int,double,datacondAPI::Scalar<double> >
    ("div", "double", 5.3, "integer", 5);

  basic_math_template<int,double,double,datacondAPI::Scalar<double> >
    ("add", "integer", 5, "double", 5.3);
  basic_math_template<int,double,double,datacondAPI::Scalar<double> >
    ("sub", "integer", 5, "double", 5.3);
  basic_math_template<int,double,double,datacondAPI::Scalar<double> >
    ("mul", "integer", 5, "double", 5.3);
  basic_math_template<int,double,double,datacondAPI::Scalar<double> >
    ("div", "integer", 5, "double", 5.3);

  basic_math_template<double,double,double,datacondAPI::Scalar<double> >
    ("add", "double", 5.0, "double", 5.3);
  basic_math_template<double,double,double,datacondAPI::Scalar<double> >
    ("sub", "double", 5.0, "double", 5.3);
  basic_math_template<double,double,double,datacondAPI::Scalar<double> >
    ("mul", "double", 5.0, "double", 5.3);
  basic_math_template<double,double,double,datacondAPI::Scalar<double> >
    ("div", "double", 5.0, "double", 5.3);

  basic_math_template<complex<double>,complex<double>,complex<double>,
    datacondAPI::Scalar<complex<double> > >
    ("add", "complex",
     complex<double>(5.0, -3.0),
     "dcomplex", complex<double>(5.3,2.9) );
  basic_math_template<complex<double>,complex<double>,complex<double>,
    datacondAPI::Scalar<complex<double> > >
    ("sub", "complex",
     complex<double>(5.0, -3.0),
     "dcomplex", complex<double>(5.3,2.9) );
  basic_math_template<complex<double>,complex<double>,complex<double>,
    datacondAPI::Scalar<complex<double> > >
    ("mul", "complex",
     complex<double>(5.0, -3.0),
     "dcomplex", complex<double>(5.3,2.9) );
  basic_math_template<complex<double>,complex<double>,complex<double>,
    datacondAPI::Scalar<complex<double> > >
    ("div", "complex",
     complex<double>(5.0, -3.0),
     "dcomplex", complex<double>(5.3,2.9) );

  Test.Exit();
}
