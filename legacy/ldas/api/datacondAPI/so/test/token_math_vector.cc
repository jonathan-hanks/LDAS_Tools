#include "datacondAPI/config.h"

#include <sstream>

#include "general/unittest.h"

#include "token.hh"

#include "SequenceUDT.hh"
#include "TimeSeries.hh"

using namespace std;   
using namespace datacondAPI;

General::UnitTest	Test;

enum op_type {
  ADDITION,
  SUBTRACTION,
  MULTIPLICATION,
  DIVISION
};

//-----------------------------------------------------------------------
// Forward declaration of local functions
//-----------------------------------------------------------------------

template<class ArrayClass, class DataType>
void
bm_scalar(const std::string TestName,
	  const std::string& OP, DataType Base, DataType Magnifier,
	  const std::string& ArrayStrType,
	  const std::string& BaseStrType,
	  const std::string& MagnifierStrType,
	  bool ScalarOnRight = true);

template<class Array1Class, class Data1Type,
  class Array2Class, class Data2Type>
void
bm_vector(const std::string TestName,
	  const std::string& OP,
	  Data1Type Base1, Data2Type Base2,
	  const std::string& Array1StrType,
	  const std::string& Base1StrType,
	  const std::string& Array2StrType,
	  const std::string& Base2StrType,
	  bool ScalarOnRight = true);

void bm_udt( op_type OP, const udt& Operand_1, const udt& Operand_2 );

void bm_udt_op( op_type OP, const udt& LHS, const udt& RHS );

void bm_udt_group(int Line, const udt& Operand_1, const udt& Operand_2 );

bool bm_udt_validate( op_type OP, const udt& LHS, const udt& RHS,
		      const udt& Result );

template< class type_ >
bool in_tolerance( const type_& Value );

template< class abs_ >
abs_ t_abs( abs_& V );

template<class lhs_, class rhs_, class result_>
bool ts_op( op_type OP, const udt& LHS, const udt& RHS, const udt& Result );

template< class lhs_, class rhs_, class result_ >
bool
ts_result_check( op_type OP, const udt& LHS, const udt& RHS,
		 const udt& Result );

bool ts_validate( op_type OP, const udt& LHS, const udt& RHS, const udt& Result );

template< class type_ >
type_
variance( );

//-----------------------------------------------------------------------
// Template specialization
//-----------------------------------------------------------------------
namespace std
{
  basic_ostream< char >&
  operator<<( basic_ostream<char>& os, const valarray< complex<double> >& s )
  {
    typedef valarray< complex<double> > va_type;

    for ( size_t
	    cur = 0,
	    last = s.size( );
	  cur != last;
	  ++cur )
    {
      if ( cur != 0 )
      {
	os << ", ";
      }
      os << "(" << s[cur].real( ) << ", " << s[cur].imag( ) << ")"
	;
    }
    return os;
  }
}

template< >
float
variance< float >( )
{
  return 10e-6;
}

template< >
double
variance< double >( )
{
  return 10e-6;
}

template< >
std::complex< float >
variance< std::complex< float> >( )
{
  static std::complex< float > r( variance<float>(), variance<float>() );
  return r;
}

template< >
std::complex< double >
variance< std::complex< double > >( )
{
  static std::complex< double > r( variance< double >(),
				   variance< double >() );
  return r;
}

template< >
bool 
in_tolerance< std::complex< float > >( const std::complex< float >& Value )
{
  std::complex< float >	v( variance< std::complex< float > >() );
  return ( ( Value.real() <= v.real() ) &&
	   ( Value.imag() <= v.imag() ) );
}

template< >
bool 
in_tolerance< std::complex< double > >( const std::complex< double >& Value )
{
  std::complex< double >	v( variance< std::complex< double > >() );
  return ( ( Value.real() <= v.real() ) &&
	   ( Value.imag() <= v.imag() ) );
}

template< class type_ >
bool
in_tolerance( const type_& Value )
{
  return ( Value <= variance< type_ >() );
}

template< >
float
t_abs( float& V )
{
  return fabs( V );
}

template< >
double
t_abs( double& V )
{
  return fabs( V );
}

template< >
std::complex<float>
t_abs( std::complex< float >& V )
{
  V = std::complex< float >( fabs( V.real() ), fabs( V.imag() ) );
  return V;
}

template< >
std::complex<double>
t_abs( std::complex< double >& V )
{
  V = std::complex< double >( fabs( V.real() ), fabs( V.imag() ) );
  return V;
}


template<class lhs_, class rhs_, class result_>
bool
ts_op( op_type OP, const udt& LHS, const udt& RHS,
       const TimeSeries< result_ >& Result )
{
  const TimeSeries< lhs_ >&	lhs
    ( udt::Cast< TimeSeries< lhs_ > >( LHS ) );
  const TimeSeries< rhs_ >&	rhs
    ( udt::Cast< TimeSeries< rhs_ > >( RHS ) );

  const TimeSeries< result_ >&	result
    ( udt::Cast< TimeSeries< result_ > >( Result ) );
  std::valarray< result_ >	expected( result.size() );
  std::valarray< result_ >	vrhs( result.size() );
  std::valarray< result_ >	vresult( result.size() );

  for ( unsigned int x = 0; x < result.size(); x++ )
  {
    expected[x] = lhs[x];
    vrhs[x] = rhs[x];
    vresult[x] = result[x];
  }

  switch( OP )
  {
  case ADDITION:
    expected += vrhs;
    break;
  case SUBTRACTION:
    expected -= vrhs;
    break;
  case MULTIPLICATION:
    expected *= vrhs;
    break;
  case DIVISION:
    expected /= vrhs;
    break;
  default:
    throw std::runtime_error( "Unhandled operation" );
    break;
  }

  for ( unsigned int x = 0; x < result.size(); x++ )
  {
    result_ v( expected[x] - result[x] );
    if ( ! in_tolerance( t_abs( v ) ) )
    {
      return false;
    }
  }
  return true;
}

inline void
bm_udt( op_type OP, const udt& Operand_1, const udt& Operand_2 )
{
  bm_udt_op( OP, Operand_1, Operand_2);
  bm_udt_op( OP, Operand_2, Operand_1);
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_math_vector.cc,v 1.12 2009/05/20 00:14:59 emaros Exp $"
		 << endl;

  //---------------------------------------------------------------------
  // Valarrays
  //---------------------------------------------------------------------

  bm_scalar<std::valarray<float>,float>
    ("basic_math: valarray<float> + scalar<int>:",
     "add", 1.32, 5,
     "svalarray", "double", "integer");
  bm_scalar<std::valarray<float>,float>
    ("basic_math: valarray<float> - scalar<double>:",
     "sub", 1.32, 5.34,
     "svalarray", "double", "double");
  bm_scalar<std::valarray<float>,float>
    ("basic_math: valarray<float> * scalar<double>:",
     "mul", 1.32, 5.34,
     "svalarray", "double", "double");
  bm_scalar<std::valarray<float>,float>
    ("basic_math: valarray<float> / scalar<double>:",
     "div", 1.32, 5.34,
     "svalarray", "double", "double");
  bm_scalar<std::valarray< std::complex< double> >, std::complex< double> >
    ("basic_math: valarray< complex< double> > + scalar< complex< double > >:",
     "mul", std::complex< double >( 1.0, 0.0 ),
     std::complex< double >( 2.0, 0.0 ),
     "zvalarray", "complex", "complex");
  bm_scalar<std::valarray<float>,float>
    ("basic_math: scalar<double> / valarray<float>:",
     "div", 1.32, 5.34,
     "svalarray", "double", "double", false);

  bm_scalar<std::valarray<double>,double>
    ("basic_math: valarray<double> + scalar<int>:",
     "add", 1.32, 5,
     "dvalarray", "double", "integer");
  bm_scalar<std::valarray<double>,double>
    ("basic_math: valarray<double> - scalar<double>:",
     "sub", 1.32, 5.34,
     "dvalarray", "double", "double");
  bm_scalar<std::valarray<double>,double>
    ("basic_math: valarray<double> * scalar<double>:",
     "mul", 1.32, 5.34,
     "dvalarray", "double", "double");
  bm_scalar<std::valarray<double>,double>
    ("basic_math: valarray<double> / scalar<double>:",
     "div", 1.32, 5.34,
     "dvalarray", "double", "double");
  bm_scalar<std::valarray<double>,double>
    ("basic_math: scalar<double> / valarray<double>:",
     "div", 1.32, 5.34,
     "dvalarray", "double", "double", false);

  bm_vector<std::valarray<float>,float,
    std::valarray<float>,float>
    ("basic_math: valarray<float> + valarray<float>:",
     "add", 1, 2,
     "svalarray", "double", "svalarray", "double");
  bm_vector<std::valarray<float>,float,
    std::valarray<float>,float>
    ("basic_math: valarray<float> + valarray<float>:",
     "sub", 1, 2,
     "svalarray", "double", "svalarray", "double");
  bm_vector<std::valarray<float>,float,
    std::valarray<float>,float>
    ("basic_math: valarray<float> + valarray<float>:",
     "mul", 1, 2,
     "svalarray", "double", "svalarray", "double");
  bm_vector<std::valarray<float>,float,
    std::valarray<float>,float>
    ("basic_math: valarray<float> + valarray<float>:",
     "div", 1, 2,
     "svalarray", "double", "svalarray", "double");

  //---------------------------------------------------------------------
  // Test the basic math routines on real world problems
  //---------------------------------------------------------------------

  Sequence<float>		seq_flt( 10.95, 16 );
  Sequence<double>		seq_dbl( 4.32, 16 );
  Sequence< complex<double> >	seq_dcomplex( complex< double >(4.32, 30.3),
					      16 );

  TimeSeries<float>			ts_flt( seq_flt, 1.0/16.0 );
  TimeSeries<double>			ts_dbl( seq_dbl, 1.0/16.0 );
  TimeSeries< complex< double> >	ts_dcomplex( seq_dcomplex, 1.0/16.0 );

  bm_udt_group( __LINE__, ts_dbl, ts_dbl );
  bm_udt_group( __LINE__, ts_dbl, ts_dcomplex );
  bm_udt_group( __LINE__, ts_flt, ts_dcomplex );

  //---------------------------------------------------------------------
  // :TODO: Excption testing
  //---------------------------------------------------------------------

  Test.Exit();
}

namespace {
  template< class T >
  void formatter( const std::string& Type,
		  const std::string& VarName,
		  CallChain& Chain,
		  const T Value )
  {
    Parameters		args;
    
    ostringstream		value;

    value << Value;

    Chain.AppendCallFunction( Type,
			      args.set(1, value.str( ).c_str( ) ),
			      VarName );
  }

  template< >
  void formatter ( const std::string& Type,
		   const std::string& VarName,
		   CallChain& Chain,
		   const std::complex< double > Value )
  {
    Parameters		args;

    ostringstream		real;
    ostringstream		imag;

    real << Value.real( );
    imag << Value.imag( );

    Chain.AppendCallFunction( "double",
			      args.set(1, real.str( ).c_str( ) ),
			      "_real" );
    Chain.AppendCallFunction( "double",
			      args.set(1, imag.str( ).c_str( ) ),
			      "_imag" );
    Chain.AppendCallFunction( Type,
			      args.set(2, "_real", "_imag" ),
			      VarName );
  }

  template< class T >
  const char** format_magnifier( const T& Magnifier,
				 Parameters& Args );

  template< >
  const char**
  format_magnifier< std::complex< double > >( const std::complex< double >& Magnifier, Parameters& Args )
  {
    ostringstream	magnifier_real;
    ostringstream	magnifier_imag;

    magnifier_real << Magnifier.real( );
    magnifier_imag << Magnifier.imag( );

    return Args.set( 2,
		     magnifier_real.str( ).c_str( ),
		     magnifier_imag.str( ).c_str( ) );
  }
} // namespace - anonymouse

template< class T >
static const char** format_magnifier( const T& Magnifier,
				      Parameters& Args )
{
  ostringstream	magnifier;

  magnifier << Magnifier;

  return Args.set( 1, magnifier.str( ).c_str( ) );
}

template<class ArrayClass, class DataType>
void
bm_scalar(const std::string TestName,
       const std::string& OP, DataType Base, DataType Magnifier,
       const std::string& ArrayStrType,
       const std::string& BaseStrType,
       const std::string& MagnifierStrType,
       bool ScalarOnRight)
{
  Parameters		args;
  DataType		data[] = { Base, Base, Base, Base };

  ArrayClass expected(data,
		      sizeof(data)/
		      sizeof(*data));
  if (OP == "add")
  {
    expected += Magnifier;
  }
  else if (OP == "sub")
  {
    expected += -Magnifier;
  }
  else if (OP == "mul")
  {
    expected *= Magnifier;
  }
  else if (OP == "div")
  {
    if (ScalarOnRight) expected /= Magnifier;
    else expected = Magnifier / expected;
  }

  try {
    CallChain		cmds;
    CallChain::Symbol*	result;
    ostringstream	base;

    base << Base;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "4"), "N");
#if 1
    formatter( BaseStrType, "base", cmds, Base );
    formatter( MagnifierStrType, "magnifier", cmds, Magnifier );
#else
    cmds.AppendCallFunction( BaseStrType,
			     format_magnifier( Base, args ), "base");
    cmds.AppendCallFunction( MagnifierStrType,
			     format_magnifier( Magnifier, args ), "magnifier");
#endif
    cmds.AppendCallFunction(ArrayStrType, args.set(2, "base", "N"), "array");
    if (ScalarOnRight)
    {
      cmds.AppendCallFunction(OP, args.set(2, "array", "magnifier"),
			      "result");
    }
    else
    {
      cmds.AppendCallFunction(OP, args.set(2, "magnifier", "array"),
			      "result");
    }

    cmds.AppendIntermediateResult("result", "result", "Final Result", "" );

    cmds.Execute();

    result = cmds.GetSymbol("result");

    ostringstream	test_name;
    ArrayClass src(data, sizeof(data)/sizeof(*data));
    test_name << "basic_math: "
	      << OP << "(";
    if (ScalarOnRight)
    {
      test_name << src << ", " << Magnifier;
    }
    else
    {
      test_name << Magnifier << ", " << src;
    }
    test_name << "):";

    check_valarray(expected, result, test_name.str());
  }
  CATCH(Test);
}

template<class Array1Class, class Data1Type,
  class Array2Class, class Data2Type>
void
bm_vector(const std::string TestName,
	  const std::string& OP,
	  Data1Type Base1, Data2Type Base2,
	  const std::string& Array1StrType,
	  const std::string& Base1StrType,
	  const std::string& Array2StrType,
	  const std::string& Base2StrType,
	  bool ScalarOnRight)
{
  Parameters		args;
  Data1Type		data1[] = { Base1, Base1, Base1, Base1 };
  Data2Type		data2[] = { Base2, Base2, Base2, Base2 };

  Array1Class expected(data1,
		       sizeof(data1)/
		       sizeof(*data1));
  Array2Class magnifier(data2,
		       sizeof(data2)/
		       sizeof(*data2));
  if (OP == "add")
  {
    expected += magnifier;
  }
  else if (OP == "sub")
  {
    expected += -magnifier;
  }
  else if (OP == "mul")
  {
    expected *= magnifier;
  }
  else if (OP == "div")
  {
    if (ScalarOnRight) expected /= magnifier;
    else expected = magnifier / expected;
  }

  try {
    CallChain		cmds;
    CallChain::Symbol*	result;
    ostringstream	base1;
    ostringstream	base2;

    base1 << Base1;
    base2 << Base2;

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, "4"), "N");
    cmds.AppendCallFunction(Base1StrType, args.set(1, base1.str().c_str()), "base1");
    cmds.AppendCallFunction(Base2StrType, args.set(1, base2.str().c_str()), "base2");
    cmds.AppendCallFunction(Array1StrType, args.set(2, "base1", "N"),
			    "array1");
    cmds.AppendCallFunction(Array2StrType, args.set(2, "base2", "N"),
			    "array2");
    if (ScalarOnRight)
    {
      cmds.AppendCallFunction(OP, args.set(2, "array1", "array2"),
			      "result");
    }
    else
    {
      cmds.AppendCallFunction(OP, args.set(2, "array2", "array1"),
			      "result");
    }

    cmds.AppendIntermediateResult("result", "result", "Final Result", "" );

    cmds.Execute();
    

    result = cmds.GetSymbol("result");
    check_valarray(expected, result, TestName);

  }
  CATCH(Test);
}

void
bm_udt_op( op_type OP, const udt& LHS, const udt& RHS )
{
  CallChain	cmds;
  std::string	op;
  Parameters	args;
  std::string	leader;

  //---------------------------------------------------------------------
  // Initialize
  //---------------------------------------------------------------------

  switch( OP )
  {
  case ADDITION:
    op = "add";
    break;
  case SUBTRACTION:
    op = "sub";
    break;
  case MULTIPLICATION:
    op = "mul";
    break;
  case DIVISION:
    op = "div";
    break;
  }

  leader = op;

  //---------------------------------------------------------------------
  // Ensure that the CallChain is in the initial state
  //---------------------------------------------------------------------

  cmds.Reset();

  //---------------------------------------------------------------------
  // Add Symbols
  //---------------------------------------------------------------------

  cmds.AddSymbol( "lhs", LHS.Clone() );
  cmds.AddSymbol( "rhs", RHS.Clone() );

  //---------------------------------------------------------------------
  // Add steps to the execution
  //---------------------------------------------------------------------

  cmds.AppendCallFunction( op, args.set(2, "lhs", "rhs"), "result" );
  cmds.AppendIntermediateResult("result", "result", "Final Result", "" );

  //---------------------------------------------------------------------
  // Evaluate steps
  //---------------------------------------------------------------------

  cmds.Execute();

  CallChain::Symbol*	result = cmds.GetSymbol( "result" );

  //---------------------------------------------------------------------
  // Validate answer
  //---------------------------------------------------------------------

  if ( result == (CallChain::Symbol*)NULL )
  {
    Test.Check( false ) << leader 
			<< ": NULL result"
			<< std::endl;
  }
  else
  {
    try
    {
      Test.Check( bm_udt_validate( OP, LHS, RHS, *result ) )
	<< leader
	<< ": Result validation"
	<< std::endl;
    }
    catch( const std::exception& e )
    {
      Test.Check( false ) << leader
			  << ": Caught excption: "
			  << e.what()
			  << std::endl;
    }
    catch( ... )
    {
      Test.Check( false ) << leader
			  << ": Caught unknown excption"
			  << std::endl;
    }
  }
}

void 
bm_udt_group( int Line, const udt& Operand_1, const udt& Operand_2 )
{
  bm_udt( ADDITION, Operand_1, Operand_2 );
  bm_udt( SUBTRACTION, Operand_1, Operand_2 );
  bm_udt( MULTIPLICATION, Operand_1, Operand_2 );
  bm_udt( DIVISION, Operand_1, Operand_2 );
}

#define TIMESERIES(lhs_,rhs_) \
  ( udt::IsA< TimeSeries<lhs_> >( LHS ) && \
    ( udt::IsA< TimeSeries<rhs_> >( RHS ) ) )

bool
bm_udt_validate( op_type OP, const udt& LHS, const udt& RHS, const udt& Result )
{
  if ( TIMESERIES( double, double ) )
  {
    return ts_result_check< double, double, double >( OP, LHS, RHS, Result );
  }
  //---------------------------------------------------------------------
  // std::complex< double >
  //---------------------------------------------------------------------
  else if ( TIMESERIES( float, std::complex< double > ) )
  {
    return ts_result_check< float, std::complex<double>,
      std::complex< double > >
      ( OP, LHS, RHS, Result );
  }
  else if ( TIMESERIES( std::complex< double >, float ) )
  {
    return ts_result_check< std::complex<double>, float,
      std::complex< double > >
      ( OP, LHS, RHS, Result );
  }
  else if ( TIMESERIES( double, std::complex< double > ) )
  {
    return ts_result_check< double, std::complex<double>,
      std::complex< double > >
      ( OP, LHS, RHS, Result );
  }
  else if ( TIMESERIES( std::complex< double >, double ) )
  {
    return ts_result_check< std::complex<double>, double,
      std::complex< double > >
      ( OP, LHS, RHS, Result );
  }
  else if ( TIMESERIES( std::complex< double >, std::complex< double > ) )
  {
    return ts_result_check< std::complex<double>, std::complex< double >,  std::complex< double > >
      ( OP, LHS, RHS, Result );
  }
  else
  {
    std::ostringstream	oss;

    oss << "Unable to validate: LHS: " << TypeInfoTable.GetName( typeid(LHS) )
	<< " RHS: " << TypeInfoTable.GetName( typeid(RHS) );
    throw std::runtime_error( oss.str().c_str() );
  }
  return false;
}

template< class lhs_, class rhs_, class result_ >
bool
ts_result_check( op_type OP, const udt& LHS, const udt& RHS,
		 const udt& Result )
{
    if ( udt::IsA< TimeSeries< result_ > >( Result ) )
    {
      return ts_op< lhs_, rhs_ >( OP, LHS, RHS,
				    udt::Cast< TimeSeries< result_ > >
				    ( Result ) );
    }
    else
    {
      std::ostringstream	oss;

      oss << "Result is not of type "
	  << TypeInfoTable.GetName( typeid( TimeSeries< result_ > ) );
      throw std::domain_error( oss.str().c_str() );
    }
}

bool
ts_validate( op_type OP, const udt& LHS, const udt& RHS, const udt& Result )
{
  if ( TIMESERIES( double, double ) )
  {
    return ts_result_check< double, double, double >( OP, LHS, RHS, Result );
  }
  else if ( TIMESERIES( std::complex< double >, std::complex< double > ) )
  {
    return ts_result_check< std::complex<double>, std::complex< double >,  std::complex< double > >
      ( OP, LHS, RHS, Result );
  }
  return false;
}
