#include "datacondAPI/config.h"

#include <valarray>

#include "general/unittest.h"

General::UnitTest Test;

int
main(int ArgC, char** ArgV)
{
    Test.Init(ArgC, ArgV);
    const size_t size = 10;
    const float value1 = 3;
    const float value2 = -7;
    std::valarray<float> v1(value1, size);
    std::slice           s1(0, size/2, 1);
    std::slice           s2(size/2, size - size/2, 1);
    std::valarray<float> v2(value2, size);
    size_t i = 0;

    Test.Message() << "Testing that gcc patch has been applied - see\n"
	"http://www.ldas-dev.ligo.caltech.edu/doc/INSTALL.html" << std::endl;

    for (i = 0; i < v1.size(); i++)
    {
        v1[i] = float(i);
    }

    v2[s2] = static_cast< const std::valarray<float> >(v1)[s1];
    v2[s1] = static_cast< const std::valarray<float> >(v1)[s2];

    for (i = 0; i < size/2; i++)
    {
        Test.Check(v2[i] == (i + size/2)) << "Sliced values are equal: "
                   << v2[i] << " =?= " << (i + size/2) << std::endl;
    }
    for (i = size/2; i < v2.size(); i++)
    {
        Test.Check(v2[i] == (i - size/2)) << "Sliced values are equal: "
                   << v2[i] << " =?= " << (i - size/2) << std::endl;
    }

    Test.Exit();
}
