#include "datacondAPI/config.h"

#include <math.h>

#include "general/unittest.h"

#include "TimeSeries.hh"
#include "Statistics.hh"

General::UnitTest Test;

using namespace datacondAPI;


bool operator==(const GapMetaData<GPSTime>::GapInterval_type& lhs,
		const GapMetaData<GPSTime>::GapInterval_type& rhs)
{
    return ((lhs.start == rhs.start) && (lhs.stop == rhs.stop));
}

bool operator!=(const GapMetaData<GPSTime>::GapInterval_type& lhs,
		const GapMetaData<GPSTime>::GapInterval_type& rhs)
{
    return !(lhs == rhs);
}

GapMetaData<GPSTime>::GapInterval_type gapIndices(const GPSTime& tstart,
					       const GPSTime& gstart,
					       const GPSTime& gstop,
					       const double& dt)
{
    const int start = (int) floor((gstart - tstart)/dt);
    const int stop  = (int) floor((gstop - tstart)/dt) + 1;

    return GapMetaData<GPSTime>::GapInterval_type(start, stop);
}

// dummy class derived from GapMetaData
template<class Unit_type>
class GapMetaDataTest : public GapMetaData<Unit_type> {
public:
    virtual Unit_type GapGetDataBegin() const
    {
	return Unit_type();
    }
    
    virtual Unit_type GapGetDataEnd() const
    {
	return Unit_type();
    }
    
    virtual unsigned int GapGetDataSize() const
    {
	return 0;
    }
    
    virtual double GetStepSize() const
    {
	return 0.0;
    }
    
private:
};

//
// This function checks that:
// 1) gaps don't overlap
// 2) data segments (ie. non-gaps) don't overlap
// 3) gaps and data don't overlap
// 4) all samples are either in gap segments or in data segments
//
template<class Unit_type>
void testDataTransitions(const GapMetaData<Unit_type>& gmd,
			 const size_t len)
{
    const int unset = 0;
    const int gap = 1;
    const int data = 2;

    bool pass = true;
    valarray<int> status(unset, len);
    
    typedef GapMetaData<Unit_type>::Transitions_type::const_iterator CI;
    GapMetaData<Unit_type>::Transitions_type trans;
    
    // Fill in the gaps
    gmd.CalcTransitions(trans);

    pass = true;
    for (CI i = trans.begin(); i != trans.end(); ++i)
    {
	for (size_t k = (*i).start; k < (*i).stop; ++k)
	{
	    pass = pass && (status[k] == unset);
	    status[k] = gap;
	}
    }
    
    Test.Check(pass) << "Gaps do not overlap" << endl;

    // Fill in the data
    gmd.CalcTransitionsOfData(trans);

    pass = true;
    for (CI i = trans.begin(); i != trans.end(); ++i)
    {
	for (size_t k = (*i).start; k < (*i).stop; ++k)
	{
	    pass = pass && (status[k] == unset);
	    status[k] = data;
	}
    }
    
    Test.Check(pass) << "Non-gaps do not overlap" << endl;

    // Check that nothing was left out

    pass = true;
    for (size_t k = 0; k < status.size(); ++k)
    {
	pass = pass && (status[k] != unset);
    }

    Test.Check(pass) << "All elements are in either gap or non-gap" << endl;
}

template<class Unit_type>
void testTransitions(const GapMetaData<Unit_type>& gmd,
		     const GapMetaData<Unit_type>::Transitions_type& expTrans)
{
    GapMetaData<Unit_type>::Transitions_type trans;
    
    gmd.CalcTransitions(trans);

    Test.Check(trans.size() == expTrans.size())
	<< "Object has " << trans.size()
	<< " gaps, expected " << expTrans.size() << " gaps" << endl;

    bool pass = (equal(trans.begin(), trans.end(), expTrans.begin()));

    Test.Check(pass)
	<< "List of gaps obtained is equal to expected gaps" << endl;

    typedef GapMetaData<Unit_type>::Transitions_type::const_iterator CI;

    if (!pass)
    {
	for (CI i = trans.begin(); i != trans.end(); ++i)
	{
	    Test.Message() << "-- Actual transitions:" << endl;
	    Test.Message()
		<< "-- start = " << (*i).start
		<< ", stop = " << (*i).stop << endl;
	}

	for (CI i = expTrans.begin(); i != expTrans.end(); ++i)
	{
	    Test.Message() << "-- Expected transitions:" << endl;
	    Test.Message()
		<< "-- start = " << (*i).start
		<< ", stop = " << (*i).stop << endl;
	}
    }
}

// Verify that contruction/destruction does not lead to memory leaks
// or crashes
void testConstruction()
{
    {
	GapMetaData<GPSTime>* p = new GapMetaDataTest<GPSTime>();
    
	Test.Check(true) << "Construction" << endl;

	const GapMetaData<GPSTime>::Transitions_type expTrans;

	testTransitions(*p, expTrans);

	delete p;
	
	Test.Check(true) << "Destruction" << endl;
    }

    {
	const Sequence<float> s(0.0, 1024);

	udt* p = new TimeSeries<float>(s, 2048.0);
    
	Test.Check(true) << "Construction" << endl;

	delete p;

	Test.Check(true) << "Destruction" << endl;
    }
}

void testTransitions()
{
    const size_t n = 4096;
    const double srate = 2048.0; 
    const GPSTime startTime(600000, 0);
    const float value = 0.0;
    const Sequence<float> s(value, n);

    {
	Test.Message() << "-- Test default case" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;
	
	testTransitions(tseries, expTrans);
	testDataTransitions(tseries, tseries.size());
    }

    {
	Test.Message() << "-- Test one gap at the beginning" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dt = 5.1*delta_t;
	const GPSTime start = startTime;
	const GPSTime stop = start + dt;

	const GapMetaData<GPSTime>::GapInterval_type gap
	    = gapIndices(startTime, start, stop, delta_t);

	expTrans.push_back(gap);
	tseries.GapAppendGapRange(start, stop);

	testTransitions(tseries, expTrans);
	testDataTransitions(tseries, tseries.size());
    }

    {
	Test.Message() << "-- Test one gap at the end" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dt = 4.7*delta_t;
	// Gap is *inclusive* of start time but *exclusive* of stop time
	const GPSTime start = startTime + dt;
	const GPSTime stop = tseries.GetEndTime() + delta_t/2;

	const GapMetaData<GPSTime>::GapInterval_type gap
	    = gapIndices(startTime, start, stop, delta_t);

	expTrans.push_back(gap);
	tseries.GapAppendGapRange(start, stop);

	testTransitions(tseries, expTrans);
	testDataTransitions(tseries, tseries.size());
    }

    {
	Test.Message() << "-- Test one gap in the middle" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dtStart = 4.5*delta_t;
	const double dtEnd = 6.3*delta_t;
	const GPSTime start = startTime + dtStart;
	const GPSTime stop = tseries.GetEndTime() - dtEnd;
	
	const GapMetaData<GPSTime>::GapInterval_type gap
	    = gapIndices(startTime, start, stop, delta_t);

	expTrans.push_back(gap);
	tseries.GapAppendGapRange(start, stop);

	testTransitions(tseries, expTrans);
	testDataTransitions(tseries, tseries.size());
    }
}

// In place
template<class T>
void testGapFillZero1(TimeSeries<T>& tseries)
{
    const T value = 1;
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;

    tseries.CalcTransitions(trans);
    tseries = value;
    
    GapFill fill(GapFill::ZERO);

    fill.apply(tseries, tseries);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == 0.0);
	    tseries[j] = value;
	}
	++i;
    }

    Test.Check(pass) << "Gaps have been filled with correct value" << endl;

    // Now check the complement
    pass = true;
    tseries -= value;
    for (size_t j = 0; j < tseries.size(); ++j)
    {
	pass = pass && (tseries[j] == 0.0);
    }

    Test.Check(pass) << "No non-gap data was overwritten" << endl;
}

// Non-null UDT
template<class T>
void testGapFillZero2(const TimeSeries<T>& tseriesInConst)
{
    const T value = 1;
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;
    TimeSeries<T> tseriesIn(tseriesInConst);
    TimeSeries<T> tseries;
    udt* tseries_p = &tseries;

    tseriesIn.CalcTransitions(trans);
    tseriesIn = value;
    
    GapFill fill(GapFill::ZERO);

    fill.apply(tseries_p, tseriesIn);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == 0.0);
	    tseries[j] = value;
	}
	++i;
    }

    Test.Check(pass) << "Gaps have been filled with correct value" << endl;

    // Now check the complement
    pass = true;
    tseries -= value;
    for (size_t j = 0; j < tseries.size(); ++j)
    {
	pass = pass && (tseries[j] == 0.0);
    }

    Test.Check(pass) << "No non-gap data was overwritten" << endl;
}

// Null UDT
template<class T>
void testGapFillZero3(const TimeSeries<T>& tseriesInConst)
{
    TimeSeries<T> tseriesIn(tseriesInConst);
    const T value = 1;
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;
    udt* tseries_p = 0;

    tseriesIn.CalcTransitions(trans);
    tseriesIn = value;
    
    GapFill fill(GapFill::ZERO);

    fill.apply(tseries_p, tseriesIn);

    TimeSeries<T>& tseries = dynamic_cast<TimeSeries<T>& >(*tseries_p);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == 0.0);
	    tseries[j] = value;
	}
	++i;
    }

    Test.Check(pass) << "Gaps have been filled with correct value" << endl;

    // Now check the complement
    pass = true;
    tseries -= value;
    for (size_t j = 0; j < tseries.size(); ++j)
    {
	pass = pass && (tseries[j] == 0.0);
    }

    delete tseries_p;

    Test.Check(pass) << "No non-gap data was overwritten" << endl;
}

void testGapFillZero()
{
    const size_t n = 4096;
    const double srate = 2048.0; 
    const GPSTime startTime(600000, 0);
    const float value = 0.0;
    const Sequence<float> s(value, n);

    Test.Message() << "-- Test zero fill method" << endl;

    {
	Test.Message() << "-- Test fill - default case" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	
	testGapFillZero1(tseries);
	testGapFillZero2(tseries);
	testGapFillZero3(tseries);
    }

    {
	Test.Message() << "-- Test fill - one gap at the beginning" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 5.1*delta_t;
	const GPSTime start = startTime;
	const GPSTime stop = start + dt;

	tseries.GapAppendGapRange(start, stop);

	testGapFillZero1(tseries);
	testGapFillZero2(tseries);
	testGapFillZero3(tseries);
    }

    {
	Test.Message() << "-- Test one gap at the end" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 4.7*delta_t;
	const GPSTime start = startTime + dt;
	const GPSTime stop = tseries.GetEndTime() + delta_t/2;

	tseries.GapAppendGapRange(start, stop);

	testGapFillZero1(tseries);
	testGapFillZero2(tseries);
	testGapFillZero3(tseries);
    }

    {
	Test.Message() << "-- Test one gap in the middle" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dtStart = 4.5*delta_t;
	const double dtEnd = 6.3*delta_t;
	const GPSTime start = startTime + dtStart;
	const GPSTime stop = tseries.GetEndTime() - dtEnd;
	
	tseries.GapAppendGapRange(start, stop);

	testGapFillZero1(tseries);
	testGapFillZero2(tseries);
	testGapFillZero3(tseries);
    }
}

template<class T>
void testGapFillMean(TimeSeries<T>& tseries)
{
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;

    tseries.CalcTransitions(trans);
    const T value = 1;

    tseries = value;

    GapFill fillz(GapFill::ZERO);
    GapFill filla(GapFill::AVERAGE);

    fillz.apply(tseries, tseries);
    filla.apply(tseries, tseries);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == value);
	}
	++i;
    }

    Test.Check(pass) << "Gaps have been filled with correct value" << endl;

    // Now verify that no gaps contribute to the mean
    pass = true;

    tseries = 0.0;

    i = trans.begin();
    while (i != trans.end())
    {
	std::slice s((*i).start, (*i).stop - (*i).start, 1);
	tseries[s] = 100.0;
	++i;
    }

    filla.apply(tseries, tseries);

    for (size_t j = 0; j < tseries.size(); ++j)
    {
	pass = pass && (tseries[j] == 0.0);
    }

    Test.Check(pass) << "No gap data contributed to average" << endl;
}

void testGapFillMean()
{
    const size_t n = 4096;
    const double srate = 2048.0; 
    const GPSTime startTime(600000, 0);
    const float value = 0.0;
    const Sequence<float> s(value, n);

    Test.Message() << "-- Test average fill method" << endl;

    {
	Test.Message() << "-- Test fill - default case" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	
	testGapFillMean(tseries);
    }

    {
	Test.Message() << "-- Test fill - one gap at the beginning" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 5.1*delta_t;
	const GPSTime start = startTime;
	const GPSTime stop = start + dt;

	tseries.GapAppendGapRange(start, stop);

	testGapFillMean(tseries);
    }

    {
	Test.Message() << "-- Test one gap at the end" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 4.7*delta_t;
	const GPSTime start = startTime + dt;
	const GPSTime stop = tseries.GetEndTime() + delta_t/2;

	tseries.GapAppendGapRange(start, stop);

	testGapFillMean(tseries);
    }

    {
	Test.Message() << "-- Test one gap in the middle" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dtStart = 4.5*delta_t;
	const double dtEnd = 6.3*delta_t;
	const GPSTime start = startTime + dtStart;
	const GPSTime stop = tseries.GetEndTime() - dtEnd;
	
	tseries.GapAppendGapRange(start, stop);

	testGapFillMean(tseries);
    }
}

template<class T>
void testGapFillLinear(TimeSeries<T>& tseries)
{
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;

    tseries.CalcTransitions(trans);

    for (size_t k = 0; k < tseries.size(); ++k)
    {
	tseries[k] = k;
    }

    GapFill fillz(GapFill::ZERO);
    GapFill filla(GapFill::LINEAR);

    fillz.apply(tseries, tseries);
    filla.apply(tseries, tseries);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    T value = 0.0;

	    if ((*i).start == 0)
	    {
		value = tseries[(*i).stop];
	    }
	    else if ((*i).stop == tseries.size())
	    {
		value = tseries[(*i).start - 1];
	    }
	    else
	    {
		value = j;
	    }

	    pass = pass && (tseries[j] == value);
	    if (!pass)
	    {
		Test.Message() << "tseries[j] = " << tseries[j]
			       << ", value = " << value << endl;
		break;
	    }
	}
	++i;
    }

    Test.Check(pass) << "Gaps have been filled with correct value" << endl;
}

void testGapFillLinear()
{
    const size_t n = 4096;
    const double srate = 2048.0; 
    const GPSTime startTime(600000, 0);
    const float value = 0.0;
    const Sequence<float> s(value, n);

    Test.Message() << "-- Test linear fill method" << endl;

    {
	Test.Message() << "-- Test fill - default case" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	
	testGapFillLinear(tseries);
    }

    {
	Test.Message() << "-- Test fill - one gap at the beginning" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 5.1*delta_t;
	const GPSTime start = startTime;
	const GPSTime stop = start + dt;

	tseries.GapAppendGapRange(start, stop);

	testGapFillLinear(tseries);
    }

    {
	Test.Message() << "-- Test one gap at the end" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 4.7*delta_t;
	const GPSTime start = startTime + dt;
	const GPSTime stop = tseries.GetEndTime() + delta_t/2;

	tseries.GapAppendGapRange(start, stop);

	testGapFillLinear(tseries);
    }

    {
	Test.Message() << "-- Test one gap in the middle" << endl;

	TimeSeries<float> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dtStart = 4.5*delta_t;
	const double dtEnd = 6.3*delta_t;
	const GPSTime start = startTime + dtStart;
	const GPSTime stop = tseries.GetEndTime() - dtEnd;
	
	tseries.GapAppendGapRange(start, stop);

	testGapFillLinear(tseries);
    }
}

void testExceptions()
{
    {
	Test.Message() << "-- Test bad gap" << endl;

	GapMetaDataTest<GPSTime> gmd;
	const GPSTime start(600000, 0);
	const GPSTime stop(start - 1);

	try
	{
	    gmd.GapAppendGapRange(start, stop);
	}
	catch(std::invalid_argument& e)
	{
	    Test.Check(true) << "Caught exception: " << e.what() << endl;
	}
	catch(std::exception& e)
	{
	    Test.Check(false) << "Caught exception: " << e.what() << endl;
	}
	catch(...)
	{
	    Test.Check(false) << "Caught unknown exception" << endl;
	}
    }

}

int
main(int argc, char** argv)
{
    Test.Init(argc, argv);

    try {
	Test.Message() << "-- $Id: tGapFill.cc,v 1.7 2005/12/01 22:55:02 emaros Exp $" << endl;

	testConstruction();
	testTransitions();
	testGapFillZero();
	testGapFillMean();
	testGapFillLinear();

	testExceptions();
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Unhandled exception: " 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception" << std::endl;
    }
    
    Test.Exit();
}
