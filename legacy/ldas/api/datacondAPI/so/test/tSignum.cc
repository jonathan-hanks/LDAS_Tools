#include "datacondAPI/config.h"

#include <cstdlib>

#include "general/unittest.h"

#include "CallChain.hh"
#include "UDT.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "SignumFunction.hh"

// the following tests are performed:
// test processing of <float> data with 0, 1, and 2 thresholds
// test processing of <double> data with 0, 1, and 2 thresholds

using namespace datacondAPI;

General::UnitTest Test;

#define NN 20		// number of data samples

template<class T>
bool testData(int nth, double x1=0., double x2=0.)
{
 bool pass = true;
 double v;

 double buf1[NN] = { 0.0, -2.3, -5.7, 1.1, -0.4, 4.9, 1.6, -2.2, 0.8, 3.4,
                    -0.3, 0.1, -3.2, -1.3, 0.9, 8.3, -1.5, 1.7, -2.9, 3.7 };

 Scalar<double> th1(x1), th2(x2);
 const char* comm="two thresholds result";

// list of parameters
 const char* par[4]={"input", "threshold1", "threshold2", NULL};

 if  ( nth == 1 ) 
 {
   par[2] = NULL;
   comm = "single threshold result";
 }
 if  ( nth == 0 ) 
 {
   par[1] = NULL;
   comm = "default threshold result";
 }

 try {

    CallChain chain;

    Sequence<T> in(0.,NN);

// fill different types of Sequence by sample data
    if ( udt::IsA<Sequence<float> >(in) )
    {
      for (unsigned int i=0; i<NN; i++) 
        (udt::Cast<Sequence<float> >(in))[i] = (float) buf1[i];
    }

    if ( udt::IsA<Sequence<double> >(in) )
    {
      for (unsigned int i=0; i<NN; i++)
        (udt::Cast<Sequence<double> >(in))[i] = buf1[i];
    }

    CallChain::Symbol* input = in.Clone();

    // set name and reference for 1st parameter
    chain.AddSymbol("input", input);

    // set name and reference for 2nd parameter 
    if ( nth > 0 )
    {
      CallChain::Symbol* t1 = th1.Clone();
      chain.AddSymbol("threshold1", t1);
    }

    // set name and reference for 3d parameter
    if ( nth > 1 )
    {
       CallChain::Symbol* t2 = th2.Clone();
       chain.AddSymbol("threshold2", t2);
    }

    chain.AppendCallFunction("signum",  par, "result");

    chain.AppendIntermediateResult("result", "result", comm, "" );

    chain.Execute();

// Print results in ASCII for debugging
/*
   cout << "Results:" << endl;
   ILwd::LdasContainer* r;

   if ((r = chain.GetResults()))
   {
     r->write(cout, ILwd::ASCII);
     cout << endl;
   }
*/
    udt* out=chain.GetSymbol("result");	// get pointer to output

    if ( udt::IsA<Sequence<T> >(*out) )
    {
      Sequence<T>* rs =
          &(udt::Cast<Sequence<T> >(*out));
      if ( NN != rs->size() )  pass = false;

// compare data samples
      if ( (nth == 2) && (x1 > x2) ) { double x = x1; x1 = x2; x2 = x; }
      if (nth == 1)  x2 = x1; 

      for (unsigned int i=0; i<NN; i++) 
      {
        if ( in[i] < (T)x1 ) v = -1.;
        else if ( in[i] > (T)x2 ) v = 1.;
        else v = 0.;
        
        if ( (*rs)[i] != v ) pass = false;
      }
    }
    else pass = false;

  } // end of 'try'

  catch(CallChain::Exception& x)
  {
    Test.Check( false ) << "Caught a CallChain exception: " <<x.what()<< std::endl;
    pass = false;
  }

  catch (std::invalid_argument& x)
  {
      Test.Message() << x.what() << std::endl;
      pass = false;
  }

  catch (std::exception& x)
  {
      Test.Check( false ) << x.what() << std::endl;
      pass = false;
    }

  catch(...)
  {
    Test.Check( false ) << "Caught an Unknown exception." << std::endl;
    pass = false;
  }

  return pass;
}

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  Test.Message() << "$Id: tSignum.cc,v 1.6 2009/05/20 00:14:59 emaros Exp $"
		 << std::endl << std::endl;

  Test.Check(testData<float>(0))
  << "(test of default threshold with <float> data type)" << std::endl;

  Test.Check(testData<float>(1, 1.1))
  << "(test of single threshold with <float> data type)" << std::endl;

  Test.Check(testData<float>(2, 1.6,-0.9))
  << "(test of two thresholds with <float> data type)" << std::endl;

  Test.Check(testData<double>(0))
  << "(test of default threshold with <double> data type)" << std::endl;

  Test.Check(testData<double>(1, -2.3))
  << "(test of single threshold with <double> data type)" << std::endl;

  Test.Check(testData<double>(2, -2.1, 1.7))
  << "(test of two thresholds with <double> data type)" << std::endl;

  // all done!!
  Test.Exit();
}

