#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "Matrix.hh"

using namespace datacondAPI;

General::UnitTest Test;

template<typename type> const std::string& name();

#define NAME_TYPE(ARGUMENT) template<> const std::string& name< ARGUMENT >() { static std::string name( #ARGUMENT ); return name; }

NAME_TYPE(float)
NAME_TYPE(double)
NAME_TYPE(std::complex<float>)
NAME_TYPE(std::complex<double>)

#undef NAME_TYPE

template<typename type> void randomize(std::valarray<type>& v)
{
    for (std::size_t i = 0; i < v.size(); ++i)
        v[i] = static_cast<type>(rand());
}

template<typename type> void randomize(Matrix<type>& A)
{
    for (std::size_t row = 0; row < A.rows(); ++row)
        for (std::size_t column = 0; column < A.columns(); ++column)
            A[row][column] = static_cast<type>(rand());
}

template<typename type> bool soft_equality(const type& left, const type& right)
{
    return ((std::abs(left - right) / std::abs(left + right + 0.123456789)) < 1e-5);
}
   
template<typename type> bool soft_equality(const std::complex< type >& left,
   const std::complex< type >& right)
{
   const std::complex< type > const_value( 0.123456789 );
   return ((std::abs(left - right) / std::abs(left + right + const_value)) < 1e-5);
}   

template<typename type> void test(const std::size_t& rows, const std::size_t& columns)
{
    try
    {
        Matrix<type> A(rows, columns);
        Test.Check(A.rows() == rows && A.columns() == columns) << "Matrix<" << name<type>() << " >::Matrix(" << rows << ", " << columns << ")" << std::endl;
        
        randomize(A);
        Matrix<type> B(A);
        Test.Check(A == B) << "Matrix<" << name<type>() << " >::Matrix(const Matrix<" << name<type>() << ">&)" << std::endl;
        
        Matrix<type>* C = B.Clone();
        Test.Check(C != 0 && A == *C) << "Matrix<" << name<type>() << " >::Matrix(const Matrix<" << name<type>() << ">&)" << std::endl;        
        delete C;

        Matrix<type> D;
        B.resize((rows + 1) % 10, (columns + 1) % 10);
        Test.Check(B != A && (B = A) == A) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator=(const Matrix<" << name<type>() << ">&)" << std::endl;

        const Matrix<type> E(A);
        Test.Check(E.rows() == rows) << "std::size_t Matrix<" << name<type>() << " >::rows() const" << std::endl;
        Test.Check(E.columns() == columns) << "std::size_t Matrix<" << name<type>() << " >::columns() const" << std::endl;
        Test.Check(E.size() == std::make_pair(rows, columns)) << "std::pair<std::size_t, std::size_t> Matrix<" << name<type>() << " >::columns() const" << std::endl;

        std::pair<std::size_t, std::size_t> other_size(rand() % 10, rand() % 10);
        B.resize(other_size.first, other_size.second);
        Test.Check(B.size() == other_size) << "void Matrix<" << name<type>() << " >::resize(std::size_t, std::size_t)" << std::endl;

        Test.Check(E == +E) << "Matrix<" << name<type>() << " > Matrix<" << name<type>() << " >::operator+() const" << std::endl;
        
        Test.Check(-E == (E * static_cast<type>(-1))) << "Matrix<" << name<type>() << " > Matrix<" << name<type>() << " >::operator-() const" << std::endl;

        type other_value(static_cast<type>(rand()));
        for (std::size_t row = 0; row < E.rows(); ++row)
            for (std::size_t column = 0; column < E.columns(); ++column)
                A[row][column] = E[row][column] * other_value;
        Test.Check(A == (Matrix<type>(E) *= other_value)) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator*=(const " << name<type>() << "&)" << std::endl;

        for (std::size_t row = 0; row < E.rows(); ++row)
            for (std::size_t column = 0; column < E.columns(); ++column)
                A[row][column] = E[row][column] / other_value;
        Test.Check(A == (Matrix<type>(E) /= other_value)) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator/=(const " << name<type>() << "&)" << std::endl;

        randomize(B = E);
        for (std::size_t row = 0; row < E.rows(); ++row)
            for (std::size_t column = 0; column < E.columns(); ++column)
                A[row][column] = E[row][column] + B[row][column];
        Test.Check(A == (Matrix<type>(E) += B)) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator+=(const Matrix<" << name<type>() << " >&)" << std::endl;
        try
        {
            D.resize(rand() % 10, rand() % 10);
            D += E;
            Test.Check(D.size() == E.size()) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator+=(const Matrix<" << name<type>() << " >&) (different sizes)" << std::endl;
        }
        catch (...)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator+=(const Matrix<" << name<type>() << " >&) (different sizes)" << std::endl;
        }

        randomize(B = E);
        for (std::size_t row = 0; row < E.rows(); ++row)
            for (std::size_t column = 0; column < E.columns(); ++column)
                A[row][column] = E[row][column] - B[row][column];
        Test.Check(A == (Matrix<type>(E) -= B)) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator-=(const Matrix<" << name<type>() << " >&)" << std::endl;
        try
        {
            D.resize(rand() % 10, rand() % 10);
            D -= E;
            Test.Check(D.size() == E.size()) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator-=(const Matrix<" << name<type>() << " >&) (different sizes)" << std::endl;
        }
        catch (...)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " >& Matrix<" << name<type>() << " >::operator-=(const Matrix<" << name<type>() << " >&) (different sizes)" << std::endl;
        }

        for (std::size_t row = 0; row < rows; ++row)
        {
            Test.Check(std::valarray<type>(A.row(row)).size() == columns) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (length)" << std::endl;
            std::valarray<type> v(columns);
            randomize(v);
            A.row(row) = v;
            for (std::size_t column = 0; column < columns; ++column)
                Test.Check(A.row(row)[column] == v[column]) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (subscripting)" << std::endl;
            randomize(v);
            for (std::size_t column = 0; column < columns; ++column)
            {
                A.row(row)[column] = v[column];
                Test.Check(A.row(row)[column] == v[column]) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (subscripted assignment)" << std::endl;
            }
            A.row(row) += v;
            v += v;
            for (std::size_t column = 0; column < columns; ++column)
                Test.Check(A.row(row)[column] == v[column]) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (computed assignment)" << std::endl;
            try
            {
                A.row(row)[columns];
                Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (column range checking)" << std::endl;
            }
            catch (const std::logic_error&)
            {
                Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (column range checking)" << std::endl;
            }
        }
        try
        {
            A.row(rows);
            Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (row range checking)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) (row range checking)" << std::endl;
        }
            
        for (std::size_t row = 0; row < rows; ++row)
        {
            Test.Check(std::valarray<type>(E.row(row)).size() == columns) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) const (length)" << std::endl;
            try
            {
                E.row(row)[columns];
                Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) const (column range checking)" << std::endl;
            }
            catch (const std::logic_error&)
            {
                Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) const (column range checking)" << std::endl;
            }
        }
        try
        {
            E.row(rows);
            Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) const (row range checking)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::row(std::size_t) const (row range checking)" << std::endl;
        }

        for (std::size_t column = 0; column < columns; ++column)
        {
            Test.Check(std::valarray<type>(A.column(column)).size() == rows) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (length)" << std::endl;
            std::valarray<type> v(rows);
            randomize(v);
            A.column(column) = v;
            for (std::size_t row = 0; row < rows; ++row)
                Test.Check(A.column(column)[row] == v[row]) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (subscripting)" << std::endl;
            randomize(v);
            for (std::size_t row = 0; row < rows; ++row)
            {
                A.column(column)[row] = v[row];
                Test.Check(A.column(column)[row] == v[row]) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (subscripted assignment)" << std::endl;
            }
            A.column(column) += v;
            v += v;
            for (std::size_t row = 0; row < rows; ++row)
                Test.Check(A.column(column)[row] == v[row]) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (computed assignment)" << std::endl;
            try
            {
                A.column(column)[rows];
                Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (row range checking)" << std::endl;
            }
            catch (const std::logic_error&)
            {
                Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (row range checking)" << std::endl;
            }
        }
        try
        {
            A.column(columns);
            Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (column range checking)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) (column range checking)" << std::endl;
        }
            
        for (std::size_t column = 0; column < columns; ++column)
        {
            Test.Check(std::valarray<type>(E.column(column)).size() == rows) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) const (length)" << std::endl;
            try
            {
                E.column(column)[rows];
                Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) const (row range checking)" << std::endl;
            }
            catch (const std::logic_error&)
            {
                Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) const (row range checking)" << std::endl;
            }
        }
        try
        {
            E.column(columns);
            Test.Check(false) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) const (column range checking)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "(implementation-defined) Matrix<" << name<type>() << " >::column(std::size_t) const (column range checking)" << std::endl;
        }

        for (std::size_t row = 0; row < rows; ++row)
            for (std::size_t column = 0; column < columns; ++column)
                A[row][column] = static_cast<type>(10 * row + column);
        for (std::size_t row = 0; row < rows; ++row)
            for (std::size_t column = 0; column < columns; ++column)
                Test.Check(A[row][column] == static_cast<type>(10 * row + column)) << "Matrix<" << name<type>() << " > (indexing sanity)" << std::endl;

        randomize(A);
        B.resize(rows + 1, columns + 1);
        Test.Check(!(A == B) && (A != B)) << "bool operator==(and operator!=)(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (no similarity)" << std::endl;
        B.resize(rows, columns + 1);
        Test.Check(!(A == B) && (A != B)) << "bool operator==(and operator!=)(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same rows)" << std::endl;
        B.resize(rows + 1, columns);
        Test.Check(!(A == B) && (A != B)) << "bool operator==(and operator!=)(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same columns)" << std::endl;
        if (rows && columns)
        {
            randomize(B = A);
            Test.Check(!(A == B) && (A != B)) << "bool operator==(and operator!=)(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same size)" << std::endl;
        }
        B = A;
        Test.Check((A == B) && !(A != B)) << "bool operator==(and operator!=)(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (equality)" << std::endl;

        randomize(B = A);
        Test.Check((A + B) == (Matrix<type>(A) += B)) << "Matrix<" << name<type>() << " > operator+(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&)" << std::endl;
        B.resize(rows + 1, columns + 1);
        try
        {
            B.resize(rows + 1, columns + 1);
            A + B;
            Test.Check(false) << "Matrix<" << name<type>() << " > operator+(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (size mismatch)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " > operator+(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (size mismatch)" << std::endl;
        }
        try
        {
            B.resize(rows, columns + 1);
            A + B;
            Test.Check(false) << "Matrix<" << name<type>() << " > operator+(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same rows)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " > operator+(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same rows)" << std::endl;
        }
        try
        {
            B.resize(rows + 1, columns);
            A + B;
            Test.Check(false) << "Matrix<" << name<type>() << " > operator+(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same columns)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " > operator+(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same columns)" << std::endl;
        }

        randomize(B = A);
        Test.Check((A - B) == (Matrix<type>(A) -= B)) << "Matrix<" << name<type>() << " > operator-(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&)" << std::endl;
        B.resize(rows + 1, columns + 1);
        try
        {
            B.resize(rows + 1, columns + 1);
            A - B;
            Test.Check(false) << "Matrix<" << name<type>() << " > operator-(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (size mismatch)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " > operator-(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (size mismatch)" << std::endl;
        }
        try
        {
            B.resize(rows, columns + 1);
            A - B;
            Test.Check(false) << "Matrix<" << name<type>() << " > operator-(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same rows)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " > operator-(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same rows)" << std::endl;
        }
        try
        {
            B.resize(rows + 1, columns);
            A - B;
            Test.Check(false) << "Matrix<" << name<type>() << " > operator-(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same columns)" << std::endl;
        }
        catch (const std::logic_error&)
        {
            Test.Check(true) << "Matrix<" << name<type>() << " > operator-(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&) (same columns)" << std::endl;
        }

        Test.Check((A * other_value) == (Matrix<type>(A) *= other_value)) << "Matrix<" << name<type>() << " > operator*(const Matrix<" << name<type>() << " >&, const " << name<type>() << "&)" << std::endl;
        Test.Check((other_value * A) == (Matrix<type>(A) *= other_value)) << "Matrix<" << name<type>() << " > operator*(const " << name<type>() << "&, const Matrix<" << name<type>() << " >&)" << std::endl;
        Test.Check((A / other_value) == (Matrix<type>(A) /= other_value)) << "Matrix<" << name<type>() << " > operator/(const Matrix<" << name<type>() << " >&, const " << name<type>() << "&)" << std::endl;

        std::size_t other_common = rand() % 10;
        A.resize(rows, other_common);
        B.resize(other_common, columns);
        randomize(A);
        randomize(B);
        Matrix<type> X(rows, columns);
        for (std::size_t row = 0; row < rows; ++row)
            for (std::size_t column = 0; column < columns; ++column)
                for (std::size_t k = 0; k < other_common; ++k)
                    X[row][column] += A[row][k] * B[k][column];
        for (std::size_t row = 0; row < rows; ++row)
            for (std::size_t column = 0; column < columns; ++column)
                Test.Check(soft_equality((A * B)[row][column], X[row][column])) << "Matrix<" << name<type>() << " > operator*(const Matrix<" << name<type>() << " >&, const Matrix<" << name<type>() << " >&)" << std::endl;

        A.resize(rows, columns);
        randomize(A);

        std::valarray<type> x(columns);
        randomize(x);

        type named_zero = type(); // See Meyers, Effective STL, Item 6
        std::valarray<type> b(named_zero, rows);

        for (std::size_t row = 0; row < rows; ++row)
            for (std::size_t column = 0; column < columns; ++column)
                b[row] += A[row][column] * x[column];
        for (std::size_t row = 0; row < rows; ++row)
            Test.Check(soft_equality((A * x)[row], b[row])) << "std::valarray<" << name<type>() << " > operator*(const Matrix<" << name<type>() << " >&, const std::valarray<" << name<type>() << " >&)" << std::endl;
  
    }
    catch (const std::exception& e)
    {
        Test.Check(false) << "unexpected exception: " << e.what() << std::endl;
    }
    catch (...)
    {
        Test.Check(false) << "unknown exception" << std::endl;
    }
    try
    {
        Matrix<type> A(rows, columns);
        type scalar = 1.0 + rand();
        A = scalar;
        Matrix<type> B(rows, columns, scalar);
        Test.Check(A == B) << "const Matrix<" << name<type>() << " >& operator=(const " << name<type>() << "&)" << std::endl;
        A += scalar;
        Test.Check(A == (B * static_cast<type>(2))) << "const Matrix<" << name<type>() << " >& operator+=(const " << name<type>() << "&)" << std::endl;
        A -= scalar;
        Test.Check(A == B) << "const Matrix<" << name<type>() << " >& operator-=(const " << name<type>() << "&)" << std::endl;
        for (std::size_t row = 0; row < rows; ++row)
            A[row] += scalar;
        Test.Check(A == (B * static_cast<type>(2))) << "subscripted computed assignment" << std::endl;
        for (std::size_t row = 0; row < rows; ++row)
            A[row] = scalar;
        Test.Check(A == B) << "subscripted assignment" << std::endl;
    }
    catch (const std::exception& e)
    {
        Test.Check(false) << "unexpected exception: " << e.what() << std::endl;
    }
    catch (...)
    {
        Test.Check(false) << "unknown exception" << std::endl;
    }
}

template<typename type> void test()
{
    try
    {
        {
            Matrix<type> A;
            Test.Check(A.rows() == 0 && A.columns() == 0) << "Matrix::Matrix<" << name<type>() << " >();" << std::endl;
        }
        Test.Check(true) << "virtual Matrix::~Matrix<" << name<type>() << " >();" << std::endl;
    }
    catch (const std::exception& e)
    {
        Test.Check(false) << "unexpected exception: " << e.what() << std::endl;
    }
    catch (...)
    {
        Test.Check(false) << "unknown exception" << std::endl;
    }

    for (std::size_t rows = 0; rows < 10; ++rows)
        for (std::size_t columns = 0; columns < 10; ++columns)
            test<type>(rows, columns);
}

int main(int argc, char* argv[])
{
    try
    {
        std::string id("$Id: tMatrix.cc,v 1.6 2005/12/01 22:55:03 emaros Exp $");
        Test.Init(argc, argv);
        Test.Message() << id << std::endl;

        test<float>();
        test<double>();
        test<std::complex<float> >();
        test<std::complex<double> >();
    }
    catch (const std::exception& e)
    {
        Test.Check(false) << "unexpected exception: " << e.what() << std::endl;
    }
    catch (...)
    {
        Test.Check(false) << "unknown exception" << std::endl;
    }

    Test.Exit();
}
