#include "datacondAPI/config.h"

#include <cmath>

#include "filters/LDASConstants.hh"
#include "general/unittest.h"
#include "SequenceUDT.hh"
#include "token.hh"

General::UnitTest Test;

template<typename F> void test(const std::string& name, const F& functor)
{
  Parameters arguments;
  CallChain commands;
  // zero arguments
  {
    commands.Reset();
    commands.AppendCallFunction(name, arguments.set(0), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(); threw" << std::endl;
    }
  }
  // one argument
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction(name, arguments.set(1, "n"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(n); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(n); threw" << std::endl;
    }
  }
  // six arguments
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "phi");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "A");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "x0");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "dummy");
    commands.AppendCallFunction(name, arguments.set(6, "n", "f", "phi", "A", "x0", "dummy"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(n,f,phi,A,x0,dummy); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(n,f,phi,A,x0,dummy); threw" << std::endl;
    }
  }
  // two arguments (1st type bad)
  {
    commands.Reset();
    commands.AppendCallFunction("double", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction(name, arguments.set(2, "n", "f"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "([double],f); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "([double],f); threw" << std::endl;
    }
  }
  // two arguments (2nd type bad)
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("dcomplex", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction(name, arguments.set(2, "n", "f"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(n,[complex]); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(n,[complex]); threw" << std::endl;
    }
  }
  // two arguments (1st value bad)
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "0"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction(name, arguments.set(2, "n", "f"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(0,f); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(0,f); threw" << std::endl;
    }
  }
  // two arguments
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction(name, arguments.set(2, "n", "f"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    commands.Execute();
    datacondAPI::Sequence<double>& y = dynamic_cast<datacondAPI::Sequence<double>&>(*commands.GetSymbol("y"));
    std::valarray<double> z(10);
    for (std::size_t i = 0; i < z.size(); ++i)
    {
      z[i] = functor(LDAS_TWOPI * .3 * i);
    }
    z -= y;
    z *= z;
    Test.Check(z.sum() < .001) << name << "(10, .3)" << std::endl;
  }
  // three arguments (3rd type bad)
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction("dcomplex", arguments.set(1, ".4"), "phi");
    commands.AppendCallFunction(name, arguments.set(3, "n", "f", "phi"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(n,f,[complex]); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(n,f,[complex]); threw" << std::endl;
    }
  }
  // three arguments
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction("double", arguments.set(1, ".4"), "phi");
    commands.AppendCallFunction(name, arguments.set(3, "n", "f", "phi"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    commands.Execute();
    datacondAPI::Sequence<double>& y = dynamic_cast<datacondAPI::Sequence<double>&>(*commands.GetSymbol("y"));
    std::valarray<double> z(10);
    for (std::size_t i = 0; i < z.size(); ++i)
    {
      z[i] = functor(LDAS_TWOPI * .3 * i + .4);
    }
    z -= y;
    z *= z;
    Test.Check(z.sum() < .001) << name << "(10, .3, .4)" << std::endl;
  }
  // four arguments (4th type bad)
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction("double", arguments.set(1, ".4"), "phi");
    commands.AppendCallFunction("dcomplex", arguments.set(1, ".5"), "A");
    commands.AppendCallFunction(name, arguments.set(4, "n", "f", "phi", "A"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(n,f,phi,[complex]); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(n,f,phi,[complex]); threw" << std::endl;
    }
  }
  // four arguments
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction("double", arguments.set(1, ".4"), "phi");
    commands.AppendCallFunction("double", arguments.set(1, ".5"), "A");
    commands.AppendCallFunction(name, arguments.set(4, "n", "f", "phi", "A"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    commands.Execute();
    datacondAPI::Sequence<double>& y = dynamic_cast<datacondAPI::Sequence<double>&>(*commands.GetSymbol("y"));
    std::valarray<double> z(10);
    for (std::size_t i = 0; i < z.size(); ++i)
    {
      z[i] = .5 * functor(LDAS_TWOPI * .3 * i + .4);
    }
    z -= y;
    z *= z;
    Test.Check(z.sum() < .001) << name << "(10, .3, .4, .5)" << std::endl;
  }
  // five arguments (5th type bad)
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction("double", arguments.set(1, ".4"), "phi");
    commands.AppendCallFunction("double", arguments.set(1, ".5"), "A");
    commands.AppendCallFunction("dcomplex", arguments.set(1, ".6"), "x0");
    commands.AppendCallFunction(name, arguments.set(5, "n", "f", "phi", "A", "x0"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    try
    {
      commands.Execute();
      Test.Check(false) << "y = " << name << "(n,f,phi,A,[complex]); did not throw" << std::endl;
    }
    catch (...)
    {
      Test.Check(true) << "y = " << name << "(n,f,phi,A,[complex]); threw" << std::endl;
    }
  }
  // five arguments
  {
    commands.Reset();
    commands.AppendCallFunction("integer", arguments.set(1, "10"), "n");
    commands.AppendCallFunction("double", arguments.set(1, ".3"), "f");
    commands.AppendCallFunction("double", arguments.set(1, ".4"), "phi");
    commands.AppendCallFunction("double", arguments.set(1, ".5"), "A");
    commands.AppendCallFunction("double", arguments.set(1, ".6"), "x0");
    commands.AppendCallFunction(name, arguments.set(5, "n", "f", "phi", "A", "x0"), "y");
  	commands.AppendIntermediateResult("y", "y", "y", "");
    commands.Execute();
    datacondAPI::Sequence<double>& y = dynamic_cast<datacondAPI::Sequence<double>&>(*commands.GetSymbol("y"));
    std::valarray<double> z(10);
    for (std::size_t i = 0; i < z.size(); ++i)
    {
      z[i] = .5 * functor(LDAS_TWOPI * .3 * i + .4) + .6;
    }
    z -= y;
    z *= z;
    Test.Check(z.sum() < .001) << name << "(10, .3, .4, .5, .6)" << std::endl;
  }
}

namespace sgt
{
  double square(double x)
  {
    return fmod(x, LDAS_TWOPI) < LDAS_PI ? 1. : -1.;
  }

  double sawtooth(double x)
  {
    return fmod(x, LDAS_TWOPI) / LDAS_TWOPI;
  }

  double triangle(double x)
  {
    return square(x) > 0 ? 2. * fmod(x, LDAS_PI) / LDAS_PI - 1. : 1. - 2. * fmod(x, LDAS_PI) / LDAS_PI;
  }
}

int main(int argc, char* argv[])
{
  Test.Init(argc, argv);
  Test.Message() << "$Id: token_SignalGeneratorFunction.cc,v 1.4 2005/12/01 22:55:03 emaros Exp $" << std::endl;

  try
  {
    // Sine and cosine may be overloaded so need to ensure getting the double
    //   version of these functions.
    double (*sin_func)( double ) = &::sin;
    double (*cos_func)( double ) = &::cos;

    test("sine", sin_func);
    test("cosine", cos_func);
    test("square", &sgt::square);
    test("sawtooth", &sgt::sawtooth);
    test("triangle", &sgt::triangle);
  }
  catch (const std::exception& x)
  {
    Test.Check(false) << "unexpected exception " << x.what() << std::endl;
  }
  catch (...)
  {
    Test.Check(false) << "unexpected exception" << std::endl;
  }

  Test.Exit();
}

