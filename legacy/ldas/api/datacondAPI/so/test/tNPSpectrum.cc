/* -*- mode: c++; c-basic-offset: 2; -*- */
// $Id: tNPSpectrum.cc,v 1.10 2005/12/01 22:55:03 emaros Exp $
#include "datacondAPI/config.h"

#include <iostream>
#include <sstream>
#include <stdexcept>
#include <string>
#include <valarray>
#include <complex>

#include "general/unittest.h"

#include "NPSpectrum.hh"
#include "ScalarUDT.hh"

using namespace std;

General::UnitTest	Test;

template <class T>
bool Constructors(void);

template <class T>
bool Exceptions(void);

template <class T>
bool Accessors(void);

bool Mutators(void);

int main(int ArgC, char** ArgV) 
{
  
  Test.Init(ArgC, ArgV);
  if (Test.IsVerbose())
    {
      cout 
	<< "$Id: tNPSpectrum.cc,v 1.10 2005/12/01 22:55:03 emaros Exp $" 
	<< std::endl;
    }
  
  // Test constructors
  
  Test.Message() << "Testing constructors (float). . . " << std::endl;
  Test.Check(Constructors<float>()) 
    << "Constructors<float>" << std::endl;
  
  Test.Message() << "Testing constructors (double). . . " << std::endl;
  Test.Check(Constructors<double>()) 
    << "Constructors<double>" << std::endl;
  
  Test.Message() << "Testing constructors (complex<float>). . . " << std::endl;
  Test.Check(Constructors<complex<float> >()) 
    << "Constructors<complex<float> >" << std::endl;
  
  Test.Message() << "Testing constructors (complex<double>). . . " 
		 << std::endl;
  Test.Check(Constructors<complex<double> >()) 
    << "Constructors<complex<double> >" << std::endl;
  
  // Test exceptions
  
  Test.Message() << "Testing exceptions (float) . . . " << std::endl;
  Test.Check(Exceptions<float>()) << "Exceptions<float>" << std::endl;
  
  Test.Message() << "Testing exceptions (double) . . . " << std::endl;
  Test.Check(Exceptions<double>()) << "Exceptions<double>" << std::endl;
  
  Test.Message() << "Testing exceptions (complex<float>) . . . " << std::endl;
  Test.Check(Exceptions<complex<float> >()) 
    << "Exceptions<complex<float> >" << std::endl;
  
  Test.Message() << "Testing exceptions (complex<double>) . . . " << std::endl;
  Test.Check(Exceptions<complex<double> >()) 
    << "Exceptions<complex<double> >" << std::endl;
  
  // Test accessors
  
  Test.Message() << "Testing accessors (float) . . . " << std::endl;
  Test.Check(Accessors<float>()) << "Accessors<float>" << std::endl;
  
  Test.Message() << "Testing accessors (double) . . . " << std::endl;
  Test.Check(Accessors<double>()) << "Accessors<double>" << std::endl;
  
  Test.Message() << "Testing accessors (complex<float>) . . . " << std::endl;
  Test.Check(Accessors<complex<float> >()) 
    << "Accessors<complex<float> >" << std::endl;
  
  Test.Message() << "Testing accessors (complex<double>) . . . " << std::endl;
  Test.Check(Accessors<complex<double> >()) 
    << "Accessors<complex<double> >" << std::endl;
  
  // Test mutators
  
  Test.Message() << "Testing mutators . . . " << std::endl;
  Test.Check(Mutators()) << "Mutators" << std::endl;
  
  Test.Exit();
}

string debugInfo(const string&, int);

template <class T>
bool Constructors(void)
{
  
  bool flag = true;
  
  // NPSpectrum()
  try 
    {
      datacondAPI::NPSpectrum<T> nps;
      Test.Check(true) 
	<< "Default constructor" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  catch (...)
    {
      Test.Check(flag = false) 
	<< "Default constructor caught exception" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  
  //: Construct from dimensions, data, axis values
  //!param: dims - the dimension bounds of the axes
  //!param: data - the spectrum data in column-major order
  //!param: axesLB - value associated with index 0 on each axis
  //!param: axesUB - value associated with index upper bound on each axis
  
  // Create 2x3x5 
  
  valarray<size_t> dims(3);
  dims[0] = 2;
  dims[1] = 3;
  dims[2] = 5;
  valarray<T> data(30);
  for (size_t k = 0; k < data.size(); k++)
    data[k] = k;
  valarray<double> axesLB(3);
  valarray<double> axesUB(3);
  
  for (size_t k = 0; k < 3; k++)
    {
      axesLB[k] = k;
      axesUB[k] = 2*(k+1);
    }
  
  try 
    {
      datacondAPI::NPSpectrum<T> nps(dims, data, axesLB, axesUB);
      Test.Check(true) 
	<< "Omnibus constructor" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  catch (...)
    {
      Test.Check(flag = false) 
	<< "Omnibus constructor" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  
  // Copy constructor
  
  datacondAPI::NPSpectrum<T> nps(dims, data, axesLB, axesUB);
  try
    {
      datacondAPI::NPSpectrum<T> nps(dims, data, axesLB, axesUB);
      datacondAPI::NPSpectrum<T> npsCopy(nps);
      Test.Check(true) 
	<< "Copy constructor" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  catch (...)
    {
      Test.Check(flag = false) 
	<< "Copy constructor" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  
  // Clone method
  
  datacondAPI::NPSpectrum<T>* p_nps;
  try
    {
      datacondAPI::NPSpectrum<T> nps(dims, data, axesLB, axesUB);
      p_nps = nps.Clone();
      Test.Check(true) 
	<< "Clone method" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  catch (...)
    {
      Test.Check(flag = false) 
	<< "Clone method (caught unexpected exception)" 
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  
  return flag;
  
}

template <class T>
bool Exceptions(void) { 
  
  bool flag = true;
  
  string what;
  
  // Constructor exceptions
  
  const int ndims = 3;
  std::valarray<size_t> dims(ndims);
  std::valarray<T> data;
  std::valarray<double> LB(ndims);
  std::valarray<double> UB(ndims);
  
  int ndata = 1;
  for (int k = 0; k < ndims; k++)
    {
      dims[k] = 2*k+1;
      ndata *= dims[k];
      LB[k] = 2*k+1;
      UB[k] =3*k+2;
    }
  data.resize(ndata);
  for (int k = 0; k < ndata; k++)
    {
      data[k] = k;
    }
  
  what = "dims not compatible with data";
  try
    {
      // dims not compatible with data
      std::valarray<size_t> bdims(dims.size());
      bdims = dims + size_t(1);
      datacondAPI::NPSpectrum<T> nps(bdims,data,LB,UB);
      Test.Check(false) 
	<< what
	<< debugInfo(__FILE__, __LINE__)
	<< std::endl;
      flag = false;
    }
  catch (std::length_error& e)
    {
      Test.Check(true) 
	<< what
	<< " (" << e.what() << ")"
	<< debugInfo(__FILE__, __LINE__)
	<< std::endl;
    }
  catch (...)
    {
      Test.Check(false)
	<< what
	<< " (unexpected exception)"
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  
  what = "bounds not compatible with dims";
  try
    {
      // bounds not compatible with data
      std::valarray<double> bBounds(ndims-1); 
      datacondAPI::NPSpectrum<T> nps(dims,data,bBounds,UB);
      Test.Check(false) 
	<< what
	<< debugInfo(__FILE__, __LINE__)
	<< std::endl;
      flag = false;
    }
  catch (std::length_error& e) 
    {
      Test.Check(true) 
	<< what
	<< " (" << e.what() << ")"
	<< debugInfo(__FILE__, __LINE__)
	<< std::endl;
    }
  catch (...)
    {
      Test.Check(false)
	<< what
	<< " (unexpected exception)"
	<< debugInfo(__FILE__,__LINE__)
	<< std::endl;
    }
  
  {
    std::valarray<double> bBounds(ndims+1); 
    what = "bounds not compatible with dims";
    try
      {
	// bounds not compatible with data
	datacondAPI::NPSpectrum<T> nps(dims,data,LB,bBounds);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::length_error& e) 
      {
	Test.Check(true) 
	  << what
	  << " (" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
  }
  
  // non-positive dimension
  
  {
    std::valarray<size_t> bdims(dims.size());
    bdims = dims;
    bdims[1] = 0;
    what = "zero dimension";
    try
      {
	datacondAPI::NPSpectrum<T> nps(bdims,data,LB,UB);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::out_of_range& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
  }
  // getAxis exceptions
  
  {
    datacondAPI::NPSpectrum<T> nps(dims,data,LB,UB);
    datacondAPI::Scalar<double> min(double(0.0));
    datacondAPI::Scalar<double> max(double(0.0));
    size_t k = dims.size();
    what = "getAxis(Scalar<double>&,Scalar<double>&,size_t k >= dims.size())";
    try
      {
	nps.getAxis(min,max,k);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::length_error& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
    
    double dmin;
    double dmax;
    what = "getAxis(double&,double&,size_t k >= dims.size())";
    try
      {
	nps.getAxis(dmin,dmax,k);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::length_error& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
  }
  
  // operator[] exceptions
  
  {
    std::valarray<size_t> ndx;
    datacondAPI::NPSpectrum<T> nps(dims,data,LB,UB);
    
    what = "operator[](const valarray<size_t>&)";
    T foo;
    try
      {
	ndx.resize(dims.size());
	ndx = dims;
	foo = nps[ndx];
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::out_of_range& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
  }
  
  // slice exceptions
  
  {
    datacondAPI::NPSpectrum<T> nps(dims,data,LB,UB);
    datacondAPI::NPSpectrum<T> nps0(nps);
    datacondAPI::udt* p_udt;
    std::valarray<int> mask(dims.size());
    
    mask = 0; // no dimensions selected
    
    what = "slice(datacondAPI::udt*& out, const valarray<int>& mask) const";
    try
      {
	p_udt = &nps0;
	nps.slice(p_udt, mask);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::invalid_argument& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
    
    what = "slice(datacondAPI::udt*& out, const valarray<int>& mask) const";
    p_udt = 0;
    try
      {
	nps.slice(p_udt, mask);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::invalid_argument& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
    
    mask.resize(dims.size()+1);
    mask = 1;
    
    nps0 = nps;
    p_udt = &nps0;
    what = "slice(datacondAPI::udt*& out, const valarray<int>& mask) const";
    try
      {
	nps.slice(p_udt, mask);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::invalid_argument& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
    
    p_udt = 0;
    what = "slice(datacondAPI::udt*& out, const valarray<int>& mask) const";
    try
      {
	nps.slice(p_udt, mask);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::invalid_argument& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
  }
  
  // getData exceptions
  
  
  {
    datacondAPI::NPSpectrum<T> nps(dims,data,LB,UB);
    std::valarray<int> mask(dims.size());
    std::valarray<T> data0;
    
    mask = 0; // no dimensions selected
    
    what = "getData(valarray<T>&, const valarray<int>& mask) const";
    try
      {
	nps.getData(data0, mask);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::invalid_argument& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
    
    mask.resize(dims.size()+1);
    mask = 1;
    
    what = "getData(valarray<T>&, const valarray<int>& mask) const";
    try
      {
	nps.getData(data0, mask);
	Test.Check(false) 
	  << what
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = false;
      }
    catch (std::invalid_argument& e)
      {
	Test.Check(true) 
	  << what
	  << "(" << e.what() << ")"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (...)
      {
	Test.Check(false)
	  << what
	  << " (unexpected exception)"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
  }
  
  // resize exceptions
  
  flag = false;
  
  // setAxes exceptions
  
  flag = false;
  
  // setData exceptions
  
  flag = false;
  
  return flag;
} 

template <class T>
bool Accessors(void) 
{ 
  bool flag = true;
  bool tmp = true;
  
  // Create 2x3x5 
  
  const int ndims = 3;
  
  valarray<size_t> dims(ndims);
  int ndata = 1;
  for (int k = 0; k < ndims; k++)
    {
      dims[k] = k*k+2;
      ndata *= dims[k];
    }
  
  std::valarray<size_t> strides(ndims);
  strides = 1;
  for (size_t k = ndims-1; k > 0; k--)
    {
      strides[k-1] = strides[k]*dims[k];
    }
  
  valarray<T> data(ndata);
  for (size_t k = 0; k < data.size(); k++)
    {
      data[k] = k;
    }
  
  valarray<double> axesLB(ndims);
  valarray<double> axesUB(ndims);
  for (size_t k = 0; k < axesLB.size(); k++)
    {
      axesLB[k] = k;
      axesUB[k] = 2*(k+1);
    }
  
  datacondAPI::NPSpectrum<T> nps(dims, data, axesLB, axesUB);
  
  // getNDims()
  
  Test.Check( tmp = (nps.getNDims() == dims.size()) )
    << "getNDims()"
    << debugInfo(__FILE__,__LINE__)
    << std::endl;
  flag = flag && tmp;
  
  // getDims(), getAxes(), getData(valarray<T>&)
  
  valarray<size_t> dimsCopy(0);
  valarray<T> dataCopy(0);
  valarray<double> axesLBCopy(0);
  valarray<double> axesUBCopy(0);
  
  nps.getDims(dimsCopy);
  // getDims
  
  Test.Check( tmp = dimsCopy.size() == dims.size() )
    << "getDims(dims): dims.size()"
    << debugInfo(__FILE__,__LINE__)
    << std::endl;
  flag = flag && tmp;
  
  tmp = true;
  for (size_t k = 0; k < dims.size(); k++)
    {
      tmp = tmp && dims[k] == dimsCopy[k];
    }
  Test.Check(tmp) 
    << "dims values" 
    << debugInfo(__FILE__, __LINE__)
    << std::endl;
  
  nps.getAxes(axesLBCopy, axesUBCopy);
  Test.Check( tmp = (axesLBCopy.size() == axesLB.size()) )
    << "getAxes(LB, UB): LB.size()"
    << debugInfo(__FILE__,__LINE__)
    << std::endl;
  flag = flag && tmp;
  
  Test.Check( tmp = (axesUBCopy.size() == axesUB.size()) )
    << "getAxes(LB, UB): UB.size()"
    << debugInfo(__FILE__,__LINE__)
    << std::endl;
  flag = flag && tmp;
  
  tmp = true;
  for (size_t k = 0; k < axesUB.size(); k++)
    {
      tmp = tmp && (axesUB[k] == axesUBCopy[k]);
      tmp = tmp && (axesLB[k] == axesLBCopy[k]);
    }
  Test.Check(tmp) 
    << "getAxes() values" 
    << debugInfo(__FILE__, __LINE__)
    << std::endl;
  flag = flag && tmp;
  
  // getAxis(double&,double&)
  
  tmp = true;
  for (size_t k = 0; k < axesUB.size(); k++)
    {
      double min = 0; 
      double max = 0;
      nps.getAxis(min, max, k);
      tmp = tmp && (axesLB[k] == min);
      tmp = tmp && (axesUB[k] == max);
    }
  Test.Check(tmp) 
    << "getAxis() values" 
    << debugInfo(__FILE__, __LINE__)
    << std::endl;
  flag = flag && tmp;
  
  // getData(valarray<T>&)
  
  nps.getData(dataCopy);
  Test.Check( tmp = (data.size() == dataCopy.size()) )
    << "getData(valarray<T>&): data.size()"
    << debugInfo(__FILE__,__LINE__)
    << std::endl;
  flag = flag && tmp;
  
  tmp = true;
  for (size_t k = 0; k < data.size(); k++)
    {
      tmp = tmp && (data[k] == dataCopy[k]);
    }
  Test.Check(tmp) 
    << "getData(valarray<T>&) values" 
    << debugInfo(__FILE__, __LINE__)
    << std::endl;
  
  // operator[]
  
  {
    std::valarray<size_t> ndx(ndims);
    ndx = 0;
    tmp = true;
    for (int n = 0; n < ndata; n++)
      {
	size_t ndx0 = 0;
	for (int k = 0; k < ndims; k++)
	  {
	    ndx0 += ndx[k]*strides[k];
	  }
	tmp = tmp && ( nps[ndx] == data[ndx0]);
	
	int incr = 1;
	for ( int k = 0; k < ndims; k++)
	  {
	    ndx[k] += incr;
	    if (ndx[k] < dims[k])
	      {
		incr = 0;
	      }
	    else
	      {
		ndx[k] = 0;
		incr = 1;
	      }
	  }
      }
    Test.Check(tmp) 
      << "operator[](valarray<size_t>) values" 
      << debugInfo(__FILE__, __LINE__)
      << std::endl;
    flag = flag && tmp;
  }
  
  // slice(udt*&, const valarray<int>&)
  
  Test.Message() << "Starting slice tests..." << std::endl;
  
  {
    std::valarray<int> mask(dims.size());
    for (int k = 0; k < ndims; k++)
      {
	mask[k] = dims[k] - 1;
      }
    mask[1] = -1;
    
    datacondAPI::udt* p_udt = 0;
    try
      {
	nps.slice(p_udt,mask);
	Test.Check (flag = flag && (p_udt != 0))
	  << "slice() allocates to null pointer"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (std::length_error& e)
      {
	Test.Check (flag = false)
	  << "slice() allocates to null pointer:"
	  << " caught length_error "
	  << "(" << e.what() << ")?"
	  << debugInfo(__FILE__,__LINE__)
	  << std::endl;
      }
    catch (std::invalid_argument& e)
      {
	Test.Check (flag = false)
	  << "slice() allocates to null pointer:"
	  << " caught invalid_argument "
	  << "(" << e.what() << ")?"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    catch (std::out_of_range& e)
      {
	Test.Check (flag = false)
	  << "slice() allocates to null pointer:"
	  << " caught out_of_range "
	  << "(" << e.what() << ")?"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }	  
    catch (...)
      {
	Test.Check (flag = false)
	  << "slice() allocates to null pointer:"
	  << " Caught unexpected exception"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
      }
    
    Test.Check(datacondAPI::udt::IsA<datacondAPI::NPSpectrum<T> >(*p_udt)) 
      << "slice() allocates to null pointer:"
      << " NPSpectrum<T> allocated"
      << debugInfo(__FILE__, __LINE__)
      << std::endl;
    
    if (p_udt != 0)
      {
	datacondAPI::NPSpectrum<T>& nps0
	  = datacondAPI::udt::Cast<datacondAPI::NPSpectrum<T> >(*p_udt);
	
	int ndata = 1;
	for (size_t k = 0; k < dims.size(); k++)
	  {
	    if (mask[k] < 0) ndata *= dims[k];
	  }
	Test.Message() << "ndata = " << ndata << std::endl;
	
	// Check values
	
	std::valarray<size_t> ndx(nps.getNDims());
	std::valarray<size_t> ndx0(nps0.getNDims());
	std::valarray<size_t> dims0;
	nps0.getDims(dims0);
	
	// initialize ndx
	for (size_t k = 0; k < mask.size(); k++)
	  {
	    if (mask[k] < 0) 
	      {
		ndx[k] = 0;
	      }
	    else
	      {
		ndx[k] = mask[k];
	      }
	  }
	
	// initialize ndx0
	ndx0 = 0;
	
	bool tmp = true;
	for (int k = 0; k < ndata; k++)
	  {
	    
	    // advance ndx to next element in slice
	    int incr = 1;
	    for (size_t j = 0; j < ndx.size(); j++)
	      {
		// only increment if not masked
		if (mask[j] <= 0)
		  {
		    ndx[j] += incr;
		    incr = 0;
		    if (ndx[j]>=dims[j])
		      {
			incr = 1;
			ndx[j] = 0;
		      }
		  }
	      }
	    
	    incr = 1;
	    for (size_t j = 0; j < ndx0.size(); j++)
	      {
		ndx0[j] += incr;
		incr = 0;
		if (ndx0[j] >= dims0[j])
		  {
		    ndx0[j] = 0;
		    incr = 1;
		  }
	      }
	    
	    Test.Message() 
	      << "nps[ndx] = " 
	      << nps[ndx] 
	      << " == "
	      << nps0[ndx0]
	      << " = nps0[ndx0]"
	      << std::endl;
	    
	    tmp = tmp && (nps[ndx] == nps0[ndx0]);
	  }
	Test.Check(tmp) 
	  << "slice(udt*& spec, const valarray<int>& mask) values"
	  << debugInfo(__FILE__, __LINE__)
	  << std::endl;
	flag = flag && tmp;
	
	// getData(valarray<T>& data, const valarray<int>& mask)
	
	std::valarray<T> d1;
	std::valarray<T> d2;
	
	nps.getData(d1,mask);
	nps0.getData(d2);
	
	if (d1.size() == d2.size())
	  {
	    Test.Check(true) 
	      << "getData(data,mask): data.size() check"
	      << debugInfo(__FILE__,__LINE__)
	      << std::endl;
	    
	    bool tmp = true;
	    for (size_t k = 0; k < d1.size(); k++)
	      {
		tmp = tmp && (d1[k] == d2[k]);
	      }
	    Test.Check(tmp) 
	      << "getData(data,mask): values"
	      << debugInfo(__FILE__,__LINE__)
	      << std::endl;
	    flag = flag && tmp;
	  }
      }
  }
  return flag;
}

bool Mutators(void)
{
  
  // resize
  
  // setAxis
  
  // setAxes
  
  // slice
  
  // setData
  
  return true;
  //  return false;
}

string debugInfo(const string& file, int line)
{
  ostringstream ss;
  ss << " (" << file << ":" << line << ") ";
  return ss.str();
}
