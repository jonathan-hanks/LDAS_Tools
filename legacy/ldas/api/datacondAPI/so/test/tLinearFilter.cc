#include "datacondAPI/config.h"

#include <complex>
#include <stdexcept>

#include "general/unittest.h"

#include "LinearFilter.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "random.hh"

General::UnitTest Test;

// LinearFilter(const LinearFilter&)

// ~LinearFilter()

// LinearFilter& operator=(const LinearFilter&)

// LinearFilter* Clone() const

// ILwd::LdasElement* ConvertToIlwd(const CallChain&, udt::target_type = udt::TARGET_GENERIC) const

// template<typename type> void SetCoefficientsA(const std::valarray<type>&, const signed int& = 0)

// template<typename type> void SetCoefficientsB(const std::valarray<type>&, const signed int& = 0)

// template<typename in, typename out> void apply(std::valarray<out>&, const std::valarray<in>&)

// void SetCoefficientsA(const udt&)

// void SetCoefficientsA(const udt&, const udt&)

// void SetCoefficientsB(const udt&)

// void SetCoefficientsB(const udt&, const udt&)

// void apply(udt*&, const udt&)

datacondAPI::gasdevstate seed(-314159);

template<typename type> void TestCoefficientA()
{
    datacondAPI::Sequence<type> a;
    datacondAPI::gasdev(seed, a, 10);
    datacondAPI::Scalar<int> n(10);

    {
        datacondAPI::LinearFilter f;
        Test.Message() << "A Coefficients std::valarray\n";
        f.SetCoefficientsA(a, 10);
    }
    {
        datacondAPI::LinearFilter f;
        Test.Message() << "A Coefficients UDT\n";
        f.SetCoefficientsA(a, n);
    }
    try
    {
        datacondAPI::LinearFilter f;
        f.SetCoefficientsA(n, n);
        Test.Check(false) << "A Coefficients bad UDT (nothrow)\n";
    }
    catch(const std::invalid_argument& x)
    {
        Test.Check(true) << "A Coefficients bad UDT (threw: " << x.what() << ")\n";
    }
}

int main(int ArgC, char** ArgV)
{
    try
    {
        Test.Init(ArgC, ArgV);
        Test.Message() << "$Id: tLinearFilter.cc,v 1.6 2005/12/01 22:55:03 emaros Exp $\n";

        {
            // messages before errors that would be fatal
            Test.Message() << "default constructor: LinearFilter a\n";
            datacondAPI::LinearFilter a;
            Test.Message() << "copy constructor (null LinearFilter): LinearFilter b(a)\n";
            datacondAPI::LinearFilter b(a);
            Test.Message() << "clone (null LinearFilter): LinearFilter* c = a.Clone();\n";
            datacondAPI::LinearFilter* c = a.Clone();
            Test.Check(c) << "clone produces non-null pointer: c != 0\n";
            Test.Message() << "destructor (null LinearFilter): delete c\n";
            delete c;
        }
        Test.Message() << "A Coefficients <float>\n";
        TestCoefficientA<float>();
        Test.Message() << "A Coefficients <double>\n";
        TestCoefficientA<double>();
        Test.Message() << "A Coefficients <std::complex<float> >\n";
        TestCoefficientA<std::complex<float> >();
        Test.Message() << "A Coefficients <std::complex<double> >\n";
        TestCoefficientA<std::complex<double> >();
    }
    catch (const std::exception& x)
    {
        Test.Check(false) << "Caught standard exception \"" << x.what() << "\n";
    }
    catch (...)
    {
        Test.Check(false) << "Caught non-standard exception\n";
    }
  Test.Exit();
}
