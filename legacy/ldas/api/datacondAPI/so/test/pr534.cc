//! author="Edward Maros"
// $Id: pr534.cc,v 1.3 2005/12/01 22:55:02 emaros Exp $

/*
=========================================================================
 Number:        534 
 Category:      data_conditioning_api 
 Synopsis:      REAL_4 scalars are converted to REAL_8 
 Confidential:  no 
 Severity:      non-critical 
 Priority:      low 
 Responsible:   dataConditioningAPI 
 Class:         sw-bug 
 Submitter-Id:  ldas 
 Arrival-Date:  Wed Nov 15 19:16:00 PST 2000 
 Closed-Date:
 Last-Modified:
 Originator:    charlton@ligo.caltech.edu 
 Release:       ldas-i.j.k 
 Organization:
 Environment:

 Description:
                 When the dcAPI call chain ingests a real_4 scalar from
                 an ILWD it converts it to a real_8 scalar. At present
                 there is no support for real_4 scalar types in the call
                 chain.
 File Attachments:
 How-To-Repeat:
                 Ingest an ILWD file with an element such as
                 <real_4 name='bias'>0</real_4>
                 and write out the the value using the value() action -
                 it will come out as real_8 instead of real_4
 Fix:

 Release-Note:
 Unformatted:
=========================================================================
*/

#include "datacondAPI/config.h"

#include "general/unittest.h"
#include "general/util.hh"

#include "genericAPI/registry.hh"

#include "token.hh"

#include "CallChain.hh"

#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "TypeInfo.hh"

General::UnitTest	Test;

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */
    int main(int argc, char** argv);
#ifdef __cplusplus
}
#endif	/* __cplusplus */

int
main(int argc, char** argv)
{
  Test.Init(argc, argv);
  Test.Message()
    << "$Id: pr534.cc,v 1.3 2005/12/01 22:55:02 emaros Exp $"
    << endl << flush;

  ILwd::LdasArray<REAL_4>	data((REAL_4)(2147483647), "data");

  CallChain	cmds;
  Parameters	args;

  data.setComment("Max Signed Int");

  try
  {
    datacondAPI::Scalar<REAL_4>*	r;
    REAL_4				expect(2147483648U);

    cmds.Reset();
    cmds.IngestILwd(data);
    cmds.AppendCallFunction("integer", args.set(1, "1"), "one");
    cmds.AppendCallFunction("add", args.set(2, "one", "data"), "result");
    cmds.Execute();

    r = dynamic_cast<datacondAPI::Scalar<REAL_4>*>(cmds.GetSymbol("data"));
    Test.Message()
      << datacondAPI::TypeInfoTable.GetName(typeid(*(cmds.GetSymbol("data"))))
      << endl << flush;
    Test.Check(r) << "data" << endl << flush;
    if (r)
    {
      Test.Check(r->GetValue() == data.getData()[0])
	<< "result: " << r->GetValue()
	<< " expected: " << data.getData()[0]
	<< endl << flush;
    }

    r = dynamic_cast<datacondAPI::Scalar<REAL_4>*>(cmds.GetSymbol("result"));
    Test.Check(r) << "result" << endl << flush;
    if (r)
    {
      Test.Check(r->GetValue() == expect)
	<< "result: " << r->GetValue()
	<< " expected: " << expect
	<< endl << flush;
    }
  }
  CATCH(Test);

    

  Test.Exit();
}
