//
// tStatistics.cc:
//
// Test driver for Statistics class
//
//

#include "datacondAPI/config.h"

// other includes
#include <general/unittest.h>

#include "Statistics.hh"
#include "StatsUDT.hh"
#include "TimeSeries.hh"

// the following tests are performed:
//
// test all exceptions (n == 0, n== 1)
// test trivial data (n == 1)
// test valid floating point data (n == 2) 
// test valid integer data (n == 3, all ones)
// test valid integer data (n == 4)
// test valid floating point data (n == 5)
// test valid double precision data (n == 6)
// test valid random floating point data (n=1024)

using namespace datacondAPI;
using General::GPSTime;
using namespace std;

General::UnitTest Test;

//-----------------------------------------------------------------------
// read input data from a file 
// (returns length of input)
//
template<class T>
int readData(const char* fileName, 
	     T*          input)
{

  // attach source directory to file name
  std::string path;

  if (getenv("DATADIR"))
  {
    path  = getenv("DATADIR");
    path += "/";
  }
  path += fileName;

  std::ifstream inFile(path.c_str());

  // make sure that file exists
  if (inFile == 0) 
  {
    std::cout << "missing file: " << path << std::endl;
    abort();
  }

  int i = 0;
  while (1) 
  {
    inFile >> input[i]; // must try to read in data before generating an
                        // end-of-file flag.  works ok if file is empty.  
    if (inFile.eof()) 
    {
      return i;  
    }
    
    i++;
  }
}
//------------------------------------------------------------------------
// test error exceptions for an individual statistic
//
template<class T>
bool testIndividualError(const std::string&      name,
	                 double (Statistics::* pmf)(const Sequence<T>& data) const,
			 const Sequence<T>& data)
{
  bool pass = false;

  Statistics statsObj;

  try
  {
    try
    {
      (statsObj.*pmf)(data);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name << " ("<< r.what() << ")" << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name << " (no exception thrown)" << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }
  return pass;
}
//------------------------------------------------------------------------
// test error exception for the "all" statistics method
//
template<class T>
bool testAllStatsError(const std::string&      name,
		       const Sequence<T>& data)
{
  bool pass = false;

  Statistics statsObj;
  Stats      allstats;

  try
  {
    try
    {
      statsObj.all(allstats, data);
    }
    catch (std::invalid_argument& r)
    {
      pass = true;
      Test.Check(pass) << name << " ("<< r.what() << ")" << std::endl;
    }
    if (!pass)
    {
      Test.Check(pass) << name << " (no exception thrown)" << std::endl;
    }
  }
  catch (std::exception& r) 
  {
    pass = false;
    Test.Check(pass) << name << " (unexpected exception: "<< r.what() << ")" 
		     << std::endl;
  }

  return pass;
}
//------------------------------------------------------------------------
// test all error exceptions (n==0, n==1)
//
template<class T>
bool testErrors(void)
{
  bool allPass = true;

  // no data (n == 0)
  Sequence<T> data0;               
  
  allPass = allPass && testIndividualError<T>("n == 0", 
					      &Statistics::min, 
					      data0);

  allPass = allPass && testIndividualError<T>("n == 0", 
					      &Statistics::max, 
					      data0);

  allPass = allPass && testIndividualError<T>("n == 0", 
					      &Statistics::mean, 
					      data0);

  allPass = allPass && testIndividualError<T>("n == 0", 
					      &Statistics::rms, 
					      data0);

  allPass = allPass && testIndividualError<T>("n == 0", 
					      &Statistics::variance, 
					      data0);

  allPass = allPass && testIndividualError<T>("n == 0", 
					      &Statistics::skewness, 
					      data0);

  allPass = allPass && testIndividualError<T>("n == 0", 
					      &Statistics::kurtosis, 
					      data0);

  allPass = allPass && testAllStatsError<T>("n == 0", data0);

  // single element (n == 1)
  Sequence<T> data1(1,1);

  allPass = allPass && testIndividualError<T>("n == 1", 
					      &Statistics::variance, 
					      data1);

  allPass = allPass && testIndividualError<T>("n == 1", 
					      &Statistics::skewness, 
					      data1);

  allPass = allPass && testIndividualError<T>("n == 1", 
					      &Statistics::kurtosis, 
					      data1);

  allPass = allPass && testAllStatsError<T>("n == 1", data1);



  return allPass;
}
//------------------------------------------------------------------------
// test trivial data (n == 1)
//
template <class T>
bool testTrivial(void) 
{
  bool allPass = true;
  bool pass;

  Sequence<T> data(1,1);
  Statistics  statsObj;

  pass = true;
  if (statsObj.size(data) != 1) 
  {
    pass = false;
  }
  Test.Check(pass) << "size" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (statsObj.min(data) != 1) 
  {
    pass = false;
  }
  Test.Check(pass) << "min" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (statsObj.max(data) != 1) 
  {
    pass = false;
  }
  Test.Check(pass) << "max" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (statsObj.mean(data) != 1) 
  {
    pass = false;
  }
  Test.Check(pass) << "mean" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (statsObj.rms(data) != 1) 
  {
    pass = false;
  }
  Test.Check(pass) << "rms" << std::endl;
  allPass = allPass && pass;

  return allPass;
}
//------------------------------------------------------------------------
// test valid data (n > 1)
//
template <class T>
bool testValid(const std::string&      name,
	       const Sequence<T>& data, 
	       Stats&             results) 
{
  bool allPass = true;
  bool pass;

  double tolerance = 1e-5;

  Statistics statsObj;
  Stats      allstats;

  // check individual statistics
  //
  pass = true;
  if (statsObj.size(data) != results.GetSize() )  
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "size" << std::endl;
  allPass = allPass && pass;
  
  pass = true;
  if (std::abs(statsObj.min(data) - results.GetMin() )  > tolerance )
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "min" << std::endl;
  allPass = allPass && pass;
  
  pass = true;
  if (std::abs(statsObj.max(data) -  results.GetMax() )  > tolerance )
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "max" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (std::abs(statsObj.rms(data) -  results.GetRMS() )  > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "rms" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (std::abs(statsObj.mean(data) - results[0]) > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "mean" << std::endl;
  allPass = allPass && pass;

  
  pass = true;
  if (std::abs(statsObj.variance(data) - results[1]) > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "variance" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (std::abs(statsObj.skewness(data) - results[2]) > tolerance) 
  {
    pass = false;
  }
  Test.Check(pass) << name << ": " << "skewness" << std::endl;
  allPass = allPass && pass;

  pass = true;
  if (std::abs(statsObj.kurtosis(data) - results[3]) > tolerance)  
  {
    pass = false;
  }  
  Test.Check(pass) << name << ": " << "kurtosis" << std::endl;
  allPass = allPass && pass;
 
  
  // check all statistics method
  //
  pass = true;
  statsObj.all(allstats, data);

  if ((allstats.GetSize() != results.GetSize() ) || 
      (std::abs(allstats.GetMin()  - results.GetMin() )  > tolerance) || 
      (std::abs(allstats.GetMax()  - results.GetMax() )  > tolerance) || 
      (std::abs(allstats.GetRMS()  - results.GetRMS() )  > tolerance) || 
      (std::abs(allstats[0] - results[0]) > tolerance) || 
      (std::abs(allstats[1] - results[1]) > tolerance) || 
      (std::abs(allstats[2] - results[2]) > tolerance) || 
      (std::abs(allstats[3] - results[3]) > tolerance) )
  {
    pass = false;
  }

  Test.Check(pass) << name << ": " << "all stats" << std::endl;
  allPass = allPass && pass;
 
     
  return allPass;
}
//------------------------------------------------------------------------
// test different sets of valid data (float, double) (n > 1)
//
bool testAllValid()
{
  bool allPass = true;
  
  unsigned int size;
  double min;
  double max;
  double rms;
  double mean;
  double variance;
  double skewness;
  double kurtosis;

  std::valarray<double> moments(4);
 
  // valid floating point data (n == 2) -----------------------------------
  //
  float in2[] = {1.5, -2.2};
  Sequence<float> data2(in2, 2);
  
  // results from matlab
  size = 2;
  min = -2.2;
  max = 1.5;
  rms = 1.88281703837627;
  mean = -0.35;
  variance = 6.845;
  skewness = 0;
  kurtosis = 0.25*size/(size-1) - 3.0;

  moments[0] = mean;
  moments[1] = variance;
  moments[2] = skewness;
  moments[3] = kurtosis;

  Stats results2(size, min, max, rms, moments);

  allPass = allPass && testValid<float>("(n == 2, float)",
					data2,
					results2);


  // test valid floating point data (n == 5) -------------------------------
  //
  float in5[] = {0.1, 0.3, -1.1, 1.2, 1.2};
  Sequence<float> data5(in5, 5);

  // results from matlab
  size     = 5;
  min      = -1.1;
  max      = 1.2;
  rms      = 0.915423399307665;
  mean     = 0.34;
  variance = 0.903;
  skewness = -0.402700144523167*size/(size-1);
  kurtosis = 1.3237905394716*size/(size-1) - 3.0;

  moments[0] = mean;
  moments[1] = variance;
  moments[2] = skewness;
  moments[3] = kurtosis;

  Stats results5(size, min, max, rms, moments);

  allPass = allPass && testValid<float>("(n == 5, float)",
					data5,
					results5);


  
  // test valid double precision data (n == 6) -----------------------------
  //
  double in6[] = {-1.5, 0.3, 6.2, 1.1, -3.6, 4.7};
  Sequence<double> data6(in6,6);

  // results from matlab
  size     = 6;
  min      = -3.6;
  max      = 6.2;
  rms      = 3.58329457343378;
  mean     = 1.2;
  variance = 13.68;
  skewness = 0.121448663846069*size/(size-1);
  kurtosis = 1.21093562976642*size/(size-1) - 3.0;

  moments[0] = mean;
  moments[1] = variance;
  moments[2] = skewness;
  moments[3] = kurtosis;

  Stats results6(size, min, max, rms, moments);

  allPass = allPass && testValid<double>("(n == 6, double)",
					 data6,
					 results6);


  // test valid random floating point data (n == 1024) ---------------------
  //
  // read in data from a file
  float inrnd[1024];
  int i;
  i = readData<float>("tStatistics.dat",inrnd);

  if ( i != ( sizeof( inrnd ) / sizeof( *inrnd ) ) )
  {
    allPass = false;
  }

  Sequence<float> datarnd(inrnd,1024);

  // results from matlab
  size     = 1024;
  min      = -3.046144;
  max      = 3.086818;
  rms      = 0.992345034873754;
  mean     = 0.0271905978886719;
  variance = 0.984971225587534;
  skewness = -0.0529134461664719*size/(size-1);
  kurtosis = 3.12922936559784*size/(size-1) - 3.0;

  moments[0] = mean;
  moments[1] = variance;
  moments[2] = skewness;
  moments[3] = kurtosis;

  Stats resultsrnd(size, min, max, rms, moments);

  allPass = allPass && testValid<float>("(n == 1024, float, random)",
					datarnd,
					resultsrnd);
 

  return allPass;
}

//-----------------------------------------------------------------------
bool testMetaData()
{

  bool allPass = true;

  double buffer[10] = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};

  TimeSeries<double> data(Sequence<double>(buffer, 10), 1.0/10.0);
  data.SetStartTime(GPSTime(1, 0));
  data.SetName("metadata test series");

  Stats allStats;

  Statistics object;

  object.all(allStats, data);

  bool pass = true;

  pass =  (allStats.GetStartTime() == data.GetStartTime());
  Test.Check(pass) << "StartTime metadata" << std::endl;
  allPass = allPass && pass;

  pass = (allStats.GetEndTime() == data.GetEndTime());
  Test.Check(pass) << "EndTime metadata" << std::endl; 
  allPass = allPass && pass;

  pass = (allStats.GetStatisticsOfName() == data.name());
  Test.Check(pass) << "StatisticsOfName metadata" << std::endl;
  allPass = allPass && pass;

  Test.Message() << allStats.GetStatisticsOfName() << " =?= " << data.name() << std::endl;

  return allPass;

}

template<class T>
bool testComplexSeq();

//------------------------------------------------------------------------
//
int main(int ArgC, char** ArgV)
{

  Test.Init(ArgC, ArgV);

  if (Test.IsVerbose())
  { 
    std::cout << "$Id: tStatistics.cc,v 1.29 2005/12/01 22:55:03 emaros Exp $" << std::endl << std::endl;
  }


  // test all error exceptions (n == 0, n == 1)
  //

  Test.Check(testErrors<float>()) << "(all error exception tests <float>)" 
				  << std::endl;

  Test.Check(testErrors<double>()) << "(all error exception tests <double>)" 
				   << std::endl;


  // test trivial data (n == 1)
  //

  Test.Check(testTrivial<float>()) << "(n == 1, size, min, max, mean, rms test <float>)"  
				   << std::endl;

  Test.Check(testTrivial<double>()) << "(n == 1, size, min, max, mean, rms test <double>)" 
				    << std::endl;


  // test all types of valid data (n > 1) 
  //
  Test.Check(testAllValid()) << "(n > 1, valid data tests)"
			     << std::endl;


  Test.Check(testMetaData()) << "(test metadata set properly)"
                             << std::endl;


  Test.Check(testComplexSeq<float>()) << "(test exceptions on complex<float> sequences)"
			    << std::endl;

  Test.Check(testComplexSeq<double>()) << "(test exceptions on complex<double> sequences)"
			    << std::endl;


  // all done!!
  Test.Exit();

  return 0;
}

template<class T>
bool testComplexSeq()
{

  bool pass = true;
  Statistics stats;
  Stats allStats;

  const size_t n = 100;
  Sequence<complex<T> > data(n);

  for (size_t k = 0; k < n; ++k)
  {
    data[k] = complex<T>(T(k), -T(k));
  }

  Sequence<T> Re(n);
  Sequence<T> Im(n);

  for (size_t k = 0; k < n; ++k)
  {
    Re[k] = data[k].real();
    Im[k] = data[k].imag();
  }

  // size
  {
    const size_t x = stats.size(data);
    Test.Check(x == n) << "size()" << endl;
  }

  // sum
  {
    const complex<double> x = stats.sum(data);
    const complex<double> xCheck = complex<double>(stats.sum(Re), stats.sum(Im));

    Test.Check(x == xCheck) << "sum()" << endl;
  }

  // min
  try {
    stats.min(data);
    pass = false;
    Test.Check(pass) << "min() exception" << endl;
  }
  catch (const invalid_argument& e)
  {
    pass = true;
    Test.Check(pass) << "min() exception: " << e.what() << endl;
  }

  // max
  try {
    stats.max(data);
    pass = false;
    Test.Check(pass) << "max() exception" << endl;
  }
  catch (const invalid_argument& e)
  {
    pass = true;
    Test.Check(pass) << "max() exception: " << e.what() << endl;
  }

  // mean
  {
    const complex<double> x = stats.mean(data);
    const complex<double> xCheck = complex<double>(stats.mean(Re), stats.mean(Im));

    Test.Check(x == xCheck) << "mean()" << endl;
  }

  // rms
  {
    const complex<double> x = stats.rms(data);
    Test.Check(x != 0.0) << "trivial rms()" << endl;
  }

  // variance
  {
    const complex<double> x = stats.variance(data);
    Test.Check(x != 0.0) << "trivial variance()" << endl;
  }

  // kurtosis
  {
    const complex<double> x = stats.kurtosis(data);
    Test.Check(x != 0.0) << "trivial kurtosis()" << endl;
  }

  // skewness
  {
    const complex<double> x = stats.skewness(data);
    x == 0.0;
    Test.Check(pass) << "trivial skewness()" << endl;
  }

  // all
  try {
    stats.all(allStats, data);
    pass = false;
    Test.Check(pass) << "all() exception" << endl;
  }
  catch (const invalid_argument& e)
  {
    pass = true;
    Test.Check(pass) << "all() exception: " << e.what() << endl;
  }

  return pass;

}

