// Mersenne Twister test suite
// $Id: tMT19937.cc,v 1.5 2005/12/01 22:55:03 emaros Exp $
#include "datacondAPI/config.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <memory>

#include "general/unittest.h"

#include "mtstate.hh"
#include "mt19937.hh"

using namespace std;

void replacement_unexpected_handler()
{
  cout << "bad exception" << endl;
  throw bad_exception(); 
}

General::UnitTest	Test;

int main(int ArgC, char** ArgV)
{ 
  Test.Init(ArgC, ArgV);
  if (Test.IsVerbose())
  {
    cout << "$Id: tMT19937.cc,v 1.5 2005/12/01 22:55:03 emaros Exp $" << endl;
  }

  if (Test.IsVerbose())
    {
      Test.Message() << "First thousand prns, default seed:" << endl;
      datacondAPI::mtstate s;
      auto_ptr<stringstream> ss(new stringstream);
      for (int j = 0; j < 1000; j++) 
	{
	  if (j%8 == 0)
	    {
	      *ss << setw(9) 
		  << setprecision(5) 
		  << datacondAPI::mt19937(s);
	    }
	  else
	    {
	      *ss << setw(9) 
		  << setprecision(5) 
		  << datacondAPI::mt19937(s); 
	    }
	  if (j%8==7) 
	    {
	      Test.Message() << ss->str() << endl;
	      ss.reset(new stringstream);
	    }
	}
    }

  datacondAPI::mtstate s0;
  datacondAPI::mtstate s1;

  const int N(500);
  valarray<double> x0(2*N);
  valarray<double> x1(2*N);

  datacondAPI::mt19937(x1,2*N,s1);
  for (int j=0; j < 2*N; j++) 
    {
      x0[j] = datacondAPI::mt19937(s0);
    }
  x1 -= x0;
  x1 *= x1;
  Test.Check(x1.sum() == 0) << "vector == discrete" << endl << flush;
    
  valarray<double> tmp;
  datacondAPI::mt19937(tmp,N,s0);
  x0.resize(2*N);
  x0[slice(0,N,1)] = tmp;
  datacondAPI::mt19937(tmp,N,s0);
  x0[slice(N,N,1)] = tmp;
  datacondAPI::mt19937(x1,2*N,s1);
  x1 -= x0;
  x1 *= x1;
  Test.Check(x1.sum() == 0) << "split vector" << endl << flush;
  
  Test.Exit();
}
