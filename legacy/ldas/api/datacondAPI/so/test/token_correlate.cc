#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"

General::UnitTest	Test;

template<class CorrelateType, class BaseType>
void
correlate_test_template(const std::string& CorrelateTypeStr,
		  	const std::string& BaseTypeStr, 
			const BaseType Base)
{
  try {

      //-------------------------------------------------------------------
      // Calculate the expected value
      //-------------------------------------------------------------------

      //-------------------------------------------------------------------
      // Calculate value using Call Chain.
      //-------------------------------------------------------------------

      //-------------------------------------------------------------------
      // Compare the results to the exepected value.
      //-------------------------------------------------------------------
  }
  CATCH(Test);
}
int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_correlate.cc,v 1.4 2005/12/01 22:55:03 emaros Exp $"
		 << endl;

  Test.Exit();
}
