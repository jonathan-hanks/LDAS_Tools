#include "datacondAPI/config.h"

#include <iostream>
#include <sstream>

namespace datacondAPI {
  class ActionDriver;
}

#include "general/unittest.h"
#include "ActionParser.hh"
#include "DatacondCaller.h"

//-----------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------

General::UnitTest Test;

const char* parsable_algorithms[] = {
  //---------------------------------------------------------------------
  "value(spect);\n"
  "chan = slice( rawchan, 10, 6815744, 1);\n"
  "output(chan,_,_,_,channel);\n"
  "output(cav_gain,_,_,_,CAL-CAV_GAIN);\n"
  "output(resp,_,_,_,CAL-RESPONSE);\n"
  "cav_fac_cplx = float( cav_fac );\n"
  "cav_fac_cplx = complex( cav_fac_cplx );\n"
  "output(cav_fac_cplx,_,_,_,CAL-CAV_FAC);\n"
  "oloop_fac_cplx = float( oloop_fac );\n"
  "oloop_fac_cplx = complex( oloop_fac_cplx );\n"
  "output(oloop_fac_cplx,_,_,_,CAL-OLOOP_FAC);\n",
  //---------------------------------------------------------------------
  "x = value(x0);\n"
  "gwchn = double(x);\n"
  "gwchn = linfilt(fb1,fa1,gwchn);\n"
  "cavfaccplx = complex(cavfac);\n"
  "\n"
  "gwchns = slice(gwchn,16384,4915200,1);\n"
  "gwchns = float(gwchns);\n"
  "output(gwchns,_,_,GW_STRAIN_DATA:primary,GW_STRAIN_DATA);\n"
  "\n"
  "cavfac = float(h1cavfac);\n"
  "cavfaccplx = complex(cavfac);\n"
  "\n"
  "output(cavfaccplx,_,_,H1:CAL-CAV_FAC,H1 cavity factor [COMPLEX8TimeSeries]);\n"
  "\n"
  "oloop = float(h1oloop);\n"
  "oloopcplx = complex(oloop);\n"
  "\n"
  "output(oloopcplx,_,_,H1:CAL-OLOOP_FAC,H1 open loop factor [COMPLEX8TimeSeries]);\n"
  "output(h1gain,_,_,H1:CAL-CAV_GAIN,H1 reference cavity gain [COMPLEX8FrequencySeries]);\n"
  "output(h1resp,_,_,H1:CAL-RESPONSE,H1 reference response [COMPLEX8FrequencySeries]);\n"
  "\n"
  "h1cavfacf = float(h1cavfac);\n"
  "h1cavfacf = complex(h1cavfacf);\n"
  "\n"
  "h1oloopf = float(h1oloop);\n"
  "h1oloopf = complex(h1oloopf);\n"
  "\n"
  "zero = float(Zero);\n"
  "ihpt = tseries(ihp1,16384.0,754180313);\n"
  "hpf = respfilt(ihpt,h1resp,h1gain,h1cavfacf,h1oloopf);\n"
  "hpf = linfilt(fb1,fa1,hpf);\n"
  "hpfr = float(hpf);\n"
  "output(hpfr,_,_,SG130_p,SG130_p);\n"
  "output(zero,_,_,SG130_c,SG130_c);\n"
  "\n"
  "spec = psd( gwchns, 307200 );\n"
  "output(spec,_,_,GW_STRAIN_PSD,GW_STRAIN_PSD);\n"
};
//-----------------------------------------------------------------------
// Entry Point
//-----------------------------------------------------------------------

int
main( int ArgC, char** ArgV )
try
{
  Test.Init( ArgC, ArgV );

  char*		message;

  const char* alias =
    ""
    ;
  
  for ( unsigned int x = 0;
	x < ( sizeof( parsable_algorithms ) / sizeof( *parsable_algorithms ) );
	++x )
  {
    bool	pass;
    int		err = DATACOND_OK;
    err = DatacondCaller( parsable_algorithms[ x] ,
			  alias,
			  (datacond_symbol_type*)NULL /* Symbols */,
			  0 /* Symbol Count */,
			  &message );
    switch( err )
    {
    case DATACOND_PARSE_ALIASES_FAILURE:
    case DATACOND_PARSE_FAILURE:
      pass = false;
      break;
    default:
      pass = true;
      break;
    }

      Test.Check( pass )
	<< "Parsing of algorithm[" << x << "]"<< std::endl;
      if ( ( pass == false ) && ( message ) )
      {
	Test.Message( ) << " Err: " << err << " Message: " << message << std::endl;
      }
      if ( message )
      {
	free( message );
	message = (char*)NULL;
      }
  }

  Test.Exit();
}
catch( std::exception& e )
{
  Test.Check(false) << "Caught exception: " << e.what() << std::endl;
  Test.Exit();
}
catch( ... )
{
  Test.Check(false) << "Caught unknown exception" << std::endl;
  Test.Exit();
}
