#include "datacondAPI/config.h"

#include <valarray>

#include "general/unittest.h"

#include "GapUMD.hh"

using namespace datacondAPI;

// Testing
General::UnitTest	Test;

// Instantiate some special versions of GapMetaData

// Create classes for testing
template <class DataType_>
class TestGap: public GapMetaData<DataType_>
{
public:
  TestGap( const DataType_& Start, const DataType_& End,
	   double SampleRate )
    : m_start( Start ),
      m_end( End ),
      m_sample_rate( SampleRate )
  {
    m_data_size = (INT_4U)ceil( ( End - Start ) * SampleRate );
    m_array.resize( m_data_size );
  }

  virtual DataType_ GapGetDataBegin() const
  {
    return m_start;
  }
  virtual DataType_ GapGetDataEnd() const
  {
    return m_end;
  }
  virtual unsigned int GapGetDataSize() const
  {
    return m_data_size;
  }
  virtual double GetStepSize() const
  {
    return 1.0/m_sample_rate;
  }

  DataType_			m_start;
  DataType_			m_end;
  double			m_sample_rate;
  INT_4U			m_data_size;
  std::valarray<DataType_>	m_array;
};

template<class DataType_>
void
check_trans( const typename GapMetaData<DataType_>::Transitions_type& First,
	     const typename GapMetaData<DataType_>::Transitions_type& Second )
{
  typename GapMetaData<DataType_>::Transitions_type::const_iterator i_s( Second.begin() );
  typename GapMetaData<DataType_>::GapInterval_type last;

  for ( typename GapMetaData<DataType_>::Transitions_type::const_iterator
	  i_f( First.begin() );
	i_f != ( First.end() );
	i_f++ )
  {
    if ( i_f != First.begin() )
    {
      if ( (*i_f).start != last.stop )
      {
	throw std::runtime_error( "transitions are not complimentary(2)" );
      }
    }
    if ( i_s != Second.end() )
    {
      if ( (*i_f).stop != (*i_s).start )
      {
	throw std::runtime_error( "transitions are not complimentary(1)" );
      }
      last = *i_s;
      i_s++;
    }
  }
}


template<class DataType_>
void
test_data( const std::string& Leader,
	   const DataType_& Start,
	   const DataType_& End,
	   double SampleRate,
	   const DataType_& DataStart,
	   const DataType_& DataEnd,
	   const INT_4U& TransStart,
	   const INT_4U& TransEnd )
try 
{
  TestGap<DataType_>				gap( Start, End, SampleRate );
  typename GapMetaData<DataType_>::Transitions_type	trans;

  gap.GapReset( true );	// Consider it all gap

  gap.GapAppendDataRange( DataStart, DataEnd );
  gap.CalcTransitions( trans );
  if ( trans.size() != 1 )
  {
    Test.Check( false ) << Leader << ": Produced wrong number of transitions: "
			<< trans.size()
			<< std::endl;
  }
  else if ( ( trans.front().start != TransStart ) ||
	    ( trans.front().stop != TransEnd ) )
  {
    Test.Check( false ) << Leader << ": Produced wrong range: ("
			<< trans.front().start
			<< ","
			<< trans.front().stop
			<< ") instead of ("
			<< TransStart
			<< ","
			<< TransEnd
			<< ")"
			<< std::endl;
  }
  else
  {
    typename GapMetaData<DataType_>::Transitions_type	data_trans;

    gap.CalcTransitionsOfData( data_trans );
    if ( data_trans.front().start == 0 )
    {
      check_trans<DataType_>( data_trans, trans );
    }
    else
    {
      check_trans<DataType_>( trans, data_trans );
    }
    Test.Check( true ) << Leader << std::endl;
  }
}
catch( ... )
{
  Test.Check( false ) << Leader << ": Caught exception";

  try
  {
    throw;
  }
  catch( std::exception& e )
  {
    Test.Message( false ) << ": " << e.what();
  }
  catch( ... )
  {
    Test.Message( false ) << ": unknown";
  }
  Test.Message( false ) << std::endl;
}

template<class DataType_>
void
test_gap( const std::string& Leader,
	  const DataType_& Start,
	  const DataType_& End,
	  double SampleRate,
	  const DataType_& GapStart,
	  const DataType_& GapEnd,
	  const INT_4U& TransStart,
	  const INT_4U& TransEnd )
try 
{
  TestGap<DataType_>				gap( Start, End, SampleRate );
  typename GapMetaData<DataType_>::Transitions_type	trans;

  gap.GapAppendGapRange( GapStart, GapEnd );
  gap.CalcTransitions( trans );
  if ( trans.size() != 1 )
  {
    Test.Check( false ) << Leader << ": Produced wrong number of transitions: "
			<< trans.size()
			<< std::endl;
  }
  else if ( ( trans.front().start != TransStart ) ||
	    ( trans.front().stop != TransEnd ) )
  {
    Test.Check( false ) << Leader << ": Produced wrong range: ("
			<< trans.front().start
			<< ","
			<< trans.front().stop
			<< ") instead of ("
			<< TransStart
			<< ","
			<< TransEnd
			<< ")"
			<< std::endl;
  }
  else
  {
    typename GapMetaData<DataType_>::Transitions_type	data_trans;

    gap.CalcTransitionsOfData( data_trans );
    if ( data_trans.front().start == 0 )
    {
      check_trans<DataType_>( data_trans, trans );
    }
    else
    {
      check_trans<DataType_>( trans, data_trans );
    }
    Test.Check( true ) << Leader << std::endl;
  }
}
catch( ... )
{
  Test.Check( false ) << Leader << ": Caught exception";

  try
  {
    throw;
  }
  catch( std::exception& e )
  {
    Test.Message( false ) << ": " << e.what();
  }
  catch( ... )
  {
    Test.Message( false ) << ": unknown";
  }
  Test.Message( false ) << std::endl;
}

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Initialize for testing
  //---------------------------------------------------------------------
  Test.Init( ArgC, ArgV );

  //---------------------------------------------------------------------
  // Test Single Gaps by specifying gaps
  //---------------------------------------------------------------------

#if 0
  test_gap( "Integer: Gap pre start",	0, 5, 1, -1, 1, 0, 2 );
  test_gap( "Integer: Gap at start",	0, 5, 1, 0, 1, 0, 2 );
  test_gap( "Integer: Gap in middle",	0, 5, 1, 2, 3, 2, 4 );
  test_gap( "Integer: Gap at post-end",	0, 5, 1, 3, 6, 3, 5 );
#endif

  test_gap<float>( "Float: Gap at start-1",	0, 5, 2, 0, 1, 0, 3 );
  test_gap<float>( "Float: Gap at start-2",	0, 5, 2, 0, .75, 0, 3 );
  test_gap<float>( "Float: Gap at start-3",	0, 5, 2, 0, .5, 0, 2 );
  test_gap<float>( "Float: Gap in middle",	0, 5, 2, 2, 3, 4, 7 );
  test_gap<float>( "Float: Gap at end-1",	0, 5, 2, 3, 5, 6, 10 );
  test_gap<float>( "Float: Gap at end-2",	0, 5, 2, 3.5, 5, 7, 10 );
  test_gap<float>( "Float: Gap at end-3",	0, 5, 2, 3.25, 5, 6, 10 );

  //---------------------------------------------------------------------
  // Test Single Gaps by specifying data
  //---------------------------------------------------------------------

#if 0
  test_data( "Integer: Gap at start",	0, 5, 1, 2, 5, 0, 2 );
  test_data( "Integer: Gap at end",	0, 5, 1, 0, 2, 3, 5 );
#endif

  test_data<float>( "Float: Gap at start-1",	0, 5, 2, 1, 5, 0, 2 );
#if 0
  test_data<float>( "Float: Gap at start-2",	0, 5, 2, 0, .75, 0, 2 );
  test_data<float>( "Float: Gap at start-3",	0, 5, 2, 0, .5, 0, 1 );
  test_data<float>( "Float: Gap in middle",	0, 5, 2, 2, 3, 4, 6 );
#endif
  test_data<float>( "Float: Gap at end-1",	0, 5, 2, 0, 2, 5, 10 );
#if 0
  test_data<float>( "Float: Gap at end-2",	0, 5, 2, 3.5, 5, 7, 10 );
  test_data<float>( "Float: Gap at end-3",	0, 5, 2, 3.25, 5, 6, 10 );
#endif

  //---------------------------------------------------------------------
  // Exit with testing status
  //---------------------------------------------------------------------
  Test.Exit();
}

#include "GapUMD.cc"

#undef	INSTANTIATE

#define	INSTANTIATE(lm_units) \
template class TestGap<lm_units>; \
template class GapMetaData<lm_units>

#if 0
INSTANTIATE(int);
#endif
INSTANTIATE(float);
