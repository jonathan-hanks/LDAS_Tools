#include "datacondAPI/config.h"

#include <sstream>
   
#include "general/unittest.h"

#include "token.hh"

#include "UDT.hh"
#include "SequenceUDT.hh"
#include "fft.hh"

using namespace std;
   
General::UnitTest	Test;

template<class FFTType, class BaseType>
void
fft_test_template(const std::string& FFTTypeStr,
		  const std::string& BaseTypeStr, const BaseType Base)
{
  try {
    //-------------------------------------------------------------------
    // Calculate the expected value
    //-------------------------------------------------------------------
    Parameters		args;
    FFTType		data[] = { Base, Base, Base, Base };

    datacondAPI::Sequence<FFTType>	in(data, sizeof(data)/sizeof(*data));
    datacondAPI::udt*			expected((datacondAPI::udt*)NULL);
    datacondAPI::FFT			fft;

    fft.apply(expected, in);

    //-------------------------------------------------------------------
    // Calculate value using Call Chain.
    //-------------------------------------------------------------------

    CallChain		cmds;
    CallChain::Symbol*	result;
    ostringstream	base;
    ostringstream	size;

    base << Base;
    size << in.size();

    cmds.Reset();
    cmds.AppendCallFunction("integer", args.set(1, size.str().c_str()), "N");
    cmds.AppendCallFunction(BaseTypeStr, args.set(1, base.str().c_str()), "base");
    cmds.AppendCallFunction(FFTTypeStr, args.set(2, "base", "N"), "array");
    cmds.AppendCallFunction("fft", args.set(1, "array"), "result");

    cmds.AppendIntermediateResult( "result", "result", "Final Resut", "" );

    cmds.Execute();

    //-------------------------------------------------------------------
    // Compare the results to the exepected value.
    //-------------------------------------------------------------------
    result = cmds.GetSymbol("result");
    check_udt_valarray(*expected, result, "fft");

    delete expected;
  }
  CATCH(Test);
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_fft.cc,v 1.6 2005/12/01 22:55:03 emaros Exp $"
		 << endl;

  fft_test_template<double, double>("dvalarray", "double", 1.0);
  fft_test_template<double, float>("dvalarray", "double", 2.0);
  fft_test_template<float, double>("svalarray", "double", 3.0);
  fft_test_template<float, float>("svalarray", "double", 4.0);

  Test.Exit();
}
