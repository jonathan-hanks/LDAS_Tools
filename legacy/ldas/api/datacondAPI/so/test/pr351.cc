//! author="Edward Maros"
// $Id: pr351.cc,v 1.11 2005/12/01 22:55:02 emaros Exp $

/*
=========================================================================
 Number:        351 
 Category:      data_conditioning_api 
 Synopsis:      mixer unable to handle large data sources without
 	        slicing first. 
 Confidential:  no 
 Severity:      critical 
 Priority:      high 
 Responsible:   dataConditioningAPI 
 State:	        closed
 Class:         sw-bug 
 Submitter-Id:  ldas 
 Arrival-Date:  Thu Aug 03 15:49:00 PDT 2000 
 Closed-Date:   Tue Sep 19 19:33:13 PDT 2000 
 Last-Modified: Tue Sep 19 19:34:00 PDT 2000 
 Originator:    emaros@ligo.caltech.edu 
 Release:       ldas-i.j.k 
 Organization:
 Environment:

 Description:
                 The script described in the How-To-Repeat section causes an
                 ABORT. If there is a line above mix which takes a
                 slice of ch01, "x = slice(ch01, 0, 8388608, 1);", and use
                 x for ch01 in the mix command, the job completes
                 successfully.
=========================================================================
*/

#include "datacondAPI/config.h"

#include "general/unittest.h"
#include "general/util.hh"
#include "genericAPI/registry.hh"

#include "token.hh"

#include "CallChain.hh"

General::UnitTest	Test;

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */
    int main(int argc, char** argv);
#ifdef __cplusplus
}
#endif	/* __cplusplus */

bool pr351(int Length, int Index)
{
    bool retval = true;

    COMPLEX_16* c = new COMPLEX_16[Length];
    ILwd::LdasArray<COMPLEX_16> ch01(&c[0], Length);
    if (c)
    {
      delete [] c;
    }
    ch01.setNameString("ch01");
    ch01.setComment("sample data");
      
    CallChain  cmds;
    Parameters args;
  
    cmds.Reset();
    cmds.IngestILwd(ch01);
    cmds.AppendCallFunction("double", args.set(1, "0.0"), "phi");
    cmds.AppendCallFunction("double", args.set(1, "0.125"), "f");
      
    cmds.AppendCallFunction("mix", args.set(3, "phi", "f", "ch01"), "z");

    cmds.Execute();

    cmds.GetResults();

    Registry::elementRegistry.reset(); // This removes the results!

    return retval;
}

int
main(int argc, char** argv)
{
    Test.Init(argc, argv);
    size_t last = 23;
    int size = 2;
    for (size_t y = 2; y <= last; y++)
    {
	try
	{
	    size *= 2;
	    bool result = pr351(size, size/2);
	    Test.Check(result) << "Size = 2^" << y
			       << " (" << size << ")" << endl;
	}
	//:TRICKY: Explicately catch this error because it should not
	//:TRICKY:   be considered a failure.
	catch ( std::bad_alloc& e )
	{
	  Test.Check(true) << "Size = 2^" << y
			   << " (" << size << ")"
			   << " EXCEPTION: " << e.what()
			   << endl;
	}
	// Catch all other exceptions and report them as failures.
	CATCH(Test);
    }
    Test.Exit();
    //---------------------------------------------------------------------
    // Should never get here, but if we do, consider it a failure.
    //---------------------------------------------------------------------
    return 1;
}
