#include "datacondAPI/config.h"

#include <sstream>
   
#include "general/unittest.h"

#include "token.hh"

#include "UDT.hh"
#include "TimeSeries.hh"
#include "WelchCSDEstimate.hh"
#include "WelchSpectrumUDT.hh"
#include "TypeInfo.hh"

using namespace std;   
using General::GPSTime;

General::UnitTest	Test;

template<class PSDType, class BaseType>
void
psd_test_template(const std::string& PSDTypeStr,
		  const std::string& BaseTypeStr, const BaseType Base)
{
  using namespace datacondAPI;

  try {
    //-------------------------------------------------------------------
    // Calculate the expected value
    //-------------------------------------------------------------------
    Parameters args;
    size_t     n = 32;
    double     fs = 2048.0; // Sampling rate
    GPSTime	start_time(100000, 0);
    std::string name("H2:LSC-AS_Q");
    size_t     fftLen = 8;
    size_t     overlap = 4;
    CallChain		cmds;

    ILwd::LdasElement*	ilwd;

    datacondAPI::CSDEstimate::DetrendMethod detrendMethod
	= datacondAPI::CSDEstimate::none;

    datacondAPI::TimeSeries<PSDType> in;
    in.resize(n, Base);
    in.SetName(name);
    in.SetSampleRate(fs);
    in.SetStartTime(start_time);
    in.SetBaseFrequency(100.0);

    datacondAPI::udt*              expected = 0;

    datacondAPI::WelchCSDEstimate welch;

    welch.set_fftLength(fftLen);
    welch.set_overlapLength(overlap);
    welch.set_detrendMethod(detrendMethod);
   
    welch.apply(expected, in);

    WelchSpectrum<PSDType>& exp
	= dynamic_cast<WelchSpectrum<PSDType>&>(*expected);

    Test.Message(false) << "Expected data in metadata format" << endl;

    ilwd = exp.ConvertToIlwd(cmds, udt::TARGET_METADATA);
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
						    Test.Message(),
						    ILwd::ASCII);
    Test.Message(false) << endl;

    cmds.Reset();

    delete ilwd;

    Test.Message(false) << "Expected data in wrapper format" << endl;

    ilwd = exp.ConvertToIlwd(cmds, udt::TARGET_WRAPPER);
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
						    Test.Message(),
						    ILwd::ASCII);
    Test.Message(false) << endl;

    cmds.Reset();

    delete ilwd;

    //-------------------------------------------------------------------
    // Calculate value using Call Chain.
    //-------------------------------------------------------------------

    CallChain::Symbol*	result;
    ostringstream	base;
    ostringstream	size;
    ostringstream	fftlen;
    ostringstream	overlaplen;
    ostringstream	dflag;

    // Format for final result
    const string format = "frame";

    base << Base;
    size << in.size();
    fftlen << fftLen;
    overlaplen << overlap;
    dflag << detrendMethod;

    cmds.Reset();

    cmds.SetReturnTarget("wrapper");

    cmds.AppendCallFunction("integer", args.set(1, size.str().c_str()), "N");
    cmds.AppendCallFunction(BaseTypeStr, args.set(1, base.str().c_str()), "base");
    cmds.AppendCallFunction("integer", args.set(1, fftlen.str().c_str()), "fftLen");
    cmds.AppendCallFunction("integer", args.set(1, overlaplen.str().c_str()),
			    "overlap");
    cmds.AppendCallFunction("integer", args.set(1, dflag.str().c_str()),
                            "detrendMethod");
    cmds.AppendCallFunction(BaseTypeStr, args.set(1, base.str().c_str()), "base");
    cmds.AppendCallFunction(PSDTypeStr, args.set(2, "base", "N"), "array");

    cmds.AppendCallFunction("psd",
			    args.set(5, "array", "fftLen", "",
				     "overlap", "detrendMethod"),
			    "result");

    cmds.AppendIntermediateResult( "result", "result", "Final Result",
                                   format.c_str() );

    cmds.Execute();

    //-------------------------------------------------------------------
    // Compare the results to the exepected value.
    //-------------------------------------------------------------------
    result = cmds.GetSymbol("result");

    Test.Message() << "Result in wrapper format" << endl;

    ilwd = result->ConvertToIlwd(cmds, datacondAPI::udt::TARGET_WRAPPER);
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
						    Test.Message(),
						    ILwd::ASCII);
    Test.Message(false) << endl;
    delete ilwd;

    Test.Message() << "Result in metadata format" << endl;

    ilwd = result->ConvertToIlwd(cmds, datacondAPI::udt::TARGET_METADATA);
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
						    Test.Message(),
						    ILwd::ASCII);
    Test.Message(false) << endl;
    delete ilwd;

    Test.Message() << "Result in " << format << " format" << endl;

    ilwd = cmds.GetResults();
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
						    Test.Message(),
						    ILwd::ASCII);
    Test.Message(false) << endl;

    //    delete ilwd;

    Test.Message(false) << endl;
  }
  CATCH(Test);
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_psd_tbl.cc,v 1.18 2005/12/01 22:55:03 emaros Exp $"
		 << endl << flush;

  psd_test_template<double, double>("dvalarray", "double", 1.0);
  psd_test_template<double, float>("dvalarray", "double", 2.0);
  psd_test_template<float, double>("svalarray", "double", 3.0);
  psd_test_template<float, float>("svalarray", "double", 4.0);

  Test.Exit();
}
