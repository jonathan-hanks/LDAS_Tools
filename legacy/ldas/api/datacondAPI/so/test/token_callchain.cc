#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"

#include "CallChain.hh"

General::UnitTest	Test;

using namespace datacondAPI;

static void create_delete( )
{
  std::string	test( "create_delete" );
  try {
    CallChain*	cc = new CallChain();

    delete cc;
    Test.Check( true ) << test << std::endl;
  }
  catch( ... )
  {
    Test.Check( false ) << test << ": Caught exception: ";
    try
    {
      throw;
    }
    catch( const std::exception& e )
    {
      Test.Message( false ) << e.what();
    }
    catch( ... )
    {
      Test.Message( false ) << "unknown exception";
    }
    Test.Message( false ) << std::endl;
  }
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_callchain.cc,v 1.4 2005/12/01 22:55:03 emaros Exp $"
		 << std::endl;

  create_delete();
  
  Test.Exit();
}
