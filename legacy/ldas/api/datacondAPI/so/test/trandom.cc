#include "datacondAPI/config.h"

#include <cmath>
#include <string>
#include <valarray>
#include <iostream>   

#include "general/unittest.h"

#include "random.hh"


using namespace std;      
using namespace datacondAPI;

General::UnitTest Test;

int ok=-248673140;
ran0state ok0;
ran1state ok1(ok);
ran2state ok2;
ran3state ok3;
ran4state ok4;
gasdevstate okg;


//Computes the chi-squared statistics for the data in the data valarray given
//the corresponding probabilities in probs, the number of bins in k, the number
//of trials in n.  The output in percent give the percentage points for
//0=.01; 1=.05; 2=.25; 3=.5; 4=.75; 5=.95; 6=.99 and the result is held in V
void chi2(valarray<double>& data, valarray<double>& probs, int k,int n, valarray<double>& percent, double& V)
{
	V=0;
	for(int i=0;i<k;i++)
		V+=data[i]*data[i]/probs[i];
	V/=double(n);
	V-=double(n);
	percent.resize(7);
	valarray<double> xp(7);
	xp[0]= -2.33;
	xp[1]= -1.64;
        xp[2]= -.675;
        xp[3]= 0.00;
        xp[4]= .675;
        xp[5]= 1.64;
        xp[6]= 2.33;
	for( unsigned int i=0;i<percent.size();i++)
		percent[i]=double(k-1)+sqrt(2.*(k-1))*xp[i]+(2./3.*xp[i]*xp[i])-(2./3.);
}

//Computes and returns the factorial of number
double factorial(int number)
{
	double product = 1;

	for ( ; number > 0 ; number--)
		product *= number;

	return product;
}
   
//Computes and returns the sterling numbers for m and n
double sterling(int m, int n)
{
        double sum=0;
        double tmp_k( 0 );
	for(int k=0;k<=n;k++)
	{
           tmp_k = k;
           sum+=std::pow(tmp_k,m)*std::pow(-1.0,k)*factorial(n)/(factorial(n-k)*factorial(k));
	}
	sum/=pow(-1.0,n)*factorial(n);
	return sum;
}

//Tests for randomness by checking to ensure that sequences of random
//numbers are equidistributed.  It does this by multiplying floats of
//numbers between 0 and 1 by 100 then truncating the result (from here called "int(100)")
//and applying chi-squared tests to the total quantity of each integer.
void FreqTest()
{
        int n=500000,d=100;
	valarray<int> rand0(n),rand1(n),rand2(n),rand3(n),rand4(n);
	valarray<double> freq(0.,d),probs(1/double(d),d);
	
	for(int i=0;i<n;i++)
	  {
	    rand0[i]=(int)( d*ran0(ok0) );
	    rand1[i]=(int)( d*ran1(ok1) );
	    rand2[i]=(int)( d*ran2(ok2) );
	    rand3[i]=(int)( d*ran3(ok3) );
	    rand4[i]=(int)( d*ran4(ok4) );
	  }
	
	for(int i=0;i<n;i++)
	    freq[rand0[i]]++;

	double V=0;
	valarray<double> results(d);
	string what = "Frequency Test for ran0";
	chi2(freq,probs,d,n,results,V);
	if(V<results[5])
	  Test.Check(true) << what << endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Results per degree of freedom: " << V/(d-1) << endl;

	freq=0;
	for(int i=0;i<n;i++)
	    freq[rand1[i]]++;

	what = "Frequency Test for ran1";
	chi2(freq,probs,d,n,results,V);
	if(V<results[5])
	  Test.Check(true) << what << endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Results per degree of freedom: " << V/(d-1) << endl;

	freq=0;
	for(int i=0;i<n;i++)
	    freq[rand2[i]]++;

	what = "Frequency Test for ran2";
	chi2(freq,probs,d,n,results,V);
	if(V<results[5])
	  Test.Check(true) << what << endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Results per degree of freedom: " << V/(d-1) << endl;

	freq=0;
	for(int i=0;i<n;i++)
	    freq[rand3[i]]++;

	what = "Frequency Test for ran3";
	chi2(freq,probs,d,n,results,V);
	if(V<results[5])
	  Test.Check(true) << what << endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Results per degree of freedom: " << V/(d-1) << endl;

	freq=0;
	for(int i=0;i<n;i++)
	    freq[rand4[i]]++;

	what = "Frequency Test for ran4";
	chi2(freq,probs,d,n,results,V);
	if(V<results[5])
	  Test.Check(true) << what << endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Results per degree of freedom: " << V/(d-1) << endl << endl;
	
}//FreqTest

//The serial test groups random numbers into int(50) then checks
//ordered pairs of adjacent numbers are equally distributed over all
//possible ordered pairs by using the chi-squared test.
void SerialTest()
{
	int d=50, n2=50000,n=25000;
	valarray<int> rand0(n2), rand1(n2), rand2(n2), rand3(n2), rand4(n2); 
	valarray<double> results0(0.,d*d),results1(0.,d*d),results2(0.,d*d),results3(0.,d*d),results4(0.,d*d),probs(d*d);
	for(int i=0; i<n2;i++)
	{
		rand0[i]=int(d*ran0(ok0));
		rand1[i]=int(d*ran1(ok1));
		rand2[i]=int(d*ran2(ok2));
		rand3[i]=int(d*ran3(ok3));
		rand4[i]=int(d*ran4(ok4));
	}
	for(int q=0; q<d;q++)
	for(int r=0; r<d;r++)
	{
		for(int j=0;j<n;j++)
		{
			if(rand0[j]==q&&rand0[j+1]==r)
				results0[q*d+r]++;
			if(rand1[j]==q&&rand1[j+1]==r)
                                results1[q*d+r]++;
			if(rand2[j]==q&&rand2[j+1]==r)
                                results2[q*d+r]++;
			if(rand3[j]==q&&rand3[j+1]==r)
                                results3[q*d+r]++;
			if(rand4[j]==q&&rand4[j+1]==r)
                                results4[q*d+r]++;
		}
	}
	for(unsigned int i=0;i<probs.size();i++)
	  probs[i]=1/(double(d)*d);
	valarray<double> probs1(d);
	double V=0;
	string what = "Serial Test for ran0";
	chi2(results0,probs,d*d,n,probs1,V);
	if(V<probs1[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/(d*d-1) << endl;

	what = "Serial Test for ran1";
        chi2(results1,probs,d*d,n,probs1,V);
        if(V<probs1[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(d*d-1) << endl;

	what = "Serial Test for ran2";
        chi2(results2,probs,d*d,n,probs1,V);
        if(V<probs1[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(d*d-1) << endl;

	what = "Serial Test for ran3";
        chi2(results3,probs,d*d,n,probs1,V);
        if(V<probs1[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(d*d-1) << endl;

	what = "Serial Test for ran4";
        chi2(results4,probs,d*d,n,probs1,V);
        if(V<probs1[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(d*d-1) << endl << endl;
}

//The gap test counts the length of sub-sequences between the values of 0 and .5.
//Using probabilities defined in The Art of Computer Programming, a chi-squared test
//then applied to the number of gaps of each length. 
void GapTest()
{
	int t=10,n=500000, N=60000,j,s;
	double b=0.5,V;
	valarray<double> count(0.,t+1),probs(t+1), percents(t);
	valarray<double> rand0(n), rand1(n), rand2(n), rand3(n), rand4(n);
	for(int i=0;i<n;i++)
	{
	  rand0[i]=ran0(ok0);
	  rand1[i]=ran1(ok1);	 
	  rand2[i]=ran2(ok2);
	  rand3[i]=ran3(ok3);
	  rand4[i]=ran4(ok4);
	}

	string what = "Gap Test for ran0";
	{
	j=-1,s=0;
	int r=0;
	do{
		j++;
		if(rand0[j]<b)
		{
			if(r>t)
				count[t]++;
			else
				count[r]++;
			s++;
			r=0;
		}
		else
			r++;
	}while(s<N);
	probs[0]=b;
	probs[1]=b*b;
	probs[2]=b*b*b;
	probs[3]=b*b*b*b;
	probs[4]=b*b*b*b*b;
	probs[5]=b*b*b*b*b*b;
	probs[6]=probs[5]*b;
	probs[7]=probs[6]*b;
	probs[8]=probs[7]*b;
	probs[9]=probs[8]*b;
	probs[10]=probs[9];
		
	chi2(count,probs,t+1,N,percents,V);
	if(V<percents[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() <<"Result per degree of freedom: " << V/t << endl;
	}

	count =0;
	{
        j=-1,s=0;
        int r=0;
        do{
                j++;
                if(rand1[j]<b)
                {
                        if(r>t)
                                count[t]++;
                        else
                                count[r]++;
                        s++;
                        r=0;
                }
                else
                        r++;
        }while(s<N);
	what = "Gap Test for ran1";
	chi2(count,probs,t+1,N,percents,V);
        if(V<percents[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/t << endl;
        }

        count =0;
	{
        j=-1,s=0;
        int r=0;
        do{
                j++;
                if(rand2[j]<b)
                {
                        if(r>t)
                                count[t]++;
                        else
                                count[r]++;
                        s++;
                        r=0;
                }
                else
                        r++;
        }while(s<N);
	what = "Gap Test for ran2";
        chi2(count,probs,t+1,N,percents,V);
        if(V<percents[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/t << endl;
        }

        count =0;
	{
        j=-1,s=0;
        int r=0;
        do{
                j++;
                if(rand3[j]<b)
                {
                        if(r>t)
                                count[t]++;
                        else
                                count[r]++;
                        s++;
                        r=0;
                }
                else
                        r++;
        }while(s<N);
	what = "Gap Test for ran3";
        chi2(count,probs,t+1,N,percents,V);
        if(V<percents[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/t << endl;
        }

        count =0;
	{
        j=-1,s=0;
        int r=0;
        do{
                j++;
                if(rand4[j]<b)
                {
                        if(r>t)
                                count[t]++;
                        else
                                count[r]++;
                        s++;
                        r=0;
                }
                else
                        r++;
        }while(s<N);
        what = "Gap Test for ran4";
        chi2(count,probs,t+1,N,percents,V);
        if(V<percents[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/t << endl<< endl;
	}
}

//The poker test uses numbers of int(10) in sub-sequences of length 5.
//The number of different integers is found in each sub-sequence.  A
//chi-squared test is then applied to the totals of each different numbers.  
void PokerTest()
{
  int n=100000,d=10,k=5;
	valarray<int> rand0(n),rand1(n),rand2(n),rand3(n),rand4(n);
	valarray<double> res0(0.,k),res1(0.,k),res2(0.,k),res3(0.,k),res4(0.,k);
	valarray<int> temp(0,d);
	for(int i=0;i<n;i++)
	{
		rand0[i]=int(d*ran0(ok0));
		rand1[i]=int(d*ran1(ok1));
		rand2[i]=int(d*ran2(ok2));
		rand3[i]=int(d*ran3(ok3));
		rand4[i]=int(d*ran4(ok4));
	}
	for(int i=0;i<n/5;i++)
	{
		temp=0;
		for(int j=0;j<5;j++)
		{
			temp[rand0[5*i+j]]++;
		}
		int tot=0;
		for(int j=0;j<d;j++)
		{
			if(temp[j]!=0)
				tot++;
		}
		if(tot==1)
		  tot=2;
		res0[tot-2]++;
		
		temp=0;
                for(int j=0;j<5;j++)
                {
                        temp[rand1[5*i+j]]++;
                }
                tot=0;
                for(int j=0;j<d;j++)
                {
                        if(temp[j]!=0)
                                tot++;
                }
		if(tot==1)
		  tot=2;
                res1[tot-2]++;
		
		temp=0;
                for(int j=0;j<5;j++)
                {
                        temp[rand2[5*i+j]]++;
                }
                tot=0;
                for(int j=0;j<d;j++)
                {
                        if(temp[j]!=0)
                                tot++;
                }
		if(tot==1)
		  tot=2;
                res2[tot-2]++;

		temp=0;
                for(int j=0;j<5;j++)
                {
                        temp[rand3[5*i+j]]++;
                }
                tot=0;
                for(int j=0;j<d;j++)
                {
                        if(temp[j]!=0)
                                tot++;
                }
		if(tot==1)
		  tot=2;
                res3[tot-2]++;

		temp=0;
                for(int j=0;j<5;j++)
                {
                        temp[rand4[5*i+j]]++;
                }
                tot=0;
                for(int j=0;j<d;j++)
                {
                        if(temp[j]!=0)
                                tot++;
                }
		if(tot==1)
		  tot=2;
                res4[tot-2]++;
	}
	valarray<double> probs(4);
        const double tmp_d( d );
	for(int r=2;r<=5;r++)
	{
		probs[r-2]=sterling(k,r)*factorial(d)/(pow(tmp_d,k)*factorial(d-r));
	}
	probs[0]+=sterling(k,1)*factorial(d)/(pow(tmp_d,k)*factorial(d-1));

	valarray<double> results(d);
	double V=0;

	string what = "Poker Test for ran0";
	chi2(res0,probs,k,n/5,results,V);

 	if(V<results[5]&&V>results[1])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/(k-1) << endl;

	what = "Poker Test for ran1";
        chi2(res1,probs,k,n/5,results,V);

        if(V<results[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(k-1) << endl;

	what = "Poker Test for ran2";
        chi2(res2,probs,k,n/5,results,V);

        if(V<results[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(k-1) << endl;

	what = "Poker Test for ran3";
        chi2(res3,probs,k,n/5,results,V);

        if(V<results[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(k-1) << endl;

	what = "Poker Test for ran4";
        chi2(res4,probs,k,n/5,results,V);

        if(V<results[5])
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Result per degree of freedom: " << V/(k-1) << endl << endl;
}

//The coupon collector's test is one with uses int(22) and counts how long sub-sequences
//which contain the complete sets of numbers (0 to 21).  The probabilities for each
//length we calculated using formula from The Art of Computer Programming.
void CouponTest()
{
	int d=22,n=1200,t=133,N=100000;
	int j=0,s=0,q=0,r=0;
	valarray<double> count(0.,t-d+1);
	valarray<int> rand0(N), rand1(N), rand2(N), rand3(N), rand4(N),occurs(0,d);
	for(int i=0;i<N;i++)
	{
	  rand0[i]=(int)( d*ran0(ok0) );
	  rand1[i]=(int)( d*ran1(ok1) );
	  rand2[i]=(int)( d*ran2(ok2) );
	  rand3[i]=(int)( d*ran3(ok3) );
	  rand4[i]=(int)( d*ran4(ok4) );
	}
	string what = "Coupon Test for ran0";
	do{
		q=0;
		r=1;
		occurs=0;
		while(q<d)
		{
		  int temp;
		  while(occurs[temp=rand0[j]]!=0)
			{
				r++;
				j++;
			}

			occurs[temp]=1;
			q++;
		}
		if(r>=t)
		  count[t-d]++;
		else
		  count[r-d]++;
		s++;
	}while(s<n);

	valarray<double> probs(t-d+1);
	for(unsigned int i=0;i<probs.size()-1;i++)
	{
		probs[i]=sterling(i+d-1,d-1)*factorial(d)/pow(double(d),(int)(i+d));
	}
	probs[t-d]=1-sterling(t-1,d)*factorial(d)/pow(double(d),t-1);

	valarray<double> results(d);
	double V=0;
	chi2(count,probs,t-d+1,n,results,V);
	if(V<results[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() <<"Result per degree of freedom: " << V/(t-d) << endl;

	s=0;
	j=0;
	count=0;
	what = "Coupon Test for ran1";
	do{
		q=0;
		r=1;
		occurs=0;
		while(q<d)
		{
		  int temp;
		  while(occurs[temp=rand1[j]]!=0)
			{
				r++;
				j++;
			}

			occurs[temp]=1;
			q++;
		}
		if(r>=t)
			count[t-d]++;
		else
			count[r-d]++;
		s++;
	}while(s<n);

	chi2(count,probs,t-d+1,n,results,V);
	if(V<results[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() <<"Result per degree of freedom: " << V/(t-d) << endl;

	s=0;
	j=0;
	count=0;
	what = "Coupon Test for ran2";
	do{
		q=0;
		r=1;
		occurs=0;
		while(q<d)
		{
		  int temp;
		  while(occurs[temp=rand2[j]]!=0)
			{
				r++;
				j++;
			}

			occurs[temp]=1;
			q++;
		}
		if(r>=t)
			count[t-d]++;
		else
			count[r-d]++;
		s++;
	}while(s<n);

	chi2(count,probs,t-d+1,n,results,V);
	if(V<results[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() <<"Result per degree of freedom: " << V/(t-d) << endl;

	s=0;
	j=0;
	count=0;
	what = "Coupon Test for ran3";
	do{
		q=0;
		r=1;
		occurs=0;
		while(q<d)
		{
		  int temp;
		  while(occurs[temp=rand3[j]]!=0)
			{
				r++;
				j++;
			}

			occurs[temp]=1;
			q++;
		}
		if(r>=t)
			count[t-d]++;
		else
			count[r-d]++;
		s++;
	}while(s<n);

	chi2(count,probs,t-d+1,n,results,V);
	if(V<results[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() <<"Result per degree of freedom: " << V/(t-d) << endl;
	
	s=0;
	j=0;
	count=0;
	what = "Coupon Test for ran4";
	do{
		q=0;
		r=1;
		occurs=0;
		while(q<d)
		{
		  int temp;
		  while(occurs[temp=rand4[j]]!=0)
			{
				r++;
				j++;
			}

			occurs[temp]=1;
			q++;
		}
		if(r>=t)
			count[t-d]++;
		else
			count[r-d]++;
		s++;
	}while(s<n);

	chi2(count,probs,t-d+1,n,results,V);
	if(V<results[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() <<"Result per degree of freedom: " << V/(t-d) << endl<<endl;
}

//The permutation test checks the number of permutations (U1<U2<U3 or U1<U2>U3 etc.)
//in a fixed length subsequece over the whole sequence.  Each permutation is equally
//likely. 
void PermTest()
{
	int N=201264,t=7,r=t,f=0,s=0;
        const unsigned int perms_size( static_cast<const unsigned int>(factorial(t) ) );
	valarray<double> perms(0.,perms_size);
	valarray<double> rand0(N), rand1(N), rand2(N), rand3(N), rand4(N);
	double temp=0;
	for(int i=0; i<N;i++)
	{
		rand0[i]=ran0(ok0);
		rand1[i]=ran1(ok1);
		rand2[i]=ran2(ok2);
		rand3[i]=ran3(ok3);
		rand4[i]=ran4(ok4);
	}
	valarray<double> small(t);
	const valarray<double>&
	  const_rand0( static_cast< const valarray<double>& >( rand0 ) ),
	  const_rand1( static_cast< const valarray<double>& >( rand1 ) ),
	  const_rand2( static_cast< const valarray<double>& >( rand2 ) ),
	  const_rand3( static_cast< const valarray<double>& >( rand3 ) ),
	  const_rand4( static_cast< const valarray<double>& >( rand4 ) );
	for(int j=0;j<N/t;j++)
	{
	  small=const_rand0[slice (j*t,t,1)];
		r=t-1;
		f=0;
		s=0;
		while(r>0)
		{
		  s=0;
			for(int i=1;i<=r;i++)
			{
				if(small[s]<small[i])
					s=i;
			}
			temp=small[r];
			small[r]=small[s];
			small[s]=temp;
			f=(r+1)*f+s;
			r--;
		}
		perms[f]++;
	}
	string what = "Permutation Test for ran0";
	double V=0;
	valarray<double> probs(1/factorial(t),static_cast<const unsigned int>(factorial(t)));
	valarray<double> outputs(t);
	chi2(perms,probs,int(factorial(t)),N/t,outputs,V);
	if(V<outputs[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/(factorial(t)-1) << endl;

	perms=0;
	for(int j=0;j<N/t;j++)
	{
	  small=const_rand1[slice (j*t,t,1)];
		r=t-1;
		f=0;
		s=0;
		while(r>0)
		{
		  s=0;
			for(int i=1;i<=r;i++)
			{
				if(small[s]<small[i])
					s=i;
			}
			temp=small[r];
			small[r]=small[s];
			small[s]=temp;
			f=(r+1)*f+s;
			r--;
		}
		perms[f]++;
	}
	what = "Permutation Test for ran1";
	chi2(perms,probs,int(factorial(t)),N/t,outputs,V);
	if(V<outputs[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/(factorial(t)-1) << endl;

	perms=0;
	for(int j=0;j<N/t;j++)
	{
	  small=const_rand2[slice (j*t,t,1)];
		r=t-1;
		f=0;
		s=0;
		while(r>0)
		{
		  s=0;
			for(int i=1;i<=r;i++)
			{
				if(small[s]<small[i])
					s=i;
			}
			temp=small[r];
			small[r]=small[s];
			small[s]=temp;
			f=(r+1)*f+s;
			r--;
		}
		perms[f]++;
	}
	what = "Permutation Test for ran2";
	chi2(perms,probs,int(factorial(t)),N/t,outputs,V);
	if(V<outputs[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/(factorial(t)-1) << endl;

	perms=0;
	for(int j=0;j<N/t;j++)
	{
	  small=const_rand3[slice (j*t,t,1)];
		r=t-1;
		f=0;
		s=0;
		while(r>0)
		{
		  s=0;
			for(int i=1;i<=r;i++)
			{
				if(small[s]<small[i])
					s=i;
			}
			temp=small[r];
			small[r]=small[s];
			small[s]=temp;
			f=(r+1)*f+s;
			r--;
		}
		perms[f]++;
	}
	what = "Permutation Test for ran3";
	chi2(perms,probs,int(factorial(t)),N/t,outputs,V);
	if(V<outputs[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/(factorial(t)-1) << endl;

	perms=0;
	for(int j=0;j<N/t;j++)
	{
	  small=const_rand4[slice (j*t,t,1)];
		r=t-1;
		f=0;
		s=0;
		while(r>0)
		{
		  s=0;
			for(int i=1;i<=r;i++)
			{
				if(small[s]<small[i])
					s=i;
			}
			temp=small[r];
			small[r]=small[s];
			small[s]=temp;
			f=(r+1)*f+s;
			r--;
		}
		perms[f]++;
	}
	what = "Permutation Test for ran4";
	chi2(perms,probs,int(factorial(t)),N/t,outputs,V);
	if(V<outputs[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/(factorial(t)-1) << endl<<endl;

}

//The run test is an int(50) test which checks the lengths of consecutive
//"runs" in which the previous number was smaller than the current number.
//In order to ensure the lengths of such runs are independent, the value
//right after the run is ignored.  The probability of the lengths of runs
//is give in The Art of Computer Programming.
void RunTest()
{
	int i=0,r=1,N=1200000,R=6,n=0,d=50;
	valarray<double> lengths(0.,R);
	valarray<int> rand0(N), rand1(N), rand2(N), rand3(N), rand4(N);
	
	for(int k=0;k<N;k++)
	{
		rand0[k]=(int)( d*ran0(ok0) );
		rand1[k]=(int)( d*ran1(ok1) );
		rand2[k]=(int)( d*ran2(ok2) );
		rand3[k]=(int)( d*ran3(ok3) );
		rand4[k]=(int)( d*ran4(ok4) );
	}

	n=0;
	while(i<N&&n<10000)
	{
		if(rand0[i]<rand0[i+1])
		{
			r++;
			i++;
		}
		else if(rand0[i]>rand0[i+1])
		{
			i+=2;
			n++;
			if(r>R)
				lengths[R-1]++;
			else
				lengths[r-1]++;
			r=1;
		}
		else
		  i+=2;
	}

	valarray<double> probs(R);
	for(int k=1;k<=R;k++)
	{
		probs[k-1]=1/factorial(k) - 1/factorial(k+1);
	}

	probs[R-1]+=.0002;
	double V;
	valarray<double> outputs(R);
	chi2(lengths,probs,R,n,outputs,V);

	string what = "Run Test for ran0";
	if(V<outputs[5]&&V>outputs[1])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() <<"Result per degree of freedom is: "  <<V/(R-1)<<endl;
	
		n=0;
		i=0;
		lengths=0;
		r=1;
	while(i<N&&n<10000)
	{
		if(rand1[i]<rand1[i+1])
		{
			r++;
			i++;
		}
		else if(rand1[i]>rand1[i+1])
		{
			i+=2;
			n++;
			if(r>R)
				lengths[R-1]++;
			else
				lengths[r-1]++;
			r=1;
		}
		else
		  i+=2;
	}

	chi2(lengths,probs,R,n,outputs,V);

	what = "Run Test for ran1";

	if(V<outputs[5]&&V>outputs[1])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom is: "  <<V/(R-1)<<endl;
	
	n=0;
	i=0;
	lengths=0;
	r=1;
	while(i<N&&n<10000)
	{
		if(rand2[i]<rand2[i+1])
		{
			r++;
			i++;
		}
		else if(rand2[i]>rand2[i+1])
		{
			i+=2;
			n++;
			if(r>R)
				lengths[R-1]++;
			else
				lengths[r-1]++;
			r=1;
		}
		else
		  i+=2;
	}

	chi2(lengths,probs,R,n,outputs,V);

	what = "Run Test for ran2";

	if(V<outputs[5]&&V>outputs[1])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom is: "  <<V/(R-1)<<endl;

			n=0;
		i=0;
		lengths=0;
		r=1;
	while(i<N&&n<10000)
	{
		if(rand3[i]<rand3[i+1])
		{
			r++;
			i++;
		}
		else if(rand3[i]>rand3[i+1])
		{
			i+=2;
			n++;
			if(r>R)
				lengths[R-1]++;
			else
				lengths[r-1]++;
			r=1;
		}
		else
		  i+=2;
	}

	chi2(lengths,probs,R,n,outputs,V);

	what = "Run Test for ran3";

	if(V<outputs[5]&&V>outputs[1])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom is: "  <<V/(R-1)<<endl;

			n=0;
		i=0;
		lengths=0;
		r=1;
	while(i<N&&n<10000)
	{
		if(rand4[i]<rand4[i+1])
		{
			r++;
			i++;
		}
		else if(rand4[i]>rand4[i+1])
		{
			i+=2;
			n++;
			if(r>R)
				lengths[R-1]++;
			else
				lengths[r-1]++;
			r=1;
		}
		else
		  i+=2;
	}

	chi2(lengths,probs,R,n,outputs,V);

	what = "Run Test for ran4";

	if(V<outputs[5]&&V>outputs[1])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom is: "  <<V/(R-1)<<endl<<endl;
}

//The maximum-of-T test takes the maximum values of T length (in this case T=5)
//sub-sequences to form a new sequence.  When the values of the new sequence are
//taken to the Tth power the resulting sequence should be equidistributed.
void Max_T_Test()
{
	int t=5,N=1000000;
	int max=0,d=100;
	valarray<double> rand0(N), rand1(N), rand2(N), rand3(N), rand4(N),maxT(N/t);
	for(int i=0;i<N;i++)
	{
		rand0[i]=ran0(ok0);
		rand1[i]=ran1(ok1);
		rand2[i]=ran2(ok2);
		rand3[i]=ran3(ok3);
		rand4[i]=ran4(ok4);
	}
	
	valarray<double> count(0.,d);
	for(int j=0;j<N/t;j++)
	{
		for(int k=0;k<t;k++)
		{
			if(rand0[t*j+max]<rand0[t*j+k])
				max=k;
		}
		maxT[j]=rand0[t*j+max];
	}

	for(unsigned int i=0;i<maxT.size();i++)
	  {
	    maxT[i]=pow(maxT[i],t);
	    maxT[i]=int(d*maxT[i]);
	    count[static_cast<const unsigned int>(maxT[i])]++;
	  }

	valarray<double> probs(1/double(d),d),result(t);
	double V=0;
	chi2(count,probs,d,N/t,result,V);
	string what = "Maximum-of-5 test for ran0";
	if(V<result[5])
	  Test.Check(true)<<what<<endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/double(d-1) << endl;

	count=0;
	for(int j=0;j<N/t;j++)
	{
		for(int k=0;k<t;k++)
		{
			if(rand1[t*j+max]<rand1[t*j+k])
				max=k;
		}
		maxT[j]=rand1[t*j+max];
	}

	for(unsigned int i=0;i<maxT.size();i++)
	  {
	    maxT[i]=pow(maxT[i],t);
	    maxT[i]=int(d*maxT[i]);
	    count[static_cast<const unsigned int>(maxT[i])]++;
	  }

	chi2(count,probs,d,N/t,result,V);
	what = "Maximum-of-5 test for ran1";
	if(V<result[5])
	  Test.Check(true)<<what<<endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/double(d-1) << endl;

	count=0;
	for(int j=0;j<N/t;j++)
	{
		for(int k=0;k<t;k++)
		{
			if(rand2[t*j+max]<rand2[t*j+k])
				max=k;
		}
		maxT[j]=rand2[t*j+max];
	}

	for(unsigned int i=0;i<maxT.size();i++)
	  {
	    maxT[i]=pow(maxT[i],t);
	    maxT[i]=int(d*maxT[i]);
	    count[static_cast<const unsigned int>(maxT[i])]++;
	  }

	chi2(count,probs,d,N/t,result,V);
	what = "Maximum-of-5 test for ran2";
	if(V<result[5])
	  Test.Check(true)<<what<<endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/double(d-1) << endl;

	count=0;
	for(int j=0;j<N/t;j++)
	{
		for(int k=0;k<t;k++)
		{
			if(rand3[t*j+max]<rand3[t*j+k])
				max=k;
		}
		maxT[j]=rand3[t*j+max];
	}

	for(unsigned int i=0;i<maxT.size();i++)
	  {
	    maxT[i]=pow(maxT[i],t);
	    maxT[i]=int(d*maxT[i]);
	    count[static_cast<const unsigned int>(maxT[i])]++;
	  }

	chi2(count,probs,d,N/t,result,V);
	what = "Maximum-of-5 test for ran3";
	if(V<result[5])
	  Test.Check(true)<<what<<endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/double(d-1) << endl;

	count=0;
	for(int j=0;j<N/t;j++)
	{
		for(int k=0;k<t;k++)
		{
			if(rand4[t*j+max]<rand4[t*j+k])
				max=k;
		}
		maxT[j]=rand4[t*j+max];
	}

	for(unsigned int i=0;i<maxT.size();i++)
	  {
	    maxT[i]=pow(maxT[i],t);
	    maxT[i]=int(d*maxT[i]);
	    count[static_cast<const unsigned int>(maxT[i])]++;
	  }

	chi2(count,probs,d,N/t,result,V);
	what = "Maximum-of-5 test for ran4";
	if(V<result[5])
	  Test.Check(true)<<what<<endl;
	else
	  Test.Check(false) << what << endl;
	Test.Message() << "Result per degree of freedom: " << V/double(d-1) << endl << endl;
}

//The collision tests that for N balls "thrown" into M urns, about the expected number of 
//collisions will occur.  The expected number of collisions and corresponding probabilities
//are calculated using an alogrithm from The Art of Computer Programming. 
void CollisionTest()
{
	int n=16384,m=1048576,d=2,N=n*20;
	valarray<int> rand0(N),rand1(N),rand2(N),rand3(N),rand4(N);
	valarray<double> A(0.,n+1);
	A[1]=1;
	int j0=1,j1=1;
	for(int i=1;i<n;i++)
	{
		j1++;
		for(int j=j1;j>=j0;j--)
		{
			A[j]=(double(j)/m)*A[j]+((1+1.0/m)-(double(j)/m))*A[j-1];
			if(A[j]<10e-20)
			{
				A[j]=0;
				if(j==j1)
					j1--;
				else if(j==j0)
					j0++;
			}
		}
	}
	valarray<double> probs(8),collisions(0.,probs.size());
	probs[0]=.01;
	probs[1]=.05;
	probs[2]=.25;
	probs[3]=.5;
	probs[4]=.75;
	probs[5]=.95;
	probs[6]=.99;
	probs[7]=1.00;
	double p=0.0;
	unsigned int t=0,j=j0-1;
	while(t<probs.size())
	{
		j++;
		p=p+A[j];
		if(p>=probs[t]-.0001)
		{
		  collisions[t]=n-j-1;
		  t++;
		}
	}

	valarray<int> urns(0,m);	
	for(int i=0;i<N;i++)
	{
		rand0[i]=(int)( d*ran0(ok0) );
		rand1[i]=(int)( d*ran1(ok1) );
		rand2[i]=(int)( d*ran2(ok2) );
		rand3[i]=(int)( d*ran3(ok3) );
		rand4[i]=(int)( d*ran4(ok4) );
	}
	int temp=1,total=0,hits=0;	
	for(int j=0;j<n;j++)
	{
		temp=1;
		total=0;
		for(int i=0;i<20;i++)
		{
			total+=rand0[20*j+i]*temp;
			temp*=2;
		}
		if(urns[total]==0)
			urns[total]=1;
		else
			hits++;
	}

	string what = "Collision Test for ran0";
	if(hits<collisions[1]&&hits>collisions[5])
		Test.Check(true) << what << endl;
	else
		Test.Check(false) << what << endl;

	Test.Message() << "Total number of hits: " << hits << endl
			<< "-- Expected number of hits: approximately 128" << endl;

	hits=0;
	urns=0;
        for(int j=0;j<n;j++)
        {
                temp=1;
                total=0;
                for(int i=0;i<20;i++)
                {
                        total+=rand1[20*j+i]*temp;
                        temp*=2;
                }
                if(urns[total]==0)
                        urns[total]=1;
                else
                        hits++;
        }
        what = "Collision Test for ran1";
        if(hits<collisions[1]&&hits>collisions[5])
                Test.Check(true) << what << endl;
        else 
                Test.Check(false) << what << endl;

        Test.Message() << "Total number of hits: " << hits << endl 
                        << "-- Expected number of hits: approximately 128" << endl;

	hits=0;
	urns=0;
        for(int j=0;j<n;j++)
        {
                temp=1;
                total=0;
                for(int i=0;i<20;i++)
                {
                        total+=rand2[20*j+i]*temp;
                        temp*=2;
                }
                if(urns[total]==0)
                        urns[total]=1;
                else
                        hits++;
        }
        what = "Collision Test for ran2";
        if(hits<collisions[1]&&hits>collisions[5])
                Test.Check(true) << what << endl;
        else 
                Test.Check(false) << what << endl;

        Test.Message() << "Total number of hits: " << hits << endl 
                        << "-- Expected number of hits: approximately 128" << endl;

	hits=0;
	urns=0;
        for(int j=0;j<n;j++)
        {
                temp=1;
                total=0;
                for(int i=0;i<20;i++)
                {
                        total+=rand3[20*j+i]*temp;
                        temp*=2;
                }
                if(urns[total]==0)
                        urns[total]=1;
                else
                        hits++;
        }
        what = "Collision Test for ran3";
        if(hits<collisions[1]&&hits>collisions[5])
                Test.Check(true) << what << endl;
        else 
                Test.Check(false) << what << endl;

        Test.Message() << "Total number of hits: " << hits << endl 
                        << "-- Expected number of hits: approximately 128" << endl;

	hits=0;
	urns=0;
        for(int j=0;j<n;j++)
        {
                temp=1;
                total=0;
                for(int i=0;i<20;i++)
                {
                        total+=rand4[20*j+i]*temp;
                        temp*=2;
                }
                if(urns[total]==0)
                        urns[total]=1;
                else
                        hits++;
        }
        what = "Collision Test for ran4";
        if(hits<collisions[1]&&hits>collisions[5])
                Test.Check(true) << what << endl;
        else 
                Test.Check(false) << what << endl;

        Test.Message() << "Total number of hits: " << hits << endl 
		       << "-- Expected number of hits: approximately 128" << endl << endl;
}

//The serial correlation test checks to ensure that successive values of the random 
//sequence are uncorrelated.  Test parameters are taken from The Art of Computer Programming.
void SerialCorTest()
{
	int n=5000;
	valarray<double> rand0a(n), rand0b(n), rand1a(n), rand1b(n), rand2a(n), rand2b(n), rand3a(n), rand3b(n),rand4a(n), rand4b(n);
	for(int i=0;i<n;i++)
	{
		rand0a[i]=ran0(ok0);
		rand1a[i]=ran1(ok1);
		rand2a[i]=ran2(ok2);
		rand3a[i]=ran3(ok3);
		rand4a[i]=ran4(ok4);
	}
	for(int i=0;i<n;i++)
	{
		rand0b[i]=rand0a[(i+1)%n];
		rand1b[i]=rand1a[(i+1)%n];
		rand2b[i]=rand2a[(i+1)%n];
		rand3b[i]=rand3a[(i+1)%n];
		rand4b[i]=rand4a[(i+1)%n];
	}
	double UV=0, U=0, V=0, U2=0, V2=0;
	for(int j=0;j<n;j++)
	{
		UV+=rand0a[j]*rand0b[j];
		U+=rand0a[j];
		V+=rand0b[j];
		U2+=rand0a[j]*rand0a[j];
		V2+=rand0b[j]*rand0b[j];
	}
	double C = (n*UV-U*V)/sqrt((n*U2-U*U)*(n*V2-V*V));
	double un=-1/double(n-1),sigma=sqrt(double(n*n)/((n-1)*(n-1)*(n-2)));
	string what = "Serial Correlation Test for ran0";
	if(C>un-2*sigma&&C<un+2*sigma)
		Test.Check(true) << what << endl;
	else 
		Test.Check(false) << what << endl;
	Test.Message() << "Correlation Coefficient is: " << C << endl;

	UV=0; 
	U=0; 
	V=0;
	U2=0; 
	V2=0;
        for(int j=0;j<n;j++)
        {
                UV+=rand1a[j]*rand1b[j];
                U+=rand1a[j];
                V+=rand1b[j];
                U2+=rand1a[j]*rand1a[j];
                V2+=rand1b[j]*rand1b[j];
        }
        C = (n*UV-U*V)/sqrt((n*U2-U*U)*(n*V2-V*V));
        what = "Serial Correlation Test for ran1";
        if(C>un-2*sigma&&C<un+sigma)
                Test.Check(true) << what << endl;
        else 
                Test.Check(false) << what << endl;
        Test.Message() << "Correlation Coefficient is: " << C << endl;
	
	UV=0; 
        U=0; 
        V=0; 
        U2=0;
        V2=0;
        for(int j=0;j<n;j++)
        {
                UV+=rand2a[j]*rand2b[j];
                U+=rand2a[j];
                V+=rand2b[j];
                U2+=rand2a[j]*rand2a[j];
                V2+=rand2b[j]*rand2b[j];
        }
        C = (n*UV-U*V)/sqrt((n*U2-U*U)*(n*V2-V*V));
        what = "Serial Correlation Test for ran2";
        if(C>un-2*sigma&&C<un+sigma)
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Correlation Coefficient is: " << C << endl;

	UV=0; 
        U=0; 
        V=0; 
        U2=0;
        V2=0;
        for(int j=0;j<n;j++)
        {
                UV+=rand3a[j]*rand3b[j];
                U+=rand3a[j];
                V+=rand3b[j];
                U2+=rand3a[j]*rand3a[j];
                V2+=rand3b[j]*rand3b[j];
        }
        C = (n*UV-U*V)/sqrt((n*U2-U*U)*(n*V2-V*V));
        what = "Serial Correlation Test for ran3";
        if(C>un-2*sigma&&C<un+sigma)
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Correlation Coefficient is: " << C << endl;

	UV=0; 
        U=0; 
        V=0; 
        U2=0;
        V2=0;
        for(int j=0;j<n;j++)
        {
                UV+=rand4a[j]*rand4b[j];
                U+=rand4a[j];
                V+=rand4b[j];
                U2+=rand4a[j]*rand4a[j];
                V2+=rand4b[j]*rand4b[j];
        }
        C = (n*UV-U*V)/sqrt((n*U2-U*U)*(n*V2-V*V));
        what = "Serial Correlation Test for ran4";
        if(C>un-2*sigma&&C<un+sigma)
                Test.Check(true) << what << endl;
        else
                Test.Check(false) << what << endl;
        Test.Message() << "Correlation Coefficient is: " << C << endl <<endl;
}

//The spectral test checks to ensure the inverse of random numbers are 
//sufficiently large for T consecutive values.  See 3.3.4 of The Art of
//Computer Programming for a more in-depth discussion of the spectral test.	
void SpectralTest()
{
	int T=6,a=0,m=0;
	
	{
		a=48271;
		m=2147483647;
		valarray<double> Vm(T+1);
		long long t=2,h=a,h1=m,p=1,p1=0,u=0,v=0,j=0;
		long long q=0,r=a;
		double s=1+pow(static_cast<const double>(a),2);
		do{
			q=(long long)( floor(double(h1)/h) );
			u=h1-q*h;
			v=p1-q*p;
			if(s>(pow(double(u),2)+pow(double(v),2)))
			{
			  s=pow(static_cast<const double>(u),2) + 
                            pow(static_cast<const double>(v),2);
				h1=h;
				h=u;
				p1=p;
				p=v;
			}
			else
				break;
		}while(1);
		u=u-h;	
		v=v-p;
		if(u*u+v*v<s)
		{
			s=double(u)*u+double(v)*v;
			h1=u;
			p1=v;
		}
                Vm[2]=sqrt(s);
		valarray<double> U(t*t);
		valarray<double> V(t*t);
		U[0]= -h;
		U[1]= p;
		U[2]= -h1;
		U[3]= p1;
		int temp=1;
		if(p1>0)
			temp=-1;
		V[0]= temp*p1;
		V[1]= temp*h1;
		V[2]= temp*(-p);
		V[3]= temp*(-h);
		int k=0;
//step 4
while(t!=T)
{
  t++;
  r=a*r%m;
  valarray<double> Ut(0.,t);
  Ut[0]=-r;
  Ut[t-1]=1;
  valarray<double> Vt(0.,t);
  Vt[t-1]=m;
  valarray<double> tempV(V),tempU(U);
  V.resize(t*t);
  U.resize(t*t);
  for(int rep=0;rep<t-1;rep++)
     for(int rep1=0;rep1<t-1;rep1++)
       {
	 V[rep*(t)+rep1]=tempV[rep*(t-1)+rep1];
	 U[rep*(t)+rep1]=tempU[rep*(t-1)+rep1];
        }
  V[slice (t*(t-1),t,1)]=Vt;
  U[slice (t*(t-1),t,1)]=Ut;
  for(int rep=0;rep<t-1;rep++)
    {
      q=(long long)( floor(V[rep*t]*r/m+0.5) );
      V[rep*t+t-1]=V[rep*t]*r-q*m;
      for(int rep1=0;rep1<t;rep1++)
	U[t*(t-1)+rep1]=U[t*(t-1)+rep1]+q*U[rep*t+rep1];
    }
  double prod=0;
  for(int rep=0;rep<t;rep++)
    prod+=U[t*(t-1)+rep]*U[t*(t-1)+rep];
  s=min(s,prod);
  k=t-1;
  j=0;
  //step 5
  do{
    prod=0;
    double prod1=0;
    for(int rep=0;rep<t;rep++)
      { 
	prod=0;
	prod1=0;
	for(int rep1=0;rep1<t;rep1++)
	  {
	    prod+=V[rep*t+rep1]*V[j*t+rep1];
	    prod1+=V[j*t+rep1]*V[j*t+rep1];
	  }
	if(rep!=j && 2*abs(prod)>prod1)
	  {
	    q=(long long)( floor(prod/prod1+.5) );
	    for(int rep1=0;rep1<t;rep1++)
	      {
		V[rep*t+rep1]=V[rep*t+rep1]-q*V[j*t+rep1];
		U[j*t+rep1]=U[j*t+rep1]+q*U[rep*t+rep1];
	      }
	    k=j;
	  }
      }
     //step 6
    if(k==j)
      {
	prod=0;
	for(int rep=0;rep<t;rep++)
	  prod+=U[j*t+rep]*U[j*t+rep];
	s=min(s,prod);
      }
   //step 7
    if((j+1)==t)
      j=0;
    else
      j++;
  }while(j!=k);
  //step 8
  valarray<double> X(0.,t),Y(0.,t),Z(t);
  k=t-1;
  for(j=0;j<t;j++)
    {
      prod=0;
      for(int rep=0;rep<t;rep++)
	  prod+=V[j*t+rep]*V[j*t+rep];
      double nat = m;
      Z[j]=floor(sqrt(floor(prod*s/pow(nat,2))));
    }
  //Step 9
  do{
	if(X[k]!=Z[k])
      {
	X[k]++;
	for(int rep=0;rep<t;rep++)
	    Y[rep]=Y[rep]+U[k*t+rep];
	//step 10
	do{
	  k++;
	  if(k<t)
	    {
	      X[k]=-Z[k];
	      for(int rep=0;rep<t;rep++)
		  Y[rep]=Y[rep]-2*Z[k]*U[k*t+rep];
	    }
	}while(k<t);
	prod=0;
	for(int rep=0;rep<t;rep++)
	  prod+=Y[rep]*Y[rep];
	s=min(s,prod);
      }  
      //Step 11
    k--;
  }while((k+1)>=1);
  Vm[t]=sqrt(s);
}

		string what = "Spectral Test for ran0 and ran1";
		bool pass=true;
		for(unsigned int i=2;i<Vm.size();i++)
		{
			Test.Message() << "v" << i << " = " << Vm[i] << " > "
				       << pow(2.0,30.0/i) << "?"<<endl; 
			pass = pass && Vm[i]>=pow(2.0,30.0/i);
		}
		Test.Check(pass) << what << endl;
	}//local stuff
		
	{
		a=40014;
		m=2147483563;
		valarray<double> Vm(T+1);
		long long t=2,h=a,h1=m,p=1,p1=0,u=0,v=0,j=0;
		long long q=0,r=a;
		double s=1+pow(static_cast<const double>(a),2);
		do{
			q=(long long)( floor(double(h1)/h) );
			u=h1-q*h;
			v=p1-q*p;
			if(s>(pow(double(u),2)+pow(double(v),2)))
			{
			  s=pow(static_cast<const double>(u),2) +
                            pow(static_cast<const double>(v),2);
				h1=h;
				h=u;
				p1=p;
				p=v;
			}
			else
				break;
		}while(1);
		u=u-h;	
		v=v-p;
		if(u*u+v*v<s)
		{
			s=double(u)*u+double(v)*v;
			h1=u;
			p1=v;
		}
                Vm[2]=sqrt(s);
		valarray<double> U(t*t);
		valarray<double> V(t*t);
		U[0]= -h;
		U[1]= p;
		U[2]= -h1;
		U[3]= p1;
		int temp=1;
		if(p1>0)
			temp=-1;
		V[0]= temp*p1;
		V[1]= temp*h1;
		V[2]= temp*(-p);
		V[3]= temp*(-h);
		int k=0;
//step 4
		
while(t!=T)
{
  t++;
  r=a*r%m;
  valarray<double> Ut(0.,t);
  Ut[0]=-r;
  Ut[t-1]=1;
  valarray<double> Vt(0.,t);
  Vt[t-1]=m;
  valarray<double> tempV(V),tempU(U);
  V.resize(t*t);
  U.resize(t*t);
  for(int rep=0;rep<t-1;rep++)
     for(int rep1=0;rep1<t-1;rep1++)
       {
	 V[rep*(t)+rep1]=tempV[rep*(t-1)+rep1];
	 U[rep*(t)+rep1]=tempU[rep*(t-1)+rep1];
        }
  V[slice (t*(t-1),t,1)]=Vt;
  U[slice (t*(t-1),t,1)]=Ut;
  for(int rep=0;rep<t-1;rep++)
    {
      q=(long long)( floor(V[rep*t]*r/m+0.5) );
      V[rep*t+t-1]=V[rep*t]*r-q*m;
      for(int rep1=0;rep1<t;rep1++)
	U[t*(t-1)+rep1]=U[t*(t-1)+rep1]+q*U[rep*t+rep1];
    }
  double prod=0;
  for(int rep=0;rep<t;rep++)
    prod+=U[t*(t-1)+rep]*U[t*(t-1)+rep];
  s=min(s,prod);
  k=t-1;
  j=0;
  //step 5
  do{
    prod=0;
    double prod1=0;
    for(int rep=0;rep<t;rep++)
      { 
	prod=0;
	prod1=0;
	for(int rep1=0;rep1<t;rep1++)
	  {
	    prod+=V[rep*t+rep1]*V[j*t+rep1];
	    prod1+=V[j*t+rep1]*V[j*t+rep1];
	  }
	if(rep!=j && 2*abs(prod)>prod1)
	  {
	    q=(long long)( floor(prod/prod1+.5) );
	    for(int rep1=0;rep1<t;rep1++)
	      {
		V[rep*t+rep1]=V[rep*t+rep1]-q*V[j*t+rep1];
		U[j*t+rep1]=U[j*t+rep1]+q*U[rep*t+rep1];
	      }
	    k=j;
	  }
      }
     //step 6
    if(k==j)
      {
	prod=0;
	for(int rep=0;rep<t;rep++)
	  prod+=U[j*t+rep]*U[j*t+rep];
	s=min(s,prod);
      }
   //step 7
    if((j+1)==t)
      j=0;
    else
      j++;
  }while(j!=k);
  //step 8
  valarray<double> X(0.,t),Y(0.,t),Z(t);
  k=t-1;
  for(j=0;j<t;j++)
    {
      prod=0;
      for(int rep=0;rep<t;rep++)
	  prod+=V[j*t+rep]*V[j*t+rep];
      double nat = m;
      Z[j]=floor(sqrt(floor(prod*s/pow(nat,2))));
    }
  //Step 9
  do{
	if(X[k]!=Z[k])
      {
	X[k]++;
	for(int rep=0;rep<t;rep++)
	    Y[rep]=Y[rep]+U[k*t+rep];
	//step 10
	do{
	  k++;
	  if(k<t)
	    {
	      X[k]=-Z[k];
	      for(int rep=0;rep<t;rep++)
		  Y[rep]=Y[rep]-2*Z[k]*U[k*t+rep];
	    }
	}while(k<t);
	prod=0;
	for(int rep=0;rep<t;rep++)
	  prod+=Y[rep]*Y[rep];
	s=min(s,prod);
      }  
      //Step 11
    k--;
  }while((k+1)>=1);
  Vm[t]=sqrt(s);
}

		string what = "Spectral Test for ran2 part 1";
		bool pass=true;
		for(unsigned int i=2;i<Vm.size();i++)
		{
		  Test.Message() << "v" << i << " = " << Vm[i] << " > " << pow(2.0,30.0/i) <<"?" <<endl; 
			pass = pass && Vm[i]>=pow(2.0,30.0/i);
		}
		Test.Check(pass) << what << endl;
	}//local stuff

	{
		a=40692;
		m=2147483399;
		valarray<double> Vm(T+1);
		long long t=2,h=a,h1=m,p=1,p1=0,u=0,v=0,j=0;
		long long q=0,r=a;
		double s=1+pow(static_cast<const double>(a),2);
		do{
			q=(long long)( floor(double(h1)/h) );
			u=h1-q*h;
			v=p1-q*p;
			if(s>(pow(double(u),2)+pow(double(v),2)))
			{
			  s=pow(static_cast<const double>(u),2) +
                            pow(static_cast<const double>(v),2);
				h1=h;
				h=u;
				p1=p;
				p=v;
			}
			else
				break;
		}while(1);
		u=u-h;	
		v=v-p;
		if(u*u+v*v<s)
		{
			s=double(u)*u+double(v)*v;
			h1=u;
			p1=v;
		}
                Vm[2]=sqrt(s);
		valarray<double> U(t*t);
		valarray<double> V(t*t);
		U[0]= -h;
		U[1]= p;
		U[2]= -h1;
		U[3]= p1;
		int temp=1;
		if(p1>0)
			temp=-1;
		V[0]= temp*p1;
		V[1]= temp*h1;
		V[2]= temp*(-p);
		V[3]= temp*(-h);
		int k=0;
//step 4
while(t!=T)
{
  t++;
  r=a*r%m;
  valarray<double> Ut(0.,t);
  Ut[0]=-r;
  Ut[t-1]=1;
  valarray<double> Vt(0.,t);
  Vt[t-1]=m;
  valarray<double> tempV(V),tempU(U);
  V.resize(t*t);
  U.resize(t*t);
  for(int rep=0;rep<t-1;rep++)
     for(int rep1=0;rep1<t-1;rep1++)
       {
	 V[rep*(t)+rep1]=tempV[rep*(t-1)+rep1];
	 U[rep*(t)+rep1]=tempU[rep*(t-1)+rep1];
        }
  V[slice (t*(t-1),t,1)]=Vt;
  U[slice (t*(t-1),t,1)]=Ut;
  for(int rep=0;rep<t-1;rep++)
    {
      q=(long long)( floor(V[rep*t]*r/m+0.5) );
      V[rep*t+t-1]=V[rep*t]*r-q*m;
      for(int rep1=0;rep1<t;rep1++)
	U[t*(t-1)+rep1]=U[t*(t-1)+rep1]+q*U[rep*t+rep1];
    }
  double prod=0;
  for(int rep=0;rep<t;rep++)
    prod+=U[t*(t-1)+rep]*U[t*(t-1)+rep];
  s=min(s,prod);
  k=t-1;
  j=0;
  //step 5
  do{
    prod=0;
    double prod1=0;
    for(int rep=0;rep<t;rep++)
      { 
	prod=0;
	prod1=0;
	for(int rep1=0;rep1<t;rep1++)
	  {
	    prod+=V[rep*t+rep1]*V[j*t+rep1];
	    prod1+=V[j*t+rep1]*V[j*t+rep1];
	  }
	if(rep!=j && 2*abs(prod)>prod1)
	  {
	    q=(long long)( floor(prod/prod1+.5) );
	    for(int rep1=0;rep1<t;rep1++)
	      {
		V[rep*t+rep1]=V[rep*t+rep1]-q*V[j*t+rep1];
		U[j*t+rep1]=U[j*t+rep1]+q*U[rep*t+rep1];
	      }
	    k=j;
	  }
      }
     //step 6
    if(k==j)
      {
	prod=0;
	for(int rep=0;rep<t;rep++)
	  prod+=U[j*t+rep]*U[j*t+rep];
	s=min(s,prod);
      }
   //step 7
    if((j+1)==t)
      j=0;
    else
      j++;
  }while(j!=k);
  //step 8
  valarray<double> X(0.,t),Y(0.,t),Z(t);
  k=t-1;
  for(j=0;j<t;j++)
    {
      prod=0;
      for(int rep=0;rep<t;rep++)
	  prod+=V[j*t+rep]*V[j*t+rep];
      double nat = m;
      Z[j]=floor(sqrt(floor(prod*s/pow(nat,2))));
    }
  //Step 9
  do{
	if(X[k]!=Z[k])
      {
	X[k]++;
	for(int rep=0;rep<t;rep++)
	    Y[rep]=Y[rep]+U[k*t+rep];
	//step 10
	do{
	  k++;
	  if(k<t)
	    {
	      X[k]=-Z[k];
	      for(int rep=0;rep<t;rep++)
		  Y[rep]=Y[rep]-2*Z[k]*U[k*t+rep];
	    }
	}while(k<t);
	prod=0;
	for(int rep=0;rep<t;rep++)
	  prod+=Y[rep]*Y[rep];
	s=min(s,prod);
      }  
      //Step 11
    k--;
  }while((k+1)>=1);
  Vm[t]=sqrt(s);
}

		string what = "Spectral Test for ran2 part 2";
		bool pass=true;
		for(unsigned int i=2;i<Vm.size();i++)
		{
		  Test.Message() << "v" << i << " = " << Vm[i] << " > " << pow(2.0,30.0/i) << "?" << endl; 
			pass = pass && Vm[i]>=pow(2.0,30.0/i);
		}
		Test.Check(pass) << what << endl << endl;
	}//local stuff

}//SpectralTest()

//The normal test checks that gasdev creates a normal distribution of random
//numbers.  It calculates the mean and variance of a sequence and then checks
//to make sure the mean is close enough to 0 and the variance is close enough
//to 1.
void NormalTest()
{
  int N=81;
  valarray<double> num1(N);
  for(int i=0;i<N;i++)
      num1[i]=gasdev(okg);
  
  double mean1=0;
  
  for(int i=0;i<N;i++)
      mean1+=num1[i];
  mean1/=N;

  bool pass = abs(mean1) < 1.96/sqrt(double(N));
  Test.Check(pass) << "Normal Distribution's mean is close enough to zero\n";
  Test.Message() << "For " << N << " numbers the mean is: " << mean1 <<endl;

  double var1=0;
  for(int i=0;i<N;i++)
      var1+=pow(num1[i]-mean1,2);
      
  var1/=(N-1);
  pass = ((N-1.0)*var1>=57.15&&(N-1)*var1<=106.6);
  Test.Check(pass) << "Normal Distribution's variance is close enough to one\n";
  Test.Message() << "For " << N << " numbers the variance is: " << var1 <<endl;
}

int main( int ArgC, char** ArgV)
{
	try{
	string id("$Id: trandom.cc,v 1.12 2005/12/01 22:55:03 emaros Exp $");
	Test.Init( ArgC, ArgV);
	if(Test.IsVerbose())
		Test.Message() << id << std::endl;
      
	FreqTest();
	SerialTest();
	RunTest();
	PokerTest();
	CouponTest();
	GapTest();
	PermTest();
	Max_T_Test();
	SerialCorTest();
	CollisionTest();
	SpectralTest();		
	NormalTest();
	}
	catch(std::exception& x)
	{
		Test.Check(false) << "Caught std::exception " << x.what() << std::endl;
	}
	catch(...)
	{
		Test.Check(false) << "Caught unknown exception" << std::endl;
	}
	Test.Exit();
}
