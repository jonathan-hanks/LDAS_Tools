#include "datacondAPI/config.h"

#include <sstream>
   
#include "general/unittest.h"

#include "genericAPI/registry.hh"

#include "token.hh"

#include "WelchCSDEstimate.hh"
#include "TimeSeries.hh"
#include "UDT.hh"
#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "TSeriesFunction.hh"

using std::ostringstream;   
   
General::UnitTest	Test;

static void test_primary( int Type,
			  const std::string& TestName,
			  const int Expected );

static void test_seg_fault( int Type, int N = 1024 );

static int get_primary( const ILwd::LdasContainer& Element );

enum {
  NO_PRIMARY,
  PRIMARY_IN_FINAL_RESULT,
  ENSURE_PRIMARY_IN_FINAL_RESULT,
  PRIMARY_IN_INTERMEDIATE_RESULT,
  ENSURE_PRIMARY_IN_INTERMEDIATE_RESULT,
  MULTIPLE_PRIMARIES,
  NO_VALID_PRIMARIES,
  SEG_1
};

#define NN 10		// number of data samples

template<class T>
bool testData(void)
{
 bool pass = true;
 double dr = 1024.;

 try {

    CallChain chain;
    Parameters	args;

    datacondAPI::Sequence<T> in(0.0,NN);

    for (unsigned int i=0; i < NN; i++) 
    {
        in[i] = i;
    }

    datacondAPI::Scalar<double> Rate(dr); // assign data rate to second parameter
    
    datacondAPI::TimeSeries<T> ts(in, dr);

    // Need to create copies of the input data because once AddSymbol
    // has been called on an object it's the CallChains responsibility
    // to delete it
    CallChain::Symbol* input = ts.Clone();

    chain.SetReturnTarget("wrapper");

    // set name and reference for 1st parameter
    chain.AddSymbol("input", input);

    chain.AppendCallFunction("value",  args.set(1, "input"), "result");
    chain.AppendIntermediateResult("result", "foo:primary", "Final Primary",
				   "wrapper");

    chain.Execute();

    datacondAPI::udt* out=chain.GetSymbol("result");	// get pointer to output

    if ( datacondAPI::udt::IsA<datacondAPI::TimeSeries<T> >(*out) )
    {
      datacondAPI::TimeSeries<T>* ts =
          &(datacondAPI::udt::Cast<datacondAPI::TimeSeries<T> >(*out));
      if ( dr != ts->GetSampleRate() ) pass = false;
      if ( NN != ts->size() )  pass = false;

      // compare data samples
      for (unsigned int i=0; i<NN; i++) 
      {
        if ( (*ts)[i] != in[i] )
          pass = false;
        }
    }
    else pass = false;

  } // end of 'try'
 CATCH( Test );

  return pass;
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  test_primary( NO_PRIMARY, "No Primary", -1 );

  test_primary( PRIMARY_IN_FINAL_RESULT,
		"Primary in Final Result",
		0 );
  test_primary( ENSURE_PRIMARY_IN_FINAL_RESULT,
		"Ensure Primary in Final Result",
		0 );

  test_primary( PRIMARY_IN_INTERMEDIATE_RESULT,
		"Primary in Intermediate Result",
		1 );
  test_primary( ENSURE_PRIMARY_IN_INTERMEDIATE_RESULT,
		"Ensure Primary in Intermediate Result",
		0 );
  test_primary( MULTIPLE_PRIMARIES,
		"Catch exception for multiple primaries",
		-1 );
  test_primary( NO_VALID_PRIMARIES,
		"Catch exception for No Valid Primaries",
		-1 );

  test_seg_fault( SEG_1 );

  Test.Check( testData<float>() )
    << "(test if data <float> assigned properly)" << std::endl;

  Test.Exit();
}

static void
test_primary( int Type,
	      const std::string& TestName,
	      const int Expected )
{
  try {
    //-------------------------------------------------------------------
    // Calculate the expected value
    //-------------------------------------------------------------------
    Parameters args;

    size_t     		n( 32 );  
    size_t     		fftLen( 8 );
    size_t     		overlap( 4 );

    datacondAPI::CSDEstimate::DetrendMethod detrendMethod
	( datacondAPI::CSDEstimate::none );

    const double	Base( 1.0 );
    const std::string	BaseTypeStr("double");
    const std::string	PSDTypeStr("dvalarray");
    datacondAPI::Sequence<double> in(Base, n);

    //-------------------------------------------------------------------
    // Calculate value using Call Chain.
    //-------------------------------------------------------------------

    CallChain		cmds;
    ostringstream	base;
    ostringstream	size;
    ostringstream	fftlen;
    ostringstream	overlaplen;
    ostringstream	dflag;
    
    base << Base;
    size << in.size();
    fftlen << fftLen;
    overlaplen << overlap;
    dflag << detrendMethod;

    CallChain::IntermediateResult::primary_type
      intermediate_as_primary( CallChain::IntermediateResult::MAYBE_PRIMARY );

    if ( PRIMARY_IN_INTERMEDIATE_RESULT == Type )
    {
      intermediate_as_primary = CallChain::IntermediateResult::PRIMARY;
    }
    cmds.Reset();

    cmds.AppendCallFunction("integer", args.set(1, size.str().c_str()), "N");
    cmds.AppendCallFunction(BaseTypeStr, args.set(1, base.str().c_str()), "base");
    cmds.AppendCallFunction("integer", args.set(1, fftlen.str().c_str()), "fftLen");
    cmds.AppendCallFunction("integer", args.set(1, overlaplen.str().c_str()),
			    "overlap");
    cmds.AppendCallFunction("integer", args.set(1, dflag.str().c_str()), "detrendMethod");
    cmds.AppendCallFunction(BaseTypeStr, args.set(1, base.str().c_str()), "base");
    cmds.AppendCallFunction(PSDTypeStr, args.set(2, "base", "N"), "array");

    cmds.AppendCallFunction("psd",
			    args.set(5, "array", "fftLen", "",
				     "overlap", "detrendMethod"),
			    "result");
    if ( ( ENSURE_PRIMARY_IN_FINAL_RESULT != Type ) &&
	 ( PRIMARY_IN_FINAL_RESULT != Type ) &&
	 ( NO_VALID_PRIMARIES != Type) )
    {
      cmds.AppendIntermediateResult("result", "y1", "Should be primary",
				    "wrapper",
				    CallChain::IntermediateResult::MAYBE_PRIMARY );
      cmds.AppendIntermediateResult("result", "y1", "Should be primary",
				    "wrapper",
				    intermediate_as_primary );
    }
    if ( MULTIPLE_PRIMARIES == Type )
    {
      cmds.AppendIntermediateResult( "result", "y1", "Should be primary",
				     "wrapper",
				     CallChain::IntermediateResult::PRIMARY );
      cmds.AppendIntermediateResult( "result", "y1", "Should be primary",
				     "wrapper",
				     CallChain::IntermediateResult::PRIMARY );
    }

    if ( PRIMARY_IN_FINAL_RESULT == Type )
    {
      cmds.AppendIntermediateResult( "result", "y1", "Should be primary",
				     "wrapper",
				     CallChain::IntermediateResult::PRIMARY );
    }
    else if ( ENSURE_PRIMARY_IN_FINAL_RESULT == Type )
    {
      cmds.AppendIntermediateResult( "result", "y1", "Should be primary",
				     "wrapper" );
    }
    if ( (PRIMARY_IN_FINAL_RESULT == Type ) 
	|| ( ENSURE_PRIMARY_IN_FINAL_RESULT == Type ) )
    {
      cmds.SetReturnTarget( "wrapper" );
    }
    if ( ( ENSURE_PRIMARY_IN_FINAL_RESULT == Type ) ||
	 ( ENSURE_PRIMARY_IN_INTERMEDIATE_RESULT == Type ) ||
	 ( MULTIPLE_PRIMARIES == Type ) ||
	 ( NO_VALID_PRIMARIES == Type) )
    {
      cmds.EnsurePrimary();
    }
    cmds.Execute();

    //-------------------------------------------------------------------
    // Compare the results to the exepected value.
    //-------------------------------------------------------------------
    
    ILwd::LdasContainer*	ilwd = cmds.GetResults();
    bool			pass;
    int				primary_offset( get_primary( *ilwd ) );

    Test.Check( (pass = ( (ilwd) && (Expected == primary_offset ) ) ) )
      << TestName
      << ": " << "primary at: " << primary_offset
      << " (expected " << Expected << ")"
      << std::endl;
    if ( !pass )
    {
      ilwd->write(2, 2, Test.Message(), ILwd::ASCII);
      Test.Message( false ) << std::endl;

    }

    Registry::elementRegistry.destructObject(ilwd);
  }
  catch ( const std::logic_error& e )
  {
    if ( ( MULTIPLE_PRIMARIES == Type ) ||
	 ( NO_VALID_PRIMARIES == Type ) )
    {
      Test.Check( true ) << TestName << std::endl;
    }
    else
    {
      Test.Check(false) << TestName
			<< ": logic error: " << e.what() << std::endl;
    }
  }
  CATCH(Test);
}

static void
test_seg_fault( int Type, int N )
{
  try {
    const double				dr(1024);

    Parameters				args;
    CallChain				cmds;
    datacondAPI::Sequence<double>		in( 0.0, N );

    for ( int x = 0; x < N; x++ )
    {
      in[x] = x;
    }

    datacondAPI::TimeSeries<double>	gw(in, dr);
    
    cmds.AddSymbol( "gw", gw.Clone() );
    cmds.AppendCallFunction("value", args.set(1, "gw"), "x" );
    cmds.AppendIntermediateResult("x", "foo", "Should be primary", "wrapper",
				  CallChain::IntermediateResult::PRIMARY );
    cmds.AppendCallFunction("psd", args.set(1, "x"), "result" );
    cmds.AppendIntermediateResult( "result", "result", "Finarl Result",
				   "wrapper" );

    cmds.EnsurePrimary();
    cmds.Execute();
    
    ILwd::LdasContainer*	ilwd = cmds.GetResults();

    Registry::elementRegistry.destructObject(ilwd);
  }
  CATCH( Test );
}

static int
get_primary( const ILwd::LdasContainer& Element )
{
  int retval(0);
  for (ILwd::LdasContainer::const_iterator i = Element.begin();
       i != Element.end();
       i++, retval++)
  {
    if ( (*i)->getName(3) == "primary")
    {
      return retval;
    }
  }
  return -1;
}

