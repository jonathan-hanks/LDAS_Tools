#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "fft_check.hh"

#include "ifft.hh"
#include "fft.hh"

#include "SequenceUDT.hh"
#include "DFTUDT.hh"

using std::complex;
using namespace datacondAPI;

// Symmetry - generic case is false
template<class T>
bool symmetry_for_fft()
{
    return false;
}

// For real, should return true
template<>
bool symmetry_for_fft<float>()
{
    return true;
}

template<>
bool symmetry_for_fft<double>()
{
    return true;
}


//
// Test FFT for a particular length n
//
// tdata is the time-domain data to be FFT'ed
// fdata is the expected frequency domain data
//
template<class TDataType_, class FDataType_>
void
TestFFT(const TDataType_* tdata,
	const std::complex<FDataType_>* fdata,
	const size_t n,
	const FDataType_ TOLERANCE,
	General::UnitTest& Test)
{
    Test.Message() << "Starting...\n";

    const unsigned int nyquist = (n + 1) / 2;
    const std::complex<FDataType_>* p = fdata;
    Sequence<std::complex<FDataType_> > fft_result(n);
    for(unsigned int i = nyquist; i < n; ++i)
    {
      fft_result[i] = *p;
      ++p;
    }
    for(unsigned int i = 0; i < nyquist; ++i)
    {
      fft_result[i] = *p;
      ++p;
    }
    Test.Message() << "Copied...\n";
    Sequence< TDataType_ > fft_in(tdata, n);

    // CONTINUES
    
    // The FFT object is static so that we can test that successive FFT's
    // of different lengths work
    static FFT fft;
    
    udt* fft_out = 0;

    fft.apply(fft_out, fft_in);
    Test.Message() << "Applied\n";  

    Test.Message() << "Dereferencing " << fft_out << std::endl;

    DFT<std::complex<FDataType_> >&
      r_fft_out(udt::Cast<DFT<std::complex<FDataType_> > >(*fft_out));

    Test.Message() << "Referenced\n";
    Test.Check(r_fft_out.size() == (size_t)n)
      << "FFT output has correct size" << std::endl;
    Test.Check(r_fft_out.IsSymmetric() == symmetry_for_fft<TDataType_>())
      << "FFT output has correct symmetry" << std::endl;
    for(unsigned int i = 0; i < n; ++i)
    {
      const FDataType_ diff = std::abs(r_fft_out[i] - fft_result[i]);
      Test.Check(diff < TOLERANCE)
	<< "Error is within tolerance "
	<< i << "    "
	<< r_fft_out[i] << "    "
	<< fft_result[i] << "    "
	<< diff << std::endl;
    }

    r_fft_out.resize(0);
    Test.Message() << "Continuing...\n";
    
    fft.apply(fft_out, fft_in);

    Test.Check(r_fft_out.size() == (size_t)n)
      << "FFT output has correct size" << std::endl;
    Test.Check(r_fft_out.IsSymmetric() == symmetry_for_fft<TDataType_>())
      << "FFT output has correct symmetry" << std::endl;
    for(unsigned int i = 0; i < n; ++i)
    {
      const FDataType_ diff = std::abs(r_fft_out[i] - fft_result[i]);
      Test.Check(diff < TOLERANCE)
	<< "Error is within tolerance "
	<< i << "    "
	<< r_fft_out[i] << "    "
	<< fft_result[i] << "    "
	<< diff << std::endl;
    }

    r_fft_out.resize(n + 3);
    
    fft.apply(fft_out, fft_in);

    Test.Check(r_fft_out.size() == (size_t)n)
      << "FFT output has correct size" << std::endl;
    Test.Check(r_fft_out.IsSymmetric() == symmetry_for_fft<TDataType_>())
      << "FFT output has correct symmetry" << std::endl;
    for(unsigned int i = 0; i < n; ++i)
    {
      const FDataType_ diff = std::abs(r_fft_out[i] - fft_result[i]);
      Test.Check(diff < TOLERANCE)
	<< "Error is within tolerance "
	<< i << "    "
	<< r_fft_out[i] << "    "
	<< fft_result[i] << "    "
	<< diff << std::endl;
    }
    
    delete fft_out;
}

//
// Test IFFT for a particular length n
//
// tdata is the expected time-domain data
// fdata is the frequency domain data to be IFFT'ed
//
template<class TDataType_, class FDataType_>
void
TestIFFT(const TDataType_* tdata,
	 const complex<FDataType_>* fdata, const size_t n,
	 const FDataType_ TOLERANCE,
	 General::UnitTest& Test)
{
  Sequence< TDataType_ > ifft_result(tdata, n);

  const unsigned int nyquist = (n + 1) / 2;
  const complex<FDataType_>* p = fdata;
  DFT<std::complex<FDataType_> > ifft_in(n);
  for(unsigned int i = nyquist; i < n; ++i)
  {
    ifft_in[i] = *p;
    ++p;
  }
  for(unsigned int i = 0; i < nyquist; ++i)
  {
    ifft_in[i] = *p;
    ++p;
  }

  udt* ifft_out = 0;

  // The FFT object is static so that we can test that successive FFT's
  // of different lengths work
  static IFFT ifft;
     
  ifft.apply(ifft_out, ifft_in);

  Sequence<TDataType_>&
    r_ifft_out(udt::Cast<Sequence<TDataType_ > >(*ifft_out));
  Test.Check(r_ifft_out.size() == (size_t)n) << "IFFT output has correct size" << std::endl;
  for(unsigned int i = 0; i < n; ++i)
  {
    const FDataType_ diff = std::abs(r_ifft_out[i] - ifft_result[i]);
    Test.Check(diff < TOLERANCE)
      << "Error is within tolerance "
      << i << "    "
      << r_ifft_out[i] << "    "
      << ifft_result[i] << "    "
      << diff << std::endl;
  }
  r_ifft_out.resize(n);
  // CONTINUES
  
  ifft.apply(ifft_out, ifft_in);
    
  Test.Check(r_ifft_out.size() == (size_t)n)
    << "IFFT output has correct size" << std::endl;
  for(unsigned int i = 0; i < n; ++i)
  {
    const FDataType_ diff = std::abs(r_ifft_out[i] - ifft_result[i]);
    Test.Check(diff < TOLERANCE)
      << "Error is within tolerance "
      << i << "    "
      << r_ifft_out[i] << "    "
      << ifft_result[i] << "    "
      << diff << std::endl;
  }
  r_ifft_out.resize(n + 3);

  ifft.apply(ifft_out, ifft_in);

  Test.Check(r_ifft_out.size() == (size_t)n)
    << "IFFT output has correct size" << std::endl;
  for(unsigned int i = 0; i < n; ++i)
  {
    const FDataType_ diff = std::abs(r_ifft_out[i] - ifft_result[i]);
    Test.Check(diff < TOLERANCE)
      << "Error is within tolerance "
      << i << "    "
      << r_ifft_out[i] << "    "
      << ifft_result[i] << "    "
      << diff << std::endl;
  }

  delete ifft_out;
}

//
// Test FFT/IFFT reversibility for a particular length n
//
// tdata is the expected time-domain data
// fdata is the frequency domain data to be IFFT'ed
//
template<class TDataType_, class FDataType_>
void
TestFFTandIFFT(const TDataType_* tdata, const size_t n,
	       const FDataType_ TOLERANCE,
	       General::UnitTest& Test)
{
  Sequence< TDataType_ > fft_in(tdata, n);
  DFT<complex<FDataType_> > fft_out(n);
  Sequence< TDataType_ > ifft_out(n);
  udt* p_fft_out = &fft_out;
  udt* p_ifft_out = &ifft_out;

  // The FFT object is static so that we can test that successive FFT's
  // of different lengths work
  static FFT fft;
  static IFFT ifft;
  
  fft.apply(p_fft_out, fft_in);
  ifft.apply(p_ifft_out, fft_out);

  Test.Check(udt::IsA<Sequence<TDataType_> >(*p_ifft_out))
      << "IFFT is correct type" << std::endl;

  Test.Check(ifft_out.size() == (size_t)n)
    << "IFFT output has correct size" << std::endl;
  Test.Check(fft_out.size() == fft_in.size())
    << "IFFT output has same size as FFT input" << std::endl;
  Test.Check(fft_out.IsSymmetric() == symmetry_for_fft<TDataType_>())
    << "FFT output has correct symmetry" << std::endl;

  for (size_t i = 0; i < ifft_out.size(); i++)
  {
    const FDataType_ diff = std::abs(ifft_out[i] - fft_in[i]);
    Test.Check(diff < TOLERANCE)
      << "Error is within tolerance "
      << i << "    "
      << ifft_out[i] << "    "
      << fft_in[i] << "    "
      << diff << std::endl;
  }
}

#define INSTANTIATION(T, F) \
template void TestFFT< T, F >(const T* tdata, \
                              const std::complex< F >* fdata, \
                              const size_t n, \
			      const F TOLERANCE, \
			      General::UnitTest& Test); \
template void TestIFFT< T, F >(const T* tdata, \
                               const std::complex< F >* fdata, \
                               const size_t n, \
			       const F TOLERANCE, \
			       General::UnitTest& Test); \
template void TestFFTandIFFT< T, F >(const T* tdata, \
                                     const size_t n, \
				     const F TOLERANCE, \
				     General::UnitTest& Test)

INSTANTIATION(float, float);
INSTANTIATION(std::complex<float>, float);
INSTANTIATION(double, double);
