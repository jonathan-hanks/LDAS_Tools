//tResolver.cc

#include "datacondAPI/config.h"

#include <vector>
#include <valarray>
#include <iostream>

#include "general/unittest.h"

#include "SequenceUDT.hh"
#include "ScalarUDT.hh"
#include "WaveletPSU.hh"
#include "HaarPSU.hh"
#include "resolver.hh"
#include "result.hh"

using namespace std;   
using namespace datacondAPI::psu;

void TestOne();
void TestTwo();
void ExceptionTest();

General::UnitTest Test;

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  Test.Message() << "Executing TestOne." << endl;
  TestOne();

  Test.Message() << "Executing TestTwo." << endl;
  TestTwo();

  Test.Message() << "Executing ExceptionTest." << endl;
  ExceptionTest();
  
  Test.Check(true) << "End of test harness reached." << endl;
  
  Test.Exit();

} //end main


void TestOne()
{
  datacondAPI::Sequence<double> data(0.0, 8);
  data[2] = 1.0;

  Haar haarWavelet;
  Resolver test(haarWavelet, 4);

  //test Resolver function called getLevels
  Test.Message() << "Testing Resolver::getLevels." << endl;
  int levelsSet = test.getLevels();
  Test.Message() << "Levels set to " << levelsSet << endl;

  bool levelCheck = false;
  if (levelsSet == 4)
    levelCheck = true;
  Test.Check(levelCheck) << "Levels set." << endl;

  //test Resolver function called setLevels
  Test.Message() << "Testing Resolver::setLevels." << endl;
  test.setLevels(2);
  levelsSet = test.getLevels();
  Test.Message() << "Levels reset to " << levelsSet << endl;

  levelCheck = false;
  if (levelsSet == 2)
    levelCheck = true;
  Test.Check(levelCheck) << "Levels reset." << endl;

  Result<double> testResult;
  datacondAPI::udt* ptestResult = &testResult;

  //test Resolver function called applyFilter
  Test.Message() << "Testing Resolver::apply." << endl;
  try{
    test.apply(ptestResult, data);
  }
  catch (std::exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //test Result function called getLevels:
  Test.Message() << "Testing Result::getLevels." << endl;
  int resultLevels = testResult.getLevels();
  Test.Message() << "Levels of decomposition = "
		 << resultLevels << endl;
  levelCheck = false;
  if (resultLevels == 2)
    levelCheck = true;
  Test.Check(levelCheck) << "Result has correct number of levels"
			 << " of decomposition." << endl;

  //check filtered signal
  datacondAPI::Sequence<double> first;
  datacondAPI::udt* pfirst = &first;
  datacondAPI::Scalar<int> level0(0);
  try
    {
      testResult.getResult(pfirst, level0);
    }

  catch(exception &e)
    {
      Test.Check(false) << e.what() << endl;
      exit(1);
    }

  datacondAPI::Sequence<double> second;
  datacondAPI::udt* psecond = &second;
  datacondAPI::Scalar<int> level1(1);
  try
    {
      testResult.getResult(psecond, level1);
    }

  catch(exception &e)
    {
      Test.Check(false) << e.what() << endl;
      exit(1);
    }

  datacondAPI::Sequence<double> lowpass;
  datacondAPI::udt* plowpass = &lowpass;
  datacondAPI::Scalar<int> lowpassnum(2);
  try
    {
      testResult.getResult(plowpass, lowpassnum);
    }

  catch(exception &e)
    {
      Test.Check(false) << e.what() << endl;
      exit(1);
    }

  Test.Message() << "First decomposition:" << endl;
  for (unsigned int i = 0; i < first.size(); i++)
    Test.Message() << first[i] << endl;

  Test.Message() << "Second decomposition:" << endl;
  for (unsigned int j = 0; j < second.size(); j++)
    Test.Message() << second[j] << endl;

  Test.Message() << "Final lowpass result:" << endl;
  for (unsigned int k = 0; k < lowpass.size(); k++)
    Test.Message() << lowpass[k] << endl;

  //expected output
  datacondAPI::Sequence<double> e_first(0.0, 4);
  e_first[1] = 1.0;
  datacondAPI::Sequence<double> e_second(0.0, 2);
  e_second[1] = -1.0;
  datacondAPI::Sequence<double> e_lowpass(0.0, 2);
  e_lowpass[1] = 1.0;

  Test.Message() << "Expected first decomposition:" << endl;
  for (unsigned int i = 0; i < e_first.size(); i++)
    Test.Message() << e_first[i] << endl;

  Test.Message() << "Expected second decomposition:" << endl;
  for (unsigned int j = 0; j < e_second.size(); j++)
    Test.Message() << e_second[j] << endl;

  Test.Message() << "Expected final lowpass result:" << endl;
  for (unsigned int k = 0; k < e_lowpass.size(); k++)
    Test.Message() << e_lowpass[k] << endl;

  //find/display difference between expected and actual
  datacondAPI::Sequence<double> first_difference(e_first - first);
  datacondAPI::Sequence<double> second_difference(e_second - second);
  datacondAPI::Sequence<double> lowpass_difference(e_lowpass - lowpass);

  Test.Message() << "Difference in first decomposition:" << endl;
  for (unsigned int i = 0; i < first_difference.size(); i++)
    Test.Message() << first_difference[i] << endl;

  Test.Message() << "Difference in second decomposition:" << endl;
  for (unsigned int j = 0; j < second_difference.size(); j++)
    Test.Message() << second_difference[j] << endl;

  Test.Message() << "Difference in final lowpass result:" << endl;
  for (unsigned int k = 0; k < lowpass_difference.size(); k++)
    Test.Message() << lowpass_difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check_first(e_first == first);
  std::valarray<bool> check_second(e_second == second);
  std::valarray<bool> check_lowpass(e_lowpass == lowpass);

  bool pass1, pass2, pass3;
  pass1 = pass2 = pass3 = true;

  for (unsigned int i = 0; i < check_first.size(); i++)
    pass1 = pass1 && check_first[i];

  for (unsigned int i = 0; i < check_second.size(); i++)
    pass2 = pass2 && check_second[i];

  for (unsigned int i = 0; i < check_lowpass.size(); i++)
    pass3 = pass3 && check_lowpass[i];

  bool overallpass = false;
  if(pass1 == true && pass2 == true && pass3 == true)
    overallpass = true;

  Test.Check(overallpass) << "Filtered output." <<endl;


}//end TestOne.


void TestTwo()
{
  datacondAPI::Sequence<double> data(0.0, 48);
  data[3] = 1.0;

  Haar haarWavelet;
  Resolver test;
  Result<double> testResult;
  datacondAPI::udt* ptestResult = &testResult;

  Test.Message() << "Testing Resolver::isReady." << endl;
  bool ready_check = false;
  ready_check = test.isReady();

  if (ready_check == false)
  {
    Test.Message() << "Wavelet and/or levels were not set." << endl;
    test.setWavelet(haarWavelet);
    Test.Message() << "setWavelet executed." << endl;
    test.setLevels(4);
    Test.Message() << "setLevels executed." << endl;
    test.getWavelet();
    Test.Message() << "getWavelet executed." << endl;
  }
  ready_check = test.isReady();

  Test.Check(ready_check) << "isReady" << endl;

  Resolver test2(haarWavelet, 4);
  Test.Message()<< "Applying filter." << endl;
  try{
    test2.apply(ptestResult, data);
  }
  catch (exception &e)
    {
      Test.Check(false) << e.what() << endl;
    }
  Test.Message() << "Filter applied to data." << endl;

  //test Result function called getLevels:
  Test.Message() << "Testing Result::getLevels." << endl;
  int resultLevels = testResult.getLevels();
  Test.Message() << "Levels of decomposition = "
		 << resultLevels << endl;
  bool levelCheck = false;
  if (resultLevels == 4)
    levelCheck = true;
  Test.Check(levelCheck) << "Result has correct number of levels"
			 << " of decomposition." << endl;

  //check filtered signal
  datacondAPI::Sequence<double> zero;
  datacondAPI::udt* pzero = &zero;
  datacondAPI::Scalar<int> level0(0);
  testResult.getResult(pzero, level0);

  datacondAPI::Sequence<double> one;
  datacondAPI::udt* pone = &one;
  datacondAPI::Scalar<int> level1(1);
  testResult.getResult(pone, level1);

  datacondAPI::Sequence<double> two;
  datacondAPI::udt* ptwo = &two;
  datacondAPI::Scalar<int> level2(2);
  testResult.getResult(ptwo, level2);

  datacondAPI::Sequence<double> three;
  datacondAPI::udt* pthree = &three;
  datacondAPI::Scalar<int> level3 (3);
  testResult.getResult(pthree, level3);

  datacondAPI::Sequence<double> lowpass;
  datacondAPI::udt* plowpass = &lowpass;
  datacondAPI::Scalar<int> lowpassnum (4);
  testResult.getResult(plowpass, lowpassnum);

  Test.Message() << "Zeroth decomposition:" << endl;
  for (unsigned int i = 0; i < zero.size(); i++)
    Test.Message() << zero[i] << endl;

  Test.Message() << "First decomposition:" << endl;
  for (unsigned int j = 0; j < one.size(); j++)
    Test.Message() << one[j] << endl;

  Test.Message() << "Second decomposition:" << endl;
  for (unsigned int j = 0; j < two.size(); j++)
    Test.Message() << two[j] << endl;

  Test.Message() << "Third decomposition:" << endl;
  for (unsigned int j = 0; j < three.size(); j++)
    Test.Message() << three[j] << endl;

  Test.Message() << "Final lowpass result:" << endl;
  for (unsigned int k = 0; k < lowpass.size(); k++)
    Test.Message() << lowpass[k] << endl;

  //expected output
  datacondAPI::Sequence<double> e_zero(0.0, 24);
  e_zero[2] = -1.0;
  datacondAPI::Sequence<double> e_first(0.0, 12);
  e_first[1] = 1.0;
  datacondAPI::Sequence<double> e_second(0.0, 6);
  e_second[1] = -1.0;
  datacondAPI::Sequence<double> e_third(0.0, 3);
  e_third[1] = -1.0;
  datacondAPI::Sequence<double> e_lowpass(0.0, 3);
  e_lowpass[1] = 1.0;

  Test.Message() << "Expected zeroth decomposition:" << endl;
  for (unsigned int i = 0; i < e_zero.size(); i++)
    Test.Message() << e_zero[i] << endl;

  Test.Message() << "Expected first decomposition:" << endl;
  for (unsigned int j = 0; j < e_first.size(); j++)
    Test.Message() << e_first[j] << endl;

  Test.Message() << "Expected second decomposition:" << endl;
  for (unsigned int j = 0; j < e_second.size(); j++)
    Test.Message() << e_second[j] << endl;

  Test.Message() << "Expected third decomposition:" << endl;
  for (unsigned int j = 0; j < e_third.size(); j++)
    Test.Message() << e_third[j] << endl;

  Test.Message() << "Expected final lowpass result:" << endl;
  for (unsigned int k = 0; k < e_lowpass.size(); k++)
    Test.Message() << e_lowpass[k] << endl;

  //find/display difference between expected and actual
  datacondAPI::Sequence<double> first_difference(e_zero - zero);
  datacondAPI::Sequence<double> second_difference(e_first - one);
  datacondAPI::Sequence<double> third_difference(e_second - two);
  datacondAPI::Sequence<double> fourth_difference(e_third - three);
  datacondAPI::Sequence<double> lowpass_difference(e_lowpass - lowpass);

  Test.Message() << "Difference in zeroth decomposition:" << endl;
  for (unsigned int i = 0; i < first_difference.size(); i++)
    Test.Message() << first_difference[i] << endl;

  Test.Message() << "Difference in first decomposition:" << endl;
  for (unsigned int j = 0; j < second_difference.size(); j++)
    Test.Message() << second_difference[j] << endl;

  Test.Message() << "Difference in second decomposition:" << endl;
  for (unsigned int j = 0; j < third_difference.size(); j++)
    Test.Message() << third_difference[j] << endl;

  Test.Message() << "Difference in third decomposition:" << endl;
  for (unsigned int j = 0; j < fourth_difference.size(); j++)
    Test.Message() << fourth_difference[j] << endl;

  Test.Message() << "Difference in final lowpass result:" << endl;
  for (unsigned int k = 0; k < lowpass_difference.size(); k++)
    Test.Message() << lowpass_difference[k] << endl;

  //test to see if output is correct
  std::valarray<bool> check_zero(e_zero == zero);
  std::valarray<bool> check_one(e_first == one);
  std::valarray<bool> check_two(e_second == two);
  std::valarray<bool> check_three(e_third == three);
  std::valarray<bool> check_lowpass(e_lowpass == lowpass);

  bool pass1, pass2, pass3, pass4, pass5;
  pass1 = pass2 = pass3 = pass4 = pass5 = true;

  for (unsigned int i = 0; i < check_zero.size(); i++)
    pass1 = pass1 && check_zero[i];

  for (unsigned int i = 0; i < check_one.size(); i++)
    pass2 = pass2 && check_one[i];

  for (unsigned int i = 0; i < check_two.size(); i++)
    pass3 = pass3 && check_two[i];

  for (unsigned int i = 0; i < check_three.size(); i++)
    pass4 = pass4 && check_three[i];

  for (unsigned int i = 0; i < check_lowpass.size(); i++)
    pass5 = pass5 && check_lowpass[i];

  bool overallpass = false;
  if(pass1 == true && pass2 == true && pass3 == true
     && pass4 == true && pass5 == true)
    overallpass = true;

  Test.Check(overallpass) << "Filtered output." <<endl;

}//end TestTwo.


void ExceptionTest()
{
  //create a data sequence of odd length
  datacondAPI::Sequence<double> data(0.0, 7);
  data[2] = 1.0;

  Haar haarWavelet;

  //error strings
  std::string what = "Levels must be greater than zero";
  std::string what2 = "Data input must have even number of data points.";

  //give Resolver constructor an invalid argument
  try
    {
      Resolver test(haarWavelet, 0);
      Test.Check(false) << what << endl;
    }
  catch(invalid_argument& i)
    {
      Test.Check(true) << what << " (" <<i.what()<< ")" << endl;
    }

  
  //give setLevels an invalid argument
  try
    {
      Resolver test;
      test.setLevels(-4);
      Test.Check(false) << what << endl;
    }
  catch(invalid_argument& j)
    {
      Test.Check(true) << what << " (" << j.what() << ")" << endl;
    }

  Result<double> output;
  datacondAPI::udt* poutput = &output;
  //give apply an invalid argument
  try
    {
      Resolver test(haarWavelet, 2);
      test.apply(poutput, data);
      Test.Check(false) << what2 << endl;
    }
  catch(exception& k)
    {
      Test.Check(true) << what2 << " (" << k.what() << ")" << endl;
    }

  //check error handling in getResult
  std::string what3 = "Level requested is illegal value";
  std::string what5 = "First parameter not correct data type";

  datacondAPI::Sequence<double> data2(0.0, 8);
  data[2] = 1.0;
  Resolver test2(haarWavelet, 2);
  Result<double> testResult2;
  datacondAPI::udt* ptestResult2 = &testResult2;
  test2.apply(ptestResult2, data2);
  datacondAPI::Sequence<double> a;
  datacondAPI::udt* pa = &a;
  datacondAPI::Scalar<int> level(5);
  try
    {
      testResult2.getResult(pa, level);
      Test.Check(false) << what3 << endl;
    }
  catch(exception& l)
    {
      Test.Check(true) << what3 << " (" << l.what() << ")" << endl;
    }

  datacondAPI::Scalar<int> level1(-4);
  try
    {
      testResult2.getResult(pa, level1);
      Test.Check(false) << what3 << endl;
    }
  catch(exception& l)
    {
      Test.Check(true) << what3 << " (" << l.what() << ")" << endl;
    }


  datacondAPI::Sequence<float> b;
  datacondAPI::udt* pb = &b;
  datacondAPI::Scalar<int> level2(0);
  try
    {
      testResult2.getResult(pb, level2);
      Test.Check(false) << what5 << endl;
    }
  catch(exception& l)
    {
      Test.Check(true) << what5 << " (" << l.what() << ")" << endl;
    }
  datacondAPI::TimeSeries<float> c;
  datacondAPI::udt* pc = &c;
  try
    {
      testResult2.getResult(pc, level2);
      Test.Check(false) << what5 << endl;
    }
  catch(exception& l)
    {
      Test.Check(true) << what5 << " (" << l.what() << ")" << endl;
    }

} //end ExceptionTest

