#include "datacondAPI/config.h"

#include <complex>
#include <general/unittest.h>
#include <filters/LDASConstants.hh>

#include "ExpandPoly.hh"
#include "token.hh"

using namespace std;
using namespace General;
using namespace datacondAPI;

UnitTest Test;

const double TOL = 1.0e-05;

void TestExceptions()
{
    bool pass = true;
    string exc;

    // d.size() == 0
    try
    {
        const valarray<double> d(0);
        valarray<double> c(1.0, 1);
        
        multiplyPolys(c, d);

        pass = false;
    }
    catch(const std::invalid_argument& e)
    {
        pass = true;
        exc = e.what();
    }

    Test.Check(pass) << "d.size() == 0. Caught exception: " << exc << endl;

    // c.size() == 0
    try
    {
        const valarray<double> d(1.0, 1);
        valarray<double> c(0);
        
        multiplyPolys(c, d);

        pass = false;
    }
    catch(const std::invalid_argument& e)
    {
        pass = true;
        exc = e.what();
    }

    Test.Check(pass) << "c.size() == 0. Caught exception: " << exc << endl;

    // both.size() == 0
    try
    {
        const valarray<double> d(0);
        valarray<double> c(0);
        
        multiplyPolys(c, d);

        pass = false;
    }
    catch(const std::invalid_argument& e)
    {
        pass = true;
        exc = e.what();
    }

    Test.Check(pass) << "both.size() == 0. Caught exception: " << exc << endl;
}


template<class T>
void TestExpandPolyR()
{
    bool pass = true;
    valarray<complex<T> > a;
    valarray<T> c;

    Test.Message() << "Degree 0: P(x) = -5" << endl;

    a.resize(0);
    
    expandPoly(c, a, T(-5));

    Test.Check(c.size() == 1) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == -5);

    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message() << "Degree 0: P(x) = 2" << endl;

    a.resize(0);
    
    expandPoly(c, a, T(2));

    Test.Check(c.size() == 1) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == 2);

    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message() << "Degree 1: (x - 3) = -3 + x" << endl;

    a.resize(1, 0);
    a[0] = 3;
    
    expandPoly(c, a, T(1));

    Test.Check(c.size() == 2) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == -3);
    pass = pass && (c[1] == 1);

    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message() << "Degree 2: 3(x - 2)(x + 5) = -30 + 9x + 3x^2"
                   << endl;

    pass = true;

    a.resize(2, 0);
    a[0] = 2;
    a[1] = -5;

    expandPoly(c, a, T(3));

    Test.Check(c.size() == 3) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == -30);
    pass = pass && (c[1] == 9);
    pass = pass && (c[2] == 3);
    
    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message()
        << "Degree 3: (x + 4)(x - 7)(x + 1) = -28 -31x - 2x^2 + x^3"
        << endl;

    pass = true;

    a.resize(3, 0);
    a[0] = -4;
    a[1] = 7;
    a[2] = -1;
    
    expandPoly(c, a, T(1));

    Test.Check(c.size() == 4) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == -28);
    pass = pass && (c[1] == -31);
    pass = pass && (c[2] == -2);
    pass = pass && (c[3] == 1);
    
    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message()
<< "Degree 4: -(x - 4)(x - 3)(x - 2)(x - 1) = -24 + 50x - 35x^2 + 10x^3 - x^4"
<< endl;

    pass = true;

    a.resize(4, 0);
    a[0] = 4;
    a[1] = 3;
    a[2] = 2;
    a[3] = 1;

    expandPoly(c, a, T(-1));

    Test.Check(c.size() == 5) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == -24);
    pass = pass && (c[1] == 50);
    pass = pass && (c[2] == -35);
    pass = pass && (c[3] == 10);
    pass = pass && (c[4] == -1);
    
    Test.Check(pass) << "Coefficients are correct" << endl;
}

template<class T>
void TestExpandPolyC()
{
    bool pass = true;
    valarray<complex<T> > a;
    valarray<T> c;

    Test.Message() << "Degree 2 test: -2(x - 3i)(x + 3i) = -18 + 0 x - 2x^2"
                   << endl;

    a.resize(1, 0);
    a[0] = complex<T>(0, 3);

    expandPoly(c, a, T(-2));

    Test.Check(c.size() == 3) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == -18);
    pass = pass && (c[1] == 0);
    pass = pass && (c[2] == -2);

    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message()
        << "Degree 2 test: (x - (2 + 3i))(x - (2 - 3i)) = 13 - 4 x + x^2"
        << endl;

    pass = true;

    a.resize(1, 0);
    a[0] = complex<T>(2, 3);

    expandPoly(c, a, T(1));

    Test.Check(c.size() == 3) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (std::abs(c[0] - 13) < TOL);
    pass = pass && (std::abs(c[1] - (-4)) < TOL);
    pass = pass && (std::abs(c[2] - 1) < TOL);

    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message()
        << "Degree 4 test: (x - (2 + 3i))(x - (2 - 3i))(x - (-1 + i))(x - (-1 - i)) = 26 + 18 x + 7 x^2 - 2 x^3 + x^4"
        << endl;

    pass = true;

    a.resize(2, 0);
    a[0] = complex<T>(2, 3);
    a[1] = complex<T>(-1, 1);

    expandPoly(c, a, T(1));

    Test.Check(c.size() == 5) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (abs(c[0] - 26) < TOL);
    pass = pass && (abs(c[1] - 18) < TOL);
    pass = pass && (abs(c[2] - 7) < TOL);
    pass = pass && (abs(c[3] - (-2)) < TOL);
    pass = pass && (abs(c[4] - 1) < TOL);

    Test.Check(pass) << "Coefficients are correct" << endl;

}

template<class T>
void TestExpandPolyMixed()
{
    bool pass = true;
    valarray<complex<T> > a;
    valarray<T> c;

    Test.Message()
        << "Degree 3 test: (x - 3i)(x + 3i)(x - 2) = -18 + 9 x + -2 x^2 + x^3"
        << endl;

    a.resize(2, 0);
    a[0] = complex<T>(0, 3);
    a[1] = 2;

    expandPoly(c, a, T(1));

    Test.Check(c.size() == 4) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (c[0] == -18);
    pass = pass && (c[1] == 9);
    pass = pass && (c[2] == -2);
    pass = pass && (c[3] == 1);

    Test.Check(pass) << "Coefficients are correct" << endl;

    Test.Message()
        << "Degree 5 test: (x - (2 - 3i))(x - (2 + 3i))(x + 2)(x - (-1 - i))(x - (-1 + i)) = 52 + 62 x + 32 x^2 + 3 x^3 + 0 x^4 + x^5"
        << endl;

    a.resize(3, 0);
    a[0] = complex<T>(2, -3);
    a[1] = -2;
    a[2] = complex<T>(-1, -1);

    expandPoly(c, a, T(1));

    Test.Check(c.size() == 6) << "Poly has correct number of coefficients"
                              << endl;

    pass = pass && (abs(c[0] - 52) < TOL);
    pass = pass && (abs(c[1] - 62) < TOL);
    pass = pass && (abs(c[2] - 32) < TOL);
    pass = pass && (abs(c[3] - 3) < TOL);
    pass = pass && (abs(c[4] - 0) < TOL);
    pass = pass && (abs(c[5] - 1) < TOL);

    Test.Check(pass) << "Coefficients are correct" << endl;

}

template<class T>
void TestRoots(const size_t& n)
{
    // even, +/- 1 are roots
    {
        Test.Message() << "Even roots of unity" << endl;

        const size_t m = 2*n;
        
        valarray<complex<T> > r(n + 1);
        valarray<T> c;

        r[0] = 1;
        
        for (size_t k = 1; k < n; ++k)
        {
            r[k] = complex<T>(cos((LDAS_PI*k)/n), sin((LDAS_PI*k)/n));
        }
        
        r[n] = -1;

        expandPoly(c, r, T(1));
        
        Test.Check(c.size() == (m+1))
            << "Poly has correct number of coefficients"
            << endl;
        bool pass = true;
        
        double diff = abs(c[0] - (-1));
        pass = pass & (diff < TOL);
        
        for (size_t k = 1; k < c.size() - 1; ++k)
        {
          diff = abs(c[k]);
          pass = pass & (diff < TOL);
        }

        diff = abs(c[c.size() - 1] - 1);
        pass = pass & (diff < TOL);

        Test.Check(pass) << "Coefficients are correct" << endl;
    }

    // odd, +1 is only real root
    {
        Test.Message() << "Odd roots of unity" << endl;

        const size_t m = 2*n + 1;
        
        valarray<complex<T> > r(n + 1);
        valarray<T> c;

        r[0] = 1;
        
        for (size_t k = 1; k <= n; ++k)
        {
            r[k] = complex<T>(cos((LDAS_TWOPI*k)/(2*n + 1)),
                              sin((LDAS_TWOPI*k)/(2*n + 1)));
        }
        
        expandPoly(c, r, T(1));
        
        Test.Check(c.size() == (m+1))
            << "Poly has correct number of coefficients"
            << endl;
        bool pass = true;
        
        const double TOL = 1.0e-06;
        double diff = abs(c[0] - (-1));
        pass = pass & (diff < TOL);
        
        for (size_t k = 1; k < c.size() - 1; ++k)
        {
          diff = abs(c[k]);
          pass = pass & (diff < TOL);
        }

        diff = abs(c[c.size() - 1] - 1);
        pass = pass & (diff < TOL);

        Test.Check(pass) << "Coefficients are correct" << endl;
    }
    
}

int main(int ArgC, char** ArgV)
{
    Test.Init(ArgC, ArgV);

    try {
        Test.Message() << "Exceptions" << endl;
        TestExceptions();

        Test.Message() << "expandPoly<T>" << endl;
        TestExpandPolyR<float>();
        TestExpandPolyR<double>();

        TestExpandPolyC<float>();
        TestExpandPolyC<double>();

        TestExpandPolyMixed<float>();
        TestExpandPolyMixed<double>();
        
        Test.Message() << "Roots of unity" << endl;
        TestRoots<double>(8);
    }
    catch (const std::exception& e) 
    {
	Test.Check(false) << e.what() << std::endl;
    }

    Test.Exit();

    return 0;
}
