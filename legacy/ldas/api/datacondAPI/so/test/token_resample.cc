#include "datacondAPI/config.h"

#include <filters/LDASConstants.hh>

#include "general/unittest.h"	// Needed for doing "make check"

#include "token.hh"		// Common header for token testing programs.
#include "Resample.hh"		// Needed for computing resample results.
#include "ScalarUDT.hh"		// Needed for converting CallChain results.
#include "TimeSeries.hh"	// Needed for converting CallChain results.

using namespace std;   
using namespace datacondAPI;

General::UnitTest	Test;	// Class supporting testing

// true if data values and time-series meta-data are the same.
// Names may differ.
template<class T>
bool AreSimilar(const TimeSeries<T>& x, const TimeSeries<T>& y)
{
    if (x.size() != y.size())
    {
        return false;
    }

    for (size_t k = 0; k < x.size(); ++k)
    {
        if (x[k] != y[k])
        {
            return false;
        }
    }

    if (x.GetStartTime() != y.GetStartTime())
    {
        return false;
    }

    if (x.GetSampleRate() != y.GetSampleRate())
    {
        return false;
    }

    if (x.GetBaseFrequency() != y.GetBaseFrequency())
    {
        return false;
    }

    if (x.GetPhase() != y.GetPhase())
    {
        return false;
    }

    return true;
}

//-----------------------------------------------------------------------
// This template functions is a pattern for testing the functionality
//   of resample.
//-----------------------------------------------------------------------

template<class T>
void resample_type(const std::string& type_name, const std::string& array_name)
{
  Test.Message() << "Resample<" << type_name << ">: start" << endl;

  try
  {
    // call chain version

    CallChain cmds;
    Parameters args;
    
    cmds.Reset();

    Test.Message() << "...appending classes...\n";    

    cmds.AppendCallFunction("integer", args.set(1, "4"), "N");
    cmds.AppendCallFunction("double", args.set(1, "1.0"), "dbase");
    cmds.AppendCallFunction("double", args.set(1, "1.0"), "sbase"); // no native float
    cmds.AppendCallFunction("scomplex", args.set(2, "1.0", "0.0"), "cbase");
    cmds.AppendCallFunction("dcomplex", args.set(2, "1.0", "0.0"), "zbase");
    cmds.AppendCallFunction("integer", args.set(1, "1"), "p");
    cmds.AppendCallFunction("integer", args.set(1, "2"), "q");
    if (array_name == "svalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "sbase", "N"), "x");
    }
    else if (array_name == "dvalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "dbase", "N"), "x");
    }
    else if (array_name == "cvalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "cbase", "N"), "x");
    }
    else if (array_name == "zvalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "zbase", "N"), "x");
    }
    cmds.AppendCallFunction("double", args.set(1, "1.0"), "z2"); // wrong type on purpose

    Test.Message() << "...appending functions...\n";        

    // y1 = resample(x, p, q) 
    cmds.AppendCallFunction("resample", args.set(3, "x", "p", "q"), "y1");

    // y2 = resample(x, p, q, z) // no state 
    cmds.AppendCallFunction("resample", args.set(4, "x", "p", "q", "z"), "y2");

    // y3 = resample(x, p, q, z) // overwrite state
    cmds.AppendCallFunction("resample", args.set(4, "x", "p", "q", "z"), "y3");

    // y4 = resample(x, z) // use state
    cmds.AppendCallFunction("resample", args.set(2, "x", "z"), "y4"); 

    // delay = getResampleDelay(z)
    cmds.AppendCallFunction("getResampleDelay", args.set(1, "z"), "delay"); 

    cmds.AppendIntermediateResult("y4", "result", "Final Result", "" );

    //-----------------------------------------------------------------------------------------
    // NEED TO TEST N AND BETA
    //----------------------------------------------------------------------------------------

    // execute

    Test.Message() << "...executing...\n";

    cmds.Execute();

    Test.Message() << "...generating...\n";
    
    // c++ version

    int cpp_p = 1;
    int cpp_q = 2;
    const datacondAPI::Sequence<T> cpp_x(1.0, 4);

    datacondAPI::Sequence<T> cpp_y123;
    datacondAPI::Sequence<T> cpp_y4;
    datacondAPI::Sequence<T> cpp_z;

    datacondAPI::Resample cpp_resample = datacondAPI::ResampleState(cpp_p, cpp_q);
    cpp_resample.apply(cpp_y123, cpp_x);
    cpp_resample.apply(cpp_y4, cpp_x);

    Test.Message() << "...checking...\n";

    check_valarray(cpp_y123, cmds.GetSymbol("y1"), "resample results (y1)");
    check_valarray(cpp_y123, cmds.GetSymbol("y2"), "resample results (y2)");
    check_valarray(cpp_y123, cmds.GetSymbol("y3"), "resample results (y3)");
    check_valarray(cpp_y4, cmds.GetSymbol("y4"), "resample results (y4)");

    check_value(cpp_resample.getDelay(), *cmds.GetSymbol("delay"),
                "resample delay");

  }
  CATCH(Test);

  Test.Message() << "Resample<" << type_name << ">: end" << endl;
}

template<class T>
void resample2Test1(const int oldSrate, const int newSrate)
{
  const double n = 20;
  const double beta = 10;
  
  const size_t sz = 3*oldSrate;
  TimeSeries<T> ts;
  ts.resize(sz);
  for (size_t k = 0; k < ts.size(); ++k)
  {
      ts[k] = sin(LDAS_TWOPI*0.1*k);
  }
  ts.SetSampleRate(oldSrate);
  ts.SetStartTime(General::GPSTime(6000000, 0));

  udt* tsUdt = 0;
  const TimeSeries<T>* tsCheck = 0;

  // call chain version
  
  CallChain cmds;
  Parameters args;
  
  cmds.Reset();
  
  TimeSeries<T>* const tsPtr = ts.Clone();
  Scalar<int>* const nPtr = new Scalar<int>(static_cast<int>(n));
  Scalar<double>* const betaPtr = new Scalar<double>(beta);
  Scalar<double>* const sratePtr = new Scalar<double>(double(newSrate));

  cmds.ResetOrAddSymbol("ts", tsPtr);
  cmds.ResetOrAddSymbol("n", nPtr);
  cmds.ResetOrAddSymbol("beta", betaPtr);
  cmds.ResetOrAddSymbol("srate", sratePtr);
  
  // 2 params
  // y0 = resample2(ts, srate);
  //
  // 3 params
  // y1 = resample2(ts, srate, n);
  // y2 = resample2(ts, srate, z);
  //
  // 4 params
  // y3 = resample2(ts, srate, n, beta);
  // y4 = resample2(ts, srate, n, z);
  //
  // 5 params
  // y5 = resample2(ts, srate, n, beta, z);
  //
  // Use a state
  // y6 = resample2(ts, z);

  cmds.AppendCallFunction("resample2",
                          args.set(2, "ts", "srate"),
                          "y0");

  cmds.AppendCallFunction("resample2",
                          args.set(3, "ts", "srate", "n"),
                          "y1");
  cmds.AppendCallFunction("resample2",
                          args.set(3, "ts", "srate", "z"),
                          "y2");

  cmds.AppendCallFunction("resample2",
                          args.set(4, "ts", "srate", "n", "beta"),
                          "y3");
  cmds.AppendCallFunction("resample2",
                          args.set(4, "ts", "srate", "n", "z"),
                          "y4");

  cmds.AppendCallFunction("resample2",
                          args.set(5, "ts", "srate", "n", "beta", "z"),
                          "y5");

  cmds.AppendCallFunction("resample2",
                          args.set(2, "ts", "z"),
                          "y6");

  cmds.AppendIntermediateResult("y6", "result", "Final Result", "");

  cmds.Execute();

  const TimeSeries<T>& y0
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y0"));
  Resample r(newSrate, oldSrate);
  r.apply(tsUdt, ts);
  tsCheck = dynamic_cast<const TimeSeries<T>*>(tsUdt);
  Test.Check(AreSimilar(y0, *tsCheck)) << "Result is correct" << endl;

  const TimeSeries<T>& y1
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y1"));
  r = Resample(newSrate, oldSrate, static_cast<int>(n));
  r.apply(tsUdt, ts);
  tsCheck = dynamic_cast<const TimeSeries<T>*>(tsUdt);
  Test.Check(AreSimilar(y1, *tsCheck)) << "Result is correct" << endl;

  const TimeSeries<T>& y3
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y3"));
  r = Resample(newSrate, oldSrate, static_cast<int>(n), beta);
  r.apply(tsUdt, ts);
  tsCheck = dynamic_cast<const TimeSeries<T>*>(tsUdt);
  Test.Check(AreSimilar(y3, *tsCheck)) << "Result is correct" << endl;

  const TimeSeries<T>& y6
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y6"));
  // Use the internal state from the last call!
  r.apply(tsUdt, ts);
  tsCheck = dynamic_cast<const TimeSeries<T>*>(tsUdt);
  Test.Check(AreSimilar(y6, *tsCheck)) << "Result is correct" << endl;

  Test.Message() << "resample2() end" << endl;
}

template<class T>
void resample2Test2()
{
  const int oldSrate = 8192;
  const double n = 25;
  const double beta = 15;
  
  const size_t sz = 2*oldSrate;
  TimeSeries<T> ts;
  ts.resize(sz);
  for (size_t k = 0; k < ts.size(); ++k)
  {
      ts[k] = sin(LDAS_TWOPI*0.1*k);
  }
  ts.SetSampleRate(oldSrate);
  ts.SetStartTime(General::GPSTime(6000000, 0));

  // call chain version
  
  CallChain cmds;
  Parameters args;
  
  cmds.Reset();
  
  TimeSeries<T>* const tsPtr = ts.Clone();
  Scalar<int>* const nPtr = new Scalar<int>(static_cast<const int>(n));
  Scalar<double>* const betaPtr = new Scalar<double>(beta);
  Scalar<double>* const sratePtr = new Scalar<double>(double(oldSrate));

  cmds.ResetOrAddSymbol("ts", tsPtr);
  cmds.ResetOrAddSymbol("n", nPtr);
  cmds.ResetOrAddSymbol("beta", betaPtr);
  cmds.ResetOrAddSymbol("srate", sratePtr);
  
  // 2 params
  // y0 = resample2(ts, srate);
  //
  // 3 params
  // y1 = resample2(ts, srate, n);
  // y2 = resample2(ts, srate, z);
  //
  // 4 params
  // y3 = resample2(ts, srate, n, beta);
  // y4 = resample2(ts, srate, n, z);
  //
  // 5 params
  // y5 = resample2(ts, srate, n, beta, z);
  //
  // Use a state
  // y6 = resample2(ts, z);

  cmds.AppendCallFunction("resample2",
                          args.set(2, "ts", "srate"),
                          "y0");

  cmds.AppendCallFunction("resample2",
                          args.set(3, "ts", "srate", "n"),
                          "y1");
  cmds.AppendCallFunction("resample2",
                          args.set(3, "ts", "srate", "z"),
                          "y2");

  cmds.AppendCallFunction("resample2",
                          args.set(4, "ts", "srate", "n", "beta"),
                          "y3");
  cmds.AppendCallFunction("resample2",
                          args.set(4, "ts", "srate", "n", "z"),
                          "y4");

  cmds.AppendCallFunction("resample2",
                          args.set(5, "ts", "srate", "n", "beta", "z"),
                          "y5");

  cmds.AppendCallFunction("resample2",
                          args.set(2, "ts", "z"),
                          "y6");

  cmds.AppendIntermediateResult("y6", "result", "Final Result", "");

  cmds.Execute();

  const TimeSeries<T>& y0
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y0"));
  Test.Check(AreSimilar(y0, ts)) << "Result is correct" << endl;

  const TimeSeries<T>& y1
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y1"));
  Test.Check(AreSimilar(y1, ts)) << "Result is correct" << endl;

  const TimeSeries<T>& y3
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y3"));
  Test.Check(AreSimilar(y3, ts)) << "Result is correct" << endl;

  const TimeSeries<T>& y6
      = dynamic_cast<const TimeSeries<T>&>(*cmds.GetSymbol("y6"));
  Test.Check(AreSimilar(y6, ts)) << "Result is correct" << endl;

  Test.Message() << "resample2() end" << endl;
}

//-----------------------------------------------------------------------
// Main
//-----------------------------------------------------------------------
int
main(int ArgC, char** ArgV)
{
  //---------------------------------------------------------------------
  // Initialize UnitTest class
  //---------------------------------------------------------------------

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  try
  {
    //-------------------------------------------------------------------
    // Perform tests
    //-------------------------------------------------------------------

    resample_type<double>("double", "dvalarray");
    resample_type<float>("float", "svalarray");
    resample_type<std::complex<float> >("complex<float>", "cvalarray");
    resample_type<std::complex<double> >("complex<double>", "zvalarray");

    resample2Test1<float>(150, 150);
    resample2Test1<float>(16384, 2048);
    resample2Test1<float>(2048, 16384);
    resample2Test1<float>(11, 12);
    resample2Test1<float>(12, 11);
    resample2Test1<double>(16384, 1024);
    resample2Test1<double>(1024, 16384);
    resample2Test1<complex<float> >(2000, 100);
    resample2Test1<complex<float> >(100, 2000);
    resample2Test1<complex<double> >(25, 5);
    resample2Test1<complex<double> >(5, 25);

    resample2Test2<float>();
    resample2Test2<double>();
    resample2Test2<complex<float> >();
    resample2Test2<complex<double> >();
  }

  //---------------------------------------------------------------------
  // Display the result
  //---------------------------------------------------------------------

  CATCH(Test);

  //---------------------------------------------------------------------
  // Terminate program with the appropriate exit status.
  //---------------------------------------------------------------------

  Test.Exit();
}
