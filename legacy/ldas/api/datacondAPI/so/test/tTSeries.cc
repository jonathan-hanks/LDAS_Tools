#include "datacondAPI/config.h"

#include <cstdlib>
#include <sstream>

#include <general/unittest.h>

#include "CallChain.hh"
#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "TSeriesFunction.hh"

// the following tests are performed:
// test assignment of <float> data
// test assignment of <double> data
// test assignment of <complex<float> > data
// test assignment of <complex<double> > data
// each test includes test of assignment of SampleRate

using namespace datacondAPI;

General::UnitTest Test;

#define NN 10		// number of data samples

void testTypeInfo( )
{
  Test.Message( ) << "Ensure that datacondAPI does not prefix data types"
		  << std::endl;
  std::ostringstream	oss;

  TheTypeInfoTable().DumpTable( oss, TypeInfoTable_type::VALUE );

  std::istringstream	iss( oss.str() );

  char line[256];
  static char s[] = "datacondAPI::";

  while ( iss.getline( line, sizeof(line), '\n' ) )
  {
    Test.Check( strncmp(line, s, sizeof(s)-1 ) )
      << "Checking for leading namespace: " << line << std::endl;
  }
}

template<class T>
bool testData(void)
{
 bool pass = true;
 double dr = 1024.;

 double buf1[NN] = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};
 double buf2[NN] = {0.0, 1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9};

 try {

    CallChain chain;
    const char* par[4]={"input", "rate", NULL}; // list of parameters

    Sequence<T> in(0.,NN); // first parameter is sequence initialized by zeroes

// fill different types of Sequence by non-zero data
    if ( udt::IsA<Sequence<std::complex<float> > >(in) )
    {
      for (unsigned int i=0; i<NN; i++) 
        (udt::Cast<Sequence<std::complex<float> > >(in))[i] 
         = std::complex<float>( buf1[i], buf2[i] );
    }

    if ( udt::IsA<Sequence<std::complex<double> > >(in) )
    {
      for (unsigned int i=0; i<NN; i++) 
        (udt::Cast<Sequence<std::complex<double> > >(in))[i]
         = std::complex<double>( buf1[i], buf2[i] );
    }

    if ( udt::IsA<Sequence<float> >(in) )
    {
      for (unsigned int i=0; i<NN; i++) 
        (udt::Cast<Sequence<float> >(in))[i] = (float) buf1[i];
    }

    if ( udt::IsA<Sequence<double> >(in) )
    {
      for (unsigned int i=0; i<NN; i++)
        (udt::Cast<Sequence<double> >(in))[i] = buf1[i];
    }

    Scalar<double> Rate(dr);		// assign data rate to second parameter

    // Need to create copies of the input data because once AddSymbol
    // has been called on an object it's the CallChains responsibility
    // to delete it
    CallChain::Symbol* input = in.Clone();
    CallChain::Symbol* rate = Rate.Clone();

    // set name and reference for 1st parameter
    chain.AddSymbol("input", input);
    // set name and reference for 2nd parameter 
    chain.AddSymbol("rate", rate);

    // Don't need to do anything for the result to be created, it is
    // created automatically by the LHS of the assignment
    chain.AppendCallFunction("tseries",  par, "result");

    chain.AppendIntermediateResult("result", "result", "Final Result", "" );

    chain.Execute();

// Print results in ASCII 
/*
   cout << "Results:" << endl;
   ILwd::LdasContainer* r;

   if ((r = chain.GetResults()))
   {
     r->write(cout, ILwd::ASCII);
     cout << endl;
   }
*/
    udt* out=chain.GetSymbol("result");	// get pointer to output

    if ( udt::IsA<TimeSeries<T> >(*out) )
    {
      TimeSeries<T>* ts =
          &(udt::Cast<TimeSeries<T> >(*out));
      if ( dr != ts->GetSampleRate() ) pass = false;
      if ( NN != ts->size() )  pass = false;

// compare data samples
      for (unsigned int i=0; i<NN; i++) 
      {
        if ( (*ts)[i] != in[i] )
          pass = false;
        }
    }
    else pass = false;

  } // end of 'try'

  catch(CallChain::Exception& x)
  {
    Test.Check( false ) << "Caught a CallChain exception: " <<x.what()<< std::endl;
    pass = false;
  }

  catch (std::invalid_argument& x)
  {
      Test.Message() << x.what() << std::endl;
      pass = false;
  }

  catch (std::exception& x)
  {
      Test.Check( false ) << x.what() << std::endl;
      pass = false;
  }

  catch(...)
  {
    Test.Check( false ) << "Caught an Unknown exception." << std::endl;
    pass = false;
  }

  return pass;
}

int main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  Test.Message() << "$Id: tTSeries.cc,v 1.7 2005/12/01 22:55:03 emaros Exp $"
		 << std::endl << std::endl;

  testTypeInfo( );

  Test.Check(testData<float>())
  << "(test if data <float> assigned properly)" << std::endl;

  Test.Check(testData<double>())
  << "(test if data <double> assigned properly)" << std::endl;

  Test.Check(testData<std::complex<float> >())
  << "(test if data <std::complex<float> > assigned properly)" << std::endl;

  Test.Check(testData<std::complex<double> >()) 
  << "(test if data <std::complex<double> > assigned properly)" << std::endl;

  // all done!!
  Test.Exit();
}

