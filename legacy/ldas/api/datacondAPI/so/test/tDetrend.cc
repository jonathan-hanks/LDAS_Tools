#include "datacondAPI/config.h"

#include <math.h>

#include "general/unittest.h"

#include "Detrend.hh"
#include "TimeSeries.hh"

General::UnitTest Test;

using namespace datacondAPI;
using General::GPSTime;

template< class T >
bool
within_tolerance( T Value );

template< >
inline bool
within_tolerance<float>( float Value )
{
  const static float	tolerance = 1.0e-1;

  return ( ( Value < tolerance ) &&
	   ( Value > -tolerance ) );
}

template< >
inline bool
within_tolerance<double>( double Value )
{
  const static double	tolerance = 1.0e-1;

  return ( ( Value < tolerance ) &&
	   ( Value > -tolerance ) );
}

template< >
inline bool
within_tolerance< std::complex< float> >( std::complex< float > Value )
{
  const static float	tolerance( 1.0e-1 );

#if 1
  return( std::abs( Value ) < tolerance );
#else /* 0 */
  return ( ( Value.real( ) < tolerance ) &&
	   ( Value.real( ) > -tolerance ) );
#endif /* 0 */
}

template< >
inline bool
within_tolerance< std::complex< double > >( std::complex< double > Value )
{
  const static double	tolerance( 1.0e-1 );

#if 1
  return( std::abs( Value ) < tolerance );
#else /* 0 */
  return ( ( Value.real( ) < tolerance ) &&
	   ( Value.real( ) > -tolerance ) );
#endif /* 0 */
}

// Verify that contruction/destruction does not lead to memory leaks
// or crashes
void testConstruction()
{
    {
	Detrend* p = new Detrend(Detrend::mean);
    
	Test.Check(true) << "Construction" << std::endl;

	delete p;
	
	Test.Check(true) << "Destruction" << std::endl;
    }

    {
	Detrend* p = new Detrend(Detrend::linear);
    
	Test.Check(true) << "Construction" << std::endl;

	delete p;
	
	Test.Check(true) << "Destruction" << std::endl;
    }
}

// In place
template<class T>
void testMeanDetrend1(TimeSeries<T>& tseries)
{
    const T value = 1;
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;

    tseries.CalcTransitions(trans);
    tseries = value;
    
    Detrend detrend(Detrend::mean);

    detrend.apply(tseries, tseries);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == value);
	}
	++i;
    }

    Test.Check(pass) << "Gaps have not been changed" << std::endl;

    // Now check the complement
    tseries.CalcTransitionsOfData(trans);
    pass = true;

    i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && ( within_tolerance( tseries[j] ) );
	}
	++i;
    }

    Test.Check(pass) << "Non-gaps were detrended" << std::endl;
}

#if 0
// Non-null UDT
template<class T>
void testGapFillZero2(const TimeSeries<T>& tseriesInConst)
{
    const T value = 1;
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;
    TimeSeries<T> tseriesIn(tseriesInConst);
    TimeSeries<T> tseries;
    udt* tseries_p = &tseries;

    tseriesIn.CalcTransitions(trans);
    tseriesIn = value;
    
    GapFill fill(GapFill::ZERO);

    fill.apply(tseries_p, tseriesIn);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == 0.0);
	    tseries[j] = value;
	}
	++i;
    }

    Test.Check(pass) << "Gaps have been filled with correct value" << std::endl;

    // Now check the complement
    pass = true;
    tseries -= value;
    for (size_t j = 0; j < tseries.size(); ++j)
    {
	pass = pass && (tseries[j] == 0.0);
    }

    Test.Check(pass) << "No non-gap data was overwritten" << std::endl;
}

// Null UDT
template<class T>
void testGapFillZero3(const TimeSeries<T>& tseriesInConst)
{
    TimeSeries<T> tseriesIn(tseriesInConst);
    const T value = 1;
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;
    udt* tseries_p = 0;

    tseriesIn.CalcTransitions(trans);
    tseriesIn = value;
    
    GapFill fill(GapFill::ZERO);

    fill.apply(tseries_p, tseriesIn);

    TimeSeries<T>& tseries = dynamic_cast<TimeSeries<T>& >(*tseries_p);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == 0.0);
	    tseries[j] = value;
	}
	++i;
    }

    Test.Check(pass) << "Gaps have been filled with correct value" << std::endl;

    // Now check the complement
    pass = true;
    tseries -= value;
    for (size_t j = 0; j < tseries.size(); ++j)
    {
	pass = pass && (tseries[j] == 0.0);
    }

    delete tseries_p;

    Test.Check(pass) << "No non-gap data was overwritten" << std::endl;
}
#endif

template<class T>
void testMeanDetrend()
{
    const size_t n = 4096;
    const double srate = 2048.0; 
    const GPSTime startTime(600000, 0);
    const float value = 0.0;
    const Sequence<T> s(value, n);

    Test.Message() << "-- Test mean detrend method" << std::endl;

    {
	Test.Message() << "-- Test detrend - default case" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	
	testMeanDetrend1(tseries);
    }

#if 0 /* DEBUG: to reduce noise */
    {
	Test.Message() << "-- Test fill - one gap at the beginning" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 5.1*delta_t;
	const GPSTime start = startTime;
	const GPSTime stop = start + dt;

	tseries.GapAppendGapRange(start, stop);

	testMeanDetrend1(tseries);
    }

    {
	Test.Message() << "-- Test one gap at the end" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 4.7*delta_t;
	const GPSTime start = startTime + dt;
	const GPSTime stop = tseries.GetEndTime() + delta_t/2;

	tseries.GapAppendGapRange(start, stop);

	testMeanDetrend1(tseries);
    }

    {
	Test.Message() << "-- Test one gap in the middle" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dtStart = 4.5*delta_t;
	const double dtEnd = 6.3*delta_t;
	const GPSTime start = startTime + dtStart;
	const GPSTime stop = tseries.GetEndTime() - dtEnd;
	
	tseries.GapAppendGapRange(start, stop);

	testMeanDetrend1(tseries);
    }
#endif /* 0 */
}


template<class T>
void testLinearDetrend1(TimeSeries<T>& tseries,
			double a,
			double b)
{
    bool pass = true;
    GapMetaData<GPSTime>::Transitions_type trans;

    tseries.CalcTransitions(trans);
    
    // Initialise with a ramp
    for (size_t k = 0; k < tseries.size(); ++k)
    {
	tseries[k] = a + b*k;
    }
    
    Detrend detrend(Detrend::linear);

    detrend.apply(tseries, tseries);

    GapMetaData<GPSTime>::Transitions_type::const_iterator i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && (tseries[j] == T(a + b*j));
	}
	++i;
    }

    Test.Check(pass) << "Gaps have not been changed" << std::endl;

    // Now check the complement
    tseries.CalcTransitionsOfData(trans);
    pass = true;

    i = trans.begin();
    while (i != trans.end())
    {
	for (size_t j = (*i).start; j < (*i).stop; ++j)
	{
	    pass = pass && ( within_tolerance( tseries[j] ) );
	}
	++i;
    }

    Test.Check(pass) << "Non-gaps were detrended" << std::endl;
}

template<class T>
void testLinearDetrend()
{
    const size_t n = 4096;
    const double srate = 2048.0; 
    const GPSTime startTime(600000, 0);
    const float value = 0.0;
    const Sequence<T> s(value, n);

    Test.Message() << "-- Test linear detrend method" << std::endl;

    {
	Test.Message() << "-- Test detrend - default case" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	
#if 0
	testLinearDetrend1(tseries, 2, 3);
	testLinearDetrend1(tseries, 3, -2);
	testLinearDetrend1(tseries, 4, 0);
#endif
	testLinearDetrend1(tseries, 0, -4);
    }

    {
	Test.Message() << "-- Test fill - one gap at the beginning" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 5.1*delta_t;
	const GPSTime start = startTime;
	const GPSTime stop = start + dt;

	tseries.GapAppendGapRange(start, stop);

	testLinearDetrend1(tseries, 2, 3);
	testLinearDetrend1(tseries, 3, -2);
	testLinearDetrend1(tseries, 4, 0);
	testLinearDetrend1(tseries, 0, -4);
    }

    {
	Test.Message() << "-- Test one gap at the end" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	const double delta_t = tseries.GetStepSize();
	const double dt = 4.7*delta_t;
	const GPSTime start = startTime + dt;
	const GPSTime stop = tseries.GetEndTime() + delta_t/2;

	tseries.GapAppendGapRange(start, stop);

	testLinearDetrend1(tseries, 2, 3);
	testLinearDetrend1(tseries, 3, -2);
	testLinearDetrend1(tseries, 4, 0);
	testLinearDetrend1(tseries, 0, -4);
    }

    {
	Test.Message() << "-- Test one gap in the middle" << std::endl;

	TimeSeries<T> tseries(s, srate, startTime);
	GapMetaData<GPSTime>::Transitions_type expTrans;

	const double delta_t = tseries.GetStepSize();

	const double dtStart = 4.5*delta_t;
	const double dtEnd = 6.3*delta_t;
	const GPSTime start = startTime + dtStart;
	const GPSTime stop = tseries.GetEndTime() - dtEnd;
	
	tseries.GapAppendGapRange(start, stop);

	testLinearDetrend1(tseries, 2, 3);
	testLinearDetrend1(tseries, 3, -2);
	testLinearDetrend1(tseries, 4, 0);
	testLinearDetrend1(tseries, 0, -4);
    }
}

void testExceptions()
{
    {
	Test.Message() << "-- Test bad method" << std::endl;

	try
	{
	    Detrend* p = new Detrend((Detrend::DetrendMethod)(Detrend::mean-1));
	    delete p;
	}
	catch(std::invalid_argument& e)
	{
	    Test.Check(true) << "Caught exception: " << e.what() << std::endl;
	}
	catch(std::exception& e)
	{
	    Test.Check(false) << "Caught exception: " << e.what() << std::endl;
	}
	catch(...)
	{
	    Test.Check(false) << "Caught unknown exception" << std::endl;
	}
    }

}

int
main(int argc, char** argv)
{
    Test.Init(argc, argv);

    try {
	Test.Message() << "-- $Id: tDetrend.cc,v 1.9 2005/12/01 22:55:02 emaros Exp $" << std::endl;

	testConstruction();

#if 0 /* DEBUG: Excluding noise */
	testMeanDetrend<float>();
	testMeanDetrend<double>();
	testMeanDetrend<std::complex<float> >();
	testMeanDetrend<std::complex<double> >();

	testLinearDetrend<float>();
	testLinearDetrend<double>();
	testLinearDetrend<std::complex<float> >();
#endif
	testLinearDetrend<std::complex<double> >();

	testExceptions();
    }
    catch(std::exception& e)
    {
	Test.Check(false) << "Unhandled exception: " 
			  << e.what() << std::endl;
    }
    catch(...)
    {
	Test.Check(false) << "Unhandled exception" << std::endl;
    }
    
    Test.Exit();
}
