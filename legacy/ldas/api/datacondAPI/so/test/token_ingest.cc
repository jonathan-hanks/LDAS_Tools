#include "datacondAPI/config.h"

#include <memory>
#include <sstream>

#include "general/autoarray.hh"
#include "general/unordered_map.hh"
#include "general/util.hh"
#include "general/ldasexception.hh"
#include "general/unittest.h"		// Needed for doing "make check"

#include "ilwd/ldasarray.hh"

#include "mime/spectrum.hh"

#include "genericAPI/registry.hh"

#include "token.hh"		// Common header for token testing programs.
#include "TimeSeries.hh"
#include "WelchSpectrumUDT.hh"
#include "WelchCSDSpectrumUDT.hh"

using namespace std;   
using General::unordered_map;  
using General::GPSTime;

General::UnitTest	Test;

typedef enum {
  PROCESS_TABLE_ALL,
  PROCESS_TABLE_SIMPLE,
  PROCESS_TABLE_SIMPLE_NULL,
  PROCESS_TABLE_ZERO,
  SNGL_INSPIRAL_TABLE_SIMPLE_NULL,
  SUMM_SPECTRUM_ALL,
  SUMM_SPECTRUM_MULTIPLE,
  SUMM_SPECTRUM_EMPTY,
  SUMM_SPECTRUM_REAL,
  QC_GOOD,
  QC_EMPTY,
  QC_BINARY,
  FREQUENCY_SIMPLE,
  FREQUENCY_SFT_1,
  FREQUENCY_SFT_2,
  FREQUENCY_STOCASTIC_1,
  MDC_INPUT_1
} ilwd_types_type;

typedef enum {
  RESULT_NONE,	// Don't care what it looks like
  RESULT_DUMP,	// Force the dumping of the ILwd since it will never match this
  RESULT_TABLE_SIMPLE_PUSH_LIGOLW,
  RESULT_SUMM_SPECTRUM_ALL_MPI,
  RESULT_SUMM_SPECTRUM_ALL_LIGOLW,
  RESULT_SUMM_SPECTRUM_ALL_PASS_LIGOLW,
  RESULT_FREQUENCY_SFT_1_WRAPPER,
  RESULT_FREQUENCY_STOCASTIC_1_WRAPPER,
  RESULT_MDC_INPUT_1_GENERIC
} result_s_type;

typedef unordered_map< int, std::string > map_of_names_type;
typedef unordered_map< int, std::string > map_of_ilwds_type;

map_of_names_type	MapOfNames;
map_of_ilwds_type	MapOfILwds;
map_of_ilwds_type	MapOfResultILwds;

#if 0
#define AT() Test.Message() << __FILE__ << " " << __LINE__ << std::flush << std::endl;
#else
#define	AT()
#endif

//-----------------------------------------------------------------------
// Forward declaration of functions
//-----------------------------------------------------------------------

static void init();

//-----------------------------------------------------------------------
// Declaration of templates
//-----------------------------------------------------------------------

template <class T> std::auto_ptr< T >
create( const std::string& Input )
{
  istringstream	is( Input );
  ILwd::Reader 	reader(is);
  std::auto_ptr< ILwd::LdasElement >
    r( ILwd::LdasElement::createElement( reader ) );
  if ( dynamic_cast< T* >( r.get( ) ) != (T*)NULL )
  {
    return std::auto_ptr< T >( dynamic_cast< T* >( r.release( ) ) );
  }
  return std::auto_ptr< T >( NULL );
}

//-----------------------------------------------------------------------
// Read an ilwd string into an memory
//-----------------------------------------------------------------------

std::auto_ptr< ILwd::LdasElement >
read_ilwd_string( const std::string& IlwdString )
{
  std::istringstream	iss( IlwdString );
  ILwd::Reader		reader( iss );

  return std::auto_ptr< ILwd::LdasElement >
    ( ILwd::LdasElement::createElement( reader ) );
}

//-----------------------------------------------------------------------
// Attempt to put an ilwd into the CallChain.
//-----------------------------------------------------------------------
bool
ingest(const size_t Length)
{
  General::AutoArray< COMPLEX_16 > c( new COMPLEX_16[Length] );
  ILwd::LdasArray<COMPLEX_16>	idata(c.get( ), Length);

  idata.setNameString("idata");
  idata.setComment("sample data");
  
  CallChain	cmds;
  
  cmds.Reset();
  cmds.IngestILwd(idata);

  cmds.AppendIntermediateResult( "idata", "idata", "Result", "generic" );

  cmds.Execute();
  
  cmds.GetResults();
  
  Registry::elementRegistry.reset( ); // This removes the results!
  AT();
  return true;
}

void
ingest_sample_data(const char* Filename, const int Count = 1)
{
  try
  {
    std::auto_ptr< const ILwd::LdasElement >
      element( read_ilwd( Filename, "DATADIR" ) );
    CallChain			cmds;
    std::string			symbols;
  
    cmds.Reset();
    symbols = cmds.IngestILwd(*element);

    Test.Message() << "Ingested symbols: " << symbols << std::endl;
#if 0
    cmds.Execute();
    
    cmds.GetResults();
#endif	/* 0 */
  
    std::string::size_type	start(0);
    std::string::size_type	end(symbols.find(' '));
    int				seen_time_series( 0 );
    std::string			time_series_name;
    CallChain::Symbol*		symbol((CallChain::Symbol*)NULL);
    std::vector<std::string>	symbol_names;

    while (1)
    {
      using namespace datacondAPI;

      std::string		symbol_name(symbols.substr(start,
							   (end == std::string::npos)
							   ? end
							   : end - start));
      symbol = cmds.GetSymbol(symbol_name);

      if (udt::IsA<TimeSeries<float> >(*symbol) ||
	  udt::IsA<TimeSeries<double> >(*symbol) ||
	  udt::IsA<TimeSeries<std::complex<float> > >(*symbol) ||
	  udt::IsA<TimeSeries<std::complex<double> > >(*symbol))
      {
	ILwd::LdasContainer* c( dynamic_cast<ILwd::LdasContainer*>
				(symbol->ConvertToIlwd( cmds,
							datacondAPI::udt::TARGET_GENERIC ) ) );

	Test.Message( 5 ) << "Ingested TimeSeries: ";
	c->write( 2, 2, Test.Message( 5, false ), ILwd::ASCII);
	Test.Message( 5, false ) << std::endl;
	delete c;

	time_series_name = symbol_name;
	seen_time_series++;
	symbol_names.push_back( symbol->name() );
      }
      if (end == std::string::npos)
      {
	break;
      }
      start = symbols.find_first_not_of(' ', end);
      end = symbols.find(' ', start);
    }
    Test.Check( seen_time_series == Count )
      << "IngestSampleData: Ingested "
      << seen_time_series
      << " of "
      << Count
      << " timeseries channel(s): "
      << time_series_name;

    for ( std::vector<std::string>::const_iterator s = symbol_names.begin();
	  s != symbol_names.end();
	  s++ )
    {
      Test.Message(false) << " ts:" << *s;
    }

    Test.Message(false) << endl << flush;

    Registry::elementRegistry.reset( ); // This removes the results!

  }
  CATCH(Test);
}

//-----------------------------------------------------------------------
// Attempt to put an ilwd into the CallChain.
//-----------------------------------------------------------------------
bool
ingest_ilwd( const ILwd::LdasElement& Element, std::string Name )
try
{
  CallChain	cmds;
  
  cmds.Reset();
  cmds.IngestILwd(Element);

  cmds.AppendIntermediateResult( Name.c_str(),
				 Name.c_str(),
				 "Result",
				 "generic" );

  cmds.Execute();
  
  cmds.GetResults();
  
  // Registry::elementRegistry.reset(); // This removes the results!
  AT();
  return true;
}
catch( std::exception& e )
{
  Test.Check( false ) << "ingest_ilwd: Caught exception: " << e.what()
		      << std::endl;
  return false;
}
catch( ... )
{
  Test.Check( false ) << "ingest_ilwd: Caught unknown exception"
		      << std::endl;
  return false;   
  
}

//-----------------------------------------------------------------------
// Attempt to put an ilwd into the CallChain.
//-----------------------------------------------------------------------

template<class Type_>
bool
ingest_ilwd_check( const ILwd::LdasElement& Element, std::string Name,
		   datacondAPI::udt::target_type Type =
		     datacondAPI::udt::TARGET_GENERIC,
		   std::string ResultILwd = "" )
try
{
  CallChain	cmds;
  
  AT();
  cmds.Reset();
  AT();
  cmds.IngestILwd(Element);

  AT();
  // cmds.AppendCallFunction("value", args.set(1, Name.c_str()), "d");
  cmds.AppendIntermediateResult( Name.c_str(),
				 "query_results",
				 "This is the result of the action sequence",
				 datacondAPI::udt::GetTargetType( Type ).
				 c_str() );

  AT();
  cmds.Execute();
  
  AT();
  cmds.GetResults();

  AT();
  CallChain::Symbol*		symbol((CallChain::Symbol*)NULL);

  AT();
  symbol = cmds.GetSymbol( Name );
  
  AT();
  Test.Check( datacondAPI::udt::IsA< Type_ >( *symbol ) )
    << "Ingested: " << Name
    << std::endl;

  AT();
  if ( ResultILwd.length() > 0 )
  {
    AT();
    if ( symbol )
    {
      bool			pass;
      AT();
      std::auto_ptr< ILwd::LdasContainer >
	c( dynamic_cast<ILwd::LdasContainer*>
	   ( symbol->ConvertToIlwd( cmds, Type ) ) );
      std::auto_ptr< ILwd::LdasElement >
	re( read_ilwd_string( ResultILwd ) );
      std::auto_ptr< ILwd::LdasContainer > r;

      if ( dynamic_cast< ILwd::LdasContainer* >( re.get( ) ) !=
	   (ILwd::LdasContainer*)NULL )
      {
	r.reset( dynamic_cast< ILwd::LdasContainer* >( re.release( ) ) );
      }

      Test.Check( (pass = ( *c == *r ) ) )
	<< Name << ": Generated expected result: ";
      if ( !pass )
      {
	Test.Message( false ) << std::endl << " Expected: ";
	r->write( 2, 2, Test.Message( false ), ILwd::ASCII );
	Test.Message( false ) << std::endl << " Generated: ";
	c->write( 2, 2, Test.Message( false ), ILwd::ASCII );
	Test.Message( false ) << std::endl;
      }
      Test.Message( false ) << std::endl;
    }
  }

  Registry::elementRegistry.reset(); // This removes the results!
  AT();
  return true;
}
catch( ... )
{
  Registry::elementRegistry.reset( ); // This removes the results!
  throw;
}

bool
ingest_ilwd_symbol_check( const std::string& Name,
			  const ILwd::LdasElement& Element,
			  datacondAPI::udt::target_type Type =
			  datacondAPI::udt::TARGET_GENERIC,
			  std::string ResultILwd = "" )
{
  CallChain			cmds;
  std::string::size_type	pos;
  
  AT();
  cmds.Reset();
  AT();
  std::string symbols( cmds.IngestILwd(Element) );

  while( symbols.length() > 0 )
  {
    AT();
    pos = symbols.find( " " );
    cmds.AppendIntermediateResult( symbols.substr(0, pos ).c_str(),
				   symbols.substr(0, pos ).c_str(),
				   "result_data",
				   datacondAPI::udt::GetTargetType( Type ).
				   c_str() );
    if ( pos != std::string::npos )
    {
      pos = symbols.find_first_not_of( " ", pos );
    }
    symbols.erase( 0, pos );
  }

  AT();

  AT();
  cmds.Execute();
  
  AT();
  ILwd::LdasContainer* c = cmds.GetResults();

  AT();
  if ( ResultILwd.length() > 0 )
  {
    AT();
    bool			pass;
    AT();
    
    std::auto_ptr< ILwd::LdasElement >
      re( read_ilwd_string( ResultILwd ) );
    std::auto_ptr< ILwd::LdasContainer > r;

    if ( dynamic_cast< ILwd::LdasContainer* >( re.get( ) ) !=
	 (ILwd::LdasContainer*)NULL )
    {
      r.reset( dynamic_cast< ILwd::LdasContainer* >( re.release( ) ) );
    }

    Test.Check( (pass = ( *c == *r ) ) )
      << Name << ": Generated expected result: ";
    if ( !pass )
    {
      Test.Message( false ) << std::endl << " Expected: ";
      r->write( 2, 2, Test.Message( false ), ILwd::ASCII );
      Test.Message( false ) << std::endl << " Generated: ";
      c->write( 2, 2, Test.Message( false ), ILwd::ASCII );
      Test.Message( false ) << std::endl;
    }
    Test.Message( false ) << std::endl;
  }

  // Registry::elementRegistry.reset(); // This removes the results!
  AT();
  return true;
}

template<class Type_>
bool
ingest_ilwd( const std::string& IlwdString,
	     datacondAPI::udt::target_type Type =
	       datacondAPI::udt::TARGET_GENERIC,
	     std::string ResultILwd = "",
	     std::string NameString = "" )
{
  try {
    std::auto_ptr< ILwd::LdasElement >	e( read_ilwd_string( IlwdString ) );

    if ( NameString.length() <= 0 )
    {
      NameString = e->getNameString();
    }
    return ingest_ilwd_check<Type_>( *e, NameString, Type, ResultILwd );
  }
  CATCH(Test);
  return false;
}

bool
ingest_ilwd_symbols( const std::string& Name,
		     const std::string& IlwdString,
		     datacondAPI::udt::target_type Type =
		     datacondAPI::udt::TARGET_GENERIC,
		     std::string ResultILwd = "" )
{
  try {
    std::auto_ptr< ILwd::LdasElement >	e( read_ilwd_string( IlwdString ) );

    return ingest_ilwd_symbol_check( Name, *e, Type, ResultILwd );
  }
  CATCH(Test);
  return false;
}

//-----------------------------------------------------------------------
// Attempt to put database data into the CallChain.
//-----------------------------------------------------------------------
void
ingest_database_data( const datacondAPI::udt& Object,
		      const std::string& Type,
		      CallChain::query_type QueryType )
try {
  using namespace datacondAPI;

  ILwd::LdasContainer*	results;
  std::string		symbols;

  CallChain	cmds;

  cmds.Reset();
  AT();
  cmds.SetReturnTarget( "wrapper" );

  AT();
  std::auto_ptr< ILwd::LdasElement >
    expected( Object.ConvertToIlwd( cmds, udt::TARGET_WRAPPER ) );
  AT();
  if ( Test.IsVerbose() )
  {
    ILwd::LdasContainer* c = dynamic_cast<ILwd::LdasContainer*>( expected.get( ) );

    Test.Message() << "expected: ";
    c->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
  }


  AT();
  std::auto_ptr< ILwd::LdasElement >
    ilwd( Object.ConvertToIlwd( cmds, udt::TARGET_METADATA_FINAL_RESULT ) );
  AT();
  if ( Test.IsVerbose() )
  {
    Test.Message() << "ilwd: ";
    ILwd::LdasContainer* c = dynamic_cast<ILwd::LdasContainer*>( ilwd.get( ) );

    c->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
  }

  ILwd::LdasContainer* c = dynamic_cast<ILwd::LdasContainer*>( ilwd.get( ) );

  AT();
  if ( !c )
  {
    AT();
    Test.Check( false ) << "Unable to convert ILwd::LdasElement* to "
			<< "ILwd::ldasContainer* "
			<< __FILE__ << " " << __LINE__
			<< endl;
    AT();
    c->write(2, 2, Test.Message(false), ILwd::ASCII);
    Test.Message(false) << endl;
  }

  AT();
  switch( QueryType )
  {
  case CallChain::QUERY_SPECTRUM:
    cmds.IngestDatabaseData( "spectrum_name", *(c->operator[](1)),
			     CallChain::INTERMEDIATE,
			     QueryType,
			     &symbols );
    break;
  default:
    symbols = cmds.IngestILwd( *(c->operator[](1)), CallChain::INTERMEDIATE );
    break;
  }
  Test.Message() << "Ingested symbols: " << symbols << std::endl;

  AT();
  results = cmds.GetResults();
  if ( ( !results ) || ( results->size() <= 0 ) )
  {
    AT();
    Test.Check( false ) << "No results returned: "
			<< __FILE__ << " " << __LINE__
			<< endl;
    if ( Test.IsVerbose() )
    {
      cmds.Dump();
    }
    AT();
    return;
  }

  AT();
  switch( QueryType )
  {
  case CallChain::QUERY_SPECTRUM:
    cmds.IngestDatabaseData( "spectrum_name", *(c->operator[](1)),
			     CallChain::INTERMEDIATE,
			     QueryType,
			     &symbols );
    break;
  default:
    symbols = cmds.IngestILwd( *(c->operator[](1)), CallChain::INTERMEDIATE );
    break;
  }
  Test.Message() << "Ingested symbols: " << symbols << std::endl;

  AT();
  results = cmds.GetResults();
  if ( ( !results ) || ( results->size() <= 0 ) )
  {
    AT();
    Test.Check( false ) << "No results returned: "
			<< __FILE__ << " " << __LINE__
			<< endl;
    if ( Test.IsVerbose() )
    {
      cmds.Dump();
    }
    AT();
    return;
  }
  AT();
  expected->setName( 0, results->operator[](0)->getName( 0 ), true );
  expected->setComment( results->operator[](0)->getComment() );

  AT();
  Test.Check( *( results->operator[](0) ) == *expected )
    << " Ingestion of database data: " << Type
    << std::endl;

  if ( *( results->operator[](0) ) != *expected )
  {
    c = dynamic_cast<ILwd::LdasContainer*>( expected.get( ) );
    Test.Message( false ) << std::endl << " Expected: ";
    c->write( 2, 2, Test.Message( false ), ILwd::ASCII );
    Test.Message( false ) << std::endl;

    c = dynamic_cast<ILwd::LdasContainer*>( results->operator[](0) );
    Test.Message( false ) << std::endl << " Generated: ";
    c->write( 2, 2, Test.Message( false ), ILwd::ASCII );
    Test.Message( false ) << std::endl;
  }
  AT();
}
CATCH( Test )

void
ingest_result_database_data( const std::string& result_able_string,
			     unsigned int ResultSize = 1 )
{
  std::auto_ptr< ILwd::LdasElement >
    ilwd( ( ILwd::LdasElement* )NULL );

  try {
    AT();
#if __GNUC__ >= 3
    std::istringstream	is( result_able_string );
#else
    std::istringstream	is( result_able_string.c_str(),
			    result_able_string.length() );
#endif
    ILwd::Reader 	reader(is);

    ilwd.reset( ILwd::LdasElement::createElement( reader ) );

    Test.Message() << "DataInput: ";
    dynamic_cast<ILwd::LdasContainer*>(ilwd.get( ))->write( 2, 2, Test.Message(false), ILwd::ASCII );
    Test.Message(false) << std::endl;

    AT();
    CallChain	cmds;

    AT();
    cmds.SetReturnTarget( "wrapper" );
    AT();
    cmds.IngestILwd( *ilwd, CallChain::INTERMEDIATE );
    AT();
    ILwd::LdasContainer* c = cmds.GetResults();
    AT();
    Test.Check( c->size() == ResultSize )
      << " Ingestion of result database data"
      << ": Received " << c->size() << " of " << ResultSize << " results"
      << endl;
    AT();
    if ( c->size() != ResultSize )
    {
      AT();
      if ( Test.IsVerbose() )
      {
	cmds.Dump();
      }
      Test.Message() << "Result set: ";
      c->write(2, 2, Test.Message(false), ILwd::ASCII);
      Test.Message(false) << std::endl;
    }
    Registry::elementRegistry.reset( );
  }
  catch ( const LdasException& e )
  {
    Registry::elementRegistry.reset( );
    Test.Check( false ) << " Ingestion of result database data: exception: "
			<< endl;
    for ( size_t x = 0; x < e.getSize(); x++ )
    {
      Test.Message( false ) << "        "
			    << " lib: " << e.getError( x ).getLibrary()
			    << " msg: " << e.getError( x ).getMessage()
			    << " info: " << e.getError( x ).getInfo()
			    << endl;
    }
  }
  catch ( const std::exception& e )
  {
    Registry::elementRegistry.reset( );
    Test.Check( false ) << " Ingestion of result database data: exception: "
			<< e.what() << endl;
  }
  catch ( ... )
  {
    Registry::elementRegistry.reset( );
    Test.Check( false ) << " Ingestion of result database data: exception: unknown"
			<< endl;
  }
}

void
ingest_table( const std::string& Header,
	      CallChain::query_type QueryType,
	      const std::string& PushPass,
	      const std::string& Target,
	      bool ThrowsException,
	      const std::string& TableString,
	      const std::string& Result )
{
  CallChain	cmds;
  CallChain::ingestion_type	pushpass( ( PushPass == "push" )
					  ? CallChain::SYMBOL
					  : CallChain::INTERMEDIATE );

  try
  {
    // Keep track of what symbols where created
    std::string		symbols;

    cmds.SetReturnTarget( Target );
    
    // Ingest the table into the CallChain
    switch( QueryType )
    {
    case CallChain::QUERY_GENERIC:
    case CallChain::QUERY_NTUPLE:
      {
	// Create the ilwd for ingestion
	AT();
	std::auto_ptr< ILwd::LdasContainer >
	  ilwd( create< ILwd::LdasContainer >( TableString ) );

	AT();
	cmds.IngestDatabaseData( "query", *ilwd, pushpass, QueryType,
				 &symbols );
	break;
      }
    case CallChain::QUERY_SPECTRUM:
      {
	using namespace datacondAPI;
	using namespace DB;

	AT();
	std::auto_ptr< ILwd::LdasContainer >
	  ilwd( create< ILwd::LdasContainer >( TableString ) );

	AT();
	std::auto_ptr< Table >
	  table( DB::Table::MakeTableFromILwd( *ilwd, true ) );

	AT();
	std::vector<udt*>		data;
	std::vector<std::string>	names;

	AT();
	cmds.IngestDatabaseSpectrumData( *table,
					 data,
					 names,
					 "query",
					 CallChain::EXACTLY_ONE,
					 &symbols );
	break;
      }
    }

    if ( pushpass == CallChain::SYMBOL )
    {
      std::string	symbol_name( "query" );

      if ( Test.IsVerbose( ) )
      {
	Test.Message( ) << "Ingested Symbols: " << symbols << std::endl;
      }
      if ( symbols.find( " " ) != std::string::npos )
      {
	symbol_name += "_1";
      }
      cmds.AppendIntermediateResult(symbol_name.c_str(),
				    "query_results",
				    "This is the result of the action sequence",
				    Target.c_str() );
    }
    cmds.Execute();

    ILwd::LdasContainer*	ilwd = cmds.GetResults( );

    if ( Result.length() > 0 )
    {
      ILwd::LdasContainer*	calc( dynamic_cast<ILwd::LdasContainer*>
				      ( ilwd->operator[](0) ) );
      std::auto_ptr< ILwd::LdasContainer >
	expected( create< ILwd::LdasContainer >( Result ) );
      bool			pass( *expected == *calc );

      Test.Check( pass ) << Header << std::endl;
      if ( Test.IsVerbose( ) && !pass )
      {
	Test.Message( ) << "Expected: ";
	expected->write( 2, 2, Test.Message( false ), ILwd::ASCII );
	Test.Message( false ) << std::endl
			<< "Generated: ";
	calc->write( 2, 2, Test.Message( false ), ILwd::ASCII );
	Test.Message( false ) << std::endl;
      }
    }
    Registry::elementRegistry.reset( );
  }
  catch( const CallChain::LogicError& e )
  {
    Registry::elementRegistry.reset( );
    Test.Check( ThrowsException )
      << Header
      << ": Caught exception: " << e.what()
      << std::endl;
    return;
  }
  catch( ... )
  {
    Registry::elementRegistry.reset( );
    Test.Check( ThrowsException )
      << Header
      << ": Caught exception: ";
    try
    {
      throw;
    }
    catch( const std::exception& e )
    {
      Test.Message( false ) << e.what();
    }
    catch( ... )
    {
      Test.Message( false ) << "Unknown exception";
    }
    Test.Message( false ) << std::endl;
    return;
  }
  Registry::elementRegistry.reset( );
  Test.Check( !ThrowsException )
    << Header
    << ": Completed" << std::endl;
}

void
ingest_qc( const std::string& Header,
	   const INT_4S StartSec,
	   const INT_4S StartNanosec,
	   const INT_4S StopSec,
	   const INT_4S StopNanosec,
	   const std::string& PushPass,
	   const std::string& Target,
	   bool ThrowsException,
	   const std::string& TableString,
	   const std::string& Result )
{
  CallChain	cmds;
  CallChain::ingestion_type	pushpass( ( PushPass == "push" )
					  ? CallChain::SYMBOL
					  : CallChain::INTERMEDIATE );

  try
  {
    // Keep track of what symbols where created
    std::string	symbol_name( "qc" );
    INT_4U	sample_rate( 256 );

    cmds.SetReturnTarget( Target );
    
    // Create the ilwd for ingestion
    AT();
    std::auto_ptr< ILwd::LdasContainer >
      ilwd( create< ILwd::LdasContainer >( TableString ) );

    AT();
    cmds.IngestQualityChannel( *ilwd,
			       StartSec, StartNanosec,
			       StopSec, StopNanosec,
			       sample_rate,
			       symbol_name,
			       pushpass );

    AT();
    if ( pushpass == CallChain::SYMBOL )
    {
      cmds.AppendIntermediateResult( symbol_name.c_str(),
				     symbol_name.c_str(),
				     "Result",
				     Target.c_str() );
    }
    cmds.Execute();

    ilwd.reset( cmds.GetResults( ) );

    if ( Result.length() > 0 )
    {
      ILwd::LdasContainer*	calc( dynamic_cast<ILwd::LdasContainer*>
				      ( ilwd->operator[](0) ) );
      std::auto_ptr< ILwd::LdasContainer >
	expected( create< ILwd::LdasContainer >( Result ) );
      bool			pass( *expected == *calc );

      Test.Check( pass ) << Header << std::endl;
      if ( Test.IsVerbose( ) && !pass )
      {
	Test.Message( ) << "Expected: ";
	expected->write( 2, 2, Test.Message( false ), ILwd::ASCII );
	Test.Message( false ) << std::endl
			<< "Generated: ";
	calc->write( 2, 2, Test.Message( false ), ILwd::ASCII );
	Test.Message( false ) << std::endl;
      }
    }
    Registry::elementRegistry.destructObject( ilwd.release( ) );
    Registry::elementRegistry.reset( );
  }
  catch( const CallChain::LogicError& e )
  {
    Test.Check( ThrowsException )
      << Header
      << ": Caught exception: " << e.what()
      << std::endl;
    return;
  }
  catch( ... )
  {
    Test.Check( false )
      << Header
      << ": Caught exception: ";
    try
    {
      throw;
    }
    catch( const std::exception& e )
    {
      Test.Message( false ) << e.what();
    }
    catch( ... )
    {
      Test.Message( false ) << "Unknown exception";
    }
    Test.Message( false ) << std::endl;
    return;
  }
  Test.Check( !ThrowsException )
    << Header
    << ": Completed" << std::endl;
}

int
main(int ArgC, char** ArgV)
{
  using namespace datacondAPI;

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Initialize test data
  //---------------------------------------------------------------------

  init();

  //---------------------------------------------------------------------
  // test data
  //---------------------------------------------------------------------
  const size_t last(20);
  const size_t first(2);

  const unsigned int welch_length(100);
  datacondAPI::WelchSpectrum<double>	welch;
  datacondAPI::WelchCSDSpectrum<float>	welch_csd;
  welch.resize( welch_length );
  welch_csd.resize( welch_length );

  welch.SetFrequencyBase( -0.5 );
  welch_csd.SetFrequencyBase( -0.5 );
  welch.SetFrequencyDelta( 1.0/welch_length );
  welch_csd.SetFrequencyDelta( 1.0/welch_length );
  for ( unsigned int x = 0; x < welch_length; x++ )
  {
    welch[x] = (double)x;
    welch_csd[x] = (float)x;
  }

  welch.SetStartTime( GPSTime( 0, 0 ) );
  welch.SetEndTime( GPSTime( 10, 0 ) );
  welch_csd.SetStartTime( GPSTime( 0, 0 ) );
  welch_csd.SetEndTime( GPSTime( 10, 0 ) );

  welch.SetNameOfChannel("channel");

  welch_csd.SetNameOfChannel1("channel_1");
  welch_csd.SetNameOfChannel2("channel_2");

  Mime::Spectrum		mime_spectrum_info;

  //---------------------------------------------------------------------
  // Simple ingestion of a single sequence of varying length
  //---------------------------------------------------------------------

  for (size_t y = first; y < last; y++)
  {
    bool pass = false;

    try
    {
      AT();
      pass = ingest(y);
    }
    CATCH(Test);
    Test.Check(pass) << "Size = 2^" << y << " (" << ( 1 << y ) << ")"
		     << endl << flush;
  }

  //---------------------------------------------------------------------
  // Ingestion of ADC/Frame data files
  //---------------------------------------------------------------------

  static struct {
    const char* s_filename;
    const int	s_element_cnt;
  } files[] =
  {
    { "Adc_H2-LSC-AS_Q.ilwd", 1 },
    { "H-663711990_c.ilwd", 1 },
    { "H-663711990.ilwd", 1 },
    { "H-688011800-10.ilwd", 1 },	// Sample which has gaps
    { "H-688011001-15.ilwd", 1 },	// Sample which has gaps
    { "1-chan-gap.ilwd", 1 },
    { "2-chan-gap.ilwd", 2 },
    { "multi-channel.ilwd", 2 },	// Multiple channels
    { "multi-channel-2.ilwd", 3 },	// Multiple channels
  };

  for (size_t i = 0; i < sizeof(files)/sizeof(*files); i++)
  {
    AT();
    ingest_sample_data( files[i].s_filename, files[i].s_element_cnt );
  }

  //---------------------------------------------------------------------
  // Ingestion of transfer function
  //---------------------------------------------------------------------

  {
    REAL_8			ddata[] = { 5.2, 1.3, 6.4 };
    ILwd::LdasContainer		data_set;
    ILwd::LdasArray<REAL_8>	data(ddata, sizeof(ddata)/sizeof(*ddata));

    data.setNameString("data");

    data_set.setNameString("filter:transfer_function:sequence");
    data_set.push_back(data);

    AT();
    ingest_ilwd(data_set, data_set.getNameString());
  }

  //---------------------------------------------------------------------
  // Ingestion of frequency series
  //---------------------------------------------------------------------

  ingest_ilwd< FrequencySequence<COMPLEX_16> >( MapOfILwds[FREQUENCY_SIMPLE] );

  ingest_ilwd< FrequencySequence<COMPLEX_16> >
    ( MapOfILwds[FREQUENCY_SFT_1],
      udt::TARGET_WRAPPER,
      MapOfResultILwds[RESULT_FREQUENCY_SFT_1_WRAPPER],
      MapOfNames[FREQUENCY_SFT_1] );

  ingest_ilwd< FrequencySequence<COMPLEX_16> >
    ( MapOfILwds[FREQUENCY_SFT_2],
      udt::TARGET_WRAPPER,
      MapOfResultILwds[RESULT_FREQUENCY_SFT_1_WRAPPER],
      MapOfNames[FREQUENCY_SFT_1] );

  ingest_ilwd< FrequencySequence<COMPLEX_8> >
    ( MapOfILwds[FREQUENCY_STOCASTIC_1],
      udt::TARGET_WRAPPER,
      MapOfResultILwds[RESULT_FREQUENCY_STOCASTIC_1_WRAPPER],
      MapOfNames[FREQUENCY_STOCASTIC_1] );

  //---------------------------------------------------------------------
  // Ingestion of datacondAPI MDC data
  //---------------------------------------------------------------------

  ingest_ilwd_symbols
    ( "MDC_INPUT_1 (GENERIC)",
      MapOfILwds[MDC_INPUT_1],
      udt::TARGET_GENERIC,
      MapOfResultILwds[RESULT_MDC_INPUT_1_GENERIC] );

  //---------------------------------------------------------------------
  // Ingestion of Database information
  //---------------------------------------------------------------------

  AT();
  ingest_database_data( welch, "WelchSpectrum<double>",
			CallChain::QUERY_SPECTRUM );
  AT();
  ingest_database_data( welch_csd, "WelchCSDSpectrum<float>",
			CallChain::QUERY_SPECTRUM );
  //---------------------------------------------------------------------
  // Testing of database ingestion
  //---------------------------------------------------------------------

  struct {
    const char* 		s_label;
    const CallChain::query_type	s_query_type;
    const char*			s_push_pass;
    const char* 		s_target;
    const bool			s_throws_exception;
    ilwd_types_type		s_ilwd;
    result_s_type		s_result;
  } ingestion_test [] = {
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // These test simulate -dbquery option
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    //-------------------------------------------------------------------
    // 
    //-------------------------------------------------------------------

    { "GenericQuery: SUMM_SPECTRUM_ALL: push: wrapper",
      CallChain::QUERY_GENERIC, "push", "wrapper", true,
      SUMM_SPECTRUM_ALL, RESULT_NONE },
    { "GenericQuery: SUMM_SPECTRUM_ALL: pass: wrapper",
      CallChain::QUERY_GENERIC, "pass", "wrapper", true,
      SUMM_SPECTRUM_ALL, RESULT_NONE },
    { "GenericQuery: SUMM_SPECTRUM_MULTIPLE: push: wrapper",
      CallChain::QUERY_GENERIC, "push", "wrapper", true,
      SUMM_SPECTRUM_MULTIPLE, RESULT_NONE },
    { "GenericQuery: SUMM_SPECTRUM_MULTIPLE: pass: wrapper",
      CallChain::QUERY_GENERIC, "pass", "wrapper", true,
      SUMM_SPECTRUM_MULTIPLE, RESULT_NONE },

    //-------------------------------------------------------------------
    // Same as above, just being sent to ligolw
    //-------------------------------------------------------------------

    { "GenericQuery: SUMM_SPECTRUM_ALL: push: ligolw",
      CallChain::QUERY_GENERIC, "push", "ligolw", false,
      SUMM_SPECTRUM_ALL, RESULT_NONE },
    { "GenericQuery: SUMM_SPECTRUM_ALL: pass: ligolw",
      CallChain::QUERY_GENERIC, "pass", "ligolw", false,
      SUMM_SPECTRUM_ALL, RESULT_NONE },
    { "GenericQuery: SUMM_SPECTRUM_MULTIPLE: push: ligolw",
      CallChain::QUERY_GENERIC, "push", "ligolw", false,
      SUMM_SPECTRUM_MULTIPLE, RESULT_NONE },
    { "GenericQuery: SUMM_SPECTRUM_MULTIPLE: pass: ligolw",
      CallChain::QUERY_GENERIC, "pass", "ligolw", false,
      SUMM_SPECTRUM_MULTIPLE, RESULT_NONE },

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // These test simulate -dbspectrum
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    { "SpectrumQuery: SUMM_SPECTRUM_ALL: push",
      CallChain::QUERY_SPECTRUM, "push", "wrapper", false,
      SUMM_SPECTRUM_ALL, RESULT_NONE },
    { "SpectrumQuery: SUMM_SPECTRUM_ALL: pass",
      CallChain::QUERY_SPECTRUM, "pass", "wrapper", false,
      SUMM_SPECTRUM_ALL, RESULT_NONE },

    { "SpectrumQuery: SUMM_SPECTRUM_MULTIPLE: push",
      CallChain::QUERY_SPECTRUM, "push", "wrapper", true,
      SUMM_SPECTRUM_MULTIPLE, RESULT_NONE },
    { "SpectrumQuery: SUMM_SPECTRUM_MULTIPLE: pass",
      CallChain::QUERY_SPECTRUM, "pass", "wrapper", true,
      SUMM_SPECTRUM_MULTIPLE, RESULT_NONE },

    { "SpectrumQuery: SUMM_SPECTRUM_EMPTY: push",
      CallChain::QUERY_SPECTRUM, "push", "wrapper", true,
      SUMM_SPECTRUM_EMPTY, RESULT_NONE },
    { "SpectrumQuery: SUMM_SPECTRUM_EMPTY: pass",
      CallChain::QUERY_SPECTRUM, "pass", "wrapper", true,
      SUMM_SPECTRUM_EMPTY, RESULT_NONE },

    { "SpectrumQuery: SUMM_SPECTRUM_REAL: push",
      CallChain::QUERY_SPECTRUM, "push", "wrapper", true,
      SUMM_SPECTRUM_REAL, RESULT_NONE },
    { "SpectrumQuery: SUMM_SPECTRUM_REAL: pass",
      CallChain::QUERY_SPECTRUM, "pass", "wrapper", true,
      SUMM_SPECTRUM_REAL, RESULT_NONE },

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    // These test simulate -dbntuple
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    //-------------------------------------------------------------------
    // Should succeed
    //-------------------------------------------------------------------

    { "NTupleQuery: PROCESS_TABLE_SIMPLE: push: wrapper",
      CallChain::QUERY_NTUPLE, "push", "wrapper", false,
      PROCESS_TABLE_SIMPLE, RESULT_NONE },
    { "NTupleQuery: PROCESS_TABLE_SIMPLE: pass: wrapper",
      CallChain::QUERY_NTUPLE, "pass", "wrapper", false,
      PROCESS_TABLE_SIMPLE, RESULT_NONE },

    { "NTupleQuery: PROCESS_TABLE_SIMPLE: push: ligolw",
      CallChain::QUERY_NTUPLE, "push", "ligolw", false,
      PROCESS_TABLE_SIMPLE, RESULT_TABLE_SIMPLE_PUSH_LIGOLW },
    { "NTupleQuery: PROCESS_TABLE_SIMPLE: pass: ligolw",
      CallChain::QUERY_NTUPLE, "pass", "ligolw", false,
      PROCESS_TABLE_SIMPLE, RESULT_NONE },

    { "NTupleQuery: PROCESS_TABLE_SIMPLE: push: frame",
      CallChain::QUERY_NTUPLE, "push", "frame", true,
      PROCESS_TABLE_SIMPLE,   RESULT_NONE },
    { "NTupleQuery: PROCESS_TABLE_SIMPLE: pass: frame",
      CallChain::QUERY_NTUPLE, "pass", "frame", true,
      PROCESS_TABLE_SIMPLE, RESULT_NONE },

    //-------------------------------------------------------------------
    // Fail due to null data
    //-------------------------------------------------------------------

    { "NTupleQuery: PROCESS_TABLE_SIMPLE_NULL: push",
      CallChain::QUERY_NTUPLE, "push", "wrapper", true,
      PROCESS_TABLE_SIMPLE_NULL, RESULT_NONE },
    { "NTupleQuery: PROCESS_TABLE_SIMPLE_NULL: pass",
      CallChain::QUERY_NTUPLE, "pass", "wrapper", true,
      PROCESS_TABLE_SIMPLE_NULL, RESULT_NONE },
    { "NTupleQuery: SNGL_INSPIRAL_TABLE_SIMPLE_NULL: push",
      CallChain::QUERY_NTUPLE, "push", "wrapper", true,
      SNGL_INSPIRAL_TABLE_SIMPLE_NULL, RESULT_NONE },
    { "NTupleQuery: SNGL_INSPIRAL_TABLE_SIMPLE_NULL: pass",
      CallChain::QUERY_NTUPLE, "pass", "wrapper", true,
      SNGL_INSPIRAL_TABLE_SIMPLE_NULL, RESULT_NONE },

    //-------------------------------------------------------------------
    // Fail due to presence of binary data
    //-------------------------------------------------------------------

    { "NTupleQuery: SUMM_SPECTRUM_ALL: push",
      CallChain::QUERY_NTUPLE, "push", "wrapper", true,
      SUMM_SPECTRUM_ALL, RESULT_NONE },
    { "NTupleQuery: SUMM_SPECTRUM_ALL: pass",
      CallChain::QUERY_NTUPLE, "pass", "wrapper", true,
      SUMM_SPECTRUM_ALL, RESULT_NONE },
  };

  for ( unsigned int x = 0;
	x < sizeof( ingestion_test )/sizeof( *ingestion_test );
	x++ )
  {
    ingest_table( ingestion_test[x].s_label,
		  ingestion_test[x].s_query_type,
		  ingestion_test[x].s_push_pass,
		  ingestion_test[x].s_target,
		  ingestion_test[x].s_throws_exception,
		  MapOfILwds[ingestion_test[x].s_ilwd],
		  MapOfResultILwds[ingestion_test[x].s_result]);
  }

  //---------------------------------------------------------------------
  // Testing of quality channels
  //---------------------------------------------------------------------
  struct {
    const char* 		s_label;
    const INT_4U		s_start_sec;
    const INT_4U		s_start_nsec;
    const INT_4U		s_stop_sec;
    const INT_4U		s_stop_nsec;
    const char*			s_push_pass;
    const char*			s_target;
    const bool			s_throws_exception;
    ilwd_types_type		s_ilwd;
    result_s_type		s_result;
  } qc_test [] = {

    //-------------------------------------------------------------------
    // GOOD
    //-------------------------------------------------------------------

    { "QualityChannel: GOOD: push: wrapper",
      0, 0, 10, 0, "push", "wrapper", false,
      QC_GOOD, RESULT_NONE },
    { "QualityChannel: GOOD: pass: wrapper",
      0, 0, 10, 0, "pass", "wrapper", false,
      QC_GOOD, RESULT_NONE },
    { "QualityChannel: GOOD: push: ligolw",
      0, 0, 10, 0, "push", "ligolw", false,
      QC_GOOD, RESULT_NONE },
    { "QualityChannel: GOOD: pass: ligolw",
      0, 0, 10, 0, "pass", "ligolw", false,
      QC_GOOD, RESULT_NONE },

    //-------------------------------------------------------------------
    // EMPTY
    //-----------------------------------------------------------
    // GOOD
    //-------------------------------------------------------------------

    { "QualityChannel: GOOD: push: wrapper",
      0, 0, 10, 0, "push", "wrapper", false,
      QC_GOOD, RESULT_NONE },
    { "QualityChannel: GOOD: pass: wrapper",
      0, 0, 10, 0, "pass", "wrapper", false,
      QC_GOOD, RESULT_NONE },
    { "QualityChannel: GOOD: push: ligolw",
      0, 0, 10, 0, "push", "ligolw", false,
      QC_GOOD, RESULT_NONE },
    { "QualityChannel: GOOD: pass: ligolw",
      0, 0, 10, 0, "pass", "ligolw", false,
      QC_GOOD, RESULT_NONE },

    //-------------------------------------------------------------------
    // EMPTY
    //-------------------------------------------------------------------

    { "QualityChannel: EMPTY: push: wrapper",
      0, 0, 10, 0, "push", "wrapper", false,
      QC_EMPTY, RESULT_NONE },
    { "QualityChannel: EMPTY: pass: wrapper",
      0, 0, 10, 0, "pass", "wrapper", false,
      QC_EMPTY, RESULT_NONE },
    { "QualityChannel: EMPTY: push: ligolw",
      0, 0, 10, 0, "push", "ligolw", false,
      QC_EMPTY, RESULT_NONE },
    { "QualityChannel: EMPTY: pass: ligolw",
      0, 0, 10, 0, "pass", "ligolw", false,
      QC_EMPTY, RESULT_NONE },

    //-------------------------------------------------------------------
    // BINARY
    //-------------------------------------------------------------------

    { "QualityChannel: BINARY: push: wrapper",
      0, 0, 10, 0, "push", "wrapper", true,
      QC_BINARY, RESULT_NONE },
    { "QualityChannel: BINARY: pass: wrapper",
      0, 0, 10, 0, "pass", "wrapper", true,
      QC_BINARY, RESULT_NONE },
    { "QualityChannel: BINARY: push: ligolw",
      0, 0, 10, 0, "push", "ligolw", true,
      QC_BINARY, RESULT_NONE },
    { "QualityChannel: BINARY: pass: ligolw",
      0, 0, 10, 0, "pass", "ligolw", true,
      QC_BINARY, RESULT_NONE },
  };

  for ( unsigned int x = 0;
	x < sizeof( qc_test )/sizeof( *qc_test );
	x++ )
  {
    ingest_qc( qc_test[x].s_label,
	       qc_test[x].s_start_sec,
	       qc_test[x].s_start_nsec,
	       qc_test[x].s_stop_sec,
	       qc_test[x].s_stop_nsec,
	       qc_test[x].s_push_pass,
	       qc_test[x].s_target,
	       qc_test[x].s_throws_exception,
	       MapOfILwds[qc_test[x].s_ilwd],
	       MapOfResultILwds[qc_test[x].s_result]);
  }

  //---------------------------------------------------------------------
  // All testing is now complete
  //---------------------------------------------------------------------

  Test.Message() << "Done" << endl << flush;
  Test.Exit();
}


static void
init( )
{
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  // MapOfNames
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  MapOfNames[ FREQUENCY_SFT_1 ] = "::Frame:sft_output\\:\\:sequence::ProcData:600000000:0:Frame";
  MapOfNames[ FREQUENCY_STOCASTIC_1 ] = "::Frame:filter_output\\:\\:sequence::ProcData:681932000:31250000:Frame";

  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  // MapOfILwds
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  MapOfILwds[PROCESS_TABLE_ALL] =
"    <Ilwd comment='SQL=SELECT * FROM PROCESS ORDER BY start_time FETCH FIRST 5 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_able:table' size='17'>"
"        <int_4s dims='5' name='CREATOR_DB'>1 1 1 1 1</int_4s>"
"        <lstring dims='5' name='PROGRAM' size='88'>wrapperAPI      \\,wrapperAPI      \\,wrapperAPI      \\,program_1000424 \\,program_1000424 </lstring>"
"        <lstring dims='5' name='VERSION' size='59'>1.0.0\\,1.0.0\\,1.0.0\\,version_1000424969\\,version_1000424970</lstring>"
"        <lstring dims='5' name='CVS_REPOSITORY' size='163'>/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,cvs_repository_1000424969\\,cvs_repository_1000424970</lstring>"
"        <int_4s dims='5' name='CVS_ENTRY_TIME'>681531199 681531199 681531199 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='COMMENT' size='143'>wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,comment_1000424969\\,comment_1000424970</lstring>"
"        <int_4s dims='5' name='IS_ONLINE'>0 0 0 0 0</int_4s>"
"        <lstring dims='5' name='NODE' size='59'>beowulf\\,beowulf\\,beowulf\\,node_1000424969\\,node_1000424970</lstring>"
"        <lstring dims='5' name='USERNAME' size='88'>search07        \\,search06        \\,search05        \\,username_100042 \\,username_100042 </lstring>"
"        <int_4s dims='5' name='UNIX_PROCID'>24270 24522 24735 1000424969 1000424970</int_4s>"
"        <int_4s dims='5' name='START_TIME'>684460096 684460107 684460128 684460181 684460182</int_4s>"
"        <int_4s dims='5' name='END_TIME'>684460200 684460217 684460228 684460181 684460182</int_4s>"
"        <ilwd name='PROCESS_ID' size='5'>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\120\\111\\220\\143\\043\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\121\\003\\170\\226\\162\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\121\\023\\140\\040\\151\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\111\\066\\204\\007\\051\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\011\\023\\043\\111\\066\\204\\007\\145\\000\\000\\000</char_u>"
"        </ilwd>"
"        <int_4s dims='5' name='PARAM_SET' nullmask='4'>0 0 0 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='IFOS' nullmask='4' size='32'>\\,\\,\\,ifos_100042 \\,ifos_100042 </lstring>"
"        <int_4s dims='5' name='JOBID'>38151 38148 38149 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='DOMAIN' size='66'>ldas-dev\\,ldas-dev\\,ldas-dev\\,domain_1000424969\\,domain_1000424970</lstring>"
"    </ilwd>"
;

  MapOfILwds[PROCESS_TABLE_SIMPLE] =
"    <Ilwd comment='SQL=SELECT * FROM PROCESS ORDER BY start_time FETCH FIRST 5 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_able:table' size='16'>"
"        <int_4s dims='5' name='CREATOR_DB'>1 1 1 1 1</int_4s>"
"        <lstring dims='5' name='PROGRAM' size='88'>wrapperAPI      \\,wrapperAPI      \\,wrapperAPI      \\,program_1000424 \\,program_1000424 </lstring>"
"        <lstring dims='5' name='VERSION' size='59'>1.0.0\\,1.0.0\\,1.0.0\\,version_1000424969\\,version_1000424970</lstring>"
"        <lstring dims='5' name='CVS_REPOSITORY' size='163'>/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,cvs_repository_1000424969\\,cvs_repository_1000424970</lstring>"
"        <int_4s dims='5' name='CVS_ENTRY_TIME'>681531199 681531199 681531199 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='COMMENT' size='143'>wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,comment_1000424969\\,comment_1000424970</lstring>"
"        <int_4s dims='5' name='IS_ONLINE'>0 0 0 0 0</int_4s>"
"        <lstring dims='5' name='NODE' size='59'>beowulf\\,beowulf\\,beowulf\\,node_1000424969\\,node_1000424970</lstring>"
"        <lstring dims='5' name='USERNAME' size='88'>search07        \\,search06        \\,search05        \\,username_100042 \\,username_100042 </lstring>"
"        <int_4s dims='5' name='UNIX_PROCID'>24270 24522 24735 1000424969 1000424970</int_4s>"
"        <int_4s dims='5' name='START_TIME'>684460096 684460107 684460128 684460181 684460182</int_4s>"
"        <int_4s dims='5' name='END_TIME'>684460200 684460217 684460228 684460181 684460182</int_4s>"
"        <int_4s dims='5' name='PARAM_SET'>0 0 0 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='IFOS' size='32'>\\,\\,\\,ifos_100042 \\,ifos_100042 </lstring>"
"        <int_4s dims='5' name='JOBID'>38151 38148 38149 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='DOMAIN' size='66'>ldas-dev\\,ldas-dev\\,ldas-dev\\,domain_1000424969\\,domain_1000424970</lstring>"
"    </ilwd>"
;

  MapOfILwds[PROCESS_TABLE_SIMPLE_NULL] =
"    <Ilwd comment='SQL=SELECT * FROM PROCESS ORDER BY start_time FETCH FIRST 5 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_able:table' size='16'>"
"        <int_4s dims='5' name='CREATOR_DB'>1 1 1 1 1</int_4s>"
"        <lstring dims='5' name='PROGRAM' size='88'>wrapperAPI      \\,wrapperAPI      \\,wrapperAPI      \\,program_1000424 \\,program_1000424 </lstring>"
"        <lstring dims='5' name='VERSION' size='59'>1.0.0\\,1.0.0\\,1.0.0\\,version_1000424969\\,version_1000424970</lstring>"
"        <lstring dims='5' name='CVS_REPOSITORY' size='163'>/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,cvs_repository_1000424969\\,cvs_repository_1000424970</lstring>"
"        <int_4s dims='5' name='CVS_ENTRY_TIME'>681531199 681531199 681531199 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='COMMENT' size='143'>wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,comment_1000424969\\,comment_1000424970</lstring>"
"        <int_4s dims='5' name='IS_ONLINE'>0 0 0 0 0</int_4s>"
"        <lstring dims='5' name='NODE' size='59'>beowulf\\,beowulf\\,beowulf\\,node_1000424969\\,node_1000424970</lstring>"
"        <lstring dims='5' name='USERNAME' size='88'>search07        \\,search06        \\,search05        \\,username_100042 \\,username_100042 </lstring>"
"        <int_4s dims='5' name='UNIX_PROCID'>24270 24522 24735 1000424969 1000424970</int_4s>"
"        <int_4s dims='5' name='START_TIME'>684460096 684460107 684460128 684460181 684460182</int_4s>"
"        <int_4s dims='5' name='END_TIME'>684460200 684460217 684460228 684460181 684460182</int_4s>"
"        <int_4s dims='5' name='PARAM_SET' nullmask='4'>0 0 0 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='IFOS' nullmask='4' size='32'>\\,\\,\\,ifos_100042 \\,ifos_100042 </lstring>"
"        <int_4s dims='5' name='JOBID'>38151 38148 38149 1000424969 1000424970</int_4s>"
"        <lstring dims='5' name='DOMAIN' size='66'>ldas-dev\\,ldas-dev\\,ldas-dev\\,domain_1000424969\\,domain_1000424970</lstring>"
"    </ilwd>"
;

  MapOfILwds[PROCESS_TABLE_ZERO] =
"    <ilwd comment='SQL=SELECT * FROM PROCESS ORDER BY start_time FETCH FIRST 5 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_able:table' size='9'>"
"        <lstring dims='0' name='LSTRING' size='0'></lstring>"
"        <int_2s dims='0' name='INT_2S'></int_2s>"
"        <int_2u dims='0' name='INT_2U'></int_2u>"
"        <int_4s dims='0' name='INT_4S'></int_4s>"
"        <int_4u dims='0' name='INT_4U'></int_4u>"
"        <int_8s dims='0' name='INT_8S'></int_8s>"
"        <int_8u dims='0' name='INT_8U'></int_8u>"
"        <real_4 dims='0' name='REAL_4'></real_4>"
"        <real_8 dims='0' name='REAL_8'></real_8>"
"    </ilwd>"
    ;

  MapOfILwds[SNGL_INSPIRAL_TABLE_SIMPLE_NULL] =
"    <ilwd comment='SQL=select ifo, case end_time when 610000109 then null when 610000129 then null else end_time end as end_time, impulse_time from sngl_inspiral where ((process_id, creator_db) in (select process_id, creator_db from process where (jobid = 2934))) fetch first 5 rows only FOR READ ONLY' name='ldasgroup:result_able:table' size='3'>"
"        <lstring dims='5' name='IFO' size='18'>L1\\,L1\\,L1\\,L1\\,L1</lstring>"
"        <int_4s dims='5' name='END_TIME' nullmask='o'>0 610000119 0 610000144 610000152</int_4s>"
"        <int_4s dims='5' name='IMPULSE_TIME'>0 0 0 0 0</int_4s>"
"    </ilwd>"
    ;

  MapOfILwds[SUMM_SPECTRUM_ALL] =
"    <ilwd comment='SQL=SELECT * FROM SUMM_SPECTRUM ORDER BY start_time, spectrum_type FETCH FIRST 1 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_able:table' size='18'>"
"        <int_4s name='CREATOR_DB'>1</int_4s>"
"        <lstring name='PROGRAM' size='16'>stochasticDSO   </lstring>"
"        <ilwd name='PROCESS_ID'>"
"            <char_u dims='13'>\\040\\001\\020\\046\\026\\050\\130\\224\\043\\003\\000\\000\\000</char_u>"
"        </ilwd>"
"        <lstring name='FRAMESET_GROUP' nullmask='g'></lstring>"
"        <lstring name='SEGMENT_GROUP' nullmask='g'></lstring>"
"        <int_4s name='VERSION' nullmask='g'>0</int_4s>"
"        <int_4s name='START_TIME'>681932000</int_4s>"
"        <int_4s name='START_TIME_NS'>0</int_4s>"
"        <int_4s name='END_TIME'>681932000</int_4s>"
"        <int_4s name='END_TIME_NS'>31250000</int_4s>"
"        <int_4s name='FRAMES_USED'>1</int_4s>"
"        <real_8 name='START_FREQUENCY'>0.0000000000000000e+00</real_8>"
"        <real_8 name='DELTA_FREQUENCY'>3.2000000000000000e+01</real_8>"
"        <lstring name='MIMETYPE' size='30'>x-ligo/complex-spectrum-little</lstring>"
"        <lstring name='CHANNEL' size='45'>optimally filtered cross-correlation spectrum</lstring>"
"        <lstring name='SPECTRUM_TYPE' size='85'>IFO IFO differential mode cross correlation spectrum for stochastic background search</lstring>"
"        <ilwd name='SPECTRUM'>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\020\\072\\374\\114\\333\\122\\027\\310\\117\\027\\167\\106\\327\\112\\271\\302\\077\\330\\037\\307\\253\\256\\134\\304\\302\\055\\154\\105\\032\\114\\212\\102\\116\\331\\352\\103\\251\\002\\353\\100\\326\\302\\253\\303\\321\\027\\354\\300\\121\\276\\163\\102\\214\\252\\242\\076\\333\\264\\310\\101\\276\\174\\354\\075</char_u>"
"        </ilwd>"
"        <int_4s name='SPECTRUM_LENGTH'>9</int_4s>"
"    </ilwd>"
    ;
  MapOfILwds[SUMM_SPECTRUM_MULTIPLE] =
"    <ilwd comment='SQL=SELECT * FROM SUMM_SPECTRUM ORDER BY start_time, spectrum_type FETCH FIRST 2 ROWS ONLY FOR READ ONLY' name='ldasgroup:result_able:table' size='18'>"
"        <int_4s dims='2' name='CREATOR_DB'>1 1</int_4s>"
"        <lstring dims='2' name='PROGRAM' size='34'>stochasticDSO   \\,stochasticDSO   </lstring>"
"        <ilwd name='PROCESS_ID' size='2'>"
"            <char_u dims='13'>\\040\\001\\020\\046\\026\\050\\130\\224\\043\\003\\000\\000\\000</char_u>"
"            <char_u dims='13'>\\040\\001\\020\\046\\026\\050\\130\\224\\043\\003\\000\\000\\000</char_u>"
"        </ilwd>"
"        <lstring dims='2' name='FRAMESET_GROUP' nullmask='w' size='2'>\\,</lstring>"
"        <lstring dims='2' name='SEGMENT_GROUP' nullmask='w' size='2'>\\,</lstring>"
"        <int_4s dims='2' name='VERSION' nullmask='w'>0 0</int_4s>"
"        <int_4s dims='2' name='START_TIME'>681932000 681932000</int_4s>"
"        <int_4s dims='2' name='START_TIME_NS'>0 31250000</int_4s>"
"        <int_4s dims='2' name='END_TIME'>681932000 681932000</int_4s>"
"        <int_4s dims='2' name='END_TIME_NS'>31250000 62500000</int_4s>"
"        <int_4s dims='2' name='FRAMES_USED'>1 1</int_4s>"
"        <real_8 dims='2' name='START_FREQUENCY'>0.0000000000000000e+00 0.0000000000000000e+00</real_8>"
"        <real_8 dims='2' name='DELTA_FREQUENCY'>3.2000000000000000e+01 3.2000000000000000e+01</real_8>"
"        <lstring dims='2' name='MIMETYPE' size='62'>x-ligo/complex-spectrum-little\\,x-ligo/complex-spectrum-little</lstring>"
"        <lstring dims='2' name='CHANNEL' size='92'>optimally filtered cross-correlation spectrum\\,optimally filtered cross-correlation spectrum</lstring>"
"        <lstring dims='2' name='SPECTRUM_TYPE' size='172'>IFO IFO differential mode cross correlation spectrum for stochastic background search\\,IFO IFO differential mode cross correlation spectrum for stochastic background search</lstring>"
"        <ilwd name='SPECTRUM' size='2'>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\020\\072\\374\\114\\333\\122\\027\\310\\117\\027\\167\\106\\327\\112\\271\\302\\077\\330\\037\\307\\253\\256\\134\\304\\302\\055\\154\\105\\032\\114\\212\\102\\116\\331\\352\\103\\251\\002\\353\\100\\326\\302\\253\\303\\321\\027\\354\\300\\121\\276\\163\\102\\214\\252\\242\\076\\333\\264\\310\\101\\276\\174\\354\\075</char_u>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\306\\337\\372\\114\\267\\156\\035\\311\\046\\331\\164\\106\\030\\145\\212\\303\\226\\246\\042\\307\\272\\061\\045\\304\\005\\326\\156\\105\\012\\027\\325\\101\\307\\223\\353\\103\\241\\262\\300\\077\\245\\125\\254\\303\\263\\344\\055\\276\\041\\146\\166\\102\\064\\107\\222\\077\\154\\050\\313\\101\\257\\220\\022\\077</char_u>"
"        </ilwd>"
"        <int_4s dims='2' name='SPECTRUM_LENGTH'>9 9</int_4s>"
"    </ilwd>"
    ;
  MapOfILwds[SUMM_SPECTRUM_EMPTY] =
"<ilwd jobid='153373' metadata='dboperation=dbspectrum:pushpass=push:alias=spect:rows=0:cols=19:truncated=0' name='ligo:ldas:file'>"
"    <ilwd comment='"
"             SELECT *"
"             FROM SUMM_SPECTRUM"
"             WHERE"
"                channel=&apos;_not_there_&apos;"
"                AND spectrum_type=&apos;_not_there_&apos;"
"                AND start_time=0"
"                AND start_time_ns=0"
"                AND start_frequency=0.0000000000000000e+00"
"                AND delta_frequency=0.0000000000000000e+00"
"                AND spectrum_length=0"
"             ORDER BY start_time, spectrum_type"
"             FETCH FIRST 1 ROWS ONLY"
"              for read only' jobid='153373' name='ldas:result_table:table' size='19'>"
"        <int_4s dims='0' name='CREATOR_DB'></int_4s>"
"        <lstring dims='0' name='PROGRAM'></lstring>"
"        <char_u dims='0' name='PROCESS_ID'></char_u>"
"        <lstring dims='0' name='FRAMESET_GROUP'></lstring>"
"        <lstring dims='0' name='SEGMENT_GROUP'></lstring>"
"        <int_4s dims='0' name='VERSION'></int_4s>"
"        <int_4s dims='0' name='START_TIME'></int_4s>"
"        <int_4s dims='0' name='START_TIME_NS'></int_4s>"
"        <int_4s dims='0' name='END_TIME'></int_4s>"
"        <int_4s dims='0' name='END_TIME_NS'></int_4s>"
"        <int_4s dims='0' name='FRAMES_USED'></int_4s>"
"        <real_8 dims='0' name='START_FREQUENCY'></real_8>"
"        <real_8 dims='0' name='DELTA_FREQUENCY'></real_8>"
"        <lstring dims='0' name='MIMETYPE'></lstring>"
"        <lstring dims='0' name='CHANNEL'></lstring>"
"        <lstring dims='0' name='SPECTRUM_TYPE'></lstring>"
"        <char_u dims='0' name='SPECTRUM'></char_u>"
"        <int_4s dims='0' name='SPECTRUM_LENGTH'></int_4s>"
"        <lstring dims='0' name='INSERTION_TIME'></lstring>"
"    </ilwd>"
"</ilwd>"
    ;
  MapOfILwds[SUMM_SPECTRUM_REAL] =
"<ilwd jobid='153369' metadata='dboperation=dbspectrum:pushpass=push:alias=spect:rows=1:cols=19:truncated=0' name='ligo:ldas:file'>"
"    <ilwd comment='"
"             SELECT *"
"             FROM SUMM_SPECTRUM"
"             WHERE"
"                channel=&apos;resample(P2:LSC-AS_Q,1,8)&apos;"
"                AND spectrum_type=&apos;Welch&apos;"
"                AND start_time=609999999"
"                AND start_time_ns=995117188"
"                AND start_frequency=0.0000000000000000e+00"
"                AND delta_frequency=7.8125000000000000e-03"
"                AND spectrum_length=131073"
"             ORDER BY start_time, spectrum_type"
"             FETCH FIRST 1 ROWS ONLY"
"              for read only' jobid='153369' name='ldas:result_table:table' size='19'>"
"        <int_4s name='CREATOR_DB'>1</int_4s>"
"        <lstring name='PROGRAM' size='16'>datacondAPI     </lstring>"
"        <ilwd name='PROCESS_ID'>"
"            <char_u dims='13'>\040\003\022\044\000\104\062\225\011\124\000\000\000</char_u>"
"        </ilwd>"
"        <lstring name='FRAMESET_GROUP' nullmask='g'></lstring>"
"        <lstring name='SEGMENT_GROUP' nullmask='g'></lstring>"
"        <int_4s name='VERSION' nullmask='g'>0</int_4s>"
"        <int_4s name='START_TIME'>609999999</int_4s>"
"        <int_4s name='START_TIME_NS'>995117188</int_4s>"
"        <int_4s name='END_TIME'>610000511</int_4s>"
"        <int_4s name='END_TIME_NS'>995117188</int_4s>"
"        <int_4s name='FRAMES_USED' nullmask='g'>0</int_4s>"
"        <real_8 name='START_FREQUENCY'>0.0000000000000000e+00</real_8>"
"        <real_8 name='DELTA_FREQUENCY'>7.8125000000000000e-03</real_8>"
"        <lstring name='MIMETYPE' size='27'>x-ligo/real-spectrum-little</lstring>"
"        <lstring name='CHANNEL' size='25'>resample(P2:LSC-AS_Q,1,8)</lstring>"
"        <lstring name='SPECTRUM_TYPE' size='5'>Welch</lstring>"
"        <ilwd name='SPECTRUM'>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\020\\072\\374\\114\\333\\122\\027\\310\\117\\027\\167\\106\\327\\112\\271\\302\\077\\330\\037\\307\\253\\256\\134\\304\\302\\055\\154\\105\\032\\114\\212\\102\\116\\331\\352\\103\\251\\002\\353\\100\\326\\302\\253\\303\\321\\027\\354\\300\\121\\276\\163\\102\\214\\252\\242\\076\\333\\264\\310\\101\\276\\174\\354\\075</char_u>"
"        </ilwd>"
"        <int_4s name='SPECTRUM_LENGTH'>18</int_4s>"
"        <lstring name='INSERTION_TIME' size='26'>2003-12-23 16:45:40.267087</lstring>"
"    </ilwd>"
"</ilwd>"
    ;
  MapOfILwds[QC_GOOD] =
"    <ilwd name='ldasgroup:result_able:table' size='4'>"
"        <int_4s name='StartSec'>0</int_4s>"
"        <int_4s name='StartNanoSec'>0</int_4s>"
"        <int_4s name='StopSec'>9</int_4s>"
"        <int_4s name='StopNanoSec'>0</int_4s>"
"    </ilwd>"
    ;
  MapOfILwds[QC_EMPTY] =
"    <ilwd name='ldasgroup:result_able:table' size='4'>"
"        <int_4s dims='0' name='StartSec'></int_4s>"
"        <int_4s dims='0' name='StartNanoSec'></int_4s>"
"        <int_4s dims='0' name='StopSec'></int_4s>"
"        <int_4s dims='0' name='StopNanoSec'></int_4s>"
"    </ilwd>"
    ;
  MapOfILwds[QC_BINARY] =
"    <ilwd name='ldasgroup:result_able:table' size='4'>"
"        <ilwd name='StartSec' size='1'>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\020\\072\\374\\114\\333\\122\\027\\310\\117\\027\\167\\106\\327\\112\\271\\302\\077\\330\\037\\307\\253\\256\\134\\304\\302\\055\\154\\105\\032\\114\\212\\102\\116\\331\\352\\103\\251\\002\\353\\100\\326\\302\\253\\303\\321\\027\\354\\300\\121\\276\\163\\102\\214\\252\\242\\076\\333\\264\\310\\101\\276\\174\\354\\075</char_u>"
"        </ilwd>"
"        <ilwd name='StartNanoSec' size='1'>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\020\\072\\374\\114\\333\\122\\027\\310\\117\\027\\167\\106\\327\\112\\271\\302\\077\\330\\037\\307\\253\\256\\134\\304\\302\\055\\154\\105\\032\\114\\212\\102\\116\\331\\352\\103\\251\\002\\353\\100\\326\\302\\253\\303\\321\\027\\354\\300\\121\\276\\163\\102\\214\\252\\242\\076\\333\\264\\310\\101\\276\\174\\354\\075</char_u>"
"        </ilwd>"
"        <ilwd name='StopSec' size='1'>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\020\\072\\374\\114\\333\\122\\027\\310\\117\\027\\167\\106\\327\\112\\271\\302\\077\\330\\037\\307\\253\\256\\134\\304\\302\\055\\154\\105\\032\\114\\212\\102\\116\\331\\352\\103\\251\\002\\353\\100\\326\\302\\253\\303\\321\\027\\354\\300\\121\\276\\163\\102\\214\\252\\242\\076\\333\\264\\310\\101\\276\\174\\354\\075</char_u>"
"        </ilwd>"
"        <ilwd name='StopNanoSec' size='1'>"
"            <char_u dims='72'>\\000\\000\\000\\000\\000\\000\\000\\000\\020\\072\\374\\114\\333\\122\\027\\310\\117\\027\\167\\106\\327\\112\\271\\302\\077\\330\\037\\307\\253\\256\\134\\304\\302\\055\\154\\105\\032\\114\\212\\102\\116\\331\\352\\103\\251\\002\\353\\100\\326\\302\\253\\303\\321\\027\\354\\300\\121\\276\\163\\102\\214\\252\\242\\076\\333\\264\\310\\101\\276\\174\\354\\075</char_u>"
"        </ilwd>"
"    </ilwd>"
    ;
  MapOfILwds[FREQUENCY_SIMPLE] =
"    <ilwd name='sft_output\\:\\:sequence::ProcData:600000002:0:Frame' size='9'>"
"        <lstring name='comment'></lstring>"
"        <int_2u name='type'>2</int_2u>"
"        <int_2u name='subType'>0</int_2u>"
"        <real_8 name='timeOffset'>0.0</real_8>"
"        <real_8 name='tRange'>1.0</real_8>"
"        <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"        <real_8 name='fRange'>0.0000000000000000e+00</real_8>"
"        <real_8 name='BW'>0.0000000000000000e+00</real_8>"
"        <ilwd name=':data:Container(Vect):Frame' size='1'>"
"            <complex_16 dataValueUnit='s^1/2 count' dims='3' name='data' units='hz'>2.8871234506368637e-02 1.1958857066929340e-02 -2.5983424857258797e-02 -1.7361570149660110e-02 2.2097086533904076e-02 2.2097086533904076e-02</complex_16>"
"        </ilwd>"
"    </ilwd>"
    ;

  MapOfILwds[FREQUENCY_SFT_1] =
"<ilwd name='::Frame' size='9'>"
"    <int_4s name='run'>0</int_4s>"
"    <int_4u name='frame'>0</int_4u>"
"    <int_4u name='dataQuality'>0</int_4u>"
"    <int_4u dims='2' name='GTime'>600000000 0</int_4u>"
"    <int_2u name='ULeapS'>0</int_2u>"
"    <int_4s name='localTime'>1006986168</int_4s>"
"    <real_8 name='dt'>1.0000000000000000e+00</real_8>"
"    <ilwd name=':history:Container(History):Frame' size='9'>"
"        <ilwd name='percent_pad::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='14'>-1.0000000e+00</lstring>"
"        </ilwd>"
"        <ilwd name='max_power::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>9.7656250e-04</lstring>"
"        </ilwd>"
"        <ilwd name='freq_pwmax::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>0.0000000e+00</lstring>"
"        </ilwd>"
"        <ilwd name='power_mean::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>9.7656244e-04</lstring>"
"        </ilwd>"
"        <ilwd name='power_stddev::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>2.2819075e-11</lstring>"
"        </ilwd>"
"        <ilwd name='startSec::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='9'>600000000</lstring>"
"        </ilwd>"
"        <ilwd name='startNan::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='1'>0</lstring>"
"        </ilwd>"
"        <ilwd name='sample_rate::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>3.2000000e+01</lstring>"
"        </ilwd>"
"        <ilwd name='ndata_input::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='2'>32</lstring>"
"        </ilwd>"
"    </ilwd>"
"    <ilwd name=':procData:Container(ProcData):Frame'>"
"        <ilwd name='sft_output\\:\\:sequence::ProcData:600000000:0:Frame' size='9'>"
"            <lstring name='comment' size='24'>multiDimData to ProcData</lstring>"
"            <int_2u name='type'>2</int_2u>"
"            <int_2u name='subType'>0</int_2u>"
"            <real_8 name='timeOffset'>0.0</real_8>"
"            <real_8 name='tRange'>1.0</real_8>"
"            <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"            <real_8 name='fRange'>0.0000000000000000e+00</real_8>"
"            <real_8 name='BW'>0.0000000000000000e+00</real_8>"
"            <ilwd name=':data:Container(Vect):Frame'>"
"                <complex_16 dataValueUnit='s^1/2 count' dx='1.0' dims='17' name='data' units='hz'>3.1250000000000000e-02 0.0000000000000000e+00 -3.0649539083242416e-02 -6.0965726152062416e-03 2.8871234506368637e-02 1.1958857066929340e-02 -2.5983424857258797e-02 -1.7361570149660110e-02 2.2097086533904076e-02 2.2097086533904076e-02 -1.7361570149660110e-02 -2.5983424857258797e-02 1.1958857066929340e-02 2.8871234506368637e-02 -6.0965726152062416e-03 -3.0649539083242416e-02 0.0000000000000000e+00 3.1250000000000000e-02 6.0965726152062416e-03 -3.0649539083242416e-02 -1.1958857066929340e-02 2.8871234506368637e-02 1.7361570149660110e-02 -2.5983424857258797e-02 -2.2097086533904076e-02 2.2097086533904076e-02 2.5983424857258797e-02 -1.7361570149660110e-02 -2.8871234506368637e-02 1.1958857066929340e-02 3.0649539083242416e-02 -6.0965726152062416e-03 -3.1250000000000000e-02 0.0000000000000000e+00</complex_16>"
"            </ilwd>"
"        </ilwd>"
"    </ilwd>"
"</ilwd>"
    ;
  MapOfILwds[FREQUENCY_SFT_2] =
"<ilwd name='::Frame' size='9'>"
"    <int_4s name='run'>0</int_4s>"
"    <int_4u name='frame'>0</int_4u>"
"    <int_4u name='dataQuality'>0</int_4u>"
"    <int_4u dims='2' name='GTime'>600000000 0</int_4u>"
"    <int_2u name='ULeapS'>0</int_2u>"
"    <int_4s name='localTime'>1006986168</int_4s>"
"    <real_8 name='dt'>1.0000000000000000e+00</real_8>"
"    <ilwd name=':history:Container(History):Frame'>"
"      <ilwd name=':history:Container(History):Frame' size='9'>"
"        <ilwd name='percent_pad::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='14'>-1.0000000e+00</lstring>"
"        </ilwd>"
"        <ilwd name='max_power::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>9.7656250e-04</lstring>"
"        </ilwd>"
"        <ilwd name='freq_pwmax::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>0.0000000e+00</lstring>"
"        </ilwd>"
"        <ilwd name='power_mean::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>9.7656244e-04</lstring>"
"        </ilwd>"
"        <ilwd name='power_stddev::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>2.2819075e-11</lstring>"
"        </ilwd>"
"        <ilwd name='startSec::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='9'>600000000</lstring>"
"        </ilwd>"
"        <ilwd name='startNan::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='1'>0</lstring>"
"        </ilwd>"
"        <ilwd name='sample_rate::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='13'>3.2000000e+01</lstring>"
"        </ilwd>"
"        <ilwd name='ndata_input::History:Frame' size='2'>"
"            <int_4u name='time'>691021358</int_4u>"
"            <lstring name='comment' size='2'>32</lstring>"
"        </ilwd>"
"      </ilwd>"
"    </ilwd>"
"    <ilwd name=':procData:Container(ProcData):Frame'>"
"        <ilwd name='sft_output\\:\\:sequence::ProcData:600000000:0:Frame' size='9'>"
"            <lstring name='comment' size='24'>multiDimData to ProcData</lstring>"
"            <int_2u name='type'>2</int_2u>"
"            <int_2u name='subType'>0</int_2u>"
"            <real_8 name='timeOffset'>0.0</real_8>"
"            <real_8 name='tRange'>1.0</real_8>"
"            <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"            <real_8 name='fRange'>0.0000000000000000e+00</real_8>"
"            <real_8 name='BW'>0.0000000000000000e+00</real_8>"
"            <ilwd name=':data:Container(Vect):Frame'>"
"                <complex_16 dataValueUnit='s^1/2 count' dx='1.0' dims='17' name='data' units='hz'>3.1250000000000000e-02 0.0000000000000000e+00 -3.0649539083242416e-02 -6.0965726152062416e-03 2.8871234506368637e-02 1.1958857066929340e-02 -2.5983424857258797e-02 -1.7361570149660110e-02 2.2097086533904076e-02 2.2097086533904076e-02 -1.7361570149660110e-02 -2.5983424857258797e-02 1.1958857066929340e-02 2.8871234506368637e-02 -6.0965726152062416e-03 -3.0649539083242416e-02 0.0000000000000000e+00 3.1250000000000000e-02 6.0965726152062416e-03 -3.0649539083242416e-02 -1.1958857066929340e-02 2.8871234506368637e-02 1.7361570149660110e-02 -2.5983424857258797e-02 -2.2097086533904076e-02 2.2097086533904076e-02 2.5983424857258797e-02 -1.7361570149660110e-02 -2.8871234506368637e-02 1.1958857066929340e-02 3.0649539083242416e-02 -6.0965726152062416e-03 -3.1250000000000000e-02 0.0000000000000000e+00</complex_16>"
"            </ilwd>"
"        </ilwd>"
"    </ilwd>"
"</ilwd>"
    ;
  MapOfILwds[FREQUENCY_STOCASTIC_1] =
"<ilwd name='::Frame' size='8'>"
"    <int_4s name='run'>0</int_4s>"
"    <int_4u name='frame'>0</int_4u>"
"    <int_4u name='dataQuality'>0</int_4u>"
"    <int_4u dims='2' name='GTime'>681932000 31250000</int_4u>"
"    <int_2u name='ULeapS'>0</int_2u>"
"    <int_4s name='localTime'>1007073764</int_4s>"
"    <real_8 name='dt'>3.1250000000000000e-02</real_8>"
"    <ilwd name=':procData:Container(ProcData):Frame'>"
"        <ilwd name='filter_output\\:\\:sequence::ProcData:681932000:31250000:Frame' size='9'>"
"            <lstring name='comment' size='24'>multiDimData to ProcData</lstring>"
"            <int_2u name='type'>2</int_2u>"
"            <int_2u name='subType'>0</int_2u>"
"            <real_8 name='timeOffset'>0.0</real_8>"
"            <real_8 name='tRange'>0.03125</real_8>"
"            <real_8 name='fShift'>0.0000000000000000e+00</real_8>"
"            <real_8 name='fRange'>0.0000000000000000e+00</real_8>"
"            <real_8 name='BW'>0.0000000000000000e+00</real_8>"
"            <ilwd name=':data:Container(Vect):Frame'>"
"                <complex_8 dataValueUnit='s^2' dims='9' dx='32' name='data' units='hz'>0.0000000e+00 0.0000000e+00 1.3153029e+08 -6.4484344e+05 1.5670287e+04 -2.7678979e+02 -4.1638586e+04 -6.6077698e+02 3.8213762e+03 2.6636250e+01 4.7115451e+02 1.5054513e+00 -3.4466910e+02 -1.6981773e-01 6.1599735e+01 1.1427979e+00 2.5394737e+01 5.7252020e-01</complex_8>"
"            </ilwd>"
"        </ilwd>"
"    </ilwd>"
"</ilwd>"
    ;
  MapOfILwds[MDC_INPUT_1] =
"<ilwd name='mdc_input_data' size='3'>"
"  <ilwd comment='data array' name='chan_01' size='3'>"
"    <int_4s comment='result_data' name='chan_01:rate'>16384</int_4s>"
"    <int_4s comment='result_data' name='length'>10</int_4s>"
"    <real_4 comment='result_data' dims='10' name='data'>-3.5440000e+03 -1.3645000e+04 1.0260000e+03 2.3560000e+03 -9.3920000e+03 9.7550000e+03 9.7410000e+03 -3.0900000e+02 2.6810000e+03 1.4300000e+03</real_4>"
"  </ilwd>"
"  <ilwd comment='b_coefficients' name='chan_02' size='2'>"
"    <int_4s comment='result_data' name='length'>5</int_4s>"
"    <real_8 comment='result_data' dims='5' name='data'>9.0430090701050004e-04 1.0851610884129999e-02 5.9683859862690002e-02 1.9894619954230000e-01 4.4762894897020000e-01</real_8>"
"  </ilwd>"
"  <ilwd comment='a_coefficients' name='chan_03' size='2'>"
"    <int_4s comment='result_data' name='length'>3</int_4s>"
"    <real_8 comment='result_data' dims='3' name='mdc_input_data:chan_03:data'>1.0000000000000000e+00 0.0000000000000000e+00 1.6178340893050001e+00</real_8>"
"  </ilwd>"
"</ilwd>"
    ;
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  // MapOfResultILwds
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

  MapOfResultILwds[ RESULT_NONE ] = "";
  MapOfResultILwds[ RESULT_DUMP ] = "<ilwd name='dump' size='0'></ilwd>";
  MapOfResultILwds[RESULT_TABLE_SIMPLE_PUSH_LIGOLW] =
"   <ilwd comment='This is the result of the action sequence' name='query_results' size='16'>"
"    <ilwd name='CREATOR_DB'>"
"      <int_4s dims='5' name='CREATOR_DB'>1 1 1 1 1</int_4s>"
"    </ilwd>"
"    <ilwd name='PROGRAM'>"
"      <lstring dims='5' name='PROGRAM' size='88'>wrapperAPI      \\,wrapperAPI      \\,wrapperAPI      \\,program_1000424 \\,program_1000424 </lstring>"
"    </ilwd>"
"    <ilwd name='VERSION'>"
"      <lstring dims='5' name='VERSION' size='59'>1.0.0\\,1.0.0\\,1.0.0\\,version_1000424969\\,version_1000424970</lstring>"
"    </ilwd>"
"    <ilwd name='CVS_REPOSITORY'>"
"      <lstring dims='5' name='CVS_REPOSITORY' size='163'>/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,/ldcg_server/common/repository/ldas\\,cvs_repository_1000424969\\,cvs_repository_1000424970</lstring>"
"    </ilwd>"
"    <ilwd name='CVS_ENTRY_TIME'>"
"      <int_4s dims='5' name='CVS_ENTRY_TIME'>681531199 681531199 681531199 1000424969 1000424970</int_4s>"
"    </ilwd>"
"    <ilwd name='COMMENT'>"
"      <lstring dims='5' name='COMMENT' size='143'>wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,wrapperAPI is running on 2 nodes.\\,comment_1000424969\\,comment_1000424970</lstring>"
"    </ilwd>"
"    <ilwd name='IS_ONLINE'>"
"      <int_4s dims='5' name='IS_ONLINE'>0 0 0 0 0</int_4s>"
"    </ilwd>"
"    <ilwd name='NODE'>"
"      <lstring dims='5' name='NODE' size='59'>beowulf\\,beowulf\\,beowulf\\,node_1000424969\\,node_1000424970</lstring>"
"    </ilwd>"
"    <ilwd name='USERNAME'>"
"      <lstring dims='5' name='USERNAME' size='88'>search07        \\,search06        \\,search05        \\,username_100042 \\,username_100042 </lstring>"
"    </ilwd>"
"    <ilwd name='UNIX_PROCID'>"
"      <int_4s dims='5' name='UNIX_PROCID'>24270 24522 24735 1000424969 1000424970</int_4s>"
"    </ilwd>"
"    <ilwd name='START_TIME'>"
"      <int_4s dims='5' name='START_TIME'>684460096 684460107 684460128 684460181 684460182</int_4s>"
"    </ilwd>"
"    <ilwd name='END_TIME'>"
"      <int_4s dims='5' name='END_TIME'>684460200 684460217 684460228 684460181 684460182</int_4s>"
"    </ilwd>"
"    <ilwd name='PARAM_SET'>"
"      <int_4s dims='5' name='PARAM_SET'>0 0 0 1000424969 1000424970</int_4s>"
"    </ilwd>"
"    <ilwd name='IFOS'>"
"      <lstring dims='5' name='IFOS' size='32'>\\,\\,\\,ifos_100042 \\,ifos_100042 </lstring>"
"    </ilwd>"
"    <ilwd name='JOBID'>"
"      <int_4s dims='5' name='JOBID'>38151 38148 38149 1000424969 1000424970</int_4s>"
"    </ilwd>"
"    <ilwd name='DOMAIN'>"
"      <lstring dims='5' name='DOMAIN' size='66'>ldas-dev\\,ldas-dev\\,ldas-dev\\,domain_1000424969\\,domain_1000424970</lstring>"
"    </ilwd>"
"  </ilwd>"
    ;
  MapOfResultILwds[RESULT_SUMM_SPECTRUM_ALL_MPI] =
"  <ilwd comment='This is the result of the action sequence' name='sft_output\\:\\:spectrum:sequence::optimally filtered cross-correlation spectrum' size='10'>"
"    <lstring name='real:domain' size='4'>FREQ</lstring>"
"    <int_4u name='gps_sec:start_time' units='sec'>681932000</int_4u>"
"    <int_4u name='gps_nan:start_time' units='nanosec'>0</int_4u>"
"    <int_4u name='gps_sec:stop_time' units='sec'>681932000</int_4u>"
"    <int_4u name='gps_nan:stop_time' units='nanosec'>31250000</int_4u>"
"    <real_8 name='start_freq' units='hz'>0.0000000000000000e+00</real_8>"
"    <real_8 name='stop_freq' units='hz'>2.5600000000000000e+02</real_8>"
"    <real_8 name='freq:step_size' units='hz'>3.2000000000000000e+01</real_8>"
"    <real_8 name='base_freq' units='hz'>0.0000000000000000e+00</real_8>"
"    <complex_8 dims='9' name='data'>0.0000000e+00 0.0000000e+00 1.3223949e+08 -1.5495542e+05 1.5813827e+04 -9.2646172e+01 -4.0920246e+04 -8.8272919e+02 3.7788599e+03 6.9148636e+01 4.6969769e+02 7.3440747e+00 -3.4352216e+02 -7.3779073e+00 6.0935856e+01 3.1770742e-01 2.5088308e+01 1.1547230e-01</complex_8>"
"  </ilwd>"
;
  MapOfResultILwds[RESULT_SUMM_SPECTRUM_ALL_LIGOLW] =
" <ilwd comment='This is the result of the action sequence' name='query_results' size='3'>"
"    <ilwd size='2'>"
"      <ilwd size='2'>"
"        <complex_8 dims='9'>0.0000000e+00 0.0000000e+00 1.3223949e+08 -1.5495542e+05 1.5813827e+04 -9.2646172e+01 -4.0920246e+04 -8.8272919e+02 3.7788599e+03 6.9148636e+01 4.6969769e+02 7.3440747e+00 -3.4352216e+02 -7.3779073e+00 6.0935856e+01 3.1770742e-01 2.5088308e+01 1.1547230e-01</complex_8>"
"        <ilwd size='2'>"
"          <real_8 name='FrequencyBase'>0.0000000000000000e+00</real_8>"
"          <real_8 name='FrequencyDelta'>3.2000000000000000e+01</real_8>"
"        </ilwd>"
"      </ilwd>"
"      <ilwd size='2'>"
"        <int_4u name='DetrendMethod'>0</int_4u>"
"        <lstring name='Estimator'></lstring>"
"      </ilwd>"
"    </ilwd>"
"    <ilwd size='5'>"
"      <real_8 name='BaseFrequency'>0.0000000000000000e+00</real_8>"
"      <int_4u name='FFTLength'>0</int_4u>"
"      <int_4u name='FFTOverlap'>0</int_4u>"
"      <lstring name='WindowName'></lstring>"
"      <real_8 name='WindowParameter'>0.0000000000000000e+00</real_8>    </ilwd>"
"    <lstring name='NameMetaData'></lstring>"
"  </ilwd>"
    ;
  MapOfResultILwds[RESULT_SUMM_SPECTRUM_ALL_PASS_LIGOLW] =
"  <ilwd comment='Pass Through' name='query' size='3'>"
"    <ilwd size='2'>"
"      <ilwd size='2'>"
"        <complex_8 dims='9'>0.0000000e+00 0.0000000e+00 1.3223949e+08 -1.5495542e+05 1.5813827e+04 -9.2646172e+01 -4.0920246e+04 -8.8272919e+02 3.7788599e+03 6.9148636e+01 4.6969769e+02 7.3440747e+00 -3.4352216e+02 -7.3779073e+00 6.0935856e+01 3.1770742e-01 2.5088308e+01 1.1547230e-01</complex_8>"
"        <ilwd size='2'>"
"          <real_8 name='FrequencyBase'>0.0000000000000000e+00</real_8>"
"          <real_8 name='FrequencyDelta'>3.2000000000000000e+01</real_8>"
"        </ilwd>"
"      </ilwd>"
"      <ilwd size='2'>"
"        <int_4u name='DetrendMethod'>0</int_4u>"
"        <lstring name='Estimator'></lstring>"
"      </ilwd>"
"    </ilwd>"
"    <ilwd size='5'>"
"      <real_8 name='BaseFrequency'>0.0000000000000000e+00</real_8>"
"      <int_4u name='FFTLength'>0</int_4u>"
"      <int_4u name='FFTOverlap'>0</int_4u>"
"      <lstring name='WindowName'></lstring>"
"      <real_8 name='WindowParameter'>0.0000000000000000e+00</real_8>"
"    </ilwd>"
"    <lstring name='NameMetaData'></lstring>"
"  </ilwd>"
    ;
  MapOfResultILwds[RESULT_FREQUENCY_SFT_1_WRAPPER] =
"  <ilwd name='sft_output\\:\\:sequence:spectrum:sequence:' size='10'>"
"    <lstring name='complex:domain' size='4'>FREQ</lstring>"
"    <real_8 name='start_freq' units='hz'>0.0000000000000000e+00</real_8>"
"    <real_8 name='stop_freq' units='hz'>1.6000000000000000e+01</real_8>"
"    <real_8 name='freq:step_size' units='hz'>1.0000000000000000e+00</real_8>"
"    <ilwd name='LDAS_History' size='9'>"
"      <ilwd name='percent_pad::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='14'>-1.0000000e+00</lstring>"
"      </ilwd>"
"      <ilwd name='max_power::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='13'>9.7656250e-04</lstring>"
"      </ilwd>"
"      <ilwd name='freq_pwmax::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='13'>0.0000000e+00</lstring>"
"      </ilwd>"
"      <ilwd name='power_mean::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='13'>9.7656244e-04</lstring>"
"      </ilwd>"
"      <ilwd name='power_stddev::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='13'>2.2819075e-11</lstring>"
"      </ilwd>"
"      <ilwd name='startSec::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='9'>600000000</lstring>"
"      </ilwd>"
"      <ilwd name='startNan::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='1'>0</lstring>"
"      </ilwd>"
"      <ilwd name='sample_rate::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='13'>3.2000000e+01</lstring>"
"      </ilwd>"
"      <ilwd name='ndata_input::History:Frame' size='2'>"
"        <int_4u name='time'>691021358</int_4u>"
"        <lstring name='comment' size='2'>32</lstring>"
"      </ilwd>"
"    </ilwd>"
"    <complex_16 dims='17' name='data'>3.1250000000000000e-02 0.0000000000000000e+00 -3.0649539083242416e-02 -6.0965726152062416e-03 2.8871234506368637e-02 1.1958857066929340e-02 -2.5983424857258797e-02 -1.7361570149660110e-02 2.2097086533904076e-02 2.2097086533904076e-02 -1.7361570149660110e-02 -2.5983424857258797e-02 1.1958857066929340e-02 2.8871234506368637e-02 -6.0965726152062416e-03 -3.0649539083242416e-02 0.0000000000000000e+00 3.1250000000000000e-02 6.0965726152062416e-03 -3.0649539083242416e-02 -1.1958857066929340e-02 2.8871234506368637e-02 1.7361570149660110e-02 -2.5983424857258797e-02 -2.2097086533904076e-02 2.2097086533904076e-02 2.5983424857258797e-02 -1.7361570149660110e-02 -2.8871234506368637e-02 1.1958857066929340e-02 3.0649539083242416e-02 -6.0965726152062416e-03 -3.1250000000000000e-02 0.0000000000000000e+00</complex_16>"
"    <int_4u name='gps_sec:start_time' units='sec'>600000000</int_4u>"
"    <int_4u name='gps_nan:start_time' units='nanosec'>0</int_4u>"
"    <int_4u name='gps_sec:stop_time' units='sec'>600000001</int_4u>"
"    <int_4u name='gps_nan:stop_time' units='nanosec'>0</int_4u>"
"  </ilwd>"
    ;
  MapOfResultILwds[RESULT_FREQUENCY_STOCASTIC_1_WRAPPER] =
"  <ilwd name='filter_output\\:\\:sequence:spectrum:sequence:' size='9'>"
"    <lstring name='complex:domain' size='4'>FREQ</lstring>"
"    <real_8 name='start_freq' units='hz'>0.0000000000000000e+00</real_8>"
"    <real_8 name='stop_freq' units='hz'>2.5600000000000000e+02</real_8>"
"    <real_8 name='freq:step_size' units='hz'>3.2000000000000000e+01</real_8>"
"    <complex_8 dims='9' name='data'>0.0000000e+00 0.0000000e+00 1.3153029e+08 -6.4484344e+05 1.5670287e+04 -2.7678979e+02 -4.1638586e+04 -6.6077698e+02 3.8213762e+03 2.6636250e+01 4.7115451e+02 1.5054513e+00 -3.4466910e+02 -1.6981773e-01 6.1599735e+01 1.1427979e+00 2.5394737e+01 5.7252020e-01</complex_8>"
"    <int_4u name='gps_sec:start_time' units='sec'>681932000</int_4u>"
"    <int_4u name='gps_nan:start_time' units='nanosec'>31250000</int_4u>"
"    <int_4u name='gps_sec:stop_time' units='sec'>681932000</int_4u>"
"    <int_4u name='gps_nan:stop_time' units='nanosec'>62500000</int_4u>"
"  </ilwd>"
    ;
  MapOfResultILwds[RESULT_MDC_INPUT_1_GENERIC] =
"<ilwd size='7'>"
"    <int_4s comment='result_data' name='mdc_input_data:chan_01:chan_01:rate'>16384</int_4s>"
"    <int_4s comment='result_data' name='mdc_input_data:chan_01:length'>10</int_4s>"
"    <real_4 comment='result_data' dims='10' name='mdc_input_data:chan_01:data'>-3.5440000e+03 -1.3645000e+04 1.0260000e+03 2.3560000e+03 -9.3920000e+03 9.7550000e+03 9.7410000e+03 -3.0900000e+02 2.6810000e+03 1.4300000e+03</real_4>"
"    <int_4s comment='result_data' name='mdc_input_data:chan_02:length'>5</int_4s>"
"    <real_8 comment='result_data' dims='5' name='mdc_input_data:chan_02:data'>9.0430090701050004e-04 1.0851610884129999e-02 5.9683859862690002e-02 1.9894619954230000e-01 4.4762894897020000e-01</real_8>"
"    <int_4s comment='result_data' name='mdc_input_data:chan_03:length'>3</int_4s>"
"    <real_8 comment='result_data' dims='3' name='mdc_input_data:chan_03:mdc_input_data:chan_03:data'>1.0000000000000000e+00 0.0000000000000000e+00 1.6178340893050001e+00</real_8>"
"</ilwd>"
    ;
}
