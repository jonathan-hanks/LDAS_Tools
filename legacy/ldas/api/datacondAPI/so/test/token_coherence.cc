#include "datacondAPI/config.h"

#include <cmath>
#include <fstream>

#include "general/unittest.h"
#include "token.hh"
#include "ScalarUDT.hh"
#include "SequenceUDT.hh"
#include "WelchCSDSpectrumUDT.hh"
#include "WindowUDT.hh"
#include "Coherence.hh"

General::UnitTest Test;

using namespace datacondAPI;



template<typename T> void test()
{
    Sequence<T> x(1024);
	Sequence<T> y(1024);

	for (unsigned i = 0; i < x.size(); ++i)
	{
	    x[i] = std::sin(static_cast<const double>(i));
            y[i] = std::cos(static_cast<const double>(i*2));
	}

	CallChain commands;
	Parameters arguments;

	commands.AddSymbol("x", x.Clone());
	commands.AddSymbol("y", y.Clone());

        Test.Check(true) << "added x, y\n";

	commands.AppendCallFunction("coherence", arguments.set(2, "x", "y"), "z");

        Test.Check(true) << "added z = coherence(x, y)\n";

	commands.AppendIntermediateResult("z","z","z","");

        Test.Check(true) << "added output(x)\n";

	commands.Execute();

	Test.Check(true) << "sanity\n";

	udt& z = *commands.GetSymbol("z");

	Sequence<typename traits<T>::type>& wcsds =
	    udt::Cast< Sequence<typename traits<T>::type> >(z);

	Test.Check(true) << "returned WelchCSDSpectrum of size " << wcsds.size() << '\n';

	//compare to udt returned from Coherence

        Coherence coherer;
        Sequence<typename traits<T>::type> a_name;
        coherer.apply(a_name, x, y);

        Test.Check(std::abs(std::valarray<typename traits<T>::type>(wcsds - a_name).sum()) < 0.0001) << " sort of equal, yeah, cool\n"; 

}

void bugger_up()
{

    try
	{
        CallChain commands;
		Parameters arguments;

		commands.AppendCallFunction("coherence", arguments.set(0), "z");
    	commands.AppendIntermediateResult("z","z","z","");

		commands.Execute();

		Test.Check(false) << "coherence() did not throw\n";
    }
	catch(std::exception& x)
	{
	    Test.Check(true) << "coherence() threw " << x.what() << '\n';
	}

	try
	{
	    CallChain commands;
		Parameters arguments;

        commands.AddSymbol("x", new Scalar<double>(1.0));
		commands.AddSymbol("y", new Scalar<double>(1.0));

		commands.AppendCallFunction("coherence", arguments.set(2,"x","y"), "z");
    	commands.AppendIntermediateResult("z","z","z","");

		commands.Execute();

		Test.Check(false) << "coherence(scalar,scalar) did not throw\n";
    }
	catch(std::exception& x)
	{
	    Test.Check(true) << "coherence(scalar,scalar) threw " << x.what() << '\n';
	}

}

/*
template<typename type> void dump()
{

    // coherence(x, y, 256,

    Sequence<type> x(1024);
	Sequence<type> y(1024);

	for (unsigned i = 0; i < x.size(); ++i)
	{
	    x[i] = std::sin(i);
		y[i] = std::cos(i*2);
	}

	KaiserWindowUDT kw(3);

	CallChain commands;
	Parameters arguments;

	commands.AddSymbol("x", x.Clone());
	commands.AddSymbol("y", y.Clone());
	commands.AddSymbol("kw", kw.Clone());

	commands.AppendCallFunction("coherence", arguments.set(4, "x", "y", "", "kw"), "z");

	commands.AppendIntermediateResult("z","z","z","");

	commands.Execute();

	Test.Check(true) << "sanity\n";

	udt& z = *commands.GetSymbol("z");

	WelchCSDSpectrum<traits<type>::ct>& wcsds =
	    udt::Cast<WelchCSDSpectrum<traits<type>::ct> >(z);

	Test.Check(true) << "returned WelchCSDSpectrum of size " << wcsds.size() << '\n';

	if (true)
	{
	    std::ofstream file("coherence");
		for (unsigned i = 0; i < wcsds.size(); ++i)
		{
		    file << std::real(wcsds[i]) << ' ';
			file << std::imag(wcsds[i]) << '\n';
		}
	}

}
*/

int
main(int ArgC, char** ArgV)
{

  Test.Init(ArgC, ArgV);

  try
  {
    test<float>();
    test<double>();
    test<std::complex<float> >();
    test<std::complex<double> >();
	bugger_up();
	//dump<double>();
  }

  CATCH(Test);

  Test.Exit();
}
