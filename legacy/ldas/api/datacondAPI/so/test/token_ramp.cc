#include "datacondAPI/config.h"

#include "general/unittest.h"

#include "token.hh"

#include "UDT.hh"
#include "SequenceUDT.hh"
#include "rampFunction.hh"

#include "TimeSeries.hh"
#include "WelchCSDEstimate.hh"
#include "WelchSpectrumUDT.hh"
#include "TypeInfo.hh"

using namespace std;

General::UnitTest	Test;

template<class Tout, class Tin>
void
ramp_test_template(const std::string& junk1,
		   const std::string& junk2, 
		   const std::string& junk3)
{
  try {
    //-------------------------------------------------------------------
    // Calculate the expected value
    //-------------------------------------------------------------------
    Parameters		args;
    //    FFTType		data[] = { Base, Base, Base, Base };

    //datacondAPI::Sequence<FFTType>	in(data, sizeof(data)/sizeof(*data));
    //datacondAPI::udt*			expected((datacondAPI::udt*)NULL);
    //datacondAPI::FFT			fft;

    //fft.apply(expected, in);

    //-------------------------------------------------------------------
    // Calculate value using Call Chain.
    //-------------------------------------------------------------------

    CallChain		cmds;
    CallChain::Symbol*	result;
    ILwd::LdasElement*	ilwd;

    cmds.Reset();
    // Code to create a length 100 ramp from 0 to 10 by 0.1's
    // N = integer(100);
    cmds.AppendCallFunction("integer", args.set(1, "101"), "N");

    // dy = double(0.1);
    cmds.AppendCallFunction("double", args.set(1, "0.1"), "dy");

    // y0 = double(0.0);
    cmds.AppendCallFunction("double", args.set(1, "0.0"), "y0");

    // y = ramp(N, dy, y0);
    cmds.AppendCallFunction("ramp", args.set(3, "N", "dy", "y0"), "result");
    cmds.AppendIntermediateResult( "result", "result", "Final Result", "" );

    //base.freeze(false);
    //size.freeze(false);

    Test.Message() << "...executing...\n";
    cmds.Execute();

    //-------------------------------------------------------------------
    // Compare the results to the exepected value.
    //-------------------------------------------------------------------
    //result = cmds.GetSymbol("result");
    //check_udt_valarray(*expected, result, "fft");

    //delete expected;

    //-------------------------------------------------------------------
    // Compare the results to the exepected value, output (from psd_tbl):
    //-------------------------------------------------------------------
    result = cmds.GetSymbol("result");

    ilwd = result->ConvertToIlwd(cmds, datacondAPI::udt::TARGET_GENERIC);

    //if (ILwd::LdasContainer* p = dynamic_cast<ILwd::LdasContainer*>(ilwd))
    //  {
    //	p->write(2, 2, Test.Message(), ILwd::ASCII);
    //  }
    //else
    //  {
    //Test.Check(false) << "bad dynamic_cast<ILwd::LdasContainer*>(ilwd)\n";
    //  }
 
    //dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
    //					    Test.Message(),
    //						    ILwd::ASCII);

    Test.Message(false) << endl;
    delete ilwd;

    ilwd = cmds.GetResults();
    dynamic_cast<ILwd::LdasContainer*>(ilwd)->write(2, 2,
						    Test.Message(),
						    ILwd::ASCII);
    Test.Message(false) << endl;

    //    delete ilwd;

    Test.Message(false) << endl;

  }
  CATCH(Test);
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);
  Test.Message() << "$Id: token_ramp.cc,v 1.3 2005/12/01 22:55:03 emaros Exp $"
		 << endl;

  ramp_test_template<float, float>("blah", "blah", "blah");
  // ramp_test_template<double, float>("dvalarray", "double", 2.0);
  // ramp_test_template<float, double>("svalarray", "double", 3.0);
  // ramp_test_template<float, float>("svalarray", "double", 4.0);

  Test.Exit();
}
