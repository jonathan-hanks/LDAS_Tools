#include "datacondAPI/config.h"

#include <stdexcept>
#include <general/util.hh>
#include <general/unittest.h>
#include <filters/valarray_utils.hh>

#include "token.hh"

#include "RespFilt.hh"

#include "ScalarUDT.hh"
#include "TimeSeries.hh"
#include "FrequencySequenceUDT.hh"

#include "token.hh"

using namespace std;
using namespace General;
using namespace datacondAPI;

namespace
{
  template <class T>
  T
  readit( ifstream& Input )
  {
    union {
      T		d;
      char	c[ sizeof( T ) ];
    } data;
    Input.read(data.c, sizeof(data.c));
    return( data.d );
  }

  template <>
  complex< float >
  readit< complex< float > >( ifstream& Input )
  {
    union {
      float	d[ 2 ];
      char	c[ 2 * sizeof( float ) ];
    } data;
    Input.read(data.c, sizeof(data.c));
    return( complex< float>( data.d[ 0 ], data.d[ 1 ] ) );
  }

  template <>
  complex< double >
  readit< complex< double > >( ifstream& Input )
  {
    union {
      double	d[ 2 ];
      char	c[ 2 * sizeof( double ) ];
    } data;
    Input.read(data.c, sizeof(data.c));
    return( complex< float>( data.d[ 0 ], data.d[ 1 ] ) );
  }
}

UnitTest Test;

// input data
TimeSeries<float> series;

// expected output from Matlab
TimeSeries<float> filts;

// calibration measurements
TimeSeries<float> realA;
TimeSeries<float> realG;

TimeSeries<complex<float> > A;
TimeSeries<complex<float> > G;

// response function
FrequencySequence<complex<float> > R;

// sensing function
FrequencySequence<complex<float> > C;

std::string dataDir()
{
  const char* const srcdir = getenv("DATADIR");

  if (srcdir != 0)
  {
      return string(srcdir) + string("/");
  }
  else
  {
      return string("./");
  }
}

template<class T>
inline
void reverse(T* const p)
{
  reverse<sizeof(T)>((void *) p, 1);
}

template<class T>
inline
void reverse(complex<T>* const p)
{
  reverse<sizeof(T)>((void *) p, 2);
}

template<class T>
void readArray(valarray<T>& x, const string& fileName)
{
#if 0
    union {
        char buf[sizeof(T)];
        T    t;
    } data;
#else
    T data;
#endif

    const std::string fName = dataDir() + fileName;

    ifstream file(fName.c_str(), ios_base::binary | ios_base::in);
    
    if (!file)
    {
        throw std::runtime_error("Couldn't open input file: " + fName);
    }

    vector<T> input;
    
    data = readit< T >(file);

    while (!file.eof())
    {
#ifdef WORDS_BIGENDIAN
        reverse(&data);
#endif
        input.push_back(data);
        data = readit< T >(file);
    }

    x.resize(input.size());

    copy(input.begin(), input.end(), &x[0]);

    Test.Message() << "Read " << x.size() << " inputs from file "
                   << fileName
                   << endl;
    Test.Message() << x[0] << " " << x[1] << " " << x[2] << " ..." << endl;
}

void TestExceptions()
{
    bool pass = true;
    string what;

    TimeSeries<float> ts(series);

    // Empty time-series
    try {
        TimeSeries<float> s(ts);
        s.resize(0);
        respFilt(s, R, C, A, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Empty time-series: " << what << endl;

    // Empty R
    try {
        FrequencySequence<complex<float> > s(R);
        s.resize(0);
        respFilt(ts, s, s, A, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Empty response: " << what << endl;

    // R.size() < 2
    try {
        FrequencySequence<complex<float> > s(R);
        s.resize(1);
        respFilt(ts, s, s, A, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Response function size < 2: " << what << endl;

    // Empty C
    try {
        FrequencySequence<complex<float> > s(C);
        s.resize(0);
        respFilt(ts, R, s, A, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Empty sensing function: " << what << endl;

    // C.size() < 2
    try {
        FrequencySequence<complex<float> > s(C);
        s.resize(1);
        respFilt(ts, s, C, A, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Sensing function size < 2: " << what << endl;

    // Empty alphas
    try {
        TimeSeries<complex<float> > s(A);
        s.resize(0);
        respFilt(ts, R, C, s, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Empty alphas array: " << what << endl;

    // Empty gammas
    try {
        TimeSeries<complex<float> > s(G);
        s.resize(0);
        respFilt(ts, R, C, A, s);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Empty gammas array: " << what << endl;

    // Alphas time out of range
    try {
        TimeSeries<complex<float> > s(A);
        s.SetStartTime(GPSTime(600000,0));
        respFilt(ts, R, C, s, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Time series start not in time spanned by alphas: "
                     << what << endl;

    // Gammas time out of range
    try {
        TimeSeries<complex<float> > s(G);
        s.SetStartTime(ts.GetStartTime() + 10000.0);
        respFilt(ts, R, C, G, s);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }

    Test.Check(pass) << "Time series start not in time spanned by gammas: "
                     << what << endl;

    // Zero alpha
    try {
        TimeSeries<complex<float> > a(A);
        a = 0;
        respFilt(ts, R, C, a, G);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Zero alpha: " << what << endl;

    // Zero gamma
    try {
        TimeSeries<complex<float> > g(G);
        g = 0;
        respFilt(ts, R, C, A, g);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Zero gamma: " << what << endl;

    // Zero sensing
    try {
        FrequencySequence<complex<float> > c(C);
        c.Sequence<complex<float> >::operator=(complex<float>(0));
        respFilt(ts, R, c, A, G, RESPFILT_FORWARD);
        pass = false;
    }
    catch(const invalid_argument& e)
    {
        pass = true;
        what = e.what();
    }
    
    Test.Check(pass) << "Zero sensing function: " << what << endl;
}

template<class T>
void TestRespFilt()
{
    const double TOL = 1.0e-06;
    const size_t checkIdx = series.size()/2 - 4;

    bool pass = true;
    
    TimeSeries<T> ts;
    ts.resize(series.size());
    Filters::valarray_copy(ts, series);
    ts.TimeSeriesMetaData::operator=(series);

    respFilt(ts, R, C, A, G);

    Test.Message() << "Sample output:" << endl;
    for (size_t k = 0; k < 8; ++k)
    {
        Test.Message() << filts[checkIdx + k] << "  "
                       << ts[checkIdx + k] << endl;
    }

    Test.Check(filts.size() == ts.size())
        << "Result has correct size" << endl;

    for (size_t k = 0; k < ts.size(); ++k)
    {
        const double diff = std::abs(ts[k] - filts[k]);
        pass = pass && (diff < TOL);
    }

    Test.Check(pass) << "Result has correct values (difference < "
                     << TOL << ")" << endl;

#if 0
    for (size_t k = 0; k < ts.size(); ++k)
    {
        cerr << ts[k] << endl;
    }
#endif

    Test.Check(pass) << "Nominal FORWARD test" << endl;

    // Now check inverse
    Test.Message() << "Backward RespFilt" << endl;

    respFilt(ts, R, C, A, G, RESPFILT_BACKWARD);

    Test.Message() << "Sample output:" << endl;
    for (size_t k = 0; k < 8; ++k)
    {
        Test.Message() << series[checkIdx + k] << "  "
                       << ts[checkIdx + k] << endl;
    }
    
    Test.Check(series.size() == ts.size())
        << "Result has correct size" << endl;

    pass = true;
    for (size_t k = 0; k < ts.size(); ++k)
    {
        const double diff = std::abs(ts[k] - series[k]);
        pass = pass && (diff < TOL);
    }

    Test.Check(pass) << "Result has correct values (difference < "
                     << TOL << ")" << endl;

    Test.Check(pass) << "Nominal BACKWARD test" << endl;
}

void TestRespFiltChain()
{
    const double TOL = 1.0e-06;
    bool pass = true;

    const Scalar<int> forward(0);
    const Scalar<int> backward(1);

    CallChain cmds;
    Parameters args;
    
    cmds.Reset();
    
    cmds.ResetOrAddSymbol("x", series.Clone());
    cmds.ResetOrAddSymbol("R", R.Clone());
    cmds.ResetOrAddSymbol("C", C.Clone());
    cmds.ResetOrAddSymbol("alphas", A.Clone());
    cmds.ResetOrAddSymbol("gammas", G.Clone());
    cmds.ResetOrAddSymbol("forward", forward.Clone());
    cmds.ResetOrAddSymbol("backward", backward.Clone());
    
    // yf = respfilt(x, R, C, alphas, gammas);
    cmds.AppendCallFunction("respfilt",
                            args.set(5, "x", "R", "C", "alphas", "gammas"),
                            "yf");

    // xf = respfilt(yf, R, C, alphas, gammas, backward);
    cmds.AppendCallFunction("respfilt",
                   args.set(6, "yf", "R", "C", "alphas", "gammas", "backward"),
                            "xf");
    
    // x = double(x)
    cmds.AppendCallFunction("double",
                            args.set(1, "x"),
                            "x");
    
    // yd = respfilt(x, R, C, alphas, gammas, forward);
    cmds.AppendCallFunction("respfilt",
                     args.set(6, "x", "R", "C", "alphas", "gammas", "forward"),
                            "yd");

    // xd = respfilt(yd, R, C, alphas, gammas, backward);
    cmds.AppendCallFunction("respfilt",
                   args.set(6, "yd", "R", "C", "alphas", "gammas", "backward"),
                            "xd");
    
    cmds.AppendIntermediateResult( "yd", "result", "Final Result", "" );
    
    cmds.Execute();
    
    Test.Check(pass) << "CallChain executed" << endl;

    // Check single-precision
    const udt* const yfUdt = cmds.GetSymbol("yf");
    
    const TimeSeries<float>& yf
        = dynamic_cast<const TimeSeries<float>&>(*yfUdt);

    Test.Check(filts.size() == yf.size())
        << "Single-precision result has correct size" << endl;

    for (size_t k = 0; k < yf.size(); ++k)
    {
        const double diff = std::abs(yf[k] - filts[k]);
        pass = pass && (diff < TOL);
    }

    Test.Check(pass)
        << "Single-precision result has correct values (difference < "
        << TOL << ")" << endl;

    // Backward
    const udt* const xfUdt = cmds.GetSymbol("xf");
    
    const TimeSeries<float>& xf
        = dynamic_cast<const TimeSeries<float>&>(*xfUdt);

    Test.Check(series.size() == xf.size())
        << "Single-precision reverse result has correct size" << endl;

    for (size_t k = 0; k < xf.size(); ++k)
    {
        const double diff = std::abs(xf[k] - series[k]);
        pass = pass && (diff < TOL);
    }

    Test.Check(pass)
        << "Single-precision reverse result has correct values (difference < "
        << TOL << ")" << endl;

    // Check double-precision
    const udt* const ydUdt = cmds.GetSymbol("yd");
    
    const TimeSeries<double>& yd
        = dynamic_cast<const TimeSeries<double>&>(*ydUdt);

    Test.Check(filts.size() == yd.size())
        << "Double-precision result has correct size" << endl;

    for (size_t k = 0; k < yd.size(); ++k)
    {
        const double diff = std::abs(yd[k] - filts[k]);
        pass = pass && (diff < TOL);
    }

    Test.Check(pass)
        << "Double-precision result has correct values (difference < "
        << TOL << ")" << endl;

    // Backward
    const udt* const xdUdt = cmds.GetSymbol("xd");
    
    const TimeSeries<double>& xd
        = dynamic_cast<const TimeSeries<double>&>(*xdUdt);

    Test.Check(series.size() == xd.size())
        << "Double-precision reverse result has correct size" << endl;

    for (size_t k = 0; k < xd.size(); ++k)
    {
        const double diff = std::abs(xd[k] - series[k]);
        pass = pass && (diff < TOL);
    }

    Test.Check(pass)
        << "Double-precision reverse result has correct values (difference < "
        << TOL << ")" << endl;

    Test.Check(pass) << "CallChain test" << endl;
}

int main(int ArgC, char** ArgV)
{
    Test.Init(ArgC, ArgV);

    try {
        readArray(series, "series.bin");
        series.SetName("H1:LSC-AS_Q");
        series.SetSampleRate(16384);
        series.SetStartTime(GPSTime(71000601, 0));

        readArray(filts, "filts.bin");
        filts.SetName("H1:LSC-AS_Q");
        filts.SetSampleRate(16384);
        filts.SetStartTime(GPSTime(71000601, 0));

        readArray(realA, "alphas.bin");
        realA.SetName("H2:CAL-CAV_FAC");
        realA.SetSampleRate(1/60.0);
        realA.SetStartTime(GPSTime(71000000, 0));

        readArray(realG, "gammas.bin");
        realG.SetName("H2:CAL-OLOOP_FAC");
        realG.SetSampleRate(1/60.0);
        realG.SetStartTime(GPSTime(71000000, 0));

        readArray(R, "response.bin");
        R.SetName("H1:CAL-RESPONSE");
        R.SetFrequencyBase(0.0);
        R.SetFrequencyDelta(1/4.0);

        readArray(C, "sense.bin");
        C.SetName("H1:CAL-CAV_GAIN");
        C.SetFrequencyBase(0.0);
        C.SetFrequencyDelta(1/4.0);

        A.resize(realA.size());
        G.resize(realG.size());

        copy(&realA[0], &realA[realA.size()], &A[0]);
        copy(&realG[0], &realG[realG.size()], &G[0]);

        A.SetName("H2:CAL-CAV_FAC");
        A.SetSampleRate(1/60.0);
        A.SetStartTime(GPSTime(71000000, 0));

        G.SetName("H2:CAL-OLOOP_FAC");
        G.SetSampleRate(1/60.0);
        G.SetStartTime(GPSTime(71000000, 0));
    
        TestExceptions();

        Test.Message() << "Nominal test, respFilt<float>" << endl;
        TestRespFilt<float>();

        Test.Message() << "Nominal test, respFilt<double>" << endl;
        TestRespFilt<double>();

        Test.Message() << "Nominal CallChain test" << endl;
        TestRespFiltChain();
    }
    catch (const std::exception& e) 
    {
	Test.Check(false) << e.what() << std::endl;
    }

    Test.Exit();

    return 0;
}
