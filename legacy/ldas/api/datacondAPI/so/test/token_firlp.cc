#include "datacondAPI/config.h"

#include <general/unittest.h>
#include <filters/FIRLP.hh>
#include <filters/KaiserWindow.hh>

#include "SequenceUDT.hh"
#include "ResampleState.hh"
#include "token.hh"

using namespace std;   
General::UnitTest Test;

//
// Check that firlp() filter really does match default Resample filter
//
void
DefaultTest()
{
    Test.Message() << "firlp() default window test" << endl;

    const int p = 1;
    const int q = 4;

    CallChain chain;
    Parameters args;
    
    chain.Reset();
    
    chain.AppendCallFunction("double", args.set(1, "0.25"), "fc");
    chain.AppendCallFunction("integer", args.set(1, "80"), "order");
    
    chain.AppendCallFunction("double", args.set(1, "5.0"), "beta");
    chain.AppendCallFunction("KaiserWindow", args.set(1, "beta"), "w");

    chain.AppendCallFunction("firlp", args.set(3, "fc", "order", "w"), "b");
    
    chain.AppendIntermediateResult("b", "result", "Final result", "");
    
    chain.Execute();
    
    const CallChain::Symbol* const result = chain.GetSymbol("b");
    const datacondAPI::Sequence<double>& b
        = dynamic_cast<const datacondAPI::Sequence<double>&>(*result);
    
    Test.Check(true) << "firlp() action successfully executed" << endl;
    
    datacondAPI::ResampleState r(p, q);

    datacondAPI::Sequence<double> bCheck;
    
    r.getB(bCheck);
    
    Test.Check(b.size() == bCheck.size())
        << "Filter size is correct" << endl;
    
    bool pass = true;
    for (size_t k = 0; k < bCheck.size(); ++k)
    {
        pass = pass && (b[k] == bCheck[k]);
    }
    
    Test.Check(pass) << "Filters coefficients match default Resample filter"
                     << endl;
}

void
GeneralTest()
{
    Test.Message() << "firlp() general test" << endl;

    CallChain chain;
    Parameters args;
    
    chain.Reset();
    
    chain.AppendCallFunction("double", args.set(1, "0.125"), "fc");
    chain.AppendCallFunction("integer", args.set(1, "50"), "order");
    
    chain.AppendCallFunction("double", args.set(1, "5.0"), "beta");
    chain.AppendCallFunction("HannWindow", args.set(0), "w");

    chain.AppendCallFunction("firlp", args.set(3, "fc", "order", "w"), "b");
    
    chain.AppendIntermediateResult("b", "result", "Final result", "");
    
    chain.Execute();
    
    const CallChain::Symbol* const result = chain.GetSymbol("b");
    const datacondAPI::Sequence<double>& b
        = dynamic_cast<const datacondAPI::Sequence<double>&>(*result);
    
    Test.Check(true) << "firlp() action successfully executed" << endl;
    
    const double fc = 0.125;
    const int order = 50;
    const Filters::HannWindow w;
    
    Filters::FIRLP firlp(fc, order, w);
    datacondAPI::Sequence<double> bCheck;
    
    firlp.apply(bCheck);
    
    Test.Check(b.size() == bCheck.size())
        << "Filter size is correct" << endl;
    
    bool pass = true;
    for (size_t k = 0; k < bCheck.size(); ++k)
    {
        pass = pass && (b[k] == bCheck[k]);
    }
    
    Test.Check(pass) << "Filters coefficients are correct" << endl;
}

int
main(int ArgC, char** ArgV)
{
  Test.Init(ArgC, ArgV);

  Test.Message()
      << "$Id: token_firlp.cc,v 1.4 2005/12/01 22:55:03 emaros Exp $" << endl;

  try {
      
      DefaultTest();
      GeneralTest();

  }
  catch(std::exception& e)
  {
    Test.Check(false) << "std::exception<" << e.what() << ">\n";
  }
  catch(...)
  {
    Test.Check(false) << "firlp test threw an exception\n";
  }

  Test.Exit();
}
