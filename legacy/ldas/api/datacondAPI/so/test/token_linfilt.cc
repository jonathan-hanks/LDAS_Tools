#include "datacondAPI/config.h"

#include <complex>

#include "general/unittest.h"

#include "token.hh"		// Common header for token testing programs.
#include "LinFilt.hh"		// Needed for computing linear filter results.
#include "ScalarUDT.hh"		// Needed for converting CallChain results.
#include "TimeSeries.hh"	// Needed for converting CallChain results.

using namespace std;
using namespace datacondAPI;

General::UnitTest	Test;	// Class supporting testing

//-----------------------------------------------------------------------
// This template functions is a pattern for testing the functionality
//   of linear filter.
//-----------------------------------------------------------------------

template<class T>
void linear_filter_type(const std::string& type_name, const std::string& array_name)
{
  Test.Message() << "LinFilt<" << type_name << "," << type_name << ">: start"
		 << std::endl;

  try
  {
    // call chain version

    CallChain cmds;
    Parameters args;
    
    cmds.Reset();

    Test.Message() << "...appending classes...\n";    

    cmds.AppendCallFunction("integer", args.set(1, "4"), "N");
    cmds.AppendCallFunction("double", args.set(1, "1.0"), "dbase");
    cmds.AppendCallFunction("double", args.set(1, "1.0"), "sbase"); // no native float
    cmds.AppendCallFunction("scomplex", args.set(2, "1.0", "0.0"), "cbase");
    cmds.AppendCallFunction("dcomplex", args.set(2, "1.0", "0.0"), "zbase");
    cmds.AppendCallFunction("dvalarray", args.set(2, "dbase", "N"), "b");
    cmds.AppendCallFunction("dvalarray", args.set(2, "dbase", "N"), "a");
    if (array_name == "svalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "sbase", "N"), "x");
    }
    else if (array_name == "dvalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "dbase", "N"), "x");
    }
    else if (array_name == "cvalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "cbase", "N"), "x");
    }
    else if (array_name == "zvalarray")
    {
      cmds.AppendCallFunction(array_name, args.set(2, "zbase", "N"), "x");
    }
    // cmds.AppendCallFunction("double", args.set(1, "1.0"), "z2"); // wrong type on purpose

    Test.Message() << "...appending functions...\n";        

    // y1 = linfilt(b, x) // check stateless
    
    cmds.AppendCallFunction("linfilt", args.set(2, "b", "x"), "y1");

    // y2 = linfilt(b, a, x) // check stateless

    cmds.AppendCallFunction("linfilt", args.set(3, "b", "a", "x"), "y2");

    // y3 = linfilt(b, x, z1) // create state

    cmds.AppendCallFunction("linfilt", args.set(3, "b", "x", "z1"), "y3");

    // y4 = linfilt(b, x, z1) // overwrite state

    cmds.AppendCallFunction("linfilt", args.set(3, "b", "x", "z1"), "y4");

    // y5 = linfilt(b, a, x, z2) // create state

    cmds.AppendCallFunction("linfilt", args.set(4, "b", "a", "x", "z2"), "y5");

    // y6 = linfilt(b, a, x, z2) // overwrite state

    cmds.AppendCallFunction("linfilt", args.set(4, "b", "a", "x", "z2"), "y6");

    // y7 = linfilt(x, z1) // use state

    cmds.AppendCallFunction("linfilt", args.set(2, "x", "z1"), "y7");

    // y8 = linfilt(x, z2) // use state

    cmds.AppendCallFunction("linfilt", args.set(2, "x", "z2"), "y8");

    cmds.AppendIntermediateResult( "y8", "result", "Final Result", "" );

    // execute

    Test.Message() << "...executing...\n";

    cmds.Execute();

    Test.Message() << "...generating...\n";
    
    // c++ version

    const datacondAPI::Sequence<double> cpp_b(1.0, 4);
    const datacondAPI::Sequence<double> cpp_a(1.0, 4);

    const datacondAPI::Sequence<double> dummy_b(1.0, 1);
    const datacondAPI::Sequence<double> dummy_a(1.0, 1);

    const datacondAPI::Sequence<T> cpp_x(1.0, 4);

    datacondAPI::Sequence<T> cpp_y134;
    datacondAPI::Sequence<T> cpp_y256;
    datacondAPI::Sequence<T> cpp_y7;
    datacondAPI::Sequence<T> cpp_y8;

    datacondAPI::LinFiltState cpp_z1(dummy_b, dummy_a);
    datacondAPI::LinFiltState cpp_z2(dummy_b, dummy_a);

    {
      datacondAPI::LinFilt lf = datacondAPI::LinFiltState(cpp_b);
      lf.apply(cpp_y134, cpp_x);
      lf.apply(cpp_y7, cpp_x);
      lf.getState(cpp_z1);
    }
    {
      datacondAPI::LinFilt lf = datacondAPI::LinFiltState(cpp_b, cpp_a);
      lf.apply(cpp_y256, cpp_x);
      lf.apply(cpp_y8, cpp_x);
      lf.getState(cpp_z2);
    }

    Test.Message() << "...checking...\n";

    check_valarray(cpp_y134, cmds.GetSymbol("y1"), "linear filter results (y1)");
    check_valarray(cpp_y256, cmds.GetSymbol("y2"), "linear filter results (y2)");
    check_valarray(cpp_y134, cmds.GetSymbol("y3"), "linear filter results (y3)");
    check_valarray(cpp_y134, cmds.GetSymbol("y4"), "linear filter results (y4)");
    check_valarray(cpp_y256, cmds.GetSymbol("y5"), "linear filter results (y5)");
    check_valarray(cpp_y256, cmds.GetSymbol("y6"), "linear filter results (y6)");
    check_valarray(cpp_y7, cmds.GetSymbol("y7"), "linear filter results (y7)");
    check_valarray(cpp_y8, cmds.GetSymbol("y8"), "linear filter results (y8)");

    //    check_valarray(cpp_z1, cmds.GetSymbol("z1"), "linear filter results (z1)");
    //    check_valarray(cpp_z2, cmds.GetSymbol("z2"), "linear filter results (z2)");

  }
  CATCH(Test);

  Test.Message() << "LinFilt<" << type_name << "," << type_name << ">: end" << std::endl;
}

template<class T>
void zpg_linear_filter(const std::string& type_name,
                       const std::string& array_name)
{
  Test.Message() << "ZPG LinFiltState<" << type_name
                 << "," << type_name << ">: start"
		 << std::endl;

  const complex<double> i(0.0, 1.0);
  TimeSeries<T> x;
  x.resize(128);
  x = T(1);

  x.SetName("H1:LSC-AS_Q");
  x.SetSampleRate(16384.0);
  x.SetStartTime(General::GPSTime(6000000, 0));

  Scalar<double> gain(3.0);
  Sequence<complex<double> > z(2);
  Sequence<complex<double> > p(3);
  
  z[0] = 1.0;
  z[1] = 1.0 + i;
  
  p[0] = 1.0;
  p[1] = 1.0 - 2.0*i;
  p[2] = 1.0 + 3.0*i;

  try
  {
    CallChain cmds;
    Parameters args;
    
    cmds.Reset();

    Test.Message() << "...appending classes..." << endl;    

    cmds.ResetOrAddSymbol("x", x.Clone());

    cmds.ResetOrAddSymbol("zeroes", z.Clone());
    cmds.ResetOrAddSymbol("poles", p.Clone());
    cmds.ResetOrAddSymbol("gain", gain.Clone());

    // z = zpg2linfilt(zeroes, poles, gain);
    cmds.AppendCallFunction("zpg2linfilt",
                            args.set(3, "zeroes", "poles", "gain"),
                            "z");

    // y = linfilt(x, z);
    cmds.AppendCallFunction("linfilt", args.set(2, "x", "z"), "y");

    // y2 = linfilt(y, z);
    cmds.AppendCallFunction("linfilt", args.set(2, "y", "z"), "y2");

    cmds.AppendIntermediateResult( "y2", "result", "Final Result", "" );

    cmds.Execute();

    Test.Message() << "executed" << endl;
  }
  CATCH(Test);

  Test.Message() << "ZPG LinFiltState<"
                 << type_name << "," << type_name << ">: end" << std::endl;
}

//-----------------------------------------------------------------------
// Main
//-----------------------------------------------------------------------

int
main(int ArgC, char** ArgV)
{
  //---------------------------------------------------------------------
  // Initialize UnitTest class
  //---------------------------------------------------------------------

  Test.Init(ArgC, ArgV);

  //---------------------------------------------------------------------
  // Ensure that any error that is thrown by the CallChain is caught.
  //---------------------------------------------------------------------

  try
  {
    //-------------------------------------------------------------------
    // Perform tests
    //-------------------------------------------------------------------

    linear_filter_type<double>("double", "dvalarray");
    linear_filter_type<float>("float", "svalarray");
    linear_filter_type<std::complex<float> >("complex<float>", "cvalarray");
    linear_filter_type<std::complex<double> >("complex<double>", "zvalarray");

    zpg_linear_filter<double>("double", "dvalarray");
  }

  //---------------------------------------------------------------------
  // Display the result
  //---------------------------------------------------------------------

  CATCH(Test);

  //---------------------------------------------------------------------
  // Terminate program with the appropriate exit status.
  //---------------------------------------------------------------------

  Test.Exit();
}
