#include "datacondAPI/config.h"

#include <iomanip>

#include <general/unittest.h>

#include "SequenceUDT.hh"
#include "Coherence.hh"
#include "WelchCSDEstimate.hh"

#include "token.hh"
#include "random.hh"

using namespace General;
using namespace datacondAPI;

using std::complex;   
using std::endl;   
   
UnitTest Test;
static gasdevstate seed;


void gasdev(std::valarray<std::complex<double> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<double>(gasdev(seed), gasdev(seed));
    }
}

template<typename type> std::ostream& operator<<(std::ostream& left, const std::valarray<type>& right)
{
    for (unsigned int i = 0; i < right.size(); ++i)
        left << right[i];
    return left;
}

int main(int ArgC, char** ArgV)
{
    Test.Init(ArgC, ArgV);

    try {
      datacondAPI::Sequence<complex<double> > inX(1024), inY(1024);
      gasdev(inX);
      gasdev(inY);
      
      Coherence coherence;
      
      Sequence<complex<double> > out;

      coherence.apply(out, inX, inY);

      Test.Check(true) << "Trivial apply" << endl;

      {

          datacondAPI::Sequence<complex<double> > x(8), y(8), z(8);
          datacondAPI::WelchCSDEstimate welch(8, 0);

          x[0] = std::complex<double>(-0.397712,-1.51522);
          x[1] = std::complex<double>(-1.24636,-1.13439);
          x[2] = std::complex<double>(-0.84439,-0.938024);
          x[3] = std::complex<double>(0.479696,1.63709);
          x[4] = std::complex<double>(-1.1173,-1.00396);
          x[5] = std::complex<double>(0.277351,-0.400358);
          x[6] = std::complex<double>(-0.0837637,-1.36741);
          x[7] = std::complex<double>(-1.04293,0.674217);

          y[0] = std::complex<double>(0.423066,0.205146);
          y[1] = std::complex<double>(-0.699862,0.635745);
          y[2] = std::complex<double>(-2.69241,-1.20991);
          y[3] = std::complex<double>(0.712633,0.0531042);
          y[4] = std::complex<double>(0.171335,0.803627);
          y[5] = std::complex<double>(1.85922,0.0463263);
          y[6] = std::complex<double>(-0.0427588,1.18396);
          y[7] = std::complex<double>(-0.0149437,0.631178);

          z[0] = std::complex<double>(0.554372,-0.832269);
          z[1] = std::complex<double>(0.969418,-0.245415);
          z[2] = std::complex<double>(0.397844,0.917453);
          z[3] = std::complex<double>(0.954591,0.297919);
          z[4] = std::complex<double>(-0.798117,-0.602503);
          z[5] = std::complex<double>(-0.720501,0.693454);
          z[6] = std::complex<double>(-0.815614,-0.578596);
          z[7] = std::complex<double>(0.38778,-0.921752); 

          Coherence coherence(welch);
          Sequence<complex<double> > z_computed;
          coherence.apply(z_computed, x, y);

          Test.Check(z_computed.size() == z.size()) << "right size\n";
          
          for (unsigned i = 0; i < z.size(); ++i)
              Test.Check(std::abs(z[i] - z_computed[i]) < 1e-3) << "equality\n";
      }
    }
    catch (const std::exception& e) 
      {
	Test.Check(false) << e.what() << std::endl;
    }

    Test.Exit();

    return 0;
}
