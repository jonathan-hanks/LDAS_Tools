#include "datacondAPI/config.h"

#include <iomanip>
#include <complex>   

#include <general/unittest.h>

#include "TimeSeries.hh"
#include "ConcatFunction.hh"

#include "token.hh"

using namespace General;
using namespace datacondAPI;

using std::complex;   
using std::endl;   
   
UnitTest Test;

template<class T>
void TestConcatAction()
{
    const size_t n = 10;

    const double fs = 16384;
    const GPSTime initialTime(6000000, 0);
    
    const double freq = 1.0;
    Sequence<T> data(T(0), n);

    for (size_t k = 0; k < n; ++k)
    {
        data[k] = cos(2*3.14159265*freq*k/n);
    }

    const TimeSeries<T> x0(data, fs, initialTime);
    const TimeSeries<T> x1(data, fs, initialTime + n/fs);
    const TimeSeries<T> x2(data, fs, initialTime + (n + n)/fs);

    TimeSeries<T>* x0Ptr = x0.Clone();
    TimeSeries<T>* x1Ptr = x1.Clone();
    TimeSeries<T>* x2Ptr = x2.Clone();

    x0Ptr->SetName("H1:LSC-AS_Q");
    x1Ptr->SetName("H1:LSC-AS_Q");
    x2Ptr->SetName("H1:LSC-AS_Q");

    CallChain chain;
    Parameters args;

    chain.Reset();
    
    Test.Message( ) << "Checkpoint 1" << std::endl;

    // for concat
    chain.AddSymbol("x0", x0Ptr);
    chain.AddSymbol("x1", x1Ptr);
    chain.AddSymbol("x2", x2Ptr);
    
    Test.Message( ) << "Checkpoint 2" << std::endl;
    // y = concat(x0, x1);
    chain.AppendCallFunction("concat",
                             args.set(2, "x0", "x1"),
                             "y");

    // y = concat(x0, x1, x2);
    chain.AppendCallFunction("concat",
                             args.set(3, "x0", "x1", "x2"),
                             "y");

    Test.Message( ) << "Checkpoint 3" << std::endl;
    // Needed so doesn't throw exception
    chain.AppendIntermediateResult("y", "result", "Final Result", "" );

    Test.Message( ) << "Checkpoint 4" << std::endl;
    try
    {
      Test.Message( ) << "Checkpoint 4.1" << std::endl;
      chain.Execute();
      Test.Message( ) << "Checkpoint 4.2" << std::endl;
    }
    catch( ... )
    {
      Test.Check( false ) << "Caught unknown exception" << std::endl;
    }
    Test.Message( ) << "Checkpoint 5" << std::endl;

    {
        const udt* const result = chain.GetSymbol("y");
        
        const ILwd::LdasElement* const ilwd
            = result->ConvertToIlwd(chain, datacondAPI::udt::TARGET_WRAPPER);
        
	Test.Message( ) << "Checkpoint 5.1" << std::endl;
        dynamic_cast<const ILwd::LdasContainer*>(ilwd)->write(2, 2,
                                                      Test.Message() << endl,
                                                      ILwd::ASCII);
        Test.Message(false) << endl;

	Test.Message( ) << "Checkpoint 5.2" << std::endl;
        delete ilwd;
    }

    Test.Message( ) << "Checkpoint 6" << std::endl;
    // Don't need to delete anything else, obj. registry does it
}

int main(int argc, char** argv)
{
    Test.Init(argc, argv);
    Test.Message() << "$Id: tConcatFunction.cc,v 1.4 2006/10/13 19:32:40 emaros Exp $" << endl;

    try {


        Test.Message( ) << "Testing case 1" << std::endl;
        TestConcatAction<float>();
        Test.Message( ) << "Testing case 2" << std::endl;
        TestConcatAction<double>();
        Test.Message( ) << "Testing case 3" << std::endl;
        TestConcatAction<complex<float> >();
        Test.Message( ) << "Testing case 4" << std::endl;
        TestConcatAction<complex<double> >();

    }
    catch(std::exception& e)
    {
        Test.Check(false) << "Exception: " << e.what() << endl;
    }
    catch( ... )
    {
        Test.Check(false) << "Unknown exception"  << endl;
    }

    Test.Message( ) << "Testing complete" << std::endl;

    Test.Exit();
}
