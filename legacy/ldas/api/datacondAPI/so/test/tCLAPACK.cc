#include "datacondAPI/config.h"

#include <complex>
#include <iostream>

#include "general/unittest.h"

#include "LinearAlgebra.hh"
#include "Matrix.hh"
#include "SequenceUDT.hh"

#include "random.hh"

using namespace datacondAPI;

General::UnitTest Test;

void test_specific()
{
}

template<typename type>
static std::ostream& operator<<(std::ostream& a, const std::valarray<type>& b)
{
    a << "{ ";
    if (b.size() <= 10)
    {
	for (unsigned int i = 0; i < b.size(); ++i)
	{
	    a << b[i] << ' ';
	}
    }
    else
    {
	for (unsigned int i = 0; i < 9; ++i)
	{
	    a << b[i] << ' ';
	}
	a << "... " << b[b.size() - 1] << ' ';
    }
    a << "}";
    return a;
}

static gasdevstate seed(-3141);

static void gasdev(std::valarray<float>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

static void gasdev(std::valarray<double>& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = gasdev(seed);
    }
}

static void gasdev(std::valarray<std::complex<float> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<float>(gasdev(seed), gasdev(seed));
    }
}

static void gasdev(std::valarray<std::complex<double> >& x)
{
    for (unsigned int i = 0; i < x.size(); ++i)
    {
	x[i] = std::complex<double>(gasdev(seed), gasdev(seed));
    }
}

template<typename type>
void test_generic(const std::string& name)
{
    Test.Message() << "type " << name << '\n';

    const unsigned int n = 4;

    // y = A * x

    Sequence<type> x(n);

    Matrix<type> A(n, n);

    for (unsigned int i = 0; i < n; ++i)
    {
	gasdev(x);
	A.setCol(i, x);
    }

    gasdev(x);

    Sequence<type> y;

    LinearAlgebra::MV(y, A, x);

    Test.Message() << "        y = AX = " << y << '\n';

    Test.Message() << "             x = " << x << '\n';

    // X = A^{-1} * Y

    Matrix<type> X;
    Matrix<type> Y(y, n, 1);

    LinearAlgebra::SV(A, X, Y);

    Test.Message() << "X = A^{-1}(Ax) = " << X.getCol(0) << '\n';

    Test.Message() << "         error = " << std::valarray<type>(x - X.getCol(0)) << '\n';

}

template<typename type> void test_matrix(const std::string& name)
{
  Test.Message() << "type " << name << '\n';
  Sequence<type> x;
  for (int a_rows = 1; a_rows < 10; ++a_rows)
    for (int a_columns = 1; a_columns < 10; ++a_columns)
      {
	Matrix<type> A(a_rows, a_columns);
	x.resize(a_rows);
	for (int i = 0; i < a_columns; ++i)
	  {
	    gasdev(x);
	    A.setCol(i, x);
	  }
	Test.Message() << "A(" << a_rows << ", " << a_columns << ");\n";
	for (int b_rows = 1; b_rows < 10; ++b_rows)
	  for (int b_columns = 1; b_columns < 10; ++b_columns)
	    {
	      Matrix<type> B(b_rows, b_columns);
	      x.resize(b_rows);
	      for (int i = 0; i < b_columns; ++i)
		{
		  gasdev(x);
		  B.setCol(i, x);
		}
	      Test.Message() << "B(" << b_rows << ", " << b_columns << ");\n";
	      bool a_transpose = false;
	      do
		{
		  bool b_transpose = false;
		  do
		    {
		      bool c_degenerate = false;
		      do
			{
		          type alpha = gasdev(seed);
			  type beta = c_degenerate ? 0.0 : gasdev(seed);
			  Matrix<type> C;
			  if (!c_degenerate)
			    {
			      C.resize(a_transpose ? a_columns : a_rows, b_transpose ? b_rows : b_columns);
                              x.resize(C.getNRows());
			      for (unsigned int i = 0; i < C.getNCols(); ++i)
				{
				  gasdev(x);
				  C.setCol(i, x);
				}
			    }
			  Test.Message() << "C = " << alpha << (a_transpose ? " * A' * B" : " * A * B") << (b_transpose ? "' + " : " + ") << beta << " * C\n";
			  try
			    {
			      Matrix<type> C_old(C);
			      LinearAlgebra::MM(C, A, B, alpha, beta, a_transpose, b_transpose);
			      Test.Check((a_transpose ? a_rows : a_columns) == (b_transpose ? b_columns : b_rows)) << "no throw from row/column match\n";

			      for (unsigned int i = 0; i < C.getNRows(); ++i)
				for (unsigned int j = 0; j < C.getNCols(); ++j)
				  {
				    type element = std::valarray<type>(((a_transpose ? A.getCol(i) : A.getRow(i)) * (b_transpose ? B.getRow(j) : B.getCol(j)))).sum();
                                    type zero_elem( 0.0 );
				    if (beta == zero_elem)
				      {
					element *= alpha;
					type target = C.getRow(i)[j];
					Test.Check(std::abs(element - target) < 1e-3) << element << " ~= " << target << " (" << element - target << ")\n"; 
				      }
				    else
				      {
					element *= alpha;
					element += beta * C_old.getRow(i)[j];
					type target = C.getRow(i)[j];
					Test.Check(std::abs(element - target) < 1e-3) << element << " ~= " << target << " (" << element - target <<  ")\n";
				      }
				  }
			      
			    }
			  catch (const std::exception& x)
			    {
			      Test.Check((a_transpose ? a_rows : a_columns) != (b_transpose ? b_columns : b_rows)) << "throw caused by row/column mismatch\n";
			    }
			  c_degenerate = !c_degenerate;
			}
		      while (c_degenerate);
		      b_transpose = !b_transpose;
		    }
		  while (b_transpose);
		  a_transpose = !a_transpose;
		}
	      while (a_transpose);
	    }
      }
}

int main(int argc, char** argv)
try
{
    Test.Init(argc, argv);
    
    test_specific();
    test_generic<float>("float");
    test_generic<double>("double");
    test_generic<std::complex<float> >("std::complex<float>");
    test_generic<std::complex<double> >("std::complex<double>");
    test_matrix<float>("float");
    test_matrix<double>("double");
    test_matrix<std::complex<float> >("std::complex<float> ");
    test_matrix<std::complex<double> >("std::complex<double> ");
    Test.Exit();
}
catch (const std::exception& x)
{
    Test.Check(false) << "unexpected std::exception " << x.what() << '\n';
    Test.Exit();
}
