#! /ldcg/bin/tclsh

## ********************************************************
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) script for ldas.
##
## this script exercises all the ldas user commands
## and level 1 and level 2 createRDS
## it requires cmonClient ldas command files
## and it runs on ldas system to be tested
##
## requires test_cleanup.tcl
## Note: cleanup of the database could take a while if the
## number of process rows are large.
## ********************************************************

## DO NOT puts to stderr for ssh pipe runs

set ::auto_path "/ldas/lib . $auto_path"
if	{ ! [ regexp "/ldas/bin" $::env(PATH) ] } {
    set ::env(PATH) "/ldas/bin:$::env(PATH)"
}

set startTime [ clock seconds ]

set cntlmonstate /ldas_outgoing/cntlmonAPI/cntlmon.state
if	{ [ file exist $cntlmonstate ] } {
	source /ldas_outgoing/cntlmonAPI/cntlmon.state
} else {
	catch { exec cat /etc/ldasname } ::RUNCODE
	set ::RUNCODE [ string trim [ string toupper $::RUNCODE ] \n ]
	
}
set API test
source /ldas_outgoing/LDASapi.rsc

package require generic

source /ldas_outgoing/cntlmonAPI/LDAScntlmon.rsc

## ******************************************************** 
##
## Name: MydebugPuts
##
## Description:
## output debug or interim results to a log file
##
## Usage:
##       
## Comments:

proc MydebugPuts { msg } {
	puts $::fout $msg
	flush $::fout
}

proc addLogEntry { args } {

	MydebugPuts [ file join $args ]
}

## ******************************************************** 
##
## Name: exitTest
##
## Description:
## record results so far, cleanup and exit
##
## Usage:
##       
## Comments:

proc exitTest {} {

	set details ""
	if	{ [ info exist ::fout ] } {
		flush $::fout
		seek $::fout 0 start
		set details [ read -nonewline $::fout ]
		close $::fout
		;## strip embedded html 
		regsub -all {<html>} $details {html}  details
		regsub -all {</html>} $details {/html}  details
	}
	if	{ ( $::NUMSENT == $::NUMRECV ) && $::NUMSENT} {
		if	{ !$::::NUMFAIL } {
			set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, <font color='$::darkgreen'>TEST PASSED</font>"
		} else {
			set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, <font color='red'> TEST FAILED</font>"
		}
	} else {
		set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, <font color='red'>TEST FAILED</font>"
	}
	
    if  { [ info exist ::flock ] } {
	    file delete -force $::LOCKFILE 
	    catch { file delete -force $::USER_COMMANDS_CANCEL } err
    }

	set endTime [ clock format [ clock seconds ] -format "%x %X %Z" ]
	set startTime [ clock format $::startTime -format "%x %X %Z" ]
    
    ;## will be captured by cntlmon via pipe
	puts "<html><pre>From $startTime - $endTime\n$text\n$details\n</pre></html>"
    RawGlobusClient::cleanup
	exit
}

## ******************************************************** 
##
## Name: outputInterimReport
##
## Description:
## display interim results if desired by user
##
## Usage:
##       
## Comments:
;## do not flush stdout or it will hang

proc outputInterimReport {} {
  	
    if  { $::INTERIM } {   
        if  { ! [ expr ${::NUMRECV}%$::INTERIM ] && ${::NUMRECV} >= $::INTERIM } {          
	        set fd [ ::open $::LOGFILE r ]
	        set details [ ::read -nonewline $fd ]
            ::close $fd
            set endTime [ clock format [ clock seconds ] -format "%x %X %Z" ]
	        set startTime [ clock format $::startTime -format "%x %X %Z" ] 
            set numRemain [ expr $::NUMTESTS - $::NUMSENT ]
            set text "User $::USER submitted $::NUMSENT jobs, received $::NUMRECV replies, $::::NUMFAIL failed, $numRemain more tests to run"
            ::puts "<html><pre>From $startTime - $endTime\n$text\n$details\n</pre></html>"
        }
    }

}    

## ******************************************************** 
##
## Name: strip
##
## Description:
## strip ldas job reply
##
## Usage:
##       
## Comments:

proc strip {data} {
    regsub -all -- {[\n\s\t]+} $data { } data
    set idx [string first "=====" $data]
    if {$idx >= 0} {
        set data [string replace $data $idx end]
    }
    return [string trim $data]
}

## ******************************************************** 
##
## Name: killer
##
## Description:
## remove 
##
## Usage:
##       
## Comments:

;## remove all running program of a pattern
proc killer { pattern } {
	catch { exec ps -A -o "pid,args" | grep $pattern } data
   	foreach line [ split $data \n ]  {
    	if	{ [ regexp {(\d+).+sh} $line -> pid ] } {
            MydebugPuts "killing $pattern $pid, $line"
            catch { exec kill -9 $pid }
        }
    }
}

## ******************************************************** 
##
## Name: numTests2Run
##
## Description:
## determine the total number of tests to run
##
## Usage:
##       
## Comments:

proc numTests2Run {} {

    ;## count tests from cmonClient 
    catch { exec ls -lR ${::cmddir} | grep "\\\.cmd" } data
    catch { exec ls -lR ${::cmddir} | grep createRDS_ } data1
    catch { exec ls -lR ${::cmddir} | grep createRDS_H } data2
    catch { exec ls -lR ${::cmddir} | grep createRDS_L } data3
	catch { exec ls -lR ${::cmddir} | grep createRDS_mit } data4
    
    set numRDS 0
    set numRDS_LHO	0
    set numRDS_LLO 	0
    set numRDS_MIT	0
    
    set ::NUMTESTS [ llength [ split $data \n ] ]
    if	{ [ regexp {createRDS_} $data1 ] } {	    		
    	set numRDS [ llength [ split $data1 \n ] ]
    }
    if	{ [ regexp {createRDS_H} $data2 ] } {
    	set numRDS_LHO [ llength [ split $data2 \n ] ]
    }
    if	{ [ regexp {createRDS_L} $data3 ] } {
    	set numRDS_LLO [ llength [ split $data3 \n ] ]
    }
    if	{ [ regexp {createRDS_mit} $data4 ] } {
		set numRDS_MIT [ llength [ split $data4 \n ] ]
    }
    
	switch -regexp -- $::SITE {
			mit  { set ::NUMTESTS [ expr $::NUMTESTS - $numRDS + $numRDS_MIT ] }
			llo  { set ::NUMTESTS [ expr $::NUMTESTS - $numRDS + $numRDS_LLO ] }
			lho  { set ::NUMTESTS [ expr $::NUMTESTS - $numRDS + $numRDS_LHO ] }
			default  {}
	}

    ;## include the ones submitted internally
    incr ::NUMTESTS [ llength $::moretests ]
    ;## MydebugPuts "::NUMTESTS $::NUMTESTS numRDS $numRDS numRDS_LHO $numRDS_LHO numRDS_LLO $numRDS_LLO other tests [ llength $::moretests ]"
    MydebugPuts "Total number of tests to run on $::SITE is $::NUMTESTS"

}

## ******************************************************** 
##
## Name: bgerror 
##
## Description:
## Handles uncaught errors
##
## Usage:
##
## Comments:

proc bgerror { msg } {
     puts stderr "bgerror: $msg"
	 puts stderr $::errorInfo 

     set strlist [ split $msg ]
     set index [ lsearch $strlist "*sock*" ]
     if  { $index > -1 } {
         set sid [ lindex $strlist $index ]
         regsub -all  {[\"]} $sid {} sid 
         puts "bgerror sid = $sid."
    }
}


## ******************************************************** 
##
## Name: sendJob
##
## Description:
## enter state1 by opening socket to server via RawGlobusClient XIO 
##
## Usage:
##
## Comments:

proc sendJob { usercmd cmd tag args } {

	if	{ [ file exist $::CANCELFILE ] } {
			MydebugPuts "<font color='red'>Test cancelled by user</font>"
			exitTest
    }
	catch { unset ::DONE }
    catch { unset ::ALLDONE }
	if	{ [ catch {

    set retval ""  
    set tmp [ list ] 
    ;## strip comments and new lines 
    foreach line [split $cmd "\n"] {
        set line [string trim $line]
        if { [regexp {^\s*#} $line ] } {
            continue
        }
        lappend tmp $line
    }
    set cmd [join $tmp "\n"]
	set cmd [subst -nobackslashes $cmd]
	regsub -all -- {[\s\n\t]+} $cmd { } cmd
    
	if	{ [ string equal $usercmd $tag ] } {
		set text ""
	} else {
		set text $tag
	}
	
    regsub -all -- {[\s\n\t]+} $cmd { } cmd
    
    	set key [ key::time ]
		RawGlobusClient::init $key host
		
		set ldasjob ldasJob
        ;## get the name from cert 
		
		lappend ldasjob "-name $::USER -password x.509 -email persistent_socket"
		lappend ldasjob $cmd
		set ::${key}(callback) [ list getResult $usercmd $cmd $text $key ]
        if	{ ![ string equal abortJob $usercmd ] } {
        	set ::${key}(done) ::DONE
        } else {
        	set ::${key}(done) ::ALLDONE
        }
        RawGlobusClient::connect $::contact_string $key  
    	RawGlobusClient::sendCmd $ldasjob $key
               
        #lassign [ globus_xio_register_read $::RawGlobusClient::networkhandle($key) $::RawGlobusClient::buffersize \
    	# $::RawGlobusClient::waitforbytes NULL \
    	#[ list ldasJobCallback $key ] ] result
        # MydebugPuts "register read callback result for key $key $result" 		
    } err ] } {
    	incr ::NUMFAIL 1
        MydebugPuts "\nsocket error: \n'$cmd'\n$err\n*** $usercmd $text-----FAILED"
        catch { unset ::${key} }
    }
    vwait [ set ::${key}(done) ]
	outputInterimReport

}

## ******************************************************** 
##
## Name: readDataCallback
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:
## a callback is registered each time to listen to server data
## may return "An end of file occurred" from result if there is nothing

proc ldasJobCallback { user_parms handle result buffer data_desc } {

	if	{ [ catch {
       
		#MydebugPuts "readCallback read callback $user_parms handle $handle result  \
		#	$result buffer [ string length $buffer ] bytes\n$buffer"
        
		set error 0
		set key [ lindex $user_parms 0 ]
        if	{ [ regexp -nocase {end of file} $result ] } {
    		set color blue 
            RawGlobusClient::close $key
            catch { unset ::$key }
			set ::DONE 1    
		}
       	if	{ [ string length $result ] } {
			set usercmd [ lindex [ set ::${key}(callback) ] 1 ]
			MydebugPuts "$usercfmd Error $result"
        	RawGlobusClient::close $key
            catch { unset ::$key }
            set error 1
       	}
        
		if	{ [ string length $buffer ] && ! $error } {
        	set data [ set ::${key}(callback) ]
			set callback [ lindex $data 0 ]
			set args [ lrange $data 1 end ] 
		
			lappend args $buffer
        	eval $callback $args 
       
        	lassign [ globus_xio_register_read $handle $::RawGlobusClient::buffersize \
    	 	$::RawGlobusClient::waitforbytes NULL \
    	 	[ list ldasJobCallback $key ] ] result
            # MydebugPuts "register another callback key $key result $result"
		}
    } err ] } {    	
    	MydebugPuts "ldasJobCallback error $err, closing socket" $color
		set ::DONE 1   
    }

}

## ******************************************************** 
##
## Name: getResult
##
## Description:
## handler for email replies from mgr
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments
## some debug statements for globus can cause cmonClient errors in html rendering

proc getResult { usercmd cmd text key args } {
    set i 0
	set reply [ lindex $args 0 ]
    # MydebugPuts  "usercmd $usercmd reply\n$reply"    
	if	{ [ regexp "Your job is running.+(${::RUNCODE}\\d+)" $reply -> jobid ] } {
		incr ::NUMSENT 1
        set ::${key}(jobid) $jobid   
        if	{ [ info exist ::JOB2ABORT ] && $::JOB2ABORT == 1 } {
        	set ::JOB2ABORT $jobid
            set ::DONE 1
            # MydebugPuts "jobid to abort is $jobid"
        } 
    	return
    }
    
    ;## dont count the job to be aborted as part of the test
    if 	{ [regexp -nocase -- "(${::RUNCODE}\\d+)" $reply -> jobid] } {
    	if	{ [ info exist ::JOB2ABORT ] && [ string equal $::JOB2ABORT $jobid ] } {
        	set pat "${::JOB2ABORT}.+(killed|aborted)"
            if	{ [ regexp -nocase $pat $reply ] } {
            	MydebugPuts "$::JOB2ABORT was aborted by abortJob command"
                incr ::NUMSENT -1
                unset ::JOB2ABORT
                return
            }
        }
		incr ::NUMRECV 1
        set errmsg TBD
        ;## do not logged the job used to test abortJob
    	if	{ [ regexp -nocase -- {error![\n\s]*([^=]+)} $reply -> errmsg ] &&
        	 ! [ string equal abortJob $usercmd ] } {
    		incr ::NUMFAIL 1			
        	MydebugPuts "\nJob Error: $cmd\n[string trim $errmsg]\n*** $jobid <font color='red'>$usercmd $text-----FAILED</font>"
    	} elseif { [ regexp -nocase -- {aborted|killed} $reply -> errmsg ] } {
        	MydebugPuts "\n*** $jobid <font color='red'>$usercmd $text aborted-----FAILED</font>"
        } else {
    		MydebugPuts "\n*** $jobid <font color='$::darkgreen'>$usercmd $text-----PASSED</font>"
        }
    	lappend ::WIPE_LIST $jobid	
        if	{ [ string equal abortJob $usercmd ] } {
        	set ::ALLDONE 1 
        } 
    } else {
    	incr ::NUMFAIL 1
    	MydebugPuts "Error: $reply"
    }
}

## ********************************************************
## MAIN
## ********************************************************

;## initialization

set ::LOCKFILE $::TOPDIR/cntlmonAPI/.user_commands.lock
set ::LOGFILE $::TOPDIR/cntlmonAPI/user_commands.log
set	::CANCELFILE $::TOPDIR/cntlmonAPI/$::USER_COMMANDS_CANCEL
set ::WIPE_LIST [ list ]
set ::SHAREDDIR /ldas/share/ldas
set ::ABORTCMD $::SHAREDDIR/ldascmds/dataStandAlone/stochastic.cmd
set ::darkgreen #568c3a
set ::moretests [ list getChannels cacheGetTimes getFrameCache activeJobSummary apiStatusSummary rmJobFiles abortJob ]

set ::DEBUG 0

set ::contact_string "${::GLOBUS_MANAGER_API_HOST}:${::TCLGLOBUS_HOST_PORT}"

;## job counters
set ::NUMSENT 0
set ::NUMRECV 0
set ::::NUMFAIL 0
set ::LATERCMDS [ list ]
set ::TIMEOUT_RECV 1000

switch -regexp -- $::LDAS_SYSTEM {
	ldas-dev 	{ set ::SITE dev }
	ldas-test 	{ set ::SITE test }
	ldas-cit	{ set ::SITE cit }
	ldas-wa		{ set ::SITE lho }
	ldas-la		{ set ::SITE llo }
    ldas-mit    { set ::SITE mit }
	tandem*		-
	default		{ set ::SITE $::env(HOST) }
}

catch { exec uname -n } ::HOST

;## only 1 copy runs at a time
if	{ [ file exist $::LOCKFILE ] } {

	set fd [ open $::LOCKFILE r ]
    set data [ read $fd ]
    close $fd
    regexp {(\d+)} $data -> thatpid
   
	catch { exec ps -Ao fname,pid,args | grep $thatpid } data
    
    foreach line [ split $data \n ] { 
    	if	{ [ regexp -- "(\\S+)\\s+$thatpid\\s+(.+)" $line -> prog rest ] } {
        	if	{ [ string match ${prog}* [ file tail $::argv0 ] ] } {
            	puts  "<html><pre><font color='red'>ERROR: Another copy of user command test is currently running \
				(remove lock file $::LOCKFILE to run test if you think this is not true).\n</font></pre></html>"
                exit
            }
         }
    }
}

;## check for cancellation
if	{ [ file exist $::CANCELFILE ] } {
	puts  "<html><pre><font color='red'>ERROR: Test cancelled by user before any jobs have been submitted.\n</font></pre></html>"
	exit
}

if  { $argc } {
    set ::INTERIM [ lindex $argv 0 ]
} else {
    set ::INTERIM 0
}

;## get a user
set info $::env(HOME)/.user_commands
source $info
set ::pw [ key::md5 [ binaryDecrypt $::CHALLENGE [ base64::decode64 $::pw ] ] ]  
file delete -force $info

set ::userinfo "-name $::USER -password x.509 -email persistent_socket"

;## write out a log 
catch { file delete $::LOGFILE }
set ::fout [ open $::LOGFILE a+ ]

set ::flock [ open $::LOCKFILE w ]
puts $::flock [ pid ]
::close $::flock	

catch { exec test_cleanup.tcl } err
# MydebugPuts "cleanup $err"

;## get jobs cmds from cmonClient lib
set ::cmddir [ file join $::SHAREDDIR ldascmds ]
set date [ clock format [ clock seconds ] -format "%m%d" ]

;## compute the number of jobs to run
numTests2Run

if  { $::NUMTESTS <= $::INTERIM || !$::INTERIM } {
    set ::INTERIM 0
    MydebugPuts "No interim results are displayed."
} else {
    MydebugPuts "Display interim results after every $::INTERIM tests\n"
} 

;## just run level1 RDS first 

foreach subdir [ lsort [ glob -nocomplain $cmddir/* ]] {
	set usercmd [ file tail $subdir ]
	regsub {\.cmd} $usercmd {} usercmd
	
	if	{ [ string equal createRDS [ file tail $subdir ] ] } {
		switch -regexp -- $::SITE {
			mit { set cmds [ glob -nocomplain $subdir/createRDS_mit*.cmd ] }
			llo  { set cmds [ glob -nocomplain $subdir/createRDS_L_*_1.cmd ] 
				   set ::LATERCMDS [ glob -nocomplain $subdir/createRDS_L_*_2.cmd ] }
			lho  { set cmds [ glob -nocomplain $subdir/createRDS_H_*_1.cmd ] 
				   set ::LATERCMDS [ glob -nocomplain $subdir/createRDS_H_*_2.cmd ]}
			default  { set cmds [ glob -nocomplain $subdir/createRDS*_1.cmd ]
                    lappend cmds $subdir/createRDS_mit.cmd
				   set ::LATERCMDS [ glob -nocomplain $subdir/createRDS*_2.cmd ] }
		}
	} else {
    	set cmds [ lsort [ glob -nocomplain $subdir/*.cmd ] ]
	}
    
    foreach cmd $cmds {
		set tag [ file tail $cmd ]
		regsub {\.cmd} $tag {} tag         
		set fd [ open $cmd r ]
		set cmdtext [ read -nonewline $fd ]
		::close $fd
        sendJob $usercmd $cmdtext $tag
    }
} 

;## additional cmds not in cmonClient
;## site specific cmds
if	{ [ string match llo $::SITE ] } {	
	set ifo L
} else {
	set ifo H
}

;## getChannels
switch -regexp -- $::SITE {
	llo -
	lho -
	cit -
	mit  { set cmd "getChannels  -returnprotocol http://daq -interferometer $ifo -frametype {RDS_R_L3} -time 756518892  -metadata 1" }
	dev -
	test { set cmd "getChannels  -returnprotocol http://daq -interferometer $ifo -frametype {RDS_R_L3} -time 733042094  -metadata 1" }
}
	
sendJob getChannels $cmd getChannels

;## cacheGetTimes
set cmd "cacheGetTimes -types {RDS_R_L3 RDS_R_L2 RDS_R_L1 R} -ifos {H L HL G} -start 751651213 -end 757699213"
sendJob cacheGetTimes $cmd cacheGetTimes

;## getFrameCache
set cmd "getFrameCache -returnprotocol http://frame.cache"
sendJob getFrameCache $cmd getFrameCache

;## activeJobSummary
set cmd "activeJobSummary -format html"
sendJob activeJobSummary $cmd activeJobSummary

set cmd "apiStatusSummary -apis -all -format link -save 0"
sendJob apiStatusSummary $cmd apiStatusSummary

;## now run level 2 createRDS

foreach file $::LATERCMDS {
	set tag [ file tail $file ]
	set fd [ open $file r ]
	set cmd [ read -nonewline $fd ]
	::close $fd
	sendJob createRDS $cmd $tag
}

;## test rmJobFiles
if  { [ llength $::WIPE_LIST ] } {
	set cmd "rmJobFiles -jobids [ list $::WIPE_LIST ] -extensions {all}"
	sendJob rmJobFiles $cmd rmJobFiles
} 

## ******************************************************** 
##
## Name: issueAbortJob
##
## Description:
## test abortJob by submitting a long job and then abort it
##
## Usage:
##       
## Comments:

proc issueJob2Abort {} {
	
	set fd [ open $::ABORTCMD r ]
	set cmd [ read -nonewline $fd ]
	::close $fd
	
	#regsub -all -- {[\n\s\t]+} $cmd { } cmd
	set cmd [subst -nobackslashes $cmd]
	set cmd [ string trimleft $cmd " " ] 
	
    set ::JOB2ABORT 1
    sendJob dataStandAlone $cmd dataStandAlone 0
}

proc issueAbortJob {} {
    
    if	{ [ info exist ::JOB2ABORT ] && $::JOB2ABORT != 1 } {
		set cmd "abortJob -stopjob $::JOB2ABORT:"
   		# MydebugPuts "issue abortJob to jobid $::JOB2ABORT"
    	sendJob abortJob $cmd abortJob 
    } else {
    	MydebugPuts "Unable to issue abortJob: no jobid to abort"    
    }       

}

issueJob2Abort
issueAbortJob
exitTest
