#! /ldcg/bin/tclsh


## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor server Tcl Script.
##
## This is the main script called by cntlmonAPI
## to bring in the cntlmon packages which all
## uses cntlmon namespace.
## 
## This module sources the following sub-modules:
##   1. cntlmonClient.tcl (handles client connection and commands)
##   2. cntlmonFile.tcl (handle request for system load and status)
##   3. cntlmonLogs.tcl (handles logging request)
##   4. cntlmonMPI.tcl (handles MPI requests)
##   5. cntlmonLDAS.tcl (handles LDAS startup/shutdown request)
##   6. cntlmonJobs.tcl (handles LDAS job queue requests)
## ;#ol
## Request calls are made from client handler namespace in 
## cntlmonClient.tcl.
## Some commands assumed that they are being called by the client
## handler. 
##
## API Interactions:
## cntlmonAPI server interacts with the following APIs
## 1. managerAPI
## 2. mpiAPI
## 3. metadataAPI ( indirectly via its utility db2utils)
##
## LDAS file Interactions:
## cntlmonAPI server parses the LDAS logs and log archive.
##
## ******************************************************** 
;#barecode

package provide cntlmon 1.0

namespace eval cntlmon {
	set clientCount 0
    set activejobs 0
    set varlist { apitimes apistatus }
    
    ;## offsets to parse memory, cpu and thread data
    set OFFSET_JOBS 1
    set OFFSET_YMIN 2
    set OFFSET_YMAX 3
    set OFFSET_RESTART 4
    set OFFSET_APIDATA 5
    set OFFSET_CPUDATA 6
    set OFFSET_CORETIMES 7
    set OFFSET_THREADDATA 8

}

package require cntlmonClient 1.0
package require cntlmonJobs 1.0
package require cntlmonFile 1.0
package require cntlmonLogs 1.0
package require cntlmonMPI 1.0
package require cntlmonLDAS 1.0
package require manager 1.0

;#end

## ******************************************************** 
##
## Name: ${API}::throttle 
##
## Description:
## controls the number of clients it can service at a given time
##
## Usage:
##       
##
## Comments:
## 

proc ${API}::throttle {} {
	while	{ $::cntlmon::activejobs > $::MAX_NUMBER_OF_CLIENTS } {
		vwait ::cntlmon::activejobs
        #waitforAny 5000 ::cntlmon::activejobs
	}
	return {}
}

## ******************************************************** 
##
## Name: sendPort 
##
## Description:
## sends the msg to the designated host and port
## usually this is controlmon emergency port to log msg
##
## Parameters:
##
## Usage:
## 
## Comments
## this function is called by parseLines which is called
## when tail greps the log file for pattern
## it is called by currDspace

proc sendPort { msg host port } {

        if 	{ [ catch { 
            set sid [ socket $host $port ]  
            fconfigure $sid -buffering line          
            puts $sid "$::ENCODEDMGRKEY $msg"
            close $sid
        } err ] } {
            catch { close $sid }
		    return -code error "failed to write $host:$port : $err"
        }

}

## ******************************************************** 
##
## Name: ${API}::init 
##
## Description:
## initialization
##  start up standalone utilities to:
##  monitor API logs for email
##  monitor database disk space, database manager, insertion rates.
##
## Usage:
##       
##
## Comments:
## must be short because manager needs to communicate right away

proc ${API}::init {} {
    
    if  { ! [ info exist ::LOGFILE_BUFSIZE ] } {
        set ::LOGFILE_BUFSIZE 81920
    }

    if  { ! [ info exist ::MAX_STDOUTERR ] } {
        set ::MAX_STDOUTERR 1048576
    }
    if  { ! [ info exist ::DELAY_UTILS_MONITOR ] } {
        set ::DELAY_UTILS_MONITOR 3600000
    }
    ;## default to monitor manager every hour
    if  { ! [ info exist ::DELAY_MGR_MONITOR ] } {
        set ::DELAY_MGR_MONITOR 3600000
    }
    ;## default to tclglobus socket timeout to 5 secs
    if  { ! [ info exist ::TCLGLOBUS_SOCKET_TIMEOUT ] } {
        set ::TCLGLOBUS_SOCKET_TIMEOUT 5
    }
    ;## remake this dir to incorporate changes
    set ::CLIENTLOGDIR [ file join $::LDASLOG cmonClient ]
    catch { file delete -force $::CLIENTLOGDIR }
    file mkdir $::CLIENTLOGDIR 
    gifBalls $::CLIENTLOGDIR
    styleSheets $::CLIENTLOGDIR
    file attributes $::CLIENTLOGDIR -permissions 0755

    ;## each day clean up jobs and logs
    if  { ! [ info exist ::CLEAR_OUTPUT_PERIOD ] } {
        set ::CLEAR_OUTPUT_PERIOD 86400000
    }
    ;## set limit for test report to flag the leak KB/sec
    if  { ! [ info exist ::MEM_LEAK_YELLOW_LIMIT ] } {
        set ::MEM_LEAK_YELLOW_LIMIT 0.3
    }    
    ;## set limit for test report to flag the leak KB/sec
    if  { ! [ info exist ::MEM_LEAK_RED_LIMIT ] } {
        set ::MEM_LEAK_RED_LIMIT 2.0
    }
    if  { ! [ info exist ::DAYS_TO_CERT_EXPIRE ] } {
        set ::DAYS_TO_CERT_EXPIRE 30
    }

    catch { eval file delete -force [ glob $::TOPDIR/cntlmonAPI/client*.log ] } err
    puts $err
    
    ;## remove lock files
    catch { eval file delete -force [ glob $::TOPDIR/cntlmonAPI/.user* ] } err
    catch { eval file delete -force [ glob $::TOPDIR/cntlmonAPI/.node* ] } err
    
    cntlmon::setVars
    
    after 1000  ::bootLock ON
    after 2000  ${::API}::initVars
    after 3000  ${::API}::ldasinit
	after 5000 ${::API}::bootlogscan
    after 8000 ${::API}::bootcoreWatch
    after 10000 ${::API}::bootlogCacheUtils
	after 11000 ${::API}::bootdb2utils  
	after 30000 ${::API}::openClientSock
	after 30000 ${::API}::stdOutErr	

    after $::DELAY_MGR_MONITOR cntlmon::checkMgrUp
      
    ;## monitor utilities
    after $::DELAY_UTILS_MONITOR [ list cntlmon::ckutils logscan $::CNTLMON_API_HOST 1 ]
    
    ;## only monitor database if metadata or metaserver exist
    if	{ [ regexp {metadata} $::API_LIST ] && [ info exist ::METADATA_API_HOST ] } {
    	after $::DELAY_UTILS_MONITOR [ list cntlmon::ckutils db2utils $::METADATA_API_HOST 1 ]
    }
    after $::DELAY_UTILS_MONITOR [ list cntlmon::ckutils coreWatch $::COREHOST 1 ]
    after $::DELAY_UTILS_MONITOR [ list cntlmon::ckutils logCacheUtils $::CNTLMON_API_HOST 1 ]
    after 100 [ list cntlmon::cleanupLDASoutput ]
    trace variable ::beowulfNodes w cntlmon::updateClientNodes
    after 10000 cntlmon::getNodeNames
    after 10000 cntlmon::openJobPort
    after 10000 cntlmon::updateDBtabcols
    after 10000 cntlmon::getMountPoints
   	after 20000 cntlmon::openGSISockets
    
    ;## check for ldas cert expiration every day
    bgLoop cntlmon_expiredCertTest "cntlmon::expiredCertTest" 86400
}

## ******************************************************** 
##
## Name: ${API}::dropdotfile
##
## Description:
## drop off the dot file for utilities
## 
## Usage:
##       
##
## Comments:

proc cntlmon::dropdotfile {} {
    
    set dotfile ".. "
    if  { [ info exist ::cntlmon::deldotfileId ] } {
        after cancel $::cntlmon::deldotfileId 
    } 
    set ::cntlmon::deldotfileId  [ after 100000 cntlmon::deldotfile ]
    if  { [ file exist $dotfile ] } {
        return
    }
    if  { [ catch {
        set fid [ open $dotfile w ]
        file attributes $dotfile -permissions 0600
        puts $fid $::MGRKEY
        ::close $fid
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: ${API}::deldotfile
##
## Description:
## remove dotfile
## 
## Usage:
##       
##
## Comments:

proc cntlmon::deldotfile {} {
    
    set dotfile ".. "
    if  { [ catch {
        if  { [ file exist $dotfile ] } {
            file delete -force $dotfile
        }        
    } err ] } {
        addLogEntry "Delete dot error: $err"
    }
    unset ::cntlmon::deldotfileId
}

## ******************************************************** 
##
## Name: ${API}::remoteBrowser 
##
## Description:
## allow a client to select a file from a remote machine
## via the server 
## Usage:
##       
##
## Comments:
## 
proc ${API}::remoteBrowser { path {offset 0} } {
    if  { [ catch {
        set result "0\n"
		if	{ [ regexp {^/ldas_outgoing} $path ] } {
			if	{ [ file isdirectory $path ] } {
        		set entries [glob -nocomplain [file join $path "*"]] 
				set dirs [ list ]
				set files [ list ]
				foreach f $entries {
					if { [file isdirectory $f] } {
					lappend dirs $f
					} else {
					lappend files $f
					}
				}
                set dirs [ lsort -dictionary $dirs ]
                set files [ lsort -dictionary $files ]
				set result "0\ndir|0|\{$dirs\} \{$files\}"
			} else {
                set fsize [ file size $path ]
				if	{ $fsize < $::MAXFILESZ } {
                    set offset 0
                    set bytes $fsize
                } else {
                    set bytes $::MAXFILESZ
                }
				set fd [ open $path r ]
                seek $fd $offset start 
				set result [ read $fd $bytes ]
				close $fd
                if  { [ string match "*html" [ file extension $path ] ] } {
                    set type html
                } else {
                    set type text
                }
			    set result "0\n$type|$offset|$result"
			}
		} else {
			set result "3\nRoot directory must be /ldas_outgoing"
		}
    } err ] } {
        catch { close $fd }
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::bootutils 
##
## Description:
## bootstrap a utility
##
## Parameters:
## client 
## Usage:
##
## Comment:
## called by cmonClient

proc cntlmon::bootutils { cmd  } {
    
    if  { [ catch {
        cntlmon::boot$cmd  
    } err ] } {
        addLogEntry "$cmd launch error: $err"
        return "3\n$err"
    }
    return "0\n$cmd restarted"
}


## ******************************************************** 
##
## Name: ${API}::bootlogscan 
##
## Description:
## boot up via ssh , logscan to monitor mail.gifs in logs
##
## Usage:
##       
##
## Comments:
## 

proc ${API}::bootlogscan {} {

    cleanup logscan 
	catch { file rename -force logscan.log logscan.bak }
    ;## start log and database monitoring utilities
    cntlmon::dropdotfile
	set port [ lindex  $::cntlmon(emergency) 0 ]
    catch { ::exec logscan $::logDelay >& logscan.log & } err
    addLogEntry "logscan launched: $err"
    after 10000 [ list cntlmon::ckutils logscan $::CNTLMON_API_HOST ]
}

## ******************************************************** 
##
## Name: ${API}::bootdb2utils 
##
## Description:
## boot up via ssh , db2utils to monitor database
##
## Usage:
##       
##
## Comments:
## 

proc cntlmon::bootdb2utils {} {

    if  { ! [ regexp {metadata} $::API_LIST ] } {
    	addLogEntry "db2utils is not started because metadataAPI is not on LDAS API list ( $::API_LIST )"  blue
        ::bootLock OFF
        return
  	}
    
    if  { ! [ info exist ::METADATA_API_HOST ] } {
    	addLogEntry "db2utils is not started because ::METADATA_API_HOST is undefined" blue
        ::bootLock OFF
        return 
    }
    cleanup db2utils $::METADATA_API_HOST
	catch { file rename -force db2utils.log db2utils.bak }
    set err ""
	set port [ lindex  $::cntlmon(emergency) 0 ]
	cntlmon::dropdotfile
	if	{  ! [ string compare $::METADATA_API_HOST $::CNTLMON_API_HOST  ] } {		
    	catch { ::exec /usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
		LD_LIBRARY_PATH=$::LDAS/lib:$::DB2PATH/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		DB2INSTANCE=ldasdb LD_PRELOAD= db2utils $::dbDelay $::TOPDIR  >& db2utils.log & } err
	} else {
        set pid [ execssh $::METADATA_API_HOST \
         cd $::TOPDIR/cntlmonAPI && /usr/bin/env HOST=$::METADATA_API_HOST PATH=$::LDAS/bin:$::env(PATH) \
		 LD_LIBRARY_PATH=$::LDAS/lib:$::DB2PATH/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		 DB2INSTANCE=ldasdb LD_PRELOAD= \
		 "db2utils $::dbDelay $::TOPDIR >& db2utils.log &" ]
	}
	addLogEntry "db2utils launched: $err"
    after 10000 "cntlmon::ckutils db2utils $::METADATA_API_HOST"
	::bootLock OFF
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${API}::bootcoreWatch 
##
## Description:
## boot up via ssh , coreWatch to archive core files
##
## Usage:
##       
##
## Comments:
## 

proc cntlmon::bootcoreWatch {} {

    cleanup coreWatch $::COREHOST
	catch { file rename -force coreWatch.log coreWatch.bak }
    set err ""
	set port [ lindex  $::cntlmon(emergency) 0 ]
	cntlmon::dropdotfile
	if	{  ! [ string compare $::COREHOST $::CNTLMON_API_HOST ] } {	
    	catch { ::exec /usr/bin/env LD_PRELOAD= coreWatch $::cwDelay >& coreWatch.log & } err
	} else {
         set pid [ execssh $::COREHOST \
         cd $::TOPDIR/cntlmonAPI && /usr/bin/env HOST=$::COREHOST PATH=$::LDAS/bin:$::env(PATH): \
		 LD_LIBRARY_PATH=$::LDAS/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) LD_PRELOAD= \
		 "coreWatch $::cwDelay >& coreWatch.log &" ]
	}
    after 10000 [ list cntlmon::ckutils coreWatch $::CNTLMON_API_HOST ]
	addLogEntry "coreWatch launched: $err"
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${API}::bootlogCacheUtils
##
## Description:
## boot up via ssh, builds simplified version of archiveIndex
##
## Usage:
##       
##
## Comments:
## 

proc ${API}::bootlogCacheUtils {} {

    cleanup logCacheUtils 
	catch { file rename -force logCacheUtils.log logCacheutils.bak }
    cntlmon::dropdotfile
    ;## start log and database monitoring utilities
    catch { ::exec logCacheUtils >& logCacheUtils.log & } err
    addLogEntry "logCacheUtils launched: $err"
    after 10000 [ list cntlmon::ckutils logCacheUtils $::CNTLMON_API_HOST ]
}

## ******************************************************** 
##
## Name: ${API}::cleanup
##
## Description:
## remove standalone utils before exiting 
## via ssh
## Usage:
##       
##
## Comments:
## 

proc cntlmon::cleanup { prog { host "" } } {

	if	{ ! [ string length $host ] } {
		set host $::CNTLMON_API_HOST
	}
    ;## this getPid does not work for cntlmon
    ;## set pids [ ::getPid $prog $host ]
    
    set pids [ cntlmon::getPid $prog $host ]
	set pid [ split $pids ]
    if	{ ! [ string length $pid ] } {
    	return
    }
	if	{ [ string equal $host $::CNTLMON_API_HOST ] } {
		catch { kill -9 $pid } err
		addLogEntry "$prog $pid killed on $host: $err"
	} else {
		set cmd "execssh $host kill -9 $pid"
		catch { eval $cmd } err
		addLogEntry "$prog $pid killed on ssh to $host: $err"
	}
    set pids [ ::getPid $prog $host ]
	set pid [ split $pids ]
	addLogEntry "after cleaning prog $prog,pid=$pid"
}

## ********************************************************

## ******************************************************** 
##
## Name: ${API}::atExit
##
## Description:
## remove standalone utils before exiting 
## via ssh
## Usage:
##       
##
## Comments:
## this currently does nothing, let utilities die if
## there is no connection to cntlmonAPI

proc ${API}::atExit {} {

	if	{ [ catch {
		cleanup logscan 
       	if  { [ regexp {metadata} $::API_LIST ] && [ info exist ::METADATA_API_HOST ] } {
    		cleanup db2utils $::METADATA_API_HOST
        }
    	cleanup coreWatch $::COREHOST
    	cleanup logCacheUtils
   	 	catch { file delete -force ".. " }
    
    	;## unload globus
    	foreach client [ namespace children :: client* ] { 
    		${client}::delete
    	}
        ;## delete any outstanding ldas jobs
        if	{ [ array exist ::RawGlobusClient::networkhandle ] } {
        	foreach key [ array names ::RawGlobusClient::networkhandle ] {
            	catch { RawGlobusClient::close $key }
            }
        }
        if	{ [ info exist ::cntlmon::GlobusListenSocket ] } {
        	addLogEntry "closed globus tcl channel listening port $::cntlmon::GlobusListenSocket"
        	::close $::cntlmon::GlobusListenSocket
        } elseif  { [ info exist ::USE_GLOBUS ] && $::USE_GLOBUS } {
    		;## close server accept socket
        	catch { RawGlobus::serverClose $::TCLGLOBUS_PORT }
    		catch { RawGlobus::cleanup }
        }
    } err ] } {
    	addLogEntry $err 2
    }
} 	

## ******************************************************** 
##
## Name: ${API}::registerUtil 
##
## Description:
## register a utility's port and host as reported
##
## Usage:
##       
##
## Comments:
## 

proc ${API}::registerUtil { prog host pid { port 0 } } {
	
	lappend ::utils $prog
	set ::${prog} [list $host $pid $port ]
	addLogEntry "$prog launched at $host, pid $pid, port $port" blue
    
    if  { [ string match db2utils $prog ] && \
        ( ! [ info exist ::cntlmon::db2version ] || [ string equal X.X.X $::cntlmon::db2version ] ) } {
        catch { exec grep DB2 db2utils.log } db2data
        if  { ![ regexp {DB2/\S+\s+(\S+)} $db2data -> ::cntlmon::db2version ] } {
            set ::cntlmon::db2version X.X.X
            error "unable to determine DB2 version, set to X.X.X"
        }
    }
}

## ******************************************************** 
##
## Name: ${API}::updateClient
##
## Description:
## send code down to update client on site specifics.
##
## Usage:
##       
##
## Comments:
## This is needed to successfully compared the base64
## encoded md5 encrypted password from client

proc ${API}::updateClient {} {
    
    cntlmon::getdbnames
    cntlmon::getDSOs
    cntlmon::getDBtabcols 
    cntlmon::getMountPoints
    set msg ""   
    append msg "\{array set ::LDASmachines \{ $::LDAS_SYSTEM \{ $::LDASmachines \} \} ;"
    append msg "set ::DATABASE_NAME $::DATABASE_NAME; "
   	append msg "array set ::dbnames \{ [ array get ::site_dbnames ] \}; "
    append msg "set ::DSOs \{ $::DSOs \}; "
	append msg "set ::JOBS_PURGE_BEFORE $::JOBS_PURGE_BEFORE; "
    append msg "set ::LOGS_PURGE_BEFORE $::LOGS_PURGE_BEFORE; "
    append msg "set ::CLEAR_OUTPUT_PERIOD [ expr $::CLEAR_OUTPUT_PERIOD /1000 ]; "
    append msg "set ::NEXT_CLEANUP_SECS $::NEXT_CLEANUP_SECS ; "
    
    if  { [ array exist ::TABCOLS ] } {
        append msg "array set ::TABCOLS [ list  [ array get ::TABCOLS ] ] ; "
    }
    ;## will auto update client 
    if      { [ regexp {mpi} $::API_LIST ] && [ info exist ::MPI_API_HOST ]} {
    	append msg "set ::MPI_API_HOST $::MPI_API_HOST; "
    	if  { ! [ info exist ::beowulfNodes ] } {
        	after 1000 cntlmon::getNodeNamesFromFile
    	} else {
        	set ::NUMNODES [ llength $::beowulfNodes ]  
        	set nodes [ lsort -dictionary $::beowulfNodes ]
        	append msg "array set ::beowulfNodes \{ $::LDAS_SYSTEM \{ $nodes \}\};"
        	append msg "array set ::NUMNODES \{ $::LDAS_SYSTEM \{ $::NUMNODES \} \} ;"
        }
    } 
    	
    if  { [ info exist ::MOUNT_PT ] } {
        append msg "set ::MOUNT_PT \{ $::MOUNT_PT \};"
    }
    append msg "\}"
	return $msg
} 

## ******************************************************** 
##
## Name: cntlmon::registerClient 
##
## Description:
## registerClient for data broadcast 
##
## Parameters:
## e.g. client0 cmd0 apitimes
##
## Usage:
##
## Comment:

proc cntlmon::registerClient { client cmdId var } {

    if  { [ info exist ${var}(clients) ] } {
        if	{ [ lsearch -exact [ set ${var}(clients) ] $client ] == -1 } {
            lappend ${var}(clients) $client $cmdId
        }
    } else {
        lappend ${var}(clients) $client $cmdId 
    }
	addLogEntry "$client registered for $var,[array get $var]"
}

## ******************************************************** 
##
## Name: cntlmon::unregisterClient 
##
## Description:
## take client off notification list since it may have disconnected
## if no clients remain, repeat of updates is cancelled.
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc cntlmon::unregisterClient { client { vars {} } } {
    if  { ! [ llength $vars ] } {
        set vars $::cntlmon::varlist
    }
    foreach var $vars {
        set index [ lsearch -exact [ set ${var}(clients) ] $client ]
        if  { $index > -1 } {
            set ${var}(clients) [ lreplace [ set ${var}(clients) ] \
                $index [ expr $index + 1 ] ]
        }
        ;## no more clients, do not repeat updates 
        if  { ! [ llength [ set ${var}(clients) ] ] } {
            after cancel [ set ${var}(afterId) ]
            catch { unset $var }
            addLogEntry "$var updates cancelled, no clients"
        }
    }
	addLogEntry "$client unregistered for $vars"
}

## ******************************************************** 
##
## Name: cntlmon::notifyClient 
##
## Description:
## notify interested clients of the updates for a function
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc cntlmon::notifyClients { msg var } {
    if  { [ info exist ${var}(clients) ] } {
    	foreach { client cmdId } [ set ${var}(clients) ] {
        	if  { [ string length [ info proc ::${client}::reply ] ] } {
            	::${client}::reply $cmdId $msg
        	} else {
            	cntlmon::unregisterClient $client $var
            	addLogEntry "client $client removed from $var"
        	}
    	}
    }
}

proc cntlmon::notifyClientsNew { data } {

    foreach client [ namespace children :: client* ] {  
        if  { [ catch {      
            regsub {::} $client {} client
            set key  [ set ::${client}::serverKey ]
		    set msg "nocallback\n${client}:$::LDAS_VERSION:$key:priviledged\n4\n$data\n0"
            puts [ set ::${client}::sid ] $msg
            addLogEntry "$client notified" blue
        } err ] } {
            addLogEntry "$client error: $err"
        }
    }
        
}

## ******************************************************** 
##
## Name: cntlmon::updateAllClients
##
## Description:
## broadcast node changes to client
##
## Parameters:
##
## Usage:
##
## Comment:
## 

proc cntlmon::updateAllClients {} {
    
    if  { [ catch {
        set msg [ cntlmon::updateClient ]
        cntlmon::notifyClientsNew $msg
    } err ] } {
        addLogEntry $err 2
    }
}
       
## ******************************************************** 
##
## Name: cntlmon::ckutils 
##
## Description:
## check if utilities start up ok by examining log file
##
## Parameters:
## client 
## Usage:
##
## Comment:
## for linux, can only get first 8 chars of program
## ::getPid is not working yet on RH 9.0

proc cntlmon::ckutils { cmd { host "" } { force 0 } } {
      
    if  { [ info exist ::$cmd ] && ! $force } {
        return
    }
    ;## force check to see if utils are up
    if  { [ catch {
        set pids [ ::getPid [ string range $cmd 0 7 ] $host ]
	    if	{ ! [ string length $pids ] } {
            if  { [ string match $::CNTLMON_API_HOST $host ] } {
                set thishost ""
            } else {
                set thishost $host
            }
            set pids [ cntlmon::getPid $cmd $thishost ]
        }
        if  { ! [ string length $pids ] } {
        	if	{ [ file exist $cmd.log ] } {
            	set fd [ open $cmd.log r ] 
            	set data [ read $fd ]
            	close $fd
            } else {
            	set data "$cmd.log does not exist"
            }
            addLogEntry "$::LDAS_SYSTEM $cmd startup failed: pid '$pids',$data; rebooting" email
            catch { unset ::$cmd }
            if  { $force } {
                after 0 [ list cntlmon::boot$cmd ]
            }
        }                        
    } err ] } {
        addLogEntry $err 2
    }
    if  { $force } {
        after $::DELAY_UTILS_MONITOR [ list cntlmon::ckutils $cmd $host 1 ]
    }
}

## ******************************************************** 
##
## Name: cntlmon::ldasinit
##
## Description:
## set up ssh env for securePipe
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc cntlmon::ldasinit {} {

    set ::utils [ list ]

    catch {
	exec /ldas/bin/ssh-agent-mgr \
	    --agent-file=/ldas_outgoing/managerAPI/.ldas-agent \
	    --shell=tclsh check } err
    eval $err
    if { ! [ info exists ::env(SSH_AUTH_SOCK) ] } {
	set subject "cntlmonAPI unable to set SSH_AUTH_SOCK"
	set body [ "$err\nYou may need to restart all of LDAS" ]
	addLogEntry "Subject: $subject; Body: $body" email
    }
}
    
## ******************************************************** 
##
## Name: ${API}::getdbnames 
##
## Description:
## get database names from metadataAPI ini file
## Usage:
##
## Comments:

proc ${API}::getdbnames {} {

    if  { [ catch {
        set dbfile [ file join $::TOPDIR metadataAPI LDASdsnames.ini ]
        source $dbfile
        set dblist [ set ::site_dbnames($::LDAS_SYSTEM) ] 
		foreach db $dblist {
			catch { unset ::${db}(login) }
			catch { unset ::${db}(rlogin) }
		}
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: ${API}::getNodeNames 
##
## Description:
## get node names from mpiAPI itself 
## Usage:
##
## Comments: 
## On startup of LDAS there are 2 updates
## 1. from mpi init
## 2. from cntlmonAPI request to mpi
## these updates are needed in case either party is rebooted

proc ${API}::getNodeNames {} {

   if  { ! [ regexp {mpi} $::API_LIST ] || ! [ info exist ::MPI_API_HOST ] } {
    	addLogEntry "Unable to get node names because mpiAPI does not exist or \
        	::MPI_API_HOST is undefined" blue
        return
    }
    
    if  { [ catch {
        set cmd "mpi::updateCmonNodelist"
        set result [ getEmergData mpi $cmd ]  
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: ${API}::getRscValues
##
## Description:
## get resource values from rsc file of an api
## 
## Usage:
##
## Comments:      
## patterns would be like { "set ::NODENAMES" "set ::DEBUG" }
## ******************************************************** 

proc ${API}::getRscValues { api pattern } {
    
    set fname [ file join $::TOPDIR mpiAPI LDASmpi.rsc ]
	if	{ ! [ file exist $fname ] } {
		set fname [ file join $::LDAS lib mpiAPI LDASmpi.rsc ]
	}
    set values ""
    set rsctext ""
    if  { [ catch {
        if  { [ string length $pattern ] } {
		    set fd [ open $fname r ]
		    set data [ read $fd ]
		    set data [ split $data \n ]
		    close $fd 
		    set i 0
			set pattern "$pattern\[\\t\\s\]+(.+)$"
		    foreach line $data {
				if	{ [ regexp $pattern $line -> values ] } {
					set rsctext [ subst $values ]
                    break
				}
		    }
        }
    } err ] } {
        addLogEntry "$fname $err"
        catch { close $fd }
        return none
    }
    return $rsctext
}

## ******************************************************** 
##
## Name: cntlmon::initVars
##
## Description:
## initialize some vars for later use
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc ${API}::initVars {} {

	set ::MAXFILESZ_KB [ expr $::MAXFILESZ/1024 ]
	set ::ENCODEDMGRKEY [ encode64 $::MGRKEY ]
	set ::TOPDIR [ file dirname $::PUBDIR ] 

	;## gather list of machines
	set ::LDASmachines [ list ]
    foreach api $::API_LIST {
    	set hostvar [ string toupper $api ]_API_HOST
        if	{ [ info exist ::$hostvar ] } {
			set host [ set ::$hostvar ]
			if	{ [ lsearch -exact $::LDASmachines $host ] == -1 } {
				lappend ::LDASmachines $host
            }
		}
	}
	if	{ [ info exist ::OTHER_HOSTS ] } {
		foreach host $::OTHER_HOSTS {
			if	{ [ lsearch -exact $::LDASmachines $host ] == -1 } {
				set  ::LDASmachines [ linsert $::LDASmachines end-1 $host ]
			}
		}
	}
	puts "LDAS machines $::LDASmachines"

    foreach { api host ::cntlmon::mgroper } [ validService manager operator ] { break }	
	puts "manager is @$::MANAGER_API_HOST:$::cntlmon::mgroper"
    
    if	{ [ catch {
    	set result [ validService mpi emergency ]
    	foreach { api host ::cntlmon::mpiport } $result { break }
		puts "mpi is @$::MPI_API_HOST:$::cntlmon::mpiport"
    } err ] } {
    	addLogEntry "mpiAPI is not active" blue
        set ::cntlmon::mpiport ""
    }
	writeState
}


## ******************************************************** 
##
## Name: cntlmon::usersQueueFile
##
## Description:
## determine the users queue file to access
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc ${API}::usersQueueFile {} {
	
	set dir [ file join $::TOPDIR managerAPI ]
	set clearmtime 0
	set cryptmtime 0 
	if	{ [ file exist $dir/users.queue ] } {
		set clearmtime [ file mtime $dir/users.queue ]
	}
	if	{ [ file exist $dir/users.queue.crypt ] } {
		set cryptmtime [ file mtime $dir/users.queue.crypt ]
	}
	if	{ $clearmtime >= $cryptmtime } {
		return $dir/users.queue 
	} else {
		return $dir/users.queue.crypt
	}
}

## ******************************************************** 
##
## Name: cntlmon::reopenListenSock
##
## Description:
## determine the users queue file to access
##
## Parameters:
## client 
## Usage:
##
## Comment:
## force operator on baseport for firewall
## adopted from genericAPI openListenSock

proc ${API}::openClientSock { { service "" } { port "" } { cid "" } } {

	 ::bootLock OFF

     ;## poll every 10 seconds to see if service has died
     if { [ string length $cid ] } {
        if { [ catch {
           ;## if this throws, we are definitely gone
           set data [ fconfigure $cid -sockname ]
           set currentport [ lindex $data 2 ]
           ;## if we are ok, just pass on by
           if { $currentport == $port } {
              after 10000 [ list cntlmon::openClientSock $service $port $cid ]
           } else {
              ;## if not, pass on by, but rerun in 0.100 secs
              ;## and restart
              addLogEntry "my $service socket has died!!" mail
              after 100 cntlmon::openClientSock $service $::BASEPORT
           }
        } err ] } {
           addLogEntry "my $service socket has died!!" mail
           after 100 openListenSock $service
        }
        ;## short circuit return point if we are polling
        return {}
     }

	#set port [ lindex  $::cntlmon(operator) 0 ]
	#closeListenSock operator
	#set service operator
	set service clientHandler
	
	#set target [ validService $::API $::BASEPORT ]
    #foreach {api host port service sid} $target {break}
	
	;## override the default service port
	set port $::BASEPORT
	set service clientHandler
	set ::cntlmon(clientHandler) $::BASEPORT
	set host $::cntlmon(host)
	set api cntlmon
    upvar #0 ::${::API}($service) registry
     
     if { ! [ info exists registry ] } {
        set msg    "$api $service not registered in .rsc file"
        return -code error "[ myName ]: $msg"
     }
     addLogEntry "registry $registry" purple
     #catch { set registry [ lreplace $registry 1 1 ] }
     
     if { [ catch {
        set cid [ socket -server "sock::cfg $service" $::BASEPORT ]
     } err ] } {
        if { [ catch {
           foreach { name pid owner } [ portInfo $port ] { break }
           set info "name: $name pid: $pid owner: $owner service $service"
        } err2 ] } {
           set info $err2
        }
        return -code error "$err : socket $port info: $info"
     }   
     set ::cntlmon(clientHandler) $::BASEPORT
	 
     lappend registry $cid
     addLogEntry "port $port ($service) opened on $host as $cid" blue
	 after 10000 [ list cntlmon::openClientSock $service $port $cid ]
     return $cid
}

## ******************************************************** 
##
## Name: operator (overides the one in LDASgwrap)
##
## Description:
## Handles requests at the wrapped API's operator socket.
## A request must be in the form of a list of 3 elements,
## where element 0 is the manager's key, element 2 is the
## job id, and element 3 is a code block to be evaluated.
##
## The command code block is assumed to begin with a call
## to metaOpts (see the genericAPI.tcl) which sets the
## argument list.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket (see sock.tcl).
##
## Comments:
## This is the only entry point exposed on the operator
## socket.
## See the asstMgr.tcl procedure ${name}::sockhandler
## for the format of the reply and the significance of the
## return values.
## The variable ::blank::errlvl may be modified by a called
## procedure generating an error condition.
## may add a key check ?

proc clientHandler { cid { port ""} { addr "" } } {
     
	set seqpt ""
	set cgi 0
	if	{ [ catch {  
     	set cmdlist [ cmd::result $cid ]	
		set rc "0"
        if  { [ llength $cmdlist ] } {
			if	{ [ regexp {^cgi} $cmdlist ] } {
				set cgi 1
				set rc [ cntlmon::CgiRequest $cid $cmdlist ]
				puts $cid $rc
				::close $cid
			} else {
				;## cmonClient connections
				if { [ string match connect $cmdlist ] } {
				   set key [ expr abs([ clock clicks ]) ]	
				   set msg "key=$key\n$key\n0\n0\n0"
				} else {
            		foreach { login passwd version key } $cmdlist { break }
					if	{ [ string match nokey $key ] } {
						set key [ expr abs([ clock clicks ]) ]
					}
            		set status [ validateLogin $login $passwd $key 2 ]
					set reply [  validateVersion $version ]
            		set data [ cntlmon::updateClient ]
            		incr cntlmon::clientCount 1
            		set num $cntlmon::clientCount
		    		createClient client$::cntlmon::clientCount $login 
		    		client${num}::setsid $cid
					set ::client${num}::serverKey $key
					set ::client${num}::ipaddr $addr 
            		# acknowledge login ok 
					set msg "nocallback\nclient${num}:$::LDAS_VERSION:$key:$status\n0\n$data\n$reply"
				}
				set msg [ cmd::size $msg ]
				puts $cid $msg
				flush $cid
        		;## transfer handler to client 
			}
        } else {
            catch { close $cid }
        }
    } err ] } {
		if	{ $cgi } {
			catch { puts $cid "3 $err" }
		} else {
        	;## reply with the standard protocol
        	catch { puts $cid "nocallback\n0\n3\n$err\n" }
		}
		addLogEntry "operator error, closing fd $cid: $err" 2
		catch { close $cid }
   	}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: emergency 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## port and addr args are from utilities checking if server is up
## DO NOT remove need this for cntlmon for port and addr

proc emergency { cid { port "" } { addr "" } } {
     if { [ catch {
        fileevent $cid readable {}
        set key [ ::key::intgen ]
        set peerinfo [ fconfigure $cid -peername ]
        
        if { ! [ info exists ::PRIVELEGED_IP_ADDRESSES ] } {
           set ::PRIVELEGED_IP_ADDRESSES {^(10\.|192\.168\.|127\.0\.0\.)}
        }
        
        set accept_rx $::PRIVELEGED_IP_ADDRESSES
        
        if { ! [ regexp $accept_rx $peerinfo ] } {
           ::close $cid
           set err "rejected connection from $peerinfo "
           append err "which was not matched by "
           append err "::PRIVELEGED_IP_ADDRESSES pattern."
           return -code error $err
        }
        
        ;## no point in reporting every inter AP connection...
        if { [ info exists ::DEBUG_EMERGENCY_PORT ] && \
             [ string equal 1 $::DEBUG_EMERGENCY_PORT ] } {
           addLogEntry "accepted connection from: $peerinfo" green
        }
        # trace variable ::emergency_socket($key$cid) w emergency_callback
        after 10 [ list cmd::receive $cid emergency_socket($key$cid) emergency_callback ] 
     } err ] } {
        if { [ regexp {PRIVELEGED_IP} $err ] } {
           addLogEntry $err blue
        } else {
           addLogEntry $err email
        }
     }
}

## ******************************************************** 
##
## Name: emergency_callback 
##
## Description:
## Provides the functions required to handle requests of
## the blank API's emergency socket.  Communications with
## this socket are assumed to originate with the manager,
## and will be validated by comparing the first element
## in the received command list with the manager key.
## Commands must consist of a list with 2 elements, the
## first one must be the manager key, the second is Tcl
## code to be eval'd.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket (see sock.tcl).
## If you expect to get anything back through the socket,
## make sure the command does a:
##
##    puts $cid "some such [ command ]"
##
## Comments:
## This is the only entry point exposed on the emergency
## socket.  It requires a key, but will try to evaluate
## anything that comes through with a valid key.

proc emergency_callback { args } {
     
     set jobid IDLE
    
     set key {}
     set cmd {}
	set msg {}
    
     set var   [ lindex $args 0 ]
     set index [ lindex $args 1 ]
     set var   ::${var}($index)
     regexp {sock\d+} $var cid
     
     set cmd [ set $var ]
     catch { ::unset $var }
    
     set peerinfo [ fconfigure $cid -peername ]
     
     if { ! [ string length $cmd ] } {
        catch { close $cid }
        return {}
     }

     if { [ regexp -nocase {MGRKEY} $cmd ] } {
        set msg "reserved character sequence MGRKEY received"
        append msg " from: '$peerinfo'"
     }

     regexp {([^ ]+) (.+)} $cmd -> key cmd
     	
     regexp {^\{(.*)\}$} $cmd -> cmd
     regexp {^\"(.*)\"$} $cmd -> cmd
     
     if { ! [ string length $cmd ] || ! [ string length $key ] } {
	   set msg "badly formed command recieved"
	}
	
     set key  [ key::md5 $key ]
     set lock [ key::md5 $::MGRKEY ]
     
     if { ! [ string equal $lock $key ] } {
	   set msg "bad key recieved: '$key'"
	}

     if { [ regexp -- $::BAD_WORDS $cmd ] } {
        set msg "forbidden command recieved: '$cmd'"
     }

     ;## either we have an error at this point or we can go!
	if { [ string length $msg ] } {
	   ${::API}::reply $cid "!!ERROR!!\n"
	   addLogEntry $msg red
	} else {  
        ;## don't allow references to the ::MGRKEY
        regsub -all -- {\$::MGRKEY} $cmd $lock cmd
        set cmd [ subst -novariables -nocommands $cmd ]
	   if { [ catch { eval $cmd } err ] } {
           regsub -all -- \[\\\(\\\{\\\s\]$::MGRKEY\[\\\)\\\}\\\s\] $err " ::MGRKEY " err
		 set msg "$::API emergency error:\n$err"
           ${::API}::reply $cid $msg
           addLogEntry $msg 2
        } else {
			set loglevel green
			regsub -all -- \[\\\(\\\{\\\s\]$::MGRKEY\[\\\)\\\}\\\s\] $cmd " ::MGRKEY " cmd
           # for mgr::addUser and mgr::updateUserInfo
           if { [ regexp -nocase {(add|update|manage)User} $cmd ] } {
              set loglevel blue
           }
           set msg "executed: $cmd"
           ${::API}::reply $cid "${::API}:emergency_port:$msg"
           if { [ string equal blue $loglevel ] } {
              set msg "user info updated successfully"
           }
           if { [ regexp {(sysData|countChannels)} $cmd ] } {
              ;## time to disable the log hog!
              #debugPuts $msg 0
           } else {
              if { [ info exists ::LOG_ALL_EMERGENCY_COMMANDS ] && \
                   [ string equal 1 $::LOG_ALL_EMERGENCY_COMMANDS ] } {
                 addLogEntry $msg $loglevel
              }
           }   
        }
	}   
     
	catch { ::close $cid }
}

## ******************************************************** 
##
## Name: socketTimeout 
##
## Description:
## make client socket connection with a timeout
##
## Usage:
##
## Comments:

proc socketTimeout { host port timeout } {

    if  { [ catch {
        set sid [ socket $host $port ]
        set ::connected start
        fileevent $sid w {set ::connected ok}
        vwait ::connected
    } err ] } {
        return -code error $err
    }
    if  { [ string equal $::connected timeout ] } {
        return -code error timeout
    } else {
        return $sid
    }
}


## ******************************************************** 
##
## Name: validateLogin 
##
## Description:
## Validates login and password for functions if provided
## Usage:
##
## Comments:
## client provides control for login and password 
## password is encrypted.

proc validateLogin { login passwd { serverkey "" } { group 0 } } {

	if	{ [ regexp {control} $login ] } {
    	if  { ! [ info exist ::md5sum($login) ] } {
        	return -code error "Please establish a password first for \
        	user '$login' on the server."
		}
		set passwd [ binaryDecrypt $::CHALLENGE [ base64::decode64 $passwd ] ]
		if  { ! [ string equal [ key::md5 $passwd ] $::md5sum($login) ] } {
        	return -code error "Invalid password for user $login; \
        	please contact $::passwd_auth to obtain a valid login Id"
		}
		if	{ $group } {
			if 	{ ! [ string match control $login ] } {
				return -code error "User $login is not authorized to \
        		perform this function"
			}
		}
		return -code ok				
    }
	cntlmon::buildUsersQueue
	set cmd "mgr::validateUserPassword -password md5Protocol -name $login -md5digest $passwd -md5salt $serverkey \] \}"

    set status normal
	if	{ [ catch { 
		set result [ eval $cmd ]
		if	{ ! [ string equal 1 $result ] } {
			error "Invalid password for user $login; \
        	please contact $::passwd_auth to obtain a valid login Id"			
		}
		if	{ $group } {
			if	{ [ info exist ::control_group ] && [ llength $::control_group ] } {
				set index [ lsearch -exact $::control_group $login ]
				if	{ $index == -1 } {
                    if  { $group == 1 } {
					    error "User $login is not authorized to perform this function."
                    } 
				} else {
                    set status priviledged
                }
			} else {
				error "Please establish a control group alias in cntlmonAPI first."
			}
		}
	} err ] } {
		catch { unset ::QUEUE(USERS) }
		return -code error $err
	}
	catch { unset ::QUEUE(USERS) }
	return -code ok $status
}
## ******************************************************** 
##
## Name: validateVersion 
##
## Description:
## Validates login and password for functions if provided
## Usage:
##
## Comments:
## client provides control for login and password 
## password is md5 encrypted and base64 encoded.

proc validateVersion { version } {

	if	{ ! [ string length $version ] } {
		error "Your cmonClient version is unknown and maybe incompatible \
			with cntlmonAPI server's version $::LDAS_VERSION; \
			please update your cmonClient package to $::LDAS_VERSION before connecting."
	}
	if	{ [ string match $::LDAS_VERSION $version ] } {
		return -code ok
	}
	
	set reply ""
    set error 0
    set state 0
    set ver1 ""
    set ver2 ""
    set ver3 ""
    
	if	{ [ catch {
        regexp {(\d+).(\d+).(\d+)} $version -> ver1 ver2 ver3
        
        ;## reject major version mismatch and allow a new client to connect to older server with
        ;## one version backwards compatibility
        
        if  { $ver1 != $::LDAS_ver1 } {
            set state 1
        } elseif { ( $ver2 != $::LDAS_ver2 ) && [ expr $ver2 - $::LDAS_ver2 ] != 1 } {
            set state 1
        } elseif { $ver3 != $::LDAS_ver3 } {
            set state 2
        }
        switch $state {
            1 { error "Error: your cmonClient version $version is incompatible \
			    with cntlmonAPI server's version $::LDAS_VERSION; \
			    please update your cmonClient package to $::LDAS_VERSION before connecting." 
            }
            2 { set reply "Warning: your cmonClient version $version does not match \
			    cntlmonAPI server's version $::LDAS_VERSION; \
			    to ensure compatibility, please use cmonClient version $::LDAS_VERSION if possible."
		    }
            default { set reply "" }
        }
	} err ] } {
		return -code error $err
	}
	return -code ok $reply
}

## ******************************************************** 
##
## Name: sock::cfg 
##
## Description:
## Configures a listening socket opened by openListenSock.
## A procedure named $service must exist to handle all
## incoming requests.
##
## Usage:
## See openListenSock.
##
## Comments:
## overriding sock::cfg

proc sock::cfg { service cid addr port } {

     if { [ string equal data $service ] } {
        set service dataRecv
     }
     fileevent $cid readable "$service $cid $port $addr"
     fconfigure $cid -blocking off
     if { ! [ string equal data $service ] } {
        fconfigure $cid -buffersize 50000
        fconfigure $cid -buffering line
     }
}
## ******************************************************** 

## ******************************************************** 
## sock handling procs (from sock.tcl)
##
## Name: cmd::result
##
## Description:
## Receives data from a socket.  The socket is opened in
## blocking mode, and is read using this function.
## cmd::result puts the socket in non-blocking mode and
## attempts to read as much as possible in line buffered
## mode.  After reading the socket is put back into
## blocking mode.
##
## Usage:
##       set sid [ socket $host $port ]
##       puts $sid "get_me_some_data"
##       flush $sid
##       fileevent $sid readable [ return [ cmd::result $sid ] ]
##
## Comments:
## This procedure provides "connectionful" communication.
## For "connectionless" communication, call cmd::disconnect
## immediately afterwards.

proc cmd::result { { sid "" } { timeout 10 } } {
     set i 0
     set data {}
     ;## line buffered non-blocking configuration forced
     fconfigure $sid -buffering line
     fconfigure $sid -blocking off
     
     while { 1 } {
        after $i;
        if { [ eof $sid ] } { break }
        ;## if no bytes are read
        if { [ gets $sid line ] < 0 } {
           ;## then increment delay
           incr i
           ;## if delay has reached the timeout threshhold
           if { $i > $timeout } {
              ;## then don't try to read any more
              break
           } else {
              ;## otherwise loop and try to read some more
              continue
           }   
        } else {
           ;## if bytes WERE read, reset the delay to 0
           ;## to read remaining data as quickly as
           ;## possible
           set i 0
        }
        
        append data "$line\n"
        
        set line {}
        ;## special handler for persistent connections
        if { [ regexp {^[\n]*~~(\d+)\n$} $data -> size ] } {
           ;## do block data read 
           ;## must set blocking on to read entire msg
           fconfigure $sid -buffering full
           fconfigure $sid -blocking on
           set data [ read $sid $size ]
           break
        }   
     }
     set data [ string trim $data "\n" ]
     ;## reconfigure the socket to blocking mode
     ;## so that fileevent can handle future
     ;## connections
     fconfigure $sid -blocking on
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cmd::size
##
## Description:
## Attach proper size info for handling by cmd::result
## when using a persistent connection.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cmd::size { cmd } {
     set size [ string length $cmd ]
     set cmd "~~$size\n$cmd"
     return $cmd
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cntlmon::writeState
##
## Description:
## Attach proper size info for handling by cmd::result
## when using a persistent connection.
##
## Parameters:
##
## Usage:
##
## Comments:
## some are needed by logscan for sending email

proc cntlmon::writeState {} {

	set fd [ open [ file join $::TOPDIR cntlmonAPI cntlmon.state ]  w ]
	set text "set ::TOPDIR $::TOPDIR\n"
	append text "set ::LDASLOG $::LDASLOG\n"
	append text "set ::LDASARC $::LDASARC\n" 
	append text "set ::RUNCODE $::RUNCODE\n"
	append text "set ::LDASTMP $::TOPDIR/tmp\n"
	append text "set ::PUBDIR  $::TOPDIR/jobs\n"
	append text "set ::LDAS_SYSTEM $::LDAS_SYSTEM\n"
	append text "set ::LDAS $::LDAS\n"
    append text "set ::API cntlmon\n"
    append text "set ::${::API}(host) [ set ::${::API}(host) ]\n"
    append text "set ::${::API}(emergency) [ lindex $::cntlmon(emergency) 0 ]\n"
    append text "set ::env(USER) [ set ::env(USER) ]\n"
    append text "set ::manager(host) [ set ::manager(host) ]\n"
    append text "set ::CLIENTLOGDIR $::CLIENTLOGDIR"
	puts $fd $text
	close $fd
}

## ******************************************************** 
##
## Name: cntlmon::stdOutErr
##
## Description:
## Converts errors that normally only ever get reported in
## in the ::API.log file into log entries.
##
## Relies on a file named lookup.patterns in the generic API
## installation directory.
##
## Parameters:
##
## Usage:
## The format of an entry in the lookup.patterns file is:
## 
##      [ list api regex message errorlevel ]
##
## where the regex is probably a unique substring and the
## errorlevel is a log level like "blue" or "email".
##
## Comments:
##

proc cntlmon::stdOutErr {} {

	 foreach API $::API_LIST {
     if { [ catch {
        set filename [ file join $::TOPDIR ${API}API $API.log ]
        set patfile ${::LDAS}/lib/genericAPI/lookup.patterns
        set now [ clock seconds ]
        set subject "$::LDAS_SYSTEM LDAS ${API}API error!"
        set tmp [ list ]
        
        if { [ file exists $patfile ] } {
           
           if { ! [ info exists ::stdout_stderr_fpos($API) ] } {
              set ::stdout_stderr_fpos($API) 0
           }
                      
           set patterns [ dumpFile $patfile ]
           set patterns [ split $patterns "\n" ]
           foreach pattern $patterns {
              foreach [ list api pattern msg errlvl ] $pattern { break }
              if { [ string equal $API $api ] } {
                 lappend tmp [ list $pattern $msg $errlvl ] 
              }
           }
           set patterns $tmp
           
           ;## read the new part of the file written in last minute
           set dsize [ file size $filename ]
           set newbytes [ expr { $dsize - $::stdout_stderr_fpos($API) } ]
		   
		   ;## if an API was restarted the newbytes could be
           ;## negative, which will cause an error about -NNN
           ;## being invalid and that only -nonewline is valid...
           if { $newbytes < 0 } {
              set ::stdout_stderr_fpos($API) 0
              set newbytes $dsize
           } elseif { $newbytes > $::MAX_STDOUTERR } {
		   	  addLogEntry "${API}API log file ( $newbytes bytes ) exceeded $::MAX_STDOUTERR, \
                reading in $::MAX_STDOUTERR bytes only" yellow
		   	  set newbytes $::MAX_STDOUTERR
		   }

           if { $newbytes } {
              set fid [ ::open $filename r ]
              seek $fid $::stdout_stderr_fpos($API)
              set data [ ::read $fid $newbytes ]
              set ::stdout_stderr_fpos($API) [ tell $fid ]
              ::close $fid
              if { [ string length $data ] > 100000 } {
                 ;## big trouble in this API!!
              }
              set data [ split $data "\n" ]
           } else {
              set data [ list ]
           }
           
           foreach line $data {
              foreach pattern $patterns {
                 foreach [ list pattern msg errlvl ] $pattern { break }
                 if { [ regexp -- $pattern $line ] } {
                    set line "notable line in $filename: ' $line '"
                    if { [ regexp -nocase {(mail|phon|pager)} $errlvl ] } {
                       set msg "Subject: ${subject}; Body: $msg ($line)"
					   set cmd "addLogEntry \"$msg\" $errlvl"
                    } else {
					   set cmd "addLogEntry \"$msg ($line)\" $errlvl"
                    }
					foreach { api host port service } [ validService $API emergency ] { break }
					# puts "API=$API,cmd='$cmd',host=$host,port=$port"
					set result [ getEmergData $api $cmd 0 ]
                    continue
                 }
              }
           }
        }
     } err ] } {
        if { [ string length $err ] } {
           if { [ info exists ::errorInfo ] } {
              append err " $::errorInfo"
           }
           addLogEntry $err red
        }
        catch { ::close $fid }
     }
	 }
	 if	{ ! [ info exist ::APISTDOUTTIME ] } {
	 	set ::APISTDOUTTIME 60000
	 }
	 after $::APISTDOUTTIME cntlmon::stdOutErr
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cntlmon::CgiRequest
##
## Description:
## Attach proper size info for handling by cmd::result
## when using a persistent connection.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cntlmon::CgiRequest { cid cmdlist } {

	set rc "0"
	if	{ [ catch {
		fileevent $cid readable {}
        set key [ ::key::intgen ]
        set peerinfo [ fconfigure $cid -peername ]
        
        if { ! [ info exists ::CGIHOST_IP_ADDRESSES ] } {
           set ::CGIHOST_IP_ADDRESSES {^(131\.215\.115\.247)}
        }
        
        set accept_rx $::CGIHOST_IP_ADDRESSES
        
        if { ! [ regexp $accept_rx $peerinfo ] } {
           ::close $cid
           set err "rejected connection from $peerinfo "
           append err "which was not matched by "
           append err "::CGIHOST_IP_ADDRESSES pattern."
           error $err
        }
		set cgicmd "[ lrange $cmdlist 1 end ]"
		set rc [ eval $cgicmd ]
	} err ] } {
		return -code error $err
    }
	return $rc         	
}

## ******************************************************** 
##
## Name: cntlmon::checkMgrUp
##
## Description:
## send email notification based on log contents 
##
## Parameters:
##
## Usage:
##   
## Comments:
## getPid only handles 8 chars for prog name on Linux

proc cntlmon::checkMgrUp {} {

    if  { [ catch {
        set pids [ cntlmon::getPid manager $::MANAGER_API_HOST ] 
        if	{ ! [ string length $pids ] } {
            error "$::LDAS_SYSTEM LDAS manager may not be up: $pids"
        }
    } err ] } {
        set subject $err
        set body "$::LDAS_SYSTEM LDAS may need to be restarted."
        addLogEntry "Subject: $subject; Body: $body" email
    }  
    ;## monitor manager every hour as set
    after $::DELAY_MGR_MONITOR [ list cntlmon::checkMgrUp ]    
 
}

## ******************************************************** 
##
## Name: cntlmon::cleanupLDASoutput
##
## Description:
## remove old jobs and log files
##
## Parameters:
##
## Usage:
##   
## Comments:
## getPid only handles 8 chars for prog name on Linux

proc cntlmon::cleanupLDASoutput {} {
    catch { exec cleanup_ldas_output.tcl 1 >& cleanup.log & } err
    set ::NEXT_CLEANUP_SECS [ expr [ clock seconds ] + $::CLEAR_OUTPUT_PERIOD/1000 ]
    catch { after cancel $::cntlmon::nextcleanupId } 
    set ::cntlmon::nextcleanupId [ after $::CLEAR_OUTPUT_PERIOD cntlmon::cleanupLDASoutput ]
    addLogEntry "next cleanup at $::NEXT_CLEANUP_SECS secs" blue
    return "set ::NEXT_CLEANUP_SECS $::NEXT_CLEANUP_SECS" 
}

## ******************************************************** 
##
## Name: cntlmon::trimLogCache
##
## Description:
## trim logCache to desired date
##
## Parameters:
##
## Usage:
##   
## Comments:

proc cntlmon::trimLogCache { utcstart } {

    if  { [ catch {
        if  { [ info exist ::logCacheUtils ] } {
            foreach { host pid port } [ set ::logCacheUtils ] { break }
            sendPort [ list logCacheUtils::trimLogCache $utcstart 1 ] $host $port
        } else {
            error "logCacheUtils is down"
        }
    } err ] } {
        addLogEntry $err 2
        return "3\n$err"
    }
               
} 

## ******************************************************** 
##
## Name: cntlmon::updateDBtabcols
##
## Description:
## update database table names and cols
##
## Parameters:
##
## Usage:
##   
## Comments:

proc cntlmon::updateDBtabcols {} {

    if  { ! [ regexp {metadata} $::API_LIST ] } {
        return
    }
    
    set err ""
	set port [ lindex  $::cntlmon(emergency) 0 ]
	if	{  ! [ string compare $::METADATA_API_HOST $::CNTLMON_API_HOST  ] } {		
    	catch { ::exec /usr/bin/env PATH=$::LDAS/bin:$::env(PATH) \
		LD_LIBRARY_PATH=$::LDAS/lib:$::DB2PATH/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		DB2INSTANCE=ldasdb LD_PRELOAD= dbcolnames.tcl $::TOPDIR ldas_tst  >& dbcolnames.log & } err
	} else {
        set pid [ execssh $::METADATA_API_HOST \
         cd $::TOPDIR/cntlmonAPI && /usr/bin/env HOST=$::METADATA_API_HOST PATH=$::LDAS/bin:$::env(PATH) \
		 LD_LIBRARY_PATH=$::LDAS/lib:$::DB2PATH/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		 DB2INSTANCE=ldasdb LD_PRELOAD= \
		 "dbcolnames.tcl $::TOPDIR ldas_tst >& dbcolnames.log &" ]
	}
	addLogEntry "Updating database tables: $err"
}

## ******************************************************** 
##
## Name: cntlmon::setVars
##
## Description:
## pick up required global vars
##
## Parameters:
##
## Usage:
##   
## Comments:

proc cntlmon::setVars {} {

    if  { [ catch {
        set metadatarsc  [ file join $::TOPDIR metadataAPI LDASmetadata.rsc ]
        if  { ! [ file exist $metadatarsc ] } {
            set metadatarsc  [ file join $::LDAS lib metadataAPI LDASmetadata.rsc ]
        }
        ;## avoid using exec
        #set rc [ catch { exec grep MAXROWS $metadatarsc } err ]
        # eval $err
		set interp [ interp create ]
		$interp eval source $metadatarsc
		set ::MAXROWS [ $interp eval set ::MAXROWS ]
		interp delete $interp

        if	{ ! [ info exist ::SERVICE_NAME ] } {
        	set ::SERVICE_NAME ldas
        }
        if	{ ![ array exist ::DISKCACHE_LOG_PATTERN_DESC ] } {
;## desc=description for diskcache log patterns
array set ::DISKCACHE_LOG_PATTERN_DESC [ list "hash updated" "MOUNT_PT updated if dcmangle is running" \
"does not exist" "mount point does not exist" \
missing "missing mount points" \
"overlap error(s)" "overlap frame time error(s)" \
duplicate "duplicate frame times" \
RUNNING "RUNNING thread" \
"MOUNT_PT updated" "updated (hash updated)" \
"errors!; Body:" "cache errors!' No time interval found to cover startTime=" \
"update dt:" "Excessive scan time" ] }

        ;## set globus version
        catch { exec ls -l $::GLOBUS_LOCATION } data
        if  { ! [ regexp {globus-([^\/]+)} $data -> ::cntlmon::globus_version ] } {
            set  ::cntlmon::globus_version X.X.X
            addLogEntry "cannot determine globus version, set to 0.0.0" blue
        } else {
            set ::cntlmon::globus_version [ string trim $::cntlmon::globus_version ]
        }
        ;## set tclglobus version 
        catch { exec ls -l [ file join $::TCLGLOBUS_DIR libtclglobus_module.so ]  } libdata
        if  { ! [ regexp {tclglobus-([^\/]+)} $libdata -> ::cntlmon::tclglobus_version ] } {
            set  ::cntlmon::tclglobus_version X.X.X
            addLogEntry "cannot determine tclglobus version, set to 0.0.0" blue
        } else {
            set ::cntlmon::tclglobus_version [ string trim $::cntlmon::tclglobus_version ]
        }        
    } err ] } {
        return -code error $err
    }
    
}

## ******************************************************** 
##
## Name: cntlmon::getPid 
##
## Description:
## Given a program name, return the pid if the program
## is running.  Given a pid, returns the program name!
##
## Usage:
##
## Comments:
## Used to get information needed to be able to shutdown
## erstwhile processes.
## Depends on the existence of the "ps" system call.
## If the manager is trying to get the pid of a remote API
## then execssh call will be used to get the pids.

proc cntlmon::getPid { { prog "" } { host "" } } { 
     set tmp  [ list ]
     set data [ list ] 
     if { [ catch {
           if { ! [ string length $host ] } {             
              set data [ shellPipe "|/bin/ps -Ao fname,args,pid" ]
           } else {
              catch { [ execssh $host {/bin/ps -Ao fname,args,pid} ] } data 
           }
     } err ] } {
        return -code error "[ myName ]: $err"
     }   
     
     set data [ lrange [ split $data "\n" ] 1 end ]
     
     if { [ catch {
        foreach line $data {
           set name {}
           set pid  {}
           #if { [ llength $line ] != 2 } { continue }
		   #foreach { name pid } $line { break }
           
		   if	{ [ regexp {(\S+)[\s\t]*(.+)[\s\t]+(\d+)} $line -> name args pid ] } {
                if { ! [ regexp -nocase {[a-z]} $name ] } {
                    set pid $name
                    set name <defunct>
                }
           ;## we are only interested in the base name of
           ;## the process.
                set name [ lindex $name 0 ]
           ;## c++ programmers can wreck us!
                if { [ string equal $name c++ ] } {
                    continue
                } 
		    
                if { [ regexp ^$name  $prog ] || \
                    [ regexp ^$prog $name  ] || \
				    [ regexp $prog  $args  ] } {
                    lappend tmp $pid
                } elseif {   
                    [ regexp ^$pid\$ $prog ] || \
                    [ regexp ^$prog\$ $pid ] } {
                    lappend tmp $name
                }
		   }
        }
     } err ] } {
        ;## the ps call sometimes fails in a strange way
        ;## producing an error in the regexp compilation.
        if { [ regexp {compile regular} $err ] } {
           set vars "(name: '$name' pid: '$pid' prog: '$prog')"
           set err "$err $vars possible failure of ps call."
        }   
        return -code error "[ myName ]: $err"
     }
    
     set tmp [ lsort -dictionary $tmp ]  
     return $tmp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cntlmon::getMountPoints
##
## Description:
## update database table names and cols
##
## Parameters:
##
## Usage:
##   
## Comments:

proc cntlmon::getMountPoints {} {

    if  { ! [ regexp {diskcache} $::API_LIST ] } {
        return
    }
    set slave [ interp create ]
    $slave eval source [ file join $::TOPDIR diskcacheAPI LDASdiskcache.rsc ]
    set ::MOUNT_PT [ $slave eval set ::MOUNT_PT ]
    
    interp delete $slave
    
}  
## ******************************************************** 
##
## Name: cntlmon::validateControl 
##
## Description:
## Validates login and password for functions if provided
## Usage:
##
## Comments:
## client provides control for login and password 
## password is encrypted.

proc cntlmon::validateControl { user { group 0 } } {

    set status priviledged
	if	{ [ catch {     	
		if	{ [ info exist ::globus_control_group ] && [ llength $::globus_control_group ] } {
			set index [ lsearch -exact $::globus_control_group $user ]
			if	{ $index == -1 } {
                if  { $group == 1 } {
					error "User $user is not authorized to perform this function."
				} else {
                    set status normal
                }
			}
		} else {
			error "Please establish a control group alias first."
		}
	} err ] } {
		return -code error $err
	}
	return $status
}

## ******************************************************** 
##
## Name: cntlmon::GlobusChannelRead
##
## Description:
## handler for data read off globus socket from client
##
## Usage:
## 
##
## Comments:

proc cntlmon::GlobusChannelRead { sid } {

	if	{ [ catch {
		set bufdata [ ::read $sid ]
        if	{ [ string length $bufdata ] }  {
    		foreach { user version } $bufdata { break } 
        	set clientStatus [ cntlmon::validateControl $user 0 ]
        	set reply [  validateVersion $version ]
     		set data [ cntlmon::updateClient ]
     		incr cntlmon::clientCount 1
     		set num $cntlmon::clientCount
        	;## temporary hardcoded myself
            
	 		createClient client$::cntlmon::clientCount $user
            
            addLogEntry "registered user $user as $clientStatus client$::cntlmon::clientCount" purple
            set addr quark
            set port globus
            set peerinfo [ fconfigure $sid -peername ]
            set ip   [ lindex $peerinfo 0 ]
        	set port [ lindex $peerinfo 2 ]
            
			set ::client${num}::serverKey x.509
			set ::client${num}::ipaddr "$ip:$port"
        	set ::client${num}::sid $sid
            set ::client${num}::status $clientStatus
            
            ;## redirect socket events to client handler
            fileevent $sid readable [ list ::client${num}::GlobusChannelHandler $sid ]
            
        	# acknowledge login ok 
        	set key x.509
            
        	set msg "nocallback\nclient${num}:$::LDAS_VERSION:$key:$clientStatus\n0\n$data\n$reply"
			set msg [ cmd::size $msg ]
        	puts -nonewline $sid $msg 
            flush $sid 
		}
   } err ] } {
   		addLogEntry "$err; closing globus channel $sid" 2
        catch { puts $sid "nocallback\n0\n3\n$err\n" }
        catch { close $sid }
   }
   if	{ [ eof $sid ] } {
   		addLogEntry "closing globus channel $sid" purple
   		catch { close $sid }
   }
}

## ******************************************************** 
##
## Name: cntlmon:GlobusChannelAccept
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc cntlmon::GlobusChannelAccept { newsock addr port } {

	if	{ [ catch {
    	debugPuts "newsock $newsock addr $addr port $port"
        # fconfigure $newsock -buffering full
        fconfigure $newsock -blocking 0
        fconfigure $newsock -translation binary -encoding binary
        fileevent $newsock readable [ list cntlmon::GlobusChannelRead $newsock ]
    } err ] } {
    	addLogEntry $err 2 
    }
}

## ******************************************************** 
##
## Name: cntlmon:init
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:
## for use of grid_mapfile
## specify non-default in resource ::GRIDMAP
## or tclglobus will pick up /etc/grid-security/grid-mapfile

proc cntlmon::GlobusChannelInit { host port } {

	if	{ [ catch {
    	set mode "gsi service"
    	set cmd "gt_xio_socket -server cntlmon::GlobusChannelAccept"
    	if	{ [ info exist ::GSI_AUTH_ENABLED ] } {
        	if	{ [ string equal "-gsi_auth_enabled" $::GSI_AUTH_ENABLED ] } {
        		append cmd " $::GSI_AUTH_ENABLED"
            } else {
            	set mode "non-gsi service"
            }
        } 
        if	{ [ info exist ::GRIDMAP_ENABLED ] } {        	
        	if	{ $::GRIDMAP_ENABLED } {
        		append cmd " -gridmap_file"
            }
        } 
        append cmd " -myaddr $host $port"
        addLogEntry "globus socket cmd '$cmd'" purple

        set ::cntlmon::GlobusListenSocket [ eval $cmd ]
        addLogEntry "Globus channel $::cntlmon::GlobusListenSocket opened for $mode at $port" blue        
    } err ] } {
    	addLogEntry $err 2
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: cntlmon::openGSISockets
##
## Description:
## update database table names and cols
##
## Parameters:
##
## Usage:
##   
## Comments:

proc cntlmon::openGSISockets {} {

	if	{ [ catch {
		if	{ [ info exist ::USE_GLOBUS_CHANNEL ] && $::USE_GLOBUS_CHANNEL } {        	
    		cntlmon::GlobusChannelInit $::CNTLMON_API_HOST $::TCLGLOBUS_PORT
    	} 
	} err ] } {
    	addLogEntry $err 2 
    }
}

## ******************************************************** 
##
## Name: cntlmon::openGSIClientChannel
##
## Description:
## open a globus tcl channel as client
##
## Usage:
##
## Comments:

proc cntlmon::openGSIClientChannel { host port args } {

	if	{ [ catch {
    	if	{ [ info exist ::GSI_AUTH_ENABLED ] && [ string length $::GSI_AUTH_ENABLED ] } {
    		set sock [ gt_xio_socket -host -timeout $::TCLGLOBUS_SOCKET_TIMEOUT $::GSI_AUTH_ENABLED $host $port ] 
            debugPuts "opened client tclglobus gsi channel $sock to $host:$port with -timeout $::TCLGLOBUS_SOCKET_TIMEOUT secs"
        } else {
        	set sock [ gt_xio_socket -host -timeout $::TCLGLOBUS_SOCKET_TIMEOUT $host $port ] 
            debugPuts "opened client tclglobus non-gsi channel $sock to $host:$port with -timeout $::TCLGLOBUS_SOCKET_TIMEOUT secs"
        }
        ;## dont set buffersize for globus channel
        fconfigure $sock -buffering full -blocking 0
        fileevent $sock readable [ list cntlmon::readGSIClientChannel $sock $args ]
        fconfigure $sock -translation binary -encoding binary
    } err ] } {
    	addLogEntry $err 2
        return -code error $err
    }
    return $sock
}
       
## ******************************************************** 
##
## Name: cntlmon::readGSIClientChannel
##
## Description:
## process results from getMetaData into data sets
##
## Parameters:
##
## Usage:
##
## Comment:
## works for persistent sockets only for now

proc cntlmon::readGSIClientChannel { sid args } {

    if	{ [ catch {  
    	if	{ [ eof $sid ] } {
        	set eof 1
        	error "end of file detected on client globus channel $sid"
        }    
    	set buffer [ read $sid ] 	
        if	{ [ string length $buffer ] } {
			# debugPuts "read [ string length $buffer ] bytes\n$buffer"
            set callback [ lindex $args 0 ]
            set args [ lrange $args 1 end ]
        	lappend args $sid 
            lappend args $buffer 
        	eval $callback $args
        }     
    } err ] } {
    	catch { ::close $sid } err1       
        if	{ ! [ info exist eof ] } { 	
			addLogEntry "Error: $err; $sid closed"  2 
        }
    }

}
       
## ******************************************************** 
##
## Name: cntlmon::updateCleanUp
##
## Description:
## wrapper to eval a set of cmds sent from cmonClient
## and provide the correct response format to client
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::updateCleanup { cmd } {

	if	{ [ catch {
    	addLogEntry "$cmd [ llength $cmd ]" purple
    	set result [ cmdSet $cmd ]
		append result "; set ::JOBS_PURGE_BEFORE $::JOBS_PURGE_BEFORE; "
    	append result "set ::LOGS_PURGE_BEFORE $::LOGS_PURGE_BEFORE; "
    	append result "set ::CLEAR_OUTPUT_PERIOD [ expr $::CLEAR_OUTPUT_PERIOD /1000 ]; "
    	append result "set ::NEXT_CLEANUP_SECS $::NEXT_CLEANUP_SECS ; "
    } err ] } {
    	addLogEntry $err red 
        return -code error "3\n$err"
    }
    return "0\n$result"
}

## ******************************************************** 
##
## Name: cntlmon::styleSheets
##
## Description:
## copy or link style sheets
##
## Parameters:
##
## Usage:
##
## Comments:

proc cntlmon::styleSheets { dir { copy 1 } { linksrc "" } } {

    if  { [ catch {
        foreach file [ glob -nocomplain $::LDAS/doc/testing/system_results/*.css ] {
            set linkname [ file tail $file ]
            if  { $copy } {
                file copy -force $file $dir
            } else {
                set linkname [ file tail $file ]
                file link -symbolic $dir/$linkname $linksrc/$linkname
            }
        }
    } err ] } {
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: cntlmon::expiredCertTest
##
## Description:
## check if ldas service certificate is going to expire
## within a time defined by resource ::SECS_TO_EXPIRE 
##
## Parameters:
##
## Usage:
##
## Comment:
## recompute the secs in case it got updated by user

proc cntlmon::expiredCertTest {} {

    if  { [ catch {
        set secs_to_cert_expire [ expr $::DAYS_TO_CERT_EXPIRE * 24 * 60 * 60 ]
        set rc [ catch { exec openssl x509 -checkend $secs_to_cert_expire \
        -in $::X509_USER_CERT -enddate } data ]
        ;## check for expire/not expire instead of rc
		 if { [	regexp -nocase -- {notAfter=(.+GMT).+will\s+(\S*)\s*expire} $data -> gmt expire ] } {
        	set gmt_secs [ clock scan $gmt -gmt 1 ]
        	set local [ clock format $gmt_secs -format "%x %X %Z" ]
        	set localDays [ clock format $gmt_secs -format "%j" ]
        	set now [ clock format [ clock seconds ] -format "%j" ]
        	set delta [ expr $localDays-$now ]        
        	if  { ! [ string equal not $expire ] } {
            	set subject "Expiration of ldas service certificate"
            	addLogEntry "Subject: $subject; Body: ldas service certificate $::X509_USER_CERT will expire at $local,\
                 $delta days from now (within $::DAYS_TO_CERT_EXPIRE days)" certmail
        	} elseif { $::DEBUG > 2} {
            	addLogEntry "ldas service certificate $::X509_USER_CERT will expire at $local, \
                but not within $::DAYS_TO_CERT_EXPIRE days" purple
        	}
		} else {
			error $data
		}
    } err ] } {
        addLogEntry $err red
    }
}

regexp {(\d+).(\d+).(\d+)} $::LDAS_VERSION -> ::LDAS_ver1 ::LDAS_ver2 ::LDAS_ver3
