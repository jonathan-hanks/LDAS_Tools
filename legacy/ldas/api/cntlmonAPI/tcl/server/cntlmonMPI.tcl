## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon server Tcl Script.
##
## This script returns html file data  for server.
## related to mpiAPI information.
## 
## ******************************************************** 

;## does ps on machine and formats the return for gui
;## will have to ssh for remote machine

package provide cntlmonMPI 1.0

## ******************************************************** 
##
## Name: readData
##
## Description:
## reads mpi return data on emergency socket
##
## Parameters:
##
## Usage:
##
## Comment:
proc readData { sid var } {
	if	{ [ catch {
	    set seqpt "cmd::result $sid"
	    set $var [ cmd::result $sid ]
    } err ] } {
        catch { close $sid }
		puts "readData socket err: $seqpt, $err"
        set $var $err
	}
    catch { close $sid }
}


## ******************************************************** 
##
## Name: cntlmon::getJobInfo
##
## Description:
## retrieve file contents 
##
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getJobInfo {} {
    if  { [ catch { 
    	if	{ ! [ regexp {mpi} $::API_LIST ] } {
    		error "mpiAPI is not on LDAS API list ( $::API_LIST )" 
        }
        set msg "\{puts \$cid \[ array get ::mpi::queue ]\}"
        set result [ getEmergData mpi $msg ]
        set result "0\n$result"
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::setJobChgs
##
## Description:
## send kill job and adjust priority requests to mpi 
##
## Parameters:
##
## Usage:
##
## Comment:
## returns the current job queue after modifications

proc cntlmon::setJobChgs { killlist prilist } {
  
    if  { [ catch { 
    	if	{ ! [ regexp {mpi} $::API_LIST ] } {
    		error "mpiAPI is not on LDAS API list ( $::API_LIST )" 
        }
        foreach job $killlist {
            set msg "\{puts \$cid \[ mpi::killJob $job ]\}"
            set result [ getEmergData mpi $msg ]
            addLogEntry "Killed job $job $result"       
        }
        foreach { job newpri } $prilist {
            set msg "\{puts \$cid \[ mpi::priorityAdjust $job $newpri ]\}"
            set result [ getEmergData mpi $msg ]
            addLogEntry "Priority adjusted job $job $result"       
        }
        set result [ getJobInfo ]     
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::getUserNodes
##
## Description:
## retrieve from mpi queue list of users and nodes used by each 
## in alphabetic order of users
## Parameters:
##
## Usage:
##
## Comment:

proc cntlmon::getUserNodes {} {

	if	{ [ catch {
    	if	{ ! [ regexp {mpi} $::API_LIST ] } {
    		error "mpiAPI is not on LDAS API list ( $::API_LIST )" 
        }
        set msg "\{puts \$cid \[ array get ::mpi::queue ]\}"
        set data [ getEmergData mpi $msg ]
		set data [ split $data \n ]
        set len [ llength $data ]
		if	{ $len == 1 } {
			set data [ lindex $data 0 ]
		}
        set userlist [ list ]
        foreach { name val } $data {			
            if  { [ regexp {(.+),usr} $name -> jobid ] } {
				if	{ ! [ string match NULL $jobid ] } {
                	lappend userlist [ list $name $val ]
					continue
				}
			}
			regexp {(.+),(.+)} $name -> jobid type ]
            if  { [ regexp {nod|dso} $type ] } {
                array set mpiqueue [ list $name $val ]
            } 
        }
        set result ""
        foreach entry [ lsort -ascii -index 0 $userlist ] {
            foreach { name user } $entry { break }
			set nodelist [ list ]
            set name [ split $name , ]
            set job [ lindex $name 0 ]
            if  { [ info exist mpiqueue($job,nod) ] &&  
				 [ llength $mpiqueue($job,nod) ] } {
				foreach mentry $mpiqueue($job,nod) {
					foreach { nodename node } $mentry { break }
					if	{ [ regexp {(\d+)} $nodename -> nodenum ] } {
						lappend nodelist "n$nodenum"
					}
				}
                set numNodes "1+[ llength $nodelist ]"
				set nodelist "0+[ join $nodelist , ]"
            } else {
                set numNodes 0
                set nodelist n0
            }
            if  { [ info exist mpiqueue($job,dso) ] &&
				  [ llength $mpiqueue($job,dso) ] } {
				set sharedobj $mpiqueue($job,dso)
           	} else {
               	set sharedobj none
            }
			if	{ ! [ string length $user ] } {
				set user none
			}
            append result "$user $job $numNodes $nodelist $sharedobj "
        }
		if	{ ! [ string length $result ] } {
			append result "No MPI users found"
		}
        set result "0\n$result"
    } err ] } {
        set result "3\n$err"
    }
    return $result
}

## ******************************************************** 
##
## Name: cntlmon::nodeChgs 
##
## Description:
## sends node change list to mpi to delete and add Nodes
##
## Parameters:
##
## Usage:
##
## Comments:
## supports broadcasting to multi-clients

proc cntlmon::nodeChgs { delnodes addnodes comment } {

	if	{ [ catch {
    	if	{ ! [ regexp {mpi} $::API_LIST ] } {
    		error "mpiAPI is not on LDAS API list ( $::API_LIST )" 
        }
		set update 0
		if	{ [ llength $delnodes ] } {
        	set msg "\{puts \$cid \[ mpi::liveNodelistUpdate ${::RUNCODE}0 remove \{$delnodes\} \]\}"
        	set data [ getEmergData mpi $msg ]
            set subject "Nodes $data removed at $::LDAS_SYSTEM"
            set body "$subject $comment"
			addLogEntry "Subject: $subject; Body: $body" email
			set update 1
		}
		if	{ [ llength $addnodes ] } {
			set msg "\{puts \$cid \[ mpi::liveNodelistUpdate ${::RUNCODE}0 add \{$addnodes\} \]\}"
			set data [ getEmergData mpi $msg ]
            set subject "Nodes $data added at $::LDAS_SYSTEM"
            set body "$subject $comment"
            addLogEntry "Subject: $subject; Body: $body" email
			set update 1
		}
        # addLogEntry "msg to mpi '$msg'" blue
		# if	{ $update } {
		#	cntlmon::getNodeNames
		#}
	} err ] } {
		return "3\n$err"
	}
	return "0\n"
}


## ******************************************************** 
##
## Name: cntlmon::undoNodeChgs 
##
## Description:
## request mpi to undo the last node Chg
##
## Parameters:
##
## Usage:
##
## Comments:
## supports broadcasting to multi-clients

proc cntlmon::undoNodeChgs { { comment "" } } {

	if	{ [ catch {
       	set msg "\{puts \$cid \[ mpi::liveNodelistUpdate ${::RUNCODE}0 undo \]\}"
        set data [ getEmergData mpi $msg ]
        set subject "Last change node at $::LDAS_SYSTEM has been undone"
        set body "$subject $comment"
		addLogEntry "Subject: $subject; Body: $body" email
        ;## mpi will notify cntlmon
		# cntlmon::getNodeNames
	} err ] } {
		return "3\n$err"
	}
	return "0\n[ lsort -dictionary $::beowulfNodes ]"
}

## ******************************************************** 
##
## Name: cntlmon::nodesUsageGraph
##
## Description:
## gathers data for API memory allocations
##
## Parameters:
##
## Usage:
##
## Comment:
## 

proc cntlmon::nodesUsageGraph { range args } {

	if	{ [ catch {
		
		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 	
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/nodeusage/mpi.nod.* ] ] {
				regexp {(\d+)$} [ file tail $file ] -> fendtime 
				if	{ $fendtime < $start_time } {
					continue
		 		}
				if	{ $fendtime > $end_time } {
					lappend filelist $file 
					set done 1
					break 
				} else {
					lappend filelist $file
				}
		}
		if	{ ! $done } {
			lappend filelist [ file join $::LDASLOG mpi.nod ]
		}
		# debugPuts "files $filelist"
				
		set result "0\n"
		set jobtotal 0
		set points [ list ]
		
		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if  { [ catch {
                    set len [ llength $line ]
                    if  { $len != 2 } {
                        error "expected 2 columns, got $len"
                    }
                    foreach { gpstime nodes } $line { break }
                } err ] } {
                    addLogEntry "$file error at '$line': $err" red
                }
				if	{ $gpstime < $start_time || $gpstime > $end_time } {
					continue
				}				 
				lappend points [ list $gpstime $nodes ]
				incr jobtotal 1
				if	{ [ info exist ymax ] } {
					if	{ $nodes > $ymax } {
						set ymax $nodes
					} 
				} else {
					set ymax $nodes
				}
				if	{ [ info exist ymin ] } {
					if	{ $nodes < $ymin } {
						set ymin $nodes
					} 
				} else {
					set ymin $nodes
				}
			}
		}
		if	{ $jobtotal } {
			append result "$jobtotal $ymin $ymax "
			if	{ [ llength $points ] } {
				;## already in time order
				set points [ join $points ]
				append result [ list $points ]
			} 	
		} else {
			append result "0"
		}
	} err ] } {
		set result "3\n$err"
	}
	return $result		

}

## ******************************************************** 
##
## Name: cntlmon::beowulfLoadSummary
##
## Description:
## gathers data for beowulf cluster usage
##
## Parameters:
##
## Usage:
##
## Comment:
## 

proc cntlmon::beowulfLoadSummary { range { userfilter .+} { dsofilter .+} } {

	if	{ [ catch {    
		cntlmon::validateUser $userfilter
		regexp {(\d+)-(\d+)} $range -> start_time end_time ] 	
		
		set filelist [ list ]
		set done 0
		foreach file [ lsort [ glob -nocomplain $::LDASARC/beowulf/beowulf.report.* ] ] {
				regexp {(\d+)$} [ file tail $file ] -> fendtime 
				if	{ $fendtime < $start_time } {
					continue
		 		}
				if	{ $fendtime > $end_time } {
					lappend filelist $file 
					set done 1
					break 
				} else {
					lappend filelist $file
				}
		}
		if	{ ! $done } {
			lappend filelist [ file join $::LDASLOG beowulf.report ]
		}
		# debugPuts "files $filelist"
				
		set result "0\n"
		set jobtotal 0
		set points [ list ]
		
		foreach file $filelist {
            set data [ dumpFile $file ]
            set data [ split $data \n ]
			foreach line $data {
                if  { [ catch {
                    set len [ llength $line ]
                    if  { $len != 5 } {
                        error "expected 5 columns, got $len"
                    }
                    foreach { gpstime nodes runtime user dso } $line { break }
                } err ] } {
                    addLogEntry "$file error at '$line': $err" red
                }
				if	{ $gpstime < $start_time || $gpstime > $end_time } {
					continue
				}	
                if { ! [ regexp $userfilter $user ] } {
					continue
				}
				if	{ ! [ regexp -nocase $dsofilter $dso ] } {
					continue
				}					 
				lappend points [ list $gpstime $nodes $runtime ]
				incr jobtotal 1
			}
		}        
	    
        append result "$range $jobtotal "
        set points [ join $points ]
		append result [ list $points ]

	} err ] } {
		set result "3\n$err"
	}
	return $result		
}


## ******************************************************** 
##
## Name: cntlmon::updateClientNodes
##
## Description:
## broadcast node changes to client after receiving an mpi
## cmd to refresh node list via emergency port.
##
## Parameters:
##
## Usage:
##
## Comment:
## do not update var ::beowulfNodes or it will trigger trace

proc cntlmon::updateClientNodes { args } {
    
    if  { [ catch {  
        set ::NUMNODES [ llength $::beowulfNodes ]
        set nodes [ lsort -dictionary $::beowulfNodes ]
        addLogEntry "updated $::NUMNODES nodes from mpi: $nodes" blue
   	    set msg "\{array set ::beowulfNodes \{ $::LDAS_SYSTEM \{ $nodes \}\};"
        append msg "array set ::NUMNODES \{ $::LDAS_SYSTEM \{ $::NUMNODES \} \} \}"
        cntlmon::notifyClientsNew $msg
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: cntlmon::getNodeMap
##
## Description:
## get list of nodes
##
## Parameters:
##
## Usage:
##   
## Comments:

proc ${API}::getNodeMap {} {

    if  { [ catch {
        if  { [ file exist $::mpiNodeMapFile ] } {
            set ::beowulfNodes [ dumpFile $::mpiNodeMapFile ]
            set ::beowulfNodes [ lsort -dictionary $::beowulfNodes ]
            set ::NUMNODES 1
        }
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: cntlmon::getNodeNamesFromFile
##
## Description:
## decides if mpi is running on a single node or not
## from ::NODENAMES resource var
## Parameters:
##
## Usage:
##   
## Comments:
## set beowulfNodes turn on updates to client

proc ${API}::getNodeNamesFromFile {} {

	if  { ! [ regexp {mpi} $::API_LIST ] } {
    	addLogEntry "mpiAPI is not on LDAS API list ( $::API_LIST )"  blue
        return
    }
    
   	if  { ! [ info exist ::MPI_API_HOST ] } {
    	addLogEntry "There is no mpiAPI host defined"  blue
        return
    }
    
   if   { [ catch {
        set rc [ catch { exec grep "set ::NODENAMES" $::mpirsc } data ]
        if  { !$rc } {
            eval $data
            set ::beowulfNodes [ lsort -dictionary $::NODENAMES ]
            set ::NUMNODES [ llength $::NODENAMES ]
            unset ::NODENAMES
        }
    } err ] } {
        addLogEntry $err 2
        catch { unset ::NODENAMES }
    }
    return                
}

## ******************************************************** 
set ::mpiNodeMapFile [ file join $::TOPDIR mpiAPI virtual.nodemap ]
set ::mpirsc [ file join $::TOPDIR mpiAPI LDASmpi.rsc ]
