 ## ********************************************************
##
## Name: LDASdatabase.rsc
##
## Description:
## resource file for db2utils.
##
## Usage:
##
## Comments:
## All API's have an API specific resource file found in
## the LDAS bin directory.
## ********************************************************

;## TOPDIR is /ldas_outgoing
;## can only output to PUBDIR for web 

;## site specific 
set ::LDASTMP $TOPDIR/tmp
set ::PUBDIR $TOPDIR/jobs
set ::LDASLOG $TOPDIR/logs

;## path of db2 client
set ::db2 "/usr1/ldasdb/sqllib/bin/db2"
;## path of database directory
set ::dbdir "/usr1/databases"

set ::DB2DOC [ file join /doc db2 doc html ]
set ::DB2SCRIPTS [ file join $::LDAS share doc db2 doc text ]
set ::mainhtml [ file join $::LDASLOG LDASdatabase.html ]
set ::gif [ file join /doc gifs footnote.gif ]

set ::api metadata

set ::rx {putMetaData|getMetaData}

set ::dlock yourlock

;## max number of history lines
set ::maxLines 250

## disk percentage full before alarm is raised
set ::diskHiWater 90

;## millisecs between checks for  database manager (10 mins)
set ::dbinterval 600000

;## set DEBUG level
set ::DEBUG 1

;## set diskunit
set ::DISKUNIT GB

;## set diskfactor (1 MB=1048576 bytes, 1 GB=1073741824 bytes)
set ::DISKFACTOR 1073741824.0

;## millisecs delay betwween checks for non-standard database (24 hrs)
set ::DELAY_DB_MONITOR 86400000

;## define performance files and dates 
array set ::rates { ldas-dev { \
    LDAS-0.0.6 cit-rates.html 12-02-1999 \
	LDAS-0.0.8 cit-rates-6.1.html 01-25-2000 \
	LDAS-0.0.9 cit-rates-009.html 02-25-2000 \
	LDAS-0.0.10 cit-rates-010.html 03-28-2000 \
	LDAS-0.0.11 cit-rates-011.0628.html 06-28-2000 \
    LDAS-0.0.11 cit-rates-011.1031.html 10-31-2000 \
    LDAS-0.0.12 cit-rates-012.1104.html 11-04-2000 \
	LDAS-0.0.13 cit-rates-013.0125.html 01-25-2001 \
    LDAS-0.0.14 cit-rates-014.0222.html 02-22-2001 \
    LDAS-0.0.15 cit-rates-015.0306.html 03-06-2001 \
    LDAS-0.0.16 insert.cit_test.ldas-0.0.16.20010409.html 04-09-2001 \
    LDAS-0.0.17 insert.cit_test.ldas-0.0.17.20010612.html 05-15-2001 \
    LDAS-0.0.18 insert.cit_test.ldas-0.0.18.20010613.html 06-12-2001 \
    LDAS-0.0.19 insert.cit_test.ldas-0.0.19.20010718.html 07-18-2001 \
    LDAS-0.0.20 insert.cit_test.ldas-0.0.20.20010816.html 08-16-2001 \
	LDAS-0.0.21 ldas-dev.ldas_tst.ldas-0.0.21.20011018.html 10-18-2001 \
    LDAS-0.0.22 ldas-dev.ldas_tst.ldas-0.0.22.20011107.html 11-07-2001 \
	LDAS-0.0.23 ldas-dev.cit_1.ldas-0.0.23.20011214.html    12-14-2001 \
	LDAS-0.2.0  ldas-dev.cit_1.ldas-0.1.97.20020415.html 04-15-2002 \
	LDAS-0.3.0	ldas-dev.cit_1.ldas-0.2.40.20020605.html 06-05-2002 \
	LDAS-0.4.0	ldas-cit.cit_test.ldas-0.3.41.20020730.html 07-30-2002 \
	LDAS-0.5.0  ldas-dev.cit_1.ldas-0.4.101.20021107.html 11-07-2002 \
    LDAS-0.6.0	ldas-dev.dev_1.ldas-0.5.65.20030117.html 01-17-2003 \
    LDAS-0.7.0	ldas-dev.dev_1.ldas-0.6.111.20030602.html 06-02-2003 \
    LDAS-0.8.0	ldas-dev.dev_1.ldas-0.7.149.20031016.html 10-16-2003 \
    LDAS-0.9.0	ldas-dev.dev_1.ldas-0.8.8.20031027.html 10-27-2003 \
    LDAS-1.0.0	ldas-dev.dev_1.ldas-0.9.97.20040204.html 02-04-2004 \
	LDAS-1.1.0	ldas-dev.dev_1.ldas-1.1.0.20040514.html 05-14-2004 \
	LDAS-1.2.0	ldas-dev.dev_1.ldas-1.2.0.20040831.html 08-31-2004 \
	LDAS-1.3.0  ldas-dev.dev_1.ldas-1.3.0.20041112.html 11-12-2004 \
	LDAS-1.4.0	ldas-dev.dev_1.ldas-1.4.0.20050114.html 01-14-2005 \
	LDAS-1.5.0	ldas-dev.dev_1.ldas-1.5.0.20050215.html 02-15-2005 \
	LDAS-1.6.0  ldas-dev.dev_1.ldas-1.6.0.20050524.html 05-24-2005 \
	LDAS-1.7.0	ldas-dev.dev_1.ldas-1.7.0.20050727.html 07-27-2005 \
	LDAS-1.8.0  ldas-dev.dev_1.ldas-1.8.0.20051031.html 05-31-2005 \
    } }
	
array set ::rates { ldas-wa { \
    LDAS-0.0.6 lho-rates.html 12-04-1999 \
	LDAS-0.0.9 lho-rates-009.html 03-01-2000 \
	LDAS-0.0.11 lho-rates-011.html 06-29-2000 \
    LDAS-0.0.11 lho-rates-011.1031.html 10-31-2000 \
    LDAS-0.0.12 lho-rates-012.1104.html 11-04-2000 \
	LDAS-0.0.13 lho-rates-013.0116.html 01-16-2001 \
    LDAS-0.0.14 lho-rates-014.0222.html 02-22-2001 \
    LDAS-0.0.15 lho-rates-015.0306.html 03-06-2001 \
    LDAS-0.0.16 insert.lho_test.ldas-0.0.16.20010409.html 04-09-2001 \
    LDAS-0.0.17 insert.lho_test.ldas-0.0.17.20010608.html 05-15-2001 \
    LDAS-0.0.18 insert.lho_test.ldas-0.0.18.20010613.html 06-13-2001 \
	LDAS-0.0.19 insert.lho_test.ldas-0.0.19.20010718.html 07-18-2001 \
    LDAS-0.0.21 ldas-wa.ldas_tst.ldas-0.0.21.20011031.html 10-31-2001 \
	} }
	
array set ::rates { ldas-la { \
    LDAS-0.0.6 lho-rates.html 12-08-1999 \
	LDAS-0.0.11 llo-rates-011.html 06-28-2000 \
    LDAS-0.0.11 llo-rates-011.1031.html 10-31-2000 \
    LDAS-0.0.12 llo-rates-012.1104.html 11-04-2000 \
	LDAS-0.0.13 llo-rates-013.0129.html 01-29-2001 \
    LDAS-0.0.14 llo-rates-014.0222.html 02-22-2001 \
    LDAS-0.0.15 llo-rates-015.0306.html 03-06-2001 \
    LDAS-0.0.16 insert.llo_test.ldas-0.0.16.20010409.html 04-09-2001 \
	LDAS-0.0.17 insert.llo_test.ldas-0.0.17.20010716.html 05-15-2001 \
	LDAS-0.0.18 insert.llo_test.ldas-0.0.19.20010718.html 07-18-2001 \
    LDAS-0.0.21 ldas-la.ldas_tst.ldas-0.0.21.20011031.html 10-31-2001 \
	} }
    
array set ::rates { ldas-test { \
	LDAS-0.0.12 cprod-rates-012.0111.html 01-11-2001 \
    LDAS-0.0.14 cprod-rates-014.0222.html 02-22-2001 \
    LDAS-0.0.15 cprod-rates-015.0306.html 03-06-2001 \
    LDAS-0.0.16 insert.cprod_t.ldas-0.0.16.20010409.html 04-09-2001 } }

array set ::rates { ldas-mit { \
	LDAS-0.0.21 ldas-mit.ldas_tst.ldas-0.0.21.20011031.html 10-31-2001 \
	LDAS-0.0.23 ldas-mit.ldas_tst.ldas-0.0.23.20020104.html 01-04-2002 } }

set ::notes(lho_1) { 12-12-2000 lho_1-notes.1212.html \
    12-19-2000 lho_1-notes.1219.html }
    
;## dates of E runs inserting into database
;## default is 
set ::insertdates(lho_1) { "04-03-2000 to 04-04-2000" "E1" \
	"11-08-2000 to 11-15-2000" E2 "03-09-2001 to 03-12-2001" E3 \
    "05-11-2001 to 05-13-2001" E4 }
	
set ::insertdates(lho_2) { "08-03-2001 to 08-06-2001" E5 \
	"11-16-2001 to 11-19-2001" E6 "12-28-2001 to 01-14-2002" E7 }

set ::insertdates(llo_1) { "03-09-2001 to 03-12-2001" E3 \
     "05-11-2001 to 05-13-2001" E4 }
	 
set ::insertdates(llo_2) { "08-03-2001 to 08-06-2001" E5 \
	"11-16-2001 to 11-19-2001" E6 "12-28-2001 to 01-14-2002" E7 }

set ::insertdates(lho_s1) { "02-14-2003 to 08-23-2002" S1 }
set ::insertdates(llo_s1) { "02-14-2003 to 08-23-2002" S1 }

set ::insertdates(lho_s2) { "02-14-2003 to 04-14-2003" S2 }
set ::insertdates(llo_s2) { "02-14-2003 to 04-14-2003" S2 }

set ::insertdates(lho_e10) { "10-17-2003 to 10-24-2003" E10 }
set ::insertdates(llo_e10) { "10-17-2003 to 10-24-2003" E10 }

set ::insertdates(lho_s3) { "10-31-2003 to 01-09-2004" S3 }
set ::insertdates(llo_s3) { "10-31-2003 to 01-09-2004" S3 }

set ::insertdates(lho_e11) { "11-17-2004 to 11-23-2004" E11 }
set ::insertdates(llo_e11) { "11-17-2004 to 11-23-2004" E11 }
set ::insertdates(tst_e11) { "11-17-2004 to 11-23-2004" E11 }
set ::insertdates(mit_e11) { "11-17-2004 to 11-23-2004" E11 }
set ::insertdates(cit_e11) { "11-17-2004 to 11-23-2004" E11 }
