#! /ldcg/bin/tclsh

## ******************************************************** 
## procs to generate a web of table information
## i.e. column data, keys, triggers, indices.
## called by db2utils to generate the page if 
## the page does not exist. 
## The information is static until the tables are updated.
## 
## ********************************************************

package provide db2static 1.0

namespace eval db2utils {
    set event_table_users [ list sngl_datasource \
        sngl_transdata sngl_mime ] 
    set event_tables [ list gds_trigger sngl_inspiral \
        sngl_burst sngl_ringdown sngl_unmodeled \
        sngl_dperiodic ]
} 

;## redefine generic debugPuts
proc debugPuts { msg } {
	if	{ $::DEBUG } {
		puts $msg
	}
}

## ******************************************************** 
##
## Name: setLdasSystemName
##
## Description:
## derives site name 
##
## Parameters:
##
## Usage:
##
## 

proc setLdasSystemName {} {
    if { ! [ info exists ::LDAS_SYSTEM ] } {
	    if	{ [ file exist /etc/ldasname ] } {
            set fd [ open /etc/ldasname r ]
            set ::LDAS_SYSTEM [ read $fd ]
            close $fd 
	    } else {
		    set ::LDAS_SYSTEM localhost
	    }
    }
}

## ******************************************************** 
##
## Name: db2utils::schema
##
## Description:
## creates html page to show table schema
##ddd
## Parameters:
##
## Usage:
##
## This is done via a db2 script selectall.sql
## and parsing the output 

proc db2utils::schema { db } {

    if  { [ catch { 
		db2utils::connect $db
        db2utils::getKeys $db	
		;## include script with ldas release
        set cmd "select tabname,colname,typename,length,nulls \
         from syscat.columns where tabschema='LDASDB' order by tabname,colname"
        set cmd "exec $::db2 $cmd"
        catch { eval $cmd } lines
		set cmd "exec $::db2 terminate"
        set lines [ split $lines \n ]
        if  { [ llength $lines ] < 10 } {
            error $lines
        }
        set prevtable ""
        set fhtml [ open $::LDASLOG/${db}Static.html w ]
        set html "\
<!doctype html public '-/w3c/dtd html 4.0 transitional/en'>\n\
<html>\n\
<head>\n\
   <meta name='GENERATOR' content='Mozilla/4.5 \[en\] (X11; I; SunOS 5.7 sun4u) \[Netscape\]'>\n\
   <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\n\
   <title>LDAS database $db at $::LDAS_SYSTEM</title>\n"
        append html "</head>\n\<p><font size=+1>Table schema for LDAS database $db.</font>\n"
        append html "<p><p><body>\n"
        append html "<p><table border type=1 cellpadding='4'><caption><b>Key for column colors</b></caption>\n"
        append html "<tr>\n"	
	    append html "<td align=left width=300 bgcolor='#aefcaa'>Column is part of the primary key.</td>\n"	
        append html "</tr>\n"
        append html "<tr>\n"
	    append html "<td align=left width=300 bgcolor='#80b0da'>Column is part of a foreign key.</td>\n"	
        append html "</tr>\n"
	    append html "<td align=left width=300 bgcolor='#fccc9a'>Column is not part of any key.</td>\n"	
        append html "</tr>\n"
        append html "<td align=left width=300 bgcolor='#eba4eb'>Column is part of both primary and foreign keys.</td>\n"	
        append html "</tr>\n"
        append html "<td align=left width=300 bgcolor='#fab224'><b>Non null fields are in bold.</b></td>\n"	
        append html "</tr>\n"
        append html "</table>\n"
      
        append html "<p>Check <a href='${db}References.html'>Foreign Key References</a>\n"
        append html "for more information on the foreign keys.\n"
        
        ;## a link to the main page 
        append html "<p><a href='$::mainhtml'>Main Page</a><p>\n"
        
        append html "<table border type=1 cellpadding='4' >"
		append html "<caption><b>$db database schema:</b></caption>\n"
    
        set tabdata [ list ]
        ;## remove the headings

        foreach line $lines {
            if  { [ regexp {^[\t\s]+$} $line ] || ! [ string length $line ] } {
                continue
            }
            foreach { table col type len nulls } $line { break }
            if  { ! [ regexp {^\w+} $table ] || [ regexp -nocase {tabname} $table ] } {           
                continue
            }
            if  { [ string equal $prevtable $table ] } {
                lappend tabdata [ list $col $type $len $nulls ]
            } else  {
                #puts "$db,$prevtable, primary,[ set ::${prevtable}($db,primary) ]"
                #puts "$db,$prevtable, foreign,[ set ::${prevtable}($db,foreign) ]"
                if  { [ string length $prevtable ] } { 
                ;## push out prev table data
                if  { [ llength $tabdata ] } {
                append html "<tr>\n\
    <th align=left bgcolor='#d0b664' width=20>Column</th>\n\
    <th bgcolor='#d0b664' width=20>Type</th>\n\
    <th bgcolor='#d0b664' width=20>Size</th>\n\
    <th align=left bgcolor='#d0b664' width=20>Column</th>\n\
    <th bgcolor='#d0b664' width=20>Type</th>\n\
    <th bgcolor='#d0b664' width=20>Size</th>\n\
    <th align=left bgcolor='#d0b664' width=20>Column</th>\n\
    <th bgcolor='#d0b664' width=20>Type</th>\n\
    <th bgcolor='#d0b664' width=20>Size</th>\n\
    <th align=left bgcolor='#d0b664' width=20>Column</th>\n\
    <th bgcolor='#d0b664' width=20>Type</th>\n\
    <th bgcolor='#d0b664' width=20>Size</th>\n\
</tr>\n"  }                 
                    append html "<tr>\n" 
                    foreach { tab1 tab2 tab3 tab4 } $tabdata {
			            for { set i 1 } { $i < 5 } { incr i 1  } {
				            set tabname [ set tab${i} ]
                            set found 0
				            if	{ [ string length $tabname ] } {
                                # set found 0
                                foreach { col1 type1 len1 null1 } $tabname { break } 
                                ;## the primary key color overrides the foreign key color
                                if  { [ string length $prevtable ] } {
                                if  { [ info exist ::${prevtable}($db,primary) ] && 
                                      [ lsearch -exact [ set ::${prevtable}($db,primary) ] $col1 ] > -1 } {
                                    set found [ expr $found | 1 ]
                                } 
                                if { [ info exist ::${prevtable}($db,foreign) ] && 
                                      [ lsearch -exact [ set ::${prevtable}($db,foreign) ] $col1 ] > -1 } {                                    
                                    set found [ expr $found | 2 ]
                                    # puts "foreign $db,$prevtable,$col1,found=$found"
                                }
                                }
                                switch $found {
                                    1   { set bg #aefcaa }
                                    2   { set bg #80b0da }
                                    3   { set bg #eba4eb }
                                    0   -
                                    default { set bg #fccc9a }                              
                                }
					            append html "<td align=left bgcolor='$bg' width=20 font size=-2>$col1</td>\n"
                                append html "<td align=right bgcolor='#ecea7c' width=10>$type1</td>\n"
                                if  { [ string equal Y $null1 ] } {
                                    append html "<td align=right bgcolor='#fab224' width=10>$len1</td>\n"
                                } else {
                                    append html "<td align=right bgcolor='#fab224' width=10><b>$len1</b></td>\n"
                                }
				            }
                        }
                        append html "</tr>\n"	
                    }
                    append html "</table>\n</tr>\n"
                    set tabdata [ list ]
                }
                if  { [ regexp {^[[:alpha:]]} $table ] } {
                    set prevtable $table
                    append html "<tr>\n\
                        <th align=left bgcolor='#d0b664' width=20>Table</th>\n"
                    append html "<th bgcolor='#d0b664' font size=-1>Column Information</th>\n</tr>\n"
                    append html "<tr>\n<td bgcolor='#d0b664' width=20 font size=-1>$table</td>\n" 
                    append html "<td>\n<table border type=1 cellpadding='4' >"    
                    lappend tabdata [ list $col $type $len $nulls ]
                }
            }
		}
        append html "</table>\n</body>\n</html>"
        puts $fhtml $html
        close $fhtml
        referencePage $db
        schemaCleanup $db
    } err ] } {
        puts "[myName] $err"
        catch { close $fhtml }
    }       
}

## ******************************************************** 
##
## Name: db2utils::getdbs
##
## Description:
## returns the current list of databases
##
## Parameters:
##
## Usage:
##
## done via cmd db2 "list database directories"

proc db2utils::getdbs { var } {       

    if  { [ catch {
        set $var [ list ]
        set cmd "exec $::db2 list database directory on $::dbdir"
        catch { eval $cmd } lines
		;## debugPuts "sql=$cmd"
        set lines [ split $lines \n ]
        set state 0
		set $var [ list ]
        foreach line $lines {
            if  { [ regexp -nocase "Database.*entry.*" $line ] } {
                set state 0
                continue
            } 
            if  { [ regexp -nocase {Database alias.*=\s*(\S+)} $line match dbname] } {
				set dbname [ string tolower $dbname ]
				lappend $var $dbname
                set state 1
                continue
            } 
        }
        debugPuts "databases [ set $var ]"
    } err ] } {
        return -code error "getdb error: $err"
    }
}

## ******************************************************** 
##
## Name: db2utils::connect
##
## Description:
## makes a db2 connection so cmd lines can be issued
##
## Parameters:
##
## Usage:
##
## done via cmd db2 connect

proc db2utils::connect { db } {

    if  { ![ info exist ::${db}(login) ] } {
        source [ file join $::TOPDIR metadataAPI LDASdsnames.ini ]        
    }
    foreach { dbuser dbpasswd } [ set ::${db}(login) ] { break }
	set cmd "connect to $db user ldasdb using [decode64 $dbpasswd]"
    set cmd "exec $::db2 $cmd"
    catch { eval $cmd } data
    debugPuts "db2 connection $data"
    if  { [ regexp {^SQL\d+.+} $data ] } {
        return -code error $data
    }
}

## ******************************************************** 
##
## Name: addlist
##
## Description:
## add a variable to a global list
##
## Parameters:
##
## Usage:
##

proc addlist { globlist var } {

	if	{ ! [ info exist $globlist ] } {
		set $globlist $var
		return
	}
    if  { [ lsearch -exact [set $globlist ] $var ] == -1 } {
        lappend $globlist $var
    }
}

## ******************************************************** 
##
## Name: db2utils::getPrimaryKeys
##
## Description:
## get all the primary key info from a database
## into global array 
##
## Parameters:
##
## Usage:
##

proc db2utils::getKeys { db } {

		;## include script with ldas release
        set cmd "select t.tabname, t.type, k.colname \
        from syscat.tabconst t,syscat.keycoluse k \
        where t.tabname = k.tabname and t.constname = k.constname \
        	order by t.tabname"
		# debugPuts "sql=$cmd"
        set cmd "exec $::db2 $cmd"
        catch { eval $cmd } lines
        set ::tables($db) [ list ]
		set ::columns($db) [ list ]
		;## do not split lines by newline
        set lines [ split $lines \n ]
        foreach line $lines { 
            foreach { tabname type colname } $line {break }
            if  { [ info exist type ] } {
                # debugPuts "tabname $tabname type $type colname $colname"
                if  { [ regexp {^U|P$} $type ] } {
                    lappend ::${tabname}($db,primary) $colname
                    addlist ::tables($db) $tabname
				    addlist ::columns($db) $colname 
                } elseif { [ regexp {^F$} $type ] } {
                    addlist ::tables($db) $tabname
                    addlist ::columns($db) $colname
                    lappend ::${tabname}($db,foreign) $colname
                }
            }                 
        }
        #if  { $::DEBUG } {
        #    foreach tabname $::tables($db) {
		#	    if	{ [ info exist ::${tabname}($db,primary) ] } {
        #    	    debugPuts "$db,$tabname primary keys [ set ::${tabname}($db,primary) ]" 
        #        }
        #        if	{ [ info exist ::${tabname}($db,foreign) ] } {
        #    	    debugPuts "$db,$tabname foreign keys [ set ::${tabname}($db,foreign) ]" 
        #        }
        #    }
        #}
}

## ******************************************************** 
##
## Name: db2utils::createSet
##
## Description:
## create a set for sql selection from set
##
## Parameters:
##
## Usage:
##

proc db2utils::createSet { varlist } {
	set tabset [ list ]
	foreach var [ set $varlist ] {
		set var '[ string toupper $var]'
		lappend tabset $var
	}
	return [ join $tabset , ]
}

## ******************************************************** 
##
## Name: db2utils::getUniqIdCols
##
## Description:
## extract the uniqueIds of a database into global array
##
## Parameters:
##
## Usage:
## 
## Comments
## Must have called getPrimaryKeys to set up the list of primary keys

proc db2utils::getUniqIdCols { db } {
        ;##debugPuts "db2 connection $data"
		;## include script with ldas release
		set tabset [ createSet ::tables($db) ]
		set colset [ createSet ::columns($db) ]
        set cmd "select tabname, colname, typename, length \
        	from syscat.columns where tabname in ( $tabset ) \
			and typename = 'CHARACTER' and length=13 order by tabname"
		debugPuts "sql=$cmd"
        set cmd "exec $::db2 $cmd"
        catch { eval $cmd } lines
		;## do not split lines by newline
        foreach { tabname colname typename length } $lines { 
			debugPuts "$tabname $colname $typename $length"
			if	{ [ regexp -nocase {^character$} $typename ] && $length == 13 } {
                if  { [ info exist ::${tabname}($db,foreign) ] } {
				    if  { [ lsearch -exact [ set ::${tabname}($db,foreign) ] $colname ] == -1 } {                     
					    debugPuts "$db $tabname uniqueId is $colname (foreign)"
					    lappend ::${tabname}($db,uniqueId) $colname
                    } else {
                        lappend ::${tabname}($db,uniqueId,foreign) $colname
                    }
				} elseif { [  info exist ::${tabname}($db,primary) ] } {
				    if  { [ lsearch -exact [ set ::${tabname}($db,primary) ] $colname ] > -1  } {                     
					    debugPuts "$db $tabname uniqueId is $colname (primary)"
					    lappend ::${tabname}($db,uniqueId) $colname
                    } 
                }
			}
		}
}

## ******************************************************** 
##
## Name: db2utils::getForeignKeys
##
## Description:
## extract 
##
## Parameters:
##
## Usage:
## 
## Comments

proc db2utils::getForeignKeys { db } {

	;##debugPuts "db2 connection $data"
	;## include script with ldas release	
    set cmd "select tabname, reftabname, fk_colnames \
   		from syscat.references order by tabname"
	#debugPuts "sql=$cmd"
    set cmd "exec $::db2 $cmd"
   	catch { eval $cmd } lines
	set lines [ split $lines \n ]
	foreach line $lines { 
		if	{ [ regexp {(\S+)\s+(\S+)\s+(\S.+)} $line -> tabname reftabname fk_colnames ] } {
			if	{ [ lsearch -exact $::tables($db) $tabname ] > -1 } {
				debugPuts "$db, $tabname foreign key $reftabname:$fk_colnames"
                if  { [ info exist ::${tabname}($db,uniqueId,foreign) ] } {
				    set uniqIds [ set ::${tabname}($db,uniqueId,foreign) ]
				    foreach colname $fk_colnames {
				        if	{ [ lsearch -exact $uniqIds $colname ]  > -1 } {
						    debugPuts "$db $colname forign key uniqueId"
							set ::${tabname}($db,foreign,$reftabname) \
							    [ string tolower $reftabname:$colname ]
						}
					}
				}
			}
		}
	}
}

## ******************************************************** 
##
## Name: db2utils::colSize
##
## Description:
## determine size of each col based on column header
## for columns with multiple entries.
## line contains the column headers
##
## Parameters:
##
## Usage:
## 
## Comments

proc db2utils::colSize  {} {
    uplevel {
        set str $line 
        set start 0
        set i 0
        while { [ regexp {(\S+\s+)} $str -> col ] == 1 } {
	        set start [ string length $col ]
	        set collen($i) $start 
	        set str [ string range $str $start end ]
	        incr i 1
        }
        set cols [ lsort -integer [ array names collen ] ]
        unset str
        unset start
        unset i
    }
}
## ******************************************************** 
##
## Name: db2utils::outUniqIdFile
##
## Description:
## extract 
##
## Parameters:
##
## Usage:
## 
## Comments
## special handling for sngl_transdata, sngl_datasource,
## sngl_mime that has event_table and event_id to
## reference another event but no relations defined

proc db2utils::outUniqIdFile { db fname } {

	set fd [ open $fname w ]
	set hdr [ subst $::hdr ]
	puts $fd $hdr
	set out ";## tables with uniqueIds\n\n"
	foreach table $::tables($db) {
		if	{ [ info exist ::${table}($db,uniqueId) ] } {
            set tabletext [ string tolower $table ]
            if  { [ lsearch -exact $::db2utils::event_table_users $tabletext ] > -1 } {
                append out ";## "
            }
            puts "$db, $table, uniqueIds [ set ::${table}($db,uniqueId) ]" 
			foreach id [ set ::${table}($db,uniqueId) ] {
				set id [ string tolower $id ]
                if  { [ regexp -nocase {coinc_sngl} $table ] } {
                    if  { ! [ regexp -nocase {coinc_id} $id ] } {
                        append out ";## "
                    }
                }
				append out "set ::[ string tolower $table]\(uniqueId\) \{ $id \}\n"
			}
		}
	}
	;## get the foreign keys
	append out "\n;## tables with foreign keys that reference uniqueIds\n"
	append out ";## uniqueIdCols - child column referenced\n"
	append out ";## <field> points to the parent's column\n\n"
	foreach table $::tables($db) {
        if  { [ regexp -nocase {coinc_sngl} $table ] } {
            append out ";## coinc_sngl not yet implemented\n"
        }
		set fklist [ array names ::${table} $db,foreign,* ] 
        set cols [ list ]
        set tabletext [ string tolower $table ]
		foreach fk_entry $fklist {
			set fk_table [ set ::${table}($fk_entry) ] 
			regexp {([^:]+):(.+)} $fk_table -> reftab colname
			debugPuts "$db $table reftab $reftab colname $colname"
			append out "set ::$tabletext\($colname\) $fk_table\n"
            lappend cols $colname
		}
        if  { [ lsearch -exact $::db2utils::event_table_users $tabletext ] > -1 } {
            lappend cols event_id
            append out "set ::$tabletext\(event_id\) \{ \
            [ join $::db2utils::event_tables ":event_id \\\n " ]:event_id \}\n\n"
        }
        if  { [ llength $cols ] } {
            append out "set ::$tabletext\(uniqueIdCols\)\t{ $cols }\n\n"
        }
	}
	
	puts $fd $out
	close $fd  
} 

## ******************************************************** 
##
## Name: db2utils::referencePage
##
## Description:
## extract 
##
## Parameters:
##
## Usage:
## 
## Comments

proc db2utils::referencePage { db } {

    if  { [ catch {
    set cmd "select tabname, fk_colnames,reftabname, pk_colnames \
   		from syscat.references order by tabname, reftabname"
	#debugPuts "sql=$cmd"
    set cmd "exec $::db2 $cmd"
   	catch { eval $cmd } lines
	set lines [ split $lines \n ]
    set fhtml [ open $::LDASLOG/${db}References.html w ]
    set html "\
<!doctype html public '-/w3c/dtd html 4.0 transitional/en'>\n\
<html>\n\
<head>\n\
   <meta name='GENERATOR' content='Mozilla/4.5 \[en\] (X11; I; SunOS 5.7 sun4u) \[Netscape\]'>\n\
   <meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>\n\
   <title>LDAS database $db at $::LDAS_SYSTEM</title>\n\
</head>\n\
<p><font size=+1>Foreign keys in LDAS database $db.</font>\n\
<p>
<p>"   
    append html "<a href='$::mainhtml'>Main Page</a>"     
    append html "<p><table border type=1 cellpadding='4'><caption><b>Foreign Key References</b></caption>\n"
    append html "<p><p><body>\n"    
                append html "<tr>\n\
    <th align=left bgcolor='#d0b664' width=50>Table</th>\n\
    <th bgcolor='#d0b664' width=100>Column</th>\n\
    <th bgcolor='#d0b664' width=50>Parent Table</th>\n\  
    <th bgcolor='#d0b664' width=300>Referenced Column</th>\n</tr>"
    
    array set colcolor { 0 #fab224 }
    array set colcolor { 1 #ecea7c }
    array set colcolor { 2 #fab224 }
    array set colcolor { 3 #ecea7c }
    
    set line [ lindex $lines 1 ]
    colSize
    foreach line [ lrange $lines 4 end ] {
	    set start 0
	    if	{ [ regexp {^[\s\t\d]*$} $line ] } {
		    continue
	    }
	    if	{ [ regexp {\d+ record} $line ] } {
		    break
	    }
        append html "<tr>\n"
	    foreach col $cols {
		    set end [ expr $start + [ expr $collen($col)-1 ] ]
		    set data [ string range $line $start $end ]
		    regsub -all {\s+} $data { }  data        
		    set data [ string trim $data ]
            if	{ ! [ string length $data ] } {
			    break
		    } 
            set data [ join [ split $data ] , ]
		    set start [ expr $start + $collen($col)	]
            append html "<td align=left bgcolor='$colcolor($col)' width=50 font size=-2>$data</td>\n"
        }
        append html "</tr>\n"
	}  
    append html "</table>\n</body>\n</html>"
    puts $fhtml $html
    close $fhtml   
    } err ] } {
        catch { close $html }
        return $err
    } 
}

## ******************************************************** 
##
## Name: db2utils::cleanup
##
## Description:
## extract 
##
## Parameters:
##
## Usage:
## 
## Comments

proc db2utils::schemaCleanup { db } {
	
	foreach table $::tables($db) {
		catch { unset ::$table($db) }
	}
	unset ::tables
}
