#! /ldcg/bin/tclsh

## ********************************************************
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) cleanup script for /ldas_outgoing
##
## This script removes RDS output files from directories
## and clean up the database used in cmonClient LDASTest.
##
## cleanup of the database could take a while if the
## number of process rows are large.
## ********************************************************

## DO NOT puts to stderr for ssh pipe runs
;## this script runs on cntlmon host e.g. gateway

source /ldas_outgoing/cntlmonAPI/cntlmon.state
source /ldas_outgoing/cntlmonAPI/LDAScntlmon.rsc
source /ldas_outgoing/LDASapi.rsc

;## make sure env is set up for ssh to dataserver and metaserver
catch {
	exec /ldas/bin/ssh-agent-mgr \
	    --agent-file=/ldas_outgoing/managerAPI/.ldas-agent \
	    --shell=tclsh check } err
eval $err

;## cleanup on diskcache host leave directory there for mount point
proc cleanup_RDS { { dirlist {} } { round 0 } } {

	if	{ ! [ string length $dirlist ] } {
    	foreach dir ${::CREATERDS_FRAME_DIR} {
        	append dirlist "$dir/* "
        }
    } 
    set cmd "exec ssh -x -n -obatchmode=yes $::DISKCACHE_API_HOST \
        \"rm -rf $dirlist\""
    catch { eval $cmd } err
	puts "$cmd $err"
   	if 	{ [ string length $err ] && ![ regexp {No match} $err ] } {
        puts stderr "cleanup $err, will retry\n$cmd"
       	if	{ ! [ regexp -nocase -- {Bad file number} $err ]  }  {
            incr round 1
            if 	{ $round < 5 }  {
                cleanup_RDS $dirlist $round
           	}
       	}
	}
    # puts "round $round $err"
}

;## clear database of duplicates marked with version LDASTest in process table
proc cleanup_DB {} {

    set cmd "exec ssh -x -n -obatchmode=yes $::METADATA_API_HOST \
	 	 cd $::TOPDIR/cntlmonAPI && /usr/bin/env HOST=$::METADATA_API_HOST PATH=$::LDAS/bin:$::env(PATH) \
		 LD_LIBRARY_PATH=$::LDAS/lib:$::DB2PATH/lib:/ldcg/lib:$::env(LD_LIBRARY_PATH) \
		 DB2INSTANCE=ldasdb LD_PRELOAD= $::LDAS/bin/del_db_process.tcl $::TEST_DBNAME version=LDASTest"  
		 
    catch { eval $cmd } err
	puts "$cmd\n$err"
}

if	{ $argc } {
	set test [ lindex $argv 0 ]
    switch $test {
    	RDS { set rdsOutput [ lindex $argv 1 ]
        	  cleanup_RDS $rdsOutput
            }
		DB 	{ cleanup_DB } 
    	default { cleanup_RDS
        		cleanup_DB }
   	}
} else {
	cleanup_RDS
	cleanup_DB
}
