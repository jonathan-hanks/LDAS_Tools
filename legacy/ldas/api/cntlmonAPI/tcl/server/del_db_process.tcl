#! /ldcg/bin/tclsh

## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) cleanup script for /ldas_outgoing
##
## This script removes a process row and its associated
## child tables.
##
## usage: del_db_process.tcl <database> colname=xxxx
## e.g. del_db_process.tcl ldas_tst version=LDASTest
## deletion could take a while for large data
## ******************************************************** 

## DO NOT puts to stderr for ssh pipe runs

proc terminate {} {
    catch { file delete -force .del_db_process.lock }
    catch { close $::fd }
    exit
}

proc execdb2cmd {} {
	uplevel {
		set cmd "exec $::db2 $cmd"
        ;## if  { ![ regexp {connect to} $cmd ] } {
        ;##    debugPuts "$cmd"
        ;## }
		catch { eval $cmd } data
        ;## debugPuts $data
	}
}

proc printcnts {} {
	uplevel {
		if	{ [ catch {
            set rows 0
			regexp -- {\n[\s\t]+(\d+)} $data -> rows
			if	{ ! [ info exist ::totalrows($table) ] } {
				set ::totalrows($table) 0
			}
			incr ::totalrows($table) $rows
            if  { $rows } {
			    append output "Found $rows rows for table $table\n"
            }
		} err ] } {
			append output "Error: $err,data=$data\n"
		}
	}
}

proc LDASTestprocess {} {

    uplevel {
        set dmtprogs { BitTest-ldas Slice2-ldas glitchMon-ldas LockLoss-ldas PSLmon-ldas ServoMon-ldas }
        foreach program $dmtprogs { 
            set cmd "select process_id from process where program='$program'"
            execdb2cmd
            append alldata $data
        }
        ;## putStandAlone
        set cmd "select process_id from process where jobid=14080616"
        execdb2cmd
        append alldata $data
        set data $alldata    
        unset alldata
    }
}
    
set TOPDIR /ldas_outgoing
set LDAS /ldas
source /ldas_outgoing/cntlmonAPI/LDASdb2utils.rsc

set auto_path "$::LDAS/lib $auto_path"
source $::LDAS/lib/genericAPI/b64.tcl
source [ file join $::TOPDIR metadataAPI LDASdsnames.ini ]  

proc debugPuts { msg } {
    if  { ![ info exist ::fd ] } {
        set ::fd [ open debug.log w ]
    }
    puts $::fd $msg
}
        
set dsname [ lindex $argv 0 ]
set colparms [ lindex $argv 1 ]
set output ""
if	{ ! [ regexp {([^=]+)=(.+)} $colparms  -> colname value ] } {
	set msg "del_db_process.tcl <database> <colname>=<value>, e.g. del_db_process.tcl ldas_tst program=SegGener"
    puts <html><pre>$msg</pre></html>
	exit
}
if	{ ! [ regexp {^\d+$} $value ] } {
	set value "'$value'"
}

if  { $argc == 3 } {
    set delete [ lindex $argv 2 ]
} else {
	set delete 1
}


;## check for duplicate copies of myself and remove stale locks
if  { [ file exist .del_db_process.lock ] } {
	set mtime [ file mtime .del_db_process.lock ] 
    set now [ clock seconds ]
    if	{ [ expr $now - $mtime ] < 3600 } {
    	set msg "Error! database is being cleared by another process, please resubmit your request later."
    	puts <html><pre>$msg</pre></html>
		exit
    } else {
    	file delete .del_db_process.lock 
    }
} else {
    catch { touch .del_db_process.lock }
}

foreach { dbuser dbpasswd } [ set ::${dsname}(login) ] { break }
set cmd "connect to $dsname user $dbuser using [ decode64 $dbpasswd ]"
execdb2cmd

source $::DB2SCRIPTS/dbtables.tcl

set childtables [concat $tbl_level4 $tbl_level3 $tbl_level2 $tbl_level1]

# first select the process
;## special handling for LDASTest
if  { [ string match "'LDASTest'" $value ] } {
    LDASTestprocess
} else {
    append output "selecting $colname from process from database $dsname\n"
    set cmd "select process_id from process where $colname=$value"
    execdb2cmd
}

set data [ split $data \n ]
set process_id ""
set processes [ list ]
set ::totalrows(process) 0

foreach line $data {
	if	{ [ regexp {^(x'\d+')$} $line -> process_id ] } {
		lappend processes $process_id
	}
}

;## faster for DB2 if select on one process_id
set numprocess [ llength $processes ]
append output "Found $numprocess rows in process table.\n"
#set processes [ join $processes , ]
incr ::totalrows(process) $numprocess

if	{ ! $numprocess } {
    set msg "No rows found in process table for $colname=$value"
	puts "<html><pre>$msg</pre></html>"
	terminate
}

;## first delete all child tables associated with process
foreach table $childtables {
    foreach process_id $processes {
        #set cmd "select count(*) from $table where process_id in ($processes)"
        set cmd "select count(*) from $table where process_id=$process_id"
	    execdb2cmd
        printcnts
	    if  { $delete } {
		    #set cmd "delete from $table where process_id in ($processes)"
            set cmd "delete from $table where process_id=$process_id"
		    execdb2cmd
		    #set cmd "select count(*) from $table where process_id in ($processes)"
            set cmd "select count(*) from $table where process_id=$process_id"
		    execdb2cmd
		    printcnts
            if  { $rows } {
                append output "Error: found $rows after deletion\n"
            }
	    }
    }	
} 

;## delete process 
if  { $delete && $numprocess } {
    foreach process_id $processes {
        set cmd "delete from process where process_id=$process_id"
	    execdb2cmd
	    ;## verify the deletion
	    set cmd "select count(*) from process where process_id=$process_id"
	    execdb2cmd
	    printcnts
    }
}
set cmd terminate
execdb2cmd
puts "<html><pre>$output</pre></html>"
terminate



