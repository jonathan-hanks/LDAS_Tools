#! /ldcg/bin/64/tclsh

## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) cleanup script for /ldas_outgoing
##
## This script removes by default job directories over
## 1 month old and log directories over 6 months 
##
## ******************************************************** 

set auto_path "/ldas/lib $auto_path"

if  { [ file exist /ldas_outgoing/cntlmonAPI/cntlmon.state ] } {
    source /ldas_outgoing/cntlmonAPI/cntlmon.state
    source /ldas_outgoing/LDASapi.rsc
    source /ldas_outgoing/cntlmonAPI/LDAScntlmon.rsc
} else {        
    set ::LDASARC /ldas_outgoing/logs/archive
    set ::RUNCODE [string toupper [exec cat /etc/ldasname]]
    set ::LDASTMP $TOPDIR/tmp
    set ::PUBDIR  $TOPDIR/jobs
}

set API test
package require gpstime

set one_month  [ expr 60*60*24*30 ]
set six_months [ expr $one_month * 6 ]
set one_year [ expr $one_month * 12 ]

proc debugPuts { args } {
    if  { $::debug } {
        puts stderr [ join $args ]
    }
}

;## save inode info before deleting
proc record_inode { dir } {	
 	catch { exec ls -ilR $dir } data 
    set fd [ open $::env(HOME)/inodelist a+ ]
    puts $fd $data
    close $fd
}

    
if  { $argc } {
    set debug [ lindex $argv 0 ]
} else {
    set debug 0
}

set ::API_LIST [ concat $::API_LIST wrapper ]
namespace eval ldasCleanup {}

## ******************************************************** 
##
## Name: ldasCleanup::clearLogs
##
## Description:
## clean expired jobs,logs,stats,reports
##
## Parameters:
##
## Usage:
##
## Comment:

proc ldasCleanup::clearJobs {} {
        
    set tag Jobs
    if	{ [ catch {    
	    if	{ ! [ info exist ::JOBS_PURGE_BEFORE ] } {
		    set joblimit [ expr $::curtime - $::one_month ]
	    } else {
		    set joblimit [ expr $::curtime - $::JOBS_PURGE_BEFORE ]
	    }
	    puts "Removing job directories older than $joblimit \
	    [ clock format $joblimit -format "%x %X %Z" ]..."
               
	    set jobdirs [ glob -nocomplain $::PUBDIR/${::RUNCODE}* ]
	    ;## deleting job output 
	    foreach jobdir $jobdirs {
        
        if  { [ catch {
		    set modtime [ file mtime $jobdir ]
            set modtimeStr [ clock format $modtime -format "%x %X %Z" ]
            regexp "$::PUBDIR/(\\S+)" $jobdir -> jobdir1
		    if	{ $modtime < $joblimit } {
                # record_inode $jobdir
				#catch { $raw_exec rm -rf $jobdir } err
                puts "deleted $jobdir $modtimeStr"
                file delete -force $jobdir
			    continue 
		    }
		    set subdirs [ glob -nocomplain $jobdir/${::RUNCODE}* ]
		    foreach subdir $subdirs {	
                if  { [ catch {
                    set modtime [ file mtime $subdir ]
                    set modtimeStr [ clock format $modtime -format "%x %X %Z" ]
                    regexp "$::PUBDIR/(\\S+)" $subdir -> subdir1
			        if 	{ $modtime < $joblimit } {
                        # record_inode $subdir
                        puts "$subdir1 $modtimeStr"
                        file delete -force $subdir
                    }
                } err ] } { 
                    puts "${subdir} error: $err , file exist ? [ file exist $subdir ]"
                    ;## PR 2868: work around for dev being unable to delete some files
                    # catch { file delete -force $subdir } err1
                    # record_inode $subdir
					catch { exec rm -rf $subdir } err1
                    puts "[ info level 0 ] deleted ${subdir1} via 'rm -rf' $err1, exist ? [ file exist $subdir ]"
		        }
            }
        } err ] } {
            puts "$jobdir1 error: $err" red
            ;## PR 2868: work around for dev being unable to delete some files
            # catch { file delete -force $jobdir } err
            # record_inode $jobdir
			catch { exec rm -rf $jobdir } err1 
            puts "[ info level 0 ] $jobdir1 error: $err, deleted $jobdir via 'rm -rf' $err1, exist ? [ file exist $jobdir ]"
            continue
        }  
        ;## end foreach jobdir      	
	    }
    } err ] } {
        puts "Error: $err"
    }
}

## ******************************************************** 
##
## Name: ldasCleanup::clearLogs
##
## Description:
## clean expired jobs,logs,stats,reports
##
## Parameters:
##
## Usage:
##
## Comment:

proc ldasCleanup::clearLogs {} {

    if  { [ catch {
	    ;## deleting logs
        set tag Logs
        set result "deleting Logs: "
        if	{ ! [ info exist ::LOGS_PURGE_BEFORE ] } {
		    set loglimit [ expr $::curtime - $::six_months ]
	    } else {
		    set loglimit [ expr $::curtime - $::LOGS_PURGE_BEFORE ]
	    }
        set localtime [ clock format $loglimit -format "%x %X %Z" ]
	    set gpslimit [ utc2gps $localtime 0 ]
        puts "Removing $::LDASARC log files older than $localtime ( $gpslimit )..."
	    foreach api $::apilist {
            if  { [ catch {
                set apidir [ file join $::LDASARC ${api}API ]
                if  { [ file exist $apidir ] } {
                    set files [ glob -nocomplain $apidir/* ]
                    foreach file $files {
					    catch { unset linked }
                        if  { [ catch {
        	                if	{ [ regexp {(\d+)$} $file -> endtime ] } {
                                if	{ $endtime < $gpslimit } {
                                    set html1 [ file link $::LDASLOG/LDAS${api}.1.html ]
                                    set html2 [ file link $::LDASLOG/LDAS${api}.2.html ]
								    if	{ [ regexp $file $html1 ] } {
									    set linked $::LDASLOG/LDAS${api}.1.html
								    } elseif { [ regexp $file $html2 ] } {
									    set linked $::LDASLOG/LDAS${api}.2.html
								    } else {
                                        # record_inode $file
                                        regexp "$::LDASARC/(\\S+)" $file -> file1
									    file delete -force $file
                                        puts $file1
                               	    }
                                }
                            }
           	            } err ] } {
                            puts  "[ info level 0 ] Error cleaning $api $file: $err"
                        }
                    }              
                }
            } err ] } {
                puts "[ info level 0 ] Error cleaning $apidir: $err"
            }
	    }
    } err ] } {
	    puts  "[ info level 0 ] Error $err"      
    }
}

## ******************************************************** 
##
## Name: ldasCleanup::clearStats
##
## Description:
## clean expired jobs,logs,stats,reports
##
## Parameters:
##
## Usage:
##
## Comment:

proc ldasCleanup::clearStats {} {

    if  { [ catch {
	    ;## deleting logs
        set tag Logs
        set result "deleting Logs: "
        if	{ ! [ info exist ::STATS_PURGE_BEFORE ] } {
		    set statslimit [ expr $::curtime - $::one_year ]
	    } else {
		    set statslimit [ expr $::curtime - $::STATS_PURGE_BEFORE ]
	    }
        set localtime [ clock format $statslimit -format "%x %X %Z" ]
	    set gpslimit [ utc2gps $localtime 0 ]
        puts "Removing statistics files older than $localtime ( $gpslimit )..."
        set stats [ list jobstats_archive memusage nodeusage shutdown_status ]
	    foreach item $stats {
            if  { [ catch {
                set statsdir [ file join $::LDASARC $item ]
                if  { [ file exist $statsdir ] } {
                    set files [ glob -nocomplain $statsdir/* ]
                    foreach file $files {
                        if  { [ catch {
        	                if	{ [ regexp {(\d+)$} $file -> endtime ] } {
                                if	{ $endtime < $gpslimit } {
                                    regexp "$::LDASARC/(\\S+)" $file -> file1
									file delete -force $file
                                    puts $file1
                                }
                            }
           	            } err ] } {
                            puts  "[ info level 0 ] Error cleaning $api $file: $err"
                        }
                    }              
                }
            } err ] } {
                puts "[ info level 0 ] Error cleaning $apidir: $err"
            }
	    }
    } err ] } {
	    puts  "[ info level 0 ] $err"      
    }
}

## ******************************************************** 
##
## Name: ldasCleanup::isRelease
##
## Description:
## remove expired test reports
##
## Parameters:
##
## Usage:
##
## Comment:

proc ldasCleanup::isRelease { item } {

    if  { [ catch {
        if  { [ regexp {\d+\.\d+\.0[^\d]+} $item ] } {
            set rc 1
        } else {
            set rc 0
        }
    } err ] } {
        return -code error $err
    }
    return $rc
}
    
## ******************************************************** 
##
## Name: ldasCleanup::clearGeneral
##
## Description:
## general proc to remove expired files from a directory
##
## Parameters:
## resource var - controls when they expire
## pattern - the file pattern to find them
##
## Usage:
##
## Comment:
## release reports are never removed

proc ldasCleanup::clearGeneral { resource pattern tag } {

    if  { [ catch {
        if  { [ regexp {^::} $resource ] } {
            set resource ::$resource
        }
        if	{ ! [ info exist $resource  ] } {
		    set reportlimit [ expr $::curtime - $::one_year ]
	    } else {
		    set reportlimit [ expr $::curtime - [ set $resource ] ]
	    }
        set localtime [ clock format $reportlimit -format "%x %X %Z" ]
	    set gpslimit [ utc2gps $localtime 0 ]
        puts "Removing $tag older than $localtime ( $gpslimit )..."
        
        set reports [ glob -nocomplain $pattern ]
	    foreach resultdir $reports {
            ;## dont delete gifs and style sheets
            if  { ! [ file isdirectory $resultdir ] } {
                continue
            }
            if  { [ ldasCleanup::isRelease $resultdir ] } {
                puts "skipping $resultdir"
                continue
            }
            if  { [ catch {
                if  { [ file exist $resultdir ] } {
                    set modtime [ file mtime $resultdir ]
                    set modtimeStr [ clock format $modtime -format "%x %X %Z" ]
                    set subdir [ file tail $resultdir ]
			        if 	{ $modtime < $reportlimit } {
                        puts "$subdir $modtimeStr"
                        file delete -force $resultdir
                    }
                }
            } err ] } {
                puts "[ info level 0 ] Error cleaning $resultdir: $err"
            }
	    }
    } err ] } {
	    puts  "[ info level 0 ]: $err"      
    }
}

## ******************************************************** 
##
## Name: main
##
## Description:
## clean expired jobs,logs,stats,reports
##
## Parameters:
##
## Usage:
##
## Comment:

;## main
    if  { [ catch {
        set ::apilist [ concat manager diskcache datacond frame eventmon ligolw metadata cntlmon mpi wrapper ]
        set ::curtime [ clock seconds ]
        set ::curtimeStr [ clock format $::curtime -format "%x %X %Z" ]
        puts "At $::curtimeStr"
        ldasCleanup::clearJobs
        ldasCleanup::clearLogs
        ldasCleanup::clearStats
        ldasCleanup::clearGeneral ::SYSREPORT_PURGE_BEFORE $::LDASTMP/SystemTest* "System Test Reports"
        ldasCleanup::clearGeneral ::TESTREPORT_PURGE_BEFORE $::LDASARC/cmonClient/* "Nightly Test Reports"
    } err ] } {
        puts "main Error: $err"
    }

