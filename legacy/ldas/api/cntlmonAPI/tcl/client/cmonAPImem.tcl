## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles LDAS log filtering 
##
## cmonAPImem Version 1.0
##
## ******************************************************** 

package provide cmonAPImem 1.0

namespace eval cmonAPImem {
    set notebook ""
    set pages { cmonAPImem }
	set tab(cmonAPImem) "API Memory Usage "	
    set cputab(cmonAPImem) "API CPU Usage "	
    set threadtab(cmonAPImem) "API Thread Usage "
	set title(cmonAPImem) "API Memory Usage" 
	set xlabel(cmonAPImem) "GPS Time"
	set ylabel(cmonAPImem) "Memory\nallocated\n(MB)"
	set graph apimem
	set wTop .tmpAPImem
    set wCPU .tmpAPIcpu
    set wThread .tmpAPIthread
	set APIs $::API_LIST(no-server)

    set OFFSET_TIME 0
    set OFFSET_JOBS 1
    set OFFSET_YMIN 2
    set OFFSET_YMAX 3
    set OFFSET_RESTART 4
    set OFFSET_APIDATA 5
    set OFFSET_CPUDATA 6
    set OFFSET_CORETIMES 7
    set OFFSET_THREADDATA 8
    
}


## ******************************************************** 
##
## Name: cmonAPImem::create
##
## Description:
## creates tab for this page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook tab
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAPImem::create { notebook } {

    $notebook insert end cmonAPImem -text "API" \
        -createcmd "cmonAPImem::createPages $notebook" \
        -raisecmd "cmonCommon::pageRaise cmonAPImem cmonAPImem; \
            cmonCommon::setLastTime cmonAPImem cmonAPImem"
}


## ******************************************************** 
##
## Name: cmonAPImem::create 
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAPImem::createPages { notebook0 } {

	if	{ [ catch { 
		set fnb [ $notebook0 getframe cmonAPImem ]
        set page [ lindex $::cmonAPImem::pages 0 ]
        	
		set ::cmonAPImem::notebook [ NoteBook $fnb.nb \
        -foreground white -background $::darkgreen -bd 2 ]
		
        set notebook $::cmonAPImem::notebook
        set frame [ $notebook insert end cmonAPImem:$page \
			-text "System Resources" ]	
	
		specific $frame $page 
        array set ::cmonAPImem::state [ list $page 0 ]	
        set ::cmonAPImem::prevsite($page) $::cmonClient::var(site)
		cmonAPItimes::create $notebook
		$notebook compute_size
	    pack $notebook -fill both -expand yes -padx 4 -pady 4
        $notebook raise [ $notebook page 0 ]
        
	} err ] } {
		puts $err
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cmonAPImem::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAPImem::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]
    set labf1 [LabelFrame $parent.labf1 -text "Select an API and times" -side top -anchor w \
                   -relief sunken -borderwidth 2 ]
    set subf  [$labf1 getframe]
	set ::cmonAPImem::topfw($page) $subf
    
	set subf1 [ frame $subf.subf1 -relief sunken -borderwidth 2 ]
	
	;## select APIs 
	set name [ namespace tail [ namespace current ] ]	
	createAPICheckList $subf1 $name $page 
	    
	;## allow plot of points, lines or both
	set f3 [ frame $subf1.fit1 -relief groove -borderwidth 2 ]
	set but1 [ checkbutton $f3.memUsage -text "show Memory Usage" -width 20 \
    	-variable ::cmonAPImem::var($page,memUsage) -onvalue 1 -offvalue 0 ]
	set but2 [ checkbutton $f3.cpuUsage -text "show CPU Usage" -width 20 \
    	-variable ::cmonAPImem::var($page,cpuUsage) -onvalue 1 -offvalue 0 ]
    set f4 [ frame $subf1.fit2 -relief groove -borderwidth 2 ]
    set but3 [ checkbutton $f4.threadUsage -text "show Thread Usage" -width 20 \
    	-variable ::cmonAPImem::var($page,threadUsage) -onvalue 1 -offvalue 0 ]
	pack $but1 $but2 -side left -anchor w -padx 2
    pack $but3 -side left -anchor w 
    pack $f3 $f4 -side top -expand 1 -fill both
    
    set ::cmonAPImem::memUsagew($page) $but1
	set ::cmonAPImem::var($page,memUsage) 1
	
	set f2 [ frame $subf.ftime -relief groove -borderwidth 2 ]
	
	set f [ cmonCommon::createTimeOptions $f2 $name $page ]

	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $subf.ftime $name $page ] { break }
	
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x -expand 1
	
	set f3 [ frame $f2.but -relief groove -borderwidth 2 ]
	set  but1 [ Button $f3.start -text "SUBMIT" -width 6 -helptext "submit one time request to server" \
        -command  "cmonAPImem::sendRequest $page" ]
    set  but2 [ Button $f3.down -text "CLOSE GRAPH WINDOWS" -width 25 -helptext "close all graph windows" \
        -command  "cmonAPImem::closeWindows $name $page" ]
	pack $but1 -side left -padx 10 -pady 2
    pack $but2 -side right -padx 5 -pady 2
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x -expand 1
	pack $f3 -side top -padx 1 -pady 1 -fill both -expand 1
	pack $subf1 $f2 -side left -padx 1 -pady 1 -fill both -expand 1
	;## place the cmd status on the bottom
	
	set labref [LabelFrame $parent.refresh -text "Command Status" -side top -anchor w \
                   -relief sunken -borderwidth 4]
                   
    set subf2  [ $labref getframe ]
	
	set statusw [ ::createStatus  $subf2 ]
	array set ::cmonAPImem::statusw [ list $page $statusw ]
	
	set ::cmonAPImem::bstart($page) $but1  
    pack $labf1 -side top -fill x
	pack $labref -side top -fill both -expand 1

}

## ******************************************************** 
##
## Name: cmonAPImem::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
## this proc name is generic for all packages.
## sample test times 
## start 		05/04/00 16:38:27 641493520
## end times 	05/04/00 18:13:24 641499217
##	start		957892986	641928192
## end 		957893198	641928404
##	start		957892986	641928192
## end			641928192	641928192
## start		now
## end			now
	
proc cmonAPImem::sendRequest { page { apilist {} } } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonAPImem::statusw($page) "Please select an LDAS site"
		return
	}
	set name [ namespace tail [ namespace current ] ]
    
    if  { ! [ llength $apilist ] } {
	    set apilist [ ::setAPIs $page $name ]
        if  { ! [ llength $apilist ] } {
            appendStatus $::cmonAPImem::statusw($page) "Please select at least one API"
            return
        }
    }
    if	{ ! [ set ::cmonAPImem::var($page,memUsage) ] && 
    	  ! [ set ::cmonAPImem::var($page,cpuUsage) ] && 
          ! [ set ::cmonAPImem::var($page,threadUsage) ] } {
        appendStatus $::cmonAPImem::statusw($page) "Please select at least one graph type:\
        Memory, CPU or Thread Usage"
        return
    }
    if	{ $::USE_GLOBUS } {
    	cmonAPImem::sendRequestGlobus $page $apilist 
        return
    }
        
    if  { [ catch {
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
        ;## backward compatibility
        set ::cmonAPImem::xmin($page) $starttime
		set ::cmonAPImem::xmax($page) $endtime
    } err ] } {
		cmonAPImem::state0 $page 
		appendStatus $::cmonAPImem::statusw($page) $err
        return
	}
    
    set client $::cmonClient::client
    set cmdId "new"
    set freq 0
    set repeat 0
    cmonAPImem::state1 $page 
    foreach api $apilist {
    	if	{ [ catch {
            set servercmd "cntlmon::memoryUsageGraph $api " 
            append servercmd $starttime-$endtime
            ;## use a separater between each request
       	    set cmd "cmonAPImem::showReply ${page}_$api\n$repeat\n$freq\n\$client:$cmdId\n$servercmd"  
            sendCmd $cmd
        } err ] } {
        	appendStatus $::cmonAPImem::statusw($page) "$api $err"
        }
    } 
}

;## bundle all requests into one so cntlmon can get all at once
proc cmonAPImem::sendRequestGlobus { page { apilist {} } } {

	set name [ namespace tail [ namespace current ] ]
    if  { [ catch {
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
        ;## backward compatibility
        set ::cmonAPImem::xmin($page) $starttime
		set ::cmonAPImem::xmax($page) $endtime
    } err ] } {
		cmonAPImem::state0 $page 
		appendStatus $::cmonAPImem::statusw($page) $err
        return
	}
    
    set client $::cmonClient::client
    set cmdId "new"
    set freq 0
    set repeat 0
    cmonAPImem::state1 $page 
    set cmdlist [ list ]
    foreach api $apilist {
            set servercmd "cntlmon::memoryUsageGraph $api " 
            append servercmd $starttime-$endtime
            ;## use a separater between each request
       	    set cmd "cmonAPImem::showReply ${page}_$api\n$repeat\n$freq\n\$client:$cmdId\n$servercmd"  
            set cmd [ cmd::size $cmd ]
            append cmdlist $cmd
    }
    
    if	{ [ catch { 
    	sendCmd $cmdlist
    } err ] } {
        appendStatus $::cmonAPImem::statusw($page) "$api $err"
    }
}

## ******************************************************** 
##
## Name: cmonAPImem::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonAPImem::showReply { page rc clientCmd html } {

	if	{ [ catch {
    set name [ namespace tail [ namespace current ] ]
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "in showReply, rc=$rc, page=$page, client=$client,cmdId=$cmdId, afterid=$afterid"
	# puts "html size [ string length $html ]"
    set ::cmonAPImem::cmdId($page) $cmdId 
    set ::cmonClient::client $client
    
    regexp {([^_]+)_([^_]+)} $page -> page api

	switch $rc {
	0 -
	2 {	if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
		    cmonAPImem::dispGraph $page $api $html 
			appendStatus $::cmonAPImem::statusw($page) updated 0 blue
        } else {
            appendStatus $::cmonAPImem::statusw($page) \
                "No data" 
        }
		if	{ ! [ string length $afterid ] } {
                cmonAPImem::state0 $page 
            } else {
                cmonAPImem::state2 $page 
        }
	  }
	3 {
		appendStatus $::cmonAPImem::statusw($page) $html
		;## may need to cancel request	
		cmonAPImem::state0 $page 	
	  }
	}
	} err ] } {
		appendStatus $::cmonAPImem::statusw($page) $err
		cmonAPImem::state0 $page 
	}
}

##******************************************************* 
##
## Name: cmonAPImem::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonAPImem::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonAPImem::topfw($page) normal
	array set ::cmonAPImem::state [ list $page 0 ]
}

##******************************************************* 
##
## Name: cmonAPImem::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonAPImem::state1 { page } {
	set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonAPImem::topfw($page) disabled	
    $::cmonAPImem::bstart($page) configure -state disabled 
	array set ::cmonAPImem::state [ list $page 1 ]
	
}

##******************************************************* 
##
## Name: cmonAPImem::formatTimePeriod
##
## Description:
## set label text to nulls 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPImem::formatTimePeriod {} {

	uplevel {
        regexp {(\d+)-(\d+)} $range -> gps_start gps_end 
        set localstart [ gps2utc $gps_start 0 ]
        set localend   [ gps2utc $gps_end 0 ]
	}
}


##*******************************************************
##
## Name: cmonAPImem::memoryUsageStats 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPImem::memoryUsageStats { page statsdata parent api } {

	if	{ [ catch {
        set range [ lindex $statsdata $::cmonAPImem::OFFSET_TIME ]
        cmonAPImem::formatTimePeriod 
        
  		set ::${parent}(ptitle) "$api API memory usage @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
        
        set text [ list ]
        lappend text  "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)" [ list brown fixed ]
        
		set jobs [ lindex $statsdata $::cmonAPImem::OFFSET_JOBS ]

		set ymin [ lindex $statsdata $::cmonAPImem::OFFSET_YMIN ] 
		set ymax [ lindex $statsdata $::cmonAPImem::OFFSET_YMAX ] 
		set restarttimes [ lindex $statsdata $::cmonAPImem::OFFSET_RESTART ]
  
		;## do not get api from here since data could be blank
		set apidata [ lindex $statsdata $::cmonAPImem::OFFSET_APIDATA ]

        if  { [ llength $statsdata ] > $::cmonAPImem::OFFSET_CPUDATA } {
            set coretimes [ lindex $statsdata  $::cmonAPImem::OFFSET_CORETIMES ]
        } else {
            set coretimes {}
        }

		set numSets [ llength $apidata ]
        cmonAPImem::formatcoreRestartLine
        lappend text "\n$jobs jobs in $api" [ list $::Color($api) bold ]
        lappend text "$coreRestartLine" [ list brown bold ]
        
		# set ::${parent}(ptitle) "$api API memory usage @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
		
		set ::${parent}(title)   "$api $::cmonAPImem::title($page)"
		set ::${parent}(y1label) "Memory allocated ( MB )"
        ;## convert from bytes to MB 1/1024 = 0.0009765625
		#set ::${parent}(y1factor) 0.001024
        set ::${parent}(y1factor) 0.0009765625
		set ::${parent}(submit) "cmonAPImem::sendRequest $page $api"
			
		set lastSet [ expr $numSets - 1 ]
		set leastsqPoints [ list ]
		set xmin [ expr double($gps_start) ]
		set xmax [ expr double($gps_end) ]
		set alldata [ list ]
		
		if	{ ! [ string length $ymin ] } {
			set ymin 0
			set ymax 5
		} 
        if  { $ymin && $ymin == $ymax } {
            set ymax [ expr $ymin + 5000 ]
            incr ymin -5000
        }
				
		set set 0
		set i 2
		if	{ $jobs } {
		foreach data $apidata { 
			set numPoints [ expr [ llength $data ]/2 ]
			cmonCommon::leastSquaresInit
			if	{ $numPoints > 1 } {
				set points $data
				if	{ $numPoints > 2  } {
				;## set least square fit
					cmonCommon::leastSquareCoefficients
				} else {
					cmonCommon::fit2points
				}
				;## dont extraplolate put in begin and end points 
				cmonCommon::leastSquarePoints 
				foreach { xdelta ydelta } [ cmonCommon::slope $newpoints ] { break }
				
				if	{ $ydelta } {
					set rate $b
					set jobrate [ expr $ydelta/$numPoints ]
				} else {    
					set rate 0.0
                    set jobrate 0.0
				}
                set rate [ format %.3f $rate ]
                if  { $rate < 0.010 } {
                    set ystart [ lindex $ylist 0 ]
                    set yend   [ lindex $ylist end ]
                    if  { [ expr abs($ystart - $yend) ] < 0.010 } {
                        set rate 0.000
                        set jobrate 0.0
                        set xstart [ lindex $xlist 0 ]
                        set xend   [ lindex $xlist end ]
                        set newpoints [ list $xstart $ystart $xend $yend ]
                    }
                }
				;## adjust ymin and ymax for least square points
				foreach { ymin ymax } [ cmonCommon::computeYMinMax $newpoints $ymin $ymax ] { break }
				lappend text "[ format "\nLeast Squares fitted rate for period [ expr $set + 1 ]: %s KB/sec " $rate ]" [ list #b6558e bold ]
				if	{ [ info exist sigma ] } {
					lappend text "( +/- [ format %.3f $sigma ] KB/sec )" [ list #b6558e bold ]
				}
				lappend text " [ format "%.3f KB/job" $jobrate ]" [ list #b6558e bold ]
				set newpoints${set} $newpoints
			}
			set alldata [ concat $alldata $data ]
			incr set 1
		}
        cmonAPImem::mergeCoreDumpStart
        lappend text "\n$newtimesLine" [ list brown bold ]
		catch { unset newpoints }
		}
        updateTextWin [ set ::${parent}(textw) ] $text
        
		set linewidth 2 			
		set bltdata [ list ]
       
        foreach { reduced_data msg } [ cmonCommon::reduceDisplayData $alldata $::MAX_GRAPH_POINTS ] { break }
        if  { [ llength $msg ] } {
            appendStatus $::cmonAPImem::statusw($page) "$api memory usage: $msg" 0 brown
        }	
		if	{ [ string length [ info vars newpoints* ] ] } {	
		    foreach set [ lsort -dictionary [ info vars newpoints* ] ] {
				regexp {(\d+)} $set -> i
				lappend bltdata [ set $set ]
				lappend bltdata ${api}_fitted$i
				lappend bltdata none 
				lappend bltdata #b6558e
				lappend bltdata 3	
			}
			lappend bltdata $reduced_data
			lappend bltdata ${api}_memory
			lappend bltdata circle 
			lappend bltdata $::Color($api)	
			lappend bltdata 0	
			
		
		} elseif { $jobs > 1 } {
			lappend bltdata $reduced_data
			lappend bltdata $api
			lappend bltdata circle 
			lappend bltdata $::Color($api)	
			lappend bltdata 3
		} else {
			set bltdata [ list ]
			lappend bltdata $reduced_data
			lappend bltdata $api
			lappend bltdata circle 
			lappend bltdata $::Color($api)	
			lappend bltdata 0
		}
		if	{ [ llength $restarttimes ] } {
			set restartpts [ list ]
			set i 0
			foreach x $restarttimes {	
				set restartpts [ list ]				
				lappend restartpts $x
				lappend restartpts $ymin
				lappend restartpts $x
				lappend restartpts $ymax
					
				lappend bltdata $restartpts
				lappend bltdata ${api}_restart$i
				lappend bltdata none 
				lappend bltdata #f8929e
				lappend bltdata 2
				incr i 1	
			}
		}
        if	{ [ llength $coretimes ] } {
			set corepts [ list ]
			set i 0
			foreach x $coretimes {	
				set corepts [ list ]				
				lappend corepts $x
				lappend corepts $ymin
				lappend corepts $x
				lappend corepts $ymax
					
				lappend bltdata $corepts
				lappend bltdata ${api}_core$i
				lappend bltdata none 
				lappend bltdata #1220ac
				lappend bltdata 2
				incr i 1	
			}
		}					        
		set graph [ blt_graph::showGraph $bltdata $gps_start \
				$gps_end $ymin $ymax $parent	]
		$graph legend configure -hide yes			 
		raise $parent
		update idletasks

	} err ] } {
		debugPuts "$api memory page error $err"
		appendStatus $::cmonAPImem::statusw($page) $err
	}
}


##*******************************************************
##
## Name: cmonAPImem::cpuUsageStats
##
## Description:
## graph the cpu data from $api.mem files
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPImem::cpuUsageStats { page statsdata parent api } {

	if	{ [ catch {

        set range [ lindex $statsdata $::cmonAPImem::OFFSET_TIME ]
        cmonAPImem::formatTimePeriod 
        
        set ::${parent}(ptitle) "$api API CPU usage @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
        set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown fixed ]
        
		set jobs [ lindex $statsdata $::cmonAPImem::OFFSET_JOBS ]

		set restarttimes [ lindex $statsdata $::cmonAPImem::OFFSET_RESTART ]

		;## do not get api from here since data could be blank
        set alldata [ lindex $statsdata $::cmonAPImem::OFFSET_CPUDATA ]
        
        if  { [ llength $statsdata ] > $::cmonAPImem::OFFSET_CPUDATA } {
            set coretimes [ lindex $statsdata $::cmonAPImem::OFFSET_CORETIMES ]
        } else {
            set coretimes [ list ]
        }
        cmonAPImem::formatcoreRestartLine
        
        lappend text "$jobs jobs in $api API" [ list $::Color($api) bold ]
        lappend text $coreRestartLine [ list brown bold ]
        
		# set ::${parent}(ptitle) "$api API CPU usage @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
			
		set ::${parent}(title)   "$api CPU Usage"
		set ::${parent}(y1label) "CPU used ( % )"
		set ::${parent}(submit) "cmonAPImem::sendRequest $page $api"
			
		set xmin $gps_start
		set xmax $gps_end
        
		updateTextWin [ set ::${parent}(textw) ] $text
		if	{ $jobs } {
            cmonAPImem::mergeCoreDumpStart
		}
		set linewidth 2 			
		set bltdata [ list ]
        
        set ymin 0
        set ymax 2
        foreach { ymin ymax } [ cmonCommon::computeYMinMax $alldata $ymin $ymax ] { break }
        set ymax [ expr int($ymax * 1.5 ) ]
        
        foreach { reduced_data msg } [ cmonCommon::reduceDisplayData $alldata $::MAX_GRAPH_POINTS ] { break }
        if  { [ llength $msg ] } {
            appendStatus $::cmonAPImem::statusw($page) "$api cpu usage: $msg" 0 brown
        }        
		set bltdata [ list ]
        
	    lappend bltdata $reduced_data
		lappend bltdata ${api}_cpu
		lappend bltdata circle 
		lappend bltdata $::Color($api)	
		lappend bltdata 0

		if	{ [ llength $restarttimes ] } {
			set restartpts [ list ]
			set i 0
			foreach x $restarttimes {	
				set restartpts [ list ]				
				lappend restartpts $x
				lappend restartpts $ymin
				lappend restartpts $x
				lappend restartpts $ymax
				
				lappend bltdata $restartpts
				lappend bltdata ${api}_cpu_restart$i
				lappend bltdata none 
				lappend bltdata #f8929e
				lappend bltdata 2
				incr i 1	
			}
		}
        if	{ [ llength $coretimes ] } {
			set corepts [ list ]
			set i 0
			foreach x $coretimes {	
				set corepts [ list ]				
				lappend corepts $x
				lappend corepts $ymin
				lappend corepts $x
				lappend corepts $ymax
					
				lappend bltdata $corepts
				lappend bltdata ${api}_cpu_core$i
				lappend bltdata none 
				lappend bltdata #1220ac
				lappend bltdata 2
				incr i 1	
			}
		}		        
		set graph [ blt_graph::showGraph $bltdata $gps_start \
				$gps_end $ymin $ymax $parent	]
		$graph legend configure -hide yes			 
		raise $parent
		update idletasks

	} err ] } {
		puts $err
		appendStatus $::cmonAPImem::statusw($page) $err
	}
}

##*******************************************************
##
## Name: cmonAPImem::threadUsageStats
##
## Description:
## graph #threads data from $api.mem files
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPImem::threadUsageStats { page statsdata parent api } {

	if	{ [ catch {
        set alldata [ lindex $statsdata $::cmonAPImem::OFFSET_THREADDATA ]

        set range [ lindex $statsdata $::cmonAPImem::OFFSET_TIME ]
        cmonAPImem::formatTimePeriod 
  		set ::${parent}(ptitle) "$api API thread usage @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
        set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown fixed ]
        
		set jobs [ lindex $statsdata $::cmonAPImem::OFFSET_JOBS ]

		set restarttimes [ lindex $statsdata $::cmonAPImem::OFFSET_RESTART ]
		;## do not get api from here since data could be blank
        
        ;## backward compatibility
        if  { [ llength $statsdata ] > $::cmonAPImem::OFFSET_CPUDATA } {
            set coretimes [ lindex $statsdata $::cmonAPImem::OFFSET_CORETIMES ]
        } else {
            set coretimes [ list ]
        }

        cmonAPImem::formatcoreRestartLine
        lappend text "$jobs jobs in $api API" [ list $::Color($api) bold ]
        lappend text $coreRestartLine [ list brown bold ]
            
		#set ::${parent}(ptitle) "$api API thread usage @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
			
		set ::${parent}(title)   "$api thread Usage"
		set ::${parent}(y1label) "# threads used"
		set ::${parent}(submit) "cmonAPImem::sendRequest $page $api"
			
		set xmin $gps_start
		set xmax $gps_end
		
		if	{ $jobs } {
            cmonAPImem::mergeCoreDumpStart
		}
        
        updateTextWin [ set ::${parent}(textw) ] $text
        
		set linewidth 2 			
		set bltdata [ list ]
        
        set ymin 0
        set ymax 2
        foreach { ymin ymax } [ cmonCommon::computeYMinMax $alldata $ymin $ymax ] { break }
        set ymax [ expr int($ymax * 1.5 ) ]
        
        foreach { reduced_data msg } [ cmonCommon::reduceDisplayData $alldata $::MAX_GRAPH_POINTS ] { break }
        if  { [ llength $msg ] } {
            appendStatus $::cmonAPImem::statusw($page) "$api thread usage: $msg" 0 brown
        }        
	    lappend bltdata $reduced_data
		lappend bltdata ${api}_threads
		lappend bltdata circle 
		lappend bltdata $::Color($api)	
		lappend bltdata 0

		if	{ [ llength $restarttimes ] } {
			set restartpts [ list ]
			set i 0
			foreach x $restarttimes {	
				set restartpts [ list ]				
				lappend restartpts $x
				lappend restartpts $ymin
				lappend restartpts $x
				lappend restartpts $ymax
					
				lappend bltdata $restartpts
				lappend bltdata ${api}_thread_restart$i
				lappend bltdata none 
				lappend bltdata #f8929e
				lappend bltdata 2
				incr i 1	
			}
		}
        if	{ [ llength $coretimes ] } {
			set corepts [ list ]
			set i 0
			foreach x $coretimes {	
				set corepts [ list ]				
				lappend corepts $x
				lappend corepts $ymin
				lappend corepts $x
				lappend corepts $ymax
					
				lappend bltdata $corepts
				lappend bltdata ${api}_thread_core$i
				lappend bltdata none 
				lappend bltdata #1220ac
				lappend bltdata 2
				incr i 1	
			}
		}		        
		set graph [ blt_graph::showGraph $bltdata $gps_start \
				$gps_end $ymin $ymax $parent	]
		$graph legend configure -hide yes			 

	    updateTextWin [ set ::${parent}(textw) ] $text 
		if	{ [ string length $text ] } {
			set ::${parent}(extralines) $text
		}
		raise $parent
		update idletasks	

	} err ] } {
		appendStatus $::cmonAPImem::statusw($page) $err
	}
}

##*******************************************************
##
## Name: cmonAPImem::dispGraph
##
## Description:
## display graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPImem::dispGraph { page api html } {

	set site [ string trim $::cmonClient::var(site) ]
    set parent $::cmonAPImem::wTop${page}${api}	
    if  { $::cmonAPImem::var($page,memUsage) } {
        if  { ! [ winfo exist $parent ] } {
            toplevel $parent 
	        set statusw [ createTextWin $parent 5 ]  
            set ::${parent}(textw) $statusw
        }    
	    wm deiconify $parent
	    wm title $parent "$api $::cmonAPImem::tab($page)@$site"
        cmonAPImem::memoryUsageStats $page $html $parent $api
    } else {
        catch { destroy $parent }
    }
    
    set parent $::cmonAPImem::wCPU${page}${api}	
    if  { $::cmonAPImem::var($page,cpuUsage) } {
        if  { ! [ winfo exist $parent ] } {	
            toplevel $parent 
            set statusw [ createTextWin $parent 5 ]  
            set ::${parent}(textw) $statusw
        }    
	    wm deiconify $parent
        wm title $parent "$api $::cmonAPImem::cputab($page)@$site"
        cmonAPImem::cpuUsageStats $page $html $parent $api
    } else {
        catch { destroy $parent }
    }
    
    set parent $::cmonAPImem::wThread${page}${api}	
    if  { $::cmonAPImem::var($page,threadUsage) } {
        if  { ! [ winfo exist $parent ] } {	
            toplevel $parent 
            set statusw [ createTextWin $parent 5 ]  
            set ::${parent}(textw) $statusw
        }    
	    wm deiconify $parent
        wm title $parent "$api $::cmonAPImem::threadtab($page)@$site"
        cmonAPImem::threadUsageStats $page $html $parent $api
    } else {
        catch { destroy $parent }
    }
    
}


##*******************************************************
##
## Name: cmonAPImem::mergeCoreDumpRestart
##
## Description:
## merge times for coredump and restart so they can be
## listed in time order
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:	

proc cmonAPImem::mergeCoreDumpStart {} {

    uplevel {
        set newtimes [ list ]
        set newtimesLine ""
        ;## merge restart and core times 
        foreach gpstime $restarttimes {
            lappend newtimes [ list $gpstime "Restarted " ]
        }
        foreach gpstime $coretimes {
            lappend newtimes [ list $gpstime Coredumped ]
        } 
        set newtimes [ lsort -index 0 -integer $newtimes ]
        foreach entry $newtimes {
            foreach { gpstime mode } $entry { break } 
            set utctime [ gps2utc $gpstime 0 ]
            append newtimesLine "$mode at $utctime\n" 
        }
        set newtimesLine [ string trim $newtimesLine \n ]
    }
}

##*******************************************************
##
## Name: cmonAPImem::formatcoreRestartLine
##
## Description:
## format core restart line for graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:	

proc cmonAPImem::formatcoreRestartLine {} {

    uplevel {
        set numRestarts [ llength $restarttimes ]
        set numCores [ llength $coretimes ]
        set coreRestartLine " "
        if  { $numCores } {
            append coreRestartLine "dumped core $numCores times  "
        }
        if  { $numRestarts } {
            append coreRestartLine "restarted $numRestarts times  "	
        } 
    }

}

## ******************************************************** 
## 
## Name: cmonAPImem::closeWindows
##
## Description:
## create database tables widget
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonAPImem::closeWindows { name page } {

    foreach api [ set ::${name}::APIs ] {
    
        set win [ set ::${name}::wTop ]${page}${api}
        if  { [ winfo exist $win ] } {
            destroy $win
        }
        set win [ set ::${name}::wCPU ]${page}${api}
        if  { [ winfo exist $win ] } {
            destroy $win
        }
        set win [ set ::${name}::wThread ]${page}${api}
        if  { [ winfo exist $win ] } {
            destroy $win
        }
    }
}

;## initialize length of ytick
if  { ! [ info exist ::YTICK_FMTSTR ] } {
    set ::YTICK_FMTSTR "%10.10s"
}

proc verify_mem_data { data } {

    set ylist [ list ]
    set ymblist [ list ]
    foreach { x y } $data {
        lappend ylist [ list $x $y ]
        lappend ymblist [ list $x [ expr $y/1024.0 ] ]                  
    }
    set fd [ open temp.dat1 w ]
    puts $fd "ylist [ lsort -integer -index 1 -unique $ylist ]\n
ymblist [ lsort -real -unique -index 1 $ymblist ]" 
    close $fd
}
