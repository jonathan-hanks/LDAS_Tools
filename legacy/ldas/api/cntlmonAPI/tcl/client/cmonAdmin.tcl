## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles administration of LDAS user etc.
##
## repeat and frequency are here for conformance to
## standard communication with server and are not actually used.
## ******************************************************** 

package provide cmonAdmin 1.0

namespace eval cmonAdmin {

	set userlen 20
	set emaillen 30
	set name [ namespace tail [ namespace current ] ]
	set pages { cmonAdmin }
	set expired [ clock scan "12/31/[clock format [clock seconds] -format "%Y"]" ]
	set fields { usrname passwd email fullname phone expires }
   
}

## ******************************************************** 
##
## Name: cmonAdmin::create
##
## Description:
## creates tab for this page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook tab
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAdmin::create { notebook } {

    set page [ lindex $::cmonAdmin::pages 0 ]
    set pagetext "User Admin" 
    $notebook insert end cmonAdmin -text $pagetext \
        -createcmd "cmonAdmin::createPages $notebook $page" \
        -raisecmd [ list cmonCommon::pageRaise cmonAdmin $page "LDAS users" ]
}

## ******************************************************** 
##
## Name: cmonAdmin::createPages
##
## Description:
## creates all status pages
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAdmin::createPages { notebook page } {

    set frame [ $notebook getframe cmonAdmin ]

	set topf  [frame $frame.topf]		
    specific $topf $page	
	set ::cmonAdmin::topfw($page) $topf
	pack $topf -fill x -pady 1

	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }

	set titf2 [TitleFrame $pane2.titf2 -text "Command Status" -font $::ITALICFONT]
	;## create a status to display errors and status
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonAdmin::statusw [ list $page $statusw ]
	pack $titf2 -pady 1 -padx 1 -fill both -expand yes

	;## create a scrollable window to hold variable entries
	set titf3 [TitleFrame $pane3.titf2 -text "LDAS users" -font $::ITALICFONT]
	set ::cmonAdmin::titf3($page) $titf3 
    cmonCommon::pageUpdate cmonAdmin $page "LDAS users"
    
	set frame3 [ $titf3 getframe ]
	set sw [ ScrolledWindow $frame3.sw -relief sunken -borderwidth 2 ]
	foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }	
	set lbhdr [listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single -relief ridge \
        -font $::LISTFONT ]
	$lbhdr insert end [ format "%20.20s %20.20s %20.20s %20.20s"  "User Id" "User Id" "User Id" "User Id" ]

	$sw setwidget $lbhdr
	set ::cmonAdmin::lbhdr($page) $lbhdr
		
	set sf [ScrollableFrame $sw.f -height 1000 -areaheight 0 ]
		
	$sw setwidget $sf
	;## make the two lists scroll together
			
	$hscroll configure -command [ list ::BindXview [ list $lbhdr $sf ] ]
		
	set subf [$sf getframe]
	set subfh [ frame $subf.fhdr -relief sunken -borderwidth 2 ]
	set ::cmonAdmin::varFrame($page) $subfh
	set ::cmonAdmin::varFrameParent($page) [ winfo parent $subfh ]
	pack $sw -fill both -expand yes	
	pack $titf3 -pady 1 -padx 1 -fill both -expand yes
    set ::cmonAdmin::titf3($page) $titf3 
	pack $pw1 -side top -fill both -expand 1
    array set ::cmonAdmin::state [ list $page 0 ]
    set ::cmonAdmin::prevsite($page) $::cmonClient::var(site)
    return $frame
}

## ******************************************************** 
##
## Name: cmonAdmin::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAdmin::specific { parent page } {

	#set titf1 [TitleFrame $parent.titf1 -text \
	#"Select an option, fill in the information and hit GO" -font $::ITALICFONT ]
	#set psubf [ $titf1 getframe ]
	set labref [LabelFrame $parent.labref -text "" -side top -anchor w \
                   -relief sunken -borderwidth 4 ]
    set subf2  [$labref getframe] 
	# set f1 [ frame $subf2.f1 -relief ridge -borderwidth 3 ]
	# label $f1.lbl -text "Select one of these options: "
	# pack $f1.lbl $f1.chk3 $f1.chk4 $f1.chk5 -side left -padx 2 -pady 2 
	# pack $f1.lbl $radbut1 $radbut2 $radbut3 -side left -padx 2 -pady 2 
    
	set ::cmonAdmin::var($page,admincmd) updateUser
	
	set f2 [ frame $subf2.f2 -relief ridge -borderwidth 3 ]
	label $f2.lbl -text "User information: "
	set username_ent   [LabelEntry $f2.usrname -label "User Name: " -labelwidth 15 -labelanchor w \
                   -textvariable ::cmonAdmin::var($page,usrname) -editable 1 -width 20 -labeljustify right \
                   -helptext "LDAS user name"]
	set passwd_ent   [LabelEntry $f2.passwd -label "Password: " -labelwidth 15 -labelanchor w \
                   -textvariable ::cmonAdmin::var($page,passwd) -editable 1 -width 20 -labeljustify right \
				   -helptext "LDAS user password" -show *]
				   
	set email_ent   [LabelEntry $f2.email -label "Email: " -labelwidth 15 -labelanchor w \
                   -textvariable ::cmonAdmin::var($page,email) -editable 1 -width 20 -labeljustify right \
                   -helptext "user's email address"]	
	set fullname_ent   [LabelEntry $f2.fullname -label "Full Name: " -labelwidth 15 -labelanchor w \
                   -textvariable ::cmonAdmin::var($page,fullname) -editable 1 -width 20 -labeljustify right \
                   -helptext "LDAS user's full name"]
	set phone_ent   [LabelEntry $f2.phone -label "Phone: " -labelwidth 15 -labelanchor w \
                   -textvariable ::cmonAdmin::var($page,phone) -editable 1 -width 20 -labeljustify right \
                   -helptext "LDAS user's phone" ]
	set expires_ent   [LabelEntry $f2.expires -label "Expires: " -labelwidth 15 -labelanchor w \
                   -textvariable ::cmonAdmin::var($page,expires) -editable 1 -width 20 -labeljustify right \
                   -helptext "user's pasword expiration date"]		
	$username_ent bind <Return> "focus $passwd_ent"
	$passwd_ent bind <Return> "focus $email_ent"
	$email_ent bind <Return> "focus $fullname_ent"
	$fullname_ent bind <Return> "focus $phone_ent"
	$phone_ent bind <Return> "focus $expires_ent"
	$expires_ent bind <Return> "focus $username_ent"
    lappend ::cmonAdmin::entrywidgets($page) $username_ent $passwd_ent $email_ent \
        $fullname_ent $phone_ent $expires_ent
	 
	pack $f2.lbl -side top 
	pack $f2.usrname $f2.passwd $f2.email $f2.fullname $f2.phone $f2.expires -side top -fill x 
	pack $subf2.f2 -side top -fill both -expand 1         
	
	set  f2 [ frame $subf2.faction -relief flat ]
    #set but1 [ Button $f2.start -text "Update user" -helptext "update/delete user" \
    #    -command  "cmonAdmin::sendRequest $page" ]
    
	set but2 [ Button $f2.refresh -text "Refresh user's List" -helptext "get current manger's queue" \
        -command  "cmonAdmin::sendRequest $page getUsersQueue" ]
	set radbut1 [ radiobutton $f2.chk3 -text "Add LDAS User" -variable ::cmonAdmin::var($page,admincmd) \
		-anchor w -value addUser -command "cmonAdmin::setUser $page {} {} {}; cmonAdmin::sendRequest $page"  \
        -command "cmonAdmin::sendRequest $page" ] 
	set radbut2 [ radiobutton $f2.chk4 -text "Update User Info" -variable ::cmonAdmin::var($page,admincmd) \
		-anchor w -value updateUser -command "cmonAdmin::sendRequest $page" ]
	set radbut3 [ radiobutton $f2.chk5 -text "Delete User" -variable ::cmonAdmin::var($page,admincmd) \
		-anchor w -value deleteUser -command "cmonAdmin::sendRequest $page" ]
    set but3 [ radiobutton $f2.blockusr -text "Block user" -bg red -fg white -relief raised \
        -variable ::cmonAdmin::var($page,admincmd) -command "cmonAdmin::sendRequest $page" \
		-anchor w -value blockUser ]  
                        
	set ::cmonAdmin::bstart2($page) $but2
	set ::cmonAdmin::bstart3($page) $but3
    
	pack $subf2.f2 -side top -fill x 
    pack $but2 $radbut1 $radbut2 $radbut3 -side left -padx 2 -pady 2 
    pack $but3 -side right -padx 2 -pady 2
	pack $f2 -side bottom -fill x -expand 1
	pack $labref -side left -fill both -expand 1

}

## ******************************************************** 
##
## Name: cmonAdmin::sendRequest 
##
## Description:
## send request to server for controlling individual APIs
##
## Parameters:
## page name
##
## Usage:
##  send request to server using standard protocol
## 
## Comments:
## this proc name is generic for all packages.

proc cmonAdmin::sendRequest { page { type "" } } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonAdmin::statusw($page) "Please select an LDAS site"
		return 
	}
	if	{ ! [ string match getUsersQueue $type ] } {
		foreach field $::cmonAdmin::fields  {
			set value [ string trim $::cmonAdmin::var($page,$field) ]
			if	{ ! [ string length $value ] || [ regexp {^\[\s\t]*$} $value ] } {
				appendStatus $::cmonAdmin::statusw($page) "$field must not be blank"
				return
			}
			set $field $value
			if	{ [ regexp {(usrname|passwd|email)} $field ] } {
				if	{ [ regexp {\s+} $value ] } {
					appendStatus $::cmonAdmin::statusw($page) "$field cannot have embedded blanks"
					return
				}
			}
		}
		if	{ [ string equal $usrname $passwd ] } {
			appendStatus $::cmonAdmin::statusw($page) "user name and password cannot be the same"
			return
		}
		if 	{ ! [ regexp {.+@.+\..+} $email ] } {
           	appendStatus $::cmonAdmin::statusw($page) "invalid e-mail: '$email'"
			return
        }
	}
	
    set client $::cmonClient::client

    set cmdId "new"
    set repeat 0
    set freq 0

	set login ""
	set passwd ""
	
	if	{ [ catch {
    	foreach { login passwd } [ validateLogin 1 ] { break }
	} err ] } {
		appendStatus $::cmonAdmin::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
	
	if	{ [ catch {
		if	{ [ string match getUsersQueue $type ] } {
			set cmd "cmonAdmin::showReply $page\n$repeat\n$freq\n\
        	$client:$cmdId:$login:$passwd\ncntlmon::getUsersQueue" 
		} else { 
            if  { [ regexp -nocase blockUser $::cmonAdmin::var($page,admincmd) ] } {
                set bg [ $::cmonAdmin::bstart3($page) cget -bg ]
                if  { [ string match green $bg ] } {
                    set admincmd unblockUser
                } else {
                    set admincmd blockUser 
                }
            } else {
                set admincmd $::cmonAdmin::var($page,admincmd)
            }
            ;## must place encrypted data in {} to get passed as one arg
			set data "$::cmonAdmin::var($page,usrname) $::cmonAdmin::var($page,passwd) \
			$email [ list $fullname ] [ list $phone ] [ list $expires ]"
			set data "\{[ base64::encode64 [ binaryEncrypt $::CHALLENGE $data ] ]\}"
			set cmd "cmonAdmin::showReply $page\n$repeat\n$freq\n\
       		$client:$cmdId:$login:$passwd\ncntlmon::useradmin $login $admincmd $data" 			 
		}
		# puts "cmd=$cmd"                                                                                                                                    
		cmonAdmin::state1 $page 
		sendCmd $cmd   	
	} err ] } {
       	cmonAdmin::state0 $page 
		appendStatus $::cmonAdmin::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonAdmin::showReply 
##
## Description:
## display result of cmd
## cmd 
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.
## invoked asynchronously when the reply is received from server

proc cmonAdmin::showReply { page rc clientCmd html } {  
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
	switch $rc {
	0 {	set status ""; set data ""
		# regexp {^([^\.]+[\.]*) (.+)} $html -> status data 
        regexp {^([^\n]+)\n(.+)} $html -> status data 
		if	{ [ string length $status ] } {
			appendStatus $::cmonAdmin::statusw($page) $status 0 blue
		}		
		if	{ [ string length $data ] } {
			set data [ binaryDecrypt $::CHALLENGE [ decode64 $data ] ]
			;## remove nonprintable chars 
			set last [ string last \} $data ]
			set data [ string range $data 0 $last ]
			cmonAdmin::dispUsersQueue $page $data 
        }
	  }
	3 {
		appendStatus $::cmonAdmin::statusw($page) $html
	  }
	}
	cmonAdmin::state0 $page	
}

## ******************************************************** 
##
## Name: cmonAdmin::dispUsersQueue
##
## Description:
## extract configurable resources from rsc file
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## also display vars that cannot be changed

proc cmonAdmin::dispUsersQueue { page data } {

	if	{ [ catch {
		set i 0

		;## create a header 
		set parent $::cmonAdmin::varFrameParent($page) 
		;## must destroy widget in parseRsc or globalVar array cannot be unset
		catch { destroy $::cmonAdmin::varFrame($page) }
	
		set subfh [ frame $parent.fhdr -relief sunken -borderwidth 2 ]
		set ::cmonAdmin::varFrame($page) $subfh
		set ::cmonAdmin::varFrameParent($page) [ winfo parent $subfh ]
	
		set i 0
		if	{ [ llength $data ] == 1 } {
			set data [ lindex $data 0 ]
		} 
		set user ""
		set passwd ""
		set email {}
		set fullname {}
		set phone {}
		set expires {}
        set blocked 0
		foreach { user passwd email fullname phone expires blocked } $data {
			if	{ ! [ expr $i % 4 ] } {
				set subf1 [ frame $subfh.f$i -relief sunken ]
				pack $subf1 -side top -fill x -expand 1
				set sep ""
			} else {
				set sep [ Separator $subf1.sep$i -bg blue -orient vertical -relief ridge ]
				pack $sep -side left  -fill both -anchor w -padx 2 
			}
			set but [ button $subf1.user$i -text $user \
				-command  "cmonAdmin::setUser $page $user $passwd $email $fullname $phone $expires $blocked" \
				-font $::LISTFONT -bg PapayaWhip -width $::cmonAdmin::userlen ]
					
			pack $but -side left  -fill both -anchor w -padx 2 
            if  { $blocked >= 0 } {
                $but configure -fg red
            } 
			incr i 1
			set user ""
			set passwd ""
			set email {}
			set fullname {}
			set phone {}
			set expires {}
            set blocked
		}
		pack $subfh -side top -fill both -expand 1
		appendStatus $::cmonAdmin::statusw($page) "$i users registered" 0 blue
	} err ] } {
		puts "[myName],$page,$err"
		appendStatus $::cmonAdmin::statusw($page) "[myName] $err"
	}
}

## ******************************************************** 
##
## Name: cmonAdmin::setUser
##
## Description:
## set entries field to selected user
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## 

proc cmonAdmin::setUser { page user passwd email fullname phone expires blocked } {

	set ::cmonAdmin::var($page,usrname) $user
	set ::cmonAdmin::var($page,passwd) $passwd
	set ::cmonAdmin::var($page,email) $email
	set ::cmonAdmin::var($page,fullname) $fullname
	set ::cmonAdmin::var($page,phone) $phone
	if	{ ! [ string length $expires ] } {
		set ::cmonAdmin::var($page,expires) $::cmonAdmin::expired 
	} else {
		if	{ ! [ regexp {^\d+$} $expires ] } {
			set expires [ clock scan $expires ]
		}
		set ::cmonAdmin::var($page,expires) [ clock format $expires -format "%m/%d/%Y" ]
	}
    set isblockcmd [ regexp -nocase {blockUser} $::cmonAdmin::var($page,admincmd) ]
    if  { $blocked >= 0 } {
        $::cmonAdmin::bstart3($page) configure -text "Unblock user" -bg green -fg brown
    } else {
        $::cmonAdmin::bstart3($page) configure -text "Block user" -bg red -fg white
    }
}

#******************************************************** 
##
## Name: cmonAdmin::reset  
##
## Description:
## additional reset
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments: 

proc cmonAdmin::reset { page args } {
    catch { destroy $::cmonAdmin::varFrame($page) }
    foreach field $::cmonAdmin::fields {
        set ::cmonAdmin::var($page,$field) ""
    }
    $::cmonAdmin::bstart3($page) configure -text "Block user" -bg red

}

## ******************************************************** 
##
## Name: cmonAdmin::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonAdmin::state0 { page }  {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonAdmin::topfw($page) normal	
	$::cmonAdmin::bstart2($page) configure -state normal 
    $::cmonAdmin::bstart3($page) configure -state normal -fg white -bg red \
        -text "Block user"
}

#******************************************************** 
##
## Name: cmonAdmin::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonAdmin::state1 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonAdmin::topfw($page) disabled
	$::cmonAdmin::bstart2($page) configure -state disabled
}


