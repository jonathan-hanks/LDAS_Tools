## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles the startup/shutdown of LDAS APIs
## and control of tape controller at Hanford.
##
## repeat and frequency are here for conformance to
## standard communication with server and are not actually used.
## ******************************************************** 

package provide cmonCores 1.0

namespace eval cmonCores {
	set pages { cmonCores }
	set apilen 15
	set localtimelen 24
	set gpstimelen 10
	set corefilelen 60
	set filelen 80
	set APIs $::API_LIST(no-server) 
	set numAPIs [ llength $::API_LIST(no-server) ]
}

## ******************************************************** 
##
## Name: cmonCores::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCores::create { notebook } {

    set page cmonCores
    set ::cmonCores::prevsite($page) none
    $notebook insert end cmonCores -text Cores \
        -createcmd "cmonCores::createPages $notebook" \
        -raisecmd [ list cmonCommon::pageRaise cmonCores cmonCores "Core files ( select for debug/delete )" ]
}

## ******************************************************** 
##
## Name: cmonCores::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## must do notebook insert into main page cmonLDAS

proc cmonCores::createPages { notebook } {

	    set frame [ $notebook getframe cmonCores ]

		set topf  [ frame $frame.topf ]	
		foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	    set page cmonCores
	    specific $topf $page 
	    set ::cmonCores::topfw($page) $topf
        pack $topf -side top -fill x -pady 2
		
		set titf2 [TitleFrame $pane2.titf2 -text "Command Status" -font $::ITALICFONT]
		;## create a status to display errors and status
		set statusw [ ::createStatus  [ $titf2 getframe ] ]
		array set ::cmonCores::statusw [ list $page $statusw ]
		pack $titf2 -pady 1 -padx 1 -fill both -expand yes
	
		;## create a scrollable window to hold variable entries
		set titf3 [TitleFrame $pane3.titf3 -text "Core files" -font $::ITALICFONT ]
		set ::cmonCores::titf3($page) $titf3 
        cmonCommon::pageUpdate cmonCores cmonCores "Core files"
        
		set tframe [ $titf3 getframe ]
		set sw [ ScrolledWindow $tframe.sw -relief sunken -borderwidth 2 ]
		foreach { hscroll vscroll } [ scrolledWindow_yscroll $sw ] { break }	
		set lbhdr [ listbox $sw.lhdr -height 0  \
        -highlightthickness 0 -selectmode single -relief ridge \
        -font $::LISTFONT ]
		$lbhdr insert end [ format "%15.15s|%24.24s|%10.10s|%61.61s|%80.80s"  "API  " "Local time  " \
				"GPS Time  " "Core File    " "File Output    " ] 

		$sw setwidget $lbhdr
		set ::cmonCores::lbhdr($page) $lbhdr
		
		set sf [ScrollableFrame $sw.f -height 1000 -areaheight 0 ]
		
		$sw setwidget $sf
		;## make the two lists scroll together
			
		$hscroll configure -command [ list ::BindXview [ list $lbhdr $sf ] ]
		
		set subf [$sf getframe]
		set subfh [ frame $subf.fhdr -relief sunken -borderwidth 2 ]
		set ::cmonCores::varFrame($page) $subfh
		set ::cmonCores::varFrameParent($page) [ winfo parent $subfh ]
		pack $sw -fill both -expand yes		
		pack $titf3 -side top -pady 2 -padx 2 -fill both -expand yes
		pack $pw1 -side top -fill both -expand 1
		
        array set ::cmonCores::state [ list $page 0 ]
        set ::cmonCores::prevsite($page) $::cmonClient::var(site)

}

## ******************************************************** 
##
## Name: cmonCores::specific 
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCores::specific { parent page } {

    set labf1 [LabelFrame $parent.labf1 -text \
		"Archived core files from: " \
		-side top -anchor w -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]
	
	set name [ namespace tail [ namespace current ] ]	
	createAPICheckList $subf $name $page 
	
	;## options menu for selecting apis	
	;## create the apply and save buttons
    set labf2 [LabelFrame $parent.labf2 -text \
		"Select core files to debug or delete" \
		-side top -anchor w -relief sunken -borderwidth 4]
	set subf [ $labf2 getframe ]
	
    set name cmonCores
	set f [ frame $subf.faction ]
    set f2 [ frame $f.ftime -relief groove -borderwidth 2 ]
    set ftime [ cmonCommon::createTimeOptions $f2 $name $page ]
    foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $f2 $name $page ] { break }
    pack $ftime $fstart $fend -side top -padx 2 -pady 1 -fill x 
    
	pack $f2 -side top -anchor w -expand 1 -fill both
    
    set f1 [ frame $f.fdelete ]
    set but1 [ Button $f1.start -text "Get Core Files" -helptext "get core Files from archive" \
        -command  [ list cmonCores::sendRequest $page cntlmon::coreArchives ] ]
	set but2 [ Button $f1.delete -text "Delete Cores" -helptext "submit request to server to delete cores" \
        -command  [ list cmonCores::sendRequest $page cntlmon::deleteCores ] -state disabled ]
	
    set selectmenu [ eval tk_optionMenu $f1.select ::cmonCores::select($page) \
        { "select all" "deselect all" }  ]

	pack $but1 $f1.select $but2 -side left -padx 2
    pack $f1 -side top -fill x -expand 1
    
	set ::cmonCores::bstart($page) $but1 
	set ::cmonCores::bdelete($page) $but2 
    set ::cmonCores::selectw($page) $selectmenu
    
	$selectmenu entryconfigure 0 -command [ list cmonCores::selectAllCores $page red ] -state disabled
	$selectmenu entryconfigure 1 -command [ list cmonCores::selectAllCores $page black ] -state disabled
    
    pack $f -side top -expand 1 -fill both -anchor nw
	pack $labf1 $labf2 -side left -fill x -padx 5 -expand 1

	array set ::cmonCores::state [ list $page 0 ]
	
}

## ******************************************************** 
##
## Name: cmonCores::sendRequest
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCores::sendRequest { page cmd } {

	if  { [ string equal $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonCores::statusw($page) "Please select an LDAS site"
		return 
	}	
	set apilist [ ::setAPIs $page cmonCores ]
	if	{ ! [ string length $apilist ] } {
		appendStatus $::cmonCores::statusw($page) "Please select at least one API"
		return
	}
	
	if	{ [ llength $::cmonCores::var($page,apilist) ] >= $::cmonCores::numAPIs } {
		set filter .+
	} else {
		set filter [ join $::cmonCores::var($page,apilist) | ]
	}
	
	if	{ [ catch { 
		foreach { login passwd } [ validateLogin 1 ] { break } 	
	} err ] } {
		appendStatus $::cmonCores::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
	
	if	{ [ regexp {deleteCore} $cmd ] } {
		set corelist [ coreChgs $page ]
		if	{ [ llength $corelist ] } {
			set msg "Delete core files [ join $corelist \n ] ?"
			set ack [ MessageDlg .cmonCores$page -title Warning -type okcancel -aspect 300 \
				-message $msg -icon question -justify left -font $::MSGFONT ]	
		;## a cancel returns 1
			if	{ $ack } {
				return
			}
			set cmd "cntlmon::deleteCores \{$corelist\}"
		} else {
			appendStatus $::cmonCores::statusw($page) "Nothing to delete"	
            return	
		}
	} 
    
	if	{ [ catch {
        foreach { starttime endtime } [ cmonCommon::timeValidate $page cmonCores ] { break }
    	set client $::cmonClient::client
		set cmdId "new"
    	set repeat 0
    	set freq 0
		cmonCores::state1 $page 
        set cmd "cmonCores::showReply $page\n$repeat\n$freq\n\
        	$client:$cmdId:$login:$passwd\n$cmd $starttime-$endtime $filter" 
		# puts "cmd=$cmd"
		sendCmd $cmd              
	} err ] } {
        cmonCores::state0 $page 
		appendStatus $::cmonCores::statusw($page) $err
    }      		
}

## ******************************************************** 
##
## Name: cmonCores::showReply
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCores::showReply { page rc clientCmd html} {

	if	{ [ catch {
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
	# puts "rc=$rc,page=$page,clientCmd $clientCmd"
	switch $rc {
	0 {
    	if	{ [ regexp {invoked} $html ] } {
        	appendStatus $::cmonCores::statusw($page) $html 0 blue
            cmonCores::state2 $page 
        } else {
			dispCores $page $html 
			if	{ [ string length $html ] } {
				appendStatus $::cmonCores::statusw($page) updated 0 blue			
            	cmonCores::state2 $page      
        	} else {
				appendStatus $::cmonCores::statusw($page) "No cores found" 0 blue
				cmonCores::state0 $page 
			}
       	}
	  }
	3 {
		appendStatus $::cmonCores::statusw($page) $html
        cmonCores::state0 $page 
	  }
	}
	} err ] } {
		appendStatus $::cmonCores::statusw($page) $err		
        cmonCores::state0 $page 	
	}
}

## ******************************************************** 
##
## Name: cmonCores::dispCores
##
## Description:
## extract configurable resources from rsc file
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## also display vars that cannot be changed

proc cmonCores::dispCores { page html } {

	if	{ [ catch {
	set i 0

	;## create a header 
	set parent $::cmonCores::varFrameParent($page) 
	;## must destroy widget in parseRsc or globalVar array cannot be unset
	catch { destroy $::cmonCores::varFrame($page) }
	
	set subfh [ frame $parent.fhdr -relief sunken -borderwidth 2 ]
	set ::cmonCores::varFrame($page) $subfh
	set ::cmonCores::varFrameParent($page) [ winfo parent $subfh ]
	
	#set fg_global #127818 (green)
	set color_global blue
	set color_api #127818
	set ::cmonCores::corefiles($page) [ list ]
    
	foreach core $html {
		foreach { api gpstime corefile filedata } $core { break }
		regsub -all {\n} $filedata {} filedata 
		# regexp {.(\d+).core} $corefile -> gpstime
		if	{ ! [ regexp -nocase {elf[^,]+,\s*(\S+).*,} $filedata -> platform ] } {
			set platform unknown
		}
		regsub -all {,} $platform {} platform
		#puts "core $corefile, platform $platform"
		# puts "api $api, gpstime $gpstime, corefile $corefile, filedata $filedata"
		
		set subf1 [ frame $subfh.f$i -relief sunken ]
		set localtime [ gps2utc $gpstime 0 ]
		Label $subf1.api$i -width $::cmonCores::apilen \
			-text $api -justify left \
			-anchor w -font $::LISTFONT	-relief ridge -bg white
		Label $subf1.localtime$i -width  $::cmonCores::localtimelen -justify left \
			-text $localtime -fg brown -justify left \
			-anchor w -font $::LISTFONT	-relief ridge -bg gray
			
		Label $subf1.gpstime$i -width  $::cmonCores::gpstimelen -justify left \
			-text $gpstime -fg black -justify left \
			-anchor w -font $::LISTFONT	-relief ridge -bg white
		
		set mb [ menubutton $subf1.corefile$i -text $corefile \
			-relief raised -borderwidth 2 -activeforeground blue \
			-menu $subf1.corefile$i.menuDB \
			-font $::LISTFONT -bg PapayaWhip -width $::cmonCores::corefilelen ]
		
		set m1Debug [ menu $mb.menuDB -tearoff 0 ]
		set index 0
		if	{ ! [ string match unknown $platform ] } {	
			foreach debugger $::DEBUGGERS {
				$m1Debug add command -label "debug via $debugger" \
				-command "cmonCores::debug $page $corefile $platform $debugger"	
				incr index 1
			}
						
		}		
		$m1Debug add command -label "delete" \
				-command [ list cmonCores::deleteCore $m1Debug $mb $index $page ]	
    	
		Label $subf1.filedata$i -justify left \
			-text [ string trim $filedata ] -fg black -justify left -width $::cmonCores::filelen \
			-font $::LISTFONT -relief ridge -bg white 
			
		pack $subf1.api$i $subf1.localtime$i $subf1.gpstime$i \
			$mb $subf1.filedata$i \
			-side left  -fill both -anchor w
		pack $subf1 -side top -fill x -expand 1
		lappend ::cmonCores::corefiles($page) $subf1.corefile$i
		incr i 1
	}
	pack $subfh -side top -fill both -expand 1
	
	} err ] } {
		puts "[myName],$page,$err"
		appendStatus $::cmonCores::statusw($page) "[myName] $err"
	}
}
	
	
## ******************************************************** 
##
## Name: cmonCores::debug
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## need password to change 

proc cmonCores::debug { page corefile platform debugger } {

	;## if client, do it locally
	if	{ [ string match unknown $platform ] } {
		appendStatus $::cmonCores::statusw($page) "cannot debug on $platform platform"
		return
	}
	;## if API send to server
	if  { [ string equal $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonCores::statusw($page) "Please select an LDAS site"
		return 
	}
	if	{ [ catch { 
		foreach { login passwd } [ validateLogin 1 ] { break } 	
	} err ] } {
		appendStatus $::cmonCores::statusw($page) $err
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
	set cmdId "new"
    set repeat 0
    set freq 0
	set platform [ string tolower $platform ]
	if	{ [ catch {
		foreach { host port } $::siteport($::cmonClient::var(site)) { break }
        set client $::cmonClient::client
		set cmdId "new"
    	set repeat 0
    	set freq 0
		;## need to exec from ldas and expect no reply if it works
        set cmd "cmonCores::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::debugger $debugger $corefile"
        sendCmd $cmd 
        cmonCores::state1 $page 
		#catch { exec $::XTERM -geometry 40x10 -e ssh ldas@$host $::LDASDEBUG \
		#	$platform $debugger tclsh $corefile & } pid
		#if	{ ! [ regexp {^[\s\d\t]+$} $pid ] } {
		#	error $pid
		#}	       
	} err ] } {
		appendStatus $::cmonCores::statusw($page) $err
    }   
}

## ******************************************************** 
##
## Name: cmonCores::deleteCore
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## need password to change 

proc cmonCores::deleteCore { menuw buttonw index page } {

	set text [ $menuw entrycget $index -label ]
	if	{ [ string match delete $text ] } {
		$buttonw configure -foreground red
		$menuw entryconfigure $index -label undelete
	} else {
		$buttonw configure -foreground black
		$menuw entryconfigure $index -label delete	
	}
}

##******************************************************** 
##
## Name: cmonCore::coreChgs 
##
## Description:
## gather list of nodes to change 
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonCores::coreChgs { page } {
	
	set delnodes [ list ]
	set done 0
	set dellist [ list ]
	foreach lbl $::cmonCores::corefiles($page) {
		set color [ $lbl cget -fg ] 
		if	{ [ string match red $color ] } {
			set corefile [ $lbl cget -text ]
			lappend dellist $corefile
		}  
	}
	return $dellist 
}

##******************************************************** 
##
## Name: cmonCore::selectAllCores 
##
## Description:
## select cores for delete
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonCores::selectAllCores { page color } {
    if  { [ info exist ::cmonCores::corefiles($page) ] } {
	    foreach menub $::cmonCores::corefiles($page) {    
            if  { [ string equal red $color ] } {
                $menub.menuDB entryconfigure 2 -label undelete
            } else {
                $menub.menuDB entryconfigure 2 -label delete
            }
            $menub configure -fg $color
        }
	}
}

## ******************************************************** 
##
## Name: cmonCores::requestDone
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCores::requestDone { page rc clientCmd html} {

    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
	switch $rc {
	0 {
		if	{ [ string length $html ] } {
			appendStatus $::cmonCores::statusw($page) $html 0 blue
            cmonCores::state0 $page      
        }
	  }
	3 {
		appendStatus $::cmonCores::statusw($page) $html
        cmonCores::state0 $page 
	  }
	}	
}

##******************************************************** 
##
## Name: cmonCores::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonCores::state0 { page } {

	;## create a header 
	set parent $::cmonCores::varFrameParent($page) 
	;## must destroy widget in parseRsc or globalVar array cannot be unset
	catch { destroy $::cmonCores::varFrame($page) }
	
	updateWidgets $::cmonCores::topfw($page) normal
	$::cmonCores::bdelete($page) configure -state disabled
        
    set selectmenu [ set ::cmonCores::selectw($page) ]
    
	$selectmenu entryconfigure 0 -state disabled
	$selectmenu entryconfigure 1 -state disabled
    
	array set ::cmonCores::state [ list $page 0 ]

}

##******************************************************** 
##
## Name: cmonCores::state1  
##
## Description:
## sending request and pending reply
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonCores::state1 { page } {
	updateWidgets $::cmonCores::topfw($page) disabled
	array set ::cmonCores::state [ list $page 1 ]
}

##******************************************************** 
##
## Name: cmonCores::state2  
##
## Description:
## sending request and pending reply
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonCores::state2 { page } {
	updateWidgets $::cmonCores::topfw($page) normal 
	array set ::cmonCores::state [ list $page 1 ]
   
    $::cmonCores::bdelete($page) configure -state normal
        
    set selectmenu [ set ::cmonCores::selectw($page) ]
    
	$selectmenu entryconfigure 0 -state normal
	$selectmenu entryconfigure 1 -state normal
    
}

## ******************************************************** 
##
## Name: cmonCores::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCores::reset { page } {

    catch { destroy $::cmonCores::varFrame($page) }

}
