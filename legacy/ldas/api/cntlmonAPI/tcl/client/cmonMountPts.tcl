## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This package displays the directory tree 
## on the remote machine
## 
## cmonJobs version 1.0
##
## ******************************************************** 
;##barecode
package provide cmonMountPts 1.0

namespace eval cmonMountPts {
    set page cmonMountPts
    set mountpointTag mountpoint
}

#end

## ******************************************************** 
##
## Name: cmonMountPts::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMountPts::create { notebook } {

    set page $::cmonMountPts::page
    set pagetext "Directories"
    $notebook insert end cmonMountPts -text $pagetext \
        -createcmd "cmonMountPts::createPages $notebook $page" \
        -raisecmd "cmonCommon::pageRaise cmonMountPts $page {}"
}


## ******************************************************** 
##
## Name: cmonMountPts::createPages 
##
## Description:
## creates Job listing page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMountPts::createPages { notebook page } {

    set frame [ $notebook getframe cmonMountPts ] 
	set ::cmonMountPts::topframe $frame
	set subf  [ frame $frame.f ]
	set titf2 [TitleFrame $subf.titf2 -text "Command status" -font $::ITALICFONT] 
	set subf1  [ $titf2 getframe ]
	set topf  [ frame $subf.topf ]
    set ::cmonMountPts::topfw($page) $topf
	specific $topf $page
	;## create a status to display errors and status
	set statusw [ ::createStatus  $subf1 ]
    $statusw configure -height 10
	array set ::cmonMountPts::statusw [ list $page $statusw ]
	pack $topf -side left -padx 2 -padx 4 
	pack $titf2 -side right -padx 2 -pady 1 -fill both -expand 1
	pack $subf -side top -fill x  -padx 5
	  
    set title [TitleFrame $frame.lf -text "Double click on a mount point to get sub-directory updates from server" -font $::ITALICFONT ]
	set ::cmonMountPts::titf3($page) $title
	
    set sw    [ScrolledWindow [$title getframe].sw -relief sunken -borderwidth 2]
    set ::cmonMountPts::treeview_mountpts [ blt::treeview $sw.mountpts -tree $::cmonMountPts::tree_mountpts \
        -bg PapayaWhip -font $::LISTFONT -relief raised ]
        
    $sw setwidget $::cmonMountPts::treeview_mountpts

    pack $sw -side top  -expand yes -fill both
    pack $title -fill both -expand yes -padx 2 -pady 2
   
    set ::cmonMountPts::prevsite($page) $::cmonClient::var(site)
    $::cmonMountPts::treeview_mountpts bind all <Double-ButtonPress-1> \
        {+cmonMountPts::selectMountPoint %W %p %#}
    
}

## ******************************************************** 
##
## Name: cmonMountPts::refreshMountPoints
##
## Description:
## gets an updated list of mount points from server 
##
## Parameters:
##
## Usage:
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMountPts::refreshMountPoints { page } {

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonMountPts::statusw($page) "Please select an LDAS site"
		return 
	}
    
    if  { ! [ info exist ::MOUNT_PT ] || ! [ llength $::MOUNT_PT ] } {
        appendStatus $::cmonMountPts::statusw($page) "This version of cntlmonAPI server does not support this function."
		return 
	}
    
    set cmdId "new"   
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0

	if	{ [ catch {		
        set cmd "cmonMountPts::refreshMountPointsReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::refreshMountPoints"
        sendCmd $cmd 
        cmonMountPts::state1 $page        
	} err ] } {
		appendStatus $::cmonMountPts::statusw($page) $err
        cmonMountPts::state0 $page
	}
}

## ******************************************************** 
##
## Name: cmonMountPts::specific 
##
## Description:
## creates Refresh button to get dirs 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonMountPts::specific { parent page } {

	set titf1 [TitleFrame $parent.titf1 -text \
	"Mount Point Tree functions:" -font $::ITALICFONT -relief ridge ]
	set subf1  [$titf1 getframe]
    
    set but1 [ Button $subf1.refresh -text "Refresh Mount Points" -helptext "refresh mount points from diskcacheAPI" \
        -command  "cmonMountPts::refreshMountPoints $page" -width 30 -justify left ]
        
    set mountpt_ent   [LabelEntry $subf1.mountpt -label "Get subdirectories for: "  -labelanchor w \
    	-textvariable ::cmonMountPts::selectedMountPoint -editable 0 -width 30 \
        -helptext "select a mount point from the list of mount points on the left." ]
        
    set but3 [ checkbutton $subf1.flat -text "Flatten Directory Tree" \
        -variable ::cmonMountPts::flat -onvalue 1 -offvalue 0 \
        -command "cmonMountPts::flattenDirectories" ]
        
    set but4 [ Button $subf1.save -text "Save Tree to File" -helptext "write directories to file" \
        -command  "cmonMountPts::saveTree $page"  -justify left ]
    
	pack $mountpt_ent $but3 $but4 $but1 -side top -anchor w -anchor w -padx 1 -pady 5 
	pack $titf1 -fill both -expand 1
}

## ******************************************************** 
##
## Name: cmonMountPts::updateMountPoints
##
## Description:
## update actual mount points when conncted to server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc is invoked via trace

proc cmonMountPts::updateMountPoints { page args } {

    if  { [ catch {
        if  { [ info exist ::MOUNT_PT ] } {
            catch { $::cmonMountPts::tree_mountpts delete root } err
            foreach entry $::MOUNT_PT {
                $::cmonMountPts::tree_mountpts insert root -label $entry -tags { mountpoint } 
            }
        }
        if  { [ llength $::MOUNT_PT ] } {
            set ::cmonMountPts::selectedMountPoint [ lindex $::MOUNT_PT 0 ]
            set ::cmonMountPts::site_MountPoint $::cmonClient::var(site)
            catch { appendStatus $::cmonMountPts::statusw($page) "Updated Mount Points." 0 blue } 
        } 
    } err ] } {
        catch { appendStatus $::cmonMountPts::statusw($page) $err }
        if  { [ string length $::cmonMountPts::statusw($page) ] } {
            appendStatus $::cmonMountPts::statusw($page) $err
        }
    }  
    cmonMountPts::state0 $page
}

## ******************************************************** 
##
## Name: cmonMountPts::refreshMountPointsReply
##
## Description:
## display results from server 
##
## Parameters:
## parent widget 
## rc 	-	return code from cmd to server
## clientCmd -	client name and cmd Id
## html	-	text returned from cmd
##
## Usage:
##  called a button command 
## Comments:
## this proc name is generic for all packages.

proc cmonMountPts::refreshMountPointsReply { page rc clientCmd html } {  

	if	{ [ catch {
    	set clientCmd [ split $clientCmd : ]
    	set client [ lindex $clientCmd 0 ]
    	set cmdId [ lindex $clientCmd 1 ]
    	set afterid [ lindex $clientCmd 2 ]
		#puts "html=$html"
		set ::cmonMountPts::cmdId($page) $cmdId 
    	set ::cmonClient::client $client
		switch $rc {
		0 {    
            set cmd $html
            # puts "cmd='$cmd'"
            eval $cmd        
	  	}
		3 {
			appendStatus $::cmonMountPts::statusw($page) $html	
	  	}
		}
	} err ] } {
        appendStatus $::cmonMountPts::statusw($page) $err
	}
    cmonMountPts::state0 $page
	
}

## ******************************************************** 
##
## Name: cmonMountPts::getDirectories
##
## Description:
## gets of subdirectories for the selected mount point
##
## Parameters:
##
## Usage:
## 
## Comments:

proc cmonMountPts::getDirectories { page } {

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonMountPts::statusw($page) "Please select an LDAS site"
		return 
	}
    
    if  { ! [ info exist ::MOUNT_PT ] || ! [ llength $::MOUNT_PT ] } {
        appendStatus $::cmonMountPts::statusw($page) "This version of cntlmonAPI server does not support this function."
		return 
	}
    
    if  { $::cmonMountPts::state($page) != 0 } {
        # error "servicing request right now, resubmit later"
        return
    } 
    
    ;## check proxy for globus
    if	{ $::USE_GLOBUS } {
    	if  { [ catch {
        	foreach { login passwd } [ validateLogin 1 ] { break }
    	} err ] } {
			appendStatus $::cmonLDASTest::statusw($page) $err
			return        
		}
    	if  { [ string equal cancelled $login ] } {
			return
		}
    }
	
	if	{ [ catch {	
        set treesize [ $::cmonMountPts::tree_mountpts size 0 ]
        if  { $treesize > $::MAX_TREE_NODES } {
            error "Mount Point Tree size $treesize exceeds maximum $::MAX_TREE_NODES; \
                hit \"Rrefresh Mount Points\" to start over again"
        }
        set cmdId "new"   
        set name [ namespace tail [ namespace current ] ]
        set client $::cmonClient::client
        set freq 0
        set repeat 0
        if	{ $::USE_GLOBUS } {
        	set userpass $passwd 
            set userinfo "-name $login -password $passwd -email persistent_socket"
        } else {
        	set userpass [ base64::encode64 [ binaryEncrypt $::CHALLENGE $::passwdtext] ]
            set userinfo "-name $::logintext -password $userpass -email 12345:6789"
        }
        regsub -all -- {\s+} $userinfo { } userinfo
        
        set mountpoint $::cmonMountPts::selectedMountPoint
        set ldascmd "cacheGetDirs -mountpoint $mountpoint"
       
        set cmd "cmonMountPts::getDirectoriesReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::submitLDASjob $page \{$userinfo\} \{$ldascmd\}"
        # puts "cmd=$cmd"
        sendCmd $cmd 
        cmonMountPts::state1 $page        
	} err ] } {
		appendStatus $::cmonMountPts::statusw($page) $err
        cmonMountPts::state0 $page
	}
}

## ******************************************************** 
##
## Name: cmonMountPts::getDirectoriesReply
##
## Description:
## display results from server 
##
## Parameters:
## parent widget 
## rc 	-	return code from cmd to server
## clientCmd -	client name and cmd Id
## html	-	text returned from cmd
##
## Usage:
##  called a button command 
## Comments:

proc cmonMountPts::getDirectoriesReply { page rc clientCmd html } {  

	if	{ [ catch {
    	set clientCmd [ split $clientCmd : ]
    	set client [ lindex $clientCmd 0 ]
    	set cmdId [ lindex $clientCmd 1 ]
    	set afterid [ lindex $clientCmd 2 ]
		set ::cmonMountPts::cmdId($page) $cmdId 
    	set ::cmonClient::client $client
		switch $rc {
		0 {   
            if  { [ regexp {results\n([^\n]+).+=} $html -> result ] } {          
                foreach { dirlist msg } $result { break  }
                dispDirectories $dirlist 
                set msg [ join $msg \n ]
                appendStatus $::cmonMountPts::statusw($page) $msg 0 blue 0 
                cmonMountPts::state0 $page
            } else {
                error "Error: $html"
            }
	  	}
        2 { appendStatus $::cmonMountPts::statusw($page) $html 0 blue }
		3 {
			appendStatus $::cmonMountPts::statusw($page) $html	
            cmonMountPts::state0 $page
	  	}
		}
	} err ] } {
        appendStatus $::cmonMountPts::statusw($page) $err
        cmonMountPts::state0 $page	
	}
	
}


## ******************************************************** 
##
## Name: cmonMountPts::getdir 
##
## Description:
## display new directories on nodes
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:

proc cmonMountPts::dispDirectories { dirlist } {

    if  { [ catch {
        set tree $::cmonMountPts::tree_mountpts
        set mountpoint $::cmonMountPts::selectedMountPoint 
        if  { ! [ string length $mountpoint ]  } {
            error "No selected mount point $mountpoint"
        }
        set numdirs [ llength $dirlist ]
        if  { $numdirs > $::MAX_TREE_NODES } {
            error "Unable to display $numdirs directories for $mountpoint, max size allowed is $::MAX_TREE_NODES"
        }
        set rootnode [ $tree index $mountpoint ]
        set number [ $tree position $rootnode ] 

        catch { $tree delete $rootnode }
        set rootnode [ $tree insert root -at $number -label $mountpoint -tags { mountpoint } ]

        set subdirs [ list ]
        foreach entry $dirlist {
            if  { [ string equal $entry $mountpoint ] } {
                continue
            }
            set parentdir ""
            set thisdir $entry
            while { ! [ string equal $parentdir $mountpoint ] } {
                # puts "entry $entry, parentdir $parentdir"
                if  { [ string equal "/" $parentdir ] } {
                    error "$entry error: not under $mountpoint, directories $dirlist"
                }
                set parentdir [ file dirname $thisdir ]
                set subdirs [ linsert $subdirs 0 $parentdir ]
                set thisdir $parentdir
            }
            lappend subdirs $entry
        }
        set subdirs [ lsort -unique $subdirs ]
        
        foreach entry $subdirs {
            set parentdir [ file dirname $entry ]            
            if  { [ info exist nodeId($parentdir) ] } {
                set parentNode $nodeId($parentdir)
            } else {
                set parentNode $rootnode
            }
            set nodeId($entry) [ $tree insert $parentNode -label [ file tail $entry ] -tags { subdirectory } ]
            # puts "$entry, id $nodeId($entry), path [ $tree path $nodeId($entry) ], parent $parentNode [ $tree label $parentNode ]"
        }
        # blt::bltdebug 100
        appendStatus $::cmonMountPts::statusw($::cmonMountPts::page) "Found [ llength $subdirs ] directories for $mountpoint." 0 blue       
        $::cmonMountPts::treeview_mountpts open -recurse $rootnode
        
        if  { ! [ llength $subdirs ] } {
            error "No subdirectories found for $mountpoint."
        }
            
    } err ] } {
        appendStatus $::cmonMountPts::statusw($cmonMountPts::page) $err
    }
    # blt::bltdebug 0
}


## ******************************************************** 
##
## Name: cmonMountPts::selectMountPoint 
##
## Description:
## handle mouse selection
##
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonMountPts::selectMountPoint { treew args } {

    set nodeId [ $treew index current ]
    if  { [ $::cmonMountPts::tree_mountpts size root ] > 1 } {
        if  { $nodeId == 0 } {
            set nodeId [ $::cmonMountPts::tree_mountpts firstchild root ]
        }
        set selectednode [ $::cmonMountPts::tree_mountpts label $nodeId ] 
        set tag [ $::cmonMountPts::tree_mountpts tag names $nodeId ]
        if  { [ regexp $::cmonMountPts::mountpointTag $tag ]  } {
            set ::cmonMountPts::selectedMountPoint $selectednode
            cmonMountPts::getDirectories cmonMountPts
        }
   }
}
#******************************************************** 
##
## Name: cmonMountPts::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonMountPts::state0 { page } {
 
    if  { [ info exist ::cmonMountPts::topfw($page) ] } {
        updateWidgets $::cmonMountPts::topfw($page) normal
    }
	array set ::cmonMountPts::state [ list $page 0 ]
}


## ******************************************************** 
##
## Name: cmonMountPts::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonMountPts::state1 { page } {
    updateWidgets $::cmonMountPts::topfw($page) disabled    	
	array set ::cmonMountPts::state [ list $page 1 ]
	appendStatus $::cmonMountPts::statusw($::cmonMountPts::page) \
	"Sending request, please wait..." 0 blue
}

## ******************************************************** 
##
## Name: cmonMountPts::state2  
##
## Description:
## got Mount points from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonMountPts::state2 { page } {
    if  { [ info exist ::cmonMountPts::topfw($page) ] } {
        updateWidgets $::cmonMountPts::topfw($page) normal  
    }	
	array set ::cmonMountPts::state [ list $page 2 ]
}

## ******************************************************** 
##
## Name: cmonMountPts::reset  
##
## Description:
## update title to reflect the site
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMountPts::reset { page } {

    if  { [ info exist ::MOUNT_PT ]  && [ llength $::MOUNT_PT ] } {
        if  { ! $::cmonClient::state || $::cmonMountPts::site_MountPoint != $::cmonClient::var(site) } {
            

            cmonMountPts::noMountPoints
        }
    } elseif { $::cmonClient::var(site) } {
        cmonMountPts::noMountPoints
    }
}

#******************************************************** 
##
## Name: cmonMountPts::state4 
##
## Description:
## disable all buttons since cntlmonAPI does not support this
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonMountPts::state4 { page } {
    if  { [ info exist ::cmonMountPts::topfw($page) ] } {
        updateWidgets $::cmonMountPts::topfw($page) disabled
    }
	array set ::cmonMountPts::state [ list $page 4 ]
}

## ******************************************************** 
##
## Name: cmonMountPts::saveTree
##
## Description:
## dump directory hierarchy to printer
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMountPts::saveTree { page } {

    set tree $::cmonMountPts::tree_mountpts
    if  { [ $tree size root ] < 3 } {
        appendStatus $::cmonMountPts::statusw($::cmonMountPts::page) "No tree data to save"
        return
    }

    if  { [ catch {
        set text ""   
        set data [ $tree dump root ] 
        if  { [ $tree size root ] > 2 } {
            set pat "$tree\\s(\[^\}\]+)"
            foreach line [ split $data \n ] {
                if  { [ regexp $pat $line -> dirs ] } {
                    regsub -all {\s} $dirs {/} dir
                    regsub -all {\{} $dir {} dir 
                    if  { [ string length $dir ] } {
                        append text $dir\n
                    }
                }
            }
          
            if  { [ string length $text ] } {
                set typelist {
			    {"TEXT" {".txt"}}
		        }
     	        set fname [ tk_getSaveFile -filetypes $typelist -title "save directory tree" \
			    -defaultextension .txt -initialdir [ pwd ] ]
                set fd [ open $fname w ]
                puts $fd [ string trim $text \n ]
                close $fd
                appendStatus $::cmonMountPts::statusw($::cmonMountPts::page) \
                "Directory tree is saved in $fname" 1 blue 1
            } else {
                error "No data to save"
            }
        } else {
            error "No data to save"
        }
    } err ] } {
        appendStatus $::cmonMountPts::statusw($::cmonMountPts::page) "[myName]: $err"
    }
}

## ******************************************************** 
##
## Name: cmonMountPts::flattenDirectories
##
## Description:
## dump directory hierarchy to printer
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMountPts::flattenDirectories {} {

    if  { $::cmonMountPts::flat } {
        $::cmonMountPts::treeview_mountpts configure -flat 1
    } else {
        $::cmonMountPts::treeview_mountpts configure -flat 0
    }

}

## ******************************************************** 
##
## Name: cmonMountPts::noMountPoints
##
## Description:
## display message for sites without mount points (old server)
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonMountPts::noMountPoints {} {
    
    set ::MOUNT_PT [ list ]
    ;## trigger trace clean up   
    set page $::cmonMountPts::page 
    appendStatus $::cmonMountPts::statusw($page) \
        "If you do not see mount points when connected to a cntlmonAPI server, \
        this cntlmonAPI version does not support this function." 
}

set ::cmonMountPts::tree_mountpts [ blt::tree create tree_mountpts ]

trace variable ::MOUNT_PT w "::cmonMountPts::updateMountPoints $::cmonMountPts::page"      
