## ******************************************************** 
##
## Name: plot.tcl
##
## Description:
## Provides plotting of data via gnuplot 
## can output result as gif for html 
## or redirected into tk canvas widget (default)
##
## Usage:
##        create a canvas widget (say, .c)
##  and then do:
##        plot::graph .c $data
##  
##
## Comments:
##  uses gnuplot
##
## ********************************************************

;#barecode
package provide plot 1.0

namespace eval plot {
   }


## ******************************************************** 
##
## Name: plot::plotData
##
## Description:
## Passes user cmd to gnuplot via pipe
## Parses results of gnuplot 
##
## Parameters:
## canvas - canvas widget
## formula - cmd to gnuplot 
##
## Usage:
##
## Comments:
## originally created by Donald Fellows

proc plot::plotData {canvas formula} {

    if  { [ catch {
        array set plot_widths {
                    -2 0  -1 0
                     0 1   1 1
                     2 2   3 2
        }
        set imfont "-adobe-helvetica-medium-r-*-*-*-[expr {[$canvas cget -width]/8}]-*-*-*-*-iso8859-*"
        set fp [open "|gnuplot" r+]
        puts $fp "set term xlib\n${formula}\nexit\n"; flush $fp
        set anchor c
        set x 0
        set y 0
        set lwidth 0
        while {[gets $fp line] >=0} {
            ;## get all ascii chars from trailing string if any
            set n [scan $line "%1s%4d%4d%\[ -a-zA-Z0-9!#*+\(\)/\"\[\]\^\_\`\\\\]" t x y s]                   
            set y [expr (4096-$y)]
            switch $t {
                        G { $canvas delete all }
                        M {
                            set oldx $x
                            set oldy $y
                        }
                        J { set anchor [ lindex { w c e } $x ] }
                        V {
                            $canvas create line $oldx $oldy $x $y -width $lwidth
                            set oldx $x
                            set oldy $y
                        }
                        L { catch {set lwidth "$plot_widths($x)"} }
                        T {
                            $canvas create text $x $y -text $s \
                                    -anchor $anchor -font $imfont
                        }
                        E {}
                        R {}
                        default { puts stderr "plot: unknown \"$line\"" }
             }
       }
       close $fp
       $canvas scale all 0 0 \
            [expr {[$canvas cget -width]/4096.0}] \
            [expr {[$canvas cget -height]/4096.0}]
    } err ] } {
        return -code error $err
    }
}


## ******************************************************** 
##
## Name: plot::zoom
##
## Description:
## set the indent spacing (in cm) for lists.
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc plot::zoom { canv wfactor hfactor } {
     $canv scale all 0 0 $wfactor $hfactor
     $canv configure -scrollregion [ $canv bbox all ]
	 if	{ ! [ info exist ::${canv}(zoomfactorX) ] } {
	 	set ::${canv}(zoomfactorX) 1.0
	 }
	 if	{ ! [ info exist ::${canv}(zoomfactorY) ] } {
	 	set ::${canv}(zoomfactorY) 1.0 
	 }
	 set ::${canv}(zoomfactorX) [expr $wfactor*[ set ::${canv}(zoomfactorX)]]
	 set ::${canv}(zoomfactorY) [expr $hfactor*[ set ::${canv}(zoomfactorY)]]
	 catch {plot::zoomClear $canv}
}
## ********************************************************

## ******************************************************** 
##
## Name: plot::config
##
## Description:
## canvas resize notification
##
## Parameters:
## canv 	- canvas widget
## width	- current width
## height	- current height
## prev_w	- previous width
## prev_h	- previous height
##
## Usage:	
##
## Comments:

proc plot::config {canv width height prev_w prev_h } {	
	set wratio [ expr $width * 1.0 /$prev_w  ]
	set hratio [ expr $height * 1.0 /$prev_h  ]
	plot::zoom $canv $wratio $hratio
	bind $canv <Configure> "plot::config %W %w %h $width $height"
} 
         
## ******************************************************** 
##
## Name: plot::Canvas
##
## Description:
## set the indent spacing (in cm) for lists.
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## creates a 
## canvas with resize handling
## Usage: 
##
## created by Phil Ehrens
## adapted by Mary Lei
## using plot::canvas results in bad window path
## must have a name clash somewhere

proc plot::Canvas { c name page log args } {
	set fc [ frame $c.fcanvas -relief sunken -bd 2 ]
	eval {canvas $fc.canvas \
		-xscrollcommand [ list $fc.xscroll set ] \
		-yscrollcommand [ list $fc.yscroll set ] \
		-highlightthickness 0 \
		-borderwidth 0 } $args
	scrollbar $fc.xscroll -orient horizontal \
		-command [ list $fc.canvas xview ]
	scrollbar $fc.yscroll -orient vertical \
		-command [ list $fc.canvas yview ]
    set top [ winfo parent $c ]
	
	set f1 [ frame $fc.fbuttons ]
	button $f1.zm_in  -text "zoom in" \
    	-command "plot::zoom $fc.canvas 1.25 1.25" 
    button $f1.zm_out -text "zoom out" \
        -command "plot::zoom $fc.canvas 0.80 0.80"		
	button $f1.close -text Close \
		-command "cmonCommon::cleanupGraph $name $page $fc.canvas"
	button $f1.save -text Save \
		-command "plot::save $fc.canvas"
	button $f1.print -text Print \
		-command "plot::print $fc.canvas"
		
    pack $f1.close $f1.zm_in $f1.zm_out $f1.save $f1.print -side right -padx 2
	if	{ $log } {
		set flog [ frame $f1.flog -relief ridge -bd 2 ]
		set but1 [ checkbutton $f1.logx -text "Log X" \
        -variable ::${name}::var($fc.canvas,logx) -onvalue log -offvalue round \
		-command "${name}::replot $page $fc.canvas" ]
		set but2 [ checkbutton $f1.logy -text "Log Y" \
        -variable ::${name}::var($fc.canvas,logy) -onvalue log -offvalue round \
		-command "${name}::replot $page $fc.canvas" ]
		pack $but1 $but2 -side left -padx 1
		pack $flog -side right 
	}	
	set ::${fc}.canvas(print) $f1.print
	pack $fc.yscroll -side right -fill y
	pack $fc.xscroll -side bottom -fill x	
	pack $fc.canvas -fill both -expand 1
	pack $f1 -fill x -side bottom 
	pack $fc -fill both -expand 1
	set width [ $fc.canvas cget -width ]
	set height [ $fc.canvas cget -height ]
	bind $fc.canvas <Configure> "plot::config %W %w %h $width $height"
	plot::zoomInit $fc.canvas
    return $fc.canvas
}
## ********************************************************

## ******************************************************** 
## the following zoom procs work with emu_graph on canvas
## ******************************************************** 

## ******************************************************** 
##
## Name: plot::zoomBox
##
## Description:
## allow user to drag out a rectangular box to zoom further
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc plot::zoomBox { canvas graph } {

	bind $canvas <Button-1> [ list plot::zoomBegin %W %x %y $graph ]
	bind $canvas <B1-Motion> [ list plot::zoomDrag  %W %x %y $graph ]
	bind $canvas <ButtonRelease-1> [ list plot::zoomDrag %W %x %y $graph 1 ]
	bind $canvas <Button-3> [ list plot::zoomClear %W ] 
} 

## ******************************************************** 
##
## Name: plot::zoomBegin
##
## Description:
## record start of rectangle
##
## Parameters:
## canvas 
## x, y are view coordinates 
## graph is emu_graph handle
## Usage:
##
## Comments:

proc plot::zoomBegin { canvas x y graph } {

	global emu_graph
	catch { $canvas delete zoom }
	catch {unset ::${canvas}(zoomLast)}
	set ::${canvas}(zoomAnchor) [ list $x $y ]
}

## ******************************************************** 
##
## Name: plot::zoomDrag
##
## Description:
## draw when dragging; put up label when 
##
## Parameters:
## canvas
## x,y are view coordinates
## graph is emu_graph
##
## Usage:
##
## Comments:

proc plot::zoomDrag { canvas x y graph { done 0 } } {

    global emu_graph
	set lblframe [ winfo parent $canvas ]
	set labelname $lblframe.zoom
	catch { destroy $labelname }
	catch {$canvas delete [ set ::${canvas}(zoomLast) ]}

	set view_x1 [ lindex [ set ::${canvas}(zoomAnchor) ] 0 ]
	set view_y1 [ lindex [ set ::${canvas}(zoomAnchor) ] 1 ]
	
	if	{ $x == $view_x1 } {
		return
	}
	
	set cx1 [ $canvas canvasx $view_x1 ]
	set cy1 [ $canvas canvasy $view_y1 ]
	
	set cx2 [ $canvas canvasx $x ]
	set cy2 [ $canvas canvasy $y ]
	
	set unzoom_cx1 [ expr $cx1/[ set ::${canvas}(zoomfactorX) ] ]
	set unzoom_cy1 [ expr $cy1/[ set ::${canvas}(zoomfactorY) ] ]
	set unzoom_cx2 [ expr $cx2/[ set ::${canvas}(zoomfactorX) ] ]
	set unzoom_cy2 [ expr $cy2/[ set ::${canvas}(zoomfactorY) ] ]
	
	set x1 [ $graph canvas2x $unzoom_cx1 ]
	set x2 [ $graph canvas2x $unzoom_cx2 ]
	
	set y1 [ $graph canvas2y $unzoom_cy1 ]
	set y2 [ $graph canvas2y $unzoom_cy2 ]
	
	if  { ! [emu_graph:point_in_bounds $graph [ list $unzoom_cx1 $unzoom_cy1 ] ] ||
		  ! [emu_graph:point_in_bounds $graph [ list $unzoom_cx2 $unzoom_cy2 ] ] } {
		return 
	}
	set ::${canvas}(zoomLast) [eval {$canvas create rect} [ list $cx1 $cy1 ] \
		[ list $cx2 $cy2 ] -tag zoom -outline white ]
		
	if	{ $done } {
		$canvas focus current
		if	{ $x1 < $x2 } {
			set start $x1 
			set end $x2
		} else {
			set start $x2
			set end $x1
		}
        set gpsstart [ expr round($start) ]
        set gpsend   [ expr round($end) ]
		set start [ gps2utc $gpsstart 0 ]
		set end   [ gps2utc $gpsend 0 ]
		label $labelname -text "$start-\n$end" -relief ridge -bg PapayaWhip -fg black \
		-font $::SMALLFONT  
		place $labelname -in $lblframe -x $x -y $y -anchor n
		cmonCommon::setTime $gpsstart $gpsend
	}
}

## ******************************************************** 
##
## Name: plot::zoomClear
##
## Description:
## reset zoom
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc plot::zoomClear { canvas } {
	catch { $canvas delete zoom }
	set lblframe [ winfo parent $canvas ]
	set labelname $lblframe.zoom
	catch { destroy $labelname }
}

## ******************************************************** 
##
## Name: plot::zoomInit
##
## Description:
## reset zoom
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc plot::zoomInit { canvas } {
	 set ::${canvas}(zoomfactorX) 1.0
	 set ::${canvas}(zoomfactorY) 1.0 
}

## ******************************************************** 
##
## Name: plot::save
##
## Description:
## save image to a file 
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc plot::save { canvasw } {
	
	if	{ [ catch { 
		set typelist {
			{"GIF IMage" {".gif"}}
		}
     	set fname [ tk_getSaveFile -filetypes $typelist -title "save graph" \
			-defaultextension .gif -initialdir [ pwd ] ]
		set parent [ winfo toplevel $canvasw ] 
		if	{ [ string length $fname ] } {
	 		set ext [ file extension $fname ]
			if	{ [ string length $ext ] } {
	 			regsub $ext$ $fname {.gif} fname
			} else {
				set fname ${fname}.gif
			}
			set dlg [ plot::saveHelp $parent ]
			tkwait window $dlg
			raise $parent .
			set old [ focus ]
			focus $parent 
			update 
	 		exec $::plot::import $fname
			focus $old 
			set ack [ tk_messageBox -type ok -default ok \
				-message "Your graph has been saved in $fname." \
				-icon info ]	
		}	
	} err ] } {
		set ack [ tk_messageBox -type ok -default ok \
				-message "Error saving graph: $err." \
				-icon error ]
	}
}
## ********************************************************

## ******************************************************** 
##
## Name: plot::print
##
## Description:
## set the indent spacing (in cm) for lists.
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc plot::print { canvas } {

	 regexp {^.([^.]+).} $canvas -> fname
	 set out $::TMPDIR/${fname}.ps

	 foreach { dd ss width height } [ $canvas cget -scrollregion ] { break }
	 if	{ ! [ info exist dd ] } {
	 	set ack [ tk_messageBox -type ok -default ok \
				-message "No data to print." \
				-icon error ]
		return
	 }
	 set width [ expr $width - $dd ]
	 set height [ expr $height - $ss ]
	 set data [ $canvas postscript -fontmap ::fontMap -colormap colorMap  \
	 	-x $dd -y $ss -width $width -height $height -rotate 1 ]
	 
	 printEPS $out $data $canvas
	 if	{ ! [ info exist ::PRINTCMD ] } {
	 	set printcmd lpr
	 } else {
	 	set printcmd $::PRINTCMD
	 }
	 set cmd "exec $printcmd $out &"
	 if	{ [ file exist $out ] } {
	 	catch { eval $cmd } err
		set err ""
		set msg [ message ${canvas}Print -justify left -text \
			"Printing [ file size $out ] bytes $err" -aspect 800 ]
		pack $msg
	 	after 5000 "catch { destroy $msg; file delete $out }"
	 }	 	
}
## ********************************************************


## ******************************************************** 
##
## Name: plot::saveHelp
##
## Description:
## set the indent spacing (in cm) for lists.
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc plot::saveHelp { parent } {

		set win ${parent}savehelp[ clock seconds ]
		set image [ Bitmap::get info ]
		if	{ ! [ winfo exist $win ] } {
			set dlg [ Dialog $win -parent . -modal none \
          		-separator 1 -title  info \
        		-side bottom -anchor  s -default 0 -image $image ]
        	$dlg add -name ok -anchor s -command [ list destroy $dlg ]
			set dlgframe [ $dlg getframe ]
			set msg "To print the graph:\n\when cursor turns to crosshair,\n\
				click on the graph to save to file and wait for the beep"
			message $dlgframe.msg -aspect 300 -text $msg \
					-justify left -font $::MSGFONT
			pack $dlgframe.msg -side top 
			$dlg draw 
			after 5000 [ list destroy $dlg ]
		}
		return $win  
}

## ********************************************************

## ******************************************************** 
##
## Name: plot::setUp
##
## Description:
## set up fonts for canvas printing
##
## Parameters:
## c - frame of canvas
## Usage:
##

proc plot::setUp {} {

 	# Tweak the output color
    set colorMap(blue) {0.1 0.1 0.9 setrgbcolor}
    set colorMap(green) {0.0 0.9 0.1 setrgbcolor}

	foreach family {times courier helvetica} {
		set weight bold
		switch -- $family {
			times { set fill blue; set psfont Times}
			courier { set fill green; set psfont Courier }
			helvetica { set fill red; set psfont Helvetica }
		}
		foreach size {10 14 24} {
			# Guard against missing fonts
			set ::fontMap(-*-$family-$weight-*-*-*-$size-*)\
					[list $psfont $size]
		}
	}
	set ::fontMap(fixed) [list Courier 12]
}

## ******************************************************** 
##
## Name: plot::includeEPS
##
## Description:
## includes encapsulated postscript of canvas into output postscript file
##
## Parameters:
## file is eps file to include 
## out is an open writable filehandle 
## x is the xposition on the page for the lower left corner of the eps file to go 
## y is the yposition on the page for the lower left corner of the eps file to go 
## xratio is the x scaling factor 
## yratio is the y scaling factor 
## xtrans and ytrans are for the translation sometimes needed after scaling
##
## Usage:
##

proc plot::includeEPS {canvasdata out x y xratio yratio xtrans ytrans} { 

   puts $out {save 
   /showpage {} def 
   /erasepage {} def 
   /copypage {} def 
   /letter {} def 
   /legal {} def
   /a4 {} def    
   0 setgray 
   1 setlinewidth 
   0 setlinejoin 
   10 setmiterlimit 
   [] 0 setdash 
   /languagelevel where
   { pop languagelevel 2 ge { 
   		false setoverprint 
		false setstrokeadjust 
   } if 
   } if 
   newpath 
   } 
   puts $out "$x $y translate" 
   puts $out "$xratio $yratio scale" 
   if {$xtrans != 0 || $ytrans != 0} { 
   		puts $out "[expr -1 * $xtrans] [expr -1 * $ytrans] translate" 
   } 
   puts $out $canvasdata
   puts $out "restore" 
} 
   
## ******************************************************** 
##
## Name: plot::printEPS
##
## Description:
## print canvas as encapsulated postscript
##
## Parameters:
## c - frame of canvas
## Usage:
## Comment
## do not unset the canvas var

proc plot::printEPS { filename canvasdata canvas } {
	set f [open $filename w] 
	puts $f "%!PS-Adobe-3.0" 
	puts $f "%%Page: 1 1" 
	puts $f "%%EndComments" 
	puts $f "gsave"
	upvar #0 $canvas canvasp

	puts $f "/Times-Roman findfont % Get the basic font \n\
	14 scalefont            % Scale the font to 20 points\n\
	setfont                 % Make it the current font\n\
	newpath                 % Start a new path\n\
	gsave					\n\
	30 100 moveto          % Lower left corner of text at (72, 72)\n\
	0.75 0.75 scale				\n\
	90 rotate				\n\
	($canvasp(version)) show			\n\
	grestore				\n\
	gsave					\n\
	60 100 moveto          % Lower left corner of text at (72, 72)\n\
	90 rotate				\n\
	($canvasp(ptitle)) show		\n\
	grestore				\n\
	gsave					\n\
	80 100 moveto 			\n\
	0.75 0.75 scale				\n\
	90 rotate				\n\
	($canvasp(line1)) show			\n\	
	grestore				\n\
	gsave					\n\
	100 100 moveto 			\n\
	0.75 0.75 scale				\n\
	90 rotate				\n\
	($canvasp(line2)) show			\n\	
	grestore				\n\
	gsave"

	includeEPS $canvasdata $f 0 0 1.2 1.2 20 70
	puts $f "grestore" 
	puts $f "showpage"

	;## put extra lines to page 2
	set i 0	
	if	{ [ info exist canvasp(extralines) ] } {
		puts $f "%%Page: 2	2\n\	
		%%EndComments	\n\
		newpath			\n\
		gsave"
				  			
		foreach line [ split $canvasp(extralines) \n ] {			
			puts $f "[ expr 80 + $i ]  100 moveto 	\n\
				0.75 0.75 scale			 	\n\
				90 rotate					\n\
				($line) show				\n\
				grestore					\n\
				gsave"
			incr i 20									
		}
	}
			
	puts $f "grestore" 
	puts $f "showpage"

    ;## 5/25/04 cause dictstackunderflow error with new millikan printserver
	;## puts $f "%%Trailer\nend\n%%EOF"
	close $f
}

proc demoPlot {} {
    set canvasw [ plot::scrolledCanvas .f -width 800 -height 500 \
                           -background PapayaWhip \
                           -scrollregion { -500 -500 1000 1000 } ]

    plot::plotData $canvasw {
        #load 'test.plot'
        set xlabel "gpstime"
        set ylabel "# queries or #rows inserted"
        set format x "%0.0f"
        set format y "%0.0f"
        set title "LDAS Metadata Usage"
        set pointsize 2
        set grid 15  
        set function style linespoints
        set data style linespoints
        set autoscale xy
        set key below
        plot '-' title "queries" with linespoints 1, '-' title "insertions" with linespoints 3
        660175015 3  
        660175016 5
        660175018 8
        e
        660175020 30"
        660175030 50" 
        e
    }
}

proc demoPlot2 {} {
    set canvasw [ plot::scrolledCanvas .f -width 800 -height 500 \
                           -background PapayaWhip \
                           -scrollregion { -500 -500 1000 1000 } ]
    update
    puts "Working ..."
   
    plot::plotData $canvasw {   
        ;## the follow cmd to gnuplot draws landscape surface
        set view 52,30
        set isosam 41,41
        set hidden3d
        splot [-2.5:2.5] [-2.5:2.5] x*y*exp(-(x**2+y**2))
    }
}

set ::plot::import [ auto_execok import ]
plot::setUp

# wish 
#set auto_path "/ldas/lib $auto_path"
#set API cntlmon
#source plot.tcl
#demoPlot
# destroy .f.canvas
# demoPlot2

