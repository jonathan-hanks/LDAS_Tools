## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles LDAS log filtering 
##
## cmonLogs Version 1.0
##
## ******************************************************** 

package provide cmonLogs 1.0

namespace eval cmonLogs {
    set notebook ""
    set pages [ list ]
    set APIs  [ concat $::API_LIST_w_cntlmon wrapper ]
	set apilist $APIs
    set allAPIs [ llength $apilist ]
	set wLogs .tmpLogs 
    set notebook0 ""
}

## ******************************************************** 
##
## Name: cmonLogs::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLogs::create { notebook } {

    if  { ! [ info exist ::NUM_LOG_PAGES ] } {
        set ::NUM_LOG_PAGES 3
    }
    if  { $::NUM_LOG_PAGES > 5  } {
        puts "max number of log filters is 5 pages"
        set ::NUM_LOG_PAGES 5
    }
    set ::cmonLogs::pages [ list ]
    for { set i 1 } { $i <= $::NUM_LOG_PAGES } { incr i 1 } {
        set page Log-Filter-$i 
        lappend ::cmonLogs::pages $page
    }
    set page [ lindex $::cmonLogs::pages 0 ] 
    regsub -all {\-} $page " " pagetext
    $notebook insert end cmonLogs -text "Logs" \
        -createcmd "cmonLogs::createPages $notebook cmonLogs" \
        -raisecmd "cmonCommon::pageRaise cmonLogs $page \{$pagetext\}; \
            cmonCommon::setLastTime cmonLogs $page"
    set ::cmonLogs::notebook0 $notebook 
}

## ******************************************************** 
##
## Name: cmonLogs::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLogs::createPages { notebook0 mainpage } {

    set frame [ $notebook0 getframe $mainpage ]
    
	set ::cmonLogs::notebook [ NoteBook $frame.nb -homogeneous 1 \
		-foreground white -background blue -bd 2 ]
	set notebook $::cmonLogs::notebook

	;## needs to create logtimes list first 
	foreach page $::cmonLogs::pages {
	
		if	{ [ regexp -nocase {filter} $page ] } {
			regsub -all {\-} $page " " pagetext
	    	set frame [ $notebook insert end $page -text $pagetext ]
        	set topf  [frame $frame.topf]
			set pane2 [ frame $frame.pane2 ]
			specific $topf $page 
	    	set ::cmonLogs::topfw($page) $topf
 			pack $topf -fill both -expand 1 -pady 2
			
	    	set titf2 [TitleFrame $pane2.titf2 -text "Command Status" -font $::ITALICFONT ]
	
	        ;## create a status to display errors and status
			set statusw [ ::createStatus  [ $titf2 getframe ] ]
			array set ::cmonLogs::statusw [ list $page $statusw ]
			pack $titf2 -pady 2 -padx 2 -fill both -expand yes
			pack $pane2 -fill both -expand yes
            array set ::cmonLogs::state [ list $page 0 ]
            set ::cmonLogs::prevsite($page) $::cmonClient::var(site)
		} 
	}
    $notebook compute_size
    pack $notebook -fill both -expand yes -padx 4 -pady 4
	$notebook raise [ $notebook page 0 ]
}

## ******************************************************** 
##
## Name: cmonLogs::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLogs::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]
    set titf1 [TitleFrame $parent.titf1 -text \
	"Specify jobid or time periods for log filter" -font $::ITALICFONT ]
    set subf  [$titf1 getframe]
	set ::cmonLogs::var($page,focus) $subf
	
	set labfent [LabelFrame $subf.fent -text "Filter Logs by Jobid OR by Time Period" -side top -anchor w \
                   -relief sunken -borderwidth 4]
    set subf1 [ $labfent getframe ]
	
	frame $subf1.fjobid -relief groove -borderwidth 2
    radiobutton $subf1.fjobid.seljob -text "Get logs for " \
		-variable ::cmonLogs::var($page,type) \
        -value jobid -anchor w -command "cmonLogs::setdefaultJobFilter $page"
    
	set jobid_ent   [LabelEntry $subf1.fjobid.ejobid -label "Jobid " -labelwidth 6 -labelanchor w \
                   -textvariable ::cmonLogs::var($page,jobid) -editable 1 -width 15 \
                   -helptext "Formats: numeric jobid e.g. 12345, or with optional runcode, e.g. LDAS-DEV12345"]
	pack $subf1.fjobid.seljob $subf1.fjobid.ejobid -side left -padx 2 -pady 1 -anchor w
	$jobid_ent bind <Button-1> "cmonLogs::setdefaultJobFilter $page"
	
	frame $subf1.ftime -relief groove -borderwidth 2
	set f [ frame $subf1.ftime.fopts ]
	radiobutton $f.seltime -text "For times: " -justify left -variable ::cmonLogs::var($page,type) \
		-value time -anchor w -command "cmonLogs::setdefaultTimesFilter $page"
	set cmd "tk_optionMenu $f.timemenu ::cmonCommon::timeopt $::cmonCommon::timeoptions"
    set timemenu [ eval $cmd ]
    
	$timemenu entryconfigure 0 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 1 0; cmonLogs::setdefaultTimesFilter $page"
	$timemenu entryconfigure 1 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 60 1; cmonLogs::setdefaultTimesFilter $page"
	$timemenu entryconfigure 2 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 360 2; cmonLogs::setdefaultTimesFilter $page"
	$timemenu entryconfigure 3 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 720 3; cmonLogs::setdefaultTimesFilter $page"	
	$timemenu entryconfigure 4 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 1440 4; cmonLogs::setdefaultTimesFilter $page"
	$timemenu entryconfigure 5 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 2880 5; cmonLogs::setdefaultTimesFilter $page"
	$timemenu entryconfigure 6 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 4320 6; cmonLogs::setdefaultTimesFilter $page"		
	$timemenu entryconfigure 7 -command "set ::cmonLogs::var($page,type) time; \
		cmonCommon::setLastTime cmonLogs $page 10080 7; cmonLogs::setdefaultTimesFilter $page"	
	$timemenu entryconfigure 8 -command "set ::cmonLogs::var($page,type) time; \
	    cmonCommon::setRunTime E10 8; cmonLogs::setdefaultTimesFilter $page"  
    $timemenu entryconfigure 9 -command "set ::cmonLogs::var($page,type) time; \
	    cmonCommon::setRunTime S1 9; cmonLogs::setdefaultTimesFilter $page"   
    $timemenu entryconfigure 10 -command "set ::cmonLogs::var($page,type) time; \
	    cmonCommon::setRunTime S2 10; cmonLogs::setdefaultTimesFilter $page"  
    $timemenu entryconfigure 11 -command "set ::cmonLogs::var($page,type) time; \
	    cmonCommon::setRunTime S3 11; cmonLogs::setdefaultTimesFilter $page" 
    $timemenu entryconfigure 12 -command "set ::cmonLogs::var($page,type) time; \
	    cmonCommon::setRunTime E12 12; cmonLogs::setdefaultTimesFilter $page"  
    $timemenu entryconfigure 13 -command "set ::cmonLogs::var($page,type) time; \
	    cmonCommon::setRunTime S4 13; cmonLogs::setdefaultTimesFilter $page"      
	$timemenu entryconfigure 14 -command "set ::cmonLogs::var($page,type) time; \
	cmonCommon::clearTime $name $page; cmonLogs::setdefaultTimesFilter $page" 

	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $subf1.ftime $name $page ] { break }
	
	pack $f.seltime $f.timemenu -padx 2 -pady 1 -side left -anchor w
	pack $f $fstart $fend -side top \
        -padx 2 -pady 1 -fill x -expand 1
           
	pack $subf1.fjobid $subf1.ftime -side top -anchor w -expand 1 -fill both
	
	frame $subf1.fstart 
    set  but1 [ Button $subf1.fstart.start -text "SUBMIT" -width 5 -helptext "submit one time request to server" \
        -command  "::cmonLogs::sendRequest $page" ]

	set ::cmonLogs::bstart($page) $but1  
	
	set but2 [ Button $subf1.fstart.tips -image $::image_tips -width 10 \
		-helptext "How to display logs on your web browser" \
        -command  "::cmonLogs::helpTips $page" ]
		
	set but3 [ checkbutton $subf1.fstart.browser -text "Logs on Web Browser only" \
        -variable ::cmonLogs::var($page,browserOnly) -onvalue 1 -offvalue 0 \
		-foreground blue ]
	pack $but2 $but3 -side right -padx 3
    pack $but1 -side left -padx 5 -pady 5  

    pack $subf1.fstart -side top -expand 1 -fill x
    set labfilter [ LabelFrame $subf.filter -text "And apply additional filter" -side top -anchor w \
                   -relief sunken -borderwidth 4 ]
    set subf3  [ $labfilter getframe ]
    set ::cmonLogs::var($page,filter) "none"
    radiobutton $subf3.none -text "No filter (all logs)" -variable ::cmonLogs::var($page,filter) \
        -value "none" -anchor w

    frame $subf3.fpattern
    radiobutton $subf3.fpattern.pattern -text "Regular Expression:" -variable ::cmonLogs::var($page,filter) \
        -value "pattern" -anchor w -command "cmonCommon::regexpDialog cmonLogs $page $parent"
    entry $subf3.fpattern.epattern -textvariable ::cmonLogs::var($page,pattern) -state disabled	
    pack $subf3.fpattern.pattern -side left -anchor w
	pack $subf3.fpattern.epattern -side right -fill x -anchor w -expand 1
    set fsev [ frame $subf3.ftype ]
    set sevb [ radiobutton $fsev.type -text "Severity:" -variable ::cmonLogs::var($page,filter) \
        -value "severity" ]
	set f1  [ frame $fsev.f1 -relief ridge -bd 2 ]
	set f11 [ frame $f1.f11 ]
	set f12 [ frame $f1.f12 ]
	
	set chk1 [ checkbutton $f11.tnotify -text Notify \
        -variable ::cmonLogs::var($page,notify) -onvalue 1 -offvalue 0 \
		-fg red -bg gray -width 7 -justify left -anchor w \
        -command "set ::cmonLogs::var($page,filter) severity" ]
    set chk2  [ checkbutton $f11.tfatal -text  Fatal \
        -variable ::cmonLogs::var($page,fatal) -onvalue 1 -offvalue 0 \
		-fg red -bg gray -width 7 -justify left -anchor w \
        -command "set ::cmonLogs::var($page,filter) severity" ]
	set chk3  [ checkbutton $f11.tnfatal -text "N Fatal" \
        -variable ::cmonLogs::var($page,nfatal) -onvalue 1 -offvalue 0 \
		-fg $::orange -bg gray -width 7 -justify left -anchor w \
        -command "set ::cmonLogs::var($page,filter) severity" ]
    set chk4 [ checkbutton $f11.twarn -text     Warning \
        -variable ::cmonLogs::var($page,warning) -onvalue 1 -offvalue 0 \
		-fg yellow -bg gray -width 7 -justify left -anchor w \
        -command "set ::cmonLogs::var($page,filter) severity" ]
	set chk5 [ checkbutton $f12.tnot  -text    Note \
        -variable ::cmonLogs::var($page,notice) -onvalue 1 -offvalue 0  \
		-fg blue -bg gray -width 7 -justify left -anchor w \
        -command "set ::cmonLogs::var($page,filter) severity" ]
    set chk6 [ checkbutton $f12.tnom  -text    Nominal \
        -variable ::cmonLogs::var($page,nominal) -onvalue 1 -offvalue 0  \
		-fg green -bg gray -width 7 -justify left -anchor w \
        -command "set ::cmonLogs::var($page,filter) severity" ]
	set chk7 [ checkbutton $f12.tdebug  -text  Debug \
        -variable ::cmonLogs::var($page,debug) -onvalue 1 -offvalue 0  \
		-fg purple -bg gray -width 7 -justify left -anchor w \
        -command "set ::cmonLogs::var($page,filter) severity" ]
		
	set ::cmonLogs::var($page,notify) 1
	pack $chk1 $chk2 $chk3 $chk4 -side left -anchor w
	pack $chk5 $chk6 $chk7 -side left -anchor w
	pack $f11 $f12 -side top -anchor w
	pack $sevb $f1 -side left -fill x -anchor w
	pack $subf3.none $subf3.fpattern $fsev -padx 2 -anchor w -fill x -expand 1
	pack $subf.fent $subf.filter -side left -fill both -expand 1
	
	set ::${name}::var($page,apilist) [ list ]
	createAPICheckList $subf3 $name $page
    cmonCommon::setLastTime cmonLogs $page 
    cmonLogs::setdefaultTimesFilter $page
    pack $titf1 -fill both -expand 1
}

## ******************************************************** 
##
## Name: cmonLogs::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
## this proc name is generic for all packages.
## sample test times 
## start 		05/04/00 16:38:27 641493520
## end times 	05/04/00 18:13:24 641499217
##	start		957892986	641928192
## end 		957893198	641928404
##	start		957892986	641928192
## end			641928192	641928192
## start		now
## end			now
	
proc cmonLogs::sendRequest { page } {
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLogs::statusw($page) "Please select an LDAS site"
		return 
	}
	set name [ namespace tail [ namespace current ] ]
	set runcode ""
	;## set servercmd "cntlmon::queryLogsPipe grepLogs $starttime-$endtime"
	if	{ [ catch {
		set repeat 0
		set freq 0 
        set filter [ cmonLogs::setfilter $page ] 
		set regexpOpt "--"
		if 	{ [ info exist ::cmonLogs::var($page,regexpOpt) ] } {
			set regexpOpt $::cmonLogs::var($page,regexpOpt) 
		} else {
            set regexpOpt --
        }	
       	switch $::cmonLogs::var($page,type) {
            jobid   { foreach { jobid runcode } [ ::cmonLogs::jobIdValidate $::cmonLogs::var($page,jobid) ] {break }
                      set servercmd "cntlmon::queryLogsPipe queryJobLogs $jobid $page"
                    }                  
            time    { foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
                      
                      if    { [ info exist ::cmonLogs::logCmd($page) ] } {
                            set logcmd $::cmonLogs::logCmd($page)
                            set servercmd "cntlmon::queryLogsPipe $logcmd $starttime-$endtime $page"
                      } else {
                            set servercmd "cntlmon::queryLogsPipe grepLogs $starttime-$endtime $page"
                      }
                    }
       	}
        set client $::cmonClient::client
        set cmdId "new"
        set apilist [ ::setAPIs $page $name ]
        set numapis [ llength $apilist ]
		if	{ ! $numapis } {
			appendStatus $::cmonLogs::statusw($page) "Please select at least one API"
			return
		}
        if  { $numapis == $::cmonLogs::allAPIs } {
            set apilist all
        }
        
        set cmd "cmonLogs::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\n$servercmd \{$filter\} [ list $apilist ] \"$regexpOpt\""  
		cmonLogs::state1 $page 
		sendCmd $cmd 
	} err ] } {
		cmonLogs::state0 $page 
		appendStatus $::cmonLogs::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLogs::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonLogs::showReply { page rc clientCmd html } {

	if	{ [ catch {
    set name [ namespace tail [ namespace current ] ]
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    # puts "in showReply, rc=$rc, api=$page, client=$client,cmdId=$cmdId"
    set ::cmonLogs::cmdId($page) $cmdId 
    set ::cmonClient::client $client
    
    if  { ! [ string compare $page "jobid" ] } {
        set cmd showJobLog
    } 

    set status ""
	regexp {(^[^<\n]*)\n*(<.*>$)*} $html match status html 	
    set state $::cmonLogs::state($page)
    set site [ string trim $::cmonClient::var(site) ]
  
	switch $rc {
	0 { cmonLogs::state3 $page 	
        
		;## if empty html, do not erase the screen 
		if	{ [ regexp {^<html>} $html ] } { 
            set win .log-cmonLogs:$page 
            cmonCommon::dispLogs cmonLogs $page $status $html $win 
			appendStatus $::cmonLogs::statusw($page) \
			"Filter done; you can also point browser to $status."  1 blue	
		} else  {
            appendStatus $::cmonLogs::statusw($page) $status 1 blue
        }
	  }
	1 {
        appendStatus $::cmonLogs::statusw($page) $status 0 blue	
	  }
	2 { appendStatus $::cmonLogs::statusw($page) $status 0 blue
        cmonLogs::state2 $page
	  }
	3 { appendStatus $::cmonLogs::statusw($page) $status }
	
    }
	} err ] } {
		puts "[myName] err $err" 
	}
	if	{ $rc !=2 } {
		cmonLogs::state0 $page 	
	} 
}

## ******************************************************** 
##
## Name: cmonLogs::setfilter 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLogs::setfilter { page { name cmonLogs } } {
    set type [ set ::${name}::var($page,filter) ]
    catch { unset ::cmonLogs::logCmd($page) }
    
    switch $type {
        none    { set pat .+ }
        jobid   { set pat [ set ::${name}var($page,jobid) ] }   
        pattern { set pat [ set ::${name}::var($page,pattern) ]
				  if	{ ! [ string length $pat ] } {
				  		set pat .+
						set ::${name}::var($page,pattern) .+
				  }
				  # regsub -all {\s} $pat {\\\s+} pat
 				} 
        severity  { set pat [ list ]
				  if  { [ set ::${name}::var($page,notify) ] } {
                      lappend pat mail.gif
					  lappend pat telephone.gif
                  } 
                  if  { [ set ::${name}::var($page,fatal) ] } {
                      lappend pat ball_red.gif
                  } 
				  if  { [ set ::${name}::var($page,nfatal) ] } {
                      lappend pat ball_orange.gif
                  } 
                  if  { [ set ::${name}::var($page,warning) ]  } {
                      lappend pat ball_yellow.gif
                  }
				  if  { [ set ::${name}::var($page,notice) ] } {
                      lappend pat ball_blue.gif
                  }
                  if  { [ set ::${name}::var($page,nominal) ] } {
                      lappend pat ball_green.gif
                  }
				  if  { [ set ::${name}::var($page,debug) ] } {
                      lappend pat ball_purple.gif
                  }
				  
                  set len [ llength $pat ]
                  if  { ! $len } {
                      set pat ".+" 
                      set ::cmonLogs::logCmd($page) grepLogs
                  } else {
                      getLogTimeCmd $pat $page
                      set pat [ join $pat | ]
				  }
                }
        default { set pat ".+" }
    }
    return $pat
}

## ******************************************************** 
##
## Name: cmonLogs::jobIdValidate  
##
## Description:
## ensures jobid is numeric
##
## Parameters:
## jobid in entry widget
##
## Usage:
##  
## 
## Comments:
## 

proc cmonLogs::jobIdValidate { jobid } {
	
	if	{ [ regexp {^([^\d]*)(\d+)$} [ string trim $jobid ] -> runcode jobid ] } {	
        return "$jobid $runcode"
    }
    return -code error "JobId format should be \[<RUNCODE>\]<JOBID>, e.g. LDAS-DEV12345 or 12345."
}


##******************************************************* 
##
## Name: cmonLogs::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonLogs::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
    updateWidgets $::cmonLogs::topfw($page) normal
    enableStart $name $page normal sendRequest
    array set ::cmonLogs::state [ list $page 0 ]
}

##******************************************************* 
##
## Name: cmonLogs::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonLogs::state1 { page } {
    updateWidgets $::cmonLogs::topfw($page) disabled
    array set ::cmonLogs::state [ list $page 1 ]
}


##******************************************************* 
##
## Name: cmonLogs::state2  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonLogs::state2 { page } {

    updateWidgets $::cmonLogs::topfw($page) disabled
    array set ::cmonLogs::state [ list $page 2 ]
	$::cmonLogs::bstart($page) configure -state normal -text CANCEL \
		-command "cmonLogs::cancelRequest $page"
}

#******************************************************** 
##
## Name: cmonLogs::state3  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLogs::state3 { page } {
    $::cmonLogs::bstart($page) configure -state disabled
	array set ::cmonLogs::state [ list $page 3 ]
}

##******************************************************* 
##
## Name: cmonLogs::helpTips
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLogs::helpTips { page } {

	set msg "To display the logs on your web browser,\
open your browser to the html file specified in the status window
e.g. Updated; you can also point browser to \
/home/mlei/dev/cmonClient-tmp[pid]/Log-Filter-2_ldas-dev.html;\
reload page with each new request"
	catch { set ack [ MessageDlg .filtertips -title Tips -type ok -aspect 300 \
		-message $msg -icon info -justify left -font $::MSGFONT ] }
		
}

##******************************************************* 
##
## Name: cmonLogs::autoLogFilter
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## with blt there is only one point located

proc cmonLogs::autoLogFilter { jobid } {
    
    set jobid [ string trim $jobid ]

    if  { ![ string length $jobid ] } {
        return
    }
        
    if  { ! [ info exist ::cmonLogs::statusw ] } {
        $::cmonLogs::notebook0 raise cmonLogs
    }
    set page [ $::cmonLogs::notebook raise ]
    if  { ! $::cmonLogs::state($page) } {
        $::cmonLogs::notebook0 raise cmonLogs
        if	{ [ string length $jobid ] } {
	        set ::cmonLogs::var($page,type) jobid
		    set ::cmonLogs::var($page,jobid) $jobid
            cmonLogs::setdefaultJobFilter $page
        }
    } 
}

## ******************************************************** 
##
## Name: cmonLogs::cancelRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:

proc cmonLogs::cancelRequest { page } {
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLogs::statusw($page) "Please select an LDAS site"
		return 
	}
    if  { $::cmonLogs::state($page) != 2 } {
        return
    }
	if	{ [ catch {
		set repeat 0
		set freq 0
        set client $::cmonClient::client
        set cmdId new	
		set cmd "cmonLogs::showReply $page\n$repeat\n$freq\n\
        $:$cmdId\ncntlmon::cancelLogFilter $page"
        set msg "Cancelling ..."
        appendStatus $::cmonLogs::statusw($page) $msg 0 blue
		cmonLogs::state3 $page 
		sendCmd $cmd 
	} err ] } {
		cmonLogs::state0 $page 
		appendStatus $::cmonLogs::statusw($page) $err
	}
}


## ******************************************************** 
##
## Name: cmonLogs::getLogTimeCmd
##
## Description:
## determine the server cmd to send based on log filter
##
##
## Parameters:
##
## Comments:

proc cmonLogs::getLogTimeCmd { pat page } {

    set fposlist { mail.gif telephone.gif ball_red.gif }
    foreach item $pat {
        if  { [ lsearch $fposlist $item ] == -1 } {
            set ::cmonLogs::logCmd($page) grepLogBalls
            return
        }
    }
    set ::cmonLogs::logCmd($page) grepMailRedballs
       
}

## ******************************************************** 
##
## Name: cmonLogs::setdefaultJobFilter
##
## Description:
## set default options for jobId filter 
##
##
## Parameters:
##
## Comments:
   
proc cmonLogs::setdefaultJobFilter { page } {

    set ::cmonLogs::var($page,type) jobid
    set ::cmonLogs::var($page,filter) none
    foreach api $::cmonLogs::apilist {
        set ::cmonLogs::var($page,$api) 1
    }
}

## ******************************************************** 
##
## Name: cmonLogs::setdefaultTimesFilter
##
## Description:
## set default options for times filter 
##
##
## Parameters:
##
## Comments:
   
proc cmonLogs::setdefaultTimesFilter { page } {

    set ::cmonLogs::var($page,type) time
    ;## set ::cmonLogs::var($page,filter) severity
    ;## set ::cmonLogs::var($page,notify) 1
    ;## set ::cmonLogs::var($page,fatal) 1
    ;##foreach api $::cmonLogs::apilist {
    ;##    set ::cmonLogs::var($page,$api) 0
    ;##}
    ;##set ::cmonLogs::var($page,manager) 1
}
