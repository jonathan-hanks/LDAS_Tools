#! /ldcg/bin/wish

## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) blt_graph Tcl Script.
##
## This script creates a BLT graph and plots data
## based on gproto written by Peter Shawhan
## 
## BLT graph Version 1.0
##
## ******************************************************** 

package provide blt_graph 1.0
 
#------------------------------------------------------------------
# 
# blt_graph widget 
# 
#-------------------------------------------------------------------

package require BLT

#-- Set the blt_library global variable to point to the location of the
#-- Tcl code (and the PostScript prologue files), and append it to auto_path

#-- Set up font
font create normhelv -family helvetica -size 11
font create smallhelv -family helvetica -size 9

namespace eval blt_graph {

#-- Set some global parameters
	set statusColor "#fd7"
    set bgColor "#fff8d0"
	set buttonColor "#cfc"
	set buttonActiveColor "#efe"
	set gridColor "#eee"

}

##*******************************************************
##
## Name: blt_graph::showGraph 
##
## Description:
## creates or updates graph with data supplied
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::showGraph { data start end ymin ymax { parent . } } {

	set set 0
	set typelist [ list ]
	
	foreach { points type symbol dotcolor linewidth } $data {
		set npoints [ expr [ llength $points ] /2 ]
	debugPuts "npoints $npoints, type $type symbol $symbol dotcolor $dotcolor linewidth $linewidth"	

		catch { blt::vector destroy ${type}X } err1
		catch { blt::vector destroy ${type}Y } err2
        if  { $::DEBUG } {
            debugPuts "destroy $type X $err1, $type Y $err2"
        }
		
		if	{ $npoints } {
    		blt::vector create \
	    		${type}X($npoints) ${type}Y($npoints)
			set i 0
			${type}X notify always
			${type}Y notify always
			lappend typelist $type 

			foreach { x y } $points {
				set ${type}X($i) $x
				set ${type}Y($i) $y
				incr i 1
			}
			set symbol$type $symbol
			set dotcolor$type $dotcolor
			set linewidth$type $linewidth
		}
	}

	debugPuts "typelist $typelist"	

    #-- Duplicate the value at the end, so that the last line segment is drawn
    #set ymean(++end) $ymean(end)
    #set ymin(++end) $ymin(end)
    #set ymax(++end) $ymax(end)
    #set npoints [xtime length]
    #set xtime(++end) [expr {2*$xtime($npoints-1)-$xtime($npoints-2)}]
	#set yvalue(++end) [expr {2*$yvalue($npoints-1)-$yvalue($npoints-2)}]

    #-- Create a set of sub-vectors for the time axis, along with an "index"
    #-- vector which tells us the first value of each sub-vector.  These will
    #-- be used to quickly look up the data values at the cursor location.
    #-- This is the kind of thing that would best be done in C, but I haven't
    #-- needed to resort to writing C code yet, so I'll do it with BLT vectors.
    #set ndiv 256
    #set nsub [expr {($npoints-1)/$ndiv+1} ]

    #-- Create the index vector
    # global xtimeIndex
    #blt::vector create xtimeIndex($nsub)

    #set jsub 0
    #set ifirst 0
    #while 1 {
	#-- Calculate the section to copy from the original vector
	#set ilast [expr {$ifirst+$ndiv-1} ]
	#if { $ilast >= $npoints } { set ilast [expr {$npoints-1}] }

	#-- Create the sub-vector
	#set size [expr {$ilast-$ifirst+1}]
	# global xtime$jsub
	#blt::vector create xtime$jsub

	#-- Fill the sub-vector with a section of the original vector
	#xtime$jsub set [xtime0 range $ifirst $ilast]

	#-- Fill the appropriate element of the index vector
	#set xtimeIndex($jsub) $xtime0($ifirst)

	#-- Prepare to continue the loop
	#incr jsub
	#incr ifirst $ndiv
	#if { $ifirst >= $npoints } break
    #}

    #-- Create a new graph mega-widget
    #-- A frame is created with the Tk pathname you specify (.g in this case)
    #-- The BLT stripchart is .g.sc in this case
	
	set graph $parent.g
	if	{ ! [ winfo exist $parent.g ] } {
    	blt_graph::NewGraph $parent.g
    	pack $graph -side top -fill both -expand true
	}

	debugPuts "element names [ $graph.sc element names ]"

	foreach elem [ $graph.sc element names ] {
		$graph.sc element delete $elem
	}
	
    #-- Insert the data
	foreach type $typelist {
			# puts "data for type $type, [ ${type}X length ], [ ${type}Y length ] "
    		$graph.sc element create $type -xdata ${type}X -ydata ${type}Y -mapx GPS \
	    	-symbol [ set symbol$type ] -color [ set dotcolor$type ] -pixels 2 \
			-linewidth [ set linewidth${type} ] -outlinewidth 1
	}
	
    #-- Add some padding at the top and bottom of the plot
    #set useymin $ymin(min)
    #set useymax $ymax(max)
	#set useymin $yvalue(min)
	#set useymax $yvalue(max)

	if	{ !$ymin && !$ymax } {
		set ymax 0.015
	}

	set useymin $ymin
	set useymax $ymax

    set yrange [expr {$useymax-$useymin}]
    #-- Add some padding at the top and bottom of the plot
    if { $yrange > 0.0 } {
	set useymin [expr {$useymin-0.015*$yrange}]
	set useymax [expr {$useymax+0.015*$yrange}]
    } else {
	set useymin [expr {0.999*$useymin}]
	set useymax [expr {1.001*$useymax}]
    }

    #-- Update the axis limits and ticks
	blt_graph::UpdateAxes $graph $start $end $useymin $useymax $parent
	$graph.sc configure -title [ set ::${parent}(title) ]
    
	#set geom [ winfo geometry $parent ]
	#regexp {(\d+)x(\d+)\+(\d+)\+(\d+)} $geom -> width height x y
	#puts "geom=$geom,$width $height $x $y"
	#wm geometry $parent "[ expr round($width*1.0) ]x${height}+${x}+${y}"
	#Blt_ZoomStack $graph.sc
    
    return $graph.sc
}

##*******************************************************
##
## Name: blt_graph::barChart  
##
## Description:
## creates or updates graph with data supplied
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::barChart { data xmin xmax ymin ymax { parent . } } {

	set set 0
	set typelist [ list ]
	
	foreach { points type color barwidth } $data {
		set npoints [ expr [ llength $points ] /2 ]

	debugPuts "npoints $npoints, type $type  color $color $barwidth,\
    xmin=$xmin xmax=$xmax ymin=$ymin ymax=$ymax "	
    debugPuts "data\n$data"

    set halfbar [ expr $barwidth/2.0 ]

		catch { blt::vector destroy ${type}X } err1 
		catch { blt::vector destroy ${type}Y } err2
        if  { $::DEBUG } {
            debugPuts "destroy $type X $err1, $type Y $err2"
        }
				
		if	{ $npoints } {
    		blt::vector create \
	    		${type}X($npoints) ${type}Y($npoints)
			set i 0
			${type}X notify always
			${type}Y notify always
			lappend typelist $type 

			foreach { x y } $points {
				set ${type}X($i) [ expr $x + $halfbar ]
				set ${type}Y($i) $y
				incr i 1
			}
			set color$type $color
			set barwidth$type $barwidth
		}
	}
	debugPuts "typelist $typelist"	

    set ::${parent}(npoints)  $npoints
    #-- Create a new graph mega-widget
    #-- A frame is created with the Tk pathname you specify (.g in this case)
    #-- The BLT stripchart is .g.sc in this case
	
	set graph $parent.g
	if	{ ! [ winfo exist $parent.g ] } {
    	blt_graph::NewBarGraph $parent.g $xmin $xmax $ymin $ymax $npoints
    	pack $graph -side top -fill both -expand true
	}
	debugPuts "element names [ $graph.sc element names ]"

	foreach elem [ $graph.sc element names ] {
		$graph.sc element delete $elem
	}
	
    set stepsize [ set ::${parent}(x1step) ]
    
    #-- Insert the data
	foreach type $typelist {
			# puts "data for type $type, [ ${type}X length ], [ ${type}Y length ] "
    		$graph.sc element create $type -xdata ${type}X -ydata ${type}Y  \
	    	-fg [ set color$type ] -bd 2 -barwidth $stepsize \
			-relief raised -mapx BARX -mapy BARY
	}

    #-- Add some padding at the top and bottom of the plot
	
	if	{ !$ymin && !$ymax } {
		set ymax 5
	}

	set useymin $ymin
	set useymax $ymax

    set yrange [expr {$useymax-$useymin}]
    #-- Add some padding at the top and bottom of the plot
    if { $yrange > 0.0 } {
	set useymin [expr {$useymin-0.015*$yrange}]
	set useymax [expr {$useymax+0.015*$yrange}]
    } else {
	set useymin [expr {0.999*$useymin}]
	set useymax [expr {1.001*$useymax}]
    }
    
    #-- Update the axis limits and ticks
	blt_graph::UpdateBarAxes $graph $stepsize $xmin $xmax $useymin $useymax $parent
	#Blt_ZoomStack $graph.sc
    $graph.sc configure -title [ set ::${parent}(title) ]
    
    if  { $npoints } {
        set ::${parent}(ystats)  "[ blt::vector expr sum(${type}Y) ]  [ blt::vector expr mean(${type}Y) ] \
        [ blt::vector expr median(${type}Y) ] [ blt::vector expr sdev(${type}Y) ]"
    } else {
        catch { unset ::${parent}(ystats) }
        catch { unset ::${parent}(xstats) }
    }
    
    return $graph.sc
}

##*******************************************************
##
## Name: blt_graph::NewGraph 
##
## Description:
## creates new time graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::NewGraph { tkpath args } {

    #-- Create a global state array for this graph mega-widget
    upvar #0 gstate$tkpath gstate
	set localzone [ clock format [clock seconds] -format %Z ]
    set gstate(action) ""
    set gstate(gridLines) UTC
    set gstate(Yformat) "% 2.2g"
    set gstate(y1factor) 1.0
    #set gstate(UTCformat) "%b %e '%y\n%H:%M:%S"
    set gstate(zoomLevel) 0
	regexp {(.[^.]+).} $tkpath -> parent
	
    if  { [ info exist ::${parent}(y1factor) ] } {
        set gstate(y1factor) [ set ::${parent}(y1factor) ]
    }
    
    #-- Create a frame
    frame $tkpath

    #------ Create the status/control area on the right side of the frame

    frame $tkpath.info -borderwidth 1 -relief solid \
	    -background $::blt_graph::statusColor


    #------ Create the grid-line toggle stuff

    frame $tkpath.info.grid -borderwidth 0 -highlightthickness 3 \
	    -background $::blt_graph::bgColor -highlightcolor $::blt_graph::statusColor \
	    -highlightbackground $::blt_graph::statusColor
    label $tkpath.info.grid.label -text "Grid lines:" -font normhelv \
	    -padx 0 -pady 0 -background $::blt_graph::bgColor
    radiobutton $tkpath.info.grid.utc -text $localzone -font normhelv \
	    -variable gstate$tkpath\(gridLines\) -value UTC \
	    -padx 0 -pady 0 -command \
	    "$tkpath.sc grid configure -mapx UTC; blt_graph::UpdateSpacingText $tkpath" \
	    -background $::blt_graph::bgColor -highlightcolor $::blt_graph::bgColor \
	    -highlightbackground $::blt_graph::bgColor \
	    -activebackground $::blt_graph::bgColor
    radiobutton $tkpath.info.grid.gps -text "GPS " -font normhelv \
	    -variable gstate$tkpath\(gridLines\) -value "GPS" \
	    -padx 0 -pady 0 -command \
	    "$tkpath.sc grid configure -mapx GPS; blt_graph::UpdateSpacingText $tkpath" \
	    -background $::blt_graph::bgColor -highlightcolor $::blt_graph::bgColor \
	    -highlightbackground $::blt_graph::bgColor \
	    -activebackground $::blt_graph::bgColor
    label $tkpath.info.grid.spacing -text "Spacing: " -font normhelv \
	    -padx 0 -pady 0 -background $::blt_graph::bgColor

    pack $tkpath.info.grid.label -side top -anchor w
    pack $tkpath.info.grid.spacing -side bottom -anchor w
    pack $tkpath.info.grid.utc -side left
    pack $tkpath.info.grid.gps -side right -expand true -anchor w
    pack $tkpath.info.grid -side top


    #------ Create the cursor readback stuff

    text $tkpath.info.readback -width 0 -height 9 -font normhelv \
	    -borderwidth 0 -relief flat -padx 2 \
	    -background $::blt_graph::bgColor -highlightthickness 3 \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor
    $tkpath.info.readback insert end "Cursor:"
    $tkpath.info.readback config -state disabled
    pack $tkpath.info.readback -side top -fill x


    #------ Create an area with some buttons
  
    frame $tkpath.info.buttons -borderwidth 0 -highlightthickness 3 \
	    -background $::blt_graph::bgColor -highlightcolor $::blt_graph::statusColor \
	    -highlightbackground $::blt_graph::statusColor
        
    ;## if the graph has histogram, create a button for it 
    if  { [ info exist ::${parent}(replot) ] } {
        button $tkpath.info.buttons.hist -text "Y-axis\nHistogram" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::showHistogram $parent" -relief raised -width 12
        pack $tkpath.info.buttons.hist -side top -fill x -expand true
    }
	button $tkpath.info.buttons.save -text "Save" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::save $tkpath" -relief raised -width 12
        
    button $tkpath.info.buttons.queueprint -text "Queue Print" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font smallhelv -command "blt_graph::queuePrint $tkpath.sc" -relief raised
        
    button $tkpath.info.buttons.print -text Print \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::print $tkpath.sc" -relief raised 

    #button $tkpath.info.buttons.help -text "Help" \
	#    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	#    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor \
	#    -font normhelv -command ShowHelp -relief raised -state disabled 
    
	button $tkpath.info.buttons.refresh -text "Refresh" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::refreshPlot $tkpath.sc" -relief raised
    button $tkpath.info.buttons.close -text "Close" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command [ list blt_graph::closeGraph $tkpath ${parent} ] -relief raised
		
	pack $tkpath.info.buttons.save -side top -fill x -expand true
    pack $tkpath.info.buttons.queueprint -side top -fill x -expand true
    pack $tkpath.info.buttons.print -side top -fill x -expand true
    pack $tkpath.info.buttons.refresh -side top -fill x -expand true
    pack $tkpath.info.buttons.close -side top -fill x -expand true
    pack $tkpath.info.buttons -side top


    #------ Create a BLT stripchart

    #-- The default properties in the command below may be overrriden
    #-- by the user, by passing arguments to this proc
    #eval blt::stripchart $tkpath.sc [list \
	#    -height 600 -width 800 -font normhelv \
	#    -borderwidth 0 -relief flat -plotborderwidth 0 -plotrelief solid \
	#    -plotpadx 0 -plotpady 0 -background $::blt_graph::bgColor \
	#    -halo 10000 -bufferelements yes \
	#    ] $args

	blt::graph $tkpath.sc -title [ set ::${parent}(title) ] -borderwidth 0 \
        -relief sunken -plotborderwidth 0 -plotrelief solid -plotpadx 0 -plotpady 0 \
        -width 6i
        
	
    #-- Create GPS and UTC axes
    $tkpath.sc axis create GPS -tickfont smallhelv -titlefont normhelv
    $tkpath.sc axis create UTC -tickfont smallhelv -titlefont normhelv \
	    -command blt_graph::UTCTickLabel
    $tkpath.sc xaxis use GPS
    $tkpath.sc x2axis use UTC
	$tkpath.sc axis configure "y" -title [ set ::${parent}(y1label) ] \
        -command blt_graph::FormatYTickLabel

    #-- Turn on the grid and configure it
    $tkpath.sc grid on
    $tkpath.sc grid configure -color $::blt_graph::gridColor -dashes "" \
	    -mapx $gstate(gridLines) -minor yes 

 	#-- Configure the legend
    $tkpath.sc legend configure -hide no -background PapayaWhip -font smallhelv \
	    -padx 0 -pady 0 -position plotarea -anchor nw \
	    -relief sunken -borderwidth 2

    #-- Configure the crosshairs
    $tkpath.sc crosshairs config -linewidth 1 -dashes "" -color black

    #-- Configure options for encapsulated PostScript output
    #$tkpath.sc postscript config -landscape yes -maxpect yes -decorations no
	# dont max out the image or it will mass out other info put out
	$tkpath.sc postscript config -landscape yes -decorations no
	
    #-- Set up bindings to do things with the mouse within the stripchart
    bind $tkpath.sc <Enter> "blt_graph::MouseHandler $tkpath.sc Enter %x %y %X %Y"
    bind $tkpath.sc <Motion> "blt_graph::MouseHandler $tkpath.sc Motion %x %y %X %Y"
    bind $tkpath.sc <Leave> "blt_graph::MouseHandler $tkpath.sc Leave %x %y %X %Y"
    bind $tkpath.sc <ButtonPress-1> "blt_graph::MouseHandler $tkpath.sc B1Down %x %y %X %Y"
    bind $tkpath.sc <ButtonRelease-1> "blt_graph::MouseHandler $tkpath.sc B1Up %x %y %X %Y"
    bind $tkpath.sc <ButtonPress-3> "blt_graph::MouseHandler $tkpath.sc B3Down %x %y %X %Y"
    bind $tkpath.sc <ButtonRelease-3> "blt_graph::MouseHandler $tkpath.sc B3Up %x %y %X %Y"
    bind $tkpath.sc <Configure> "after idle [list blt_graph::ResizeHandler $tkpath.sc]"
    
    #-- shift key with drag will set times 
    bind $tkpath.sc <Shift-ButtonRelease-1> [ list blt_graph::MouseHandler $tkpath.sc B1Shift %x %y %X %Y ]
    
    #-- Create a BLT marker to frame the plot area.  This may seem redundant,
    #-- but actually it serves two important purposes:  (1) It shows up in the
    #-- PostScript output.  Otherwise, no plot-area border would be drawn since
    #-- we use "-decorations no" when we generate PostScript output.  (2) We
    #-- set the "halo" to a very large number so that all mouse events will be
    #-- checked against the (null) bindings of this marker.  Otherwise, BLT
    #-- would waste a lot of CPU time uselessly identifying the nearest data
    #-- point in the graph and checking its bindings.
    #-- Note that the coordinates of this marker are updated whenever
    #-- UpdateAxes is called.
    $tkpath.sc marker create line -name "plotframe" -linewidth 0 -dashes "" \
		    -mapx GPS -coords [list 0.0 0.0 0.0 0.0]

    #------ Create scrollbars
    #scrollbar $tkpath.scrollx -orient horizontal
    $tkpath.sc axis configure GPS -title GPS


    #------ Lay out the widgets

##    grid $tkpath.sc - - $tkpath.info -sticky news
##    grid x $tkpath.scrollx x ^ -sticky news

    grid $tkpath.sc -row 0 -column 0 -columnspan 3 -sticky news
    grid $tkpath.info -row 0 -column 3 -rowspan 2 -sticky news
    # grid $tkpath.scrollx -row 1 -column 1 -sticky news

    grid rowconfigure $tkpath 0 -weight 1
    grid columnconfigure $tkpath 1 -weight 1
    return
}

##*******************************************************
##
## Name: blt_graph::NewBarGraph 
##
## Description:
## creates new bar graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::NewBarGraph { tkpath args } {

    #-- Create a global state array for this graph mega-widget
    upvar #0 gstate$tkpath gstate
    set gstate(action) ""
    set gstate(gridLines) BARX
    set gstate(Yformat) "%2g"
    #set gstate(UTCformat) "%b %e '%y\n%H:%M:%S"
    set gstate(zoomLevel) 0
	regexp {(.[^.]+).} $tkpath -> parent
	
	set xmin [ lindex $args  0 ]
	set xmax [ lindex $args  1 ]
	set ymin [ lindex $args  2 ]
	set ymax [ lindex $args  3 ]
    set npoints [ lindex $args 4 ]
    
    #-- Create a frame
    frame $tkpath


    #------ Create the status/control area on the right side of the frame

    frame $tkpath.info -borderwidth 1 -relief solid \
	    -background $::blt_graph::statusColor


    #------ Create the cursor readback stuff

    text $tkpath.info.readback -width 0 -height 9 -font normhelv \
	    -borderwidth 0 -relief flat -padx 2 \
	    -background $::blt_graph::bgColor -highlightthickness 3 \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor
    $tkpath.info.readback insert end "Cursor:"
    $tkpath.info.readback config -state disabled
    pack $tkpath.info.readback -side top -fill x


    #------ Create histogram bin function
    foreach { name page min max } [ set ::${parent}(histargs) ] { break }
    set fhist [ cmonCommon::createHistogramSettings $tkpath.info $name $page $min $max ]
    pack $fhist -side top -fill x
    #------ Create an area with some buttons

    frame $tkpath.info.buttons -borderwidth 0 -highlightthickness 3 \
	    -background $::blt_graph::bgColor -highlightcolor $::blt_graph::statusColor \
	    -highlightbackground $::blt_graph::statusColor
 
	button $tkpath.info.buttons.save -text "Save" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::save $tkpath" -relief raised -width 12
    button $tkpath.info.buttons.queueprint -text "Queue Print" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font smallhelv -command "blt_graph::queuePrint $tkpath.sc" -relief raised
        
    button $tkpath.info.buttons.print -text "Print" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::print $tkpath.sc" -relief raised  
                    
    #button $tkpath.info.buttons.refresh -text "Refresh" \
	#    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	#    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor \
	#    -font normhelv -command "blt_graph::refreshPlot $tkpath.sc" -relief raised
    button $tkpath.info.buttons.close -text "Close" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "destroy [ winfo parent $tkpath ]; catch { unset ::${parent} }" -relief raised
        
    #checkbutton $tkpath.info.buttons.logX -text "Log X" \
	#    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	#    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	#    -font normhelv -command "blt_graph::logAxis $tkpath.sc BARX" -relief raised	\
	#	-variable ::${parent}(logX) -on 1 -off 0
    
    checkbutton $tkpath.info.buttons.logY -text "Log Y" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::logYAxis $tkpath.sc BARY" -relief raised	\
		-variable ::${parent}(logY)	-onvalue 1 -offvalue 0 
    checkbutton $tkpath.info.buttons.cumm -text "Cumm" \
	    -background $::blt_graph::buttonColor -activebackground $::blt_graph::buttonActiveColor \
	    -highlightcolor $::blt_graph::statusColor -highlightbackground $::blt_graph::statusColor\
	    -font normhelv -command "blt_graph::cummPlot $tkpath.sc BARY" -relief raised	\
		-variable ::${parent}(cummPlot)	-onvalue 1 -offvalue 0  
                
	pack $tkpath.info.buttons.save -side top -fill x -expand true
    pack $tkpath.info.buttons.queueprint -side top -fill x -expand true
    pack $tkpath.info.buttons.print -side top -fill x -expand true
    # pack $tkpath.info.buttons.refresh -side top -fill x -expand true
	# pack $tkpath.info.buttons.logX -side top -fill x -expand true
	pack $tkpath.info.buttons.logY -side top -fill x -expand true
    pack $tkpath.info.buttons.cumm -side top -fill x -expand true
    pack $tkpath.info.buttons.close -side top -fill x -expand true	
    pack $tkpath.info.buttons -side top


    #------ Create a BLT stripchart

    #-- The default properties in the command below may be overrriden
    #-- by the user, by passing arguments to this proc
    #eval blt::stripchart $tkpath.sc [list \
	#    -height 600 -width 800 -font normhelv \
	#    -borderwidth 0 -relief flat -plotborderwidth 0 -plotrelief solid \
	#    -plotpadx 0 -plotpady 0 -background $::blt_graph::bgColor \
	#    -halo 10000 -bufferelements yes \
	#    ] $args

	regexp {(.[^.]+).} $tkpath -> parent
	if	{ [ info exist ::${parent}(barmode) ] } {
		set barmode [ set ::${parent}(barmode) ]
	} else {
		set barmode normal
	}
	blt::barchart $tkpath.sc -title [ set ::${parent}(title) ] -barmode normal \
        -borderwidth 0 -relief sunken -plotborderwidth 0 -plotrelief solid \
        -plotpadx 0 -plotpady 0 -width 6i   
	
    #-- Create X and Y axis 
    ;## do not set min max for axis create or it may abort due to too many majorticks
	$tkpath.sc axis create BARX -tickfont normhelv -titlefont normhelv -rotate 90 \
		-stepsize  [ set ::${parent}(x1step) ] -subdivisions 1 \
		-title [ set ::${parent}(x1label) ] 

    $tkpath.sc axis create BARY -tickfont normhelv -titlefont normhelv \
		-title [ set ::${parent}(y1label) ] -command blt_graph::FormatLogYTickLabels 

    $tkpath.sc xaxis use BARX
    $tkpath.sc yaxis use BARY
	$tkpath.sc axis configure y -title [ set ::${parent}(y1label) ]

    #-- Turn on the grid and configure it
    $tkpath.sc grid on
    $tkpath.sc grid configure -color $::blt_graph::gridColor -dashes "" \
	    -mapx BARX -mapy BARY -minor yes

    #-- Configure the legend
    $tkpath.sc legend configure -hide yes -background white -font smallhelv \
	    -padx 0 -pady 0 -position top -anchor ne \
	    -relief sunken -borderwidth 2 

    #-- Configure the crosshairs
    $tkpath.sc crosshairs config -hide no -linewidth 1 -dashes "" -color black

    #-- Configure options for encapsulated PostScript output
    #$tkpath.sc postscript config -landscape yes -maxpect yes -decorations no
	$tkpath.sc postscript config -landscape yes -decorations no
	
    #-- Set up bindings to do things with the mouse within the stripchart
    bind $tkpath.sc <Enter> "blt_graph::BarMouseHandler $tkpath.sc Enter %x %y"
    bind $tkpath.sc <Motion> "blt_graph::BarMouseHandler $tkpath.sc Motion %x %y"
    bind $tkpath.sc <Leave> "blt_graph::BarMouseHandler $tkpath.sc Leave %x %y"
    bind $tkpath.sc <ButtonPress-1> "blt_graph::BarMouseHandler $tkpath.sc B1Down %x %y"
    bind $tkpath.sc <ButtonRelease-1> "blt_graph::BarMouseHandler $tkpath.sc B1Up %x %y"
    bind $tkpath.sc <ButtonPress-3> "blt_graph::BarMouseHandler $tkpath.sc B3Down %x %y"
    bind $tkpath.sc <ButtonRelease-3> "blt_graph::BarMouseHandler $tkpath.sc B3Up %x %y"
    bind $tkpath.sc <Configure> "after idle [list blt_graph::ResizeHandler $tkpath.sc]"
	  
    #-- Create a BLT marker to frame the plot area.  This may seem redundant,
    #-- but actually it serves two important purposes:  (1) It shows up in the
    #-- PostScript output.  Otherwise, no plot-area border would be drawn since
    #-- we use "-decorations no" when we generate PostScript output.  (2) We
    #-- set the "halo" to a very large number so that all mouse events will be
    #-- checked against the (null) bindings of this marker.  Otherwise, BLT
    #-- would waste a lot of CPU time uselessly identifying the nearest data
    #-- point in the graph and checking its bindings.
    #-- Note that the coordinates of this marker are updated whenever
    #-- UpdateAxes is called.
    $tkpath.sc marker create line -name "plotframe" -linewidth 0 -dashes "" \
		    -mapx BARX -coords [list 0.0 0.0 0.0 0.0]

    #------ Create scrollbars
    # scrollbar $tkpath.scrollx -orient horizontal -command "blt_graph::ViewAxes $tkpath"
    # $tkpath.sc axis configure GPS -scrollcommand "$tkpath.scrollx set" -title GPS


    #------ Lay out the widgets

##    grid $tkpath.sc - - $tkpath.info -sticky news
##    grid x $tkpath.scrollx x ^ -sticky news

    grid $tkpath.sc -row 0 -column 0 -columnspan 3 -sticky news
    grid $tkpath.info -row 0 -column 3 -rowspan 2 -sticky news
    # grid $tkpath.scrollx -row 1 -column 1 -sticky news

    grid rowconfigure $tkpath 0 -weight 1
    grid columnconfigure $tkpath 1 -weight 1
    update

    $tkpath.sc grid config -mapx BARX

    return
}

##*******************************************************
##
## Name: blt_graph::UpdateAxes 
##
## Description:
## creates new time graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::UpdateAxes { tkpath tmin tmax ymin ymax parent } {

    #-- Get access to the state array for this graph
    upvar #0 gstate$tkpath gstate

    #-- Store the axis limits at the current zoom level
    set yrange [ expr $ymax-$ymin ]
    set xrange [ expr $tmax-$tmin ]
    if  { $xrange <= 1 } {
        if  { $xrange <= 0 } {
            set xrange 0.01
        }
        set tmax [ expr $tmin + $xrange ]
        set xmajorticks [ list $tmin $tmax ]
    }
    if  { $yrange <= 1  } {
        if  { $yrange <= 0 } {
            set yrange 0.01
        }
        set ymax [ expr $ymin + $yrange ]
        set ymajorticks [ list $ymin $ymax ]
    }
    
    set gstate(axisLimits,$gstate(zoomLevel)) [list $tmin $tmax $ymin $ymax]

    #------ Update the Y axis

    #-- Decide on the spacing of the major tick marks--we want at least 3 ticks
    
    ;## only compute the subdivisions etc. if there is a good range 
    set range $yrange
    if  { ! [ info exist ymajorticks ] } {
        set exponent [expr {round(floor(log10($range)))}]
        set power10 [expr {pow(10,$exponent)}]
    
    #-- "power10" is the largest power of 10 that is smaller than the range

        foreach divmult [list 2.0 1.0 0.5 0.2] {
	        set stepsize [expr {$power10*$divmult}]
	        set ntick [expr {int($range/$stepsize)}]
	        if { $range/$stepsize >= 3.5 } break
        }

    #-- Decide how to format the tick labels - count decimal places in stepsize
    
        if { $divmult < 1.0 } {
	        set dplaces [expr {1-$exponent}]
        } else {
	        set dplaces [expr {-$exponent}]
        }
        if { $dplaces > 0 } {
	        set gstate(Yformat) "%.${dplaces}f"
        } else {
	        set gstate(Yformat) "%2g"
        }

    #-- Calculate subdivisions - we want at least 10
        if { $ntick >= 5 } {
	        set subdivs 2
        } elseif { $divmult == 0.5 } {
	        set subdivs 5
        } else {
	        set subdivs 4
        }
    } else {
        set stepsize 1
        set subdivisions 0
    }
    
    ;## if the graph ticks are  is not numeric, format them
    ;## else if the range is too small, use defined majorticks
    ;## else just do majorticks and subdivisions
    
    if  { [ info exist ::${parent}(formatyticks) ] } {
        $tkpath.sc axis config y -min $ymin -max $ymax \
        -subdivisions 0 -stepsize 1 -minorticks 0 -ticklength 0 -command blt_graph::FormatYTickLabel        
    } elseif { [ info exist ymajorticks ] } {
        $tkpath.sc axis configure y -min $ymin -max $ymax -majorticks $ymajorticks        
    } else {
        $tkpath.sc axis config y -min $ymin -max $ymax \
	    -stepsize $stepsize -subdivisions $subdivs -command blt_graph::FormatYTickLabel -majorticks {} 
    }

    #------ Update the GPS axis

    #-- Decide on the spacing of the major tick marks--we want at least 3 ticks
    
    ;## only compute subdivisions for range large enough 
    set range $xrange
    
    if  { ! [ info exist xmajorticks ] } {
        set exponent [expr {round(floor(log10($range)))}]
        set power10 [expr {pow(10,$exponent)}]
        
    #-- "power10" is the largest power of 10 that is smaller than the range
        foreach divmult [list 2.0 1.0 0.5 0.2] {
	        set stepsize [expr {$power10*$divmult}]
	        set ntick [expr {int($range/$stepsize)}]
	        if { $range/$stepsize >= 3.5 } break
        }

    #-- Calculate subdivisions - we want at least 10
        if { $ntick >= 5 } {
	        set subdivs 2
        } elseif { $divmult == 0.5 } {
	        set subdivs 5
        } else {
	        set subdivs 4
        }
    } else {
        set stepsize 1
        set subdivs 0
    }
    #-- Specify the tick interval, and let BLT calculate the actual positions
    
    if  { [ info exist xmajorticks ] } {
        $tkpath.sc axis configure GPS -min $tmin -max $tmax -majorticks $xmajorticks
    } else {
        $tkpath.sc axis configure GPS -min $tmin -max $tmax \
	    -stepsize $stepsize -subdivisions $subdivs -majorticks {} 
    }


    #------ Update the UTC axis 
    #-- This is tricky, because the ticks are not always equally spaced!

    ;## compute ticks only for range large enough
    if  { ! [ info exist xmajorticks ] } {
        set trange [expr {$tmax-$tmin}]
    #-- Loop over lots of possible tick granularities
        foreach { ticktype step nomspace format } [list \
	        year 1 31536000 "%Y" \
	        month 6 15811200 "%b %Y" \
	        month 3 7948800 "%b %Y" \
	        month 1 2678400 "%b %Y" \
	        day 10 864000 "%b %e '%y" \
	        day 5 432000 "%b %e '%y" \
	        day 2 172800 "%b %e '%y" \
	        day 1 86400 "%b %e '%y" \
	        hour 12 43200 "%b %e '%y\n%H:%M" \
	        hour 6 21600 "%b %e '%y\n%H:%M" \
	        hour 3 10800 "%b %e '%y\n%H:%M" \
	        hour 1 3600 "%b %e '%y\n%H:%M" \
	        min 30 1800 "%b %e '%y\n%H:%M" \
	        min 10 600 "%b %e '%y\n%H:%M" \
	        min 5 300 "%b %e '%y\n%H:%M" \
	        min 2 120 "%b %e '%y\n%H:%M" \
	        min 1 60 "%b %e '%y\n%H:%M" \
	        sec 30 30 "%b %e '%y\n%H:%M:%S" \
	        sec 10 10 "%b %e '%y\n%H:%M:%S" \
	        sec 5 5 "%b %e '%y\n%H:%M:%S" \
	        sec 2 2 "%b %e '%y\n%H:%M:%S" \
	        sec 1 1 "%b %e '%y\n%H:%M:%S" \
	        sec 0.5 0.5 "%b %e '%y\n%H:%M:%S" \
	        sec 0.2 0.2 "%b %e '%y\n%H:%M:%S" \
	        sec 0.1 0.1 "%b %e '%y\n%H:%M:%S" \
	        sec 0.05 0.05 "%b %e '%y\n%H:%M:%S" \
	        sec 0.02 0.02 "%b %e '%y\n%H:%M:%S" \
	        sec 0.01 0.01 "%b %e '%y\n%H:%M:%S" ] {

	#-- Use the first one which yields a sufficient number of tick marks
	        if { $trange/$nomspace >= 3.0 } break
        }
    #-- Store information about the tick spacing we selected
        array set gstate [list \
	        tickType $ticktype \
	        tickStep $step \
	        UTCformat $format ]

    #-- Generate the list of tick times
        set ticklist [blt_graph::UTCticks $tkpath $tmin $tmax $ticktype $step]
    } else {
        ;## has to do GPS time as integer for conversion to local time
        set ticklist [ expr int($tmax) ]
    }
    #-- Now configure with this tick list
	set localzone [ clock format [clock seconds] -format %Z ]
    $tkpath.sc axis configure UTC -min $tmin -max $tmax -majorticks $ticklist -title $localzone

    #-- Update the grid spacing info item
    blt_graph::UpdateSpacingText $tkpath
    
    #-- Update the coordinates of the "plotframe" marker
    $tkpath.sc marker config "plotframe" \
	    -coords [list $tmin $ymin $tmax $ymin $tmax $ymax \
	    $tmin $ymax $tmin $ymin ]

    #-- Finally, do an "update" and then re-assert the mapping of the grid to
    #-- one of the axes.  This is not necessary in itself, but it has the
    #-- curious side effect of restoring the black border around the plot
    #-- which otherwise would be missing on two edges.  i.e. this is a
    #-- workaround to an apparent bug (feature?) in BLT, and has no other
    #-- purpose.
    update

    $tkpath.sc grid config -mapx $gstate(gridLines)

    return
}

;## if the range is too fine, no tick marks would be displayed.
;## e.g. xmin 774525799.0 xmax 774525799.001 stepsize 0.001 subdivs 0

proc blt_graph::UpdateBarAxes { tkpath stepsize xmin xmax ymin ymax parent } {

    #-- Get access to the state array for this graph
    upvar #0 gstate$tkpath gstate
	
    set stepsize [ set ::${parent}(x1step) ]
    set range [ expr $xmax-$xmin ]
    set npoints [ set ::${parent}(npoints) ]
    if  { $range < 10 && $npoints < 10 } {
        set subdivs 0
    } else {
        set exponent [expr {round(floor(log10($range)))}]
        set power10 [expr {pow(10,$exponent)}]   
        foreach divmult [list 2.0 1.0 0.5 0.2] {
	        set stepsize [expr {$power10*$divmult}]
	        set ntick [expr {int($range/$stepsize)}]
	        if { $range/$stepsize >= 3.5 } break
        }
         
    #-- Calculate subdivisions - we want at least 10
        if { $ntick >= 5 } {
	        set subdivs 2
        } elseif { $divmult == 0.5 } {
	        set subdivs 5
        } else {
	        set subdivs 1
        }
    }
    
    #-- Store the axis limits at the current zoom level
    set gstate(axisLimits,$gstate(zoomLevel)) [list $xmin $xmax $ymin $ymax]

    $tkpath.sc axis configure BARX -tickfont normhelv -titlefont normhelv  \
		-stepsize $stepsize -subdivisions $subdivs -min $xmin -max $xmax \
         -title [ set ::${parent}(x1label) ] 
         
    $tkpath.sc axis configure BARY -tickfont normhelv -titlefont normhelv -min {} \
		-max {} -title [ set ::${parent}(y1label) ] 
   
   
    if  { [ set ::${parent}(logY) ] } {
        $tkpath.sc axis configure BARY -logscale 1 -autorange 0.0 -min {} -max {}
        foreach { ylimitmin ylimitmax } [ $tkpath.sc axis limits BARY ] { break }
        if  { [ expr abs($ylimitmax - $ylimitmin) ] < 10  } {
            $tkpath.sc axis configure BARY -min 1 -max 10
        }
    } else {
        $tkpath.sc axis configure BARY -logscale 0  -autorange 0.0 -min 0 
        foreach { ylimitmin ylimitmax } [ $tkpath.sc axis limits BARY ] { break }
        if  { [ expr abs($ylimitmax - $ylimitmin) ] < 5  } {
            $tkpath.sc axis configure BARY -min 0 -max 5
        }
    }

    #-- Update the coordinates of the "plotframe" marker
    ;## get y limits from graph
    foreach { ylimitmin ylimitmax } [ $tkpath.sc axis limits BARY ] { break }
    $tkpath.sc marker config "plotframe" \
	    -coords [list $xmin $ylimitmin $xmax $ylimitmin $xmax $ylimitmax \
	    $xmin $ylimitmax $xmin $ylimitmin ]
    
    update idletasks
}

##*******************************************************
##
## Name: blt_graph::ViewAxes 
##
## Description:
## creates new time graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::ViewAxes { tkpath args } {

    #-- Get access to the state array for this graph
    upvar #0 gstate$tkpath gstate

    #-- Scroll the GPS axis according to the usual scrollbar protocol
    eval $tkpath.sc axis view GPS $args

    #-- Move the UTC axis to match it
    set tmin [$tkpath.sc axis cget GPS -min]
    set tmax [$tkpath.sc axis cget GPS -max]
    #-- Update the list of UTC ticks
    set ticklist \
	    [UTCticks $tkpath $tmin $tmax $gstate(tickType) $gstate(tickStep)]
    $tkpath.sc axis config UTC -min $tmin -max $tmax -majorticks $ticklist

    #-- Also update the coordinates of the "plotframe" marker
    set ymin [$tkpath.sc axis cget y -min]
    set ymax [$tkpath.sc axis cget y -max]
    $tkpath.sc marker config "plotframe" \
	    -coords [list $tmin $ymin $tmax $ymin $tmax $ymax \
	    $tmin $ymax $tmin $ymin ]

    return
}

##*******************************************************
##
## Name: blt_graph::UTCticks 
##
## Description:
## creates new time graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::UTCticks { tkpath tmin tmax ticktype step } {

    #-- Get access to the state array for this graph
    upvar #0 gstate$tkpath gstate

    #-- Days per month
    array set mdays [list \
	    1 31 2 29 3 31 4 30 5 31 6 30 7 31 8 31 9 30 10 31 11 30 12 31]

    #-- Truncate to integer
    set tminint [expr {int($tmin)}]

	set utctmin [ gps2utc $tminint 0 ]

	set minsecs [ clock scan $utctmin ]

    #-- Figure out where the ticks should be
    set ticklist {}

    switch -- $ticktype {

	"year" {
	    # set year [tconvert -f "%Y" $tminint]
		set year [ clock format $minsecs -format %Y ]
        set gps [ blt_graph::convert2gps "1/1/$year" ]
        ;## set gps [ utc2gps "1/1/$year" 0 ]
        
	    while { $gps <= $tmax } {
		if { $gps >= $tminint } { lappend ticklist $gps }
		incr year
		# set gps [tconvert "jan 1, $year"]
            set gps [ blt_graph::convert2gps "1/1/$year" ]
	    }
	}

	"month" {
	    # set year [tconvert -f "%Y" $tminint]
	    # set month [tconvert -f "%m" $tminint]
		set year [ clock format $minsecs -format %Y ]
		set month [ clock format $minsecs -format %m ]
		
	    #-- Strip off any leading zero
	    regsub {^0} $month {} month
	    #-- Round down to a multiple of the step

	    set month [expr {int((($month-1)/$step)*$step+1)}]

	    #set gps [tconvert "$month/1/$year"]
        set gps [ blt_graph::convert2gps "$month/1/$year" ]
		# set gps [ utc2gps "$month/1/$year" 0 ]
        
	    while { $gps <= $tmax } {
		if { $gps >= $tminint } { lappend ticklist $gps }
		incr month $step
		if { $month > 12 } {
		    set month 1
		    incr year
		}
		#set gps [tconvert "$month/1/$year"]
        set gps [ blt_graph::convert2gps "$month/1/$year" ]
		;## set gps [ utc2gps "$month/1/$year" 0 ]
	    }
	}

	"day" {
	    #set year [tconvert -f "%Y" $tminint]
	    #set month [tconvert -f "%m" $tminint]
		set year [ clock format $minsecs -format %Y ]
		set month [ clock format $minsecs -format %m ]
		
	    #-- Strip off any leading zero
	    regsub {^0} $month {} month
	    #-- Round down to a multiple of the step
		set day [ clock format $minsecs -format %e ]
		set day [expr {int((($day-1)/$step)*$step+1)}]
	    #set day [expr {(([tconvert -f "%e" $tminint]-1)/$step)*$step+1}]

	    #-- If we're at the end of a month, back off by one step; the
	    #-- code below will handle this case more intelligently
	    if { $day >= 28 } { incr day -$step }

	    #set gps [tconvert "$month/$day/$year"]
        set gps [ blt_graph::convert2gps "$month/$day/$year" ]
		;## set gps [ utc2gps "$month/$day/$year" 0 ]
		
	    while { $gps <= $tmax } {
		if { $gps >= $tminint } { lappend ticklist $gps }
		incr day $step

		#-- Look up the number of days in this month
		if { $month == 2 && $year%4 != 0 } {
		    #-- February in a non-leap year
		    set ndays 28
		} else {
		    set ndays $mdays($month)
		}

		#-- Go on to the next month if we're beyond the end of this
		#-- month, OR if we're on the last day of this month and the
		#-- step size is 2 or more days
		if { $day > $ndays || ($day==$ndays && $step>=2) } {
		    set day 1
		    incr month
		}

		if { $month > 12 } {
		    set month 1
		    incr year
		}
		# set gps [tconvert "$month/$day/$year"]
        set gps [ blt_graph::convert2gps "$month/$day/$year" ]
		;## set gps [ utc2gps "$month/$day/$year" 0 ]
	    }
	}

	"hour" -
	"min" {
	    #-- Handle both hour and minute with same code
	    switch -- $ticktype {
		"hour" { set hstep $step ; set mstep 0 }
		"min" { set hstep 0 ; set mstep $step }
	    }

	    #set year [tconvert -f "%Y" $tminint]
	    #set month [tconvert -f "%m" $tminint]
		set year [ clock format $minsecs -format %Y ]
		set month [ clock format $minsecs -format %m ]
		
	    #-- Strip off any leading zero
	    regsub {^0} $month {} month
	    #set day [tconvert -f "%e" $tminint]
		set day [ clock format $minsecs -format %e ]

	    #-- Round down to nearest hour or nearest minute
	    switch -- $ticktype {
		"hour" {
		    # set hour [tconvert -f "%H" $tminint]
			set hour [ clock format $minsecs -format %H ]
			
		    #-- Strip off any leading zero
		    regsub {^0} $hour {} hour
		    #-- Round down according to the step
		    set hour [expr {($hour/$step)*$step}]
		    set minute 0
		    set hstep $step
		    set mstep 0
		}
		"min" {
		    #set hour [tconvert -f "%H" $tminint]
			set hour [ clock format $minsecs -format %H ]
		    #-- Strip off any leading zero
		    regsub {^0} $hour {} hour

		    #set minute [tconvert -f "%M" $tminint]
			set minute [ clock format $minsecs -format %M ]
			
		    #-- Strip off any leading zero
		    regsub {^0} $minute {} minute
		    #-- Round down according to the step
		    set minute [expr {($minute/$step)*$step}]
		    set hstep 0
		    set mstep $step
		}
	    }

	    # set gps [tconvert "$month/$day/$year $hour:$minute"]
        ;## if gps time is < 01/05/80 16:00:00 PST (01/06/80 00:00:00 GMT)
        ;## will get an error from gpsTime for predating epoch.
        ;## just keep increasing until it does not predate 
        
        set gps [ blt_graph::convert2gps "$month/$day/$year $hour:$minute" ]
	    while { $gps <= $tmax } {
		if { $gps >= $tminint } { lappend ticklist $gps }
		incr minute $mstep
		incr hour $hstep
		if { $minute >= 60 } {
		    incr minute -60
		    incr hour
		}
		if { $hour >= 24 } {
		    incr hour -24
		    incr day
		}
		#-- Check whether to switch to the next month
		if { $day > $mdays($month) || \
			($month == 2 && $day == 29 && $year%4 != 0) } {
		    set day 1
		    incr month
		}
		if { $month > 12 } {
		    set month 1
		    incr year
		}
		#set gps [tconvert "$month/$day/$year $hour:$minute"]
        set gps [ blt_graph::convert2gps "$month/$day/$year $hour:$minute" ]
	    }
	}

	"sec" {

	    #-- Align with UTC minute boundary	
	    #set gps [tconvert [tconvert -f "%m/%d/%Y %H:%M" \
		#    [expr {int($tmin+$step)}] ] ]
        
        ;## prevent infinite loop of small ranges where (int of tmin + step) produce same gps
        
        if	{ [expr round($tmin+$step)] <= $tmax && $step < 0.5 } {
			lappend ticklist [ expr int($tmax) ]
		} else {
            set gps [ blt_graph::convert2gps [ gps2utc [ expr {int($tmin+$step)}] 0 ] ]
            if	{ [ expr $tmax - $tmin ] < 2 && $step < 0.5 } {
			    lappend ticklist [ expr int($tmax) ]
                set ticklist int($tmax)
	        } else {
		# set utctime [ clock format [ expr {int($tmin+$step)}] -format "%m/%d/%Y %H:%M" ]
                set gps [ blt_graph::convert2gps [ gps2utc [ expr {int($tmin+$step)}] 0 ] ]
        
	            while { $gps <= $tmax } {
		            if { $gps >= $tmin } { lappend ticklist $gps }
		            set gps [expr round($gps+$step)]
		#-- If step is greater than 1, and we hit a leap second,
		#-- then increment by one second
		            set tsecs [ gps2utc $gps 0 ]
		            set tsecs [ clock format [ clock scan $tsecs ] -format %S ]
		#if { $step > 1 && [tconvert -f %S $gps] == 60 } { incr gps }		
		#-- Avoid cumulative round-off error by tweaking value to
		#-- be an integer plus a multiple of the step
		            if  { $step > 1 && $tsecs == 60 } { incr gps }
		            set gpsint [expr {round($gps)}]
		            set gpsfrac [expr {$gps-double($gpsint)}]
		            set gpsfrac [expr {round($gpsfrac/$step)*$step}]
		            set gps [expr {$gpsint+$gpsfrac}]
               }
            }
        }
	}

    }
    return $ticklist
}

##*******************************************************
##
## Name: blt_graph::UTCTickLabel
##
## Description:
## creates new time graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::UTCTickLabel { sc value } {

    #-- Get access to the state array for this graph
    upvar #0 gstate[winfo parent $sc] gstate
    #if { [catch {tconvert -f $gstate(UTCformat) $value} string ] } {
###	puts "Error doing tconvert -f $gstate(UTCformat) $value : $string"
	#return "???"
    #}
	if	{ [ catch {
		set string [ string trim [ gps2utc [ expr round($value) ] 0 ] " PDST" ]
		regsub {\s} $string \n string
	} err ] } {
		puts "Error converting x axis GPS to UTC: $value, $err"
		return
	}
	
    #-- Condense consecutive spaces
    regsub -all { {2,}} $string { } string
    return $string
}

##*******************************************************
##
## Name: blt_graph::UpdateSpacingText
##
## Description:
## creates new time graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::UpdateSpacingText { tkpath } {

    #-- Get access to the state array for this graph
    upvar #0 gstate$tkpath gstate

    #-- Look up some info to infer the spacing

    switch -- $gstate(gridLines) {

	"GPS" {
	    set count [$tkpath.sc axis cget GPS -stepsize]
	    if { $count < 100000 } {
		set text "Spacing: [format %g $count] s"
	    } else {
		set text "Spacing: [format %.0e $count] s"
	    }
	}

	"UTC" {
	    set text "Spacing: [format %g $gstate(tickStep)] $gstate(tickType)"
	}

    }

    $tkpath.info.grid.spacing config -text $text

    return
}

##*******************************************************
##
## Name: blt_graph::MouseHandler
##
## Description:
## creates new time graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::MouseHandler { sc event x y X Y } {

    #-- Get access to the state array for this graph
    set tkpath [winfo parent $sc]
    upvar #0 gstate$tkpath gstate
	regexp {(.[^.]+).} $tkpath -> parent
	
    switch -- $event {

	"Enter" -
	"Motion" {
	    #-- If we're in the middle of something, act
	    switch -- $gstate(action) {

		"" {

		    #-- Enable the crosshairs
		    $sc crosshairs config -hide no -position @$x,$y

		    #------ Update the readback area

		    set text "Cursor:\n"

		    #-- Look up the extent of the plot area
		    foreach {px py pdx pdy} [$sc extent plotarea] break
        
		    if { $x >= $px && $x < $px+$pdx } {
			#-- Display the time at the cursor

			    if { $y >= $py && $y < $py+$pdy } {
                
                    set rc [ $sc element closest $x $y point ]
                    if  { !$rc } {
                        set gps [expr {int([$sc axis invtransform GPS $x])} ]
                        set utc [ gps2utc $gps 0 ]
                        append text "  $utc\n  $gps\n"
			    #-- Display channel data
				# append text "y=[ expr round( $gstate(y1factor) * [ $sc axis invtransform y $y ] ) ]"
				        append text "y=[ format "%.2f" [ expr $gstate(y1factor) * [ $sc axis invtransform y $y ] ] ]"
                        if	{ [ info exist ::${parent}(Leave) ] } {
                            set cmd "[ set ::${parent}(Leave) ]"
                            eval $cmd
                        }
				#-- handle user defined action
                } else {
                    set gps [ expr int($point(x)) ] 
                    set utc [ gps2utc $gps 0 ]                
                    append text "  $utc\n  $gps\n"
                    append text "y=[ format "%.2f" [ expr $gstate(y1factor) * $point(y) ] ]"
				    if	{ [ info exist ::${parent}(Enter) ] } {
                        set cmd "[ set ::${parent}(Enter) ] $sc $parent $point(x) $point(y) $X $Y"
				        eval $cmd
                    }
				}
                }
            } else {
                set gps [expr {int([$sc axis invtransform GPS $x])} ]
                set utc [ gps2utc $gps 0 ]
                    
		    } 

		    #-- Update the readback text widget
		    $tkpath.info.readback config -state normal
		    $tkpath.info.readback delete 0.0 end
		    $tkpath.info.readback insert end $text
		    $tkpath.info.readback config -state disabled
			
		}
        
		"ZoomIn" {
		    #-- Update the zoom box
		    set cx [$sc axis invtransform GPS $x]
		    set cy [$sc axis invtransform y $y]
            set gps [expr {int([$sc axis invtransform GPS $x])} ]
           
		    switch -- $gstate(zoomType) {
			"XY" {
			    set cx1 $gstate(mcx)
			    set cx2 $cx
			    set cy1 $gstate(mcy)
			    set cy2 $cy
			}
			"X" {
			    set cx1 $gstate(mcx)
			    set cx2 $cx
			    set cy1 $gstate(cpy1)
			    set cy2 $gstate(cpy2)
			}
			"Y" {
			    set cx1 $gstate(cpx1)
			    set cx2 $gstate(cpx2)
			    set cy1 $gstate(mcy)
			    set cy2 $cy
			}
		    }
            set names [ $sc marker names ]
            if  { [ string first zoomBox $names ] > -1 } {
		        $sc marker config zoomBox -coords [list \
			    $cx1 $cy1 $cx2 $cy1 $cx2 $cy2 $cx1 $cy2 $cx1 $cy1 ]

		        set text "Cursor:\n"
		        if { $gstate(zoomType) != "Y" } {
			    #-- Display the time at the cursor
			        set gps [expr {int([$sc axis invtransform GPS $x])} ]
			    # set utc [tconvert -f "%Y %b %e\n  %H:%M:%S UTC" $gps]
			        set utc [ gps2utc $gps 0 ]
			        append text "  $utc\n  $gps\n"
		        }

		        #-- Update the readback text widget
		        $tkpath.info.readback config -state normal
		        $tkpath.info.readback delete 0.0 end
		        $tkpath.info.readback insert end $text
		        $tkpath.info.readback config -state disabled
            }
		}

		"MoveLegend" {

		    #-- Calculate new position for legend
		    set newx [expr {$gstate(lx)-$gstate(mx)+$x}]
		    set newy [expr {$gstate(ly)-$gstate(my)+$y}]

		    #-- Constrain the legend to remain within the plot area
		    foreach {px py pdx pdy} [$sc extent plotarea] break
		    if { $newx < $px } { set newx $px }
		    if { $newx+$gstate(ldx) >= $px+$pdx-1 } {
			set newx [expr {$px+$pdx-$gstate(ldx)-1}]
		    }
		    if { $newy < $py } { set newy $py }
		    if { $newy+$gstate(ldy) >= $py+$pdy-1 } {
			set newy [expr {$py+$pdy-$gstate(ldy)-1}]
		    }
		    
		    #-- Move the legend
		    $sc legend config -position @$newx,$newy
		}

	    }

	}

	"Leave" {

	    #-- If we're in the middle of something, clean up
	    switch -- $gstate(action) {
		"MoveLegend" {
		    #-- Leave the legend in its last known position
		    set gstate(action) ""
		}
        "B1Shift" -
		"ZoomIn" {
		    #-- If zooming along just one axis, keep the zoom active
		    #-- despite being outside the window.  If zooming along
		    #-- both axes, cancel the zoom.
		    if { $gstate(zoomType) == "XY" } {
			#-- Cancel the zoom
			$sc marker delete zoomBox
			set gstate(action) ""
		    } else {
			#-- Zooming along just one axis
			return
		    }
		}	
	    }
		
		# remove markers
		if	{ [ info exist ::${parent}(Leave) ] } {
			set cmd "[ set ::${parent}(Leave) ]"
			eval $cmd
		}	
		
	    #-- Disable the crosshairs
	    $sc crosshairs config -hide yes

	    #-- Clear the readback area
	    $tkpath.info.readback config -state normal
	    $tkpath.info.readback delete 0.0 end
	    $tkpath.info.readback insert end "Cursor:"
	    $tkpath.info.readback config -state disabled

	}

	"B1Down" {

	    #-- Disable the crosshairs
	    $sc crosshairs config -hide yes

	    #-- Clear the readback area
	    $tkpath.info.readback config -state normal
	    $tkpath.info.readback delete 0.0 end
	    $tkpath.info.readback insert end "Cursor:"
	    $tkpath.info.readback config -state disabled

	    #-- Get the current dimensions of the legend
	    foreach { lx ly ldx ldy } [$sc extents legend] break
	    #-- Adjust legend dimensions to account for padding
	    set pads [$sc legend cget -padx]
	    incr lx -[lindex $pads 0]
	    incr ldx [lindex $pads 0] ; incr ldx [lindex $pads 1]
	    set pads [$sc legend cget -pady]
	    incr ly -[lindex $pads 0]
	    incr ldy [lindex $pads 0] ; incr ldy [lindex $pads 1]

	    #-- Check whether we are within the legend box
	    if { ($x>=$lx) && ($x<$lx+$ldx) && ($y>=$ly) && ($y<$ly+$ldy) } {
		#-- Record some state information, including the original
		#-- positions of the legend and the mouse
		array set gstate [list \
			action "MoveLegend" \
			lx $lx ly $ly \
			ldx $ldx ldy $ldy \
			mx $x my $y ]
		#-- We're done handling this event
		return
	    }

	    #-- If we get here, then we must be initiating a zoom

	    #-- If we're in the plot area, zoom into a rectangular area.
	    #-- Otherwise, zoom along just one axis.
	    foreach {px py pdx pdy} [$sc extent plotarea] break
	    if { $x >= $px && $x < $px+$pdx } {
		if { $y >= $py && $y < $py+$pdy } {
		    set zoomtype "XY"
		} else {
		    set zoomtype "X"
		}
	    } elseif { $y >= $py && $y < $py+$pdy } {
		set zoomtype "Y"
	    } else {
		#-- In a corner ... don't zoom at all
		return
	    }

	    #-- Create the zoom marker (using data coordinates)
	    set cx [$sc axis invtransform GPS $x]
	    set cpx1 [$sc axis cget GPS -min]
	    set cpx2 [$sc axis cget GPS -max]
	    set cy [$sc axis invtransform y $y]
	    set cpy1 [$sc axis cget y -min]
	    set cpy2 [$sc axis cget y -max]
	    switch -- $zoomtype {
		"XY" { set coords [list $cx $cy $cx $cy] }
		"X" { set coords [list $cx $cpy1 $cx $cpy2] }
		"Y" { set coords [list $cpx1 $cy $cpx2 $cy] }
	    }

	    $sc marker create line -name "zoomBox" -linewidth 1 -dashes 5 \
		    -mapx GPS -coords $coords

	    #-- Record some state information
	    array set gstate [list \
			action "ZoomIn" \
			zoomType $zoomtype \
			mx $x my $y \
			mcx $cx mcy $cy \
			cpx1 $cpx1 cpx2 $cpx2 cpy1 $cpy1 cpy2 $cpy2 ]

	    #-- Pretend there is motion to here; that turns crosshairs on, etc.
	    blt_graph::MouseHandler $sc Motion $x $y $X $Y 
	}

	"B1Up" {
    	if	{ [ info exist ::${parent}(B1Up) ] } {
		    foreach elem [ $sc element names ] { 
			    set  rc [ $sc element closest $x $y point ]
				if	{ $rc } {
			        set cmd "[ set ::${parent}(B1Up) ] $sc $parent $point(x) $point(y) $X $Y"
			        eval $cmd
                }
            }
		}		
	    #-- If we were zooming, finish it
	    if { $gstate(action) == "ZoomIn" } {
		#-- Delete the zoom marker
		$sc marker delete zoomBox
		set gstate(action) ""

		#-- If the mouse moved more than three pixels in the zoom
		#-- direction(s), zoom in now
		if { ( abs($x-$gstate(mx)) > 3 || $gstate(zoomType)=="Y" ) && \
		     ( abs($y-$gstate(my)) > 3 || $gstate(zoomType)=="X" ) } {

		    #-- Sort the data points
		    if { $x < $gstate(mx) } {
			set sx1 $x
			set sx2 $gstate(mx)
		    } else {
			set sx1 $gstate(mx)
			set sx2 $x
		    }
		    if { $y > $gstate(my) } {
			set sy1 $y
			set sy2 $gstate(my)
		    } else {
			set sy1 $gstate(my)
			set sy2 $y
		    }

		    #-- Convert to data coordinates
		    switch -- $gstate(zoomType) {
			"XY" {
			    set cx1 [$sc axis invtransform GPS $sx1]
			    set cx2 [$sc axis invtransform GPS $sx2]
			    set cy1 [$sc axis invtransform y $sy1]
			    set cy2 [$sc axis invtransform y $sy2]
			}
			"X" {
			    set cx1 [$sc axis invtransform GPS $sx1]
			    set cx2 [$sc axis invtransform GPS $sx2]
			    set cy1 $gstate(cpy1)
			    set cy2 $gstate(cpy2)
			}
			"Y" {
			    set cx1 $gstate(cpx1)
			    set cx2 $gstate(cpx2)
			    set cy1 [$sc axis invtransform y $sy1]
			    set cy2 [$sc axis invtransform y $sy2]
			}
		    }

		    #-- Constrain the zoom to the current plot area
		    if { $cx1 < $gstate(cpx1) } { set cx1 $gstate(cpx1) }
		    if { $cx2 > $gstate(cpx2) } { set cx2 $gstate(cpx2) }
		    if { $cy1 < $gstate(cpy1) } { set cy1 $gstate(cpy1) }
		    if { $cy2 > $gstate(cpy2) } { set cy2 $gstate(cpy2) }

		    #-- Increment the zoom level and update the plot axes
		    incr gstate(zoomLevel)
		    blt_graph::UpdateAxes $tkpath $cx1 $cx2 $cy1 $cy2 $parent
		}

	    }

	    #-- Clear the action
	    set gstate(action) ""

	    #-- Pretend there is motion to here; that turns crosshairs on, etc.
	    blt_graph::MouseHandler $sc Motion $x $y $X $Y

	}

	"B3Up" {

	    #-- Un-zoom (if zoomed)
	    if { $gstate(zoomLevel) > 0 } {
		#-- Retrieve the axis coords at the previous zoom level
		incr gstate(zoomLevel) -1
		set coords $gstate(axisLimits,$gstate(zoomLevel))
		eval UpdateAxes $tkpath $coords $parent
	    }
	}
    
    "B1Shift"  {
    	#-- Update the zoom box
		    set cx [$sc axis invtransform GPS $x]
		    set cy [$sc axis invtransform y $y]
            set gps [expr {int([$sc axis invtransform GPS $x])} ]
           
		    switch -- $gstate(zoomType) {
			"XY" {
			    set cx1 $gstate(mcx)
			    set cx2 $cx
			    set cy1 $gstate(mcy)
			    set cy2 $cy
			}
			"X" {
			    set cx1 $gstate(mcx)
			    set cx2 $cx
			    set cy1 $gstate(cpy1)
			    set cy2 $gstate(cpy2)
			}
			"Y" {
			    set cx1 $gstate(cpx1)
			    set cx2 $gstate(cpx2)
			    set cy1 $gstate(mcy)
			    set cy2 $cy
			}
		    }
 
            cmonCommon::setTime [ expr round($cx1) ] [ expr round($cx2) ]
            set names [ $sc marker names ] 
            if  { [ string first zoomBox $names ] > -1 } {
                $sc marker configure zoomBox -linewidth 2 -dashes 10 \
		        -mapx GPS -coords  [ list $cx1 $cy1 $cx2 $cy1 $cx2 $cy2 $cx1 $cy2 $cx1 $cy1 ] \
                -fill orange
 	            #-- Record some state information
	            array set gstate [list \
			    action "B1Shift" \
			    zoomType XY \
			    mx $x my $y \
			    mcx $cx mcy $cy \
			    cpx1 $cx1 cpx2 $cx2 cpy1 $cy1 cpy2 $cy2 ]
            
		        set text "New Time Period:\n"
		        if  { $gstate(zoomType) != "Y" } {
			    #-- Display the time at the cursor
			        append text "From: $cx1\nTo:  $cx2\n"
		        }

		        #-- Update the readback text widget
		        $tkpath.info.readback config -state normal
		        $tkpath.info.readback delete 0.0 end
		        $tkpath.info.readback insert end $text
		        $tkpath.info.readback config -state disabled
            }            
        }

    }

    return
}

proc blt_graph::BarMouseHandler { sc event x y } {

    #-- Get access to the state array for this graph
    set tkpath [winfo parent $sc]
    upvar #0 gstate$tkpath gstate
	regexp {(.[^.]+).} $tkpath -> parent

    switch -- $event {

	"Enter" -
	"Motion" {
		if 	{ [ info exist ::${parent}(Leave) ] } {
			set cmd "[ set ::${parent}(Leave) ]"
			eval $cmd
		}
	    #-- If we're in the middle of something, act
	    switch -- $gstate(action) {
		"" {

		    #-- Enable the crosshairs
		    $sc crosshairs config -hide no -position @$x,$y

		    #------ Update the readback area

		    set text "Cursor:\n"

		    #-- Look up the extent of the plot area
		    foreach {px py pdx pdy} [$sc extent plotarea] break
    
		    if { ( $x >= $px && $x < $px+$pdx ) && ( $y >= $py && $y < $py+$pdy ) } {
                set binsize [ set ::${parent}(x1step) ]
                set halfbin [ expr $binsize / 2.0 ]
            #-- Display channel data only if data is present
                set  rc [ $sc element closest $x $y point ]
                if  { $rc } { 
                    set x_value [ expr $point(x) - $halfbin ]                   
                    set nextx   [ expr $x_value + $binsize ]
                    set y_value $point(y)
				    append text "x ($x_value, $nextx)\ny=[ format %0.f $y_value]"   
                } else {
                    set x_value [ format "%.2f" [$sc axis invtransform BARX $x ] ]
                    # set nextx   [ format "%.2f" [ expr $x_value + $binsize ] ]
                    append text "x ($x_value)\n"
                    append text "y=[ format "%.2f" [ expr [$sc axis invtransform BARY $y ] ] ]" 
                }           
            } elseif { [ info exist ::${parent}(Leave) ] } {
				set cmd "[ set ::${parent}(Leave) ]"
				eval $cmd			
		    } elseif { [ info exist ::${parent}(Leave) ] } {
				set cmd "[ set ::${parent}(Leave) ]"
				eval $cmd
			}	
		    #-- Update the readback text widget
		    $tkpath.info.readback config -state normal
		    $tkpath.info.readback delete 0.0 end
		    $tkpath.info.readback insert end $text
		    $tkpath.info.readback config -state disabled
			
		}
                
		"ZoomIn" {
		    #-- Update the zoom box
		    set cx [$sc axis invtransform BARX $x]
		    set cy [$sc axis invtransform BARY $y]

			;## fix : draw only X zoom for now"
		    set  gstate(zoomType) "X"
		    switch -- $gstate(zoomType) {
			"XY" {
			    set cx1 $gstate(mcx)                
			    set cx2 $cx
			    set cy1 $gstate(mcy)
			    set cy2 $cy
			}
			"X" {
			    set cx1 $gstate(mcx)
			    set cx2 $cx
			    #set cy1 $gstate(cpy1)
			    #set cy2 $gstate(cpy2)
                foreach {cy1 cy2} [ $sc axis limits BARY ] { break }
			}
			"Y" {
			    set cx1 $gstate(cpx1)
			    set cx2 $gstate(cpx2)
			    set cy1 $gstate(mcy)
			    set cy2 $cy
			}
		    }
            set names [ $sc marker names ]
            if  { [ string first zoomBox $names ] > -1 } {
		        $sc marker configure zoomBox -coords [ list \
			    $cx1 $cy1 $cx2 $cy1 $cx2 $cy2 $cx1 $cy2 $cx1 $cy1 ]
                
		        #-- Update the readback text widget
		        $tkpath.info.readback config -state normal
		        $tkpath.info.readback delete 0.0 end
		        $tkpath.info.readback insert end "Cursor:\n$cx1 $cy1\n$cx2 $cy2"
		        $tkpath.info.readback config -state disabled
            }
		}

		"MoveLegend" {

		    #-- Calculate new position for legend
		    set newx [expr {$gstate(lx)-$gstate(mx)+$x}]
		    set newy [expr {$gstate(ly)-$gstate(my)+$y}]

		    #-- Constrain the legend to remain within the plot area
		    foreach {px py pdx pdy} [$sc extent plotarea] break
		    if { $newx < $px } { set newx $px }
		    if { $newx+$gstate(ldx) >= $px+$pdx-1 } {
			set newx [expr {$px+$pdx-$gstate(ldx)-1}]
		    }
		    if { $newy < $py } { set newy $py }
		    if { $newy+$gstate(ldy) >= $py+$pdy-1 } {
			set newy [expr {$py+$pdy-$gstate(ldy)-1}]
		    }
		    
		    #-- Move the legend
		    $sc legend config -position @$newx,$newy
		}

	    }

	}

	"Leave" {

	    #-- If we're in the middle of something, clean up
	    switch -- $gstate(action) {
		"MoveLegend" {
		    #-- Leave the legend in its last known position
		    set gstate(action) ""
		}
        "B1Shift" -
		"ZoomIn" {
		    #-- If zooming along just one axis, keep the zoom active
		    #-- despite being outside the window.  If zooming along
		    #-- both axes, cancel the zoom.
		    if { $gstate(zoomType) == "XY" } {
			#-- Cancel the zoom
			$sc marker delete zoomBox
			set gstate(action) ""
		    } else {
			#-- Zooming along just one axis
			return
		    }
		}	
	    }
		
	    #-- Disable the crosshairs
	    $sc crosshairs config -hide yes

	    #-- Clear the readback area
	    $tkpath.info.readback config -state normal
	    $tkpath.info.readback delete 0.0 end
	    $tkpath.info.readback insert end "Cursor:"
	    $tkpath.info.readback config -state disabled

	}

	"B1Down" {

	    #-- Disable the crosshairs
	    $sc crosshairs config -hide yes

	    #-- Clear the readback area
	    $tkpath.info.readback config -state normal
	    $tkpath.info.readback delete 0.0 end
	    $tkpath.info.readback insert end "Cursor:"
	    $tkpath.info.readback config -state disabled

	    #-- Get the current dimensions of the legend
	    foreach { lx ly ldx ldy } [$sc extents legend] break
	    #-- Adjust legend dimensions to account for padding
	    set pads [$sc legend cget -padx]
	    incr lx -[lindex $pads 0]
	    incr ldx [lindex $pads 0] ; incr ldx [lindex $pads 1]
	    set pads [$sc legend cget -pady]
	    incr ly -[lindex $pads 0]
	    incr ldy [lindex $pads 0] ; incr ldy [lindex $pads 1]

	    #-- Check whether we are within the legend box
	    if { ($x>=$lx) && ($x<$lx+$ldx) && ($y>=$ly) && ($y<$ly+$ldy) } {
		#-- Record some state information, including the original
		#-- positions of the legend and the mouse
		array set gstate [list \
			action "MoveLegend" \
			lx $lx ly $ly \
			ldx $ldx ldy $ldy \
			mx $x my $y ]
		#-- We're done handling this event
		return
	    }

	    #-- If we get here, then we must be initiating a zoom

	    #-- If we're in the plot area, zoom into a rectangular area.
	    #-- Otherwise, zoom along just one axis.
	    foreach {px py pdx pdy} [$sc extent plotarea] break
	    if { $x >= $px && $x < $px+$pdx } {
		if { $y >= $py && $y < $py+$pdy } {
		    set zoomtype "XY"
		} else {
		    set zoomtype "X"
		}
	    } elseif { $y >= $py && $y < $py+$pdy } {
		set zoomtype "Y"
	    } else {
		#-- In a corner ... don't zoom at all
		return
	    }

	    #-- Create the zoom marker (using data coordinates)
	    set cx [$sc axis invtransform BARX $x]
	    set cpx1 [$sc axis cget BARX -min]
	    set cpx2 [$sc axis cget BARX -max]
	    set cy [$sc axis invtransform BARY $y]
	    set cpy1 [$sc axis cget BARY -min]
	    set cpy2 [$sc axis cget BARY -max]
	    switch -- $zoomtype {
		"XY" { set coords [list $cx $cy $cx $cy] }
		"X" { set coords [list $cx $cpy1 $cx $cpy2] }
		"Y" { set coords [list $cpx1 $cy $cpx2 $cy] }
	    }
	    $sc marker create line -name "zoomBox" -linewidth 1 -dashes 5 \
		    -mapx BARX -coords $coords
			
	    #-- Record some state information
	    array set gstate [list \
			action "ZoomIn" \
			zoomType $zoomtype \
			mx $x my $y \
			mcx $cx mcy $cy \
			cpx1 $cpx1 cpx2 $cpx2 cpy1 $cpy1 cpy2 $cpy2 ]

	    #-- Pretend there is motion to here; that turns crosshairs on, etc.
	    blt_graph::BarMouseHandler $sc Motion $x $y
	}

	"B1Up" {
		if	{ [ info exist ::${parent}(B1Up) ] } {
			set cmd "[ set ::${parent}(B1Up) ]"
			eval $cmd
		}		
	    #-- If we were zooming, finish it
	    if { $gstate(action) == "ZoomIn" } {
		#-- Delete the zoom marker
		$sc marker delete zoomBox
		set gstate(action) ""

		#-- If the mouse moved more than three pixels in the zoom
		#-- direction(s), zoom in now
		if { ( abs($x-$gstate(mx)) > 3 || $gstate(zoomType)=="Y" ) && \
		     ( abs($y-$gstate(my)) > 3 || $gstate(zoomType)=="X" ) } {

		    #-- Sort the data points
		    if { $x < $gstate(mx) } {
			set sx1 $x
			set sx2 $gstate(mx)
		    } else {
			set sx1 $gstate(mx)
			set sx2 $x
		    }
		    if { $y > $gstate(my) } {
			set sy1 $y
			set sy2 $gstate(my)
		    } else {
			set sy1 $gstate(my)
			set sy2 $y
		    }

		    #-- Convert to data coordinates
		    switch -- $gstate(zoomType) {
			"XY" {
			    set cx1 [$sc axis invtransform BARX $sx1]
			    set cx2 [$sc axis invtransform BARX $sx2]
			    set cy1 [$sc axis invtransform BARY $sy1]
			    set cy2 [$sc axis invtransform BARY $sy2]
			}
			"X" {
			    set cx1 [$sc axis invtransform BARX $sx1]
			    set cx2 [$sc axis invtransform BARX $sx2]
			    set cy1 $gstate(cpy1)
			    set cy2 $gstate(cpy2)
			}
			"Y" {
			    set cx1 $gstate(cpx1)
			    set cx2 $gstate(cpx2)
			    set cy1 [$sc axis invtransform BARY $sy1]
			    set cy2 [$sc axis invtransform BARY $sy2]
			}
		    }

		    #-- Constrain the zoom to the current plot area
		    if { $cx1 < $gstate(cpx1) } { set cx1 $gstate(cpx1) }
		    if { $cx2 > $gstate(cpx2) } { set cx2 $gstate(cpx2) }
		    if { $cy1 < $gstate(cpy1) } { set cy1 $gstate(cpy1) }
		    if { $cy2 > $gstate(cpy2) } { set cy2 $gstate(cpy2) }

		    #-- Increment the zoom level and update the plot axes
		    incr gstate(zoomLevel) 
            set stepsize [ $sc axis cget BARX -stepsize ]     
		    blt_graph::UpdateBarAxes $tkpath $stepsize $cx1 $cx2 $cy1 $cy2 $parent
		}

	    }

	    #-- Clear the action
	    set gstate(action) ""

	    #-- Pretend there is motion to here; that turns crosshairs on, etc.
	    blt_graph::BarMouseHandler $sc Motion $x $y

	}

	"B3Up" {

	    #-- Un-zoom (if zoomed)
	    if { $gstate(zoomLevel) > 0 } {
		#-- Retrieve the axis coords at the previous zoom level
		incr gstate(zoomLevel) -1
		set coords $gstate(axisLimits,$gstate(zoomLevel))
        set stepsize [ $sc axis cget BARX -stepsize ]
		eval UpdateBarAxes $tkpath $stepsize $coords $parent
	    }
	}
    
    }

    return
}

##*******************************************************
##
## Name: blt_graph::ResizeHandler
##
## Description:
## 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::ResizeHandler { sc } {

    #-- Get the current dimensions of the legend
    foreach { lx ly ldx ldy } [$sc extents legend] break
    if { $ldx == 0 || $ldy == 0 } { return }

    #-- Adjust legend dimensions to account for padding
    set pads [$sc legend cget -padx]
    incr lx -[lindex $pads 0]
    incr ldx [lindex $pads 0] ; incr ldx [lindex $pads 1]
    set pads [$sc legend cget -pady]
    incr ly -[lindex $pads 0]
    incr ldy [lindex $pads 0] ; incr ldy [lindex $pads 1]

    #-- Get the current dimensions of the plot area
    foreach { px py pdx pdy } [$sc extent plotarea] break

    #-- Make sure the legend remains within the plot area
    set move 0
    set newx $lx
    set newy $ly
    if { $lx+$ldx >= $px+$pdx-1 } {
	set move 1
	set newx [expr {$px+$pdx-$ldx-1}]
    }
    if { $ly+$ldy >= $py+$pdy-1 } {
	set move 1
	set newy [expr {$py+$pdy-$ldy-1}]
    }
    if { $move } {
	$sc legend config -position @$newx,$newy
    }

    return
}

##*******************************************************
##
## Name: blt_graph::PrintDialog
##
## Description:
## 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::PrintDialog { tkpath } {

    set file [tk_getSaveFile -parent $tkpath \
	    -defaultextension eps -initialfile graph.eps \
	    -filetypes [list {"Encapsulated PostScript" .eps} {All *}] \
	    -title "Save as encapsulated PostScript" ]
    set file [string trim $file]
    if { $file == "" } return

    set data [ $tkpath.sc postscript output ]

    #-- Make some tweaks to the PostScript output.  This takes A LOT of CPU
    #-- time, so it would be better to modify the BLT code to fix these
    #-- things.

    #-- Fix the cap style to be "projecting" rather than "butt", so that
    #-- there are nice square corners where line segments meet
    regsub -all {0 setlinecap} $data {2 setlinecap} data

    #-- Move the stuff which draws the "plotframe" marker to the end of the
    #-- file, so that it lies on top of the graph and forms a nice border
    regsub {^(.*)(% Marker "plotframe".*)(% Unset clipping.*)(showpage.*)$} \
	    $data {\1\3\2\4} data

    #-- Write the data to the file
    set fid [open $file w]
    puts $fid $data
    close $fid

    return
}

##*******************************************************
##
## Name: blt_graph::ShowHelp
##
## Description:
## 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc blt_graph::ShowHelp {} {

    set tl [toplevel .help]
    
    ;##- Create a frame with a text widget and a scrollbar
    frame $tl.msg
    text $tl.msg.text -width 76 -height 16 -wrap word \
	    -yscrollcommand "$tl.msg.yscroll set" -setgrid true -font normhelv
    bind $tl.msg.text <Button> "focus %W"
    scrollbar $tl.msg.yscroll -orient vertical \
	    -command "$tl.msg.text yview"

    ;##- Insert the message into the text widget
    $tl.msg.text insert end {
You can zoom in on a rectangular region, or zoom just one axis.  To zoom in on\
a rectangular region, move the pointer to one corner of the desired region and\
drag a rectangle with the left mouse button.  To zoom just one axis, move the\
pointer to the label/tick area for that axis (i.e. outside of the plot area)\
and drag a region with the left mouse button.  While zoomed, you can use the\
scrollbar to view a different part of the data.  To un-zoom, click the right\
mouse button.
	} {} {
You can drag the legend around, but it can be sluggish if there are a lot of\
data points, because the entire graph is redrawn frequently as it is dragged. \
I don't know if this slowness can be avoided without changing the BLT library\
code.  Maybe by making it a text marker instead of an official legend?
	} {} {
Encapsulated PostScript output is very slow due to tweaks made to the output\
in the Tcl script.  This slowness could be fixed by modifying the BLT code.
	}

    ;##- Disable modifications to the text widget
    $tl.msg.text config -state disabled

    ;##- Create a "Close" button
    button $tl.close -text "Close" -command "destroy $tl" -default active

    ;##- Bind the Return key to the Close button
    bind $tl <Return> "$tl.close invoke"

    ;##- Lay out the widgets
    pack $tl.close -side bottom
    grid $tl.msg.text $tl.msg.yscroll -sticky news
    grid rowconfigure $tl.msg 0 -weight 1
    grid columnconfigure $tl.msg 0 -weight 1
    pack $tl.msg -side top -fill both -expand true

    return
}

## ******************************************************** 
##
## Name: blt_graph::save
##
## Description: 
## save image to a file 
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## adapted from plot::save

proc blt_graph::save { tkpath } {
	
	if	{ [ catch { 
		set typelist {
			{"GIF IMage" {".gif"}}
		}
     	set fname [ tk_getSaveFile -filetypes $typelist -title "save graph" \
			-defaultextension .gif -initialdir [ pwd ] ]
		set parent [ winfo toplevel $tkpath ] 
		if	{ [ string length $fname ] } {
	 		set ext [ file extension $fname ]
			if	{ [ string length $ext ] } {
	 			regsub $ext$ $fname {.gif} fname
			} else {
				set fname ${fname}.gif
			}
			set dlg [ plot::saveHelp $parent ]
			tkwait window $dlg
            catch { unset ::$fname }
            ;## push this in bg so window can be refreshed
            blt::bgexec ::$fname $::plot::import $fname &
            raise $parent
            tkwait variable ::$fname
            catch { unset ::$fname }
			set ack [ tk_messageBox -type ok -default ok \
				-message "Your graph has been saved in $fname." \
				-icon info ]	
		}	
	} err ] } {
		set ack [ tk_messageBox -type ok -default ok \
				-message "Error saving graph: $err." \
				-icon error ]
	}
}

## ******************************************************** 
##
## Name: blt_graph::logAxis
##
## Description: 
## toggle axis between log and non log scale
##
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## Turn off cummulative plot

proc blt_graph::logXAxis { graph axis } {

	regexp {(.[^.]+).} $graph -> parent
    set state [ set ::${parent}(log$type) ] 
	if	{ [ string match X $type ] } {
		if 	{ $state } {
			$graph axis configure $axis -step 1.0 -subdivisions 1
		} else {
			$graph axis configure $axis -step [ set ::${parent}(x1step) ] \
				-subdivisions 1
		}
	}
    if  { $state } {
        set ::${parent}(cummPlot) 0
    }
	if	{ [ info exist ::${parent}(replot) ] } {
		eval [ set ::${parent}(replot) ]
	} else {
		if	{ [ set ::${parent}(log$type) ] } {
			$graph axis configure $axis -logscale 1 
            set ::${parent}(cummPlot) 0
		} 	else {
			$graph axis configure $axis -logscale 0
		}
	}
}

## ******************************************************** 
##
## Name: blt_graph::logYAxis
##
## Description: 
## toggle axis between log and non log scale
##
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## Turn off cummulative plot

proc blt_graph::logYAxis { graph axis } {

    set logoption [ $graph axis cget $axis -logscale ] 
    regexp {(.[^.]+).} $graph -> parent    
    if  { $logoption } {   
        $graph axis configure $axis -logscale 0 -autorange 0.0 -min 0
        foreach { ylimitmin ylimitmax } [ $graph axis limits BARY ] { break }
        if  { [ expr abs($ylimitmax - $ylimitmin) ] < 5  } {
            $graph axis configure BARY -min 0 -max 5
        }
    } else {    
        ;## turn off cummplot
        set cummPlot [ set ::${parent}(cummPlot) ]  
        if  { $cummPlot } {
            set ::${parent}(cummPlot) 0
            eval [ set ::${parent}(replot) ]
        }  
        $graph axis configure $axis -logscale 1 -autorange 0.0 -min {} -max {}
        foreach { ylimitmin ylimitmax } [ $graph axis limits $axis ] { break }
        if  { [ expr abs($ylimitmax - $ylimitmin) ] < 10  } {
            $graph axis configure $axis -min 1 -max 10
        }
    }
}

## ******************************************************** 
##
## Name: blt_graph::cummPlot
##
## Description: 
## cummulative plot
##
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## Turn off log plot

proc blt_graph::cummPlot { graph axis } {

    regexp {(.[^.]+).} $graph -> parent   
	if	{ [ info exist ::${parent}(replot) ] } {
        set cummPlot [ set ::${parent}(cummPlot) ]
        ;## turn off logY
        set logoption [ $graph axis cget $axis -logscale ] 
        
        regexp {(.[^.]+).} $graph -> parent    
        if  { $logoption && $cummPlot } {              
            $graph axis configure $axis -logscale 0 -autorange 0.0 -min 0
            set ::${parent}(logY) 0
        }
		eval [ set ::${parent}(replot) ]
	} 
}

## ******************************************************** 
##
## Name: blt_graph::refreshPlot
##
## Description: 
## cummulative plot
##
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## Turn off log and cum when refreshing for histograms
## also used by time graph

proc blt_graph::refreshPlot { graph } {
    regexp {(.[^.]+).} $graph -> parent 
    if  { [ info exist ::${parent}(cummPlot) ] } {
        set ::${parent}(cummPlot) 0
    }
	if	{ [ info exist ::${parent}(submit) ] } {
	    eval [ set ::${parent}(submit) ]
	} 
}

## ******************************************************** 
##
## Name: blt_graph::FormatLogTickLabels
##
## Description: 
## format the log 10 tick symbol
##
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:

proc blt_graph::FormatLogYTickLabels { graph value } {

	regexp {(.[^.]+).} $graph -> parent
    return [ format "%.f" [ expr round($value) ] ]
}

## ******************************************************** 
##
## Name: blt_graph::FormatLogXTickLabels
##
## Description: 
## format the log 10 tick symbol
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc blt_graph::FormatLogXTickLabels { graph value } {

	regexp {(.[^.]+).} $graph -> parent
	if	{ [ set ::${parent}(logX) ] }  {
		return "10^[ expr round($value) ]"
	} else {
		return $value
	}
}

## ******************************************************** 
##
## Name: blt_graph::FormatYtickLabel
##
## Description: 
## format the log 10 tick symbol
##
##
## Parameters:
## 
## Usage:
##
## Comments:
## currently does not work

proc blt_graph::FormatYTickLabel { graph value } {

    #-- Get access to the state array for this graph
    upvar #0 gstate[winfo parent $graph] gstate

	regexp {(.[^.]+).} $graph -> parent
    if  { [ info exist ::${parent}(formatyticks) ] } {
        set cmd "[ set ::${parent}(formatyticks) ] $value"
        return [ eval $cmd ]
    } else {
        return [ format $::YTICK_FMTSTR [format $gstate(Yformat) [ expr $value * $gstate(y1factor) ] ] ]
    }
}

## ******************************************************** 
##
## Name: bltgraph::print
##
## Description:
## set the indent spacing (in cm) for lists.
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## use for print of 1 graph, does not support queing of graph prints

proc blt_graph::print { tkpath } {

	 regexp {^.([^.]+).} $tkpath -> fname
	 set out $::TMPDIR/${fname}.ps
	 $tkpath postscript output $out -maxpect yes

	 regexp {(.[^.]+).} $tkpath -> parent

     set    textsw [ set ::${parent}(textw) ]
     set text [ $textsw get 1.0 end ]
     
     set txtfile $::TMPDIR/${fname}.txt
     set fd [ open $txtfile w ]
     puts $fd $text
     close $fd
     
     set orientation landscape
     set chars_per_line 132
     set site [ string trim $::cmonClient::var(site) ]
     
     set right_title "cmonClient $::version"
     set header ""
     set footer ""
     set title [ set ::${parent}(ptitle) ]
     set left_title ""
     
     #set cmd "exec echo \"$data\" \"$text\" | $printcmd &"

	 if	{ [ file exist $out ] } {
        if { [ string length [ auto_execok a2ps ] ] } {  
            catch { exec a2ps -1 --$orientation -l $chars_per_line --header=$header --center-title=$title \
            --left-title=$left_title --right-title=$right_title --footer=$footer --font-size=12 --sides=2  \
            $txtfile $out &
            } err1
        } else {
            set ack [ tk_messageBox -type ok -default ok \
				-message "Unable to use a2ps, line wrapping is not possible." \
				-icon info ]  
            catch { exec lpr ${fname}.txt ${fname}.ps &  } err1	
        }
		set err ""
        set bytes [ file size $out ]
        incr bytes [ string length $text ]
        set msg	"Printing $bytes bytes $err $err1"
        set msg [ string trim $msg ]
		set win $parent[clock seconds]
		set icon info
		set image [ Bitmap::get $icon ]
		if	{ ! [ winfo exist $win ] } {
			set dlg [ Dialog $win -parent . -modal none \
          		-separator 1 -title  $icon \
        		-side bottom -anchor  s -default 0 -image $image ]
        	$dlg add -name ok -anchor s -command [ list destroy $dlg ]
			set dlgframe [ $dlg getframe ]
			message $dlgframe.msg -aspect 300 -text $msg \
					-justify left -font $::MSGFONT
			pack $dlgframe.msg -side top 
			$dlg draw 
			after 10000 [ list destroy $dlg ]
		}
	 	after 10000 "catch { file delete $out $txtfile }"
	 }	 	
}

## ******************************************************** 
##
## Name: bltgraph::queuePrint
##
## Description:
## write graphs to file until end print button is pushed
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## queue up graphs for printing to save banners

proc blt_graph::queuePrint { tkpath } {

	if	{ [ catch {

	 	regexp {^.([^.]+).} $tkpath -> fname
		set out $::TMPDIR/${fname}.ps
	 	$tkpath postscript output $out -maxpect yes

	 	regexp {(.[^.]+).} $tkpath -> parent

     	set    textsw [ set ::${parent}(textw) ]
     	set text [ $textsw get 1.0 end ]
     
     	set txtfile $::TMPDIR/${fname}.txt
     	set fd [ open $txtfile w ]
     	puts $fd $text
     	close $fd
     
		;## save title file for display
     	set title [ set ::${parent}(ptitle) ]
        set titlefile $::TMPDIR/${fname}.title
        set fd [ open $titlefile w ]  
        puts -nonewline $fd $title 
        close $fd
        set ::POPUP_PRINT_QUEUE 1
        blt_graph::showPrintQueue
        
  	} err ] } {
    	set ack [ tk_messageBox -type ok -default ok \
			-message $err -icon info ]
    }  
}


## ******************************************************** 
##
## Name: bltgraph::printQueued 
##
## Description:
## write graphs to file until end print button is pushed
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## queue up graphs for printing to save banners

proc blt_graph::printQueued {} {

	if	{ [ catch {
        set orientation landscape
     	set chars_per_line 100
     	set site [ string trim $::cmonClient::var(site) ]
       
     	set right_title "cmonClient-$::version"
     	set header ""
     	set footer ""
                
     	set left_title ""
        ;## can only support one title
        
     	set title "${site}_Graphs"

     	#set cmd "exec echo \"$data\" \"$text\" | $printcmd &"
		set psfiles [ glob -nocomplain $::TMPDIR/*.ps ]
        set a2ps [ auto_execok a2ps ]
        if	{ [ string length $a2ps ] } {
        	set use_a2ps 1
        } else {
        	set use_a2ps 0
        }
        set printcmd [ list ]
        foreach out $psfiles {
                regexp {([^\.]+)} [ file tail $out ] -> fname 
                lappend printcmd $::TMPDIR/$fname.txt $out 
        } 
        ;## --output=<file> will output to file for postscript viewing
        if	{ $use_a2ps } {  
        	catch { eval exec a2ps -1 --$orientation -l $chars_per_line --header=$header --center-title=${title}\
            	--left-title=$left_title --right-title=${right_title} --footer=$footer --font-size=12 --sides=duplex  \
                $printcmd & } err1
        } else {
            set ack [ tk_messageBox -type ok -default ok \
				-message "Unable to use a2ps, line wrapping is not possible." \
				-icon info ]  
            catch { eval exec lpr $printcmd & } err1
        }
		set err ""
        set msg	"Printing all graphs queued $err1"
        set msg [ string trim $msg ]
		set win .tmpPrintQueued
		set icon info
		set image [ Bitmap::get $icon ]
		if	{ ! [ winfo exist $win ] } {
			set dlg [ Dialog $win -parent . -modal none \
          		-separator 1 -title  $icon \
        		-side bottom -anchor  s -default 0 -image $image ]
        	$dlg add -name ok -anchor s -command [ list destroy $dlg ]
			set dlgframe [ $dlg getframe ]
			message $dlgframe.msg -aspect 300 -text $msg \
					-justify left -font $::MSGFONT
			pack $dlgframe.msg -side top 
			$dlg draw 
			after 10000 [ list destroy $dlg ]
		}
		set delfiles "$printcmd [ glob -nocomplain $::TMPDIR/*.title ]"
	 	after 50000 "catch { eval file delete $delfiles }; blt_graph::showPrintQueue"
        
    } err ] } {
    	set ack [ tk_messageBox -type ok -default ok \
			-message $err -icon info ]
    }  	 	
} 

## ******************************************************** 
##
## Name: blt_graph::closeGraph
##
## Description: 
## cummulative plot
##
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
       
proc blt_graph::closeGraph { tkpath parent } {

	destroy [ winfo parent $tkpath ]
    catch { unset ::${parent} }
    catch { set ::POPUP_PRINT_QUEUE 0 }
}

## ******************************************************** 
##
## Name: blt_graph::showPrintQueue
##
## Description: 
## show the title of graphs queued for printing
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc blt_graph::showPrintQueue {} {
	
    set win .tmpPrintQueue
    if	{ [ info exist  ::POPUP_PRINT_QUEUE ] } {
    	if	{ ! $::POPUP_PRINT_QUEUE } {
        	if	{ [ winfo exist $win ] } {
        		catch { wm iconify $win }
            }
            return
        }
    }
	if	{ [ catch {
		set icon info
		set image [ Bitmap::get $icon ]
       	set site [ string trim $::cmonClient::var(site) ]
        
		if	{ ! [ winfo exist $win ] } {
        	toplevel $win -relief raised -borderwidth 2
            wm title $win "$site graphs queued for printing"
			wm protocol . WM_DELETE_WINDOW  "set ::POPUP_PRINT_QUEUE 0; wm iconify $win"
            
            set sw  [ ScrolledWindow $win.sw -auto both ]	
	
            set lbox [listbox $sw.lb -height 10 -width 60 \
        		-highlightthickness 1 -selectmode single \
        		-font $::LISTFONT -selectbackground yellow \
                -selectforeground black ]
            
            set ::${win}(lbox) $lbox
            $sw setwidget $lbox
    		pack $sw -side top -fill both -expand yes
            
            set f1 [ frame $win.faction ]
          	set but1 [ button $f1.print -text "PRINT" -width 10 \
        		-command  "blt_graph::printQueued"  ]
            set but2 [ button $f1.close -text "CLOSE" -width 10 \
        		-command  "set ::POPUP_PRINT_QUEUE 0; wm iconify $win"  ]   
            pack $but1 $but2 -side left -padx 5 -anchor s
            
            pack $f1 -side bottom -fill x -expand 1 -anchor s
        } 
        wm deiconify $win 
        set lbox [ set ::${win}(lbox) ]
        $lbox delete 0 end 
        set queued [ glob -nocomplain $::TMPDIR/*.title ]

        foreach file $queued {
            catch { exec cat $file} title
			$lbox insert end $title
		}
        raise $win 
        update idletasks
    } err ] } {
    	set ack [ tk_messageBox -type ok -default ok \
			-message $err -icon info ]
    }  	 	
}

## ******************************************************** 
##
## Name: blt_graph::showHistogram
##
## Description: 
## cummulative plot
##
##
## Parameters:
## c - frame of canvas
## Usage:
##
## Comments:
## Turn off log and cum when refreshing for histograms
## also used by time graph

proc blt_graph::showHistogram { parent } {

	if	{ [ info exist ::${parent}(replot) ] } {
	    eval [ set ::${parent}(replot) ]
	} 
}

## ******************************************************** 
##
## Name: blt_graph::convert2gps
##
## Description: 
## catch gps conversion errors converting local time
##
##
## Parameters:
## timestr, may contain spaces
## Usage:
##
## Comments:

proc blt_graph::convert2gps { timestr } {

    regsub -all {\s*/\s*} $timestr {/} timestr

    set rc [ catch { set gps [ utc2gps $timestr 0 ] } err ]
    
    if  { $rc } {
        return -1
    } else {
        return $gps
    }
}

#==============================================================================
# save this for standalone test
# e.g. readData ./data.recv 

proc readTestData { fname } {

	set fd [ open $fname r ]
	set data [ read $fd ]
	close $fd	
	blt_graph::showGraph $data

}

## ******************************************************** 
##
## Name: bltstats
##
## Description:
## adaption of stats for use with large numbers 
##
## Parameters:
## samples - list of numbers 
##
## Comments:

proc bltstats { samples } {
        set mean 0.0
        set S2   0.0
        set cov  0.0
        set N [ llength $samples ]
        set total 0.0
        if { $N == 1 } {
           return [ format "%.3f %.3f %.3f %.3f %3.2f" 1 $samples $samples $S2 $cov ]
        }
        
        ;## calculate the arithmetic mean
        set total 0.0
        foreach entry $samples {
            set total [ expr $total + $entry ]
        }
        set mean [ expr $total / $N ]
        
        ;## calculate the median
        set index [ expr ( $N + 1 ) / 2 ]
        set lsamples [ lsort -real $samples ]
        if  { [ expr $N % 2 ] } {
            set median [ lindex $lsamples $index ]
        } elseif { $N == 2 } {
            set median $mean
        } else {
            set before  [ lindex $lsamples [ expr $index - 1 ] ]
            set after   [ lindex $lsamples [ expr $index + 1 ] ]
            set median  [ expr ( $before + $after )/ 2 ]
        }
        ;## calculate the standard deviation
        foreach s $samples {
           set S2  [ expr { $S2+pow(($s-$mean),2) } ]
        }
        set S2     [ expr { $S2/($N-1) } ]
        set S      [ expr { sqrt($S2) } ]
        
        ;## calcualte the % coefficient of variation
        if { $mean != 0 } {
           set cov [ expr { ($S/$mean)*100 } ]
        }
        
        ;## return the values in a formatted list
        return [ format "%.3f %.3f %.3f %.3f %3.2f" $total $mean $median $S $cov ]
}
