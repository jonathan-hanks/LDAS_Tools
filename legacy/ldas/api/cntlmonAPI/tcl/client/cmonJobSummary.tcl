## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This package displays the directory tree 
## on the remote machine
## 
## cmonJobs version 1.0
##
## ******************************************************** 
;##barecode
package provide cmonJobSummary 1.0

namespace eval cmonJobSummary {
    variable count
    variable dblclick
    set pages JobSummary
	set page JobSummary
	set wviewFile .tmpTreeFile
	set pids [ list ]
}

#end

## ******************************************************** 
##
## Name: cmonJobSummary::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobSummary::create { notebook toppage } {

    set page [ lindex $::cmonJobSummary::pages 0 ] 
    set pagetext "Usage Details"
    $notebook insert end cmonJobSummary -text $pagetext \
        -createcmd "cmonJobSummary::createPages $notebook $page" \
        -raisecmd "cmonCommon::pageRaise cmonJobSummary $page \
        \{Job Summary by Command/User\} ; \
        cmonCommon::setLastTime cmonJobSummary $page"
}


## ******************************************************** 
##
## Name: cmonJobSummary::createPages 
##
## Description:
## creates Job listing page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobSummary::createPages { notebook page } {

    set frame [ $notebook getframe cmonJobSummary ] 
	set ::cmonJobSummary::topframe $frame
	set subf  [ frame $frame.f ]
	set titf2 [TitleFrame $subf.titf2 -text "Command status" -font $::ITALICFONT] 
	set subf1  [ $titf2 getframe ]
	set topf  [ frame $subf.topf ]
    set ::cmonJobSummary::topfw($page) $topf
	specific $topf $page
	;## create a status to display errors and status
	set statusw [ ::createStatus  $subf1 ]
    $statusw configure -height 10
	array set ::cmonJobSummary::statusw [ list $::cmonJobSummary::page $statusw ]
	pack $topf -side left -padx 2 -padx 4 
	pack $titf2 -side right -padx 2 -pady 1 -fill x -expand 1
	pack $subf -side top -fill x  -padx 5
		
	set pw1	  [PanedWindow $frame.pw1 -side top ]
    set pane  [$pw1 add -minsize 100 -weight 1]
    set title [TitleFrame $pane.lf -text "Remote Directory tree" -font $::ITALICFONT ]
	set ::cmonJobSummary::titf3($page) $title
	
    set sw    [ScrolledWindow [$title getframe].sw \
                  -relief sunken -borderwidth 2]
    set tree  [Tree $sw.tree \
                   -relief flat -borderwidth 0 -width 15 -highlightthickness 0\
		   -redraw 0 -dropenabled 1 -dragenabled 1 \
                   -dragevent 3 \
                   -droptypes {
                       TREE_NODE    {copy {} move {} link {}}
                       LISTBOX_ITEM {copy {} move {} link {}}
                   } \
                   -opencmd   "cmonJobSummary::moddir 1 $sw.tree" \
                   -closecmd  "cmonJobSummary::moddir 0 $sw.tree" \
				   -selectbackground yellow ]
    $sw setwidget $tree
	set ::cmonJobSummary::tree $tree
	
    pack $sw    -side top  -expand yes -fill both
    pack $title -fill both -expand yes -padx 2 -pady 2

    set pane [$pw1 add -minsize 100 -weight 2]
    set lf   [TitleFrame $pane.lf -text "Directory Content" -font $::ITALICFONT ]
    set sw   [ScrolledWindow [$lf getframe].sw \
                  -scrollbar horizontal -auto none -relief sunken -borderwidth 2]
    set list [ListBox::create $sw.lb \
                  -relief flat -borderwidth 0 \
                  -dragevent 3 \
                  -dropenabled 1 -dragenabled 1 \
                  -width 20 -highlightthickness 0 -multicolumn true \
                  -redraw 0 -dragenabled 1 \
                  -droptypes {
                      TREE_NODE    {copy {} move {} link {}}
                      LISTBOX_ITEM {copy {} move {} link {}}} \
				  -selectbackground yellow ]
    $sw setwidget $list
	set ::cmonJobSummary::list $list
    pack $sw $lf -fill both -expand yes
	pack $pw1 -side top -fill both -expand yes 

    $tree bindText  <ButtonPress-1>        "cmonJobSummary::select tree 1 $tree $list"
    # $tree bindText  <Double-ButtonPress-1> "cmonJobSummary::select tree 2 $tree $list"
    $list bindText  <ButtonPress-1>        "cmonJobSummary::select list 1 $tree $list"
    #$list bindText  <Double-ButtonPress-1> "cmonJobSummary::select list 2 $tree $list"
    #$list bindImage <Double-ButtonPress-1> "cmonJobSummary::select list 2 $tree $list"

	array set ::cmonJobSummary::state [ list $::cmonJobSummary::page 0 ]
    set ::cmonJobSummary::prevsite($page) $::cmonClient::var(site)
    
	set ::cmonJobSummary::origCursor [ $frame cget -cursor ]
    set ::cmonJobSummary::failedimage [ Bitmap::get $::GIFDIR/ball_red.gif ]
    set ::cmonJobSummary::passedimage [ Bitmap::get $::GIFDIR/ball_green.gif ]
    set ::cmonJobSummary::rejectedimage [ Bitmap::get $::GIFDIR/ball_purple.gif ]
    set ::cmonJobSummary::image_openfold [ Bitmap::get openfold ]
	set ::cmonJobSummary::image_folder [Bitmap::get folder]
    set ::cmonJobSummary::image_file [Bitmap::get file]

}

## ******************************************************** 
##
## Name: cmonJobSummary::sendRequest
##
## Description:
## creates initialize the directory treze 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobSummary::sendRequest { { tree "" } { list "" } args } {
    global   tcl_platform

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonJobSummary::statusw($page) "Please select an LDAS site"
		return 
	}
    
    set login ""
	set passwd ""
    set cmdId "new"   
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0
    if  { [ catch {
        foreach { login passwd } [ validateLogin 1 ] { break }
    } err ] } {
		appendStatus $::cmonJobSummary::statusw($page) $err
		return        
	}
    if  { [ string equal cancelled $login ] } {
		return
	}
    
	if	{ ! [ string length $tree ] } {
		set tree $::cmonJobSummary::tree
		set list $::cmonJobSummary::list
	}
    set page $::cmonJobSummary::page
	if	{ [ catch {
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
		
        set cmd "cmonJobSummary::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::jobSummary $starttime-$endtime"
        sendCmd $cmd 
        cmonJobSummary::state1 $page        
	} err ] } {
		appendStatus $::cmonJobSummary::statusw($page) $err
        cmonJobSummary::state0 $page
	}
}

## ******************************************************** 
##
## Name: cmonJobSummary::specific 
##
## Description:
## creates Refresh button to get dirs 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobSummary::specific { parent page } {

	set titf1 [TitleFrame $parent.titf1 -text \
	"View Job Summary" -font $::ITALICFONT ]
	set subf1  [$titf1 getframe]
    
	set f2 [ frame $subf1.ftime -relief groove -borderwidth 2 ]
	set f [ cmonCommon::createTimeOptions $f2 cmonJobSummary $page ]	
	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $f2 cmonJobSummary $page ] { break }
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x -expand 1
	
	pack $f2 -side top -anchor w -expand 1 -fill both
	set but1 [ Button $subf1.bstart -text SUBMIT -helptext "job summary by user/command" \
        -command  "cmonJobSummary::sendRequest" -padx 10 -pady 2  ]
    set ::cmonJobSummary::bstart($page) $but1
	pack $but1 -side top -padx 10 -pady 10 -anchor s
	pack $titf1 -fill both -expand 1
}

## ******************************************************** 
##
## Name: cmonJobSummary::getFileContents
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonJobSummary::getFileContents { page path } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonJobSummary::statusw($::cmonJobSummary::page) "Please select an LDAS site"
		return -code error 
	}
	if	{ $::cmonJobSummary::state($page) } {
		appendStatus $::cmonJobSummary::statusw($::cmonJobSummary::page) \
		"Retrieving file data, please wait" 0 blue
		return 
	}

	cmonJobSummary::state1 $page 
    set client $::cmonClient::client
    set cmdId "new"
	set freq 0
	set repeat 0
	if	{ [ catch {
        set cmd "cmonJobSummary::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::remoteBrowser $path" 
		# puts "cmd=$cmd"
		sendCmd $cmd                 
	} err ] } {
        cmonJobSummary::state0 $page 
		appendStatus $::cmonJobSummary::statusw($::cmonJobSummary::page) $err
	}
	vwait ::cmonJobSummary::reply
}

## ******************************************************** 
##
## Name: cmonJobSummary::showReply
##
## Description:
## display results from server 
##
## Parameters:
## parent widget 
## rc 	-	return code from cmd to server
## clientCmd -	client name and cmd Id
## html	-	text returned from cmd
##
## Usage:
##  called a button command 
## Comments:
## this proc name is generic for all packages.

proc cmonJobSummary::showReply { page rc clientCmd html } {  

	if	{ [ catch {
    	set clientCmd [ split $clientCmd : ]
    	set client [ lindex $clientCmd 0 ]
    	set cmdId [ lindex $clientCmd 1 ]
    	set afterid [ lindex $clientCmd 2 ]
    	#puts "clientCmd=$clientCmd"
    	#puts "showReply, $page, client=$client,cmdId=$cmdId, rc=$rc,afterid=$afterid"
		#puts "html=$html"
		set ::cmonJobSummary::cmdId($page) $cmdId 
    	set ::cmonClient::client $client
		switch $rc {
		0 {           
            cmonJobSummary::getdir home $html
	  	}
		3 {
			appendStatus $::cmonJobSummary::statusw($page) $html	
	  	}
		}
	} err ] } {
		puts $err
	}
    cmonJobSummary::state0 $page
	
}

## ******************************************************** 
##
## Name: cmonJobSummary::getdir 
##
## Description:
## display new directories on nodes
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:

proc cmonJobSummary::getdir { node lentries } {

    if  { [ catch {
		set tree $::cmonJobSummary::tree
		set list $::cmonJobSummary::list
        
        catch { $tree delete [ $tree nodes $node ] }
        catch { $tree delete $node }        
        catch { unset ::cmonJobSummary::lfiles }
        set count 0
	    set image_openfold $::cmonJobSummary::image_openfold
	    set image_folder   $::cmonJobSummary::image_folder
        
        set rootdir [ lindex $lentries 0 ]
        regsub -all {\"} $rootdir {} rootdir
	    $tree insert end root home -text $rootdir -data $rootdir -open 1 \
            -image $image_folder
	    set parentNode($rootdir) home
    
        set cmddir [ lindex $lentries 1 ]
        set userdir [ lindex $lentries 2 ]
	    $tree insert end home n:0 -text "By [ file tail $cmddir ]" \
            -data $cmddir -open 0 -image $image_folder
        regexp {([^:]+):} $cmddir -> cmddir
        set parentNode($cmddir) n:0
        $tree insert end home n:1 -text "By [ file tail $userdir ]" -data $userdir -open 0 \
            -image $image_folder    
        regexp {([^:]+):(\S+)} $userdir -> userdir cnts
        set parentNode($userdir) n:1
        set top home
    
	    foreach entry [ lrange $lentries 3 end ] {
            regexp {([^:]+):} $entry -> fname
		    set dir [ file dirname $fname ]
            set tail [ file tail $fname ]
            set entrytail [ file tail $entry ]
        
        ;## if this dir already exist , just append file info
        ;## else insert the node

            if	{ [ info exist parentNode($dir) ] } {
			    set parent $parentNode($dir)
                lappend ::cmonJobSummary::lfiles($parent) $entrytail
	        } else {
                set parent ""
                set parentdir $dir
                while { ! [ string equal $top $parent ] } {
		            if	{ [ info exist parentNode($parentdir) ] } {
			            set parent $parentNode($parentdir)
                        set found 1
                        break
		            } else {
                        set parentdir [ file dirname $parentdir ]
                        if  { [ string equal $parentdir / ] } {
                            set parent $top
                        }
                    }
                }
		        set nodeName $parent:$count
                set image $image_folder
		        $tree insert end $parent $nodeName \
                    -text      [ file tail $dir ] \
                    -image     $image \
                    -drawcross allways \
                    -data      $entry -open 0
		        set parentNode($dir) $nodeName
                lappend ::cmonJobSummary::lfiles($nodeName) $entrytail
                incr count
            } 
	    }	
        $list delete [$list item 0 end]
        cmonJobSummary::select tree 1 $tree $list home  
        $tree itemconfigure home -image $image_openfold -open 1
		$tree selection set home
    	$tree configure -redraw 1
    	$list configure -redraw 1
    } err ] } {
        appendStatus $::cmonJobSummary::statusw($cmonJobSummary::page) "Error: $err"
    }
}

## ******************************************************** 
##
## Name: cmonJobSummary::moddir 
##
## Description:
## selected a new directory, display new leaves
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## must transition back to state0 after calling sendRequest

proc cmonJobSummary::moddir { idx tree node } {

	if	{ $::cmonJobSummary::state($::cmonJobSummary::page) } {
		return
	}
    if  { $idx } {
        set parent [$tree parent $node]
		while { ! [ string equal $parent root ] } {
        # puts "moddir opening parent $parent [ $tree itemcget $parent -text]"
	    $tree itemconfigure $parent -image $::cmonJobSummary::image_openfold
	    set parent [$tree parent $parent]
        }  
    }      
    if { $idx && [$tree itemcget $node -drawcross] == "allways" } {
		select_node $tree $::cmonJobSummary::list $node
        if { [llength [$tree nodes $node]] } {
            $tree itemconfigure $node -image $::cmonJobSummary::image_openfold
        } else {
            $tree itemconfigure $node -image $::cmonJobSummary::image_folder
        }
        
    } else {
        $tree itemconfigure $node -image [Bitmap::get [lindex {folder openfold} $idx]]
    }	
}

## ******************************************************** 
##
## Name: cmonJobSummary::select 
##
## Description:
## handle mouse selection
##
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonJobSummary::select { where num tree list node } {

	if	{ $::cmonJobSummary::state($::cmonJobSummary::page) } {
		return
	}
    set dblclick 1
	update idletasks
	if	{ [ catch { 
    if { $num == 1 } {
        if { $where == "tree" && [lsearch [$tree selection get] $node] != -1 } {
            unset dblclick
        } elseif { $where == "list" && [lsearch [$list selection get] $node] != -1 } {
            unset dblclick
        } elseif { $where == "tree" } {
		    select_node $tree $list $node
        } else {
            $list selection set $node
        }
    } elseif { $where == "list" && [$tree exists $node] } {
		set parent [$tree parent $node]
		while { ! [ string equal $parent root ] } {
	    $tree itemconfigure $parent -open 1
	    set parent [$tree parent $parent]
		select_node $tree $list $node 
		} 
    } elseif { $num == 2 } {
        ;## do nothing
        # puts "double click function not supported yet"
	} 
	} err ] } {
		appendStatus $::cmonJobSummary::statusw($::cmonJobSummary::page) "select error: $err"
		cmonJobSummary::state0 $::cmonJobSummary::page
	}
}

## ******************************************************** 
##
## Name: cmonJobSummary::select_node 
##
## Description:
## handle selection of a node
##
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonJobSummary::select_node { tree list node } {

	if	{ $::cmonJobSummary::state($::cmonJobSummary::page) } {
		return
	}
	# catch { $tree itemconfigure $::cmonJobSummary::node_selected -image [Bitmap::get folder] }
    $tree selection set $node
    update
    eval $list delete [$list item 0 end]
	set dir [$tree itemcget $node -data]
    
	;## if this is a directory
    if { [$tree itemcget $node -drawcross] == "allways" } {
		$tree itemconfigure $node -image [Bitmap::get openfold]
    } 
    set folderimage $::cmonJobSummary::image_folder
    set fileimage   $::cmonJobSummary::image_file
    
    ;## do not insert folders 
    ;## foreach subnode [$tree nodes $node] {
    ;##    $list insert end $subnode \
    ;##        -text  [$tree itemcget $subnode -text] \
    ;##        -image $folderimage
    ;##}
    
    set num 0

	if	{ [ info exist ::cmonJobSummary::lfiles($node) ] } {
        set files [ lsort  [ set ::cmonJobSummary::lfiles($node) ] ]
    	foreach f $::cmonJobSummary::lfiles($node) {
            regexp {([^\:]+):} $f -> type
            if  { [ regexp {passed|failed|rejected} $type ] } {
                set image [ set ::cmonJobSummary::${type}image ]
            } else {
                set image $fileimage
            }
        	$list insert end f:$num \
            	-text  $f \
            	-image $image
        	incr num
		}
    }
}

## ******************************************************** 
##
## Name: cmonJobSummary::expand 
##
## Description:
## handle expansion of a node when selected 
##
## Parameters:
## page name
##
## Usage:
##  
## 
## Comments:

proc cmonJobSummary::expand { tree but } {
    if { [set cur [$tree selection get]] != "" } {
        if { $but == 0 } {
            $tree opentree $cur
        } else {
            $tree closetree $cur
        }
    }
}

#******************************************************** 
##
## Name: cmonJobSummary::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonJobSummary::state0 { page } {
    updateWidgets $::cmonJobSummary::topfw($page) normal
	$::cmonJobSummary::bstart($page) configure -state normal
	array set ::cmonJobSummary::state [ list $page 0 ]
	$::cmonJobSummary::topframe configure -cursor $::cmonJobSummary::origCursor
}


#******************************************************** 
##
## Name: cmonJobSummary::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonJobSummary::state1 { page } {
    updateWidgets $::cmonJobSummary::topfw($page) disabled    	
	array set ::cmonJobSummary::state [ list $page 1 ]
    $::cmonJobSummary::bstart($page) configure -state disabled
	appendStatus $::cmonJobSummary::statusw($::cmonJobSummary::page) \
	"Sending request, please wait..." 0 blue
	$::cmonJobSummary::topframe configure -cursor watch 
}

## ******************************************************** 
##
## Name: cmonJobSummary::reset  
##
## Description:
## update title to reflect the site
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonJobSummary::reset { page } {

    set tree [ set ::cmonJobSummary::tree ]
    catch { $tree delete [ $tree nodes home ] }
    catch { $tree delete home }        
    catch { unset ::cmonJobSummary::lfiles }
    $::cmonJobSummary::list delete [ $::cmonJobSummary::list item 0 end ]

}
