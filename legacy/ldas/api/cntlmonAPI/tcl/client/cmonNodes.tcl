## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script displays the load and cpu usage information:
## on all the Bwatch nodes.
## 
## ******************************************************** 

package provide cmonNodes 1.0

namespace eval cmonNodes {
    set notebook ""
    set pages  {Node-Balance}
    set pagetext "Node Balance"
	set fmtstr "%-10s |%10s |%10s |%40s | %-40s"
	set color(delete) red
	set color(undo) black
	set wLogin .cmonNodes
    set wMsg .tmpLogs
    set terms [ list ]
    set activeNParent ""
    set activePage cmonNodes
}

## ******************************************************** 
##
## Name: cmonNodes::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodes::create { notebook } {
    set page [ lindex $::cmonNodes::pages 0 ] 
    $notebook insert end cmonNodes -text Beowulf \
        -raisecmd "cmonNodes::pageRaise $page"
	cmonNodes::createPages $notebook $page
    cmonNodes::pageRaise $page
}

## ******************************************************** 
##
## Name: cmonNodes::create 
##
## Description:
## creates Bwatch page
##
## Parameters:
## notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodes::createPages { notebook0 page } {

	set frame [ $notebook0 getframe cmonNodes ]	
	set ::cmonNodes::notebook [ NoteBook $frame.nb \
		-foreground white -background $::darkpurple -bd 2 \
		-activeforeground white -activebackground blue ]
	set notebook $::cmonNodes::notebook
	
	set frame [$notebook insert end $page -text "Node Balance" \
        -raisecmd [ list cmonNodes::pageRaise $page ] ]
    set topf  [frame $frame.topf]
	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	
	set ::cmonNodes::notebook $notebook
   
    specific $topf $page
	set ::cmonNodes::topfw($page) $topf
	pack $topf -fill x -pady 2
	
    ;## create a status to display errors and status
	set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonNodes::statusw [ list $page $statusw ]
	pack $titf2 -pady 2 -padx 2 -fill both -expand yes 

	set pw31   [ PanedWindow $pane3.pw1 -side top ]
    set pane32  [ $pw31 add -minsize 200 ]
    set pane33  [ $pw31 add -minsize 50 ]
	
	set titf3 [TitleFrame $pane32.titf3 -text "Delete Active Nodes" -font $::ITALICFONT ]
	set titf4 [TitleFrame $pane33.titf4 -text "Add Nodes ( enter node names )" -font $::ITALICFONT ]
  
	;## create 2 scrolled windows, one for active nodes, one for adding nodes
	nodeList [ $titf3 getframe ] [ $titf4 getframe ] $page
    pack $titf3 $titf4 -side left -pady 2 -padx 4 -fill both -expand yes 
	set ::cmonNodes::titf3($page) $titf3 
	set ::cmonNodes::titf4($page) $titf4 
    cmonNodes::pageUpdate $page
    
	pack $pw31 -side top -fill both -expand 1
 	pack $pw1 -side top -fill both -expand 1 -anchor n

    array set ::cmonNodes::state [ list $page 0 ]
    set ::cmonNodes::prevsite($page) $::cmonClient::var(site)

	;## create other pages	
	cmonBeowulfUsers::create $notebook
    cmonNodesUsers::create $notebook
	cmonBwatch::create $notebook	
    
    $notebook compute_size
    pack $notebook -fill both -expand yes -padx 4 -pady 4
	$notebook raise [ $notebook page 0 ]
}

## ******************************************************** 
##
## Name: cmonNodes::specific
##
## Description:
## creates specific action area for the page types
##
## Parameters:
## page
## parent widget  
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodes::specific { parent page } {

	set labfent [LabelFrame $parent.fent -text "Add new nodes or delete active ones" \
		-side top -anchor w \
        -relief sunken -borderwidth 4]
    set subf1 [ $labfent getframe ]
	label $subf1.lbl -text " The nodes are listed in alphabetical order.\n\
	Mark nodes to delete, input new ones, hit SUBMIT to commit changes." \
		-justify left -wraplength 500 -fg blue -font $::LISTFONT 
	pack $subf1.lbl -side top -padx 2 -anchor w 
	
    set fadd [ frame $subf1.fadd -relief groove -borderwidth 2 ]  
	set but1 [ Button $fadd.addrange -text "Add" -width 6 \
        -command  "cmonNodes::addNodeRanges $page" -fg purple \
		-helptext "add a range of nodes" -state disabled ]
    set but3 [ Button $fadd.clearadd -text "Undo" -width 6 \
        -command  "cmonNodes::clearAddNodes $page" -fg purple \
		-helptext "clear all entries in add node list"	-state disabled ]
    set but4 [ Button $fadd.sortadd -text "Sort" -width 6 \
        -command  "cmonNodes::sortAddNodes $page 1" -fg purple \
		-helptext "sort nodes in dictionary order"	-state disabled ]
    set lbl1 [ label $fadd.lbl1 -text "Node suffix:" -width 15 ] 
    
    set ent1 [ Entry $fadd.addsuffix -textvariable ::cmonNodes::var($page,addsuffix) -editable 1 -width 10 \
       	-helptext "suffix for node name" ]
    set spin1  [SpinBox $fadd.addspin1 -range {0 500 1} -textvariable ::cmonNodes::addfrom($page) \
                   -helptext "start node number of add range" -width 5 ]
    set lbl2 [ label $fadd.lbl2 -text "--" -font $::MSGFONT ]
    set spin2  [SpinBox $fadd.addspin2 -range {0 500 1} -textvariable ::cmonNodes::addto($page) \
                   -helptext "end node number of add range" -width 5 ]
               
    $spin1 bind <Leave>  "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::addfrom($page) 0 $::MAXNODESADD"
    $spin1 bind <Return> "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::addfrom($page) 0 $::MAXNODESADD"  
    $spin2 bind <Leave>  "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::addto($page)   0 $::MAXNODESADD"
    $spin2 bind <Return> "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::addto($page)   0 $::MAXNODESADD"

    set chk1 [ checkbutton $fadd.ck1 -text "Single node system" \
		-variable ::cmonNodes::var($page,singlebox) -onvalue 1 -offvalue 0 ]
             
    pack $lbl1 $ent1 $spin1 $lbl2 $spin2 $chk1 -side left -padx 1 -pady 1  
    pack $but4 $but3 $but1 -side right -padx 1 -pady 1       
	pack $fadd -side top -fill x
    set ::cmonNodes::addfrom($page) 1
    set ::cmonNodes::addto($page) $::MAXNODESADD
    set ::cmonNodes::cksingleW($page) $chk1
    set ::cmonNodes::nodesuffixW($page) $ent1
    
    set fdel [ frame $subf1.fdel -relief groove -borderwidth 2 ]  
    set lbl1 [ label $fdel.lbl1 -text "Node Range:" -width 15 ]
	set but1 [ Button $fdel.addrange -text "Delete " -width 6 \
        -command  "cmonNodes::delNodeRanges $page" -fg white -bg red \
		-helptext "delete a range of nodes" -state disabled ]
    set spin1  [SpinBox $fdel.delspin1 -range {0 500 1} -textvariable ::cmonNodes::delfrom($page) \
                   -helptext "start node number of add range" -width 5 ]
    set lbl2 [ label $fdel.lbl2 -text "--" ]
    set spin2  [SpinBox $fdel.delspin2 -range {0 500 1} -textvariable ::cmonNodes::delto($page) \
                   -helptext "end node number of add range" -width 5 ] 
                   
    $spin1 bind <Leave>  "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::delfrom($page) 0 $::MAXNODESADD"
    $spin1 bind <Return> "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::delfrom($page) 0 $::MAXNODESADD"  
    $spin2 bind <Leave>  "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::delto($page)   0 $::MAXNODESADD"
    $spin2 bind <Return> "cmonCommon::histSpinNew cmonNodes $page ::cmonNodes::delto($page)   0 $::MAXNODESADD"
    
    set but2 [ Button $fdel.clearactive -text "Undo" -width 6 \
        -command  "cmonNodes::resetActiveNodes $page" -fg purple \
		-helptext "undo all deletes" ] 
    set but4 [ button $fdel.sortdel -text "" -width 6 \
		-relief flat ]           
    pack $lbl1 $spin1 $lbl2 $spin2 -side left -padx 1 -pady 1  
    pack $but4 $but2 $but1 -side right -padx 1 -pady 1             
	pack $fdel -side top -fill x	
    
    frame $subf1.freason -relief ridge -borderwidth 2
    set comment_ent [ cmonCommon::createRecordWidget $subf1.freason cmonNodes $page ]
	
	pack $comment_ent -side top -anchor w -fill x
	pack $subf1.freason -side top -padx 2 -pady 2 -fill x
	
	set f1 [ frame $subf1.f1 ]
	Button $f1.start -text SUBMIT -width 10 \
        -command  "cmonNodes::sendRequest $page nodeChgs" \
		-helptext "update node list"
	Button $f1.undo -text "UNDO Last Change" -width 15 \
        -command  "cmonNodes::sendRequest $page undo" \
		-helptext "undo the last change" -state disabled
	set ::cmonNodes::bstart($page) $f1.start	
	set ::cmonNodes::bundo($page) $f1.undo	
	pack $f1.start $f1.undo -side left -padx 5 -pady 5
	pack $f1 -side bottom -expand 1 -fill x 
	
	pack $labfent -side left -padx 4 -fill both -expand 1
	;## put a trace in variable each time beowulf nodes are updated
	trace variable ::beowulfNodes w "::cmonNodes::traceUpdate $page"
}

## ******************************************************** 
##
## Name: cmonNodes::stopToggle
##
## Description:
## disallows checkbutton to be toggled 
##
## Parameters:
## page
## parent widget  
##
## Usage:
##  
## 
## Comments:
##

proc cmonNodes::stopToggle { widget setvalue } {

    if  { $setvalue } {
        $widget select
    } else {
        $widget deselect 
    }
}

## ******************************************************** 
##
## Name: cmonNodes::nodeList
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the list of nodes and their usage
## 
## Comments:

proc cmonNodes::nodeList { parent1 parent2 page } {

    set sw1  [ ScrolledWindow $parent1.sw1 ]	
	set sw2  [ ScrolledWindow $parent2.sw2 ]	
	
	set sf1 [ ScrollableFrame $sw1.f -height 100 -width 300 ]
	set sf2 [ ScrollableFrame $sw2.f -height 100 -width 200 ]
	$sw1 setwidget $sf1
	$sw2 setwidget $sf2 
    pack $sw1 $sw2 -side left -fill both -expand yes
	
	set subf1 [ $sf1 getframe]
	set subfh [ frame $subf1.fhdr -relief sunken -borderwidth 2 ]
	
	set ::cmonNodes::activeNparent($page) $subfh
	dispActiveNodes $page 
	
	set subf2 [ $sf2 getframe]
	dispAddNodes $page $subf2 
	
}

## ******************************************************** 
##
## Name: cmonNodes::dispActiveNodes
##
## Description:
## extract configurable resources from rsc file
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## also display vars that cannot be changed

proc cmonNodes::dispActiveNodes { page } {

	if	{ [ catch {

	;## create a header 
	set parent [ set ::cmonNodes::activeNparent($page) ]
	;## must destroy widget in parseRsc or globalVar array cannot be unset
	if	{ [ winfo exist $parent ] } {
        set grandparent [ winfo parent $parent ]
		destroy $parent
        set ::cmonNodes::delframes($page) [ list ]
	}
	set subf1 [ frame $grandparent.fhdr -relief sunken -borderwidth 2 ]

	set color_global blue
	set color_api #127818
	set site [ string trim $::cmonClient::var(site) ]
	set i 0
	set varlist [ list ]
	;## compile node list var names in one row 
	for { set index 0 } { $index < $::MAXNODESROW } { incr index 1 } {
		lappend varlist node$index
	} 

    ;## do not set ::beowulfNodes($site) or it would trigger a traceUpdate

    set ::cmonNodes::activeNodes($page) [ lsort -dictionary $::beowulfNodes($site) ]
    set nodelist $::cmonNodes::activeNodes($page)

	foreach $varlist $nodelist  {
		if	{ [ string length $node0 ] } {
			set fnodes [ frame $subf1.fnodes$i -relief flat -borderwidth 2 ]
			for { set j 0 } { $j < $::MAXNODESROW } { incr j 1 } {
				if	{ [ string length [ set node$j ] ] } {
					set nodenum [ expr $i + $j ]
					set nodename [ set node$j ] 
					set mb [ menubutton $fnodes.node$nodenum -text $nodename \
					-relief raised -borderwidth 2 -activeforeground brown \
					-menu $fnodes.node${nodenum}.menuDB -width 12 \
					-font $::LISTFONT -bg PapayaWhip ]
		            bind $mb <Enter> "cmonNodes::showNodeOrder $mb $nodenum %x %y"
                    bind $mb <Leave> "cmonNodes::popdownNodeOrder $mb"
                    
					set m1delete [ menu $mb.menuDB -tearoff 0 ]
					if	{ ! [ string match none $nodename ] } {
						$m1delete add check -label delete \
						-variable ::cmonNodes::delete($page,$nodenum) \
						-offvalue -0 -onvalue 1 \
						-command [ list cmonNodes::deleteNode $m1delete $mb 0 $page ]
						$m1delete add separator
						$m1delete add command -label login \
						-command [ list cmonNodes::loginScrn $m1delete $mb 0 $page ]
						pack $mb -side left -fill both -anchor w -padx 2 -pady 1
					} else {					
						pack $mb -side left -fill both -anchor w -padx 2 -pady 1
						$mb configure -state disabled
					}
				}
			}
			pack $fnodes -side top -fill both -anchor w
			lappend ::cmonNodes::delframes($page) $fnodes
		}
		incr i $::MAXNODESROW
	}

	pack $subf1 -side top -fill both -expand 1
	
    ;## verify a single box system: all nodes have same name
    # set firstnode [ lindex $nodelist 0 ]
    # regsub -all $firstnode $nodelist "" tmp
    
    if  { [ llength [ lsort -unique $nodelist ] ] == 1 } {
        set ::cmonNodes::var($page,singlebox) 1
        set ::cmonNodes::var($page,addsuffix) [ lindex $nodelist 0 ]
        $::cmonNodes::nodesuffixW($page) configure -editable 0
    } else {
        set ::cmonNodes::var($page,singlebox) 0
        regexp {([^\d]+)} [ lindex $nodelist 0 ] -> ::cmonNodes::var($page,addsuffix) 
        $::cmonNodes::nodesuffixW($page) configure -editable 1
    }
    $cmonNodes::cksingleW($page) configure -command "cmonNodes::stopToggle $cmonNodes::cksingleW($page) \
        $::cmonNodes::var($page,singlebox)"
	} err ] } {
		appendStatus $::cmonNodes::statusw($page) "[myName] $err"
	}
   	updateWidgets $::cmonNodes::topfw($page) normal
}
	
## ******************************************************** 
##
## Name: cmonNodes::dispAddNodes
##
## Description:
## entries to add nodes
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## also display vars that cannot be changed

proc cmonNodes::dispAddNodes { page parent } {

	if	{ [ catch {
	set subf1 [ frame $parent.fhdr -relief sunken -borderwidth 2 ]
	;## create a header 
	# set parent $::cmonNodes::varFrameParent($page) 
	
	#set fg_global #127818 (green)
	set color_global blue
	set color_api #127818

	set nodesRow $::MAXNODESROW
	for { set i 0 } { $i < $::::MAXNODESADD } { incr i $nodesRow } {
		set entries [ list ]
		set fnodes [ frame $subf1.fnodes$i -relief flat -borderwidth 2 ]
		for { set j 0 } { $j < $nodesRow } { incr j 1 } {
			set num [ expr $i + $j ]
			set ent [ entry $fnodes.ent$num -justify left -textvariable ::cmonNodes::addNodes($page,$num) \
			-fg blue -font $::LISTFONT -width 15 -relief sunken -bg white ]
			lappend entries $ent
		    bind $ent <Enter> "cmonNodes::showNodeOrder $ent $num %x %y"
            bind $ent <Leave> "cmonNodes::popdownNodeOrder $ent"            
		}
		eval pack $entries -side left -fill both -expand 1 -pady 2
		pack $fnodes -side top -fill both -anchor w
	}
	pack $subf1 -side top -fill both -expand 1

	} err ] } {
		appendStatus $::cmonNodes::statusw($page) "[myName] $err"
	}
}
		
## ******************************************************** 
##
## Name: cmonNodes::sendRequest 
##
## Description:
## send request 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodes::sendRequest { page type } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonNodes::statusw($page) "Please select an LDAS site" 
		return 
	}
	
	if	{ ! [ string length $::cmonNodes::var($page,comment) ] } {
	    appendStatus $::cmonNodes::statusw($page) "Please supply a reason for your action"
		return
    }
        
	if	{ [ string match undo $type ] } {
		set servercmd "cntlmon::undoNodeChgs"
	} else {
        array set ::cmonNodes::state [ list $page 0 ]
        if  { [ catch {
		    foreach { delnodes addnodes } [ cmonNodes::nodeChgs $page ] { break }
        } err ] } {
            appendStatus $::cmonNodes::statusw($page) $err
            return
        }
		set numDel [ llength $delnodes ]
		set numAdd [ llength $addnodes ]
		if	{ ! $numDel && ! $numAdd } {
			appendStatus $::cmonNodes::statusw($page) "No changes have been made." 
			return
		} 
		set action [ list ]
		if	{ $numDel } {
			lappend action "DELETE NODES:\n$delnodes"
		} 
		if	{ $numAdd } { 
			lappend action "ADD NODES\n$addnodes"
			set undomsg "\nAfter SUBMIT, you can hit 'Undo Last Change' to undo addition of nodes"
		} else {
			set undomsg "\nAfter SUBMIT, you can hit 'Undo Last Change' to undo deletion of nodes"
		}
		set msg "Your request is:\n[ join $action \n\n ]\n$undomsg"
		# set ack [ MessageDlg .cmonNodes$page -title Warning -type okcancel -aspect 300 \
		#		-message $msg -icon question -justify left -font $::MSGFONT ]
        cmonNodes::dispChgMsg $page $msg
		;## a cancel returns 1
		if	{ ! $::cmonNodes::var(submitOK) } {
			return
		}
		set servercmd "cntlmon::nodeChgs \{$delnodes\} \{$addnodes\}"
	}

	if	{ [ catch { 
		foreach { login passwd } [ validateLogin 1 ] { break } 	
	} err ] } {
		appendStatus $::cmonNodes::statusw($page) "[ myName ] $err"
		return        
	}
	if	{ [ string equal cancelled $login ] } {
		return
	}
    set comment " by user $login for '$::cmonNodes::var($page,comment)'."
    append servercmd " \{$comment\}"
    if  { [ catch {
		set cmdId new
    	set repeat 0
    	set freq 0
    	set name [ namespace tail [ namespace current ] ]
    	set client $::cmonClient::client
		set cmd "cmonNodes::showReply $page\n$repeat\n$freq\n\$client:$cmdId:$login:$passwd\n$servercmd"
		# puts "cmd $cmd"
		sendCmd $cmd
		appendStatus $::cmonNodes::statusw($page) "Working on it, please wait ..." 0 blue 
		set state $::cmonNodes::state($page)	
		state[expr $state + 1 ] $page	
	} err ] } {
		 cmonNodes::state0 $page 
         appendStatus $::cmonNodes::statusw($page) "[ myName ] $err"
	}
}

## ******************************************************** 
##
## Name: cmonNodes::showReply 
##
## Description:
## display results from server 
##
## Parameters:
## page
## rc   -   return code for request 
## clientCmd
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodes::showReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
	set cmonClient::client $client
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    ;## clear out previous additions
    cmonNodes::clearAllNodes $page
    	
	switch $rc {
	0 { set ::cmonNodes::cmdId($page) $cmdId
		;## refresh the nodes by triggering trace			
		set site [ string trim $::cmonClient::var(site) ]	
		;## forces cmonLoad to do one update
		;## set ::beowulfNodes($site) $html
        ;## automatically updated due to server monitoring mpi rsc file    
            
		switch $::cmonNodes::state($page) {
			1 { cmonNodes::state2 $page }
			3 { cmonNodes::state0 $page }
	    	default { cmonNodes::state0 $page }	
		}
	  }
	3 { cmonNodes::state0 $page
        appendStatus $::cmonNodes::statusw($page) $html    
	  }
	}
}

##******************************************************** 
##
## Name: cmonNodes::deleteNode  
##
## Description:
## mark a node for delete and allow undo
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonNodes::deleteNode { menuw cbutton index page } {
	
	set text [ $menuw entrycget $index -label ]
	if	{ [ string match delete $text ] } {
		$cbutton configure -foreground red
		$menuw entryconfigure $index -label undo
	} else {
		$cbutton configure -foreground black
		$menuw entryconfigure $index -label delete	
	}
} 

##******************************************************** 
##
## Name: cmonNodes::nodeChgs 
##
## Description:
## gather list of nodes to change 
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonNodes::nodeChgs { page } {
	
    set delnodes [ list ]
    set addnodes [ list ]
    if  { [ catch {
	    set delnodes [ list ]
	    set done 0
        set nodenum 0
        set ::cmonNodes::delframes($page) [ lsort -unique $::cmonNodes::delframes($page) ]
  
	    foreach frame $::cmonNodes::delframes($page) {
		    regexp {fnodes(\d+)} $frame -> begin 
		    set end [ expr $begin + $::MAXNODESROW ]  
		    for { set i $begin } { $i < $end } { incr i 1 } {          
			    set nodebutton $frame.node$i
			    if	{ [ winfo exist $nodebutton ] } {
				    set node [ $nodebutton cget -text ]
				    if  { $::cmonNodes::delete($page,$i) } {
					    lappend delnodes $node
				    }
			    } 
		    }
        }
        set delnodes [ lsort -dictionary $delnodes ]	
	    ;## nodes to add but may have no index if no nodes on system
        catch { foreach { index addnodes } [ cmonNodes::sortAddNodes $page ] { break } }
    } err ] } {
        return -code error $err
    } 
	return [ list $delnodes $addnodes ]
}

##******************************************************** 
##
## Name: cmonNodes::clearAddNodes 
##
## Description:
## reset all nodes back to original active list
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonNodes::clearAddNodes { page } {
	;## clear node list 
	for { set i 0 } { $i < $::::MAXNODESADD } { incr i 1 } {
		set ::cmonNodes::addNodes($page,$i) ""
	}    
}

##******************************************************** 
##
## Name: cmonNodes::clearAllNodes 
##
## Description:
## reset all nodes back to original active list
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonNodes::clearAllNodes { page } {
	;## clear node list 
	for { set i 0 } { $i < $::::MAXNODESADD } { incr i 1 } {
		set ::cmonNodes::addNodes($page,$i) ""
        set ::cmonNodes::delete($page,$i) 0
	}    
}

##******************************************************** 
##
## Name: cmonNodes::resetActiveNodes 
##
## Description:
## reset all nodes back to original active list
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonNodes::resetActiveNodes { page } {

	set done 0
	foreach frame $::cmonNodes::delframes($page) {
		if	{ $done } {
			break
		}
		regexp {fnodes(\d+)} $frame -> begin 
		set end [ expr $begin + $::MAXNODESROW ]
		for { set i $begin } { $i < $end } { incr i 1 } {
			set nodebutton $frame.node$i 
			if	{ [ winfo exist $nodebutton ] } {
				set ::cmonNodes::delete($page,$i) 0
				$nodebutton configure -fg black 
				$nodebutton.menuDB entryconfigure 0 -label delete
			} else {
				set done 1
			}
		}	
	}

}

## ******************************************************** 
##
## Name: cmonNodes::state0
##
## Description:
## go back to initial state 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodes::state0 { page } {

    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonNodes::topfw($page) normal
	$::cmonNodes::bundo($page) configure -state disabled
	array set ::cmonNodes::state [ list $page 0 ]
    cmonNodes::clearAllNodes $page
}

## ******************************************************** 
##
## Name: cmonNodes::state1
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodes::state1 { page } {

    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonNodes::topfw($page) disabled
	array set ::cmonNodes::state [ list $page 1 ]
}

## ******************************************************** 
##
## Name: cmonNodes::state2
##
## Description:
## go back to initial state 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodes::state2 { page } {

    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonNodes::topfw($page) normal
	$::cmonNodes::bundo($page) configure -state normal
	array set ::cmonNodes::state [ list $page 2 ]
    cmonNodes::clearAllNodes $page
}


## ******************************************************** 
##
## Name: cmonNodes::state3
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodes::state3 { page } {

    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonNodes::topfw($page) disabled
	array set ::cmonNodes::state [ list $page 3 ]
}

			
##******************************************************** 
##
## Name: cmonNodes::loginScrn 
##
## Description:
## bring dialog to request login and password
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonNodes::loginScrn { menuw cbutton index page } {
	
	set node [ $cbutton cget -text ]
	set dlg [Dialog $::cmonNodes::wLogin${node} -parent . -modal local \
          	-separator 1 -title   "Node $node login " \
        	-side bottom -default 0 -cancel 3 ]
	set parent $dlg
	set subf1 [ frame $parent.fent -relief sunken -borderwidth 2 ] 
	frame $subf1.fterms
	label $subf1.fterms.lbl -text "Select Terminal type: " -width 20 -justify right -anchor w
	radiobutton $subf1.fterms.xterm -text "xterm" -width 8 \
    	-variable ::cmonNodes::var($page,term) -value "xterm" 
    radiobutton $subf1.fterms.rxvt -text "rxvt" -width 8 \
    	-variable ::cmonNodes::var($page,term) -value "rxvt" 
		
	set login_ent   [LabelEntry $subf1.login -label "Beowulf Login: " -labelwidth 20 -labelanchor w \
                   -textvariable ::cmonNodes::var($page,login) -editable 1 -width 20 -labeljustify right \
                   -helptext "enter Login Id for beowulf nodes & hit Return to login"]
				   
	set ::cmonNodes::var($page,login) ldas
	;## do not bind return because this could cause multiple ssh windows on some systems			   
	;##$login_ent bind <Return>  "::cmonNodes::nodeLogin $page"
	
	pack $subf1.fterms.lbl -side left 
	pack $subf1.fterms.xterm $subf1.fterms.rxvt -side left
	
	;## this is preferred over the binding of Return key
	set f1 [ frame $subf1.buts ]
	button $f1.start -text "SUBMIT" -width 10 \
        -command  "destroy $dlg; cmonNodes::nodeLogin $page $node"
	button $f1.cancel -text "CANCEL" -width 10 \
        -command  "destroy $dlg"	
	pack $f1.start $f1.cancel -side left -padx 5 -pady 5     
	pack $subf1.fterms $subf1.login $f1 -side top 
	
    set subf2  [ frame $parent.foption -relief sunken -borderwidth 2 ]
	message $subf2.msg1 -font $::LISTFONT -text \
		"1. After you hit Return, enter a password at the window that pops up." \
		-justify left -aspect 1000 -anchor w -foreground blue
	message $subf2.msg2 -font $::LISTFONT -text \
		"2. A new window will take you into the beowulf nodes (n000 is beowulf)." \
		-justify left -aspect 1000 -foreground blue -anchor w
	message $subf2.msg3 -font $::LISTFONT -text \
		"3. Please remember to close window (exit) when done." \
		-justify left -aspect 1000 -foreground blue -anchor w
	pack $subf2.msg1 $subf2.msg2 $subf2.msg3 -side top -expand 1
    set ::cmonNodes::var($page,term) "rxvt"
	
	pack $subf1 $subf2 -side left -anchor n -fill both -expand yes -padx 2	
	$dlg draw
} 

##******************************************************** 
##
## Name: cmonNodes::sendRequest  
##
## Description:
## executes the GO command by getting a terminal or bwatch up
## on the client's display
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## Must run the ssh from script or the rxvt will terminate
## before login prompt
## This code may be obsolete
## if requirement is to use ssh to reach the nodes instead of
## cntlmonAPI

proc cmonNodes::nodeLogin { page node } {	

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonNodes::statusw($page) "Please select an LDAS site"
		return 
	}
	foreach { host port } $::siteport($::cmonClient::var(site)) { break }
	set user $::cmonNodes::var($page,login)
	if	{ [ regexp {^[\s\t\n]*$} $user ] } { 
		appendStatus $::cmonNodes::statusw($page) "No user login entered." 
        return  
   	}
	set term $::cmonNodes::var($page,term)
	;## use an rxvt to do the ssh into the node
    catch { exec $::XTERM -geometry 40x6 \
		-title $user@$host,$node \
		-e ssh $user@$host $::NODELOGIN  \
		$node $::cmonNodes::var($page,term) \
		$user@$node $user & } pid	
	if	{ [ regexp {^\d+} $pid ] } {
		lappend ::cmonNodes::terms $pid
		lappend ::cmonClient::childPids $pid
	} else {
		appendStatus $::cmonNodes::statusw($page) $pid
	}
}
 
## ******************************************************** 
##
## Name: cmonNodes::pageRaise   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats
## nodes and bwatch pages are updated via trace

proc cmonNodes::pageRaise { page } {

	set ::cmonClient::noPass 0
    set ::cmonClient::pageReset "cmonNodes::pageRaise $page"
	set ::cmonClient::pageUpdate "cmonNodes::pageRaise $page"
 
    if  { ! [ string equal $::cmonNodes::prevsite($page) $::cmonClient::var(site) ] } {
        cmonNodes::state0 $page
        foreach pid $::cmonNodes::terms {
		    catch { exec kill -9 $pid } err
	    }
	    set ::cmonNodes::terms [ list ]  
        set ::cmonNodes::prevsite($page) $::cmonClient::var(site)
        cmonNodes::pageUpdate $page 
    }
}

## ******************************************************** 
##
## Name: cmonNodes::pageUpdate   
##
## Description:
## update title to reflect the site
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodes::pageUpdate { page } {

	if	{ $::cmonClient::state } {
		$::cmonNodes::titf3($page) configure -text "Delete Active Nodes at $::cmonClient::var(site)"
		$::cmonNodes::titf4($page) configure -text "Add Nodes at $::cmonClient::var(site)"
	} else {
		$::cmonNodes::titf3($page) configure -text "Delete Active Nodes"
		$::cmonNodes::titf4($page) configure -text "Add Nodes"
	}
}

## ******************************************************** 
##
## Name: cmonNodes::addNodeRanges   
##
## Description:
## add a range of nodes
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodes::addNodeRanges { page } {

    set site [ string trim $::cmonClient::var(site) ]
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonNodes::statusw($page) "Please select an LDAS site" 
		return 
	}
    if	{ ! [ info exist ::beowulfNodes($site)  ] } {
    	set ack [ tk_messageBox -type ok -default ok -message \
        	"$site does not have mpiAPI or beowulf nodes to support this function." \
            -icon info ]
        return
    }

	set suffix $::cmonNodes::var($page,addsuffix)
    if  { ! [ string length $suffix ] } {
        appendStatus $::cmonNodes::statusw($page) "Please enter node name suffix, e,g. node"
        return
    }
    set start  $::cmonNodes::addfrom($page)
    set end    $::cmonNodes::addto($page)
    
    if  { $end < $start } {
        appendStatus $::cmonNodes::statusw($page) "Invalid Range $start-$end"
        return
    }    
    ;## organize the existing set
    if  { [ catch {
        foreach { index addednodes } [ cmonNodes::sortAddNodes $page ] { break }

    ;## this is a loop to weed out duplicates and add as many unique nodes
    ;## as possible
    
         while { $index < $::::MAXNODESADD } { 

            for { set j $start } { $j <= $end } { incr j } {
                if  { ! $::cmonNodes::var($page,singlebox) } {
                    set name ${suffix}${j}
                } else {
                    set name ${suffix}
                }
                if  { $index < $::::MAXNODESADD } {              
                    set ::cmonNodes::addNodes($page,$index) $name
                    incr index 1
                } else {
                    break
                }
            }           
            foreach { index addednodes } [ cmonNodes::sortAddNodes $page ] { break }
            if  { $j >= $end } {
                break
            } else {
                set start $j
            }
        }
    } err ] } {
        appendStatus $::cmonNodes::statusw($page) $err
        return
            
    }
    if  { $j < $end } {
            set msg "Not enough entries to add the rest. You may need to \
            increase cmonClient resource ::MAXNODESADD for the number of entries, \
            if necessary and restart cmonClient."
            appendStatus $::cmonNodes::statusw($page) $msg
    }
        
}


##******************************************************** 
##
## Name: cmonNodes::delNodeRanges 
##
## Description:
## reset all nodes back to original active list
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  
proc cmonNodes::delNodeRanges { page } {

	set site $::cmonClient::var(site)
	if  { ! [ string compare $site $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonNodes::statusw($page) "Please select an LDAS site" 
		return 
	}
    
    if	{ ! [ info exist ::beowulfNodes($site)  ] } {
    	set ack [ tk_messageBox -type ok -default ok -message \
        	"$site does not have mpiAPI or beowulf nodes to support this function." \
            -icon info ]
        return
    }

	set done 0
    
    set begin  $::cmonNodes::delfrom($page)
    set end    $::cmonNodes::delto($page)
    set nodeIndex 0
    if  { $end < $begin } {
        appendStatus $::cmonNodes::statusw($page) "Invalid Range $begin-$end"
        return
    }
    set numnodes [ llength $::cmonNodes::activeNodes($page) ]
    if  { $begin > $numnodes || $end > $numnodes } {
        appendStatus $::cmonNodes::statusw($page) "Valid range is from 0-$numnodes" 
		return 
	}
    set deleted 0
    
	foreach frame $::cmonNodes::delframes($page) {
		regexp {fnodes(\d+)} $frame -> nodebegin
		set nodeend [ expr $nodebegin + $::MAXNODESROW ]
		for { set i $nodebegin } { $i < $nodeend } { incr i 1 } {
			set nodebutton $frame.node$i
			if	{ [ winfo exist $nodebutton ] } {
                if  { ! $::cmonNodes::var($page,singlebox) } {
                    set nodetext [ $nodebutton cget -text ]
                    regexp {(\d+)} $nodetext -> nodeNum
                } else {
                    set nodeNum $i 
                }
                if  { $nodeNum >= $begin && $nodeNum <= $end } {
				    set ::cmonNodes::delete($page,$i) 1
				    $nodebutton configure -fg red 
				    $nodebutton.menuDB entryconfigure 0 -label undo
                    incr deleted 1
                }
			} 
            incr nodeIndex 1
		}	
	}
    if  { $deleted } {
        appendStatus $::cmonNodes::statusw($page) "$deleted nodes to be deleted." 0 blue
    } else {
        appendStatus $::cmonNodes::statusw($page) "No nodes to be deleted."
    }
}


##******************************************************** 
##
## Name: cmonNodes::showNodeOrder
##
## Description:
## show order of node in the list 
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  

proc cmonNodes::showNodeOrder { button nodenum x y } {

    catch {
	set labelname .lblframe
    label $labelname -text "Entry $nodenum" -relief ridge -bg yellow -fg black \
			-font $::SMALLFONT 
    place $labelname -in $button -x $x -y $y 
    }

}

##******************************************************** 
##
## Name: cmonNodes::popdownNodeOrder
##
## Description:
## remove label when out of the node
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
##  

proc cmonNodes::popdownNodeOrder { button } {

    catch { destroy .lblframe } err 

}

##******************************************************** 
##
## Name: cmonNodes::sortAddNodes
##
## Description:
## sort nodes in 
##
## Parameters:
## node number
##
## Usage:
##  
## 
## Comments:
## Only check for duplicates if the node numbers are used, not suffix alone. 

proc cmonNodes::sortAddNodes { page { bgcall 0 } } {

	set site [ string trim $::cmonClient::var(site) ]
	if	{ ! [ info exist ::beowulfNodes($site)  ] } {
    	set ack [ tk_messageBox -type ok -default ok -message \
        	"$site do not have mpiAPI or beowulf nodes to support this function." \
            -icon info ]
        return
    }
    if  { [ catch {
    set newnodes [ list ]
    set uniques  [ list ]
    set duplicates [ list ]
    set add_duplicates [ list ]    
    set addnodes [ list ]
    set addednodes [ list ]
    
    ;## get all nodes in entries
    for { set i 0 } { $i < $::::MAXNODESADD } { incr i 1 } {
        set node [ set ::cmonNodes::addNodes($page,$i) ] 
        if  { [ string length $node ] } {
		    lappend addednodes $node
        }
	}

    set index 0
    if  { [ llength $addednodes ] } {
        ;## check for duplicate active nodes        
        if  { ! $::cmonNodes::var($page,singlebox) } { 
            foreach nodename $addednodes {
                if  { [ lsearch -exact $::beowulfNodes($site) $nodename ] > -1 } {
                    lappend duplicates $nodename
                } else {
                    lappend newnodes $nodename
                }
            } 
            ;## check for add duplication and remove the duplicate
            foreach nodename $newnodes {
                if  { [ regsub -all $nodename $newnodes {} tmp ] > 1 } {
                    if  { [ lsearch -exact $add_duplicates $nodename ] > -1 } {
                        lappend add_duplicates $nodename 
                        continue                       
                    }
                } 
                lappend uniques $nodename
            }
            set uniques [ lsort -unique -dictionary $uniques  ]
        } else {
            set uniques $addednodes
            if  { [ llength [ lsort -unique $uniques ] ] > 1 } {
                error "Node names must all be '[ lindex $::cmonNodes::activeNodes($page) 0 ]' for a single box system" 
            }
        }
        
        ;## enter the nodes in sorted order
        foreach node $uniques {
            set ::cmonNodes::addNodes($page,$index) $node
            incr index 1
        }
        
        ;## clear out the remaining entries 
        for { set i $index } { $i < $::::MAXNODESADD } { incr i 1 } {
            set ::cmonNodes::addNodes($page,$i) ""
        }
        
        set text ""
        if  { [ llength $duplicates ] } {
            append text "Already active nodes: [ join $duplicates ]\n"
        }
        if  { [ llength $add_duplicates ] } {
            append text "Already on add list: [ join $add_duplicates ]\n"
        }
        if  { [ string length $text ] } {
            error [ string trim $text \n ]
        }  
    }
    } err ] } {
        appendStatus $::cmonNodes::statusw($page) $err
        if  { $bgcall } {
            appendStatus $::cmonNodes::statusw($page) $err
        } else {
            return -code error $err
        }
    }
    if  { !$bgcall } {
        return "$index [ list $uniques ]"
    }
}

##******************************************************** 
##
## Name: cmonNodes::dispChgMsg
##
## Description:
## use a scrolling window to display msg to user
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodes::dispChgMsg { page msg } {

    if  { [ catch {
        set site [ string trim $::cmonClient::var(site) ]
    
    ;## file saved in html:;saveHtml    
	;## set fd [ open $::TMPDIR/${page}_${site}.html w ]
	;## puts $fd $html
	;## close $fd 
        set dialogw $::cmonNodes::wMsg${page}
        if  { ! [ winfo exist $::cmonNodes::wMsg${page} ] } {	
            set dlg [ Dialog $dialogw -parent . -modal local \
    		-separator 1 -title   "Your Node change request " \
        	-side bottom -default 0 -cancel 3 ]	
            set dlgframe [$dlg getframe]
            set textw [ ::createStatus $dlgframe ]
            $dlg add -name ok -text OK -command "set ::cmonNodes::var(submitOK) 1; destroy $dlg"
            $dlg add -name cancel -text CANCEL -command "set ::cmonNodes::var(submitOK) 0; destroy $dlg"
            # $dlg add -name find -text FIND -command "cmonCommon::findDialog cmonNodes $page $dlgframe $textw"
		    set ::cmonNodes::msgtextw($page) $textw
            appendStatus $textw $msg 0 brown 0
            $dlg draw 
            destroy $dlg
        } 
    } err ] } {
       appendStatus $::cmonNodes::statusw($page) $err
	}
}

##******************************************************** 
##
## Name: cmonNodes::traceUpdate
##
## Description:
## update the options button for the nodes since the site
## may have changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## This is needed to receive the nodes updated from MPI
## only if page has been created.

proc cmonNodes::traceUpdate { args } {

	set page [ lindex $args 0 ]
    if  { [ winfo exist $::cmonNodes::statusw($page) ] } {
	    set site [ string trim $::cmonClient::var(site) ]
		set sites [ array names ::beowulfNodes ]
        if	{ ! [ string equal {no-server} $sites ] } {  
        	if	{ ! [ info exist ::beowulfNodes($site)  ] } {
        		return
           	}  	
	    	if	{ [ catch {
		    	cmonNodes::dispActiveNodes $page
				cmonBwatch::createAllPages
				cmonLoad::recreateNodes               
	    	} err ] } { 
	        	appendStatus $::cmonNodes::statusw($page) "[myName] $err"
	    	}
      	}
    }
}
