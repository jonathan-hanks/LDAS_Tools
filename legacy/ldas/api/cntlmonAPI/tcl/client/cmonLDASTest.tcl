## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles administration of LDAS user etc.
##
## repeat and frequency are here for conformance to
## standard communication with server and are not actually used.
## ******************************************************** 

package provide cmonLDASTest 1.0

namespace eval cmonLDASTest {
    set repeat { 0 5 10 100 } 
    set freq { 5 10 100 1000 } 
    set notebook ""
    set freqUnit 1000  
	set freqUnits secs
    set pages [ list ]
    set allcmdsRun 0
    set allcmdsTest "TEST ALL CMDS"
    set allcmdsCancel "CANCEL ALL TESTS"
}


## ******************************************************** 
##
## Name: cmonLDASTest::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::create { notebook } {
    
    set state normal
    if  { [ catch {
        init 
    } err ] } {
        set state disabled
        set errmsg $err
    }
    if  { [ string match disabled $state ] } {
        after 5000 [ list cmonLDASTest::reportError $err ]

    }	
    set fnb [ $notebook insert end cmonLDASTest -text "LDAS test" -state $state ]

    set ::cmonLDASTest::notebook [ NoteBook $fnb.nb \
        -foreground white -background #fa9498 -bd 2 \
		-activeforeground white -activebackground blue ] 
        
	set notebook1 $::cmonLDASTest::notebook
    
    ;## add a page for running all user commands
    set page $::cmonLDASTest::allcmdsTest
    regsub -all {\s+} $page {-} page
    set ::cmonLDASTest::pages [ linsert $::cmonLDASTest::pages 0 $page ]
        
    foreach page $::cmonLDASTest::pages {
        regsub -all {\-} $page { } pagetext
        set ::cmonLDASTest::prevsite($page) none
        $notebook1 insert end $page -text $pagetext \
        -createcmd "cmonLDASTest::createPages $notebook1 $page"  \
        -raisecmd "cmonLDASTest::pageRaise $notebook1"
    }
    
    $notebook1 raise [ $notebook1 page 0 ]
    $notebook itemconfigure cmonLDASTest -raisecmd "cmonLDASTest::pageRaise $notebook1"
    pack $notebook1 -fill both -expand yes -padx 4 -pady 4  
}

## ******************************************************** 
##
## Name: cmonLDASTests::createStatus
##
## Description:
## 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## 

proc cmonLDASTest::createStatus { frame page var } { 

    if  { [ catch {
	    set textw [ ::createStatus $frame ]
        ;## create red and blue tag for errors and info msgs
	    $textw tag configure red -foreground red -wrap word 
	    $textw tag configure blue -foreground blue -wrap word
	    $textw tag configure green -foreground #188a00 -wrap word
        $textw configure -bg PapayaWhip
        set varname ::cmonLDASTest::${var} 
        array set $varname [ list $page $textw ]
    } err ] } {
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: cmonLDASTests::createPages
##
## Description:
## 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## 

proc cmonLDASTest::createPages { notebook page } {
    
    if  { [ catch {
        set frame [ $notebook getframe $page ]        	
	    set topf  [frame $frame.topf]	
        set ::cmonLDASTest::topfw($page) $topf
        if  { [ regexp -nocase {TEST-ALL-CMDS} $page ] } {
            specificAll $topf $page
            set allpage 1
        } else {      
            specific $topf $page
            set allpage 0
        }
	    pack $topf -fill x -pady 1
        
        set ::cmonLDASTest::notebook2 [ NoteBook $frame.nb2 -homogeneous 0 \
        	-foreground white -background $::darkbrown -bd 2 \
			-activeforeground white -activebackground blue ]
        
        set notebook2 $::cmonLDASTest::notebook2
        set responsePage TestResponse$page
        set ::cmonLDASTest::prevsite($responsePage) $::cmonClient::var(site)  
        $notebook2 insert end TestResponse$page -text "$page responses" \
        	-createcmd "cmonLDASTest::createStatus [ $notebook2 getframe $responsePage ] $page textw" \
            -raisecmd [ list cmonLDASTest::pageRaise $notebook2 ]  
       
        set statusPage TestCmdStatus$page
        set ::cmonLDASTest::prevsite($statusPage) $::cmonClient::var(site) 
        $notebook2 insert end $statusPage -text "$page Command status" \
        	-createcmd "cmonLDASTest::createStatus [ $notebook2 getframe $statusPage ] $page statusw" \
            -raisecmd [ list cmonLDASTest::pageRaise $notebook2 ]      
       
        array set ::cmonLDASTest::state [ list $page 0 ]
        cmonCommon::pageUpdate cmonLDASTest $page $page
        pack $notebook2 -fill both -expand yes -padx 2 -pady 2   
        $notebook2 raise [ $notebook2 page 1 ]
        $notebook2 raise [ $notebook2 page 0 ]
        $notebook compute_size
        set ::cmonLDASTest::prevsite($page) $::cmonClient::var(site) 
        
    } err ] } {
        puts $err
    } 
}

## ******************************************************** 
##
## Name: cmonLDASTest::init
##
## Description:
## reads in cmd files
##
## Parameters:
## 
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::init {} {

    if  { [ catch {
        set ldascmds [ glob $::LDASCMDDIR/* ]
        foreach ldascmd $ldascmds {     
            set cmdfiles [ lsort [ glob -nocomplain ${ldascmd}/*.cmd ] ]
            if  { [ string length $cmdfiles ] } {
                set ldascmd [ file tail $ldascmd ]
                lappend ::cmonLDASTest::pages $ldascmd
                foreach cmdfile $cmdfiles {
                    set fd [ open $cmdfile r ] 
                    set cmd [ read -nonewline $fd ]
                    close $fd
                    set cmdfile [ file tail $cmdfile ]
                    set ::cmonLDASTest::${ldascmd}($cmdfile) $cmd
                    lappend ::cmonLDASTest::${ldascmd}(tests) $cmdfile
                }
                set ::cmonLDASTest::${ldascmd}(tests) [ lsort \
                    [ set ::cmonLDASTest::${ldascmd}(tests) ] ]
            }
        }
        set ::cmonLDASTest::pages [ lsort -dictionary $::cmonLDASTest::pages ]
    } err ] } {
        return -code error $err
    }
}


## ******************************************************** 
##
## Name: cmonLDASTest::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::specific { parent page } {

    set labf1 [LabelFrame $parent.labf1 \
        -text "press SUBMIT to send LDAS job" -side top -anchor w \
               -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]
	set name [ namespace tail [ namespace current ] ]
    
    createTestlist $page $subf

    set f1 [ frame $subf.f1 ]
    set ck1 [ checkbutton $f1.jobloop -text "Loop for (mins):" \
        -variable ::${name}::${page}(jobloop) -width 15 \
        -justify left -anchor w -onvalue 1 -offvalue 0 ]
	set spin  [SpinBox $f1.looptime -range {1 100 1} -textvariable ::${name}::${page}(looptime) \
                   -helptext "Repeat job within this time" -editable 1 -width 33 ]
	pack $ck1 $spin -side left -anchor w -expand 1 -fill x
    
    set fbut [ frame $subf.buttons ]                    
    set but1 [ Button $fbut.start -text "SUBMIT" -helptext "submit request to server" \
        -command  "::cmonLDASTest::sendRequest $page" ]
    set but2 [ Button $fbut.restore -text "RESTORE CMD" -helptext "restore original cmd template" \
        -command  [ list cmonLDASTest::setLDASTest [ set ::cmonLDASTest::${page}(combo) ] $page ] ]
    
    set but3 [ Button $fbut.clear -text "INITIALIZE TEST" -helptext "initialize test data so it can be run again" \
        -command  [ list cmonLDASTest::initTestData  $page ] ]
        
	set ::cmonLDASTest::bstart($page) $but1

    pack $but1 $but2 $but3 -side left -padx 5 -pady 10
    
    cmonLDASTest::allcmdsCheck $page
	pack $f1 $fbut -side top -pady 5 -expand 1 -fill x
    
    set labref [LabelFrame $parent.refresh -text "Edit command parameters if necessary:" -side top -anchor w \
                   -relief sunken -borderwidth 4]    
    set sw [ScrolledWindow $labref.fcmdtext -relief raised -borderwidth 0 -auto both]
    set subf [$sw getframe]
	set textw [ text $subf.cmdtext -height 10 ]
	$sw setwidget $textw
    $textw tag configure blue -foreground blue -wrap word
    bind $textw <Button-3> "clearStatus %W"
    set test [ lindex [ set ::cmonLDASTest::${page}(tests) ] 0 ]
    set cmd [ set ::cmonLDASTest::${page}($test) ]
    updateStatus $textw $cmd 0 blue
    $textw configure -state normal
	pack $sw -fill both -expand 1
    set ::cmonLDASTest::cmdtextw($page) $textw
	pack $labf1 $labref -side left -fill both -expand 1

}

## ******************************************************** 
##
## Name: cmonLDASTest::specificAll 
##
## Description:
## creates test all cmds page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:

proc cmonLDASTest::specificAll { parent page } {

    set labf1 [LabelFrame $parent.labf1 \
        -text "Test all user commands" -side top -anchor w \
               -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]
	set name [ namespace tail [ namespace current ] ]
                       
    set but3 [ Button $subf.allcmds -text $::cmonLDASTest::allcmdsTest -helptext "test all ldas user commands" \
        -command "::cmonLDASTest::allcmdsRequest $page" ]
    
    set fbut [ frame $subf.interim]
    set ckbut3 [ checkbutton $fbut.interim -text "Show interim results after every " \
        -variable ::cmonLDASTest::interim($page) -onvalue 1 -offvalue 0 ]
      
    set spin1  [SpinBox $fbut.testspin -range [ list 1 $::MAXNUMTESTS 1 ] -textvariable ::cmonLDASTest::numTests($page) \
                   -helptext "show interim results after nth number of tests" -width 5 -editable 1 ]
                   
    set lbl2 [ label $fbut.lbl2 -text "tests" ] 
    
    $spin1 bind <Leave>  "cmonCommon::histSpinNew cmonLDASTest $page ::cmonLDASTest::numTests($page) 1 $::MAXNUMTESTS"
    $spin1 bind <Return> "cmonCommon::histSpinNew cmonLDASTest $page ::cmonLDASTest::numTests($page) 1 $::MAXNUMTESTS"  
                  
    set ::cmonLDASTest::ballcmds($page) $but3
    pack $ckbut3 $spin1 $lbl2 -side left -padx 1
    pack $fbut $but3 -side top -anchor w -padx 5 -pady 5

    ;## default is show interim results
    set ::cmonLDASTest::interim($page) 1
    
    cmonLDASTest::allcmdsCheck $page
	pack $fbut -side top -pady 5 -expand 1 -fill x
    
    set labref [LabelFrame $parent.results -text "Last test results " \
        -side top -anchor w -relief sunken -borderwidth 4 ]
	set subf2 [ $labref getframe ]

    set lbl1 [ label $subf2.indicator -text "" -font $::MSGFONT -state disabled ]
    set ::cmonLDASTest::indicatorw($page) $lbl1
    pack $lbl1 -side top -fill both -expand 1
     
	pack $labf1 $labref -side left -fill both -expand 1

}

## ******************************************************** 
##
## Name: cmonLDASTest::createTestlist
##
## Description:
## create droplist for tests
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::createTestlist { page parent } {

    set tests [ set ::cmonLDASTest::${page}(tests) ]
    set ::cmonLDASTest::${page}(selectedTest) [ lindex $tests 0 ]
	set fcmd [ frame $parent.fcmd ]
	set lab [ label $fcmd.lab -text " Select test: " -width 20 -justify left -anchor w ]
	set cmd_ent   [ ComboBox $fcmd.cmd -width 35 \
    	-textvariable ::cmonLDASTest::${page}(selectedTest) -editable 0 \
		-values $tests \
        -helptext "$page test sets" \
		-modifycmd "cmonLDASTest::setLDASTest $fcmd.cmd $page" ]
	pack $lab $fcmd.cmd -side left -anchor w -expand 1 -fill x
    set ::cmonLDASTest::${page}(combo) $fcmd.cmd
    pack $fcmd -side top -anchor w -fill x
}

## ******************************************************** 
##
## Name: cmonLDASTest::setLDASTest
##
## Description:
## display dso cmd in text window
##
## Parameters:
##
## Comments:

proc cmonLDASTest::setLDASTest { combo page } {
    
    $combo setvalue @[ $combo getvalue ] 
	set test [ set ::cmonLDASTest::${page}(selectedTest) ]
    set cmd  [ set ::cmonLDASTest::${page}($test) ]
    set cmdtextw [ set ::cmonLDASTest::cmdtextw($page) ]
    updateStatus $cmdtextw $cmd 0 blue
    $cmdtextw configure -state normal
}

## ******************************************************** 
##
## Name: cmonLDASTest::sendRequest
##
## Description:
## sends request to server
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::sendRequest { page } {

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLDASTest::statusw($page) "Please select an LDAS site"
		return 
	}
    set login ""
	set passwd ""
    set cmdId "new"   
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0
    if  { [ catch {
        foreach { login passwd } [ validateLogin 1 ] { break }
    } err ] } {
		appendStatus $::cmonLDASTest::statusw($page) $err
		return        
	}
    if  { [ string equal cancelled $login ] } {
		return
	}
    if  { [ catch {
    	if	{ ! $::USE_GLOBUS } {
        	set userpass [ base64::encode64 [ binaryEncrypt $::CHALLENGE $::cntlpasswd ] ]
        } else {
        	set userpass $passwd
            ckPriviledged
        }
        set userinfo "-name $login -password $userpass -email persistent_socket"
        regsub -all -- {\s+} $userinfo { } userinfo
        
        set cmdinfo [ $::cmonLDASTest::cmdtextw($page) get 0.0 end ]        
        set tmp [ list ]
        foreach line [ split $cmdinfo "\n" ] {
            if  { ! [ regexp {^\s*#} $line ] } {
                lappend tmp $line
            }
        }
        set cmdinfo [ join $tmp "\n" ]
        ;## collapse to a single line
        regsub -all -- {\s+} $cmdinfo { } cmdinfo 
        set cmdinfo [ string trim $cmdinfo ]
    
        set cmd "cmonLDASTest::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::submitLDASjob $page \{$userinfo\} \{$cmdinfo\}"
        # puts "cmd=$cmd"
        sendCmd $cmd 
        if  { [ set ::cmonLDASTest::${page}(jobloop) ] } {
        	if	{ ![ info exist ::cmonLDASTest::${page}(loopcount) ] } {
            	set ::cmonLDASTest::${page}(loopcount) 0
            } 
            incr ::cmonLDASTest::${page}(loopcount) 1
        	appendStatus $::cmonLDASTest::statusw($page) \
            "sent [ set ::cmonLDASTest::${page}(loopcount) ] round" 0 blue 0

            set ::cmonLDASTest::${page}(serverCmd) $cmd
            set ::cmonLDASTest::${page}(loopEndTime) [ expr [ clock seconds ] + \
                [ set ::cmonLDASTest::${page}(looptime) ] * 60 ]
            state2 $page
        } else {
            state3 $page
        }
    } err ] } {
        cmonLDASTest::state0 $page 
        appendStatus $::cmonLDASTest::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLDASTest::repeatRequest
##
## Description:
## repeat a job request to server
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::repeatRequest { page } {
    
    if  { ! [ set ::cmonLDASTest::${page}(jobloop) ] } {
    	catch { unset ::cmonLDASTest::${page}(loopcount) } 
        cmonLDASTest::state0 $page
        return
    }
    if  { [ catch {
        set currtime [ clock seconds ]
        if  { $currtime < [ set ::cmonLDASTest::${page}(loopEndTime) ] } {
        	incr ::cmonLDASTest::${page}(loopcount)
            set cmd [ set ::cmonLDASTest::${page}(serverCmd) ]
            sendCmd $cmd
            appendStatus $::cmonLDASTest::statusw($page) \
             "sent [ set ::cmonLDASTest::${page}(loopcount) ] round" 0 blue 0
        } else {
            cmonLDASTest::state0 $page
        }
    } err ] } {
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: cmonLDASTest::getResult
##
## Description:
## sends request to server
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::getResult { data page } {

    set i 0
    if  { [ catch {
    
        set reply [string trim $data]      
        if  { $::DEBUG } {
            puts $::logfd "reply='$reply'"
            flush $::logfd
        }
        
        set msg {}
        set cmd {}
        set rest {}

        ;## cntlmonAPI submitted job
        if  { [ regexp -nocase -- {submitted '([^']+)'.+as\s+job\s+(\S+)} $data -> cmd jobid ] } {
            set test [ set ::cmonLDASTest::${page}(selectedTest) ]
            appendStatus $::cmonLDASTest::textw($page) "SEND $cmd as $jobid" 0 blue 0         
        ;## manager or cntlmonAPI got error  
        } elseif { [ regexp -nocase -- {error|abort|connect|shut|kill} $data ] } {
            set test [ set ::cmonLDASTest::${page}(selectedTest) ] 
            appendStatus $::cmonLDASTest::textw($page) "$page $test failed. RECV:\n$data" 0 red 0
            state0 $page
        } else {
            regexp -nocase -- {Subject:\n[^\n]+\n(.+)} $data -> rest
            set test [ set ::cmonLDASTest::${page}(selectedTest) ] 
            appendStatus $::cmonLDASTest::textw($page) "$page $test completed OK. RECV:\n$rest" 0 green 0
            state2 $page
            cmonLDASTest::repeatRequest $page
        }                
    } err ] } {
        appendStatus $::cmonLDASTest::textw($page) $err 0 red 1
        # puts $::logfd "reply='$reply'"
        catch { state0 $page }
    }
    ;## send notification from cntlmonAPI and reply from manager
}

## ******************************************************** 
##
## Name: cmonLDASTest::showReply
##
## Description:
## display results from server 
##
## Parameters:
## parent widget 
## rc 	-	return code from cmd to server
## clientCmd -	client name and cmd Id
## html	-	text returned from cmd
##
## Usage:
##  called a button command 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::showReply { page rc clientCmd html } {  

    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "showReply, rc=$rc, $page, client=$client,cmdId=$cmdId, afterid=$afterid,html [string length $html]"
    # puts "html=$html"
	set ::cmonLDASTest::cmdId($page) $cmdId 
    set ::cmonClient::client $client
	switch $rc {

    0 { cmonLDASTest::getResult $html $page }
        
    2 { if  { [ regexp submitted $html ] } {
            cmonLDASTest::getResult $html $page
        } elseif { [ regexp -nocase {do not run} $html ] } {
            appendStatus $::cmonLDASTest::statusw($page) $html 0 red 1
            if  { [ regexp -nocase {putMetaData} $page ] } {
                $::cmonLDASTest::notebook itemconfigure putStandAlone -state disabled
            } else {
                $::cmonLDASTest::notebook itemconfigure putMetaData -state disabled
            }
        } else {
            appendStatus $::cmonLDASTest::statusw($page) $html 0 blue 0
            if  { [ regexp -nocase {putMetaData} $page ] } {
                $::cmonLDASTest::notebook itemconfigure putStandAlone -state normal
            } else {
                $::cmonLDASTest::notebook itemconfigure putMetaData -state normal
            }
        }
      }
	3 { 
		appendStatus $::cmonLDASTest::statusw($page) $html
		;## may need to cancel request	
		cmonLDASTest::state0 $page 	
	  }
    default {}
	}
}

## ******************************************************** 
##
## Name: cmonLDASTest::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonLDASTest::state0 { page { enableCmd sendRequest } }  {

    updateWidgets $::cmonLDASTest::topfw($page) normal
    array set ::cmonLDASTest::state [ list $page 0 ]
    if  { [ info exist ::cmonLDASTest::ballcmds($page) ] } {
        $::cmonLDASTest::ballcmds($page) configure -state normal \
	        -command "cmonLDASTest::allcmdsRequest $page" \
            -text $::cmonLDASTest::allcmdsTest
    } else {    
     	$::cmonLDASTest::bstart($page) configure -text "SUBMIT" \
        	-state normal -command "::cmonLDASTest::sendRequest $page" -helptext "submit request to server"        
   	}
    set ::cmonLDASTest::allcmdsRun 0
    array set ::cmonLDASTest::state [ list $page 0 ]
}

## ******************************************************** 
##
## Name: cmonLDASTest::state1  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable all buttons, enable cancel button

proc cmonLDASTest::state1 { page }  {

	updateWidgets $::cmonLDASTest::topfw($page) disabled
    array set ::cmonLDASTest::state [ list $page 1 ]

}

## ******************************************************** 
##
## Name: cmonLDASTest::state2  
##
## Description:
## request is repeated, enable cancel only
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASTest::state2 { page } {
	updateWidgets $::cmonLDASTest::topfw($page) disabled	
    $::cmonLDASTest::bstart($page) configure -text "CANCEL" \
        -state normal -command "set ::cmonLDASTest::${page}(jobloop) 0; \
         $::cmonLDASTest::bstart($page) configure -state disabled"
	array set ::cmonLDASTest::state [ list $page 2 ]
}

## ******************************************************** 
##
## Name: cmonLDASTest::state3  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASTest::state3 { page } {
	updateWidgets $::cmonLDASTest::topfw($page) disabled
	array set ::cmonLDASTest::state [ list $page 3 ]
}

## ******************************************************** 
##
## Name: cmonLDASTest::state4 
##
## Description:
## enter test of all user cmds
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## used by Test all cmd 

proc cmonLDASTest::state4 { page }  {

	updateWidgets $::cmonLDASTest::topfw($page) disabled
    array set ::cmonLDASTest::state [ list $page 4 ]
    $::cmonLDASTest::ballcmds($page) configure -state normal \
	        -command "cmonLDASTest::allcmdsRequest $page" \
            -text $::cmonLDASTest::allcmdsCancel

}

## ******************************************************** 
##
## Name: cmonLDASTest::reportError 
##
## Description:
## report error if bad port
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASTest::reportError { err } {
        set ack [ tk_messageBox -type ok -default ok \
				-message "$err, Test panel disabled" \
				-icon error ]
            
}

## ******************************************************** 
##
## Name: cmonLDASTest::pageRaise
##
## Description:
## page raise
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## the subpages for job response and status do not have topf

proc cmonLDASTest::pageRaise { notebook } {

    set ::cmonClient::nopass 0
    set currpage [ $notebook raise ]
    cmonCommon::pageUpdate cmonLDASTest $currpage $currpage
    if  { ![ regexp -nocase {status|response} $currpage ] } {
        cmonLDASTest::allcmdsCheck $currpage
    }

}
  
## ******************************************************** 
##
## Name: cmonLDASTest::allcmdsRequest
##
## Description:
## sends request to server to test all user cmds
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## the request would be denied if user is not authorized
## so keep the same states if denied

proc cmonLDASTest::allcmdsRequest { page } {
   
    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLDASTest::statusw($page) "Please select an LDAS site"
		return 
	}
    set login ""
	set passwd ""
    set cmdId "new"   
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0
    if  { [ catch {
        foreach { login passwd } [ validateLogin 1 ] { break }
        set ::cmonLDASTest::login $login
        set ::cmonLDASTest::passwd $passwd
    } err ] } {
		appendStatus $::cmonLDASTest::statusw($page) $err
		return        
	}
    if  { [ string equal cancelled $login ] } {
		return
	}
       
    [ set ::cmonLDASTest::indicatorw($page) ] configure -state normal -text ""
    set ::cmonLDASTest::prev_state($page) $::cmonLDASTest::state($page)
    if  { $::cmonLDASTest::state($page) == 4 } {
        set cmd "cmonLDASTest::allcmdsReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::cancelAllCmdsTest user_commands"
        cmonLDASTest::state1 $page
        appendStatus $::cmonLDASTest::statusw($page) "Cancelling all tests ..." 0 red 0
        sendCmd $cmd 
        return
    }
    set ack [ tk_messageBox -type okcancel -default cancel \
			-message "If you are authorized to run this test, it could take up to 60 minutes \
and no other LDAS test can be submitted at \
the same time. You can cancel any time. Proceed (if authorized to run )?" -icon warning ]
    if  { ! [ string equal ok  $ack ] } {  
        return
    }
    updateDisplay $::cmonLDASTest::textw($page) ""
    set numTests 0
    if  { $::cmonLDASTest::interim($page) } {
        set numTests $::cmonLDASTest::numTests($page) 
    }

    if  { [ catch {
    	if	{ ! $::USE_GLOBUS } {
        	set userpass [ base64::encode64 [ binaryEncrypt $::CHALLENGE $::cntlpasswd ] ]
        } else {
        	set userpass $passwd
        }
        [ set ::cmonLDASTest::indicatorw($page) ] configure -state normal -text ""
        set ::cmonLDASTest::allcmdsRun 1  

    	set userinfo "-name $login -password $userpass -email 12345:678"
    	regsub -all -- {\s+} $userinfo { } userinfo
        set cmd "cmonLDASTest::allcmdsReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::runTest user_commands \{$userinfo\}  $numTests"
        cmonLDASTest::state4 $page
        sendCmd $cmd 
    } err ] } {
        cmonLDASTest::state0 $page 
        appendStatus $::cmonLDASTest::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLDASTest::allcmdsFinalUpdate
##
## Description:
## updates the screen with final results of all user's test
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:

proc cmonLDASTest::allcmdsFinalUpdate {} {

	uplevel { if  { [ regexp {FAILED|ERROR} $html ] } {
                set color red
                set text FAILED
            } elseif { [ regexp {TEST PASSED} $html ] } {
                set color $::brightgreen
                set text PASSED
            } else {
            	set color red  
                regsub -all -- {[\n\s\t]} $html {} temp
                if	{ ![ string length $temp ] } {
                	set html ""
                }              
                set text $html  
                if	{ [ string length $text ] } {
            		if	{ ! [ regexp {<font color=[^>]+>([^<]+)</font>.*</pre></html>} $text -> text ] } {
                		set text $html
                    }
                }        
            }
       
            if  { [ regexp {<html><pre>} $html ] } {
                updateDisplay $::cmonLDASTest::textw($page) $html 
            } elseif { [ string length $html ] } {
                appendStatus $::cmonLDASTest::statusw($page) $html 0 blue
            }
            if	{ [ string length $text ] } {
            	set time [ clock format [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]
            	[ set ::cmonLDASTest::indicatorw($page) ] configure -state normal -fg $color \
                -text "$time:  ***  $::cmonClient::var(site) TEST $text ***"
            } 
            set ::cmonLDASTest::allcmdsRun 0
            cmonLDASTest::state0 $page
   	}
}

## ******************************************************** 
##
## Name: cmonLDASTest::allcmdsReply
##
## Description:
## display result of ldas cmds test
## 
## Parameters:
##
## Usage:
##  
## 
## Comments:
## invoked asynchronously when the reply is received from server
## saves cmdId in reply for cancellation of repeats.
## renders html
## if rejected due to invalid password, restore previous state

proc cmonLDASTest::allcmdsReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts $::logfd "rc=$rc showReply, $page"
    # puts $::logfd "rc=$rc showReply, $page, client=$client,cmdId=$cmdId, html\n'$html'"
    # flush $::logfd 
	set ::cmonLDASTest::cmdId($page) $cmdId 
    set ::cmonClient::client $client

	;## the forced cancel will return a \n so dont update the test result label
    if  { [ catch {
	    switch $rc {
	    0 { cmonLDASTest::allcmdsFinalUpdate
        	appendStatus $::cmonLDASTest::statusw($page) "normal completion" 0 blue
          }                        
	    2 {	if	{ [ regexp {<html><pre>} $html ] } {
        		updateDisplay $::cmonLDASTest::textw($page) $html 
            } else {
               	appendStatus $::cmonLDASTest::statusw($page) $html 0 blue
            }
          }	
	    3  { error $html          
	       }
        }
    } err ] } {
    	if	{ [ regexp {<html><pre>} $err ] } {
        	updateDisplay $::cmonLDASTest::textw($page) $html 
        } else {
       	 	appendStatus $::cmonLDASTest::statusw($page) $err
        }
        if  { [ regexp -nocase {cancelled|authorized|invalid\s+password} $err ] } {
            set prevstate $::cmonLDASTest::prev_state($page)
            cmonLDASTest::state${prevstate} $page
        } else {
            updateStatus $::cmonLDASTest::textw($page) ""            
            cmonLDASTest::state0 $page
        }
    }
}

## ******************************************************** 
##
## Name: cmonLDASTest::allcmdsCheck 
##
## Description:
## display result of cmd
## cmd 
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:

proc cmonLDASTest::allcmdsCheck { page } {

    if  { [ string equal $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
        set ::cmonLDASTest::allcmdsRun 0
        cmonLDASTest::state0 $page
    }
    if  { $::cmonLDASTest::allcmdsRun } {
        if  { ! [ string equal  "TEST-ALL-CMDS" $page ] } {
            cmonLDASTest::state1 $page
        }
    } else {
    	if	{ [ info exist ::cmonLDASTest::state($page) ] } {
    		set state [ set ::cmonLDASTest::state($page) ]
    		cmonLDASTest::state${state} $page 
        }
    }
}

## ******************************************************** 
##
## Name: cmonLDASTest::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASTest::reset { page } {

   set ::cmonLDASTest::allcmdsRun 0
   cmonLDASTest::state0 $page
    
}

## ******************************************************** 
##
## Name: cmonLDASTest::initTestData
##
## Description:
## sends request to server
##
## Parameters:
## page and cmd
##
## Usage:
##  call by main code to create specific area 
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASTest::initTestData { page } {

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLDASTest::statusw($page) "Please select an LDAS site"
		return 
	}
    set login ""
	set passwd ""
    set cmdId "new"   
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0
    if  { [ catch {
        foreach { login passwd } [ validateLogin 1 ] { break }
    } err ] } {
		appendStatus $::cmonLDASTest::statusw($page) $err
		return        
	}
    if  { [ string equal cancelled $login ] } {
		return
	}
    if  { [ catch {
        set cmd "cmonLDASTest::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId:$login:$passwd\ncntlmon::localCmd ::TEST_CLEANUP"
        # puts "cmd=$cmd"
        sendCmd $cmd 
        state3 $page
    } err ] } {
        cmonLDASTest::state0 $page 
        appendStatus $::cmonLDASTest::statusw($page) $err
	}
}

