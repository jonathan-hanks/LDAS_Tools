## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon client Tcl Script.
##
## This script handles display of system load 
## fix: add beowulf nodes which needs ssh to beowulf
## ******************************************************** 
;##barecode

package provide cmonLoad 1.0

namespace eval cmonLoad {

    set repeat { 0 5 100 10000 } 
    set freq { 30 60 900 3600 } 
	set freqUnit 1000
	set freqUnits secs
	set sortfields { "sort field" user group process pid "cpu usage" "memory used" \
		"virtual memory" "resident memory" "elapsed time" "process state" }
	set sortorders { "sort order" increasing decreasing }
	set prevsite none
    set pages gateway
}
#end

## ******************************************************** 
##
## Name: cmonLoad::create
##
## Description:
## creates Load page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoad::create { notebook0 } {
      
    set frame [ $notebook0 insert end cmonLoad -text "System Load" \
        -raisecmd [ list cmonLoad::pageRaise ] ]
    
    set ::cmonLoad::pages $::LDASmachines($::cmonClient::var(site)) 

	;## build notebook to hold all hosts
	set ::cmonLoad::notebook [ NoteBook $frame.nb -homogeneous 0 \
        -foreground white -background $::darkbrown -bd 2 ]
        
    $notebook0 itemconfigure cmonLoad -createcmd \
        "cmonLoad::createPages $::cmonLoad::notebook"
}


## ******************************************************** 
##
## Name: cmonLoad::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoad::createPages { notebook } {

	set ::cmonLoad::pages $::LDASmachines($::cmonClient::var(site)) 
    set ::cmonLoad::prevsite $::cmonClient::var(site)
    
	foreach page $::cmonLoad::pages {
	
	    set frame [$notebook insert end cmonLoad:${page} -text $page ]
        set topf  [frame $frame.topf]
        specific $topf $page	
	    set ::cmonLoad::topfw($page) $topf
	    pack $topf -fill x -pady 2
	    foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	
	    ;## create a status to display errors and status
	    set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
	    set statusw [ ::createStatus  [ $titf2 getframe ] ]
	    array set ::cmonLoad::statusw [ list $page $statusw ]
	    pack $titf2 -pady 2 -padx 4 -fill both -expand yes
        
	    set titf3 [TitleFrame $pane3.titf3 -text "System Usage" -font $::ITALICFONT ]
        if  { $::cmonClient::state } {
            $titf3 configure -text "System Usage at $::cmonClient::var(site)"
        }
	    set ::cmonLoad::titf3($page) $titf3 
	    set textw [ ::createDisplay [ $titf3 getframe ] ]
        array set ::cmonLoad::textw [ list $page $textw ]
	    pack $titf3 -pady 2 -padx 4 -fill both -expand yes
	    pack $pw1 -side top -fill both -expand 1

        array set ::cmonLoad::state [ list $page 0 ]
	}
    $notebook compute_size
	pack $notebook -fill both -expand yes -padx 4 -pady 4
 
	$notebook raise [ $notebook page 0 ]
    
    return $frame
}

## ******************************************************** 
##
## Name: cmonLoad::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoad::specific { parent page } {

	set ::cmonLoad::var($page,sortorder) [ lindex $::cmonLoad::sortorders 0 ]
	set ::cmonLoad::var($page,sortfield) [ lindex $::cmonLoad::sortfields 0 ]
	
    set labf1 [LabelFrame $parent.labf1 -text \
		"Specify sort field, sort order, and filter for system load" \
		-side top -anchor w \
        -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]
    ;## same order as server ps userfmt
	frame $subf.sortmenuf -relief ridge -borderwidth 2
	Label $subf.sortmenuf.lbl1 -text "Sort by:" -justify left \
		-helptext "sort by ps command options" 
	set cmd "tk_optionMenu $subf.sortmenuf.sort ::cmonLoad::var($page,sortfield) $::cmonLoad::sortfields"
    set sortmenu [ eval $cmd ]
				
	for { set i 0 } { $i < [ llength $::cmonLoad::sortfields ] } { incr i 1 } {
		if	{ ! $i } { continue }
		$sortmenu entryconfigure $i -command "cmonLoad::setTitle $page" 
	}	
	Label $subf.sortmenuf.lbl2 -text " in " -justify left \
		-helptext "sort by ps command options" 
	set cmd "tk_optionMenu $subf.sortmenuf.order ::cmonLoad::var($page,sortorder) $::cmonLoad::sortorders"
    set sortmenu [ eval $cmd ]
	
	for { set i 0 } { $i < [ llength $::cmonLoad::sortorders ] } { incr i 1 } {
		if	{ ! $i } { continue }
		$sortmenu entryconfigure $i -command "cmonLoad::setTitle $page" 
	}	
	label $subf.sortmenuf.lbl3 -text " order " -justify left 

	$subf.sortmenuf.sort configure -width 20
	$subf.sortmenuf.order configure -width 15
	lappend ::cmonLoad::optionw($page) $subf.sortmenuf.sort 
	lappend ::cmonLoad::optionw($page) $subf.sortmenuf.order 
    pack $subf.sortmenuf.lbl1 $subf.sortmenuf.sort $subf.sortmenuf.lbl2 $subf.sortmenuf.order \
		$subf.sortmenuf.lbl3 -side left -padx 4 -padx 4
    set ::cmonLoad::sortw $subf.sortmenuf.sort
    set ::cmonLoad::orderw $subf.sortmenuf.order
	
	;## allow filter by user
	frame $subf.filter -relief ridge -borderwidth 2
	checkbutton $subf.filter.chfilter -text "Filter for: " \
		-variable ::cmonLoad::var($page,filter) -onvalue 1 -offvalue 0 -state normal \
		-command "cmonLoad::setTitle $page" -anchor w 
		
	set cmd "tk_optionMenu $subf.filter.ffield ::cmonLoad::var($page,ffield) $::cmonLoad::sortfields"
    set filtermenu [ eval $cmd ]
	
	for { set i 0 } { $i < [ llength $::cmonLoad::sortfields ] } { incr i 1 } {
		if	{ ! $i } { continue }
		$filtermenu entryconfigure $i -command "cmonLoad::setTitle $page" 
	}	
	label $subf.filter.lbl1 -text " = " -justify left
    Entry $subf.filter.ftext -textvariable ::cmonLoad::var($page,ftext) -width 15 \
		-command "cmonLoad::setTitle $page"
	bind $subf.filter.chfilter <Leave> "cmonLoad::setTitle $page"
	bind $subf.filter.ftext <Leave> "cmonLoad::setTitle $page"
    bind $subf.filter.ftext <Button-1> "set ::cmonLoad::var($page,filter) 1"
	pack $subf.filter.chfilter $subf.filter.ffield $subf.filter.lbl1 $subf.filter.ftext -side left \
		-anchor w
	pack $subf.sortmenuf $subf.filter -side top -expand 1 -fill x
	set site [ string trim $::cmonClient::var(site) ]
    set mpi_host [ setMPIhost ]
    if  { [ regexp -nocase $mpi_host $page ] } {
        createNodeList $subf.filter.fnode $page
	}
	frame $subf.action -relief ridge -borderwidth 2
	label $subf.action.lbl1 -text "No Action" -justify left -font $::LISTFONT -wraplength 400
	Button $subf.action.start -text "SUBMIT" -width 10 -helptext "submit request to server" \
        -command  "::cmonLoad::sendRequest $page " -state normal	
	set ::cmonLoad::bstart($page) 	$subf.action.start
	set ::cmonLoad::action($page) 	$subf.action.lbl1
	pack $subf.action.lbl1 $subf.action.start -side left -padx 5 -pady 5
	pack $subf.sortmenuf $subf.filter $subf.action -side top -expand 1 -fill x

    set labf2 [LabelFrame $parent.refresh -text "Repeat this request" \
        -side top -anchor w -relief sunken -borderwidth 4]
    set subf2  [ $labf2 getframe ]
	set name [ namespace tail [ namespace current ] ]
	createRepeatFreq $subf2 $name $page
	pack $labf1 $labf2 -side left -fill both -expand 1 -padx 4 
	cmonLoad::setTitle $page
    
}

## ******************************************************** 
##
## Name: cmonLoad::sendRequest 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoad::sendRequest { page } {
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLoad::statusw($page) "Please select an LDAS site"
		return 
	}
	set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
	
    set cmdId "new"
	set freq [ expr $::cmonLoad::var($page,freq)*$::cmonLoad::freqUnit ]
    set repeat $::cmonLoad::var($page,repeat)
    set sortfield $::cmonLoad::var($page,sortfield)
	if	{ [ regexp [ lindex $::cmonLoad::sortfields 0 ] $sortfield ] } {
		appendStatus $::cmonLoad::statusw($page) "Please select sort field"
		return
	}
	regsub -all {\s} $sortfield {_} sortfield 
    set sortorder "-$::cmonLoad::var($page,sortorder)"
	if	{ [ regexp [ lindex $::cmonLoad::sortorders 0 ] $sortorder ] } {
		appendStatus $::cmonLoad::statusw($page) "Please select sort order"
		return
	}
	if	{ $::cmonLoad::var($page,filter) } {
		if	{ ! [ string length $::cmonLoad::var($page,ftext) ] } {
			appendStatus $::cmonLoad::statusw($page) \
			"Please enter a filter pattern when selecting filter (8 chars for process)"
			return
		}	
		if	{ [ regexp {sort\sfield} $sortfield ] } {
			appendStatus $::cmonLoad::statusw($page) "Please select a sort field for filter"
			return
		}
		regsub -all {\s} $::cmonLoad::var($page,ffield) {_} ffield
		regsub -all {\s} $::cmonLoad::var($page,ftext) {\\\s} ftext
		set filter "$ffield=$ftext"
	} else {
		set filter ""
	}
	
	set host [ $::cmonLoad::notebook itemcget cmonLoad:$page -text ]
	if	{ [ regexp -nocase $::MPI_API_HOST $host] } {
		if	{ [ regexp {none} $::cmonLoad::var($page,node) ] } {
			appendStatus $::cmonLoad::statusw($page) "No host/nodes for Beowulf"
			return
		}
		set host $::cmonLoad::var($page,node)
	} else {
		set host [ $::cmonLoad::notebook itemcget cmonLoad:$page -text ]
	}
	if	{ [ catch {
        set cmd "cmonLoad::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::loadLevel $host $sortfield $sortorder $filter" 
		#puts "cmd=$cmd"
		cmonLoad::state1 $page 
		sendCmd $cmd 
	} err ] } {
        cmonLoad::state0 $page 
		appendStatus $::cmonLoad::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLoad::cancelRequest 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLoad::cancelRequest { page } {
	
	set cmdId $::cmonLoad::cmdId($page)
    set client $::cmonLoad::client
    set repeat 0
    set freq 0
	if	{ [ catch {
		set cmd "cmonLoad::showReply $page\n0\n0\n\
        $client:$cmdId\n${client}::cancel $cmdId" 
		#puts "cmd=$cmd"
		cmonLoad::state3 $page 
		sendCmd $cmd		 
	} err ] } {
		cmonLoad::state0 $page 
		appendStatus $::cmonLoad::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLoad::showReply 
##
## Description:
## display result of cmd
## cmd 
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.
## invoked asynchronously when the reply is received from server
## saves cmdId in reply for cancellation of repeats.

proc cmonLoad::showReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
	set ::cmonLoad::cmdId($page) $cmdId 
    set ::cmonLoad::client $client
    set afterid [ lindex $clientCmd 2 ]
    #puts "in showReply, page=$page, client=$client,cmdId=$cmdId,afterid=$afterid"
    if  { ! [ string compare $afterid "" ] } {
        #appendStatus $::cmonLoad::statusw($page) "No more updates"
        cmonLoad::state0 $page 
    }
    ;## do not anchor </html> to end because there may be
    ;## ssh garbage returned 
	if  { [ regexp {(^[^<]*)(<.*/html>)*} $html match status html ] } {
		#puts "status=$status,html=$html"
	} else {
		set html "<html>"
	}
	switch $rc {
	0 {
		#appendStatus $::cmonLoad::statusw($page) $status 1 blue
        if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
            if  { [ regexp {^<html} $html ] } {
                updateDisplay $::cmonLoad::textw($page) $html
            } else {
                appendStatus $::cmonLoad::statusw($page) $html 0 blue
            }
        } 
        if  { ! [ string length $afterid ] } {
            cmonLoad::state0 $page 
            } else {
            cmonLoad::state2 $page 
        }	
	  }
	3 {
		cmonLoad::state0 $page 
		appendStatus $::cmonLoad::statusw($page) $status 1 red
		updateDisplay $::cmonLoad::textw($page) $html
	  }
	}	
}


#******************************************************** 
##
## Name: cmonLoad::reset  
##
## Description:
## changes all pages back to initial state
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
##  
proc cmonLoad::reset { page } {

		 set ::cmonLoad::var($page,sortorder) [ lindex $::cmonLoad::sortorders 0 ]
		 set ::cmonLoad::var($page,sortfield) [ lindex $::cmonLoad::sortfields 0 ]
		 set ::cmonLoad::var($page,ffield) [ lindex $::cmonLoad::sortfields 0 ]
		 set ::cmonLoad::var($page,ftext) ""
         cmonLoad::pageUpdate
         
}   

#******************************************************** 
##
## Name: cmonLoad::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonLoad::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonLoad::topfw($page) normal
	foreach optionw $::cmonLoad::optionw($page) {
		$optionw configure -state normal
	}
    enableStart $name $page normal sendRequest
    updateRepeat $name $page normal

}

#******************************************************** 
##
## Name: cmonLoad::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonLoad::state1 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonLoad::topfw($page) disabled
	foreach optionw $::cmonLoad::optionw($page) {
		$optionw configure -state disabled
	}
    updateRepeat $name $page disabled
}

#******************************************************** 
##
## Name: cmonLoad::state2  
##
## Description:
## request is repeated, enable cancel only
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLoad::state2 { page } {
    $::cmonLoad::bstart($page) configure -text "CANCEL" \
        -state normal -command "cmonLoad::cancelRequest $page"
}

#******************************************************** 
##
## Name: cmonLoad::state3  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLoad::state3 { page } {
    $::cmonLoad::bstart($page) configure -state disabled
}

## ******************************************************** 
##
## Name: cmonLoad::setTitle  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLoad::setTitle { page } {

	if	{ [ catch {
	set text "You request is: sort processes by $::cmonLoad::var($page,sortorder) $::cmonLoad::var($page,sortfield) "
	if	{ [ string match beowulf [ string tolower $page ] ] } {
		append text "on node $::cmonLoad::var($page,node) "
	}
	if	{ $::cmonLoad::var($page,filter) && [ string length $::cmonLoad::var($page,ftext) ] } {
		append text "filter for $::cmonLoad::var($page,ffield)=$::cmonLoad::var($page,ftext) "
	} 
	$::cmonLoad::action($page) configure -text $text -fg blue
	} err ] } {
		catch { appendStatus $::cmonLoad::statusw($page) "[myName] $err" }
	}
}

## ******************************************************** 
##
## Name: cmonLoad::pageRaise
##
## Description:
## update the options button for the nodes since the site
## may have changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## updated from server setting array 
## args=gateway ::LDASmachines ldas-dev w

proc cmonLoad::pageRaise { args } {
	if	{ [ catch {
        set ::cmonClient::noPass 0
        set ::cmonClient::pageReset  "cmonLoad::pageRaise"
        set ::cmonClient::pageUpdate "cmonLoad::pageRaise"
     
        if  { ! [ string equal $::cmonLoad::prevsite $::cmonClient::var(site) ] } {
            set currPages [ lrange [ $::cmonLoad::notebook pages ] 0 end ]
            foreach page [ lrange $currPages 0 end ] {
                catch { $::cmonLoad::notebook delete $page }
            } 
            createPages $::cmonLoad::notebook
        }
    } err ] } {
        puts "err=$err"
        # appendStatus $::cmonLoad::statusw($page) "[myName ] $err"
    }
}

## ******************************************************** 
##
## Name: cmonLoad::createNodeList 
##
## Description:
## update the options button for the nodes since the site
## may have changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## updated from server setting array 
## args=gateway ::LDASmachines ldas-dev w

proc cmonLoad::createNodeList { parent page } {

    ;## update nodes in beowulf page 
    set site  $::cmonClient::var(site)
    if	{ ![ info exist ::beowulfNodes($::cmonClient::var(site)) ] } {
    	set ack [ tk_messageBox -type ok -default ok \ 
        	-message "$site may not have mpiAPI or beowulf nodes to support this function." \
            -icon info ]
        return
    }    
    frame $parent 
	label $parent.lbl1 -text "On node: " -justify left	
	label $parent.nodeName -text none -justify left	-fg blue 
    set nodelist $::beowulfNodes($::cmonClient::var(site))
    set nodelist [ lsort -dictionary $nodelist ]
    set mb [ createNodeWidget $parent ::cmonLoad::var($page,node) \
	    $nodelist $parent.nodeName ]
	set ::cmonLoad::wnodesframe($page) $parent
	set ::cmonLoad::var($page,node) [ lindex $nodelist 0 ]
	pack $parent.lbl1 $parent.nodeName $parent.mb -side left -padx 5 -pady 5 -anchor w
	pack $::cmonLoad::wnodesframe($page) -side left -anchor w -padx 5 -fill x -expand 1	

}    

## ******************************************************** 
##
## Name: cmonLoad::createNodeList 
##
## Description:
## recreates the nodelist when nodes are updated
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLoad::recreateNodes {} {

	set site [ string trim $::cmonClient::var(site) ]
    set page [ cmonLoad::setMPIhost ]
	if	{ [ info exist ::cmonLoad::wnodesframe($page) ] } {
		set nodelist [ set ::beowulfNodes($site) ]
		set nodelist [ lsort -dictionary $nodelist ]
		set parent $::cmonLoad::wnodesframe($page)
		set mb [ createNodeWidget $parent ::cmonLoad::var($page,node) \
	    	$nodelist $parent.nodeName ]
		pack $parent.mb -side left -padx 5 -pady 5 -anchor w
	}

}

## ******************************************************** 
##
## Name: cmonLoad::setMPIHost
##
## Description:
## determines the name of MPI host for sites that have not upgraded yet
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLoad::setMPIhost {} {

    set site [ string trim $::cmonClient::var(site) ]
    if  { [ info exist ::LDASmachines($site) ] } {
        if  { [ string first beowulf $::LDASmachines($site) ] > -1 } {
            set host beowulf
        } elseif { [ regexp {dev|test|cit|mit|wa|la} $site ] } {
		    set host beowulf
        } else {
		    set host $::MPI_API_HOST
	    }
    }
    return $host
}
