## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon client Tcl Script.
##
## This script has additional global functions used by packages
## in cmonClient
## some are imported from LDAS generic 
;#barecode
package provide cmonCommon2 1.0
namespace eval ComboBox {}

namespace eval cmonCommon {
    set fixedTimeIndex 8
	set timeModes { "LOCAL  " "GMT  " "GPS" }
	set timeoptions { "last 5 minutes" "last hour" "last 6 hours" \
	"last 12 hours" "last day" "last 2 days" "last 3 days" "last week" \
    "last Eng Run E10" "Science Run S1" "Science Run S2" "Science Run S3" \
    "last Eng Run E12" "Science Run S4" "specify times" }
	set histogramTypes { 1x 2x 10x log }
	set histHelpMsg "Histogram settings:\n\
bin size = range of a bin e.g. 10 msecs, 100 msecs\n\
histogram types:\n\
1x: each bin is 1x value, e.g. bin 0 (values 0-10), bin 1 (values 10-20) ...\n\
2x: each bin is 2x value, e.g. bin 0 (values 0-10), bin 2 (values 10-20), bucket 3 (values 20-40) ...\n\
10x: each bin is 10x value, e.g. bin 0 (values 0-10), bin 2 (values 10-100), bucket 3 (values 100-1000) ...\n\
log: each bin is log10 value of the other e.g. (TBD)"

	set histHelpMsg "Histogram settings:\n\
bin size = range of a bin e.g. a bin size of 10 will have bins:\n\
bin 0-10, 10-20, 20-30, .... etc."
}
;#end

## ******************************************************** 
##
## Name: createGPSTimeWidget
##
## Description:
## creates 2 label entry widgets for input of time in GPS,local or gmt
##
## Usage:
##       
##
## Comments:
## 

proc cmonCommon::createGPSTimeWidget { parent namespace page } {  

	$parent configure -relief ridge -borderwidth 2 
	set fstart [ frame $parent.fstart ]
    set s_ent   [LabelEntry $fstart.logstart -label "   From: " -labelwidth 10 -labelanchor w \
                   -textvariable ::cmonCommon::starttime -width 20 -editable 1 \
                   -helptext "Formats: MM/DD/YY HH:MM:YY, MM/DD/YY, MM/DD/YYYY,GPS seconds or now"]
    $s_ent bind <ButtonRelease-3> "set ::cmonCommon::starttime [ gpsTime now ]" 	     
    set timemenu [ eval tk_optionMenu $fstart.smode ::cmonCommon:stimeMode \
        $::cmonCommon::timeModes ]
	
	set numModes [ llength $::cmonCommon::timeModes ]	
	for { set i 0 } { $i < $numModes } { incr i 1 } {
		$timemenu entryconfigure $i -command "cmonCommon::to[ lindex $::cmonCommon::timeModes $i ] $namespace $page starttime"
	}	
    pack $s_ent $fstart.smode -side left 
 
    set fend [ frame $parent.fend ]
	set e_ent   [LabelEntry $fend.logend -label   "    To: " -labelwidth 10 -labelanchor w \
                   -textvariable ::cmonCommon::endtime -width 20 -editable 1 \
                   -helptext "Formats: MM/DD/YY HH:MM:YY, MM/DD/YY, MM/DD/YYYY, seconds or now" ]
				   
    $e_ent bind <ButtonRelease-3> "set ::cmonCommon::endtime [ gpsTime "now" ]"
    set timemenu [ eval tk_optionMenu $fend.emode ::cmonCommon::etimeMode $::cmonCommon::timeModes ]
		
	for { set i 0 } { $i < $numModes } { incr i 1 } {
		$timemenu entryconfigure $i -command "cmonCommon::to[ lindex $::cmonCommon::timeModes $i ] $namespace $page endtime"
	}	
    pack $e_ent $fend.emode -side left 
	set ::cmonCommon::starttime_prevmode [ lindex $::cmonCommon::timeModes 0 ]
	set ::cmonCommon::endtime_prevmode [ lindex $::cmonCommon::timeModes 0 ]
	cmonCommon::setLastTime $namespace $page
	return "$fstart $fend"
}

##******************************************************** 
##
## Name: cmonCommon::createTimeOptions  
##
## Description:
## creates time options for last min, hour , day etc
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## 

proc cmonCommon::createTimeOptions { parent name page } {

	set f [ frame $parent.fopts ]
	Label $f.seltime -text "For times: " -justify left \
		-helptext "specify time ranges" 

	set cmd "tk_optionMenu $f.timemenu ::cmonCommon::timeopt $::cmonCommon::timeoptions"
    set timemenu [ eval $cmd ]
  
	$timemenu entryconfigure 0 -command "cmonCommon::setLastTime $name $page 5 0"
	$timemenu entryconfigure 1 -command "cmonCommon::setLastTime $name $page 60 1"
	$timemenu entryconfigure 2 -command "cmonCommon::setLastTime $name $page 360 2"
	$timemenu entryconfigure 3 -command "cmonCommon::setLastTime $name $page 720 3"
	$timemenu entryconfigure 4 -command "cmonCommon::setLastTime $name $page 1440 4"
	$timemenu entryconfigure 5 -command "cmonCommon::setLastTime $name $page 2880 5"
	$timemenu entryconfigure 6 -command "cmonCommon::setLastTime $name $page 4320 6"
	$timemenu entryconfigure 7 -command "cmonCommon::setLastTime $name $page 10080 7"
    $timemenu entryconfigure 8 -command "cmonCommon::setRunTime E10 8"
    $timemenu entryconfigure 9 -command "cmonCommon::setRunTime S1 9"
    $timemenu entryconfigure 10 -command "cmonCommon::setRunTime S2 10"
    $timemenu entryconfigure 11 -command "cmonCommon::setRunTime S3 11"
    $timemenu entryconfigure 12 -command "cmonCommon::setRunTime E12 12"
    $timemenu entryconfigure 13 -command "cmonCommon::setRunTime S4 13"
	$timemenu entryconfigure 14 -command "cmonCommon::clearTime $name $page" 
	pack $f.seltime $f.timemenu -padx 2 -pady 1 -side left -anchor w
	return $f 
}

##******************************************************** 
##
## Name: cmonCommon::timeValidate  
##
## Description:
## validates that start time is <= end time
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## 
proc cmonCommon::timeValidate { page namespace } {

    if  { [ catch {	
        set starttime [ cmonCommon::convertTime [ set ::cmonCommon::starttime ] \
            [ set ::cmonCommon:stimeMode ] start ]
        set endtime [ cmonCommon::convertTime [ set ::cmonCommon::endtime ] \
            [ set ::cmonCommon::etimeMode ] end ]
        if	{ $starttime > $endtime } {   
            error "Start time must be <= end time"
        }
    } err ] } {
        return -code error $err
    }        
    return [ list $starttime $endtime ]
}

##******************************************************** 
##
## Name: cmonCommon::dbTimeStamp 
##
## Description:
## converts time into database timestamp
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## 
proc cmonCommon::dbTimeStamp { page namespace } {

    if  { [ catch {	
        set starttime [ cmonCommon::convertDBTime [ set ::cmonCommon::starttime ] \
            [ set ::cmonCommon:stimeMode ] start ]
        set endtime [ cmonCommon::convertDBTime [ set ::cmonCommon::endtime ] \
            [ set ::cmonCommon::etimeMode ] end ]
        if	{ $starttime > $endtime } {   
            error "Start time must be <= end time"
        }
    } err ] } {
        return -code error $err
    }        
    return [ list $starttime $endtime ]
}

##******************************************************** 
##
## Name: cmonCommon::convertTime   
##
## Description:
## Returns gpstime for the time string 
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## default time is gmt for gpsTime

proc cmonCommon::convertTime { time mode type } {

    if  { [ catch {
		set time [ string trim $time ]
		if	{ ! [ string length $time ] } {
			error "Enter a $type time value"
		}
        if  { [ regexp {LOCAL} $mode ] } {
            set gpstime [ utc2gps $time 0 ]
        } elseif { [ regexp {GMT} $mode ] } {
        	set gpstime [ utc2gps $time 1 ]
		} else {
			set gpstime $time
            if  { ! [ regexp {^\d+$} $gpstime ] } {
                error "Invalid GPS format"
            }
		}
    } err ] } {
        return -code error $err
    }
    return $gpstime
}

##******************************************************** 
##
## Name: cmonCommon::convertDBTime  
##
## Description:
## Returns database timestamp string for time 
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## default time is gmt for gpsTime

proc cmonCommon::convertDBTime { localtime mode type } {
    if  { [ catch {
		set time [ string trim $localtime ]
		if	{ ! [ string length $localtime ] } {
			error "Enter a $type time value"
		}
        if  { [ regexp {GPS} $mode ] } {
            set localtime [ gps2utc $time 0 ]
        } elseif { [ regexp {GMT} $mode ] } {
        	set localtime [ gmt2local $time 1 ]
		} 
    } err ] } {
        return -code error $err
    }
    return [ clock format [ clock scan $localtime ] -format "%Y%m%d%H%M%S" ]
}

##******************************************************** 
##
## Name: cmonCommon::toGPS   
##
## Description:
## Converts string in entry field to GPS time.
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::toGPS { namespace page field { fieldNamespace ::cmonCommon } } {
	if	{ [ catch {
    	if	{ ! [ regexp {^::} $fieldNamespace ] } {
        	set fieldNamespace "::$fieldNamespace"
        }
		if	{ [ string length [ set ${fieldNamespace}::$field ] ] } {	
			set prevmode [ set ${fieldNamespace}::${field}_prevmode ]
			set timestr [ string trim [ set ${fieldNamespace}::$field ] ]
			;## convert local time to gmt format
			if	{ [ regexp {LOCAL} $prevmode ] } {
				set ${fieldNamespace}::$field [ utc2gps $timestr 0 ]
			} elseif { [ regexp {GMT} $prevmode ] } { 
				set ${fieldNamespace}::$field [ utc2gps $timestr ]
			}
		}
	} err ] } {
		appendStatus [ set ::${namespace}::statusw($page) ] $err
		set ${fieldNamespace}::$field ""
	}
	set ${fieldNamespace}::${field}_prevmode GPS
}

##******************************************************** 
##
## Name: cmonCommon::toLOCAL   
##
## Description:
## Converts string in entry field to local time.
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::toLOCAL { namespace page field { fieldNamespace ::cmonCommon } } {
	if	{ [ catch {
    	if	{ ! [ regexp {^::} $fieldNamespace ] } {
        	set fieldNamespace "::$fieldNamespace"
        }
		if	{ [ string length [ set ${fieldNamespace}::$field ] ] } {
			set prevmode [ set ${fieldNamespace}::${field}_prevmode ]
			set timestr [ string trim [ set ${fieldNamespace}::$field ] ]
			;## from gps to local	
			if	{ [ regexp {GPS} $prevmode ] } {
				set  ${fieldNamespace}::$field [ gps2utc $timestr 0 ]
			} elseif { [ regexp {GMT} $prevmode ] } { 
				set ${fieldNamespace}::$field [ gmt2local $timestr ]
			}					 
		}
	} err ] } {
		appendStatus [ set ::${namespace}::statusw($page) ] $err
		set ${fieldNamespace}::$field ""
	}
	set ${fieldNamespace}::${field}_prevmode LOCAL
}

##******************************************************** 
##
## Name: cmonCommon::toGMT   
##
## Description:
## Converts string in entry field to GMT time.
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::toGMT { namespace page field { fieldNamespace ::cmonCommon } } {
	if	{ [ catch {
		if	{ [ string length [ set ${fieldNamespace}::$field ] ] } {
			set prevmode [ set ${fieldNamespace}::${field}_prevmode ]
			set timestr [ string trim [ set ${fieldNamespace}::$field ] ]
			;## from gps to local		
			if	{ [ regexp {GPS} $prevmode ] } {
				set  ${fieldNamespace}::$field [ gps2utc $timestr 1 ]
			} elseif { [ regexp {LOCAL} $prevmode ] } { 
				set  ${fieldNamespace}::$field [ local2gmt $timestr ]
			}	
		}
	} err ] } {
		appendStatus [ set ::${namespace}::statusw($page) ] $err
		set ${fieldNamespace}::$field ""
	}
	set ${fieldNamespace}::${field}_prevmode GMT
}

##******************************************************** 
##
## Name: cmonCommon::setLastTime  
##
## Description:
## set time widget to last day or hour
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## only set time mode if not called with minutes
## otherwise set time

proc cmonCommon::setLastTime { namespace page { minutes -1 } { index -1 } } {

	if		{ [ catch {
            if  { $minutes < 0 } {
                if  { ! [ info exist ::cmonCommon::timeindex ] } {
                    set ::cmonCommon::timeindex 1
                    set ::cmonCommon::minutes 60 
                    set minutes $::cmonCommon::minutes
                    set ::cmonCommon::timeopt [ lindex $::cmonCommon::timeoptions $::cmonCommon::timeindex ]
                } 
            } else {    
                set ::cmonCommon::timeindex $index
                set ::cmonCommon::minutes $minutes  
                set ::cmonCommon::timeopt [ lindex $::cmonCommon::timeoptions $::cmonCommon::timeindex ]
            }
            if  { $minutes > -1 && $::cmonCommon::timeindex < $::cmonCommon::fixedTimeIndex } {
			    set curtime [ clock seconds ]
			    set starttime [ expr $curtime - ( $minutes * 60 ) ]
			    ;## set start time
			    set mode [ set ::cmonCommon::starttime_prevmode ]
			    switch -glob -- $mode {	
				    GPS* {
					    set  ::cmonCommon::starttime [ gpsTime $starttime ]
				    }
				    LOCAL* {	
					    set ::cmonCommon::starttime  \
					    [ clock format $starttime -format "%x %X %Z" ]
				    }
				    GMT* {
					    set ::cmonCommon::starttime  \
					    [ clock format $starttime -format "%x %X %Z" -gmt 1 ]
				    }
				    default { error "no match" }
			    }
			    ;## set end time 
			    set mode [ set ::cmonCommon::endtime_prevmode ]
			    switch -glob -- $mode {	
				    GPS* {
					    set  ::cmonCommon::endtime [ gpsTime now ]
				    }
				    LOCAL* {	
					    set ::cmonCommon::endtime  \
					    [ clock format $curtime -format "%x %X %Z" ]
				    }
				    GMT* {
					    set ::cmonCommon::endtime  \
					    [ clock format $curtime -format "%x %X %Z" -gmt 1 ]
				    }
				    default { error "no match" }
			    }
            }				
	} err ] } {
		catch { appendStatus [ set ::${namespace}::statusw($page) ] $err }	
		set ::cmonCommon::starttime ""
		set ::cmonCommon::endtime ""
	}
}

##******************************************************** 
##
## Name: cmonCommon::clearTime  
##
## Description:
## set time widget to last day or hour
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::clearTime { namespace page } {

	set  ::cmonCommon::starttime ""
	set  ::cmonCommon::endtime ""
    set  ::cmonCommon::timeindex end 
    set ::cmonCommon::timeopt [ lindex $::cmonCommon::timeoptions end ]

}

##******************************************************** 
##
## Name: cmonCommon::setTime  
##
## Description:
## set time widget to last day or hour
##
## Parameters:
## page - page name
##
## Usage:
##  
## 
## Comments:
## used in plot.tcl and bltgraph.tcl

proc cmonCommon::setTime { starttime endtime } {

    foreach field { start end } {    
	    set prevmode [ set ::cmonCommon::${field}time_prevmode ]
		;## convert local time to gmt format
		if	{ [ regexp -nocase {LOCAL} $prevmode ] } {
			set ::cmonCommon::${field}time [ gps2utc [ set ${field}time ] 0 ]
		} elseif { [ regexp -nocase {GMT} $prevmode ] } { 
			set ::cmonCommon::${field}time [ gps2utc [ set ${field}time ] 1 ]
		} else {
            set ::cmonCommon::starttime $starttime
	        set ::cmonCommon::endtime 	$endtime
        }
    }
}

##******************************************************** 
##
## Name: cmonCommon::findDialog  
##
## Description:
## put up search dialog for text widget
##
## Parameters:
## textw 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::findDialog { namespace page parent textw } {

	if	{ [ catch { 
		set dialogw .${namespace}find$page
		if 	{ ! [ winfo exist $dialogw ] } {
			set dlg [Dialog $dialogw -parent $parent -modal none \
    		-separator 1 -title   "$page Find: " \
        	-side bottom -default 0 -cancel 3 ]
   	 		set bfind [$dlg add -name 0 -command "cmonCommon::findtext $namespace $page $textw" -text find ]
    		$dlg add -name cancel -command "destroy $dlg" 
    		set dlgframe [$dlg getframe]
			set findstr  [LabelEntry $dlg.find -label "String to find: " -labelwidth 15 -labelanchor w \
                   -textvariable ::${namespace}::var($page,findstr) -editable 1 -width 20 -labeljustify right \
                   -helptext "string to search for" ]

			set f1 [ frame $dlg.strtype -relief flat ]
			radiobutton $f1.literal -text "Literal" -variable ::${namespace}::var($page,findtype) \
			-value -nocase
			radiobutton $f1.caseliteral -text "Case Sensitive Literal" -variable ::${namespace}::var($page,findtype) \
			-value -exact
			radiobutton $f1.regexp -text "Regular Expression" -variable ::${namespace}::var($page,findtype) \
			-value -regexp
			pack $f1.literal $f1.caseliteral $f1.regexp -side left -padx 2 -pady 1 -anchor w
			set ::${namespace}::var($page,findtype) -nocase

			set f2 [ frame $dlg.finddir -relief flat ]
			radiobutton $f2.forward -text "Search Forward" -variable ::${namespace}::var($page,finddir) \
			-value -forward 
			radiobutton $f2.backward -text "Search Backward" -variable ::${namespace}::var($page,finddir) \
			-value -backward 
			set ::${namespace}::var($page,finddir) -forward
			pack $f2.forward $f2.backward -side left -padx 2 -pady 1 -anchor w
			pack $findstr $f1 $f2 -side top -expand 1 -fill x
			set ::${namespace}::var($page,textindex) 1.0
			$dlg draw 
		}
	} err ] } {
			set ack [ MessageDlg .cmonClientDlg -type ok -aspect 120 \
			-message $err -icon error ]
	}
}

##******************************************************** 
##
## Name: cmonCommon::findtext 
##
## Description:
## put up search dialog for text widget
##
## Parameters:
## textw 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::findtext { namespace page textw } {

	set finddir [ set ::${namespace}::var($page,finddir) ]
	set pattern [ set ::${namespace}::var($page,findstr) ]
	set findtype [ set ::${namespace}::var($page,findtype) ]
	set curindex  [ set ::${namespace}::var($page,textindex) ]
	
	if	{ ! [ regexp {(\S+)\s\+\s(\d+)\schars} $curindex -> startindex len ] } {
		set startindex $curindex
		set len 0
	}
	
	if	{ [ regexp -- {backward} $finddir ] } {
		set found [ $textw search -count cnt $finddir $findtype -- $pattern "$startindex - 1 chars" 1.0 ]
		set backward 1
	} else {
		set found [ $textw search -count cnt $finddir $findtype -- $pattern "$startindex + $len chars" end ]	
	}
	if	{ ! [ info exist cnt ] } {
		set cnt [ string length $pattern ]
	}
	if	{ [ string length $found ] } {
		$textw tag remove select $startindex "$startindex + $len chars"
		$textw tag add select $found "$found + $cnt chars"
		set ::${namespace}::var($page,textindex) "$found + $cnt chars"	
		$textw see $found 
	} else {
		bell 		
	}
}
 
##******************************************************** 
##
## Name: cmonCommon::busyDialog 
##
## Description:
## put up busy dialog 
##
## Parameters:
## textw 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::busyDialog { tag var parent } {
	
	set tag [ string tolower $tag ]
	return [ ProgressDlg $parent.$tag[clock seconds] -fg blue -bg gray \
		-textvariable $var -troughcolor blue ]
}

## ******************************************************** 
##
## Name: createDBCheckList
##
## Description:
## creates a checkbox for databases
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc cmonCommon::createDBCheckList { parent name page } {

	frame $parent.flab
	set lab [ label $parent.flab.lbl -text "Select one or more databases "  \
		-justify left -font $::LISTFONT -wraplength 300 ]
	set selectmenu [ eval tk_optionMenu $parent.flab.select ::${name}::var($page,select) \
        { "select all" "deselect all" } ]
	$selectmenu entryconfigure 0 -command [ list DBselect $name $page 1 $lab ]
	$selectmenu entryconfigure 1 -command [ list DBselect $name $page 0 $lab ]
	pack $parent.flab.select -side right
	pack $parent.flab.lbl -side left -fill x -expand 1
	pack $parent.flab -side top -fill x -expand 1
	set sw [ScrolledWindow $parent.sw -relief sunken -borderwidth 2]
	set sf [ScrollableFrame $sw.fdb -height 20 -areaheight 0 ]
	$sw setwidget $sf
	set subfdb [ $sf getframe ]
	frame $subfdb.fleft 
	frame $subfdb.fright 
	
	set site [ string trim $::cmonClient::var(site) ]
	set dblist [ set ::dbnames($site) ]
    set numdbs [ llength $dblist ]
    if  { $numdbs == 1 } {
         set ack [ tk_messageBox -type ok -default ok \
			-message "There is only one database so nothing to compare." -icon info ]
        return 0
    }
	set halfindex [ expr ( [ llength $dblist ] / 2 ) - 1 ]
	set dbleft [ lrange $dblist 0 $halfindex ] 
	set dbright [ lrange $dblist [ expr $halfindex + 1 ] end ]
   
	foreach db $dbleft {
		checkbutton $subfdb.fleft.$db -text $db -width 9 -anchor w \
        -variable ::${name}::var($page,$db) -onvalue 1 -offvalue 0 \
        -font $::LISTFONT 
		set ::${name}::var($page,$db) 1
		pack $subfdb.fleft.$db -side left -anchor w 
	}
	foreach db $dbright {
		checkbutton $subfdb.fright.$db -text $db -width 9 -anchor w \
        -variable ::${name}::var($page,$db) -onvalue 1 -offvalue 0 \
        -font $::LISTFONT 
		set ::${name}::var($page,$db) 1
		pack $subfdb.fright.$db -side left -anchor w 
	} 
	pack $subfdb.fleft $subfdb.fright -side top -expand 1 -fill both
	pack $sw -anchor w -fill both -expand 1 -padx 2	
	$sf see $subfdb.fleft.[ lindex $dblist 0 ]
	$selectmenu entryconfigure 0 -command [ list DBselect $name $page 1 $dblist ]
	$selectmenu entryconfigure 1 -command [ list DBselect $name $page 0 $dblist ]
	set ::${name}::dblist($page) $dblist
    return 1
}

## ******************************************************** 
##
## Name: DBselect 
##
## Description:
## turn on or off API checkboxes
## update the title to reflect this 
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc DBselect { name page value dblist } {

	foreach db $dblist {
		set ::${name}::var($page,$db) $value
	}
}

## ******************************************************** 
##
## Name: setDBs 
##
## Description:
## turn on or off API checkboxes
## update the title to reflect this 
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc setDBs { name page } {
	
	set selected [ list ]
	foreach db [ set ::${name}::dblist($page) ] {
		set value  [ set ::${name}::var($page,$db) ]
		if	{ $value } {
			lappend selected $db
		}
	}
	set ::${name}::var($page,dblist) $selected
}

## ******************************************************** 
## the following procs are related to least squares fit
## ********************************************************

## ******************************************************** 
##
## Name:  
##
## Description:
## initialize for least square 
##
## Parameters:
##
## Comments:

proc cmonCommon::leastSquaresInit {} {

	uplevel {
		catch { unset xlist }
		catch { unset ylist }
		catch { unset newpoints }
		catch { unset a }
		catch { unset b }
		catch { unset xdelta  }
		catch { unset ydelta }
	}

}


## ******************************************************** 
##
## Name:  
##
## Description:
## compute the a and b values for least square fit 
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Algorithm given by Stuart Anderson from:
## See equation 15.2.15 - 15.2.18 in the Second Edition of
## Numerical Recipes in C (ISBN 0-521-43108-5)
## 
## The basic idea is to subtract the mean value from the x numbers
## so that you do not end up subtracting two large numbers to get a small
## one for the answer.

proc cmonCommon::leastSquareCoefficients  {} {

         uplevel {
                 ;## split into x and y
                 set xlist [ list ]
                 set ylist [ list ]

                 set sumx [ expr double(0) ]
                 set sumy [ expr double(0) ]
                 set sumtsq  [ expr double(0) ]
                 set sumty [ expr double(0) ]
                 
                 if { [ expr [llength $points] % 2 ] != 0 } {
                         ;## An odd number of points was found in the list
                         ;## This is an internal error condition that
                         ;## should print a fatal message
					error "Data set does not have even number of points"
                 }

                 set numItems [ expr [ llength $points ] / 2 ]
                 foreach { x y } $points {
                         set x [ expr double($x) ]
                         set y [ expr double($y) ]
                         lappend xlist $x
                         lappend ylist $y
                         set sumx [ expr $sumx + $x ]
                         set sumy [ expr $sumy + $y ]
                 }

                 set meanx  [ expr $sumx / $numItems ]
                 foreach { x y } $points {
                         set t [ expr double($x) - $meanx ]
                         set sumtsq [ expr $sumtsq + ( $t * $t ) ]
                         set sumty [ expr $sumty + ( $t * $y ) ]
                 }

                 if      { $sumtsq != 0.000000 } {
                         set b [ expr $sumty / $sumtsq ]
                         set a [ expr ($sumy - $b*$sumx) / $numItems ]
                 } else {
                         set b 0.0
                         set a [ expr $sumy/$numItems ]
                 }
				 set sigma [ expr sqrt(1.0 / $sumtsq) ]
				 set sumchi2 0.0
				 foreach { x y } $points {
				 	set tempsum [ expr $y - $a - $b * $x ]
				 	set sumchi2 [ expr $sumchi2 + $tempsum * $tempsum ]
				 }
				 set sigdat [ expr sqrt($sumchi2/($numItems-2)) ]
				 set sigma  [ expr $sigma * $sigdat ] 
         }
}

## ******************************************************** 
##
## Name: leastSquarePoints
##
## Description:
## compute new data set based on known a and b
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:
## This is done in the context of caller

proc cmonCommon::leastSquarePoints {} {
	uplevel {
		set newpoints [ list ]
		if	{ $a == 0.00000 && $b == 0.00000 } {
			set constanty [ lindex $data 1 ]
			set x [ lindex $xlist 0 ]
			set y $constanty 
			lappend newpoints $x 
		    lappend newpoints $y
			set x [ lindex $xlist end ]
			set y $constanty 
			lappend newpoints $x
			lappend newpoints $y	
			if	{ [ info exist ymin ] } {
				if	{ $y < $ymin } {
					set ymin $y
				}
		  	} else {
				set ymin $y
		  	}
			set y [ expr $y + 10.0 ]
	 	  	if	{ [ info exist ymax ] } {
				if	{ $y > $ymax } {
					set ymax $y
				}
		  	} else {
				set ymax $y
		  	}	
		} else {
		  set x1 [ lindex $xlist 0 ]
		  set x2 [ lindex $xlist end ]
		  foreach x [ list $x1 $x2 ] {
		  	set y [ expr $a + $b * $x ] 
		  	lappend newpoints $x
		  	lappend newpoints $y			
		  	if	{ [ info exist ymin ] } {
				if	{ $y < $ymin } {
					set ymin $y
				}
		  	} else {
				set ymin $y
		  	}
	 	  	if	{ [ info exist ymax ] } {
				if	{ $y > $ymax } {
					set ymax $y
				}
		  	} else {
				set ymax $y
		  	}
		  }
		}
	}
}

## ******************************************************** 
##
## Name: computeYMin
##
## Description:
## compute ymin for graph
## coefficients
##  y = a + bx
##
## Parameters:
##
## Comments:

proc cmonCommon::computeYMinMax { set ymin ymax } {

	foreach { x y } $set {	
		if	{ $ymin > $y } {
			set ymin $y
		}
		if	{ $y > $ymax } {
			set ymax $y
		} 
	}
	return "$ymin $ymax"
}


## ******************************************************** 
##
## Name: computeXYMinMax
##
## Description:
## compute ymin for histogram limits
##
## Parameters:
##
## Comments:
## set ymin to 1 so bltgraph log would not complain

proc cmonCommon::computeXYMinMax { set binsize } {

    set ymin 0
    set ymax 0
    if  { ! [ llength $set ] } {
        return "0 1 10 10"
    }
    set xlist [ list ]
    set ylist [ list ]
	foreach { x y } $set {
        lappend xlist $x
        lappend ylist $y
    }
    set xlist [ lsort -real $xlist ]
    set ylist [ lsort -real $ylist ]

    set xmin [ lindex $xlist 0 ]
    set ymin [ lindex $ylist 0 ]
    
    set xmax [ expr [ lindex $xlist end ] + $binsize ]
    set ymax [ lindex $ylist end ]
       
    ;## ymin and ymax are not actually for y scale with log option
    if	{ [ expr abs($xmax -$xmin) ] < 1 || [ llength $xlist ] < 2 } { 
	    set xmax [ expr round($xmax) + $binsize ]
	}
    ;## ymin and ymax are not actually for y scale with log option
    if	{ [ expr abs($ymax -$ymin) ] < 1 } { 
	    set ymax [ expr round($ymax) + 2 ]
	}
	return "$xmin $ymin $xmax $ymax"
}

## ******************************************************** 
##
## Name: slope
##
## Description:
## compute slope for a least square fitted line
##
##
## Parameters:
##
## Comments:

proc cmonCommon::slope { data } {
	
	set xstart [ lindex $data 0 ]
	set ystart [ lindex $data 1 ]
	set num [ llength $data ]
	set xend [ lindex $data [ expr $num - 2 ] ]
	set yend [ lindex $data end ]
	#puts "xstart=$xstart, ystart=$ystart, xend=$xend yend=$yend, xdelta [ expr $xend - $xstart ], ydelta=[ expr $yend - $ystart ]"
	return "[ expr $xend - $xstart ] [ expr $yend - $ystart ]"
	
}

## ******************************************************** 
##
## Name: cmonCommon::fit2points
##
## Description:
## fit 2 points and compute 2 slope 
##
##
## Parameters:
##
## Comments:

proc cmonCommon::fit2points {} {
	uplevel {
		;## split into x and y
		set numItems 0.0
		set xlist [ list ]
		set ylist [ list ]
	
		foreach { x y } $points {
			set x [ expr $x * 1.0 ]
			set y [ expr $y * 1.0 ]
			lappend xlist $x
			lappend ylist $y
		}
		set xstart [ lindex $xlist 0 ]
		set xend [ lindex $xlist end ]
		set ystart [ lindex $ylist 0 ]
		set yend [ lindex $ylist end ]
		set b [ expr ( $yend - $ystart )/( $xend - $xstart ) * 1.0 ]
		set a [ expr $ystart - ( $b * $xstart ) ]	
	}
}

## ******************************************************** 
##
## Name: cmonCommon::roundYaxis 
##
## Description:
## round up the yaxis min and max
##
##
## Parameters:
##
## Comments:

proc cmonCommon::roundYaxis {} {

	uplevel { 
		if	{ !$ymin && !$ymax || $ymax < 5 } {
			set ymin 0
			set ymax 5
		} else {
			set ymax [ expr round($ymax*$::YMAXSCALE)  ]
			set ymin [ expr round($ymin*$::YMINSCALE) ]
		}
	}
}

### ends code for least squares fit

## redefine some binding callbacks for extended select
## ******************************************************** 
##
## Name: cmonCommon::listSelectMode
##
## Description:
## creates usercmd selection 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## adapted from BWidget ComboBox::_select

proc ComboBox::myselect { path index } {
	regexp {(.+).shell} $path -> path
    set index [$path.shell.listb index $index]
    # _unmapliste $path
    if { $index != -1 } {
        if { [ setvalue $path @$index] } {
	    set cmd [Widget::getMegawidgetOption $path -modifycmd]
            if { $cmd != "" } {
                uplevel \#0 $cmd
            }
        }
    }
    #$path.e selection clear
    #$path.e selection range 0 end
    return -code break
}

## ******************************************************** 
##
## Name: cmonCommon::listSelectMode
##
## Description:
## creates usercmd selection 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 
proc cmonCommon::listSelectMode { path } {

    set listb $path.shell.listb
    $listb configure -selectmode extended
    ::bind $listb <ButtonRelease-1> "ComboBox::myselect $path @%x,%y"
    # ::bind $listb <ButtonPress-3>   "ComboBox::_unmapliste $path"  
    ::bind $listb <Leave>   "ComboBox::_unmapliste $path"  
    
}

## ******************************************************** 
##
## Name: cmonCommon::ldasdsos
##
## Description:
## creates combo with standard list of dsos
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## also triggered when server sents updates

proc cmonCommon::ldasDSOs { combo name page state args } {

    if  { [ catch {
        catch { destroy $combo }
        set edit 0
        if  { [ string equal normal $state ] } {
            set edit 1
        }
        set dsolist [ concat all $::DSOs user_specified ]
	    set cmd_ent   [ ComboBox $combo \
		    -text "By DSO (dataPipeline only): " -width 25 \
    	    -textvariable ::${name}::var($page,dso) -editable $edit \
		    -values $dsolist -state $state \
            -helptext "Dynamic Shared Objects for dataPipeline" \
		    -modifycmd [ list cmonCommon::setLDASdso $combo $name $page ] ]
	    pack $combo -side left -anchor w -fill x -expand 1
	    $combo setvalue first
	    set ::${name}::dsow($page) $combo
        set command [ ${combo}.a cget -command ]
	    ${combo}.a configure -command "$command; cmonCommon::listSelectMode $combo"
    } err ] } {
        puts $err
    }
}

## ******************************************************** 
##
## Name: cmonCommon::ldascmds
##
## Description:
## creates usercmd selection 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonCommon::ldascmds { name page parent } {

	set f1 [ frame $parent.f1 -relief groove -borderwidth 2 ]

	;## select ldas cmds
	set fldascmds [ frame $f1.fldascmds ]
    set lab [ label $fldascmds.lab -text "By command: " -width 25 -justify left -anchor w ]
	set cmd_ent   [ ComboBox $fldascmds.cmd \
		-text "By command: " -width 15 \
    	-textvariable ::${name}::var($page,ldascmds) -editable 0 \
		-values $::ldascmds \
        -helptext "LDAS user cmds" -modifycmd "cmonCommon::setLDAScmd $fldascmds.cmd $name $page" ]
	pack $lab  -side left -anchor w 
	pack $fldascmds.cmd -side left -anchor w -fill x -expand 1
	pack $fldascmds -side top -anchor w -fill both -expand 1
	$fldascmds.cmd setvalue first
	
	;## select datapipeline dsos
	
	set fdso [ frame $f1.fdso ]
	set lab [ label $fdso.lab -text "By DSO(dataPipeline only): " -width 25 -justify left -anchor w ]
    pack $fdso.lab -side left -anchor w 
    cmonCommon::ldasDSOs $fdso.dso $name $page disabled
	pack $fdso -side top -anchor w -fill both -expand 1
    trace variable ::DSOs w [ list cmonCommon::ldasDSOs $fdso.dso $name $page disabled ]

	set but [ button $f1.view -text "View Selection" \
		-command "cmonCommon::showCmdsDsos $name $page" ]
	pack $but -side top -anchor w -padx 5
	pack $f1 -side top -fill both -expand 1 
	
	;## configure listbox to be extended select instead of browse
	set command [ $fldascmds.cmd.a  cget -command ]
	$fldascmds.cmd.a configure -command "$command; cmonCommon::listSelectMode $fldascmds.cmd"
	set command [ $fdso.dso.a cget -command ]
	$fdso.dso.a configure -command "$command; cmonCommon::listSelectMode $fdso.dso"
	
	return $fldascmds.cmd 
}	

## ******************************************************** 
##
## Name: cmonCommon::setLDAScmd
##
## Description:
## handle ldas cmd selection from combo
##
## Parameters:
##
## Comments:

proc cmonCommon::setLDAScmd { combo name page } {

    $combo setvalue @[ $combo getvalue ] 
	set listb $combo.shell.listb
	set selected [ $listb curselection ]

    set numselected [ llength $selected ]
    if  { $numselected == [ $listb size ] } {
        set items all
    } else {
	    set items [ list ]
        foreach item $selected {
            set itemtxt [ $listb get $item ]
            if  { [ string match all $itemtxt ] } {
                set items all
                break
            }
    	    lappend items $itemtxt
        }
	    set items [ join $items  ]
	}
    set ::${name}::var($page,ldascmds) $items
	set dso [ set ::${name}::var($page,dso) ]
	set dsow [ set ::${name}::dsow($page) ] 
	if	{ [ regexp -nocase {dataPipeline|dataStandAlone|other|user-specified} $items ] } {
		$dsow configure -state normal 
		$combo configure -editable 1
	} else {
		$dsow configure -state disabled
		$combo configure -editable 0
	}
}

## ******************************************************** 
##
## Name: cmonCommon::setLDASdso
##
## Description:
## creates a checkbox for APIs
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc cmonCommon::setLDASdso { combo name page } {

    $combo setvalue @[ $combo getvalue ] 
	set listb $combo.shell.listb
	set selected [ $listb curselection ]
    set numselected [ llength $selected ]
   
    if  { $numselected == [ $listb size ] } {
        set items all
    } else {
	    set items [ list ]
        foreach item $selected {
            set itemtxt [ $listb get $item ]
            if  { [ string match all $itemtxt ] } {
                set items all
                break
            }
    	    lappend items $itemtxt
        }
	    set items [ join $items  ]
    }
	set ::${name}::var($page,dso) $items
	if	{ [ regexp -nocase {other|user-specified} $items ] } {
		$combo configure -editable 1
	} else {
		$combo configure -editable 0
	}
}

## ******************************************************** 
##
## Name: cmonCommon::showCmdsDsos
##
## Description:
## show the selected ldas cmds and DSOs
##
## Parameters:
##
## Comments:

proc cmonCommon::showCmdsDsos { name page { showmsg 1 }} {
	set cmds [ set ::${name}::var($page,ldascmds) ]
	set text "LDAS commands:\t$cmds\t"
	if	{ [ regexp -nocase -- {dataPipeline|dataStandAlone} $cmds ] } {
        set dsolist [ set ::${name}::var($page,dso) ]
		append text "\nDSO:\t$dsolist"
	}
    if  { $showmsg } {
	    set ack [ tk_messageBox -type ok -default ok \
		    -message $text -icon info ]
    } else {
        return $text
    }
}

## ******************************************************** 
##
## Name: cmonCommon::showCmdsDsos
##
## Description:
## show the selected ldas cmds and DSOs
##
## Parameters:
##
## Comments:

proc cmonCommon::setCmdDsoPattern { name page } {

	set cmdpat ""
	if	{ [ catch {
		if	{ [ info exist ::${name}::var($page,ldascmds) ] } {
			set cmds [ set ::${name}::var($page,ldascmds) ]
			set cmds [ split $cmds  ]
			foreach cmd $cmds {
				if	{ [ string match all $cmds ] } {
					set cmdpat .+
					break
				}
				if  { [ regexp {(dataPipeline)} $cmd ] } {
					set dsos [ set ::${name}::var($page,dso) ]
                    set dsopat ""
					if	{ ! [ string match all $dsos ] } {
                        if  { [ string length $dsos ] } {
						    foreach dso [ split $dsos  ] {
							    append dsopat ".+$dso.+|"
						    }
                        }
                        append cmdpat "$cmd\([ string trim $dsopat \| ]\)|"
					} else {
                        ;## all dsos
						append cmdpat "$cmd|" 
					}
				} else {
                    ;## non dataPipeline
					append cmdpat "$cmd|"
				}
			}
			set cmdpat [ string trim $cmdpat \| ]
		}
	} err ] } {
		return -code error $err
	}
    set ::${name}::cmdpat($page) $cmdpat
	return $cmdpat
}

##******************************************************* 
##
## Name: cmonCommon::histHelp
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::histHelp { page } {

	catch { set ack [ MessageDlg .histogramtips -title Tips -type ok -aspect 300 \
		-message $::cmonCommon::histHelpMsg -icon info -justify left -font $::MSGFONT ] }
		
}

##******************************************************* 
##
## Name: cmonCommon::histHelp
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::histSpin { name page binsize } {

	;## force it to 10 if blank
    set selected_bin [ set ::${name}::var($page,bucketsz) ] 
	if 	{ [ regexp {(?n)^[\s\t\n]*$} $selected_bin ] } { 
		set ::${name}::var($page,bucketsz) $binsize
	} elseif { $selected_bin <= 0 } {
        set ::${name}::var($page,bucketsz) 1
    }
}

proc cmonCommon::histSpinNew { name page var min max } {

	;## force it to 1 if blank
    
    set currvalue [ set ${var} ]
	if 	{ [ regexp {(?n)^[\s\t\n]*$} $currvalue ] || \
          ! [ regexp {^[\.\d]+$} $currvalue ] } { 
		set $var $min
	} elseif { $currvalue < $min } {
        set $var $min
    } elseif { $currvalue > $max } {
        set $var $max
    }
    update idletasks
}

## ******************************************************** 
##
## Name: cmonCommon::createHistogramSettings
##
## Description:
## creates 2 label entry widgets for input of time in GPS,local or gmt
##
## Usage:
##       
##
## Comments:
## 

proc cmonCommon::createHistogramSettings { parent name page min max { binsize 1 } { relief ridge } { color gray } } {  

    set fhist [ frame $parent.fhist -relief $relief -borderwidth 2 -bg $color ]
	set f1 [ frame $fhist.f1 -bg $color -relief $relief ]
	set f2 [ frame $fhist.f2 -bg $color -relief $relief ]
	;## type of histogram
    ;## set s_ent   [LabelEntry $f1.type -label "types: " -labelwidth 10 -labelanchor w \
    ;##               -textvariable ::${name}::histtype($page) -width 20 -editable 1 \
    ;##               -helptext "Histogram types of 1x, 2x, 10x or log value of buckets"] 	     
    ;## set histmenu [ eval tk_optionMenu $f1.histtypes ::${name}::histtype($page) \
    ;##    $::cmonCommon::histogramTypes ]
	;## set ::${name}::histtype($page) [ lindex $::cmonCommon::histogramTypes 0 ]
	
	;## bucket size
    regexp {(.[^.]+).} $parent -> toplevel

	set lab [ Label $f1.lab -text "Bin size: " -width 10 -justify left -anchor w \
        -helptext "Valid range: $min - $max" ]
	set spin  [SpinBox $f1.bucketsz -range {1 500 1} -textvariable ::${toplevel}(bucketsz) \
                   -helptext "size of histogram bucket" -width 5 ]
    regexp {(.[^.]+).} $parent -> toplevel
	set but  [ button $f2.rebin -text "Re-bin" -command "cmonCommon::histSpinNew $name $page ::${toplevel}(bucketsz) $min $max; \
        eval [ set ::${toplevel}(replot) ]" -bg $color ]  
        
	set ::${parent}(bucketsz) $binsize
      
	;## force it to default if blank
	$spin bind <Leave> "cmonCommon::histSpinNew $name $page ::${toplevel}(bucketsz) $min $max"
	
	;## help text 
	set help [ Button $f2.tips -image $::image_tips -width 10 \
		-helptext "Histogram tips" \
        -command  "cmonCommon::histHelp $page" ]
    pack $lab -side left
    pack $spin -side right -fill x -expand 1
    pack $but $help -side left -fill x -expand 1
    pack $f1 $f2 -side top -fill both -expand 1
    pack $fhist -fill x -expand 1
	return $fhist
}

## ******************************************************** 
##
## Name: cmonCommon::getHistType
##
## Description:
## returns the counter package option for a type of histogram
##
## Usage:
##       
##
## Comments:
## 

proc cmonCommon::getHistType { name page } {  

	set type [ set ::${name}::histtype($page) ] 
	switch $type {
		1x { return "-hist" }
		2x { return "-hist2x" }
		10x { return "-hist10x" }
		log { return "-histlog" }
		default { return "-hist" }
	}
}

## ******************************************************** 
##
## Name: createAPIRadioList
##
## Description:
## creates a checkbox for APIs
##
## Parameters:
##
## Comments:
## This is done in the context of caller
## Assume caller has vars set for parent, name, page 

proc cmonCommon::createAPIRadioList { parent name page } {

	frame $parent.flab
	set subfapi [ frame $parent.fapi -relief ridge -borderwidth 1 ]
	set apilist [ set ::${name}::APIs ]
	set numAPIs [ llength $apilist ]
	set apirows [ list ]
	for { set i 0 } { $i < $numAPIs } { incr i 3 } {
		if	{ [ catch {
			set apirow$i [ lrange $apilist $i [ expr $i + 2 ] ]
		} err ] } {
			set apirow$i [ lrange $apilist $i end ]
		}
		lappend apirows apirow$i
	}
	foreach apirow $apirows {
		set f [ frame $subfapi.f$apirow ]
		foreach api [ set $apirow ] {
			radiobutton $f.$api -text $api -width 9 -anchor w \
        	-variable ::${name}::var($page,api) -value $api \
        	-font $::LISTFONT 
			pack $f.$api -side left -anchor w 
		}
		pack $f -side top -anchor w -expand 1 -fill both
	}
	set ::${name}::var($page,api) datacond
	pack $subfapi -fill both -expand 1

}

##*******************************************************
##
## Name: cmonCommon::showRegexp
##
## Description:
## derive regular expression from user input
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::showRegexp { statusw args } {

	set namespace [ lindex $args 0 ]
	set page [ lindex $args 1 ]
	set type [ set ::${namespace}::var($page,regtype) ]
	set var  ::${namespace}::var($page,$type)
	catch { set msg [ string trim [ set $var ] ] }
	switch $type {
		and  { set $var [ join $msg & ] ; ::updateStatus $statusw [ set $var ] }
		literal { set $var $msg ; ::updateStatus $statusw [ set $var ] }
		or 	 { set $var [ join $msg | ] ; ::updateStatus $statusw [ set $var ] }
		not	 {  if	{ ! [ regexp {^!!} $msg ] } {
					set msg "!!$msg"
				}
				set $var $msg ;
				::updateStatus $statusw [ set $var ] 
			 }
		any  { set msg [ $statusw get 0.0 end ]; set $var [ string trim $msg \n ] }
		default {} 
	}
	set ::${namespace}::var($page,pattern) [ set $var ]
}

##*******************************************************
##
## Name: cmonCommon::clearRegexp
##
## Description:
## clear regular expression from all fields
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::clearRegexp { statusw args } {
	set namespace [ lindex $args 0 ]
	set page [ lindex $args 1 ]
	foreach type [ list and literal or not ] {
		set ::${namespace}::var($page,$type) ""
	}
	set ::${namespace}::var($page,regtype) ""
	$statusw delete 0.0 end
}

##*******************************************************
##
## Name: cmonCommon::regexpDialog
##
## Description:
## dialog to support user's input to build regular expression
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::regexpDialog { namespace page parent } {
	
	set dialogw .regexp${namespace}$page
	
	if 	{ ! [ winfo exist $dialogw ] } {
		set dlg [Dialog $dialogw -parent $parent -modal none \
    		-separator 1 -title   "$page Regular Expression builder: " \
        	-side bottom -default 0 -cancel 3 ]
		
    	set dlgframe [ $dlg getframe ]
		set lbl [ label $dlgframe.help -text "Enter your words and hit Return to compile regular expression:" \
			-wraplength 400 -justify left ]
		set casebut [ checkbutton $dlgframe.optcase -text "case sensitive" \
        	-variable ::cmonLogs::var($page,regexpOpt) -onvalue "" -offvalue -nocase ]
		set ::${namespace}::var($page,regexpOpt) -nocase
		set f1 [ frame $dlgframe.ffindstr1 ]
		set r1 [ radiobutton $f1.and -text "" -variable ::${namespace}::var($page,regtype) \
			-value and ]
		set findstr1  [ LabelEntry $f1.find -label "with all of the words: " -labelwidth 27 -labelanchor w \
        	-textvariable ::${namespace}::var($page,and) -editable 1 -width 40 -labeljustify right \
            -helptext "and condition" ]
		pack $r1 $findstr1 -side left -anchor w
		$findstr1 bind <KeyRelease> "set ::${namespace}::var($page,regtype) and"
		
		set f2 [ frame $dlgframe.ffindstr2 ]
		set r2 [ radiobutton $f2.literal -text "" -variable ::${namespace}::var($page,regtype) \
			-value literal ]
		set findstr2  [ LabelEntry $f2.find -label "with the exact phrase: " -labelwidth 27 -labelanchor w \
            -textvariable ::${namespace}::var($page,literal) -editable 1 -width 40 -labeljustify right \
            -helptext "literal" ]
		pack $r2 $findstr2 -side left -anchor w	  
		$findstr2 bind <KeyRelease> "set ::${namespace}::var($page,regtype) literal"
		
		set f3 [ frame $dlgframe.ffindstr3 ]
		set r3 [ radiobutton $f3.or      -text "" -variable ::${namespace}::var($page,regtype) \
			-value or ]
		set findstr3  [ LabelEntry $f3.find -label "with at least one of the words: " -labelwidth 27 -labelanchor w \
            -textvariable ::${namespace}::var($page,or) -editable 1 -width 40 -labeljustify right \
            -helptext "or condition" ]
		pack $r3 $findstr3 -side left -anchor w	
		$findstr3 bind <KeyRelease> "set ::${namespace}::var($page,regtype) or"
		
		set f4 [ frame $dlgframe.ffindstr4 ]
		set r4 [ radiobutton $f4.not      -text "" -variable ::${namespace}::var($page,regtype) \
			-value not ]
		set findstr4  [ LabelEntry $f4.find -label "without the words: " -labelwidth 27 -labelanchor w \
            -textvariable ::${namespace}::var($page,not) -editable 1 -width 40 -labeljustify right \
            -helptext not ]
		pack $r4 $findstr4 -side left -anchor w	
		$findstr4 bind <KeyRelease> "set ::${namespace}::var($page,regtype) not"
		
		set f5 [ frame $dlgframe.ffindstr5 ]
		set r5 [ radiobutton $f5.any -text "Enter your own regular expression ( use !! for NOT, e.g. !!patty ):" -variable ::${namespace}::var($page,regtype) \
			-value any ]
		pack $r5 -side top -anchor w
		set statusw [ ::createStatus $f5 ]
		pack $lbl $casebut $f1 $f2 $f3 $f4 $f5 -side top -pady 1 -anchor w
		bind $dialogw <Return> ""

		$findstr1 bind <Return> "cmonCommon::showRegexp $statusw $namespace $page"
		$findstr2 bind <Return> "cmonCommon::showRegexp $statusw $namespace $page"
		$findstr3 bind <Return> "cmonCommon::showRegexp $statusw $namespace $page"
		$findstr4 bind <Return> "cmonCommon::showRegexp $statusw $namespace $page"
		bind $statusw <KeyPress> "set ::${namespace}::var($page,regtype) any; $statusw configure -state normal"
		bind $statusw <Leave> "$statusw configure -state disabled"
		$dlg add -name done  -text "Done" -command "cmonCommon::showRegexp $statusw $namespace $page; destroy $dlg" 
		$dlg add -name clear -text "Clear" -command "cmonCommon::clearRegexp $statusw $namespace $page" 
	   	$dlg add -name cancel -command "set ::${namespace}::var($page,pattern) {} ; destroy $dlg" 
		;## cmonCommon::clearRegexp $statusw $namespace $page
		$dlg draw
	} 
}

##*******************************************************
##
## Name: cmonCommon::removeLabelBLT
##
## Description:
## dialog to support user's input to build regular expression
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::removeLabel { page graph parent } {

	set label $parent.lbl
	if	{ [ winfo exist $label ] } {
    	destroy $label 
	}
}

##*******************************************************
##
## Name: cmonCommon::createRecordWidget
##
## Description:
## creates the widget for recording reason for action
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::createRecordWidget { parent name page } {

	set comment_ent  [LabelEntry $parent.comment  -label "Reason for your action: " -labelanchor w \
    	-textvariable ::${name}::var($page,comment) -width 30 -editable 1 -labeljustify left  \
        -font $::MSGFONT -entryfg blue ]
    
    return $comment_ent
}

##*******************************************************
##
## Name: cmonCommon::notifyPrint
##
## Description:
## creates widget for notifying printing in progress
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::notifyPrint { out parent { msg "" } } {

	 if	{ ! [ info exist ::PRINTCMD ] } {
	 	set printcmd lpr
	 } else {
	 	set printcmd $::PRINTCMD
	 }
	 set cmd "exec $printcmd $out &"
	 if	{ [ file exist $out ] && [ file size $out ] } {
	 	catch { eval $cmd } err
		set msg	"Printing [ file size $out ] bytes"
		set win $parent[clock seconds]
		set icon info
		set image [ Bitmap::get $icon ]
		if	{ ! [ winfo exist $win ] } {
			set dlg [ Dialog $win -parent . -modal none \
          		-separator 1 -title  $icon \
        		-side bottom -anchor  s -default 0 -image $image ]
        	$dlg add -name ok -anchor s -command [ list destroy $dlg ]
			set dlgframe [ $dlg getframe ]
			message $dlgframe.msg -aspect 300 -text $msg \
					-justify left -font $::MSGFONT
			pack $dlgframe.msg -side top 
			$dlg draw 
			after 10000 [ list destroy $dlg ]
		}
	 	after 60000 "catch { file delete $out }"
	} else {
		return -code error "No postscript file $out was generated: $msg"
	}
}

##*******************************************************
##
## Name: cmonCommon::getHistCummData
##
## Description:
## generate cummulative data for histogram option
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::getHistCummData { histdata binsize } {

    set cumtotal 0.0
    set y_total 0.0

	if	{ [ catch {
        foreach { x y } $histdata {
            set y_total [ expr $y + $y_total ]
        }
		foreach { x y } $histdata {
            set cumtotal [ expr $y + $cumtotal ]
            set newdata1($x) [ expr $cumtotal*100.0/$y_total ]
		}
	} err ] } {
		return -code error $err
	}
	return [ array get newdata1 ]	
}



##*******************************************************
##
## Name: cmonCommon::createLDASmachinesCombo
##
## Description:
## creates the combo widget to display list of machines on LDAS
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## -text does not display text

proc cmonCommon::createLDASmachinesCombo { name page combo } {

    catch { destroy $combo }
    set site [ string trim $::cmonClient::var(site) ]
    set machines [ lsort -dictionary $::LDASmachines($site) ]
    lappend machines user-specified
	set cmd_ent   [ ComboBox $combo \
		-text "Host: " -width 15 \
    	-textvariable ::${name}::${page}(hostname) -editable 0 \
		-values $machines  \
        -modifycmd "cmonCommon::setLDASmachine $combo $name $page" ]
    set ::${name}::${page}(machCombo) $combo
    pack $combo -side left -anchor w -fill x -expand 1
    $combo setvalue first
   
}

## ******************************************************** 
##
## Name: cmonCommon::setLDASmachine
##
## Description:
## handle ldas cmd selection from combo
##
## Parameters:
##
## Comments:

proc cmonCommon::setLDASmachine { combo name page } {

    $combo setvalue @[ $combo getvalue ] 
    set item [ set ::${name}::${page}(hostname) ]
	if	{ [ string match {user-specified} $item ] } {
		$combo configure -editable 1
	} else {
		$combo configure -editable 0
	}
}

## ******************************************************** 
##
## Name: cmonCommon::pageRaise   
##
## Description:
## common code for page raise
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats
## Note that: a notebook will not call raisecmd for the current page.
## So need to invoke it manually

proc cmonCommon::pageRaise { name page { text "" } { nopass 0 } } {

    if  { ! [ string length $text ] } {
        set text $page
    }

	set ::cmonClient::noPass $nopass
    set ::cmonClient::pageUpdate "cmonCommon::pageRaise $name $page \{$text\}"
   
    ;## if connection site has changed
    if  { ! [ string equal [ set ::${name}::prevsite($page) ] $::cmonClient::var(site) ] } {
        catch { updateStatus [ set ::${name}::statusw($page) ] "" }
        ${name}::state0 $page 
        if  { [ string length [ info proc ::${name}::reset ] ] } {
            catch { ${name}::reset $page }
        } 
        set ::${name}::prevsite($page) $::cmonClient::var(site)
        
        if  { [ string length [ info proc ::${name}::pageUpdate ] ] } {
            ${name}::pageUpdate $page
        } else { 
            cmonCommon::pageUpdate $name $page $text
        }
    } 
}

## ******************************************************** 
##
## Name: cmonCommon::pageUpdate  
##
## Description:
## generic proc to update title to reflect the new site
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::pageUpdate { name page text } {

    if  { [ info exist ::${name}::titf3($page) ] } {
	    if	{ $::cmonClient::state } {
	        [ set ::${name}::titf3($page) ] configure -text "$text at $::cmonClient::var(site)"
	    } else {
		    [ set ::${name}::titf3($page) ] configure -text $text  
        }                	        
    }
}

## ******************************************************** 
##
## Name: cmonCommon::toppageRaise   
##
## Description:
## page raise for the top tab: raise the current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## top page raise another page and then the current page
## just raising current page has no effect
## one page - issue the page raise cmd

proc cmonCommon::toppageRaise { notebook } {

    set curpage [ $notebook raise ]
    set pages [ $notebook pages ]
    if  { [ llength $pages ] > 1 } {
        foreach page $pages {
            if  { ! [ string equal $page $curpage ] } {
                break
            }   
        }    
        $notebook raise [ $notebook raise $page ]
        $notebook raise [ $notebook raise $curpage ]
    } else {
        set raisecmd [ $notebook itemcget $curpage -raisecmd ]
        eval $raisecmd
    }

}

##*******************************************************
##
## Name: cmonStatistics::showJobs
##
## Description:
## show job under mouse in a top level window
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## if x-ord cannot be located the data may have contained decimal points
## but server data does not, so just use int(${x-ord})
proc cmonCommon::showJobs { name page graph parent x-ord y-ord X Y { display 1 } } {

	set _top $parent.showjob
    set jobs ""

	if	{ [ catch {
        set x-ord [ expr int(${x-ord}) ]
		if	{ [ info exist ::${name}::pass${page}(${x-ord}) ] } {
			set jobs [ set ::${name}::pass${page}(${x-ord}) ]
			set jobtext $jobs
			set color green
		} elseif { [ info exist ::cmonStatistics::fail${page}(${x-ord}) ] } {
			set jobs [ set ::${name}::fail${page}(${x-ord}) ]
			set color #feaa88
			set jobtext $jobs
	    } else {
			set jobs ""
			set jobtext ""
		}
		if	{ [ string length $jobs ] } {
			set text "[ expr round(${x-ord}) ],$jobtext"
	    } 
		if	{ [ string length $jobs ] && $display } {
			set text [ string trim $text \n ]
			if	{ ! [ winfo exist $_top ] } {
                toplevel $_top -relief flat \
                -bg $color -bd 0 \
                -screen [ winfo screen $graph ]

                wm overrideredirect $_top 1
                wm transient $_top
                wm withdraw $_top

			    set lbl [ label $_top.lbl -text $text -relief flat -bg $color -fg black \
				-font $::SMALLFONT ]
			    pack $lbl -side left -anchor w
            
                update idletasks 
           
                set  scrwidth  [winfo vrootwidth  .]
                set  scrheight [winfo vrootheight .]
                set  width     [winfo reqwidth  $_top]
                set  height    [winfo reqheight $_top]

                if { $X+$width > $scrwidth } {
                    set X [expr {$scrwidth - $width}]
                }
                if { $Y+$height > $scrheight } {
                    set Y [expr {$Y - 2 - $height}]
                }

                wm geometry $_top "+$X+$Y"
                update idletasks
                wm deiconify $_top
                raise $_top
            }
		}
	} err ] } {
		catch { destroy $_top }
	}
	return $jobs
}
		
##*******************************************************
##
## Name: cmonCommon::setLogFilter
##
## Description:
## set log filter to job id
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::setLogFilter { name page graph parent x-ord y-ord X Y } {

    set jobs [ cmonCommon::showJobs $name $page $graph $parent ${x-ord} ${y-ord} $X $Y 0 ]
    if	{ [ llength $jobs ] } {
		cmonLogs::autoLogFilter $jobs
    }
}

##*******************************************************
##
## Name: cmonCommon::removeLabel
##
## Description:
## display graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::removeLabel { parent } {

	set label $parent.showjob
	if	{ [ winfo exist $label ] } {
    	destroy $label 
	}
}


## ******************************************************** 
##
## Name: cmonCommon::updateDBinfo
##
## Description:
## update database name selection when site is changed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::updateDBinfo { name page args } {

    if  { ! [ info exist ::${name}::wdbname($page) ] } {
        return
    }
	if	{ [ catch {
        cmonCommon::createDBinfo $name $page
        update
	} err ] } {
        return -code error $err
	}
}


## ******************************************************** 
##
## Name: cmonCommon::createDBinfo
##
## Description:
## create database widget
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::createDBinfo { name page { parent .none } } {

        if  { [ info exist ::${name}::wdbname($page) ] } {
            set combo [ set ::${name}::wdbname($page) ]
        } else {
            set combo $parent.dbname
        }
        catch { destroy $combo }
	    set cmd_ent   [ ComboBox $combo \
		    -text "Database: " -width 15 \
    	    -textvariable ::${name}::dbname($page) -editable 0 \
		    -values $::dbnames($::cmonClient::var(site)) -state normal \
		    -modifycmd "setCombo $combo" ]
        
        set ::${name}::wdbname($page) $combo
        pack $combo -side left -anchor w -fill x -expand 1
        set default [ lsearch -exact $::dbnames($::cmonClient::var(site)) $::DATABASE_NAME ]
        $combo setvalue @$default
        return $combo
}

## ******************************************************** 
## 
## Name: cmonCommon::createTablesCombo
##
## Description:
## create database tables widget
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::createTablesCombo { name page { parent .none } } {

        if  { [ info exist ::${name}::wdbtables($page) ] } {
            set combo [ set ::${name}::wdbtables$page) ]
        } else {
            set combo $parent.dbtables
        }
        catch { destroy $combo }
        set dbtables [ lsort -ascii $::dbtables ] 
		set dbtables [ concat all $dbtables ] 
	    set cmd_ent   [ ComboBox $combo \
		    -text "By Tables: " -width 15 \
    	    -textvariable ::${name}::dbtable($page) -editable 0 \
		    -values $dbtables -state normal \
		    -modifycmd "setCombo $combo" ]
        
        set ::${name}::wdbtables($page) $combo
        pack $combo -side left -anchor w -fill x -expand 1
        $combo setvalue first

}

## ******************************************************** 
##
## Name: cmonCommon::extendedSelect
##
## Description:
## handle ldas cmd selection from combo
##
## Parameters:
##
## Comments:

proc cmonCommon::extendedSelect { combo name page var { flagall 1 } } {

    $combo setvalue @[ $combo getvalue ] 
	set listb $combo.shell.listb
	set selected [ $listb curselection ]

    set numselected [ llength $selected ]
    if  { ( $numselected == [ $listb size ] ) && $flagall } {
        set items all
    } else {
	    set items [ list ]
        foreach item $selected {
            set itemtxt [ $listb get $item ]
    	    lappend items $itemtxt
        }
	    set items [ join $items " " ]
	}
    set ::${name}::${var}($page) $items
}    

## ******************************************************** 
##
## Name: cmonCommon::createIFOs
##
## Description:
## create database widget
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::createIFOs { name page { parent .none } } {

        if  { [ info exist ::${name}::wifos($page) ] } {
            set combo [ set ::${name}::wifos($page) ]
        } else {
            set combo $parent.ifos
        }
        set lab [ label $parent.lifos -text "Sites: " -width 15 -justify left -anchor w ]
        pack $lab -side left -anchor w -fill x
	    set cmd_ent   [ ComboBox $combo \
		    -text "Sites: " -width 15 \
    	    -textvariable ::${name}::ifos($page) -editable 1 \
		    -values $::FRAME_SITES  -state normal \
		    -modifycmd "cmonCommon::extendedSelect $combo $name $page ifos 0" ]
        
        set ::${name}::wifos($page) $combo
        pack $combo -side left -anchor w -fill x -expand 1
        $combo setvalue first
        ;## configure listbox to be extended select instead of browse
	    set command [ $combo.a  cget -command ]
	    $combo.a configure -command "$command; cmonCommon::listSelectMode $combo"
        return $combo
}

## ******************************************************** 
##
## Name: cmonCommon::createFrameTypes
##
## Description:
## create database widget
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::createFrameTypes { name page { parent .none } } {

        if  { [ info exist ::${name}::wftype($page) ] } {
            set combo [ set ::${name}::wftype($page) ]
        } else {
            set combo $parent.ftypes
        }
        set lab [ label $parent.ltype -text "Type: " -width 15 -justify left -anchor w ]
        pack $lab -side left -anchor w -fill x
	    set cmd_ent   [ ComboBox $combo \
		    -text "Type: " -width 15 \
    	    -textvariable ::${name}::ftypes($page) -editable 1 \
		    -values $::FRAME_TYPES -state normal \
		    -modifycmd "cmonCommon::extendedSelect $combo $name $page ftypes 0" ]
        
        set ::${name}::wftype($page) $combo
        pack $combo -side left -anchor w -fill x -expand 1
        $combo setvalue first
        ;## configure listbox to be extended select instead of browse
	    set command [ $combo.a  cget -command ]
	    $combo.a configure -command "$command; cmonCommon::listSelectMode $combo"
        return $combo
}

##******************************************************* 
##
## Name: cmonCommon::dispLogs
##
## Description:
##
## Parameters:
## convention for parent  .logs-$name:$page:$job
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::dispLogs { name page url html parent } {

    set site [ string trim $::cmonClient::var(site) ]
    
	if	{ [ info exist ::${name}::var($page,browserOnly) ] } {
        if  { [ set ::${name}::var($page,browserOnly) ] } {
		    return
        }
	}

    if  { ! [ winfo exist $parent ] } {
        toplevel $parent 
        set frame [ frame $parent.f -relief sunken \
            -borderwidth 2 ]		
		frame $parent.factions -relief raised -borderwidth 2
        set textw [ ::createDisplay $frame ]
        set ::${parent}(textw) $textw        
        Button $parent.factions.back -text Back \
			-command "cmonCommon::backLink $name $page $parent $textw"
		Button $parent.factions.find -text Find \
			-command "cmonCommon::findDialog $name $page $parent $textw"
		Button $parent.factions.close -text Close \
			-command "cmonCommon::closeLogs $parent"
		Button $parent.factions.print -text Print \
			-command "cmonCommon::printLogs $name $page $parent" 
		Button $parent.factions.save -text Save \
			-command "cmonCommon::saveFile $name $page $parent" 

		pack $parent.factions.back $parent.factions.find $parent.factions.print \
            $parent.factions.save $parent.factions.close \
			-side left -padx 2 -pady 2
		pack $parent.factions -side bottom
        pack $frame -fill both -expand yes
        update idletasks
    } 
    set textw [ set ::${parent}(textw) ]   
    set ::${textw}(linkStack) $url
    if  { [ winfo exist $parent ] } {
		wm title $parent "$::cmonClient::client $page @$site"
		wm deiconify $parent
        updateDisplay $textw $html
		raise [ winfo toplevel $parent ] .
    }
}


## ******************************************************** 
##
## Name: cmonCommon::saveFile 
##
## Description:
## save log html to user specified file
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:

proc cmonCommon::saveFile { name page parent } {

	if	{ [ catch { 
		set typelist {
			{"HTML" {".html"}}
		}
        set site [ string trim $::cmonClient::var(site) ]
     	set fname [ tk_getSaveFile -filetypes $typelist -title "save log filter" \
			-defaultextension .html -initialdir [ pwd ] -title "save $page" ]
			
		if	{ [ string length $fname ] } {
			set ext [ file extension $fname ]
			if	{ [ string length $ext ] } {
	 			regsub $ext $fname {.html} fname
			} else {
				set fname ${fname}.html
			}
            if  { ! [ file exist $::TMPDIR ] } {
                file mkdir $::TMPDIR 
            }
            regexp {.log-([^\.]+)} $parent -> pageetc
			file copy -force $::TMPDIR/${pageetc}_${site}.html $fname
			set ack [ tk_messageBox -type ok -default ok \
				-message "Your log filter has been saved in $fname." \
				-icon info ]	
		}	
	} err ] } {
		set ack [ tk_messageBox -type ok -default ok \
				-message "Error saving log filter $page: $err" \
				-icon error ]
	}
}

proc cmonCommon::saveFileText { name page parent } {

	if	{ [ catch { 
		set typelist {
			{"text" {".txt"}}
		}
        set site [ string trim $::cmonClient::var(site) ]
     	set fname [ tk_getSaveFile -filetypes $typelist -title "save log filter" \
			-defaultextension .txt -initialdir [ pwd ] -title "save $page" ]
		
        if  { [ string length $fname ] } {
            set	textsw [ set ::${parent}(textw) ]
            set text [ $textsw get 1.0 end ]
            set fd [ open $fname w ]
            puts -nonewline $fd $text
            close $fd 
		    set ack [ tk_messageBox -type ok -default ok \
				-message "Your log filter has been saved in $fname." \
				-icon info ]	
        }
	} err ] } {
		set ack [ tk_messageBox -type ok -default ok \
				-message "Error saving file for $page: $err" \
				-icon error ]
	}
}

## ******************************************************** 
##
## Name: cmonCommon::backLink
##
## Description:
## redisplay the last url
##
##
## Parameters:
##
## Comments:
   
proc cmonCommon::backLink { name page parent textw } {


    set origCursor [ $textw cget -cursor ]
    set origstate [ set ::${name}::state($page) ]
    if  { [ catch { 
        if  { [ info exist ::${textw}(linkStack) ] } {
            set linkStack [ set ::${textw}(linkStack) ]
            if  { [ llength $linkStack ] > 1 } {
                set ::${textw}(linkStack) [ lreplace $linkStack end end ]
                set url [ lindex [ set ::${textw}(linkStack) ] end ]
                $textw config -cursor watch
                catch { eval ::${name}::state3 $page }
                html::callback $textw $url  
            }              
        }
    } err ] } {
        appendStatus [ set ::${name}::statusw($page) ] $err
    }
    $textw config -cursor $origCursor
    if  { [ set ::${name}::state($page) ] == 3 } {
        eval ::${name}::state${origstate} $page
    }
}

##******************************************************* 
##
## Name: cmonCommon::printLogs
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::printLogs { name page parent } {
	
	if	{ [ catch {	
        set site [ string trim $::cmonClient::var(site) ]
        regexp {log-([^\.]+)} $parent -> pageetc
		set fname $::TMPDIR/${pageetc}_${site}.html
		set outfile $::TMPDIR/${pageetc}_${site}.ps
		if	{ [ string length [ auto_execok html2ps ] ] } {
			catch { exec html2ps -f $::HTML2PS_CFG -o $outfile $fname & } err1
            if  { ! [ regexp {^\d+$} $err1 ] } {
                error $err1
            }
			after 5000 [ list cmonCommon::notifyPrint $outfile $parent $err1 ]
		} else {
			appendStatus [ set ::${name}::statusw($page) ] \
			"You do not have html2ps needed for printing in your installation;\n\
			please print from your browser by pointing to file $fname"  
		}
	} err ] } {
		appendStatus [ set ::${name}::statusw($page) ] $err
	}
}

##******************************************************* 
##
## Name: cmonCommon::printText
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::printText { name page parent { chars_per_line 132 } { orientation landscape } { fontsize 10 } } {

	if	{ [ catch {	
        set	textsw [ set ::${parent}(textw) ]
        set text [ $textsw get 1.0 end ]
        set site [ string trim $::cmonClient::var(site) ]
     
        set right_title "cmonClient $::version"
        set header ""
        set footer ""
        set title [ wm title $parent ]
        
        set left_title ""
        set orientation $orientation
        
        set site [ string trim $::cmonClient::var(site) ]
       
        if { [ string length [ auto_execok a2ps ] ] } { 
            catch { exec echo $text | a2ps -1 --$orientation -l $chars_per_line --header=$header --center-title=$title \
            --left-title=$left_title --right-title=$right_title --footer=$footer --font-size=$fontsize --sides=2  } err
            set ack [ tk_messageBox -type ok -default ok \
				-message "Printing [ string length $text ] bytes $err" \
				-icon info ]
        } else {
            catch { exec echo $text | lpr &  } err
            set ack [ tk_messageBox -type ok -default ok \
				-message "Unable to use a2ps, line wrapping is not possible $err" \
				-icon info ]  
        }	
	} err ] } {
		appendStatus [ set ::${name}::statusw($page) ] $err
	}
}

##******************************************************* 
##
## Name: cmonCommon::closeLogs
##
## Description:
## clean up when a log window is closed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::closeLogs { parent } {
    destroy $parent
    if  { [ info exist ::${parent} ] } {
        foreach entry [ array names ::${parent} ] { 
            catch { unset ::[ set ::${parent}($entry) ] }
        } 
    }
    catch { unset ::${parent} }
    ;## remove temp files
    set site [ string trim $::cmonClient::var(site) ]
    regexp {log-([^\.]+)} $parent -> pageetc
    set fname $::TMPDIR/${pageetc}_${site}.html
    catch { file delete $fname }
}
	
##******************************************************* 
##
## Name: cmonCommon::setRunTime
##
## Description:
## set the time widget times for an Eng or Science Run
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::setRunTime { runtype index } {

    foreach { starttime endtime } [ set ::run_${runtype} ] { break }
    cmonCommon::setTime $starttime $endtime
    set ::cmonCommon::timeopt [ lindex $::cmonCommon::timeoptions $index ]
}

##******************************************************* 
##
## Name: cmonCommon::setUserIdPattern
##
## Description:
## returns the regexp pattern for specifying user id 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::setUserIdPattern { var } {

    set userId [ set ::$var ]
	if	{ ! [ string length $userId ] || [ regexp {^[\.\+\*\s\t]+$} $userId ] } {
		set userid .+
        set ::$var *
		return $userid
	}
    set userId [ split [ string trim $userId ] " " ]
    set userId [ join $userId \| ]
    return $userId
}

##******************************************************* 
##
## Name: cmonCommon::reduceDisplayData
##
## Description:
## reduce amount of points to certain size for display
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::reduceDisplayData { data max } {

    set npoints [ expr [ llength $data ] /2  ]
    if  { $npoints <= $max } {
        return [ list $data {} ]
    }
    set npoints_avg [ expr round($npoints/$max) ]
    
    if  { $npoints_avg <= 1 } {
        return [ list $data {} ]
    }
    set n 0
    set newdata [ list ]

    foreach { x y } $data {
        incr n 1
        if  { $n >= $npoints_avg } {
            lappend newdata $x $y 
            set n 0
        }
    }
    ;## tag on the first and last point to complete the line
    foreach { x1 y1 } [ lrange $data 0 1 ] { break }
    foreach { x2 y2 } [ lrange $data end-1 end ] { break }
    
    set newdata [ linsert $newdata 0 $x1 $y1 ]
    lappend newdata $x2 $y2
    set msg "$npoints points exceeded maximum of $max. Reduced $npoints points of data to [ expr [ llength $newdata ]/2 ] points, \
by plotting every $npoints_avg points."
    if  { $::DEBUG } {
        puts $msg
    } 
    return [ list $newdata $msg ]
}    
    
##******************************************************* 
##
## Name: cmonCommon::durationWidget
##
## Description:
## duration widget allowing time selection for days,hours and mins
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::durationWidget { parent var1 var2 var3  } {

	if	{ [ catch {
    	set f1 [ frame $parent.f1 -relief flat -borderwidth 3 ]
    	set lab1 [ Label $f1.lab1 -text "D:" -width 2 -justify left -anchor w ]
		set spin1  [SpinBox $f1.dayrange -range {0 365 1} -textvariable $var1 \
                   -helptext "days in a year" -width 3 ]
        $spin1 bind <Leave> "cmonCommon::histSpinNew name page $var1 0 365"
        $spin1 bind <Return> "cmonCommon::histSpinNew name page $var1 0 365"  
       	set lab2 [ Label $f1.lab2 -text "H:" -width 2 -justify left -anchor w ]
		set spin2  [SpinBox $f1.hrrange -range {0 23 1} -textvariable $var2 \
                   -helptext "hours in a day" -width 3 ]
        $spin2 bind <Leave> "cmonCommon::histSpinNew name page $var2 0 23"  
        $spin2 bind <Return> "cmonCommon::histSpinNew name page $var2 0 23" 
        set lab3 [ Label $f1.lab3 -text "M:" -width 2 -justify left -anchor w ]
		set spin3  [SpinBox $f1.minrange -range {0 59 1} -textvariable $var3 \
                   -helptext "Minutes in an hour"  -width 3 ]  
        $spin3 bind <Leave> "cmonCommon::histSpinNew name page $var3 0 59"  
        $spin3 bind <Return> "cmonCommon::histSpinNew name page $var3 0 59"     
        pack $lab1 $spin1 $lab2 $spin2 $lab3 $spin3 -side left
        
	} err ] } {
    	return -code error $err
   	}
    return $f1
}

##******************************************************* 
##
## Name: cmonCommon::durationSecs
##
## Description:
## duration widget allowing time selection for days,hours and mins
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::durationSecs { { days 0 } { hours 0} { mins 0 } } {

	if	{ [ catch {
		set total [ expr $days * 24  * 60 * 60 ]
    	incr total [ expr $hours * 60 * 60 ] 
    	incr total [ expr $mins * 60 ] 
   	} err ] } {       
        return -code error $err
    }
 	return $total
}

##******************************************************* 
##
## Name: cmonCommon::durationStr
##
## Description:
## duration widget allowing time selection for days,hours and mins
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonCommon::durationDHM { seconds } {
	
    set mins [ expr $seconds / 60 ]
    set hours [ expr $mins / 60 ]
    set mins [ expr $mins % 60 ]
    set days [ expr $hours / 24 ]
    set hours [ expr $hours % 24 ]
	return [ list $days $hours $mins ]
}
