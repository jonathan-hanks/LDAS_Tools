## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles LDAS log filtering 
##
## cmonStatistics Version 1.0
##
## ******************************************************** 

package provide cmonStatistics 1.0

namespace eval cmonStatistics {
    set notebook ""
    set pages [ list job-Stats metadata-Stats queue-Stats test-report]
	set dbaccess [ list putMetaData getMetaData ]
	set tab(job-Stats) "Job Statistics"
	set tab(metadata-Stats) "Database Statistics"
	set tab(queue-Stats) "Queue Statistics"
	set tab(rejected-Stats) "Rejected Job Statistics"
    set tab(test-report) "Test Report"
	set title(job-Stats) "Number of Jobs Passed,Failed or Rejected vs GPS Time" 
	set title(metadata-Stats) "Number of database insertions/queries vs GPS Time" 
	set title(queue-Stats) "Queue size"     
	set xlabel(job-Stats) "GPS Time"
	set xlabel(metadata-Stats) "GPS Time"
	set xlabel(queue-Stats) "GPS Time"
	set ylabel(job-Stats) "# Jobs"
	set ylabel(metadata-Stats) "# Insertions/\nQueries"
	set ylabel(queue-Stats) "Queue size\n(# Jobs)"
	set graph jobstats
	set color(pass) $::darkgreen
	set color(fail) red
	set color(reject) purple
	set color(putMetaData) magenta
	set color(getMetaData) blue
	set color(queuesize) brown
	set color(rejected) purple
    set color(selected) brown
	set wTop .tmpStatistics
}


## ******************************************************** 
##
## Name: cmonStatistics::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonStatistics::create { notebook toppage } {

    set ::cmonStatistics::notebook $notebook
    foreach page $::cmonStatistics::pages {
        $notebook insert end $toppage:$page -text $::cmonStatistics::tab($page)  \
        -createcmd "cmonStatistics::createPages $notebook $toppage:$page $page" \
        -raisecmd "cmonCommon::pageRaise cmonStatistics $page; \
            cmonCommon::setLastTime cmonStatistics $page; \
            cmonCommon::updateDBinfo cmonStatistics $page"
    }
}


## ******************************************************** 
##
## Name: cmonStatistics::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonStatistics::createPages { notebook toppage page } {

	if	{ [ catch { 
        set frame  [ $notebook getframe $toppage ]		
		specific $frame $page 
        ;## place the cmd status on the bottom
	    set titf2 [TitleFrame $frame.refresh -text "Command Status" \
            -relief sunken -borderwidth 4 -font $::ITALICFONT]
                   
        set subf2  [ $titf2 getframe ]
	
	    set statusw [ ::createStatus  $subf2 ]
	    array set ::cmonStatistics::statusw [ list $page $statusw ]
        array set ::cmonStatistics::state [ list $page 0 ]	
        pack $titf2 -side top -fill both -expand 1 -anchor n
        set ::cmonStatistics::prevsite($page) $::cmonClient::var(site)
	} err ] } {
		puts $err
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cmonStatistics::ldascmds
##
## Description:
## creates usercmd selection 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonStatistics::ldascmds { name page parent } {

	set f1 [ frame $parent.fldascmds -relief groove -borderwidth 2 ]

	;## select ldas cmds
	set fldascmds [ frame $f1.fldascmds ]
	set lab [ label $fldascmds.lab -text "By Command: " -width 15 -justify left -anchor w ]
	set cmd_ent   [ ComboBox $fldascmds.cmd \
    	-textvariable ::${name}::var($page,ldascmds) -editable 1 \
		-values $::ldascmds \
        -helptext "LDAS user cmds" -modifycmd "setCombo $fldascmds.cmd " ]
	pack $lab  -side left -anchor w  
	pack $fldascmds.cmd -side left -anchor w -fill x -expand 1
	pack $fldascmds -side top -anchor w -fill x -expand 1
	$fldascmds.cmd setvalue first
	
	pack $f1 -side top -fill x -expand 1 
	return $f1
}	

## ******************************************************** 
##
## Name: cmonStatistics::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonStatistics::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]
	if	{ ! [ regexp {queue|report} $page ] } {
        
		set labfent [LabelFrame $parent.fent -side top -anchor w \
            -relief sunken -borderwidth 4 -font $::ITALICFONT -text \
			"Specify user Id  or time periods for $::cmonStatistics::tab($page)" ]
    	set subf [ $labfent getframe ]
	    
        set ::cmonStatistics::topfw($page) $subf
       
		set subf1 [ frame $subf.subf1 -relief sunken -borderwidth 2 ]
		set fuserid [ frame $subf1.fuserid -relief groove -borderwidth 2 ]
	
		set userid_ent   [ LabelEntry $fuserid.euserid \
			-label "By User Id: " -labelwidth 15 -labelanchor w \
    		-textvariable ::cmonStatistics::var($page,userid) -editable 1 -width 15 \
        	-helptext "Formats: <user1> <user2> <user3> ..., or leave blank for users"]
	
		switch -regexp -- $page {	
			metadata { 
                       DBspecific $page $subf1 
                       set ::cmonStatistics::wldascmds($page) [ cmonCommon::ldascmds $name $page $subf1 ]
                    }
			job { 	jobspecific $page $subf1
					cmonCommon::ldascmds $name $page $subf1 
					cmonStatistics::rejectCombo cmonStatistics $page $subf1 
					$::cmonStatistics::ckrejectw($page) configure -command "cmonStatistics::setRejectCombo $page" }
			default  {}
		}
		pack $subf1.fuserid.euserid -side top -anchor w -fill x -expand 1
		pack $subf1.fuserid -side top -anchor w -expand 1 -fill x
		set ::cmonStatistics::var($page,userid) *
	} else {
        ;## queue or test report pages
		set labfent [LabelFrame $parent.fent -side top -anchor w \
            -relief sunken -borderwidth 4 -font $::ITALICFONT -text \
			"Specify time periods for $::cmonStatistics::tab($page)" ]
		set subf [ $labfent getframe ]
        set ::cmonStatistics::topfw($page) $subf
        if  { [ regexp {report} $page ] } {
            reportspecific $page $subf
        }
	}
		
	set subf2 [ frame $subf.subf2 -relief ridge -borderwidth 2 ]    
	set f2 [ frame $subf2.ftime -relief groove -borderwidth 2 ]
	set f [ cmonCommon::createTimeOptions $f2 $name $page ]
	
	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $f2 $name $page ] { break }
	
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x 
	set ::cmonStatistics::var($page,type) userid
	pack $subf2.ftime -side top -anchor w -expand 1 -fill x
	set but1 [ Button $subf2.start -text "SUBMIT" -width 5 -helptext "submit one time request to server" \
        -command  "::cmonStatistics::sendRequest $page" ]
        
	if	{ [ info exist subf1 ] } {
		pack $subf1 $subf2 -side left -fill x -expand 1
        pack $but1 -side top -padx 100 -pady 5 	
	} else {
		pack $subf2 -side left -fill x -expand 1
        pack $but1 -side top -padx 10 -pady 5 
	}
	
	set ::cmonStatistics::bstart($page) $but1  
		
    pack $labfent -side top -padx 4 -fill x -anchor n
   
}

## ******************************************************** 
##
## Name: cmonStatistics::DBspecific
##
## Description:
## creates check buttons for queries and insertions
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonStatistics::DBspecific { page parent } {

        set f1 [ frame $parent.fdb ]
        set f3 [ frame $f1.f3 -relief ridge ]
	    set lab [ label $f3.lbl -text "Database: " -width 15 -justify left -anchor w ]
        pack $lab -side left -anchor w 
        cmonCommon::createDBinfo cmonStatistics $page $f3
                
		set f2 [ frame $f1.f2 -relief ridge ]
		set but1 [ checkbutton $f2.insert -text "Insertions" \
        -variable ::cmonStatistics::var($page,insert) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(putMetaData) \
		-selectcolor $::cmonStatistics::color(putMetaData) ]
		set lbl1 [ Label $f2.tblbl \
		-text "By table: " -width 15 -helptext "filter by table" ]
        pack $but1 $lbl1 -side left -padx 1 -pady 1 -anchor w
        cmonCommon::createTablesCombo cmonStatistics $page $f2
		pack $f3 $f2 -side top -fill x -expand 1
		set but2 [ checkbutton $f1.query -text "Queries" \
        -variable ::cmonStatistics::var($page,query) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(getMetaData) \
		-selectcolor $::cmonStatistics::color(getMetaData) ]
		pack $but2 -side top -anchor w 
		pack $f1 -side top -fill x -expand 1
		set ::cmonStatistics::var($page,insert) 1
		set ::cmonStatistics::var($page,query) 1
}

## ******************************************************** 
##
## Name: cmonStatistics::jobspecific
##
## Description:
## creates check buttons for queries and insertions
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonStatistics::jobspecific { page parent } {

		set f1 [ frame $parent.fjob ]
		set but1 [ checkbutton $f1.pass -text "Jobs passed" \
        -variable ::cmonStatistics::var($page,pass) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(pass) \
		-selectcolor $::cmonStatistics::color(pass) ]
		set but2 [ checkbutton $f1.fail -text "Jobs failed" \
        -variable ::cmonStatistics::var($page,fail) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(fail) \
		-selectcolor $::cmonStatistics::color(fail) ]
		set but3 [ checkbutton $f1.reject -text "Jobs rejected" \
        -variable ::cmonStatistics::var($page,reject) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(reject) \
		-selectcolor $::cmonStatistics::color(reject) ]
		pack $but1 $but2 $but3 -side left -anchor w -padx 4
		pack $f1 -side top -fill x -expand 1
		set ::cmonStatistics::var($page,pass) 1
		set ::cmonStatistics::var($page,fail) 1
		set ::cmonStatistics::var($page,reject) 1
		set ::cmonStatistics::ckrejectw($page) $but3
}

# ******************************************************** 
##
## Name: cmonStatistics::reportspecific
##
## Description:
## options for saving test report:
##
## Parameters:
## page and parent window
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonStatistics::reportspecific { page parent } {

    if  { [ catch {
        set labfrpt [LabelFrame $parent.freport -side top -anchor w \
            -relief ridge -borderwidth 4 -font $::ITALICFONT -text \
			"Report options" ]
    	set f1 [ $labfrpt getframe ]
		set but1 [ checkbutton $f1.rptonServer -text "Leave report on server" \
        -variable ::cmonStatistics::${page}(report,server) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(selected) \
		-selectcolor $::cmonStatistics::color(selected) ]
		set but2 [ checkbutton $f1.rptonClient -text "Download to client" \
        -variable ::cmonStatistics::${page}(report,client) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(selected) \
		-selectcolor $::cmonStatistics::color(selected) -state disabled ]
		set but3 [ checkbutton $f1.rptinArchive -text "Archived on server" \
        -variable ::cmonStatistics::${page}(report,archive) -onvalue 1 -offvalue 0 \
		-foreground $::cmonStatistics::color(selected) \
		-selectcolor $::cmonStatistics::color(selected) ]
		pack $but1 $but3 $but2 -side top -anchor w
		pack $labfrpt -side right -fill both -expand 1
		set ::cmonStatistics::${page}(report,server) 1
		set ::cmonStatistics::${page}(report,client) 0
		set ::cmonStatistics::${page}(report,archive) 0
    } err ] } {
        puts $err
        return -code error $err                
    }
}

## ******************************************************** 
##
## Name: cmonStatistics::rejectCombo
##
## Description:
## creates usercmd selection 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## 

proc cmonStatistics::rejectCombo { name page parent } {

	set f1 [ frame $parent.freject1 -relief groove -borderwidth 2 ]

	;## select ldas cmds
	set frejects [ frame $f1.rejects ]
	set lab [ label $frejects.lab -text "By rejected reason: " -width 20 -justify left -anchor w ]
	set cmd_ent   [ ComboBox $frejects.cmd \
    	-textvariable ::${name}::var($page,rejectReason) -editable 0 \
		-values $::rejectReasons \
        -helptext "Job rejection reasons" -modifycmd "setCombo $frejects.cmd" ]
	pack $lab -side left -anchor w 		 
	pack $frejects.cmd -side left -anchor w -fill x -expand 1
	pack $frejects -side top -anchor w -fill both -expand 1 
	$frejects.cmd setvalue first
	set ::cmonStatistics::rejectCombo($page) $frejects.cmd 
	pack $f1 -side top -fill both -expand 1
	return $f1
}

## ******************************************************** 
##
## Name: cmonStatistics::setRejectCombo
##
## Description:
## enable or disable the rejected reason combo
##
## Parameters:
## parent widget
##
## Usage:
##  
## 
## Comments:
## 

proc cmonStatistics::setRejectCombo { page } {

	if	{ $::cmonStatistics::var($page,reject) } {
		$::cmonStatistics::rejectCombo($page) configure -state normal
	} else {
		$::cmonStatistics::rejectCombo($page) configure -state disabled
	}
}

## ******************************************************** 
##
## Name: cmonStatistics::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
## this proc name is generic for all packages.
## sample test times 
## start 		05/04/00 16:38:27 641493520
## end times 	05/04/00 18:13:24 641499217
##	start		957892986	641928192
## end 		957893198	641928404
##	start		957892986	641928192
## end			641928192	641928192
## start		now
## end			now
	
proc cmonStatistics::sendRequest { page } {

	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonStatistics::statusw($page) "Please select an LDAS site"
		return
	}
	set name [ namespace tail [ namespace current ] ]

	if	{ [ catch {
		if	{ [ info exist ::cmonStatistics::var($page,userid) ] } {
            set userid [ cmonCommon::setUserIdPattern ::cmonStatistics::var($page,userid) ]
		}
		switch $page {
			job-Stats    { set servercmd "cntlmon::jobStatsGraph $userid "  }
			metadata-Stats  { set servercmd "cntlmon::metadataGraph $userid "  }
			queue-Stats  { set servercmd "cntlmon::queueStatsGraph "  }
            test-report  { set servercmd "cntlmon::testReport " }                
			default 	 { set servercmd "cntlmon::rejectedJobsGraph $userid " }
		}
	
		;## need to fill in gap to make filter line up
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
		set ::cmonStatistics::xmin($page) $starttime
		set ::cmonStatistics::xmax($page) $endtime
        append servercmd $starttime-$endtime
        
        ;## if today is Mon prompt user for archive option is not selected
        if  { [ string match test-report $page ] } {
            if  { [ set ::cmonStatistics::${page}(report,archive) ] } {
                append servercmd " 1"
            } else {
                set day [ clock format [ clock seconds ] -format %a ]
                if  { [ string equal Mon $day ] } {
                    set ack [ tk_messageBox -type okcancel -default cancel -message \
                    "Would you like to archive this report since today is Monday ?" -icon question ]
                    if  { [ string equal ok  $ack ] } {  
                        set ::cmonStatistics::${page}(report,archive) 1
                        append servercmd " 1"
                    }
                }
            }            
        }
        
		if	{ [ info exist ::cmonStatistics::dbtable($page) ] } {
			if	{ [ string match all $::cmonStatistics::dbtable($page) ] } {
				set table .+
			} else {
				set table $::cmonStatistics::dbtable($page)
			}
			append servercmd " $table"
            
            set dbname ""
            if  { [ info exist ::cmonStatistics::dbname($page) ] } {
                set dbname $::cmonStatistics::dbname($page)
            }
            
			;## here append table filter for insertions
			set dbaccess [ list ]
			if	{ [ info exist ::cmonStatistics::var($page,insert) ] } {
				if	{ $::cmonStatistics::var($page,insert) } {
					lappend dbaccess putMetaData 
				}
			}
			if	{ [ info exist ::cmonStatistics::var($page,query) ] } {
				if	{ $::cmonStatistics::var($page,query) } {
					lappend dbaccess getMetaData 
				}
				if	{ ! [ llength $dbaccess ] } {
					error "Select insertions or queries or both"
				}
				append servercmd " \{$dbaccess\}"
                append servercmd " $dbname"
			} 
		}
		set jobtypes [ list ]
		if	{ [ info exist ::cmonStatistics::var($page,pass) ] } {
			if	{ $::cmonStatistics::var($page,pass) } {
				lappend jobtypes pass
			}
			if	{ [ info exist ::cmonStatistics::var($page,fail) ] } {
				if	{ $::cmonStatistics::var($page,fail) } {
					lappend jobtypes fail
				}
			}
			if	{ [ info exist ::cmonStatistics::var($page,reject) ] } {
				if	{ $::cmonStatistics::var($page,reject) } {
					lappend jobtypes reject
				}
			}
			append servercmd " [ list $jobtypes ]"
		}
		
		set cmdpat [ cmonCommon::setCmdDsoPattern cmonStatistics $page ]
		append servercmd " $cmdpat"
		
		if	{ [ info exist ::cmonStatistics::var($page,reject) ] } {
			set rejectReason $::cmonStatistics::var($page,rejectReason)
			if	{ [ string match all $rejectReason ] } {
				set rejectReason .+
			}
			append servercmd " $rejectReason"
		}
        set client $::cmonClient::client
        set cmdId "new"
		set freq 0
    	set repeat 0
       	set cmd "cmonStatistics::showReply $page\n$repeat\n$freq\n\$client:$cmdId\n$servercmd"  
		# puts "cmd=$cmd"    
		cmonStatistics::state1 $page 
		sendCmd $cmd 
	} err ] } {
		cmonStatistics::state0 $page 
		appendStatus $::cmonStatistics::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonStatistics::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonStatistics::showReply { page rc clientCmd html } {

	if	{ [ catch {
    set name [ namespace tail [ namespace current ] ]
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "in showReply, rc=$rc, page=$page, client=$client,cmdId=$cmdId, afterid=$afterid"
	# puts "html size [ string length $html ]"
    set ::cmonStatistics::cmdId($page) $cmdId 
    set ::cmonClient::client $client
    	

	switch $rc {
	0 -
	2 {	if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
            if  { ! [ string match "test-report" $page ] } {
			    cmonStatistics::dispGraph $page $html
                 appendStatus $::cmonStatistics::statusw($page) updated 0 blue
            } else {
                ;## no graph for test report but download the test report
			    appendStatus $::cmonStatistics::statusw($page) $html 0 blue
                if  { [ regexp {(http.+/ldas_outgoing/.+cmonClient.+testReport\S+)} $html -> url ] } {
                    set url [ string trimright $url . ]
                    catch { exec $::BROWSER $url & }
                }
            }
        } else {
            appendStatus $::cmonStatistics::statusw($page) \
                "No jobs" 
        }
	  }
	3 {
		appendStatus $::cmonStatistics::statusw($page) $html
		;## may need to cancel request	
	  }
	}
	} err ] } {
		appendStatus $::cmonStatistics::statusw($page) $err
	}
	cmonStatistics::state0 $page
}

##******************************************************* 
##
## Name: cmonStatistics::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonStatistics::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonStatistics::topfw($page) normal
	$::cmonStatistics::bstart($page) configure -state normal -text SUBMIT \
		-command "${name}::sendRequest $page"
	array set ::cmonStatistics::state [ list $page 0 ]
}

##******************************************************* 
##
## Name: cmonStatistics::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonStatistics::state1 { page } {
	set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonStatistics::topfw($page) disabled	
    $::cmonStatistics::bstart($page) configure -state disabled 
	array set ::cmonStatistics::state [ list $page 1 ]
	
}

##******************************************************* 
##
## Name: cmonStatistics::clearGraphLabels
##
## Description:
## set label text to nulls 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonStatistics::clearGraphLabels {} {
    uplevel {
        set gps_start $::cmonStatistics::xmin($page)
        set gps_end   $::cmonStatistics::xmax($page)

        set localstart [ gps2utc $gps_start 0 ]
        set localend   [ gps2utc $gps_end 0 ]
    }
}

##*******************************************************
##
## Name: cmonStatistics::jobGraphStats 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatistics::jobGraphStats { page statsdata parent } {

	if	{ [ catch {
       
        foreach { total numFail pfailed rejected passdata faildata passjobs failjobs rejectdata reasons } \
			    $statsdata { break }
                
		cmonStatistics::clearGraphLabels
		
		catch { unset ::cmonStatistics::pass${page} }
		catch { unset ::cmonStatistics::fail${page} }
		
		set ::${parent}(title) $::cmonStatistics::title($page)
		set ::${parent}(y1label) "# jobs"		
		set ::${parent}(Enter) "cmonCommon::showJobs cmonStatistics $page"
		set ::${parent}(Leave) "cmonCommon::removeLabel $parent"
		set ::${parent}(submit) "cmonStatistics::sendRequest $page"
		set ::${parent}(B1Up)   "cmonCommon::setLogFilter cmonStatistics $page"
        
		;## set print info
		set ::${parent}(ptitle) "Job Statistics @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
		set text [ list ]
		lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
		set user $::cmonStatistics::var($page,userid)
		if	{ [ regexp {^[\*\s\t]+$} $user ] } {
			set user all
		}
		set cmds $::cmonStatistics::var($page,ldascmds)
		if	{ [ regexp {^[\*\s\t]+$} $cmds ] } {
			set cmds all
		}
		set userline "For user(s) $user "
		
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]     

        lappend text "$total jobs\t" [ list brown bold ]
        
		;## set print info
		# set ::${parent}(ptitle) "Job Statistics @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"

		;## dont use pfailed for now, format the values to more decimal points
        if	{ $total } {
        	set numpass [ expr $total - $numFail ]
        	set percentpass [ format "%.5f" [ expr $numpass*100.0/$total  ] ]
            set pfailed [ format "%.5f%%" [ expr 100.0 - $percentpass ] ]
            set percentpass "$percentpass%"
        } else {
        	set percentpass 0.0%
            set pfailed 0.0%
            set numpass 0
        }
		if	{ ! $::cmonStatistics::var($page,pass) } {
			set passdata [ list ]
		} else {
            lappend text "$numpass passed ( $percentpass )\t" [ list $::brightgreen bold ]
			foreach { x-ord y-ord } $passjobs {
				set ::cmonStatistics::pass${page}(${x-ord}) ${y-ord}
			}
		}
			
		if	{ ! $::cmonStatistics::var($page,fail) } {
			set faildata [ list ]
		} else {
            lappend text "$numFail failed ( ${pfailed} )\t" [ list red bold ]
			foreach { x-ord y-ord } $failjobs {
				set ::cmonStatistics::fail${page}(${x-ord}) ${y-ord}
			}
		}
		# puts "fail [ array size ::cmonStatistics::fail${page} ], pass [ array size ::cmonStatistics::pass${page} ]"
		if	{ ! $::cmonStatistics::var($page,reject) } {
			set rejectdata [ list ]
			updateStatus $::cmonStatistics::statusw($page) ""
		} else {
			set alltotal [ expr $total+$rejected ]
			if	{ $alltotal } {
				set percentReject [ format "%.5f%%" [ expr $rejected*100.0 / $alltotal ] ]
			} else {
				set percentReject "0.0"
			}
            lappend text "$rejected rejected ( ${percentReject} )\n" [ list purple bold ]
            set reasonline ""
            if  { $rejected } {
			    set reasonline "Subtotal by rejected reason(s): $::cmonStatistics::var($page,rejectReason)\n"			
			    set i 0
			    foreach { reject_reason count } $reasons {
				    set percent [ format "%.5f" [ expr $count*100.0 / $alltotal ] ]
				    append reasonline "$reject_reason $count ( ${percent}% )\t"
				    incr i 1
				    if	{ ! [ expr $i % 4 ] } {
					    append reasonline \n
				    }
			    }			
			    set reasonline [ string trim $reasonline \n ]
                lappend text $reasonline [ list purple bold ]
            }
		}
        
        ;## put these info further down the screen
        lappend text "Selected Users:\t" [ list black fixed ]
        lappend text "$user" [ list brown bold ]
        
        set cmdtext  [ cmonCommon::showCmdsDsos cmonStatistics $page 0 ]

        ;## put black for the item description
        regexp {LDAS commands:\t(.+)} $cmdtext -> cmdtext1
        lappend text "\nLDAS commands: " [ list black bold ]
		lappend text "$cmdtext1\n" [ list brown bold ]
        updateTextWin [ set ::${parent}(textw) ] $text 
        
		set ymin 0
		set ymax 0
        foreach dataset [ list passdata faildata rejectdata ] {
            set newymax [ lindex [ set $dataset ] end ]
            if  { $ymax < $newymax } {
                set ymax $newymax 
            }
        }

		if	{ $ymax < 5 } {
			set ymax 5
		}
			
		set bltdata [ list ]
		foreach type { fail reject pass } {
			set data [ set ${type}data ]
            foreach { data msg } [ cmonCommon::reduceDisplayData $data $::MAX_GRAPH_POINTS ] { break }
            if  { [ llength $msg ] } {
                appendStatus $::cmonStatistics::statusw($page) "$type data: $msg" 0 brown
            }
			lappend bltdata $data
			lappend bltdata ${type}ed
			lappend bltdata circle
			lappend bltdata $::cmonStatistics::color($type)
			lappend bltdata 0
		}
		
		blt_graph::showGraph $bltdata $::cmonStatistics::xmin($page) \
				$::cmonStatistics::xmax($page) $ymin $ymax $parent			
		raise $parent
		update idletasks
	} err ] } {
		appendStatus $::cmonStatistics::statusw($page) $err
	}
}
##*******************************************************
##
## Name: cmonStatistics::metadataGraphStats 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatistics::metadataGraphStats { page statsdata parent } {
	
	if	{ [ catch {
		set putMetaData_data [ list ]
		set getMetaData_data [ list ]
		set putMetaData_total 0
		set getMetaData_total 0
              
		set parent $::cmonStatistics::wTop${page}

		set ymax 0
		set ymin 0
      
		foreach { dbcmd total data } $statsdata { 
			if	{ ! $total } {
				continue
			}
			set ${dbcmd}_total $total
			set ${dbcmd}_data $data 
			if	{ $total > $ymax } {
				set ymax $total
			}
		}
		
		set ::${parent}(title) "Database Insertions/Queries vs Time"
		set ::${parent}(y1label) "# rows queried/inserted"
		set ::${parent}(submit) "cmonStatistics::sendRequest $page"
        
        set ::${parent}(ptitle) "Database Statistics @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
		set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        
		cmonStatistics::clearGraphLabels
        
	    set dbname $::cmonStatistics::dbname($page)
        set user $::cmonStatistics::var($page,userid)

		if	{ [ regexp {^[\*\s\t]+$} $user ] } {
			set user all
		}
        
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]
            
		if	{ $::cmonStatistics::var($page,insert) } {
			lappend text " $putMetaData_total rows inserted for $::cmonStatistics::dbtable($page) table(s)\t" \
            [ list magenta bold ]
		} 
		if	{ $::cmonStatistics::var($page,query) } {
			lappend text "$getMetaData_total rows queried." \
            [ list blue bold ]
		}
        
        lappend text "\nDatabase: " [ list black fixed ]
        lappend text "$dbname" [ list brown bold ]
        lappend text "\nSelected Users:\t" [ list black fixed ]
        lappend text "$user" [ list brown bold ]
        
        set cmdtext  [ cmonCommon::showCmdsDsos cmonStatistics $page 0 ]        
        ;## put black for the item description
        regexp {LDAS commands:\t(.+)} $cmdtext -> cmdtext1
        lappend text "\nLDAS commands: " [ list black bold ]
		lappend text "$cmdtext1\n" [ list brown bold ]

		updateTextWin [ set ::${parent}(textw) ] $text 
        
		;## set print info		
		# set ::${parent}(ptitle) "Database Statistics @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
			
		set bltdata [ list ]
        ;## reduce the data plotted to max number of points 
		foreach type { putMetaData getMetaData } {
				set data [ set ${type}_data ]
                foreach { data msg } [ cmonCommon::reduceDisplayData $data $::MAX_GRAPH_POINTS ] { break }
                if  { [ llength $msg ] } {
                    appendStatus $::cmonStatistics::statusw($page) "$type data: $msg" 0 brown
                }
				lappend bltdata $data
				lappend bltdata $type
				lappend bltdata circle 
				lappend bltdata $::cmonStatistics::color($type)
				lappend bltdata 0
		}
        if  { ! $putMetaData_total && ! $getMetaData_total } {
            set ymin 0
            set ymax 5
        }
		blt_graph::showGraph $bltdata $::cmonStatistics::xmin($page) \
				$::cmonStatistics::xmax($page) $ymin $ymax $parent
				
		raise $parent
		update idletasks

	} err ] } {
		appendStatus $::cmonStatistics::statusw($page) $err
	}
}

##*******************************************************
##
## Name: cmonStatistics::queueGrapStats 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatistics::queueGraphStats { page statsdata parent } {

	if	{ [ catch {
		
        foreach { total numFail pfailed ymin ymax passdata faildata queuesize } $statsdata { break }
		cmonStatistics::clearGraphLabels
		
		;## print info for canvas
		set ::${parent}(ptitle) "Job Queue Statistics @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
				
		set ::${parent}(title) $::cmonStatistics::title($page)
		set ::${parent}(y1label) "# jobs in queue"
		set ::${parent}(submit) "cmonStatistics::sendRequest $page"

		set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]
		lappend text "Total jobs queued: " [ list black fixed ]
        lappend text "$total" [ list brown bold ]
        
        updateTextWin [ set ::${parent}(textw) ] $text 
        
		set ymin 0
		if	{ $ymax < 5 } {
			set ymax 5
			set ymin 0
		}
                
		set bltdata [ list ]
        foreach { data msg } [ cmonCommon::reduceDisplayData $queuesize $::MAX_GRAPH_POINTS ] { break }
		lappend bltdata $data
		lappend bltdata queuesize
		lappend bltdata circle 
		lappend bltdata $::cmonStatistics::color(queuesize)
		lappend bltdata 0

		blt_graph::showGraph $bltdata $::cmonStatistics::xmin($page) \
				$::cmonStatistics::xmax($page) $ymin $ymax $parent
		
		raise $parent
		update idletasks
        if  { [ llength $msg ] } {
            appendStatus $::cmonStatistics::statusw($page) "queue data: $msg" 0 brown
        }
	} err ] } {
		appendStatus $::cmonStatistics::statusw($page) $err
	}
}


##*******************************************************
##
## Name: cmonStatistics::dispGraph
##
## Description:
## display graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatistics::dispGraph { page html } {

	set site [ string trim $::cmonClient::var(site) ]
	set parent $::cmonStatistics::wTop${page}	
    if  { ! [ winfo exist $::cmonStatistics::wTop${page} ] } {
        toplevel $parent 
	    set statusw [ createTextWin $parent 6 ]  
        set ::${parent}(textw) $statusw
    }    
	wm deiconify $::cmonStatistics::wTop${page}
	wm title $::cmonStatistics::wTop${page} "$::cmonStatistics::tab($page)@$site"

	switch -regexp -- $page {
		job  { jobGraphStats $page $html $parent }
		metadata { metadataGraphStats $page $html $parent }
		queue  { queueGraphStats $page $html $parent }
		default  { puts "nothing done" }
	}
}


##*******************************************************
##
## Name: cmonStatistics::reset
##
## Description:
## reset data
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatistics::reset { page } {
    
    cmonCommon::updateDBinfo cmonStatistics $page
    
}

##*******************************************************
##
## Name: cmonStatistics::reset
##
## Description:
## reset data
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatistics::reset { page } {
    
    cmonCommon::updateDBinfo cmonStatistics $page
    
}

if  { ![ info exist ::BROWSER ] } {
    foreach browser { firefox mozilla konqueror } {        
        if  { [ string length [ auto_execok $browser ] ] } {
            set ::BROWSER $browser
            break
        }
    }
}

#*******************************************************
##
## Name: cmonStatistics::getReportFiles
##
## Description:
## download report data to tmp directory
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonStatistics::getReportFiles { html } {

    if  { [ catch {
        regexp {(http.+/ldas_outgoing/.+cmonClient\S+)} $html -> url
        puts "url $url"
        catch { exec $::BROWSER $url & }
        
        #set fname [ string trimright $fname . ]
        #set output $::TMPDIR/[ file tail $fname ]
        #set result [ httpGetFile $url $output ]
        ;## let browser download the logfile
        #catch { exec $::BROWSER $output & } err
    } err ] } {
        return -code error $err
    }
}
