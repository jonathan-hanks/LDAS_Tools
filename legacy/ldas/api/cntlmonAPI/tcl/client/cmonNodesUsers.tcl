## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script displays the load and cpu usage information:
## on all the Bwatch nodes.
## 
## ******************************************************** 

package provide cmonNodesUsers 1.0

namespace eval cmonNodesUsers {
    set notebook ""
    set pages { Check-Users-Nodes }
    set title(Check-Users-Nodes) "Verify All Search Users and Nodes"
	set delay { 30 60 120 } 
	set repeat { 0 5 100 10000 } 
    set freq { 30 60 900 3600 } 
	set freqUnit 1000
	set freqUnits secs
	set fmtstr "%-10s  %10s  %10s  %40s  %-40s"
	
	set userlen 10
	set jobidlen 15
	set numNodeslen 10
	set nodelistlen  40
	set objlen 40
    array set servercmd [ list Check-Users-Nodes runTest ]
    array set cancelcmd [ list Check-Users-Nodes "cmonNodesUsers::cancelRequest \$page"  ]
}

## ******************************************************** 
##
## Name: cmonNodesUsers::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesUsers::create { notebook } {
    
    foreach page $::cmonNodesUsers::pages {    
        regsub -all {\-} $page { } pagetext
        $notebook insert end $page -text $pagetext \
        -createcmd "cmonNodesUsers::createPages $notebook $page" \
        -raisecmd [ list cmonCommon::pageRaise cmonNodesUsers $page $pagetext  ]
    }
}

## ******************************************************** 
##
## Name: cmonNodesUsers::create 
##
## Description:
## creates Bwatch page
##
## Parameters:
## notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesUsers::createPages { notebook page } {

	set frame [ $notebook getframe $page ]

    set topf  [frame $frame.topf]	
	set ::cmonNodesUsers::notebook $notebook	
    specific $topf $page 
	set ::cmonNodesUsers::topfw($page) $topf
	pack $topf -fill x -pady 2
	
	foreach [ list pw1 pane2 pane3 ] [ createPaneWin $frame ] { break }
	regsub -all {\-} $page { } pagetext
    
    ;## create a status to display errors and status
	set titf2 [TitleFrame $pane2.titf2 -text "Command Status:" -font $::ITALICFONT ]
	set statusw [ ::createStatus  [ $titf2 getframe ] ]
	array set ::cmonNodesUsers::statusw [ list $page $statusw ]
	pack $titf2 -pady 2 -padx 4 -fill both -expand yes
	
	;## create a display window to hold test results
	set titf3 [TitleFrame $pane3.titf3 -text $pagetext -font $::ITALICFONT ]
	set tframe [ $titf3 getframe ]
    set textw [ ::createDisplay [ $titf3 getframe ] ]
    $textw configure -bg PapayaWhip
    array set ::cmonNodesUsers::textw [ list $page $textw ]
	pack $titf3 -pady 2 -padx 4 -fill both -expand yes
    
    set ::cmonNodesUsers::titf3($page) $titf3 
    
	pack $pw1 -side top -fill both -expand 1
    array set ::cmonNodesUsers::state [ list $page 0 ]
    set ::cmonNodesUsers::prevsite($page) none
    return $frame
}

## ******************************************************** 
##
## Name: cmonNodesUsers::specific
##
## Description:
## creates specific action area for the page types
##
## Parameters:
## page
## parent widget  
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesUsers::specific { parent page { repeat 1 } } {

	set labfent [LabelFrame $parent.fent -text $::cmonNodesUsers::title($page) \
		-side top -anchor w \
        -relief sunken -borderwidth 4 ]
    set subf1 [ $labfent getframe ]
	set cmd $::cmonNodesUsers::servercmd($page) 
	Button $subf1.start -text SUBMIT -width 10 \
        -command  "::cmonNodesUsers::sendRequest $page $cmd" 
	set ::cmonNodesUsers::bstart($page) $subf1.start	
	pack $subf1.start -side left -padx 10 -pady 10
    set but2 [ Button $subf1.tips -image $::image_tips -width 10 \
		-helptext "What is this test about" \
        -command  "::cmonNodesUsers::helpTips $page" ]
    set ::cmonNodesUsers::helpbutton($page) $but2
    
    pack $subf1.start $subf1.tips -side left -padx 5 -pady 5
    pack $labfent -side left -padx 4 -fill both -expand 1
    
	set labrefresh [LabelFrame $parent.refresh -text "Last test results " \
        -side top -anchor w -relief sunken -borderwidth 4 ]
	set subf2 [ $labrefresh getframe ]
	set name [ namespace tail [ namespace current ] ]
 
    set lbl1 [ label $subf2.indicator -text "" -font $::MSGFONT -state disabled ]
    set ::cmonNodesUsers::indicatorw($page) $lbl1
    pack $lbl1 -side top -fill both -expand 1
    pack $labrefresh -side right -padx 4 -fill both -expand 1	  
 
}

## ******************************************************** 
##
## Name: cmonNodesUsers::sendRequest 
##
## Description:
## send request 
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesUsers::sendRequest { page } {

	set site $::cmonClient::var(site)
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonNodesUsers::statusw($page) "Please select an LDAS site" 
		return 
	}

	;## let server check if there is mpi
    set cmdId new
    if  { [ info exist ::cmonNodesUsers::var($page,repeat ] } {
        set repeat $::cmonNodesUsers::var($page,repeat)
        set freq [ expr $::cmonNodesUsers::var($page,freq)*$::cmonNodesUsers::freqUnit ]    
    } else {
        set repeat 0
        set freq 0
    }
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    

    if  { [ catch {
        foreach { login passwd } [ validateLogin 1 ] { break }
    } err ] } {
		appendStatus $::cmonNodesUsers::statusw($page) $err
		return        
    }
    
   	if	{ $::USE_GLOBUS } {
        set userinfo "-name $login -password $passwd -email persistent_socket"
    } else {
    	set userpass [ base64::encode64 [ binaryEncrypt $::CHALLENGE $::cntlpasswd ] ]
        set userinfo "-name $::logintext -password $userpass -email 12345:678"
    }
    regsub -all -- {\s+} $userinfo { } userinfo
    set cmd "cntlmon::[ set ::cmonNodesUsers::servercmd($page) ]"
    set cmd "cmonNodesUsers::showReply $page\n$repeat\n$freq\n\
    $client:$cmdId\n$cmd nodesUsers \{$userinfo\}" 	
	cmonNodesUsers::state1 $page 
    updateDisplay $::cmonNodesUsers::textw($page) ""
    if  { [ catch {
		sendCmd $cmd
        cmonNodesUsers::state1 $page 
	} err ] } {
		 cmonNodesUsers::state0 $page 
         appendStatus $::cmonNodesUsers::statusw($page) $err 
	}
}

## ******************************************************** 
##
## Name: cmonNodesUsers::showReply 
##
## Description:
## display results from server 
##
## Parameters:
## page
## rc   -   return code for request 
## clientCmd
## html -   result text 
##
## Usage:
##  handler for cmd results
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesUsers::showReply { page rc clientCmd html } {
    
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "rc=$rc showReply, $page, client=$client,cmdId=$cmdId, html\n$html"
	set ::cmonNodesUsers::cmdId($page) $cmdId 
    set ::cmonClient::client $client

	switch $rc {
	0 { if  { [ regexp {<html>} $html ] } {
                updateDisplay $::cmonNodesUsers::textw($page) $html
            } else {
                appendStatus $::cmonNodesUsers::statusw($page) $html
            }
            if  { [ regexp -nocase ERROR $html ] } {
                set color red
                set text FAILED
            } else {
                set color $::brightgreen
                set text PASSED
            }
            cmonNodesUsers::updateTestStatus $page $color $text
            cmonNodesUsers::state0 $page
        }
                
	2 {	if	{ [ regexp {<html>} $html ] } {
            updateDisplay $::cmonNodesUsers::textw($page) $html
        } else {
            appendStatus $::cmonNodesUsers::statusw($page) $html 0 blue
        }
	  }
	3 { updateDisplay $::cmonNodesUsers::textw($page) ""
		appendStatus $::cmonNodesUsers::statusw($page) $html
		;## may need to cancel request	
		cmonNodesUsers::state0 $page 	
	  }
	}	
}

## ******************************************************** 
##
## Name: cmonNodesUsers::cancelRequest 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesUsers::cancelRequest { page } {
	
	set cmdId $::cmonNodesUsers::cmdId($page)
    set client $::cmonClient::client
    set repeat 0
    set freq 0
	if	{ [ catch {
		set cmd "cmonNodesUsers::showReply $page\n0\n0\n\
        $client:$cmdId\ncntlmon::cancelAllCmdsTest nodesUsers" 
		#puts "cmd=$cmd"
		cmonNodesUsers::state3 $page 
		sendCmd $cmd		 
	} err ] } {
		cmonNodesUsers::state0 $page 
		appendStatus $::cmonNodesUsers::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonNodesUsers::state0
##
## Description:
## go back to initial state 
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesUsers::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonNodesUsers::topfw($page) normal
	$::cmonNodesUsers::bstart($page) configure -state normal -text \
   		SUBMIT -command "cmonNodesUsers::sendRequest $page" \
      	-state normal 
	array set ::cmonNodesUsers::state [ list $page 0 ]
    
}


## ******************************************************** 
##
## Name: cmonNodesUsers::state1
##
## Description:
## disable buttons while waiting for request to be processed.
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesUsers::state1 { page } {
    set name [ namespace tail [ namespace current ] ]
    updateWidgets $::cmonNodesUsers::topfw($page) disabled
	$::cmonNodesUsers::bstart($page) configure -state disabled
	array set ::cmonNodesUsers::state [ list $page 1 ]
    updateDisplay $::cmonNodesUsers::textw($page) ""
    [ set ::cmonNodesUsers::indicatorw($page) ] configure -state disabled -text "" 
    ;## toggle to cancel
    set cmd [ subst $::cmonNodesUsers::cancelcmd($page) ]
    [ set ::cmonNodesUsers::helpbutton($page) ] configure -state normal
    $::cmonNodesUsers::bstart($page) configure -state normal -text \
   		CANCEL -command $cmd
}

## ******************************************************** 
##
## Name: cmonNodesUsers::state2
##
## Description:
## cancel monitor
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesUsers::state2 { page } {
	if	{ $::cmonNodesUsers::state($page) != 2 } {
		set cmd [ subst $::cmonNodesUsers::cancelcmd($page) ]
    	$::cmonNodesUsers::bstart($page) configure -text "CANCEL" \
        -state normal -command $cmd
		array set ::cmonNodesUsers::state [ list $page 2 ]
	}
}

##******************************************************** 
##
## Name: cmonNodesUsers::state3  
##
## Description:
## cancelling a repeated request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesUsers::state3 { page } {
    $::cmonNodesUsers::bstart($page) configure -state disabled
	array set ::cmonNodesUsers::state [ list $page 3 ]
    
}

##******************************************************** 
##
## Name: cmonNodesUsers::updateTestStatus
##
## Description:
## mark pages to reset
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments

proc cmonNodesUsers::updateTestStatus { page color status } {

    set time [ clock format [ clock seconds ] -format "%m-%d-%Y %H:%M:%S" ]
    [ set ::cmonNodesUsers::indicatorw($page) ] configure -state normal -fg $color \
         -text "$time: $::cmonClient::var(site) TEST $status"
  
}

##******************************************************* 
##
## Name: cmonNodesUsers::helpTips
##
## Description:
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesUsers::helpTips { page } {

	set msg "This test exercises all search users on all nodes \
by submitting a dataPipeline job for every search user to run on all nodes.\
The test passes when:\n\
1. All jobs complete successfully.\n\
2. Each search user has been invoked to run a job on all nodes, as \
confirmed by MPI and wrapper logs.\n\
This test should be run on LDAS without any other jobs running."

	catch { set ack [ MessageDlg .filtertips -title Tips -type ok -aspect 300 \
		-message $msg -icon info -justify left -font $::MSGFONT ] }
		
}
