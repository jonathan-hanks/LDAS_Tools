## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles LDAS log filtering 
##
## cmonNodesStat Version 1.0
##
## ******************************************************** 

package provide cmonNodesStat 1.0

namespace eval cmonNodesStat {
    set notebook ""
    set pages { NodesUsageStat }
	set dbaccess [ list putMetaData getMetaData ]
	set tab(NodesUsageStat) "Node Statistics"
	set title(NodesUsageStat) "MPI Nodes Usage Statistics" 
	set xlabel(NodesUsageStat,time) "GPS Time"
	set ylabel(NodesUsageStat,time) "# Nodes\nallocated"
	set xlabel(NodesUsageStat,hist) "# Nodes allocated"
	set ylabel(NodesUsageStat,hist) "# Jobs"
	set color(mpi) blue
	set wTop .tmpNodesTime
	set wHist .tmpJobNodes
    set pagetext "Node Statistics"
}

## ******************************************************** 
##
## Name: cmonNodesStat::create
##
## Description:
## creates job page tab
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesStat::create { notebook } {
    set page [ lindex $::cmonNodesStat::pages 0 ] 
    $notebook insert end cmonNodesStat -text $::cmonNodesStat::pagetext \
        -createcmd "cmonNodesStat::createPages $notebook $page" \
        -raisecmd "cmonCommon::pageRaise cmonNodesStat $page ; \
            cmonCommon::setLastTime cmonNodesStat $page"
}

## ******************************************************** 
##
## Name: cmonNodesStat::createPages 
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesStat::createPages { notebook page } {

	if	{ [ catch { 
    	set frame [ $notebook getframe cmonNodesStat ]		
		specific $frame $page 
				
        array set ::cmonNodesStat::state [ list $page 0 ]	
        set ::cmonNodesStat::prevsite($page) $::cmonClient::var(site)
	} err ] } {
		puts $err
		return -code error $err
	}
}

## ******************************************************** 
##
## Name: cmonNodesStat::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonNodesStat::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]
	
    set labf1 [LabelFrame $parent.labf1 -text "Select times" -side top -anchor w \
                   -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]
    set ::cmonNodesStat::topfw($page) $subf
    
	set f2 [ frame $subf.ftime -relief groove -borderwidth 2 ]
	set f [ cmonCommon::createTimeOptions $f2 $name $page ]	
	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $subf.ftime $name $page ] { break }
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x -expand 1
	
	#pack $fcktime $f1 $f2 -side top -anchor w -expand 1 -fill both
	
    pack $f2 -side top -anchor w -expand 1 -fill x 
	;## place the cmd status on the right
	set labref [LabelFrame $parent.refresh -text "Command Status" -side top -anchor w \
                   -relief sunken -borderwidth 4]
                   
    set subf2  [ $labref getframe ]
	
	set statusw [ ::createStatus  $subf2 ]
	array set ::cmonNodesStat::statusw [ list $page $statusw ]
	
	frame $subf.fstart 
    set  but1 [ Button $subf.fstart.start -text "SUBMIT" -width 5 -helptext "submit one time request to server" \
        -command  "::cmonNodesStat::sendRequest $page" ]

	set ::cmonNodesStat::bstart($page) $but1  
	
    pack $but1 -side left -padx 100 -pady 5  	
    pack $subf.fstart -side top -fill x	
	pack $labf1 -side top -fill x
    pack $labref -side top -fill both -expand 1

}

## ******************************************************** 
##
## Name: cmonNodesStat::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
## this proc name is generic for all packages.
## sample test times 
## start 		05/04/00 16:38:27 641493520
## end times 	05/04/00 18:13:24 641499217
##	start		957892986	641928192
## end 		957893198	641928404
##	start		957892986	641928192
## end			641928192	641928192
## start		now
## end			now
	
proc cmonNodesStat::sendRequest { page } {

	set name [ namespace tail [ namespace current ] ]

	if	{ [ catch {
		if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		  	error "Please select an LDAS site"
		}
        
		;## need to fill in gap to make filter line up
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
		set ::cmonNodesStat::xmin($page) $starttime
		set ::cmonNodesStat::xmax($page) $endtime
		set servercmd "cntlmon::nodesUsageGraph $starttime-$endtime"
        set client $::cmonClient::client
        set cmdId "new"
		set freq 0
    	set repeat 0
       	set cmd "cmonNodesStat::showReply $page\n$repeat\n$freq\n\$client:$cmdId\n$servercmd"      
		#puts "cmd=$cmd"
		cmonNodesStat::state1 $page 
		sendCmd $cmd 
	} err ] } {
		cmonNodesStat::state0 $page 
		appendStatus $::cmonNodesStat::statusw($page) $err 0 red 1
	}
}

## ******************************************************** 
##
## Name: cmonNodesStat::showReply 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## process the data received from server

proc cmonNodesStat::showReply { page rc clientCmd html } {

	if	{ [ catch {
    set name [ namespace tail [ namespace current ] ]
    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    # puts "in showReply, rc=$rc, page=$page, client=$client,cmdId=$cmdId, afterid=$afterid"
	# puts "html size [ string length $html ]"
    set ::cmonNodesStat::cmdId($page) $cmdId 
    set ::cmonClient::client $client
    	
	switch $rc {
	0 -
	2 {	if	{ [ string length $html ] } {
        ;## if true html, put in text window
        ;## else put in status window for status msgs.
			cmonNodesStat::dispGraph $page $html 
			appendStatus $::cmonNodesStat::statusw($page) updated 0 blue
        } else {
            appendStatus $::cmonNodesStat::statusw($page) \
                "No data" 
        }
		if	{ ! [ string length $afterid ] } {
                cmonNodesStat::state0 $page 
            } else {
                cmonNodesStat::state2 $page 
        }
	  }
	3 {
		appendStatus $::cmonNodesStat::statusw($page) $html
		;## may need to cancel request	
		cmonNodesStat::state0 $page 	
	  }
	}
	} err ] } {
		appendStatus $::cmonNodesStat::statusw($page) $err
		cmonNodesStat::state0 $page 
	}
}

##******************************************************* 
##
## Name: cmonNodesStat::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button   

proc cmonNodesStat::state0 { page } {
    set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonNodesStat::topfw($page) normal
	$::cmonNodesStat::bstart($page) configure -state normal -text "SUBMIT" \
		-command "${name}::sendRequest $page"
	array set ::cmonNodesStat::state [ list $page 0 ]
}

##******************************************************* 
##
## Name: cmonNodesStat::state1  
##
## Description:
## request sent, awaiting reply from server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## disable go button and repeats

proc cmonNodesStat::state1 { page } {
	set name [ namespace tail [ namespace current ] ]
	updateWidgets $::cmonNodesStat::topfw($page) disabled	
    $::cmonNodesStat::bstart($page) configure -state disabled 
	array set ::cmonNodesStat::state [ list $page 1 ]
	
}

##*******************************************************
##
## Name: cmonNodesStat::nodesUsageStats 
##
## Description:
## graph the data from job statistics
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesStat::nodesUsageStats { page statsdata parent } {
    
    if  { [ catch {
        set gps_start $::cmonNodesStat::xmin($page)
        set gps_end   $::cmonNodesStat::xmax($page)
		set localstart [ gps2utc $gps_start 0 ]
		set localend [ gps2utc $gps_end 0 ]
        
        ;## print info
		set ::${parent}(ptitle) "Node Usage by Jobs @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"

        set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]
        
        set dataitems "npoints ymin ymax timedata histdata" 
		set cmd "list $dataitems"
		foreach [ eval $cmd ] $statsdata { break }
		lappend text "$npoints jobs" [ list brown bold ]
        
		set ::${parent}(title) "Node Usage vs Time"
		set ::${parent}(y1label) "# Nodes"		
		set ::${parent}(submit) "cmonNodesStat::sendRequest $page"	

		;## print info
		# set ::${parent}(ptitle) "Node Usage by Jobs @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"

        updateTextWin [ set ::${parent}(textw) ] $text 
        
        if  { !$npoints} {
            set points [ list ]
            set ymin 0
            set ymax 5
        } else {
            foreach { npoints ymin ymax points } $statsdata { break }
        }
        foreach { points msg } [ cmonCommon::reduceDisplayData $points $::MAX_GRAPH_POINTS ] { break }
        if  { [ llength $msg ] } {
            appendStatus $::cmonNodesStat::statusw($page) "node usage: $msg" 0 brown
        }
		set bltdata [ list $points nodes circle blue 0 ]
		blt_graph::showGraph $bltdata $::cmonNodesStat::xmin($page) \
			$::cmonNodesStat::xmax($page) $ymin $ymax $parent
		raise $parent
    } err ] } {
        appendStatus $::cmonNodesStat::statusw($page) $err
	}
}

##*******************************************************
##
## Name: cmonNodesStat::dispGraph
##
## Description:
## display graph
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesStat::dispGraph { page html } {

	set site [ string trim $::cmonClient::var(site) ]
    
    if  { ! [ file exist $::TMPDIR ] } {
        file mkdir $::TMPDIR
    }
    set histwin $::cmonNodesStat::wHist${page}
    set fname $::TMPDIR/[ string trimleft $histwin . ].$site
    set fd [ open $fname  w ]
    puts $fd $html
    close $fd

	set parent $::cmonNodesStat::wTop${page}	
    
    if  { ! [ winfo exist $::cmonNodesStat::wTop${page} ] } {
        toplevel $parent 
	    set statusw [ createTextWin $parent 5 ]  
        set ::${parent}(textw) $statusw
        set ::${parent}(replot) [ list cmonNodesStat::replot $page $histwin ]
    }    
	wm deiconify $parent
    wm title $parent "$::cmonNodesStat::tab($page)@$site"
    set statusw [ set ::${parent}(textw) ]
    bind $statusw <Destroy> [ list cmonNodesStat::closeWin $page $parent $histwin $site ]	
    wm protocol $parent WM_DELETE_WINDOW [ list cmonNodesStat::closeWin $page $parent $histwin $site ]
    cmonNodesStat::nodesUsageStats $page $html $parent
    cmonNodesStat::updateHistogram $page $histwin

}

##*******************************************************
##
## Name: cmonNodesStat::histogram
##
## Description:
## graph #nodes vs #jobs
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesStat::histogram { page statsdata parent { cummPlot 0 } } {

	if	{ [ catch {
		set jobtotal [ lindex $statsdata 0 ]
        set gps_start  $::cmonNodesStat::xmin($page)
        set gps_end    $::cmonNodesStat::xmax($page)
		set localstart [ gps2utc $gps_start 0 ]
		set localend   [ gps2utc $gps_end 0 ]
        
		;## print info for parent
		set ::${parent}(ptitle) "Node Usage Histogram @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
        
        set text [ list ]
        lappend text  "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown fixed ]
     
		set dataitems "jobs ymin ymax timedata histdata" 
		set cmd "list $dataitems"
		foreach [ eval $cmd ] $statsdata { break }
			
		set dataitems "jobhistdata"
		
		set cmd "list $dataitems"
		foreach [ eval $cmd ] $statsdata { break }
		
		;## print info for parent
		# set ::${parent}(ptitle) "Node Usage Histogram @$::cmonClient::var(site) LDAS $::cmonClient::serverVersion"
				
		lappend text "$jobs jobs" [ list brown bold ]
      
        if  { ! [ info exist ::${parent}(bucketsz) ] } {
            set ::${parent}(bucketsz) 1
        }
        set binsize [ set ::${parent}(bucketsz) ]
		set ::${parent}(title) "Node Usage by Jobs"
		set ::${parent}(y1label) "# Jobs"	
		set ::${parent}(x1label) "# Nodes allocated"
		set ::${parent}(x1step) $binsize
		set ::${parent}(replot) "cmonNodesStat::replot $page $parent"
		set ::${parent}(submit) "cmonNodesStat::sendRequest $page"
        set ::${parent}(histargs) "cmonNodesStat $page [ set ::BIN_RANGE(cmonNodesStat) ]"
        
        set binsize [ set ::${parent}(bucketsz) ]
        
        set histdata [ list ]
        
        if  { [ llength $timedata ] } {   
            counter::init nodetimes -hist $binsize
            set ylist [ list ]
            foreach { x y } $timedata {
                counter::count nodetimes $y
                lappend ylist $y
            }
            foreach { xtotal xmean xmedian xstddev xcor } [ bltstats $ylist ] { break }
            set ylist [ lsort -unique $ylist ]
            set ymin [ lindex $ylist 0 ]
            set ymax [ lindex $ylist 1 ]
            unset ylist
            set histdata [ counter::getDBHistData nodetimes ]
            counter::init nodetimes
        } 
        if  { $cummPlot } {
            set histdata [ cmonCommon::getHistCummData $histdata $binsize ]
            set ::${parent}(y1label) "% Jobs, cumulative"
        } 
		foreach { xmin ymin xmax ymax } [ cmonCommon::computeXYMinMax $histdata $binsize ] { break }

		set bltdata [ list ]
		lappend bltdata $histdata
		lappend bltdata jobs
		lappend bltdata blue
		lappend bltdata $binsize

		set graph [ blt_graph::barChart $bltdata $xmin $xmax $ymin $ymax $parent ]
        set statstext ""
        if  { [ info exist xmean ] } {
            append statstext "X axis total=$xtotal, mean=$xmean, median=$xmedian, std-dev=$xstddev;\n"           
        }
        if  { [ info exist ::${parent}(ystats) ] } {
            foreach { ytotal ymean ymedian ystddev }  [ set ::${parent}(ystats) ] { break }
            append statstext "Y axis total jobs=$ytotal, mean=[ expr round($ymean) ], median=[ expr round($ymedian) ], std-dev=[ format %.2f $ystddev ]"
        } 
        lappend text "\n$statstext" [ list brown bold ]
        updateTextWin [ set ::${parent}(textw) ] $text 
		raise $parent
		update idletasks
	} err ] } {
		appendStatus $::cmonNodesStat::statusw($page) $err
	}
}

##*******************************************************
##
## Name: cmonNodesStat::replot
##
## Description:
## replot the graph again, based in points or lines
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesStat::replot { page parent } {

    set site [ string trim $::cmonClient::var(site) ]
    set fname $::TMPDIR/[ string trimleft $parent . ].$site
	if	{ [ file exist $fname ] } {
		set fd [ open $fname r  ]
		set html [ read $fd  ]
		close $fd
        cmonNodesStat::createHistWin $page $parent
		histogram $page $html $parent [ set ::${parent}(cummPlot) ]
	} else {
		appendStatus $::cmonNodesStat::statusw($page) \
		"Unable to replot for $site; please resubmit your request to server"
	}	
}


##*******************************************************
##
## Name: cmonNodesStat::createHistWin
##
## Description:
## create histogram window
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesStat::createHistWin { page parent } {

    if  { [ catch {
	    set site [ string trim $::cmonClient::var(site) ]
        if  { ! [ winfo exist $parent ] } {
            toplevel $parent 
	        set statusw [ createTextWin $parent 5 ]  
            set ::${parent}(textw) $statusw
            set ::${parent}(cummPlot) 0
        }    
	    wm deiconify $parent
        wm title $parent "$::cmonNodesStat::tab($page) Histogram @$site"
    } err ] } {
         return -code error $err
    }
    return $parent 

}

##*******************************************************
##
## Name: cmonNodesStat::updateHistogram
##
## Description:
## update histogram if it is already displayed
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonNodesStat::updateHistogram { page histwin } {

    if  { [ winfo exist $histwin ] } {
        cmonNodesStat::replot $page $histwin
    }
}

## ******************************************************** 
##
## Name: cmonDBview::closeWin 
##
## Description: 
## report on exact gaps 
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc cmonNodesStat::closeWin { page parent histwin site } {
   
    set fname $::TMPDIR/[ string trimleft $histwin .].$site
    file delete -force $fname
    destroy $parent
    catch { destroy $histwin }

}
