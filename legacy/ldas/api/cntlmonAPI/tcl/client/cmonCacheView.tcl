## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles display of frame times in diskcache 
##
## cmonCacheView Version 1.0
##
## ******************************************************** 

package provide cmonCacheView 1.0

namespace eval cmonCacheView {
    set notebook ""
    set pages [ list cmonCacheView ]
    set tab(cmonCacheView) "Cache View"
    set color(H) #568c3a
    set color(L) #568c3a
    set color(G) #568c3a
    set user_color #568c3a
    set wTop .tmpCacheView
    set color_Gaps PapayaWhip
    set color_noGaps #568c3a
    set fill_linewidth 1
    set data_linewidth 2
    set wGaps .log-cmonCacheView
}

## ******************************************************** 
##
## Name: cmonCacheView::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCacheView::create { notebook } {

    set ::cmonCacheView::notebook $notebook

    foreach page $::cmonCacheView::pages {
        $notebook insert end $page -text $::cmonCacheView::tab($page)  \
        -createcmd "cmonCacheView::createPages $notebook $page" \
        -raisecmd "cmonCommon::pageRaise cmonCacheView $page; \
            cmonCommon::setLastTime cmonCacheView $page; \
            cmonCommon::updateDBinfo cmonCacheView $page" 
    }
}

## ******************************************************** 
##
## Name: cmonCacheView::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCacheView::createPages { notebook page } {
    
	if	{ [ catch { 
        set frame  [ $notebook getframe $page ]	
		specific $frame $page 
        
	    ;## place the cmd status on the bottom
	    set titf2 [TitleFrame $frame.refresh -text "Command Status"  \
                   -relief sunken -borderwidth 4]
                   
        set subf2  [ $titf2 getframe ]
	
	    set statusw [ ::createStatus  $subf2 ]
	    array set ::cmonCacheView::statusw [ list $page $statusw ]
	    
        pack $titf2 -side top -fill both -expand 1 -anchor n     
        array set ::cmonCacheView::state [ list $page 0 ]	
        set ::cmonCacheView::prevsite($page) $::cmonClient::var(site)
	} err ] } {
        puts $err
		return -code error $err
	}
}
## ******************************************************** 
##
## Name: cmonCacheView::specific 
##
## Description:
## creates a specific page
##
## Parameters:
## parent widget
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonCacheView::specific { parent page } {

	set name [ namespace tail [ namespace current ] ]
    
    ;## elements of framequery
    ;## entries for frame types, inferometers, frames 
    
    set labfent [LabelFrame $parent.fent -side top -anchor w \
            -relief sunken -borderwidth 4 -font $::ITALICFONT -text \
			"Specify query of cached frame times" ]
    set subf [ $labfent getframe ]
	set ::cmonCacheView::topfw($page) $subf
	set subf1 [ frame $subf.subf1 -relief sunken -borderwidth 2 ]
    
    set ftype [ frame $subf1.ftype -relief flat -borderwidth 2 ]
    cmonCommon::createFrameTypes $name $page $ftype
        
    set fifos [ frame $subf1.fifos -relief flat -borderwidth 2 ]
    cmonCommon::createIFOs $name $page $fifos
    
    message $subf1.msg -font $::LISTFONT -text "" \
		-justify left -aspect 600 -foreground brown -anchor w
     
    set ::cmonCacheView::wselectmsg($page) $subf1.msg
    
    pack $ftype $fifos -side top -anchor w -fill x -expand 1 
    
    pack $subf1.msg -side top -anchor w -fill both -expand 1 
     
    trace variable ::cmonCacheView::ftypes($page) w "cmonCacheView::updateMsg $page"
    trace variable ::cmonCacheView::ifos($page) w "cmonCacheView::updateMsg $page"
    
	set subf2 [ frame $subf.subf2 -relief ridge -borderwidth 2 ]    
	set f2 [ frame $subf2.ftime -relief groove -borderwidth 2 ]
	set f [ cmonCommon::createTimeOptions $f2 $name $page ]
	
	foreach { fstart fend } [ cmonCommon::createGPSTimeWidget $f2 $name $page ] { break }
	
	pack $f $fstart $fend -side top -padx 2 -pady 1 -fill x -expand 1
	set ::cmonCacheView::var($page,type) userid
	pack $subf2.ftime -side top -anchor w -expand 1 -fill both
	
	pack $subf1 $subf2 -side left -fill both -expand 1
    
	set f3 [ frame $subf2.fstart ]
    set  but1 [ Button $f3.start -text "SUBMIT" -width 5 -helptext "submit one time request to server" \
        -command  "::cmonCacheView::sendRequest $page" ]

	set ::cmonCacheView::bstart($page) $but1  
	
    pack $but1 -side left -padx 100 -pady 5  	
    pack $f3 -side top -expand 1 -fill x	
	pack $labfent -side top -padx 4 -fill x -anchor n

}

## ******************************************************** 
##
## Name: cmonCacheView::sendRequest 
##
## Description:
## send queryLog cmd to server
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:
	
proc cmonCacheView::sendRequest { page } {

    if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonCacheView::statusw($page) "Please select an LDAS site"
		return 
	}
    set login ""
	set passwd ""
    set cmdId new
    set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set freq 0
    set repeat 0
    
    if	{ $::USE_GLOBUS } {
    	if  { [ catch {
        	foreach { login passwd } [ validateLogin 0 ] { break }
            set userinfo "-name $login -password $passwd -email persistent_socket"
    	} err ] } {
			appendStatus $::cmonCacheView::statusw($page) $err
			return        
		}
    	if  { [ string equal cancelled $login ] } {
			return
		}
    } else {
    	set userpass [ base64::encode64 [ binaryEncrypt $::CHALLENGE $::passwdtext ] ]
        set userinfo "-name $::logintext -password $userpass -email persistent_socket"
	}

    if  { [ catch {
	    
        regsub -all -- {\s+} $userinfo { } userinfo
        
        ;## need to fill in gap to make filter line up
        foreach { starttime endtime } [ cmonCommon::timeValidate $page $name ] { break }
		set ::cmonCacheView::xmin($page) $starttime
		set ::cmonCacheView::xmax($page) $endtime
        
        set cmd "cacheGetTimes "
        set types $::cmonCacheView::ftypes($page) 
        set sites $::cmonCacheView::ifos($page)
        
        foreach entry { types sites } {
            if  { [ regexp {user-specified|^[\s\t\n]*$} [ set $entry ] ] } {
                error "Please enter your user-specified data for $entry."
            }
        }
            
        cmonCacheView::queryInit $page $sites $types

        append cmd "-types [ list $types ]"
        append cmd " -ifos [ list $sites ]"
        append cmd " -start $starttime -end $endtime"
        
        set ::cmonCacheView::framequery($page) "type $types, sites $sites, times $starttime-$endtime"
        set cmd "cmonCacheView::showReply $page\n$repeat\n$freq\n\
        $client:$cmdId\ncntlmon::submitLDASjob $page \{$userinfo\} \{$cmd\}"
        cmonCacheView::state1 $page
        #puts "cmd=$cmd"
        sendCmd $cmd 
    } err ] } {
        cmonCacheView::state0 $page 
        appendStatus $::cmonCacheView::statusw($page) $err
	}
}


## ******************************************************** 
##
## Name: cmonCacheView::showReply
##
## Description:
## display results from server 
##
## Parameters:
## parent widget 
## rc 	-	return code from cmd to server
## clientCmd -	client name and cmd Id
## html	-	text returned from cmd
##
## Usage:
##  called a button command 
## Comments:
## this proc name is generic for all packages.

proc cmonCacheView::showReply { page rc clientCmd html } {  

    set clientCmd [ split $clientCmd : ]
    set client [ lindex $clientCmd 0 ]
    set cmdId [ lindex $clientCmd 1 ]
    set afterid [ lindex $clientCmd 2 ]
    #puts "showReply, rc=$rc, $page, client=$client,cmdId=$cmdId, afterid=$afterid,html [string length $html]"
    # puts "rc=$rc,html=$html"
	set ::cmonCacheView::cmdId($page) $cmdId 
    set ::cmonClient::client $client
    
	switch $rc {

    0 { cmonCacheView::processReply $page $html 
        cmonCacheView::state0 $page 
        appendStatus $::cmonCacheView::statusw($page) Updated 0 blue
      }     
    2 { appendStatus $::cmonCacheView::statusw($page) $html 0 blue }
	3 { appendStatus $::cmonCacheView::statusw($page) $html
        cmonCacheView::state0 $page 
		;## may need to cancel request		
	  }
	}
}

## ******************************************************** 
##
## Name: cmonCacheView::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonCacheView::state0 { page { enableCmd sendRequest } }  {
	updateWidgets $::cmonCacheView::topfw($page) normal	
	$::cmonCacheView::bstart($page) configure -state normal 
}

## ******************************************************** 
##
## Name: cmonCacheView::state1  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonCacheView::state1 { page }  {

	updateWidgets $::cmonCacheView::topfw($page) disabled
}

## ******************************************************** 
##
## Name: cmonCacheView::getframes
##
## Description:
## get frame data 
##
## Parameters:
## parent widget
##
## Usage:
##  sendRequest $page
## 
## Comments:

proc cmonCacheView::getEntryData { page varname} {

    if  { [ catch {
        if  { [ regexp {^[\s\t]*$} [ set ::cmonCacheView::${varname}($page) ] ] } {
            set data {}
        } else {
            set data [ set ::cmonCacheView::${varname}($page) ]
        }
    } err ] } {
        return -code error $err
    }
    return $data
}

## ******************************************************** 
##
## Name: cmonCacheView::processReply
##
## Description:
## parses reply from manager
##
## Parameters:
## reply - email response from manager
##
## Usage:
## 
## 
## Comments:

proc cmonCacheView::processReply { page reply } {
	set xmin $::cmonCacheView::xmin($page)
	set xmax $::cmonCacheView::xmax($page)
	set ::cmonCacheView::gaplist($page) [ list ]
	if	{ [ catch {
        if  { [ regexp {results[\s\n]*([^=]+)=} $reply -> result ] } {
            # puts "result '$result', [ llength $result ]"
		    foreach { ifo_type times } $result {
                regexp {([^-]+)-(.+)} $ifo_type -> ifo type
                set points($ifo,$type) [ list ]  
                if  { [ llength $times ] } {
                    foreach { startgps endgps } $times { 
			            if	{ [ llength $points($ifo,$type) ] } {
				            set prevend   [ lindex $points($ifo,$type) end-1 ]
				            set prevy  [ lindex $points($ifo,$type) end ]
							
							;## add a noframe to finish polygon for discontinuous frames
					        lappend points($ifo,$type) $prevend $::cmonCacheView::noframe($ifo,$type)
					        lappend points($ifo,$type) $startgps $::cmonCacheView::noframe($ifo,$type)
						    lappend points($ifo,$type) $startgps $::cmonCacheView::hasframe($ifo,$type)
						    lappend points($ifo,$type) $endgps $::cmonCacheView::hasframe($ifo,$type)
                            set interval [ expr $startgps - $prevend ]
							set gaps "$ifo type $type, gap $prevend - $startgps, $interval secs"
							lappend ::cmonCacheView::gaplist($page) $ifo $type $prevend $startgps $interval                                    
			            } else 	{
						    lappend points($ifo,$type) $startgps $::cmonCacheView::hasframe($ifo,$type)
						    lappend points($ifo,$type) $endgps $::cmonCacheView::hasframe($ifo,$type)
                        }
                    }
                }
		    }
        } else {
            error $reply
        }
        set not_in_cache [ list ]
        set selectedifos $::cmonCacheView::ifos($page)
		set cmd "cmonCacheView::dispGraph $page  \[ list  "
		foreach ifo $selectedifos {
            foreach type $::cmonCacheView::ftypes($page) {
                if  { [ info exist points($ifo,$type) ] } {
			        append cmd "[ list $ifo $type $points($ifo,$type) ] "
                } else {
                    lappend not_in_cache $ifo-$type
                    append cmd "[ list $ifo $type [ list ] ] "
                }
            }
		}
        append cmd " \]"
        # puts "points [ array get points ]"
		eval $cmd
        if  { [ llength $not_in_cache ] } {
            appendStatus $::cmonCacheView::statusw($page) "[ join $not_in_cache , ] not in cache."
        }
	} err ] } {
		appendStatus $::cmonCacheView::statusw($page) "[myName] $err"
        catch { destroy $::cmonCacheView::wTop } 
	}

}


## ******************************************************** 
##
## Name: cmonCacheView::cacheFrames
##
## Description:
## 
##
## Parameters:
## parent widget
##
## Usage:
## 
## 
## Comments:

proc cmonCacheView::cacheFrames { page statsdata parent } {
  
    if  { [ catch {
        set ::cmonCacheView::newData($page) 1
        set gps_start $::cmonCacheView::xmin($page)
        set gps_end   $::cmonCacheView::xmax($page)
		set localstart [ gps2utc $gps_start 0 ]
		set localend [ gps2utc $gps_end 0 ]
        set text "From $localstart ($gps_start) to $localend ($gps_end): "
		set xmin $::cmonCacheView::xmin($page)
        set xmax $::cmonCacheView::xmax($page)
        
        set npoints 0
        set bltdata [ list ]
        set selectedifos $::cmonCacheView::ifos($page)

		foreach { ifo type points } $statsdata { 
			lappend  bltdata $points ${ifo}_${type} none $::cmonCacheView::color_noGaps $::cmonCacheView::data_linewidth
        }
        ;## print info
		set ::${parent}(ptitle) "Frame times in cache at @$::cmonClient::var(site)"
        
		set text [ list ]
        lappend text "[ set ::${parent}(ptitle) ]\n" [ list black bold ]
        lappend text "Time Period:\t" [ list black fixed ]
        lappend text "From $localstart ($gps_start) to $localend ($gps_end)\n" [ list brown bold ]
        lappend text "Query:\t" [ list black fixed ]
        lappend text "$::cmonCacheView::framequery($page)" [ list brown bold ]

		set ::${parent}(title) "Frame times in cache"
        updateTextWin [ set ::${parent}(textw) ] $text 
        
        set nspaces [ expr 10 - [ llength $selectedifos ] + 1 ]
        set ylabel $selectedifos
        set spacing [ format "%${nspaces}s" " " ]
        regsub {\s} $ylabel $spacing ylabel
        
        set ::${parent}(y1label) "\nSelected sites and types\n"
        
		set ::${parent}(submit) "cmonCacheView::sendRequest $page"	
		
        set ::${parent}(formatyticks) "cmonCacheView::FormatYTicks $page"
        
		;## print info
		# set ::${parent}(ptitle) "Frame times in cache at @$::cmonClient::var(site)"
	
		set ymin $::cmonCacheView::ymin($page)
		set ymax $::cmonCacheView::ymax($page)

        if  { [ expr $xmax - $xmin ] < 2 } {
            incr xmax 2 
        }
		set graph [ blt_graph::showGraph $bltdata $xmin $xmax $ymin $ymax $parent ]
       
        set ::cmonCacheView::graph($page) $graph
                
        catch { $graph marker delete plotbg } 
        ;## clear all markers 
        
        foreach name [ $graph marker names marker* ] {
            $graph marker delete $name
        }

        ;##set coords_noframes [ list $xmin $ymin $xmin $ymax $xmax $ymax $xmax $ymin $xmax $ymin ]
		;##$graph marker create polygon -name plotbg -coords $coords_noframes \
		;##		-fill red -under true -linewidth 0 -dashes "" -mapx GPS -mapy y        
        
        set numifos [ llength $selectedifos ]
        set i 0
        set j 0
        set selectedtypes $::cmonCacheView::ftypes($page) 
        set numtypes [ llength $selectedtypes ]
        
		foreach ifo $selectedifos {
            incr i 1
            set j 0
            foreach type $selectedtypes {
                incr j 1 
			    if	{ [ $graph element exist ${ifo}_${type} ] } {
				    set data [ $graph element cget ${ifo}_${type} -data ]
                    set npoints [ llength $data ]
				    set first [ list $xmin $::cmonCacheView::noframe($ifo,$type) [ lindex $data 0 ] $::cmonCacheView::noframe($ifo,$type) ]
				    set last [ lindex $data end-1 ]
				    set coords [ concat $first $data $last $::cmonCacheView::noframe($ifo,$type) $xmax $::cmonCacheView::noframe($ifo,$type) ]
                    if  { $npoints > 1 } {		
				        $graph marker create polygon -name markerPolygon${ifo}_${type} -coords $coords \
				            -fill $::cmonCacheView::color_noGaps -under true -element ${ifo}_${type} \
					        -linewidth $::cmonCacheView::fill_linewidth -dashes "" -mapx GPS -outline $::cmonCacheView::color_Gaps
                    } elseif { $npoints == 1 } {
                        set xvalue [ lindex $data 0 ]
                        set coords [ list $xvalue $::cmonCacheView::noframe($ifo,$type) $xvalue $::cmonCacheView::hasframe($ifo,$type) ]
                        $graph marker create line -name markerPolygon${ifo}_${type} -coords $coords \
				            -fill $::cmonCacheView::color($ifo) -under false -element ${ifo}_${type} \
					        -linewidth $::cmonCacheView::fill_linewidth -dashes "" -mapx GPS 
                    }
			    }
                if  { $j <= $numtypes } {             
                    set coords [ list $xmin $::cmonCacheView::noframe($ifo,$type) $xmax $::cmonCacheView::noframe($ifo,$type) ]
                    $graph marker create line -name markerBorder${ifo}_${type} -coords $coords \
				        -under false -element ${ifo}_${type} -linewidth 3 -dashes "" -mapx GPS -fill brown
                }
            }
            ;## create lines between each site frame except the last one
            if  { $i <= $numifos && $numifos > 1 } {             
                set coords [ list $xmin $::cmonCacheView::noframe($ifo) $xmax $::cmonCacheView::noframe($ifo) ]
                $graph marker create line -name markerBorder$ifo -coords $coords  \
				    -under false -element $ifo -linewidth 3 -dashes "" -mapx GPS
            }
    }
    ;## draw last line on top if more than 1 ifo
    #if  { $numifos > 1 } {
        set coords [ list $xmin $::cmonCacheView::hasframe($ifo) $xmax $::cmonCacheView::hasframe($ifo) ]
            $graph marker create line -name markerBorder$ifo -coords $coords \
				-under false -element $ifo -linewidth 3 -dashes "" -mapx GPS
    #}
    $graph axis configure y -subdivisions 0 -stepsize 1 -minorticks 0 -ticklength 0 -tickfont $::LISTFONT \
        -command cmonCacheView::FormatYTicks
    $graph legend configure -hide yes 
    $graph grid off
    $graph configure -width 8i -height 6i -plotbackground PapayaWhip -plotrelief ridge -plotborderwidth 2
    
    update idletasks
    
    # accentGaps
    
	$::cmonCacheView::gapsw($page) configure -command [ list cmonCacheView::gapDetails $page ]
    if  { [ winfo exist $::cmonCacheView::wGaps ] && [ winfo ismapped $::cmonCacheView::wGaps ] } {
        cmonCacheView::gapDetails $page 
    }
    raise $parent
    } err ] } {
        return -code error "[myName]: $err"
	}
}

## ******************************************************** 
##
## Name: cmonCacheView::dispGraph
##
## Description:
## 
##
## Parameters:
## parent widget
##
## Usage:
## 
## 
## Comments:

proc cmonCacheView::dispGraph { page html } {

	set site [ string trim $::cmonClient::var(site) ]
	set parent $::cmonCacheView::wTop 
    if  { ! [ winfo exist $parent ] } {
        toplevel $parent 
        set  but [ button $parent.gapList -text "List of Gaps" -fg black ]
        pack $parent.gapList -side top -anchor w
        set ::cmonCacheView::gapsw($page) $but
	    set statusw [ createTextWin $parent 3 ]  
        set ::${parent}(textw) $statusw
        wm protocol $parent WM_DELETE_WINDOW [ list cmonCacheView::closeWin $page $parent ]
        bind $statusw <Destroy> [ list cmonCacheView::closeWin $page $parent ]
        ;## insert list of gaps button into text box
    }    
	wm deiconify $parent
	wm title $parent "frames at $site"
    cacheFrames $page $html $parent

}


## ******************************************************** 
##
## Name: cmonCacheView::queryInit
##
## Description:
## 
##
## Parameters:
## parent widget
##
## Usage:
## 
## 
## Comments:

proc cmonCacheView::queryInit { page ifos types } {

    set i 0 
    catch { unset ::cmonCacheView::color }
    catch { unset ::cmonCacheView::hasframe }
    catch { unset ::cmonCacheView::noframe }
    set ::cmonCacheView::color(H) #568c3a
    set ::cmonCacheView::color(L) #568c3a
    set ::cmonCacheView::color(G) #568c3a
    set numtypes [ llength $types ]
    
    foreach ifo $ifos {
        if  { ! [ info exist ::cmonCacheView::color($ifo) ] } {
            set ::cmonCacheView::color($ifo) #568c3a
        }
        set ::cmonCacheView::hasframe($ifo) [ expr $i + $numtypes ]
        set ::cmonCacheView::noframe($ifo) $i
        set j $i 
        foreach type $types {
            set ::cmonCacheView::hasframe($ifo,$type) [ expr $j + 1 ]
            set ::cmonCacheView::noframe($ifo,$type) $j
            incr j 1
        }
        incr i $numtypes   
    }
    # puts " hasframe [array get ::cmonCacheView::hasframe ], noframe [array get ::cmonCacheView::noframe ]"
    set ::cmonCacheView::ymin($page) 0
    set ::cmonCacheView::ymax($page) $i 
}

## ******************************************************** 
##
## Name: cmonCacheView::FormatYtickLabel
##
## Description: 
## format Y tick label for sites
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc cmonCacheView::FormatYTicks { graph value } {

    set page cmonCacheView
    foreach ifo $::cmonCacheView::ifos($page) {
        foreach type $::cmonCacheView::ftypes($page) {
            if  {  $value == [ set ::cmonCacheView::hasframe($ifo,$type) ] } {
                return "\n$ifo-$type"
            }
        }
    }
}

## ******************************************************** 
##
## Name: cmonCacheView::updateMsg 
##
## Description: 
## update message widget to display helptext on your picks
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc cmonCacheView::updateMsg { page args } {

    set msgw $::cmonCacheView::wselectmsg($page)
    set ftypes [ set ::cmonCacheView::ftypes($page) ]
    set text "Query for frame types \""
    foreach type $ftypes {
        append text "$type "
        if  { [ info exist ::FRAME_TYPES_HLP($type) ] } {
            append text " ([ set ::FRAME_TYPES_HLP($type) ]) "
        }
    }
    append text "\" and sites \""
    
    set ifos  [ set ::cmonCacheView::ifos($page) ]
    foreach ifo $ifos {
        append text "$ifo "
        if  { [ info exist ::FRAME_SITES_HLP($ifo) ] } {
            append text " ([ set ::FRAME_SITES_HLP($ifo) ]) "
        }
    }
    append text "\""
    $msgw configure -text $text

} 

## ******************************************************** 
##
## Name: cmonCacheView::gapDetails 
##
## Description: 
## report on exact gaps 
##
##
## Parameters:
## 
## Usage:
##
## Comments:
## does not scroll 

proc cmonCacheView::gapDetails { page  } {
    
    set parent $::cmonCacheView::wGaps
    set ldassite [ string trim $::cmonClient::var(site) ]
    
    if  { ! $::cmonCacheView::newData($page) } {
        wm deiconify $parent
        raise $parent
        return
    }
    set fmtstr "%-8s%-15s%-35s%-35s%10s\n"
    
    ;## define max chars_per_line with padding
    set chars_per_line 132
    set text [ list ]
    appendStatus $::cmonCacheView::statusw($page) "Listing Gaps may take a little while !" 0 blue 0
    
    if  { [ catch { 
        if  { ! [ winfo exist $parent ] } {	
	        toplevel $parent
            wm protocol $parent WM_DELETE_WINDOW  "wm withdraw $parent"
            
            set fr $parent
            set textw [ createTextWin $fr 20 both yes ]
            set factions [ frame $parent.factions -relief raised -borderwidth 2 ]  
            set name cmonCacheView
		    set but1 [ Button $factions.find -text Find \
			-command "cmonCommon::findDialog $name $page $parent $textw" ]
		    set but3 [ Button $factions.print -text Print \
			-command [ list cmonCommon::printText $name $page $parent 132 landscape 10  ]  ]
		    set but4  [ Button $factions.save -text Save \
			-command "cmonCommon::saveFileText $name $page $parent" ]
             set but2 [ Button $factions.close -text Close \
			-command "wm withdraw $parent" ]

		    pack $but1 $but3 $but4 $but2 \
			-side left -padx 2 -pady 2
            
		    pack $factions -side bottom
            set ::${parent}(textw) $textw  
        }
        set starttime [ gps2utc $::cmonCacheView::xmin($page) 0 ]
        set endtime   [ gps2utc $::cmonCacheView::xmax($page) 0 ]
     
        lappend text "$ldassite Frame Gaps from $starttime ($::cmonCacheView::xmin($page)) to $endtime ($::cmonCacheView::xmax($page))\n" \
            [ list black fixed ]
        lappend text "[ expr [ llength  $::cmonCacheView::gaplist($page) ]/5 ] gaps found\n\n" [ list brown bold ]
        lappend text "[ format $fmtstr Site Type "Start Time" "End Time" "Delta secs" ]" [ list black courier ] 
        
        foreach { site type startgps endgps interval } $::cmonCacheView::gaplist($page) {
                set startutc [ gps2utc $startgps 0 ]
                set endutc [ gps2utc $endgps 0 ]
                lappend text "[ format $fmtstr \
                    $site $type "$startutc ($startgps)" "$endutc ($endgps)" $interval ]" [ list brown courier ]
        }
        set textw [ set ::${parent}(textw) ]
        updateTextWin $textw $text
        $textw configure -width 150
	    wm title $parent "Gaps in frames@$ldassite"            
        wm deiconify $parent
        raise $parent
        set ::cmonCacheView::newData($page) 0
    } err ] } {
        appendStatus $::cmonCacheView::statusw($page) "[myName] $err"
    
    }
}


## ******************************************************** 
##
## Name: cmonCacheView::closeWin 
##
## Description: 
## report on exact gaps 
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc cmonCacheView::closeWin { page parent } {

    catch { cmonCommon::closeLogs $::cmonCacheView::wGaps }
    destroy $parent

}

## ******************************************************** 
##
## Name: cmonCacheView::accentGaps 
##
## Description: 
## draw extra lines on gaps 
##
##
## Parameters:
## 
## Usage:
##
## Comments:

proc cmonCacheView::accentGaps {} {

    uplevel {
        foreach { ifo type startgps endgps interval } $::cmonCacheView::gaplist($page) {
            foreach { gaptime } [ list $startgps $endgps ] {
                set coords [ list $gaptime $::cmonCacheView::noframe($ifo,$type) $gaptime $::cmonCacheView::hasframe($ifo,$type) ]
                $graph marker create line -name markerGaps${ifo}_${type} -coords $coords -fill red \
		        -under false -linewidth 1  -element ${ifo}_${type} -dashes "" -mapx GPS
            }
        }        
    }
}

