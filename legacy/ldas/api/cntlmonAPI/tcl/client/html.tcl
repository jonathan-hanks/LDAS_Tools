## ******************************************************** 
##
## Name: html.tcl
##
## Description:
## HTML rendering functions -- provide all facilties for
## rendering HTML into a text widget in browser-like
## fashion.  Somewhat buggy...
##
## Usage:
##        create a text widget (say, .t)
##  and then do:
##            html::callback .t $html"
##
##   Where $html is html text, a local file, or a URL!
##
##   To return a "used" text widget to it's original state:
##            html::reset .t
##
## Comments:
##       1. the "back" stack doesn't exist yet.
##       2. remote inline .gif's are retrieved but not
##          rendered.
##       3. it still uses the html 1.0 lib :-(
##       4. it needs to have it's placement algorithms
##          modernised.
##       ;#ol
##       5. If ::imagelist is given, it will build the
##          images ahead of time
## ********************************************************

;#barecode
# package require -exact http 2.3
package require http
package provide ldashtml 1.0

namespace eval html {
   set basesize 12
   set lastbaseurl {}
   }

array set html::properties {
     b      {weight bold}
     blockquote  {style italic indent 1 Trindent rindent}
     bq     {style italic indent 1 Trindent rindent}
     cite   {style italic}
     code   {family courier}
     dfn    {style italic}     
     dir    {indent 1}
     dl     {indent 1}
     em     {style italic}
     h1     {size [ expr { $html::basesize + 12 } ] weight bold}
     h2     {size [ expr { $html::basesize + 10 } ] }          
     h3     {size [ expr { $html::basesize + 8  } ] }     
     h4     {size [ expr { $html::basesize + 6  } ] }
     h5     {size [ expr { $html::basesize + 4  } ] }
     h6     {style italic}
     i      {style italic}
     kbd    {family courier weight bold}
     menu   {indent 1}
     ol     {indent 1}
     pre    {fill 0 family courier size 12 weight bold Tnowrap nowrap}
     samp   {family courier}          
     strong {weight bold}          
     tt     {family helvetica}
     u      {Tunderline underline}
     ul     {indent 1}
     var    {family lucida style italic}
     center {Tcenter center}
     strike {Tstrike strike}
}

set html::properties(default) {
     family     helvetica
     weight     normal
     style      roman
     size       $html::basesize
     Tcenter    {}
     Tlink      {}
     Tnowrap    {}
     Tunderline {}
     list       list
     fill       1
     indent     {}
     counter    0
     adjust     0
	 color		black
}

array set html::properties [ subst [ array get html::properties ] ]

;## html tags that insert vertical white space
array set html::insmap {
   blockquote "\n\n" /blockquote "\n"
   br    "\n"
   dd    "\n"   /dd   "\n"
   dl    "\n"   /dl   "\n"
   dt    "\n"
   form  "\n"   /form "\n"
   h1    "\n\n" /h1   "\n"
   h2    "\n\n" /h2   "\n"
   h3    "\n"   /h3   "\n\n"
   h4    "\n"   /h4   "\n"
   h5    "\n"   /h5   "\n"
   h6    "\n"   /h6   "\n"
   li    "\n"
   /dir  "\n"
   /ul   "\n"
   /ol   "\n"
   /menu "\n"
   p     "\n\n"
   pre   "\n"   /pre "\n"
}

;## tags that are list elements, that support "compact" rendering
array set html::listelem {
     ol   1   
     ul   1   
     menu 1   
     dl   1   
     dir  1
}

;## table of escape characters
array set html::escmap {
     lt     <    gt     >    quot   \"   ob     \x7b cb     \x7d
     nbsp   \xa0 iexcl  \xa1 cent   \xa2 pound  \xa3 curren \xa4
     yen    \xa5 brvbar \xa6 sect   \xa7 uml    \xa8 copy   \xa9
     ordf   \xaa laquo  \xab not    \xac shy    \xad reg    \xae
     hibar  \xaf deg    \xb0 plusmn \xb1 sup2   \xb2 sup3   \xb3
     acute  \xb4 micro  \xb5 para   \xb6 middot \xb7 cedil  \xb8
     sup1   \xb9 ordm   \xba raquo  \xbb frac14 \xbc frac12 \xbd
     frac34 \xbe iquest \xbf Agrave \xc0 Aacute \xc1 Acirc  \xc2
     Atilde \xc3 Auml   \xc4 Aring  \xc5 AElig  \xc6 Ccedil \xc7
     Egrave \xc8 Eacute \xc9 Ecirc  \xca Euml   \xcb Igrave \xcc
     Iacute \xcd Icirc  \xce Iuml   \xcf ETH    \xd0 Ntilde \xd1
     Ograve \xd2 Oacute \xd3 Ocirc  \xd4 Otilde \xd5 Ouml   \xd6
     times  \xd7 Oslash \xd8 Ugrave \xd9 Uacute \xda Ucirc  \xdb
     Uuml   \xdc Yacute \xdd THORN  \xde szlig  \xdf agrave \xe0
     aacute \xe1 acirc  \xe2 atilde \xe3 auml   \xe4 aring  \xe5
     aelig  \xe6 ccedil \xe7 egrave \xe8 eacute \xe9 ecirc  \xea
     euml   \xeb igrave \xec iacute \xed icirc  \xee iuml   \xef
     eth    \xf0 ntilde \xf1 ograve \xf2 oacute \xf3 ocirc  \xf4
     otilde \xf5 ouml   \xf6 divide \xf7 oslash \xf8 ugrave \xf9
     uacute \xfa ucirc  \xfb uuml   \xfc yacute \xfd thorn  \xfe
     yuml   \xff amp    &	 #013	\n  #037    %	  
}

;## What should a hypertext link look like?
array set html::events {
     Enter           {-borderwidth 2 -relief groove }
     Leave           {-borderwidth 2 -relief flat   }
     1               {-borderwidth 2 -relief sunken }
     ButtonRelease-1 {-borderwidth 2 -relief raised }
}
   
;#end

## ******************************************************** 
##
## Name: html::init 
##
## Description:
## Initialises the window and stack state for the html
## enhanced text widget.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::init {win} {
     upvar #0 html::$win var
     html::initState $win
     $win tag configure underline -underline 1
     $win tag configure center    -justify center
     $win tag configure nowrap    -wrap none
     $win tag configure rindent   -rmargin $var(S_tab)c
     $win tag configure strike    -overstrike 1
     $win tag configure mark      -foreground red ;# list markers
     $win tag configure list      -spacing1 3p   -spacing3 3p
     $win tag configure compact   -spacing1 0p
     $win tag configure link      -borderwidth 2 -foreground blue

     html::indent $win $var(S_tab)
     
     $win configure -wrap word

     # configure the text insertion point
     $win mark set $var(S_insert) 1.0

     # for horizontal rules
     $win tag configure thin -font { default 2 normal }
     $win tag configure hr   -relief sunken \
                             -borderwidth 2 \
                             -wrap none \
                             -tabs [winfo width $win]
     bind $win <Configure> {
          %W tag configure hr -tabs %w
          %W tag configure last -spacing3 %h
     }
     # generic link callback
     $win tag bind link <1> "html::hitlink $win %x %y"
     
     # bullets for lining up images
     $win tag configure bulletlist -tabs ".5c center 1c left" \
	    -lmargin1 0 -lmargin2 1c
     
     if { [ info exist ::imagelist ] } {
        foreach src $::imagelist {    
	        if	{ ! [ info exist ::html::gif($src) ] } {
     	        set fsrc [ html::localImg $src ]
     	        set gif [ image create photo -file $fsrc ]
		        set ::html::gif($src) $gif
	        }
        }
    }
    set var(imgId) 0
	set var(tbId) 0
        
}
## ******************************************************** 


## ******************************************************** 
##
## Name: html::indent
##
## Description:
## set the indent spacing (in cm) for lists.
## TK uses a "weird" tabbing model that causes \t to
## insert a single space if the current line position
## is past the tab setting.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::indent {win cm} {
     set tabs [ expr { $cm / 2.0 } ]
     $win configure -tabs ${tabs}c
     foreach i {1 2 3 4 5 6 7 8 9} {
        set tab [ expr { $i * $cm } ]
        $win tag configure indent$i -lmargin1 ${tab}c \
                                    -lmargin2 ${tab}c \
      -tabs "[ expr { $tab + $tabs } ]c [ expr { $tab + 2*$tabs } ]c"
     }
}
## ******************************************************** 


## ******************************************************** 
##
## Name: html::reset 
##
## Description:
## reset the state of window - get ready for the next page
## remove all but the font tags, and remove all form state
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::reset {win} {

     upvar #0 html::$win var
     regsub -all { +[^L ][^ ]*} " [$win tag names] " {} tags
     catch "$win tag delete $tags"
     eval $win mark unset [$win mark names]
     $win delete 0.0 end
     $win tag configure hr -tabs [winfo width $win]

     # configure the text insertion point
     catch { $win mark set $var(S_insert) 1.0 }

     # remove form state.  If any check/radio buttons still exists, 
     # their variables will be magically re-created, and never get
     # cleaned up.
     catch unset [info vars html::$win.form*]

     html::initState $win
     return html::$win
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::initState
##
## Description:
## initialize the window's state array
##
## Parameters:
## (parameters beginning with S_ are NOT reset)
##
##  adjust_size:  global font size adjuster
##      unknown:  character to use for unknown entities
##          tab:  tab stop (in cm)
##         stop:  enabled to stop processing
##       update:  how many tags between update calls
##         tags:  number of tags processed so far
##      symbols:  Symbols to use on un-ordered lists
##
## Usage:
##
## Comments:
##

proc html::initState {win} {
     upvar #0 html::$win var
     array set tmp [array get var S_*]
     catch {unset var}
     array set var {
	 	  linecount		0
          stop          0
          tags          0
          fill          0
          list          list
          S_adjust_size 0
          S_tab         1.0
          S_unknown     \xb7
          S_update      1000
          S_symbols     O*=+-o\xd7\xb0>:\xb7
          S_insert      Insert
     }
     array set var [array get tmp]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::render 
##
## Description:
## Manages the display of HTML.  This proc is called for
## EVERY HTML tag.
##
## Parameters:
##   win:   The name of the text widget to render into
##   tag:   The html tag (in arbitrary case)
##   not:   a "/" or the empty string
##   param: The un-interpreted parameter list
##   text:  The plain text until the next html tag
##
## Usage:
##
## Comments:
## Can do with considerable optimization

proc html::render {win tag not param text} {

     upvar #0 html::$win var
     if {$var(stop)} return
     set tag [string tolower $tag]
     set text [html::preprocess $text]

     # manage compact rendering of lists
     if {[info exists html::listelem($tag)]} {
        set list "list [expr {[html::getparam $param compact] ? "compact" : "list"}]"
        } else {
        set list ""
        }

     # Allow text to be diverted to a different window (for tables)
     # this is not currently used
     if {[info exists var(divert)]} {
        set win $var(divert)
        upvar #0 html::$win var
        }

     # adjust (push or pop) tag state
     catch {html::stack $win $not "$html::properties($tag) $list"}

     # insert white space (with current font)
     # adding white space can get a bit tricky.  This isn't quite right
     set bad [catch {$win insert $var(S_insert) $html::insmap($not$tag) "space $var(font)"}]
     if {!$bad && [lindex $var(fill) end]} {
        set text [string trimleft $text]
        }

     # to fill or not to fill
	 ;## Fix : may get an empty list here 
     if {[lindex $var(fill) end]} {
        regsub -all {\s+} $text { } text
        }

     catch { html::tag $not$tag $win $param text } err

     # add the text with proper tags
     set tags [html::currtags $win]
	 incr var(linecount)
     $win insert $var(S_insert) $text $tags

	 # dont do update here to keep user input out of text widget
     # We need to do an update every so often to insure
     # interactive response.
	 # DO NOT USE update since this can alter the renedered text
     
     if {!([incr var(tags)] % $var(S_update))} {
	 	;## suppress user input processing
        update idletasks 
        after 100 
		;##update 
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::tag
##
## Description:
## The tag handler
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::tag { tag win param text } {

     upvar #0 html::$win var
     upvar $text data

     switch -exact -- $tag {
           default { ;## tag internal to this lib
                   $win mark gravity $var(S_insert) left
                   $win insert end "\n " last
                   $win mark gravity $var(S_insert) right
                   }
          /default { ;## tag internal to this lib
                   $win delete last.first end
                   }
             title { ;## set the help window title	
                   ;## wm title [winfo parent $win] $data
                   set data ""
                   }
                hr { ;## set a horizontal rule
                   $win insert $var(S_insert) "\n" space "\n" thin "\t" \
                                               "thin hr" "\n" thin
                   }
                ol { ;## numbered list
                   set var(count$var(level)) 0
                   }
                ul { ;## unnumbered list
                   catch {unset var(count$var(level))}
                   }
              menu { ;## unsupported, sorry
                   set var(menu) ->
                   set var(compact) 1
                   }
             /menu { ;## obvious
                   catch {unset var(menu)}
                   catch {unset var(compact)}
                   }
                dt { ;## definition term portion of dl tag
                   set level $var(level)
                   incr level -1
                   $win insert $var(S_insert) "$data" \
                   "hi [lindex $var(list) end] indent$level $var(font)"
                   set data ""
                   }
                li { ;## list item
                   set level $var(level)
                   incr level -1
                   set x [string index $var(S_symbols)+-+-+-+- $level]
                   catch {set x [incr var(count$level)]}
                   catch {set x $var(menu)}
                   $win insert $var(S_insert) \t$x\t \
                   "mark [lindex $var(list) end] indent$level $var(font)"
                   }
                 a { ;## href tag handling
                   ## for a source
                   if {[html::getparam $param href]} {
                      set var(Tref) [list L:$href]
                      html::stack $win "" "Tlink link"
                      html::linksetup $win $href
                      }
                   ## for a destination
                   if {[html::getparam $param name]} {
                      set var(Tname) [list N:$name]
                      html::stack $win "" "Tanchor anchor"
                      $win mark set N:$name "$var(S_insert) - 1 chars"
                      $win mark gravity N:$name left
                      if {[info exists var(goto)] && $var(goto) == $name} {
                         unset var(goto)
                         set var(going) $name
                         }
                      }
                   }
                /a {
                   if {[info exists var(Tref)]} {
                      unset var(Tref)
                      html::stack $win / "Tlink link"
                      }

                   # goto this link, then invoke the call-back.
                   if {[info exists var(going)]} {
                      $win yview N:$var(going)
                      update idletasks  
                      after 100    
                      html::wentto $win $var(going)
                      unset var(going)
                      }

                  if {[info exists var(Tname)]} {
                     unset var(Tname)
                     html::stack $win / "Tanchor anchor"
                     }
                   }
               img { ;## image tag handling
                   # reset all tags	/a,/font in case of truncated logs
                   if {[info exists var(Tname)]} {
                     unset var(Tname)
                     html::stack $win / "Tanchor anchor"
                   }
                   if {[info exists var(Tref)]} {
                      unset var(Tref)
                      html::stack $win / "Tlink link"
                   }
                   ;## get alternate text
                   set alt image
                   html::getparam $param alt
                   set alt [html::preprocess $alt]
                   set item $win.$var(tags)				   
                   set src ""
                   html::getparam $param src
                   html::setimg $win $src $var(S_insert) $alt
                   }
			  font { ;## font 
			  	   set color ""
			       if   { [ html::getparam $param color ] } {
				        set var(textcolor) $color
				        set var(textlen) [ string length $data ]
                   }
				   if   { [ html::getparam $param size ] } {
				        set var(S_adjust_size) $size
                        set var(textlen) [ string length $data ]
				   }
                   }
			 /font { 	
			 	    html::setcolor $win "font" 		 		
			 		set var(textcolor) black
					unset var(textlen)
				   }
             table { ;## start of tables, accumulate entries
			 		incr var(tbId) 1
					set tbId [ set var(tbId) ]
					#puts "tag=$tag, tbId=$tbId"
                    set var(table#${tbId}) {}
                    set var(tblcols#${tbId}) 0
                   }
            /table { ;## end of table, insert entries 
                    html::addTable $win $var(S_insert)
                    unset var(table#${tbId})
                    for { set i 0 } { $i < $var(tblcols#${tbId}) } { incr i 1 } {
                        set entry col#${tbId}${i}sz
                        unset var($entry) 
                     }
                    unset var(tblcols#${tbId})
					incr var(tbId) -1 
                   }
                tr {
					 set tbId [ set var(tbId) ]
                     set var(tblcols#${tbId}) 0 
                   }
               /tr { set tbId [ set var(tbId) ]
			   		 for { set i 0 } { $i < $var(tblcols#${tbId}) } { incr i 1 } {
                        set entry col#${tbId}${i}sz
						#puts "tr,entry=$entry"
                        set var($entry) 0                       
                     }              
                   }
                    
                th -
                td { ;## tables
					set tbId [ set var(tbId) ]
                    lappend var(table#${tbId}) $data
					#puts "td, [ set var(table#${tbId}) ]"  
                    set data ""
                    incr var(tblcols#${tbId}) 1
                    html::getparam $param bgcolor
                    if  { [ string length $bgcolor ] } {
                        set index [ expr [ llength $var(table#${tbId}) ] - 1 ]
                        set rc ""
                        catch { set rc [ $win tag cget cc$bgcolor -background ] } err
                        if  {  [ string length $rc ] } {
                            $win tag configure cc$bgcolor -background $bgcolor
                        } 
                        set var(cell$index) cc$bgcolor
                    }
                   }                  
           }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::goto
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
## The application should call here with the fragment name
## to cause the display to go to this spot.
## If the target exists, go there (and do the callback),
## otherwise schedule the goto to happen when we see the reference.

proc html::goto {win where {callback html::wentto}} {

     upvar #0 html::$win var
     if {[regexp N:$where [$win mark names]]} {
        $win see N:$where
        update idletasks
        after 100
        eval $callback $win [list $where]
        return 1
        } else {
        set var(goto) $where
        return 0
        }
}
## ********************************************************

## ******************************************************** 
##
## Name: html::wentto
##
## Description:
## Highlights a successful move to a local link
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::wentto {win where {count 0} {color orange}} {

     upvar #0 html::$win var
     if {$count > 5} return
     catch {$win tag configure N:$where -foreground $color}
     update idletasks
     after 100
     after 200 [list html::wentto $win $where [incr count] \
               [expr {$color=="orange" ? "" : "orange"}]]
}

## ******************************************************** 

## ******************************************************** 
##
## Name: html::setimg 
##
## Description:
## Place gifs in the text widget
##
## Parameters:
##
## Usage:
##
## Comments:
## global gifs are created at init

proc html::setimg { { win "" } { src "" } { pos "" } { alt ""  } } {

    if  { [ catch {
		regsub -all {\n} $src {} src
		if	{ [ string length $src ] && [ info exist ::html::gif($src) ] } {
        	set gif $::html::gif($src)
	    	$win insert $pos \t bulletlist
	    	set name [ $win image create $pos -padx 1 -pady 1 -image $gif ]
	    	$win insert $pos \t bulletlist
	    	html::imageTagButton $win $name "$name +1 char" $alt
		}
    } err ] } {
        puts "setimg error: $err , called by [ info level -1 ]"
    }
	;## dont pack or image will be overlapped
	;##pack propagate [winfo parent $win] 1
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::localImg
##
## Description:
## Handle log file balls differently from other images.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::localImg { src } {
     ;## temp file name
     set fsrc html.tcl.tempfile
      
     if { [ catch {
        ;## special handler for our log file balls
        if { [ regexp {ball_(red|green|yellow|orange|blue|purple)} $src ] ||
             [ regexp {mail.gif|telephone.gif} $src ] } {
           set filename [ file join $::GIFDIR $src ]
           if { [ file readable $filename ] } {
              set fsrc $filename
           }
        } else {
           set imgdata [ html::geturl $src ]
           set fid [ open $fsrc w ]
           puts $fid $imgdata
           close $fid
           unset $imgdata
        }
     } err ] } {
        catch { close $fid }
        catch { unset imgdata }
        return -code error "html::localImg: $err"
     }
     return $fsrc
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::imageTagButton 
##
## Description:
## place a tag with image to create label when cursor is over it
##
## Parameters:
##  win:	text widget
##  start:	text widget insertion start position for tag
##	end:	text widget insertion end position for tag
## Usage:
##
## Comments:
##

proc html::imageTagButton { win start end alt } {

	upvar #0 html::$win var
	
    incr var(imgId) 1
	set tag img[ set var(imgId)]
	$win tag configure $tag -relief flat -borderwidth 2
	$win tag configure $tag -background [$win cget -bg]
	$win tag configure $tag -foreground [$win cget -fg]

	# Bind the command to the tag
	$win tag add $tag $start $end
	# use another tag to remember the cursor
	$win tag bind $tag <Enter> \
		[ list html::changeCursor %W $tag $start $end top_left_arrow $alt %x %y]
	$win tag bind $tag <Leave> [ list html::restoreCursor %W ]
}	
## ******************************************************** 

## ******************************************************** 
##
## Name: html::changeCursor 
##
## Description:
## change cursor when mouse moves over the tag button
##
## Parameters:
##  win:	text widget
##  start:	text widget insertion start position for tag
##	end:	text widget insertion end position for tag
##  text:	text to be displayed on label
##  x:		x coordinate under mouse
##  y:		y coordinate under mouse 
##
## Usage:
##
## Comments:
##

proc html::changeCursor { win tag start end cursor text x y } {

	upvar #0 html::$win var
	
	$win tag add cursor=[$win cget -cursor] $start $end
	$win config -cursor $cursor
    
	;## get frame parent of textsw 
	set lblframe [ winfo parent $win ]    
	set label $lblframe.lbl
    if  { [ winfo exist $label ] } {
        destroy $label
        update idletasks
        after 100
    }
    if  { ! [ string length $text ] } {
        set text "No alt"
    }
    label $label -text $text -relief ridge -bg "light yellow" -fg black \
		-font lucida       
	;## Fix: adjusting $y gives flickering
	;## alt text has an escape sequence between times
    place $label -in $lblframe -x [ expr $x + 100 ] -y $y -anchor sw
    update idletasks
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::restoreCursor 
##
## Description:
## restore cursor when mouse leaves the tag button
##
## Parameters:
##  win:	text widget
##
## Usage:
##
## Comments:
##

proc html::restoreCursor { win } {
	regexp { cursor=([^ ]*)} [ $win tag names ] x cursor
	$win config -cursor $cursor
    set lblframe [ winfo parent $win ]    
	set label $lblframe.lbl
    destroy $label
    update idletasks
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::linksetup
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::linksetup {win href} {
     ;## escape %'s against bind's interpreter
     regsub -all {%} $href {%%} href2
     foreach i [array names html::events] {
          eval {$win tag bind  L:$href <$i>} \
               \{$win tag configure \{L:$href2\} $html::events($i)\}
          }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::hitlink 
##
## Description:
## Generic link-hit callback.
## For button hits and hypertext links.
##
## Parameters:
##   win:   The name of the text widget to render into
##   e.g. .logs$name:$page e.g. .logscmonLogs:Log-Filter-1
##        .logscmonMPIjobscmonMPIjobs
##   x,y:   The cursor position at the "click"
##
## Usage:
##
## Comments:
##!

proc html::hitlink {win x y} {
    
    set origCursor [ $win cget -cursor ]
    if  { [ catch {
     set tags [$win tag names @$x,$y]
     set link [lindex $tags [lsearch -glob $tags L:*]]
     regsub L: $link {} link    
     
     ;## use with cmonClient log filter and mpi log filter
    
     if { [ regexp {log-([^:]+):([^\:\.]+)} $win -> name page ] } {
        lappend ::${win}(linkStack) $link
        set origstate [ set ::${name}::state($page) ]
        catch { eval ::${name}::state3 $page }
        $win config -cursor watch
     }
     html::callback $win $link
     
     } err ] } {
        if  { [ info exist page ] } {
            appendStatus [ set ::${name}::statusw($page) ] $err 
            catch { cmonCommon::backLink $name $page $win }
            catch { eval ::${name}::state${origstate} $page }
        }
    }
    catch { eval ::${name}::state$origstate $page }
    $win config -cursor $origCursor
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::callback 
##
## Description:
##
## Parameters:
##    win:  The name of the text widget to render into
##   href:  The HREF link for this <a> tag.
##
## Usage:
##
## Comments:
##

proc html::callback {win href} {
     set text {}
  
     ;## some html text
     if { [ regexp {^[\s\n\t]*<} $href ] } {
        html::saveHtml $href $win
        $win configure -state normal
        html::reset $win
        html::init $win
        html::parse $href "html::render $win"
        $win configure -state disabled
        return {}
     }
	      
     ;## a local tag in the current document
     if { [ regexp {^#[^ ].+} $href ] } {     
        regsub {^#} $href {} href
        html::goto $win $href
        return {}
     }
     
     ;## a local file:
     regsub {file:} $href {} href 
     set filename [ lindex [ glob -nocomplain $href ${href}.help */$href */${href}.help */*/$href */*/${href}.help ] 0 ]

     if { [ file readable $filename ] } {
        set fid [ open $filename r ]
        set text [ read $fid ]
        close $fid
        $win configure -state normal
        html::saveHtml $text $win
        html::reset $win
        html::init $win
        if { [ regexp {^GIF8[79]a} $text ] } {
           html::setimg $win $href
        } else {
           html::parse $text "html::render $win"
        }   
        $win configure -state disabled
        return {}
     }

     ;## got here?  It must be a real URL!
     if { [ regexp {^http:} $href ] || [ llength $html::lastbaseurl ] } {
        set text [ html::geturl $href ]
        html::saveHtml $text $win
        $win configure -state normal
        html::reset $win
        html::init $win
        if { [ regexp {^GIF8[79]a} $text ] } {
           html::setimg $win $href
        } else {
           html::parse $text "html::render $win"
        }   
        $win configure -state disabled
        return {}
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::geturl
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::geturl { { url "" } } {
     regexp {^\{(.*)\}$} $url -> url
     regexp {^\"(.*)\"$} $url -> url
     regexp {^([^,]+)$} $url -> url
     set url [ string trim $url ]
     
     ;## reject URL's with embedded spaces
     set url_rx {^(file|http|mailto|ftp|port):/*([^ :]+):?([0-9]+)?$}
     
     if { [ regexp $url_rx $url -> protocol target port ] } {

        switch -exact -- $protocol {
           http {
                set tempfile html.tcl.tempfile
                set temp [ open $tempfile w ]           
                set token [ http::geturl $url -channel $temp ]
                close $temp
                set fid [ open $tempfile r ]
                set text [ read $fid ]
                close $fid
                file delete -- $tempfile
                set html::lastbaseurl [ file dirname $target ]
                if { [ regexp {\.} $html::lastbaseurl ] } {
                   set html::lastbaseurl $target
                }   
                return $text
                }
            file { 
                set fid [ open "/$target" r ]
                set text [ read -nonewline $fid ]
                close $fid
                return $text
                }
        default {
                return -code error "html::geturl: $protocol not supported"
                }
        } ;## end of switch
     }
     if { [ llength $html::lastbaseurl ] } {
        set url $html::lastbaseurl/$url
        set tempfile html.tcl.tempfile
        set temp [ open $tempfile w ]
        set token [ http::geturl $url -channel $temp ]
        close $temp
        set fid [ open $tempfile r ]
        set text [ read $fid ]
        close $fid
        file delete -- $tempfile
        return $text
     } else {
     return -code error "html::geturl: no base for relative URL \"$url\""
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::getparam
##
## Description:
## extract a value from parameter list (this needs a re-do)
## returns "1" if the keyword is found, "0" otherwise.
##
## Parameters:
##   param: A parameter list.  It should alredy have been
##          processed to remove any entity references.
##     key: The parameter name
##     val: The variable to put the value into (use key as default)
##
## Usage:
##
## Comments:
## This can EASILY be improved!!

proc html::getparam { param key { val "" } } {
     if {$val == ""} {
        upvar $key result
        } else {
        upvar $val result
        }
     set ws "    \n\r"
    ;## look for params.  Either (') or (") are valid delimiters
    if {
       [regsub -nocase [format {.*%s[%s]*=[%s]*"([^"]*).*} $key $ws $ws] $param {\1} value] ||
       [regsub -nocase [format {.*%s[%s]*=[%s]*'([^']*).*} $key $ws $ws] $param {\1} value] ||
       [regsub -nocase [format {.*%s[%s]*=[%s]*([^%s]+).*} $key $ws $ws $ws] $param {\1} value] } {
       set result $value
       return 1
       }

     ;## now look for valueless names
     ;## I should strip out name=value pairs,
     ;## so we don't end up with "name"
     ;##inside the "value" part of some other key word - some day.
     set bad \[^a-zA-Z\]+
     if {[regexp -nocase  "$bad$key$bad" -$param-]} {
        return 1
        } else {
        return 0
        }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::stack
##
## Description:
## Push or pop tags to/from stack.
## Each orthogonal text property has its own stack,
## stored as a list.
## The current (most recent) tag is the last item
## on the list.
## Push is {} for pushing and {/} for popping
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::stack {win push list} {

     upvar #0 html::$win var
     array set tags $list
     if {$push == ""} {
        foreach tag [array names tags] {
             lappend var($tag) $tags($tag)
             }
        } else {
        foreach tag [array names tags] {
             #set cnt [regsub { *[^ ]+$} $var($tag) {} var($tag)]
             set var($tag) [lreplace $var($tag) end end]
             }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::currtags
##
## Description:
## extract set of current text tags.
## tags starting with T map directly to text tags,
## all others are handled specially.
## There is an application callback, html::setfont
## to allow the application to do font error handling
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::currtags {win} {

     upvar #0 html::$win var
     set font font
     foreach i {family size weight style} {
          set $i [lindex $var($i) end]
          if  { [ string match family $i ] && ! [ string length [ set $i ] ] } {
            set $i helvetica
          }
          append font [set $i]
          }
     set xfont [html::xfont $family $size $weight $style $var(S_adjust_size)]
     html::setfont $win $font $xfont
     set indent [llength $var(indent)]
     incr indent -1
     lappend tags $font indent$indent
     foreach tag [array names var T*] {
          lappend tags [lindex $var($tag) end]  ;# test
          }
     set var(font) $font
     set var(xfont) [$win tag cget $font -font]
     set var(level) $indent
     return $tags
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::setfont 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::setfont {win tag font} {
	upvar #0 ::html::$win var
     catch {$win tag configure $tag -font $font} msg
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::setcolor 
##
## Description:
##	turns on color for text with font attribute specifying color
## Parameters:
##
## Usage:
##  html::setcolor $win $htag
##
## Comments:
## 

proc html::setcolor  { { win "" } { htag "font" } } {
	upvar #0 html::$win var
	set color $var(textcolor)
	set textlen [ expr $var(textlen) + 2 ]
	if	{ [ string compare $color "" ] } {
		set tag  "${htag}_${color}"
		if	{ [ catch {
			$win tag cget $tag -foreground 
			} err ] } {
			$win tag configure $tag -foreground $color 
		}
		$win tag add $tag "insert - $textlen chars" end
	}	
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::xfont
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::xfont { { family lucida } { size 12 } { weight normal } \
                { style roman } {adjust_size 0} } {
     catch {incr size $adjust_size}

     set font "${family}${size}${weight}$style"
 
     if { [ catch { font configure $font } ] } {
        catch {
               font create $font -family $family \
                                 -size   $size   \
                                 -weight $weight \
                                 -slant  $style;
              }
     }
    return $font
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::parse 
##
## Description:
## Render HTML into Tcl text widget syntax
## Parameters:
##    html: A string containing an html document
##     cmd: A command to run for each html tag found
##   start: The name of the default html start/stop tags
##
## Usage:
##
## Comments:
##

proc html::parse {html {cmd html::test} {start default}} {

     regsub -all \{ $html {\&ob;} html
     regsub -all \} $html {\&cb;} html
     ;## regexp for a tag
     set exp {<(/?)([^>\s]+)\s*([^>]*)>}
     set sub "\}\n$cmd {\\2} {\\1} {\\3} \{"
     regsub -all $exp $html $sub html
     eval "$cmd {$start}   {} {} \{ $html \}"
     eval "$cmd {$start} / {} {}"
}

proc html::test { command tag slash text_after_tag } {
     puts "==> $command $tag $slash $text_after_tag"
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::preprocess
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc html::preprocess { text } {
     ;## maybe we are rendering something with embedded
     ;## tcl code -- declaw it!
     regsub -all {\$} $text {\\$} text
     ;## taken care by -nocommands 
     #regsub -all {\[} $text {\[} text
     #regsub -all {\]} $text {\]} text
     ;## replace html escape sequences with literals
     regsub -all -nocase {&([0-9a-z#]*);} $text {$html::escmap(\1)} text 
     if { [ catch {
        set text [ subst -nocommands $text ]
     } err ] } {
        ;## \$HOME seemed to cause some problems in mpi Log
        regsub -all {\$HOME} $text {\\\\\$HOME} text
        #regsub -all {\$} $text {\\\\\$} text
        if  { [ catch {
            set text [ subst -nocommands $text ]
        } err ] } {
            ;## last resort is dont do variable substitution
            set text [ subst -nocommands -novariables $text ]
        }
     }
     return $text
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::addTable
##
## Description:
## keep track of the max len of each column in table
##
## Parameters:
##  win:	text widget
##  data:   current text 
##
## Usage:
##
## Comments:
## 

proc html::addTable { win pos } {

	upvar #0 html::$win var
    set entries {}
    set row 0
    
	set tbId [ set var(tbId) ]
	#puts "addTable, tbId=$tbId"
    set numCols $var(tblcols#${tbId})
    for { set index 0 } { $index < $numCols } { incr index 1 } {
        lappend entries col$index
    }
    set index 0
    foreach $entries $var(table#${tbId}) {
        foreach entry $entries {
			#puts "entry=$entry"
            set data [ lindex $var(table#${tbId}) $index ]
			#puts "data=$data"
            set len [ string length $data ]
			#puts "len=$len"
            if  { $len > $var(${entry}sz)  } {
                set var(${entry}sz) $len
            }
            incr index 1            
        }    
    }

    ;## make each table entry same size
    set index 0 
    foreach $entries $var(table#${tbId}) {
        $win insert $pos "\n"
        foreach entry $entries {
            set text [ lindex $var(table#${tbId}) $index ]
            set text "| [ format "%-*s" $var(${entry}sz) $text ] "
            set textlen [ expr [ string length $text] -  1 ]
            $win insert $pos $text 
            if  { [ info exist var(cell$index) ] } {
                $win tag add $var(cell$index) "$pos - $textlen chars" $pos
                unset var(cell$index)
            }            
            incr index 1
        }
        $win insert $pos "|"
    }
    $win insert $pos "\n"
}
## ******************************************************** 

## ******************************************************** 
##
## Name: html::saveHtml
##
## Description:
## save the current html in tmp directory if in cmonClient
##
## Parameters:
##  html:	html text
##  win :   text window
##
## Usage:
##
## Comments:
## 

proc html::saveHtml { html win } {

    if  { [ regexp {.log-([^\.]+)} $win -> pageetc ] } {
        set site [ string trim $::cmonClient::var(site) ]
        set fd [ open $::TMPDIR/${pageetc}_${site}.html w ]
	    puts $fd $html
	    close $fd
    } 
}
