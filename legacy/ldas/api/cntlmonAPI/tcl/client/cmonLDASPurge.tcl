## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) Control and Monitor client Tcl Script.
##
## This script handles the startup/shutdown of LDAS APIs
## and control of tape controller at Hanford.
##
## repeat and frequency are here for conformance to
## standard communication with server and are not actually used.
## ******************************************************** 

package provide cmonLDASPurge 1.0

namespace eval cmonLDASPurge {
	set pages { cmonLDASPurge }
    set fixedTimeIndex 8
	set timeModes { "LOCAL  " "GMT  " "GPS" }
	set timeoptions { "last 30 minutes" "last hour" "last 6 hours" \
	"last 12 hours" "last day" "last 2 days" "last 3 days" "last week" \
    	"specify times" }
    set jobDays 0
    set jobHours 0
    set jobMins 0
    set logDays 0
    set	logHours 0
    set logMins 0
    set cleanupDays 0
    set	cleanupHours 0
    set cleanupMins 0
    set serverVars [ list ::JOBS_PURGE_BEFORE ::LOGS_PURGE_BEFORE ::CLEAR_OUTPUT_PERIOD \
    ::NEXT_CLEANUP_SECS ]
    set noSupportMsg "\$::cmonClient::var(site) server does not support this function but \
    	you can modify cntlmon resources ::JOB_PURGE_BEFORE, ::LOGS_PURGE_BEFORE and \
       	::CLEAR_OUTPUT_PERIOD"
    set chgsToSave 0
}

## ******************************************************** 
##
## Name: cmonLDASPurge::create
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASPurge::create { notebook } {

    set page cmonLDASPurge
    set ::cmonLDASPurge::prevsite($page) none
    $notebook insert end cmonLDASPurge -text "LDAS Cleanup" \
        -createcmd "cmonLDASPurge::createPages $notebook" \
        -raisecmd [ list cmonLDASPurge::pageRaise cmonLDASPurge ]
}

## ******************************************************** 
##
## Name: cmonLDASPurge::createPages
##
## Description:
## creates Load page
##
## Parameters:
## parent widget notebook 
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.
## must do notebook insert into main page cmonLDAS

proc cmonLDASPurge::createPages { notebook } {

	    set frame [ $notebook getframe cmonLDASPurge ]

		set topf  [ frame $frame.topf ]	
		set pane2 [ frame $frame.pane2 ]
        
	    set page cmonLDASPurge
	    specific $topf $page 
	    set ::cmonLDASPurge::topfw($page) $topf
        pack $topf -side top -fill x -pady 2
		
		set titf2 [TitleFrame $pane2.titf2 -text "Command Status" -font $::ITALICFONT]
		;## create a status to display errors and status
		set statusw [ ::createStatus  [ $titf2 getframe ] ]
		array set ::cmonLDASPurge::statusw [ list $page $statusw ]
		pack $titf2 -pady 1 -padx 1 -fill both -expand yes
		pack $pane2 -fill both -expand yes
        
		pack $topf -side top -fill x -expand 1

        array set ::cmonLDASPurge::state [ list $page 0 ]
        set ::cmonLDASPurge::prevsite($page) $::cmonClient::var(site)

}

## ******************************************************** 
##
## Name: cmonLDASPurge::specific 
##
## Description:
## creates Load page specific portion for LDAS APIs
##
## Parameters:
## parent widget
## page name
##
## Usage:
##  
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASPurge::specific { parent page } {

    set labf1 [LabelFrame $parent.labf1 -text \
		"Purge Job Output and Logs: " \
		-side top -anchor w -relief sunken -borderwidth 4]
    set subf  [$labf1 getframe]
	
	set name [ namespace tail [ namespace current ] ]	
	;## jobs purge
       
	set fstart [ frame $subf.fstart ]
    set label1 [ label $fstart.joblbl -text "Purge Jobs older than: " -width 10 -justify right -width 20 ]
    set durationw1 [ cmonCommon::durationWidget $fstart \
    	::cmonLDASPurge::jobDays ::cmonLDASPurge::jobHours ::cmonLDASPurge::jobMins ]
    pack $label1 $durationw1 -side left -anchor w
    set s_ent   [LabelEntry $fstart.logstart -label "Before: " -labelwidth 10 -labelanchor e \
                   -textvariable ::cmonLDASPurge::jobtime -width 22 -editable 0 \
                   -helptext "Reflects the time before now"]
     
    set timemenu [ eval tk_optionMenu $fstart.smode ::cmonLDASPurge:stimeMode \
        $::cmonLDASPurge::timeModes ]
        
	set numModes [ llength $::cmonLDASPurge::timeModes ]	
	for { set i 0 } { $i < $numModes } { incr i 1 } {
		$timemenu entryconfigure $i -command "cmonCommon::to[ lindex $::cmonLDASPurge::timeModes $i ] $name $page jobtime ::cmonLDASPurge"
	}	
    pack $s_ent $fstart.smode -side left 
 
    set fend [ frame $subf.fend ]
    set label2 [ label $fend.loglbl -text "Purge Logs older than: " -width 20 -justify right -width 20 ]
    set durationw2 [ cmonCommon::durationWidget $fend \
    	::cmonLDASPurge::logDays ::cmonLDASPurge::logHours ::cmonLDASPurge::logMins ]
    pack $label2 $durationw2 -side left -anchor w
	set e_ent   [LabelEntry $fend.logend -label   "Before: " -labelwidth 10 -labelanchor e \
                   -textvariable ::cmonLDASPurge::logtime -width 22 -editable 0 \
                   -helptext "Reflects the time from now" ]
				   
    set timemenu [ eval tk_optionMenu $fend.emode ::cmonLDASPurge::etimeMode $::cmonLDASPurge::timeModes ]
		
	for { set i 0 } { $i < $numModes } { incr i 1 } {
		$timemenu entryconfigure $i -command "cmonCommon::to[ lindex $::cmonLDASPurge::timeModes $i ] $name $page logtime ::cmonLDASPurge"
	}	
    pack $e_ent $fend.emode -side left 
    
	set ::cmonLDASPurge::starttime_prevmode [ lindex $::cmonCommon::timeModes 0 ]
	set ::cmonLDASPurge::endtime_prevmode [ lindex $::cmonCommon::timeModes 0 ]
	# cmonCommon::setLastTime $name $page
    
    ;## frequency of cleanup 
    set fcleanup [ frame $subf.freq ]
    set label3 [ label $fcleanup.freqlbl -text "Cleanup every: " -width 20 -justify right -width 20 ]
    set durationw3 [ cmonCommon::durationWidget $fcleanup \
    	::cmonLDASPurge::cleanupDays ::cmonLDASPurge::cleanupHours ::cmonLDASPurge::cleanupMins ]
    pack $label3 $durationw3 -side left -anchor w
    
    set c_ent   [LabelEntry $fcleanup.nextlbl -label   " Next at: " -labelwidth 10 -labelanchor e \
                   -textvariable ::cmonLDASPurge::nextCleanup -width 22 -editable 0 \
                   -helptext "This time is scheduled by cntlmon server." ]
				   
    set timemenu [ eval tk_optionMenu $fcleanup.mode ::cmonLDASPurge::etimeMode $::cmonCommon::timeModes ]
		
	for { set i 0 } { $i < $numModes } { incr i 1 } {
		$timemenu entryconfigure $i -command "cmonCommon::to[ lindex $::cmonLDASPurge::timeModes $i ] $name $page nextCleanup ::cmonLDASPurge"
	}	
    pack $c_ent $fcleanup.mode -side left
    
   
	;## actions
    set f3 [ frame $subf.options -relief ridge ]
    set mb1 [ menubutton $f3.mb -text "Purge Options" -width 15 -menu $f3.mb.menuA \
		-relief raised -borderwidth 2 -font $::LISTFONT ]
    set m1menu [ menu $mb1.menuA -tearoff 1 ]
    set ::cmonLDASPurge::functionmenu($page) $mb1
     
    $m1menu add check -label "Apply changes" -variable ::cmonLDASPurge::applyChanges
    $m1menu add check -label "Run cleanup" -variable ::cmonLDASPurge::runCleanup 
    $m1menu add check -label "Save Changes" -variable ::cmonLDASPurge::saveChanges 

	;## add submit and review button
    
    set  but1 [ Button $f3.submit -text "SUBMIT" -width 15 -helptext "submit request to server" \
        -command  "::cmonLDASPurge::sendRequest $page" ]
    
    set  but2 [ Button $f3.vieworig -text "View Changes" -width 15 -helptext "view the original settings before changes" \
        -command  "::cmonLDASPurge::viewChanges $page" ] 
    
    set  but3 [ Button $f3.update -text "Update" -width 15 -helptext "update the values from server" \
        -command  "::cmonLDASPurge::updateRequest $page" ] 
    set ::cmonLDASPurge::submitBut($page) $but1
    set ::cmonLDASPurge::viewChgsBut($page) $but2
    set ::cmonLDASPurge::updateBut($page) $but3
    pack $mb1 $but1 $but2 $but3 -side left -padx 5 -pady 5
    
    pack $fstart $fend $fcleanup $f3 -side top -expand 1 -fill both -anchor nw
    
    pack $labf1 -side top -fill x -padx 5 -expand 1
	array set ::cmonLDASPurge::state [ list $page 0 ]
	;## trace to update the date strings when the day/hour/mins are set by user
    
	foreach tag [ list job log ] {
    	trace add variable ::cmonLDASPurge::${tag}Days { write } [ list cmonLDASPurge::updateTimeStr $tag ]
    	trace add variable ::cmonLDASPurge::${tag}Hours { write } [ list cmonLDASPurge::updateTimeStr $tag ]
    	trace add variable ::cmonLDASPurge::${tag}Mins { write } [ list cmonLDASPurge::updateTimeStr $tag ]
	}
    set ::cmonLDASPurge::jobtime_prevmode [ lindex $::cmonCommon::timeModes 0 ]
	set ::cmonLDASPurge::logtime_prevmode [ lindex $::cmonCommon::timeModes 0 ]
   	set ::cmonLDASPurge::nextCleanup_prevmode [ lindex $::cmonCommon::timeModes 0 ]
}


## ******************************************************** 
##
## Name: cmonLDASPurge::sendRequest 
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASPurge::sendRequest { page } {
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLDASPurge::statusw($page) "Please select an LDAS site" 
		return 
	}
	set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set cmdId "new"
    set freq 1
    set repeat 1
    
    set cmd [ list ]
    set rsccmd [ list ]
    
    set site $::cmonClient::site
    if	{ ![ info exist ::JOBS_PURGE_BEFORE ] } {
    	appendStatus $::cmonLDASPurge::statusw($page) "[ subst $::cmonLDASPurge::noSupportMsg ]"
		return 
	}
    if	{ $::cmonLDASPurge::applyChanges || $::cmonLDASPurge::saveChanges } {
    	foreach tag [ list job log ] {
    		set seconds [ cmonCommon::durationSecs [ set ::cmonLDASPurge::${tag}Days ] \
            	[ set ::cmonLDASPurge::${tag}Hours ] [ set ::cmonLDASPurge::${tag}Mins ] ]	
    		lappend rsccmd "set ::[ string toupper $tag]S_PURGE_BEFORE $seconds "
        }
        set seconds [ cmonCommon::durationSecs [ set ::cmonLDASPurge::cleanupDays ] \
            	[ set ::cmonLDASPurge::cleanupHours ] [ set ::cmonLDASPurge::cleanupMins ] ]	
    	lappend rsccmd "set ::CLEAR_OUTPUT_PERIOD [ expr $seconds * 1000 ] "
        set cmd [ concat $cmd $rsccmd ]
    }
    if	{ $::cmonLDASPurge::runCleanup } {
    	lappend cmd "cntlmon::cleanupLDASoutput"
  	}
    
    if	{ $::cmonLDASPurge::saveChanges } {
    	set savecmd "cntlmon::saveResource cntlmon LDAScntlmon.rsc [ list [ list "set ::JOBS_PURGE_BEFORE" \
        "set ::LOGS_PURGE_BEFORE" "set ::CLEAR_OUTPUT_PERIOD"  ] ] \
         [ list $rsccmd ]" 
        lappend cmd $savecmd 
    }   
 	
	;## this is a broadcast function 
	if	{ [ catch {
    	if	{ ! [ string length $cmd ] } {
    		error "Please select at least one purge option"
    	}
        set rc [ cmonLDASPurge::viewChanges $page 1 ]
        ;## ok = 0
        if	{ ! $rc } {
			set cmd "cmonLDASPurge::showReply $page\n$repeat\n$freq\n\
        	$client:$cmdId\ncntlmon::updateCleanup [ list $cmd ]"
			# puts "cmd=$cmd"
        	cmonLDASPurge::state1 $page 
			sendCmd $cmd  
        }            
	} err ] } {
        cmonLDASPurge::state0 $page 
		appendStatus $::cmonLDASPurge::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLDASPurge::updateRequest 
##
## Description:
## request an update of values from server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASPurge::updateRequest { page } {
	if  { ! [ string compare $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		appendStatus $::cmonLDASPurge::statusw($page) "Please select an LDAS site" 
		return 
	}
	set name [ namespace tail [ namespace current ] ]
    set client $::cmonClient::client
    set cmdId "new"
    set freq 1
    set repeat 1
      
    set site $::cmonClient::site
    
    if	{ ![ info exist ::JOBS_PURGE_BEFORE ] } {
    	appendStatus $::cmonLDASPurge::statusw($page) "[ subst $::cmonLDASPurge::noSupportMsg ]"
		return 
	}
    set cmd [ list ]
    lappend cmd "set ::JOBS_PURGE_BEFORE"
 
 	set ::cmonLDASPurge::chgsToSave 0
    
	;## this is a broadcast function 
	if	{ [ catch {
		set cmd "cmonLDASPurge::showReply $page\n$repeat\n$freq\n\
        	$client:$cmdId\ncntlmon::updateCleanup [ list $cmd ]"
			# puts "cmd=$cmd"
        cmonLDASPurge::state1 $page 
		sendCmd $cmd          
	} err ] } {
        cmonLDASPurge::state0 $page 
		appendStatus $::cmonLDASPurge::statusw($page) $err
	}
}

## ******************************************************** 
##
## Name: cmonLDASPurge::showReply
##
## Description:
## sends cmd to cntlmonAPI server
##
## Parameters:
## page name
##
## Usage:
##  call by main code to create the sub notebook.
## 
## Comments:
## this proc name is generic for all packages.

proc cmonLDASPurge::showReply { page rc clientCmd html } {

	if	{ [ catch {
    	set clientCmd [ split $clientCmd : ]
    	set client [ lindex $clientCmd 0 ]
		set cmonClient::client $client
    	set cmdId [ lindex $clientCmd 1 ]
    	set afterid [ lindex $clientCmd 2 ]

    	# puts "in showReply, page=$page,rc=$rc, client=$client, html $html"    
		switch $rc {
			0 { cmonLDASPurge::state0 $page 
                if	{ [ regexp {(set .+)} $html -> cmd ] } {
                	eval $cmd
                    set msg Updated
                    if	{ $::cmonLDASPurge::chgsToSave } {
        				if	{ $::cmonLDASPurge::saveChanges } {
        					set msg "Changes saved to file."
                            set ::cmonLDASPurge::chgsToSave 0
            			} else {
            				set msg "Changes made in memory only."
            			}
        			} 
                    appendStatus $::cmonLDASPurge::statusw($page) $msg 0 blue
                } else {
                	appendStatus $::cmonLDASPurge::statusw($page) $msg 0 blue   
                } 
	  		}
			3 {
        		appendStatus $::cmonLDASPurge::statusw($page) $html 0  
	 	 	}
         }
	}	err ] } {
		appendStatus $::cmonLDASPurge::statusw($page) $err 
	}
    cmonLDASPurge::state0 $page 
}

##******************************************************** 
##
## Name: cmonLDASPurge::state0  
##
## Description:
## initial state ready for request
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonLDASPurge::state0 { page } {
	
    if	{ $::cmonClient::state } {
    	if	{ ! [ info exist ::JOBS_PURGE_BEFORE ] } {
            set site $::cmonClient::var(site)
            [ set ::cmonLDASPurge::functionmenu($page) ] configure -state disabled
            [ set ::cmonLDASPurge::submitBut($page) ] configure -state disabled
    		[ set ::cmonLDASPurge::viewChgsBut($page) ] configure -state disabled
            
            appendStatus $::cmonLDASPurge::statusw($page) "[ subst $::cmonLDASPurge::noSupportMsg ]"
     	} else {
        	[ set ::cmonLDASPurge::functionmenu($page) ] configure -state normal
        	[ set ::cmonLDASPurge::submitBut($page) ] configure -state normal
    		[ set ::cmonLDASPurge::viewChgsBut($page) ] configure -state normal
        }
    } else {
        # updateWidgets $::cmonLDASPurge::topfw($page) normal
        [ set ::cmonLDASPurge::functionmenu($page) ] configure -state normal
        [ set ::cmonLDASPurge::submitBut($page) ] configure -state normal
    	[ set ::cmonLDASPurge::viewChgsBut($page) ] configure -state normal
    }
	array set ::cmonLDASPurge::state [ list $page 0 ]
	updateWidgets $::cmonLDASPurge::topfw($page) normal
}

##******************************************************** 
##
## Name: cmonLDASPurge::state1  
##
## Description:
## sending request and pending reply
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## enable go button and repeats  

proc cmonLDASPurge::state1 { page } {
	updateWidgets $::cmonLDASPurge::topfw($page) disabled
	array set ::cmonLDASPurge::state [ list $page 1 ]
}

## ******************************************************** 
##
## Name: cmonLDASPurge::reset  
##
## Description:
## reset on site disconnect if this is current page
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASPurge::reset { page } {
	
	cmonLDASPurge::state0 $page    
}

## ******************************************************** 
##
## Name: cmonLDASPurge::pageRaise   
##
## Description:
## dialog for db compare to allow selection of databases
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:
## top page just page raise the current page


proc cmonLDASPurge::pageRaise { page } {

	cmonLDASPurge::state0 $page

}

## ******************************************************** 
##
## Name: cmonLDASPurge::updateTimes   
##
## Description:
## trace proc to handle write/unset of server vars
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASPurge::updateTimes { tag args } {
    
	if	{ [ catch {
    	set tracevar [ lindex $args 0 ]
        set mode [ lindex $args 2 ]    
        if	{ [ string match w* $mode ] } {
        		set limit [ set $tracevar ]
				set before [ expr [ clock seconds ]  - $limit ]
        		set ::cmonLDASPurge::${tag}time [ clock format $before -format "%x %X %Z" ]
        		foreach { days hours mins }  [ cmonCommon::durationDHM $limit ] { break }
        		set ::cmonLDASPurge::${tag}Days $days
        		set ::cmonLDASPurge::${tag}Hours $hours
        		set ::cmonLDASPurge::${tag}Mins $mins
        } else {
        		set ::cmonLDASPurge::${tag}Days 0
        		set ::cmonLDASPurge::${tag}Hours 0
        		set ::cmonLDASPurge::${tag}Mins 0
        }
   	} err ] } {
    	set ack [ tk_messageBox -type ok -default ok -message $err -icon warning ]
        catch { appendStatus $::cmonLDASPurge::statusw(cmonLDASPurge) $err }
    }
    catch { cmonLDASPurge::state0 cmonLDASPurge }
}

## ******************************************************** 
##
## Name: cmonLDASPurge::updateTimeStr   
##
## Description:
## trace proc to match time string to the spin box changes
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASPurge::updateTimeStr { tag args } {

	if	{ [ catch {
    	set seconds [ cmonCommon::durationSecs [ set ::cmonLDASPurge::${tag}Days ] \
        	[ set ::cmonLDASPurge::${tag}Hours ] [ set ::cmonLDASPurge::${tag}Mins ] ]
        if	{ $seconds } {
        	set before [ expr [ clock seconds ] - $seconds ]
        	set ::cmonLDASPurge::${tag}time [ clock format $before -format "%x %X %Z" ]
        } else {
        	set ::cmonLDASPurge::${tag}time ""
        }
   	} err ] } {
    	#set ack [ tk_messageBox -type ok -default ok -message $err -icon warning ]
        #catch { appendStatus $::cmonLDASPurge::statusw(cmonLDASPurge) $err }
    }
}

## ******************************************************** 
##
## Name: cmonLDASPurge::updateCleanup  
##
## Description:
## trace proc to update spinbox or next cleanup time when
## sent by server
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASPurge::updateCleanup { args } {

	if	{ [ catch {
    	set tracevar [ lindex $args 0 ]
        set mode [ lindex $args 2 ]
        if	{ [ string match w* $mode ] } {
        	set limit [ set $tracevar ]
       		if	{ [ string equal ::NEXT_CLEANUP_SECS $tracevar ] } {  
        		set ::cmonLDASPurge::nextCleanup [ clock format $limit -format "%x %X %Z" ]
        	} else {
        		foreach { days hours mins }  [ cmonCommon::durationDHM $limit ] { break }
        		set ::cmonLDASPurge::cleanupDays $days
        		set ::cmonLDASPurge::cleanupHours $hours
        		set ::cmonLDASPurge::cleanupMins $mins
            }
        } else {
        	set ::cmonLDASPurge::cleanupDays 0
        	set ::cmonLDASPurge::cleanupHours 0
        	set ::cmonLDASPurge::cleanupMins 0
            set ::cmonLDASPurge::nextCleanup ""
         }
   	} err ] } {
    	set ack [ tk_messageBox -type ok -default ok -message $err -icon warning ]
        catch { appendStatus $::cmonLDASPurge::statusw(cmonLDASPurge) $err }
    }
    catch { cmonLDASPurge::state0 cmonLDASPurge }
}

## ******************************************************** 
##
## Name: cmonLDASPurge::viewChanges   
##
## Description:
## list changes to be made
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASPurge::viewChanges { page { allowcancel 0 } } {

	set site $::cmonClient::site
	if	{ ! [ info exist ::JOBS_PURGE_BEFORE ] } {
       	set ack [ tk_messageBox -type ok -default ok -message [ subst $::cmonLDASPurge::noSupportMsg ] \
			-icon error ]
        return
    }
	if	{ [ catch {
        foreach tag [ list job log ] {
    		set ${tag}seconds [ cmonCommon::durationSecs [ set ::cmonLDASPurge::${tag}Days ] \
            	[ set ::cmonLDASPurge::${tag}Hours ] [ set ::cmonLDASPurge::${tag}Mins ] ]	
        }
        set cleanup_seconds [ expr [ cmonCommon::durationSecs [ set ::cmonLDASPurge::cleanupDays ] \
            	[ set ::cmonLDASPurge::cleanupHours ] [ set ::cmonLDASPurge::cleanupMins ] ] * 1000 ]	

		set text ""
        set nochg 1
    	foreach { days hours mins }  [ cmonCommon::durationDHM $::JOBS_PURGE_BEFORE ] { break }
        set jobstring "[ set ::cmonLDASPurge::jobDays ]:[ set ::cmonLDASPurge::jobHours ]:[ set ::cmonLDASPurge::jobMins ]"
        if	{ ! [ string equal $jobstring "$days:$hours:$mins" ] } {        
        	append text    [ format "Before: purge jobs older than %0d:%0d:%0d\n" $days $hours $mins ]        
        	append text    [ format "    Now: purge jobs older than %0d:%0d:%0d\n" [ set ::cmonLDASPurge::jobDays ] \
            	[ set ::cmonLDASPurge::jobHours ] [ set ::cmonLDASPurge::jobMins ] ]
            set nochg 0
		} else { 
        	append text    [ format "No changes: purge jobs older than %0d:%0d:%0d\n" $days $hours $mins ]
        }
        
        foreach { days hours mins }  [ cmonCommon::durationDHM $::LOGS_PURGE_BEFORE ] { break }
        set logstring "[ set ::cmonLDASPurge::logDays ]:[ set ::cmonLDASPurge::logHours ]:[ set ::cmonLDASPurge::logMins ]"
        if	{ ! [ string equal $logstring "$days:$hours:$mins" ] } { 
        	append text [ format "Before: purge logs older than %0d:%0d:%0d\n" $days $hours $mins ]
        	append text [ format "    Now: purge logs older than %0d:%0d:%0d\n" [ set ::cmonLDASPurge::logDays ] \
            	[ set ::cmonLDASPurge::logHours ] [ set ::cmonLDASPurge::logMins ] ]
            set nochg 0
        } else {
        	 append text    [ format "No changes: purge logs older than %0d:%0d:%0d\n" $days $hours $mins ] 
        }       
        foreach { days hours mins }  [ cmonCommon::durationDHM $::CLEAR_OUTPUT_PERIOD ] { break }
        set cleanupstring "[ set ::cmonLDASPurge::cleanupDays ]:[ set ::cmonLDASPurge::cleanupHours ]:[ set ::cmonLDASPurge::cleanupMins ]"
        if	{ ! [ string equal $cleanupstring "$days:$hours:$mins" ] } {
        	append text [ format "Before: cleanup runs every %0d:%0d:%0d\n" $days $hours $mins ]
        	append text [ format "    Now: cleanup runs every %0d:%0d:%0d\n" [ set ::cmonLDASPurge::cleanupDays ] \
            	[ set ::cmonLDASPurge::cleanupHours ] [ set ::cmonLDASPurge::cleanupMins ] ]
            set nochg 0
        } else {
        	append text    [ format "No changes: Cleanup runs every %0d:%0d:%0d\n" $days $hours $mins ] 
        } 
        set text [ string trim $text \n ]
        if	{ ! $nochg } {
        	if	{ $allowcancel } {
        		append text "\n\nHit OK to submit or Cancel to NOT submit to server"
            }
            set ack [ MessageDlg .cmonLDASPurgeTmp -title Info -type okcancel -aspect 500 \
				-message $text -icon question -justify left -font $::MSGFONT ]	
        } else {
        	if	{ $::cmonLDASPurge::chgsToSave } {
            	append text "\n\nChanges have not been saved to cntlmon resource file"
            }
        	set ack [ MessageDlg .cmonLDASPurgeTmp -title Info -type ok -aspect 500 \
				-message $text -icon question -justify left -font $::MSGFONT ]	
        }   
	} err ] } {
    	set ack [ tk_messageBox -type ok -default ok -message $err -icon info ]
        appendStatus $::cmonLDASPurge::statusw($page) $err
    }
    if	{ $nochg } {
    	return 0
    } 
    set ::cmonLDASPurge::chgsToSave 1
    return $ack 
}

## ******************************************************** 
##
## Name: cmonLDASPurge::unsetServerVars   
##
## Description:
## remove server vars if disconnected
##
## Parameters:
## 
##
## Usage:
##  
## 
## Comments:

proc cmonLDASPurge::unsetServerVars { args } {

	if	{ ! $::cmonClient::state  } {
    	foreach var $::cmonLDASPurge::serverVars {
        	if	{ [ info exist $var ] } {
        		unset $var
            }
        }
        ;## restore traces   
		trace add variable ::JOBS_PURGE_BEFORE { write unset } [ list cmonLDASPurge::updateTimes job ]
		trace add variable ::LOGS_PURGE_BEFORE { write unset } [ list cmonLDASPurge::updateTimes log ]
		trace add variable ::CLEAR_OUTPUT_PERIOD { write unset } [ list cmonLDASPurge::updateCleanup ]
		trace add variable ::NEXT_CLEANUP_SECS { write unset } [ list cmonLDASPurge::updateCleanup ]
    }
}

;## update client display when server sends these  
trace add variable ::JOBS_PURGE_BEFORE { write unset } [ list cmonLDASPurge::updateTimes job ]
trace add variable ::LOGS_PURGE_BEFORE { write unset } [ list cmonLDASPurge::updateTimes log ]
trace add variable ::CLEAR_OUTPUT_PERIOD { write unset } [ list cmonLDASPurge::updateCleanup ]
trace add variable ::NEXT_CLEANUP_SECS { write unset } [ list cmonLDASPurge::updateCleanup ]

;## unset server vars if disconnected
trace add variable ::cmonClient::state { write } [ list cmonLDASPurge::unsetServerVars ]

