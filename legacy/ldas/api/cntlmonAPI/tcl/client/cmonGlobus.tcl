## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) controlmon client Tcl Script.
##
## This script has global functions used by packages
## in cmonClient
## for support of using globus toolkit for authenication
##
## requires globus toolkit to be installed on the
## system running cmonClient
##

;#barecode
package provide cmonGlobus 1.0

namespace eval cmonGlobus {
	set gridprogs [ list grid-proxy-info grid-proxy-init grid-proxy-destroy ]
    set globuslibs [ list globus_module \
		  globus_error \
		  globus_object \
		  globus_xio \
          gssapi ]
    set waitforbytes 1
    set buffersize 4096
    set server_data ""
    set wproxyDialog .wproxyInit
    set info ".info"
}


;#end  

;## use grid-proxy-destroy to trigger use of grid-proxy-init

#------------------------------------------------------------------------
# From: Practical Programming in Tcl and Tk 4th Edition pp. 139
# Assign a set of variables from a alist of values.
# If there are more values than variables, they are returned.
# If there are fewer values than variables,
# the variables get the empty string
#------------------------------------------------------------------------
proc lassign {valueList args} {
    if { [llength $args] == 0} {
	error "wrong \# args: lassign list varname ?varname..?"
    }
    if { [llength $valueList] == 0} {
	# Ensure one trip through the foreach loop
	set valueList [list {}]
    }
    uplevel 1 [list foreach $args $valueList {break}]
    return [lrange $valueList [llength $args] end]
}

## ******************************************************** 
##
## Name: cmonGlobus data structures
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:
## is there a limit on buffer so need to read again

;## tcpdriver - tcp driver from globus_xio_driver_load
;## filedriver - file driver from globus_xio_driver_load
;## driver stack - driver stack from globus_xio_stack_push_driver 
;## tcphandle - tcp handle   from globus_xio_handle_create 
;## filehandle - file handle from globus_xio_handle_create 
;## buffer - to hold data for read/write operations
;## buffersize - number of bytes to read 
;## nbytes - number of bytes to write
;## contact_string - data passed during globus_xio_open 


## ******************************************************** 
##
## Name: cmonGlobus::init
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc cmonGlobus::init {} {
    
    ;## if already on stack , do nothing
    if	{ [ info exist ::cmonGlobus::client_tcpstack ] } {
    	return
    }

    if	{ [ catch {
    
    	if	{ ![ regexp -nocase -- {tclglobus} [ info loaded ] ] } {
        	foreach lib $::cmonGlobus::globuslibs {
    			load [ eval file join $::TCLGLOBUS_DIR libtcl${lib}.so ]
            }
		}
    	
    #----------------------------------------------------------------
	# Activate Globus XIO module
	#----------------------------------------------------------------
		set seqpt "globus_module_activate"
		globus_module_activate $::globus_i_xio_module
	
	#----------------------------------------------------------------
	# Load transport driver
	#----------------------------------------------------------------
		set seqpt "globus_xio_driver_load"
		lassign [ globus_xio_driver_load tcp ] result ::cmonGlobus::tcpdriver
        debugPuts "load tcp result $result"
    	lassign [ globus_xio_driver_load gsi ] result ::cmonGlobus::gsidriver
        debugPuts "load gsi result $result"
        
	    lassign [ globus_xio_stack_init NULL ] result ::cmonGlobus::client_tcpstack	    
	    debugPuts "client init globus_xio_stack_push_driver result $result"
        
	    lassign [ globus_xio_stack_push_driver $::cmonGlobus::client_tcpstack $::cmonGlobus::tcpdriver ] result
        debugPuts "push tcp driver result $result"
		lassign [ globus_xio_stack_push_driver $::cmonGlobus::client_tcpstack $::cmonGlobus::gsidriver ] result
        debugPuts "push gsi driver result $result"
    
    #----------------------------------------------------------------
	# Authenication -  Set GSI authorization mode
	#---------------------------------------------------------------- 
    
    # TCP & GSI driver specific attribute initialization
    
	    debugPuts "GSI driver specific attribute initialization"
	    lassign [ globus_xio_attr_init ] result ::cmonGlobus::client_attr
        
        ;## do not use IPV6 protocol
        lassign [ globus_xio_attr_cntl \
			      $::cmonGlobus::client_attr \
			      $::cmonGlobus::tcpdriver \
			      $::GLOBUS_XIO_TCP_SET_NO_IPV6 \
			      $::GLOBUS_TRUE \
			     ] status
		debugPuts "GLOBUS_XIO_TCP_SET_NO_IPV6: status: $status"
        
        if	{ [ string equal ldas $::RUNMODE ] } {
    	# $gss_c_no_credential indicates a default host credential 
	    	lassign [ globus_xio_attr_cntl \
			  $::cmonGlobus::client_attr \
			  $::cmonGlobus::gsidriver \
			  $::GLOBUS_XIO_GSI_SET_CREDENTIAL \
			  $::gss_c_no_credential \
			 ] status
	    	debugPuts "via host cert GLOBUS_XIO_GSI_SET_CREDENTIAL status: $status"
		} else {
	    # Authentication part - Set GSI authorization mode
	    lassign [ globus_xio_attr_cntl \
			  $::cmonGlobus::client_attr \
			  $::cmonGlobus::gsidriver \
			  $::GLOBUS_XIO_GSI_SET_AUTHORIZATION_MODE \
			  $::GLOBUS_XIO_GSI_HOST_AUTHORIZATION \
			 ] status 
	    debugPuts "GLOBUS_XIO_GSI_SET_AUTHORIZATION_MODE status: $status"
	    debugPuts "GSI driver specific attribute initialization $::cmonGlobus::client_attr"
    	
        }
        cmonGlobus::readReset
    } err ] } {
    	debugPuts $err
    	return -code error $err
    }

}

## ******************************************************** 
##
## Name: cmonGlobus::close
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc cmonGlobus::close {  { user_parms NULL } } {
	
    if	{ [ catch {

    	if	{ [ info exist ::cmonGlobus::networkhandle ] } {    
			;## remove outstanding callback
        	lassign [ globus_xio_handle_cancel_operations \
        		$::cmonGlobus::networkhandle $::GLOBUS_XIO_CANCEL_READ ] result 
        	debugPuts "cancel operation read result $result"
        
        	lassign [ globus_xio_handle_cancel_operations \
        		$::cmonGlobus::networkhandle $::GLOBUS_XIO_CANCEL_WRITE ] result 
        	debugPuts "cancel operation write result $result"       
        
    		lassign [ globus_xio_close $::cmonGlobus::networkhandle NULL ] result
            catch { unset ::cmonGlobus::networkhandle }
            catch { unset ::globus_control_status }
    		if	{ [ string equal exit $user_parms ] } {
    			cmonGlobus::cleanup
    		}
       }
    } err ] } {
    	return -code error $err
    }
}

## ******************************************************** 
##
## Name: cmonGlobus::closeCallback
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc cmonGlobus::closeCallback { user_parms handle result } {
	
    if	{ [ catch {
    	debugPuts "closeCallback user_parms $user_parms handle $handle result $result"
    	unset ::cmonGlobus::networkhandle
    } err ] } {
    	puts "[myName] Error $err"
    }
    set ::cmonGlobus::done 1
}

## ******************************************************** 
##
## Name: cmonGlobus::cleanup
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc cmonGlobus::cleanup {} {
    
    global globus_i_xio_module
	global tcpdriver
	global filedriver
	
    #----------------------------------------------------------------
	# destroy the client attribute
	#------------::cmonGlobus::client_attr----------------------------------------------------
    
    if	{ [ info exist ::cmonGlobus::client_attr ] } {
    	lassign [ globus_xio_attr_destroy $::cmonGlobus::client_attr ] status
    	debugPuts "destroy attr status $status"
        unset ::cmonGlobus::client_attr
    }
    
    if	{ [ info exist ::cmonGlobus::client_tcpstack ] } {
    	lassign [ globus_xio_stack_destroy $::cmonGlobus::client_tcpstack ] status
    	debugPuts "destroy stack status $status"
        unset ::cmonGlobus::client_tcpstack
    }
    
	#----------------------------------------------------------------
	# Unload both drivers
	#----------------------------------------------------------------
    
    if	{ [ info exist ::cmonGlobus::gsidriver ] } {
    	lassign [ globus_xio_driver_unload $::cmonGlobus::gsidriver ] status
    	debugPuts "globus_xio_driver_unload gsi status $status"
        unset ::cmonGlobus::gsidriver
    }
	
    if	{ [ info exist ::cmonGlobus::tcpdriver ] } {
		lassign [ globus_xio_driver_unload $::cmonGlobus::tcpdriver ] status
    	debugPuts "globus_xio_driver_unload tcp status $status"
        unset ::cmonGlobus::tcpdriver
    }
 
	#----------------------------------------------------------------
	# Deactivate Globus XIO module
	#----------------------------------------------------------------
    
    if	{ [ info exist ::globus_i_xio_module ] } {
		lassign [ globus_module_deactivate $::globus_i_xio_module ] status 
    	debugPuts "globus_module_deactivate status $status, ::globus_i_xio_module [ info exist ::globus_i_xio_module ]"
   	}
}

## ******************************************************** 
##
## Name: openCallback
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:

proc cmonGlobus::openCallback { user_parms handle result } {

	if	{ [ catch {
    	debugPuts "open callback, user_parms $user_parms handle $handle result $result"
		set ::cmonClient::sid $handle
		if	{ ! [ string length $result ] } {
			set cmd $user_parms 
    		cmonGlobus::sendCmd "$::globusUser $::version" 1
    	} else {
        	error $result
    	}
   	} err ] } {
    	set ack [ tk_messageBox -type ok -default ok \
			-message "[ myName ]: $err" -icon info ]
    }
    
}

## ******************************************************** 
##
## Name: cmonGlobus::state1
##
## Description:
## enter state1 by opening socket to server via globus XIO 
##
## Usage:
##
## Comments:

proc cmonGlobus::state1 {} {
    
	if  { [ string equal $::cmonClient::var(site) $::cmonClient::NOSERVER ] } {
		set ack [ MessageDlg .cmonClientDlg -type ok -aspect 100 -justify left \
        	-message "Please select an LDAS site" -icon info \
    		-title Info -font $::MSGFONT ]
		return
	} 
	if	{ [ string equal $::cmonClient::oldsite $::cmonClient::var(site) ] } {
		if	{ [ llength $::cmonClient::sid ] > 0 } {
			return
		}
	}
	set ::cmonClient::oldsite $::cmonClient::var(site)
	
	if  { [ llength $::cmonClient::sid ] == 0 } {
		if	{ [ catch {	
			set login ""
			set passwd ""
			if	{ ! $::cmonClient::noPass } {
            
            ;## verify proxy           
    			foreach { login passwd } [ validateLogin ] { break }
                              
				if	{ ! [ string equal cancelled $login ] } {
                    ;## file handle construction
					if	{ [ info exist ::siteport($::cmonClient::var(site)) ] } {
						foreach { host port } $::siteport($::cmonClient::var(site)) { break }
					} else {
						set host $::cmonClient::var(site)
						set port $::cntlmonPort
					}
                                        
                    ;## open a network handle, contact string is /tmp/xxx certificate ??
                    ;## need to read/write on handle 
                    
                    lassign [ globus_xio_handle_create $::cmonGlobus::client_tcpstack ] result \
                    	::cmonGlobus::networkhandle
                    
                    debugPuts "handle create result $result $::cmonGlobus::networkhandle result $result"
                    
                    ;## use blocking IO   
                    lassign [ globus_xio_open $::cmonGlobus::networkhandle \
                    	$host:$::TCLGLOBUS_PORT $::cmonGlobus::client_attr ] result 
                     
                    cmonGlobus::sendCmd "$::globusUser $::version" 1
				}
			} 
		} err ] } {
            set ack  [ scrolledMessageDialog $err ok "GSI socket error"]
			cmonClient::setDisconnected 
			return -code error $err
		}
	}
}

## ******************************************************** 
##
## Name: sendCmd
##
## Description:
## issue a command, generally to operator socket of cntlmonAPI
##
## Usage:
##
## Comments:

proc cmonGlobus::sendCmd { cmd { register 0 } } {

	catch { unset ::cmonGlobus::sendCallbackDone } 
    ;## write cmd to server
    if  { [ catch { 
    	if	{ [ string length $cmd ] } {
            set cmd [ cmd::size $cmd ]
        	lassign [ globus_xio_register_write $::cmonGlobus::networkhandle \
		    $cmd \
		    $::cmonGlobus::waitforbytes \
		    NULL \
		    [ list cmonGlobus::sendCmdCallback $register ] ] result 
            debugPuts "sendCmd done result $result"
            vwait ::cmonGlobus::sendCallbackDone                         
        } 
    } err ] } {
    	debugPuts "[myName] error: $err, state $::cmonClient::state"
        # cmonClient::state0 lostconnect
		return -code error $err
    }  
}

## ******************************************************** 
##
## Name: sendCmdCallback
##
## Description:
## issue a command, generally to operator socket of cntlmonAPI
##
## Usage:
##
## Comments:
proc cmonGlobus::sendCmdCallback { user_parms handle result buffer data_desc } {

     if	{ [ regexp -nocase {end of file} $result ] } {
    	debugPuts "$handle eof"
    	cmonClient::setDisconnected  
        return 
    }
    
    ;## get ready to read data from server
    if	{ [ catch {
		debugPuts "sendCmd callback $user_parms handle $handle result $result buffer $buffer data_desc $data_desc"
        if	{ [ string length $result ] } {
        	error $result
        }
        if	{ $user_parms } {
        ;## dont register if it has been done
  			lassign [ globus_xio_register_read $handle $::cmonGlobus::buffersize \
    	 	$::cmonGlobus::waitforbytes NULL \
    		[ list cmonGlobus::readCallback $user_parms ] ] result
        }
        set ::cmonGlobus::sendCallbackDone 1

   	} err ] } {
    	if	{ ![ regexp {Operation was canceled} $err ] } {   
    		set ack [ tk_messageBox -type ok -default ok \
			-message "Possible data transmission error $err, please examine details in \
				file $::TMPDIR/dataRecv.[ set ::cmonClient::var(site)]" \
				-icon info ]
        }
        debugPuts "[ myName ] error: $err"
    }
                   
}

## ******************************************************** 
##
## Name: readReset
##
## Description:
## reset read buffer
##
## Usage:
##
## Comments:
## reset buffer accumulating data to empty for the next read
 
proc cmonGlobus:::readReset {} {

	set ::cmonGlobus::server_data ""
    catch { unset ::cmonGlobus::server_data_size }
    
}

## ******************************************************** 
##
## Name: accumulateRead
##
## Description:
## accumulate data into local var and process if msg has been read
##
## Usage:
##
## Comments:

proc cmonGlobus::accumulateRead { buffer { size 0 } } {
    
    if	{ [ catch {   		
    	if	{ ! [ info exist ::cmonGlobus::server_data_size ] } {
        	set ::cmonGlobus::server_data_size $size
    	}
		append ::cmonGlobus::server_data $buffer 
   	 	debugPuts " server_data [ string length $::cmonGlobus::server_data ], server sent $::cmonGlobus::server_data_size bytes"
    	if	{ [ string length $::cmonGlobus::server_data ] >= $::cmonGlobus::server_data_size } {
        	cmonGlobus::processResult
        	cmonGlobus::readReset
   		}
   	} err ] } {
        return -code error $err
    }	
}

## ******************************************************** 
##
## Name: processResult
##
## Description:
## process result read from server
##
## Usage:
##
## Comments:
proc cmonGlobus::processResult {} {

	if	{ [ catch {
    			set site [ string trim $::cmonClient::var(site) ]
            	set result [ split $::cmonGlobus::server_data \n ]
            	regexp {^\{(.+)\}$} $::cmonGlobus::server_data match result
            	set callback [ lindex $result 1 ]
            	regexp {^\{(.*)} $callback match callback
            	set clientCmd [ lindex $result 2 ]		
	    		set rc [ lindex $result 3 ]
            	set data [ join [ lrange $result 4 end ] \n ]
            	;## initial handshake
            	if	{ [ string match nocallback $callback ] } {
					set update 1
					if	{ $rc == 3 } {
                		set disconnect 1
						error $data
					}
                	;## rc=4 for pure update
               		# set data [ join [ lrange $result 4 end ] \n ]
            		# set reply [ lindex $result 5 ]      		
					set cmd [ lindex $result 4 ]
					set reply [ lindex $result 5 ]
					regexp {^\{(.+)\}$} $cmd -> cmd
	 				set cmd [ subst -nobackslashes -novariables -nocommands $cmd ]
					eval $cmd  
            		if  { $rc == 0 } {
			    		regexp {([^:]+):([^:]+):([^:]+)[:]*(.*)} $clientCmd -> ::cmonClient::client \
						::cmonClient::serverVersion ::cmonClient::serverKey cstatus
                		set ::serverVersionF [ split $::cmonClient::serverVersion . ]            
			    		cmonClient::setConnected
                        set ::cmonClient::sid $::cmonGlobus::networkhandle
			    		if	{ [ string length $reply ] } {
				    		set ack [ MessageDlg .cmonClient[clock seconds] -title Warning \
							-type ok -aspect 500 -message $reply -icon warning -justify left -font $::MSGFONT ]
			    		}
                		;## if user is control, set control password
                		if  { [ string match priviledged $cstatus ] && $::passwdRemain == -1 } {
                        	;
                		}
                        set ::globus_control_status $cstatus
            		} else {
                    	error $reply
                    }
            	} else {
					set data [ subst -nobackslashes -novariables -nocommands $data ]
        			set seqpt "$callback $rc $clientCmd"
					set data [ concat $data ]
					if	{ $::DEBUG } {
                		if  { ! [ file exist $::TMPDIR ] } {
                    		file mkdir $::TMPDIR
                		}
						set fd [ open $::TMPDIR/dataRecv.$site w ]
						puts $fd "$callback $rc\n$data"
						::close $fd 
					}
					foreach { procname page } $callback { break }
					;## unset control password if error
                    debugPuts "procname $procname page $page rc $rc clientCmd $clientCmd data $data"
					eval { $procname $page $rc $clientCmd $data }
            
				}
	} err ] } {    	
    	set fd [ ::open $::TMPDIR/dataRecv.$site w ]
		puts $fd "$callback $rc\n$data"
		::close $fd 
        set ack [ tk_messageBox -type ok -default ok \
			-message "Possible data transmission error $err, please examine details in \
				file $::TMPDIR/dataRecv.$site" \
				-icon info ]
    }
}

## ******************************************************** 
##
## Name: readCallback
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:
## a callback is registered each time to listen to server data
## may return "An end of file occurred" from result if there is nothing

proc cmonGlobus::readCallback { user_parms handle result buffer data_desc } {
   
   	set ::cmonGlobus::readCallbackDone 1
    catch { unset ::cmonGlobus::readInProgress }
    if	{ [ regexp -nocase {end of file} $result ] } {
    	debugPuts "$handle eof"
    	cmonClient::setDisconnected  
        return 
    }
 
    if	{ [ catch {
    	debugPuts "readCallback read callback $user_parms handle $handle result  \
		$result buffer"
        
        if	{ [ catch {
        	set nbytes [ string length $buffer ] 
        } err ] } {
        	debugPuts "error taking length of buffer  $err"
        }
        if	{ [ string length $result ] } {
        	error $result
        }
    	set size 0 	
        if	{ [ string length $buffer ] } {
        	if { [ regexp {~~(\d+)([^~]+)} $buffer -> size bufdata ] } {  
            	debugPuts "size $size, [ info exist ::cmonGlobus::server_data_size ] "  
            } 
            cmonGlobus::accumulateRead $buffer $size  
        } 
        ;## always register a read callback
        unset ::cmonGlobus::readCallbackDone
        set ::cmonGlobus::readInProgress 1
        lassign [ globus_xio_register_read $handle $::cmonGlobus::buffersize \
    	 		$::cmonGlobus::waitforbytes NULL \
    			[ list cmonGlobus::readCallback NULL ] ] result

    } err ] } {
    	debugPuts "error [ myName ] $err" 
        if	{ ! [ regexp {Operation was canceled} $err ] } {        	
    		set ack [ tk_messageBox -type ok -default ok \
			-message "[ myName]: $err" -icon error ]  
        }      
    }
}

debugPuts " tclglobus $::TCLGLOBUS_DIR"
