## ******************************************************** 
## metaschema.tcl Version 1.0
##
## generates the sql query for getting column names
## column data types and primary or foreign keys
##
##
##
## Release Date: Not Available
##
## Valid options are :
## -table [ all | <tabname> ]
## -column [ all | none | <colname> ]
## -type [ all | none ]
## -key [ primary | foreign | none ]
##
## Valid objectives are:
## 1. get column names of a table or all tables
## 
## -table [ all | <tabname> ]
## -column [ all <colname> ]
## -type "none" 
## -key  "none"
##
## 2. get data types of one or all columns of a table or all tables
## -table [ all | <tabname> ]
## -column [ all <colname> ]
## -type "all" 
## -key  "none"
##
## 3. get primary or foreign key of a table or all tables
## -table [ all | <tabname> ]
## -column "none"
## -type "none" 
## -key [ primary | foreign ]

package provide metaschema 1.0

namespace eval metaschema {}

## ******************************************************** 
##
## Name: metaschema::init 
##
## Description:
## Initialization - set up defaults 
##
## Parameters:
## None.
##
## Usage:
##  metaschema::init 
##
## Comments:
## 

proc metaschema::init {} {
    set condition { { tabschema !='SYSCAT' } { tabschema !='SYSIBM' } \
        { tabschema !='SYSSTAT' } }
    set tabnames { tabname }
    set fromtabs { syscat.tables } 
    set order { tabname }
    array set ::metaschema::select  [ list what $tabnames ] 
    array set ::metaschema::select  [ list from $fromtabs ] 
    array set ::metaschema::select  [ list condition $condition ] 
    array set ::metaschema::select  [ list order $order ] 
    set ::metaschema::state  stateINIT
}

## ******************************************************** 
##
## Name: metaschema::makeQuery
##
## Description:
## generates the sql query to get schema information 
## based on input options
##
## Parameters:
## None.
##
## Usage:
##  metaschema::makequery
##
## Comments:
## 
proc metaschema::makeQuery {} {
       
    set sql "select [ join $::metaschema::select(what) , ] \
        from [ join $::metaschema::select(from) , ] \
        where [ join $::metaschema::select(condition) "and " ] \
        order by [ join $::metaschema::select(order) , ] \
		for read only"

#    metadata::log "sql=$sql"
    return $sql
}

## ******************************************************** 
##
## Name: metaschema::setTable 
##
## Description:
## Set up for -table option 
##
## Parameters:
## optTable  - table option [all | <tabname>]
##
## Usage:
##  metaschema::setTable $optTable  
##
## Comments:
## 

proc metaschema::setTable { optTable } {
##  specific table, name must be in uppercase
##    
    if  { ! [ regexp -nocase {all} $optTable ] } {
        if  { $::metaschema::state == "stateKEY" } {
            lappend ::metaschema::select(condition) \
            "t.tabname='[ string toupper $optTable ]'" 
            set order " t.tabname "
            array set ::metaschema::select  [ list order $order ] 
        } else {
            lappend ::metaschema::select(condition) \
            "tabname='[ string toupper $optTable ]'" 
        }
        array set ::metaschema::select [ list table $optTable ]
    }
    if  { $::metaschema::state == "stateINIT" } {
        set ::metaschema::state stateCOLNAME
    }
}


## ******************************************************** 
##
## Name: metaschema::setKey 
##
## Description:
## Set up for -key option 
##
## Parameters:
## optKey  - key option [ <primary> | <foreign> | none ]
##
## Usage:
##  metaschema::setKey $optTable  
##
## Comments:
## 
proc metaschema::setKey { optKey } {

    if  { ! [ regexp -nocase {none} $optKey ] } {
        set tabnames { t.tabname k.colname }
        set fromnames { { syscat.tabconst t} { syscat.keycoluse k } }
        switch -exact $optKey {
            primary { set keyType "P"}
            foreign { set keyType "F" }
            default { set keyType "P"}        
        }
        set condition { { t.tabname = k.tabname } \
            { t.constname = k.constname } }
        lappend condition " t.type = '$keyType' "
        array set ::metaschema::select [ list what $tabnames ]
        array set ::metaschema::select [ list from $fromnames ]
        array set ::metaschema::select  [ list condition $condition ] 
        set ::metaschema::state stateKEY
    }
}

## ******************************************************** 
##
## Name: metaschema::setColumn 
##
## Description:
## Set up for -column option 
##
## Parameters:
## optKey  - column option [ <colname> | none ]
##
## Usage:
##  metaschema::setColumn $optCol  
##
## Comments:
## 
proc metaschema::setColumn { optCol } {
    
    if  { $::metaschema::state != "stateCOLNAME" && \
          $::metaschema::state != "stateTYPE"  } {
        return
    }
    set fromtabs { syscat.columns } 
    array set ::metaschema::select  [ list from $fromtabs ] 
    lappend ::metaschema::select(what) colname 
    
    if  { ! [ regexp -nocase {(all|none)} $optCol ] } {
        lappend ::metaschema::select(condition) \
            "colname='[ string toupper $optCol ]'"
    }
}

## ******************************************************** 
##
## Name: metaschema::setType 
##
## Description:
## Set up for -type option 
##
## Parameters:
## optType  - type option [ all | none ]
##
## Usage:
##  metaschema::setType $optType  
##
## Comments:
## 
proc metaschema::setType { optType } {
    if  { $::metaschema::state == "stateKEY" } {
        return
    }
    if  {  [ regexp -nocase {all} $optType ] } {
        lappend ::metaschema::select(what) typename
        set metaschema::state stateTYPE
    }
}

## ******************************************************** 
## Initialization automatically called on package require
##
metaschema::init
