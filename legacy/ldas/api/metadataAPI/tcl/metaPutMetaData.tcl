## ******************************************************** 
## putMetaData.tcl Vrename $oldname ""ersion 1.0
##
## putMetaData cmd functions for slave interp
##
## Provides query retrieval functions for the LDAS.
## Contains subset of cmds in metadata.tcl
## to be run in a slave interp.
##
## Release Date: Not Available
##
## This module requires the following sub-modules:
##   1. genericAPI.tcl (stack and queue manipulation functions)
## ;#ol
## ******************************************************** 

package provide putMetaData 1.0

## ******************************************************** 
##
## Name: dumpILwd 
##
## Description:
## output ilwd file for debugging  
##
## Usage:
##       
##
## Comments:

proc dumpILwd { jobid ptr type { format ascii } { comp none } } {
    if  { [ string match *LdasContainer* $ptr ] } {
        set ilwdfile ${jobid}${type}.ilwd
        if  { [ catch {
            set target [ ilwd::writeFile $jobid $ptr $ilwdfile $format $comp ]
        } err ] } {
            addLogEntry "failed to write $ilwdfile: $err" red
        }
    }
    return $target
}

## ******************************************************** 
##
## Name: metadata::isTableContp
##
## Description:
## determines if container is the table container
## from name attribute
##
## Parameters:             
##
## Usage:
##  metadata::isTableContp $contp
##
## Comments:

proc metadata::isTableContp { contp } {
    set rc 0
    if  { [ catch {
        set name [ getElementAttribute $contp name ]
        regsub {:Table:XML} $name {} name
        if  { [ regexp {:table$} $name ] } {       
            set rc 1
        }
    } err ] } { 
        set rc 0 
    }
    return $rc
}

## ******************************************************** 
##
## Name: metadata::getTxnContp
##
## Description:
## locate the transaction container to handle
## single txn or multi-txns 
##
## Parameters:             
##
## Usage:
##  metadata::setSchema $stmtp  
##
## Comments:
## do not return with a catch

proc metadata::getTxnContp { contp } {
    set rc 0
    if  { [ catch {
        if  { [ isTableContp $contp ] } {
            set rc 1
        } else {
        ;## go one level deep to get the txn container
            # set tcontp [ refContainerElement $contp 0 ]
			set tcontp [ getContSkipComment $contp ]
            ;## see if the children are tables
			set tcontp1 [ getContSkipComment $tcontp ] 
            if  { [ isTableContp $tcontp1 ] } {             
                set rc 1
            }
        }
    } err ] } {
		return -code error $err
    }
    return $rc
}

## ******************************************************** 
##
## Name: metadata::getContSkipComment
##
## Description:
## locate first container within a container but skip comment if it hits one
## wrapper for refContainerElement
##
## Parameters:             
##
## Usage:
##  metadata::getContSkipComment
##
## Comments:
## parentcontp is already a confirmed container

proc metadata::getContSkipComment { parentcontp } {

	set size [ getElementAttribute $parentcontp size ]
	for { set i 0 } { $i < $size } { incr i 1 } {
		set elemp [ refContainerElement $parentcontp $i ] 
		set name  [ getElementAttribute $elemp name ]
		if  { [ regexp -nocase {^comment} $name ] } { 
			continue
		} else {
			if { [ catch {
			   getElementAttribute $elemp size
			} err ] } {
				continue
			}
			;## found a container
			break
		}
	}
	 
	if	{ $i > $size } {
		return -code error "No container found in $parentcontp"
	} 
	return $elemp
}

## ******************************************************** 
##
## Name: metadata::updateTable
##
## Description:
## extract data from ilwd object and insert rows into table
##
## Parameters:
##   container_p        pointer to group or row container
##                       ( if no group container )
##
## Usage:
## updateGroup $contp
## 
## Comments:
## Assumes table containers within this container
## container can be group or row
## if group, check insert order
##  group container name  name='<group name>:group'
##
## if row (no group), assume one table in insertlist 
##  row container name name='<group name>:<table name>'
##  and assume block insert 

proc metadata::updateTable { jobid contp stmtp } {

    set code ok
    set errmsg ""
    set dberr ""
    
	if	{ $::DEBUG >=3 } {
		puts "[myName]\n[getElement $contp]"
	}
    if  { [ catch {
        set seqpt "getElementAttribute $contp name"
        set errlvl 0
        set contname [ getElementAttribute $contp name ]
		log "contname $contname"

#   outer container has group information e.g.ligo:<group>:file
    
        set grpname [ lindex [ split $contname : ] 0 ]
        set seqpt "getElementAttribute $contp size"
        set numTbls [ getElementAttribute $contp size ]
        ;## table name is before column name 
        regsub {:Table:XML} $contname {} contname
        set tablist [ split $contname : ]
        set len [ llength $tablist ]
        set table_name [ lindex $tablist [ expr $len - 2 ] ]
        set ::${jobid}($table_name,contp) $contp
        #upvar ::$table_name $table_name
        #set ${table_name}(contp) $contp
        
        ;## puts "[getElement [ set ::${jobid}($table_name,contp) ]]"
		set seqpt "metadata::insertBlock $jobid $contp $table_name $stmtp $grpname" 
        metadata::insertBlock $jobid $contp $table_name $stmtp $grpname 
    
#   put each table in order of insertion

#   order them in insertlist - this part maybe obsolete 
        
        set err "" 
        set code "ok"    
    ;##   insert all rows for each table in insert order 
    } err ] } {
        ;## let the toplevel caller log this 
	    ##log "$seqpt: $err, $dberr" 2
        set code error
        set errmsg "$seqpt: $err"
		return -code error $errmsg
    }       
}
## ********************************************************

## ******************************************************** 
##
## Name: metadata::updateTxn 
##
## Description:
## update a single txn or multi-txn 
##
## Parameters:
##     contp    - pointer to outermost container
##
## Usage:
## 
## Comments:
## Assumes ilwd has layers :
##  File container
##          Table container
##              Row elements
## Returns 0 for single txn, 1 for multi-txn
## destroys the C++ object here
## first gather a list 
## ******************************************************** 

proc metadata::updateTxn { jobid contp } {

    if  { [ catch {
        set size [ getContainerSize $contp ]
        if  { !$size } {
            error "cannot process 0 size container"
        }
        set dbname [ set ::${jobid}(-database) ]
        foreach { dbuser dbpasswd } [ set ::${dbname}(login) ] { break }     
        __t::start ${jobid}_${contp}
        set tid [ DBPush_t $dbname $dbuser [ decode64 $dbpasswd ] $contp \
            $::MAX_TRIES $::pattern_putMetaData ] 
            
        ::setAlert $tid ::$tid 
        ::setTIDCallback $tid "::metadata::submitInsertReaper $tid $jobid $contp"
        
        ;## after $::THREAD_CK_DELAY [ list ::metadata::submitInsertReaper $tid $jobid $contp ]
        log "started DBPUsh thread $tid" 
    } err ] } {
		;## cleanup done on slave deletion
        addLogEntry $err 2
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: metadata::updateTableFile 
##
## Description:
## gets Ilwd data from file and ingest entire Contents
##  for processing
##
## Parameters:
##     filename         data filename 
##
## Usage:
## 
## Comments:
## Assumes ilwd has layers :
##  File container
##          Table container
##              Row elements
## Returns 0 to main interp if error, 1 if ok
## destroys the C++ object here
## ******************************************************** 

proc metadata::updateTableFile { jobid filename } {
# read entire file in at once 

    if  { [ catch { 
        set seqpt "openILwdFile $filename r"
        ;##log "before openfile [ lindex [ sysData ] 4 ] Kb"
        set filwd [ openILwdFile $filename "r" ] 
        set seqpt "readElement $filwd r"
        set contp [ readElement $filwd ]
        ;##log "after read [ lindex [ sysData ] 4 ] Kb"
        log "ilwd file is $filename" 
        closeILwdFile $filwd
        updateTxn $jobid $contp 
    } err ] } {
        catch { closeILwdFile $filwd }
        return -code error "$seqpt: $err"
    }
	return -code ok
}

## ******************************************************** 
##
## Name: metadata::updateTableSocket
##
## Description:
## gets Ilwd data from data socket and ingest entire Contents
##  for processing
##
## Parameters:
##     None.
##
## Usage:
## 
## Comments:
## Assumes ilwd has layers :
##  File container
##          Table container
##              Row elements
## Returns 0 to main interp if error, 1 if ok
## destroys the C++ object here
## ******************************************************** 

proc metadata::updateTableSocket { jobid contp } {
# process the container received from socket
    if  { [ catch { 
        ;## for each container inside main container 
        updateTxn $jobid $contp
    } err ] } {
        return -code error $err
    }
	return -code ok 
}

## ******************************************************** 
##
## Name: metadata::submitInsertReaper
##
## Description:
## output results of a query to ilwd file
##
## Parameters:
## easyInstance     stmt instance name
## ilwd filename    filename for ilwd
## tag              tag to identify the results
##
## Usage:
##  metadata::outIlwd $stmtp "results.out" 
##
## Comments:
## uses ilwd namespace procs to write ilwd output
## of metadata results as native ilwd 
## pointer to ilwd object is set in ::metadata::ilwdObject
## which must be destroyed by caller.
## how is database error captured 
## thread vars turnover is very fast so make sure it is
## associated with the same job

proc metadata::submitInsertReaper { tid jobid contp args } {
	
	set caller [ myName ]
    if	{ ! [ info exist ::$tid ] } {
    	;## addLogEntry "::$tid does not exist." purple
    	return
    }
    set state [ set ::$tid ]
    if 	{ ! [ string equal FINISHED $state ] &&
          ! [ string equal $state $::TID_FINISHED ]  } {
        return
    }
    set seqpt ""
	if	{ [ catch {
    	set seqpt "DBPush_r" 
		set result [ DBPush_r $tid ]
        ::unset ::$tid
        
        set seqpt "thread $tid completed"
		log "$tid completed for $jobid, result='$result'"
        
		# set endtic [ exectime ]
        set endtic 1.0
        set totsecs [ __t::mark ${jobid}_${contp} ]
        __t::cancel ${jobid}_${contp}
		set begintic 1.0
        set dbname [ set ::${jobid}(-database) ]
           
        set text ""
        set total_rows 0 
        set curtime [ gpsTime now ]
        set DBmsg [ lindex $result 0 ]
       
        ;## log as non-fatal if there is msg
        if  { [ string length [ string trim $DBmsg ] ] } {
            regsub -nocase -all -- {iteration} $DBmsg {*** Iteration} DBmsg
            addLogEntry $DBmsg orange
        }
        set result [ lrange $result 1 end ]   
        foreach { table rows wallsecs } $result {
            addLogEntry "putMetaData inserted $rows rows into $dbname database table $table \
            at the rate of [ format "%3.5f" [ expr $rows / $wallsecs ] ] rows/sec, \
            took walltime [ format "%3.5f" $wallsecs ] secs" blue
            append ::${jobid}(products) "Inserted $rows rows into table $table\n"
            incr total_rows $rows
            runningStatsFile $jobid putMetaData $rows $table
        }
        
        set ::${jobid}(dims) $total_rows
        append ::${jobid}(products) "Inserted $total_rows rows into $dbname database, took $totsecs secs.\n"
	} err ] } {
        regsub -nocase -all -- {iteration} $err {*** Iteration} err
        addLogEntry $err 2
        if	{ [ string match DBPush_r $seqpt ] } {
        	catch { unset ::$tid }
        }
        catch { __t::cancel ${jobid}_${contp} } 
        if  { ! [ info exist ::${jobid}(errors)  ] } {
            set ::${jobid}(errors) $err
        } else {
            append ::${jobid}(errors) "$err\n"
        }
	}
    metadata::cleanupJob $jobid [ myName ]
}

## ********************************************************
## Here list the interface cmds 
## ********************************************************

## ******************************************************** 
##
## Name: metadata::putMetaData
##
## Description:
## user command to handle table insertion 
##  from ilwd
##
## Parameters:
## args has
## -ingestdata    [ <fileURL> | <httpURL> | <ftpURL> | <socketURL> ]
## -returnprotocol [ <fileURL> | <httpURL> | <mailURL> | <ftpURL> ]
##
## Usage:
## 
## Comments:
## will not return to macro since trace displayed
## have to do exit
## ******************************************************** 

proc metadata::putMetaData { jobid } {
	
    set ::${jobid}(dims) 0
    if  { [ catch {
        set ingestdata [ set ::${jobid}(-ingestdata) ]   
        if  { [ regexp {port:(.+)} $ingestdata -> target ] } {
            updateTableSocket $jobid $target
        } else {
            foreach { protocol target port } [ fixUrlTarget $ingestdata ] { break }
            switch $protocol {
            file { log "ingestdata from file $target"
                updateTableFile $jobid $target
            }
            default { return -code error "protocol $protocol not supported"}
            }
        }
    } err ] } {
		set ::${jobid}(errors) $err
        putMetaDataExit
    }          
}
## ******************************************************** 
