## ******************************************************** 
## metadata_debug.tcl Version 1.0
##
## Contains debugging procs borrowed from Tcl books etc.
##
##

## ******************************************************** 
##
## Name: Namespace_List
##
## Description:
## list namespace for debugging
##
## Usage:
##     
## 
##    Returns:
##        Completion status of table loading. 
##    
## Comments:
## Debugging 

proc Namespace_List { { namespace {} } }  {
    if  { [ string length $namespace ] == 0 } {
        set namespace [ uplevel { namespace current } ]
    }
    set result {}
    foreach cmd [ info commands ${namespace}::* ] {
        if  { [ namespace origin $cmd ] == $cmd } {
            lappend result $cmd
        }
    }
    return $result
}

## ******************************************************** 
##
## Name: Call_Trace
##
## Description:
## prints the call stack
##
## Usage:
##     
## 
## Returns:
##    call stack 
##    
## Comments:
## Debugging 

proc Call_Trace { { file stdout } } {
    puts $file "Tcl call trace "
    for { set x [ expr [ info level ]-1 ] } { $x > 0 } { incr x -1} {
        puts $file "$x: [info level $x]"
    }
}
        
## ********************************************************



