# to print 
#set term hpljii 300
# this works but takes a while and outputs 10 pages
#set term postscript landscape "Times-Roman"   
#set out "|lpr -P hpmlkn5mv"

# load 'perform.plot'
clear
#set multiplot
set xrange [1:10000]
set xlabel "number of rows inserted per ilwd"
set logscale x 
set logscale y
set yrange [0.1:100]
set ylabel "#rows/sec"
set format x "%f"
set format y "%2.2f"
set title "LDAS Metadata Insertion Rates"
set pointsize 2
set grid 1 
set function style linespoints
set data style linespoints

set nokey

set label "--program" at 1000,1.80
set label "--frameset" at 1000,3.55
set label "frame--" at 4000,6.16
set label "--statistics" at 1000,1.3
set label "ringdown--" at 4,1
set label "----burstevent" at 1000,1.15
set label "directedperiodic--" at 1.5,1.25
set label "--gds triggers" at 100,0.36 
set label "--binaryinspiral" at 1000,0.90
set label "--Block insert" at 100,90
set label "--single Row" at 100,0.86

plot 'plotdata' index 0
replot 'plotdata' index 1
replot 'plotdata' index 2
replot 'plotdata' index 3
replot 'plotdata' index 4
replot 'plotdata' index 5
replot 'plotdata' index 6
replot 'plotdata' index 7
replot 'plotdata' index 8
replot 'plotdata' index 9 w linesp lt 1 lw 5
replot 'plotdata' index 10 w linesp lt 1 lw 5
#set nomultiplot
