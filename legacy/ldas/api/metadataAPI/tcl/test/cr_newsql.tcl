#! /ldcg/bin/tclsh

## ********************************************************
##
## Name: cr_newsql.tcl
##
## Description:
## use db2 and script selgetMeta.sql to extract
## some current db info and substitue in template 
## to produce the actual sql file.
## ******************************************************** 
;#barecode

set auto_path "/ldas/lib $auto_path"
source /ldas/lib/genericAPI/b64.tcl

proc getdbInfo { sql db } {
    if  { [ catch { 
		set cmd "connect to $db user ldasdb using [decode64 $::dblock]"
        set cmd "exec $::db2 $cmd"
        catch { eval $cmd } data
        puts "db2 connection $data"
		;## include script with ldas release
        set cmd "-tf $::DB2SCRIPTS/$sql"
        set cmd "exec $::db2 $cmd"
        catch { eval $cmd } lines
		set cmd "exec $::db2 terminate"
        set lines [ split $lines \n ]
    } err ] } {
        return -code error $err
    }
    return $lines
}

## ********************************************************
##
## Name: parsedbInfo
##
## Description:
## make hash tables out of results from db2
## ********************************************************

proc parsedbInfo {} {
uplevel #0 {

 set xrefs $::lines 
 set tables {} 
 set state "NEWTABLE"
 set numRows 0
 set reserved { VERSION } 

 foreach line $xrefs {
    regsub -all {[ \t]+} $line " " line
    
    ;## skip blank lines 
    if  { [ regexp -nocase {^[-DB2]+} $line ] } {
        continue
    }
    if  { [ regexp -nocase {table (coinc_sngl|multi_burst|sngl_ringdown|multi_inspiral|sngl_transdata|filter_params|process_params|sngl_unmodeled_v|filter|process|sngl_unmodeled|frameset_chanlist|segment_definer|summ_comment|frameset_loc|segment|summ_spectrum|frameset_writer|sngl_burst|summ_statistics|frameset|sngl_datasource|summ_value|gds_trigger|sngl_dperiodic|sngl_inspiral)} \
$line -> table ] } {
        set state "NEWTABLE"
        set fields [ list ]
        set oldtable $table            
        continue
    }
    if  {  ! [ string compare $state "NEWTABLE" ] } {
        set linelist [ split $line ]
        if  { [ llength $linelist ] == 0 } {
            continue
        }
        foreach field $linelist {
            if  { [ lsearch -exact $reserved $field ] > -1 } {
                set field "${field}_local"
            }
            if  { $field != "" } {
                lappend fields [ string tolower $field ]
            }      
        }
        set state "NEWFIELDS"
        continue
    }
    if  { [ regexp -nocase {(\d+) record} $line match numRows ] } {
            puts  "// $table has $numRows rows" 
            puts "table $table,[array get ::${table}]"
            continue
    }   
  
    if  {  ! [ string compare $state "NEWFIELDS" ] || ! [ string compare $state "NEWDATA" ] } {
        set valuelist [ split $line ]
        if  { [ llength $valuelist ] > 0 } {
            if  { ! [ string compare $state "NEWFIELDS" ] } {
                set state "NEWDATA" 
            }
            set i 0
            foreach data $line {
                if  { [ regexp -nocase {([x']*[\+A-Za-z0-9_.-]+[']*)} $data -> data ] } {
                    set field [ lindex $fields $i ]
                    if  { [ info exist ::${table}($field) ] } {
                        lappend ::${table}($field) $data
                    } else {
                        set ::${table}($field) $data
                    }
                    incr i
                }
            }
        }
        continue
    }
    } ;## foreach
} ;## end uplevel
}

## ********************************************************
##
## Name: main
##
## Description:
## make hash tables out of results from db2
## ********************************************************

set ::dblock RmVpZGtpbW0=
set ::DB2SCRIPTS /ldas_usr/ldas/test/database
set ::db2 /usr2/ldasdb/sqllib/bin/db2
set ::db [ lindex $argv 1 ]
puts "db=$::db"
;## connect to db2 and select necessary fields into an out file
if  { [ catch {
    set lines [ getdbInfo selgetMeta.sql $::db ]
;## parse out file into memory hash table
    parsedbInfo
;## write out examples_new.sql
    set fname [ lindex $argv 0 ]
    set fout [ open "${::db}_$fname.sql" w ]
    set fin [ open "$fname.sqltmpl" r ]
    set lines [ read $fin ]
    close $fin
    set lines [ split $lines \n ]
    foreach line $lines {
        if  { [ catch {
            puts $fout [ subst $line ]
        } err ] } {
            puts "subst error=$err"
            continue
        }
    }
    close $fout 
} err ] } {
    puts "$err, exiting"
}

set cmd "terminate"
set cmd "exec $::db2 $cmd"
catch { eval $cmd } data
