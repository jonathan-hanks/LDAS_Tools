#!/ldcg/bin/tclsh

## ******************************************************** 
##
## This is the Laser Interferometer Gravitational
## Oservatory (LIGO) LDASgwrap Tcl Script.
##
## This script is a generic wrapper for an LDAS low level
## API, providing the standard communication facilities
## required for processing user commands with the wrapped
## API via the manager.
##
## In addition to the API code which is required to be
## wrapped, a resource file must be available named:
## .
##             LDASapi.rsc
## .
## Then this script can be invoked as the name of the API
## from a hard link with the name of an API, and the API
## will be wrapped with operator and emergency services
## which the manager knows how to access.
##
## hard links or copies will be made from this script with
## appropriate names; frame, metadata, ligolw etc., by the
## install process.
## 
## ******************************************************** 
;#barecode
catch { file delete [ glob ./logs/.metadata.*.lock ] }
set ::RCS_ID_LDASgwrapin {$Id: mleitest.tcl,v 1.9 2004/01/20 17:22:52 emaros Exp $}
set ::RCS_ID_LDASgwrapin [ string trim $::RCS_ID_LDASgwrapin "\$" ]

set ::MGRKEY [ lindex $argv 0 ]
if { [ string length $::MGRKEY ] < 3 } {
   return -code error "Manager key not provided on command line!!"
 } 

set ::LDAS /ldas

if  { ! [ info exist ::env(USE_DB) ] } {
    puts "please set database with env USE_DB"
    exit
}

;## This line was set by the build process, if it is
;## not correct, please file a bug report!
set auto_path "$::LDAS/lib $auto_path"

;## check to see if the first path in the auto_path is real!
if { ! [ file exists [ lindex $auto_path 0 ] ] } {
   set err "The default library path for LDAS shared objects:\n"
   append err "[ lindex $auto_path 0 ]\n"
   append err "does not exist or is not readable!"
   return -code error $err
 }
   
;## this script is invoked through a link which has the
;## name of the API it should be.  relies on correct
;## name formatting for file, e.g.: apinameAPI
regexp {[a-z]+} [ file tail $argv0 ] API
set API metadata

;## source a resource file in the current directory if it exists
if { [ file exists LDASapi.rsc ] } {
   file attributes LDASapi.rsc -permissions 0600
   source LDASapi.rsc
 } else {
   source [ file join $::LDAS bin LDASapi.rsc ]
 }

;## API specific resources required by generic API
if { [ file exists LDAS${API}.rsc ] } {
   file attributes LDAS${API}.rsc -permissions 0600
   source LDAS${API}.rsc
 } else {
   source [ file join $::LDAS lib ${API}API LDAS${API}.rsc ]
 }   

;## API specific pre-init requirements before generic API is loaded
if { [ file exists LDAS${API}.ini ] } {
   file attributes LDAS${API}.ini -permissions 0600
   source LDAS${API}.ini
 } else {   
   source [ file join $::LDAS lib ${API}API LDAS${API}.ini ]
 }   

package require generic 1.0
package require genericAPI

namespace eval $API {}

;#end

## ******************************************************** 
##
## Name: operator 
##
## Description:
## Handles requests at the wrapped API's operator socket.
## A request must be in the form of a list of 3 elements,
## where element 0 is the manager's key, element 2 is the
## job id, and element 3 is a code block to be evaluated.
##
## The command code block is assumed to begin with a call
## to metaOpts (see the genericAPI.tcl) which sets the
## argument list.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket (see sock.tcl).
##
## Comments:
## This is the only entry point exposed on the operator
## socket.
## See the asstMgr.tcl procedure ${name}::sockhandler
## for the format of the reply and the significance of the
## return values.
## The variable ::blank::errlvl may be modified by a called
## procedure generating an error condition.

proc operator { cid } {
     
     ;## initialise some variables.
     set result "done!"
     
     ;## get the command
     set cmd [ cmd::result $cid ]
     fileevent $cid readable {}
     
     ;## verify that the command is well formed,
     ;## or reply with error message to assistant
     ;## and hang up.
     
     if { [ catch {
        set cmdlen [ llength $cmd ]
     } err ] } {
        set result "bad list element in command: '$cmd'"
        addLogEntry $result 2
        ${::API}::reply $cid [ list 3 $result error! ]
        catch { close $cid }
        return {}
     }
     
     if { $cmdlen != 3 } {
        set result  "command list length != 3: '$cmd'"
        addLogEntry $result 2
        ${::API}::reply $cid [ list 3 $result error! ]
        catch { close $cid }
        return {}
     }

     ;## parse the command
     foreach { key jobid cmd } $cmd { break }
    
     regexp {^\{(.*)\}$} $cmd -> cmd
     regexp {^\"(.*)\"$} $cmd -> cmd
     
     if { [ string length $cmd ] < 6 } {
        set result  "truncated command received: '$cmd'"
        addLogEntry $result 2
        ${::API}::reply $cid [ list 3 $result error! ]
        catch { close $cid }
        return {}
     }
     ;## attach the job id to the metaOpts and "set opts" commands
     ;## don't do this since cmd already has jobid
     ;##regsub -- "\{" $cmd "\{ -jobid $jobid " cmd
     
     ;## verify the key and eval the command...
     ;## or return an error message and disconnect.
     ;## errlvl 3 causes the asstMgr to abort.
     set key  [ key::md5 $key ]
     set lock [ key::md5 $::MGRKEY ]
     if { [ string equal $lock $key ] } {
        set ::jobid IDLE
        debugPuts "begin processing $jobid"
        after 0 ${::API}::opthread [ list $jobid $cid $cmd ]
        update
     } else {
        set result "bad key received: $key"
        ;## reply to the assistant manager and hang up.
        ${::API}::reply $cid [ list 3 $result error! ]
        catch { close $cid }   
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: reattach
##
## Description:
## Allows asynchronously processing job to reattach to
## it's assistant manager.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc reattach { jobid cid args } {
     if { [ catch {
        trace vdelete ::$cid w "reattach $jobid $cid"
        set ::jobid $jobid
        set result [ set ::$cid ]
        unset ::$cid
        set ::jobid IDLE
        debugPuts "done processing $jobid"
        ;## reply to the assistant manager and hang up.
        ${::API}::reply $cid $result
        catch { close $cid }    
     } err ] } {
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${API}::opthread
##
## Description:
##
## Usage:
##
## Comments:
## NOTE: adapted for standalone run to instantiate
## the ::${jobid} array

proc ${API}::opthread { jobid cid args } {

     trace variable ::$cid w "reattach $jobid $cid"
	 ;## modified for meta cmds
	 set opts {
	 	-jobid	TEST1 
		-returnformat ilwd 
		-returnprotocol default
		-sqlquery	""
		-ingestdata default
	 }
	 set args [ lindex $args 0 ]
	 set cmd [ lindex $args 0 ]
	 set args [ lrange $args 1 end ]
	 set tmp [ expandOpts opts ] 
	 foreach { var value } $tmp {
	 	array set ::${jobid} [ list $var $value ]
	 }
	 puts "$cmd job $jobid options: [array get ::${jobid}]"
     if { [ catch { set ::$cid [ metadata::evalCmd $jobid $cmd ] } err ] } {
        set ::$cid $err
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: emergency 
##
## Description:
## Provides the functions required to handle requests of
## the blank API's emergency socket.  Communications with
## this socket are assumed to originate with the manager,
## and will be validated by comparing the first element
## in the received command list with the manager key.
## Commands must consist of a list with 2 elements, the
## first one must be the manager key, the second is Tcl
## code to be eval'd.
##
## Usage:
## Registered by the sock::cfg function when openListenSock
## calls it to configure the socket (see sock.tcl).
## If you expect to get anything back through the socket,
## make sure the command does a:
##
##    puts $cid "some such [ command ]"
##
## Comments:
## This is the only entry point exposed on the emergency
## socket.  It requires a key, but will try to evaluate
## anything that comes through with a valid key.

proc emergency { cid } {
     
     set jobid IDLE
    
     set key {}
     set cmd {}
	set msg {}
     
     ;## get the command
     set cmd [ cmd::result $cid ]
     fileevent $cid readable {}

     if { ! [ string length $cmd ] } {
        catch { close $cid }
        return {}
     }   

     regexp {([^ ]+) (.+)} $cmd -> key cmd
     	
     regexp {^\{(.*)\}$} $cmd -> cmd
     regexp {^\"(.*)\"$} $cmd -> cmd
     
     if { ! [ string length $cmd ] || ! [ string length $key ] } {
	   set msg "badly formed command recieved"
	}
	
     set key  [ key::md5 $key ]
     set lock [ key::md5 $::MGRKEY ]
     
     if { ! [ string equal $lock $key ] } {
	   set msg "bad key recieved: '$key'"
	}

     if { [ regexp -- $::BAD_WORDS $cmd ] } {
        set msg "forbidden command recieved: '$cmd'"
     }

     ;## either we have an error at this point or we can go!
	if { [ string length $msg ] } {
	   ${::API}::reply $cid "!!ERROR!!\n"
	   addLogEntry $msg 1
	} else {  
        ;## don't allow references to the ::MGRKEY
        regsub -all -- {\$::MGRKEY} $cmd $lock cmd
        set cmd [ subst -novariables -nocommands $cmd ]
	   if { [ catch { eval $cmd } err ] } {
           regsub -all -- \[\\\(\\\{\\\s\]$::MGRKEY\[\\\)\\\}\\\s\] $err " ::MGRKEY " err
		 set msg "$::API emergency error:\n$err"
           ${::API}::reply $cid $msg
           addLogEntry $msg 2
        } else {
	      regsub -all -- \[\\\(\\\{\\\s\]$::MGRKEY\[\\\)\\\}\\\s\] $cmd " ::MGRKEY " cmd
           set msg "executed: $cmd"
           ${::API}::reply $cid "$::API :emergency:$msg"
           if { [ regexp {(sysData|countChannels)} $cmd ] } {
              ;## time to disable the log hog!
              #debugPuts $msg 0
           } else {   
              addLogEntry $msg 1
           }   
        }
	}   
     
	catch { close $cid }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${API}::reply
##
## Description:
## Just a slightly smart catter, tries to avoid actually
## sending junk back.
##
## Usage:
##            ${API}::reply $cid $msg
##
## Comments:
## This channel should NEVER be flushed!!
## The errorlevel for debugPuts can be set to 2 to force
## writes to stderr for debugging of socket problems.

proc ${API}::reply { { cid "" } { args "" } } {
     
     if { [ uplevel info exists jobid ] } {
        if { ! [ string length [ uplevel set jobid ] ] } {
           set jobid IDLE
        } else {
           set jobid [ uplevel set jobid ]
        }
     } else {
        if { [ info exists ::jobid ] } {
           if { [ string length $::jobid ] } {
              set jobid $::jobid
           } else {
              set jobid IDLE
           }
        }
     }
     
     if { ! [ regexp {^sock[0-9 ]+$} $cid ] } {
        set msg "received: \"$args\"\n"
        append msg "First argument must be channel i.d."
        addLogEntry $msg 2
        }
     if { [ string length $args ] > 4 } {
        if { [ catch {
           puts $cid $args
           } err ] } {
           set msg  "Reply socket '$cid' not open."
           debugPuts $msg 1
           }
        }
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${API}::msg2mgr
##
## Description:
## Interact with the manager API through the manager's
## emergency socket.
##
## Parameters:
##
## Usage:
##
## Comments:
## Blocks while waiting for the manager to reply.
## This is an emergency procedure, so this should
## be acceptable.

proc ${API}::msg2mgr { msg } {
     
     set uniqid [ key::time ]
     set ::$uniqid {}
     
     if { [ catch {
        ;## try to rationalise the input
        regsub -all {([^\\])(\$)} $msg \\1\\\\$ msg
        regsub      $::MGRKEY     $msg {}       msg
        regexp      {^\{(.*)\}$}  $msg ->       msg
        regexp      {^\"(.*)\"$}  $msg ->       msg
        
        ;## connect and make the transaction
        set sid [ sock::open manager emergency ]
        puts        $sid "$::MGRKEY \"$msg\""
        flush       $sid
        fileevent   $sid readable "set ::$uniqid [ gets $sid ]"
        fconfigure  $sid -buffering line
        vwait ::$uniqid
        close $sid
       
        } err ] } {
        unset ::$uniqid
        return -code error "[ myName ]: $err"
        }
     set retval [ set ::$uniqid ]
     unset ::$uniqid
     return $retval   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ${API}::sHuTdOwN
##
## Description:
## Controlled shutdown function for LDAS API's.
## Closes open server sockets, closes log files,
## removes lock files, and exit's the process.
##
## Parameters:
##
## Usage:
##
## Comments:
## This is the LDAS API controlled exit point.
## If the api has an atExit function registered, then it
## will be evaluated before services are disconnected.
## put out shutdown message first so cntlmonAPI can send email

proc ${API}::sHuTdOwN { } {
     set seqpt {}
     set ::jobid SHUTDOWN
     
     set site $::env(HOST)
     regexp {\-(wa|la|dev)} $::DOMAINNAME -> site
     switch -exact $site {
        wa  { set site Hanford     }
        la  { set site Livingston  }
        dev { set site Caltech-Dev }
     }

     addLogEntry "Subject: $site $::API shutdown at [ gpsTime now ]; Body:  $::API shutting down NOW!" 3
     
     ;## execute an atExit function if it exists
     if { [ llength [ info procs ::${::API}::atExit ] ] } {
        catch { ::${::API}::atExit }
     }   
     if { [ catch {
        closeListenSock operator
     } err ] } {
        addLogEntry $err
     }   
     if { [ catch {
        closeListenSock emergency
     } err ] } {
        addLogEntry $err
     }
     if { [ catch {
        closeDataSock data
     } err ] } {
        addLogEntry $err
     } 
     closeLog
     log::lock
     exit
}     
## ******************************************************** 

## ******************************************************** 
##
## Name: bgerror 
##
## Description:
## Any Tcl script which has an event loop must have a
## bgerror procedure defined.  Tk defines a bgerror,
## and you should too!
##
## Usage:
## Called internally by Tcl.  See the bgerror man page.
##
## Comments:
## AddLogEntry with a level of 3 will result in e-mail
## being sent.

proc bgerror { msg } {
     set trace {}
     catch { set trace $::errorInfo }
     if { [ string length $trace ] } {
        set msg $trace
     }   
     addLogEntry $msg 3
}
## ******************************************************** 

;#barecode

;## initialise!
set ::jobid STARTUP

;## load the API specific code.
package require $API

roVar MGRKEY
checkMySetup
log::lock
archiveLog

;## API initialise (if you catch this, it will fail!)
${API}::init

;## and start services.
if { [ catch { 
   openListenSock operator
   openListenSock emergency
   openDataSock data
   } err ] } {                                                        
   addLogEntry $err 2                                                 
   }   

;## run the leak logger at an interval set in the rsc file.
leakLogger
bgLoop memflag memFlag $::LOGINTERVAL
set ::jobid {}

vwait enter-mainloop
