## ********************************************************
##
## Name: cmds.metadata
##
## Description:
## Examples of SQL input to metadataAPI
## presented as user command options
## used as input to test driver also
## ******************************************************** 
;#barecode

;##
;## 1. get all rows from frame
;##
-sqlquery {select * from frame} 

;## 
;## 2. get all rows from frameset
;##
-sqlquery {select * from frameset} 

;##
;## 3. get up to max allowed rows (e.g. 100 for now) from binaryinspiral 
;##
;##-sqlquery {select * from binaryinspiral fetch first 4 rows only} 

;##
;## 4. get all the rows from frameset (this is less than 100 so return all)
;##
-sqlquery {select * from frameset} 

;##
;## 5. get a count of number of frames
;## 1 row
-sqlquery {select count(*) from frame} 

;##
;## 6. get max field from frameset
;## 1 row
-sqlquery {select max(frameset_time) from frameset} 

;##
;## 7. get a unique id
;## 1 row
-sqlquery {values(generate_unique())} 

;##
;## 8. get days field from frameset
;##
-sqlquery {select days(frameset_time) from frameset} 

;##
;## include some comments, ambiguious sql
;## 9. SQL error returned
-sqlquery {select * from frame ##frame data} 

;##
;## 10. two queries, can only get from the first query, ambiguous 
;## gets error from assistant: unmatched open brace in list
;## errmsg returned: unmatched open brace in list
-sqlquery {select * from frame; select * from frameset} 

;##
;## 11. join 2 tables with common columns, e.g. frame and frameset
;## 
-sqlquery {select * from frame, frameset where frame.frameset_time = frameset.frameset_time} 

;##
;## below are sqls to get database schema data
;## table name must be uppercase
;##
;## 12. get all table names
-sqlquery {select tabname, tabschema from syscat.tables where tabschema != 'SYSCAT' and 
tabschema != 'SYSIBM' and tabschema != 'SYSSTAT'}

;## 13. get all column names for all tables
-sqlquery {select tabname, colname from syscat.columns where tabschema != 'SYSCAT' and 
tabschema != 'SYSIBM' and tabschema != 'SYSSTAT' order by tabname}

;## 14. get column names for a specific table
-sqlquery {select tabname, colname from syscat.columns where tabname = 'STATISTICS' }

;## 15. get all column names and type for all tables
-sqlquery {select tabname, colname, typename from syscat.columns where tabschema != 'SYSCAT' 
and tabschema != 'SYSIBM' and tabschema != 'SYSSTAT' order by tabname}

;## 16. get column names and type for a specific table, e.g. FRAMESET
-sqlquery {select tabname, colname, typename from syscat.columns 
where tabname = 'PROCESS' }

;## 17. get primary keys for specific table, e.g. FRAMESET
-sqlquery {select  t.tabname, k.colname from syscat.tabconst t,syscat.keycoluse k 
where t.tabname = k.tabname and t.constname = k.constname and t.tabname = 'FRAMESET' 
and t.type = 'P'}

;## 18. get foreign keys for specific table, e.g. FRAMESET
-sqlquery {select  t.tabname, k.colname from syscat.tabconst t,syscat.keycoluse k 
where t.tabname = k.tabname and t.constname = k.constname and t.tabname = 'FRAMESET' 
and t.type = 'F'}

;##
;## Bruce Allen's sql on binaryinspiral
;## takes several minutes to complete and return one line
;##
-sqlquery {WITH ZZZ AS (select snr from multi_inspiral where mass1 < 952802000) 
select * from ZZZ where snr = (select max(snr) from ZZZ)}
;#end
