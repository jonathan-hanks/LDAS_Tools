## ********************************************************
##
## Name: examples2.sql 
##
## Description:
## verifies putMetaData insertions
## presented as user command options
## used as input to test driver also
## ******************************************************** 
;#barecode

;##
;## get all rows from each table
;##
-sqlquery {select * from process fetch first 2 rows only} 

;##
-sqlquery {select * from process_params order by program fetch first 2 rows only} 

;##
-sqlquery {select * from frameset_chanlist fetch first 2 rows only} 
 
;##
-sqlquery {select * from frameset_writer fetch first 2 rows only} 

;## 
-sqlquery {select * from frameset fetch first 2 rows only} 

;## 
-sqlquery {select * from frameset_loc fetch first 2 rows only} 

;##
-sqlquery {select * from segment_definer fetch first 2 rows only} 

;##
-sqlquery {select * from segment fetch first 2 rows only} 

;##
-sqlquery {select * from summ_value fetch first 2 rows only} 

;##
-sqlquery {select * from summ_statistics fetch first 2 rows only} 

;## 
;##-sqlquery {select * from summ_spectrum fetch first 2 rows only} 

;## 
-sqlquery {select * from summ_comment fetch first 2 rows only} 

;##
-sqlquery {select * from filter fetch first 2 rows only} 

;## 
-sqlquery {select * from filter_params fetch first 2 rows only} 

;##
;##-sqlquery {select * from gds_trigger fetch first 2 rows only} 

;## 
-sqlquery {select * from sngl_inspiral fetch first 2 rows only} 

;##
-sqlquery {select * from sngl_burst fetch first 2 rows only} 

;## 
-sqlquery {select * from sngl_ringdown fetch first 2 rows only}

;##
-sqlquery {select * from sngl_unmodeled fetch first 2 rows only} 

;## 
-sqlquery {select * from sngl_dperiodic fetch first 2 rows only} 

;##
-sqlquery {select * from sngl_datasource fetch first 2 rows only} 

;## 
;##-sqlquery {select * from sngl_transdata fetch first 2 rows only} 

;## 
-sqlquery {select * from sngl_unmodeled_v fetch first 2 rows only} 

;##
-sqlquery {select * from multi_inspiral fetch first 2 rows only} 

;## 
-sqlquery {select * from multi_burst fetch first 2 rows only}
  
;## 
-sqlquery {select * from coinc_sngl fetch first 2 rows only}  

;#end
