## ********************************************************
##
## Name: examples_new.sql
##
## Description:
## Examples of SQL input to metadataAPI
## presented as user command options
## used as input to test driver also
## ******************************************************** 
;#barecode

;##
;## 1. get list of frameset_group
;##
-sqlquery {select distinct frameset_group from frameset_writer 
order by frameset_group}

;## 
;## 2. get framesets within a given time interval, refresh
;##
-sqlquery {select frameset_group, start_time, end_time, name
from frameset where (start_time >= 638299081  and end_time <= 638299142)
order by start_time, frameset_group} 

;##
;## 3. select one row, refresh
;##
-sqlquery {SELECT * from process where username='U_952796377'}
;##
;## 4. get list of framesets within a given time interval, refresh 
;##
-sqlquery {select fs.frameset_group, fs.start_time, fs.end_time, fs.name
from frameset fs join frameset_chanlist ch 
on (fs.creator_db = ch.creator_db and fs.chanlist_id = ch.chanlist_id)
where (fs.start_time >= 638299081 and fs.end_time <= 638299142 )
order by fs.start_time, fs.frameset_group} 

;##
;## 5. get summary statistics period for a channel whose rms is >=
;## 1 row, test angle bracket conversion in comment of xml output
;## refresh
-sqlquery {select start_time, end_time, mean, rms 
from summ_statistics 
where (channel = 'H1.channel.954264857' and rms >= 954264857 )
order by start_time} 

;##
;## 6. query with no rows, has empty container for char_u unique Id
;## 0 row, expect empty table in xml, not exceptions
-sqlquery {select * from frameset where frameset_group = 'non-existent'} 

;##
;## below are sqls to get database schema data
;## table name must be uppercase
;##
;## 7. get all table names
-sqlquery {select tabname, tabschema from syscat.tables where tabschema != 'SYSCAT' and 
tabschema != 'SYSIBM' and tabschema != 'SYSSTAT'}

;## 8. get all column names for all tables
-sqlquery {select tabname, colname from syscat.columns where tabschema != 'SYSCAT' and 
tabschema != 'SYSIBM' and tabschema != 'SYSSTAT' order by tabname}

;## 9. get column names for a specific table
-sqlquery {select tabname, colname from syscat.columns where tabname = 'SNGL_INSPIRAL'}

;## 10. get all column names and type for all tables
-sqlquery {select tabname, colname, typename from syscat.columns where tabschema != 'SYSCAT' 
and tabschema != 'SYSIBM' and tabschema != 'SYSSTAT' order by tabname}

;## 11. get column names and type for a specific table, e.g. FRAME
-sqlquery {select tabname, colname, typename from syscat.columns 
where tabname = 'PROCESS'}

;## 12. get primary keys for specific table, e.g. segment
-sqlquery {select  t.tabname, k.colname from syscat.tabconst t,syscat.keycoluse k 
where t.tabname = k.tabname and t.constname = k.constname and t.tabname = 'SEGMENT' 
and t.type = 'P'}

;## 13. get foreign keys for specific table, e.g. segment
-sqlquery {select  t.tabname, k.colname from syscat.tabconst t,syscat.keycoluse k 
where t.tabname = k.tabname and t.constname = k.constname and t.tabname = 'SEGMENT' 
and t.type = 'F'}

;## 14. Ordinary query:
-sqlquery {SELECT tabname FROM syscat.tables WHERE tabname LIKE 'FUNC%'}

;## 15. Query which does not return any rows:
-sqlquery {SELECT tabname,tabschema,definer FROM syscat.tables WHERE definer='zzzz'}

;## 16. Query which matches a single row containing a unique ID:
-sqlquery {SELECT * FROM process WHERE start_time = (SELECT MIN(start_time) FROM process)}

;## 17. Query with angle brackets:
-sqlquery {SELECT tabname,tabschema,definer FROM syscat.tables WHERE definer<>'SYSIBM'}

;## 18. Query which tries to return a large table:
-sqlquery {SELECT * FROM syscat.columns}

;## 19. Nonexistent table, return exceptions to manager 
-sqlquery {SELECT * FROM zzzyyy}

;## 20. Bad SQL: should result in db error with quotes escaped
-sqlquery {SELET * FROM frameset_chanlist}

;## 21. select a single row which may have blank columns, refresh 
-sqlquery {SELECT * from coinc_sngl where coinc_id = x'20000328174832057419000000'}

;## 22. select a single row from table which has blob data, refresh
-sqlquery {SELECT * from sngl_transdata where event_id =x'20000328172620903311000000'}

;## 23. select results in an empty container need a char_u element
-sqlquery {SELECT * from frameset where frameset_group='mary'}

;## 24. make sure it is ok to select gds_trigger, uniqueId
-sqlquery {SELECT * from gds_trigger for read only}

;## 25. if read only or fetch only is in, dont tag read-only clause
-sqlquery {SELECT * from summ_comment for fetch only}

;## 26. attempt to modify the database
-sqlquery {delete from filter where filter_name like 'FFT_%'}

;## 27. select table which has blob but result in no rows testing, use ilwd:null
-sqlquery {SELECT * from sngl_transdata where event_id =x'12345678901234567890123456'}

;## 28. example of complex query that does not begin with select, refresh
-sqlquery {WITH ZZZ AS (select snr from multi_inspiral where mass1 < 95426400 ) 
select * from ZZZ where snr = (select max(snr) from ZZZ)}

;## 29. Need an example to test char with numbers for ligolw, has clob ->lstring
-sqlquery {select * from frameset_chanlist fetch first 5 rows only}
;#end
