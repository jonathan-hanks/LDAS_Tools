## ******************************************************** 
##  metadata_testOper.tcl Version 1.0
##
##  this script is read in by metadata_test.tcl to
##  do user cmd testing for getMetaData 
##  test sending cmds to operator socket concurrently
## ******************************************************** 

;#barecode

set index 0
set jobcnt 0

;## test insertion of all table data

;## default directory for data files
if  { [ info exist env(ILWDDIR) ] } {
    set basedir $env(ILWDDIR)
} else {
    set basedir /ldas_usr/ldas/test/database
}

;## test both ilwd and xml input formats 

if  { ! [ regexp {^/} $::options(-data)  ] } {
    set fname "$basedir/$::options(-data)"
} else {
    set fname $::options(-data)
}
puts "ilwd file is $fname"
#set email "\{\}"
#puts "email=$email"
set email $::options(-email)
puts "email $email"

set key foo
if  { [ string length $::options(-database) ] } {
    set dbcmd "-database $::options(-database)"
} else {
    set dbcmd ""
}    

;## how to ingest from http site with address
;## putMetaData -database cit_test 
;##    -ingestdata http://18.120.0.66:38925/file1_1.xml

;## test various protocols also
;##set delsql {delete from binaryinspiral where ifo_site between 'ifo_site_0' and 'ifo_site_9'}
if	{ [ string compare "exception" $::options(-data) ] } {
    
        set ingestdata "file:$fname"
		puts "ingesting $ingestdata"
		;##set ingestdata "ftp://ldas.ligo-wa.caltech.edu/pub/incoming/try1.xml"
		;##set ingestdata "file:/ldas_outgoing/data/mlei.xml"
		;##set ingestdata "http://www.ldas.ligo-wa.caltech.edu/ldas_outgoing/jobs/dmt/$fname"
		    
        ;## user cmd or standalone cmd 
        if  {  ! [ string compare $::options(-type) user ] } {
            set cmd "ldasJob \{-name $user -password $password -email $email\} \{putMetaData -ingestdata $ingestdata\
            $dbcmd\}"                    
        } else {
             set cmd "foo NORMAL$index \{putMetaData -jobid NORMAL$index -ingestdata $ingestdata \
			 $dbcmd\}" 
        }  
        ;## set cmd "putMetaData -jobid NORMAL$index -ingestdata $ingestdata -returnprotocol $returnprotocol"
        puts "file submitting job $index, cmd=$cmd"
        sendCmd $cmd $index
} else {
	set fname "$basedir/nofile.$ext"
    set ingestdata "file:$fname"
    if  {  ! [ string compare $::options(-type) "user" ] } {
	    set cmd "ldasJob \{-name $user -password $password -email $email\} \
		 \{putMetaData -ingestdata $ingestdata\ -returnprotocol $returnprotocol $dbcmd\}"
    } else {
        set cmd "putMetaData -jobid NORMAL$index -ingestdata $ingestdata $dbcmd"
    }
	sendCmd $cmd $index
}

;## test metadataAPI not up, should give connection error at manager
;##
;## 3 {can't set "::__TEST151": dataSend: sock::openDataClient(metadata): 
;## sock::openDataClient:connectDataClient(_2f18e8_os_tcp_socket_p metaserver 12009): 
;## connectDataClient: connectDataSocket(_2f18e8_os_tcp_socket_p metaserver 12009): 
;## connect_failure: 10.3.0.2 {os err:146:Connection refused} 12009}

;## for bad command formats detected in macro, return response to user, not reported at terminal
;## ldasJob {-name ldas -password rumplestiltskin -email mlei@ligo.caltech.edu} {getMetaData -returnformat LIGO_LW -returnprotocol bad:/nodir/nopath select * from process}
;## {Manager operator: mgr::validateopts(getMetaData -returnformat LIGO_LW -returnprotocol bad:/nodir/nopath select * from process):
;## mgr::validateopts: User option: "from"
;## is not a valid option to getMetaData.
;## Please see the documentation for
;## getMetaData before resubmitting your
;## request.}
