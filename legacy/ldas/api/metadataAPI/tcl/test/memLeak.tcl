#! /ldcg/bin/tclsh

set fd [open "temp1" r]
set count 0
array set previous { heap 0 }
array set previous { stack 0 }
array set previous { txn 0 }
array set previous { total 0 }
set lines [ read $fd ]
set lines [ split $lines \n]
set lineno 0
set getMetaData 0
set putMetaData 0

foreach line $lines {
    incr lineno 1    
    if  { [ regexp {total \{([0-9]+) Kb\} heap ([0-9]+)K stack ([0-9]+)K} \
        $line match total heap stack ] } {
        ;## puts "total=$total, heap=$heap, stack = $stack"
        incr count 1 

        if  { $previous(total) != $total } {
            set heapdelta  [ expr $heap - $previous(heap) ]
            set stackdelta [ expr $stack - $previous(stack) ] 
            set totaldelta [ expr $total - $previous(total) ]
            puts "After $count cmds, increased $totaldelta KB ( heap increased $heapdelta KB, stack increased $stackdelta KB) "
            array set previous [ list heap $heap ]
            array set previous [ list stack $stack ]
            array set previous [ list txn $count ]
            array set previous [ list total $total ]
        }
        if  { [ regexp {putMetaData|getMetaData} $line match ] } {
            set cmd [ lindex $match 0 ]
            incr $cmd 1
        }
    }
}
set cmdtot [ expr $putMetaData + $getMetaData ]
if  { $count != $cmdtot } {
    puts "Error: $count does not match expected $cmdtot"
}
puts "$lineno lines read, $count cmds processed, \
putMetaData=$putMetaData, getMetaData=$getMetaData"
close $fd            
