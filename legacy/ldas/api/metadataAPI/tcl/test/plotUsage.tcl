#! /ldcg/bin/wish

;## unit test of log functions
source ./LDASapi.rsc
set MGRKEY "foo"
set PUBDIR /ldas_outgoing/jobs

set LDASLOG /ldas_outgoing/logs
set LDASARC [ file join $LDASLOG archive ]

set auto_path "/ldas/lib $auto_path"

set API cntlmon
package require generic
package require plot
#source log.tcl
#source plot.tcl

set timelimits [ lindex $argv 1 ]
set rx walltime 

proc parseLines { result } { 
    set seqpt ""
    if  { [ catch {
        set ::gpstimes {}
        set ::tables {}
        set lines [ split $result \n ]
        set ::queries 0
        set ::insertions 0
	    #puts stderr "parseLines found [ llength $lines ] lines"
        foreach line $lines { 
            ;## queries with time
			set seqpt queries
            if  { [ regexp {getMetaData.+ (\d+) queries.} $line match ::queries ] } {
                ;##puts "queries=$::queries"
                if  { [ regexp {alt=\"([^']*)\">\s+<b>(\d+)</b>} $line match alt gpstime ] } { 
                    ;##puts "alt=$alt,gpstime=$gpstime"
                    set ::query($::queries) [ list $gpstime $alt ]
                }       
            } else {  
            ;## insertions with time 
			set seqpt insertions             
	        if	{ [ regexp {CPUtime=([\d.]+) secs, walltime=([\d.]+) secs; inserted (\d+) rows into table ([a-z_]+) at a rate=([\d.]+) rows/sec} \
		        $line match cputime walltime dims table rate ] } {
		        #puts stderr "table=$table,cputime=$cputime,walltime=$walltime,dims=$dims,rate=$rate"
                incr ::insertions 1
                if  { [ lsearch -exact $::tables $table ] == -1 } {
                    lappend ::tables $table
		        }
		        ;##upvar #0 $gpstime data
		        if	{ [ regexp {(\d+) rows inserted into database ([^\s.]+)} $line match ::rows dbname ] } {
			        ;##puts stderr "dbname=$dbname, rows inserted = $::rows"
                    if  { [ regexp {alt=['\"]+([^']*)[,\"]+>\s+<b>(\d+)</b>} $line match alt gpstime ] } { 
                        if  { [ lsearch -exact $::gpstimes $gpstime ] == -1 } {
                            lappend ::gpstimes $gpstime
                            set ::insertion($::rows) [ list $gpstime $alt ]
                            #puts "gpstimes=$::gpstimes"
		                }         
                        upvar #0 $gpstime data
			            if	{ ! [ info exist data($table) ] } {
				            set data($table) [list]
			            }       
                        lappend data($table) $dims $walltime $cputime $rate $alt
                    } 
                    ;##puts "gpstime=$gpstime,[ set data($table) ]"
                    ;## puts stderr "alt=$alt, gpstime=$gpstime"      
                }
                continue     
            }
           } 
        }
	} err ] } {
		puts "error: $seqpt, $err"
	}
	
}

proc plotUsage {parent} {

	if  { [ array size ::query ] > 0 || [ array size ::insertion ] > 0 } { 		
	    set canvasw [ plot::scrolledCanvas $parent.query -width 800 -height 500 \
                           -background PapayaWhip \
                           -scrollregion { -500 -500 1000 1000 }  ]
	puts "queries [ array get ::query ]"					   
	set fd [ open "plotquery" w ]
    
    # adjust point size
    set numInserts [ array size ::insertion ] 
    set numQueries [ array size ::queries ]
    
    if  { $numInserts > 100 } {
        set pointsz 0.5
    } else {
        set pointsz 1.0
    }
    # 		set logscale y 10
	puts $fd "\ 
        \#load 'test.plot'\n\
        set xlabel \"gpstime\"\n\
        set ylabel \"# queries or #rows inserted\"\n\
        set format x \"%0.0f\"\n\
        set format y \"%0.0f\"\n\
        set title \"LDAS Metadata Usage\"\n\
        set grid 15\n\
        set function style points\n\
        set data style points\n\
        set autoscale xy\n\
        set key below\n\
        set pointsize $pointsz"        

    if  { $numInserts } {
        puts $fd {plot '-' title "insertions" with points 1}
	    foreach einsert [ lsort -integer [ array names ::insertion ] ] {
		    set gpstime [ lindex $::insertion($einsert) 0 ]
		    puts $fd "$gpstime $einsert"
	    }
	    puts $fd e
    }
	puts "queries [ array size ::query ]"
    if  { $numQueries > 100 } {
        set pointsz 0.5
    } else {
        set pointsz 1.0
    }
    if  { [  array size ::query ] > 0 } {
        puts $fd "set pointsize $pointsz"
        puts $fd {plot '-' title "queries" with points 3}
	    foreach equery [ lsort -integer [array names ::query]] {
		    set gpstime [ lindex $::query($equery) 0 ]
		    puts $fd "$gpstime $equery"
	    }
	    puts $fd e
    }
	close $fd 
	plot::plotData $canvasw {
		load 'plotquery'
	}
	}
}

#set result [ grepLogs 661297894-661299513 $rx 5000 {metadata} ]
# hanford 
set range 661296850-661334194
set result [ grepLogs $range $rx 10000 {metadata} ]

parseLines $result
frame .f
plotUsage .f
pack .f
