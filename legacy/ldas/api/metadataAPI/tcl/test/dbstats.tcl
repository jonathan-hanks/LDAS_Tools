#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

set ::Rows {1 10 100 1000}
#set ::Rows {1 10 100}

set ::Tables {
    calib_info
    coinc_sngl
    exttrig_search
    filter
    filter_params
    frameset
    frameset_chanlist
    frameset_loc
    frameset_writer
    gds_trigger
    multi_burst
    multi_inspiral
    process
    process_params
    runlist
    search_summary
    search_summvars
    segment
    segment_definer
    sim_inst
    sim_inst_params
    sim_type
    sim_type_params
    sngl_block
    sngl_burst
    sngl_datasource
    sngl_dperiodic
    sngl_inspiral
    sngl_mime
    sngl_ringdown
    sngl_transdata
    sngl_unmodeled
    sngl_unmodeled_v
    summ_comment
    summ_csd
    summ_mime
    summ_spectrum
    summ_statistics
    summ_value
    waveburst
    waveburst_mime
}

set dbtables "/ldas/doc/db2/doc/text/dbtables.tcl"
if {[file exists $dbtables]} {
    source $dbtables
}

proc main {} {
    set outfile {}

    ;## Get command line arguments
    set idx [lsearch -exact $::argv "-"]
    if {$idx >= 0} {
        set outfile stdout
        set ::argv [lreplace $::argv $idx $idx]
    }
    set ::argc [llength $::argv]

    if {$::argc <= 1} {
        puts stderr "Error: Missing filename for comparison"
        return
    }

    if {$::argc > 2} {
        puts stderr "Error: Too many arguments"
        return
    }

    set newfile [lindex $::argv 0]
    set oldfile [lindex $::argv 1]
    if {[file isdirectory [lindex $::argv end]]} {
        set oldfile [file join [lindex $::argv end] $newfile]
    }

    if {![string match stdout $outfile]} {
        set outfile [file rootname $newfile].stats
    }

    calculate $newfile $oldfile
    print $newfile $oldfile $outfile

    return
}


proc calculate {new old} {
    if {![file exists $old]} {
        puts stderr "Error: ${old}: No such file."
        return -code continue
    }
    if {![file exists $new]} {
        puts stderr "Error: ${new}: No such file."
        return -code continue
    }
    if {[file tail $new] != [file tail $old]} {
        ;## Warning
        puts stderr "Warning: $old and $new do not have the same name."
    }

    ;## Collect data
    foreach age {new old} {
        set fd [open [set $age] r]
        set data [split [read $fd] "\n"]
        close $fd

        foreach line $data {
            if {[regexp {rate|wall|cpu} $line type]} {
                continue
            }

            if {![string length $line]} {continue}

            set data [split $line "\t"]
            set table [lindex $data 0]
            set idx 1
            foreach row $::Rows {
                set val [lindex $data $idx]
                #if {$val == 0} {continue}
                lappend raw($type,$age,$table) $val
                lappend raw($type,$age,$row) $val
                lappend raw($type,$age,all) $val
                incr idx
            }
        }
    }

    array unset ::mean
    array unset ::delta

    ;## calculate mean
    foreach type {rate wall cpu} {
        foreach age {new old} {
            foreach table $::Tables {
                if {![info exists raw($type,$age,$table)]} {
                    set ::mean($type,$age,$table) 0.0
                } else {
                    set ::mean($type,$age,$table) [mean $raw($type,$age,$table)]
                }
            }

            foreach row $::Rows {
                if {![info exists raw($type,$age,$row)]} {
                    set ::mean($type,$age,$row) 0.0
                } else {
                    set ::mean($type,$age,$row) [mean $raw($type,$age,$row)]
                }
            }

            set ::mean($type,$age,all) [mean $raw($type,$age,all)]
        }
    }

    ;## calculate differences
    foreach type {rate wall cpu} {
        foreach table $::Tables {
            set newmean $::mean($type,new,$table)
            set oldmean $::mean($type,old,$table)

            if {$newmean == 0 || $oldmean == 0} {
                set ::delta($type,$table) NA
                continue
            }

            if {[string equal "rate" $type]} {
                set ::delta($type,$table) [expr {($newmean - $oldmean) / $oldmean}]
            } else {
                set ::delta($type,$table) [expr {($oldmean - $newmean) / $newmean}]
            }
        }

        foreach row $::Rows {
            set newmean $::mean($type,new,$row)
            set oldmean $::mean($type,old,$row)

            if {$newmean == 0 || $oldmean == 0} {
                set ::delta($type,$row) NA
                continue
            }

            if {[string equal "rate" $type]} {
                set ::delta($type,$row) [expr {($newmean - $oldmean) / $oldmean}]
            } else {
                set ::delta($type,$row) [expr {($oldmean - $newmean) / $newmean}]
            }
        }

        set newmean $::mean($type,new,all)
        set oldmean $::mean($type,old,all)

        if {$newmean == 0 || $oldmean == 0} {
            set ::delta($type,all) NA
            continue
        }

        if {[string equal "rate" $type]} {
            set ::delta($type,all) [expr {($newmean - $oldmean) / $oldmean}]
        } else {
            set ::delta($type,all) [expr {($oldmean - $newmean) / $newmean}]
        }
    }

    return
}

proc print {new old out} {
    if {[string match stdout $out]} {
        set fout $out
    } else {
        set fout [open $out w]
    }

    puts $fout "database insertion test statistics"
    puts $fout [clock format [clock seconds] -format {%B %d, %Y}]
    #puts $fout "New file: [clock format [file mtime $new] -format {%B %d, %Y}]"
    #puts $fout "Old file: [clock format [file mtime $old] -format {%B %d, %Y}]"
    puts $fout "Old file: [file tail $old]"
    puts $fout "New file: [file tail $new]"
    puts $fout ""

    array set text {
        cpu "CPU Time (s)"
        wall "Wall Time (s)"
        rate "Rate (row/s)"
    }

    puts $fout [format {%-23s%-12s%-14s%-12s} " " "New Avg" "Old Avg" Analysis]
    foreach type {rate wall cpu} {
        if {[string equal NA $::delta($type,all)]} {
            set analysis "    N/A"
        } else {
            set analysis [format {%6.2f%% %s} \
                [expr {100 * abs($::delta($type,all))}] \
                [expr {$::delta($type,all) >= 0?"Faster":"Slower"}]]
        }
        puts $fout [format {%-23s%-12.6f%-14.6f%s} \
            "Overall $text($type)" \
            $::mean($type,new,all) \
            $::mean($type,old,all) \
            $analysis]
    }

    foreach type {rate wall cpu} {
        puts $fout ""
        puts $fout [format {%-23s%-12s%-14s%-12s} "$text($type) x Table" "New Avg" "Old Avg" Analysis]
        foreach table $::Tables {
            if {[string equal NA $::delta($type,$table)]} {
                set analysis "    N/A"
            } else {
                set analysis [format {%6.2f%% %s} \
                    [expr {100 * abs($::delta($type,$table))}] \
                    [expr {$::delta($type,$table) >= 0?"Faster":"Slower"}]]
            }
            puts $fout [format {%-23s%-12.6f%-14.6f%s} \
                $table \
                $::mean($type,new,$table) \
                $::mean($type,old,$table) \
                $analysis]
        }
    }

    foreach type {rate wall cpu} {
        puts $fout ""
        puts $fout [format {%-23s%-12s%-14s%-12s} "$text($type) x Row" "New Avg" "Old Avg" Analysis]
        foreach row $::Rows {
            if {[string equal NA $::delta($type,$row)]} {
                set analysis "    N/A"
            } else {
                set analysis [format {%6.2f%% %s} \
                    [expr {100 * abs($::delta($type,$row))}] \
                    [expr {$::delta($type,$row) >= 0?"Faster":"Slower"}]]
            }
            puts $fout [format {%-23s%-12.6f%-14.6f%s} \
                $row \
                $::mean($type,new,$row) \
                $::mean($type,old,$row) \
                $analysis]
        }
    }

    catch {close $fout}
    return
}

proc mean {samples} {
    set mean 0.0
    set N [ llength $samples ]

    if { $N > 0 } {
        set mean [ expr ([ join $samples + ]) / $N. ]
    }

    return $mean
}

main

