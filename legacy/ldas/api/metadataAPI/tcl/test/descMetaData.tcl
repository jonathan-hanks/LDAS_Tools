## ******************************************************** 
##  metadata_testOper.tcl Version 1.0
##
##  test sending cmds to operator socket concurrently
##  
##  run this by entering: tclsh metadata_testOper.tcl
## 
## ******************************************************** 

;#barecode

proc sendDescCmd {} {
    uplevel #0 {
        set cmd [ array get ::opts ]
        set returnprotocol "file:descMeta${index}"
        ;## user or standalone cmd 
        if  {  ! [ string compare $::options(-type) "user" ] } {
            set cmd "ldasJob \{-name $user -password $password -email $email\}\
			 \{descMetaData -returnformat $returnformat -returnprotocol $returnprotocol\
			  $cmd $dbcmd\}"
        } else {
            set cmd "foo TEST$index \{descMetaData -jobid TEST${index} \
			-returnformat ilwd -returnprotocol $returnprotocol $cmd $dbcmd\}"
        }
        puts "file submitting job $index, cmd=$cmd"
        sendCmd $cmd $index
        incr index 1
    }
}

set index 0
set jobcnt 0
set userid $tcl_platform(user)

if  { [ string length $::options(-database) ] } {
    set dbcmd "-database $::options(-database)"
} else {
    set dbcmd ""
}    

set returnprotocol "mailto:mlei@ligo.caltech.edu"
puts "returnprotocol=$returnprotocol"

;##set returnformat "ilwd"
set returnformat "LIGO_LW"

## get column names from all the tables
## max rows 
array set ::opts {
    -table  "all"
    -column "none"
    -type   "none"
    -key    "none"
}
puts "job$index: get colnames from all tables"
sendDescCmd


## get specific column and table
## get 1 row, silly query
## <lstring size='5'>FRAMESET</lstring>
## <lstring size='8'>FRAMESET_</lstring>
array set ::opts {
    -table  "frameset"
    -column "frameset_group"
    -type   "none"
    -key    "none"
}
puts "job$index: get colname from specific table"
sendDescCmd


## get primary keys
array set ::opts {
    -table  "segment_definer"
    -column "none"
    -type   "none"
    -key    "primary"
}
puts "job$index: get primary key from segment_definer table"
sendDescCmd

## get foreign keys
array set ::opts {
    -table  "segment"
    -column "none"
    -type   "none"
    -key    "foreign"
}
puts "job$index: get foreign key from segment table"
sendDescCmd

## get foreign keys but table does not have it
## 0 rows retrieved 
array set ::opts {
    -table  "process"
    -column "none"
    -type   "none"
    -key    "foreign"
}
puts "job$index: get foreign key from process table (none)"
sendDescCmd

## get data types
array set ::opts {
    -table  "filter"
    -column "none"
    -type   "all"
    -key    "none"
}
puts "job$index: get data attributes from filter table"
sendDescCmd

## get data types, results truncated 
array set ::opts {
    -table  "all"
    -column "none"
    -type   "all"
    -key    "none"
}
puts "job$index: get data attributes from all tables"
sendDescCmd


## errors testing in user_descMetaData.tcl
## done by descMetaData.meta
## type and key together
## debug58::valid: -type,-column must be none for -key
array set ::opts {
    -table  "sngl_ringdown"
    -column "none"
    -type   "all"
    -key    "primary"
}
puts "job$index: type and key together"
sendDescCmd

## key and column together
## expected: debug59::valid: -type,-column must be none for -key
array set ::opts {
    -table  "sngl_ringdown"
    -column "all"
    -type   "none"
    -key    "primary"
}
puts "job$index: column and key together"
sendDescCmd

## no options
## debug60::valid: no function specified
array set ::opts {
    -table  "none"
    -column "none"
    -type   "none"
    -key    "none"
}
puts "job$index: invalid options"
sendDescCmd

## invalid key
## debug61::valid: -key options are primary,foreign or none
array set ::opts {
    -table  "xxxx"
    -column "none"
    -type   "none"
    -key    "garbage"
}
puts "job$index: invalid key"
sendDescCmd

## invalid type
## debug62::valid: -type must be all or none
array set ::opts {
    -table  "xxxx"
    -column "none"
    -type   "type"
    -key    "none"
}
puts "job$index: invalid type"

sendDescCmd

