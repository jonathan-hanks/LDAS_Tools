## ******************************************************** 
##  this script is read in by metadata_test.tcl to
##  do user cmd testing for getMetaData 
##  test sending cmds to operator socket concurrently
##
## ******************************************************** 

;#barecode

if  { [ info exist env(SQLDIR) ] } {
    set basedir $env(SQLDIR)
} else {
    set basedir [ pwd ]
}
# sql arg format :
# file:fname
# text:query

set sql $::options(-sql)
set email $::options(-email)
puts "email $email"

regexp {([^:]+):(.+)} $sql -> protocol data
switch  $protocol {
    file {
        if  { ! [ regexp {^/} $data  ] } {
            set fname "$basedir/$data"
        }
        set fd [ open $fname "r" ]
        set cmddata [ read $fd ]
        close $fd
        set cmddata [ split $cmddata \n ]
        }
    text { set cmddata [ list $data ] }
}

if  { [ string length $::options(-database) ] } {
    set dbcmd "-database $::options(-database)"
} else {
    set dbcmd ""
}     
set index  0
set jobcnt 0

set querylist {}
set cmdlist {}
foreach cmd $cmddata {
    if  {  [ regexp {^[;#]} $cmd ] ||
           [ regexp "^\[ \t]*$" $cmd   ] } {
        set query [ join $querylist ]
        if  { [ llength $querylist ] > 0 } {
            lappend cmdlist $query 
            set querylist [ list ]
        }
        continue
    } else {
        lappend querylist $cmd
    }
}

if  { [ llength $querylist ] > 0 } {
    lappend cmdlist [ join $querylist ]
}

set index 1
set returnformat $::options(-ext)
set returnformat "\{ $returnformat \}"
;## can repeat many times
for { set numtimes 0 } { $numtimes < $::options(-times) } { incr numtimes 1 } {
    set index 1
    foreach cmd $cmdlist {
        set cmd [ string trim $cmd ]
       	set returnprotocol "file:getMeta${index}"
        if  {  ! [ string compare $::options(-type) "user" ] } {
            set cmd "ldasJob \{-name $user -password $password -email $email\}\
			 \{getMetaData -outputformat $returnformat -returnprotocol $returnprotocol \
			 $cmd $dbcmd\}"
        } else {
            set cmd "foo TEST$index \{getMetaData -jobid TEST${index} \
			-outputformat $returnformat -returnprotocol $returnprotocol \
			$cmd $dbcmd\}"
        }
        sendCmd $cmd $index
		after 1000
        if  { $::index >= $::options(-data) } {
            puts "exiting $index"
            break
        }
        incr index 1
    }
}

;## expect an error like 
;##
;##	{Manager operator: mgr::validateopts(getMetaData -returnformat LIGO_LW -returnprotocol bad:/nodir/nopath select * from process):
;##	mgr::validateopts: User option: "from"
;##	is not a valid option to getMetaData.
;##	Please see the documentation for
;##	getMetaData before resubmitting your
;##	request.}

if	{ ! [ string compare "all" $::options(-data) ] } {
	set cmd "select * from process"
    if  {  ! [ string compare $::options(-type) user ] } {
	    set cmd "ldasJob \{-name $user -password $password -email $email\}\
		 \{getMetaData -returnformat bad_format -returnprotocol $returnprotocol $cmd $dbcmd\}"	
	} 
    sendCmd $cmd $index
	after 1000

;## send invalid returnprotocol
	set cmd "select * from process"
    if  {  ! [ string compare $::options(-type) user ] } {
	    set cmd "ldasJob \{-name $user -password $password -email $email\}\
		 \{getMetaData -returnformat $returnformat -returnprotocol bad:/nodir/nopath $cmd $dbcmd\}"	
	}
    sendCmd $cmd $index
	after 1000
}
	
#    set index 1
#    set returnprotocol "mailto:$tcl_platform(user)@ligo.caltech.edu"
#    foreach cmd $cmdlist {
#        set cmd [ string trim $cmd ]
#        set cmd "ldasJob \{-name $user -password $password -email $email\} \{getMetaData -returnformat $returnformat -returnprotocol $returnprotocol $cmd\}"
#        sendCmd $cmd $index
#        if  { $::index >= $::data } {
#            puts "exiting $index"
#            break
#        }
#        incr index 1
#    }
