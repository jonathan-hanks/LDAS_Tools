## ********************************************************
##
## Name: metaCallback.tcl
##
## Comments:
## global Callback functions for macros
##
## see various macros for setup of traces to return to manager
##
## This file is part of the metadata API.
##
## ******************************************************** 
;#barecode
package provide metaCallback 1.0
;#end

## ******************************************************** 
##
## Name: getMetaDataCallback
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc getMetaDataCallback { jobid } {

	set cid [ set ::${jobid}(cid) ] 
	if	{ [ catch {
		;## reinstall the trace to connect back 
		if	{ [ info exist ::${jobid}(errors) ] } {
			error [ set ::${jobid}(errors) ]
		}
		set fname ""
		if	{ [ info exist ::${jobid}(products) ] } {
			set fname [ set ::${jobid}(products) ] 
		} 
		if	{ [ info exist ::${jobid}(-subject) ] } {
			set subj [ set ::${jobid}(-subject) ]
		} else {
			set subj "$jobid query completed"
		}
		set msg [ list 0 0 0 ]
		set format [ set ::${jobid}(format) ]
		set targ [ set ::${jobid}(-metadatatarget) ]
		set rp [ set ::${jobid}(-returnprotocol) ]
	     
          if { [ regexp -nocase {^(http|ftp|file)(:/*)?$} $rp -> rp ] } {
             set rp ${rp}:/${jobid}_metadata.ilwd
          } 
          
          set seqpt "fixUrlTarget($rp):" 
		foreach [ list protocol target port ] [ fixUrlTarget $rp ] {
             break
          }	
        if { [ regexp {ilwd} $format ] } {
              if { [ string equal datacond $targ ] } {
                 ;## the ilwd was sent to the datacond API
                 set msg [ list 0 0 0 ]
              } else {
                 set msg [ macroReturnMsg $jobid $protocol $fname ]
                 lappend msg $subj
              }   
        }
		set ::$cid $msg
	} err ] } {
		set err "metadataAPI: getMetaData failed: $err"
		set errcode 3
		set msg [ list $errcode $err error! ]
		set ::$cid $msg			
	}
	emptyDataBucket $jobid
	unset ::$jobid
	set ::jobid {}
	reattach $jobid $cid
}

## ******************************************************** 
##
## Name: ckdatabaseQueryCompletion
##
## Description:
## process dbntuple if present
##
## Parameters:
##
## Usage:
##
## Comments:
## emptyDataBucket is done in slave

proc ckdatabaseQueryCompletion { jobid } {
	if	{ [ info exist 	::${jobid}(numQueries) ] } {
		incr ::${jobid}(numQueries) -1
		set cnt [ set ::${jobid}(numQueries) ]
		if	{ ! $cnt } {
			databaseQueryCallback $jobid
		}
	}
}

## ******************************************************** 
##
## Name: databaseQueryCallback
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc databaseQueryCallback { jobid } {

	set cid [ set ::${jobid}(cid) ] 
	if	{ [ catch {
		if	{ [ info exist ::${jobid}(errors) ] } {
			error [ set ::${jobid}(errors) ]
		}
		set fname ""
		if	{ [ info exist ::${jobid}(products) ] } {
			set fname [ set ::${jobid}(products) ] 
		} 
		if	{ [ info exist ::${jobid}(-subject) ] } {
			set subj [ set ::${jobid}(-subject) ]
		} else {
			set subj "$jobid database query completed"
		}
		
		set msg [ list 0 0 0 ]
		set format [ set ::${jobid}(format) ]
		set targ [ set ::${jobid}(-metadatatarget) ]
		set rp [ set ::${jobid}(-returnprotocol) ]
		
          if { [ regexp -nocase {^(http|ftp|file)(:/*)?$} $rp -> rp ] } {
             set rp ${rp}:/${jobid}_metadata.ilwd
          } 
          
          set seqpt "fixUrlTarget $rp" 
		foreach { protocol target port } [ fixUrlTarget $rp ] { break }	
        if { [ regexp {ilwd} $format ] } {
              if { [ string equal datacond $targ ] } {
                 ;## the ilwd was sent to the datacond API
                 set msg [ list 0 0 0 ]
              } else {
                 set msg [ macroReturnMsg $jobid $protocol $fname ]
                 lappend msg $subj
              }   
        }
		set ::$cid $msg
	} err ] } {
		set err "metadataAPI: databaseQuery failed: $err"
		set errcode 3
		set ::$cid [ list $errcode $err error! ]	
	}
	emptyDataBucket $jobid
	unset ::$jobid
	set ::jobid {}
	reattach $jobid $cid
}

## ******************************************************** 
##
## Name: putMetaDataOjDel
##
## Description:
## removes the putMetaData object if job results in error
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::putMetaDataOjDel { jobid } {
      
    if  { [ catch {  
    ;## remove the data object 
        if  { [ info exist ::${jobid}(-ingestdata) ] } {
            set object [ set ::${jobid}(-ingestdata) ]
            if  { [ regexp {port:(.+_p_Ldas.+)} $object -> datap ] } {
                addLogEntry "destroying putMetaData object $datap" purple
                destructElement $datap
            }
        }  
    } err ] } {
        return -code error $err 
    }
}

## ******************************************************** 
##
## Name: putMetaDataXCallback
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc putMetaDataXCallback { jobid } {

	set cid [ set ::${jobid}(cid) ] 
	if	{ [ catch {
		set rc1 [ set ::${jobid}(rc1) ]
		if	{ [ info exist ::${jobid}(errors) ] } {
			error [ set ::${jobid}(errors) ]
		}
		if	{ [ info exist ::${jobid}(-subject) ] } {
			set subj [ set ::${jobid}(-subject) ]
		} else {
			set subj "$jobid completed"
		}
		set result "no data"
		if	{ [ info exist ::${jobid}(products) ] } {
			set result [ set ::${jobid}(products) ]
		}
		if  { $rc1 } {
        	set msg [ list 4 $result $subj ]
    	} else {
        	set msg [ list 0 $result $subj ]
    	}
		set ::$cid $msg
	} err ] } {
		set errcode 3
		if	{ ! $rc1 } {
            set errcode 1
        }        
		set err "metadataAPI: putMetaDataX failed: $err"
		set ::$cid [ list $errcode $err error! ]
	}
	emptyDataBucket $jobid
	unset ::$jobid
	set ::jobid {}
	reattach $jobid $cid
}

## ******************************************************** 
##
## Name: putMetaDataYCallback
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc putMetaDataYCallback { jobid } {
	
	set cid [ set ::${jobid}(cid) ] 
	if	{ [ catch {
		if	{ [ info exist ::${jobid}(errors) ] } {
			error [ set ::${jobid}(errors) ]
		}
		if	{ [ info exist ::${jobid}(-subject) ] } {
			set subj [ set ::${jobid}(-subject) ]
		} else {
			set subj "$jobid completed"
		}
		set result completed
		if	{ [ info exist ::${jobid}(products) ] } {
			set result [ set ::${jobid}(products) ]
		}
        set msg [ list 4 $result $subj ]
		set ::$cid $msg
	} err ] } {
		set errcode 3
		set err "metadataAPI: putMetaDataY failed: $err"
		set ::$cid [ list $errcode $err error! ]	
	}
	emptyDataBucket $jobid
	unset ::$jobid
	set ::jobid {}
	reattach $jobid $cid
}

## ******************************************************** 
##
## Name: putMetaDataCallback
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc putMetaDataCallback { jobid } {

	set cid [ set ::${jobid}(cid) ] 
	if	{ [ catch {
		if	{ [ info exist ::${jobid}(errors) ] } {
			error [ set ::${jobid}(errors) ]
		}
		if	{ [ info exist ::${jobid}(-subject) ] } {
			set subj [ set ::${jobid}(-subject) ]
		} else {
			set subj "$jobid completed"
		}
		set result completed
		if	{ [ info exist ::${jobid}(products) ] } {
			set result [ string trim [ set ::${jobid}(products) ] \n ]
		}
		set msg [ list 4 $result $subj ]
		set ::$cid $msg
	} err ] } {
		set err "metadataAPI: putMetaData failed: $err"
		set errcode 3
		set ::$cid [ list $errcode $err error! ]	
	}
	emptyDataBucket $jobid
	unset ::$jobid
	set ::jobid {}
	reattach $jobid $cid
}

## ******************************************************** 
##
## Name: metadata::putMetaDataLateData
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::putMetaDataLateData { jobid args } {

    if  { [ catch {
        addLogEntry "getting data object after tcl macro" blue
        if	{ [ info exist ::metadata::tcl8.4 ] } {
            foreach entry [ trace info variable ::${jobid}_DATABUCKET ] {
        		foreach { oplist cmd } $entry { break }
        		trace remove variable ::${jobid}_DATABUCKET $oplist $cmd
			}
        } else {
        	set cmd [ eval lindex [ trace vinfo ::${jobid}_DATABUCKET ] 1 ]
        	trace vdelete ::${jobid}_DATABUCKET w $cmd
        }
        after cancel [ set ::${jobid}(timeoutId) ]
        
        foreach datap [ processDataBucket $jobid ] {
            # registerObj $jobid $datap
            ;## port number not used here
            array set ::${jobid} [ list -ingestdata port:$datap ] 
            set cmd "putMetaData [ array get ::${jobid} ]"     
            after 0 [ metadata::evalCmd $jobid $cmd ]
        }
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: metadata::putMetaDataYDataFromBucket
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:
## called by trace function for delayed object
## 

proc metadata::putMetaDataYDataFromBucket { jobid } {

    set errcode 0
    if  { [ catch {
        foreach datap [ processDataBucket $jobid ] {
            # registerObj $jobid $datap
        ;## port number not used here
            set name [ list ]
			if	{ [ catch { 
                set ignore [ getElementMetadata $datap ignore ]
			} err ] } {
			    set ignore no
			}
            ;## if no data from pipeline, return trivial msg
            if  { [ string match yes $ignore ] } {
                 catch { destructElement $datap }
                 set errcode 4
            } else {
                 array set ::${jobid} [ list -ingestdata port:$datap ] 
                 ;## into the event loop!
                 set cmd "putMetaData [ array get ::${jobid} ]"   
                 after 0 [ metadata::evalCmd $jobid $cmd ]
            }
        }
    } err ] } {
        if  { [ info exists  ::${jobid}(-datacondtarget) ] } {
            set dctarg [ set ::${jobid}(-datacondtarget) ]
        } else {
            set dctarg NONE
        }
         
        if { [ string equal metadata $dctarg ] } {
           set errcode 3
        } else {
           set errcode 1
        }
        addLogEntry $err red
    }
    if  { $errcode == 1 } {
        set subj "error!"
        set msg "$err (dc target not metadata)"
    } elseif { $errcode == 3 } {
        set msg "metadataAPI: putMetaDataY failed: $err"
        set subj "error!"
    } elseif { $errcode == 4 } {
        set msg "No metadata for data pipeline $err"
        set subj "no metadata"
    }
    ;## error code 4 does not result in red ball - no metadata 
    if  { $errcode } {
        set cid [ set ::${jobid}(cid) ]
        set ::$cid [ list $errcode $msg $subj ]
        reattach $jobid $cid
        catch { emptyDataBucket $jobid }
        catch { unset ::$jobid }
        catch { set ::jobid {} }
    }  
}

## ******************************************************** 
##
## Name: metadata::putMetaDataYLateData
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::putMetaDataYLateData { jobid args } {

    if  { [ catch {
        addLogEntry "getting data object after tcl macro" blue
        
        if	{ [ info exist ::metadata::tcl8.4 ] } {
            foreach entry [ trace info variable ::${jobid}_DATABUCKET ] {
        		foreach { oplist cmd } $entry { break }
        		trace remove variable ::${jobid}_DATABUCKET $oplist $cmd
			}
        } else {
        	set cmd [ eval lindex [ trace vinfo ::${jobid}_DATABUCKET ] 1 ]
        	trace vdelete ::${jobid}_DATABUCKET w $cmd
        }
        after cancel [ set ::${jobid}(timeoutId) ]
        metadata::putMetaDataYDataFromBucket $jobid 
    } err ] } {
        addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: putMetaDataMacroProcess
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::putMetaDataMacroProcess { jobid { bg 0 } } {

	if	{ [ catch {
        ;## only one datap          
        set ::${jobid}(cmd) putMetaDataCallback 
        if  { [ info exist ::${jobid}_DATABUCKET ] } {            
            foreach datap [ processDataBucket $jobid ] {
                # registerObj $jobid $datap
                ;## port number not used here
                array set ::${jobid} [ list -ingestdata port:$datap ] 
		        ;## threading the submit
                set cmd "putMetaData [ array get ::${jobid} ]"     
                after 0 [ metadata::evalCmd $jobid $cmd ]
            }
        } else {
            set ::${jobid}(timeoutId) [ after $::METADATA_MAX_JOB_TIME \
                [ list metadata::jobTimedOut $jobid ] ]
            if	{ [ info exist ::metadata::tcl8.4 ] } {
            	::trace add variable ::${jobid}_DATABUCKET { write } \
                [ list after 0 [ list metadata::putMetaDataLateData $jobid ] ] 
            } else {
            	::trace variable ::${jobid}_DATABUCKET w \
                [ list after 0 [ list metadata::putMetaDataLateData $jobid ] ] 
            }
        }              
    } err ] } {  
        if  { ! $bg } {
            return -code error $err
        }   
        set errcode 3
        catch { emptyDataBucket $jobid }
		unset ::$jobid
		set ::jobid {}
        set msg "metadataAPI: putMetaData failed: $err"
        set cid [ set ::${jobid}(cid) ]
		set ::$cid [ list $errcode $msg error! ]
        reattach $jobid $cid
    }
}

## ******************************************************** 
##
## Name: putMetaDataMacroYProcess
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::putMetaDataMacroYProcess { jobid } {

    set errcode 0
    if  { [ catch {
        ;## regexp patterns for -datacondtarget and -algorithms
        set rc1_rx {-datacondtarget\s+(\{\s*)?(metadata|wrapper|mpi)}
        set rc3_rx {output\(.*,.*,\s*(metadata|wrapper|mpi)}
     
        if { [ info exists  ::${jobid}(-datacondtarget) ] } {
            set dctarg [ set ::${jobid}(-datacondtarget) ]
        } else {
            set dctarg NONE
        }
     
        set everything [ array get ::$jobid ]
        set rc1 [ regexp -- $rc1_rx $everything ]
        set rc3 [ regexp -- $rc3_rx $everything ]
        
        ;## if nothing to metadata,wrapper, short_circuit
        set done 0
        if { $rc1 || $rc3 } {
           set ::${jobid}(rc1) $rc1
           set ::${jobid}(rc3) $rc3
           set ingestdata [ set ::${jobid}(-ingestdata) ]
           set ::${jobid}(cmd) putMetaDataYCallback
           ;## Mary says, only one datap possible, ever.
           if   { [ info exist ::${jobid}_DATABUCKET ] } {
                metadata::putMetaDataYDataFromBucket $jobid 
                set done 1
           } else {
                addLogEntry "waiting for data object" blue
                set ::${jobid}(timeoutId) [ after $::METADATA_MAX_JOB_TIME \
                [ list metadata::jobTimedOut $jobid ] ]
                if	{ [ info exist ::metadata::tcl8.4 ] } {
                	::trace add variable ::${jobid}_DATABUCKET { write } \
                	[ list after 0 [ list metadata::putMetaDataYLateData $jobid ] ]
               	} else {
                	::trace variable ::${jobid}_DATABUCKET w \
                	[ list after 0 [ list metadata::putMetaDataYLateData $jobid ] ]
                }
           }                         
        ;## neither pattern matched   
        } else {
           set errcode 4           
        }
    } err ] } { 
        if { [ string equal metadata $dctarg ] } {
           set errcode 3
        } else {
           set errcode 1
        }
        addLogEntry $err red
    }
    ;## error code 4 does not result in red ball - no metadata 
    ;## if done, already returned error via metadata::putMetaDataYDataFromBucket
    if  { !$done } {
        if  { $errcode == 1 } {
            set subj "error!"
            set msg "$err (dc target not metadata)"
        } elseif { $errcode == 3 } {
            set msg "metadataAPI: putMetaDataY failed: $err"
            set subj "error!"
        } elseif { $errcode == 4 } {
            set msg "No metadata for data pipeline $err"
            set subj "no metadata"
        }
        if  { $errcode } {
            set cid [ set ::${jobid}(cid) ]
            set ::$cid [ list $errcode $msg $subj ]
            reattach $jobid $cid
            catch { emptyDataBucket $jobid }
            catch { unset ::$jobid }
            catch { set ::jobid {} }
        }
    }   
}

## ******************************************************** 
##
## Name: putMetaDataMacroXProcess
##
## Description:
## Returns result to manager
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::putMetaDataMacroXProcess { jobid } {

	;## if there datacondtarget is not metadata or
    ;## there are no intermediates to metadata, then skip this macro
    set cid [ set ::${jobid}(cid) ]
    if  { [ catch {        
        set everything [ array get ::${jobid} ]
        set rc1 [ regexp -- {-datacondtarget\s+metadata\s*} $everything ]
        set rc2 [ regexp {output\(([^,]+,)+?metadata} $everything ]
        #set rc3 [ regexp {output\(([^,]+,)+?,(wrapper|mpi)} $everything ]
    } err ] } {
        set errcode 3
		unset ::$jobid
		set ::jobid {}		
		set err "metadataAPI: putMetaDataMacroX failed: $err"
		set ::$cid [ list $errcode $err error! ]
		reattach $jobid $cid
        return 
    }
    
    ;## if nothing to metadata,wrapper, short_circuit
    if  { ! [ expr $rc1 | $rc2 ] } {
		unset ::$jobid
		set ::jobid {}
		;## debugPuts "No output for metadata (rc1=$rc1,rc2=$rc2), bypassing."
		set ::$cid [ list 0 0 0 ]
		reattach $jobid $cid
        return 
    }
	set ::${jobid}(rc1) $rc1
	set ::${jobid}(rc2) $rc2
    if  { [ catch {
		set result "no data"
		
        set subj [ set ::${jobid}(-subject) ]
        if { ! [ string length $subj ] } {
            set subj "data inserted"
        }        
        
        set ingestdata [ set ::${jobid}(-ingestdata) ]
        set ::${jobid}(cmd) putMetaDataXCallback 
        ;## only one datap
        if  { [ info exist ::${jobid}_DATABUCKET ] } {
            foreach datap [ processDataBucket $jobid ] {
                # registerObj $jobid $datap
                ;## port number not used here
                set name ""
                array set ::${jobid} [ list -ingestdata port:$datap ] 
		        ;## threading the submit
			    set cmd "putMetaData $everything"  
                after 0 [ list metadata::evalCmd $jobid $cmd ]
            }
        } else {
            addLogEntry "waiting for data object" blue
            set ::${jobid}(timeoutId) [ after $::METADATA_MAX_JOB_TIME \
                [ list metadata::jobTimedOut $jobid ] ]
            if	{ [ info exist ::metadata::tcl8.4 ] } {
            	::trace add variable ::${jobid}_DATABUCKET { write } \
                [ list after 0 [ list metadata::putMetaDataLateData $jobid ] ] 
            } else {  
            	::trace variable ::${jobid}_DATABUCKET w \
                [ list after 0 [ list metadata::putMetaDataLateData $jobid ] ]  
            }            
        }            
    } err ] } {  
		addLogEntry $err 2    
        set errcode 3
		if  { ! $rc1 } {
            set errcode 1
        }
        catch { emptyDataBucket $jobid }
		catch { unset ::$jobid }
		catch { set ::jobid {} }
        set msg "metadataAPI: putMetaDataX failed: $err"
		set ::$cid [ list $errcode $msg error! ]
		reattach $jobid $cid
		return
    }
}

## ******************************************************** 
##
## Name: getMetaDataMacroProcess
##
## Description:
## macro processing getMetaData cmd
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::getMetaDataMacroProcess { jobid } {
     
     set cid [ set ::${jobid}(cid) ]
     if { [ catch {
	 	set seqpt ""
        set sql [ string trim [ set ::${jobid}(-sqlquery) ] ]
		;## debugPuts "$jobid,sql $sql, [ string length $sql]"
		set seqpt "getMetaDataValid $jobid"
       	getMetaDataValid $jobid
		set ::${jobid}(cmd) getMetaDataCallback 
    	set cmd "getMetaData [ array get ::${jobid} ]"
		;## command is threaded here
		set seqpt "metadata::evalCmd $jobid $cmd" 
    	after 0 [ metadata::evalCmd $jobid $cmd ]
     } err ] } {
	 	unset ::$jobid
		set ::jobid {}
		set ::$cid [ list 3 "metadataAPI:$jobid: $seqpt,$err" error! ]
		reattach $jobid $cid
     }
}

## ******************************************************** 
##
## Name: metadata::outputformatValid
##
## Description:
## Validates getMetaData user cmd parameters
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::descMetaDataOutputValid { jobid } {

	set ::${jobid}(format) LIGO_LW

    if { [ info exists ::${jobid}(-outputformat) ] } {
        set of [ set ::${jobid}(-outputformat) ]
		if	{ [ string length $of ] } {
			set ::${jobid}(format) $of
		}
    } else {
        set of [ list ]
    }
    set of [ string trim $of ]
    
    if  { ! [ regexp -nocase -- {^(ligo_lw|ilwd|ilwd ascii)$} $of ] } {
        set msg "-outputformat $of invalid\n"
        append msg "must be 'LIGO_LW' or 'ilwd ascii'."
        return -code error $msg
    } 
	foreach { format type } [ set ::${jobid}(format) ] { break }

	if	{ ! [ info exist type ] || ![ string length $type ] } {
		set ::${jobid}(format) [ list $format ascii ]
	}

}

## ******************************************************** 
##
## Name: metadata::descMetaDataValid_options
##
## Description:
##
## 
## Parameters:
##
## Usage:
## valid_options
##
## Comments:

proc metadata::descMetaDataValid_options { jobid } {
#   validate by objective

    if  { [ catch {
        set table   [ set ::${jobid}(-table) ]
        set column  [ set ::${jobid}(-column) ]
        set type    [ set ::${jobid}(-type) ]
        set key     [ set ::${jobid}(-key) ]

        if  { [ regexp -nocase {none} $table ] \
            && [ regexp -nocase {none} $column ] \
            && [ regexp -nocase {none} $type ] && \
            [ regexp -nocase {none} $key ] } {
            set msg "no function specified"
            error $msg
        }
        set state stateINIT
        ;## valid -key as primary, foreign or none        
        if  { [ regexp -nocase {(primary|foreign)} $key ] } {
            if  { [ regexp -nocase {none} $type ] && [ regexp -nocase {none} $column ] } {
                set state stateKEY
            } else {   
                error "-type,-column must be none for -key"
            }  
        } else {
            if  {  ! [ regexp -nocase {none} $key ] } {
                error "-key options are primary,foreign or none"
            }
        }
        ;## valid -type as none or all   
        if  { $state == "stateINIT" } {     
            if  { [ regexp -nocase {all} $type ] } {
                if  { [ regexp -nocase {none} $column ] } {
                    set rc stateTYPE
                } else {
                    error "-column must none for -type"
                }
            } elseif { ! [ regexp -nocase {none} $type ] } {     
                error "-type must be all or none"
            }
        }        
    } err ] } {
        return -code error $err
    }
}
## ********************************************************

## ******************************************************** 
##
## Name: metadata::descMetaDataValid 
##
## Description:
## Validates the user supplied parameters for the getData
## call to the frame API.  Depends on the existence of a
## correctly designed XXXXX.meta macro.
## 
## Parameters:
##
## Usage:
##
## Comments:
## Note that it does not rely on the current global value
## of $::jobid.  When this code throws an exception, the
## metadata API's operator knows to return an errorlevel of
## "3" (abort job) to the manager. see LDASgwrap

proc metadata::descMetaDataValid { jobid } {

    if  { [ catch {
        metadata::descMetaDataOutputValid $jobid 
        descMetaDataValid_options $jobid
    } err ] } {
        return -code error $err
    }
}

## ******************************************************** 
##
## Name: descMetaDataMacroProcess
##
## Description:
## validate the cmd and execute it 
## 
## Parameters:
##
## Usage:
##
## Comments:
## ********************************************************

proc metadata::descMetaDataMacroProcess { jobid } {
    set cid [ set ::${jobid}(cid) ]
	if  { [ catch  {
        set seqpt ""
		descMetaDataValid $jobid
		set ::${jobid}(cmd) getMetaDataCallback 
        set cmd "descMetaData [ array get ::${jobid} ]"
        set seqpt "metadata::evalCmd $jobid $cmd" 
    	after 0 [ metadata::evalCmd $jobid $cmd ]
   	} err ] } {
        unset ::$jobid
		set ::jobid {}
		set ::$cid [ list 3 "metadataAPI:$jobid: $seqpt,$err" error! ]
		reattach $jobid $cid
	}
}

## ******************************************************** 
##
## Name: metadata::jobTimedOut
##
## Description:
## job timed out
## 
## Parameters:
##
## Usage:
##
## Comments:

proc metadata::jobTimedOut { jobid } {
    addLogEntry "$jobid timed out, no data arrived" 2
    if  { [ info exist ::${jobid}(cmd) ] } {
        append ::${jobid}(errors) "No data bucket for this job\n"
        [ set ::${jobid}(cmd) ] $jobid
    } else {
        metadata::killJob $jobid
    }
}


