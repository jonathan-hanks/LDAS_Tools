/* -*- C++ -*- */

#ifndef CATALOG_H
#define CATALOG_H

#ifdef __GNUG__
#define interface
#endif

#include <string>
#include "query.h"

class dbDatabase;

class dbCatalog: public dbQuery
{
public:
  inline dbCatalog(const dbDatabase& DB);
  bool Columns(std::string Schema = dbQuery::WILDCARD,
	       std::string Table = dbQuery::WILDCARD,
	       std::string Column = dbQuery::WILDCARD);
};

dbCatalog::
dbCatalog(const dbDatabase& DB)
  : dbQuery(DB)
{
}

#endif	/* CATALOG_H */
