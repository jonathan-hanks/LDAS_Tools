#ifndef ODBC_H
#define	ODBC_H

#if defined(IODBC)
#ifdef OPEN_LINK
#include "iodbc.h"
#endif	/* OPEN_LINK */
#include "isql.h"
#ifndef SQL_DEFAULT
#define	SQL_DEFAULT SQL_C_DEFAULT
#endif	/* SQL_DEFAULT */
#else	/* IODBC */
#include "sql.h"
#endif /* IODBC */

//=======================================================================
// Things that are special to DB2 version of ODBC
//=======================================================================

#ifdef SQL_MVSDB2V23_SYNTAX
#include "sqlcli.h"
#include "sqlcli1.h"
#endif

//=======================================================================
// Include the extiontion file for everyone
//=======================================================================

#if defined(IODBC)
#include "isqlext.h"
#else
#include "sqlext.h"
#endif

//=======================================================================
// Things to do for different versions
//=======================================================================

#if ODBCVER < 0x0300

#ifdef OPEN_LINK
#define	SQLHENV		HENV
#define	SQLHDBC		HDBC
#define	SQLHSTMT	HSTMT
#endif

#define SQLSCHAR	UCHAR
#define SQLCHAR		UCHAR
#define SQLINTEGER	SDWORD
#define SQLUINTEGER	UDWORD
#define SQLSMALLINT	SWORD
#define SQLUSMALLINT	UWORD
#define SQLPOINTER	PTR

#ifndef SFLOAT
#define	SFLOAT		float
#endif
#ifndef SDOUBLE
#define	SDOUBLE		double
#endif

#define	SQL_SQLSTATE_SIZE	5

#ifndef SQL_NO_DATA
#define	SQL_NO_DATA	SQL_NO_DATA_FOUND
#endif
#endif


#endif	/* ODBC_H */
