#! /ldcg/bin/wish
## ******************************************************** 
##
## Name: Main 
##
## Description:
## Parse command line and run some canned db queries.
## Default behaviour is for dbname to be the command
## line argument.
##
## Usage:
##        just run me!
##
## Comments:
## Ed is responsible for this!
## Funny identifiers with ";#" at the beginning are part
## of TclDOC.
##
## Warning: quitting TkCons will kill the app!

;#barecode 

## ----------------------------------------------------------------------
## Make sure the local path is in our search path
## ----------------------------------------------------------------------

set auto_path [ linsert $auto_path 0 /ldcg/lib ]
set auto_path [ linsert $auto_path 0 . ]

## ----------------------------------------------------------------------
## Add packages
## ----------------------------------------------------------------------

package require genericAPI
package require metadataAPI

## ----------------------------------------------------------------------
## Start of the main demo
## ----------------------------------------------------------------------

set tkcons [ file join $tcl_library tkcon.tcl ]

set dbligo  "ligo_ol"
set dbname  ""
set dbquery ""
set queryid -1

switch -exact -- [ lindex $argv 0 ] {
    -ligo1
    {
	set dbname $dbligo
	set dbquery "select * from frameset"
    }
    -ligo2
    {
	set dbname $dbligo
	set dbquery "select * from frameset\n"
	append dbquery "where frameset_time = \'1979-01-18-21.51.15.000000\'"
    }
    -ligo3
    {
	set dbname $dbligo
	set dbquery "select * from statistics\n"
	append dbquery "where frameset_time = \'1979-01-18-21.51.15.000000\'"
    }
    -ligo4
    {
	set dbname $dbligo
	set dbquery "insert into datasource"
	append dbquery " (channel_id, frame_id, offset, time_span, online_flag, datasource_id)"
	append dbquery " values (?,?,?,?,?, generate_unique())"
	set queryid 4
    }
    -ligo4.0
    {
	set dbname $dbligo
	set dbquery "delete from datasource "
	append dbquery "where channel_id = \'channel_id__0087\'"
    }
    -ligo4.1
    {
	set dbname $dbligo
	set dbquery "select * from datasource"
    }
    -ligo_bad1
    {
	set dbname $dbligo
	set dbquery "select * from frameset"
    }
    default
    {
	set dbname [ lindex $argv 0 ]
	;## return -code error "Invalid argument(s): $argv"
    }
}     

frame .top
if { ! [ string length $dbname ] } {
    label  .top.l1 -text "Enter database name:"
    entry  .top.e1 -width 30 -textvariable dbname
    grid .top.l1 .top.e1 -pady 8 -sticky w
} ;## end of if/else

label  .top.l2 -text "enter user name:"
entry  .top.e2 -width 30 -textvariable dbuser
label  .top.l3 -text "enter password:"
entry  .top.e3 -show * -width 30 -textvariable dbpasswd
if { ! [ string length $dbname ] } {
    label  .top.l4 -text "enter query:"
    entry  .top.e4 -width 30 -textvariable dbquery
}

button .top.b1 -text "exit" -command exit
button .top.b2 -text "process" \
	-command {
              upvar #0 dbname _dbname
              upvar #0 dbuser _dbuser
              upvar #0 dbpasswd _dbpasswd
              upvar #0 dbquery _dbquery
    	      upvar #0 queryid _queryid
              goGetIt $_dbname $_dbuser $_dbpasswd \"$_dbquery\" $_queryid
              }
button .top.b3 -text "TkCons" -command "source $tkcons"
frame .bottom

text  .bottom.txt  -width 70 -height 20 -wrap none \
	-yscrollcommand { .bottom.ybar set } \
	-xscrollcommand { .bottom.xbar set }

scrollbar .bottom.ybar -orient vertical \
	-command { .bottom.txt yview }
scrollbar .bottom.xbar -orient horizontal \
	-command { .bottom.txt xview }
grid .top.l2 .top.e2 -pady 8 -sticky w
grid .top.l3 .top.e3 -pady 8 -sticky w
if { ! [ string length $dbname ] } {
    grid .top.l4 .top.e4 -pady 8 -sticky w
}
grid .top.b1 .top.b2 .top.b3 -pady 8 -sticky w
grid .top
grid .bottom.txt .bottom.ybar -sticky news
grid .bottom.xbar -sticky ew
grid .bottom
wm resizable . 0 0

## if new db does not opened ok, old db pointer is still there.
## 
proc opendb { {dbname "" } { dbuser "" } { dbpasswd "" } } {
	if	{ ![info exist ::dbopened] || 
         [ string compare $::dbopened $dbname] } {
    	if  { [ catch {
            set ::easyDB_ptr [ dbEasy ::easyDB $dbname $dbuser $dbpasswd ]
            } err ] } {
            return -code error $err
        }
    	puts "database opened..., $::easyDB_ptr"
		upvar #0 dbopened _dbopened
		puts "[package names]"
	}	
}
            
proc goGetIt { { dbname "" } { dbuser "" } { dbpasswd "" } { dbquery "" } {queryid "" } } {
    set dbquery [string trim $dbquery "\"\}\{"]
    puts "$dbname $dbquery\n"
    if  { [ catch { 
        opendb $dbname $dbuser $dbpasswd
        } err ] } {
	    return -code error $err
    }

    if { [ catch {
	    dbEasyStmt easy $::easyDB_ptr
        } err ] } {
	    return -code error $err
    }
    if { $queryid == 4 } {
	easy Prepare $dbquery;
 	easy BindParameter 1 [ putElement "<LSTRING size='16'>channel_id__0087</LSTRING>" ];
	easy BindParameter 2 [ putElement "<LSTRING size='17'>LLH_293919176.raw</LSTRING>" ];
	easy BindParameter 3 [ putElement "<INT_4U>2</INT_4U>" ];
	easy BindParameter 4 [ putElement "<INT_4U>5</INT_4U>" ];
	easy BindParameter 5 [ putElement "<INT_4U>0</INT_4U>" ];

	easy Submit

    } {
	if { [ catch {
	    easy Submit $dbquery
	} err ] } {
	    return -code error $err
	}
    }
	if { [ winfo exists . ] } {
		.bottom.txt delete 1.0 end
	}
    while { [ easy Fetch ] } {
	set data [ easy GetRowData ]
	if { [ winfo exists . ] } {
	    .bottom.txt insert end "$data\n"
	    update
	} else {
	    puts $data
	}
    }
    .bottom.txt insert end "Rows affected: [ easy GetNumberOfRows ]"
    rename easy ""
}
vwait forever
## ******************************************************** 

