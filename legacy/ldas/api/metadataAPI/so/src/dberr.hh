/* -*- C++ -*- */

#ifndef DBERROR_H

#ifdef __GNUG__
#pragma interface
#endif

#include <stdio.h>
#include <string>
#include <stdexcept>

#include "general/ldasexception.hh"

#include "odbc.h"

// class ldas_ostrstream;

//#######################################################################
// Class definitions
//#######################################################################
//: Report database errors.
class dbErr
  : public LdasException
{
public:
  enum {
    ERROR_GENERAL_DB_ERROR,
    ERROR_NEED_DATA,
    ERROR_NO_DATA
  };

  //: Constructor
  dbErr( const std::string& Filename,
	 int LineNumber,
	 SQLHENV Henv = SQL_NULL_HENV,
	 SQLHDBC Hdbc = SQL_NULL_HDBC,
	 SQLHSTMT Hstmt = SQL_NULL_HSTMT,
	 int ErrorCode = ERROR_GENERAL_DB_ERROR );
  //: Destructor
  virtual ~dbErr(void) throw ();
  //: Display error
#if 0
  virtual void Print(char *result, int size) const;

  virtual const char* what( ) const throw();
#endif /* 0 */
};

//: Error representing insufficient data supplied
class dbErrNeedData: public dbErr
{
public:
  //: Constructor
  dbErrNeedData(const std::string& Filename, int LineNumber,
		       SQLHENV Henv, SQLHDBC Hdbc, SQLHSTMT Hstmt);
};

//: Error representing the end of data
class dbErrNoData: public dbErr
{
public:
  //: Constructor
  dbErrNoData( const std::string& Filename, int LineNumber,
	       SQLHENV Henv, SQLHDBC Hdbc, SQLHSTMT Hstmt );
};

//!ignore_begin:
//#######################################################################
// Implemntation of Class' inline methods
//#######################################################################
//!ignore_end:

inline dbErr::
~dbErr(void) throw()
{
}

//!param: const std::string& Filename - Filename where error origionated.
//!param: int LineNumber - Line number where error origionated.
//!param: SQLHENV Henv - SQLHENV handle for error message.
//!param: SQLHDBC Hhdbc - SQLHHDBC handle for error message.
//!param: SQLHSTMT Hstmt - SQLHSTMT handle for error message.

inline dbErrNeedData::
dbErrNeedData( const std::string& Filename, int LineNumber,
	       SQLHENV Henv, SQLHDBC Hdbc, SQLHSTMT Hstmt )
  : dbErr( Filename, LineNumber, Henv, Hdbc, Hstmt, ERROR_NEED_DATA )
{
}

//!param: const std::string& Filename - Filename where error origionated.
//!param: int LineNumber - Line number where error origionated.
//!param: SQLHENV Henv - SQLHENV handle for error message.
//!param: SQLHDBC Hhdbc - SQLHHDBC handle for error message.
//!param: SQLHSTMT Hstmt - SQLHSTMT handle for error message.

inline dbErrNoData::
dbErrNoData( const std::string& Filename, int LineNumber,
	     SQLHENV Henv, SQLHDBC Hdbc, SQLHSTMT Hstmt )
  : dbErr( Filename, LineNumber, Henv, Hdbc, Hstmt, ERROR_NO_DATA )
{
}

#endif	/* DBERROR_H */
