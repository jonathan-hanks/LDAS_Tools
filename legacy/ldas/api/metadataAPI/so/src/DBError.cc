#include "config.h"

#include "DBError.hh"

// This constuctor ensures that certain members get properly initialized
metadataAPI::DBError::
DBError( )
  : m_iteration( 0 )
{
}

// AppendMessage appends an error message to the list of error messages.
// It prefixes the message with "Iteration: [count]:" and appends the
// message with a newline character. [count] represents which iteration
// the error occured on. [count] starts from 1.
//
//!param: const std::string& Message - Body of the message to add to the
//+	list of error messages.
void metadataAPI::DBError::
AppendMessage( const std::string& Message )
{
  std::string	msg( Message );

  for ( std::string::iterator
	  cur( &(msg[ 0 ]) ),
	  end( &(msg[ msg.length( ) ]) );
	cur != end;
	cur++ )
  {
    if ( *cur == '\n' )
    {
      *cur = ' ';
    }
  }
  if ( m_iteration )
  {
    m_message << "; ";
  }
  m_message << "Iteration: " << ++m_iteration << ": " << msg;
}
