#ifndef METADATAAPI_DBPUSH_HH
#define	METADATAAPI_DBPUSH_HH

#include <string>

#include "genericAPI/threaddecl.hh"

namespace ILwd
{
  class LdasElement;
}

namespace metadataAPI
{
  //: Put data into the database
  std::string DBPush( const std::string Database,
		      const std::string User,
		      const std::string Password,
		      ILwd::LdasElement* Tables,
		      const int MaximumNumberOfTries,
		      const std::string RetryRegex );

#if HAVE_TID
  //: Threaded version of DBPush.
  CREATE_THREADED6_DECL( DBPush, std::string, \
			 const std::string /* Database */, \
			 const std::string /* User */, \
			 const std::string /* Password */, \
			 ILwd::LdasElement* /* Tables */,
			 const int /* MaximumNumberOfTries */,
			 const std::string /* RetryRegex */ );
#endif /* HAVE_TID */

} // namespace - metadataAPI

#endif /* METADATAAPI_DBPUSH_HH */
