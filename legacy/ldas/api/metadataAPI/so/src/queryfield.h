/* -*- C++ -*- */

// Represents a data row
class WXDLLEXPORT wxQueryField: public wxObject
{
  // JACS
  DECLARE_DYNAMIC_CLASS(wxQueryField)
 private:
  void *data;
  short type;
  long size;
  bool dirty;

  bool AllocData(void);

  public:
  wxQueryField(void);
  ~wxQueryField(void);
  
  bool SetData(void*, long);
  void SetDirty(bool =TRUE);
  void ClearData(void);
  void SetType(short);
  void SetSize(long);
  
  void* GetData(void);
  short GetType(void);
  long GetSize(void);
  
  bool IsDirty(void);
};

