/* -*- C++ -*- */

#ifndef QUERY_H
#define	QUERY_H

#ifdef __GNUG__
#pragma interface
#endif

#include <vector>
#include <string>

#include "odbc.h"

class dbDatabase;
class dbColInfo;

class dbQuery
{
public:
  // Character representing wild card matches
  static const char* WILDCARD;

  //: Constructor
  dbQuery(const dbDatabase& DB);
  //: Desctructor
  ~dbQuery(void);
  
  //: Bind a column to an application variable.
  void Bind(SQLUSMALLINT Column,
	    SQLSMALLINT TargetType, SQLPOINTER TargetValuePtr,
	    SQLINTEGER BufferLength, SQLLEN* StrLen_or_IndPtr);

  //: Bind  an application variable to a parameter marker.
  void BindParameter(SQLUSMALLINT ParamNum, SQLSMALLINT CType,
		     SQLPOINTER Data, SQLUINTEGER ParamSize = 0,
		     SQLSMALLINT DataType = SQL_DEFAULT,
		     SQLLEN *FAR Length = (SQLLEN *FAR)0);

  //: Retrieve the database information associated with the query
  const dbDatabase& GetDatabase(void) const;
  //: Retrieve the HSTMT handle.
  SQLHSTMT GetHstmt(void) const;
  //: Retrieve a single row of data.
  void Fetch(void);
  //: Retrieve information about a specific column.
  void GetColInfo(int Column = -1);
  //: Retrieve the number of columns in the result.
  int GetColumnCount(void) const;
  //: Retrieve the name of a column.
  std::string GetColumnName(int Column) const;
  //: Retrieve the width of a column.
  SQLUINTEGER GetColumnSize(int Column) const;
  //: Retrieve the datatype of a column.
  SQLSMALLINT GetColumnType(int Column) const;
  //: Retrieve data from a column.
  void GetData(SQLSMALLINT Column, SQLSMALLINT TargetType,
	       SQLPOINTER TargetValue,
	       SQLLEN BufferLength,
	       SQLLEN* RetLength) const;
  //: Retrieve the number of parameters
  int GetParamCount(void) const;
  //: Retrieve the number of result rows.
  inline int GetRowCount(void) const;

  //: Prepare a query
  void Prepare( const std::string& Query);

  //: Submit a query for execution
  void Submit(std::string Query = "");

protected:
  //: Allocate a statement handle.
  void allocStmt(void);

private:
  // Reference to connected database
  const dbDatabase&		m_db;
  // HSTMT handle
  SQLHSTMT			m_hstmt;
  // Number of result rows processed
  int				m_row_count;
  // Number of columns in result
  SQLSMALLINT			m_col_count;
  // List of column information
  std::vector<dbColInfo>	m_cols;

  //: Determine if an error occured.
  void eval_retcode( const char* const Filename, int LineNumber,
		     RETCODE Retcode ) const;
  //: Allocate space for column information.
  void prep_col_info(void);
  //: Generate bad_column error.
  void throw_bad_column(int Column) const;
};

//!return: int - Number of columns in result
inline int dbQuery::
GetColumnCount(void) const
{
  return m_col_count;
}

//!return: const dbDatabase& - Reference to database
inline const dbDatabase& dbQuery::
GetDatabase(void) const
{
  return m_db;
}

//!return: SQLHSTMT - Handle to HSTMT
inline SQLHSTMT dbQuery::
GetHstmt(void) const
{
  return m_hstmt;
}

// This method should only be called once the error dbErrNoData is thrown. 
//   Calling this method before then will result in a count that may be too
//   low.
//!return: int - Number of result rows processed.
inline int dbQuery::
GetRowCount(void) const
{
  return m_row_count;
}

#endif	/* QUERY_H */
