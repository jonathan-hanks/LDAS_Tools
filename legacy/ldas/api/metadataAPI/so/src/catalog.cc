#ifdef __GNUG__
#pragma implementation "catalog.h"
#endif

#include "config.h"

#include "odbc.h"

#include "query.h"
#include "catalog.h"
#include "database.h"
#include "dberr.hh"

bool dbCatalog::
Columns(std::string Schema, std::string Table, std::string Column)
{
  RETCODE	ret;

  allocStmt();
  ret = SQLColumns(GetHstmt(),
		   NULL, 0,
		   (SQLCHAR *)(Schema.c_str()), SQL_NTS,
		   (SQLCHAR *)(Table.c_str()), SQL_NTS,
		   (SQLCHAR *)(Column.c_str()), SQL_NTS);
  
  if ((SQL_SUCCESS != ret) && (SQL_SUCCESS_WITH_INFO != ret))
  {
    throw dbErr(__FILE__, __LINE__,
		GetDatabase().GetHenv(),
		GetDatabase().GetHdbc(),
		GetHstmt());
  }
  GetColInfo();
  return true;
}
