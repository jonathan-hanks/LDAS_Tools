/* -*- C++ -*- */

#ifndef CANNED_H
#define	CANNED_H

#ifdef __GNUG__
#pragma interface
#endif

#include "dbeasy.h"
#include "dbeasystmt.h"

class dbEasy;

//: Class for static queries
class dbCannedQuery : public dbEasyStmt
{
public:
  //: Constructor
  dbCannedQuery(const dbEasy& Env);
};

//!param: const dbEasy& Env - Reference to opened database connection
inline dbCannedQuery::
dbCannedQuery(const dbEasy& Env)
  : dbEasyStmt(Env)
{
}

#endif	/* CANNED_H */

