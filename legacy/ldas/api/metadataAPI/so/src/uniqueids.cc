#include "config.h"

#include "uniqueids.h"

#if 0
#define	AT(msg) cout << msg << " - at - " << __FILE__ << ":" << __LINE__ << endl << flush
#else
#define	AT(msg)
#endif

// Generate a query appropriate to retrieve the number of requested
// unique ids.
//
//!param: const dbEays& Env - Reference to opened database connection
//!param: int Count - Number of unique ids to retrieve
dbUniqueIds::
dbUniqueIds(const dbEasy& Env, int Count)
  : dbCannedQuery(Env)
{
  AT( "Generating Query String" );
  std::string	query = "values ";

  for (int x = 0; x < Count; x++)
  {
    if (x)
    {
      query += ",";
    }
    query += "(generate_unique())";
  }
  AT( "Submitting query" );
  Submit(query);
  AT( "Done creating query" );
}
