/* -*- C++ -*- */

#ifndef DBEASY_H
#define	DBEASY_H

#ifdef __GNUG__
#pragma interface
#endif

#include <pthread.h>

#include <string>
#include <list>

#include "general/bit_vector.hh"
#include "general/types.hh"
#include "ilwd/ldasarray.hh"

#include "genericAPI/registry.hh"
#include "genericAPI/threaddecl.hh"
#include "genericAPI/tid.hh"

#include "database.h"
#include "query.h"

#undef	GLOBAL_REGISTRY

//:ignore_begin!
namespace ILwd
{
  class LdasElement;
  class LdasContainer;
}
//:ignore_end!

//: Allows for submitting statements and processing results.
class dbEasyStmt
{
public:
  class IlwdInfo {
  public:
    SQLPOINTER	m_data;
    SQLUINTEGER	m_param_size;
    bool	m_free_pointer;

    IlwdInfo(SQLPOINTER Data, SQLUINTEGER Size, bool FreePointer);
    ~IlwdInfo(void);
  };

  //: Constructor
  dbEasyStmt(const dbEasy& Env);
  //: Destructor
  ~dbEasyStmt(void);
  //: Bind a Column to an application variable.
  void Bind(SQLUSMALLINT ColumnNum, SQLINTEGER Size = 1,
	    ILwd::LdasElement* Base = NULL);
  //: Bind an application variable to a parameter
  void BindParameter(SQLUSMALLINT ParamNum, SQLSMALLINT CType,
			    SQLPOINTER Data, SQLUINTEGER ParamSize = 0,
			    SQLSMALLINT DataType = SQL_DEFAULT,
			    SQLLEN *FAR StrLen_or_IndPtr = 0);
  //: Bind an application variable to a parameter
  void BindParameter(SQLUSMALLINT ParamNum, ILwd::LdasElement* Base);
  //: Retrieve a single result row
  bool Fetch(void);
  //: Retrieve a set of result rows.
  bool FetchMulti(int MaxRows, int Inc = 16);
  //: Submit the FetchMulti for execution in a thread.
#if HAVE_TID
  tid* FetchMulti_t(int MaxRows, int Inc = 16);
  //: Wait till the threaded FetchMulti execution is completed.
  bool FetchMulti_r(tid* t);
#endif /* HAVE_TID */
  //: Retrieve the name of a column
  std::string GetColumnName(int Column) const;
  //: Retrieve the number of bytes occupied by the column.
  SQLUINTEGER GetColumnSize(int Column) const;
  //: Retrieve the raw buffer for a column result.
  const SQLPOINTER* GetDataBuffer(int Column) const;
  const ILwd::LdasElement* GetBoundColumnData(int Column);
  //: Return the number of columns for the SQL statement.
  int GetNumberOfColumns(void) const;
  //: Return the number of parameters in teh SQL statement.
  int GetNumberOfParameters(void) const;
  //: Return the number of result rows.
  int GetNumberOfRows(void) const;
  //: Prepare the query for execution.
  void Prepare( const std::string& Query );
  //: Retrieve the raw buffer for a column result.
  ILwd::LdasElement* ReleaseBoundColumnData(int Column);
  //: Submit the Query for execution.
  void Submit(std::string Query = "");
  //: Submit the Query for execution in a thread.
#if HAVE_TID
  tid* Submit_t(std::string Query = "");
  //: Wait till the threaded query execution is completed.
  void Submit_r(tid* t);
#endif /* HAVE_TID */

private:
  typedef struct {
    ILwd::LdasElement*	m_base;
    SQLLEN*		m_strlen_or_indptr;
    int			m_cnt;
  } DynamicParam;

  class BoundColumnIlwd {
  public:
    BoundColumnIlwd(ILwd::LdasElement* Base,
		    const dbEasyStmt& Query,
		    SQLUSMALLINT ColumnNumber,
		    bool Own);
    BoundColumnIlwd(const BoundColumnIlwd& Source);
    ~BoundColumnIlwd(void);
    /// Append data to the cache.
    bool AppendBufferToCache(void);
    void Bind(void);
    bool CacheToIlwd(void);
    void ExpandCache(int NewSize, bool Shrink = false);
    /// Retrieve the Ilwd base of where the data is stored.
    ILwd::LdasElement* GetBase(void) const;
    SQLPOINTER GetBuffer(void);
    SQLUSMALLINT GetColumn(void) const;
    const General::bit_vector& GetMask(void) const;
    SQLINTEGER GetRetSize(void) const;
    bool IsOk(void) const;
    void RelinquishOwnership( );
    void Reset(void);
    void ResetBase(ILwd::LdasElement* NewBase);

  private:
    ILwd::LdasElement*	m_base;		// Pointer to the ILWD class
    SQLPOINTER		m_buffer;	// Temp storage for C type
    SQLUSMALLINT	m_col;		// Column to which ilwd is bound
    SQLSMALLINT		m_ctype;	// Data type
    SQLSMALLINT		m_dtype;
    SQLUINTEGER		m_size;		// Size of data refrenced by m_buffer
    const dbEasyStmt&	m_stmt;		// Reference to the statement handle.
    SQLLEN		m_strlen_or_indptr;
    SQLPOINTER		m_cache;
    SQLINTEGER		m_cache_size;
    SQLINTEGER		m_cache_pos;
    bool		m_own_base;
    General::bit_vector		m_null_mask;

    void append_buffer_to_container(void);
    void convert_null_data(void);
  };

  class BoundParamIlwd {
  private:
    const SQLUSMALLINT	m_param;
    SQLSMALLINT		m_ctype;
    SQLSMALLINT		m_dtype;
    SQLPOINTER		m_data;
    SQLPOINTER		m_rgbValue;
    SQLUINTEGER		m_param_size;
    SQLLEN FAR*		m_str_len;
    dbEasyStmt&		m_stmt;		// Reference to the statement handle.
    std::list<IlwdInfo>	m_put_data;

  public:
    BoundParamIlwd(ILwd::LdasElement* Base,
		   dbEasyStmt& Stmt,
		   SQLUSMALLINT	ParamNum);
    ~BoundParamIlwd(void);
    bool Bind(void);
    SQLPOINTER GetRGBValue(void) const;
    void PutData(void);

  private:
    void* fast_container_mover( ILwd::LdasContainer* Container,
				SQLLEN FAR* StrLen );
  };

  friend class BoundColumnIlwd;
  friend class BoundParamIlwd;

  // Thread locks
  pthread_mutex_t	m_fetch_lock;

  // Thread key
  static const char*		m_multifetch_key;
  static const char*		m_submit_key;

  // Reference to the dbEasy class
  const dbEasy&			m_env;
  // Query to execute.
  dbQuery			m_query;
  // Number of columns in the result set
  int				m_col_count;
  SQLINTEGER*			m_data_len;
  SQLPOINTER*			m_data_pointers;
  std::list<SQLPOINTER>		m_param_pointers;
  SQLUINTEGER			m_row_array_size;
  SQLINTEGER			m_row_bind_pos;
  //: List of bounded ILWD columns
  std::list<BoundColumnIlwd>		m_col_bindings;
  //: List of bound ILWD parameters
  std::list<BoundParamIlwd>		m_param_bindings;
  std::vector<ILwd::LdasElement*>	m_elements;
  std::vector<DynamicParam>		m_dynamic_params;
  // List of objects that have been malloced.
  std::list<void*>			m_mallocs;
#ifndef GLOBAL_REGISTRY
  std::list<ILwd::LdasElement*>	m_registered_objects;
#endif
  int				m_query_bound_size;

  //: Extend cache for result.
  void cache_extend(int NewSize);
  //: Reset the cache to zero
  void cache_reset(void);
  //: Move data from the cache to ILwds
  void cache_to_ilwd(void);
  //: Copy data into an an ILwd
  template <class T,class T2> bool copy_array(T* Data,
					      const T2* Buffer,
					      int Size = 1);
  //: Convert ODBC C Type to ILwd type
  static int cnvt_ctype_to_ilwdtype(int CType);
  //: Convert ODBC SQL Type to ODBC C Type
  static int cnvt_dtype_to_ctype(int DType);
  //: Find appropriate ODBC C Type for a specified ILwd.
  static int cnvt_ilwd_to_ctype(const ILwd::LdasElement* Base,
				int DefaultCType);
  //: Find appropriate ODBC SQL Type for a specified ILwd.
  static int cnvt_ilwd_to_dtype(const ILwd::LdasElement* Base);
  //: Retrieve the dimension of the specified ILwd.
  SQLUINTEGER get_dim(ILwd::LdasElement* Base) const;
  //: Retrieve information about the given ILwd.
  IlwdInfo get_ilwd_info(ILwd::LdasElement* Base);
  //: Retrieve the column size.
  SQLUINTEGER get_csize(int ILWDType, SQLUSMALLINT Col) const;
  //: Create an ILwd.
  ILwd::LdasElement* make_ilwd(int Column, SQLINTEGER Size,
			       bool Register = true);
  //: Copy single query result into buffer.
  void post_fetch_copy(SQLUSMALLINT Column, unsigned int Offset,
		       SQLINTEGER RetSize,
		       ILwd::LdasElement* Base, void* Buffer);
  //: Determine if and ODBC call succeeded or failed
  void eval_retcode(std::string Filename, int LineNumber, SQLRETURN Retcode);
  //: Release memory that was previous malloced
  void dealloc_buffers(void);
#ifndef GLOBAL_REGISTRY
  //: Register object with TCL
  void register_obj(ILwd::LdasElement* Elem);
  //: Unregister object with TCL
  void unregister_obj(ILwd::LdasElement* Elem);
  //: Unregister all objects with TCL
  void unregister_obj(void);
#endif /* GLOBAL_REGISTRY */
  //: Allow the backgrounding of FetchMulti
  static bool fetchmulti_bg( dbEasyStmt* Easy, int MaxRows, int Inc );
  //: Submit query for execution in the background.
  static void submit_bg(dbEasyStmt* Easy, std::string Query);
  //: Tag column data with m_zero_row_key
  void zero_set( bool Register );
};

inline ILwd::LdasElement* dbEasyStmt::BoundColumnIlwd::
GetBase(void) const
{
  return m_base;
}

inline const General::bit_vector& dbEasyStmt::BoundColumnIlwd::
GetMask(void) const
{
  return m_null_mask;
}

inline SQLINTEGER dbEasyStmt::BoundColumnIlwd::
GetRetSize(void) const
{
  return m_strlen_or_indptr;
}

inline void dbEasyStmt::BoundColumnIlwd::
RelinquishOwnership( )
{
  m_own_base = false;
}

inline void dbEasyStmt::BoundColumnIlwd::
Reset(void)
{
  m_cache_pos = 0;
}

inline SQLPOINTER dbEasyStmt::BoundColumnIlwd::
GetBuffer(void)
{
  if (m_strlen_or_indptr == SQL_NULL_DATA)
  {
    convert_null_data();
  }
  return m_buffer;
}

inline SQLUSMALLINT dbEasyStmt::BoundColumnIlwd::
GetColumn(void) const
{
  return m_col;
}

inline bool dbEasyStmt::BoundColumnIlwd::
IsOk(void) const
{
  if (!m_buffer) return false;
  return true;
}

inline SQLPOINTER dbEasyStmt::BoundParamIlwd::
GetRGBValue(void) const
{
  return m_rgbValue;
}

inline void dbEasyStmt::
BindParameter(SQLUSMALLINT ParamNum, SQLSMALLINT CType, SQLPOINTER Data,
	      SQLUINTEGER ParamSize, SQLSMALLINT DataType,
	      SQLLEN *FAR StrLen_or_IndPtr)
{
  return m_query.BindParameter(ParamNum, CType, Data, ParamSize, DataType,
			       StrLen_or_IndPtr);
}

inline std::string dbEasyStmt::
GetColumnName(int Column) const
{
  return m_query.GetColumnName(Column-1);
}

inline SQLUINTEGER dbEasyStmt::
GetColumnSize(int Column) const
{
  return m_query.GetColumnSize(Column-1);
}

inline int dbEasyStmt::
GetNumberOfColumns(void) const
{
  return m_col_count;
}

inline int dbEasyStmt::
GetNumberOfParameters(void) const
{
  return m_query.GetParamCount();
}

inline int dbEasyStmt::
GetNumberOfRows(void) const
{
  return m_query.GetRowCount();
}

#endif	/* DBEASY_H */
