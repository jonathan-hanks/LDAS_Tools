#include "config.h"

#include <stdio.h>
#include <stdlib.h>

#include "ilwd/ldasarray.hh"
#include "ilwd/ldasstring.hh"

#include "odbc.h"
#include "dberr.hh"
#include "dbeasy.h"

const string dbligo("cit_1_ol7");

main(int argc, char** argv)
{
  //---------------------------------------------------------------------
  // Basic initialization of the testing system.
  //---------------------------------------------------------------------
  string	dbname;
  string	user;
  string	password;
  string	dbquery;
  int		queryid = -1;

  if (argc > 1)
  {
    dbname = "";
    dbquery = "";
    if (0 == strcmp(argv[1], "-ligo1"))
    {
      dbname = dbligo;
      dbquery = "select * from frameset";
    }
    else if (0 == strcmp(argv[1], "-ligo2"))
    {
      dbname = dbligo;
      dbquery =
	"select * from frameset"
	"where frameset_time = '1979-01-18-21.51.15.000000";
    }
    else if (0 == strcmp(argv[1], "-ligo3"))
    {
      dbname = dbligo;
      dbquery =
	"select * from statistics"
	"where frameset_time = '1979-01-18-21.51.15.000000";
    }
    else if (0 == strcmp(argv[1], "-ligo4"))
    {
      queryid = 4;
      dbname = dbligo;
      dbquery =
	"insert into datasource"
	" (channel_id, frame_id, offset, time_span, online_flag, datasource_id)"
	" values (?,?,?,?,?, generate_unique())";
    }
    else if (0 == strcmp(argv[1], "-ligo4d"))	// deletes the row
    {
      dbname = dbligo;
      dbquery =
	"delete from datasource where channel_id = 'channel_id__0087'";
    }
    else if (0 == strcmp(argv[1], "-ligo4r"))	// retrieves the row
    {
      dbname = dbligo;
      dbquery =	"select * from datasource";
    }
    else if (0 == strcmp(argv[1], "-ligo4s"))	// static insert of row
    {
      dbname = dbligo;
      dbquery =
	"insert into datasource"
	" (channel_id, frame_id, offset, time_span, online_flag, datasource_id)"
	" values ('channel_id__0087','LLH_293919176.raw',2,0,0, generate_unique())";
    }
    else if (0 == strcmp(argv[1], "-ligo5"))
    {
      queryid = 5;
      dbname = dbligo;
      dbquery =
	"insert into filter"
	" (type, filter_id)"
	" values (?, ?)";
    }
    else if (0 == strcmp(argv[1], "-ligo5s"))
    {
      dbname = dbligo;
      dbquery =
	"insert into filter"
	" (type, filter_id)"
	" values ('dummy type', generate_unique())";
    }
    else if (0 == strcmp(argv[1], "-ligo_bad1"))
    {
      dbname = dbligo;
      dbquery = "select * from framset";
    }
  }
  if (dbname.size() <= 0)
  {
    cout << "Enter Database Name: ";
    cin >> dbname;
  }
  cout << "Enter User Name    : ";
  cin >> user;
  cout << "Enter Password     : ";
  cin >> password;

  //-------------------------------------------------------------------
  // Show how easy it is to use dbEasy class
  //-------------------------------------------------------------------
  try
  {
    dbEasy	easyDB(dbname, user, password);

    try {
      dbEasyStmt	easy(easyDB);

      std::cout << "Have opended the database." << std::endl;
      switch(queryid)
      {
      case 4:
	{
	  static ILwd::LdasString channel_id("channel_id__0087");
	  static ILwd::LdasString frame_id("LLH_293919176.raw");
	  static ILwd::LdasArray<INT_4S> offset(2);
	  static ILwd::LdasArray<INT_4S> time_span(0);
	  static ILwd::LdasArray<INT_2S> online_flag(0);

	  cout << "Prepare" << endl;
	  easy.Prepare(dbquery);
	  cout << "BindParameters - ILWD_FORMAT" << endl;
          easy.BindParameter(1, &channel_id);
	  easy.BindParameter(2, &frame_id);
	  easy.BindParameter(3, &offset);
	  easy.BindParameter(4, &time_span);
	  easy.BindParameter(5, &online_flag);
	  cout << "Submit:" << dbquery << endl;
	  easy.Submit();
	}
	break;
      case 5:
	{
	  static const char* type_str[2] = {"dummy type","dummy type2"};
	  static const char* unique_id_str[2] = {"x'19991001214910400194000000'", "x'19991001214910443662000000'"};
	  static ILwd::LdasString type_name(type_str, 2);
	  static ILwd::LdasString unique_id(unique_id_str, 2);
	  cout << "Prepare" << endl;
	  easy.Prepare(dbquery);
	  cout << "BindParameters - ILWD_FORMAT" << endl;
          easy.BindParameter(1, &type_name);
          easy.BindParameter(2, &unique_id);
	  cout << "Submit:" << dbquery << endl;
	  easy.Submit();
	}
	break;
      default:
	cout << "Executing query: " << dbquery << endl;
	easy.Submit(dbquery);
	break;
      }
      cout << "Fetch" << endl;
      while(easy.Fetch())
      {
	cout << "Above GetNumberOfColumns(): " << dbquery << endl;
	for (int i = 0; i < easy.GetNumberOfColumns(); i++)
	{
	cout << "Above GetDataBuffer(): " << dbquery << endl;
	    cout << (char*)(easy.GetDataBuffer(i)) << " ";
	}
	std::cout << std::endl;
      }
      cout << std::endl
	   << "Rows affected: " << easy.GetNumberOfRows() << std::endl;
    }
    catch (dbErr& err)
    {
      cerr << "Display error message(s): dbEasyStmt" << endl;
      err.Print();
    }
  }
  catch (dbErr& err)
  {
    cerr << "Display error message(s): dbEasy" << endl;
    err.Print();
  }
}
