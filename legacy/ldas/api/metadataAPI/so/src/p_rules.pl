#!/usr/bin/env perl
#------------------------------------------------------------------------
# Glue the Method return declaration and class to the method name
#------------------------------------------------------------------------
$block =~ s/^(.+::)\n/$1/mg;
