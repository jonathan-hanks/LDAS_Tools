/* -*- C++ -*- */
#ifdef __GNUG__
#pragma implementation "dbeasystmt.h"
#endif	/* __GNUG__ */

#include "config.h"

#include <sys/types.h>
#include <ios>
#include <cstdlib>
#include <unistd.h>
#include <cstring>
#include <sstream>
#include <stdexcept>

#include "dberr.hh"
#include "dbeasy.h"
#include "dbeasystmt.h"

#include "general/types.hh"
#include "general/mutexlock.hh"
#include "general/autoarray.hh"

#include "ilwd/ldaselement.hh"
#include "ilwd/ldasarray.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasstring.hh"
#include "ilwd/errors.hh"

#include "genericAPI/registry.hh"
#include "genericAPI/thread.hh"
   
using namespace std;   

#if 0
#define	AT(msg) cout << msg << " - at - " << __FILE__ << ":" << __LINE__ << endl << flush

#else	/* MEM_DEBUG */
#define	AT(msg)
#endif	/* MEM_DEBUG */

#if GLOBAL_REGSITRY
struct element_data_type {
  ILwd::LdasElement*	s_element;
  dbEasyStmt*		s_stmt;

  inline element_data_type( ILwd::LdasElement* Element,
			 dbEasyStmt* Stmt )
    : s_element( Element ),
      s_stmt( Stmt )
  {
  }
};

class element_data_list: public ObjectRegistry< element_data_type >
{
public:
  typedef ObjectRegistry< element_data_type >::const_iterator const_iterator;

  element_data_type* GetElementData( const ILwd::LdasElement* Element );
  void GetElementData( const dbEasyStmt* Stmt,
		       std::list< element_data_type*>& Matches );
};

class element_list
{
public:
  void Register( ILwd::LdasElement* Element, dbEasyStmt* Stmt );
  void Unregister( ILwd::LdasElement* Element );
  void Unregister( dbEasyStmt* Stmt );

private:
  element_data_list	m_elements;
} theElementList;
#endif	/* GLOBAL_REGISTRY */

template <class T>
inline ILwd::LdasArray<T>* new_ldas_array(size_t Size, std::string Name)
{
  if (Size > 0)
  {
    General::AutoArray< T > b( Size == 0 ? 0 : ( new T[Size] ) );
    return new ILwd::LdasArray< T >(b.get(), Size, Name);
  }
  return new ILwd::LdasArray< T >((T *)NULL, Size, Name);
}

///======================================================================
/// dbEasyStmt::BoundColumnIlwd
///======================================================================

//!param: ILwd::LdasElement* Base - ILwd element containing the data to be
//+       bound.
//!param: const dbEasyStmt& Stmt - Statement handle containning the query.
//!param: SQLUSMALLINT Col - Column offset (starting from 1)
dbEasyStmt::BoundColumnIlwd::
BoundColumnIlwd(ILwd::LdasElement* Base,
		const dbEasyStmt& Stmt,
		SQLUSMALLINT	Col,
		bool Own)
  : m_base(Base),
    m_buffer((void*)NULL),
    m_col(Col),
    m_size(0),
    m_stmt(Stmt),
    m_cache((SQLPOINTER*)NULL),
    m_cache_size(0),
    m_cache_pos(0),
    m_own_base(Own)

{
}

dbEasyStmt::BoundColumnIlwd::
BoundColumnIlwd(const BoundColumnIlwd& Source)
  : m_base(Source.m_base),
    m_buffer(Source.m_buffer),
    m_col(Source.m_col),
    m_size(Source.m_size),
    m_stmt(Source.m_stmt),
    m_cache(Source.m_cache),
    m_cache_size(Source.m_cache_size),
    m_cache_pos(Source.m_cache_pos),
    m_own_base(Source.m_own_base)
{
  //:TRICKY: Source needs to be constant for other functions to
  //:TRICKY:   work properly. Ownership of m_base transfers with
  //:TRICKY:   copy constructor.
  const_cast<BoundColumnIlwd&>(Source).m_own_base = false;
}

dbEasyStmt::BoundColumnIlwd::
~BoundColumnIlwd(void)
{
  if (m_buffer) 
  {
    free(m_buffer);
    m_buffer = (void*)NULL;
  }
  if (m_cache)
  {
    free(m_cache);
    m_cache = (SQLPOINTER*)NULL;
  }
  // :TODO: Need to create an accurate copy constructor first.
  if (m_base && m_own_base)
  {
#ifdef GLOBAL_REGISTRY
    theElementList.Unregister( m_base );
#endif /* GLOBAL_REGISTRY */
    delete m_base;
    m_base = (ILwd::LdasElement*)NULL;
  }
}

//!return: bool - true if buffer was appened, false otherwise.
bool dbEasyStmt::BoundColumnIlwd::
AppendBufferToCache(void)
{
  bool	retval(false);
  if (m_strlen_or_indptr == SQL_NULL_DATA)
  {
    m_null_mask.resize(m_cache_pos+1);
    m_null_mask[m_cache_pos] = true;
    convert_null_data();
  }
  switch(m_base->getElementId())
  {
  case ILwd::ID_EXTERNAL:
    AT("Unsupported type");
    break;
  case ILwd::ID_ILWD:
    append_buffer_to_container();
    break;
  case ILwd::ID_LSTRING:
    {
      std::string		str((CHAR*)m_buffer, m_strlen_or_indptr);
      ILwd::LdasString*	lstr =
	dynamic_cast<ILwd::LdasString*>(m_base);

      if (lstr)
      {
	lstr->push_back(str);
	m_cache_pos++;
      }
    }
    break;
  case ILwd::ID_CHAR:
  case ILwd::ID_CHAR_U:
    {
      if (m_cache_pos)
      {
	ILwd::LdasContainer*	nc = new ILwd::LdasContainer;
      
	// Recast into a container.
	if (nc->size() == 1)
	{
	  nc->pop_back();
	}
	if (m_base->getElementId() == ILwd::ID_CHAR)
	{
	  std::string	str((const CHAR*)m_cache, m_size);
	  m_base = new ILwd::LdasArray<CHAR>(str.c_str(), str.length());
	}
	else if (m_base->getElementId() == ILwd::ID_CHAR_U)
	{
	  m_base = new ILwd::LdasArray<CHAR_U>((const CHAR_U*)m_cache, m_size);
	}
	nc->push_back(m_base);
	m_base = nc;
	retval = true;
	append_buffer_to_container();
      }
      else
      {
	//---------------------------------------------------------------
	// copy the data from m_buffer into the cache and then advance
	// the cahce position pointer by one.
	//---------------------------------------------------------------
	bcopy(m_buffer, &(((char*)m_cache)[m_cache_pos * m_size]), m_size);
	m_cache_pos++;
      }
    }

    break;
  case ILwd::ID_INT_2S:
  case ILwd::ID_INT_2U:
  case ILwd::ID_INT_4S:
  case ILwd::ID_INT_4U:
#ifdef SQL_C_SBIGINT
  case ILwd::ID_INT_8S:
#endif
#ifdef SQL_C_UBIGINT
  case ILwd::ID_INT_8U:
#endif
  case ILwd::ID_REAL_4:
  case ILwd::ID_REAL_8:
    if (m_cache_size > m_cache_pos)
    {
      //---------------------------------------------------------------
      // copy the data from m_buffer into the cache and then advance
      // the cahce position pointer by one.
      //---------------------------------------------------------------
      bcopy(m_buffer, &(((char*)m_cache)[m_cache_pos * m_size]), m_size);
      m_cache_pos++;
    }
    else
    {
      // :TODO: throw an exception
    }
    break;
  default:
    // :TODO: Throw an exception
    break;
  }
  return retval;
}

void dbEasyStmt::BoundColumnIlwd::
Bind(void)
{
  m_dtype = m_stmt.m_query.GetColumnType( GetColumn( ) - 1 );
  m_ctype = dbEasyStmt::cnvt_dtype_to_ctype(m_dtype );
  m_ctype = dbEasyStmt::cnvt_ilwd_to_ctype(m_base, m_ctype);
				      
  m_size = m_stmt.get_csize(m_stmt.cnvt_ctype_to_ilwdtype(m_ctype), m_col);

  if (m_size)
  {
    m_buffer = (void*)malloc(m_size);
  }
  else
  {
    return;
  }
  ((dbEasyStmt&)m_stmt).m_query
    .Bind(m_col-1, m_ctype, m_buffer, m_size, &m_strlen_or_indptr);
}

//!return: bool - true if cache data was converted to ILWD, false otherwise.
bool dbEasyStmt::BoundColumnIlwd::
CacheToIlwd(void)
{
  bool retval(true);
  void*	buffer((m_cache_pos == 0) ? (void*)NULL : m_cache);

  switch(m_base->getElementId())
  {
  case ILwd::ID_EXTERNAL:
  case ILwd::ID_ILWD:
  case ILwd::ID_LSTRING:
    retval = false;
    break;
  case ILwd::ID_CHAR:
    m_base = new ILwd::LdasArray<CHAR>((const CHAR*)buffer, m_cache_pos);
    break;
  case ILwd::ID_CHAR_U:
    m_base = new ILwd::LdasArray<CHAR_U>((const CHAR_U*)buffer, m_cache_pos);
    break;
 case ILwd::ID_INT_2S:
    m_base = new ILwd::LdasArray<INT_2S>((const INT_2S*)buffer, m_cache_pos);
    break;
  case ILwd::ID_INT_2U:
    m_base = new ILwd::LdasArray<INT_2U>((const INT_2U*)buffer, m_cache_pos);
    break;
  case ILwd::ID_INT_4S:
    m_base = new ILwd::LdasArray<INT_4S>((const INT_4S*)buffer, m_cache_pos);
    break;
  case ILwd::ID_INT_4U:
    m_base = new ILwd::LdasArray<INT_4U>((const INT_4U*)buffer, m_cache_pos);
    break;	
#ifdef SQL_C_SBIGINT
  case ILwd::ID_INT_8S:
    m_base = new ILwd::LdasArray<INT_8S>((const INT_8S*)buffer, m_cache_pos);
    break;
#endif	/* SQL_C_SBIGINT */
#ifdef SQL_C_UBIGINT
  case ILwd::ID_INT_8U:
    m_base = new ILwd::LdasArray<INT_8U>((const INT_8U*)buffer, m_cache_pos);
    break;
#endif	/* SQL_C_UBIGINT */
  case ILwd::ID_REAL_4:
    m_base = new ILwd::LdasArray<REAL_4>((const REAL_4*)buffer, m_cache_pos);
    break;
  case ILwd::ID_REAL_8:
    m_base = new ILwd::LdasArray<REAL_8>((const REAL_8*)buffer, m_cache_pos);
    break;
  default:
    AT("Unsupported type");
    retval = false;
    break;
  }
  return retval;
}

//!param: int NewSize - Requested new size of cache.
//!param: bool Shrink - true if the cache should be resized to a smaller
//+       value if NewSize is less than the current cache size, false
//+       otherwise.
void dbEasyStmt::BoundColumnIlwd::
ExpandCache(int NewSize, bool Shrink)
{
  if (m_cache_size == NewSize) return;
  if ((m_cache_size > NewSize) && (!Shrink)) return;
  switch(m_base->getElementId())
  {
  case ILwd::ID_EXTERNAL:
  case ILwd::ID_ILWD:
  case ILwd::ID_LSTRING:
    return;
  default:
    break;
  }
  void* result = realloc(m_cache, NewSize*m_size);
  if ( result == (void*)NULL )
  {
    throw std::bad_alloc( );
  }
  m_cache = result;
  m_cache_size = NewSize;
  if (m_cache_pos > m_cache_size)
  {
    m_cache_pos = m_cache_size;
  }
}

void dbEasyStmt::BoundColumnIlwd::
append_buffer_to_container(void)
{
  ILwd::LdasContainer* c = dynamic_cast<ILwd::LdasContainer*>(m_base);

  if (c)
  {
    void*	ptr((m_strlen_or_indptr > 0) ? m_buffer : (void*)NULL);

    int	ilwd_type = (c->size() > 0)
      ? (*c)[0]->getElementId() 
      : cnvt_ctype_to_ilwdtype(m_ctype);
    switch(ilwd_type)
    {
    case ILwd::ID_CHAR:
      c->push_back( new ILwd::LdasArray<CHAR>((CHAR*)ptr,
						m_strlen_or_indptr),
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
      break;
    case ILwd::ID_CHAR_U:
      c->push_back( new ILwd::LdasArray<CHAR_U>((CHAR_U*)ptr,
						m_strlen_or_indptr),
		    ILwd::LdasContainer::NO_ALLOCATE,
		    ILwd::LdasContainer::OWN );
      break;
    case ILwd::ID_LSTRING:
      {
	std::string	str((CHAR*)m_buffer, m_strlen_or_indptr);
	c->push_back(ILwd::LdasString(str));
      }
      break;
    default:
      AT("Unsupported type");
      break;
    }
    m_cache_pos++;
  }
}

void dbEasyStmt::BoundColumnIlwd::
ResetBase(ILwd::LdasElement* NewBase)
{
  if (m_base)
  {
#ifdef GLOBAL_REGISTRY
    theElementList.Unregister( m_base );
#endif	/* GLOBAL_REGISTRY */
    delete m_base;
  }
  m_base = NewBase;
}

void dbEasyStmt::BoundColumnIlwd::
convert_null_data(void)
{
  SQLINTEGER	new_size = 0;
  int		id = m_base->getElementId();

  if (id == ILwd::ID_ILWD)
  {
    const ILwd::LdasContainer* c =
      dynamic_cast<const ILwd::LdasContainer*>(m_base);
    if ((c) && c->size()) 
    {
      id =  ((*c)[0])->getElementId();
    }
    else
    {
      id = m_stmt.cnvt_ctype_to_ilwdtype(m_ctype);
    }
  }
  switch(id)
  {
  case ILwd::ID_LSTRING:
    *((CHAR*)m_buffer) = '\0';
    break;
  case ILwd::ID_CHAR:
    *((CHAR*)m_buffer) = '@';
    break;
  case ILwd::ID_CHAR_U:
    *((CHAR_U*)m_buffer) = '@';
    break;
  case ILwd::ID_INT_2S:
    (*(INT_2S*)m_buffer) = 0;
    break;
  case ILwd::ID_INT_2U:
    (*(INT_2U*)m_buffer) = 0;
    break;
  case ILwd::ID_INT_4S:
    (*(INT_4S*)m_buffer) = 0; 
    break;
  case ILwd::ID_INT_4U:
    (*(INT_4U*)m_buffer) = 0;
    break;
#ifdef SQL_C_SBIGINT
  case ILwd::ID_INT_8S:
    (*(INT_8S*)m_buffer) = 0;
    break;
#endif
#ifdef SQL_C_UBIGINT
  case ILwd::ID_INT_8U:
    (*(INT_8U*)m_buffer) = 0;
    break;
#endif
  case ILwd::ID_REAL_4:
    {
      double nan = atof("NaN");
      (*(REAL_4*)m_buffer) = nan;
    }
    break;
  case ILwd::ID_REAL_8:
    {
      double nan = atof("NaN");
      (*(REAL_8*)m_buffer) = nan;
    }
    break;
  default:
    new_size = m_strlen_or_indptr;
    break;
  }
  m_strlen_or_indptr = new_size;
}

//!param: ILwd::LdasElement* Base - ILwd element containing the data to be
//+       parameter bound.
//!param: dbEasyStmt& Stmt - Stateemnt handle containing the query.
//!param: SQUSMALLINT ParamNum - Parameter offset (starting from 1).
dbEasyStmt::BoundParamIlwd::
BoundParamIlwd(ILwd::LdasElement* Base,
	       dbEasyStmt& Stmt,
	       SQLUSMALLINT ParamNum)
  : m_param(ParamNum),
    m_ctype(dbEasyStmt::cnvt_ilwd_to_ctype(Base, SQL_C_DEFAULT)),
    m_dtype(dbEasyStmt::cnvt_ilwd_to_dtype(Base)),
    m_str_len((SQLLEN FAR*)NULL),
    m_stmt(Stmt)
{
  m_str_len = (SQLLEN *FAR)calloc( m_stmt.m_row_array_size,
				   sizeof(SQLLEN) );
  m_stmt.m_mallocs.push_back(m_str_len);
  switch(Base->getElementId())
  {
  case ILwd::ID_EXTERNAL:
    AT("Unsupported type");
    break;
  case ILwd::ID_ILWD:
    {
      ILwd::LdasContainer* a = dynamic_cast<ILwd::LdasContainer*>(Base);

      if (!a)
      {
	return;
      }
      //-----------------------------------------------------------------
      // Prepare for the worst case where the server will be making
      //	requests for each of the container elements.
      //-----------------------------------------------------------------
      m_param_size = 0;
      for (unsigned int x = 0; x < m_stmt.m_row_array_size; x++)
      {
	if (Base->isNull(x))
	{
	  m_str_len[x] = SQL_NULL_DATA;
	}
	else
	{
	  m_str_len[x] = SQL_DATA_AT_EXEC;
	}
	m_put_data.push_back(m_stmt.get_ilwd_info((*a)[x]));
	if (m_put_data.back().m_free_pointer)
	{
	  m_stmt.m_mallocs.push_back(m_put_data.back().m_data);
	}
	if (m_put_data.back().m_param_size > m_param_size)
	{
	  m_param_size = m_put_data.back().m_param_size;
	}
      }
      //-----------------------------------------------------------------
      // try to better the situation by moving data into a block.
      //-----------------------------------------------------------------
      m_stmt.m_mallocs.push_back(m_data = fast_container_mover(a, m_str_len));
    }
    break;
  case ILwd::ID_CHAR:
    {
      ILwd::LdasArray<CHAR>* a = dynamic_cast<ILwd::LdasArray<CHAR>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = a->getDimension(0);
    }
    break;
  case ILwd::ID_CHAR_U:
    {
      ILwd::LdasArray<CHAR_U>* a = dynamic_cast<ILwd::LdasArray<CHAR_U>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = a->getDimension(0);
    }
    break;
  case ILwd::ID_INT_2S:
    {
      ILwd::LdasArray<INT_2S>* a = dynamic_cast<ILwd::LdasArray<INT_2S>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = sizeof(INT_2S);
    }
    break;
  case ILwd::ID_INT_2U:
    {
      ILwd::LdasArray<INT_2U>* a = dynamic_cast<ILwd::LdasArray<INT_2U>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = sizeof(INT_2U);
    }
    break;
  case ILwd::ID_INT_4S:
    {
      ILwd::LdasArray<INT_4S>* a = dynamic_cast<ILwd::LdasArray<INT_4S>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = sizeof(INT_4S);
    }
    break;
  case ILwd::ID_INT_4U:
    {
      ILwd::LdasArray<INT_4U>* a = dynamic_cast<ILwd::LdasArray<INT_4U>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = sizeof(INT_4U);
    }
    break;
#ifdef SQL_C_SBIGINT
  case ILwd::ID_INT_8S:
    {
      ILwd::LdasArray<INT_8S>* a = dynamic_cast<ILwd::LdasArray<INT_8S>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = sizeof(INT_8S);
    }
    break;
#endif
#ifdef SQL_C_UBIGINT
  case ILwd::ID_INT_8U:
    {
      ILwd::LdasArray<INT_8U>* a = dynamic_cast<ILwd::LdasArray<INT_8U>*>(Base);
      if (!a) return;
      m_data = a->getData(); 
      m_param_size = sizeof(INT_8U);
    }
    break;
#endif
  case ILwd::ID_REAL_4:
    {
      ILwd::LdasArray<REAL_4>* a = dynamic_cast<ILwd::LdasArray<REAL_4>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = sizeof(REAL_4);
    }
    break;
  case ILwd::ID_REAL_8:
    {
      ILwd::LdasArray<REAL_8>* a = dynamic_cast<ILwd::LdasArray<REAL_8>*>(Base);
      if (!a) return;
      m_data = a->getData();
      m_param_size = sizeof(REAL_8);
    }
    break;
  case ILwd::ID_LSTRING:
    {
      char	*offset;
      bool	x_prime = false;
      int	array_offset = 0;
      
      m_param_size = 0;
      ILwd::LdasString* a = dynamic_cast<ILwd::LdasString*>(Base);
      if (!a) return;
      for (std::vector<std::string>::const_iterator i = a->begin();
	   i != a->end() && !x_prime;
	   i++)
      {
	unsigned int	len = (*i).length();
	// Must have at least 2 chars to pass this check
        if( len >= 2 && (*i)[0] == 'x' && (*i)[1] == '\'')
        {
	   x_prime = true;
	   len = (len-3) / 2;
	}

	if (len > m_param_size) m_param_size = len;
      }
      if (x_prime)
      {
	m_ctype = SQL_C_BINARY;
	m_dtype = SQL_DEFAULT;
      }
      else
      {
	m_param_size++;
      }
 
      m_data = (SQLPOINTER)calloc(m_stmt.m_row_array_size, m_param_size);
      m_stmt.m_param_pointers.push_front(m_data);
      offset = (char*)m_data;
      for (std::vector<std::string>::const_iterator i = a->begin();
	   i != a->end();
	   i++)
      {
	if (x_prime)
	{
	  int	end = (*i).length()-1;
	  int	y = 0;
	  unsigned char	buf;

	  for (int x = 2; x < end; x += 2)
	  {
	    buf = ((*i)[x] - '0') << 4;
	    buf += (*i)[x+1] - '0';
	    offset[y++] = buf;
	  }
	  m_str_len[array_offset++] = m_param_size;
	}
	else
	{
	  bcopy ((*i).c_str(), offset, (*i).length() + 1);
	  m_str_len[array_offset++] = (*i).length();
	}
	offset += m_param_size; 
      }
    }
    break;
  default:
    //-------------------------------------------------------------------
    // :TODO: Need to throw an exception to let anyone who cares know
    // :TODO:   that no binding really took place.
    //-------------------------------------------------------------------
    return;
  }
  //---------------------------------------------------------------------
  // Handle null data
  //---------------------------------------------------------------------
  for (size_t x = 0; x < m_stmt.m_row_array_size; x++)
  {
    if (Base->isNull(x))
    {
      m_str_len[x] = SQL_NULL_DATA;
    }
  }
  //---------------------------------------------------------------------
  // 
  //---------------------------------------------------------------------
  Bind();	// Do the first one to get any error messages
}

dbEasyStmt::BoundParamIlwd::
~BoundParamIlwd(void)
{
}

bool dbEasyStmt::BoundParamIlwd::
Bind(void)
{
  SQLINTEGER	size;
  SQLPOINTER	data;

  if (m_put_data.size() > 0)
  {
    size = m_put_data.front().m_param_size;
    data = m_put_data.front().m_data;
  }
  else
  {
    size = m_param_size;
    data = m_data;
  }
  m_stmt.BindParameter(m_param-1, m_ctype, data, size,
		       m_dtype, m_str_len);
  m_rgbValue = data;
  // Incriment to the next data
  m_data = (char*)m_data + m_param_size;
  return true;
}

void dbEasyStmt::BoundParamIlwd::
PutData(void)
{
  m_stmt.eval_retcode(__FILE__, __LINE__,
		      SQLPutData(m_stmt.m_query.GetHstmt(),
				 m_put_data.front().m_data,
				 m_put_data.front().m_param_size));
  m_put_data.pop_front();
}

void* dbEasyStmt::BoundParamIlwd::
fast_container_mover( ILwd::LdasContainer* Container, SQLLEN FAR* StrLen )
{
  void* block = (void*)calloc(m_stmt.m_row_array_size,
			      m_param_size * sizeof(char));
  if (block)
  {
    char* block_ptr = (char*)block;
    unsigned int x = 0;
    for (std::list<IlwdInfo>::const_iterator ii = m_put_data.begin();
	 ii != m_put_data.end();
	 ii++, block_ptr += m_param_size, x++)
    {
      if (StrLen[x] == SQL_NULL_DATA) continue;
      StrLen[x] = (*ii).m_param_size;
      memcpy(block_ptr, (*ii).m_data, (*ii).m_param_size);
    }
    m_put_data.erase(m_put_data.begin(), m_put_data.end());
  }
  return block;
}

//=======================================================================
// dbEasyStmt::IlwdInfo
//=======================================================================

dbEasyStmt::IlwdInfo::
IlwdInfo(SQLPOINTER Data, SQLUINTEGER Size, bool FreePointer)
  : m_data(Data),
    m_param_size(Size),
    m_free_pointer(FreePointer)
{
}

dbEasyStmt::IlwdInfo::
~IlwdInfo(void)
{
}

//=======================================================================
// dbEasyStmt::
//=======================================================================

const char* dbEasyStmt::m_multifetch_key("dbEasyStmt::FetchMulti");
const char* dbEasyStmt::m_submit_key("dbEasyStmt::Submit");


// Perform basic initialization of the class
//!param: const dbEasy& Env - Environment handle to use
dbEasyStmt::
dbEasyStmt(const dbEasy& Env)
  : m_env(Env),
    m_query(Env.GetDB()),
    m_col_count(0),
    m_data_len((SQLINTEGER*)NULL),
    m_data_pointers((SQLPOINTER*)NULL),
    m_query_bound_size(0)

{
  pthread_mutex_init( &m_fetch_lock, (pthread_mutexattr_t*)NULL );

  AT( "dbEasyStmt::dbEasyStmt" );
}

// Release resources associated with the class.
dbEasyStmt::
~dbEasyStmt(void)
{
  pthread_mutex_destroy( &m_fetch_lock );

  AT( "dbEasyStmt::~dbEasyStmt" );
  dealloc_buffers();	// Clean up all space that has been used
  AT( "dbEasyStmt::~dbEasyStmt - freeing memory" );
}

//!param SQLUSMALLINT ColumnNum - Column number to bind to (starting at 1)
//!param SQLINTEGER Size - 
void dbEasyStmt::
Bind(SQLUSMALLINT ColumnNum, SQLINTEGER Size, ILwd::LdasElement* Base)
{
  bool own(false);
  if (m_query_bound_size == 0)
  {
    m_query_bound_size = Size;
  }
  else
  {
    if (m_query_bound_size != Size)
    {
      cerr << "WARNING: Bounding sizes not the same" << endl;
    }
  }
  if (!Base)
  {
    Base = make_ilwd(ColumnNum, m_query_bound_size);
    own = true;
  }
  m_col_bindings.push_back(BoundColumnIlwd(Base, *this, ColumnNum, own));
  m_col_bindings.back().Bind();
  if (!m_col_bindings.back().IsOk())
  {
    //-------------------------------------------------------------------
    // Take it out of the list
    //-------------------------------------------------------------------
    m_col_bindings.erase(m_col_bindings.end());
    AT("Unsupported type");
  }
}

//!param: SQLUSMALLINT ParamNum - Parameter offset (starting from 1).
//!param: ILwd::LdasElement* Base - ILwd element containing the data to be
//+       bound.
void dbEasyStmt::
BindParameter(SQLUSMALLINT ParamNum, ILwd::LdasElement* Base)
{
  if (m_row_array_size == 0)
  {
    // Initialize what the row size is for this set.
    m_row_array_size = get_dim(Base);
    SQLHSTMT stmt = (SQLHSTMT)(m_query.GetHstmt());
#if  (ODBCVER < 0x0300) || defined(SQL_MVSDB2V23_SYNTAX)
    eval_retcode(__FILE__, __LINE__,
		 SQLParamOptions(stmt, m_row_array_size, &m_row_array_size));
#else
    eval_retcode(__FILE__, __LINE__,
		 SQLSetStmtAttr(stmt, SQL_ATTR_PARAMSET_SIZE,
				(SQLPOINTER) m_row_array_size,
				0));
#endif
  }
  else
  {
    // Verify that the rowset is the same size.
    if (m_row_array_size != get_dim(Base))
    {
      // :TODO: Thow a nasty exception.
      std::ostringstream	error;

      error << "Differing array sizes: "
	    << m_row_array_size << " != "
	    << get_dim(Base)
	    << " (" << ParamNum << ")";
      AT( error.str( ).c_str( ) );
    }
  }
  m_param_bindings.push_back(BoundParamIlwd(Base, *this, ParamNum));
}

//!return: bool - true if a row was fetched, false otherwise.
bool dbEasyStmt::
Fetch(void)
{
  MutexLock	lock( &m_fetch_lock );

  bool retval = true;
  try{
    //-------------------------------------------------------------------
    // Check to see if there were any column in the result set
    //-------------------------------------------------------------------
    
    if (!m_data_pointers)
    {
      return false;
    }

    //-------------------------------------------------------------------
    // Try to read information
    //-------------------------------------------------------------------

    try
    {
      for (int x = 0; x < m_query_bound_size; x++)
      {
	std::string	name;
	m_query.Fetch();
	// :TODO: For all columns that are bound to ILWD data,
	// :TODO:   move the data from the temporary buffer to
	// :TODO:   the ilwd buffer.
	for (std::list<BoundColumnIlwd>::iterator bci = m_col_bindings.begin();
	     bci != m_col_bindings.end();
	     bci++)
	{
	  post_fetch_copy(bci->GetColumn(), m_row_bind_pos++,
			  bci->GetRetSize(),
			  bci->GetBase(), bci->GetBuffer());
	}
      }
    }
    catch (dbErrNoData& err)
    {
      zero_set( false );
      retval = false;
    }
    return retval;
  }
  catch (...) {
    cerr << "Mysterious error in dbEasy::Fetch()" << endl;
    return false;
  }
}

//!param: int MaxRows - The maximum number of rows to retrieve.
//!param: int Inc - The number of rows by which to extend the buffer.
//!return: bool - true if a row was fetched, false otherwise.
bool dbEasyStmt::
FetchMulti(int MaxRows, int Inc)
{
  MutexLock	lock( &m_fetch_lock );

#ifdef GLOBAL_REGISTRY
      theElementList.Unregister( this );
#else
      unregister_obj( );
#endif

  bool	retcode(true);
  try{
    //-------------------------------------------------------------------
    // Check to see if there were any column in the result set
    //-------------------------------------------------------------------
    
    if (!m_data_pointers)
    {
      return false;
    }

    //-------------------------------------------------------------------
    // Try to read information
    //-------------------------------------------------------------------

    try
    {
      int	row;
      int	cache = 0;

      cache_reset();
      for (row = 0; row < MaxRows; row++, cache--)
      {
	if (!cache)
	{
	  cache = Inc;
	  cache_extend(row + cache);
	}
	for (int x = 0; x < m_query_bound_size; x++)
	{
	  std::string	name;
	  unsigned int	pos(0);
	  m_query.Fetch();
	  // :TODO: For all columns that are bound to ILWD data,
	  // :TODO:   move the data from the temporary buffer to
	  // :TODO:   the ilwd buffer.
	  for (std::list<BoundColumnIlwd>::iterator bci = m_col_bindings.begin();
	       bci != m_col_bindings.end();
	       bci++)
	  {
	    (void)(*bci).AppendBufferToCache();
	    pos++;
	  }
	}
      }
      // test if one more row can be read.
      m_query.Fetch();
    }
    catch (const dbErrNoData& err)
    {
      zero_set( false );
      retcode = false;
    }
    // Move cached data into ilwd
    cache_to_ilwd();
    return retcode;
  }
  catch( const std::exception& err )
  {
    std::cerr << "Std::exception in  dbEasy::FetchMulti(): "
	      << err.what( )
	      << endl;
    return false;
  }
  catch (...) {
    std::cerr << "Mysterious error in dbEasy::FetchMulti()" << endl;
    return false;
  }
}

#if HAVE_TID
//!param: string Query - SQL query to be executed.
tid* dbEasyStmt::
FetchMulti_t( int MaxRows, int Inc )
{
  tid* t = new tid3<bool,dbEasyStmt*,int,int>( &dbEasyStmt::fetchmulti_bg,
					       dbEasyStmt::m_multifetch_key,
					       this,
					       MaxRows, Inc );
  Registry::threadRegistry.registerObject(t);
  t->Spawn();
  return t;
}

//!param: tid* t - Pointer to thread handle
bool dbEasyStmt::
FetchMulti_r(tid* t)
{
  if (!Registry::threadRegistry.isRegistered(t))
    throw std::runtime_error("invalid_tid");
  if (t->getFunction() != m_multifetch_key)
    throw std::runtime_error ("invalid_tid");
  try {
    t->unsetAlert( );
    bool tmp( reinterpret_cast< tid3<bool,dbEasyStmt*,int,int>* >
	      ( t )->getReturn() );
    Registry::threadRegistry.destructObject( t );
    return tmp;
  }
  catch( ... ) {
    Registry::threadRegistry.destructObject( t );
    throw; /* rethrow the exception */
  }                                                                
}
#endif /* HAVE_TID */

//!param: int Column - The column offset (starting with 1).
//!return: const ILwd::LdasElement* - The pointer to the bound column data.
const ILwd::LdasElement* dbEasyStmt::
GetBoundColumnData(int Column)
{
  ILwd::LdasElement* retval = m_elements[Column-1];
#ifdef GLOBAL_REGISTRY
      theElementList.register( retval, this );
#else
      register_obj( retval );
#endif
  return retval;
}

//!param: int Column - The column offset (starting with 1).
//!return: const SQLPOINTER* - The pointer to the column.
const SQLPOINTER* dbEasyStmt::
GetDataBuffer(int Column) const
{
  if ((Column <= 0) || (Column > m_col_count))
  {
    return ((const SQLPOINTER*)NULL);
  }
  return ((const SQLPOINTER*)m_data_pointers[Column-1]);
}

//!param: string Query - SQL query to be executed.
void dbEasyStmt::
Prepare( const std::string& Query )
{
  AT( "dbEasyStmt::Prepare - start" );
  SQLSMALLINT	parameter_count;
  DynamicParam	param_default = {0, 0, 0};

  // Clear out pending ILWD bind info
  AT( "dbEasyStmt::Prepare" );
  dealloc_buffers();
  m_row_array_size = 0;
  m_data_pointers = (SQLPOINTER*)NULL;
  AT( "dbEasyStmt::Prepare - Prepare" );
  m_query.Prepare(Query);
  AT( "dbEasyStmt::Prepare - SQLNumParams" );
  eval_retcode(__FILE__, __LINE__,
	       SQLNumParams(m_query.GetHstmt(), &parameter_count));
  AT( "dbEasyStmt::Prepare - setup of parameters" );
  m_dynamic_params.resize(parameter_count, param_default);
  AT( "dbEasyStmt::Prepare - finish" );
}

// ReleaseBoundColumnData returns the pointer to the column ilwd that was
// generated by one of the several Fetch methods. This method can only be
// called once for each column as it replaces the pointer with a NULL
// pointer. The caller is responsible for memory management of the returned
// object.
//
// This method should never be exposed to any scripting language.
//
//!param: int Column - The column offset (starting with 1).
//!return: const ILwd::LdasElement* - The pointer to the bound column data.
//+	The caller is responsible for memory management of the returned
//+	pointer.
ILwd::LdasElement* dbEasyStmt::
ReleaseBoundColumnData(int Column)
{
  // Get a pointer to the result
  ILwd::LdasElement* retval = m_elements[Column-1];
  // Reset the pointer and trust the programmer to do proper cleanup
  m_elements[Column-1] = (ILwd::LdasElement*)NULL;
  for ( std::list<BoundColumnIlwd>::iterator
	  cur = m_col_bindings.begin( ),
	  end = m_col_bindings.end( );
	cur != end;
	++cur )
  {
    if ( ( *cur).GetColumn( ) == Column )
    {
      (*cur).RelinquishOwnership( );
      break;
    }
  }
  // Return the data
  return retval;
}

//!param: string Query - SQL query to be executed.
void dbEasyStmt::
Submit(std::string Query)
{
  AT( "dbEasyStmt::Submit" );
  if (Query.size())
  {
    Prepare(Query);
  }

  m_row_bind_pos = 0;
  if (m_param_bindings.size())
  {
    while(m_row_array_size)
    {
      try
      {
	m_query.Submit();
      }
      catch (dbErrNeedData& err)
      {
	SQLPOINTER	rgbValue;
	while ((SQLParamData(m_query.GetHstmt(), &rgbValue)) ==
	       SQL_NEED_DATA)
	{
	  for (std::list<BoundParamIlwd>::iterator bpi = m_param_bindings.begin();
	       bpi != m_param_bindings.end();
	       bpi++)
	  {
	    if ((*bpi).GetRGBValue() == rgbValue)
	    {
	      (*bpi).PutData();
	      break;
	    }
	  }
	}
      }
      if (m_env.GetMaxBindingCount() > 0)
      {
	// Simulate Array bindings for systems that do no
	//    support the feature.
	for (std::list<BoundParamIlwd>::iterator bpi = m_param_bindings.begin();
	     bpi != m_param_bindings.end();
	     bpi++)
	{
	  (*bpi).Bind();
	}
	m_row_array_size--;
      }
      else
      {
	break;
      }
    }
  }
  else
  {
    m_query.Submit();
  }
  m_col_count = m_query.GetColumnCount();
  if (m_col_count > 0)
  {
    m_elements.resize(m_col_count, 0);
    m_data_len = new SQLINTEGER[m_col_count];
    m_data_pointers = new SQLPOINTER[m_col_count];
    for (int i = 0; i < m_col_count; i++)
    {
      // :TODO: All this needs to be removed. Cannot support auto
      // :TODO:   binding with user binding.
      switch(m_query.GetColumnType(i))
      {
      default:
	m_data_pointers[i] = (SQLPOINTER)calloc(sizeof(char), 1024);
      }
    }
  }
  else
  {
    m_col_count = 0;
  }
}

#if HAVE_TID
//!param: string Query - SQL query to be executed.
tid* dbEasyStmt::
Submit_t(std::string Query)
{
  tid* t = new tidv2<dbEasyStmt*, std::string>(&dbEasyStmt::submit_bg,
					  dbEasyStmt::m_submit_key,
					  this, Query);
  Registry::threadRegistry.registerObject(t);
  t->Spawn();
  return t;
}

//!param: tid* t - Pointer to thread handle
void dbEasyStmt::
Submit_r(tid* t)
{
  if (!Registry::threadRegistry.isRegistered(t))
    throw std::runtime_error("invalid_tid");
  if (t->getFunction() != m_submit_key)
    throw std::runtime_error ("invalid_tid");
  try {
    t->unsetAlert( );
    reinterpret_cast< tidv2<dbEasyStmt*, std::string>* >( t )->getReturn();
    Registry::threadRegistry.destructObject( t );
  }
  catch( ... ) {
    Registry::threadRegistry.destructObject( t );
    throw; /* rethrow the exception */
  }                                                                
}
#endif /* HAVE_TID */

//-----------------------------------------------------------------------
// dbEasy - private methods
//-----------------------------------------------------------------------

//!param: int NewSize - New size for cache
void dbEasyStmt::
cache_extend(int NewSize)
{
  for (std::list<BoundColumnIlwd>::iterator bci = m_col_bindings.begin();
       bci != m_col_bindings.end();
       bci++)
  {
    bci->ExpandCache(NewSize);
  }
}

void dbEasyStmt::
cache_reset(void)
{
  for (std::list<BoundColumnIlwd>::iterator bci = m_col_bindings.begin();
       bci != m_col_bindings.end();
       bci++)
  {
    bci->Reset();
  }
}

void dbEasyStmt::
cache_to_ilwd(void)
{
  for (std::list<BoundColumnIlwd>::iterator bci = m_col_bindings.begin();
       bci != m_col_bindings.end();
       bci++)
  {
    SQLUSMALLINT	col((*bci).GetColumn()-1);
    std::string		name = m_query.GetColumnName(col);

    (*bci).GetBase()->setNameString(name);
    if (bci->CacheToIlwd())
    {
      if (m_elements[col])
      {
	delete m_elements[col];
      }
      m_elements[col] = (*bci).GetBase();
      m_elements[col]->setNameString(name);
    }
    // Set the Null mask if one was generated.
    m_elements[col]->setNull(bci->GetMask());
  }
  zero_set( false );
}

//!param: T* Data - Source C buffer.
//!param: const T2* Buffer - Destination ILwd buffer.
//!param: int Size - Number of elements.
template <class T, class T2> bool dbEasyStmt::
copy_array(T* Data, const T2* Buffer, int Size)
{
  if (!Data)
  {
    // :TODO: Need to inform the user on the fact that
    // :TODO:   something bad happened here.
    return false;;
  }
  if (Size == 0)
  {
    Buffer = (const T2*)NULL;
  }
#if (__GNUC__ == 2) && (__GNUC_MINOR__ == 95) && (__sparc__ == 1)
  //---------------------------------------------------------------------
  // This is a hack that is needed to avoid an internal compiler error.
  //---------------------------------------------------------------------
  T d = ILwd::LdasArray<T2>(Buffer, Size);
  *Data = d;
#else
  *Data = ILwd::LdasArray<T2>(Buffer, Size);
#endif
  return true;
}

//!param: int CType - C type specified as specified by ODBC
//!return: int - ILwd type specifier
int dbEasyStmt::
cnvt_ctype_to_ilwdtype(int CType)
{
  switch(CType)
  {
  case SQL_C_CHAR: return ILwd::ID_CHAR;
  case SQL_C_BINARY: return ILwd::ID_CHAR_U;
  case SQL_C_SSHORT: return ILwd::ID_INT_2S;
  case SQL_C_USHORT: return ILwd::ID_INT_2U;
  case SQL_C_SLONG: return ILwd::ID_INT_4S;
  case SQL_C_ULONG: return ILwd::ID_INT_4U;
#ifdef SQL_C_SBIGINT
  case SQL_C_SBIGINT: return ILwd::ID_INT_8S;
#endif
#ifdef SQL_C_UBIGINT
  case SQL_C_UBIGINT: return ILwd::ID_INT_8U;
#endif
  case SQL_C_FLOAT: return ILwd::ID_REAL_4;
  case SQL_C_DOUBLE: return ILwd::ID_REAL_8;
  default:
    //-------------------------------------------------------------------
    // :TODO: Need to throw an exception to let anyone who cares know
    // :TODO:   that no binding really took place.
    //-------------------------------------------------------------------
    break;
  }
  {
    char error[64];
    sprintf(error, "CTYPE_UNKNOWN: %d", CType);
    AT(error);
  }
  return -1;
}

//!param: int DType - SQL type specified by ODBC
//!return: int - SQL C type specified by ODBC
int dbEasyStmt::
cnvt_dtype_to_ctype(int DType)
{
  switch(DType)
  {
#if defined(SQL_C_SBIGINT) || defined(SQL_C_BIGINT)
  case SQL_BIGINT:
#ifdef SQL_C_SBIGINT
    return SQL_C_SBIGINT;
#else	/* SQL_C_SBIGINT */
    return SQL_C_BIGINT;
#endif	/* SQL_C_SBIGINT */
#endif	/* defined(SQL_C_SBIGINT) || defined(SQL_C_BIGINT) */
#ifdef SQL_BLOB
  case SQL_BLOB:		return SQL_C_BINARY;
#endif	/* SQL_BLOB */
  case SQL_CHAR:		return SQL_C_CHAR;
  case SQL_BINARY:		return SQL_C_BINARY;
#ifdef SQL_CLOB
  case SQL_CLOB:		return SQL_C_CHAR;
#endif	/* SQL_CLOB */
  case SQL_DOUBLE:		return SQL_C_DOUBLE;
  case SQL_FLOAT:		return SQL_C_DOUBLE;
  case SQL_INTEGER:		return SQL_C_LONG;
  case SQL_LONGVARCHAR:		return SQL_C_CHAR;
  case SQL_LONGVARBINARY:	return SQL_C_BINARY;
  case SQL_REAL:		return SQL_C_FLOAT;
  case SQL_SMALLINT:		return SQL_C_SHORT;
  case SQL_VARCHAR:		return SQL_C_CHAR;
  case SQL_VARBINARY:		return SQL_C_BINARY;
  }
  return SQL_C_DEFAULT;
}


//!param: const ILwd::LdasElement* Base - Source ILwd
//!param: int DefaultCType - Default SQL C type as specified by ODBC
//+       if none found for Base.
//!return: int - Appropriate SQL C type as specified by ODBC for Base
int dbEasyStmt::
cnvt_ilwd_to_ctype(const ILwd::LdasElement* Base, int DefaultCType)
{
    
  if (Base)
  {
    switch(Base->getElementId())
    {
    case ILwd::ID_EXTERNAL:
      AT("Unsupported type");
      break;
    case ILwd::ID_ILWD:

      {
	const ILwd::LdasContainer* c =
	  dynamic_cast<const ILwd::LdasContainer*>(Base);
	if ((c) && (!c->empty()))
	{
	  return cnvt_ilwd_to_ctype((*c)[0], DefaultCType);
	}
      }
      break;
    case ILwd::ID_CHAR:
      return SQL_C_CHAR;
      break;
    case ILwd::ID_CHAR_U:
      return SQL_C_BINARY;
      break;
    case ILwd::ID_INT_2S:
      return SQL_C_SSHORT;
      break;
    case ILwd::ID_INT_2U:
      return SQL_C_USHORT;
      break;
    case ILwd::ID_INT_4S:
      return SQL_C_SLONG;
      break;
    case ILwd::ID_INT_4U:
      return SQL_C_ULONG;
      break;
#ifdef SQL_C_SBIGINT
    case ILwd::ID_INT_8S:
      return SQL_C_SBIGINT;
      break;
#endif
#ifdef SQL_C_UBIGINT
    case ILwd::ID_INT_8U:
      return SQL_C_UBIGINT;
      break;
#endif
    case ILwd::ID_REAL_4:
      return SQL_C_FLOAT;
      break;
    case ILwd::ID_REAL_8:
      return SQL_C_DOUBLE;
      break;
    case ILwd::ID_LSTRING:
      return SQL_C_CHAR;
      break;
    default:
      //-----------------------------------------------------------------
      // :TODO: Need to throw an exception to let anyone who cares know
      // :TODO:   that no binding really took place.
      //-----------------------------------------------------------------
      break;
    }
  }
  return DefaultCType;
}

//!param: const ILwd::LdasElement* Base - Source ILwd
//!return: int - ILwd type specifier
int dbEasyStmt::
cnvt_ilwd_to_dtype(const ILwd::LdasElement* Base)
{
  if (Base)
  {
    switch(Base->getElementId())
    {
    case ILwd::ID_EXTERNAL:
      AT("Unsupported type");
      break;
    case ILwd::ID_ILWD:
      {
	const ILwd::LdasContainer* c =
	  dynamic_cast<const ILwd::LdasContainer*>(Base);
	if (c && (c->size() > 0)) return cnvt_ilwd_to_dtype((*c)[0]);
	else return SQL_DEFAULT;
	AT("Unsupported type");
      }
      break;
    case ILwd::ID_CHAR:
      return SQL_CHAR;
      break;
    case ILwd::ID_CHAR_U:
      return SQL_BINARY;
      break;
    case ILwd::ID_INT_2S:
    case ILwd::ID_INT_2U:
      return SQL_SMALLINT;
      break;
    case ILwd::ID_INT_4S:
    case ILwd::ID_INT_4U:
      return SQL_INTEGER;
      break;
#ifdef SQL_BIGINT
    case ILwd::ID_INT_8S:
    case ILwd::ID_INT_8U:
      return SQL_BIGINT;
      break;
#endif
    case ILwd::ID_REAL_4:
      return SQL_REAL;
      break;
    case ILwd::ID_REAL_8:
      return SQL_DOUBLE;
      break;
    case ILwd::ID_LSTRING:
      return SQL_CHAR;
      break;
    default:
      //-----------------------------------------------------------------
      // :TODO: Need to throw an exception to let anyone who cares know
      // :TODO:   that no binding really took place.
      //-----------------------------------------------------------------
      break;
    }
  }
  return SQL_DEFAULT;
}

void dbEasyStmt::
dealloc_buffers(void)
{
  //---------------------------------------------------------------------
  // Since all memory is going to be wipe out, make sure that no one
  //   has hooks to it.
  //---------------------------------------------------------------------
  AT( "dbEasyStmt::dealloc_buffers" );
#if GLOBAL_REGISTRY
  theElementList.Unregister( this );
#else	/* GLOBAL_REGISTRY */
  unregister_obj();	// Unregister all memory
#endif	/* GLOBAL_REGISTRY */
  //---------------------------------------------------------------------
  // Start wiping out the memory.
  //---------------------------------------------------------------------
  AT( "dbEasyStmt::dealloc_buffers" );
  if (m_data_len)
  {
    AT( "dbEasyStmt::dealloc_buffers" );
    delete[] m_data_len;
    m_data_len = (SQLINTEGER*)NULL;
  }
  AT( "dbEasyStmt::dealloc_buffers" );
  if (m_data_pointers)
  {
    AT( "dbEasyStmt::dealloc_buffers" );
    for (int i = 0; i < m_col_count; i++)
    {
      free(m_data_pointers[i]);
    }
    AT( "dbEasyStmt::dealloc_buffers" );
    delete[] m_data_pointers;
    m_data_pointers = (SQLPOINTER*)NULL;
  }
  AT( "dbEasyStmt::dealloc_buffers" );
  for (unsigned int x = 0; x < m_elements.size(); x++)
  {
    AT( "dbEasyStmt::dealloc_buffers" );
    if (m_elements[x])
    {
      AT( "dbEasyStmt::dealloc_buffers" );
      delete m_elements[x];
    }
  }
  AT( "dbEasyStmt::dealloc_buffers" );
  m_elements.resize(0);
  //---------------------------------------------------------------------
  // Parameter pointers that were allocated for temporary storage.
  //---------------------------------------------------------------------
  AT( "dbEasyStmt::dealloc_buffers" );
  for (std::list<SQLPOINTER>::const_iterator i = m_param_pointers.begin();
       i != m_param_pointers.end();
       i++)
  {
    AT( "dbEasyStmt::dealloc_buffers" );
    free(*i);
  }
  AT( "dbEasyStmt::dealloc_buffers" );
  m_param_pointers.resize(0);
  AT( "dbEasyStmt::dealloc_buffers" );
  m_col_bindings.clear();
  AT( "dbEasyStmt::dealloc_buffers" );
  m_param_bindings.clear();
  AT( "dbEasyStmt::dealloc_buffers" );
  m_dynamic_params.resize(0);
  //---------------------------------------------------------------------
  // Get rid of miscellaneous memory
  //---------------------------------------------------------------------
  AT( "dbEasyStmt::dealloc_buffers" );
  for (std::list<void*>::const_iterator i = m_mallocs.begin();
       i != m_mallocs.end();
       i++)
  {
    AT( "dbEasyStmt::dealloc_buffers" );
    if (*i)
    {
      AT( "dbEasyStmt::dealloc_buffers" );
      free(*i);
    }
  }
  AT( "dbEasyStmt::dealloc_buffers" );
  m_mallocs.resize(0);
  AT( "dbEasyStmt::dealloc_buffers" );
}

//!param: std::string Filename - Source filename where error occured.
//!param: int LineNumber - Source line number wher error occured.
//!param: SQLRETURN Retcode - Return value of ODBC call.
void dbEasyStmt::
eval_retcode(std::string Filename, int LineNumber, SQLRETURN Retcode)
{
  AT( "dbEasyStmt::eval_retcode - start" );
  if ((Retcode == SQL_SUCCESS) || (Retcode == SQL_SUCCESS_WITH_INFO))
  {
    AT( "dbEasyStmt::eval_retcode - succeed" );
    return;
  }
  AT( "dbEasyStmt::eval_retcode - thow exception" );
  throw dbErr(Filename, LineNumber,
	      m_env.GetDB().GetHenv(),
	      m_env.GetDB().GetHdbc(),
	      m_query.GetHstmt());
}

//!param: int ILWDType - ILwd type specifier
//!param: SQLUSMALLINT Col - The column offset (starting with 1).
SQLUINTEGER dbEasyStmt::
get_csize(int ILWDType, SQLUSMALLINT Col) const
{
  SQLINTEGER rsize = 0;

  switch(ILWDType)
  {
#if 0
  case ILwd::ID_ILWD:
    {
      const ILwd::LdasContainer* c =
	dynamic_cast<const ILwd::LdasContainer*>(Base);
      if (c && (c->size() > 0)) return get_csize((*c)[0], Col);
      AT("Unsupported type");
    }
    break;
#endif
  case ILwd::ID_CHAR:
  case ILwd::ID_CHAR_U:
  case ILwd::ID_LSTRING:
    rsize = GetColumnSize(Col);
    switch (ILWDType)
    {
    case ILwd::ID_LSTRING:
    case ILwd::ID_CHAR:
      rsize++;
      break;
    default:
      break;
    }
    break;
  case ILwd::ID_INT_2S:
    rsize = sizeof(SQLSMALLINT);
    break;
  case ILwd::ID_INT_2U:
    rsize = sizeof(SQLUSMALLINT);
    break;
  case ILwd::ID_INT_4S:
    rsize = sizeof(SQLINTEGER);
    break;
  case ILwd::ID_INT_4U:
    rsize = sizeof(SQLUINTEGER);
    break;
#if defined(SQL_C_SBIGINT) && ( defined(SQLBIGINT) || defined(SQL_BIGINT_TYPE) )
  case ILwd::ID_INT_8S:
#ifdef SQLBIGINT
    rsize = sizeof(SQLBIGINT);
#else /* SQLBIGINT */
    rsize = sizeof(SQL_BIGINT_TYPE);
#endif /* SQLBIGINT */
    break;
#endif
#if defined(SQL_C_UBIGINT) && ( defined(SQLUBIGINT) || defined(SQL_BIGINT_TYPE) )
  case ILwd::ID_INT_8U:
#ifdef SQLUBIGINT
    rsize = sizeof(SQLUBIGINT);
#else /* SQLUBIGINT */
    rsize = sizeof(SQL_BIGINT_TYPE);
#endif	/* SQLUBINT */
    break;
#endif
  case ILwd::ID_REAL_4:
    rsize = sizeof(SFLOAT);
    break;
  case ILwd::ID_REAL_8:
    rsize = sizeof(SDOUBLE);
    break;
  default:
    break;
  }
  return rsize;
}


//!param: ILwd::LdasElement* Base - Pointer to data
//!return: SQLUINTEGER - The dimension of the Base
SQLUINTEGER dbEasyStmt::
get_dim(ILwd::LdasElement* Base) const
{
  switch(Base->getElementId())
  {
  case ILwd::ID_EXTERNAL:
    //===================================================================
    // Things that will never be supported.
    //===================================================================
    AT("Unsupported type");
    return 0;
  case ILwd::ID_ILWD:
    {
      ILwd::LdasContainer* c = dynamic_cast<ILwd::LdasContainer*>(Base);

      return c->size();
    }
  case ILwd::ID_CHAR_U:    
  case ILwd::ID_CHAR:
    //===================================================================
    // Handle Character type constructs
    //===================================================================
    {
      ILwd::LdasArrayBase* elem =
	dynamic_cast<ILwd::LdasArrayBase*>(Base);
      size_t dimsize = elem->getNDim();
      int dim = 0;
      switch(dimsize)
      {
      case 1:	dim = 1; break;
      case 2:	dim = elem->getDimension(0); break;
      default:
        AT("Unsupported dimensioning of char or char_u");
	dim = 0;
      }
      return dim;
    }
  case ILwd::ID_LSTRING:
    {
      ILwd::LdasString* a = dynamic_cast<ILwd::LdasString*>(Base);

      return a->size();
    }
  case ILwd::ID_INT_2S:
  case ILwd::ID_INT_2U:
  case ILwd::ID_INT_4S:
  case ILwd::ID_INT_4U:
  case ILwd::ID_INT_8S:
  case ILwd::ID_INT_8U:
  case ILwd::ID_REAL_4:
  case ILwd::ID_REAL_8:
  case ILwd::ID_COMPLEX_8:
  case ILwd::ID_COMPLEX_16:
    //===================================================================
    // Handle LDAS Array type constructs
    //===================================================================
    {
      ILwd::LdasArrayBase* elem =
	dynamic_cast<ILwd::LdasArrayBase*>(Base);
      return elem->getDimension(0);
    }
    break;
  }
  AT("Unknown case");
  return 0;
}

//!param: ILwd::LdasElement* Base - Source ILwd
//!return: dbEasyStmt::IlwdInfo - All IlwdInfo
dbEasyStmt::IlwdInfo dbEasyStmt::
get_ilwd_info(ILwd::LdasElement* Base)
{
  SQLPOINTER	data((SQLPOINTER)NULL);
  SQLUINTEGER	size(0);
  bool		free_pointer(false);

  switch(Base->getElementId())
  {
  //=====================================================================
  // Container support
  //=====================================================================
  case ILwd::ID_EXTERNAL:
    AT("Unsupported type");
    break;
  case ILwd::ID_ILWD:
    break;
  //=====================================================================
  // Basic support
  //=====================================================================
  case ILwd::ID_CHAR:
    {
      ILwd::LdasArray<CHAR>* a = dynamic_cast<ILwd::LdasArray<CHAR>*>(Base);
      if (!a) break;
      data = a->getData();
      size = a->getDimension(a->getNDim()-1);
    }
    break;
  case ILwd::ID_CHAR_U:
    {
      ILwd::LdasArray<CHAR_U>* a = dynamic_cast<ILwd::LdasArray<CHAR_U>*>(Base);
      if (!a) break;
      data = a->getData();
      size = a->getDimension(a->getNDim()-1);
    }
    break;
  case ILwd::ID_INT_2S:
    {
      ILwd::LdasArray<INT_2S>* a = dynamic_cast<ILwd::LdasArray<INT_2S>*>(Base);
      if (!a) break;
      data = a->getData();
      size = sizeof(INT_2S);
    }
    break;
  case ILwd::ID_INT_2U:
    {
      ILwd::LdasArray<INT_2U>* a = dynamic_cast<ILwd::LdasArray<INT_2U>*>(Base);
      if (!a) break;
      data = a->getData();
      size = sizeof(INT_2U);
    }
    break;
  case ILwd::ID_INT_4S:
    {
      ILwd::LdasArray<INT_4S>* a = dynamic_cast<ILwd::LdasArray<INT_4S>*>(Base);
      if (!a) break;
      data = a->getData();
      size = sizeof(INT_4S);
    }
    break;
  case ILwd::ID_INT_4U:
    {
      ILwd::LdasArray<INT_4U>* a = dynamic_cast<ILwd::LdasArray<INT_4U>*>(Base);
      if (!a) break;
      data = a->getData();
      size = sizeof(INT_4U);
    }
    break;
#ifdef SQL_C_SBIGINT
  case ILwd::ID_INT_8S:
    {
      ILwd::LdasArray<INT_8S>* a = dynamic_cast<ILwd::LdasArray<INT_8S>*>(Base);
      if (!a) break;
      data = a->getData();
      size = sizeof(INT_8S);
    }
    break;
#endif
#ifdef SQL_C_UBIGINT
  case ILwd::ID_INT_8U:
    {
      ILwd::LdasArray<INT_8U>* a = dynamic_cast<ILwd::LdasArray<INT_8U>*>(Base);
      if (!a) break;
      data = a->getData(); 
      size = sizeof(INT_8U);
    }
    break;
#endif
  case ILwd::ID_REAL_4:
    {
      ILwd::LdasArray<REAL_4>* a = dynamic_cast<ILwd::LdasArray<REAL_4>*>(Base);
      if (!a) break;
      data = a->getData();
      size = sizeof(REAL_4);
    }
    break;
  case ILwd::ID_REAL_8:
    {
      ILwd::LdasArray<REAL_8>* a = dynamic_cast<ILwd::LdasArray<REAL_8>*>(Base);
      if (!a) break;
      data = a->getData();
      size = sizeof(REAL_8);
    }
    break;
  case ILwd::ID_LSTRING:
    {
      char	*offset;
      bool	x_prime = false;
      int	cnt(0);
      
      free_pointer = true;
      ILwd::LdasString* a = dynamic_cast<ILwd::LdasString*>(Base);
      if (!a) break;
      for (std::vector<std::string>::const_iterator i = a->begin();
	   i != a->end() && !x_prime;
	   i++)
      {
	unsigned int	len = (*i).length();
	if ((*i)[0] == 'x' && (*i)[1] == '\'')
	{
	  x_prime = true;
	  len = (len-3) / 2;
	}
	if (len > size) size = len;
	cnt++;
      }
      if (!x_prime) size++;
      data = (SQLPOINTER)calloc(cnt, size);
      offset = (char*)data;
      for (std::vector<std::string>::const_iterator i = a->begin();
	   i != a->end();
	   i++)
      {
	if (x_prime)
	{
	  int	end = (*i).length()-1;
	  int	y = 0;
	  unsigned char	buf;

	  for (int x = 2; x < end; x += 2)
	  {
	    buf = ((*i)[x] - '0') << 4;
	    buf += (*i)[x+1] - '0';
	    offset[y++] = buf;
	  }
	}
	else
	{
	  bcopy ((*i).c_str(), offset, (*i).length() + 1);
	}
	offset += size; 
      }
    }
    break;
  default:
    //-------------------------------------------------------------------
    // :TODO: Need to throw an exception to let anyone who cares know
    // :TODO:   that no binding really took place.
    //-------------------------------------------------------------------
    break;
  }
  return IlwdInfo(data, size, free_pointer);
}

//!param: int Column - The column offset (starting with 1).
//!param: SQLINTEGER Size - Number of elements.
//!param: bool Register - true if created object should be registered
//+       with Register class, false otherwise.
//!return: ILwd::LdasElement* - Created ilwd.
ILwd::LdasElement* dbEasyStmt::
make_ilwd(int Column, SQLINTEGER Size, bool Register)
{
  ILwd::LdasElement*	elem = (ILwd::LdasElement*)NULL;
  std::string		name = (Column > 0)
    ? m_query.GetColumnName(Column-1)
    : "";

  switch (m_query.GetColumnType(Column-1))
  {
  case SQL_CHAR:
  case SQL_VARCHAR:
    elem = new ILwd::LdasString((const char**)NULL, 0, name);
    break;
  case SQL_BINARY:
#ifdef SQL_VARBINARY
  case SQL_VARBINARY:
#endif
  case SQL_LONGVARBINARY:
#ifdef SQL_BLOB
  case SQL_BLOB:
#endif
    if (Register)
    {
      elem = new ILwd::LdasContainer();
    }
    else
    {
      elem = new_ldas_array<CHAR_U>(Size, name);
    }
    break;
  case SQL_SMALLINT:
    elem = new_ldas_array<INT_2S>(Size, name);
    break;
  case SQL_INTEGER:
    elem = new_ldas_array<INT_4S>(Size, name);
    break;
#ifdef SQL_BIGINT
  case SQL_BIGINT:
    elem = new_ldas_array<INT_8S>(Size, name);
    break;
#endif
  case SQL_REAL:
    elem = new_ldas_array<REAL_4>(Size, name);
    break;
  case SQL_DOUBLE:
    elem = new_ldas_array<REAL_8>(Size, name);
    break;
  default:
    elem = new ILwd::LdasString((const char**)NULL, 0, name);
    break;
  }

  m_elements[Column-1] = elem;
  return elem;
}

//!param: SQLUSMALLINT Column - The column offset (starting with 1).
//!param: unsigned int Offset - Row offset.
//!param: SQLINTEGER RetSize - Size of data returned
//!param: ILwd::LdasElement* Base - Destination buffer.
//!param: void* Buffer - Source buffer.
void dbEasyStmt::
post_fetch_copy(SQLUSMALLINT Column, unsigned int Offset,
		SQLINTEGER RetSize,
		ILwd::LdasElement* Base, void* Buffer)
{
  switch(Base->getElementId())
  {
  //===============================================================
  // Work on containers
  //===============================================================
  case ILwd::ID_ILWD:

    {
      ILwd::LdasContainer* c =
	dynamic_cast<ILwd::LdasContainer*>(Base);
      if (c)
      {
	while (c->size() <= Offset)
	{
	  c->push_back( make_ilwd(Column, 0, false),
			ILwd::LdasContainer::NO_ALLOCATE,
			ILwd::LdasContainer::OWN );
	}
	post_fetch_copy(Column, Offset, RetSize, (*c)[Offset], Buffer);
	return;
      }
      AT("Unsupported type");
    }
    break;
  //===============================================================
  // Work on characters
  //===============================================================
  case ILwd::ID_CHAR:
    copy_array(dynamic_cast<ILwd::LdasArray<CHAR>*>(Base),
	       (CHAR*)(Buffer),
	       RetSize);
    break;
  case ILwd::ID_CHAR_U:
    {
      ILwd::LdasArray<CHAR_U>* bin =
	dynamic_cast<ILwd::LdasArray<CHAR_U>*>(Base);
      
      if (!copy_array(bin, (CHAR_U*)(Buffer), RetSize))
      {
	return;
      }
      if (RetSize > (4 * 1024))
      {
	bin->setWriteCompression(ILwd::GZIP6);
      }
      else
      {
	bin->setWriteCompression(ILwd::NO_COMPRESSION);
      }
    }
    break;
  case ILwd::ID_LSTRING:
    {
      ILwd::LdasString* str =
	dynamic_cast<ILwd::LdasString*>(Base);
      CHAR* b = (CHAR*)(Buffer);
      
      if (!str)
      {
	// :TODO: Need to inform the user on the fact that
	// :TODO:   something bad happened here.
	break;
      }
      *str = ILwd::LdasString(b);
    }
    break;
  //===============================================================
  // Next work on the array classes
  //===============================================================
  case ILwd::ID_INT_2S:
    copy_array(dynamic_cast<ILwd::LdasArray<INT_2S>*>(Base),
	       (INT_2S*)(Buffer));
    break;
  case ILwd::ID_INT_2U:
    copy_array(dynamic_cast<ILwd::LdasArray<INT_2U>*>(Base),
	       (INT_2U*)(Buffer));
    break;
  case ILwd::ID_INT_4S:
    copy_array(dynamic_cast<ILwd::LdasArray<INT_4S>*>(Base),
	       (INT_4S*)(Buffer));
    break;
  case ILwd::ID_INT_4U:
    copy_array(dynamic_cast<ILwd::LdasArray<INT_4U>*>(Base),
	       (INT_4U*)(Buffer));
    break;
  case ILwd::ID_INT_8S:
#if (__GNUC__ == 2) && (__GNUC_MINOR__ == 95) && !defined( __alpha__ )
    copy_array(dynamic_cast<ILwd::LdasArray<INT_8S>*>(Base),
	       static_cast<long long int*>(Buffer));
#else
    copy_array(dynamic_cast<ILwd::LdasArray<INT_8S>*>(Base),
	       static_cast<INT_8S*>(Buffer));
#endif
    break;
  case ILwd::ID_INT_8U:
    copy_array(dynamic_cast<ILwd::LdasArray<INT_8U>*>(Base),
	       (INT_8U*)(Buffer));
    break;
  case ILwd::ID_REAL_4:
    copy_array(dynamic_cast<ILwd::LdasArray<REAL_4>*>(Base),
	       (REAL_4*)(Buffer));
    break;
  case ILwd::ID_REAL_8:
    copy_array(dynamic_cast<ILwd::LdasArray<REAL_8>*>(Base),
	       (REAL_8*)(Buffer));
    break;
  default:
    //=============================================================
    // Something really messed up. This case should have been
    //   caught when trying to do the binding.
    //=============================================================
    AT("Unsupported type");
  }
}

//!param: dbEasyStmt* Stmt - Statement handle
//!param: int MaxRows - Maximum number of rows to return
//!param: int Inc - Number of rows to increment by.
bool dbEasyStmt::
fetchmulti_bg( dbEasyStmt* Stmt, int MaxRows, int Inc )
{
  return Stmt->FetchMulti( MaxRows, Inc );
}

//!param: dbEasyStmt* Stmt - Statement handle
//!param: string Query - SQL query to be executed.
void dbEasyStmt::
submit_bg(dbEasyStmt* Stmt, std::string Query)
{
  Stmt->Submit(Query);
}

#ifndef GLOBAL_REGISTRY
//!param: ILwd::LdasElement* Elem - Object to be registered.
void dbEasyStmt::
register_obj(ILwd::LdasElement* Elem)
{
  if (Elem == (ILwd::LdasElement*)NULL)
  {
    // Don't regsiter NULL objects
    return;
  }
  for (std::list<ILwd::LdasElement*>::const_iterator i = m_registered_objects.begin();
       i != m_registered_objects.end();
       i++)
  {
    if ((*i) == Elem) return;
  }
  Registry::elementRegistry.registerObject(Elem);
  m_registered_objects.push_back(Elem);
}

//!param: ILwd::LdasElement* Elem - Element to be unregistered.
void dbEasyStmt::
unregister_obj(ILwd::LdasElement* Elem)
{
  if (Elem == (void*)NULL) return;
  for (std::list<ILwd::LdasElement*>::iterator i = m_registered_objects.begin();
       i != m_registered_objects.end();
       i++)
  {
    if ((*i) == Elem)
    {
      Registry::elementRegistry.removeObject( Elem );
      m_registered_objects.erase(i);
      break;
    }
  }
}

// U
void dbEasyStmt::
unregister_obj(void)
{
  for (std::list<ILwd::LdasElement*>::const_iterator i = m_registered_objects.begin();
       i != m_registered_objects.end();
       i++)
  {
    Registry::elementRegistry.removeObject( *i );
  }
  m_registered_objects.resize(0);
}
#endif	/* GLOBAL_REGISTRY */

void dbEasyStmt::
zero_set( bool Register )
{
  if (m_query.GetRowCount() <= 0)
  {
    for (std::list<BoundColumnIlwd>::iterator bci = m_col_bindings.begin();
	 bci != m_col_bindings.end();
	 bci++)
    {
      int	col((*bci).GetColumn());

      if ( Register )
      {
#if GLOBAL_REGISTRY
	theElementList.Unregister( (*bci).GetBase() );
#else	/* GLOBAL_REGISTRY */
	unregister_obj((*bci).GetBase());
#endif	/* GLOBAL_REGISTRY */
      }
      (*bci).ResetBase(make_ilwd(col, 0, false));
      (*bci).GetBase()->setNameString(m_query.GetColumnName(col - 1));
      if ( Register )
      {
#if GLOBAL_REGISTRY
	theElementList.Register( (*bci).GetBase(), this );
#else	/* GLOBAL_REGISTRY */
	register_obj((*bci).GetBase());
#endif	/* GLOBAL_REGISTRY */
      }
    }
  }
}

#if GLOBAL_REGISTRY
template class ObjectRegistry< element_data_type >;

void element_list::
Register( ILwd::LdasElement* Element, dbEasyStmt* Stmt )
{
  Registry::elementRegistry.registerObject( Element );
  m_elements.registerObject( new element_data_type( Element, Stmt ) );
}

void element_list::
Unregister( ILwd::LdasElement* Element )
{
  element_data_type*	ed( m_elements.GetElementData( Element ) );

  if ( ed )
  {
    Registry::elementRegistry.removeObject( ed->s_element );
    m_elements.destructObject( ed );
  }
}

void element_list::
Unregister( dbEasyStmt* Stmt )
{
  std::list<element_data_type*>	ed;

  m_elements.GetElementData( Stmt, ed );
  for ( std::list< element_data_type* >::const_iterator i = ed.begin();
	i != ed.end();
	i ++ )
  {
    Registry::elementRegistry.removeObject( (*i)->s_element );
    m_elements.destructObject( *i );
  }
}

element_data_type* element_data_list::
GetElementData( const ILwd::LdasElement* Element )
{
  General::ReadWriteLock m( m_lock, General::ReadWriteLock::READ );

  for ( const_iterator i = begin(); i != end(); i++ )
  {
    if ( (*i)->s_element == Element )
    {
      return *i;
    }
  }
  return (element_data_type*)NULL;
}
  
void element_data_list::
GetElementData( const dbEasyStmt* Stmt,
		std::list< element_data_type*>& Matches )
{
  General::ReadWriteLock m( m_lock, General::ReadWriteLock::READ );

  for ( const_iterator i = begin(); i != end(); i++ )
  {
    if ( (*i)->s_stmt == Stmt )
    {
      Matches.push_back( *i );
    }
  }
}
#endif	/* GLOBAL_REGISTRY */
