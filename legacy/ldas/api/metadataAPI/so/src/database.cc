#ifdef __GNUG__
#pragma implementation "database.h"
#endif

#include "config.h"

#include <stdexcept>
#include <iostream>

#include "dberr.hh"
#include "database.h"

SQLHENV dbDatabase::m_henv = SQL_NULL_HENV;
int dbDatabase::m_ref_count = 0;
pthread_mutex_t dbDatabase::m_lock = PTHREAD_MUTEX_INITIALIZER;

//-----------------------------------------------------------------------
//Construct a new ODBC Database instance
//
//!param: std::string DBName - Name of database to open.
//!param: std::string User - Database user to access the database.
//!param: std::string Password - Password for database user.
dbDatabase::
dbDatabase(std::string DBName, std::string User, std::string Password)
try
  : m_hdbc(SQL_NULL_HDBC),
    m_is_open(false)
{
  MutexLock	lock( m_lock );

  ++m_ref_count;
  if (m_henv == SQL_NULL_HENV)
  {
    eval_retcode(__FILE__, __LINE__, SQLAllocEnv(&m_henv));
  }
  open(DBName, User, Password);
}
catch(...)
{
  MutexLock	lock( m_lock );

  // :TODO: Need to capture DB errors
  if (m_ref_count > 0)
  {
    --m_ref_count;
    if ((m_ref_count <= 0) && (m_henv != SQL_NULL_HENV))
    {
      Close();
      SQLFreeEnv(m_henv);
      m_henv = SQL_NULL_HENV;
    }
  }
  throw;
}

//-----------------------------------------------------------------------
//Destroy an ODBC Database instance
//
dbDatabase::
~dbDatabase(void)
{
  MutexLock	lock( m_lock );

  m_ref_count--;
  Close( );
  if ( m_ref_count <= 0 )
  {
    if (m_henv != SQL_NULL_HENV)
    {
      eval_retcode(__FILE__, __LINE__, SQLFreeEnv(m_henv));
      m_henv = SQL_NULL_HENV;
    }
  }
}

//-----------------------------------------------------------------------
// Close the database and release all resources associated with opening
// the database.
//
//!return: bool - true if the database was open when this function was
//+               was executed, false otherwise.
bool dbDatabase::
Close(void)
{
  if (m_hdbc != SQL_NULL_HDBC)
  {
    if (m_is_open)
    {
      eval_retcode(__FILE__, __LINE__, SQLDisconnect(m_hdbc));
      m_is_open = false;
    }
    eval_retcode(__FILE__, __LINE__, SQLFreeConnect(m_hdbc));

    m_hdbc = SQL_NULL_HDBC;
    
    return true;
  }
  
  return false;
}

//-----------------------------------------------------------------------
// Estable a connection to the requested database.
//
//!param: std::string DBName - Name of the Database to connect to.
//!param: std::string Username - Name of Database user.
//!param: std::string Password - Password for Database user.
//-----------------------------------------------------------------------
bool dbDatabase::
open(std::string DBName, std::string Username, std::string Password)
{
  if (m_is_open)
  {
    return false;
  }
  
  if (!m_henv)
  {
    return false;
  }

  eval_retcode(__FILE__, __LINE__, SQLAllocConnect(m_henv, &m_hdbc));
  eval_retcode(__FILE__, __LINE__,
	       SQLConnect(m_hdbc,
			  (UCHAR FAR *)(DBName.c_str()), SQL_NTS,
			  (UCHAR FAR *)(Username.c_str()), SQL_NTS,
			  (UCHAR FAR *)(Password.c_str()), SQL_NTS));
  m_is_open = true;
  
  return true;
}

void dbDatabase::
TransactionBegin( )
{
  eval_retcode( __FILE__, __LINE__,
		SQLSetConnectOption( m_hdbc, SQL_AUTOCOMMIT,
				     SQL_AUTOCOMMIT_OFF ) );
  TransactionCommit( );
}

void dbDatabase::
TransactionCommit( )
{
  //---------------------------------------------------------------------
  // Need to be careful here to preserve any error messages.
  //---------------------------------------------------------------------
  MutexLock	lock( m_lock );

  eval_retcode( __FILE__, __LINE__,
		SQLTransact( m_henv, m_hdbc, SQL_COMMIT ) );
}

void dbDatabase::
TransactionRollback( )
{
  //---------------------------------------------------------------------
  // Need to be careful here to preserve any error messages.
  //---------------------------------------------------------------------
  MutexLock	lock( m_lock );

  eval_retcode( __FILE__, __LINE__,
		SQLTransact( m_henv, m_hdbc, SQL_ROLLBACK ) );
}

//-----------------------------------------------------------------------
//Determine if an error has occured. If an error has occured, thow
//  an exception.
//!param: std::string Filename - Name of file where the potential error
//+                              occured (Usually specified by '__FILE__')
//!param: int Line - Line number where potential error occured.
//+                  (Usually specified by '__LINE__')
//!param: RETCODE Retcode - Return code from an ODBC call.
//!exc: dbErr - Any database error
void dbDatabase::
eval_retcode(std::string Filename, int LineNumber, RETCODE Retcode) const
{
  if ((Retcode == SQL_SUCCESS) || (Retcode == SQL_SUCCESS_WITH_INFO))
  {
    return;
  }
  throw dbErr(Filename, LineNumber, m_henv, m_hdbc);
}
