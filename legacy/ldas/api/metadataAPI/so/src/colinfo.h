/* -*- C++ -*- */
#ifndef COLINFO_H
#define	COLINFO_H

#ifdef __GNUG__
#pragma interface
#endif

#include <string>

#include "odbc.h"

//: Keeps track of result column characteristics
// This class maintains information about a result column.
class dbColInfo
{
public:
  //: Constructor
  dbColInfo( std::string Name, SQLSMALLINT DataType,
	     SQLUINTEGER ColumnSize, SQLSMALLINT DecimalDigits,
	     bool Nullable );

  //: Return the SQL data type for the column
  SQLSMALLINT GetDataType(void) const;

  //: Return the name of the column
  std::string GetName(void) const;

  //: Return the size of the column
  SQLUINTEGER GetSize(void) const;

  dbColInfo& operator=( const dbColInfo& Source );

private:
  // Column Name
  std::string	m_name;

  // SQL data type
  SQLSMALLINT	m_data_type;

  // Number of bytes occupied by column
  SQLUINTEGER	m_column_size;

  // Number of decimal digits
  SQLSMALLINT	m_decimal_digits;

  // True if column can have NULL values
  bool		m_nullable;

};

// Construct a record to store important result column characteristics.
//
//!param: std::string Name - Name of the column
//!param: SQLSMALLINT DataType - The SQL datatype
//!param: SQLUINTEGER ColumnSize - The number of bytes occupied by the column
//!param: SQLSMALLINT DecimalDigits - Number of decimal digits
//!param: bool Nullable - true if the Column supports NULL values, false
//+                       otherwise.

inline dbColInfo::
dbColInfo( std::string Name, SQLSMALLINT DataType,
	   SQLUINTEGER ColumnSize, SQLSMALLINT DecimalDigits,
	   bool Nullable )
  : m_name(Name),
    m_data_type(DataType),
    m_column_size(ColumnSize),
    m_decimal_digits(DecimalDigits),
    m_nullable(Nullable)
{
}

// Returns the SQL type of the column
inline SQLSMALLINT dbColInfo::
GetDataType(void) const
{
  return m_data_type;
}

// Returns the name of the column
inline std::string dbColInfo::
GetName(void) const
{
  return m_name;
}

// Returns the size (in bytes) of the column
inline SQLUINTEGER dbColInfo::
GetSize(void) const
{
  return m_column_size;
}

inline dbColInfo& dbColInfo::
operator=( const dbColInfo& Source )
{
  m_name = Source.m_name;
  m_data_type = Source.m_data_type;
  m_column_size = Source.m_column_size;
  m_decimal_digits = Source.m_decimal_digits;
  m_nullable = Source.m_nullable;

  return *this;
}
#endif	/* COLINFO_H */
