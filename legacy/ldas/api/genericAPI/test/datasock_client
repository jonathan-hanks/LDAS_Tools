#!/ldcg/bin/tclsh

## ****************************************************
##
## Name: datasock_client
##
## Test client for data socket communications.
##
## Requires datasock_server to be already running.
##
## Will connect to port 20000 of the machine given
## as argument 1, and attempt to pass ilwd data
## of types corresponding to the remaining arguments.
##
## currently acceptable types: ints, lstr, ext, cont.
##
## ****************************************************


set API manager ;## dummy code to fool generic API
lappend auto_path /ldas/lib
if { [ catch {
   package require genericAPI
   package require generic
} err ] } {
   puts stderr "#****************#"
   puts stderr "      ERROR!!     "
   puts stderr "#****************#"
   puts stderr "$argv0 $err"
   exit;
}

## ****************************************************
## DATA TYPE OBJECTS

set size 10000

;## create some data (arrays of $size integers)
set i 0
while { $i < $size } {
   append data " $i"
   incr i
}

;## an array of ints is called a _0xnnnnnn_LdasArrayBase_p
set ::ints "<int_4s dims='$size'>$data</int_4s>"

;## an array of strings is called a _0xnnnnnn_LdasElement_p
regsub -all { } $data "\\," data
set data [ string trim $data \\, ]
set lsize [ string length $data ]
set ::lstr "<lstring dims='$size' size='$lsize' name='Foo:B-ar:B_az'>$data</lstring>"
;## an external is also a _0xnnnnnn_LdasElement_p
set ::ext "<external size='$lsize'>$data</external>"

;## and a container is, of course, a _0xnnnnnn_LdasContainer_p
set ::cont "<ilwd size='6'>$ext$ints$lstr$ints$lstr$ext</ilwd>"

proc getargs {} {
     if { [ catch {
        ;## this can have any or all of these types
        if { [ llength $::argv ] > 1 } {
           set testlist [ lrange $::argv 1 end ]
           foreach el $testlist {
              if { ! [ regexp {(ints|lstr|ext|cont)} $el ] } {
                 return -code error "bad ilwd element type: $el"
                 }
              }   
           } else {
           set testlist { ints lstr ext cont }
           }
        } err ] } {
        return -code error "[ myName ]: $err"
        }
     return $testlist
}

## ****************************************************

proc testloop { server port } {
     if { [ catch {
        foreach data [ getargs ] {
           __t::start $data
           set seqpt "putElement([ set ::$data ])"
           __t::start
           set datap [ putElement [ set ::$data ] ]
           puts "created $datap from data string in [ __t::mark ] seconds"
           ;## create a client
           set seqpt "createDataSocket"
           set clientp [ createDataSocket ]
   
           ;## connect to the server
           set seqpt "connectDataSocket($clientp)"
           __t::start
           connectDataSocket $clientp $server $port
           puts "connected in [ __t::mark ] seconds"
           puts  "connect status: [ isSocketConnected $clientp ]"
   
           ;## send element to the server
           set seqpt "sendElementObject($datap)"
           __t::start
           sendElementObject $clientp $datap
           puts "element sent in [ __t::mark ] seconds"
   
           ;## close the client
           set seqpt "closeDataSocket($clientp)"
           closeDataSocket $clientp
           set seqpt "destructElement($datap)"
           destructElement $datap
           puts "Entire transaction on client side took [ __t::mark $data ] seconds\n"
           ;## wait a little or the server will choke.
           ;## this definitely needs to be fixed!
           ;##after 1000;
           }
        } err ] } {
        return -code error "[ myName ]: $seqpt: $err"
        }
}
## ****************************************************

## MAIN CODE BLOCK

set port 20000
;## the location of the server can be passed as an arg
if { [ llength $argv ] } {
   set server [ lindex $argv 0 ]
   } else {
   set server $env(HOST)
   }

if { [ catch {
   testloop $server $port
   } err ] } {
   puts  "#****************#"
   puts  "      ERROR!!     "
   puts  "#****************#"
   puts  "$argv0 $err"
   exit;
   }

## ****************************************************

