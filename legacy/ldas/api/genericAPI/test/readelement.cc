#include <ilwd/ldaselement.hh>
#include <ilwd/ldascontainer.hh>
#include "../so/src/swigexception.hh"
#include "../so/src/elementcmd.hh"
#include "../so/src/genericcmd.hh"

using ILwd::LdasElement;

int
main(int argc, char *argv[]){
ILwdFile* file = openILwdFile(argv[1],"r");
string element_p = readElement(file);
exit(0);
}
