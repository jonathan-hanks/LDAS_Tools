#include "config.h"

#undef PACKAGE_VERSION
#undef PACKAGE_TARNAME
#undef PACKAGE_STRING
#undef PACKAGE_NAME
#undef PACKAGE_BUGREPORT

#include "genericAPI/RegistryLibOSpace.hh"

#if HAVE_LIBOSPACE

namespace Registry
{
  //---------------------------------------------------------------------------
  //
  //: Server Registry
  //
  ServerRegistry_type	serverRegistry;


  //---------------------------------------------------------------------------
  //
  //: Check Server Pointer
  //
  // True if the server is valid.
  //
  //!param: os_tcp_connection_server* socket
  //
  //!return: bool - True if the object is valid
  //
  bool isServerValid( const os_tcp_connection_server* server ) throw()
  {
    return ( serverRegistry.isRegistered( server ) );
  }

  //---------------------------------------------------------------------------
  //
  //: Check Socket Pointer
  //
  // True if the socket is valid.
  //
  //!param: os_tcp_socket* socket
  //
  //!return: bool - True if the socket is valid
  //
  bool isSocketValid( const os_tcp_socket* socket ) throw()
  {
    if ( !activeSocketRegistry.isRegistered( socket ) )
    {
        if ( passiveSocketRegistry.isRegistered( socket ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return true;
    }
  }
} /* namespace - Registry */

template class ObjectRegistry< os_tcp_connection_server >;
template class ObjectRegistry< os_tcp_socket >;

#endif /* HAVE_LIBOSPACE */
