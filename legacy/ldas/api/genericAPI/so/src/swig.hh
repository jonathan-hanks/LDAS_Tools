#ifndef GENERIC_API__SWIG_HH
#define GENERIC_API__SWIG_HH

#include <sstream>

namespace GenericAPI
{
  inline std::string
  MakeSwigPointer( const void* Address, const char* Label )
  {
    std::ostringstream	oss;

    oss << "_" << hex << Address << "_p_" << Label;
    return oss.str( );
  }

  inline std::string
  SwigPointerPattern( const char* Label )
  {
    std::ostringstream	oss;

    oss << "_p_" << Label;
    return oss.str( );
  }
}

#endif	/* GENERIC_API_SWIG_HH */
