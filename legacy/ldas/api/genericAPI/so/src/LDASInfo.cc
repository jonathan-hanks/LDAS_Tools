/* -*- mode: c++ ; c-basic-offset: 2; -*- */
#include "genericAPI/config.h"

#include <cstdlib>

#include <sstream>

#include "general/unordered_map.hh"

#include "genericAPI/LDASInfo.hh"

typedef General::unordered_map< std::string, std::string > tcl_string_set_type;

static tcl_string_set_type& init_tcl_string_vars( );

static tcl_string_set_type& p_tcl_string_vars = init_tcl_string_vars( );

namespace GenericAPI
{
  std::string LDASInfo::m_api_name = "UNKNOWN";
  std::string LDASInfo::m_site_name = "ldas-unknown";
  std::string LDASInfo::m_system_name = "ldas-unknown";

  void LDASInfo::
  site_via_system( const std::string& Value )
  {
    /// \todo
    ///    This function need to be rewritten to actually set the site
    ///    base on the system name. 
    Site( Value );
  }

  void LDASInfo::
  VariableString( const std::string& Name,
		  const std::string& Value )
  {
    p_tcl_string_vars[ Name ] = Value;
  }

  const std::string& LDASInfo::
  VariableString( const std::string& Name )
  {
    return p_tcl_string_vars[ Name ];
  }

} // namespace - GenericAPI

//-----------------------------------------------------------------------
/// Initialize the set of TCL variables. Some of these values may be
/// overwritten once TCL starts.
//-----------------------------------------------------------------------
static tcl_string_set_type&
init_tcl_string_vars( )
{
  static tcl_string_set_type l_tcl_string_vars;

  std::ostringstream	d;

  d << getenv( "RUNDIR" );

  d << "/logs";
  l_tcl_string_vars[ "::LDASLOG" ] = d.str( );

  d << "/archive";
  l_tcl_string_vars[ "::LDASARC" ] = d.str( );

  return l_tcl_string_vars;
}
