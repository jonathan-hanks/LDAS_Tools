/* -*- mode: c++; c-basic-offset: 4; -*- */

#include "genericAPI/config.h"

#include <iomanip>
#include <sstream>   

#include "general/ErrorLog.hh"

#include "swigexception.hh"
#include "registry.hh"

#include "util.hh"

using namespace std;   
   
#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
using ILwd::LdasElement;

ObjectRegistry< ILwdFile >                     ilwdFileRegistry;


static bool get_elem_attr(ILwd::LdasElement* Element,
			  const char* Attr,
			  std::ostringstream& StrStream);
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
//-----------------------------------------------------------------------------
/**
 * True if the file is valid.
 *
 * @param file (File*)
 *
 * @return bool - True if the object is valid
 */
bool isILwdFileValid( const ILwdFile* file )
{
    return ( ilwdFileRegistry.isRegistered( file ) );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


        
#if HAVE_LDAS_PACKAGE_ILWD
std::string
createElementPointer( const LdasElement* element )
{
    static const char* func = "createElementPointer";

    if (!element)
    {
	throw SWIGEXCEPTION( "NULL pointer" );
    }

    if ( General::StdErrLog.IsOpen( ) )
    {
	std::ostringstream	msg;

	msg << func << "( " << (void*)element << ")";
	General::StdErrLog( General::ErrorLog::DEBUG, __FILE__, __LINE__,
			    msg.str( ) );
    }

    const std::string& s = element->getIdentifier();
    if (
        ( ( s.size() == 6 )
	  && ( ( strncmp( s.c_str(), "int_", 4 ) == 0 ) &&
	       ( ( s[ 4 ] == '8' ) || ( s[ 4 ] == '4' ) ||
		 ( s[ 4 ] == '2' ) ) &&
	       ( ( s[ 5 ] == 'u' ) || ( s[ 5 ] == 's' ) ) ) )
	|| ( ( ( strncmp( s.c_str(), "real_", 5 ) == 0 ) &&
	       ( ( s[ 5 ] == '4' ) || ( s[ 5 ] == '8' ) ) ) )
	|| ( strcmp( s.c_str(), "char" ) == 0 )
	|| ( strcmp( s.c_str(), "char_u") == 0 )
        )
    {
	return makeTclPointer( element, "LdasArrayBase" );
    }
    else if ( s == "ilwd" )
    {
	return makeTclPointer( element, "LdasContainer" );
    }
    else
    {
	return makeTclPointer( element, "LdasElement" );
    }
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#if HAVE_LDAS_PACKAGE_ILWD
std::string format2String( ILwd::Format f )
{
    switch( f )
    {
        case ILwd::ASCII:
            return "ascii";

        case ILwd::BASE64:
            return "base64";
            
        case ILwd::BINARY:
            return "binary";

        case ILwd::USER_FORMAT:
            return "user";
    }
    return "";
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#if HAVE_LDAS_PACKAGE_ILWD
ILwd::Format char2Format( const char* f )
{
    if ( strcasecmp( f, "ascii" ) == 0 )
    {
        return ILwd::ASCII;
    }
    else if ( strcasecmp( f, "base64" ) == 0 )
    {
        return ILwd::BASE64;
    }
    else if ( strcasecmp( f, "binary" ) == 0 )
    {
        return ILwd::BINARY;
    }
    else if ( strcasecmp( f, "user" ) == 0 )
    {
        return ILwd::USER_FORMAT;
    }
    else
    {
	cerr << f << endl;
        throw SWIGEXCEPTION( "invalid_format" );
    }
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
std::string compression2String( ILwd::Compression c )
{
    switch( c )
    {
        case ILwd::NO_COMPRESSION:
            return "none";

        case ILwd::GZIP0:
            return "gzip0";
            
        case ILwd::GZIP1:
            return "gzip1";
            
        case ILwd::GZIP2:
            return "gzip2";
            
        case ILwd::GZIP3:
            return "gzip3";
            
        case ILwd::GZIP4:
            return "gzip4";
            
        case ILwd::GZIP5:
            return "gzip5";
            
        case ILwd::GZIP6:
            return "gzip6";
            
        case ILwd::GZIP7:
            return "gzip7";
            
        case ILwd::GZIP8:
            return "gzip8";
            
        case ILwd::GZIP9:
            return "gzip9";
            
        case ILwd::GZIP:
            return "gzip";
            
        case ILwd::USER_COMPRESSION:
            return "user";
    }
    return "";
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
ILwd::Compression char2Compression( const char* c )
{
    if ( strcasecmp( c, "none" ) == 0 )
    {
        return ILwd::NO_COMPRESSION;
    }
    else if ( strcasecmp( c, "gzip0" ) == 0 )
    {
        return ILwd::GZIP0;
    }
    else if ( strcasecmp( c, "gzip1" ) == 0 )
    {
        return ILwd::GZIP1;
    }
    else if ( strcasecmp( c, "gzip2" ) == 0 )
    {
        return ILwd::GZIP2;
    }
    else if ( strcasecmp( c, "gzip3" ) == 0 )
    {
        return ILwd::GZIP3;
    }
    else if ( strcasecmp( c, "gzip4" ) == 0 )
    {
        return ILwd::GZIP4;
    }
    else if ( strcasecmp( c, "gzip5" ) == 0 )
    {
        return ILwd::GZIP5;
    }
    else if ( strcasecmp( c, "gzip6" ) == 0 )
    {
        return ILwd::GZIP6;
    }
    else if ( strcasecmp( c, "gzip7" ) == 0 )
    {
        return ILwd::GZIP7;
    }
    else if ( strcasecmp( c, "gzip8" ) == 0 )
    {
        return ILwd::GZIP8;
    }
    else if ( strcasecmp( c, "gzip9" ) == 0 )
    {
        return ILwd::GZIP9;
    }
    else if ( strcasecmp( c, "gzip" ) == 0 )
    {
        return ILwd::GZIP;
    }
    else if ( strcasecmp( c, "user" ) == 0 )
    {
        return ILwd::USER_COMPRESSION;
    }
    else
    {
        throw SWIGEXCEPTION( "invalid_compression" );
    }
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#if HAVE_LDAS_PACKAGE_ILWD
std::string arrayOrder2String( ILwd::ArrayOrder a )
{
    switch( a )
    {
        case ILwd::C_ORDER:
            return "c";
            break;

        case ILwd::F77_ORDER:
            return "f77";
            break;

        default:
            return "invalid";
            break;
    }
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
std::string byteOrder2String( ILwd::ByteOrder b )
{
    switch( b )
    {
        case ILwd::LITTLE:
            return "little";
            break;

        case ILwd::BIG:
            return "big";
            break;
    }
    return "";
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#if HAVE_LDAS_PACKAGE_ILWD
std::string getLdasArrayBaseAttribute(
    ILwd::LdasArrayBase* array, const char* att )
{
    ostringstream ss;
    
    if (!get_elem_attr(dynamic_cast<ILwd::LdasElement*>(array), att, ss))
    {
      if ( strcasecmp( att, "mdorder" ) == 0 )
      {
        ss << arrayOrder2String( array->getArrayOrder() );
      }
      else if ( strcasecmp( att, "ndim" ) == 0 )
      {
        ss << array->getNDim();
      }
      else if ( strcasecmp( att, "dims" ) == 0 )
      {
        for ( unsigned int i = 0; i < array->getNDim(); ++i )
        {
	  ss << array->getDimension( i ) << " ";
        }
      }
      else if ( strcasecmp( att, "units" ) == 0 )
      {
        for ( unsigned int i = 0; i < array->getNDim(); ++i )
        {
	  ss << "\"" << array->getUnits( i ) << "\" ";
        }
      }
      else if ( strcasecmp( att, "byteorder" ) == 0 )
      {
#       ifdef WORDS_BIGENDIAN
        {
	  ss << "big";
        }
#       else
        {
            ss << "little";
        }
#       endif
      }
      else
      {
        throw SWIGEXCEPTION( "invalid_attribute" );
      }
    }

    return ss.str();
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */
    
        
#if HAVE_LDAS_PACKAGE_ILWD
std::string getLdasContainerAttribute(
    ILwd::LdasContainer* container, const char* att )
{
    ostringstream ss;
    
    if (!get_elem_attr(dynamic_cast<ILwd::LdasElement*>(container), att, ss))
    {
      if ( strcasecmp( att, "size" ) == 0 )
      {
        ss << container->size();
      }
      else
      {
        throw SWIGEXCEPTION( "invalid_attribute" );
      }
    }

    return ss.str();
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#if HAVE_LDAS_PACKAGE_ILWD
std::string getLdasStringAttribute(
    ILwd::LdasString* s, const char* att )
{
    ostringstream ss;
    
    if (!get_elem_attr(dynamic_cast<ILwd::LdasElement*>(s), att, ss))
    {
      if ( strcasecmp( att, "size" ) == 0 )
      {
	ss << s->getSize();
      }
      else if ( strcasecmp( att, "dims" ) == 0 )
      {
        ss << s->size();
      }
      else if ( strcasecmp( att, "units" ) == 0 )
      {
        for ( unsigned int i = 0; i < s->getNUnits(); ++i )
        {
	  ss << "\"" << s->getUnits( i ) << "\" ";
        }
      }
      else
      {
        throw SWIGEXCEPTION( "invalid_attribute" );
      }
    }

    return ss.str();
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
std::string getLdasExternalAttribute(
    ILwd::LdasExternal* ext, const char* att )
{
    ostringstream ss;
    
    if (!get_elem_attr(dynamic_cast<ILwd::LdasElement*>(ext), att, ss))
    {
      if ( strcasecmp( att, ILwd::LdasExternal::ATTR_STR_SIZE ) == 0 )
      {
        ss << ext->getSize();
      }
      else if ( strcasecmp( att, ILwd::LdasExternal::ATTR_STR_MIME_TYPE ) == 0 )
      {
	ss << ext->getMimeType();
      }
      else
      {
        throw SWIGEXCEPTION( "invalid_attribute" );
      }
    }

    return ss.str();
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
bool
get_elem_attr(ILwd::LdasElement* Element,
	      const char* Attr,
	      ostringstream& StringStream)
{
  bool	ret(true);

  
  if ( strcasecmp( Attr, "identifier" ) == 0 )
  {
    StringStream << Element->getIdentifier();
  }
  else if ( strcasecmp( Attr, ILwd::LdasElement::ATTR_STR_NAME ) == 0 )
  {
    StringStream << Element->getNameString();
  }
  else if ( strcasecmp( Attr, ILwd::LdasElement::ATTR_STR_COMMENT ) == 0 )
  {
    StringStream << Element->getComment();
  }
  else if ( strcasecmp( Attr, ILwd::LdasElement::ATTR_STR_JOBID ) == 0)
  {
    StringStream << Element->getJobId();
  }
  else
  {
    ILwd::LdasFormatElement* fe =
      dynamic_cast<ILwd::LdasFormatElement*>(Element);

    if (fe)
    {
      if ( strcasecmp( Attr, "format" ) == 0 )
      {	
	StringStream << format2String( fe->getWriteFormat() );
      }	
      else if ( strcasecmp( Attr, "compression" ) == 0 )
      {
	StringStream << compression2String( fe->getWriteCompression() );
      }
      else
      {
	ret = false;
      }
    }
    else
    {
      ret = false;
    }
  }
  return ret;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
template class ObjectRegistry< ILwdFile >;
#endif /* HAVE_LDAS_PACKAGE_ILWD */
