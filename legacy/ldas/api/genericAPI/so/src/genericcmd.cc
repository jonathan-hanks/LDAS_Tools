/* -*- c-basic-offset: 4 -*- */

#ifdef HAVE_CONFIG_H
#include "genericAPI/config.h"
#endif

// System Header Files
#include <sstream>
#include <unistd.h>
#include <sys/wait.h>
#include <cerrno>
#if HAVE_OPENSSL_BLOWFISH_H
#include <openssl/blowfish.h>
#endif

// ObjectSpace Header Files
#include "general/undef_ac.h"

#include <ospace/file.h>
#include <ospace/header.h>
#include <ospace/uss/network.h>
#include <ospace/uss/std/string.h>
#include <ospace/stream/bstream.h>

#include "general/base64.hh"
#include "general/Thread.hh"

// Local Header Files
#include "genericcmd.hh"
#include "util.hh"
#include "apiproxy.hh"
#include "thread.hh"
#include "registry.hh"


using General::Thread;
   
#if HAVE_LDAS_PACKAGE_ILWD
using ILwd::LdasElement;
using ILwd::LdasArrayBase;
using ILwd::LdasContainer;
#endif /* HAVE_LDAS_PACKAGE_ILWD */

using namespace std;   

//-----------------------------------------------------------------------------
//   
//: Set number of characters per line for Base64 encoded data.    
// 
// This function should be called by an API at an initialization time.
//   
//!param: const int num - Number of characters. Must be multiple of 4.
//
//!return: Nothing.
//   
void setBase64CharacterPerLineLimit( const int num )
{
   return Base64::setNumCharactersPerLine( num );
}

   
char*
base64decode(char* in)
{
  // convert base64 data to binary
  int bs = Base64::calculateDecodedSize(in);
  if (bs <= 0) 
    throw SWIGEXCEPTION( "bad base64 data" );
  unsigned char *bin = (unsigned char *)malloc(bs);
  if (!bin) {
    throw SWIGEXCEPTION( "out of memory" );
  }
  (void)Base64::decode(bin, in);
  return (char *) bin;
}

char *
base64encode(char* in)
{
  unsigned long l = strlen(in) + 1;
  // convert to base64 data
  int bs = Base64::calculateEncodedSize(l);
  char *dest = (char *)malloc(bs);
  if (!dest) {
    throw SWIGEXCEPTION( "out of memory" );
  }
  (void)Base64::encode(dest, in, l);
  return dest;
}

char*
decrypt(char* in, char* key)
{
#if HAVE_OPENSSL_BLOWFISH_H
  BF_KEY k;
  BF_set_key(&k, strlen(key), (unsigned char *)key);

  // convert base64 data to binary
  int bs = Base64::calculateDecodedSize(in);
  if (bs <= 0) 
    throw SWIGEXCEPTION( "bad base64 data" );
     //unsigned char *bin = (unsigned char *)malloc(bs);
  unsigned char *bin = (unsigned char *)calloc(bs, sizeof( unsigned char ) );
  if (!bin) {
    throw SWIGEXCEPTION( "out of memory" );
  }
  size_t l = Base64::decode(bin, in);
  for (unsigned int i = 0; i < l; i+=8)
    BF_ecb_encrypt(bin+i, bin+i, &k, 0);
  return (char *) bin;
#else
  throw std::runtime_error( "blowfish decryption not supported" );
#endif
}

char *
encrypt(char* in, char* key)
{
#if HAVE_OPENSSL_BLOWFISH_H
  BF_KEY k;
  BF_set_key(&k, strlen(key), (unsigned char *)key);
  unsigned long original_l = strlen(in) + 1;
  unsigned long l = strlen(in) + 1;
  l+=(8-l%8);
  unsigned char *out = (unsigned char *) calloc(l, sizeof( unsigned char ) ); // have to pad to mod 64 bits for blowfish
     //unsigned char *out = (unsigned char *) malloc(l); // have to pad to mod 64 bits for blowfish
  if (!out)
    throw SWIGEXCEPTION( "out of memory" );
  memcpy(out, in, original_l);
  for (unsigned int i = 0; i < l; i+=8)
    BF_ecb_encrypt(out+i, out+i, &k, 1);
  // convert to base64 data
  int bs = Base64::calculateEncodedSize(l) + 1;
  char *dest = (char *)malloc(bs);
  if (!dest) {
    throw SWIGEXCEPTION( "out of memory" );
  }
  (void)Base64::encode(dest, out, l);
  free(out);
  return dest;
#else
  throw std::runtime_error( "blowfish decryption not supported" );
#endif
}

int UNIXfork() {
	return (int) fork();
}

int UNIXwaitpid(int pid) {
	return (int)waitpid((pid_t)pid,0,0);
}

#if 0
//-----------------------------------------------------------------------------
//
//: Save Generic API Data
//
// Saves internal data to a file.  This function saves a list of the current
// servers to a stream along with the LdasArrayBase & LdasBinary objects which
// have been instantiated in memory.
//
//!param: os_bstream& stream
//
void saveGenericApi( os_bstream& stream )
{
    Registry::serverRegistry.write( stream );
    Registry::elementRegistry.write( stream );
    Registry::binaryRegistry.write( stream );
}
#endif /* 0 */

//-----------------------------------------------------------------------------
//
//: Save API Data
//
// Saves internal data to a file.  This calls the save function for all loaded
// API's.
//
//!param: const char* filename - The file to save to.  This file is overwritten
//     if it already exists.
//
//!exc: SwigException
//
void save( const char* filename )
try
{
    // Open the file for output, over-writing if necessary
    os_file pfile( filename, os_open_control::create_always,
                   os_io_control::write_only_access );
    os_bstream stream( pfile );

    for ( unsigned int i = 0; i < Registry::apiStore.size(); ++i )
    {
        Registry::apiStore[ i ]->save( stream );
    }
}
catch( os_toolkit_error& e )
{
    throw SWIGEXCEPTION( "file_creation_failed" );
}
CATCHSWIGUNEXPECTED()


#ifdef NOT_USED_CODE
//-----------------------------------------------------------------------------
//
//: Restores Generic API Data
//
// Restores internal data from a stream.  This function restores the internal
// state of servers, LdasArrayBase & LdasBinary objects to that which is
// specified on a stream.
//
//!param: os_bstream& stream
//!param: std::stringstream& pointermap - A mapping between old pointers and new 
//+       pointers will be written to this stringstream.  First the old      
//+       pointer and then the new pointer:                               
//+                                                                     
//+    <p>oldpointer1 newpointer1 oldpointer2 newpointer2 ...    
//
//!exc: SwigException - One of: <table>                                       +
//!exc:     <row> file_not_found  </row>                                      +
//!exc:     <row> std::bad_alloc       </row>                                      +
//!exc:     <row> bad_file_format </row></table>
//
void restoreGenericApi( os_bstream& stream, std::stringstream& pointermap )
{
    // Read the servers
    size_t q;
    stream >> q;    
    for ( unsigned int i=0; i < q; i++ )
    {
        INT_4U oldPointer;
        os_socket_address address;
            
        stream >> oldPointer >> address;
            
        pointermap << '_' << std::hex << oldPointer
                   << "_os_tcp_connection_server_p ";
            
        try
        {
            os_tcp_connection_server* server =
                new os_tcp_connection_server(
                    address );
                
            Registry::serverRegistry.registerObject( server );
                
            pointermap << std::hex << (INT_8U)(server)
                       << "_os_tcp_connection_server_p ";
        }
        catch( os_network_toolkit_error& e )
        {
            pointermap << "NULL ";
        }
    }
        
    // Read Elements
    stream >> q;    
    for ( unsigned int i=0; i < q; i++ )
    {
        LdasElement* element = 0;
        std::string oldPointer;
            
        stream >> oldPointer >> element;
            
        Registry::elementRegistry.registerObject( element );

        pointermap << oldPointer << " " << createElementPointer( element );
    }
    
    // Read Raw Binary
    stream >> q;
    for ( unsigned int i=0; i < q; i++ )
    {
        LdasBinary* binary = 0;
        INT_4U oldPointer;
            
        stream >> oldPointer >> binary;
            
        Registry::binaryRegistry.registerObject( binary );
            
        pointermap << makeTclPointer( (void*)oldPointer, "LdasBinary" )
		   << makeTclPointer( binary, "LdasBinary" );
    }
}

#endif


//-----------------------------------------------------------------------------
//
//: Restore API data.
//
// Restores internal data from a file.  This calls the restore functions for
// all of the loaded API's.
//
//!param: const char* filename - The filename to load from.  This file must
//     have been written via the save command.
//
//!return: string - String containing a char* to a TCL list.
//!return:    This Tcl list is a map of old pointer values to new pointer
//!return:    values. When the objects are read from the file, they will
//!return:    generally have a different address in memory.  This list
//!return:    provides the old pointer value followed by its new value, i.e.:
//!return:
//!return:    <p>oldpointer1 newpointer1 oldpointer2 newpointer2 ...
//
//!exc: SwigException - One of: <table>                                       +
//!exc:     <row> file_not_found  </row>                                      +
//!exc:     <row> std::bad_alloc       </row>                                      +
//!exc:     <row> bad_file_format </row>                                      +
//!exc:     </table>
std::string restore( const char* filename )
    try
{
    stringstream pointermap;
        
    os_file file( filename, os_open_control::open_existing,
                  os_io_control::read_only_access );
    os_bstream stream( file );
        
    for ( unsigned int i = 0; i < Registry::apiStore.size(); ++i )
    {
        Registry::apiStore[ i ]->restore( stream, pointermap );
    }

    return pointermap.str();
}
catch( SwigException& e )
{
    throw;
}
catch( os_file_toolkit_error& e )
{
    // This isn't quite right
    throw SWIGEXCEPTION( "file_open_failed" );
}
catch( os_toolkit_error& e )
{
    throw SWIGEXCEPTION( "bad_file_format" );
}
catch( std::bad_alloc& e )
{
    throw SWIGEXCEPTION( e );
}
catch( ... )
{
    throw SWIGEXCEPTION( "unexpected_exception" );
}



//
// Resets the api to its initial state.  This method closes all connections,
// halts all servers, and deallocates all memory in use by the API.
//
void resetGenericApi()
    throw( SwigException )
    try
{
    // destruct the elements and raw binary objects.
#if HAVE_LDAS_PACKAGE_ILWD
    Registry::elementRegistry.reset();
#endif /* HAVE_LDAS_PACKAGE_ILWD */
    Registry::binaryRegistry.reset();

    // close and destruct the sockets.  The sockets were created with
    // auto_close set to true, so they will close when destructed.
    Registry::serverRegistry.reset();
    Registry::activeSocketRegistry.reset();
    Registry::passiveSocketRegistry.reset();
}
catch( ... )
{
    throw SWIGEXCEPTION( "unexpected_exception" );
}


#if HAVE_LDAS_PACKAGE_ILWD
//-----------------------------------------------------------------------------
//
//: Get the list of currently registered element pointers
//
std::string getElementList()
{
    return Registry::elementRegistry.getElementList();
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

//-----------------------------------------------------------------------------
//
//: Get the number of elements in the registries
//
// This command returns a list with numbers of objects in registries
// in the following order: elementRegistry, binaryRegistry,
// serverRegistry, activeSocket, passiveSocket.
//
std::string getRegistrySize(void)
{
    ostringstream ss;

    ss << "{ ";

    ss
#if HAVE_LDAS_PACKAGE_ILWD
	<< Registry::elementRegistry.size() << " "
#endif /* HAVE_LDAS_PACKAGE_ILWD */
	<< Registry::binaryRegistry.size() << " "
	<< Registry::serverRegistry.size() << " "
	<< Registry::activeSocketRegistry.size() << " "
	<< Registry::passiveSocketRegistry.size()
	<< " }";
    
    return ss.str();
}

#if HAVE_LDAS_PACKAGE_ILWD
//-----------------------------------------------------------------------------
//
//: Destroy All elements in the registry
//
// This command destroys all the elements in the element registry
//
void destroyElements()
{
    Registry::elementRegistry.reset();
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


//-----------------------------------------------------------------------------
//
//: Reset the API's
//
// This command calls the reset functions for all loaded API's.
//
//!except: SwigException
//
void resetApi() throw( SwigException )
{
    for ( unsigned int i = 0; i < Registry::apiStore.size(); ++i )
    {
        Registry::apiStore[ i ]->reset();
    }
}

unsigned int getThreadStackSize( void )
{
    size_t	ssize( Thread::StackSizeDefault( ) );
    int		ret = 0;
    pthread_attr_t attr;

    pthread_attr_init (&attr);
    if ( ssize )
    {
	(void)pthread_attr_setstacksize (&attr, ssize);
    }
    ret = pthread_attr_getstacksize (&attr, &ssize);
    pthread_attr_destroy (&attr);
    
    if ( ret != 0 )
    {
	return General::Thread::StackSizeDefault( );
    }
    return ssize;
}

void setThreadStackSize( unsigned int ssize )
{
  // See if the thread stack size is valid
  pthread_attr_t attr;
  pthread_attr_init (&attr);
  int ret = 0;
  if ( ssize != 0 )
  {
      pthread_attr_setstacksize (&attr, ssize);
  }
  pthread_attr_destroy (&attr);

  if ( ret != 0 )
  {
      throw SWIGEXCEPTION( "invalid_size" );
  }
  Thread::StackSizeDefault( ssize );
}

std::string getThreadStatus( tid* t )
{
    if ( !Registry::isThreadValid( t ) )
    {
        throw SWIGEXCEPTION( "invalid_tid" );
    }
    
    return threadStatus2String( *t );
}

std::string getThreadFunction( const tid* t )
{
    if ( !Registry::isThreadValid( t ) )
    {
        throw SWIGEXCEPTION( "invalid_tid" );
    }
    
    return t->getFunction();
}
   
   
void cancelThread( tid* t )
{
    if ( !Registry::isThreadValid( t ) )
    {
        throw SWIGEXCEPTION( "invalid_tid" );
    }
    if ( t )
    {
	t->Cancel( );
    }
    Registry::threadRegistry.destructObject( t );
}

std::string getThreadList()
{
    return Registry::threadRegistry.getThreadList();
}


#if HAVE_LDAS_PACKAGE_ILWD
ILwdFile* openILwdFile( const char* filename, const char* mode )
{
    ILwdFile* file = 0;
    int a_mode;
    std::ios::openmode o_mode;
    
    if ( strcasecmp( mode, "r" ) == 0 )
    {
        a_mode = R_OK;
        o_mode = std::ios::in;
    }
    else if ( strcasecmp( mode, "w" ) == 0 )
    {
        a_mode = W_OK;
        o_mode = std::ios::out;
    }
    else if ( strcasecmp( mode, "a" ) == 0 )
    {
        a_mode = W_OK;
        o_mode = std::ios::app;
    }
    else
    {
        throw SWIGEXCEPTION( "invalid_mode: valid modes are r|w|a ." );
    }

    if ( a_mode == R_OK )
    {
        file = new InputILwdFile( filename, o_mode );
    }
    else
    {
        file = new OutputILwdFile( filename, o_mode );
    }

    ilwdFileRegistry.registerObject( file );
    return file;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


#if HAVE_LDAS_PACKAGE_ILWD
void closeILwdFile( ILwdFile* file )
{
    if ( !isILwdFileValid( file ) )
    {
        throw SWIGEXCEPTION( "invalid_ilwd_file" );
    }

    ilwdFileRegistry.destructObject( file );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */


//---------------------------------------------------------------------
//: unlimit the size of core files
//
//!exc: SwigException - reports the os error that resulted from
//+	setrlimit() system call.
void unlimitCoreSize( )
{
    struct rlimit limits;

    getrlimit( RLIMIT_CORE, &limits );	// get hard limit
    limits.rlim_cur = limits.rlim_max;	// soft limit = hard limit


    if ( setrlimit( RLIMIT_CORE, &limits ) != 0 )
    {
	std::ostringstream	is;

	is << "OS Error: " << errno << ": " << strerror( errno );
	throw SwigException( is.str(), __FILE__, __LINE__ );
    }
}

namespace
{
    Registry::ApiProxy genericApiProxy( "Generic", &resetGenericApi );
}


