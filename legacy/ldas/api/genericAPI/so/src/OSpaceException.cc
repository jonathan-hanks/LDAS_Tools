#include "genericAPI/config.h"

#ifdef HAVE_LIBOSPACE

#include <errno.h>

#include <sstream>

#include "OSpaceException.hh"

//=======================================================================
// OSpaceException
//=======================================================================
GenericAPI::OSpaceException::
OSpaceException( const os_toolkit_error& e )
  : SwigException( fmt_result( e ), fmt_info( e ) )
{
}

GenericAPI::OSpaceException::
OSpaceException( const os_toolkit_error& e, const char* File, int Line )
  : SwigException( fmt_result( e ), fmt_info( e ), File, Line )
{
}

std::string
GenericAPI::OSpaceException::
fmt_info( const os_toolkit_error& e )
{
   std::string error( e.what() );
    
    std::string::size_type p = error.find( '[' );
    std::string info;
    
    if ( p != std::string::npos )
    {
      info = error.substr( p-1 );
    }
    else
    {
      info = error;
    }
    info += "\n   ";
    return info;
}

std::string
GenericAPI::OSpaceException::
fmt_result( const os_toolkit_error& e )
{
   std::string error( e.what() );
    
    std::string::size_type p = error.find( '[' );
    std::string result;
    
    if ( p != std::string::npos )
    {
      result = error.substr( 0, p-1 );
    }
    if (errno != 0)
    {
      result += " ";
      result += SwigException::os_error();
      errno = 0;		// Reset as not to duplicate system err msgs
    }
    return result;
}

//=======================================================================
// ErrBindFailure
//=======================================================================
const char* GenericAPI::ErrBindFailure::
mOsError = "system_call_failure: ::bind";

GenericAPI::ErrBindFailure::
ErrBindFailure( const os_socket_address& address ) 
  : SwigException( std::string("bind_failure: ") +
		   inet_ntoa( address.ip_address() ) + os_error() )
{
  std::ostringstream	msg;
  msg << address.port( );
  mResult += msg.str( );
}

GenericAPI::ErrBindFailure::
ErrBindFailure( const os_socket_address& address,
		const char* file, int line ) 
  : SwigException( std::string("bind_failure: ") +
		   inet_ntoa( address.ip_address( ) ) + os_error(),
		   file, line )
{
  std::ostringstream	msg;
  msg << address.port( );
  mResult += msg.str( );
}

//=======================================================================
// ErrConnectionFailure
//=======================================================================

const char* GenericAPI::ErrConnectFailure::
mOsError = "system_call_failure: ::connect";
   
GenericAPI::ErrConnectFailure::
ErrConnectFailure( const char* host, const int port )
  : SwigException( std::string("connect_failure: address=") + host )
{
    char buffer[10];
    sprintf( buffer, "%u", port );
    mResult += " port=";
    mResult += buffer;
}

GenericAPI::ErrConnectFailure::
ErrConnectFailure( const char* host, const int port,
		   const char* file, int line ) 
  : SwigException( std::string("connect_failure: address=") + host,
		   file, line )
{
    char buffer[10];
    sprintf( buffer, "%u", port );
    mResult += " port=";
    mResult += buffer;
}

//=======================================================================
// ErrIllegalConnection
//=======================================================================
GenericAPI::ErrIllegalConnection::
ErrIllegalConnection( const os_socket_address& address )
  : SwigException( std::string( "illegal_connection: ")
		   + inet_ntoa( address.ip_address() ) )
{
  std::ostringstream	msg;
  msg << address.port( );
  mResult += msg.str( );
}

GenericAPI::ErrIllegalConnection::
ErrIllegalConnection( const os_socket_address& address,
		      const char* file, int line ) 
  : SwigException( std::string( "illegal_connection: ")
		   + inet_ntoa( address.ip_address() ) + " ",
		   file, line )
{
  std::ostringstream	msg;
  msg << address.port( );
  mResult += msg.str( );
}

#endif // HAVE_LIBOSPACE

