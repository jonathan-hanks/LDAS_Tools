/* -*- mode: c++ ; c-basic-offset: 2; -*- */
#ifndef GENERIC_API__LDAS_INFO_HH
#define GENERIC_API__LDAS_INFO_HH

#include <string>

namespace GenericAPI
{
  //---------------------------------------------------------------------
  /// \brief Information about the current ldas system.
  ///
  ///
  //---------------------------------------------------------------------
  class LDASInfo
  {
  public:
    inline static const std::string&  API( )
    {
      return m_api_name;
    }

    inline static void API( const std::string& Value )
    {
      m_api_name = Value;
    }

    inline static const std::string&  Site( )
    {
      return m_site_name;
    }

    inline static void Site( const std::string& Value )
    {
      m_site_name = Value;
    }

    inline static const std::string&  System( )
    {
      return m_system_name;
    }

    inline static void System( const std::string& Value )
    {
      m_system_name = Value;
      site_via_system( Value );
    }

    //-------------------------------------------------------------------
    /// \brief Track the current value of a TCL variable for the C++ layer.
    ///
    /// \param[in] Name
    ///     The name of the TCL variable.
    ///
    /// \param[in] Value
    ///     The TCL value associateed with \a Name.
    //-------------------------------------------------------------------
    static void VariableString( const std::string& Name,
				const std::string& Value );

    //-------------------------------------------------------------------
    /// \brief Retrieve the value of a TCL variable
    ///
    /// \param[in] Name
    ///     The name of the TCL variable.
    ///
    /// \return
    ///     The TCL value associateed with \a Name.
    //-------------------------------------------------------------------
    static const std::string& VariableString( const std::string& Name );

  private:
    static std::string	m_api_name;
    static std::string	m_site_name;
    static std::string	m_system_name;

    static void site_via_system( const std::string& Value );
  }; // class - LDASInfo
} // namespace - GenericAPI

#endif /* GENERIC_API__LDAS_INFO_HH */
