#ifndef GENERIC_API__SYS_CALL_INTERFACE_HH
#define GENERIC_API__SYS_CALL_INTERFACE_HH

namespace GenericAPI
{
  class SysCallInterface
  {
  public:
    virtual ~SysCallInterface( );

    virtual std::string
    Debug( ) const = 0;

    virtual void Init( ) = 0;

    virtual SysCallInterface* vnew( ) const = 0;
  };

  inline SysCallInterface::
  ~SysCallInterface( )
  {
  }
}

#endif /* GENERIC_API__SYS_CALL_INTERFACE_HH */
