#ifndef GENERIC_API__OSPACE_EXCEPTION_HH
#define GENERIC_API__OSPACE_EXCEPTION_HH

#include "genericAPI/config.h"

#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <string>

#include "ospace/network.h"

#include "genericAPI/swigexception.hh"

namespace GenericAPI
{
  class OSpaceException
    : public SwigException
  {
  public:
    OSpaceException( const os_toolkit_error& e );
    OSpaceException( const os_toolkit_error& e, const char* File, int Line );

  private:
    static std::string fmt_info( const os_toolkit_error& e );
    static std::string fmt_result( const os_toolkit_error& e );
  };

  //--------------------------------------------------------------------------
  //
  // ObjectSpace bind Failure
  //
  // This exception is generated when the program is unable to bind a socket to
  // an address.
  //
  class ErrBindFailure
    : public SwigException
  {
  public:
    
    /* Constructor */
    ErrBindFailure( const os_socket_address& address );
    ErrBindFailure( const os_socket_address& address, const char* file,
                    int line );

    static bool osMatch( const char* msg )
    {
      return strncmp( msg, mOsError, strlen( mOsError ) ) == 0;
    }

  private:

    static const char* mOsError;
  };

  //---------------------------------------------------------------------------
  //
  // ObjectSpace Connect Failure
  //
  // This exception is generated when the program is unable to connect a socket
  // to an address.
  //
  class ErrConnectFailure
    : public SwigException
  {
  public:

    ErrConnectFailure( const char* host, const int port );
    ErrConnectFailure( const char* host, const int port,
                       const char* file, int line );
    
    static bool osMatch( const char* msg )
    {
      return strncmp( msg, mOsError, strlen( mOsError ) ) == 0;
    }

  private:

    static const char* mOsError;
  };

  class ErrIllegalConnection : public SwigException
  {
  public:
    ErrIllegalConnection( const os_socket_address& address );

    ErrIllegalConnection( const os_socket_address& address,
                          const char* file, int line );
  };

} // namespace - GenericAPI


#if defined __GNUC__ && defined __sun__ && 3 ==__GNUC__ && 2 == __GNUC_MINOR__
#define ERRBINDFAILURE( a ) GenericAPI::ErrBindFailure( a )
#define ERRCONNECTFAILURE( a, b ) GenericAPI::ErrConnectFailure( (a), (b) )
#define ERRILLEGALCONNECTION( a ) GenericAPI::ErrIllegalConnection( a )
#define OSPACEEXCEPTION( a ) GenericAPI::OSpaceException( a )
#else
#define ERRBINDFAILURE( a ) GenericAPI::ErrBindFailure( a, __FILE__, __LINE__ )
#define ERRCONNECTFAILURE( a, b ) GenericAPI::ErrConnectFailure( (a), (b), __FILE__, __LINE__ )
#define ERRILLEGALCONNECTION( a ) GenericAPI::ErrIllegalConnection( a, __FILE__, __LINE__ )
#define OSPACEEXCEPTION( a ) GenericAPI::OSpaceException( a, __FILE__,__LINE__ )
#endif


#endif // HAVE_LIBOSPACE
#endif /* GENERIC_API__OSPACE_EXCEPTION_HH */
