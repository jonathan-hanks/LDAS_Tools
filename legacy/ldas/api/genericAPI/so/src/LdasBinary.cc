#include "genericAPI/config.h"

#include "inputfile.hh"

#include <string.h>

#include "genericAPI/LdasBinary.hh"

#ifdef HAVE_LIBOSPACE

#include <ospace/source.h>
#include "general/ospaceid.hh"

OS_STREAMABLE_0( (LdasBinary*), Stream::GenericAPI::LDASBINARY )


//-----------------------------------------------------------------------------
//   
//: Write to the stream.   
//      
// This method writes an LdasBinary object to the stream. 
// mPointer=0 if mBytes=0.
// 
//!param: os_bstream& stream - A reference to the stream to write to.
//!param: const LdasBinary& b - A const reference to the LdasBinary object   +
//!param:    to write to the stream.
//   
void os_write( os_bstream& stream, const LdasBinary& b )
{
    stream << b.mBytes;
    if ( b.mPointer != 0 )
    {
        stream.write_chunk( b.mPointer, b.mBytes );
    }
}

   
//-----------------------------------------------------------------------------
//   
//: Read from the stream.   
//      
// This method reads an LdasBinary object from the stream. 
// mPointer=0 if mBytes=0.
// 
//!param: os_bstream& stream - A reference to the stream to read from.
//!param: const LdasBinary& b - A const reference to the LdasBinary object   +
//!param:    to read from the stream.   
//      
void os_read( os_bstream& stream, LdasBinary& b )
{
    if (b.mPointer)
    {
      delete[] b.mPointer;
      b.mPointer = static_cast<unsigned char *>( NULL );
    }

    stream >> b.mBytes;
    if ( b.mBytes != 0 )
    {
        b.mPointer = new unsigned char[ b.mBytes ];
        stream.read_chunk( b.mPointer, b.mBytes );
    }
}    
#endif // HAVE_LIBOSPACE


//-----------------------------------------------------------------------------
//   
//: Constructor.   
//   
// If null pointer, no copy, 0 bytes
// If 0 bytes, no copy, null pointer.
//   
//!param: void* m - A pointer to the data to copy into the object.
//!param: unsigned int bytes - Number of bytes in the data.
//!param: bool ownsdata - A flag to indicate if object owns the data. Default +
//!param:   is true.
//
//!exc: std::bad_alloc - Memory allocation failed.
//   
LdasBinary::LdasBinary( void* m, unsigned int bytes, bool ownsdata )
        : mBytes( bytes )
{
    if ( m == 0 )
    {
        mPointer = 0;
        mBytes = 0;
    }
    else if ( mBytes == 0 )
    {
        mPointer = 0;
    }
    else
    {
        mPointer = new unsigned char[ bytes+1 ];
        memcpy( mPointer, m, bytes );
        mPointer[bytes] = '\0';
    }
}

   
//-----------------------------------------------------------------------------
//
//: Copy constructor.
//   
//!param: const LdasBinary& b - A const reference to the LdasBinary object   +
//!param:    to copy. 
//
//!exc: std::bad_alloc - Memory allocation failed.
//   
LdasBinary::LdasBinary( const LdasBinary& b )
        : mPointer( b.mPointer ), mBytes( b.mBytes )
{
    if ( mPointer != 0 )
    {
        mPointer = new unsigned char[ mBytes+1 ];
        memcpy( mPointer, b.mPointer, mBytes );
        mPointer[mBytes] = '\0';
    }
}

   
//-----------------------------------------------------------------------------
//   
//: Destructor.   
//      
LdasBinary::~LdasBinary()
{
    delete[] mPointer;
    mPointer = 0;
}

   
//-----------------------------------------------------------------------------
//   
//: Overloaded assignment operator.   
//      
//!param: const LdasBinary& b - A constant reference to the LdasBinary object.
//
//!return: LdasBinary& - A reference to the LdasBinary object.
//   
//!exc: std::bad_alloc - Memory allocation failed.
//   
LdasBinary& LdasBinary::operator=( const LdasBinary& b )
{
    if ( &b == this )
    {
        return *this;
    }

    delete[] mPointer;
    mPointer = 0;
    
    mBytes = b.mBytes;

    if ( b.getPointer() != 0 )
    {
        mPointer = new unsigned char[ mBytes+1 ];
        memcpy( mPointer, b.mPointer, mBytes );
        mPointer[mBytes] = '\0';
    }
    else
    {
        mPointer = static_cast<unsigned char *>(b.getPointer());
    }
    
    return *this;
}
