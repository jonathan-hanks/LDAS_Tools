#include "genericAPI/config.h"

#include "general/gpstime.hh"
#include "general/mutexlock.hh"
#include "general/unordered_map.hh"

#include "genericAPI/TclSymbolTable.hh"

using General::GPSTime;
using GenericAPI::TCLSymbolTable;

struct symbol_info {
  GPSTime			s_time_stamp;
  TCLSymbolTable::number_type	s_number_value;
  TCLSymbolTable::string_type	s_string_value;
};

typedef General::unordered_map< TCLSymbolTable::symbol_type,
				symbol_info > symbol_table_type;

MutexLock::lock_type	symbol_table_baton = MutexLock::Initialize( );
inline static symbol_table_type& symbol_table( )
{
  static symbol_table_type st;

  return st;
}

namespace GenericAPI
{
  const char* TCLSymbolTable::LDAS_ARC = "::LDASARC";
  const char* TCLSymbolTable::LDAS_API = "::API";
  const char* TCLSymbolTable::LDAS_LOG_DIR = "::LDASLOG";

  bool TCLSymbolTable::
  Changed( const symbol_type& SymbolName,
	   const timestamp_type& TimeStamp )
  {
    MutexLock l( symbol_table_baton );

    symbol_table_type::const_iterator
      v = symbol_table( ).find( SymbolName );
    if ( v != symbol_table( ).end( ) )
    {
      return TimeStamp != v->second.s_time_stamp;
    }
    return false;
  }

  void TCLSymbolTable::
  Lookup( const symbol_type& SymbolName,
	  string_type& Value,
	  timestamp_type* TimeStamp )
  {
    MutexLock l( symbol_table_baton );

    symbol_table_type::const_iterator
      v = symbol_table( ).find( SymbolName );
    if (  v != symbol_table( ).end( ) )
    {
      Value = v->second.s_string_value;
      if ( TimeStamp )
      {
	*TimeStamp = v->second.s_time_stamp;
      }
    }
    else
    {
      std::ostringstream	msg;

      msg << "No such symbol: " << SymbolName;

      throw std::range_error( msg.str( ) );
    }
  }

  bool TCLSymbolTable::
  IsDefined( const symbol_type& SymbolName )
  {
    MutexLock l( symbol_table_baton );

    return ( symbol_table( ).find( SymbolName )
	     != symbol_table( ).end( ) );
  }

  void TCLSymbolTable::
  Lookup( const symbol_type& SymbolName,
	  number_type& Value,
	  timestamp_type* TimeStamp )
  {
    MutexLock l( symbol_table_baton );

    symbol_table_type::const_iterator
      v = symbol_table( ).find( SymbolName );
    if (  v != symbol_table( ).end( ) )
    {
      Value = v->second.s_number_value;
      if ( TimeStamp )
      {
	*TimeStamp = v->second.s_time_stamp;
      }
    }
    else
    {
      std::ostringstream	msg;

      msg << "No such symbol: " << SymbolName;

      throw std::range_error( msg.str( ) );
    }
  }

  void TCLSymbolTable::
  Set( const symbol_type& SymbolName,
       const string_type& Value )
  {
    std::istringstream	nstream( Value );
    number_type		n;

    nstream >> n;

    MutexLock l( symbol_table_baton );

    symbol_info& si = symbol_table( )[ SymbolName ];
    si.s_time_stamp.Now( );
    si.s_string_value = Value;
    si.s_number_value = n;
  }

  void TCLSymbolTable::
  Set( const symbol_type& SymbolName,
       const number_type Value )
  {
    std::ostringstream	s;

    s << Value;

    MutexLock l( symbol_table_baton );

    symbol_info si = symbol_table( )[ SymbolName ];
    si.s_time_stamp.Now( );
    si.s_string_value = s.str( );
    si.s_number_value = Value;
  }

  void TCLSymbolTable::
  Unset( const symbol_type& SymbolName )
  {
    MutexLock l( symbol_table_baton );

    symbol_table( ).erase( SymbolName );
  }

  //---------------------------------------------------------------------
  // Register TCL variables that should be watched for changes.
  // The variables are currently restricted to the global namespace.
  //---------------------------------------------------------------------
  void TCLSymbolTable::
  Watch( Tcl_Interp* Interp,
	 const char* Name1,
	 const char* Name2 )
  {
    int Flags
      = TCL_GLOBAL_ONLY | TCL_TRACE_WRITES
      | TCL_TRACE_UNSETS | TCL_TRACE_DESTROYED
      ;
    const char* n2 = ( Name2 && *Name2 ) ? Name2 : (const char*)NULL;
    //-------------------------------------------------------------------
    // Lookup the variable to see if it currently has a value.
    //-------------------------------------------------------------------
    const char* value = Tcl_GetVar2( Interp, Name1, n2,
				     ( Flags & ( TCL_GLOBAL_ONLY ) ) );
    if ( value )
    {
      //-----------------------------------------------------------------
      // Since it has a value, store it in the symbol table
      //-----------------------------------------------------------------
      std::ostringstream	v;
      v << Name1;
      if ( n2 )
      {
	v << "(" << n2 << ")";
      }

      Set( v.str( ), value );
    }
    //-------------------------------------------------------------------
    // Check if the variable is already being traced.
    //-------------------------------------------------------------------
    if ( Tcl_VarTraceInfo2( Interp, Name1, n2, Flags,
			    TCLSymbolTable::changeCallback,
			    (ClientData)NULL )
	 == (ClientData)NULL )
    {
      //-----------------------------------------------------------------
      // Register callback for changes to the state of the variable
      //-----------------------------------------------------------------
      Tcl_TraceVar2( Interp, Name1, n2, Flags,
		     TCLSymbolTable::changeCallback,
		     (ClientData)NULL );
    }
  }

  char* TCLSymbolTable::
  changeCallback( ClientData CD,
		  Tcl_Interp* Interp,
		  CONST84 char* Name1,
		  CONST84 char* Name2,
		  int Flags )
  {
    std::ostringstream	v;
    v << Name1;
    if ( Name2 )
    {
      v << "(" << Name2 << ")";
    }

    if ( Flags & TCL_TRACE_WRITES )
    {
      //-----------------------------------------------------------------
      // The variable has changed its value.
      // Need to update the value in the symbol table.
      //-----------------------------------------------------------------
      TCLSymbolTable::symbol_type value( Tcl_GetVar2( Interp, Name1, Name2,
						      Flags & TCL_GLOBAL_ONLY ) );
      Set( v.str( ), value );
    }
    else if ( ( Flags & ( TCL_TRACE_UNSETS | TCL_TRACE_DESTROYED ) ) )
    {
      //-----------------------------------------------------------------
      // The variable is 
      //-----------------------------------------------------------------
      Unset( v.str( ) );
    }
    return (char*)NULL;
  }

} // namespace - GenericAPI
