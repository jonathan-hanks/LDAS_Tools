/* -*- c-basic-offset: 2; -*- */

#ifndef GENERIC_API__RLIMIT_HH
#define GENERIC_API__RLIMIT_HH

#include <string>

#include "general/unordered_map.hh"

namespace GenericAPI
{
  class RLimit
  {
  public:
    typedef unsigned long value_type;

    RLimit( );

    void Limit( const char* ResourceName, const char* Value ) const;
    void Limit( const char* ResourceName, value_type Value ) const;
    value_type Limit( const char* ResourceName ) const;
    std::string LimitString( const char* ResourceName ) const;

  private:
    typedef General::unordered_map< std::string, int > limit_table_type;
    static limit_table_type	m_limit_names_to_id;

    static void init( );
  };
}

#endif /* GENERIC_API__RLIMIT_HH */
