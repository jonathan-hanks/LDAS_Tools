/* -*- c-basic-offset: 4; -*- */

#ifndef RegistryHH
#define RegistryHH

#include "genericAPI/config.h"

#include "general/objectregistry.hh"
#include "general/undef_ac.h"

#include "genericAPI/LdasBinary.hh"

#include "genericAPI/RegistryILwd.hh"
#include "genericAPI/RegistryLibOSpace.hh"

#if 0
#include "file.hh"
#endif /* 0 */

namespace Registry
{    
    class BinaryRegistry_type
	: public ObjectRegistry< LdasBinary >
    {
    };

    extern BinaryRegistry_type binaryRegistry;

    bool isBinaryValid( const LdasBinary* binary ) throw();
}


#endif // RegistryHH
