#include "genericAPI/config.h"

#if HAVE_LDAS_PACKAGE_ILWD
#include <ilwd/ldaselement.hh>
#include <ilwd/ldasstring.hh>
#include <ilwd/ldasexternal.hh>
#include <ilwd/ldascontainer.hh>
#include <ilwd/ldasarraybase.hh>
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#include "genericAPI/UtilILwd.hh"

using ILwd::LdasContainer;
using ILwd::LdasElement;

#if HAVE_LDAS_PACKAGE_ILWD
bool contentsSearch( const LdasContainer& c, const LdasElement* element )
{
    bool found = false;

    for ( LdasContainer::const_iterator iter = c.begin();
          ( iter != c.end() ) && ( !found ); ++iter )
    {
        found = ( element == *iter );
        if ( !found && (**iter).getElementId() == ILwd::ID_ILWD )
        {
            found = contentsSearch(
                dynamic_cast< LdasContainer& >( **iter ), element );
        }
    }

    return found;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

