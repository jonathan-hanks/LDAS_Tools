/* -*- mode: c++; c-basic-offset: 4; -*- */
#ifndef ElementCmdHH
#define ElementCmdHH

#include "genericAPI/config.h"

#include <string>

#if HAVE_LDAS_PACKAGE_ILWD
// LDAS Includes
#include "swigexception.hh"
#include <ilwd/ldaselement.hh>
#include <ilwd/ldascontainer.hh>

#include "threaddecl.hh"
#include "ilwdfile.hh"

class os_tcp_connection_server;
class os_tcp_socket;

extern "C"
{
#if defined(__clang__)
#pragma clang diagnostic push
# if __has_warning("-Wreturn-type-c-linkage")
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif

    void
    sendDataElement( ILwd::LdasElement* Element,
		     int ServerPort,
		     const char* ServerAddress,
		     int ClientPort = 0,
		     const char* ClientAddress = "" );

    CREATE_THREADED5V_DECL( sendDataElement,
			    ILwd::LdasElement*,
			    int,
			    const char*,
			    int,
			    const char* );

    std::string
    recvDataElement( os_tcp_connection_server* ServerSocket,
		     int LocalPort = 0,
		     const char* LocalAddress = "" );
    CREATE_THREADED3_DECL( recvDataElement,
			   std::string,
			   os_tcp_connection_server*,
			   int,
			   const char* );

    unsigned int getElementRefCount( ILwd::LdasElement* element );
    std::string castElementToContainer( ILwd::LdasElement* element );
    std::string concatElement( ILwd::LdasElement* e1, 
                               const ILwd::LdasElement* e2);

    void sendElementObject( os_tcp_socket* socket, ILwd::LdasElement* element );
    CREATE_THREADED2V_DECL(
        sendElementObject, os_tcp_socket*, ILwd::LdasElement* );
   
    ILwd::LdasElement* recvElementObjectCore( os_tcp_socket* socket );
    CREATE_THREADED1_DECL(
        recvElementObjectCore, ILwd::LdasElement*, os_tcp_socket* );
   
    std::string recvElementObject( os_tcp_socket* socket );
    CREATE_THREADED1_DECL( recvElementObject, std::string, os_tcp_socket* );
   
    void sendElementAscii( os_tcp_socket* socket, ILwd::LdasElement* element );
    CREATE_THREADED2V_DECL(
        sendElementAscii, os_tcp_socket*, ILwd::LdasElement* );
   
    std::string recvElementAscii( os_tcp_socket* socket );
    CREATE_THREADED1_DECL( recvElementAscii, std::string, os_tcp_socket* );
   
    ILwd::LdasElement* recvElementAsciiCore( os_tcp_socket* socket );
    CREATE_THREADED1_DECL( 
        recvElementAsciiCore, ILwd::LdasElement*, os_tcp_socket* );

    std::string getElement(
        ILwd::LdasElement* element, const char* f = "user",
        const char* compression = "user" );

    ILwd::LdasElement* putElementCore( const char* s );

    std::string putElement( const char* s );

    void destructElement( ILwd::LdasElement* element );

    void addContainerElement( ILwd::LdasContainer* container,
                              ILwd::LdasElement* element );
    std::string copyContainerElement( ILwd::LdasContainer* container,
                              unsigned int index );
    std::string getContainerElement( ILwd::LdasContainer* container,
                                unsigned int index, const char* f = "user",
                                const char* c = "user" );
    std::string refContainerElement( ILwd::LdasContainer* container,
                                unsigned int index );
    unsigned int getContainerSize( ILwd::LdasContainer* container );
    std::string getElementAttribute( ILwd::LdasElement* element,
				     const char* att );
    void setElementAttribute( ILwd::LdasElement* element,
			      const std::string att,
			      const std::string value );
    void clearElementMetadata( ILwd::LdasElement* elmenet );
    std::string getElementMetadata( ILwd::LdasElement* element,
				    std::string Name );
    void setElementMetadata( ILwd::LdasElement* elmenet,
			     std::string Name,
			     std::string Value );
    void unsetElementMetadata( ILwd::LdasElement* elmenet,
			       std::string Name );
    std::string getContainerTOC( ILwd::LdasContainer* container );
    std::string copyElement( ILwd::LdasElement* element );
    void mergeContainer( ILwd::LdasContainer* c1,
                          const ILwd::LdasContainer* c2 );
    void deleteContainerElement( ILwd::LdasContainer* c, unsigned int index );

    void writeElement( ILwdFile* file, ILwd::LdasElement* e,
                       std::string f, std::string c );
    CREATE_THREADED4V_DECL(writeElement,ILwdFile*, ILwd::LdasElement*,
			   const char*, const char*);
   
    std::string readElement( ILwdFile* file );
    CREATE_THREADED1_DECL(readElement, std::string, ILwdFile* );
    ILwd::LdasElement* readElementCore( ILwdFile* file );
    void setWriteFormat( ILwd::LdasElement* e, const char* f, const char* c );
    std::string getWriteFormat( ILwd::LdasElement* e );
    void setJobId( ILwd::LdasElement* e, const unsigned int JobId);
   
    bool isNullInElement( ILwd::LdasElement* Element, size_t Offset );
    void setNullInElement( ILwd::LdasElement* Element,
			   size_t Offset, bool Value );
#if defined(__clang__)
#pragma clang diagnostic pop
#endif

}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#endif // ElementCmdHH
