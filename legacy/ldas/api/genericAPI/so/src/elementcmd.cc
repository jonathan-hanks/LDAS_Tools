/* -*- c-basic-offset: 4 -*- */

#include "genericAPI/config.h"

#if HAVE_LDAS_PACKAGE_ILWD
// Local Header Files
#include <string>
#include <sstream>   

#include "ilwd/ldaselement.hh"
#include "ilwd/ldascontainer.hh"
#include "ilwd/ldasarraybase.hh"
#include "ilwd/ldasarray.hh"
#include "general/mutexlock.hh"
#include "general/refcount.hh"

#include "genericAPI/socketcmd.hh"

#include "elementcmd.hh"
#include "OSpaceException.hh"
#include "util.hh"
#include "thread.hh"
#include "registry.hh"

using ILwd::LdasElement;
using ILwd::LdasContainer;
using ILwd::LdasString;
using ILwd::LdasExternal;
using ILwd::LdasArray;
using ILwd::LdasArrayBase;
using namespace std;
   

inline std::string
invalid_element( const LdasElement* element )
{
    std::ostringstream	oss;

    oss << "invalid element: " << hex << (void*)element;
    return oss.str( );
}

void
sendElementObject_unlock( os_tcp_socket* socket, LdasElement* element );

void
sendDataElement( ILwd::LdasElement* Element,
		 int ServerPort,
		 const char* ServerAddress,
		 int ClientPort,
		 const char* ClientAddress )
{
  os_tcp_socket*	sock( createDataSocket( ClientPort,
						ClientAddress ) );
  connectDataSocket( sock, ServerAddress, ServerPort );
  sendElementObject( sock, Element );
  closeDataSocket( sock );
}

CREATE_THREADED5V( sendDataElement,
		   ILwd::LdasElement*,
		   int,
		   const char*,
		   int,
		   const char* )

std::string
recvDataElement( os_tcp_connection_server* ServerSocket,
		 int LocalPort,
		 const char* LocalAddress )
{
  os_tcp_socket*	sock( acceptDataSocket( ServerSocket,
						LocalAddress, LocalPort ) );
  std::string		retval( recvElementObject( sock ) );
  closeDataSocket( sock );
  return retval;
}

CREATE_THREADED3( recvDataElement,
		  std::string,
		  os_tcp_connection_server*,
		  int,
		  const char* )

//-----------------------------------------------------------------------------
//
//: Get element reference counter
//
//!usage: set refNum [ getElementRefCount ptElem ]
//
//!param: ptElem - a pointer to an element.
//
//!return: refNum - Current number of references.
//
unsigned int getElementRefCount( LdasElement* element )
{
  if (!Registry::isElementValid(element))
  {
    throw SWIGEXCEPTION( invalid_element( element ) );
  }

  return element->getRefCount();
}

//-----------------------------------------------------------------------------
// New functions that need to be documented
//-----------------------------------------------------------------------------

std::string castElementToContainer( LdasElement* element)
{
  return makeTclPointer( dynamic_cast<LdasContainer*>(element),
			 "LdasContainer");
}

/**
 * Appends the second element to the first.
 *
 * @param e1 (LdasElement*)
 * @param e2 (LdasElement*)
 *
 * @return LdasElement
 *
 * @exception invalid_element
 * @exception Elements of incompatable types
 * @exception NULL Source Element
 * @exception Unknown Element Type
 */
std::string concatElement(LdasElement* e1, const LdasElement* e2)
try
{
  if (!Registry::isElementValid(e1) )
  {
    throw SWIGEXCEPTION( invalid_element( e1 ) );
  }
  else if (!Registry::isElementValid(e2) )
  {
    throw SWIGEXCEPTION( invalid_element( e2 ) );
  }
  if (e1->getElementId() != e2->getElementId())
  {
    throw SWIGEXCEPTION("Elements of incompatable types" );
  }
  if (!e1)
  {
    throw SWIGEXCEPTION("NULL Source Element");
  }
  if (e1 && e2)
  {
    switch(e1->getElementId())
    {
#define	ARRAY_CONCAT(T) \
      dynamic_cast<LdasArray<T>&>(*e1) \
	+= dynamic_cast<const LdasArray<T>&>(*e2)

    case ILwd::ID_CHAR:   ARRAY_CONCAT(CHAR); break;
    case ILwd::ID_CHAR_U: ARRAY_CONCAT(CHAR_U); break;
    case ILwd::ID_INT_2S: ARRAY_CONCAT(INT_2S); break;
    case ILwd::ID_INT_2U: ARRAY_CONCAT(INT_2U); break;
    case ILwd::ID_INT_4S: ARRAY_CONCAT(INT_4S); break;
    case ILwd::ID_INT_4U: ARRAY_CONCAT(INT_4U); break;
    case ILwd::ID_INT_8S: ARRAY_CONCAT(INT_8S); break;
    case ILwd::ID_INT_8U: ARRAY_CONCAT(INT_8U); break;
    case ILwd::ID_REAL_4: ARRAY_CONCAT(REAL_4); break;
    case ILwd::ID_REAL_8: ARRAY_CONCAT(REAL_8); break;
    case ILwd::ID_COMPLEX_8: ARRAY_CONCAT(COMPLEX_8); break;   
    case ILwd::ID_COMPLEX_16: ARRAY_CONCAT(COMPLEX_16); break;      
      break;
#undef ARRAY_CONCAT
    case ILwd::ID_ILWD:
      {
	dynamic_cast<LdasContainer&>(*e1)
	  += dynamic_cast<const LdasContainer&>(*e2);
      }
      break;
    case ILwd::ID_LSTRING:
      dynamic_cast<LdasString&>(*e1)
	+= dynamic_cast<const LdasString&>(*e2);
      break;
    case ILwd::ID_EXTERNAL:
      dynamic_cast<LdasExternal&>(*e1)
	+= dynamic_cast<const LdasExternal&>(*e2);
      break;
    default:
      throw SWIGEXCEPTION( "Unknown Element Type" );
    }
  }
  return createElementPointer(e1);
}
catch( SwigException& e )
{
    throw;
}
CATCHSWIGUNEXPECTED()

void sendElementAscii_unlock( os_tcp_socket* socket, LdasElement* element )
try
{
  sendElementAscii(socket, element);
  if ( Registry::isElementValid( element ) ) {
    element->refCountDown();
  }
} catch (...)
{
  if ( Registry::isElementValid( element ) ) {
    element->refCountDown();  
  }
  throw;
}

void sendElementObject_unlock( os_tcp_socket* socket, LdasElement* element )
try
{
  sendElementObject(socket, element);
  if ( Registry::isElementValid( element ) ) {
    element->refCountDown();
  }
}
catch( ... )
{
  if ( Registry::isElementValid( element ) ) {
    element->refCountDown();
  }
  throw;
}


/**
 * Sends an LDAS element in XML form across a socket.
 *
 * @param socket (os_tcp_socket*)
 * @param element (LdasArrayBase*)
 *
 * @return void
 *
 * @exception invalid_socket
 * @exception invalid_element
 * @exception unexpected_exception
 */
void sendElementAscii( os_tcp_socket* socket, LdasElement* element )
try
{

    if ( !Registry::isElementValid( element ) )
    {
        throw SWIGEXCEPTION( invalid_element( element ) );
    }

    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    ostringstream ss;
    element->write( ss );

    os_bstream stream( *socket );
    stream << socket;
   
    return;
}
catch( SwigException& e )
{
    throw;
}
catch( os_toolkit_error& e )
{
    throw OSPACEEXCEPTION( e );
}
CATCHSWIGUNEXPECTED()


/**
 * Sends an LDAS element across a stream in object form.
 *
 * @param socket (os_tcp_socket*)
 * @param element (LdasArrayBase*)
 *
 * @return void
 *
 * @exception invalid_socket
 * @exception invalid_element
 * @exception unexpected_exception
 * @exception unconnected_socket
 */ 
void sendElementObject( os_tcp_socket* socket, LdasElement* element )
try
{
    if ( !Registry::isElementValid( element ) )
    {
        throw SWIGEXCEPTION( invalid_element( element ) );
    }

    if ( !Registry::isSocketValid( socket ) )
    {
        throw ERRINVALIDSOCKET();
    }
    if ( !(socket->connected() ) )
    {
        throw SWIGEXCEPTION( "unconnected_socket" );
    }
    
    os_bstream stream( *socket );
    stream << element;
}
catch( LdasException& e )
{
    throw SwigException( e );
}
catch( SwigException& e )
{
    throw;
}
catch( os_toolkit_error& e )
{
    throw OSPACEEXCEPTION( e );
}
CATCHSWIGUNEXPECTED()


//-----------------------------------------------------------------------------
//
//: Receives an LDAS element from a socket in object form.
//
//!param: socket (os_tcp_socket*)
//
//!return: LdasArrayBase* - The element instantiated from the socket.
//
//!exc: invalid_socket
//!exc: bad_alloc
//!exc: unexpected_exception
//!exc: unconnected_socket
//

std::string recvElementObject( os_tcp_socket* socket )
try{

  LdasElement* element = 0;
    
  try{
        
    element = recvElementObjectCore( socket );

    Registry::elementRegistry.registerObject( element );
        
    return createElementPointer( element );

  } catch( ... ) {

    delete element;
    element = 0;
    throw;

  }

} catch( SwigException& e ) {

  throw;

} catch( os_toolkit_error& e ) {

  throw OSPACEEXCEPTION( e );

} catch( LdasException& e ) {

  throw SwigException( e );

} catch( ... ) {

  throw SWIGEXCEPTION( "unexpected_exception" );

}

LdasElement* recvElementObjectCore( os_tcp_socket* socket )
try {

  LdasElement* element = 0;
     
  try{
        
    if ( !Registry::isSocketValid( socket ) ) {
      throw ERRINVALIDSOCKET();
    }

    if ( !(socket->connected() ) ) {
      throw SWIGEXCEPTION( "unconnected_socket" );
    }
                 
    os_bstream stream( *socket );
    stream >> element;

    return element;

  } catch( ... ) {

    delete element;
    element = 0;
    throw;

  }

} catch( SwigException& e ) {

  throw;

} catch( os_toolkit_error& e ) {

  throw OSPACEEXCEPTION( e );

} catch( LdasException& e ) {

  throw SwigException( e );

} catch( ... ) {

  throw SWIGEXCEPTION( "unexpected_exception" );

}
     
//-----------------------------------------------------------------------------
//
//: Receives an LDAS element from a socket in XML format.
//
//!param: socket (os_tcp_socket*)
//
//!return: LdasArrayBase* - A pointer to the newly instantiated object.
//
//!exc: invalid_socket
//!exc: bad_alloc
//!exc: unexpected_exception
//!exc: numerous LWD
//

std::string recvElementAscii( os_tcp_socket* socket )
try {

  LdasElement* element = recvElementAsciiCore( socket );
     
  Registry::elementRegistry.registerObject( element );

  return createElementPointer( element );

} catch( SwigException& e ) {

  throw;

} catch( os_network_toolkit_error& e ) {
     
  throw OSPACEEXCEPTION( e );

} catch( LdasException& e ) {

  //  e.addError( e, "bad_ldas_element" );
  throw;

}
CATCHSWIGUNEXPECTED()



LdasElement* recvElementAsciiCore( os_tcp_socket* socket )
try {

  if ( !Registry::isSocketValid( socket ) ) {
    throw ERRINVALIDSOCKET();
  }

  if ( !(socket->connected() ) ) {
    throw SWIGEXCEPTION( "unconnected_socket" );
  }

  os_bstream stream( *socket );
  char* s = 0;
  stream >> s;
  stringstream ss;
  ss << s;
  delete[] s;
  s = 0;
  ILwd::Reader r( ss );
  LdasElement* element =
     dynamic_cast< LdasElement* >( ILwd::LdasElement::createElement( r ) );
  
  return element;

} catch( SwigException& e ) {

  throw;

} catch( os_network_toolkit_error& e ) {

  throw OSPACEEXCEPTION( e );

} catch( LdasException& e ) {

  //  e.addError( e, "bad_ldas_element" );
  throw;

}
CATCHSWIGUNEXPECTED()

     
/**
 * Returns an ldas element in the specified format.
 *
 * <H4>Memory Usage</H4>
 *     The memory overhead required by this method comes from two sources.
 *     First of all, the method must generate a buffer in which to print the
 *     new element in the requested form.  In the case of an 'ascii' format,
 *     the size of this buffer is uncertain.  Then the SWIG layer must copy
 *     this buffer into a std::string for the Tcl layer.  So, the approximate
 *     memory usage for this method is 2x the size of the data (not including
 *     the memory already used by the object).  1/2 of this is reclaimed when
 *     the function returns, the other 1/2 appears in the tcl layer as the
 *     returned std::string.
 *
 *     For example, a 1 MB ldas element will consume 2 more MB while this
 *     method runs (for a total of 3 MB).  When the routine returns, the
 *     element still consumes 1 MB, but now the returned std::string consumes 1 MB
 *     in the Tcl layer (total of 2 MB).
 *
 * @param element (LdasArrayBase*)
 * @param f (const char*) the desired format.  This must be one of "internal",
 *     "ascii", "binary", or "base64".  This is case-insensitive.  This
 *     defaults to 'internal'.
 * @param c (int) the desired compression level.  Compression levels are
 *     from 0-9.  If a negative level is specified, then the compression level
 *     specified within the element is used (the compression attribute).
 *     A level greater than 9 will be interpreted as being equal to 9.  The
 *     default is '-1'.  This parameter is meaningless if the requested format
 *     is ascii (or if the internal format is requested and it is ascii).
 *
 * @return <b>string</b> - A string representation of the ILWD format element.
 *
 * @exception invalid_element - The C++ layer does not recognize the pointer
 *     parameter (element) as a valid LDAS element pointer.  This may occur
 *     if the element being pointed to has already been destructed.
 * @exception invalid_format - The format passed as the second parameter to the
 *     function is not recognized.  It must be one of the following (case
 *     insensitive): internal, ascii, base64, or binary.
 */
std::string getElement( LdasElement* element, const char* f, const char* c )
try
{
    // make sure the element pointer is valid
    if ( !Registry::isElementValid( element ) )
    {
        throw SWIGEXCEPTION( invalid_element( element ) );
    }
    
    // check to see if we need to convert the format
    ILwd::Format format = char2Format( f );
    ILwd::Compression compression = char2Compression( c );
    
    // write out the data
    ostringstream ss;
    if ( element->getElementId() == ILwd::ID_ILWD )
    {
        dynamic_cast< LdasContainer* >( element )->write(
            0, 4, ss, format, compression );
    }
    else
    {
        element->write( ss, format, compression );
    }

    return ss.str();
}
catch( LdasException& e )
{
   throw SwigException( e );
}
catch( SwigException& e )
{
    throw;
}
catch( ... )
{
    throw SWIGEXCEPTION( "unexpected_exception" );
}


/**
 * Instantiates an LDAS element in the C++ layer.  This method instantiates
 * an LDAS element object in the C++ layer corresponding to a given std::string.
 * The storage method can be specified as well.  The available methods are
 * ascii (the data is stored as an ascii representation of the numbers), base64
 * (the binary data is converted into the base64 format -- a subset of ascii
 * using 64 printable characters) and binary (the actual bytes for the data).
 * If the format is Base64 or binary, optional compression can be used.
 *
 * <H4>Memory Usage</H4>
 *     Currently, this method inserts the XML std::string into a stream in order
 *     to use the createLdasArray static method (which takes a stream as
 *     a parameter).  Thie means that the memory consumed by this method is
 *     ~2x the size of the XML std::string.  1/2 of this is deallcoated when the
 *     the method finishes, the other 1/2 is memory allocated for the returned
 *     LdasArray object.
 *
 * @param s (const char*) An XML representation of the LDAS element.
 * @param f (const char*) The format in which to store the data in the C++
 *     layer.  This must be one of four std::strings:
 *         <UL><LI>internal - Store the data as specified in the format
 *             attribute in the start tag (DEFAULT).</LI>
 *         <LI>ascii  </LI>
 *         <LI>base64 </LI>
 *         <LI>binary </LI></UL>
 * @param compression (int) The compression level to use in storing the data.
 *     Normal compression values are in the range 0-9.  A value greater than
 *     9 is interpreted as 9.  If the value is less than zero, then the
 *     internal compression (specified in the start-tag) is used.  This
 *     defaults to '-1'.  If the format is ascii (or if the format is
 *     'internal' and the internal format is ascii) then this parameter is
 *     ignored.
 *
 * @return <b>LdasArray*</b> - A pointer to the dynamically allocated
 *     element.
 *
 * @exception bad_element - The std::string 's' is not a valid XML LDAS element.
 * @exception illegal_format - The format specified is not valid.  The format
 *     must be one of internal, ascii, base64, or binary (case insensitive).
 */
std::string putElement( const char* s )
try {

  LdasElement* element = putElementCore( s );

  // register this object
  Registry::elementRegistry.registerObject( element );

  return createElementPointer( element );

} catch( SwigException& e ) { 
     
  throw;

} catch( LdasException& e ) {
     
//    ADDMESSAGE( e, "bad_element" );
  throw SwigException( e );

} catch( ... ) {
     
  throw SWIGEXCEPTION( "unexpected_exception" );

}


LdasElement* putElementCore( const char* s )
try {

  // Ensure the char* is not null.
  if ( s == 0 ) {
//    throw FORMATEXCEPTION( "bad_element" );
  }

  // Write the XML std::string into a stringstream object and then create the
  // element from the stream.  Eventually, the LdasArray object should
  // be able to initialize from a std::string in addition to a stream.
  stringstream ss( s );

  ILwd::Reader r( ss );

  LdasElement* element = ILwd::LdasElement::createElement( r );

  return element;

} catch( SwigException& e ) { 
     
  throw;

} catch( LdasException& e ) {
     
//    ADDMESSAGE( e, "bad_element" );
  throw SwigException( e );

} catch( ... ) {
     
  throw SWIGEXCEPTION( "unexpected_exception" );

}


/**
 * Destructs an LDAS element.  This method deallocates memory for an LDAS
 * element in the C++ layer.
 *
 * @param element (LdasArray*) The element to destruct.
 *
 * @exception invalid_element - The pointer value passed does not correspond to
 *     an LDAS element in the C++ layer.  This may occur when the element has
 *     already been destructed.
 */
void destructElement( LdasElement* element )
{
    // Make sure the pointer is valid
    // :GOTCHA: This doesn't use the isElementValid function since it also
    //          checks the contained elements.  We don't want to destruct
    //          contained elements.
    if ( !Registry::elementRegistry.isRegistered( element ) )
    {
        throw SWIGEXCEPTION( invalid_element( element ) );
    }

    element->waitForNoReferences();

    // destruct the object
    Registry::elementRegistry.destructObject( element );
}
void addContainerElement(
    ILwd::LdasContainer* container, ILwd::LdasElement* element )
{
    if ( !Registry::isElementValid( container ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    if ( !Registry::isElementValid( element ) )
    {
        throw SWIGEXCEPTION( invalid_element( element ) );
    }

    container->push_back( *element );
}

std::string copyContainerElement( ILwd::LdasContainer* container,
                             unsigned int index ) {
  if ( !Registry::isElementValid( container ) ) {
    throw SWIGEXCEPTION( "invalid_container" );
  }

  if ( index >= container->size() ) {
    throw SWIGEXCEPTION( "out_of_range" );
  }

  LdasElement* element = (*container)[ index ]->createCopy();

  Registry::elementRegistry.registerObject( element );

  return createElementPointer( element );
}


std::string refContainerElement( ILwd::LdasContainer* container,
                            unsigned int index )
{
    if ( !Registry::isElementValid( container ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    if ( index >= container->size() )
    {
        throw SWIGEXCEPTION( "out_of_range" );
    }

    return createElementPointer( (*container)[ index ] );
}

std::string getContainerElement( ILwd::LdasContainer* container,
                            unsigned int index, const char* f,
                            const char* c ) {
     
  if ( !Registry::isElementValid( container ) ) {
    throw SWIGEXCEPTION( "invalid_container" );
  }

  if ( index >= container->size() ) {
    throw SWIGEXCEPTION( "out_of_range" );
  }

  ILwd::Format format = char2Format( f );
  ILwd::Compression compression = char2Compression( c );
    
  ostringstream ss;
  (*container)[ index ]->write( ss, format, compression );
   
  return ss.str();
}


unsigned int getContainerSize( ILwd::LdasContainer* container )
{
    if ( !Registry::isElementValid( container ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    return container->size();
}


std::string getElementAttribute( LdasElement* element, const char* att ) {
  if ( !Registry::isElementValid( element ) ) {
    throw SWIGEXCEPTION( invalid_element( element ) );
  }

  switch( element->getElementId() ) {
    case ILwd::ID_CHAR:
    case ILwd::ID_CHAR_U:
    case ILwd::ID_INT_2S:
    case ILwd::ID_INT_2U:
    case ILwd::ID_INT_4S:
    case ILwd::ID_INT_4U:
    case ILwd::ID_INT_8S:
    case ILwd::ID_INT_8U:
    case ILwd::ID_REAL_4:
    case ILwd::ID_REAL_8:
    case ILwd::ID_COMPLEX_8:
    case ILwd::ID_COMPLEX_16:   
      return getLdasArrayBaseAttribute(
          dynamic_cast< LdasArrayBase* >( element ), att );
      break;

    case ILwd::ID_ILWD:
      return getLdasContainerAttribute(
          dynamic_cast< LdasContainer* >( element ), att );
      break;

    case ILwd::ID_LSTRING:
      return getLdasStringAttribute(
          dynamic_cast< LdasString* >( element ), att );
      break;

    case ILwd::ID_EXTERNAL:
      return getLdasExternalAttribute(
          dynamic_cast< LdasExternal* >( element ), att );
      break;
       
    default:
      throw SWIGEXCEPTION( "unknown_element" );
      break;
  }
}


void setElementAttribute( LdasElement* element, const std::string att,
			  const std::string value )
{
  if ( !Registry::isElementValid( element ) )
  {
    throw SWIGEXCEPTION( invalid_element( element ) );
  }
  // Determine the attributes trying to be set
  if ( att == LdasElement::ATTR_STR_JOBID )
  {
    element->setJobId(atoi(value.c_str()));
  }
  else if ( att == LdasElement::ATTR_STR_NAME )
  {
    element->setNameString(value);
  }
  else if ( att == LdasElement::ATTR_STR_COMMENT )
  {
    element->setComment(value);
  }
  else
  {
    throw SWIGEXCEPTION( "invalid_attribute" );
  }
}


//-----------------------------------------------------------------------------
//
//: Clear all metadata
//
//!usage: clearElementMetadata ptElem
//
void clearElementMetadata( LdasElement* element )
{
  if ( !Registry::isElementValid(element) )
  {
    throw SWIGEXCEPTION( invalid_element( element ) );
  }

  element->clearMetadata( );
}

//-----------------------------------------------------------------------------
//
//: Get piece of metadata from the element
//
//!usage: set md_value [ getElementMetadata ptElem "md_name" ]
//
//!param: ptElem - a pointer to an element.
//
//!return: md_value - Value associate with metadata
//
//!exc: Excpetion thrown if no metadata found for Name
std::string getElementMetadata( ILwd::LdasElement* element, std::string Name )
{
  if ( !Registry::isElementValid(element) )
  {
    throw SWIGEXCEPTION( invalid_element( element ) );
  }

  return element->getMetadata<std::string>( Name );
}

//-----------------------------------------------------------------------------
//
//: Set metadata for element
//
//!usage: setElementMetadata ptElem "md_name" "md_value"
//
//!param: ptElem - a pointer to an element.
//
//!param: md_name - Name of the metadata
//
//!param: md_value - Value associate with metadata
//
void setElementMetadata( LdasElement* element,
			 std::string Name,
			 std::string Value )
{
  if ( !Registry::isElementValid(element) )
  {
    throw SWIGEXCEPTION( invalid_element( element ) );
  }

  element->setMetadata( Name, Value );
}



//-----------------------------------------------------------------------------
//
//: Unsets piece of metadata from the element
//
//!usage: unsetElementMetadata ptElem "md_name"
//
//!param: ptElem - a pointer to an element.
//
void unsetElementMetadata( ILwd::LdasElement* element, std::string Name )
{
  if ( !Registry::isElementValid(element) )
  {
    throw SWIGEXCEPTION( invalid_element( element ) );
  }

  element->unsetMetadata( Name );
}

std::string getContainerTOC( LdasContainer* container )
{
    if ( !Registry::isElementValid( container ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    ostringstream ss;
    ss << "{ ";

    for ( LdasContainer::const_iterator iter = container->begin();
          iter != container->end(); ++iter )
    {
        ss << "\"" << (*iter)->getNameString() << "\" " << (*iter)->getIdentifier()
           << " ";
    }

    ss << "}";

    return ss.str();
}


std::string copyElement( LdasElement* element )
{
    if ( !Registry::isElementValid( element ) )
    {
        throw SWIGEXCEPTION( invalid_element( element ) );
    }

    LdasElement* e = element->createCopy();
    Registry::elementRegistry.registerObject( e );

    return createElementPointer( e );
}


void mergeContainer( LdasContainer* c1, const LdasContainer* c2 )
{
    if ( !Registry::isElementValid( c1 ) || !Registry::isElementValid( c2 ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    int s = c2->size();
    for ( int i = 0; i < s; ++i )
    {
        c1->push_back( *( (*c2)[ i ] ) );
    }
}


void deleteContainerElement( LdasContainer* container, unsigned int index )
{
    if ( !Registry::isElementValid( container ) )
    {
        throw SWIGEXCEPTION( "invalid_container" );
    }

    if ( index >= container->size() )
    {
        throw SWIGEXCEPTION( "out_of_range" );
    }

    container->erase( container->begin() + index );
}


void writeElement( ILwdFile* file, ILwd::LdasElement* e, std::string f, std::string c )
try   
{
    if ( !isILwdFileValid( file ) )
    {
        throw SWIGEXCEPTION( "invalid_file" );
    }

    if ( !Registry::isElementValid( e ) )
    {
        throw SWIGEXCEPTION( "invalid_element" );
    }

    OutputILwdFile* out = dynamic_cast< OutputILwdFile* >( file );
    if (out == 0)
    {
        throw SWIGEXCEPTION( "invalid_mode: file is not open for output." );
    }

    ILwd::Format format = char2Format( f.c_str() );
    ILwd::Compression compression = char2Compression( c.c_str() );

    if ( e->getElementId() == ILwd::ID_ILWD )
    {
        dynamic_cast< LdasContainer* >( e )->write(
            0, 4, out->getofstream(), format, compression );
// Replacing line above with line below removes indentation
//                out->getofstream(), format, compression );
    }
    else
    {
        e->write( out->getofstream(), format, compression );
    }
}
catch( SwigException& )
{
   throw;
}   
catch( const LdasException& exception )
{
   throw SwigException( exception );
}
catch( ... )
{
   throw SwigException( "unexpected_exception" );
}
   
    
namespace {
    
    // :KLUDGE: See PR#868
    //
    // The writeElement function writes an LDAS element to a file.
    // Under Linux there is a problem which can freeze the machine
    // if writeElement is used in multiple threads.
    // As an intermediate fix, two versions of writeElement have been
    // provided, writeElement and writeElementLocked, which simply locks
    // a mutex and then calls writeElement. This function is used in
    // place of writeElement by writeElement_t. It should never be called
    // directly.

    //
    // This mutex is locked at the beginning of the threaded version
    // of writeElement ie. writeElement_t. This allows othe
    pthread_mutex_t writeElement_Mutex = PTHREAD_MUTEX_INITIALIZER;

  // PR#1023 is fixed here using the redCountUp() call

    // The locked version of writeElement
    void writeElementLocked( ILwdFile* file,
			     ILwd::LdasElement* e,
			     std::string f,
			     std::string c )
    {
	const MutexLock lock(writeElement_Mutex);
        try {
	   writeElement( file, e, f, c );
  	   if ( Registry::isElementValid( e ) ) {
             e->refCountDown();
 	   }
        } catch (...) {
  	   if ( Registry::isElementValid( e ) ) {
    	     e->refCountDown();
	   }
	   throw;
        }
    }
}

std::string readElement( ILwdFile* file )
try {

  LdasElement* e = readElementCore( file );

  Registry::elementRegistry.registerObject( e );

  return createElementPointer( e );

} catch( LdasException& e ) {
//    ADDMESSAGE( e, "bad_element" );
    throw SwigException( e );
}

namespace {
    
    // :KLUDGE: See PR#477
    //
    // The readElement function reads an LDAS element from a file.
    // Under Linux there is a problem which can freeze the machine
    // if readElement is used in multiple threads.
    // As an intermediate fix, two versions of readElement have been
    // provided, readElement and readElementLocked, which simply locks
    // a mutex and then calls readElement. This function is used in
    // place of readElement by readElement_t. It should never be called
    // directly.

    //
    // This mutex is locked at the beginning of the threaded version
    // of readElement ie. readElement_t. This allows othe
    pthread_mutex_t readElement_Mutex = PTHREAD_MUTEX_INITIALIZER;

    // The locked version of readElement
    std::string readElementLocked( ILwdFile* file )
    {
	const MutexLock lock(readElement_Mutex);
	return readElement(file);
    }
}

LdasElement* readElementCore( ILwdFile* file )
try {

  if ( !isILwdFileValid( file ) ) {
      throw SWIGEXCEPTION( "invalid_file" );
  }

  InputILwdFile* in = dynamic_cast< InputILwdFile* >( file );
  if (in == 0)
  {
      throw SWIGEXCEPTION( "invalid_mode: file is not open for input." );
  }

  ILwd::Reader r( in->getifstream() );
  r.skipWhiteSpace();
    
  LdasElement* e(
      LdasElement::createElement( r ) );

  return e;

} catch( LdasException& e ) {
     
//    ADDMESSAGE( e, "bad_element" );
    throw SwigException( e );

}



void setWriteFormat( ILwd::LdasElement* e, const char* f, const char* c )
{
    if ( !Registry::isElementValid( e ) )
    {
        throw SWIGEXCEPTION( "invalid_element" );
    }

    if ( e->getElementId() == ILwd::ID_ILWD )
    {
        ILwd::LdasContainer& lc( dynamic_cast< LdasContainer& >( *e ) );

        if ( strlen( f ) != 0 )
        {
            lc.setWriteFormat( char2Format( f ) );
        }

        if ( strlen( c ) != 0 )
        {
            lc.setWriteCompression( char2Compression( c ) );
        }
    }
    else
    {
        ILwd::LdasFormatElement& lf(
            dynamic_cast< ILwd::LdasFormatElement& >( *e ) );
        
        if ( strlen( f ) != 0 )
        {
            lf.setWriteFormat( char2Format( f ) );
        }

        if ( strlen( c ) != 0 )
        {
            lf.setWriteCompression( char2Compression( c ) );
        }
    }
}

    
std::string getWriteFormat( ILwd::LdasElement* e )
{
    std::string res;
    
    if ( !Registry::isElementValid( e ) )
    {
        throw SWIGEXCEPTION( "invalid_element" );
    }

    if ( e->getElementId() == ILwd::ID_ILWD )
    {
        ILwd::LdasContainer& lc( dynamic_cast< LdasContainer& >( *e ) );
        res = format2String( lc.getWriteFormat() ) + " " +
            compression2String( lc.getWriteCompression() );
    }
    else
    {
        ILwd::LdasFormatElement& lf(
            dynamic_cast< ILwd::LdasFormatElement& >( *e ) );
        res = format2String( lf.getWriteFormat() ) + " " +
            compression2String( lf.getWriteCompression() );
    }

    return res;
}

void setJobId( ILwd::LdasElement* e, const unsigned int JobId)
{
    if ( !Registry::isElementValid( e ) )
    {
        throw SWIGEXCEPTION( invalid_element( e ) );
    }

    e->setJobId(JobId);
}
    
bool isNullInElement( ILwd::LdasElement* Element, size_t Offset )
  try
{
    // make sure the element pointer is valid
    if ( !Registry::isElementValid( Element ) )
    {
        throw SWIGEXCEPTION( "invalid_element" );
    }
    return Element->isNull(Offset);
}
catch( SwigException& e )
{
    throw;
}
catch( ... )
{
    throw SWIGEXCEPTION( "unexpected_exception" );
}

void setNullInElement( ILwd::LdasElement* Element, size_t Offset, bool Value )
  try
{
    // make sure the element pointer is valid
    if ( !Registry::isElementValid( Element ) )
    {
        throw SWIGEXCEPTION( "invalid_element" );
    }
    Element->setNull(Offset, Value);
}
catch( SwigException& e )
{
    throw;
}
catch( ... )
{
    throw SWIGEXCEPTION( "unexpected_exception" );
}

// :KLUDGE: See PR#477
//
// Replacement for
// CREATE_THREADED1( readElement, std::string, ILwdFile* );
//
// These functions have been broken out from the macro because we need
// to use the function readElementLocked instead of the default
// which would be readElement.

tid* readElement_t( ILwdFile* p1, Tcl_Interp* interp, const char* flag )
{
    tid* t = new tid1< std::string, ILwdFile* >( &readElementLocked, "readElement", p1 );
    Registry::threadRegistry.registerObject( t );
    t->setAlert( interp, flag );
    t->Spawn();
    return t;
}

std::string readElement_r( tid* t )
{
    if ( !Registry::threadRegistry.isRegistered( t ) )
    {
        throw SWIGEXCEPTION( "invalid_tid" );
    }
    if ( t->getFunction() != "readElement" )
    {
        throw SWIGEXCEPTION( "invalid_tid" );
    }
    std::string tmp(reinterpret_cast< tid1<std::string,ILwdFile*>* >( t )->getReturn());
    Registry::threadRegistry.destructObject( t );
    return tmp;
}

//CREATE_THREADED2V( sendElementObject, os_tcp_socket*, ILwd::LdasElement* );
tid*   sendElementObject_t(   os_tcp_socket*  p1,   ILwd::LdasElement*   p2, Tcl_Interp* interp, const char* flag ) {
  tid* t = new tidv2<   os_tcp_socket* ,   ILwd::LdasElement*   >( &  sendElementObject_unlock , "sendElementObject", p1, p2 );
  Registry::threadRegistry.registerObject( t );
  t->setAlert( interp, flag );
  if ( t->Spawn() != 0) {
    Registry::threadRegistry.destructObject( t );
    throw SwigException(   "pthread_create_failed"  , "../../../../../api/genericAPI/so/src/elementcmd.cc", 1141 );
  }
  if ( Registry::isElementValid( p2 ) ) {
    p2->refCountUp();
  }
  return t;
}

void   sendElementObject_r( tid* t ) {
  if ( !Registry::threadRegistry.isRegistered( t ) )
    throw SwigException(   "invalid_tid"  , "../../../../../api/genericAPI/so/src/elementcmd.cc", 1141 );
  if ( t->getFunction() != "sendElementObject" )
    throw SwigException(   "invalid_tid"  , "../../../../../api/genericAPI/so/src/elementcmd.cc", 1141 );
  try {
    reinterpret_cast< tidv2<   os_tcp_socket* ,   ILwd::LdasElement*   >* >( t )->getReturn(); 
    Registry::threadRegistry.destructObject( t );
    return;
  } catch( ... ) {
    Registry::threadRegistry.destructObject( t );
    throw;
  }
}

CREATE_THREADED1( recvElementObject, std::string, os_tcp_socket* )

//CREATE_THREADED2V( sendElementAscii, os_tcp_socket*, ILwd::LdasElement* )

tid*   sendElementAscii_t(   os_tcp_socket*  p1,   ILwd::LdasElement*   p2, Tcl_Interp* interp, const char* flag ) {
  tid* t = new tidv2<   os_tcp_socket* ,   ILwd::LdasElement*   >( &  sendElementAscii_unlock , "sendElementAscii", p1, p2 );
  Registry::threadRegistry.registerObject( t );
  t->setAlert( interp, flag );
  if ( t->Spawn() != 0) {
    Registry::threadRegistry.destructObject( t );
    throw SwigException(   "pthread_create_failed"  , "../../../../../api/genericAPI/so/src/elementcmd.cc", 1143 ) ;
  }
  if ( Registry::isElementValid( p2 ) ) {
    p2->refCountUp();
  }

  return t;
}

void   sendElementAscii_r( tid* t ) {
  if ( !Registry::threadRegistry.isRegistered( t ) )
    throw SwigException(   "invalid_tid"  , "../../../../../api/genericAPI/so/src/elementcmd.cc", 1143 ) ;
  if ( t->getFunction() != "sendElementAscii" )
    throw SwigException(   "invalid_tid"  , "../../../../../api/genericAPI/so/src/elementcmd.cc", 1143 ) ;
  try {
    reinterpret_cast< tidv2<   os_tcp_socket* ,   ILwd::LdasElement*   >* >( t )->getReturn();
    Registry::threadRegistry.destructObject( t );
    return;
  } catch( ... ) {
    Registry::threadRegistry.destructObject( t );
    throw;
  }
}

CREATE_THREADED1( recvElementAscii, std::string, os_tcp_socket* )

// CREATE_THREADED4V( writeElement, ILwdFile*, ILwd::LdasElement*, const char*, const char* )

tid* writeElement_t( ILwdFile* p1, ILwd::LdasElement* p2, const char*  p3, const char*   p4,
		     Tcl_Interp* interp, const char* flag )
{
  tid* t = new tidv4<ILwdFile*, ILwd::LdasElement*, std::string, std::string >( &writeElementLocked , "writeElement", p1, p2, std::string(p3), std::string(p4));
  Registry::threadRegistry.registerObject( t );
  t->setAlert( interp, flag );
  if ( t->Spawn() != 0 ) {
    Registry::threadRegistry.destructObject( t );
    throw SwigException("pthread_create_failed", __FILE__, __LINE__ );
  }
  if ( Registry::isElementValid( p2 ) ) {
    p2->refCountUp();
  }
  return t;
}

void writeElement_r( tid* t )
{
  if ( !Registry::threadRegistry.isRegistered( t ) )
    throw SwigException( "invalid_tid", __FILE__, __LINE__ );
  if ( t->getFunction() != "writeElement" )
    throw SwigException( "invalid_tid", __FILE__, __LINE__ );
  reinterpret_cast< tidv4< ILwdFile*, ILwd::LdasElement*, std::string ,std::string >* >( t )->getReturn();
  Registry::threadRegistry.destructObject( t );
  return;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */
