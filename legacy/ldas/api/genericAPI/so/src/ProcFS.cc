#include "genericAPI/config.h"

#include <fstream>
#include <sstream>
#include <stdexcept>

#include "genericAPI/ProcFS.hh"

using namespace GenericAPI;

ProcFS::
ProcFS( pid_t ProcId )
{
  std::ostringstream	oss;
  oss << "/proc/" << ProcId << "/psinfo";
  
  std::ifstream	psinfo_stream( oss.str( ).c_str( ) );

  if ( psinfo_stream.bad( ) )
  {
    std::ostringstream	msg;
    msg << "Unable to open psinfo for process: " << ProcId
	<< " (" << oss.str( ).c_str( ) << ")";
    throw std::runtime_error( msg.str( ) );
  }

#if HAVE_SYS_PROCFS
  psinfo_t	psinfo;
  if ( psinfo_stream.read( &psinfo, sizeof( psinfo_t ) ) != sizeof( psinfo ) )
  {
    std::ostringstream	msg;
    msg << "Unable to read psinfo for process: " << ProcId << std::endl;
    throw std::runtime_error( msg.str( ) );
  }
#endif /* HAVE_SYS_PROCFS */
  psinfo_stream.close( );
}
