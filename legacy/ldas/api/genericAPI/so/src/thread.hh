#ifndef ThreadHH
#define ThreadHH

#include "tid.hh"
#include "threaddecl.hh"

#if HAVE_TCL
#ifndef FOR_TCL
#define FOR_TCL 1
#endif
#endif /* HAVE_TCL */

#define LITERAL1( P1 ) P1
#define LITERAL2( P1, P2 ) LITERAL1( P1 ), P2
#define LITERAL3( P1, P2, P3 ) LITERAL2( P1, P2), P3
#define LITERAL4( P1, P2, P3, P4 ) LITERAL3( P1, P2, P3 ), P4
#define LITERAL5( P1, P2, P3, P4, P5 ) LITERAL4( P1, P2, P3, P4 ), P5
#define LITERAL6( P1, P2, P3, P4, P5, P6 ) LITERAL5( P1, P2, P3, P4, P5 ), P6
#define LITERAL7( P1, P2, P3, P4, P5, P6, P7 ) LITERAL6( P1, P2, P3, P4, P5, P6 ), P7
#define LITERAL8( P1, P2, P3, P4, P5, P6, P7, P8 ) LITERAL7( P1, P2, P3, P4, P5, P6, P7), P8
#define LITERAL9( P1, P2, P3, P4, P5, P6, P7, P8, P9 ) LITERAL8( P1, P2, P3, P4, P5, P6, P7, P8), P9
#define LITERAL10( P1, P2, P3, P4, P5, P6, P7, P8, P9, P10 ) \
  LITERAL9( P1, P2, P3, P4, P5, P6, P7, P8, P9), P10

#if FOR_TCL
#define CREATE_THREADED_N( F, R, N, DECL, PARMS, VARS )    \
tid* F##_t( DECL Tcl_Interp* interp, const char* flag )       		\
{                                                                       \
  tid* t = new tid##N< R, PARMS >( &F, #F, VARS );			\
  Registry::threadRegistry.registerObject( t );			\
  t->setAlert( interp, flag );                            	\
  if ( t->Spawn() != 0)                                     	\
  {                                                       	\
    Registry::threadRegistry.destructObject( t );        		\
    throw SwigException("pthread_create_failed",__FILE__,__LINE__);	\
  }								\
  return t;								\
}                                                                       \
R F##_r( tid* t )                                                       \
{                                                                       \
  if ( !Registry::threadRegistry.isRegistered( t ) )              \
    throw SwigException( "invalid_tid", __FILE__, __LINE__);		\
  if ( t->getFunction() != #F )                                   \
    throw SwigException( "invalid_tid", __FILE__, __LINE__ );		\
  try 									\
  {									\
    t->unsetAlert( );                                             \
    R tmp( reinterpret_cast< tid##N< R, PARMS >* >( t )->getReturn() );	\
    Registry::threadRegistry.destructObject( t );			\
    return tmp;							\
  }								\
  catch( ... ) {							\
    Registry::threadRegistry.destructObject( t );			\
    throw; /* rethrow the exception */				\
  }									\
}
#else /* HAVE_<LANGUAGE> - unsupported language */
#define CREATE_THREADED_N( F, R, N, DECL, PARMS, VARS )
#endif /* HAVE_<LANGUAGE> */

#if FOR_TCL
#define CREATE_THREADEDV_N( F, N, DECL, PARMS, VARS )    \
tid* F##_t( DECL Tcl_Interp* interp, const char* flag )       		\
{                                                                       \
     tid* t = new tidv##N< PARMS >					\
                 ( &F, #F, VARS );				        \
     Registry::threadRegistry.registerObject( t );                      \
     t->setAlert( interp, flag );                                       \
     if ( t->Spawn() != 0)                                              \
       {                                                                \
          Registry::threadRegistry.destructObject( t );                 \
          throw SWIGEXCEPTION( "pthread_create_failed" );               \
       }                                                                \
     return t;                                                          \
}                                                                       \
void F##_r( tid* t )                                                    \
{                                                                       \
    if ( !Registry::threadRegistry.isRegistered( t ) )                  \
        throw SWIGEXCEPTION( "invalid_tid" );                           \
    if ( t->getFunction() != #F )                                       \
        throw SWIGEXCEPTION( "invalid_tid" );                           \
    try {								\
      t->unsetAlert( );                                                 \
      reinterpret_cast< tidv##N< PARMS >* >( t )->getReturn( );		\
      Registry::threadRegistry.destructObject( t );			\
      return;								\
    }									\
    catch( ... ) {							\
      Registry::threadRegistry.destructObject( t );			\
      throw; /* rethrow the exception */				\
    }									\
}
#else /* HAVE_<LANGUAGE> - unsupported language */
#define CREATE_THREADEDV_N( F, N, DECL, PARMS, VARS )
#endif /* HAVE_<LANGUAGE> */

#if FOR_TCL
#define CREATE_THREADED0( F, R )                                              \
tid* F##_t( Tcl_Interp* interp, const char* flag )                            \
{                                                                             \
     tid* t = new tid0< R >( &F, #F );                                        \
     Registry::threadRegistry.registerObject( t );                            \
     t->setAlert( interp, flag );                                             \
     if ( t->Spawn() != 0)                                                    \
       {                                                                      \
          Registry::threadRegistry.destructObject( t );                       \
          throw SWIGEXCEPTION( "pthread_create_failed" );                     \
       }                                                                      \
     return t;                                                                \
}                                                                             \
R F##_r( tid* t )                                                             \
{                                                                             \
    if ( !Registry::threadRegistry.isRegistered( t ) )                        \
        throw SWIGEXCEPTION( "invalid_tid" );                                 \
    if ( t->getFunction() != #F )                                             \
       throw SWIGEXCEPTION( "invalid_tid" );                                 \
    try {								      \
        t->unsetAlert( );                                                     \
        R tmp( reinterpret_cast< tid0< R >* >( t )->getReturn() );            \
        Registry::threadRegistry.destructObject( t );                         \
        return tmp;                                                           \
    }									      \
    catch( ... ) {							      \
      Registry::threadRegistry.destructObject( t );			      \
      throw; /* rethrow the exception */				      \
    }									      \
}
#else /* HAVE_<LANGUAGE> - unsupported language */
#define CREATE_THREADED0( F, R )
#endif /* HAVE_<LANGUAGE> */

#if FOR_TCL
#define CREATE_THREADED0V( F )                                                \
tid* F##_t( Tcl_Interp* interp, const char* flag )                            \
{                                                                             \
     tid* t = new tidv0( &F, #F );                                            \
     Registry::threadRegistry.registerObject( t );                            \
     t->setAlert( interp, flag );                                             \
     if ( t->Spawn() != 0)                                                    \
       {                                                                      \
          Registry::threadRegistry.destructObject( t );                       \
          throw SWIGEXCEPTION( "pthread_create_failed" );                     \
       }                                                                      \
     return t;                                                                \
}                                                                             \
void F##_r( tid* t )                                                          \
{                                                                             \
    if ( !Registry::threadRegistry.isRegistered( t ) )                        \
        throw SWIGEXCEPTION( "invalid_tid" );                                 \
    if ( t->getFunction() != #F )                                             \
        throw SWIGEXCEPTION( "invalid_tid" );                                 \
    try {								      \
        t->unsetAlert( );                                                     \
        reinterpret_cast< tidv0* >( t )->getReturn();                         \
        Registry::threadRegistry.destructObject( t );                         \
        return;                                                               \
    }									      \
    catch( ... ) {							      \
      Registry::threadRegistry.destructObject( t );			      \
      throw; /* rethrow the exception */				      \
    }									      \
}
#else /* HAVE_<LANGUAGE> - unsupported language */
#define CREATE_THREADED0V( F )
#endif /* HAVE_<LANGUAGE> */

#define CREATE_THREADED1( F, R, P1 )    				\
CREATE_THREADED_N( F, R, 1, 						\
                   THRDDECL1(P1), 					\
                   LITERAL1(P1),					\
                   LITERAL1(p1) )

#define CREATE_THREADED1V( F, P1 )    					\
CREATE_THREADEDV_N( F, 1, 						\
                   THRDDECL1(P1), 					\
                   LITERAL1(P1),					\
                   LITERAL1(p1) )

#define CREATE_THREADED2( F, R, P1, P2 )		    		\
CREATE_THREADED_N( F, R, 2, 						\
                   THRDDECL2( P1, P2 ),					\
                   LITERAL2(P1, P2),					\
                   LITERAL2( p1, p2 ) )

#define CREATE_THREADED2V( F, P1, P2 )	    				\
CREATE_THREADEDV_N( F, 2, 						\
                   THRDDECL2( P1, P2 ), 				\
                   LITERAL2(P1, P2),					\
                   LITERAL2( p1, p2 ) )

#define CREATE_THREADED3( F, R, P1, P2, P3 )		    		\
CREATE_THREADED_N( F, R, 3, 						\
                   THRDDECL3( P1, P2, P3 ),				\
                   LITERAL3(P1, P2, P3),				\
                   LITERAL3( p1, p2, p3 ) )

#define CREATE_THREADED3V( F, P1, P2, P3 )	    			\
CREATE_THREADEDV_N( F, 3, 						\
                   THRDDECL3( P1, P2, P3 ), 				\
                   LITERAL3(P1, P2, P3),				\
                   LITERAL3( p1, p2, p3 ) )

#define CREATE_THREADED4( F, R, P1, P2, P3, P4 )	    		\
CREATE_THREADED_N( F, R, 4, 						\
                   THRDDECL4( P1, P2, P3, P4 ),				\
                   LITERAL4(P1, P2, P3, P4),				\
                   LITERAL4( p1, p2, p3, p4 ) )

#define CREATE_THREADED4V( F, P1, P2, P3, P4 )	    			\
CREATE_THREADEDV_N( F, 4, 						\
                   THRDDECL4( P1, P2, P3, P4 ), 			\
                   LITERAL4(P1, P2, P3, P4),				\
                   LITERAL4( p1, p2, p3, p4 ) )

#define CREATE_THREADED5( F, R, P1, P2, P3, P4, P5 )    		\
CREATE_THREADED_N( F, R, 5, 						\
                   THRDDECL5( P1, P2, P3, P4, P5 ),			\
                   LITERAL5(P1, P2, P3, P4, P5),			\
                   LITERAL5( p1, p2, p3, p4, p5 ) )

#define CREATE_THREADED5V( F, P1, P2, P3, P4, P5 )	    		\
CREATE_THREADEDV_N( F, 5, 						\
                   THRDDECL5( P1, P2, P3, P4, P5 ), 			\
                   LITERAL5(P1, P2, P3, P4, P5),			\
                   LITERAL5( p1, p2, p3, p4, p5 ) )

#define CREATE_THREADED6( F, R, P1, P2, P3, P4, P5, P6 )    		\
CREATE_THREADED_N( F, R, 6, 						\
                   THRDDECL6( P1, P2, P3, P4, P5, P6 ),			\
                   LITERAL6(P1, P2, P3, P4, P5, P6),			\
                   LITERAL6( p1, p2, p3, p4, p5, p6 ) )

#define CREATE_THREADED6V( F, P1, P2, P3, P4, P5, P6 )    		\
CREATE_THREADEDV_N( F, 6, 						\
                   THRDDECL6( P1, P2, P3, P4, P5, P6 ), 		\
                   LITERAL6(P1, P2, P3, P4, P5, P6),			\
                   LITERAL6( p1, p2, p3, p4, p5, p6 ) )

#define CREATE_THREADED7( F, R, P1, P2, P3, P4, P5, P6, P7 )    	\
CREATE_THREADED_N( F, R, 7, 						\
                   THRDDECL7( P1, P2, P3, P4, P5, P6, P7 ),		\
                   LITERAL7(P1, P2, P3, P4, P5, P6, P7),		\
                   LITERAL7( p1, p2, p3, p4, p5, p6, p7 ) )

#define CREATE_THREADED7V( F, P1, P2, P3, P4, P5, P6, P7 )    		\
CREATE_THREADEDV_N( F, 7, 						\
                   THRDDECL7( P1, P2, P3, P4, P5, P6, P7 ), 		\
                   LITERAL7(P1, P2, P3, P4, P5, P6, P7),		\
                   LITERAL7( p1, p2, p3, p4, p5, p6, p7 ) )

#define CREATE_THREADED8( F, R, P1, P2, P3, P4, P5, P6, P7, P8 )    	\
CREATE_THREADED_N( F, R, 8, 						\
                   THRDDECL8( P1, P2, P3, P4, P5, P6, P7, P8 ),		\
                   LITERAL8(P1, P2, P3, P4, P5, P6, P7, P8),		\
                   LITERAL8( p1, p2, p3, p4, p5, p6, p7, p8 ) )

#define CREATE_THREADED8V( F, P1, P2, P3, P4, P5, P6, P7, P8 )    	\
CREATE_THREADEDV_N( F, 8, 						\
                   THRDDECL8( P1, P2, P3, P4, P5, P6, P7, P8 ), 	\
                   LITERAL8(P1, P2, P3, P4, P5, P6, P7, P8),		\
                   LITERAL8( p1, p2, p3, p4, p5, p6, p7, p8 ) )

#define CREATE_THREADED9( F, R, P1, P2, P3, P4, P5, P6, P7, P8, P9 )    \
CREATE_THREADED_N( F, R, 9, 						\
                   THRDDECL9( P1, P2, P3, P4, P5, P6, P7, P8, P9 ),	\
                   LITERAL9(P1, P2, P3, P4, P5, P6, P7, P8, P9),	\
                   LITERAL9( p1, p2, p3, p4, p5, p6, p7, p8, p9 ) )

#define CREATE_THREADED9V( F, P1, P2, P3, P4, P5, P6, P7, P8, P9 )    	\
CREATE_THREADEDV_N( F, 9, 						\
                   THRDDECL9( P1, P2, P3, P4, P5, P6, P7, P8, P9 ), 	\
                   LITERAL9(P1, P2, P3, P4, P5, P6, P7, P8, P9),	\
                   LITERAL9( p1, p2, p3, p4, p5, p6, p7, p8, p9 ) )

#define CREATE_THREADED10( F, R, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10 )    \
CREATE_THREADED_N( F, R, 10, 						\
                   THRDDECL10( P1, P2, P3, P4, P5, P6, P7, P8, P9, P10 ), \
                   LITERAL10(P1, P2, P3, P4, P5, P6, P7, P8, P9, P10),	\
                   LITERAL10( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ) )

#define CREATE_THREADED10V( F, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10 )    \
CREATE_THREADEDV_N( F, 10, 						\
                   THRDDECL10( P1, P2, P3, P4, P5, P6, P7, P8, P9, P10 ), \
                   LITERAL10(P1, P2, P3, P4, P5, P6, P7, P8, P9, P10),	\
                   LITERAL10( p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ) )
#endif // ThreadHH
