#include "genericAPI/config.h"

#include <cstdlib>
#include <cstring>

#include "TclChannel.hh"

using namespace GenericAPI;

TclChannel::
TclChannel( const std::string& Name )
{
  m_channel_type.typeName = strdup( const_cast<char*>( Name.c_str() ) );
  m_channel_type.version = TCL_CHANNEL_VERSION_2;

  SetCloseProc( (Tcl_DriverCloseProc*) NULL );
  SetInputProc( (Tcl_DriverInputProc*) NULL );
  SetOutputProc( (Tcl_DriverOutputProc*) NULL );
  SetSeekProc( (Tcl_DriverSeekProc*) NULL );
  SetSetOptionProc( (Tcl_DriverSetOptionProc*) NULL );
  SetGetOptionProc( (Tcl_DriverGetOptionProc*) NULL );
  SetWatchProc( (Tcl_DriverWatchProc*) NULL );
  SetGetHandleProc( (Tcl_DriverGetHandleProc*) NULL );
  SetClose2Proc( (Tcl_DriverClose2Proc*) NULL );
  SetBlockModeProc( (Tcl_DriverBlockModeProc*) NULL );
  SetFlushProc( (Tcl_DriverFlushProc*) NULL );
  SetHandlerProc( (Tcl_DriverHandlerProc*) NULL );
}

   
TclChannel::~TclChannel()
{
   if( m_channel_type.typeName )
   {
      free( m_channel_type.typeName );
      m_channel_type.typeName = 0;
   }
}
