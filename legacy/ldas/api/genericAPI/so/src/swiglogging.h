/* -*- mode: c++; -*- */

#include <string>
#include <iostream>

namespace GenericAPI
{

  typedef enum {
    MSG_DEBUG,
    MSG_WARN,
    MSG_ERR
  } MessageType_type;

  void AddLogMessage(std::string Message, MessageType_type Type, std::ostream& Out = std::cerr );
}
