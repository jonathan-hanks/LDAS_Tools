#ifndef GENERICAPI__OSPACE_CHANNEL_HH
#define GENERICAPI__OSPACE_CHANNEL_HH

#include "genericAPI/config.h"

#include <memory>
#include <string>
#include <tcl.h>

#include "TclChannel.hh"

#ifndef CONST84
#define CONST84
#endif /* CONST84 */

#if HAVE_LDAS_PACKAGE_ILWD
namespace ILwd
{
  class LdasElement;
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

class os_tcp_socket;

namespace GenericAPI
{
  class OSpaceChannel
  {
  public:
    struct private_type;

    static const char*		CHANNEL_TYPE;

    OSpaceChannel( Tcl_Interp* Interp, int ObjC, Tcl_Obj* CONST ObjV[] );

    OSpaceChannel( const OSpaceChannel& ServerChannel,
		   os_tcp_socket* Socket );

    ~OSpaceChannel( );

    int BlockMode( int Mode );

    int Close( Tcl_Interp* Interp );

#if HAVE_LDAS_PACKAGE_ILWD
    static std::string ElementGet( Tcl_Channel Channel );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
    static std::string ElementGet( os_tcp_socket* Socket );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
    static void ElementPut( Tcl_Channel Channel, ILwd::LdasElement* Element );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
    static void ElementPutDLP( Tcl_Channel Channel,
			       ILwd::LdasElement* Element );
#endif /* HAVE_LDAS_PACKAGE_ILWD */

    int GetHandle( int Direction, ClientData* Handle );

    int GetOption(  Tcl_Interp* Interp, CONST84 char* OptionName,
		    Tcl_DString* Result );

    
    static os_tcp_socket* GetOSpaceSocket( Tcl_Channel Channel );

    int Input( char* Buffer, int ToRead, int* Error );

    int SetOption( Tcl_Interp* Interp,
		   CONST84 char* OptionName,
		   CONST84 char* Value );

    int Output( CONST84 char* Buffer, int ToWrite, int* Error );

    void Watch( int Mask );

  private:
    std::auto_ptr<private_type>	m_private;
  };
} // namespace GenericAPI
#endif /* GENERICAPI__OSPACE_CHANNEL_HH */

