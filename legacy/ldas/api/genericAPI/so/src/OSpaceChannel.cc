#include "genericAPI/config.h"

#include <cerrno>
#include <tcl.h>
#include <unistd.h>

#include <memory>
#include <sstream>
#include <string> 
   
#include "general/undef_ac.h"
#include "ospace/rtti/classdb.h"

#include "general/AtExit.hh"

#include "elementcmd.hh"
#include "registry.hh"
#include "socketcmd.hh"
#include "OSpaceChannel.hh"

using std::string;

#ifndef CONST84
#define CONST84
#endif /* CONST84 */
   
   
using namespace GenericAPI;

extern "C" {
  int TclSockGetPort( Tcl_Interp* Interp, CONST84 char* Str,
		      CONST84 char* Proto, int* PortPtr );
}

inline void NoArgumentCheck( Tcl_Interp* Interp,
			     bool Condition,
			     std::string Option )
{
  if ( Condition )
  {
    std::ostringstream	oss;
    oss << "no argument given for " << Option << " option";
   
    const string error_string( oss.str() );
    Tcl_AppendResult( Interp, error_string.c_str( ), (char*)NULL );
    throw std::runtime_error( error_string.c_str( ) );
  }
}

extern "C" {
  void delete_proc( ClientData Data, Tcl_Interp* Interp );

  void os_accept( ClientData Data, int /* Mask */ );
  
  void server_close_proc( ClientData Data );
} /* extern "C" */

namespace
{
  inline std::string gen_socket_name( )
  {
    static INT_4U	index = 0;

    std::ostringstream	oss;
    oss << OSpaceChannel::CHANNEL_TYPE << index++;
    return oss.str( );
  }

  void
  tcl_exception_handler( Tcl_Interp* Interp )
  {
    std::ostringstream	msg;

    //-------------------------------------------------------------------
    // Rethrow the exception so it can be properly formatted
    //-------------------------------------------------------------------
    try
    {
      throw;
    }
    catch( const SwigException& e )
    {
      msg << "thrown SwigException: " << e.getResult( )
	  << " (info: " << e.getInfo( ) << ")";
    }
    catch ( const std::exception& e )
    {
      msg << "thrown std::exception: " << e.what( );
    }
    catch( ... )
    {
      msg << "unknown exception";
    }

    //-------------------------------------------------------------------
    // Formatting of message for TCL
    //-------------------------------------------------------------------
    Tcl_DString	ds;
    Tcl_DStringInit( &ds );
    Tcl_DStringAppend( &ds, msg.str( ).c_str( ), -1 );
    Tcl_DStringResult( Interp, &ds );
  } // function - tcl_exception_handler

}

#if SIZEOF_VOIDP == SIZEOF_INT
typedef int fd_type;
#elif SIZEOF_VOIDP == SIZEOF_LONG
typedef long fd_type;
#elif SIZEOF_VOIDP == SIZEOF_LONG_LONG
typedef long long fd_type;
#else
#error Unable to define types
#endif

//=======================================================================
// OSpaceChannel::private_type
//=======================================================================

struct OSpaceChannel::private_type
{
public:
  enum address_type {
    PEER,
    SELF
  };

  enum socket_type {
    ACTIVE,
    PASSIVE,
    SERVER
  };

  struct server {
    os_tcp_connection_server*	socket;
  };
  struct client {
    os_tcp_socket*		socket;
  };

  static const std::string	m_channel_name;
  static char			HASH_KEY[];


  private_type( OSpaceChannel& ChannelRef );

  ~private_type( );

  inline static private_type*
  cast( ClientData Data )
  {
    return static_cast< private_type* >( Data );
  }

  //---------------------------------------------------------------------
  //List of support functions
  //---------------------------------------------------------------------

  fd_type GetFD( ) const;

  std::string GetAddress( ) const;

  Tcl_Interp* GetInterp( ) const;

  inline void SetInterp( Tcl_Interp* Value )
  {
    m_interp = Value;
  }

  inline bool DeleteOnClose ( ) const
  {
    return m_delete_on_close;
  }

  bool FormatAddress( Tcl_DString* Result,
		      address_type Address,
		      CONST84 char* Option ) const;

  inline Tcl_Channel GetChannel( ) const
  {
    return m_channel;
  }

  inline static TclChannel& GetChannelProcs( )
  {
    return m_channel_procs;
  }

  inline const std::string& GetErrorMessage( ) const
  {
    return m_error_message;
  }

  inline os_ip_address GetIPAddress( ) const
  {
    return GetSocket( )->socket_address( ).ip_address( );
  }

  inline static OSpaceChannel& GetOSpaceChannel( ClientData Data )
  {
    return static_cast<private_type*>(Data)->m_channel_ref;
  }

  std::string GetHostname( ) const;

  std::string GetPeerAddress( ) const;

  int GetPeerPort( ) const;

  int GetPort( ) const;

  char* GetScript( ) const;

  inline void SetScript( const char* Value )
  {
    m_script = Value;
  }

  os_tcp_socket* GetTCPConnectSocket( ) const;

  os_socket* GetSocket( ) const;

  os_tcp_connection_server* GetServerSocket( ) const;

  bool IsServer( ) const;

  inline void SetSocketType( private_type::socket_type SocketType )
  {
    m_socket_type = SocketType;
  }

  os_ioctl_type IOControl( );

  void IOControl( const os_ioctl_type& ctl );

  void Open( const std::string& Host, int Port,
	     const std::string& MyHost, int MyPort );

  void Close( );

  inline void SetChannel( Tcl_Channel Channel )
  {
    m_channel = Channel;
  }

  inline void SetDeleteOnClose( bool DeleteState = false )
  {
    m_delete_on_close = DeleteState;
  }

  inline void SetError( const char* ErrorMessage )
  {
    if ( *ErrorMessage == '\0' )
    {
      m_error_message.clear( );
    }
    else
    {
      m_error_message = ErrorMessage;
    }
  }

  void SetSocket( os_tcp_socket* Socket );

  void SetSocket( os_tcp_connection_server* Socket );

  inline static void
  TclExceptionHandler( ClientData Data )
  {
    if ( cast( Data ) &&
	 cast( Data )->GetInterp( ) )
    {
      tcl_exception_handler( ::OSpaceChannel::private_type::cast( Data )->GetInterp( ) );
    }
  } // funciton - TclExceptionHandler
  
  //---------------------------------------------------------------------
  //List of channel functions
  //---------------------------------------------------------------------
  static int close( ClientData InstanceData, Tcl_Interp* Interp );

  static int input( ClientData InstanceData,
		    char* Buffer, int ToRead, int* Error );

  static int output( ClientData InstanceData,
		     CONST84 char* Buffer, int ToWrite, int* Error );

  static int set_option( ClientData InstanceData, Tcl_Interp* Interp,
			 CONST84 char* OptionName,
			 CONST84 char* Value );

  static int get_option( ClientData InstanceData, Tcl_Interp* Interp,
			 CONST84 char* OptionName, Tcl_DString* Result );

  static void watch( ClientData InstanceData,
		     int Mask );

  static int get_handle( ClientData InstanceData,
			   int Direction,
			   ClientData* Handle );

  static int block_mode( ClientData InstanceData,
			 int Mode );

  // :TODO: private:

  static void accept_callback_proc( ClientData Data, Tcl_Channel Channel,
				    char* Address, int Port );

private:
  static TclChannel&	m_channel_procs;

  union {
    // Information that is unique to a server socket
    struct server m_server;
    // Information that is unique to a data socket
    struct client m_client;
  };

  Tcl_Interp*		m_interp;

  Tcl_Channel		m_channel;

  OSpaceChannel&	m_channel_ref;

  bool			m_delete_on_close;

  socket_type		m_socket_type;

  std::string		m_error_message;

  std::string		m_script;

};

inline bool OSpaceChannel::private_type::
IsServer( ) const
{
  return ( m_socket_type == SERVER );
}

inline void OSpaceChannel::private_type::
SetSocket( os_tcp_socket* Socket )
{
  m_client.socket = Socket;
}

inline void OSpaceChannel::private_type::
SetSocket( os_tcp_connection_server* Socket )
{
  m_server.socket = Socket;
}

#if 0
#define AT() std::cerr << __FILE__ << " " << __LINE__ << std::endl;
#else
#define AT()
#endif

//=======================================================================
// Initialize the jump table
//=======================================================================

#ifdef OS_STATICS_DTOR_PROBLEM
  extern void os_delete_class_objects( );
#endif /* OS_STATICS_DTOR_PROBLEM */

static TclChannel&
create_channel_interface( )
{
  static TclChannel Interface( OSpaceChannel::CHANNEL_TYPE );

#ifdef OS_STATICS_DTOR_PROBLEM
  General::AtExit::Append( os_delete_class_objects,
			   "os_delete_class_objects",
			   0 );
#endif /* OS_STATICS_DTOR_PROBLEM */

  AT();  // Initialize the proc table
  Interface.SetCloseProc( OSpaceChannel::private_type::close );
  Interface.SetInputProc( OSpaceChannel::private_type::input );
  Interface.SetOutputProc( OSpaceChannel::private_type::output );
  Interface.SetSetOptionProc( OSpaceChannel::private_type::set_option );
  Interface.SetGetOptionProc( OSpaceChannel::private_type::get_option );
  Interface.SetGetHandleProc( OSpaceChannel::private_type::get_handle );
  Interface.SetBlockModeProc( OSpaceChannel::private_type::block_mode );
  Interface.SetWatchProc( OSpaceChannel::private_type::watch );
  return Interface;
}

//=======================================================================
// OSpaceChannel
//=======================================================================


#define EMPTY_PARAM

#define ERROR_PREP( LM_DATA ) \
  LM_DATA->SetError( "" ); \
  try \
  {

#define ERROR_PREP_STATIC( LM_DATA ) \
  ERROR_PREP( static_cast<private_type*>( LM_DATA ) )

#define ERROR_CATCH_BASE( LM_DATA, LM_ACTION ) \
  } \
  catch ( const std::exception& e ) \
  { \
    LM_DATA->SetError( e.what( ) ); \
    LM_ACTION; \
  } \
  catch( ... ) \
  { \
    LM_DATA->SetError( "Unknwon exception" ); \
    LM_ACTION; \
  }

#define ERROR_CATCH( LM_DATA ) \
	ERROR_CATCH_BASE( LM_DATA,throw)

#define ERROR_CATCH_NO_RETHROW( LM_DATA ) \
  ERROR_CATCH_BASE( LM_DATA,EMPTY_PARAM)

#define ERROR_CATCH_STATIC( LM_DATA ) \
  ERROR_CATCH( static_cast<private_type*>( LM_DATA ) )

const std::string OSpaceChannel::private_type::m_channel_name( "ldas/dsock" );

const char* OSpaceChannel::CHANNEL_TYPE( "dsock" );

OSpaceChannel::
OSpaceChannel(  Tcl_Interp* Interp, int ObjC, Tcl_Obj* CONST ObjV[] )
  : m_private( new private_type( *this ) )
{
  //---------------------------------------------------------------------
  // Handle parsing of tcl options
  //---------------------------------------------------------------------

  static CONST84 char* options[] = {
    "-server",
    "-myaddr",
    "-myport",
    (char*)NULL
  };
  enum options_type {
    SKT_SERVER,
    SKT_MYADDR,
    SKT_MYPORT
  };

  AT();
  std::string	host;
  int		port( 0 );

  std::string	myaddr;
  int		myport( 0 );

  const char* script( "" );
  char*	arg;
  int	o, index;

  AT();
  for ( o = 1; o < ObjC; o++ )
  {
    arg = Tcl_GetString( ObjV[o] );
    if ( *arg != '-' )
    {
      AT();
      break;
    }
    AT();
    if ( Tcl_GetIndexFromObj( Interp, ObjV[o], options,
			      "option", TCL_EXACT, &index) != TCL_OK )
    {
      AT();
      std::ostringstream	oss;
      oss << "Bad Option: " << arg << std::endl;
   
      const string error_string( oss.str() );
      AT();
      throw std::invalid_argument( error_string.c_str() );
    }
    AT();
    switch ( (options_type)index )
    {
    case SKT_SERVER:
      {
	m_private->SetSocketType( private_type::SERVER );
	m_private->SetSocket( (os_tcp_connection_server*)NULL );
	o++;
	NoArgumentCheck( Interp, o >= ObjC, options[ SKT_SERVER ] );
	script = Tcl_GetString( ObjV[o] );
	break;
      }
    case SKT_MYADDR:
      o++;
      NoArgumentCheck( Interp, o >= ObjC, options[ SKT_MYADDR ] );
      myaddr = Tcl_GetString( ObjV[o] );
      break;
    case SKT_MYPORT:
      {
	o++;
	NoArgumentCheck( Interp, o >= ObjC, options[ SKT_MYPORT ] );

	if ( TclSockGetPort( Interp,
			     Tcl_GetString( ObjV[o] ),
			     static_cast< CONST84 char*>( "tcp" ),
			     &(myport) ) != TCL_OK )
	{
	  AT();
	  throw std::runtime_error( "Unable to create port" );
	}
	break;
      }
    default:
      panic( "OSpaceChannel: bad option index to ospace socket options" );
      break;
    }
  }
  AT();
  if ( m_private->IsServer( ) )
  {
    AT();
    host = myaddr;
  }
  else if ( o < ObjC )
  {
    AT();
    host = Tcl_GetString( ObjV[o++] );
  }
  else
  {
    AT();
    throw std::runtime_error( "Wrong number of arguments" );
  }
  if ( o == ObjC-1 )
  {
    AT();
    if ( TclSockGetPort( Interp, Tcl_GetString( ObjV[o] ),
			 "tcp", &(port) ) != TCL_OK )
    {
      AT();
      throw std::runtime_error( "Unable to create port" );
    }
  }
  else
  {
    AT();
    throw std::runtime_error( "Wrong number of arguments" );
  }
  AT();
  if ( m_private->IsServer( ) )
  {
    // Initialize the callback structure.
    m_private->SetScript( script );
    m_private->SetInterp( Interp );
  }
  AT();
  // Get Socket started
  m_private->Open( host, port, myaddr, myport );
  if ( m_private->GetChannel( ) == (Tcl_Channel)NULL )
  {
    std::ostringstream	oss;
    oss << "Unable to create "
	<< CHANNEL_TYPE
	<< " channel";
   
    const string error_string( oss.str() );
    throw std::runtime_error( error_string.c_str() );
  }

  AT();
  Tcl_RegisterChannel( Interp, m_private->GetChannel( ) );
  AT();
  Tcl_AppendResult( Interp, Tcl_GetChannelName( m_private->GetChannel( ) ),
		    (char*)NULL );
}

OSpaceChannel::
OSpaceChannel( const OSpaceChannel& ServerChannel,
	       os_tcp_socket* Socket )
  : m_private( new private_type( *this ) )
{
  m_private->SetSocket( Socket );
  m_private->SetSocketType( private_type::PASSIVE );
  m_private->SetDeleteOnClose( true );
  m_private->SetInterp( ServerChannel.m_private->GetInterp( ) );
  m_private->SetScript( ServerChannel.m_private->GetScript( ) );

  //-------------------------------------------------------------------
  // Create a channel for this call
  //-------------------------------------------------------------------
  AT();
  std::string sn( gen_socket_name( ) );
  m_private->SetChannel( Tcl_CreateChannel
			 ( m_private->GetChannelProcs( ).
			   GetChannelType( ),
			   const_cast<char*>( sn.c_str( ) ),
			   (ClientData)m_private.get( ),
			   (TCL_READABLE|TCL_WRITABLE) ) );
  Tcl_RegisterChannel( m_private->GetInterp( ), m_private->GetChannel( ) );

  //-------------------------------------------------------------------
  // Do the callback thing
  //-------------------------------------------------------------------

  std::string pa( m_private->GetPeerAddress( ) );
  m_private->accept_callback_proc( m_private.get( ),
				   m_private->GetChannel( ),
				   const_cast<char*>( pa.c_str( ) ),
				   m_private->GetPeerPort( ) );
}

OSpaceChannel::
~OSpaceChannel( )
{
}

int OSpaceChannel::
BlockMode( int Mode )
{
  AT();
  os_ioctl_type ctl( m_private->IOControl( ) );

  if ( Mode == TCL_MODE_BLOCKING )
  {
    ctl &= ~O_NONBLOCK;
  }
  else
  {
    // NON blocking is not supported on ObjectSpace sockets. Force the issue.
    ctl &= ~O_NONBLOCK;
  }
  m_private->IOControl( ctl );
  return 0;
}

int OSpaceChannel::
Close( Tcl_Interp* Interp )
{
  AT();
  m_private->Close( );
  return 0;
}

#if HAVE_LDAS_PACKAGE_ILWD
std::string OSpaceChannel::
ElementGet( Tcl_Channel Channel )
{
  OSpaceChannel& me( private_type::GetOSpaceChannel( Tcl_GetChannelInstanceData( Channel ) ) );

  ERROR_PREP( me.m_private );
  return recvElementObject( me.m_private->GetTCPConnectSocket( ) );
  ERROR_CATCH( me.m_private );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
std::string OSpaceChannel::
ElementGet( os_tcp_socket* Socket )
{
  return recvElementObject( Socket );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
void OSpaceChannel::
ElementPut( Tcl_Channel Channel, ILwd::LdasElement* Element )
{
  OSpaceChannel& me( private_type::GetOSpaceChannel( Tcl_GetChannelInstanceData( Channel ) ) );

  ERROR_PREP( me.m_private );
  try
  {
    // Protect object from deletion before fully transmitted
    if ( Registry::isElementValid( Element ) ) {
      Element->refCountUp();
    }
    sendElementObject( me.m_private->GetTCPConnectSocket( ), Element );
    if ( Registry::isElementValid( Element ) ) {
      Element->refCountDown();
    }
  }
  catch( ... )
  {
    if ( Registry::isElementValid( Element ) ) {
      Element->refCountDown();
    }
    throw;	// Rethrow of the exception
  }
  ERROR_CATCH( me.m_private );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

#if HAVE_LDAS_PACKAGE_ILWD
void OSpaceChannel::
ElementPutDLP( Tcl_Channel Channel, ILwd::LdasElement* Element )
{
    while ( Tcl_DoOneEvent( TCL_FILE_EVENTS | TCL_DONT_WAIT ) != 0 )
    {
      // process all pending TCL events
    }
    ElementPut( Channel, Element );
}
#endif /* HAVE_LDAS_PACKAGE_ILWD */

int OSpaceChannel::
GetHandle( int /* Direction */, ClientData* Handle )
{
  AT();
  *Handle = (ClientData)( m_private->GetFD( ) );
  return TCL_OK;
}

int OSpaceChannel::
GetOption(  Tcl_Interp* Interp,
	    CONST84 char* OptionName,
	    Tcl_DString* Result )
{
  AT();
  std::string	option( ( OptionName == (char*)NULL )
			? ""
			: OptionName );
  bool		bad_option( true );

  static const char* options =
    "-error "
    "-peername "
    "-sockname "
    "-linger";

  if ( option == "-error" )
  {
    bad_option = false;
    std::string em( m_private->GetErrorMessage( ) );
    Tcl_DStringAppend( Result, em.c_str( ), -1 );
  }
  if ( ( option == "-peername" ) || ( option.length( ) == 0 ) )
  {
    bad_option = false;
    if ( ! m_private->FormatAddress( Result, private_type::PEER, OptionName ) )
    {
      Tcl_AppendResult( Interp, "can't get peername", (char*)NULL );
    }
  }
  if ( ( option == "-sockname" ) || ( option.length( ) == 0 ) )
  {
    bad_option = false;
    if ( ! m_private->FormatAddress( Result, private_type::SELF, OptionName ) )
    {
      Tcl_AppendResult( Interp, "can't get sockname", (char*)NULL );
    }
  }
  if ( ( option == "-linger" ) || ( option.length( ) == 0 ) )
  {
    bad_option = false;
    std::ostringstream	linger;
    int			seconds( 0 );
    
    if ( ! m_private->GetSocket( )->linger_information( seconds ) )
    {
      seconds = 0;
    }
    linger << seconds;

    if ( option.length( ) == 0 )
    {
      Tcl_DStringAppendElement( Result, "-linger" );
    }
   
    const string linger_string( linger.str() );
    Tcl_DStringAppendElement( Result, linger_string.c_str( ) );
  }
  if ( bad_option )
  {
    return Tcl_BadChannelOption( Interp, OptionName,
				 const_cast<char*>( options ) );
  }
  return TCL_OK;
}

os_tcp_socket* OSpaceChannel::
GetOSpaceSocket( Tcl_Channel Channel )
{
  OSpaceChannel& me( private_type::GetOSpaceChannel( Tcl_GetChannelInstanceData( Channel ) ) );

  return me.m_private->GetTCPConnectSocket( );
}

int OSpaceChannel::
Input( char* Buffer, int ToRead, int* Error )
{
  AT();
  try
  {
    ERROR_PREP( m_private );
    ToRead = m_private->GetTCPConnectSocket()->read( Buffer, ToRead );
    ERROR_CATCH( m_private );

  }
  catch( ... )
  {
    *Error = errno;
    return -1;
  }
  return ToRead;
}

int OSpaceChannel::
Output( CONST84 char* Buffer, int ToWrite, int* Error )
{
  AT();
  try
  {
    ERROR_PREP( m_private );
    ToWrite = m_private->GetTCPConnectSocket()->write( Buffer, ToWrite );
    ERROR_CATCH( m_private );
  }
  catch( ... )
  {
    *Error = errno;
    return -1;
  }
  return ToWrite;
}

int OSpaceChannel::
SetOption( Tcl_Interp* Interp,
	   CONST84 char* OptionName,
	   CONST84 char* Value )
{
  AT();
  ERROR_PREP( m_private );
  std::string option( OptionName );

  static const char* options =
    "-linger";

  if ( option == "-linger" )
  {
    std::istringstream	linger( Value );
    int			seconds( 0 );
    
    linger >> seconds;
    m_private->GetSocket( )->linger_information( true, seconds );
  }
  else
  {
    return Tcl_BadChannelOption( Interp, OptionName,
				 const_cast<char*>( options ) );
  }
  return TCL_OK;
  ERROR_CATCH_NO_RETHROW( m_private );
  return TCL_ERROR;
}

void OSpaceChannel::
Watch( int Mask )
{
  AT();
  if ( ! m_private->IsServer( ) )
  {
    AT();
    if ( Mask )
    {
      AT();
      Tcl_CreateFileHandler( m_private->GetFD( ),
			     Mask,
			     (Tcl_FileProc *) Tcl_NotifyChannel,
			     (ClientData)m_private->GetChannel( ) );
    }
    else
    {
      Tcl_DeleteFileHandler( m_private->GetFD( ) );
    }
  }
}

//=======================================================================
// OSpaceChannel::private_type
//=======================================================================

TclChannel& OSpaceChannel::private_type::
m_channel_procs = create_channel_interface( );

char OSpaceChannel::private_type::HASH_KEY[] = "ObjectSpaceAcceptCallbacks";

OSpaceChannel::private_type::
private_type( OSpaceChannel& OSChannel )
  : m_channel_ref( OSChannel ),
    m_delete_on_close( false ),
    m_socket_type( ACTIVE )
{
  AT();
  m_client.socket = (os_tcp_socket*)NULL;
}

OSpaceChannel::private_type::
~private_type( )
{
  using namespace Registry;

  switch( m_socket_type )
  {
  case SERVER:
    if ( m_server.socket )
    {
      serverRegistry.removeObject( m_server.socket );
    }
    delete m_server.socket;
    m_server.socket = (os_tcp_connection_server*)NULL;
    break;
  case ACTIVE:
  case PASSIVE:
    if ( m_client.socket )
    {
      if ( m_socket_type == ACTIVE )
      {
	activeSocketRegistry.removeObject( m_client.socket );
      }
      else
      {
	passiveSocketRegistry.removeObject( m_client.socket );
      }
    }
    delete m_client.socket;
    m_client.socket = (os_tcp_socket*)NULL;
    break;
  }
}

void OSpaceChannel::private_type::
Close( )
{
  if ( IsServer() )
  {
    closeServerSocket( GetServerSocket( ) );
    m_server.socket = (os_tcp_connection_server*)NULL;
  }
  else
  {
    closeDataSocket( GetTCPConnectSocket( ) );
    m_client.socket = (os_tcp_socket*)NULL;
  }
}

bool OSpaceChannel::private_type::
FormatAddress( Tcl_DString* Result,
	       address_type Address,
	       CONST84 char* Option ) const
{
  os_socket_address	addr;
  Tcl_DString		ds;

  if ( Address == PEER )
  {
    if ( IsServer( ) )
    {
      return false;
    }
    else
    {
      addr = m_client.socket->peer_address();
    }
  }
  else
  {
    if ( IsServer( ) )
    {
      addr = m_server.socket->socket_address( );
    }
    else
    {
      addr = m_client.socket->socket_address( );
    }
  }
  if ( ( Option == (char*)NULL ) || ( strlen( Option ) <= 0 ) )
  {
    Tcl_DStringAppendElement( Result,
			      ( Address == PEER )
			      ? "-peername"
			      : "-sockname" );
    Tcl_DStringStartSublist( Result );
  }
  // IP Address
  Tcl_ExternalToUtfDString( NULL, inet_ntoa( addr.ip_address( ) ), -1, &ds );
  Tcl_DStringAppendElement( Result, Tcl_DStringValue( &ds ) );

  // Host Name (IP Address if Host Name unknown)
  hostent*	he( gethostbyaddr( (char*)&addr,
				   sizeof( os_ip_address ),
				   AF_INET ) );
  if ( he == (hostent*)NULL )
  {
    Tcl_ExternalToUtfDString( NULL, inet_ntoa( addr.ip_address( ) ), -1, &ds );
  }
  else
  {
    Tcl_ExternalToUtfDString( NULL, he->h_name, -1, &ds );
  }
  Tcl_DStringAppendElement( Result, Tcl_DStringValue( &ds ) );

  // Port Number
  std::ostringstream	port;
  port << addr.port();
   
  const string port_string( port.str() );
   
  Tcl_ExternalToUtfDString( NULL, port_string.c_str( ), -1, &ds );
  Tcl_DStringAppendElement( Result, Tcl_DStringValue( &ds ) );

  if ( ( Option == (char*)NULL ) || ( strlen( Option ) <= 0 ) )
  {
    Tcl_DStringEndSublist( Result );
  }
  return true;
}

Tcl_Interp* OSpaceChannel::private_type::
GetInterp( ) const
{
  return m_interp;
}

fd_type OSpaceChannel::private_type::
GetFD( ) const
{
  if ( GetSocket( ) )
  {
    return GetSocket( )->descriptor( );
  }
  return -1;
}

std::string OSpaceChannel::private_type::
GetAddress( ) const
{
  return inet_ntoa( GetSocket( )->socket_address().ip_address() );
}

std::string OSpaceChannel::private_type::
GetHostname( ) const
{
  os_ip_address addr( GetIPAddress() );
  hostent*	hostname( gethostbyaddr( (char*)&addr,
					 sizeof( os_ip_address),
					 AF_INET ) );
  
  if ( hostname == (hostent*)NULL )
  {
    return GetAddress( );
  }
  return hostname->h_name;
}

std::string OSpaceChannel::private_type::
GetPeerAddress( ) const
{
  if ( IsServer( ) )
  {
    return "";
  }
  return inet_ntoa( m_client.socket->peer_address().ip_address() );
}

int OSpaceChannel::private_type::
GetPeerPort( ) const
{
  if ( IsServer( ) )
  {
    return 0;
  }
  return m_client.socket->peer_address().port( );
}

int OSpaceChannel::private_type::
GetPort( ) const
{
  return GetSocket( )->socket_address().port( );
}

char* OSpaceChannel::private_type::
GetScript( ) const
{
  return const_cast<char*>( m_script.c_str( ) );
}

os_tcp_socket* OSpaceChannel::private_type::
GetTCPConnectSocket( ) const
{
  AT();
  if ( IsServer( ) )
  {
    throw std::runtime_error( "Cannot get socket for server" );
  }
  else
  {
    return m_client.socket;
  }
}

os_socket* OSpaceChannel::private_type::
GetSocket( ) const
{
  AT();
  if ( IsServer( ) )
  {
    return dynamic_cast<os_socket*>( m_server.socket );
  }
  return dynamic_cast<os_socket*>( m_client.socket );
}

os_tcp_connection_server* OSpaceChannel::private_type::
GetServerSocket( ) const
{
  AT();
  if ( IsServer( ) )
  {
    return m_server.socket;
  }
  else
  {
    throw std::runtime_error( "Cannot get socket for client" );
  }
}

os_ioctl_type OSpaceChannel::private_type::
IOControl( )
{
  if ( IsServer( ) )
  {
    return m_server.socket->io_control( );
  }
  return m_client.socket->io_control( );
}

void OSpaceChannel::private_type::
IOControl( const os_ioctl_type& ctl )
{
  if ( IsServer( ) )
  {
    m_server.socket->io_control( ctl );
  }
  else
  {
    m_client.socket->io_control( ctl );
  }
}

void OSpaceChannel::private_type::
Open( const std::string& Host, int Port,
      const std::string& MyHost, int MyPort )
{
  AT();
  if ( IsServer() )
  {
    if ( MyPort != 0 )
    {
      throw std::invalid_argument( "Option -myport is not valid for servers" );
    }
    //-------------------------------------------------------------------
    // Get Server started
    //-------------------------------------------------------------------

    AT();
    m_server.socket = createServerSocket( Port, Host.c_str( ) );

    //-------------------------------------------------------------------
    // Set up the callback mechanism for accepting connectons
    // from new client
    //-------------------------------------------------------------------

    AT();
    int fd = GetFD();
    Tcl_CreateFileHandler( fd,
			   TCL_READABLE,
			   os_accept,
			   (ClientData)this );

    AT();
    std::string	sn( gen_socket_name( ) );
    m_channel =
      Tcl_CreateChannel( m_channel_procs.GetChannelType( ),
			 const_cast<char*>( sn.c_str() ),
			 (ClientData)this,
			 (TCL_READABLE|TCL_WRITABLE) );
    if ( m_channel )
    {
      // Rgister with the interpreter to let us know whhen the
      // interpreter is deleted (by having the callback set the
      // s_interp field to NULL). This is to avoid trying to
      // eval the script in a deleted interpreter.

      int		created;
      Tcl_Interp*	interp( GetInterp( ) );
      Tcl_HashTable	*hTblPtr=
	(Tcl_HashTable*)Tcl_GetAssocData( interp, HASH_KEY, NULL );
      if ( hTblPtr == (Tcl_HashTable*)NULL )
      {
	hTblPtr = (Tcl_HashTable*)ckalloc((unsigned)sizeof(Tcl_HashTable));
	Tcl_InitHashTable( hTblPtr, TCL_ONE_WORD_KEYS );
	(void)Tcl_SetAssocData( interp, HASH_KEY,
				delete_proc, (ClientData)hTblPtr );
      }
      Tcl_HashEntry*	hPtr = Tcl_CreateHashEntry( hTblPtr,
						    (char*)this,
						    &created );
      if ( !created )
      {
	panic("Object Space Server Cleanup: damaged accept record table" );
      }
      Tcl_SetHashValue( hPtr, (ClientData)this );

      // Register a close callback. This callback will inform the
      // interpreter (if it still exists) that this channel dos not
      // need to be informed when the interpreter is deleted

      Tcl_CreateCloseHandler( m_channel,
			      server_close_proc,
			      (ClientData)this );
    }
  }
  else
  {
    AT();
    m_client.socket = createDataSocket( MyPort, MyHost.c_str( ) );
    AT();
    try
    {
      AT();
      connectDataSocket( m_client.socket, Host.c_str(), Port );
    }
    catch( ... )
    {
      AT();
      sleep( 2 );
      connectDataSocket( m_client.socket, Host.c_str(), Port );
    }
    // Actually create channel
    AT();
    std::string	sn( gen_socket_name( ) );
    m_channel =
      Tcl_CreateChannel( m_channel_procs.GetChannelType( ),
			 const_cast<char*>( sn.c_str() ),
			 (ClientData)this,
			 (TCL_READABLE|TCL_WRITABLE) );
  }
}

int OSpaceChannel::private_type::
close( ClientData InstanceData, Tcl_Interp* Interp )
try
{
  AT();
  private_type*		p( static_cast<private_type*>( InstanceData ) );
  OSpaceChannel&	channel( GetOSpaceChannel( InstanceData ) );

  channel.Close( Interp );
  if ( p->DeleteOnClose( ) )
  {
    Tcl_UnregisterChannel( p->GetInterp( ), p->GetChannel( ) );
    delete &channel;
  }

  return 0;
}
catch( ... )
{
  AT();
  tcl_exception_handler( Interp );
  return errno;
}

int OSpaceChannel::private_type::
get_handle( ClientData InstanceData,
	    int Direction,
	    ClientData* Handle )
{
  try
  {
    AT();
    ERROR_PREP_STATIC( InstanceData );
    return GetOSpaceChannel( InstanceData ).GetHandle( Direction, Handle );
    ERROR_CATCH_STATIC( InstanceData );
  }
  catch( ... )
  {
    private_type::TclExceptionHandler( InstanceData );
    return TCL_ERROR;
  }
}

int OSpaceChannel::private_type::
input( ClientData InstanceData, char* Buffer, int ToRead, int* Error )
{
  try
  {
    AT();
    ERROR_PREP_STATIC( InstanceData );
    return GetOSpaceChannel( InstanceData ).Input( Buffer, ToRead, Error );
    ERROR_CATCH_STATIC( InstanceData );
  }
  catch( ... )
  {
    private_type::TclExceptionHandler( InstanceData );
    return TCL_ERROR;
  }
}

int OSpaceChannel::private_type::
set_option( ClientData InstanceData, Tcl_Interp* Interp,
	    CONST84 char* OptionName,
	    CONST84 char* Value )
{
  try
  {
    AT();
    ERROR_PREP_STATIC( InstanceData );
    return GetOSpaceChannel( InstanceData ).SetOption( Interp, OptionName, Value );
    ERROR_CATCH_STATIC( InstanceData );
  }
  catch( ... )
  {
    tcl_exception_handler( Interp );
    return TCL_ERROR;
  }

}

int OSpaceChannel::private_type::
get_option( ClientData InstanceData, Tcl_Interp* Interp,
	    CONST84 char* OptionName, Tcl_DString* Result )
{
  try
  {
    AT();
    ERROR_PREP_STATIC( InstanceData );
    return GetOSpaceChannel( InstanceData ).GetOption( Interp, OptionName, Result );
    ERROR_CATCH_STATIC( InstanceData );
  }
  catch( ... )
  {
    tcl_exception_handler( Interp );
    return TCL_ERROR;
  }
}


int OSpaceChannel::private_type::
output( ClientData InstanceData,
	CONST84 char* Buffer,
	int ToWrite,
	int* Error )
{
  try
  {
    AT();
    ERROR_PREP_STATIC( InstanceData );
    return GetOSpaceChannel( InstanceData ).Output( Buffer, ToWrite, Error );
    ERROR_CATCH_STATIC( InstanceData );
  }
  catch( ... )
  {
    private_type::TclExceptionHandler( InstanceData );
    return TCL_ERROR;
  }

}

int OSpaceChannel::private_type::
block_mode( ClientData InstanceData, int Mode )
{
  try
  {
    AT();
    ERROR_PREP_STATIC( InstanceData );
    return GetOSpaceChannel( InstanceData ).BlockMode( Mode );
    ERROR_CATCH_STATIC( InstanceData );
  }
  catch( ... )
  {
    private_type::TclExceptionHandler( InstanceData );
    return TCL_ERROR;
  }
}

void OSpaceChannel::private_type::
watch( ClientData InstanceData, int Mask )
{
  try
  {
    AT();
    ERROR_PREP_STATIC( InstanceData );
    GetOSpaceChannel( InstanceData ).Watch( Mask );
    ERROR_CATCH_STATIC( InstanceData );
  }
  catch( ... )
  {
    private_type::TclExceptionHandler( InstanceData );
  }
}

//=======================================================================
// Callback routines
//=======================================================================

void OSpaceChannel::private_type::
accept_callback_proc( ClientData Data, Tcl_Channel Channel,
		      char* Address, int Port )
{
  private_type*	me( static_cast<private_type*>( Data ) );

  AT();	

  if ( me->GetInterp() != (Tcl_Interp*)NULL )
  {
    AT();
    //-------------------------------------------------------------------
    // Protect ourselves
    //-------------------------------------------------------------------

    Tcl_Preserve( (ClientData)( me->GetScript( ) ) );
    Tcl_Preserve( (ClientData)( me->GetInterp( ) ) );

#if WORKING
    Tcl_RegisterChannel( me->GetInterp( ), me->m_channel );
#endif /* WORKING */

    //-------------------------------------------------------------------
    // Invoke any script
    //-------------------------------------------------------------------

    std::ostringstream	command;

    command << me->GetScript()
	    << " " << Tcl_GetChannelName( me->m_channel )
	    << " " << Address
	    << " " << Port;
   
    const string command_string( command.str() );
    int result = Tcl_VarEval( me->GetInterp( ),
			      command_string.c_str( ),
			      (char*)NULL );
    if ( result != TCL_OK )
    {
      AT();
      Tcl_BackgroundError( me->GetInterp( ) );
      Tcl_UnregisterChannel( me->GetInterp( ), me->m_channel );
    }
    
    //-------------------------------------------------------------------
    // Release resources
    //-------------------------------------------------------------------
    AT();
#if WORKING
    Tcl_UnregisterChannel( (Tcl_Interp*)NULL, me->m_channel );
#endif	/* WORKING */
    
    Tcl_Release( (ClientData)( me->GetInterp( ) ) );
    Tcl_Release( (ClientData)( me->GetScript( ) ) );
  }
}

void
delete_proc( ClientData Data, Tcl_Interp* /* Interp */ )
{
  AT();
  Tcl_HashEntry*	hPtr;
  Tcl_HashSearch	hSearch;
  Tcl_HashTable*	hTblPtr = static_cast<Tcl_HashTable*>( Data );
    
  for ( hPtr = Tcl_FirstHashEntry( hTblPtr, &hSearch );
	hPtr != (Tcl_HashEntry*)NULL;
	hPtr = Tcl_NextHashEntry( &hSearch ) )
  {
    OSpaceChannel::private_type*
      me( static_cast<OSpaceChannel::private_type*>( Tcl_GetHashValue( hPtr ) ) );

    me->SetInterp( (Tcl_Interp*)NULL );
  }
  Tcl_DeleteHashTable( hTblPtr );
  ckfree( (char*)hTblPtr );
}

void
os_accept( ClientData Data, int /* Mask */ )
try
{
  AT();
  OSpaceChannel::private_type*
    me( static_cast<OSpaceChannel::private_type*>( Data ) );
      
  AT();
  if ( ! me->IsServer() )
  {
    AT();
    return;
  }
  if ( me->GetInterp( ) != (Tcl_Interp*)NULL )
  {
    //-------------------------------------------------------------------
    // Accept incoming calls
    //-------------------------------------------------------------------
    AT();
    std::auto_ptr<OSpaceChannel> ptr
      ( new OSpaceChannel( OSpaceChannel::private_type::GetOSpaceChannel( me ),
			   acceptDataSocket( me->GetServerSocket( ), "", 0 ) ) );
    ptr.release();
  }
}
catch( ... )
{
}

void
server_close_proc( ClientData Data )
{
  AT();
  OSpaceChannel::private_type*
    me( static_cast<OSpaceChannel::private_type*>( Data ) );
  
  if ( ! me->IsServer() )
  {
    return;
  }
  Tcl_Interp*	interp( me->GetInterp( ) );
  if ( interp != (Tcl_Interp*)NULL )
  {
    
    Tcl_HashTable*	hTblPtr;
    Tcl_HashEntry*	hPtr;

    hTblPtr = (Tcl_HashTable*)Tcl_GetAssocData( interp,
						OSpaceChannel::private_type::HASH_KEY,
						NULL );
    if ( hTblPtr == (Tcl_HashTable*)NULL )
    {
      return;
    }
    hPtr = Tcl_FindHashEntry(hTblPtr, (char*)me );
    if ( hPtr == (Tcl_HashEntry*)NULL )
    {
      return;
    }
    Tcl_DeleteHashEntry( hPtr );
    me->SetInterp( (Tcl_Interp*)NULL );
  }
}
