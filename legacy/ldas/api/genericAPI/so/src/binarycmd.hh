/* -*- c-basic-offset: 4 -*- */
#ifndef BinaryCmdHH
#define BinaryCmdHH


#undef SAVE_BUGREPORT
#undef SAVE_STRING
#undef SAVE_NAME
#undef SAVE_TARNAME
#undef SAVE_VERSION

#define SAVE_BUGREPORT PACKAGE_BUGREPORT
#define SAVE_STRING PACKAGE_STRING
#define SAVE_NAME PACKAGE_NAME
#define SAVE_TARNAME PACKAGE_TARNAME
#define SAVE_VERSION PACKAGE_VERSION

#undef PACKAGE_BUGREPORT
#undef PACKAGE_STRING
#undef PACKAGE_NAME
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION

#include "ospace/network.h"

#undef PACKAGE_BUGREPORT
#undef PACKAGE_STRING
#undef PACKAGE_NAME
#undef PACKAGE_TARNAME
#undef PACKAGE_VERSION

#define PACKAGE_BUGREPORT SAVE_BUGREPORT
#define PACKAGE_STRING SAVE_STRING
#define PACKAGE_NAME SAVE_NAME
#define PACKAGE_TARNAME SAVE_TARNAME
#define PACKAGE_VERSION SAVE_VERSION

#undef SAVE_BUGREPORT
#undef SAVE_STRING
#undef SAVE_NAME
#undef SAVE_TARNAME
#undef SAVE_VERSION

#include "genericAPI/LdasBinary.hh"

#include "OSpaceException.hh"
#include "swigexception.hh"
#include "threaddecl.hh"

extern "C"
{
    void sendRawBinary( os_tcp_socket* socket, LdasBinary* binary )
	throw( SwigException );
    CREATE_THREADED2V_DECL(sendRawBinary, os_tcp_socket*, LdasBinary* );
    LdasBinary* recvRawBinary( os_tcp_socket* socket )
        throw( SwigException );
    CREATE_THREADED1_DECL( recvRawBinary, LdasBinary*, os_tcp_socket* );
    LdasBinary* getRawBinary( LdasBinary* binary, unsigned int& bytes )
        throw( SwigException );
    LdasBinary* putRawBinary( char* s, unsigned int bytes )
        throw( SwigException );
    void destructBinary( LdasBinary* binary )
        throw( SwigException );
} // extern "C"


#endif // BinaryCmdHH
