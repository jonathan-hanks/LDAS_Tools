#ifndef GeneralLdasBinaryHH
#define GeneralLdasBinaryHH

#include "genericAPI/config.h"

#ifdef HAVE_LIBOSPACE
#include "general/undef_ac.h"

#include <ospace/header.h>
#endif // HAVE_LIBOSPACE


//-----------------------------------------------------------------------------
//
//: Binary Data
//
class LdasBinary
{
public:
    
    LdasBinary();
    LdasBinary( void* m, unsigned int bytes, bool ownsdata = true );
    LdasBinary( const LdasBinary& b );

    ~LdasBinary();

    LdasBinary& operator=( const LdasBinary& b );

    unsigned int getBytes() const;
    void* getPointer() const;

private:

    unsigned char* mPointer;
    unsigned int mBytes;

#ifdef HAVE_LIBOSPACE    
    OS_POLY_FUNCTION( (LdasBinary*) )

    friend void os_write( os_bstream& stream, const LdasBinary& b );
   
    friend void os_read( os_bstream& stream, LdasBinary& b );
#endif // HAVE_LIBOSPACE
};

   
#ifdef HAVE_LIBOSPACE    
//-----------------------------------------------------------------------
/// \brief Write to the stream.   
///      
/// This method writes an LdasBinary object to the stream. 
/// mPointer=0 if mBytes=0.
/// 
/// \param stream
///     A reference to the stream to write to.
/// \param b
///     A const reference to the LdasBinary object to write to the stream.
//-----------------------------------------------------------------------
void os_write( os_bstream& stream, const LdasBinary& b );
//-----------------------------------------------------------------------
/// \brief Read from the stream.   
///
/// This method reads an LdasBinary object from the stream. 
/// mPointer=0 if mBytes=0.
/// 
/// \param stream
///     A reference to the stream to write to.
/// \param b
///     A const reference to the LdasBinary object to write to the stream.
//-----------------------------------------------------------------------
void os_read( os_bstream& stream, LdasBinary& b );

OS_CLASS( LdasBinary )
OS_STREAM_OPERATORS( LdasBinary )
#endif // HAVE_LIBOSPACE

   
//-----------------------------------------------------------------------------
//   
//: ( Default ) Constructor.   
//   
inline LdasBinary::LdasBinary()
        : mPointer( 0 ), mBytes( 0 )
{
}

   
//-----------------------------------------------------------------------------
//   
//: getBytes method.   
//      
inline unsigned int LdasBinary::getBytes() const
{
    return mBytes;
}


//-----------------------------------------------------------------------------
//   
//: getPointer method.   
//      
inline void* LdasBinary::getPointer() const
{
    return static_cast<void *>(mPointer);
}

#endif // GeneralLdasBinaryHH
