#include "genericAPI/config.h"

#include "apiproxy.hh"

#include <sstream>   

using namespace Registry;
using namespace std;   


#if HAVE_LIBOSPACE
//-----------------------------------------------------------------------------
//
//: Constructor.
//
// Creates an ApiProxy object which stores the address of a reset * function.
//
//!param: const std::string& name - The name of the API.
//!param: void (*reset)() - The reset function for the API.
//!param: void (*save)(os_bstream&) - The save function for the API.
//!param: void (*restore)(os_bstream&,stringstream&) - The restore function for + 
//!param:     the API.
//
ApiProxy::ApiProxy(
    const std::string& name, void (*reset)(), void (*save)(os_bstream&),
    void (*restore)(os_bstream&,stringstream&) )
        : mName( name ), mReset( reset ), mSave( save ), mRestore( restore )
{
    apiStore.registerApi( this );
}


//-----------------------------------------------------------------------------
//
//: Reset an API
//
// Call the API's reset command.
//
void ApiProxy::reset()
{
    if ( mReset != 0 )
    {
        (*mReset)();
    }
}


//-----------------------------------------------------------------------------
//
//: Save an API
//
// Call the API's save command.
//
void ApiProxy::save( os_bstream& s )
{
    if ( mSave != 0 )
    {
        (*mSave)( s );
    }
}


//-----------------------------------------------------------------------------
//
//: Restore an API
//
// Call the API's restore command.
//
void ApiProxy::restore( os_bstream& s, stringstream& ss )
{
    if ( mRestore != 0 )
    {
        (*mRestore)( s, ss );
    }
}

#endif /* HAVE_LIBOSPACE */

