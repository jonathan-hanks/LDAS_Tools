namespace ILwd
{
  class LdasContainer;
  class LdasElement;
}

// Container contents pointer searches
bool contentsSearch( const ILwd::LdasContainer& c,
                     const ILwd::LdasElement* element );
