#ifndef GENERICAPI__TCL_CHANNEL_HH
#define	GENERICAPI__TCL_CHANNEL_HH

#include <tcl.h>

#include <string>

namespace GenericAPI
{
  class TclChannel
  {
  public:
    TclChannel( const std::string& Name );
    ~TclChannel();   

    Tcl_DriverCloseProc*  GetCloseProc( ) const;
    Tcl_DriverInputProc* GetInputProc( ) const;
    Tcl_DriverOutputProc* GetOutputProc( ) const;
    Tcl_DriverSeekProc* GetSeekProc( ) const;
    Tcl_DriverSetOptionProc* GetSetOptionProc( ) const;
    Tcl_DriverGetOptionProc* GetGetOptionProc( ) const;
    Tcl_DriverWatchProc* GetWatchProc( ) const;
    Tcl_DriverGetHandleProc* GetGetHandleProc( ) const;
    Tcl_DriverClose2Proc* GetClose2Proc( ) const;
    Tcl_DriverBlockModeProc* GetBlockModeProc( ) const;
    Tcl_DriverFlushProc* GetFlushProc( ) const;
    Tcl_DriverHandlerProc* GetHandlerProc( ) const;

    void SetCloseProc( Tcl_DriverCloseProc* Proc );
    void SetInputProc( Tcl_DriverInputProc* Proc );
    void SetOutputProc( Tcl_DriverOutputProc* Proc );
    void SetSeekProc( Tcl_DriverSeekProc* Proc );
    void SetSetOptionProc( Tcl_DriverSetOptionProc* Proc );
    void SetGetOptionProc( Tcl_DriverGetOptionProc* Proc );
    void SetWatchProc( Tcl_DriverWatchProc* Proc );
    void SetGetHandleProc( Tcl_DriverGetHandleProc* Proc );
    void SetClose2Proc( Tcl_DriverClose2Proc* Proc );
    void SetBlockModeProc( Tcl_DriverBlockModeProc* Proc );
    void SetFlushProc( Tcl_DriverFlushProc* Proc );
    void SetHandlerProc( Tcl_DriverHandlerProc* Proc );

    Tcl_ChannelType* GetChannelType( );

  private:
    Tcl_ChannelType	m_channel_type;
  }; // class TclChannel

#define PROC(a,b) \
  inline Tcl_Driver##a##Proc* TclChannel:: \
  Get##a##Proc( ) const \
  { \
    return m_channel_type.b##Proc; \
  } \
  inline void TclChannel:: \
  Set##a##Proc( Tcl_Driver##a##Proc* Proc ) \
  { \
    m_channel_type.b##Proc = Proc; \
  }

  PROC(Close,close)
  PROC(Input,input)
  PROC(Output,output)
  PROC(Seek,seek)
  PROC(SetOption,setOption)
  PROC(GetOption,getOption)
  PROC(Watch,watch)
  PROC(GetHandle,getHandle)
  PROC(Close2,close2)
  PROC(BlockMode,blockMode)
  PROC(Flush,flush)
  PROC(Handler,handler)

#undef PROC

  inline Tcl_ChannelType* TclChannel::
  GetChannelType( )
  {
    return &m_channel_type;
  }

} // namespace GenericAPI

#endif	/* GENERICAPI__TCL_CHANNEL_HH */
