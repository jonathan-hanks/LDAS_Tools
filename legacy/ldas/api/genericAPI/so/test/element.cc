#include "genericAPI/config.h"

#include <algorithm>
#include <memory>
#include <sstream>
#include <stdexcept>

#include "general/unittest.h"

#include "ilwd/ldasarray.hh"

#include "genericAPI/elementcmd.hh"
#include "genericAPI/genericcmd.hh"
#include "genericAPI/registry.hh"

General::UnitTest	Test;

void
rwr_test( )
{
  //---------------------------------------------------------------------
  // Generate test data
  //---------------------------------------------------------------------
  const std::string filename( "pass1.ilwd" );
  double data[10];
  const size_t	data_size = sizeof(data)/sizeof(*data);
  std::fill( &data[0], &data[ data_size ], 0.0 );

  ILwd::LdasElement*
    e( new ILwd::LdasArray< double >( data, data_size,
				      "sample", "count" ) );
  Registry::elementRegistry.registerObject( e );

  try {
    //-------------------------------------------------------------------
    // Write data to file
    //-------------------------------------------------------------------
    ILwdFile* out = openILwdFile( filename.c_str( ), "w" );
    writeElement( out, e, "binary", "gzip1" );
    closeILwdFile( out );
  }
  catch ( const SwigException& e )
  {
    Test.Check( false ) << "Read data from file: Caught SWIG exception:"
			<< " Result: "<< e.getResult( )
			<< " Info: "<< e.getInfo( )
			<< std::endl;
  }
  catch( const std::exception& e )
  {
    Test.Check( false ) << "Write data to file: Caught exception: " << e.what( ) << std::endl;
  }
  catch( ... )
  {
    Test.Check( false ) << "Write data to file: Caught unknown exception" << std::endl;
  }

  try {
    //-------------------------------------------------------------------
    // Read data from file
    //-------------------------------------------------------------------
    ILwdFile* in = openILwdFile( filename.c_str( ), "r" );
    std::string ptr = readElement( in );
    closeILwdFile( in );
  }
  catch ( const SwigException& e )
  {
    Test.Check( false ) << "Read data from file: Caught SWIG exception:"
			<< " Result: "<< e.getResult( )
			<< " Info: "<< e.getInfo( )
			<< std::endl;
  }
  catch( const std::exception& e )
  {
    Test.Check( false ) << "Read data from file: Caught exception: " << e.what( ) << std::endl;
  }
  catch( ... )
  {
    Test.Check( false ) << "Read data from file: Caught unknown exception" << std::endl;
  }

}

int
main( int ArgC, char** ArgV )
{
  //---------------------------------------------------------------------
  // Prepare the system tests
  //---------------------------------------------------------------------
  Test.Init( ArgC, ArgV );

  //---------------------------------------------------------------------
  // System tests
  //---------------------------------------------------------------------

  rwr_test( );

  //---------------------------------------------------------------------
  // Exit with the status of system tests
  //---------------------------------------------------------------------
  Test.Exit();
}
