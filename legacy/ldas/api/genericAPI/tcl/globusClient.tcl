## ******************************************************** 
## globusClient.tcl
##
## Provides globus client xio socket interface functions
## with gsi certification and authenication via hostcert and hostkey
##
## Provides functions for opening and closing globus sockets
## via tclglobus library interface to globus toolkit
##
## This is the module of the generic API that loads the
## genericAPI.so, since it is the functions declared here
## that use it.
##
## ******************************************************** 

;#barecode

package provide RawGlobusClient 1.0

if	{ [ regexp -nocase Tk [ package names ] ] } {
	set ::WISH 1
} else {
	set ::WISH 0
}

;## 
;## globus libraries loaded in LDAScntlmon.ini
;## namespace eval Globus 

namespace eval RawGlobusClient {
    set gridprogs [ list grid-proxy-info grid-proxy-init grid-proxy-destroy ]
    set globuslibs [ list globus_module \
			 globus_error \
			 globus_object \
			 globus_xio \
			 gssapi ]
    set waitforbytes 1
    set info ".info"
}

if { [ info exist ::GLOBUS_LOCATION ] } {
    set env(GLOBUS_LOCATION) $::GLOBUS_LOCATION
}

#------------------------------------------------------------------------
# From: Practical Programming in Tcl and Tk 4th Edition pp. 139
# Assign a set of variables from a alist of values.
# If there are more values than variables, they are returned.
# If there are fewer values than variables,
# the variables get the empty string
#------------------------------------------------------------------------

if	{ ![ string length [ info command lassign ] ] }  {

proc lassign {valueList args} {
    if { [llength $args] == 0} {
	error "wrong \# args: lassign list varname ?varname..?"
    }
    if { [llength $valueList] == 0} {
	# Ensure one trip through the foreach loop
	set valueList [list {}]
    }
    uplevel 1 [list foreach $args $valueList {break}]
    return [lrange $valueList [llength $args] end]
}

}

## ******************************************************** 
##
## Name: globusErrLog
##
## Description:
## skip logging of canceled operation errors
##
## Usage:
##
## Comments:

proc globusErrLog { msg } {
	if	{ ![ regexp -nocase -- {Operation was canceled|end of file} $msg ] } {
    	addLogEntry $msg red
    } 
}

## ******************************************************** 
##
## Name: RawGlobusClient data structures
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:
## is there a limit on buffer so need to read again

;## tcpdriver - tcp driver from globus_xio_driver_load
;## filedriver - file driver from globus_xio_driver_load
;## driver stack - driver stack from globus_xio_stack_push_driver 
;## tcphandle - tcp handle   from globus_xio_handle_create 
;## filehandle - file handle from globus_xio_handle_create 
;## buffer - to hold data for read/write operations
;## buffersize - number of bytes to read 
;## nbytes - number of bytes to write
;## contact_string - data passed during globus_xio_open 


## ******************************************************** 
##
## Name: RawGlobusClient::init
##
## Description:
## initialize to use the RawGlobusClient toolkit
##
## Usage:
##
## Comments:

proc RawGlobusClient::init { key type } {

    set seqpt ""
  
    if	{ [ catch {
    	if	{ ![ info exist ::GLOBUS_BUFFER_SIZE ] } {
        	set ::GLOBUS_BUFFER_SIZE 204800
        }
      	if	{ ![ regexp -nocase -- {tclglobus} [ info loaded ] ] } {
    		foreach lib $::RawGlobusClient::globuslibs {
    			load [ eval file join $::TCLGLOBUS_DIR libtcl${lib}.so ]
        	}
		}
    
    #----------------------------------------------------------------
	# Activate RawGlobusClient XIO module
	#----------------------------------------------------------------
    
    	if	{ ![ info exist ::RawGlobus::tcpdriver ] &&
        	  ![ info exist ::RawGlobusClient::tcpdriver ] } {
			set seqpt "globus_module_activate"
			globus_module_activate $::globus_i_xio_module
	
	#----------------------------------------------------------------
	# Load transport driver
	#----------------------------------------------------------------
			set seqpt "globus_xio_driver_load"
			lassign [ globus_xio_driver_load tcp ] result ::RawGlobus::tcpdriver
        	debugPuts "load tcp result $result"
    		lassign [ globus_xio_driver_load gsi ] result ::RawGlobus::gsidriver
        	debugPuts "load gsi result $result"
        }           	        
        set ::RawGlobusClient::tcpdriver $::RawGlobus::tcpdriver
        set ::RawGlobusClient::gsidriver $::RawGlobus::gsidriver
 
	    lassign [ globus_xio_stack_init NULL ] result ::RawGlobusClient::client_tcpstack($key)    
	    debugPuts "client init globus_xio_stack_push_driver result $result"
        
	    lassign [ globus_xio_stack_push_driver $::RawGlobusClient::client_tcpstack($key) \
        	$::RawGlobusClient::tcpdriver ] result
        debugPuts "push tcp driver result $result"
		lassign [ globus_xio_stack_push_driver $::RawGlobusClient::client_tcpstack($key) \
        	 $::RawGlobusClient::gsidriver ] result
        debugPuts "push gsi driver result $result"
    
    #----------------------------------------------------------------
	# Authenication -  Set GSI authorization mode
	#---------------------------------------------------------------- 
    
    # TCP & GSI driver specific attribute initialization
    
	    debugPuts "GSI driver specific attribute initialization"
	    lassign [ globus_xio_attr_init ] result ::RawGlobusClient::client_attr($key)
        
        ;## do not use IPV6 protocol
        lassign [ globus_xio_attr_cntl \
			      $::RawGlobusClient::client_attr($key) \
			      $::RawGlobusClient::tcpdriver \
			      $::GLOBUS_XIO_TCP_SET_NO_IPV6 \
			      $::GLOBUS_TRUE \
			     ] status
		debugPuts "GLOBUS_XIO_TCP_SET_NO_IPV6: status: $status"
        
        if	{ [ string equal host $type ] } {
    	# $gss_c_no_credential indicates a default credential 
	    	lassign [ globus_xio_attr_cntl \
			  $::RawGlobusClient::client_attr($key) \
			  $::RawGlobusClient::gsidriver \
			  $::GLOBUS_XIO_GSI_SET_CREDENTIAL \
			  $::gss_c_no_credential \
			 ] status
	    	debugPuts "via host cert GLOBUS_XIO_GSI_SET_CREDENTIAL status: $status"
		} else {
	    # Authentication via user cert - Set GSI authorization mode
	   	 	lassign [ globus_xio_attr_cntl \
			  $::RawGlobusClient::client_attr($key) \
			  $::RawGlobusClient::gsidriver \
			  $::GLOBUS_XIO_GSI_SET_AUTHORIZATION_MODE \
			  $::GLOBUS_XIO_GSI_HOST_AUTHORIZATION \
			 ] status 
	    	debugPuts "GLOBUS_XIO_GSI_SET_AUTHORIZATION_MODE status: $status"
       	}
	    debugPuts "GSI driver specific attribute initialization $::RawGlobusClient::client_attr($key)"
    	
    } err ] } {
    	addLogEntry "$seqpt $err" blue
    	return -code error "$seqpt: $err"
    }

}

## ******************************************************** 
##
## Name: RawGlobusClient::close
##
## Description:
## initialize to use the RawGlobusClient toolkit
##
## Usage:
##
## Comments:

proc RawGlobusClient::close { key } {

    if	{ [ catch {
    	if	{ [ info exist ::RawGlobusClient::networkhandle($key) ] } {    
			;## remove outstanding callback
        	lassign [ globus_xio_handle_cancel_operations \
        		$::RawGlobusClient::networkhandle($key) $::GLOBUS_XIO_CANCEL_READ ] result 
        	#debugPuts "cancel operation read result $result"
        
        	lassign [ globus_xio_handle_cancel_operations \
        		$::RawGlobusClient::networkhandle($key) $::GLOBUS_XIO_CANCEL_WRITE ] result 
        	#debugPuts "cancel operation write result $result"       
        
        	lassign [ globus_xio_close $::RawGlobusClient::networkhandle($key) NULL ] result
            #debugPuts "close operation result $result"
            
            if	{ [ info exist ::RawGlobusClient::client_attr($key) ] } {
    			lassign [ globus_xio_attr_destroy $::RawGlobusClient::client_attr($key) ] status
    			debugPuts "destroy attr status $status"
        		unset ::RawGlobusClient::client_attr($key)
    		}
    
    		if	{ [ info exist ::RawGlobusClient::client_tcpstack($key) ] } {
    			lassign [ globus_xio_stack_destroy $::RawGlobusClient::client_tcpstack($key) ] status
    			# debugPuts "destroy stack status $status"
        		unset ::RawGlobusClient::client_tcpstack($key)
        	}            
        	unset ::RawGlobusClient::networkhandle($key)
        }
    } err ] } {
    	addLogEntry $err 2
    	return -code error $err
    }
}

## ******************************************************** 
##
## Name: RawGlobusClient::closeCallback
##
## Description:
## initialize to use the RawGlobusClient toolkit
##
## Usage:
##
## Comments:

proc RawGlobusClient::closeCallback { user_parms handle result } {
	
    if	{ [ catch {
    	debugPuts "closeCallback user_parms $user_parms handle $handle result $result"
    	set key [ lindex $user_parms 0 ]
    	set ::RawGlobusClient::done($key) 1
    
    	if	{ [ info exist ::RawGlobusClient::client_attr($key) ] } {
    		lassign [ globus_xio_attr_destroy $::RawGlobusClient::client_attr($key) ] status
    		debugPuts "destroy attr status $status"
        	unset ::RawGlobusClient::client_attr($key)
    	}
    
    	if	{ [ info exist ::RawGlobusClient::client_tcpstack($key) ] } {
    		lassign [ globus_xio_stack_destroy $::RawGlobusClient::client_tcpstack($key) ] status
    		debugPuts "destroy stack status $status"
        	unset ::RawGlobusClient::client_tcpstack($key)
        }
        set ::RawGlobusClient::closeCallbackDone($key) ok
    } err ] } {
    	addLogEntry $err 2
    	set ::RawGlobusClient::closeCallbackDone($key) $err
    }
}

## ******************************************************** 
##
## Name: RawGlobusClient::cleanup
##
## Description:
## initialize to use the RawGlobusClient toolkit
##
## Usage:
##
## Comments:
## not needed if there is a server already

proc RawGlobusClient::cleanup {} {

	#----------------------------------------------------------------
	# Unload both drivers
	#----------------------------------------------------------------
    
    if	{ [ info exist ::RawGlobusClient::gsidriver ] } {
    	lassign [ globus_xio_driver_unload $::RawGlobusClient::gsidriver ] status
    	debugPuts "globus_xio_driver_unload gsi status $status"
        unset ::RawGlobusClient::gsidriver
    }
	
    if	{ [ info exist ::RawGlobusClient::tcpdriver ] } {
		lassign [ globus_xio_driver_unload $::RawGlobusClient::tcpdriver ] status
    	debugPuts "globus_xio_driver_unload tcp status $status"
        unset ::RawGlobusClient::tcpdriver
    }
 
	#----------------------------------------------------------------
	# Deactivate RawGlobusClient XIO module
	#----------------------------------------------------------------
    
    if	{ [ info exist ::globus_i_xio_module ] } {
		lassign [ globus_module_deactivate $::globus_i_xio_module ] status 
    	debugPuts "globus_module_deactivate status $status, ::globus_i_xio_module [ info exist ::globus_i_xio_module ]"
   	}
}

## ******************************************************** 
##
## Name: sendCmd
##
## Description:
## issue a command, generally to operator socket of cntlmonAPI
## cmd : ldasjob cmd or something to send
## key - use to identify this handle 
##
## Usage:
##
## Comments:
## waits for send to complete, avoids vwait

proc RawGlobusClient::sendCmd { cmd key } {

    ;## write cmd to server
    if  { [ catch {
    	catch { unset ::RawGlobusClient::sendCmdDone($key) }
    	if	{ [ string length $cmd ] } {
        
        	#lassign [ globus_xio_register_write $::RawGlobusClient::networkhandle($key) \
		    #$cmd \
		    #$::RawGlobusClient::waitforbytes \
		    #NULL \
		    #[ list RawGlobusClient::sendCmdCallback $key ] ] result 

			lassign [ globus_xio_write $::RawGlobusClient::networkhandle($key) \
		    $cmd \
		    $::RawGlobusClient::waitforbytes \
		    NULL ] result
            # debugPuts "sendCmd done key $key  result $result"
            
            lassign [ globus_xio_register_read $::RawGlobusClient::networkhandle($key) $::GLOBUS_BUFFER_SIZE \
    	 	$::RawGlobusClient::waitforbytes NULL \
    		[ list RawGlobusClient::readCallback $key ] ] result
        	# debugPuts "register read callback result $result" 
        
        } 
    } err ] } {
		return -code error $err
    }  
}

## ******************************************************** 
##
## Name: sendCmdCallback
##
## Description:
## send completed, registers a read unless user_parms is 0
##
## Usage:
##
## Comments:

proc RawGlobusClient::sendCmdCallback { user_parms handle result buffer data_desc } {
    
    if	{ [ catch {
    	debugPuts "sendCmd callback $user_parms handle $handle result $result buffer $buffer data_desc $data_desc"
    	;## get ready to read data from server        
    	if	{ [ string length $result ] } {
    		error $result
        }      
        set ::RawGlobusClient::sendCmdDone($user_parms) ok  
   	} err ] } {
        addLogEntry $err 2
    }      
}	

## ******************************************************** 
##
## Name: readDataCallback
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:
## a callback is registered each time to listen to server data
## may return "An end of file occurred" from result if there is nothing
## pass user_parm as an array
## user_parms is an array containing callback and args for callback
## buffer read in from socket is appended to args

proc RawGlobusClient::readCallback { user_parms handle result buffer data_desc } {

	if	{ [ catch {
    	set color red

		debugPuts "readCallback read callback $user_parms handle $handle result  \
			$result buffer [ string length $buffer ] bytes\n$buffer"

		set key [ lindex $user_parms 0 ]
        if	{ [ regexp -nocase {end of file} $result ] } {
    		set color blue 
            # RawGlobusClient::close $key
            error $result
		}
       	if	{ [ string length $result ] } {
        	# RawGlobusClient::close $key
        	error $result
       	}
        
        set data  [ set ::${key}(callback) ]
        set callback [ lindex $data 0 ]
        set args [ lrange $data 1 end ]
        lappend args $buffer 
        eval $callback $args
     
        lassign [ globus_xio_register_read $handle $::GLOBUS_BUFFER_SIZE \
    	 $::RawGlobusClient::waitforbytes NULL \
    	 [ list RawGlobusClient::readCallback $key ] ] result
		debugPuts "register another read callback result $result"
        
    } err ] } { 
    	globusErrLog $err
        RawGlobusClient::close $key
        if	{ [ info exist ::${key}(done) ] } {
        	set [ set ::${key}(done) ] 1
        }
        catch { unset ::$key }
        
    }

}

## ******************************************************** 
##
## Name: openCallback
##
## Description:
## read data received from socket
##
## Usage:
##
## Comments:

proc RawGlobusClient::openCallback { user_parms handle result } {

	if	{ [ catch {
    	debugPuts "open callback, user_parms $user_parms handle $handle result $result"
		if	{ [ string length $result ] } {
        	error $result
    	}  
   } err  ] } {
   		globusErrLog $err
        RawGlobusClient::close $user_parms
        set ::RawGlobusClient::openCallbackDone($user_parms) $err
   }
   set ::RawGlobusClient::openCallbackDone($user_parms) ok
}

## ******************************************************** 
##
## Name: RawGlobusClient::connect
##
## Description:
## enter state1 by opening socket to server via RawGlobusClient XIO 
##
## Usage:
##
## Comments:

proc RawGlobusClient::connect { contact_string key } {
    
	if	{ [ catch {	
        
        lassign [ globus_xio_handle_create $::RawGlobusClient::client_tcpstack($key) ] result \
                    	::RawGlobusClient::networkhandle($key)
                    
        # debugPuts "handle create result $result $::RawGlobusClient::networkhandle($key) result $result"
                    
        ;## use blocking IO   
        lassign [ globus_xio_open $::RawGlobusClient::networkhandle($key) \
         	$contact_string $::RawGlobusClient::client_attr($key) ] result

	} err ] } {	
    	debugPuts "closing handle $::RawGlobusClient::networkhandle($key)" 
    	catch {	RawGlobusClient::close $::RawGlobusClient::networkhandle($key) } 
        return -code error $err
	}
}

## ******************************************************** 
##
## Name: initProxy
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc RawGlobusClient::initProxy {} {

	set data ""
	if	{ [ string length $::passphrase ] } {
		set fd [ ::open $::Globus::info w ]
        puts $fd $::passphrase
        ::close $fd 
        set cmd "exec grid-proxy-init -debug -verify -pwstdin < $::RawGlobusClient::info"
        catch { eval $cmd } data
        debugPuts "grid-proxy-init data '$data'"
        file delete -force $::Globus::info
    	if	{ [ regexp -nocase {ERROR:} $data ] } {
        	return -code error $data
        } else {
        	return $data
        }
    } else {
    	return -code error "No pass phrase was entered to create a valid proxy."
    }
}

## ******************************************************** 
##
## Name: initProxyDialog
##
## Description:
## dialog to get passphrase to allow user to initialize a new proxy
##
## Usage:
##
## Comments:

if	{ $::WISH } {

proc RawGlobusClient::initProxyDialog {} {      

	if	{ [ catch {
    	set ::passphrase ""
        catch { unset ::doinitProxy }
		set dlg [ Dialog $::wproxyDialog -parent . -modal local \
          	-separator 1 -title   "Enter passphrase for your proxy: " \
        	-side bottom -default 0 -cancel 2 ]
        set dlgframe [$dlg getframe]
        
        $dlg add -name 0 -text ACCEPT -command "set ::doinitProxy 1; destroy $dlg"   
        $dlg add -name cancel -command "destroy $dlg" 
        set passwidget  [ LabelEntry $dlgframe.passw -label "Your pass phrase: " -labelwidth 20 -labelanchor e \
                   -textvariable ::passphrase -editable 1 -width 50 -labeljustify right \
                   -helptext "The passphrase associated with your proxy" -show *]
        pack $passwidget -side top -fill x -expand 1
        $dlg draw
        if	{ [ info exist ::RawGlobusClient::doinitProxy ] } {
        	set rc [ initProxy ]
        } 
    } err ] } {
    	return -code error $err
    }
}

} else {

;## done with tclsh
;## must run in foreground to enter passphrase

proc RawGlobusClient::initProxyDialog {} {
	
    if	{ [ catch {
    	catch { exec /bin/stty -echo }
    	puts -nonewline "Enter passphrase for your proxy: " 
        flush stdout 
        set ::passphrase [ gets stdin ]
        catch { exec /bin/stty echo }
 		puts $::passphrase
        set ::::RawGlobusClient::doinitProxy 1
        initProxy
    } err ] } {
    	return -code error $err
    }
}


}
        
## ******************************************************** 
##
## Name: getProxyInfo
##
## Description:
## check on user proxy
##
## Usage:
##
## Comments:

proc RawGlobusClient::getProxyInfo {} {

	if	{ [ catch {
    	set rc 0
    	catch { exec grid-proxy-info -identity -timeleft } data
        debugPuts "grid-proxy-info $data"
        if	{ [ regexp {ERROR.+find a valid proxy} $data ] } {
        	set rc 1
        }
        if	{ [ regexp {\n-1} $data ] } {
            set data "Your proxy has expired"
            puts "data expired"
            set rc 1
        }
 	} err ] } {
    	return -code error $err
    }
    return [ list $rc $data ]
}
            
        
## ******************************************************** 
##
## Name: verifyProxy
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:

proc RawGlobusClient::verifyProxy {} {

    set msg ""
    if	{ [ catch { 
		foreach prog $::RawGlobusClient::gridprogs {
    		set progpath [ auto_execok $prog ]
			if	{ ![ string length $progpath ] } {
				return -code error  "$prog not found; unable to verify"
    		}
		}
		
        foreach { rc data } [ getProxyInfo ] { break }
        debugPuts "rc $rc data $data"
    	if	{ $rc } {
        	set rc [ initProxyDialog ]
            if	{ [ info exist ::RawGlobusClient::doinitProxy ] } {
            	foreach { rc data } [ getProxyInfo ] { break }
                debugPuts "from getProxyInfo2  rc=$rc, data =$data"
                if	{ $rc } {
                	error $data 
                }
            }
        } else {
        	set ::RawGlobusClient::doinitProxy 1
        }
        
        ;## if user has not cancel, check the time left on proxy		
        if	{ [ info exist ::doinitProxy ] } {
			if	{ [ regexp  {CN=([^\d]+)[^\n]+\n(\d+)} $data -> username timeleft] } {
				regsub -all {\s+} $username "_" username
        		set ::user [ string trim $username _ ]
        		debugPuts "user $::user, timeleft $timeleft"
			}

    		if	{ ![ string length $timeleft ] } {
				error "grid-proxy-info failed for -timeleft option."
			} elseif { $timeleft > 60 } {
        		set walltime [ clock format [ expr $timeleft + [ clock seconds ] ] -format "%m-%d-%Y %H:%M:%S" ]
    			set msg "This proxy is valid until $walltime."
    		}
            unset ::RawGlobusClient::doinitProxy
        } else {
        	set ::user cancelled
        }
   	} err ] } {
    	return -code error $err 
    }
    debugPuts $msg
    return $::user 
}
