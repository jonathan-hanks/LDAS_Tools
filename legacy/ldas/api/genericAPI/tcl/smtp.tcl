## ******************************************************** 
##
## Name: smtp.tcl version 1.0
##
## Description:
## Simple Tcl smtp client library.
##
## Parameters:
##
## Usage:
##
##   smtp::sendMail JUNK from "to1, to2" "subject" text/filename
##
## Comments:
## "JUNK" is a placeholder for the deprecated option
## "mailhost"
##
## ******************************************************** 

;#barecode
package provide smtp 1.0
namespace eval smtp {}
;#end

## ******************************************************** 
##
## Name: mailTo
##
## Description:
## Non-exec'ing version of mailTo from genericAPI.tcl
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc mailTo { { to "" } { subject "" } { body "" } { attach "" } } {
     
     if { [ string equal $to $subject ] } {
        return -code error "mailTo: missing required arguments"
     }
      
     ;## if the email address provided was actually a host:port
     if { [ regexp {^([^:]+):(\d+)$} $to -> host port ] } {
        portMsg $host $port $subject $body
        return {}
     }
     
     ;## should we use martian mailing via the manager emergency
     ;## port?
     set localhost [ set ::${::API}(host) ]
     set gateway   $::manager(host) 
     if { ! [ string equal $localhost $gateway ] } {
        if { [ catch {
           martianMail $to $subject $body $attach
        } err ] } {
           return -code error "[ myName ]: $err"
        }
        return {}
     }   
     
     set user $::env(USER)
     set domain localhost
     set from [ smtp::ldasUser $user $domain ]
     
     if { [ catch {
        smtp::sendMail $from $to $subject $body $attach
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     
     return {}
}

## ******************************************************** 

## ******************************************************** 
##
## Name: martianMail
##
## Description:
## Handle mail coming from API's on machines in the martian
## network which are not running sendmail by relaying through
## the manager API.
##
## Parameters:
##
## Usage:
##
## Comments:
## 

proc martianMail { { to "" } { subject "" } { body "" } { attach "" } } {
     set localhost [ set ::${::API}(host) ]
     set gateway   $::manager(host) 
     if { [ catch {
        if { ! [ string equal $localhost $gateway ] } {
           set sid [ sock::open manager emergency ]
           puts $sid "$::MGRKEY NULL NULL mailTo $to $subject $body"
           ::close $sid
        } else {
           mailTo $to $subject $body
        }
     } err ] } {
        catch { close $sid }
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: portMsg
##
## Description:
## Sends job result string directly to a users listening
## port.  Does absolutely no error checking.  If it fails,
## it fails.  The users listener must only read and
## disconnect.
##
## Parameters:
##
## Usage:
##
## Comments:
## Only ever called by mailTo

proc portMsg { host port subject body { sid "" } } {
     if { [ catch {
        set badclient 0
        
        if { ! [ string length $sid ] } {
           if { ! [ regexp -- $::RUNCODE\\d+ $subject jobid ] } {
              set jobid NULL
           }
           ;## PR #2749 persistent socket test
           set sid [ portMsgPersistentSocket $jobid $host $port ]
           set i 0
        } else {
           foreach [ list i sid ] [ split $sid , ] { break }
        }
        
        if { [ catch { fconfigure $sid -peername } ] } {
           if { $i < 25 } {
              incr i
              set delay [ expr { $i * 250 } ]
           } else {
              set msg    "failed connect to $host port $port "
              append msg "could not send user message with subject "
              append msg "'$subject'"
              set badclient 1 
              return -code error $msg
           }
           after $delay [ list portMsg $host $port $subject $body $i,$sid ]
        } else {
           fconfigure $sid -blocking off
           puts $sid "Subject:\n$subject\n$body"
           if	{ [ regexp {gtxiosock} $sid ] } {
           		addLogEntry "closing $sid" purple
           }
           close $sid
        }   
     } err ] } {     	
        catch { close $sid } err1
        if	{ [ regexp {gtxiosock} $sid ] } {
           	addLogEntry "Error $err, closed $sid '$err1'" purple
        }
        if { $::DEBUG } {
           if { [ regexp -nocase {refused} $err ] } {
              set err "remote client disconnected!"
           }
           addLogEntry "$err (host: '$host' port: '$port')"
        }
        if { $badclient == 1 } {
           addLogEntry $err orange
        }
     }   
     return {}
}
## ******************************************************** 

## ******************************************************** 
##
## Name: portMsgPersistentSocket
##
## Description:
## PR #2749 persistent socket condition detector.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc portMsgPersistentSocket { jobid host port } {
     
     if { [ catch {
        if { [ info exists ::${jobid}::persistentsocket ] } {
           set sid [ set ::${jobid}::persistentsocket ]
           ::unset ::${jobid}::persistentsocket
           if { [ string equal not_persistent $sid ] } {
              set sid [ socket -async $host $port ]
           } elseif { [ catch {
              set sfo [ fconfigure $sid -peername ]
           } err ] } {
              set sid [ socket -async $host $port ]
              if { [ info exists ::DEBUG_PERSISTENT_SOCKET ] && \
                   [ string equal 1 $::DEBUG_PERSISTENT_SOCKET ] } {
                 set msg "persistent socket condition detected, "
                 append msg "but socket not open. assuming "
                 append msg "connection based protocol."
                 addLogEntry $msg blue
              }   
           } else {
              set sfo [ lindex $sfo 0 ][ lindex $sfo 2 ]
              if { [ info exists ::DEBUG_PERSISTENT_SOCKET ] && \
                   [ string equal 1 $::DEBUG_PERSISTENT_SOCKET ] } {
                 set msg "host: '$host' port: '$port' sfo: '$sfo'"
                 addLogEntry $msg blue
              }     
              if { ! [ string equal $sfo $host$port ] } {
                 set sid [ socket -async $host $port ]
              }
           }
        } else {
           set sid [ socket -async $host $port ]
        }

     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $sid
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::ldasUser
##
## Description:
## Augment the ldas email username with the full string
## username as in the password file.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::ldasUser { user domain } {
     
     if { [ catch {
        set passfile /etc/passwd
        if { [ info exists ::${user}_smtp_full_username ] } {
           set fullname [ set ::${user}_smtp_full_username ]
           if { [ string length $fullname ] } {
              set user "\"$fullname\" <${user}@$domain>"
           }
        } elseif { [ file readable $passfile ] } {
           set data [ dumpFile $passfile ]
           foreach line [ split $data "\n" ] {
              set line [ split $line : ]
              if { [ string equal $user [ lindex $line 0 ] ] } {
                 set fullname [ lindex $line 4 ]
                 set ::${user}_smtp_full_username $fullname
                 if { [ string length $fullname ] } {
                    set user "\"$fullname\" <${user}@$domain>"
                 }
                 break
              }
           }
        } else {
           set ::${user}_smtp_full_username [ list ]
           set user ${user}@$domain
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $user
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::sendMail
##
## Description:
## Send mail via Tcl.  Note the 'HELO localhost' call,
## which seems to satisfy far more mail relays than it
## ever should!
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::sendMail { from to subject text { attachments "" } } {
     if { [ catch {
        set seqpt {}
        set from [ smtp::validate $from ]
        set to   [ smtp::validate $to   ]
        set from_rx {^\{?\"?([^\"]+)\"?\s+<([^>]+)>\}?$}
        if { [ regexp $from_rx $from -> name address ] } {
           set from "\"$name\" <$address>"
        } else {
           set from [ string trim $from >< ]
           set from "<$from>"
        }
        
        set msg    "From: LDAS $from\n"
        append msg "To: [ join $to , ]\n"
        append msg "Subject: $subject\n"

	   set text [ smtp::multipart $text $attachments ]
    
        append msg $text

        smtp::useSendmail $msg
    } err ] } {
       catch { ::close $sid }
       return -code error "[ myName ]:$seqpt $err"
    }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::useSendmail 
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::useSendmail { mail } {
     
     if { [ catch {
        
        ;## if this resource variable is defined, use it!
        if { [ info exists ::PATH_TO_SENDMAIL ] } {
           set sendmail $::PATH_TO_SENDMAIL
        } else {
           set sendmail /usr/lib/sendmail
        }
        
        set pipe [ open "|$sendmail -oi -t" a+ ]
        fconfigure $pipe -blocking off
        fconfigure $pipe -buffering line
        puts $pipe $mail
        ::close $pipe
     
     } err ] } {
        catch { ::close $pipe }
        return -code error "[ myName ]: $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::validate
##
## Description:
## Validate email addresses. Only assumes that multiple
## addresses are separated by commas.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::validate { to } {
     set rcpts [ list ]
     set to [ string trim $to ]
     set to [ split $to , ]
	regsub -all {\s+} $to { } to 
     foreach rcpt $to {
	   set rcpt [ string trimright $rcpt " >" ]
	   set rcpt [ lindex $rcpt end ]
        regsub -all {[<>]} $rcpt {} rcpt
	   set rcpt [ string trim $rcpt ]
        if { ! [ regexp {^\S+@\S+$} $rcpt ] } {
           return -code error "invalid address: '$rcpt'"
        }
        lappend rcpts $rcpt
     }
     return $rcpts
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::expand
##
## Description:
## If the text submitted was a filename, read the file
## and ship it off.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::expand { text } {
     if { [ catch { 
        if { [ file exists   $text ] && \
	        [ file readable $text ] } {
           set fid [ open $text r ]
           set text [ read $fid [ file size $text ] ]
           ::close $fid
        }
        append text [ smtp::sig ]
     } err ] } {
        catch { ::close $fid }
        return -code error $err
     }
     return $text
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::sig
##
## Description:
## Signature file handling, if there is a signature file.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::sig { { fname .signature } } {
     set signature [ list ]
     if { [ file exists   $fname ] && \
	     [ file readable $fname ] } {
        set fid [ open    $fname r ]
        set signature \
           [ read $fid [ file size $fname ] ]
        ::close $fid
        set signature "\n\n--\n$signature" 
    }
     return $signature
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::dump
##
## Description:
## Very expensive to run!!
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::dump { attachment } {
     
     if { [ catch {
        set data [ dumpFile $attachment ]
	   set data [ base64::encode64 $data ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
	return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: smtp::multipart
##
## Description:
## RFC #822 and MIME
## Parameters:
##
## Usage:
##
## Comments:
##

proc smtp::multipart { text { attachments "" } } {
     
     if { [ catch {
	   set attachments [ split $attachments , ]
	   
	   set boundary LIGO_LDAS_attachment_LDAS_LIGO
        
        set text [ smtp::expand $text ]
	   
	   if { [ string length $attachments ] } {
	   
	      set    msg "MIME-Version: 1.0\n"
	      append msg "Content-Type: multipart/mixed;\n"
	      append msg " boundary=\"$boundary\"\n\n"
	      append msg "--$boundary\n"
	      append msg "Content-Type: text/plain; charset=US-ASCII\n\n"
	      append msg "$text\n"
	      foreach attachment $attachments {
	         set attachment [ string trim $attachment ]
		    if { ! [ file readable $attachment ] } {
		       set err "attachment not found: '$attachment'"
		       return -code error $err
		    }
	         append msg "--$boundary\n"
	         append msg "Content-Type: application/octet-stream\n"
		    append msg "Content-Transfer-Encoding: base64\n"
		    append msg "Content-Disposition: attachment;\n"
		    append msg " filename=\"[ file tail $attachment ]\"\n\n"
		    append msg [ smtp::dump $attachment ]
	      }
	      append msg "\n\n--${boundary}--\n\n"
        } else {
	      set msg "\n$text"
	   }
	} err ] } {
        return -code error "[ myName ]: $err"
     }
	return $msg
}
## ******************************************************** 
