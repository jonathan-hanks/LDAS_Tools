## ******************************************************** 
## ilwd.tcl version 1.0
##
## Provides Internal Light Weight Data parsing and creation
## routines.
##
##
## Release Date: 99.04.07.
##
## ******************************************************** 

;#barecode
package provide ilwd 1.0

namespace eval ilwd {
    
     set nodebug        0
     set containerlevel 0
      set esc {
              &  \\&amp\;
              <  \\&lt\;
              >  \\&gt\;
              \" \\&quot\;
              \' \\&apos\;
              �  \\&sect\;
              } 
     set tags {
              CHAR CHAR_S CHAR_U
              INT_2S INT_2U INT_4S INT_4U INT_8S INT_8U
              REAL_4 REAL_8
              COMPLEX_8 COMPLEX_16
              ILWD
              EXTERNAL
              LSTRING
              }
    set attrs {
              name    compression
              ndim    byteorder
              dims    comment
              units   format
              mdorder bytes
              size
              }

}

;#barecode
namespace eval rx {
     
     set tags  ([ join $::ilwd::tags | ])
     
     set attrs ([ join $::ilwd::attrs | ])

     ;## the base 64 character class
     set b64_charset   {[a-zA-Z0-9\+\/ \]+}
     
     ;## a start tag (<tag opt1="foo" opt2="bar">)
     set start {<([a-zA-Z0-9\_]+)([^>]+)?>}
     
     ;## everything 
     set data  (.+)

     ;## external
     set external <EXTERNAL>(.+)</EXTERNAL>
    
     ;## container
     set container {<ILWD[^>]*>(.+)</ILWD>}
          
     ;## everything in a container up to the next tag
     set container_data {([^<]+)}
     
     ;## an end tag (</tag>)
     set end   {<(\/[a-zA-Z0-9\_]+)>}

     ;## regex of an attribute (key="value")
     set attr  {([a-zA-Z0-9]+)=['\"]([^'\"]+)['\"]}
     
     ;## an html comment (!--some stuff here-->)
     set com   {<!--([^>]+)-->}
     
     ;## a dcd (document class descriptor)
     ;## (<!FOO BAR!> or <?FOO BAR?>)
     set dcd   {^<[\!\?]([^>]+)>}

     ;## this will match the "next" struct in an ilwd
     set nextstruct ($external|$container|$start$container_data$end)
}

## ******************************************************** 
##
## Name: ilwd::switch
##
## Description:
## Callbacks can be associated with each tag and this
## function can be used to call them.
##
## Usage:
##
## Comments:
## Does nothing at present.  This is where you would
## implement a browser.

proc ilwd::switch { { tag "" } } {
     switch -exact -- $tag {
              CHAR {
                   }
             /CHAR {
                   }
            CHAR_S {
                   }
           /CHAR_S {
                   }
            CHAR_U {
                   }
           /CHAR_U {
                   }
            INT_2S {
                   }
           /INT_2S {
                   }
            INT_4S {
                   }
           /INT_4S {
                   }
            INT_4U {
                   }
           /INT_4U {
                   }
            INT_8S {
                   }
           /INT_8S {
                   }
            INT_8U {
                   }
           /INT_8U {
                   }
            REAL_4 {
                   }
           /REAL_4 {
                   }
            REAL_8 {
                   }
           /REAL_8 {
                   }
         COMPLEX_8 {
                   }
        /COMPLEX_8 {
                   }
        COMPLEX_16 {
                   }
       /COMPLEX_16 {
                   }
              ILWD {
                   }
             /ILWD {
                   }
          EXTERNAL {
                   }
         /EXTERNAL {
                   }
           LSTRING {
                   }
          /LSTRING {
                   }    
           default {
                   return {}
                   }
         } ;## end of switch          
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::parse 
##
## Description:
## Parse the ILWD into a structure.  See the comment
## section of parseILWD.
##
## Usage:
##        called by parseILWD
##
## Comments:
## The regex patterns are NOT strict.

proc ilwd::parse { { data "" } } {
     
     if { [ regsub -all $rx::start $data "" tmp ] > 1 } {
        regexp -nocase ^$rx::container$ $data all dat 
        return [ ilwd::containerHandler $dat ]
        }
     
    if { [ regexp -nocase ^$rx::external$ $data all dat ] } { 
        set dat [ string trim $dat ]
        return "external \"\" \"$dat\""
        }    
     
     if { ! [ regexp "$rx::start$rx::data$rx::end" \
                      $data -> start attr dat end ] } {
        return {}
        }
            
     if { ! [ regexp $start $end ] } {
        set msg    "ILWD Parser:\n"
        append msg "start tag: \"$start\"\n"
        append msg "and end tag: \"$end\"\n"
        append msg "do not match.  Aborting."
        return -code error $msg
        }
       
     if { ! [ regexp -nocase $rx::tags $start ] } {
        set msg    "ILWD Parser:\n"
        append msg "start tag: \"$start\"\n"
        append msg "is not an ILWD tag. Aborting."
        return -code error $msg
        }
                
     set start [ string trim $start ]
     set attr  [ string trim $attr  ]
     set dat   [ string trim $dat   ]
     regsub -all "\"" $attr "\\\"" attr

     return [ list $start $attr $dat ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::containerHandler 
##
## Description:
## Deals with the pathological container class.  Called
## internally by ilwd::parse.
##
## Usage:
##
## Comments:
## Recursive.  If the code is modified, and the rx::container
## is removed from the regex, the nesting level counter "i"
## will break.

proc ilwd::containerHandler { { data "" } } {
     incr   ::ilwd::containerlevel 1
     set i $::ilwd::containerlevel
     set j 0
     while { [ regexp -nocase $rx::nextstruct $data match ] } {
           set contents($j) $match
           ;## some rare values have parens.
           regsub -all "\[\)\]" $match "\\\)" match
           regsub -all "\[\(\]" $match "\\\(" match
           ;## remove the matched part of the input
           set x [ string first $match $data ]
           set y [ expr $x + [ string length $match ] ]
           set data "[ string range $data 0 $x ][ string range $data $y end ]"
           incr j 1
           }
     unset j
     foreach j [ lsort [ array names contents ] ] {
          set contents($i,$j) [ ilwd::parse $contents($j) ]
          unset contents($j)
          }
     return [ array get contents ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::parseattr 
##
## Description:
## Converts a list of attributes parsed from a start tag
## into a Tcl list of attribute/value pairs and returns
## the list.
##
## Usage:
##
## Comments:
## This proc is NOT called by the parseILWD routine,
## it is here for convenience to read the attribute lists
## returned by ilwd::parse.

proc ilwd::parseattr { { data "" } } {
     if { ! [ string length $data ] } {
        return {}
        }
     set attrlist [ list ]
     ;## preprocess
     regsub -all " ="  $data "="  data
     regsub -all "= "  $data "="  data
     regsub -all "'"   $data "\"" data
     regsub -all "\" " $data "\"" data
     regsub -all " \"" $data "\"" data
     while { [ regexp $rx::attr $data all attr value ] } {
           lappend attrlist $attr $value
           ;## some rare values have parens.
           regsub -all "\[\)\]" $value "\\\)" value
           regsub -all "\[\(\]" $value "\\\(" value
           regsub "$attr=\"$value\"" $data "" data
           }
     return $attrlist
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::preprocess 
##
## Description:
## Rationalises whitespace in input.
##
## Usage:
##        set data [ ilwd::preprocess $data ]
##
## Comments:
## Might damage externals that contain formatted content.

proc ilwd::preprocess { { data "" } } {
     
     regsub -all "\n"         $data {}  data   
     regsub -all "\[ \t\r\]+" $data " " data
     regsub -all "< "         $data "<" data
     regsub -all " >"         $data ">" data
     regsub -all "/ "         $data "/" data
     regsub -nocase -- $rx::dcd   $data {}  data

     ;## comments are not in ilwd, but
     ;## might be embedded in "external"
     ;## regsub -all $rx::com $data "" data
     
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: parseILWD 
##
## Description:
## Parses an ILWD object and returns a description of it.
##
## Usage:
##       array set tree [ parseILWD $ilwd_string ]
##       foreach branch [ lsort -dictionary [ array names tree ] ] {
##            puts "$branch --> $tree($branch)"
##            }
##
## Comments:
## When a container contains a container, a branch
## branches, so branches need to be "array set" when
## their llength exceeds 2, so when you parse the output
## you will need a recursive handler.  Some time soon
## Phil will give an example of one in the usage block.

proc parseILWD { { ilwd "" } } {
     ;## handle a filename as input
     if { [ file exists $ilwd ] } {
        set fid [ open $ilwd r ]
        set ilwd [ read $fid ]
        close $fid
        }
     set ::ilwd::containerlevel 0
     ;## really crude, rapid, effective garbage rejection.
     if { ! [ regexp {^<.+>$} $ilwd ] } {
        return -code error "[ myName ]: garbage received, Aborting."
        }
     set ilwd [ ilwd::preprocess $ilwd ]
     return [ ilwd::parse $ilwd ]
}
## ******************************************************** 



## ******************************************************** 
##
## Name: ilwd::str2ilwd
##
## Description:
## convert bare strings of various types into ilwd text.
##
## Parameters:
## takes a string and formats it into ilwd text
## dims are separated by "\," in LSTRING
##
## Usage:
##
## Comments:
## A "work-in-progress".  Not very useful.

proc ilwd::str2ilwd { str } {
     if { [ catch { 
        ;## order in list is critical!!
        set types {
                  ILWD    {<[^>]+>[^<]*</[^>]+>} {}
                  INT_4U  {^[0-9 \n]+$}          {}
                  INT_4S  {^[0-9 \n-]+$}         {}
                  REAL_4  {^[0-9. \n-]+$}        {}
                  LSTRING {[a-zA-Z]+}            {}
                  }
     
        foreach { type rx attrs } $types {
           if { [ regexp -- $rx $str ] } { break }
           set type  {}
           set rx    {}
           set attrs {}
        }   
        
        ;## get size for size attribute
        if { [ string match $type LSTRING ] } {
           set size [ string length $str ]
           set dim [ expr { 1 + [ regsub -all \\, $str \\\, str ] } ]
           set attrs " size='$size' dims='$dim'"
           set elem "<$type$attrs>$str</$type>"
        } elseif { ! [ string match $type ILWD ] } {
           set size [ llength $str ]
           set dim [ expr { 1 + [ regsub -all "\n" $str ", " str ] } ]
           set attrs " size='$size' dims='$dim'"
           set elem "<$type$attrs>$str</$type>"
        } else {
           set elem $str
        }   
    } err ] } {
       return -code error "[ myName ]: $err"
    }
       return $elem                
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::newcontp 
##
## Description:
## Create an empty container object in the c++ layer
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc ilwd::newcontp { { tag "none" } } {
     if { [ catch {
	 	if	{ [ string match none $tag ] } {
        	set contp [ putElement "<ilwd size='0'></ilwd>" ]
		} else {
			set contp [ putElement "<ilwd name='$tag' size='0'></ilwd>" ]
		}
    } err ] } {
        return -code error "[ myName ]: $err"
    }
   	return $contp
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::addElement
##
## Description:
## Add an element to an ilwd container object.
##
## Parameters:
## containerp - an LdasContainer_p
## ilwd - a list of ilwd elements as strings or Ldas*_p's 
##
## Usage:
##
## Comments:
## If containerp is {}, a new container will be created,
## and the pointer name of the container will be returned.

proc ilwd::addElement { { ilwds "" } { containerp "" } } {
     set seqpt {}
     set localp 0
     if { [ catch {
        if { ! [ string length $containerp ] } {
           set containerp [ ilwd::newcontp ]
        }
        if { [ llength $ilwds ] == 1 } {
           set ilwds [ lindex $ilwds 0 ]
        }
        foreach ilwd $ilwds {   
           set localp 0
           set type [ lindex [ varType $ilwd ] 0 ]
           if { [ lsearch $type ilwd ] != -1 } {
              if { [ lsearch $type pointer ] != -1 } {
                 set seqpt "addContainerElement($ilwd):"
                 addContainerElement $containerp $ilwd
              } elseif { [ lsearch $type text ] != -1 } {
                 set seqpt "putElement($ilwd):"
                 set ilwd [ putElement $ilwd ]
                 set localp 1
                 set seqpt "addContainerElement($ilwd):"
                 addContainerElement $containerp $ilwd
              }
           } else {
              set msg "received item that is not ilwd: '$ilwd'" 
              return -code error $msg
           }
           if { $localp } { catch { destructElement $ilwd } }
        } ;## end of foreach on "ilwds"  
     } err ] } {
        if { $localp } { catch { destructElement $ilwd } }
        return -code error "[ myName ]:$seqpt $err"
     }
     return $containerp ;## in case we created one
}
## ******************************************************** 


## ******************************************************** 
##
## Name: ilwd::setjob
##
## Description:
## Sets the jobid attribute of an ilwd object
##
## Parameters:
## element - the element pointer name string
##
## Usage:
##
## Comments:
## The jobid is always the current jobid.  The attribute
## consists solely of the integer portion exclusive of the
## ::RUNCODE string.

proc ilwd::setjob { element { jobid "" } } {
     set seqpt {}
     if { ! [ string length $jobid ] } {
        set jobid $::jobid
     }   
     if { [ catch {
        if { ! [ regexp {\d+} $jobid id ] } {
           return -code error "bad job i.d.: $jobid"
        }
        set seqpt "setJobid($element $id):"
        setJobId $element $id
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }   
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::getjob
##
## Description:
## Gets the jobid attribute of an ilwd object
##
## Parameters:
## element - the element pointer name string
##
## Usage:
##
## Comments:
## The complete jobid name including the runcode string
## is returned.

proc ilwd::getjob { element } {
     if { [ catch {
        set seqpt "getElementAttribute($element jobid):"
        set id [ getElementAttribute $element jobid ]
     } err ] } {
        return -code error "[ myName ]:$seqpt $err"
     }
     return ${::RUNCODE}$id
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::message 
##
## Description:
## If the element recieved at a data socket has it's "name"
## attribute set to the string "message", then the content
## of the element may be a message which causes some
## response on the data socket.  If the message is in the
## form of a known Tcl command, the command will be
## executed.
##
## Parameters:
##
## Usage:
##
## create an ilwd of the form:
##
## <ilwd>
##   <lstring name='message' jobid='123' size='3'>...</lstring>
## </ilwd>
##
## and send it to the data socket of an API.
##
## Comments:
## We have never used data sockets with a handshaking
## protocol before. :TODO:

proc ilwd::message { element } { 
     set seqpt [ list ]
     set id    [ list ]
     set msg   [ list ]
     if { [ catch {
        if { [ catch {
           set seqpt "getElementAttribute($element jobid):"
           set id [ getElementAttribute $element jobid ]
        } err ] } {
           set id [ list ]
        }
        if { [ catch {
           set seqpt "getElementAttribute($element name):"
           set name [ getElementAttribute $element name ]
        } err ] } {
           set name [ list ]
        }
        set seqpt [ list ]
        if { [ string match message $name ] } {
           set msg [ getElement $element ]
           regexp {>([^<]+)<} $msg -> msg
        }   
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return [ list $id $message ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::unpackContainer 
##
## Description:
## Bulletproof one-level-deep container unpacker with
## attribute migration.
##
## Take an ilwd object and "unpack" it one level deep.
## linserts all attributes from the outer container at
## index 0 of each contained object's arguments.
##
## The higher level API needs to establish whether the
## object NEEDS to be unpacked :TODO:
##
## The returned objects are GUARANTEED to have the
## attributes in $attributes.
## invalid_attribute errors from the input object are
## handled transparently.
##
## Parameters:
##
## Usage:
##
## Comments:
## getElementAttribute and setElementAttribute throw an
## invalid_element or invalid_attribute exception as
## appropriate.

proc ilwd::unpackContainer { ilwdp { attributes "" } } {
     
     if { [ catch {
        set returnptrs [ list ]
        ;## attributes we would like to pass on
        if { ! [ string length $attributes ] } {
           set attributes [ list jobid name comment ]
        }
        ;## this will throw if the element is invalid
        ;## (invalid_element is thrown if the element
        ;## was already destroyed also).
        set seqpt "getContainerSize($ilwdp):"
        set size [ getContainerSize $ilwdp ]

        foreach attrib $attributes {
           if { [ catch { 
              set seqpt "getElementAttribute($ilwdp $attrib):"
              set $attrib [ getElementAttribute $ilwdp $attrib ]
              if { [ catch { llength [ set $attrib ] } ] } {
                 ;## oops, $attrib is not a well-formed list!!
                 regsub -all -- {([\}\"])([\:\.\,\?\!\)])} \
                    [ set $attrib ] {\1 \2} $attrib
              }
           } err ] } {
              ;## will be "{}"
              set $attrib [ list ]
           }
        }
        
        set seqpt {}
        set i 0
        while { $i < $size } {
           ;## we are always operating on element 0 because
           ;## we delete as we go.
           set seqpt "copyContainerElement($ilwdp 0):"
           set contp [ copyContainerElement $ilwdp 0 ]
           set seqpt "deleteContainerElement($ilwdp 0):"
           deleteContainerElement $ilwdp 0 ]
           set seqpt {}
           ;## attributes in contained element
           foreach attrib $attributes {
              if { [ catch { 
                 set seqpt "getElementAttribute($contp $attrib):"
                 set ${attrib}_c [ getElementAttribute $contp $attrib ]
                 if { [ catch { llength [ set ${attrib}_c ] } ] } {
                    ;## oops, $attrib is not a well formed list!
                    regsub -all -- {([\}\"])([\:\.\,\?\!\)])} \
                       [ set ${attrib}_c ] {\1 \2} ${attrib}_c
                 }
              } err ] } {
                 ;## will be "{}"
                 set ${attrib}_c [ list ]
              }
              set ${attrib}_c [ list [ set $attrib ] [ set ${attrib}_c ] ]
              ;## could very easily turn out to be "{} {}"
              set seqpt \
                 "setElementAttribute($contp $attrib [ set ${attrib}_c ]):"
              setElementAttribute $contp $attrib [ set ${attrib}_c ]
           }
           lappend returnptrs $contp
           incr i
        }
        ;## and lastly, we destruct the original object
        set seqpt "destructElement($ilwdp):"
        destructElement $ilwdp
     } err ] } {
        ;## we DO NOT destruct the input object on error
        foreach ptr [ concat $contp $returnptrs ] {
           ::destructElement $ptr
        }
        return -code error "[ myName ]:$seqpt $err"
     }
     return $returnptrs
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::readFile
##
## Description:
## Read an input ilwd file from the local filesystem, or from
## an http or ftp url.
##
## If invoked from the ligolw API, will handle xml files as
## well.  XML handling needs to be broken out and moved to
## the ligolw API :TODO:
## 
## Parameters:
##
## Usage:
##
## Comments:
## It is intended that all file transfers, reads and writes
## happen in the "background" without interfering with Tcl
## event loop processing.

proc ilwd::readFile { fname { jobid "" } } {
     set seqpt    {}
     set tempfile {}
     set ext      {}
     
     if { [ catch {
        regexp {file:\/+(.+)} $fname -> fname

        if { ! [ file exists $fname ] } {
           
           if { ! [ file exists $fname ] } {
              if { [ file exists [ string trim $fname "/" ] ] } {
                 set fname [ string trim $fname "/" ]
              } elseif { [ file exists /$fname ] } {
                 set fname /$fname
              } else {
                 return -code error "'$fname' not found"
              }   
           }
        } ;## end of "if not file exists fname"
        
        ;## if it's xml, and we are the ligolw, we can
        ;## handle it by threading the reading of the file into lwp 
        
		set ext [ file extension $fname ]
        if { [ regexp -nocase {xml} $ext ] } {
           if { ! [ regexp -nocase {ligolw} $::API ] } {
              return -code error "$::API API cannot process xml" 
           }
		   
           set seqpt "ligolw::thread_readLwFile($jobid,$fname):"
           set tid [ ligolw::thread_readLwFile $jobid $fname ]
		   set datap $tid
        } else {
           set seqpt "openILwdFile($fname):"
           set fp   [ openILwdFile $fname r ]
           
           ;## if ::DEBUG is set to 0xbad we don't use threads
           ;## we dont use threads 
           set seqpt "readElement($fp):"
           set ilwdp [ readElement $fp ]
           
           set datap $ilwdp
           set seqpt "closeILwdFile($fp):"
           closeILwdFile $fp
           set seqpt {}
        } ;## end of xml or ilwd handler  
     } err ] } {
        if { [ info exists lwp ] } {
           catch { destructLWDocument $lwp }
        } 
        if { [ info exists fp ] } {
           catch { closeILwdFile $fp }
        }
        if { [ info exists ilwdp ] } {
           catch { destructElement $ilwdp }
        }
		if { [ info exists lwp ] } {
           catch { destructLWDocument $lwp }
        }
        ;## save copy of this ilwd for exam
        ;## debug only
        if	{ [ info exist ::BADILWDDIR ] && [ file isdirectory $::BADILWDDIR ] } {
        	catch { file copy -force $fname $::BADILWDDIR }
        }
        return -code error "[ myName ]:$seqpt $err"
     }
     
     return $datap
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::writeFile
##
## Description:
## Write an ilwd object
## Parameters:
## jobid - an LDAS jobid, i.e. NORMAL1234
## ptr - the name of an ilwd element or container object
## fname - a valid URI or local file name to write output to
## format - binary, ascii
## comp - compression level
##
## Usage:
##
## Comments:
##

proc ilwd::writeFile { jobid ptr fname { format binary } { comp none } } {
     if { [ catch {       
        set targ [ publicFile $jobid $fname $ptr $format $comp ]
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $targ
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::write2disk
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc ilwd::write2disk { jobid fname ptr format comp } {
     
     if { [ catch {
        set seqpt "openILwdFile($fname):"
        set fp [ openILwdFile $fname w ]
        
		;## removed threaded call 02/27/06        
       	set seqpt "writeElement($fp $ptr $format $comp):"
        writeElement $fp $ptr $format $comp
        
        set seqpt "closeILwdFile($fp):"
        closeILwdFile $fp
     } err ] } {
        if { [ info exists fp ] } {
           if { [ catch {
              set seq "closeILwdFile($fp):"
              closeILwdFile $fp
           } err2 ] } {
              set err "$err: $seq $err2"
           }
        }
        return -code error "[ myName ]:$seqpt $err"
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::dump
##
## Description:
## Dump an ilwd container in ilwd text format.
##
## Parameters:
## containerp - an ilwd container object pointer
##
## Usage:
##
## Comments:
##

proc ilwd::dump { containerp { format "ascii" } { compress "none" } } {
     if { [ catch {
        set seqpt "getElement($containerp):"
        set ilwdtext [ getElement $containerp $format $compress ]
        } err ] } {
        return -code error "[ myName ]:$seqpt $err"
        }
     return $ilwdtext
}
## ******************************************************** 

## ******************************************************** 
##
## Name: ilwd::decodeNullMask
##
## Description:
## Decode ilwd null mask base64 encoded value into
##  actual mask bits.
##
## Parameters:
## mask - base64 encoded string. 
##
## Usage:
##
## Comments:
##

proc ilwd::decodeNullMask { { mask "0" } } {
     set result ""
     binary scan "A" "c" valueA
     binary scan "a" "c" valuea
     binary scan "0" "c" value0
     set indexa 26
     set index0 52 
     set n 0
     for { set i 0 } { $i < [ string length $mask ] } { incr i } {
         set char [ string range $mask $i $i ]
         switch -regexp $char {
            [A-Z] { 
                  binary scan $char "c" value
                  set value [ expr $value - $valueA ]
                  }
            [a-z] { 
                  binary scan $char "c" value
                  set value [ expr $value - $valuea + $indexa ]
                  }
            [0-9] {
                  binary scan $char "c" value
                  set value [ expr $value - $value0 + $index0 ]
                  }
            [+]   { set value 62 }
            [/]   { set value 63 }
            [=]   { }
          default { }
         } ;## end of switch          
         foreach base2  { 32 16 8 4 2 1 } { 
            incr n 1
            set quotient [ expr $value / $base2 ]
            if { $quotient } {
               lappend nulllist $n
            }
            set value [ expr $value % $base2 ]
            set result "$result$quotient"
        }
     }
     return "$result $nulllist"
}
## ********************************************************
