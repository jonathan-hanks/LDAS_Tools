## ********************************************************
## stat.tcl version 1.0
##
## Provides command wrapping for OS stat calls.
##
## Release date: 99.03.23
##
##
## This namespace is private and should not be "imported".
##
## NO COMMENTS SHOULD APPEAR IN THE INPUT!!
##
## ********************************************************

;#barecode

package provide stat 1.0

if { ! [ info exists genericAPI ] } {
   set msg    "stat.tcl is a module of\n"
   append msg "the genericAPI.tcl and provides\n"
   append msg "no independent functionality."
   return -code error $msg
   }

;## 
namespace eval stat {}
;#end
