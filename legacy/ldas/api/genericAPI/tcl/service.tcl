## ******************************************************** 
## service.tcl
##
## Provides resource file parsing functions.
##
## Associates remote hostnames with local ports and socket
## identifiers, and API/service names with remote
## hostnames and local port and socket id's.
## Relies on a running API with the genericAPI.tcl code
## loaded, and a proper resource file.
##
## Allows retrieval of related resource data based on
## multiple search criteria.
##
## validService is the only exposed function in this file.
## ******************************************************** 

;#barecode

package provide service 1.0

if { ! [ info exists genericAPI ] } {
   set msg    "service.tcl is a module of\n"
   append msg "the genericAPI.tcl and provides\n"
   append msg "no independent functionality."
   return -code error $msg
   }
        
namespace eval service { }

;#end

## ******************************************************** 
## Name: service::type
##
## Description:
## Runs a series of tests on it's arguments to determine
## their "type".  Throws an exception if it thinks the
## argument is invalid due to unidentifiable type,
## unregistered host, or unregistered service.

proc ::service::type { { args "" } } {
     
     foreach [ list arg1 arg2 ] $args { break }
    
     ;## arg1 is a socket handle
     if { [ regexp {sock[0-9]+} $arg1 ] } {
        return sid
     }

     ;## arg1 is an integer, therefore a port
     if { [ regexp {^[0-9]+$}   $arg1 ] } {
        return port
     }

     ;## if we haven't matched yet we need to
     ;## go hunting api's
     if { ! [ info exists ::API_LIST ] } {
        set msg "::API_LIST not defined in LDASapi.rsc file!"
        return -code error $msg
     }

     ;## all api's must be registered with all
     ;## other api's
     foreach api $::API_LIST {
        ;## all api's must be registered with all
        ;## other api's
        if { ! [ array exists ::$api ] } {
           set msg "'::$api' array not defined in LDASapi.rsc file!"
           return -code error $msg
        }
        
        ;## exact match for an api name
        if { [ string match $arg1 $api ] } {
           return api
        }

        ;## general match for a service type,
        ;## i.e. operator, emergency, data, others?
        if { [ lsearch [ array names ::$api ] $arg1 ] > -1 } {
           return service
        }

        ;## last of all try for a hostname.  this must
        ;## be done AFTER trying to match an api name
        ;## per PR#1336, otherwise a machine can never
        ;## have the same name as an API!
        if { [ string match $arg1 [ set ::${api}(host) ] ] } {
           return host
        }
     }
     
     set msg    "I don't recognise '$arg1' as a registered\n"
     append msg "sockid, port, host, api or service."
     return -code error $msg
}
## ******************************************************** 

## ******************************************************** 
## Name: service::issid
##
## Description:
## Called when only one argument is available and it looks
## like a socket i.d. i.e. "sock1".
##
## Throws an exception when the sockid has not been
## registered by a legal call to openSock.

proc ::service::issid { { args "" } } {
     set api     {}
     set host    [ set ::${api}(host) ]
     set service {}
     set port    {}
     set sid     {}

     foreach { arg1 arg2 } $args { break }
     foreach api $::API_LIST {
        foreach service [ array names ::$api ] {
           set sid  [ lindex [ set ::${api}($service) ] 1 ] 
           set port [ lindex [ set ::${api}($service) ] 0 ] 
           if { [ string match $sid $arg1 ] } {
              return [ list $api $host $port $service $sid ]
           }
        }
     }
     return -code error "no service using socket i.d. '$sid'"
}
## ******************************************************** 

## ******************************************************** 
## Name: service::isport
##
## Description:
## Called when only one argument is provided and it's a
## bare integer, and therefore assumed to be a port on
## the local machine.
##
## Throws an exception if the port is not registerd in
## the local resource file.

proc ::service::isport { { args "" } } {
     set api     $::API
     set host    [ set ::$env(host) ]
     set service {}
     set port    {}
     set sid     {}

     foreach { arg1 arg2 } $args { break }
     
     foreach service [ array names ::$api  ] {
        set port [ lindex [ set ::${api}($service) ] 0 ]
        set sid  [ lindex [ set ::${api}($service) ] 1 ]
        if { [ string match $port $arg1 ] } {
           return [ list $api $host $port $service $sid ]
        }
     }
     set msg "no service tied to port '$arg1' on host '$host'."
     return -code error $msg
}
## ******************************************************** 

## ******************************************************** 
## Name: service::ishost
##
## Description:
## Called when arg1 of the args list is a hostname.
## the hostname must have been either the localhost or a
## remote host registered in the resource file, or
## service::type would have already rejected it.
##
## This call is deprecated.

proc ::service::ishost { { args "" } } {
     set msg "validService cannot be called on a hostname" 
     return -code error $msg
}     
## ******************************************************** 

## ******************************************************** 
## Name: service::isservice
##
## Description:
## Called when arg1 is a "service".
## Throws an exception when no matching registered service
## is found on the local host.  Will return the base port
## for data service if the service is data.

proc ::service::isservice { { args "" } } {
     set api     $::API
     set host    $::env(HOST)
     set service {}
     set port    {}
     set sid     {}

     foreach { arg1 arg2 } $args { break }
     
     set service $arg1
     set port [ lindex [ set ::${api}($service) ] 0 ]
     set sid  [ lindex [ set ::${api}($service) ] 1 ]
     return [ list $api $host $port $service $sid ]
}
## ******************************************************** 

## ******************************************************** 
## Name: service::isapi
##
## Description:
## Called when the first arg is an API.  If there is no
## second argument the operator socket is assumed.
##
## Throws an exception if no list of API's is registered
## or if the API is not registered.
## this one has got rather complicated, since it is the most
## useful.

proc ::service::isapi { { args "" } } {
     set api     {}
     set host    {}
     set service {}
     set port    {}
     set sid     {}
     foreach { arg1 arg2 } $args { break }
     
     set api $arg1
     set host [ set ::${api}(host) ]

     ;## only one arg was passed
     if { ! [ string length $arg2 ] } {
        set service operator
        set port [ set ::${api}(operator) ]
     } else {
        ;## second argument was a port
        if { [ regexp {\d+} $arg2 port ] } {  
           foreach svc { operator emergency data } {
              if { $port == [ lindex [ set ::${api}($svc) ] 0 ] } {
                 set service $svc
              }
           }
        ;## second argument was NOT a port, it must be a service
        } else {
           set service $arg2
           set port [ lindex [ set ::${api}($service) ] 0 ]
        }
     }
     
     set sid  [ lindex [ set ::${api}($service) ] 1 ]
     return [ list $api $host $port $service $sid ]
}
## ******************************************************** 

## ******************************************************** 
##
## Name: validService 
##
## Description:
## Test API name or host/service pair OR host/port pair
## for validity as a registered service known to the API
## calling the proc. Local and remote services are
## validated.  Will also return host/port/service given
## a registered socket i.d.
##
## Usage:
##    set target [ validService $arg1 $arg2 ]
##    foreach {api host port service sid} $target {break}
##
## Comments:
## Will generate errors at the level of, and with the name
## of the calling procedure.
## if only a port # is provided, it will be treated as
## a port on the local host.
##
## If arguments cannot be FULLY validated, that is if
## a host, port, and service cannot be associated, the
## call will throw an exception -- so you will need to
## register debugging test values by some such means as
## "set hostname(service) port" to test.

proc validService { { args "" } } {
     if { [ catch {
        set seq "service::type"
        set type [ eval ::service::type  $args ]
        set seq "service::is$type"
        set retval [ eval ::service::is$type $args ]
     } err ] } {
        uplevel [ return -code error "validService: $seq:\n$err" ]
     }
     return $retval
}
## ******************************************************** 

