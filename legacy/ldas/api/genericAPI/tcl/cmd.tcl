## ********************************************************
## cmd.tcl version 1.1
##
## Provides command wrapping and formatting functions
## for insuring consistent format of commands passed
## to Tcl sockets.
##
## Release date: 99.03.23
##
##
## This namespace is private and should not be "imported".
##
## NO COMMENTS SHOULD APPEAR IN THE INPUT!!
##
## ********************************************************

;#barecode

package provide cmd 1.0

if { ! [ info exists genericAPI ] } {
   set msg    "cmd.tcl is a module of\n"
   append msg "the genericAPI.tcl and provides\n"
   append msg "no independent functionality."
   return -code error $msg
   }

;## used for large data reads which cannot be completed in one read
namespace eval cmd {}
;#end

## ******************************************************** 
##
## Name: cmd::result
##
## Description:
## Better reader for persistent connections.
##
## Parameters:
##
## Usage:
##
## Comments:
## 23 retries equals roughly one second.
## Each additional retry extends the polling by
## 1/20th of a second.

proc cmd::result { { sid "" } { retries 100 } } {
     set data  {}
     set datum {}
     set blocking_flag 0
     
     fconfigure $sid -buffering full
     
     if { [ fconfigure $sid -blocking ] } {
        set blocking_flag 1
        fconfigure $sid -blocking off
     }
     
     set i 0
     while { [ incr i ] <= $retries } {
        ;## if we catch here it's probably a "broken pipe".
        ;## we started seeing these in spring of 2001 during
        ;## the manager's startup sequence when trying to
        ;## start API's on linux boxes.
        if { [ catch {
           set datum [ read $sid ]
        } err ] } {
           if { ! [ string length $data ] } {
              addLogEntry $err red
           } else {
              ;## should we try to pass on the error?
              debugPuts $err
           }
           break
        }
        
        ;## here's where we get out when we've read enough.
        if { [ string length $data ] && \
             [ info complete $data ] && \
           ! [ string length $datum ] } {
           break
        }
        
        if { [ string length $datum ] } {
           append data $datum
        }
        
        if { [ eof $sid ] } { break }
        
        if { [ info exists ::__t::S$sid ] } {
           if { [ __t::mark $sid ] < 0.100 } {
              after 20
           } else {
              after 100
           }
        } else {
           after 20
        }
        __t::start $sid
     }
     
     if { [ info exists ::__t::S$sid ] } {
        unset ::__t::S$sid
     }

     ;## backwards compatibility handler for
     ;## scary old blocking function!
     regsub {^~~\d+\n} $data {} data
     set data [ string trim $data "\n" ]
     if { $blocking_flag } {
        fconfigure $sid -blocking on
     }
     fconfigure $sid -buffering line
     return $data
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cmd::receive
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cmd::receive { cid var callback { timeout 10000 } { data "" } } {
     
     if { [ catch {
	 	if	{ [ info exist ::MAX_API_SOCKET_TIMEOUT_SECS ] && $timeout == 10000 } {
			set timeout $::MAX_API_SOCKET_TIMEOUT_SECS
		}
        fileevent $cid readable {}
        set done 0
        set datum [ list ]
     
        set now [ clock clicks -milliseconds ]
        
        ;## initialise for this socket
        if { ! [ info exists ::cmd::cmd($cid,start) ] } {
           fconfigure $cid -buffering line
           
           if { [ catch {
              set peerinfo [ fconfigure $cid -peername ]
	         regexp {[\d\.]+} $peerinfo clientip
           } err ] } {
              set clientip unknown
           }
           
           fconfigure $cid -buffering full
     
           if { [ fconfigure $cid -blocking ] } {
              set ::cmd::cmd($cid,blocking) 1
              fconfigure $cid -blocking off
           } else {
              set ::cmd::cmd($cid,blocking) 0
           }

           set ::cmd::cmd($cid,start)    $now
           set ::cmd::cmd($cid,timeout)  $timeout
           set ::cmd::cmd($cid,data)     $data
           set ::cmd::cmd($cid,clientip) $clientip
        }
        
        set elapsed \
           [ expr { $now - $::cmd::cmd($cid,start) } ]
        
        if { $elapsed < $timeout } {
           
           set datum [ read $cid ]
           
           ;## here's where we get out when we've read enough.
           ;## it says: if we have read from the socket before,
           ;## and we have read a valid list, and we have just
           ;## now read nothing, we are done.
           if { [ string length $::cmd::cmd($cid,data) ] && \
                [ info complete $::cmd::cmd($cid,data) ] && \
              ! [ string length $datum ] } {
              set done 1
           } elseif { [ string length $datum ] } {
              append ::cmd::cmd($cid,data) $datum
           }
           
           ;## socket was closed?
           if { [ eof $cid ] } { set done 1 }
           
        } else {
           set msg "ERROR: timed out reading data on $cid after "
           append msg "$elapsed ms. "
           if { [ string length $::cmd::cmd($cid,data) ] } {
              append msg \
                 "data recieved: ***-> $::cmd::cmd($cid,data) <-***"
           }
           
           ;## not sure how, but it seems possible to get here with
           ;## a valid command, so one last peek.
           if { ! [ string length $::cmd::cmd($cid,data) ] || \
                ! [ info complete $::cmd::cmd($cid,data) ] } {
              set ::cmd::cmd($cid,data) $msg
           }
           
           set done 1
        }

        if { $done } {
           set data $::cmd::cmd($cid,data)
           regsub {^~~\d+\n} $data {} data
           set data [ string trim $data "\n" ]   
           if { $::cmd::cmd($cid,blocking) } {
              fconfigure $cid -blocking on
           }
           fconfigure $cid -buffering line
           set clientip $::cmd::cmd($cid,clientip)
           foreach name [ array names ::cmd::cmd $cid,* ] {
              unset ::cmd::cmd($name)
           }
        }
        
        ;## loop again or trigger the callback
        if { [ llength [ array names ::cmd::cmd $cid,* ] ] } {
           ;## after i.d.'s are 32-bit ints and are never reused
           if { [ info exists ::cmd::cmd($cid,afterid) ] } {
              after cancel $::cmd::cmd($cid,afterid)
           }
           ;## the manager's operator socket has to poll slower!!
           if { [ string equal manager $::API ] && \
                [ regexp {operator_socket} $var ] } {
              set poll_ms 200
           } else {
              set poll_ms 30
           }
           set ::cmd::cmd($cid,afterid) \
              [ after $poll_ms \
              [ list cmd::receive $cid $var $callback $timeout $data ] ]
        } else {
	      ;## for client programs like guild which use portMsg
 	      regsub {!host!} $data $clientip data
           ;## ditto for remote clients using md5 challenge protocol
           set md5_rx {-password\s+md5protocol}
           if { [ regexp -- $md5_rx $data ] } {
              ;## at this point we have 6 elements in the list with
              ;## md5digest in element end-1.
              ;## element end is the users password with the salt
              ;## appended and then md5 hashed.
              
              if { [ string equal md5digest [ lindex $data end-1 ] ] } {
                 set digest [ lindex $data end ]
                 set data [ lrange $data 0 2 ]
                 regsub -- {-md5salt} $data \
                    "-md5digest $digest -md5salt" data
                 fileevent $cid readable {}
                 set ::$var $data
                 if	{ [ regexp {(\S+)\((\S+)\)} $var -> arrname element ] } {
              		after 0 [ list eval $callback ::$arrname $element ]
              	 } else {
              		after 0 [ list eval $callback ::$var ]
              	 }
              } else {
                 ;## we will need to compare a key::md5 of the
                 ;## salt appended to the users password later.
                 set salt  [ expr abs([ clock clicks ]) ]
                 puts $cid "md5salt $salt"
                 regsub -- {-name} $data \
                    "-md5salt $salt -name" data
                 ;## this will send us right round again when md5
                 ;## challenging is required.
                 append data { }
                 fileevent $cid readable \
                    [ list cmd::receive $cid $var $callback $timeout $data ]
              }
           } else {
              fileevent $cid readable {}
              ;## trigger the callback
              set ::$var $data
              if	{ [ regexp {(\S+)\((\S+)\)} $var -> arrname element ] } {
              		after 0 [ list eval $callback ::$arrname $element ]
              } else {
              		after 0 [ list eval $callback ::$var ]
              }
           }
        }
        
     } err ] } {
        set noasst_rx {(invalid\s+command\s+name|namespace\s+doesn)}
        foreach name [ array names ::cmd::cmd $cid,* ] {
           unset ::cmd::cmd($name)
        }
        if { [ string length $err ] } {
           if { [ info exists elapsed ] && ! $elapsed } {
              ;## job is being cleaned up and assistant is gone
              if { [ regexp -nocase $noasst_rx $err ] } {
                 catch { ::unset ::$var }
                 catch { ::close $cid }
              } else {
                 if { [ catch {
                    if { [ string equal manager $::API ] } {
                       set ::$var [ list 3 $err error! ]
                       if	{ [ regexp {(\S+)\((\S+)\)} $var -> arrname element ] } {
              				after 0 [ list eval $callback ::$arrname $element ]
              			} else {
              				after 0 [ list eval $callback ::$var ]
              			}
                    } else {
                       return !
                    }
                 } err2 ] } {
                    ;## for example, at shutdown, the manager can
                    ;## read off a socket from a defunct job.
                    if { [ regexp -nocase $noasst_rx $err2 ] } {
                       catch { ::unset ::$var }
                       catch { ::close $cid }
                    } else {
                       return -code error "[ myName ]: $err"
                    }
                 }
              }
           } elseif { ! [ regexp -nocase $noasst_rx $err ] } {
              set subj "$::LDAS_SYSTEM $::API Error"
              set msg "[ myName ]: $err while handling '$var' ($cid)"
              set msg "Subject: ${subj}; Body: $msg"
              addLogEntry $msg email
              catch { set ::$var "[ myName ]: $err" }
           } else {
		    catch { ::unset ::$var }
              catch { ::close $cid }
		 }
        }
     }
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cmd::md5Unpack
##
## Description:
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cmd::md5Unpack { cmd } {
     
     if { [ catch {
        set salt   [ list ]
        set digest [ list ]
        set md5_rx {-password\s+md5protocol}
        if { [ regexp -nocase -- $md5_rx $cmd ] } {
           ;## we are validating a user command for the operator
           ;## socket on the manager
           if { [ llength $cmd ] == 3 } {
              set user [ lindex $cmd 1 ]
              set cmd  [ lindex $cmd 2 ]
           } else {
              ;## we are just validating user info for the cntlmon
              ;## API via the emergency socket on the manager
              set user $cmd
              unset cmd
           }
           regexp -- {-md5digest\s+(\S+)}  $user -> digest
           regexp -- {-md5salt\s+(\S+)}    $user -> salt
           regsub -- {-md5digest\s+\S+\s+} $user {} user
           regsub -- {-md5salt\s+\S+\s+}   $user {} user
           uplevel set salt   $salt
           uplevel set digest $digest
           if { [ info exists cmd ] } {
              ;## we are validating a user command for the operator
              ;## socket on the manager
              set cmd [ list ldasJob $user $cmd ]
           } else {
              ;## we are just validating user info for the cntlmon
              ;## API via the emergency socket on the manager
              set cmd $user
           }
        }
     } err ] } {
        return -code error "[ myName ]: $err"
     }
     return $cmd
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cmd::size
##
## Description:
## Attach proper size info for handling by cmd::result
## when using a persistent connection.
##
## Parameters:
##
## Usage:
##
## Comments:
##

proc cmd::size { cmd } {
     set size [ string length $cmd ]
     set cmd "~~$size\n$cmd"
     return $cmd
}
## ******************************************************** 

## ******************************************************** 
##
## Name: cmd::disconnect 
##
## Description:
## Disconnect a socket, registering the disconnected socket
## in the services registry if possible, otherwise falling
## back to a plain close.
##
## Usage:
##
## Comments:
## 

proc cmd::disconnect { sid } {
     if { [ catch {
        ::close $sid
        } err ] } {
        debugPuts $err
        }    
}
## ********************************************************

## ******************************************************** 
##
## Name: cmd::discard
##
## Description:
## does cmd::result; cmd::disconnect and just ignores the
## reply.  Used for anonymous communication with the
## emergency services.
##
## Parameters:
##
## Usage:
##        fileevent $sid readable [ cmd::discard $sid ]
## Comments:
##

proc cmd::discard { sid } {
     cmd::result $sid
     cmd::disconnect $sid
}
## ******************************************************** 

