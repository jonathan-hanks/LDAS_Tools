#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

;## threaded functions for multi-threading of send and recv
package provide datasock_thread

proc threadedRecv { tid } {
     if { [ catch {
	 	set seqpt "acceptDataSocket_r $tid"
	 	set acceptp [ acceptDataSocket_r $tid ]
		unset ::threadtype($tid)
		set seqpt "getSocketPeerIpAddress $acceptp" 
		set sender [ getSocketPeerIpAddress $acceptp ]
		set seqpt "recvElementObject_t $acceptp"
		set rtid [  recvElementObject_t $acceptp ]
		;##regsub {^_} $rtid {_000} rtid
		array set ::argms [ list ${rtid} [ list recvElementDone $rtid $acceptp $sender ] ]
		array set ::threadtype [ list $rtid "R" ] 
		;##puts "after recv, threadtype=[array get ::threadtype]"
    } err ] } {
		puts stderr "threadedRecv error: $seqpt, $err"
    }
}

;## socket may have been close already by other side
proc sendElementDone {tid clientp datap} {
    set seqpt ""
    if  { [ catch {
	;##puts stderr "in sendElementDone, clientp=$clientp,datap=$datap"
	    unset ::threadtype($tid)
	    unset ::argms($tid) 
        set seqpt "closeDataSocket $clientp"
        closeDataSocket $clientp  
        set seqpt "sendElementObject_r $tid"
	    sendElementObject_r $tid
        ;##incr ::${datap}(num) 1
        ;##if  { [ set ::${datap}(num) ] > $::ntimesMax } {
        ;##    unset ::${datap}(num)
        ;##    set ::done 1
        ;## }
        ;## do not destroy this since threads have not finished 
        #set seqpt "destructElement $datap"
	set ::done 1
	destructElement $datap
    } err ] } {
        puts stderr "sendElementDone: $seqpt,$err"
    }
}

proc threadedSend { datap } {

        if  { [ catch { 
        set seqpt "retry_connect $::server $::test_server(data)"
        set clientp [ retry_connect $::server $::test_server(data) ]
		;##set clientp [ createDataSocket 0 $::localhost ]
		;##connectDataSocket $clientp $::server $::test_server(data)
		;##puts "connect status [ isSocketConnected $clientp ]"
		if	{ [ isSocketConnected $clientp ] } {
        	;##__t::start 
			;##set cpu_begin [ exectime ]
            after 10
	    set seqpt "copyElement $datap"
	    set ndatap [ copyElement $datap ]
	    set seqpt "sendElementObject_t $clientp $datap"
			set tid [ sendElementObject_t $clientp $ndatap ]            
			array set ::argms [ list ${tid} [ list sendElementDone $tid $clientp $ndatap ] ]
			array set ::threadtype [ list $tid "S" ] 
            incr ::numSent 1
			;##after 1000
			;##set cputime [ expr ( [ exectime ] - $cpu_begin ) ]
			;##set walltime [ __t::mark ]
			;##puts -nonewline "."	
		}
        } err ] } {
            puts "threadedSend err: $seqpt,$err"
        }
}

proc threadSendElements {} {

	;## create the results directory
	;##set resultdir "results_[ clock format [ clock seconds ] -format "%m-%d" ]"
	;##puts "resultdir=$resultdir"
	;##if	{  ! [ file exist $resultdir ] } {
	;##	file mkdir $resultdir
	;##}

	;##set fname "$resultdir/${localhost}_$server.[clock format [ clock seconds ] -format "%H.%M.%S" ]"

	;##set fout [ open $fname a ]

	;##if	{ $::withcont } {
	;##	set conttext "with container"
	;##} else {
	;##	set conttext "without container"
	;##}
	;##puts $fout "Data Socket Element Test $localhost->$server $conttext (seconds)\n"
	set start_time [ clock seconds ]

	;##foreach entry [ concat type dims rate wall_mean wall_stddev wall_cov cpu_mean cpu_stddev cpu_cov] {
   	;##	puts -nonewline $fout [ format "%s\t" $entry ] 
	;##}

	;##puts $fout ""

	;## now send the elements 
	;##set ntimesMax 100
	set ::ntimesMax 3

	if	{ [ catch {
        catch { unset ::done }
    	foreach dims $::dimset {
    	puts "\nsending dims=$dims on $::tcl_platform(os)"
   
    	foreach type $::types {
		;##
		;## skip doing lstring for dims 1000000 for now
		;##
		set wall_${dims}($type) {}
		set cpu_${dims}($type) {}
        	set elemps [ getIlwd $type $::withcont $dims ]
		set datap [ lindex $elemps 0 ]
		set contp [ lindex $elemps 1 ]
        	;##set ::${datap}(num) 1
        ;## create a client
		for { set ntimes 0 } { $ntimes < $::ntimesMax } { incr ntimes 1 } {
        ;## timing starts here
        ;## send element to the server
        ;## CPU time in microsecs 
		;## repeat sending over 100 times to get the variance
		;## and standard deviation
        
			threadedSend $datap
			;## pause a little for threads to finish
        	;##lappend wall_${dims}($type) $walltime
			;##lappend cpu_${dims}($type) $cputime
			;## destruct only when ntimesMax is 1
			;##destructElement $contp
		}
		
        vwait ::done
        after 1000
        destructElement $contp
		;##output 		
    	}
        puts "$::numSent sent, done with dims $dims, type $type"
        }
	} err ] } {
		puts "error: $err"
		set ::Error "Error: $err"
	}
	
	;## compute time taken
	set secs [ expr [ clock seconds ] - $start_time ]
	set mins [ expr $secs / 60 ]
	set hour [ expr $mins / 60 ]
	set mins [ expr $mins % 60 ]
	set secs [ expr $secs % 60 ]
	;##puts $fout "Test completed in $hour:$mins:$secs. $::Error"
    puts "Test completed in $hour:$mins:$secs. $::Error"
	;##close $fout
}

;## socket may already have been closed
proc recvElementDone { tid acceptp sender } {
    if  { [ catch {
	    unset ::threadtype($tid)
	    unset ::argms($tid)
	    set elem_p [ recvElementObject_r $tid ]
	    catch { closeDataSocket $acceptp }
        puts "received [ string length [ getElement $elem_p ] ]"
   	    destructElement $elem_p 
    } err ] } {
        puts stderr "recvElementDone error: $err"
    }
}


