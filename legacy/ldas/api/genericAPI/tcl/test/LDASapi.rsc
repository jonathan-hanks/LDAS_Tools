## -*- mode: tcl -*-
## ********************************************************
##
## Name: LDASapi.rsc
##
## Description:
## The generic resource code for an LDAS api.
##
## Usage:
##
## Comments:
## All API's have an API specific resource file found in
## the LDAS bin directory.
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=1.91;content-type=text%2Fplain
## ********************************************************

;#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## universal required variables
lappend ::REQUIRED_VARIABLES DEBUG
lappend ::REQUIRED_VARIABLES LDAS
lappend ::REQUIRED_VARIABLES MGRKEY
lappend ::REQUIRED_VARIABLES LOGINTERVAL
lappend ::REQUIRED_VARIABLES BASEPORT
lappend ::REQUIRED_VARIABLES RESTART_ON_MEMFLAG
lappend ::REQUIRED_VARIABLES MEMFLAG_MEGS

;## enable gsi authenication in globus channel or disable (blanks)
set ::GSI_AUTH_ENABLED "-gsi_auth_enabled"

;## columns of job_stats
set ::JOB_STATS_COLNAMES [ list jobid job_start_time user cmd status queue_t manager_t diskcache_t \
frame_t metadata_t datacond_t mpi_t wrapper_t eventmon_t ligolw_t asstmgr_t total_t queue_size cntlmon_t ]

;## cmonClient MODIFIABLE RESOURCES 

;## desc=emit debug code, 0 for no, 1 for yes
set ::DEBUG 1

;## desc=path to sendmail executable
set ::PATH_TO_SENDMAIL /usr/lib/sendmail

;## desc=path to lsof executable, if required
set ::PATH_TO_LSOF {}

;## desc=remote URL of official leapseconds table
set ::REMOTE_LEAPSECOND_DATA_URL ftp://maia.usno.navy.mil/ser7/tai-utc.dat

;## if this is set to any positive integer, the manager
;## will attempt to restart an API that exceeds it's
;## memFlag memory usage.
;## desc=auto restart of API due to mem usage
set ::RESTART_ON_MEMFLAG 1

;## desc=number of mB to trigger memFlag on
set ::MEMFLAG_MEGS 2048

;## desc=number of milliseconds defining a ping timeout between API's
set ::PING_TIME_PANIC_LIMIT_MS 2000

;## desc=number of milliseconds defining a ping timeout for specific host
set ::PING_TIME_PANIC_LIMIT_MS_metaserver 2000

;## the first port to assign to the LDAS API's
;## all other LDAS API ports are calculated from this one
;## desc=port from which LDAS API ports are based ! mod=no
set ::BASEPORT 10000

;## desc=interval in seconds at which manager will ping API's ! mod=no
set ::LOGINTERVAL 300

;## desc=subdirectory under user grid home where user running LDAS can write
set ::GRID_FTP_WRITABLE_SUBDIRECTORY ldas

;## desc=offset to first datacond-wrapper comm port
set ::DC_WRAPPER_COMM_PORT_OFFSET 600

;## desc=set to 1 to log all emergency port comm for ALL API's
set ::LOG_ALL_EMERGENCY_COMMANDS 0

;## desc=regexp pattern for IP addresses on the internal network and ldas-sw
set ::PRIVELEGED_IP_ADDRESSES ^(131\.215\.115\.248|10\.|192\.168\.|127\.0\.0\.)

;## desc=offset to first eventmon-wrapper comm port
set ::EVENTMON_DATA_SOCKET_PORT_OFFSET 800

;## desc=number of assistant managers which can be spawned
set ::NUMBER_OF_ASSISTANT_MANAGERS 20

;## desc=how many jobs to queue before rejecting jobs when multiplied by number of assistant managers
set ::MANAGER_QUEUE_SIZE_FACTOR 5

;## number of seconds to wait for data to come through
;## when a new job enters an API.  This number should
;## be fairly generous.
;## desc=seconds timeout for data to arrive from data socket
set ::DATABUCKET_TIMEOUT 60

;## desc=toplevel installation dir
set ::LDAS /ldas

;## desc=database to use
set ::DATABASE_NAME ldas_tst

;## desc=list of API's to connect to ! mod=no
set ::API_LIST [ list manager diskcache frame metadata ligolw datacond mpi eventmon cntlmon ]

;## desc=local machine names for inter-api communications
set ::MANAGER_API_HOST   gateway
set ::DISKCACHE_API_HOST dataserver-dev
set ::FRAME_API_HOST     dataserver-dev
set ::METADATA_API_HOST  metaserver
set ::LIGOLW_API_HOST    metaserver
set ::MPI_API_HOST       beowulf
set ::DATACOND_API_HOST  datacon
set ::CNTLMON_API_HOST   gateway
set ::EVENTMON_API_HOST  metaserver

;## desc=email alias for system oversight
set ::critical_email_list [ list emaros@ligo.caltech.edu ]

;## desc=email aliases for some programmer groups
set ::cpp_programmers "emaros@ligo.caltech.edu"
set ::tcl_programmers "emaros@ligo.caltech.edu"
set ::sys_admins emaros@ligo.caltech.edu

;## desc=email notification for API/system errors
set ::metadata_email    "emaros@ligo.caltech.edu"
set ::system_email [ list emaros@ligo.caltech.edu ]
set ::default_email [ list emaros@ligo.caltech.edu ]
set ::diskcache_email [ list ]
set ::ligolw_email      "emaros@ligo.caltech.edu"
set ::frame_email       "emaros@ligo.caltech.edu"
set ::manager_email [ list emaros@ligo.caltech.edu ]
set ::cntlmon_email     "emaros@ligo.caltech.edu"
set ::mpi_email         "emaros@ligo.caltech.edu"
set ::datacond_email    "emaros@ligo.caltech.edu"
set ::eventmon_email    "emaros@ligo.caltech.edu"
set ::ldas_cert_email   "pehrens@ligo.caltech.edu"

;## desc=email notification for API core files
set ::corefile_metadata_email "$::cpp_programmers"
set ::corefile_diskcache_email emaros@ligo.caltech.edu"
set ::corefile_ligolw_email "$::cpp_programmers"
set ::corefile_frame_email "$::cpp_programmers"
set ::corefile_manager_email "emaros@ligo.caltech.edu"
set ::corefile_cntlmon_email "emaros@ligo.caltech.edu"
set ::corefile_mpi_email emaros@ligo.caltech.edu
set ::corefile_datacond_email "$::cpp_programmers"
set ::corefile_eventmon_email "$::cpp_programmers"

;## desc=allow rename|open|socket|proc|file|exec|cd|pwd|load|exit|source|send on the operator socket
set ::DONT_BLOCK_BAD_WORDS 1

;## desc=the actual mount point of /ldas_outgoing
set ::WORKING_DIRECTORY_MOUNT_POINT /export/ldcg_server

;## desc=max seconds for threads to run before warning
set ::THREAD_TIMEOUT 2000000

;## desc=output directory top level for log matching
set ::TOPDIR /ldas_outgoing

;## desc=control group alias
set ::control_group [ list emaros phil anderson pshawhan dkozak ]

;## desc=should we log all emergency port communications (0 or 1)?
set ::DEBUG_EMERGENCY_PORT 0

;## desc=should we log all strings passed to execssh (0 or 1)?
set ::DEBUG_EXECSSH 0

;## desc=how many seconds between updates of the running job stat page.
set ::RUNNING_JOB_STAT_UPDATE_RATE 5

;## desc=how long before the manager should decide that a job has stuck in an api
set ::MANAGER_ABORT_AFTER_N_SECONDS_IN_ONE_API 1000

;## desc=default absolute directory location of dso objects.
set ::DYNLIB_DIRECTORY /lal/lib/lalwrapper/

;## desc=set to '1' to prevent the use of experimental/development .so's.
set ::REJECT_EXPLICIT_DYNLIB_PATHS 0

;## desc=max time allowed to update log archiveIndex
set ::MAX_LOGARCHIVE_DELAY 2000

;## desc=max number of bak files for resource changes
set ::RESOURCE_BAK_LEVEL 5

;## desc=manager globus ports for receiving jobs via user cert
set ::TCLGLOBUS_USER_PORT 10031

;## desc=manager globus ports for receiving jobs via host cert
set ::TCLGLOBUS_HOST_PORT 10030

;## desc=globus manager host in case there is more than 1 port
set ::GLOBUS_MANAGER_API_HOST $::MANAGER_API_HOST

;## desc=globus read buffer size
set ::GLOBUS_BUFFER_SIZE 204800

;## desc=tcl globus lib directory
set ::TCLGLOBUS_DIR /ldcg/lib/64

;## desc=define location of globus service key file
set ::X509_USER_KEY /etc/grid-security/ldaskey.pem

;## desc=define location of globus service cert file
set ::X509_USER_CERT /etc/grid-security/ldascert.pem

;## desc=define location of globus service certificates dir
set ::X509_CERT_DIR /etc/grid-security/certificates

;## desc=dir to save bad ilwds read in
set ::BADILWDDIR /ldas_usr/ldas/badilwds

;## desc=enable grid-mapfile for authenication
set ::GRIDMAP_ENABLED 1

;## desc=use of non-default grid-mapfile for authenication
set ::GRIDMAP /etc/grid-security/grid-mapfile

;## desc=print callstack with lots of output
set ::DEBUG_PROC 0

;## desc=cmd receive socket timeout limit, originally 10000
set ::MAX_API_SOCKET_TIMEOUT_SECS 100000

;## desc=delay in millsecs to check for thread state to reap it
set ::REAP_THREAD_DELAY 5000

;## desc=globus location
set ::GLOBUS_LOCATION /ldcg/gt4-64bit

;## desc=log archive API
set ::LOG_ARCHIVE_API manager

;## desc=log exec
set ::LOG_EXEC 1
