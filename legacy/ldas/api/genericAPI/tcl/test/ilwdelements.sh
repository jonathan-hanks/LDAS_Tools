#! /bin/sh
basedir=${2}
echo "target directory ${basedir}"
format=ascii 
echo "element type CHAR_S"
elementilwd $1 char_s $format > ${basedir}/char_s_${1}.ilwd
echo "element type CHAR_U"
elementilwd $1 char_u $format > ${basedir}/char_u_${1}.ilwd
echo "element type INT_2S"
elementilwd $1 int_2s $format > ${basedir}/int_2s_${1}.ilwd
echo "element type INT_2U"
elementilwd $1 int_2u $format > ${basedir}/int_2u_${1}.ilwd
echo "element type INT_4S"
elementilwd $1 int_4s $format > ${basedir}/int_4s_${1}.ilwd
echo "element type INT_4U"
elementilwd $1 int_4u $format > ${basedir}/int_4u_${1}.ilwd
echo "element type INT_8S"
elementilwd $1 int_8s $format > ${basedir}/int_8s_${1}.ilwd
echo "element type INT_8U"
elementilwd $1 int_8u $format > ${basedir}/int_8u_${1}.ilwd
echo "element type REAL_4"
elementilwd $1 real_4 $format > ${basedir}/real_4_${1}.ilwd
echo "element type REAL_8"
elementilwd $1 real_8 $format > ${basedir}/real_8_${1}.ilwd
echo "element type LSTRING"
elementilwd $1 lstring_2 $format > ${basedir}/lstring2_${1}.ilwd
elementilwd $1 lstring_4 $format > ${basedir}/lstring4_${1}.ilwd
elementilwd $1 lstring_8 $format > ${basedir}/lstring8_${1}.ilwd
elementilwd $1 lstring_16 $format > ${basedir}/lstring16_${1}.ilwd
