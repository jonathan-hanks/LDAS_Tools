#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

proc readData { sid } {
    if { [ eof $sid ] } {
        catch { close $sid }
        puts ""
        set ::done 1
        return
    }

    if { [gets $sid line] != -1 } {
        puts "$line"
    }

    return
}

proc bgerror { msg } {
    set trace {}
    puts stderr "Error: $msg"
    catch {set trace $::errorInfo}
    if {[string length $trace]} {
        puts stderr "*** Stack Trace ***"
        puts stderr "$trace"
    }

    set ::done 1
    return
}

proc sendCmd { cmd host port { wait 0 } } {

    puts "sending \[$cmd\] to $host:$port"
	flush stdout
    if { [catch { 
		set sid ""
		set sid [ socket $host $port ]
	} err ] } {
        set msg "Error: could not connect to $host:$port: $err\nsending '$cmd'"
        return -code error "$msg\nerr=$sid"
    }
    fconfigure $sid -buffering line

    puts $sid $cmd
    flush $sid

    if { $wait } {
        set ::done 0
        #puts "Results of \[$cmd\]:"
        fileevent $sid readable [ list readData $sid ]
        vwait ::done
    } else {
        catch { close $sid }
    }

    return
}

proc sshPipe {host cmd {timeout 10000}} {
    set ::donePipe 0
    set retval ""
    set sshcmd [concat |ssh -x -n -obatchmode=yes $host $cmd 2>@stdout]

    if {[catch {
        set fid [open $sshcmd w+]
        fconfigure $fid -blocking off

        set afterid [after $timeout [list set ::donePipe 1]]
        fileevent $fid readable [list set ::donePipe 1]
        if {!$::donePipe} {
            vwait ::donePipe
        }
        fileevent $fid readable {}
        after cancel $afterid
        unset ::donePipe

        for {set ms 0} {$ms < 3000} {incr ms 100} {
            append retval [read -nonewline $fid]
            if {[eof $fid]} {break}
            after 100
        }

        catch {close $fid}
     } err ] } {
	 	puts stderr "Error $err while doing 'concat |ssh -x -n -obatchmode=yes $host $cmd 2>@stdout'"
        catch { 
            after cancel $afterid
            unset ::donePipe
            close $fid
        }
        return -code error "sshPipe: $err"
     }

     return $retval
}

proc tester {type host} {
    switch -exact -- $type {
        stop {puts "cleanup $host"}
        start {puts "startup $host"}
        default {}
    }

    if { [ string equal $::localhost $host ] } {
        catch { exec [file join $::testdir rundatasock.tcl] -d $::testdir $type } err
    } else {
        catch {sshPipe $host [list /usr/bin/env LD_LIBRARY_PATH=/ldas/lib:$::env(LD_LIBRARY_PATH) \
            [file join $::testdir rundatasock.tcl] -d $::testdir $type]} err
        #catch { exec ssh -n -obatchmode=yes $host \
        #    /usr/bin/env LD_LIBRARY_PATH=/ldas/lib:$::env(LD_LIBRARY_PATH) \
        #    [file join $::testdir rundatasock.tcl] -d $::testdir $type } err
    }
	puts "$type $host err $err"
    return
}

proc addhosts {hosts {restart 0}} {
    foreach host $hosts {
        if { [ lsearch -exact $::hostlist $host ] == -1 } {
            lappend ::hostlist $host
            set restart 1
        }
        if {$restart} {
            tester stop $host
            after 1000
            tester start $host
        }
    }

    after 3000
    return
}

proc cleanup {} {
    foreach host $::hostlist {
        tester stop $host
    }
    set ::hostlist {}
}

;#barecode

;## set up environment
;## may eventually start up the necessary clients and server

set API "test_driver"
source LDASapi.rsc
set hostlist {}

set cmds_sent 0
set test_count 0
set test_completed 0
set wait 1
set nowait 0

;## set some default values
set client {}
set server {}
set dims "all"
set types "all"
set thread 0
set withcont 1
set samples 10
set hostfile ""
set ilwdfmt "ascii"
set testdir "/ldas_usr/ldas/test/datasock"

catch {exec /ldas/bin/ssh-agent-mgr --agent-file=/ldas_outgoing/managerAPI/.ldas-agent --shell=tclsh check} err
eval $err
if { ! [ info exists ::env(SSH_AUTH_SOCK) ]} {
    puts $err
    puts "need to setup env(SSH_AUTH_SOCK) for this test"
    exit
}

;## process command-line arguments
for {set idx 0} {$idx < $::argc} {incr idx} {
    set arg [lindex $::argv $idx]
    switch -exact -- $arg {
        -a -
        -ascii {
            set ilwdfmt "ascii"
            set ilwdfmtCL 1
        }

        -b -
        -binary {
            set ilwdfmt "binary"
            set ilwdfmtCL 1
        }

        -d -
        -dir {
            set testdir [lindex $::argv [incr idx]]
            set testdirCL 1
        }

        default { set hostfile $arg }
    }
}

if {![string length $hostfile]} {
}

set fd [ open $hostfile r ]
set data [ split [ read $fd ] "\n" ]
close $fd

set ::localhost [lindex [split [info hostname] "."] 0]
if {[string equal -length 4 "ldas" $::localhost]} {
    set ::localhost "gateway"
}

foreach line $data {
    set line [string trim $line]
    set client {}
    set server {}

    if {[regexp {^(#|;##)} $line] || ![string length $line]} {
        continue
    }
    if {[regexp {^refresh} $line]} {
        cleanup
        continue
    }
    if {[regexp {^start} $line]} {
        set hosts [lrange [split [string trim $line]] 1 end]
        puts "hosts=$hosts"
        addhosts $hosts 1
        continue
    }
    if {[regexp {=} $line]} {
        foreach item [split $line] {
            if {[regexp {([^=\s]+)=([^=\s]+)} $item -> name value]} {
                ;## if not set on command-line
                if {[catch {set ${name}CL}]} {
                    set $name $value
                }
            }
        }
    }
    if {[string length $client] && [string length $server]} {
        set hosts "$client $server"
        if {[string equal "$client" "$server"]} {
            set hosts "$client"
        }
        addhosts "$hosts"
        incr test_count 1

        set serverCmd "serverInit $thread"
        set clientCmd "clientInit $server $dims $types $thread $samples $withcont $ilwdfmt"

        puts "client=$client, server=$server"
        puts "dims=$dims, types=$types, thread=$thread, withcont=$withcont, samples=$samples, ilwdfmt=$ilwdfmt"
        if {[catch {sendCmd $serverCmd $server $::test_server(operator) $nowait} err]} {
            puts stderr "$err\n"
            continue
        }
        incr cmds_sent 1

        after 1000
        if {[catch {sendCmd $clientCmd $client $::test_client(operator) $wait} err]} {
            puts stderr "$err\n"
            continue
        }
        incr cmds_sent 1

        incr test_completed 1
    }
}

cleanup
puts "$test_count tests, $cmds_sent cmds sent, $test_completed completed"


;## post processing
set date [ clock format [ clock seconds ] -format "%m%d" ]

puts "date $date"
cd results-$date
set cmd "exec ../fmt_times.tcl [ glob * ]"
catch { eval $cmd } err
puts $err
file mkdir 170 180 190

set cmd "exec ../datasock_stats.tcl [ glob *.log ] ../results.170 -o" 
catch { eval $cmd } err
puts $err
set cmd "file copy -force [ glob *.stats ] 170"
eval $cmd

set cmd "exec ../datasock_stats.tcl [ glob *.log ] ../results.180 -o" 
catch { eval $cmd } err
puts $err
set cmd "file copy -force [ glob *.stats ] 180"
eval $cmd

set cmd "exec ../datasock_stats.tcl [ glob *.log ] ../results.190 -o" 
catch { eval $cmd } err
puts $err
set cmd "file copy -force [ glob *.stats ] 190"
eval $cmd
