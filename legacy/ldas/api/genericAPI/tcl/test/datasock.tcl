#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

## ******************************************************** 
## dataSocket.tcl Version 1.0
##
##  test sending operator cmd to metadata to open an
##  accept socket, send ilwd object  to data socket
##  
##
## 
## ******************************************************** 

;#barecode

proc getIlwd { filename } {

    set filwd "nofd"
    if  { [ catch { 
        set filwd [ openILwdFile $filename "r" ] 
        set datap [ readElement $filwd ] 
        closeILwdFile $filwd 
        } errmsg ] } {
        catch { closeIlwdFile $filwd }
        return -code error $errmsg  
    }
    return $datap       
}

proc readData { sid cmd } {
    if  { [ eof $sid ] } {
        catch { close $sid }
        return
    }
    set result ""
    while { 1 } {
        gets $sid line
        if  { [ eof $sid ] } {
            catch { close $sid }
            break
        }
        set result "$result$line\n"
    }
    puts "Results for cmd=$cmd\n$result"
    catch { close $sid }
}

proc bgerror { msg } {
     puts stderr "${::API}API bgerror: $msg"
     set strlist [ split $msg ]
     set index [ lsearch $strlist "*sock*" ]
     if  { $index > -1 } {
         set sid [ lindex $strlist $index ]
         regsub -all  {["]} $sid {} sid 
         puts "bgerror sid = $sid."
    }
}

proc sendCmd { cmd sid } {
    puts "sending cmd = $cmd to $::host:$::operator"
    ;##catch { close $sid } 
    if { [ catch { 
        set sid [ socket $::host $::operator ]
        fconfigure $sid -buffering line
        } ] } {
        set sid "unreachable"
        set msg "could not connect to metadata at $::host:$::port"
        return -code error "socket open:\n$msg"
        }    
    puts $sid $cmd
    close $sid
    ;##fileevent $sid readable [ list readData $sid $cmd ] 
    return $sid    
}

;## sends GDS ilwd object to metadataAPI socket.

  set API metadata
  
  set LDAS [ pwd ]
  source LDASmetadata.rsc                 	
  set auto_path "/ldas/ldas-0.0/lib $auto_path"
  load /ldcg/lib/libstdc++.so.2.10.0
  package require generic
  
  proc addLogEntry { args } { return {} }
  
  # 
  set host muscida
  set dataport [ set ::${::API}(data) ]
  set operator [ set ::${::API}(operator) ]

  puts "send to $host:$dataport"  
   
  ;##set basename "/home/mlei/ilwd" 
  set basename   "/home/mlei/elmtest" 
  set numRows 10  
  ;##set numRows 5         			
  ;##set ilwdlist { frame statistics } 
  ;##set ilwdlist { frameset frame } 
  ;##set ilwdlist { frameset frame ringdown burstevent directedperiodic }
  set ilwdlist { ringdown }
  ;##set ilwdlist { program }
  ;## cannot do socket put with data socket
  
  set limit 1  
  set count 0
  set sid "nosock"
  foreach table $ilwdlist {
    
    if  { [ catch {
        set sid [ sendCmd "handleDataSockCmd updateTableSocket" $sid ]        
        set filename "$basename/${table}${numRows}.ilwd"
        puts "file=$filename"
        puts "sent element $count"
        after 1000
        set datap [ getIlwd $filename ]
        ;## mus;##t wait else wont connect properly           
        dataSend $API $datap 
        
        ;## wait for database processing
        after 5000
        incr count 1 
        } err ] } {
            puts "dataSend: $table,$err"
            break
        }
  }
  
puts "datasock exiting"  
;#end
