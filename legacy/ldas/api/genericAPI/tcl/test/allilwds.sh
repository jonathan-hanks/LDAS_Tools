#! /bin/sh

# generates all ilwds for send/recv element testing
basedir=${1}
echo "target directory ${basedir}"
echo "creating dims=1"
ilwdelements.sh 1 ${basedir}
echo "creating dims=10"
ilwdelements.sh 10 ${basedir}
echo "creating dims=100"
ilwdelements.sh 100 ${basedir}
echo "creating dims=500"
ilwdelements.sh 500 ${basedir}
echo "creating dims=1000"
ilwdelements.sh 1000 ${basedir}
echo "creating dims=2000"
ilwdelements.sh 2000 ${basedir}
echo "creating dims=5000"
ilwdelements.sh 5000 ${basedir}
echo "creating dims=10000"
ilwdelements.sh 10000 ${basedir}
echo "creating dims=100000"
ilwdelements.sh 100000 ${basedir}
echo "creating dims=1000000"
ilwdelements.sh 1000000 ${basedir}
