#!/ldcg/bin/tclsh
#!/bin/sh
#\
exec tclsh "$0" ${1+"$@"}

proc printUsage {{ opt "" }} {
    if {[string length $opt]} {
        puts stderr "Invalid option: $opt"
    }
    puts stderr "Usage: [file tail $::argv0] \[-v\] \[-r\] \[start | stop | check\]"
    exit 1
}

proc getPids {{re {}}} {
    catch {exec /bin/ps -u $::tcl_platform(user) -o pid,args} psinfo
    ;##if {![string length $psinfo]} {return {}}
    set psinfo [lrange [split $psinfo "\n"] 1 end]

    set pidlist {}
    append exp {(\d+)[\w\s\t\[\.\/]+(?:tclsh[\w\s\t\.\/]+)?} "$re"
    foreach line $psinfo {
        if {[regexp -- $exp $line -> pid]} {
            lappend pidlist $pid
        }
    }
    return $pidlist
}

proc printPids {pidlist} {
    if {![llength $pidlist]} {
        return
    }
    catch {exec /bin/ps -p "$pidlist" -o user,pid,pcpu,pmem,vsz,rss,stime,time,args >@stdout 2>@stderr} err
    return
}

proc doCheck {} {
    printPids [getPids datasock_]
    return
}

proc doStart {} {
    set localhost [lindex [split [info hostname] "."] 0]
    if {[string equal -length 4 "ldas" $localhost]} {set localhost gateway}
    set newenv LD_LIBRARY_PATH=/ldas/lib:$::env(LD_LIBRARY_PATH)

    ;## Check for pre-existing processes
    if {[llength [getPids datasock_tester]]} {
        if {!$::restart} {
            if {$::verbose} {
                puts stderr "Existing Processes:"
                doCheck
            }
            return
        }

        doStop
        after 1000
    }

    cd $::testdir
    if {![file exists $::logdir]} {
        catch {file mkdir $::logdir} err
    }

    set logfileS [file join $::logdir ${localhost}_server].log
    set logfileC [file join $::logdir ${localhost}_client].log
    foreach logfile "$logfileS $logfileC" {
        if {[file exists ${logfile}]} {
            set timestamp [clock format [file mtime ${logfile}] -format "%m%d%H%M"]
            catch {file rename -force -- ${logfile} ${logfile}.${timestamp}} err
        }
    }

    if {$::verbose} {
        puts stderr "Starting Processes:"
    }

    set testScript [file join $::testdir datasock_tester.tcl]
    catch {exec /usr/bin/env $newenv nohup $testScript test_server >& ${logfileS} &} err
    catch {exec /usr/bin/env $newenv nohup $testScript test_client >& ${logfileC} &} err

    if {$::verbose} {
        after 1000
        doCheck
    }
}

proc doStop {} {
    set pids [getPids datasock_tester]
    if {$::verbose} {
        puts stderr "Stopping Processes:"
        printPids $pids
    }

    foreach pid $pids {
        catch {exec /bin/kill -TERM $pid >& /dev/null} err
        catch {exec /bin/kill -KILL $pid >& /dev/null} err
    }
}


;## MAIN ##
set ::verbose 0
set ::restart 0
set ::cmd doCheck
set ::testdir "/ldas_usr/ldas/test/datasock"
set ::logdir ""

;## process command line arguments
for {set idx 0} {$idx < $::argc} {incr idx} {
    switch -exact -- [lindex $::argv $idx] {
        -v -
        --verbose {set ::verbose 1}

        -h -
        --help {printUsage}

        -r -
        --restart {set ::restart 1}

        -l -
        --logdir {set ::logdir [lindex $::argv [incr idx]]}

        -d -
        --testdir {set ::testdir [lindex $::argv [incr idx]]}

        start {set ::cmd doStart}
        stop {set ::cmd doStop}
        check {set ::cmd doCheck}

        default {printUsage [lindex $::argv $idx]}
    }
}

if {![string length $::logdir]} {
    set ::logdir [file join $::testdir "logs"]
    #set ::logdir $::testdir
}

$::cmd
exit 0

