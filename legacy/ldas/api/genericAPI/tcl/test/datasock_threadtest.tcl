#!/bin/sh
## The next line tells sh to execute the script using tclshexe \
exec tclsh "$0" ${1+"$@"}

## ******************************************************** 
## datasock_elmtest_client Version 1.0
##
##  test sending ilwd element types to server and
##  receiving it back from server
## ******************************************************** 

package provide datasock_test
set count 0

proc getArgs {} {
    uplevel #0 {
        set API [ lindex $argv 0 ]
    }
}

## *************************************************
## returns an ilwd object from a file
;## we use this one rather than in genericAPI to handle
;## cpu values of 0 seconds.
;## maybe obsolete by use of genericAPI function stats
proc mystats { samples } {
        set sum 0
        set S2  0
        
        set N [ llength $samples ]
		
		if	{ $N <= 1 } {
			return [ format "%.6f %.6f %3.2f" [ lindex $samples 0 ] 0 100 ]
		}
        
        ;## calculate the arithmetic mean
        foreach s $samples {
           set sum [ expr { $sum+$s } ]
        }
        set mean   [ expr { $sum/$N } ]
        
        ;## calculate the standard deviation
        foreach s $samples {
           set S2  [ expr { $S2+pow(($s-$mean),2) } ]
        }
        set S2     [ expr { $S2/($N-1) } ]
        set S      [ expr { sqrt($S2) } ]
        
        ;## calcualte the % coefficient of variation
		if	{ $mean != 0 } {
        	set cov    [ expr { ($S/$mean)*100 } ]
		} else {
			set cov		[ expr 0.0 * 100 ]
		}
        
        ;## return the values in a formatted list
        return [ format "%.6f %.6f %3.2f" $mean $S $cov ]
}
;## 100BT ethernet is 100 Mbites/sec
;## or 100/8 = 12 MBytes/sec

proc compute_rate { type dims time } {

	if	{ [ regexp {char} $type ] } {
		set size 1
	} else {
		regexp {(\d+)} $type -> size
	}
	set rate [ expr ( ( 2 * $dims * $size) / $time )/ 1000000.0 ]
	return $rate
}

proc getIlwd { type withcont dims } {
    if	{ [ catch {
    	set filename "${type}_${dims}.ilwd"
    	puts stderr "\nfile=$filename"
    	set filwd "nofd"
        set filwd [ openILwdFile $filename "r" ]
        puts stderr "\nopened file=$filename"
        set st [ clock clicks ]
        puts stderr "\nabout to read element"
        set contp [ readElement $filwd ]
        set dt [ expr { ([ clock clicks ] - $st) / 1000000.0 } ]
        puts stderr "read and created ilwd object in $dt secs"
        closeILwdFile $filwd 
		if	{ ! $withcont } {
			set datap [ refContainerElement $contp 0 ]
		} else {
			set datap $contp
		}
        } errmsg ] } {
	    puts "err=$errmsg"
        catch { closeIlwdFile $filwd }
        return -code error $errmsg  
    }
    return "$datap $contp $filename"        
}
## *************************************************
## *************************************************
proc cmpElementsOld { contp1 contp2  } {
    
    ;## compare elements 
    set result1 [ getElement $contp1 ]
    set result2 [ getElement $contp2 ]
    
    return [ string compare $result1 $result2 ]
}    

;## to avoid out of memory, write element to temp file
;## compare the files, then delete the tmp file

proc outFile { objectp } {

	set fname $objectp.ilwd
	set fout [ openILwdFile $fname "w" ] 
	writeElement $fout $objectp ascii none
   	closeILwdFile $fout
	return $fname
}

proc cmpElementFiles { fname1 fname2 } {
	
	if	{ [ catch { 
		set rc [ catch { exec3 diff $fname1 $fname2 } err ]
		if	{ $rc } {
			puts stderr "cmpElementFiles: $err"
		} 
	} error ] } {
		puts stderr "cmpElementFiles error: $error"
	}
}

proc output {} {
	uplevel #0  {
		foreach entry { wall cpu } {
			set result [ mystats [ set ${entry}_${dims}($type) ] ]
			set mean [ lindex $result 0 ] 
			set stddev [ lindex $result 1 ]
			set cov [ lindex $result 2 ]	
			if	{ ! [ string compare $entry "wall" ] } {
				set rate [ compute_rate $type $dims $mean ]
    			puts -nonewline $fout [ format "%10s\t%10s\t%s\t%s\t%s\t%s\t" \
				$type $dims $rate $mean $stddev $cov ]	
			} else {
				puts -nonewline $fout [ format "%s\t%s\t%s\t" \
				$mean $stddev $cov ]
			}
		}
		puts $fout ""
		flush $fout
		unset ${entry}_${dims}($type)
	}
}

proc retry_connect {server port} {

	 	for { set retry 0 } { $retry < 1000 } { incr retry 1 } {
	 		set clientp [ createDataSocket 0 $::localhost ]
        ;## connect to the server
        	catch { connectDataSocket $clientp $server $port } err
			if	{ [ isSocketConnected $clientp ] } {
				return $clientp
			} else {
				puts stderr "retry_connect err to $server:$port: $err, $retry time"
                catch { closeDataSocket $clientp } 
                after 100               
			}
		}
		return ""
}

;## api maybe so blocked it cannot receive another msg
;## there is a bg loop to receive data 
proc dorecvData {} {
	if	{ ! $::state } {
		;##after 100 recvDataLoop
		set ::state 1
	}
}

proc runTest { server dimset types { with_cont 1 } } {
    set ::server $server
    if  { ! [ string compare $dimset "all" ] } {
        set ::dimset $::default_dimset
    } else {
        set ::dimset [ split $dimset , ]
    }
    if  { ! [ string compare $types "all" ] } {
        set ::types $::default_types
    } else {
        set ::types [ split $types , ]
    }
	set ::withcont $with_cont
	set ::Error ""
    puts stderr "dimset=$::dimset, types=$::types,localhost=$::localhost"
    threadSendElements 
	puts stderr "\nTest completed. $::Error"
	return "Test completed $::Error"
}

;## barecode
set ::numRecv 0
set ::numSent 0
;## end
## *****************************************

