## ******************************************************** 
## globus.tcl
##
## Provides globus xio socket interface functions
## with gsi certification and authenication
##
## Provides functions for opening and closing globus sockets
## via tclglobus library interface to globus toolkit
##
## This is the module of the generic API that loads the
## genericAPI.so, since it is the functions declared here
## that use it.
##
## ******************************************************** 

;#barecode

package provide RawGlobus 1.0

;## 
;## globus libraries loaded in LDAScntlmon.ini
;## namespace eval Globus 

if { [ info exist ::GLOBUS_LOCATION ] } {
    set env("GLOBUS_LOCATION") $::GLOBUS_LOCATION
}
if	{ ![ string length [ namespace children :: RawGlobus ] ] } {
	namespace eval RawGlobus {}
}
set ::RawGlobus::waitforbytes 1

if { ! [ info exist ::RawGlobus::globuslibs ] } {
    set ::RawGlobus::globuslibs [ list globus_module \
				      globus_error \
				      globus_object \
				      globus_xio \
				      gssapi ]
}

;#end

;## tcl globus shared object may have to be loaded before 
;## loading of genericAPI to ensure the proper shared objects used
;## by globus are being loaded 
;## this is done in the API's .ini file e.g. LDAScntlmon.ini

#------------------------------------------------------------------------
# From: Practical Programming in Tcl and Tk 4th Edition pp. 139
# Assign a set of variables from a alist of values.
# If there are more values than variables, they are returned.
# If there are fewer values than variables,
# the variables get the empty string
#------------------------------------------------------------------------
proc lassign {valueList args} {
    if { [llength $args] == 0} {
	error "wrong \# args: lassign list varname ?varname..?"
    }
    if { [llength $valueList] == 0} {
	# Ensure one trip through the foreach loop
	set valueList [list {}]
    }
    uplevel 1 [list foreach $args $valueList {break}]
    return [lrange $valueList [llength $args] end]
}

## ******************************************************** 
##
## Name: globusErrLog
##
## Description:
## skip logging of canceled operation errors
##
## Usage:
##
## Comments:

proc globusErrLog { msg } {
	
    if	{ ![ regexp -nocase -- {Operation was canceled|end of file} $msg ] } {
    	addLogEntry $msg red
    } 
}
	
## ******************************************************** 
##
## Name: tclglobus data structures
##
## Description:
## allow user to initialize a new proxy
##
## Usage:
##
## Comments:
## is there a limit on buffer so need to read again

;## tcpdriver - tcp driver from globus_xio_driver_load
;## filedriver - file driver from globus_xio_driver_load
;## driver stack - driver stack from globus_xio_stack_push_driver 
;## tcphandle - tcp handle   from globus_xio_handle_create 
;## filehandle - file handle from globus_xio_handle_create 
;## buffer - to hold data for read/write operations
;## buffersize - number of bytes to read 
;## nbytes - number of bytes to write
;## contact_string - data passed during globus_xio_open 
;## waitforbytes -  wait for ?? bytes

## ******************************************************** 
##
## Name: RawGlobus::init
##
## Description:
## initialize to use the globus toolkit
##
## Usage:
##
## Comments:

proc RawGlobus::init { contact_string port type args } {
	
    set seqpt ""
  	if	{ ![ info exist ::GLOBUS_BUFFER_SIZE ] } {
    	set ::GLOBUS_BUFFER_SIZE 204800
    }

    ;## this is done in API's .ini since globus libs has conflicts with
    ;## with ldas libs
     
	# foreach lib $::RawGlobus::globuslibs {
    #	load [ eval file join $::TCLGLOBUS_DIR "swig/.libs/libtcl${lib}.so" ]
	#}
	
    if	{ [ catch {
    	;## load libraries if not already loaded
        
        if	{ ![ regexp -nocase -- {tclglobus} [ info loaded ] ] } {
    		foreach lib $::RawGlobus::globuslibs {
    			load [ eval file join $::TCLGLOBUS_DIR libtcl${lib}.so ]
        	}
		}
    #----------------------------------------------------------------
	# Activate Globus XIO module
	#----------------------------------------------------------------
    
    	if	{ ![ info exist ::RawGlobus::tcpdriver ] } {
			lassign [ globus_module_activate $::globus_i_xio_module ] result
	
	#----------------------------------------------------------------
	# Load transport driver once for first time
	#----------------------------------------------------------------

			lassign [ globus_xio_driver_load tcp ] result ::RawGlobus::tcpdriver
        
    		lassign [ globus_xio_driver_load gsi ] result ::RawGlobus::gsidriver
        }
        
	    lassign [ globus_xio_stack_init NULL ] result ::RawGlobus::server_tcpstack($port)
       
	    lassign [ globus_xio_stack_push_driver $::RawGlobus::server_tcpstack($port) \
        	$::RawGlobus::tcpdriver ] status 
           
        lassign [ globus_xio_stack_push_driver $::RawGlobus::server_tcpstack($port) \
        	$::RawGlobus::gsidriver ] status

    #----------------------------------------------------------------
	# open globus server listening port
	#----------------------------------------------------------------
    	set ::RawGlobus::server_cs($port) $contact_string 

     	RawGlobus::openGlobusPort $port $type $args
        
    } err ] } {
    	return -code error "$seqpt: $err"
    }

}

## ******************************************************** 
##
## Name: RawGlobus::cleanup
##
## Description:
## unloads the globus shared objects 
##
## Usage:
##
## Comments:

proc RawGlobus::cleanup {} {

	if	{ [ catch {
    #----------------------------------------------------------------
	# destroy the server attribute
	#----------------------------------------------------------------
    
    	if	{ [ array exist ::RawGlobus::server_attr ] } {
        	foreach port [ array names ::RawGlobus::server_attr ] {
    			lassign [ globus_xio_attr_destroy $::RawGlobus::server_attr($port) ] status
            }
            unset ::RawGlobus::server_attr
    	}
    
    	if	{ [ array exist ::RawGlobus::server_tcpstack ] } {
        	foreach port [ array names ::RawGlobus::server_tcpstack ] {
    			lassign [ globus_xio_stack_destroy $::RawGlobus::server_tcpstack($port) ] status
            }
            unset ::RawGlobus::server_tcpstack 
    	}
    
	#----------------------------------------------------------------
	# Unload both drivers
	#----------------------------------------------------------------

		if	{ [ info exist ::RawGlobus::gsidriver ] } {
    		lassign [ globus_xio_driver_unload $::RawGlobus::gsidriver ] status
        	unset ::RawGlobus::gsidriver
   	 	}
    	if	{ [ info exist ::RawGlobus::tcpdriver ] } {
			lassign [ globus_xio_driver_unload $::RawGlobus::tcpdriver ] status
        	unset ::RawGlobus::tcpdriver
    	}
    
	#----------------------------------------------------------------
	# Deactivate Globus XIO module
	#----------------------------------------------------------------
    	if	{ [ info exist ::globus_i_xio_module ] } {
			lassign [ globus_module_deactivate $::globus_i_xio_module ] status 
    	}
   	} err ] } {
    	addLogEntry $err 2
    }
}

## ******************************************************** 
##
## Name: RawGlobus::openGlobusPort
##
## Description:
## determine the users queue file to access
##
## Parameters:
## client 
## Usage:
##
## Comment:

proc RawGlobus::openGlobusPort { port type args } {

	 ;## ::bootLock OFF
     set seqpt ""
     ;## assign baseport to server
     
     if { [ catch {      
      	set seqpt "globus_xio_attr_init"
    	lassign [ globus_xio_attr_init ] result ::RawGlobus::server_attr($port)
        
        ;## do not use IPV6 protocol
        lassign [ globus_xio_attr_cntl \
			      $::RawGlobus::server_attr($port) \
			      $::RawGlobus::tcpdriver \
			      $::GLOBUS_XIO_TCP_SET_NO_IPV6 \
			      $::GLOBUS_TRUE \
			     ] status
       
        set seqpt  "globus_xio_attr_cntl $::RawGlobus::server_attr($port) $::RawGlobus::tcpdriver \
        	$::GLOBUS_XIO_TCP_SET_BACKLOG 100"
        
        lassign [ globus_xio_attr_cntl $::RawGlobus::server_attr($port) $::RawGlobus::tcpdriver \
        	$::GLOBUS_XIO_TCP_SET_BACKLOG 100 ] result
               
        set seqpt  "globus_xio_attr_cntl $::RawGlobus::server_attr($port) $::RawGlobus::tcpdriver \
        	$::GLOBUS_XIO_TCP_SET_REUSEADDR $::GLOBUS_TRUE"
        
        lassign [ globus_xio_attr_cntl $::RawGlobus::server_attr($port) $::RawGlobus::tcpdriver \
        	$::GLOBUS_XIO_TCP_SET_REUSEADDR $::GLOBUS_TRUE ] result
        
        set seqpt  "globus_xio_attr_cntl $::RawGlobus::server_attr($port) $::RawGlobus::tcpdriver \
        	$::GLOBUS_XIO_TCP_SET_PORT $port"
        
        lassign [ globus_xio_attr_cntl $::RawGlobus::server_attr($port) $::RawGlobus::tcpdriver \
        	$::GLOBUS_XIO_TCP_SET_PORT $port ] result
              
		set seqpt "get tcp port"        
        lassign [ globus_xio_attr_cntl $::RawGlobus::server_attr($port) $::RawGlobus::tcpdriver \
        	$::GLOBUS_XIO_TCP_GET_PORT ] result portset
               
    	#----------------------------------------------------------------
		# Authenication -  Set GSI authorization mode
		#---------------------------------------------------------------- 
    
    	# TCP & GSI driver specific attribute initialization
        
        if	{ [ string equal user $type ] } {
			;## authenication via user cert and user key
			set seqpt "globus_xio_attr_cntl gsi driver user cert"
	    	lassign [ globus_xio_attr_cntl \
			  	$::RawGlobus::server_attr($port) \
			  	$::RawGlobus::gsidriver \
			  	$::GLOBUS_XIO_GSI_FORCE_SERVER_MODE \
			 	$::GLOBUS_TRUE \
			 	] status
       	} else {
        	;## authenication via host cert and host key
        	set seqpt "globus_xio_attr_cntl gsi driver host cert"
	    	lassign [ globus_xio_attr_cntl \
			  	$::RawGlobus::server_attr($port) \
			  	$::RawGlobus::gsidriver \
			  	$::GLOBUS_XIO_GSI_SET_CREDENTIAL \
			 	$::gss_c_no_credential \
			 	] status
        }	       	    
 		set seqpt "globus_xio_attr_cntl tcp driver"
			lassign [ globus_xio_attr_cntl \
			  $::RawGlobus::server_attr($port) \
			  $::RawGlobus::tcpdriver \
			  $::GLOBUS_XIO_GSI_FORCE_SERVER_MODE \
			  $::GLOBUS_TRUE \
			 ] status

 		addLogEntry "$port authorized via '$type' cert"
     	# Create server object  
        set seqpt "create server object"
	    lassign [ globus_xio_server_create \
			  $::RawGlobus::server_attr($port) \
			  $::RawGlobus::server_tcpstack($port) \
			 ] result ::RawGlobus::serverObject($port)
        		
        ;## open a network handle, 
        set seqpt "create network handle"
        set args [ concat $port $args ]
        addLogEntry "args $args" purple
        lassign [ globus_xio_server_register_accept $::RawGlobus::serverObject($port) \
			[ list RawGlobus::acceptCallback [ list $args ] ] ] result 
       
     } err ] } {
       	addLogEntry "$seqpt $err" 2 
     }   
	 
     addLogEntry "GSI socket open for service at $portset with authenication via $type certificate" blue

}

## ******************************************************** 
##
## Name: RawGlobus::acceptCallback
##
## Description:
## handles accept globus connection from a client
##
## Usage:
## 
##
## Comments:
## the server may close its server socket or client may disconnect
## resulting in an non-fatal error

proc RawGlobus::acceptCallback { user_parms server handle result } {

	if	{ [ catch {
    	set port [ lindex $user_parms 0 ]
		lassign [ globus_xio_open $handle $::RawGlobus::server_cs($port) \
        	$::RawGlobus::server_attr($port)  ] result
            
    } err ] } {  
    	globusErrLog "$err; closing $handle"
        catch { RawGlobus::close $handle }
        
    }
    
    if	{ [ catch {
    	;## register another accept for the next connection
    	lassign [ globus_xio_server_register_accept $server \
			[ list RawGlobus::acceptCallback [ list $user_parms ] ] ] result 
            
    } err ] } {  
    	globusErrLog $err
    }
        
}

## ******************************************************** 
##
## Name: RawGlobus::openCallback
##
## Description:
##
##
## Usage:
## 
##
## Comments:
## user_parms is the handler for processing the data

proc RawGlobus::openCallback { user_parms handle result } {

	if	{ [ catch { 

        set callbackData [ lindex $user_parms 1 ]
		set callback [ lindex $callbackData 0 ]
    	set args [ lrange $callbackData 1 end ]
        
		lassign [ globus_xio_register_read $handle $::GLOBUS_BUFFER_SIZE \
    	 $::RawGlobus::waitforbytes NULL \
    	[ list $callback $args ] ] result         
        
    } err ] } {
    	globusErrLog $err
        catch { RawGlobus::close $handle }
    }
                 
}

## ******************************************************** 
##
## Name: RawGlobus::sendCmd
##
## Description:
## issue a command, generally to operator socket of cntlmonAPI
##
## Usage:
##
## Comments:
## later we can use remote file transfer for getting job
## files over cmonTree::getFileContents 

proc RawGlobus::sendCmd { cmd handle } {

    ;## write cmd to client
    if  { [ catch {
    	set size [ string length $cmd ] 
    	if	{ $size } { 
        	lassign [ globus_xio_write $handle \
		    $cmd \
		    $::RawGlobus::waitforbytes \
		    NULL ] result
            addLogEntry "sendCmd $size bytes result $result" purple
        } 
    } err ] } {
        addLogEntry $err 2
    }  
}

## ******************************************************** 
##
## Name: RawGlobus::sendCmdCallback
##
## Description:
## globus_xio_register_write result
##
## Usage:
##
## Comments:

proc RawGlobus::sendCmdCallback { user_parms handle result buffer data_desc } {

	if	{ [ catch {
		if	{ [ string length $result ] } {
        	error $result
        }
		debugPuts "sendCmd callback $user_parms handle $handle result $result buffer $buffer"
    } err ] } {
    	globusErrLog $err
    }        
}

## ******************************************************** 
##
## Name: RawGlobus::close
##
## Description:
## close a socket opened with a client
##
## Usage:
##
## Comments:

proc RawGlobus::close { handle args } {

    if	{ [ catch {
    	;## cancel outstanding read callback
        lassign [ globus_xio_handle_cancel_operations \
        	$handle $::GLOBUS_XIO_CANCEL_READ ] result 
        addLogEntry "cancel operation read result $result $args" purple
        
       	lassign [ globus_xio_handle_cancel_operations \
        	$handle $::GLOBUS_XIO_CANCEL_WRITE ] result 
        addLogEntry "cancel operation write result $result $args" purple
        
    	#lassign [ globus_xio_register_close $handle NULL \
    	#[ list RawGlobus::closeCallback $args ] ] result 
        
        lassign [ globus_xio_close $handle NULL ] result
        addLogEntry "closed handle $handle $args result $result" purple 
        
    } err ] } {
    	globusErrLog $err
        return -code error $err
    }
}

proc RawGlobus::closeCallback { user_parms handle result } {
	
    debugPuts "closeCallback user_parms $user_parms handle $handle result $result"
    set ::RawGlobus::closeDone 1

}

## ******************************************************** 
##
## Name: RawGlobus::serverClose
##
## Description:
## close server accept socket 
##
## Usage:
##
## Comments:

proc RawGlobus::serverClose { port } {

    if	{ [ catch {  
    	catch { unset ::RawGlobus::serverCloseDone }
    	lassign [ globus_xio_server_close \
        	$::RawGlobus::serverObject($port) ] result 
        
    } err ] } {
    	globusErrLog $err
    }
}

## ******************************************************** 
##
## Name: RawGlobus::serverCloseCallback
##
## Description:
## close server accept socket 
##
## Usage:
##
## Comments:

proc RawGlobus::serverCloseCallback { user_parms server } {

	if	{ [ catch {
		debugPuts "server close callback user_parms $user_parms server $server "
    } err ] } {
    	globusErrLog $err
    }
    set ::RawGlobus::serverCloseDone 1
}

