// System Header Files
#include "config.h"

#include <sstream>   
   
// Local Header Files
#include "eventregistry.hh"
#include "eventerrors.hh"   
#include "eventdata.hh"   

// GenericAPI Header Files
#include "genericAPI/registry.hh"
#include "genericAPI/util.hh"       
   
#include "general/Memory.hh"
#include "general/mutexlock.hh"
#include "general/unordered_map.hh"
   
// using namespace std;
using namespace eventMonitor;
   
using std::ostringstream;
using std::string;

using NAMESPACE_GENERAL_MEMORY :: unique_ptr;

using General::unordered_map;

using ILwd::LdasContainer;   
using ILwd::LdasElement;      
   

//!ignore_begin:
// When defining the methods still need to use a namespace for the class,
// otherwise perceps won't generate the documenation.   
//!ignore_end:   
   
   
//------------------------------------------------------------------------------
//
//: Default constructor.
//   
//!ignore_begin:   
// Pass NULL to unordered_map constructor as a default value for mapped type.   
//!ignore_end:
//   
eventMonitor::EventRegistry::EventRegistry() :
   unordered_map< INT_4U, EventData* >()
{
   pthread_mutex_init( &mLock, NULL );
}

   
//------------------------------------------------------------------------------
//
//: Destructor.
//   
// Destroys all current jobs in the registry.
//   
eventMonitor::EventRegistry::~EventRegistry()
{
   eraseEntries();
   pthread_mutex_destroy( &mLock );
}   

   
//------------------------------------------------------------------------------
//
//: Creates and inserts new pair object or updates existing bucket with new
//: data object.   
//   
// This method creates and inserts new pair object into the unordered_map. 
// This method will be used the very first time job is registering its data.   
//   
//!param: const INT_4U& key - A reference to the search key.
//!param: LdasContainer* const e - A pointer to the element to add.
//!param: const bool selfDestruct - A flag to trigger C++ bucket self 
//+       destruction after processing last data product.
//+       This flag should be set to FALSE (0) only for "putStandAlone" user 
//+       command.         
//   
//!return: string - String representation of result data products.
//   
//!exc: Error inserting value into the map. - Couldn't insert new key,value
//+     pair into the unordered_map.
//!exc: bad_alloc - Memory allocation failed.   
//       
const string eventMonitor::EventRegistry::updateEntry( 
   const INT_4U& key, ILwd::LdasContainer* e,
   const bool selfDestruct )
{
   // Protect C++ from Tcl: remove object from registry --->
   // take over the data object
   if( Registry::elementRegistry.removeObject( e ) == false )
   {
      ostringstream msg;
      msg << "Was not able to unregister ILWD format element: " 
          << static_cast< void* >( e ) << " for jobID=" << key;
      throw EventException( msg.str() );
   }   
   
   
   unique_ptr< LdasContainer > auto_element( e );
   EventData* data( 0 );

   
   {
      MutexLock lock( mLock );
   
      // Make sure that other thread hasn't generated the entry already
      iterator iter( find( key ) );
   
      if( iter == end() )
      {
         // Pass ownership through unique_ptr copy constructor to EventData 
         // object
         insertPair ipair( insert( value_type( key, 
                                               new EventData( key, 
                                                              auto_element,
                                                              selfDestruct ) ) ) );
   
         if( ipair.second == false )
         {
            throw EventException( INSERT_ERROR );
         }

         return "";
      }
   
      data = iter->second;
   }

   // Job entry already exists, add new data object.
   // Pass ownership of data through unique_ptr copy constructor to EventData 
   // object
   string result_elements( data->addElement( auto_element, selfDestruct ) );
     
   // string will contain result data only if selfDestruct was set to TRUE:
   if( !result_elements.empty() )
   { 
      // Final object has been processed ---> remove the entry
      killEntry( key );
   }
   
   return result_elements;
}

   
//------------------------------------------------------------------------------
//
//: Creates and inserts new pair object or updates the flag of already existing 
//: job entry.   
//   
// This method creates and inserts new pair object into the unordered_map or updates
// 'singleFrame' flag of already existing job bucket.   
//   
//!param: const INT_4U& key - A reference to the search key.
//!param: const bool& flag - A flag to indicate how frame data should be
//+       formated on output. True if all mdd data should be represented by
//+       a single frame, false - by multiple frames.   
//   
//!return: Nothing.
//   
//!exc: Error inserting value into the map. - Couldn't insert new key,value
//+     pair into the unordered_map.
//!exc: bad_alloc - Memory allocation failed.   
//       
void eventMonitor::EventRegistry::updateEntry( 
   const INT_4U& key, const bool& flag )
{
   EventData* data( 0 );
   
   {
      MutexLock lock( mLock );
   
      // Make sure that other thread hasn't generated the entry already
      iterator iter( find( key ) );
   
      if( iter == end() )
      {
         insertPair ipair( insert( value_type( key, 
                                               new EventData( key, flag ) ) ) );
   
         if( ipair.second == false )
         {
            throw EventException( INSERT_ERROR );
         }

         return;
      }
   
      data = iter->second;
   }


   // Job entry already exist, update the flag.
   return data->setSingleFrame( flag );
}
   
   
//------------------------------------------------------------------------------
//
//: Removes specified element in the registry.
//   
// This method removes specified registry element. It will delete data bucket
// for the job and remove job entry from the job registry. This method still 
// parses the content of the data bucket, and returns three newly allocated 
// ILWD format container elements if any.
//      
//!param: const key_type& key - Entry position in the registry to remove.
//
//!return: string - String containing three ILWD format elements pointers
//+        representing stateVector, multiDimData and dataBase data.      
//   
//!exc: Job doesn't exist. - Specified job doesn't exist.
//!exc: bad_alloc - Memory allocation failed.   
//   
const string eventMonitor::EventRegistry::removeEntry( const key_type& key )
{
   MutexLock lock( mLock );
   iterator iter( find( key ) );

   if( iter == end() )
   {
      throw EventException( NOT_VALID_ELEMENT );   
   }
   
   string elements( iter->second->getSortedData_Lock() );
   eraseEntry( iter );

   
   return elements;
}

   
//------------------------------------------------------------------------------
//
//: Removes specified element in the registry without obtaining final data 
//: products.   
//   
// This method removes specified registry element. It will delete data bucket
// for the job and remove job entry from the job registry.
//      
//!param: const key_type& key - Entry position in the registry to remove.
//
//!return: string - String containing three ILWD format elements pointers
//+        representing stateVector, multiDimData and dataBase data.      
//   
//!exc: Job doesn't exist. - Specified job doesn't exist.
//!exc: bad_alloc - Memory allocation failed.   
//   
void eventMonitor::EventRegistry::killEntry( const key_type& key )
{
   MutexLock lock( mLock );
   iterator iter( find( key ) );

   if( iter == end() )
   {
      throw EventException( NOT_VALID_ELEMENT );   
   }
   
   eraseEntry( iter );
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Erases specified element in the registry.
//   
// This method erases specified registry element in thread unsafe way: caller
// must lock the hash before calling this method. It also invalidates iterator, 
// DON'T use it after calling this method.   
//      
//!param: iterator pos - Entry position in the registry to erase.
//
//!return: Nothing.
//   
//!exc: Job doesn't exist. - Specified job doesn't exist.
//   
void eventMonitor::EventRegistry::eraseEntry( iterator& pos )
{
   delete pos->second;
   pos->second = 0;
   
   erase( pos );
   return;   
}

   
//------------------------------------------------------------------------------
//
//: Erases all entries in the registry.
//   
// This method erases all entries in the event registry.
//
//!return: Nothing.
//   
void eventMonitor::EventRegistry::eraseEntries()
{
   MutexLock lock( mLock );
   
   for( iterator iter = begin(), end_iter = end(); iter != end_iter; ++iter )
   {
      // Destruct data element
      delete iter->second;
      iter->second = 0;
   }
   
   erase( begin(), end() );
   
   return;
}      
   
   
//------------------------------------------------------------------------------
//
//: Extracts metadata of ILWD format data elements for specified job.
//   
//!param: const INT_4U& key - A reference to the job ID.
//
//!return: string - String containing the metadata for the job.
//
//!exc: Job doesn't exist. - Specified job doesn't exist.      
//!exc: bad_alloc - Memory allocation failed.   
//
const string eventMonitor::EventRegistry::listEntry( const INT_4U& key ) const
{
   const_iterator iter( find( key ) );
   
   if( iter == end() )
   {
      throw EventException( NOT_VALID_ELEMENT );   
   }
   
   // getMetaData locks the data bucket of the job
   return iter->second->getMetaData();
}
   
   
//------------------------------------------------------------------------------
//
//: Extracts metadata of ILWD format data elements for all jobs in the registry.
//   
//!return: string - String containing the metadata for all jobs.
//
//!exc: bad_alloc - Memory allocation failed.
//   
const string eventMonitor::EventRegistry::listAllEntries() const
{
   ostringstream result;
   
   for( const_iterator iter = begin(), end_iter = end(); 
        iter != end_iter; ++iter )
   {
      result << "{";
      result << iter->first;
      result << iter->second->getMetaData();
      result << "}";
   }
   
   
   return result.str();
}
   
   
   
   
