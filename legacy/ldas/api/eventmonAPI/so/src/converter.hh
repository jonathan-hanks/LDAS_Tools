#ifndef EventConverterHH
#define EventConverterHH   

// System Header Files
#include <list>   
#include <string>   
#include <stdexcept>   
   
// General Header Files
#include <general/types.hh>   
#include <general/util.hh>   
   
   
// Forward declarations.
namespace ILwd
{
   class LdasContainer;
}
   
namespace eventMonitor
{
   class EventException;
   
   
//------------------------------------------------------------------------------
//
//: Base converter class.
//    
// This is a pure virtual base class for all conversion classes.
//       
class Converter
{
public:   

   // Default constructor
   Converter();

   // Destructor
   virtual ~Converter() = 0;

   const std::string getError() const;
   bool hasError() const;  
   
protected:
   
   typedef std::list< ILwd::LdasContainer* > containerList; 
   typedef containerList::const_iterator list_const_iterator;
   typedef containerList::iterator list_iterator;   
   
   void setError( const EventException& exc );
   void setError( const std::exception& exc );
   
   const std::string& getmError() const;
   
private:
   
   // No copy constructor
   Converter( const Converter& );
   
   // No assignment operator
   const Converter& operator=( const Converter& );   
   
   //: Error message for functional.
   std::string mError; 
   
};
   
   
//------------------------------------------------------------------------------
//
//: Gets error message if any.
//
// This method gets a reference to the convertion error message if any.
//   
//!return: const std::string& - Error message.
//      
inline const std::string& Converter::getmError() const 
{
   return mError;
}
   
   
}
   
   
#endif
