// Local Header Files
#include "eventdata.hh"
#include "eventdataelement.hh"   
#include "eventerrors.hh"   
#include "dbconverter.hh"   
#include "frameconverter.hh"   

// System Header Files
#include <algorithm>   
#include <iterator>   
#include <sstream>   
   
#include "general/Memory.hh"

// GenericAPI Header Files
#include <genericAPI/registry.hh>      
#include <genericAPI/util.hh>   

// General Header Files
#include <general/mutexlock.hh>   

// ILWD Header Files
#include <ilwd/ldascontainer.hh>  
   
using namespace std;

using namespace eventMonitor;

using ILwd::LdasContainer;   
   
//------------------------------------------------------------------------------
//
//: Constructor.
//   
//!param: const INT_4U& id - Job ID associated with an element, passed by Tcl
//+       layer.        
//!param: unique_ptr< LdasContainer >& e - An unique_ptr object storing
//+       an ILWD format element to add to the current event data bucket.
//!param: const bool selfDestruct - A flag to trigger C++ bucket self 
//+       destruction after processing last data product.
//+       This flag should be set to FALSE (0) only for "putStandAlone" user 
//+       command.            
//   
//!exc: Malformed data stream. - Malformed data stream is detected: final
//+     process information element is passed before initial process 
//+     information element.   
//!exc: NULL ILWD element. - Null ILWD format element was passed for data
//+     bucket insertion.   
//!exc: ILWD element doesn't match jobID. - Inconsistent jobID for data element.
//!exc: ILWD element exists. - Specified element for insertion already exists 
//+     in data bucket.      
//!exc: bad_alloc - Memory allocation failed.   
//   
eventMonitor::EventData::EventData( const INT_4U& id,
   std::unique_ptr< ILwd::LdasContainer >& e, const bool selfDestruct )
   : mID( id ), mWrapperCount( 0 ), mReceivedCount( 0 ), mSingleFrame( false ),
     mProcessList(), mState( 0 ), mMDD( 0 ), mDB( 0 ) 
{
   // Initialize object mutex
   pthread_mutex_init( &mLock, NULL );
   
   // Add ILWD element
   try
   {
      // The result string must be empty
      if( !addElement( e, selfDestruct ).empty() )
      { 
         throw EventException( MALFORMED_DATA_STREAM );
      }
   }
   catch( ... )
   {
      // Needed only if "e" represents process info, and it has already got
      // inserted into the internal mProcessData list.
      cleanup();
      throw;      
   }
}
   

//------------------------------------------------------------------------------
//
//: Constructor.
//   
//!param: const INT_4U& id - Job ID associated with a flag, passed by Tcl
//+       layer.           
//!param: const bool& single_frame - Flag to specify how to format frame data:
//+       true if all job data will be represented by a single frame, false -
//+       if by multiple frames.
//   
eventMonitor::EventData::EventData( const INT_4U& id, const bool& single_frame )
   : mID( id ), mWrapperCount( 0 ), mReceivedCount( 0 ), mSingleFrame( single_frame ),
     mProcessList(), mState( 0 ), mMDD( 0 ), mDB( 0 )    
{
   // Initialize object mutex
   pthread_mutex_init( &mLock, NULL );   
}
   
   
//------------------------------------------------------------------------------
//
//: Destructor.
//   
eventMonitor::EventData::~EventData()
{
   cleanup();
}

   
//------------------------------------------------------------------------------
//
//: Cleanup.
//   
void eventMonitor::EventData::cleanup()
{
   eraseProcessList();
   pthread_mutex_destroy( &mLock );      
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Adds element to data bucket.
// 
// This method actually inserts an ILWD format element into the job data 
// bucket. When destructing the bucket element, caller must remember to
// remove that element from ObjectRegistry< LdasElement* >.
//   
//!param: LdasContainer* const e - A pointer to the element to add.
//!param: const bool selfDestruct - A flag to trigger C++ bucket self 
//+       destruction after processing last data product.
//+       This flag should be set to FALSE (0) only for "putStandAlone" user 
//+       command.               
//          
//!return: const string - String containing three pointers to ILWD format 
//+        elements if <i>e</i> element to add to data bucket is final process
//+        information, empty string otherwise.   
//   
//!exc: NULL ILWD element. - Null ILWD format element was passed for data
//+     bucket insertion.   
//!exc: ILWD element doesn't match jobID. - Inconsistent jobID for data element.
//!exc: ILWD element exists. - Specified element for insertion already exists 
//+     in data bucket.   
//!exc: bad_alloc - Memory allocation failed.
//!exc: bad_cast - Malformed input data.   
//   
const std::string eventMonitor::EventData::addElement( 
   std::unique_ptr< ILwd::LdasContainer >& e, const bool selfDestruct )
{
   if( e.get() == 0 )
   {
      throw EventException( NULL_ILWD_ELEMENT );
   }

   
   if( mID != e->getJobId() )
   {
      ostringstream s;
      s << ": expected jobID = " << mID 
        << " elementID = " << e->getJobId();

      throw EventException( MISMATCHED_JOBID, s.str() );    
   }

   
   // Pass object ownership to the EventDataElement:
   // from this point on 'e' doesn't point to anything (use 'element' instead)
   EventDataElement element( e );   

   
   MutexLock lock( mLock );      
   ++mReceivedCount;
   

   // Object represents process data
   if ( element.hasData( EventDataElement::PROCESS ) )
   {
      // There are only 2 process elements generated per job.
      if( mProcessList.size() == 2 )
      {
         throw EventException( "Job already has 2 process elements." );
      }
   

      if( mProcessList.size() && isMember( element.getData() ) )
      {
         throw EventException( EXISTING_ILWD_ELEMENT );    
      }   

   
      // Pass ownership of the process data to the EventData:
      unique_ptr< LdasContainer > process( element.releaseData() );
      mProcessList.push_back( process.release() );

      // check for the number of objects job has generated within wrapperAPI:
      checkWrapperCount();   
   }
   else
   {
      sortData( element );
   }
   
   
   // If all expected elements have been received, and C++ bucket 
   // is self destructive, process it:
   if( mWrapperCount == mReceivedCount && selfDestruct == true )
   {
      return getSortedData();
   }

   // If selfDestruct is disabled, make sure number of received 
   // data objects doesn't exceed expected number of objects
   if( mWrapperCount && mWrapperCount < mReceivedCount )
   {
      throw EventException( "Number of received data products exceeded expected number for the job." );      
   }
   
   return "";
}
   
   
//------------------------------------------------------------------------------
//
//: Gets metadata on all ILWD format elements in the data bucket
//    
// This method parses all ILWD format data elements and extracts
// stateVector, dataBase and multiDimData metadata.
//
//!return: string - A string containing metadata for all ILWD elements. 
//+        The following format is used for each element, example contains all
//+        types of data:   
//+        <p>{_XXXXXX_LdasElement_p Process State 0 1 2 MDD 0 1 DB 1 2 }
//         
//!exc: bad_alloc - Memory allocation failed.
//   
const string eventMonitor::EventData::getMetaData()
{
   MutexLock lock( mLock );
   
   ostringstream elements;
   
   elements << "Process { ";
   copy( mProcessList.begin(), mProcessList.end(),
         ostream_iterator< const LdasContainer* >( elements, " " ) );         
   
   elements << "receivedObjectCount=" << mReceivedCount
            << " expectedObjectCount=" << mWrapperCount;

   elements << " } State { " << static_cast< const void* >( mState.get() )
            << " } MDD { " << static_cast< const void* >( mMDD.get() )
            << " } DB { " << static_cast< const void* >( mDB.get() ) 
            << " }";
   
   return elements.str();
}

   
//------------------------------------------------------------------------------
//
//: Gets sorted ILWD format data for the job.
//    
// This method parses all job ILWD format data elements and extracts
// stateVector, dataBase and multiDimData data into stand alone ILWD format 
// elements.   
// It locks the data bucket for the job while doing the sorting.   
//
//!return: string - A string containing three ILWD elements pointers
//+        representing the stateVector, multiDimData and dataBase data for 
//+        the job.
//
//!exc: NULL index list for the data. - Specified type of data doesn't contain
//+     any indices information.
//!exc: bad_alloc - Memory allocation failed.
//!exc: bad_cast - Malformed ILWD format data.      
//            
const string eventMonitor::EventData::getSortedData_Lock()
{
   MutexLock lock( mLock );
   return getSortedData();
}


//------------------------------------------------------------------------------
//
//: Sets frame format flag.
//       
// This method sets frame format flag for the job.
//
//!param: const bool& flag - Flag value.
//
//!return: Nothing.
//   
void eventMonitor::EventData::setSingleFrame( const bool& flag )
{
   MutexLock lock( mLock );   
   mSingleFrame = flag;
   
   if( mMDD.get() )
   {
      mMDD->setSingleFrame( flag );
   }
   return;
}


//------------------------------------------------------------------------------
//
//: Sort ILWD format data for the job.
//    
// This method parses a single ILWD format data element for the job and extracts
// stateVector, dataBase and multiDimData data into stand alone ILWD format 
// elements.   
//
//!return: Nothing.
//
//!exc: Empty list for requested type of data. - Specified type of data doesn't
//+     exist in the ILWD format element.   
//!exc: bad_alloc - Memory allocation failed.
//!exc: bad_cast - Malformed ILWD format data.      
//            
void eventMonitor::EventData::sortData( const EventDataElement& elem )
{
   if( elem.hasData( EventDataElement::STATE ) )
   {
      // List of stateVector ILWD containers, which are
      // the shallow copies of original data.
      const dataList temp( elem.getData( EventDataElement::STATE ) );
   
      if( mState.get() == 0 )
      {
         mState.reset( new LdasContainer( "stateVector" ) );
      }
   
      updateState( temp );
   }

   if( elem.hasData( EventDataElement::MDD ) )
   {
      // List of multiDimData ILWD containers, which are
      // the shallow pointers into the original data.
      const dataList temp( elem.getData( EventDataElement::MDD ) );
   
      if( mMDD.get() == 0 )
      {
         // Create frame converter
         mMDD.reset( new FrameConverter( mSingleFrame ) );
      }
   
      mMDD->storeData( temp );
   }   
   
   if( elem.hasData( EventDataElement::DB ) )
   {
      // List of dataBase ILWD containers, which are
      // the shallow copies of original data.
      const dataList temp( elem.getData( EventDataElement::DB ) );
   
      if( mDB.get() == 0 )
      {
         // Create database converter
         INT_4S data_seconds( 0 );
         string error_msg;
         try
         {
            data_seconds = elem.getAnalyzedSeconds();
         }
         catch( const EventException& exc )
         {
            error_msg = exc.what();
         }
         catch(...)
         {
            throw;
         }
  
         mDB.reset( new DataBaseConverter( data_seconds, error_msg ) );
      }  
   
      mDB->addData( temp );
   }   

   return;
}

   
//------------------------------------------------------------------------------
//
//: Adds new elements to the state ILwd format container.. 
//   
// This method appends all elements of the passed ILWD container to the mState
// container. Deep copy is done for added container elements.
//   
//!param: const dataList& states - A list of state ILwd containers to copy.
//   
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
void eventMonitor::EventData::updateState( const EventData::dataList& states )
{
   for( const_iterator iter = states.begin(), end_iter = states.end();
        iter != end_iter; ++iter )
   {
      *mState += **iter;
   }
   
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Gets sorted ILWD format data for the job.
//    
// This method parses all job ILWD format data elements and extracts
// stateVector, dataBase and multiDimData data into stand alone ILWD format 
// elements.   
//
//!return: string - A string containing three ILWD elements pointers
//+        representing the stateVector, multiDimData and dataBase data for 
//+        the job.
//
//!exc: NULL index list for the data. - Specified type of data doesn't contain
//+     any indices information.
//!exc: bad_alloc - Memory allocation failed.
//!exc: bad_cast - Malformed ILWD format data.      
//         
const string eventMonitor::EventData::getSortedData()
{
   string elements;
   ostringstream id;
   
   // Register job ID
   id << mID << ' ';
   elements += id.str();

   
   // Register state
   elements += registerObject( "", mState.get() );
   // State object is registered with elementRegistry from now on
   // ===> release its memory from unique_ptr
   mState.release();
   
   
   // Register mdd
   if( mMDD.get() == 0 )
   {
      elements += registerObject();   
   }
   else
   {
      // Process all accumulated mdd structures if any
      mMDD->addData();
   
      // Format MDD data into frameAPI expected format   
      // (if there is an error, all_mdd will be set to NULL)
      unique_ptr< LdasContainer > all_mdd( mMDD->getILWD( mID ).release( ) );
      elements += registerObject( mMDD->getError(), all_mdd.get() );         
   
      // all_mdd is a registered object now, release it from unique_ptr
      all_mdd.release();         
   
      // Done with FrameConverter, cleanup
      mMDD.reset();
   }
    

   // Register db
   if( mDB.get() == 0 )
   {
      elements += registerObject();
   }
   else
   {
      // Format dataBase data into metadataAPI expected format
      unique_ptr< LdasContainer > all_db( mDB->getILWD( mProcessList ) );
      elements += registerObject( mDB->getError(), all_db.get() );

      // all_db is a registered object now, release pointer from unique_ptr
      all_db.release();
   
      // Done with DBConverter, cleanup
      mDB.reset();
   }
   
   return elements;      
}
   
   
//------------------------------------------------------------------------------
//
//: Checks if element is already in data bucket.
//    
// This method checks if specified element is already inserted into the
// data bucket.
//
//!param: const LdasContainer* const e - A pointer to the element.
//   
//!return: const bool - True if element is already in data bucket, false
//+        otherwise.
//   
bool eventMonitor::EventData::isMember( 
   const ILwd::LdasContainer* const e ) const
{
   return ( find( mProcessList.begin(), mProcessList.end(), e )
            != mProcessList.end() );   
}

   
//------------------------------------------------------------------------------
//
//: Checks if element contains wrapper object information.
//    
// This method checks if the element at the end of the bucket contains
// information on number of objects job has generated within wrapperAPI.
//
//!return: Nothing
//   
void eventMonitor::EventData::checkWrapperCount() 
{
   // This method gets called every time new process element is inserted 
   // into the process list ===> it's guaranteed to have at least one element
   static const string meta( "wrapperApiObjects" );
   try
   {
      const string meta_val( mProcessList.back()->getMetadata<std::string>( meta ) );

      if( meta_val.empty() )
      {
         string msg( ": value is missing for " );
         msg += meta;
   
         throw EventException( MALFORMED_ILWD, msg );   
      }
         
      istringstream s( meta_val );
      INT_4U tmp( 0 );
      s >> tmp;
   
      if( !s || tmp == 0 )
      {
         string msg( ": bad data for " );
         msg += meta;
         msg += " value (";
         msg += meta_val;
         msg += ')';
   
         throw EventException( MALFORMED_ILWD, msg );
      }
   
      mWrapperCount = tmp;
      return;
   }
   catch( const runtime_error& exc )
   {
      // ILWD element doesn't have specified metadata
      return;
   }
   
   return;
}
   

//------------------------------------------------------------------------------
//
//: Erases mProcessList.
//    
// This method frees memory for mProcesslist objects.
//
//!return: Nothing.
//      
void eventMonitor::EventData::eraseProcessList()
{
   MutexLock lock( mLock );
   
   for( iterator iter = mProcessList.begin(), end_iter = mProcessList.end(); 
        iter != end_iter; ++iter )
   {
      delete *iter;
      *iter = 0;
   }   
   
   mProcessList.clear();
   return;
}


//------------------------------------------------------------------------------
//
//: Registers ILWD format element.
//       
// This method registers passed to it ILWD format element into 
// Registry::elementRegistry before returning the element to the Tcl layer.
//   
//!param: const std::string& msg - A reference to the error message if any. 
//+       Default is empty message.   
//!param: ILwd::LdasContainer* const e - A pointer to the ILWD format object
//+       to register. Default is NULL.
//   
//!return: const string - String representation of the element pointer.
//   
const string eventMonitor::EventData::registerObject( 
   const std::string& msg, ILwd::LdasContainer* const e ) const
{
   string result_msg;
   if( e == 0 )   
   {
      result_msg = '0';
   }
   else
   {
      e->setJobId( mID );
      Registry::elementRegistry.registerObject( e );                   
      result_msg = createElementPointer( e );               
   }

   result_msg += " {";
   if( msg.empty() == false )
   {
      result_msg += msg;
   }
   
   result_msg += "} ";
   return result_msg;
}

   
   
   
   
   
   
   
