// System Header Files   
#include <memory>
#include <sstream>
#include <typeinfo>
   
// Local Header Files
#include "dbconverter.hh"
#include "eventerrors.hh"   

#include "general/Memory.hh"

// ILWD Header Files
#include <ilwd/ldasarray.hh>   
#include <ilwd/ldascontainer.hh>      

// dbaccess Header Files
#include "dbaccess/FieldUniqueId.hh"
#include "dbaccess/Transaction.hh"
#include "dbaccess/Table.hh"
#include "dbaccess/Process.hh"
#include "dbaccess/ProcessParams.hh"
   
using namespace eventMonitor;
using namespace DB;
   
using NAMESPACE_GENERAL_MEMORY :: unique_ptr;

using std::bad_alloc;
using std::bad_cast;
using std::ostringstream;
using std::runtime_error;
using std::string;
using std::exception;

using ILwd::LdasContainer;   
using ILwd::LdasElement;   
using ILwd::LdasArray;
using ILwd::LdasArrayBase;   
using DB::Table;

using ILwd::ID_ILWD;   
using ILwd::ID_CHAR;
using ILwd::ID_CHAR_U; 
using ILwd::ID_INT_2S;
using ILwd::ID_INT_2U;
using ILwd::ID_INT_4S;
using ILwd::ID_INT_4U;
using ILwd::ID_INT_8S;
using ILwd::ID_INT_8U;
using ILwd::ID_REAL_4;
using ILwd::ID_REAL_8;
using ILwd::ID_COMPLEX_8;
using ILwd::ID_COMPLEX_16;    
using ILwd::ElementId;
   
   
// Static data members initialization   
const INT_4U DataBaseConverter::mTableNameIndex( 1 );
const INT_4U DataBaseConverter::mColumnNameIndex( 0 );      
const string DataBaseConverter::mDBComment( "metadata" );   
INT_4S DataBaseConverter::mMaxNumberRows( -1 );   

   
//------------------------------------------------------------------------------
//
//: Default constructor.
//   
eventMonitor::DataBaseConverter::DataBaseConverter( const INT_4S dt,
   const std::string& error )
  : Converter(), mAnalyzedSeconds( dt ), mTableHash()
{
   setError( error );
}

   
//------------------------------------------------------------------------------
//
//: Destructor.
//   
eventMonitor::DataBaseConverter::~DataBaseConverter()
{
   for( iterator iter = mTableHash.begin(), end_iter = mTableHash.end();
        iter != end_iter; ++iter )
   {
      delete iter->second;
      iter->second = 0;
   }

   mTableHash.erase( mTableHash.begin(), mTableHash.end() );   
}
   
   
//------------------------------------------------------------------------------
//
//: Adds dataBase list.
//   
// This method adds list of ILWD format containers that represent dataBase data 
// for one particular array of outPut structures to the specific to the job ILWD 
// format database element. 
// Each container in the list represents the dataBase linked list for one 
// one outPut C data structure received from the wrapperAPI job.
//
//!param: const list< const LdasContainer* >& db - A reference to the list of 
//+       dataBase linked lists in ILWD format.   
//   
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: Error inserting entry into the map. - Unsuccessful attempt to insert 
//+     new entry into the map.   
//!exc: runtime_error - Unknown column name.     
//!exc: Number of database rows per number of analyzed data seconds
//+     exceeds maximum allowed.       
//   
void eventMonitor::DataBaseConverter::addData( 
   const containerList& db )
try   
{
   // If there was already an error during DB conversion, don't proceed.
   if( getmError().size() )
   {
      return;
   }
   
   
   INT_4S num( 0 );

   // Each container represents a C dataBase linked list of outPut structure, 
   // that may contain entries for different tables   
   for( list_const_iterator iter = db.begin(), end_iter = db.end();
        iter != end_iter; ++iter )
   {
      // Each iterator is a dataBase linked list, that may contain 
      // data for different tables.
      // each container element has name = 'column_name:table_name:dataBase'
      insertDataBase( *iter );  
      //checkRowNumber();
   
      // There is limit on max number of db rows
      if( mMaxNumberRows >= 0 )
      {
         // Check number of rows job has generated so far
         num = 0;
         for( const_iterator table_iter = mTableHash.begin(), 
              end_table_iter = mTableHash.end();
              table_iter != end_table_iter; ++table_iter )
         {
            num += table_iter->second->GetRowCount();
         }
         
         REAL_8 num_rows( static_cast< REAL_8 >( num ) / mAnalyzedSeconds );
         if( num_rows > mMaxNumberRows )
         {
            ostringstream msg;
	    msg << "Number of database rows generated ("
		<< num << ") exceeds the maximum of "
		<< ( mAnalyzedSeconds * mMaxNumberRows )
		<< " (Seconds Analyzed: "<< mAnalyzedSeconds
		<< " Allowed Rows Per Analyzed Second: " << mMaxNumberRows
		<< ")"
	      ;
            throw EventException( msg.str() );
         } 
      }
   }
   
   return;
}
catch( const EventException& exc )
{
   setError( exc );
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Gets database data for the job in ILWD format.
//
// This method creates ILWD format element that represents database tables
// and process information for the whole job.   
//   
//!param: list< const LdasContainer* >& p - A reference to the list of
//+       ILWD format elements representing the job process and process 
//+       parameters information.    
//   
//!return: LdasContainer* - A pointer to the ILWD format container 
//+        representing the database data.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Process information is missing for the job. - ILWD format element 
//+     representing the process and process parameter information is 
//+     missing.   
//!exc: Required field found to be empty. - Some of the required fields in 
//+     the table are empty.   
//   
ILwd::LdasContainer* eventMonitor::DataBaseConverter::getILWD( 
   containerList& p )
try   
{
   if( mTableHash.size() == 0 || getmError().size() )
   {
      return 0;
   }
   
   if( p.size() )
   {
      const LdasContainer* elem( p.back() );  
    
      // Use very last element of the list
      if( elem->size() != 2 || 
          ( *elem )[ 0 ] == 0 ||
          ( *elem )[ 1 ] == 0  )
      {
         setError( EventException ( MALFORMED_ILWD, 
                   "( process ILWD element must have 2 sub-elements )" ) );
         return 0;
      }
   
   
      DB::Transaction request;
      static const bool own( true );
      DB::Table* process( DB::Table::CreateTable( 
                             DB::Process::TABLE_NAME ) );
      request.AppendTable( process, own );
   
      DB::Table* process_param( DB::Table::CreateTable( 
                                   DB::ProcessParams::TABLE_NAME ) ); 
      request.AppendTable( process_param, own );
   
      process->AppendILwd( 
         dynamic_cast< const LdasContainer& >( *( ( *elem )[ 0 ] ) ) );
      process_param->AppendILwd( 
         dynamic_cast< const LdasContainer& >( *( ( *elem )[ 1 ] ) ) );   
   
      // There is always only one process per data: all database
      // ILWD elements are generated by the same wrapperAPI job.
      INT_4S column_offset( process->HasField( DB::Process::PROCESS_ID ) );
      string key( process->GetFieldUniqueId( column_offset ).GetDataAsString( 0 ) );
   

      for( iterator iter = mTableHash.begin(), end_iter = mTableHash.end();
           iter != end_iter; ++iter )
      {
         // Insert unique ID into the table
         iter->second->AppendNCopies( Process::PROCESS_ID, 
            iter->second->GetRowCount(), key.c_str(), 0 );

         // Attach table to the generic request
         request.AppendTable( iter->second, !own );
      }   
  
      unique_ptr< LdasContainer > c( new LdasContainer() );
      request.GetILwd( c.get() );
   
      c->setComment( mDBComment );
      return c.release();
   }
   
   setError( EventException ( MALFORMED_ILWD, 
             "( process information is missing for the job )" ) );

   return 0;
}
catch( const bad_alloc& msg )
{
   throw;
}   
catch( const exception& exc )
{
   setError( exc );
   return 0;
}

   
//------------------------------------------------------------------------------
//
//: Set maximum number of db rows per second of analyzed data.
//      
// This method sets maximum number of rows per second of analyzed data
// a single job can generate for the database insertion.
// If set to -1, number is unlimited. By default this value is set to -1.   
//   
//!param: const INT_4S max - Maximum number of rows to allow into the database.
//
//!return: Nothing.
//   
void eventMonitor::DataBaseConverter::setMaxNumberRows( const INT_4S max )
{
   mMaxNumberRows = max;
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Inserts dataBase linked list into the job database.
//   
// This method adds C dataBase linked list in ILWD format to the job data base.
//
//!param: const LdasContainer* db - dataBase linked list in ILWD format.
//   
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Error inserting entry into the map. - Unsuccessful attempt to insert 
//+     new entry into the map.   
//!exc: Unsupported database datatype. - Column data type is not supported by
//+     database.   
//!exc: Unknown column name: column_name - Specified <i>column_name</i> doesn't
//+     exist in the table.
//!exc: Malformed ILWD element. - ILWD format element representing the column is 
//+     malformed( is not ILWD array ).          
//!exc: unknown table requested: table_name - <i>table_name</i> doesn't 
//+     correspond to any existing tables in database.      
//      
void eventMonitor::DataBaseConverter::insertDataBase( 
   const ILwd::LdasContainer* db )
{
   for( LdasContainer::const_iterator iter = db->begin(), end_iter = db->end();
        iter != end_iter; ++iter )
   {
      updateTableHash( *iter );
   }
   
   return;
}


//------------------------------------------------------------------------------
//
//: Inserts dataBase column into the job database.
//   
// This method adds dataBase column represented by ILWD format element to 
// the appropriate data base table. If job doesn't have table to which column
// belongs, it will generate that Table.   
//
//!param: const LdasElement* elem - A pointer to the ILWD format column.
//   
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: Unsupported database data type. - Column data type is not supported.
//!exc: Unknown column name: column_name - Specified <i>column_name</i> doesn't
//+     exist in the table.
//!exc: Malformed ILWD element. - ILWD format element representing the column is 
//+     malformed( is not ILWD array ).       
//!exc: Error inserting entry into the map. - Unsuccessful attempt to insert 
//+     new table into the map.   
//!exc: unknown table requested: table_name - <i>table_name</i> doesn't 
//+     correspond to any existing tables in database.   
//
void eventMonitor::DataBaseConverter::updateTableHash( 
   const ILwd::LdasElement* elem )   
try   
{
   const string table_name( elem->getName( mTableNameIndex ) );
   Table* table( 0 );
   
   iterator iter( mTableHash.find( table_name ) );

   // There is no table generated yet   
   if( iter == mTableHash.end() )
   {
      table = Table::CreateTable( table_name );
      insertPair p( mTableHash.insert( value_type( table_name, table ) ) ); 

      if( !p.second ) 
      {
         throw EventException( INSERT_ERROR );
      }
   }
   else
   {
      table = iter->second;
   }

   return insertTableColumn( table, elem );
} 
catch( const runtime_error& exc )
{
   throw EventException( exc.what() );
}
   

//------------------------------------------------------------------------------
//
//: Constructs blob data into a single buffer.
//      
// This method is used to combine all blob data for the column into
// single data buffer.   
//   
//!param: const LdasContainer* c - A pointer to the ILWD format container
//+       representing the column data.
//!param: INT_4S* dims - An array of dimensions for the blob data.   
//   
//!return: const T* - A pointer to the blob data buffer.
//
//!exc: Malformed ILWD element( inconsistent datatype for blob data ). -
//+     Inconsistent datatypes appear in blob data ILWD format container.
//!exc: Malformed ILWD element. - Malformed input ILWD format element.
//!exc: bad_alloc - Memory allocation failed.   
//   
template< class T > const T* eventMonitor::DataBaseConverter::getBlobData( 
   const ILwd::LdasContainer* c, INT_4S* dims )
try   
{ 
   LdasContainer::const_iterator iter( c->begin() ), 
                                 end_iter( c->end() );      

   ElementId id( ( *iter )->getElementId() );                                    

   INT_4S* dims_elem( dims );
   INT_4S num_data( 0 );
   
   while( iter != end_iter )
   {
      if( ( *iter )->getElementId() != id )
      {
         throw EventException( MALFORMED_ILWD, "( inconsistent datatype "
                               "for blob data )" );
      }
   
      *dims_elem = ( dynamic_cast< const LdasArrayBase* >( *iter ) )->getNData();
      num_data += *dims_elem;
   
      ++dims_elem;
      ++iter;
   }

   
   T* data( new T[ num_data ] );
   T* data_ptr( data );
   dims_elem = dims;
   
   iter = c->begin();
   
   while( iter != end_iter )
   {
      memcpy( data_ptr, ( dynamic_cast< const LdasArray< T >* >( *iter ) )->getData(),
              *dims_elem );
   
      data_ptr += *dims_elem;
   
      ++dims_elem;
      ++iter;
   }
   
   return data;
}
catch( const bad_cast& exc )
{
   throw EventException( MALFORMED_ILWD );
}


// Instantiate templates   
template const CHAR* DataBaseConverter::getBlobData< CHAR >( 
   const LdasContainer* c, INT_4S* dims );
   
template const CHAR_U* DataBaseConverter::getBlobData< CHAR_U >(
   const LdasContainer* c, INT_4S* dims );
   

//------------------------------------------------------------------------------
//
//: Inserts dataBase column into specified table.
//   
// This method inserts column data into specified database table.
//
//!param: Table* const t - A pointer to the database table to insert column into.   
//!param: const LdasElement* elem - A pointer to the ILWD format column data.
//   
//!return: Nothing.
//         
//!exc: Unsupported database datatype: type. - Column data type is not supported  
//+     by database.
//!exc: Unknown column name: column_name - Specified <i>column_name</i> doesn't
//+     exist in the table.
//!exc: Malformed ILWD element. - ILWD format element representing the column is 
//+     malformed( is not ILWD array ).    
//!exc: Malformed ILWD element( empty blob container ). - ILWD format 
//+     container representing the blob data column is empty.       
//!exc: Malformed ILWD element( only blob data can be multidimensional ) - 
//+     Other than blob data is multidimensional.   
//!exc: bad_alloc - Memory allocation failed.   
//
void eventMonitor::DataBaseConverter::insertTableColumn( DB::Table* const t,
   const ILwd::LdasElement* elem )
try   
{
   string name( elem->getName( mColumnNameIndex ) );
   
   INT_4S* dims( 0 );      
   INT_4S ndata( 0 );
   ElementId id( elem->getElementId() );
   
   if( id == ID_ILWD )
   {
      const LdasContainer* c( dynamic_cast< const LdasContainer* >( elem ) );
      if( c->size() == 0 )
      {
         throw EventException( MALFORMED_ILWD, "( empty blob container )" );
      }
   
      ndata = c->size();
      dims = new INT_4S[ ndata ];

      try 
      {
         switch( ( ( *c )[ 0 ] )->getElementId() )
         {
            case(ID_CHAR):
            {
               const CHAR* d( getBlobData< CHAR >( c, dims ) );
               try
               {
                  t->AppendColumnEntries( name, ndata, d, dims );                   
               }
               catch(...)
               {
                  delete[] d;
                  throw;
               }
   
               delete[] d;
               delete[] dims;
               dims = 0;
               return;
            }
            case(ID_CHAR_U):
            {
               const CHAR_U* d( getBlobData< CHAR_U >( c, dims ) );   
               try
               {
                  t->AppendColumnEntries( name, ndata, d, dims );                      
               }
               catch(...)
               {
                  delete[] d;
                  throw;
               }
   
               delete[] d;
               delete[] dims;
               dims = 0;   
               return;
            }
            default:
            {
               delete[] dims;
               dims = 0;
               throw EventException( "Unsupported database blob datatype." );
               break;   
            }
         }
      }
      catch(...) 
      {
         delete[] dims;
         dims = 0;   
         throw;
      }
   }

   
   try
   {
      // dynamic_cast of pointer may throw an exception or just return 0( ?! )
      const LdasArrayBase* b( dynamic_cast< const LdasArrayBase* >( elem ) );
      if( b == 0 )
      {
         throw bad_cast();
      }
      ndata = b->getNData();   
   
      // Only BLOB type of data can be multidimensional
      if( b->getNDim() > 1 )
      {
         throw EventException( MALFORMED_ILWD, "( only blob data can be"
                               " multidimensional )" );
      }
   }
   catch( const bad_cast& )
   {
      throw EventException( MALFORMED_ILWD, "( dataBase array expected )" );      
   }

   switch( id )
   {
      case (ID_CHAR):
      {
         t->AppendColumnEntries( name, ndata, 
            dynamic_cast< const LdasArray< CHAR >* >( elem )->getData(), 
            dims );   
         break;
      }
      case (ID_CHAR_U):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< CHAR_U >* >( elem )->getData(), 
            dims );    
         break;
      }
      case (ID_INT_2S):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< INT_2S >* >( elem )->getData(),
            dims ); 
         break;
      }
      case (ID_INT_2U):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< INT_2U >* >( elem )->getData(),
            dims );
         break;
      }
      case (ID_INT_4S):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< INT_4S >* >( elem )->getData(),
            dims ); 
         break;
      }
      case (ID_INT_4U):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< INT_4U >* >( elem )->getData(),
            dims ); 
         break;
      }
      case (ID_INT_8S):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< INT_8S >* >( elem )->getData(),
            dims ); 
         break;
      }
      case (ID_INT_8U):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< INT_8U >* >( elem )->getData(),
            dims ); 
         break;
      }   
      case (ID_REAL_4):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< REAL_4 >* >( elem )->getData(),
            dims );
         break;
      }
      case (ID_REAL_8):
      {
         t->AppendColumnEntries( name, ndata,    
            dynamic_cast< const LdasArray< REAL_8 >* >( elem )->getData(),
            dims );
         break;
      }
      default:
      {
         string msg( "Unsupported database datatype: " );
         msg += elem->getIdentifier();
         throw EventException( msg );
         break;
      }
   }
   
   return;
}
catch( const bad_cast& exc )   
{
   string msg( "Error inserting \"" );
   msg += elem->getName( mColumnNameIndex );
   msg += "\" column into \"";
   msg += t->GetName();
   msg += "\" table: incorrect type ";
   msg += elem->getIdentifier();
   throw EventException( msg );
}
catch( const runtime_error& exc )
{
   throw EventException( exc.what() );
}

