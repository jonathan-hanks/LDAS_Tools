// System Header Files
#ifdef __sun__
#include <sys/types.h>
#endif   
   
#include <algorithm>
#include <cstdio>   
#include <functional>   
#include <typeinfo>

#include "general/Memory.hh"

// Local Header Files
#include "frameconverter.hh"
#include "eventerrors.hh"   

// ILWD Header Files
#include <ilwd/ldasarray.hh>   
#include <ilwd/ldascontainer.hh>      
#include <ilwd/ldasstring.hh>   
#include <ilwd/style.hh>   

#include <ilwdfcs/FrameH.hh>   
#include <ilwdfcs/FrProcData.hh>      
#include <ilwdfcs/FrVect.hh>      
#include <ilwdfcs/FrHistory.hh>   
#include <ilwdfcs/FrDetector.hh>      
   
// General Header Files
#include "general/gpstime.hh"
   
   
using namespace std;
using namespace eventMonitor;
using namespace ILwdFCS;   

using ILwd::ElementId;   
using ILwd::LdasContainer;   
using ILwd::LdasElement;   
using ILwd::LdasArray;
using ILwd::LdasArrayBase;   
using ILwd::LdasString;   
using ILwd::Style;

using ILwd::ID_ILWD;   
using ILwd::ID_LSTRING;   
using ILwd::ID_CHAR;
using ILwd::ID_CHAR_U; 
using ILwd::ID_INT_2S;
using ILwd::ID_INT_2U;
using ILwd::ID_INT_4S;
using ILwd::ID_INT_4U;
using ILwd::ID_INT_8S;
using ILwd::ID_INT_8U;
using ILwd::ID_REAL_4;
using ILwd::ID_REAL_8;
using ILwd::ID_COMPLEX_8;
using ILwd::ID_COMPLEX_16;      

using General::GPSTime;   

   
//------------------------------------------------------------------------------
//
//: Initializes domainHash unordered_map.
//
// This method initializes the domain unordered_map, where a key is of string type
// and value is a corresponding enum type.
//   
//!return: const domainHash - unordered_map.
//
//!exc: bad_alloc - Memory allocation failed.
//   
const eventMonitor::FrameConverter::domainHash 
   eventMonitor::FrameConverter::initDomainHash()
{
   domainHash h;
   
   h.insert( domainValueType( string( "NONE" ), NONE ) );   
   h.insert( domainValueType( string( "TIME" ), TIME ) );
   h.insert( domainValueType( string( "FREQ" ), FREQ ) ); 
   h.insert( domainValueType( string( "BOTH" ), BOTH ) );
   
   return h;
}

//!ignore_begin:
   
// Static data members initialization  
const string FrameConverter::mFrameComment( "mdd" );   
const string FrameConverter::mAPIName( "eventmonAPI" );   
   
const string FrameConverter::mStartTimeName( "start_time" );   
const string FrameConverter::mStopTimeName( "stop_time" );     
const string FrameConverter::mStartFreqName( "start_freq" );      
const string FrameConverter::mStopFreqName( "stop_freq" );         
const string FrameConverter::mBaseFreqName( "base_freq" );            
const string FrameConverter::mSecUnits( "gps_sec" );
const string FrameConverter::mNanUnits( "gps_nan" );
const string FrameConverter::mDataName( "data" );   
const string FrameConverter::mLDASHistoryName( "LDAS_History" );  
const string FrameConverter::mDetectorContName( "Container(Detector)" );
const string FrameConverter::mPrefixName( "prefix" );      
const string FrameConverter::mLongitudeName( "longitude" );
const string FrameConverter::mLatitudeName( "latitude" );   
const string FrameConverter::mElevationName( "elevation" );   
const string FrameConverter::mArmXazimuthName( "armXazimuth" );   
const string FrameConverter::mArmYazimuthName( "armYazimuth" );      
const string FrameConverter::mArmXaltitudeName( "armXaltitude" );      
const string FrameConverter::mArmYaltitudeName( "armYaltitude" );            
const string FrameConverter::mLocalTimeName( "localTime" );   
   
const FrameConverter::domainHash FrameConverter::mDomainHash( 
   initDomainHash() );   


//!ignore_end:
   
   
//------------------------------------------------------------------------------
//
//: Default constructor.
// 
//!param: const bool& single_frame - A flag to indicate if result data should be
//+       represented by a single frame. Default is false.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
eventMonitor::FrameConverter::FrameConverter( const bool& single_frame )
   : Converter(), mSingleFrame( single_frame ),
     mFrameList(), mCurrentProc( 0 ), mCurrentDomain( NONE ), mOriginalData()
{
   // Initialize metadata arrays
   fill( mDx, mDx + MAX_META_DIM, 0.0f );
   fill( mStartX, mStartX + MAX_META_DIM, 0.0f );
   fill( mDims, mDims + MAX_META_DIM, 0 );
}
   
   
//------------------------------------------------------------------------------
//
//: Destructor.
//
// Frees dynamically allocated memory.   
//   
//!exc: None.
//   
eventMonitor::FrameConverter::~FrameConverter()
{
   // Delete all ILwdFCS objects
   for( list< Container* >::iterator fcs_iter = mFCSObjects.begin(),
        fcs_end = mFCSObjects.end(); fcs_iter != fcs_end; ++fcs_iter )
   {
      delete *fcs_iter;
      *fcs_iter = 0;
   }
   mFCSObjects.clear();
   
   for( list< FrVect* >::iterator fcs_iter = mFCSVectObjects.begin(),
        fcs_end = mFCSVectObjects.end(); fcs_iter != fcs_end; ++fcs_iter )
   {
      delete *fcs_iter;
      *fcs_iter = 0;
   }
   mFCSVectObjects.clear();

   // Delete original mdd data
   for( list_iterator iter = mOriginalData.begin(), end_iter = mOriginalData.end();
        iter != end_iter; ++iter )
   {
      delete *iter;
      *iter = 0;
   }   
   mOriginalData.clear();
   
   // Delete generated frames
   for( list< FrameH* >::iterator iter = mFrameList.begin(),
        end_iter = mFrameList.end(); iter != end_iter; ++iter )
   {
      delete ( *iter );
      *iter = 0;
   }
   mFrameList.clear();
}

   
//------------------------------------------------------------------------------
//
//: Store deep copy of original multiDimData list.
//      
// The only reason for this method is a defficiency of ilwdfcs library:
// cannot concatenate multiple frames into the single one.
// As a result, process cannot start creating frames unless mSingleFrame 
// flag is set for the job by the Tcl layer.
// Therefore store deep copies of all mdd's until the mSingleFrame is set to
// TRUE for the job, or Data::getSortedData method is called (the very last 
// object has arrived into the job bucket, or 'removeJob' tcl command has
// been issued).
//   
//!param: const containerList& mdd - A reference to the list of shallow pointers
//+       to create a deep copy of.
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
void eventMonitor::FrameConverter::storeData( 
   const containerList& mdd )
try   
{
   // If there was already an error during conversion,
   // don't proceed.
   if( getmError().size() )
   {
      return;
   }

   
   // mSingleFrame flag is still set to default value (false),
   // just create a deep copy of the data:
   if( !mSingleFrame )
   {
      // Each container represents C multiDimData structure,
      // create deep copy of each element:
      for( list_const_iterator iter = mdd.begin(),
           end_iter = mdd.end(); iter != end_iter; ++iter )
      {
         mOriginalData.push_back( new LdasContainer( **iter ) );
      }

      return;
   }
   
   
   // Tcl layer has already set the mSingleFrame to "1",
   // no need to store the data, create the frame:
   if( mOriginalData.size() )
   {
      // Some original data got into the "storage" by now,
      // process it
      addData();
   }

   
   // Process shallow copies of new data
   addShallowData( mdd );
   
   return;
}
catch( const exception& exc )
{
   setError( exc );
   return;
}

   
//------------------------------------------------------------------------------
//
//: Adds multiDimData list.
//   
// This method converts list of internally stored ILWD format containers that
// represent multiDimData structures into the global ILWD format Frame 
// element(s) that is specific to the job.
//    
void eventMonitor::FrameConverter::addData()
try   
{
   // If there was already an error during conversion,
   // don't proceed.
   if( getmError().size() || mOriginalData.size() == 0 )
   {
      return;
   }
   
   
   // Could use 'addShallowData' and then delete input data, but
   // it could increase memory usage
   
   // Each container represents C multiDimData structure
   for( list_iterator iter = mOriginalData.begin(),
        end_iter = mOriginalData.end(); iter != end_iter; ++iter )
   {
      createHeader();   
      // Each iterator is a multiDimData structure.
      // each container element has name = 'name1:name2:sequence'
      insertProcData( *iter );  

   
      delete *iter;
      *iter = 0;
   }
   

   mOriginalData.erase( mOriginalData.begin(), mOriginalData.end() );
   
   return;
}
catch( const EventException& exc )
{
   setError( exc );
   return;
}
catch( const exception& exc )
{
   setError( exc );
   return;
}
   

//------------------------------------------------------------------------------
//
//: Adds multiDimData list.
//   
// This method adds list of ILWD format containers that represent multiDimData 
// structures for one particular array of outPut structures to the global ILWD 
// format Frame element that is specific to the job. 
// Each container in the list represents the 'optional' multiDimData structure
// of outPut C data structure received from the wrapperAPI job.
//
//!param: const list< const LdasContainer* >& mdd - A reference to the list of 
//+       multiDimData in ILWD format.   
//!param: const bool delete_mdd - Flag if input multiDimData should be deleted.
//+       Default is false.   
//   
//!return: Nothing.
//   
void eventMonitor::FrameConverter::addShallowData( const containerList& mdd )
try   
{
   // If there was already an error during conversion,
   // don't proceed.
   if( getmError().size() || mdd.size() == 0 )
   {
      return;
   }
   
   
   // Each container represents C multiDimData structure
   for( list_const_iterator iter = mdd.begin(),
        end_iter = mdd.end(); iter != end_iter; ++iter )
   {
      createHeader();   
   
      // Each iterator is a multiDimData structure.
      // each container element has name = 'name1:name2:sequence'
      insertProcData( *iter );  
   }


   return;
}
catch( const EventException& exc )
{
   setError( exc );
   return;
}
catch( const exception& exc )
{
   setError( exc );
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Sets value for mSingleFrame flag.
//
//!return: Nothing.
//      
void eventMonitor::FrameConverter::setSingleFrame( const bool& flag )   
{
   mSingleFrame = flag;
   return;
}

   
//------------------------------------------------------------------------------
//
//: Gets multiDimData for the job in ILWD format.
//
// This method creates ILWD format element(s) that represents multiDimData data
// for the whole job.   
//   
//!param: const INT_4U& id - Job id.
//   
//!return: std::unique_ptr< ILwd::LdasContainer > - An unique_ptr of LdasContainer
//+        that wraps all ILWD format containers representing MDD data for the job
//+        (or a single ILWD format container if mSingleFrame flag is set to TRUE)
//
//!exc: bad_alloc - Memory allocation failed.
//      
std::unique_ptr< ILwd::LdasContainer > eventMonitor::FrameConverter::getILWD( 
   const INT_4U& id )   
{
   unique_ptr< LdasContainer > result( 0 );
   
   if( getmError().size() )
   {
      return result;
   }
   

   INT_4S index( 1 );
   unique_ptr< LdasContainer > ilwd( 0 );
   static const bool release_ownership( true );
   static const size_t name_dim( 56 );
   
   // Tcl expected name attribute
   static const string wrapped_name( "wrapped" );
   CHAR name[ name_dim ];

   result.reset( new LdasContainer( wrapped_name ) );
   
   
   for( list< FrameH* >::iterator iter = mFrameList.begin(),
        end_iter = mFrameList.end(); iter != end_iter; ++iter )
   {
      // Need to create unique file name for each frame:
      // ostringstream is known to have a problem when -finline is used:
      // ostringstream name;
      // name << int_value;
      // name.str();
      sprintf( name, "%u", index );
               
      // FrameH::ConvertToILwd returns a pointer to the FrameH object 
      // underlying ILWD container, now caller is responsible 
      // for the deletion of returned element.
      ilwd.reset( ( *iter )->ConvertToILwd( id, mAPIName, name, 
                                       release_ownership ) );
      result->push_back( ilwd.get(), 
                         LdasContainer::NO_ALLOCATE,
                         LdasContainer::OWN );
   
      // Release pointer from unique_ptr
      ilwd.release();
   
      ++index;   
   }

   
   return result;
}
   
  
//------------------------------------------------------------------------------
//
//: Creates initial frame Header element.
//
// This method allocates the memory for ILwdFCS format element that represents
// frame header for the current set of mdd data, and populates that container
// with default elements.   
//   
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//   
void eventMonitor::FrameConverter::createHeader()
{
   if( mFrameList.empty() || 
       mSingleFrame == false )
   {
      FrameH* header( new FrameH() );
      mFrameList.push_back( header );   
   
      header->SetCommentAttribute( mFrameComment );
      // GTime and dt are generated by appending FrProcData objects
   }
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Inserts multiDimData into frame ILWD format element.
//
// This method parses passed to it ILWD format element that represents 
// multiDimData structure, and inserts it as a ProcData into the Frame. If 
// data structure contains history linked list, it will be inserted as History
// into the same frame on the same level of containment as multiDimData data.   
//   
//!param: const LdasContainer* e - A reference to the multiDimData ILWD format
//+       element.   
//   
//!return: Nothing.
//
//!exc: bad_alloc - Memory allocation failed.
//!exc: EventException( Malformed ILWD element: start_time. ) - ILWD format
//+     elements representing start_time are malformed.
//!exc: EventException( Malformed ILWD element: domain. ) - Unknown domain
//+     specified for ILWD format multiDimData.
//!exc: EventException( Malformed ILWD element: unknown domain. ) - Domain 
//+     element is missing or it's not of LSTRING type.         
//!exc: EventException( Malformed ILWD element: time:step_size. ) - Malformed
//+     ILWD format element for the time step_size.   
//!exc: EventException( Malformed ILWD element: null value for 
//+     time:step_size. ) - Time step size is set to zero.      
//!exc: EventException( Malformed ILWD element: freq:step_size. ) - Malformed
//+     ILWD format element for the frequency step_size.      
//!exc: EventException( Malformed ILWD element: null value for 
//+     freq:step_size. ) - Frequency step size is set to zero.   
//!exc: EventException( Malformed ILWD element: both:step_size. ) - Malformed
//+     ILWD format element for the time or frequency step_size.         
//!exc: EventException( Malformed ILWD element: null value for 
//+     both:time_step_size. ) - Time step size in BOTH domain is set to zero.   
//!exc: EventException( Malformed ILWD element: null value for 
//+     both:freq_step_size. ) - Frequency step size in BOTH domain is set to zero.      
//!exc: EventException( Malformed ILWD element: both:time_step_size is not
//+     consistent with both:freq_step_size. ) - time:step_size is not the same as
//+     1/freq:step_size in BOTH domain.      
//!exc: EventException( Malformed ILWD element: data. ) - 
//+        ILWD format element representing the data is not found or it's
//+        not LdasArray.
//!exc: EventException( MDD:start_time is before Frame:start_time. ) -
//+     Start time of mdd data is before start time of the frame it's assigned
//+     to.
//!exc: EventException( Malformed ILWD element: stop_time. ) - ILWD format
//+     elements representing stop_time are malformed.      
//!exc: EventException( MDD:stop_time is before Frame:start_time. ) -
//+     Stop time of mdd data is before start time of the frame it's assigned
//+     to.   
//!exc: EventException( Malformed ILWD element: history. ) - 
//+        ILWD format element representing the history is not
//+        LdasContainer.
//!exc: EventException( Malformed ILWD element: empty history. ) - 
//+        ILWD format container representing the history is empty.   
//!exc: EventException( Malformed ILWD element: history record.) - History element
//+     data can't be an ILWD format container.   
//!exc: EventException( Unsupported datatype for history. ) - Unknown datatype 
//+     specified for a history record.     
//   
void eventMonitor::FrameConverter::insertProcData( const ILwd::LdasContainer* e )
{
   // Create new FrProcData object
   mCurrentProc = new FrProcData();
   mFCSObjects.push_back( mCurrentProc );
   
   // Must populate time values before appending to the FrameHeader,
   // otherwise new values won't correspond to the time data in the header.
   mCurrentProc->SetName( e->getNameString() );
   mCurrentProc->SetComment( e->getComment() );
   
   mCurrentDomain = getDomain( e );
   setData( e );      

   // Append it to the current FrameHeader( by doing that FrameHeader owns
   // the actual data for the FrProcData )
   mFrameList.back()->AppendProcData( *mCurrentProc );
   
   insertDetectorData( e );
   
   return insertHistoryData( e );
}

   
//------------------------------------------------------------------------------
//
//: Sets data for the current adc ILWD format element.
//   
// This method extracts data information from passed to it ILWD 
// format container, and generates newly formated data elements for the
// current adc ILWD format container.
//
//!param: const LdasContainer* e - A pointer to the ILWD format container 
//+       representing multiDimData.
//   
//!return: void - Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: EventException( Malformed ILWD element: domain. ) - Unknown domain
//+     specified for ILWD format multiDimData.
//!exc: EventException( Malformed ILWD element: unknown domain. ) - Domain 
//+     element is missing or it's not of LSTRING type.         
//!exc: EventException( Malformed ILWD element: time:step_size. ) - Malformed
//+     ILWD format element for the time step_size.   
//!exc: EventException( Malformed ILWD element: null value for 
//+     time:step_size. ) - Time step size is set to zero.      
//!exc: EventException( Malformed ILWD element: freq:step_size. ) - Malformed
//+     ILWD format element for the frequency step_size.      
//!exc: EventException( Malformed ILWD element: null value for 
//+     freq:step_size. ) - Frequency step size is set to zero.   
//!exc: EventException( Malformed ILWD element: both:step_size. ) - Malformed
//+     ILWD format element for the time or frequency step_size.         
//!exc: EventException( Malformed ILWD element: null value for 
//+     both:time_step_size. ) - Time step size in BOTH domain is set to zero.   
//!exc: EventException( Malformed ILWD element: null value for 
//+     both:freq_step_size. ) - Frequency step size in BOTH domain is set to zero.      
//!exc: EventException( Malformed ILWD element: both:time_step_size is not
//+     consistent with both:freq_step_size. ) - time:step_size is not the same as
//+     1/freq:step_size in BOTH domain.      
//!exc: EventException( Malformed ILWD element: data. ) - 
//+        ILWD format element representing the data is not found or it's
//+        not LdasArray.
//!exc: EventException( Malformed ILWD element: stop_time. ) - ILWD format
//+     elements representing stop_time are malformed.   
//!exc: EventException( MDD:stop_time is before Frame:start_time. ) -
//+     Stop time of mdd data is before start time of the frame it's assigned
//+     to.
//!exc: EventException( Malformed ILWD element: number of diemensions for data
//+     is different from number of dimensions for metadata. - Inconsistent number
//+     of dimensions for data and it's metadata.
//      
void eventMonitor::FrameConverter::setData( 
   const ILwd::LdasContainer* e )
{
   if( mCurrentDomain != NONE )
   {
      const GPSTime start( findTime( e, mStartTimeName ) );
      mCurrentProc->SetStartTime( start );
   
      mCurrentProc->SetTimeOffset( 0.0f );

      const GPSTime stop( findTime( e, mStopTimeName ) );   
      mCurrentProc->SetStopTime( stop );
   
      // Set tRange
      REAL_8 range( static_cast< INT_8S >( stop.GetSeconds() ) - 
                    static_cast< INT_8S >( start.GetSeconds() ) + 
                    ( static_cast< INT_8S >( stop.GetNanoseconds() ) -
                      static_cast< INT_8S >( start.GetNanoseconds() ) ) * 1e-9 );
      mCurrentProc->SetTRange( range );
   }


   const LdasElement* data( e->find( mDataName ) );
   
   if( data == 0 )
   {
      throw EventException( MALFORMED_ILWD, 
               string( ": data is missing." ) );
   }

   const LdasArrayBase* b( dynamic_cast< const LdasArrayBase* >( data ) );
   if( b == 0 )
   {
      throw EventException( MALFORMED_ILWD, 
               string( ": data is not an ILWD format array." ) );
   }
   
   const size_t dim( setProcMetadata( e ) );
   const size_t b_dim( b->getNDim() );

   if( b_dim != dim )
   {
      ostringstream s;
      s << ": number of data dimensions( " << b_dim
        << " ) is different from its metadata dimensions( "
        << dim << ").";
   
      throw EventException( MALFORMED_ILWD, s.str() );   
   }   
   

   mUnitY = "";
   string index_units;
   
   for( size_t index = 0; index < dim; ++index )
   {
      index_units = b->getUnits( index );
      if( mUnitY.empty() == false &&
          index_units.empty() == false )
      {
         mUnitY += ':';
      }
      mUnitY += index_units;
   
      mDims[ index ] = b->getDimension( index );
   }
   

   // Create FrVect
   return createData( data );
}


//------------------------------------------------------------------------------
//
//: Creates FrVect object of proper datatype.
//   
// This method creates a FrVect object for the FrProcData with proper 'units'
// attribute. 
//   
//!param: const LdasElement* e - A pointer to the ILWD format element 
//+       representing the data.
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//      
template< class T > void
eventMonitor::FrameConverter::createArray( const ILwd::LdasElement* e )
{
   const LdasArray< T >* a( dynamic_cast< const LdasArray< T >* >( e ) );
   
   const T* data( a->getData() );   
   FrVect* vect( new FrVect( ) );
   mFCSVectObjects.push_back( vect );   

   // Set FrVect metadata   
   vect->template SetData< T >( data, a->getNDim(), mDims,
                       mStartX, mUnitX, mDx, mUnitY );
   vect->SetName( a->getNameString() );   
   mCurrentProc->AppendData( *vect );
   
   return;
}

   
// Instantiate templates   
template void eventMonitor::FrameConverter::createArray< CHAR >( 
   const ILwd::LdasElement* e );   
template void eventMonitor::FrameConverter::createArray< CHAR_U >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< INT_2S >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< INT_2U >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< INT_4S >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< INT_4U >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< INT_8S >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< INT_8U >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< REAL_4 >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< REAL_8 >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< COMPLEX_8 >( 
   const ILwd::LdasElement* e );      
template void eventMonitor::FrameConverter::createArray< COMPLEX_16 >( 
   const ILwd::LdasElement* e );      
   

   
//------------------------------------------------------------------------------
//
//: Creates FrVect element for the current FrProcData.
//   
// This method creates a copy of ILwd format data element with proper 'units'
// attributes. This method wouldn't be needed if ILwd LdasArray class had a 
// method to set specific fields for 'units' attribute.
//   
//!param: const LdasElement* e - A pointer to the ILWD format element 
//+       representing the data.
//
//!return: Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//         
void eventMonitor::FrameConverter::createData( const ILwd::LdasElement* e )
{
   switch( e->getElementId() )
   {
      case(ID_CHAR):
      {
         return createArray< CHAR >( e );
      }
      case(ID_CHAR_U):
      {
         return createArray< CHAR_U >( e );
      }   
      case(ID_INT_2S):
      {
         return createArray< INT_2S >( e );
      }      
      case(ID_INT_2U):
      {
         return createArray< INT_2U >( e );
      }         
      case(ID_INT_4S):
      {
         return createArray< INT_4S >( e );
      }         
      case(ID_INT_4U):
      {
         return createArray< INT_4U >( e );
      }         
      case(ID_INT_8S):
      {
         return createArray< INT_8S >( e );
      }     
      case(ID_INT_8U):
      {
         return createArray< INT_8U >( e );
      }       
      case(ID_REAL_4):
      {
         return createArray< REAL_4 >( e );
      }         
      case(ID_REAL_8):
      {
         return createArray< REAL_8 >( e );
      }         
      case(ID_COMPLEX_8):
      {
         return createArray< COMPLEX_8 >( e );
      }         
      case(ID_COMPLEX_16):
      {
         return createArray< COMPLEX_16 >( e );
      }         
      default:
      {
         throw EventException( "Unsupported datatype for data." );
      }   
   }
   
   return;
}
   
   
//------------------------------------------------------------------------------
//
//: Inserts history data into frame ILWD format element.
//   
// This method detects if ILWD format container representing a multiDimData 
// structure contains a history. If there is history data, it reformats that
// data into new ILWD Frame History container.
//
//!param: const LdasContainer* e - A pointer to the ILWD format container 
//+       representing multiDimData.
//   
//!return: void - Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: EventException( Malformed ILWD element: history. ) - 
//+        ILWD format element representing the history is not
//+        LdasContainer.
//!exc: EventException( Malformed ILWD element: empty history. ) - 
//+        ILWD format container representing the history is empty.
//!exc: EventException( Malformed ILWD element: history record.) - History element
//+     data can't be an ILWD format container.   
//!exc: EventException( Unsupported datatype for history. ) - Unknown datatype 
//+     specified for a history record.  
//      
void eventMonitor::FrameConverter::insertHistoryData( 
   const ILwd::LdasContainer* e )   
{
   const LdasElement* he( e->find( mLDASHistoryName ) );
   
   if( he == 0 )
   {
      return;
   }
   
   if( he->getElementId() != ID_ILWD )
   {
      throw EventException( MALFORMED_ILWD, ": history." );
   }
   
   
   const LdasContainer* hc( dynamic_cast< const LdasContainer* >( he ) );
   if( hc->empty() )
   {
      throw EventException( MALFORMED_ILWD, ": empty history." );
   }
    
   for( LdasContainer::const_iterator iter = hc->begin(),
        end_iter = hc->end(); iter != end_iter; ++iter )
   {
      // Each iterator is a container itself representing one
      // history record
      setHistory( *iter );
   }
   
   return;
}   
   
  
//------------------------------------------------------------------------------
//
//: Creates one history record for the frame.
//   
//!param: const LdasElement* e - A pointer to the ILWD format element 
//+       representing one history record.
//
//!return: Nothing.
//
//!exc: EventException( Malformed ILWD element: history.) - History element
//+     is not ILWD format container.
//!exc: EventException( Malformed ILWD element: history record.) - History element
//+     data can't be an ILWD format container.
//!exc: EventException( Unsupported datatype for history. ) - Unknown datatype 
//+     specified for a history record.     
//!exc: bad_alloc - Memory allocation failed.   
//   
void eventMonitor::FrameConverter::setHistory( 
   const ILwd::LdasElement* e )   
{
   if( e->getElementId() != ID_ILWD || 
       ( dynamic_cast< const LdasContainer* >( e ) )->size() != 1 )
   {
      throw EventException( MALFORMED_ILWD, ": history.");
   }

   const LdasElement* hist_record( 
      ( dynamic_cast< const LdasContainer* >( e ) )->front() );
   // Current GPS time
   GPSTime current;
   current.Now();
   FrHistory* history( new FrHistory( historyToAscii( hist_record ), 
                                      e->getNameString(), 
                                      current.GetSeconds() ) );
   mFCSObjects.push_back( history );

   // Associate history record with current FrProc
   mCurrentProc->AppendHistory( *history );
   
   return;
}


//------------------------------------------------------------------------------
//
//: Gets ASCII representation of an array data.
//   
//!param: const LdasElement* e - A pointer to the ILWD format element 
//+       representing the data.
//
//!return: const string - A string containing the data.
//   
//!exc: bad_alloc - Memory allocation failed.
//   
template< class T > const std::string 
   eventMonitor::FrameConverter::arrayToAscii( const ILwd::LdasElement* e )
{
   size_t size( 0 );
   Style style;

   const LdasArray< T >* b( dynamic_cast< const LdasArray< T >* >( e ) );
   const CHAR* buf( style.template convertToFormat< T >( b->getData(),
                    b->getNData(), &size ) );
   const string data( buf );
   
   delete[] buf;
   return data;
}

   
// Instantiate templates
template const string eventMonitor::FrameConverter::arrayToAscii< CHAR >( 
   const ILwd::LdasElement* e );
template const string eventMonitor::FrameConverter::arrayToAscii< CHAR_U >( 
   const ILwd::LdasElement* e );   
template const string eventMonitor::FrameConverter::arrayToAscii< INT_2S >( 
   const ILwd::LdasElement* e );      
template const string eventMonitor::FrameConverter::arrayToAscii< INT_2U >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< INT_4S >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< INT_4U >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< INT_8S >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< INT_8U >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< REAL_4 >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< REAL_8 >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< COMPLEX_8 >( 
   const ILwd::LdasElement* e );         
template const string eventMonitor::FrameConverter::arrayToAscii< COMPLEX_16 >( 
   const ILwd::LdasElement* e );         
   
   
//------------------------------------------------------------------------------
//
//: Gets ASCII representation of history record data.
//   
//!param: const LdasElement* e - A pointer to the ILWD format element 
//+       representing one history record data.
//
//!return: const string - String representation of data.
//
//!exc: EventException( Malformed ILWD element: history record.) - History element
//+     data can't be an ILWD format container.
//!exc: EventException( Unsupported datatype for history. ) - Unknown datatype 
//+     specified for a history record.     
//!exc: bad_alloc - Memory allocation failed.   
//   
const string eventMonitor::FrameConverter::historyToAscii( 
   const ILwd::LdasElement* e )      
{
   switch( e->getElementId() )
   {
      case(ID_ILWD):
      {
         throw EventException( MALFORMED_ILWD, ": history record." );
      }
      case(ID_LSTRING):
      {
         return ( dynamic_cast< const LdasString* >( e ) )->getString();
      }
      case(ID_CHAR):
      {
         return arrayToAscii< CHAR >( e );
      }
      case(ID_CHAR_U):
      {
         return arrayToAscii< CHAR_U >( e );
      }   
      case(ID_INT_2S):
      {
         return arrayToAscii< INT_2S >( e );
      }      
      case(ID_INT_2U):
      {
         return arrayToAscii< INT_2U >( e );
      }         
      case(ID_INT_4S):
      {
         return arrayToAscii< INT_4S >( e );
      }         
      case(ID_INT_4U):
      {
         return arrayToAscii< INT_4U >( e );
      }         
      case(ID_INT_8S):
      {
         return arrayToAscii< INT_8S >( e );
      }     
      case(ID_INT_8U):
      {
         return arrayToAscii< INT_8U >( e );
      }       
      case(ID_REAL_4):
      {
         return arrayToAscii< REAL_4 >( e );
      }         
      case(ID_REAL_8):
      {
         return arrayToAscii< REAL_8 >( e );
      }         
      case(ID_COMPLEX_8):
      {
         return arrayToAscii< COMPLEX_8 >( e );
      }         
      case(ID_COMPLEX_16):
      {
         return arrayToAscii< COMPLEX_16 >( e );
      }         
      default:
      {
         throw EventException( "Unsupported datatype for history." );
      }
   }
   
   return "";
}

   
   
//------------------------------------------------------------------------------
//
//: Inserts detector geometry data into frame ILWD format element.
//   
// This method detects if ILWD format container representing multiDimData 
// structure contains detector geometry. If there is detector data, it reformats
// that data into new ILWD Frame Detector container( frameAPI expected format ).
//
//!param: const LdasContainer* e - A pointer to the ILWD format container 
//+       representing multiDimData.
//   
//!return: void - Nothing.
//   
//!exc: bad_alloc - Memory allocation failed.
//!exc: EventException( Malformed ILWD element: Container(Detector). ) - 
//+        ILWD format element representing detector linked list is not
//+        LdasContainer.
//!exc: EventException( Malformed ILWD element: empty Container(Detector). ) - 
//+        ILWD format container representing detector geometry linked list
//+        is empty.
//      
void eventMonitor::FrameConverter::insertDetectorData( 
   const ILwd::LdasContainer* e )   
{
   static const size_t cont_name_pos( 2 );
   const LdasElement* de( e->find( mDetectorContName, cont_name_pos ) );
   
   if( de == 0 )
   {
      return;
   }
   
   if( de->getElementId() != ID_ILWD )
   {
      string msg( ": " );
      msg += mDetectorContName;
      throw EventException( MALFORMED_ILWD, msg );
   }
   
   
   const LdasContainer* dc( dynamic_cast< const LdasContainer* >( de ) );
   if( dc->empty() )
   {
      string msg( ": empty " );
      msg += mDetectorContName;
      throw EventException( MALFORMED_ILWD, msg );
   }

    
   for( LdasContainer::const_iterator iter = dc->begin(),
        end_iter = dc->end(); iter != end_iter; ++iter )
   {
      // Each iterator is a container itself representing one
      // detector geometry structure
      setDetector( *iter );
   }
   
   return;
}   
   

//------------------------------------------------------------------------------
//
//: Creates one detector geometry entry for the frame.
//   
//!param: const LdasElement* e - A pointer to the ILWD format element 
//+       representing one detector geometry structure.
//
//!return: Nothing.
//
//!exc: EventException( Malformed ILWD element: Detector.) - Detector element
//+     is not ILWD format container.
//!exc: Malformed input format: required array is not found: <b>name</b>. - 
//+        Required ILWD format element is missing.      
//!exc: Malformed input format: wrong datatype for <b>name</b>. - Specified
//+        ILWD format element is malformed.   
//!exc: Malformed input format: <b>name</b>. - Specified ILWD format element 
//+        is malformed.         
//!exc: bad_alloc - Memory allocation failed.   
//   
void eventMonitor::FrameConverter::setDetector( 
   const ILwd::LdasElement* e )   
{
   static const size_t detector_size( 9 );
   static const size_t name_pos( 0 );
   
   if( e == 0 || 
       e->getElementId() != ID_ILWD || 
       ( dynamic_cast< const LdasContainer* >( e ) )->size() != detector_size )
   {
      throw EventException( MALFORMED_ILWD, ": Detector" );
   }

   const LdasContainer& c( dynamic_cast< const LdasContainer& >( *e ) );
   
   FrDetector* detector( new FrDetector() );
   mFCSObjects.push_back( detector );
   
   detector->SetName( e->getName( name_pos ) );
   detector->SetPrefix( getArray< CHAR >( c, mPrefixName ) );
   detector->SetLongitude( getArrayValue< REAL_8 >( c, mLongitudeName ) ); 
   detector->SetLatitude( getArrayValue< REAL_8 >( c, mLatitudeName ) );    
   detector->SetElevation( getArrayValue< REAL_4 >( c, mElevationName ) );    
   detector->SetArmXazimuth( getArrayValue< REAL_4 >( c, mArmXazimuthName ) );    
   detector->SetArmYazimuth( getArrayValue< REAL_4 >( c, mArmYazimuthName ) );       
   detector->SetArmXaltitude( getArrayValue< REAL_4 >( c, mArmXaltitudeName ) );       
   detector->SetArmYaltitude( getArrayValue< REAL_4 >( c, mArmYaltitudeName ) );       
   detector->SetLocalTime( getArrayValue< INT_4S >( c, mLocalTimeName ) );             
   
   mFrameList.back()->AppendDetector( *detector );
   
   
   return;
}

   
//------------------------------------------------------------------------------
//
//: Gets value of required 1-element ILWD format array object.
//      
// This method finds ILWD format array of specified type in the passed to it
// ILWD format container and extracts its value. Element is required.
//    
//!param: const LdasContainer& re - A reference to the ILWD format element
//+       representing one C detGeom data structure.   
//!param: const CHAR* name - Name key of the element.   
//
//!return: const T - Value of the element.   
//
//!exc: Malformed input format: required array is not found: <b>name</b>. - 
//+        Required ILWD format element is missing.      
//!exc: Malformed input format: wrong datatype for <b>name</b>. - Specified
//+        ILWD format element is malformed.   
//!exc: Malformed input format: <b>name</b>. - Specified ILWD format element 
//+        is malformed.      
//
template< class T > T eventMonitor::FrameConverter::getArrayValue( 
   const ILwd::LdasContainer& e, const string& name )
{
   static const size_t name_pos( 0 );
   const LdasElement* elem( e.find( name, name_pos ) );   
   if( elem == 0 )
   {
      string msg( ": required array is not found: " );
      msg += name;
      throw EventException( MALFORMED_ILWD, msg );   
   }

   try
   {
      const LdasArray< T >& a( dynamic_cast< const LdasArray< T >& >( *elem ) );
      if( a.getNData() != 1 )
      {
         string msg( ": " );
         msg += name;
         throw EventException( MALFORMED_ILWD, msg );   
      }
   
      return a.getData()[ 0 ];
   }
   catch( const bad_cast& exc )
   {
      string msg( ": wrong datatype for " );
      msg += name;
      throw EventException( MALFORMED_ILWD, msg );   
   }
   
   return 0;   
}

   
// Template instantiation   
template REAL_8 eventMonitor::FrameConverter::getArrayValue< REAL_8 >( 
   const ILwd::LdasContainer& c, const string& name );   
template REAL_4 eventMonitor::FrameConverter::getArrayValue< REAL_4 >( 
   const ILwd::LdasContainer& c, const string& name );      
template INT_4S eventMonitor::FrameConverter::getArrayValue< INT_4S >( 
   const ILwd::LdasContainer& c, const string& name );      
template INT_4U eventMonitor::FrameConverter::getArrayValue< INT_4U >( 
   const ILwd::LdasContainer& c, const string& name );         

   
//------------------------------------------------------------------------------
//
//: Gets internal buffer of required ILWD array element.
//      
// This method finds ILWD format array of specified type in the passed to it
// ILWD format container and returns its internal buffer.
// Element is required.
//    
//!param: const LdasContainer& re - A reference to the ILWD format element
//+       representing one C detGeom data structure.   
//!param: const CHAR* name - Name key of the element.   
//
//!return: const T* - Internal buffer.
//
//!exc: Malformed input format: required array is not found: <b>name</b>. - 
//+        Required ILWD format element is missing.      
//!exc: Malformed input format: wrong datatype for <b>name</b>. - Specified
//+        ILWD format element is malformed.   
//!exc: Malformed input format: <b>name</b>. - Specified ILWD format element 
//+        is malformed.      
//
template< class T > const T* eventMonitor::FrameConverter::getArray( 
   const ILwd::LdasContainer& e, const string& name )
{
   static const size_t name_pos( 0 );
   const LdasElement* elem( e.find( name, name_pos ) );   
   if( elem == 0 )
   {
      string msg( ": required array is not found: " );
      msg += name;
      throw EventException( MALFORMED_ILWD, msg );   
   }

   try
   {
      const LdasArray< T >& a( dynamic_cast< const LdasArray< T >& >( *elem ) );
      if( a.getNData() == 0 )
      {
         string msg( ": " );
         msg += name;
         throw EventException( MALFORMED_ILWD, msg );   
      }
   
      return a.getData();
   }
   catch( const bad_cast& exc )
   {
      string msg( ": wrong datatype for " );
      msg += name;
      throw EventException( MALFORMED_ILWD, msg );   
   }
   
   return 0;   
}

   
// Template instantiation   
template const CHAR* eventMonitor::FrameConverter::getArray< CHAR >( 
   const ILwd::LdasContainer& c, const string& name );      
   
   
//------------------------------------------------------------------------------
//
//: Finds ILWD format elements with specific name field.
//   
// This method parses passed to it ILWD format container for elements that 
// have specific name field.
//
//!param: const LdasContainer* c - A pointer to the ILWD format container.
//!param: const string& name - A reference to the name string.
//!param: const size_t pos - Name position. Default is 1.
//   
//!return: const list< const LdasElement* > - A list of found elements.
//   
//!exc: bad_alloc - Memory allocation failed.
//         
const FrameConverter::ILwdList eventMonitor::FrameConverter::findILwd( 
   const ILwd::LdasContainer* c, const std::string& name, const size_t pos )
{
   ILwdList result;
   
   for( LdasContainer::const_iterator iter = c->begin(),
        end_iter = c->end(); iter != end_iter; ++iter )
   {
      if( ( *iter )->getName( pos ) == name )
      {
         result.push_back( *iter );
      }
   }

   
   return result;
}
   
   
//------------------------------------------------------------------------------
//
//: Extract time information.
//   
// This method parses passed to it ILWD format container for elements that 
// represent time.
//
//!param: const LdasContainer* c - A pointer to the ILWD format container.
//!param: const string& name - A reference to the name string.
//   
//!return: const General::GPSTime - A time object.
//   
//!exc: bad_alloc - Memory allocation failed.
//            
const General::GPSTime eventMonitor::FrameConverter::findTime(
   const ILwd::LdasContainer* c, const std::string& name )
{
   ILwdList time( findILwd( c, name ) );                     

   if( time.size() == 0 )
   {
      string msg( ": " );
      msg += name;
      msg += " is missing.";
   
      throw EventException( MALFORMED_ILWD, msg ); 
   }
   
   if( time.size() != 2 || 
       time.front()->getName( 0 ) != mSecUnits ||
       time.back()->getName( 0 ) != mNanUnits ||
       time.front()->getElementId() != ID_INT_4U ||
       time.back()->getElementId() != ID_INT_4U ||
       dynamic_cast< const LdasArrayBase* >( time.front() )->getNData() != 1 ||
       dynamic_cast< const LdasArrayBase* >( time.back() )->getNData() != 1 )
   {
      string msg( ": " );
      msg += name;
   
      throw EventException( MALFORMED_ILWD, msg );
   }

   
   INT_4U sec( ( dynamic_cast< const LdasArray< INT_4U >* >( 
                    time.front() ) )->getData()[ 0 ] );

   INT_4U nan( ( dynamic_cast< const LdasArray< INT_4U >* >( 
                    time.back() ) )->getData()[ 0 ] );
   
   return GPSTime( sec, nan );
}
   

//------------------------------------------------------------------------------
//
//: Extract element data value.
//   
// This method searches passed to it ILWD format container for specified
// array element and extracts it's value.   
//
//!param: const LdasContainer* c - A pointer to the ILWD format container.
//!param: const string& name - A reference to the name string.
//!param: const size_t pos - Name position. Default is 0.   
//   
//!return: const General::GPSTime - A time object.
//   
//!exc: bad_alloc - Memory allocation failed.
//               
template< class T > T eventMonitor::FrameConverter::getElementData(
   const ILwd::LdasContainer* c, const std::string& name, const size_t pos )
{
   const LdasElement* elem( c->find( name, pos ) );
   if( elem == 0 )
   {
      string msg( ": ");
      msg += name;
      msg += " is missing";
      throw EventException( MALFORMED_ILWD, msg );
   }
   
   const LdasArray< T >* a_elem( dynamic_cast< const LdasArray< T >* >( elem ) );
   if( a_elem == 0 )
   {
      string msg( ": ");
      msg += name;
      msg += " has incorrect datatype";
      throw EventException( MALFORMED_ILWD, msg );
   }   
   
   if( a_elem->getNData() != 1 )
   {
      string msg( ": ");
      msg += name;
      msg += " is not one element array";
      throw EventException( MALFORMED_ILWD, msg );   
   }
   
   return a_elem->getData()[ 0 ];
}
   
   
// Instantiate templates   
template REAL_8 FrameConverter::getElementData< REAL_8 >( 
   const LdasContainer* c, const string& name, const size_t pos );
   
   
//------------------------------------------------------------------------------
//
//: Determines the domain of the passed to it multiDimData container.
//   
// This method parses passed to it ILWD format container representing 
// multiDimData and determines within what domain it's defined.
//
//!param: const LdasContainer* c - A pointer to the ILWD format container.
//
//!return: const INT_4S - Domain value.
//   
//!exc: EventException( Malformed ILWD element: domain. ) - Unknown domain
//+     specified for ILWD format multiDimData.
//!exc: EventException( Malformed ILWD element: unknown domain. ) - Domain 
//+     element is missing or it's not of LSTRING type.      
//         
INT_4S eventMonitor::FrameConverter::getDomain( 
   const ILwd::LdasContainer* c )
{
   static const size_t domainPosition( 1 );
   static const string domainName( "domain" );

   const LdasElement* domain( c->find( domainName, domainPosition ) );
   if( domain == 0 || domain->getElementId() != ID_LSTRING )
   {
      throw EventException( MALFORMED_ILWD, 
               string( ": domain." ) );
   }
   
   domainHash::const_iterator iter( mDomainHash.find( 
                                     ( dynamic_cast< const LdasString* >( 
                                       domain ) )->getString() ) );
   if( iter == mDomainHash.end() )
   {
      throw EventException( MALFORMED_ILWD, 
               string( ": uknown domain." ) );   
   }
   
   return iter->second;
}
   

//------------------------------------------------------------------------------
//
//: Sets metadata for the current FrProcData structure.
//   
// This method parses passed to it ILWD format container representing 
// multiDimData and sets appropriate to the datatype FrProcData metadata.
//
//!param: const LdasContainer* e - A pointer to the ILWD format container.
//
//!return: const size_t - Number of dimensions in metadata.
//   
//!exc: EventException( Malformed ILWD element: domain. ) - Unknown domain
//+     specified for ILWD format multiDimData.
//!exc: EventException( Malformed ILWD element: unknown domain. ) - Domain 
//+     element is missing or it's not of LSTRING type.         
//!exc: EventException( Malformed ILWD element: time:step_size. ) - Malformed
//+     ILWD format element for the time step_size.   
//!exc: EventException( Malformed ILWD element: null value for 
//+     time:step_size. ) - Time step size is set to zero.      
//!exc: EventException( Malformed ILWD element: time:base_freq. ) - Malformed
//+     ILWD format element for the time:base_freq.      
//!exc: EventException( Malformed ILWD element: freq:step_size. ) - Malformed
//+     ILWD format element for the frequency step_size.      
//!exc: EventException( Malformed ILWD element: both:step_size. ) - Malformed
//+     ILWD format element for the time or frequency step_size.         
//!exc: EventException( Malformed ILWD element: null value for 
//+     both:time_step_size. ) - Time step size in BOTH domain is set to zero.   
//            
size_t eventMonitor::FrameConverter::setProcMetadata( 
   const ILwd::LdasContainer* e )
{
   static const size_t domain_name_pos( 0 );
   static const string timeName( "time" );
   static const string freqName( "freq" );   
   static const string stepSizeName( "step_size" );
   
   ILwdList e_list( findILwd( e, stepSizeName ) );   
   REAL_8 f_shift( -1.0f );
   size_t dim( 1 ); // this is just a 99% assumption for all domains but
                    // BOTH. If exception is ever thrown that there're
                    // mismatching dimensions ===> need to fix eventmon
                    // to support it.
   FrProcData::type_type type( FrProcData::UNKNOWN_TYPE );
   FrProcData::subType_type subtype( FrProcData::UNKNOWN_SUB_TYPE );   
   REAL_8 f_range( 0.0f );
   
   
   switch( mCurrentDomain )
   {
      case(NONE):
      {
         // Just clear all metadata, since it doesn't have any
         f_shift = 0.0f;
   
         mDx[ 0 ]     = 0.0f;
         mStartX[ 0 ] = 0.0f;
         mUnitX[ 0 ]  = "";   
   
         break;
      }
      case(TIME):
      {
         if( e_list.size() != 1 || 
             e_list.front()->getName( domain_name_pos ) != timeName ||   
             e_list.front()->getElementId() != ID_REAL_8 || 
             dynamic_cast< const LdasArray< REAL_8 >* >( 
                e_list.front() )->getNData() != 1 )
         {
            throw EventException( MALFORMED_ILWD, 
                     string( ": time:step_size." ) );
         }
   
         const LdasArray< REAL_8 >* a( dynamic_cast< const LdasArray< REAL_8 >* >(
                                       ( e_list.front() ) ) );
         mDx[ 0 ] = a->getData()[ 0 ];
     
         if( mDx[ 0 ] == 0.0f )
         {
            throw EventException( MALFORMED_ILWD,
                     string( ": null value for time:step_size." ) );
         }


         // Find 'base_freq' element
         const REAL_8 base_freq( getElementData< REAL_8 >( e, mBaseFreqName ) );
   
         mStartX[ 0 ] = 0.0f;
         mUnitX[ 0 ]  = a->getUnits( 0 );
         f_shift = base_freq - 1.0f / ( 2.0f * mDx[ 0 ] );
   
         type = FrProcData::TIME_SERIES;
   
         break;
      }
      case(FREQ):
      {
         if( e_list.size() != 1 || 
             e_list.front()->getName( domain_name_pos ) != freqName ||      
             e_list.front()->getElementId() != ID_REAL_8 || 
             dynamic_cast< const LdasArray< REAL_8 >* >( 
                e_list.front() )->getNData() != 1 )
         {
            throw EventException( MALFORMED_ILWD, 
                     string( ": freq:step_size." ) );
         }
   
         const LdasArray< REAL_8 >* a( dynamic_cast< const LdasArray< REAL_8 >* >(
                                       ( e_list.front() ) ) );
         const REAL_8 freq_step( a->getData()[ 0 ] );

         // Find 'base_freq' element
         const REAL_8 base_freq( getElementData< REAL_8 >( e, mBaseFreqName ) );
         f_shift = base_freq;          
   
         // Find 'start_freq' element
         const REAL_8 start_freq( getElementData< REAL_8 >( e, mStartFreqName ) );
   
         mDx[ 0 ]     = freq_step;
         mStartX[ 0 ] = start_freq;
         mUnitX[ 0 ]  = a->getUnits( 0 );
   
         const REAL_8 stop_freq( getElementData< REAL_8 >( e, mStopFreqName ) );   
         f_range = stop_freq - start_freq;
   
         type = FrProcData::FREQUENCY_SERIES;
   
         break;
      }   
      case(BOTH):
      {
         if( e_list.size() != 2 || 
             e_list.front()->getName( domain_name_pos ) != timeName ||      
             e_list.front()->getElementId() != ID_REAL_8 || 
             dynamic_cast< const LdasArray< REAL_8 >* >( 
                e_list.front() )->getNData() != 1 ||
             e_list.front()->getName( domain_name_pos ) != freqName ||      
             e_list.back()->getElementId() != ID_REAL_8 || 
             dynamic_cast< const LdasArray< REAL_8 >* >( 
                e_list.back() )->getNData() != 1 )
         {
            throw EventException( MALFORMED_ILWD, 
                     string( ": both:step_size." ) );
         }

         // time step_size
         const LdasArray< REAL_8 >* f_a( dynamic_cast< const LdasArray< REAL_8 >* >(
                                         ( e_list.front() ) ) );
         const REAL_8 time_step( f_a->getData()[ 0 ] );
   

         // frequency step_size
         const LdasArray< REAL_8 >* t_a( dynamic_cast< const LdasArray< REAL_8 >* >(
                                         ( e_list.back() ) ) );   
         const REAL_8 freq_step( t_a->getData()[ 0 ] );
   

         // start_freq element
         const REAL_8 start_freq( getElementData< REAL_8 >( e, mStartFreqName ) );   
         
         mDx[ 0 ] = time_step;
         mDx[ 1 ] = freq_step;   
         mStartX [ 0 ] = 0.0f;
         mStartX[ 1 ]  = start_freq;
         mUnitX[ 0 ] = t_a->getUnits( 0 );
         mUnitX[ 1 ] = f_a->getUnits( 0 );   

         const REAL_8 stop_freq( getElementData< REAL_8 >( e, mStopFreqName ) );   
         f_range = stop_freq - start_freq;   
   
         type = FrProcData::TIME_FREQUENCY;
   
         dim = 2;
         break;
      }   
      default:
      {
         break;
      }
   }
   
   // Create fShift element
   mCurrentProc->SetFShift( f_shift );
   
   // Set type
   mCurrentProc->SetType( type );
   
   // Set subtype
   mCurrentProc->SetSubType( subtype );
   
   // Set fRange
   mCurrentProc->SetFRange( f_range );
   
   
   return dim;
}
