// Local Header Files
#include "eventerrors.hh"
   
using namespace std;   
using namespace eventMonitor;   

   
//------------------------------------------------------------------------------
//
//: Initializes ErrorHash unordered_map.
//
// This method initializes the error unordered_map, where key is of
// ErrorCode type and value is a string representing the error.
//   
//!return: const ErrorHash - unordered_map.
//
//!exc: bad_alloc - Memory allocation failed.
//    
const eventMonitor::EventException::ErrorHash 
   eventMonitor::EventException::initErrorHash()
{
   ErrorHash h;
   // EventData errors
   h[ MALFORMED_DATA_STREAM ] = string( "Malformed data stream" );
   h[ NULL_ILWD_ELEMENT     ] = string( "NULL ILWD element" );   
   h[ EXISTING_ILWD_ELEMENT ] = string( "ILWD element exists in data bucket" );   
   h[ MALFORMED_ILWD        ] = string( "Malformed ILWD element" );      
   h[ MISMATCHED_JOBID      ] = string( "ILWD element doesn't match jobID" );      
   h[ MALFORMED_RESULT_STREAM ] = string( "Incorrect number of result ILWD elements" );         

   // EventRegistry errors
   h[ INSERT_ERROR          ] = string( "Error inserting entry into the map" );      
   h[ NOT_VALID_ELEMENT     ] = string( "Job doesn't exist" );         
   
   return h;
}
   
   
// Static data member initialization   
const EventException::ErrorHash EventException::mErrorHash(
   EventException::initErrorHash() );

   
//------------------------------------------------------------------------------
//
//: Constructor.
//   
//!param: const ErrorCode& code - A reference to the error code.
//!param: const string& msg - A reference to the error information.  
//   
eventMonitor::EventException::EventException( const ErrorCode& code, 
   const std::string& msg )
   : LdasException( EVENTMONAPI, code,
        string( mErrorHash.find( code )->second + msg ), "", __FILE__, __LINE__ )
{}   

   
//------------------------------------------------------------------------------
//
//: Constructor.
//   
//!param: const string& msg - A reference to the error message
//   
eventMonitor::EventException::EventException( const std::string& msg )
   : LdasException( EVENTMONAPI, 0,
        msg, "", __FILE__, __LINE__ )
{}   
   

//------------------------------------------------------------------------------
//
//: Extract the whole error message.
//   
//!return: const std::string - Error message.
//      
const std::string eventMonitor::EventException::what() const
{
   string msg;
   
   for( size_t size = getSize(), i = 0; i < size; ++i )
   {
      msg += getError( i ).getMessage();
   }
   
   return msg;
}

   
