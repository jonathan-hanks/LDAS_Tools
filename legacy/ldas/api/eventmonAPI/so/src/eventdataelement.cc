// Local Header Files
#include "eventdataelement.hh"
#include "eventerrors.hh"   

// System Header Files   
#include <iostream>   
#include <iterator>   
#include <sstream>
#include <typeinfo>
   
#include "general/Memory.hh"

using ILwd::LdasContainer;   
using ILwd::LdasElement;      
using ILwd::ID_ILWD;   
using namespace std;   
using namespace eventMonitor;
   
   
//------------------------------------------------------------------------------
//
//: Initializes keyHash unordered_map.
//
// This method initializes the key unordered_map, where key is of int type
// and value is pair of string representing the name of the key and value
// representing the name field where that name is stored in the ILWD name
// attribute.   
//   
//!return: const keyHash - unordered_map.
//
//!exc: bad_alloc - Memory allocation failed.
//   
const eventMonitor::EventDataElement::keyHash 
   eventMonitor::EventDataElement::initKeyHash()
{
   keyHash h;
   
   h.insert( keyValueType( PROCESS, 
                           keyPair( string( "ligo:ldas:file" ), 0 ) ) );
   h.insert( keyValueType( DB, 
                           keyPair( string( "dataBase" ), 0 ) ) );
   h.insert( keyValueType( STATE,
                           keyPair( string( "stateVector" ), 0 ) ) );   
   h.insert( keyValueType( MDD,
                           keyPair( string( "sequence" ), 2 ) ) );   
   
   return h;
}

   
//------------------------------------------------------------------------------
//
//: Initializes metaHash unordered_map.
//
// This method initializes the metadata unordered_map, where key is of int type
// and value is a string representing the metadata name of the key.
//   
//!return: const metaHash - unordered_map.
//   
//!exc: bad_alloc - Memory allocation failed.
//  
const eventMonitor::EventDataElement::metaHash 
   eventMonitor::EventDataElement::initMetaHash()
{
   metaHash h;
   
   h.insert( metaValueType( PROCESS, string( "Process " ) ) );
   h.insert( metaValueType( DB, string( "DB " ) ) );
   h.insert( metaValueType( STATE, string( "State " ) ) );   
   h.insert( metaValueType( MDD, string( "MDD " ) ) );   
   
   return h;
}
   
   
// Static data member initialization   
const string EventDataElement::mAnalyzedSecondsMeta( "analyzedDataSeconds" );   

EventDataElement::keyHash EventDataElement::mKeyHash( 
   initKeyHash() );   
EventDataElement::metaHash EventDataElement::mMetaHash( 
   initMetaHash() );      
   
   
//------------------------------------------------------------------------------
//
//: Constructor.
//    
//!param: unique_ptr< LdasContainer > e - An unique_ptr holding pointer to the ILWD
//+       format data object. 
//
//!exc: Malformed ILWD element. - Malformed input data.    
//!exc: bad_alloc - Memory allocation failed.
//
eventMonitor::EventDataElement::EventDataElement( 
   std::unique_ptr< ILwd::LdasContainer >& e ) 
   : mDataHash(), mData( e.release() )
{
   try
   {
      // Do data evaluation
      evaluateData();
   }
   catch( const bad_cast& exc )
   {
      cleanup();
      throw EventException( MALFORMED_ILWD );   
   }
   catch( ... )
   {
      cleanup();
      throw;
   }
}
   
   
//------------------------------------------------------------------------------
//
//: Destructor.
//         
eventMonitor::EventDataElement::~EventDataElement() 
{
   mDataHash.clear();
   
   cleanup();
}

   
//------------------------------------------------------------------------------
//
//: Cleanup data pointer.
//         
void eventMonitor::EventDataElement::cleanup() 
{
   // Delete ILWD element (process ILWD format element will have mData=0)
   delete mData;
   mData = 0;

   return;
}
   

//------------------------------------------------------------------------------
//
//: Releases data pointer to the caller.
//            
// This method releases internal data pointer to the caller,
// resets mData to NULL.   
//
//!return: unique_ptr< LdasContainer > - An unique_ptr holding data object.
//      
unique_ptr< LdasContainer > eventMonitor::EventDataElement::releaseData() 
{
   unique_ptr< LdasContainer > data( mData );   
   mData = 0;   

   // Don't really need to cleanup data hash (destructor will do it),
   // this object won't be reused for other data once this method is 
   // called
   
   return data;
}
   
   
//------------------------------------------------------------------------------
//
//: Extracts data pointer.
//            
// This method exposes internal data pointer to the caller
// (like unique_ptr.get() does).
//
//!return: const LdasContainer* const - A pointer to the ILwd::LdasContainer object.
//         
const LdasContainer* eventMonitor::EventDataElement::getData() const
{   
   return mData;
}

   
//------------------------------------------------------------------------------
//
//: Gets number of data seconds job has analyzed.
//            
// Each outPut ILWD format object has 'analyzedDataSeconds' attribute set.
// It is set by the wrapperAPI, and represents number of data seconds job
// has analyzed.   
//
//!return: const INT_4U - Number of seconds.
//      
INT_4S eventMonitor::EventDataElement::getAnalyzedSeconds() const
{
   INT_4S dt;
   try
   {
      string value( mData->getMetadata<std::string>( mAnalyzedSecondsMeta ) );
      istringstream s( value );
      s >> dt;
   
      if( !s )
      {
         string msg( "Bad value for \"" );
         msg += mAnalyzedSecondsMeta;
         msg += "\": ";
         msg += value;
         throw EventException( msg );
      }
   }
   catch( const exception& exc )
   {
      throw EventException( exc.what() );
   }  
   
   // Should never happen, wrapperAPI does this kind of check
   // (have it here in case something goes wrong during data transmission)
   if( dt <= 0 )
   {
      throw EventException( "Invalid number of analyzed data seconds"
                            " (zero or negative)" );
   }
   
   return dt;
}
   
   
//------------------------------------------------------------------------------
//
//: Gets specified data in ILWD format. 
//            
// This method accesses the internal ILWD format data and extracts all
// data elements representing the <i>key</i> data.
// It just collects the pointers to the original data, without deep COPING it.
//
//!param: const DataKey& key - A reference to the key specifying the type 
//+       of data.   
//   
//!return: const list< const LdasContainer* > - A list of ILWD format data.
//   
//!exc: Empty list for requested type of data. - Specified type of data doesn't
//+     exist in the ILWD format element.
//   
const list< LdasContainer* > eventMonitor::EventDataElement::getData(
   const DataKey& key ) const
{
   // List of mData element indices that contain the data
   const list< LdasContainer* > results( mDataHash.find( key )->second );
   
   if( results.empty() )
   {
      string msg( "Empty list for requested type of data: " );
      msg += mMetaHash[ key ];
   
      throw EventException( msg );
   }
   

#ifdef GET_DATA_DEBUG   
   cout << key << "--->";
   copy( results.begin(), results.end(),
            ostream_iterator< const LdasContainer* >( cout, " " ) );   
   cout.flush();
#endif   
   
   return results;
}   
   
   
//------------------------------------------------------------------------------
//
//: Does ILWD format element contain specified data.
//   
//!param: const DataKey& key - A reference to the data key.
//   
//!return: const bool - True if ILWD format element contains the data,
//+        false otherwise.
//   
bool eventMonitor::EventDataElement::hasData( const DataKey& key ) const 
{
   return ( mDataHash.find( key ) != mDataHash.end() );  
}

   
//------------------------------------------------------------------------------
//
//: Evaluates content of ILWD data.
//            
// This method determines if internal ILWD format data contains
// specific types of data, such as process information, dataBase data,
// stateVector data, or multiDimData data.   
//
//!return: Nothing.
// 
//!exc: bad_cast - Malformed data.
//!exc: bad_alloc - Memory allocation failed.   
//
void eventMonitor::EventDataElement::evaluateData() 
{
   static const string output_name( "outPut" );
   static const INT_4U output_name_pos( 2 );
   
   // If ILWD element contains process information, it may not
   // contain any other type of data: wrapperAPI doesn't join process
   // with other types of data into single LdasContainer.
   if( !isProcessInfo() )  
   {
      // Verify that ILWD element actually represents the outPut data
      // structure:
      if( mData->size() == 0 )
      {
         throw bad_cast();
      }
   
      LdasContainer* c( 0 );
   
      for( LdasContainer::const_iterator iter = mData->begin(),
              end_iter = mData->end(); iter != end_iter; ++iter )
      {
         if( *iter == 0 ||
             ( *iter )->getElementId() != ID_ILWD || 
             ( *iter )->getName( output_name_pos ) != output_name )
         {
            throw bad_cast();
         }
   
         c = dynamic_cast< LdasContainer* >( *iter );
         if( c == 0 )
         {
            throw bad_cast();
         }
   
         // Is there a dataBase data
         evaluateElement( c, DB );

         // Is there stateVector data
         evaluateElement( c, STATE );   
   
         // Is there multiDimData data
         evaluateElement( c, MDD );      
      }
   }
   
   return;
}   

   
//------------------------------------------------------------------------------
//
//: Checks if ILWD format element represents process information.
//            
// This method determines if internal ILWD format data contains
// process information.
//
//!return: const bool - True if mData contains process information, false
//+        otherwise.   
//   
//!exc: bad_alloc - Memory allocation failed.
//   
bool eventMonitor::EventDataElement::isProcessInfo()
{
   // Check if element has name=mProcessName
   if( mData->getNameString() == mKeyHash[ PROCESS ].first )
   {
      // Insert entry into the hash (with empty list)
      mDataHash[ PROCESS ];
      return true;
   }
   
   return false;
}   

   
//------------------------------------------------------------------------------
//
//: Evaluates ILWD format element. 
//   
// This method searches passed to it ILWD format element for specified type
// of data, such as dataBase, stateVector or multiDimData. It collects
// shallow data pointers into the mDataHash for further processing.
//   
//!param: const LdasContainer* const c - An ILWD format container
//+       element representing outPut data.   
//!param: const DataKey& key - A reference to the key to analyze.
//    
//!return: Nothing.
//
void eventMonitor::EventDataElement::evaluateElement( 
   LdasContainer* const c, const DataKey& key )
{
   keyHash::mapped_type& map_type( mKeyHash[ key ] );

   // Search one outPut data structure for mdd, db and state data:
   LdasElement* const elem( c->find( map_type.first, map_type.second ) );
   if( elem )
   {
      LdasContainer* const c_elem( dynamic_cast< LdasContainer* const >( elem ) );
      if( c_elem == 0 )
      {
         throw bad_cast();
      }
      // Inserts new element into the hash first if it doesn't exist
      mDataHash[ key ].push_back( c_elem );    
   }
   
   return;
}
