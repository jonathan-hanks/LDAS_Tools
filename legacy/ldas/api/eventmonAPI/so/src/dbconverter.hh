#ifndef EventDBConverterHH
#define EventDBConverterHH   

// System Header Files
#include <list>   
   
// General Header Files
#include "general/unordered_map.hh"
#include "general/util.hh"

// Local Header Files      
#include "converter.hh"   
   
// Forward declarations      
namespace DB
{
   class Table;
}

namespace ILwd
{
   class LdasElement;
   class LdasContainer;
}
   
namespace eventMonitor
{
   class EventException;
   
   
//------------------------------------------------------------------------------
//
//: Data base converter class.
//    
// This is utility class designed to collect dataBase data for one particular 
// job and to format it into one metadataAPI expected ILWD format element.   
//       
class DataBaseConverter : public Converter
{
public:   

   // Default constructor
   DataBaseConverter( const INT_4S dt, const std::string& error = "" ); 
   
   // Destructor
   ~DataBaseConverter();
   
   void addData( const containerList& db );
   ILwd::LdasContainer* getILWD( containerList& p );
   
   static void setMaxNumberRows( const INT_4S max );

private:

   // No copy constructor
   DataBaseConverter( const DataBaseConverter& c );    
   
   // No assignment operator
   const DataBaseConverter& operator=( const DataBaseConverter& c );    
   
   template< class T > const T* getBlobData( const ILwd::LdasContainer* c,
      INT_4S* dims );
   
   void insertDataBase( const ILwd::LdasContainer* db );
   void updateTableHash( const ILwd::LdasElement* e );
   void insertTableColumn( DB::Table* const t, const ILwd::LdasElement* elem );
   
   static const INT_4U mTableNameIndex;
   static const INT_4U mColumnNameIndex;   
   static const std::string mDBComment;
   //: If set to -1, number of rows for the database is unlimited.
   static INT_4S mMaxNumberRows;   
   
   // unordered_map to keep track of existing tables for the job
   typedef General::unordered_map< std::string, DB::Table*, 
				   General::hash< std::string >,
				   CaseInsensitiveCmp > tableHash;
   typedef tableHash::iterator iterator;
   typedef tableHash::const_iterator const_iterator;   
   typedef tableHash::value_type value_type;
   typedef std::pair< iterator, bool > insertPair;         

   //: Number of data seconds job has analyzed.
   const INT_4S mAnalyzedSeconds;
   
   //: Map of currently existing tables for the job.
   tableHash mTableHash;
};

}
   
   
#endif   
