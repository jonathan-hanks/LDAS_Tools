#include "../src/config.h"

#include <iostream>
#include <fstream>   
#include <sstream>   
#include <vector>   
#include <stdexcept>
   
// LDAS Header Files
#include "general/unittest.h"   
#include <general/util.hh>
#include <general/objectregistry.hh>
#include <general/ldasexception.hh>

// ILWD Header Files
#include <ilwd/reader.hh>
#include <ilwd/util.hh>      
   
#include <genericAPI/util.hh>
#include <genericAPI/registry.hh>
#include <genericAPI/swigexception.hh>
#include <genericAPI/ilwdfile.hh>  
   
// Local Header Files
#include "eventcmd.hh"
#include "eventregistry.hh"
#include "eventdata.hh"   
#include "eventerrors.hh"
   
using General::UnitTest;
using ILwd::ID_ILWD;      
using ILwd::LdasElement;      
using ILwd::LdasContainer;   
using ILwd::Format;
using ILwd::Compression;      
   
using namespace std;  
using namespace eventMonitor;   

   
// To see detailed test information, set environment variable:
// setenv TEST_VERBOSE_MODE true    
   
UnitTest eventTest;
   
string filePath( const char* Env, const char* file_name )
{
   // attach source directory to file name
   std::string path;

   if( getenv( Env ) )
   {
      path  = getenv( Env );
      path += "/";
   }
   path += file_name;

   return path;
}
   
   
static const CHAR* process_file( "process.ilwd" );   
static const CHAR* output_file( "output.ilwd" );     
static const string cont_string( "_p_LdasContainer" );
static const string null_msg( "{}" );

LdasContainer* readILwd( const char* env, const CHAR* name );   

   
//-------------------------------------------------------------------------------
//   
//: Helper function to process one data object read from the file.
//   
//!param: const CHAR* file - File name to read data object from.
//!param: const bool selfDestruct - Flag to indicate if C++ data bucket is self
//+       destructive.
//!param: INT_4U& jobID - A reference to the jobID of read object.   
//
//!return: const std::string - "addData" result string.
//   
const string addFileData( const CHAR* file, const bool selfDestruct,
   INT_4U& jobID )
{
   // ObjectRegistry owns ILWD element 
   LdasContainer* elem( readILwd( "TEST_DATA_DIR", file ) );
   INT_4U id( elem->getJobId() );
   
   if( jobID == 0 )
   {
      // Initialize job id
      jobID = id;
   }
   else if( jobID != id )
   {
      // Verify jobID consistency
      removeJob( jobID );
      eventTest.Check( false ) << "Inconsistent jobID for the input file: "
                               << file << endl << flush;   
      eventTest.Exit();         
   }

   // addData takes over the element ownership
   string result_data( addData( jobID, elem, selfDestruct ) );    
   return result_data;
}
   
   
int main( int argc, char** argv )
try   
{
   // Initialize test
   eventTest.Init( argc, argv );
   
   // Initialize with invalid job id:
   INT_4U jobID( 0 );

   
   //------------------------------------------------------------
   // 1st test: self destructive bucket
   //------------------------------------------------------------   
   bool selfDestruct( true );   

   // Read process info
   string result( addFileData( process_file, selfDestruct, jobID ) );
   
   // Very first data product should not generate result data:
   eventTest.Check( result.empty() == true )
      << "Test #1: read first data file, result (should be empty)=\"" 
      << result << '\"' << endl << flush;   
   
   
   // Read outPut data:
   result = addFileData( output_file, selfDestruct, jobID );
   
   // Force data sorting: read process_file again
   result = addFileData( process_file, selfDestruct, jobID );

   eventTest.Check( result.empty() == false ) 
      << "Result string for test #1 after reading all data products:\"" << result << '\"' << endl << flush;   

   
   // Parse out results:
   istringstream s( result );
   string id_elem, state_elem, mdd_elem, db_elem,
      state_msg, mdd_msg, db_msg;
   s >> id_elem 
     >> state_elem >> state_msg 
     >> mdd_elem >> mdd_msg
     >> db_elem >> db_msg;
  
   eventTest.Check( state_msg == null_msg ) << "state message: " << state_msg << endl;
   eventTest.Check( mdd_msg == null_msg ) << "mdd message: " << mdd_msg << endl;
   eventTest.Check( db_msg == null_msg ) << "db message: " << db_msg << endl;

   string::size_type mdd_pos( mdd_elem.find( cont_string ) ),
                     db_pos( db_elem.find( cont_string ) );

   eventTest.Check( state_elem == "0" ) << "state=" << state_elem << endl;
   eventTest.Check( mdd_pos != std::string::npos ) << "mdd=" << mdd_elem << endl;
   eventTest.Check( db_pos != std::string::npos ) << "db="  << db_elem << endl;      
   
   
   //------------------------------------------------------------
   // 2nd test: non self destructive bucket
   //------------------------------------------------------------   
   selfDestruct = false;   
   jobID = 0;

   // Read process info
   result = addFileData( process_file, selfDestruct, jobID );
   eventTest.Check( result.empty() == true )
      << "Test #2: read first data file, result (should be empty)=\"" 
      << result << '\"' << endl << flush;   
   
   
   // Read outPut data:
   result = addFileData( output_file, selfDestruct, jobID );
   
   // Read process_file again (would trigger data sorting for 
   // self destructive C++ data bucket, should not do it here!!!)
   result = addFileData( process_file, selfDestruct, jobID );

   eventTest.Check( result.empty() == true ) 
      << "Result string for test #2 after reading all data products (should be empty):\""
      << result << '\"' << endl << flush;   
   
   // Force data sorting
   result = removeJob( jobID );
   eventTest.Check( result.empty() == false )
      << "Result string for test #2 after \"manual\" extraction: " << result << endl;   
   
   // Make sure job bucket is gone now:
   try
   {
      result = listJob( jobID );
   }
   catch( const LdasException& exc )
   {
      SwigException e( exc ); 
      eventTest.Check( true ) 
         << "Got expected exception: " << e.getResult() << endl << flush;
   }      

   
   //------------------------------------------------------------
   // 3d test: non self destructive bucket with extra data
   //------------------------------------------------------------   
   jobID = 0;

   // Read process info
   result = addFileData( process_file, selfDestruct, jobID );
   eventTest.Check( result.empty() == true )
      << "Test #3: read first data file, result (should be empty)=\"" 
      << result << '\"' << endl << flush;   
   
   
   // Read outPut data:
   result = addFileData( output_file, selfDestruct, jobID );
   
   // Read outPut data:
   result = addFileData( output_file, selfDestruct, jobID );   
   
   // Read process_file again (this is extra data, should get
   // an exception):
   try
   {
      result = addFileData( process_file, selfDestruct, jobID );
      eventTest.Check( false ) 
         << "Should not here (exception was expected): " << result << endl << flush;
   }
   catch( const LdasException& exc )
   {
      SwigException e( exc ); 
      eventTest.Check( true ) 
         << "Got expected exception: " << e.getResult() << endl << flush;
   }      
   
   eventTest.Check( result.empty() == true ) 
      << "Result string for test #3 after reading all data products (should be empty):\"" 
      << result << '\"' << endl << flush;   
   
   // Force data sorting
   result = removeJob( jobID );
   eventTest.Check( result.empty() == false )
      << "Result string for test #3 after \"manual\" extraction: " << result << endl;   
   
   // Make sure job bucket is gone now:
   try
   {
      result = listJob( jobID );
   }
   catch( const LdasException& exc )
   {
      SwigException e( exc ); 
      eventTest.Check( true ) 
         << "Got expected exception: " << e.getResult() << endl << flush;
   }      

   
   Registry::elementRegistry.reset();                
   eventTest.Exit();
   return 0;
}
catch( const exception& e )
{
   eventTest.Check( false ) << e.what()
                            << endl << flush;
   eventTest.Exit();   
}
catch( const LdasException& exc )
{
   SwigException e( exc ); 
   eventTest.Check( false ) << e.getResult() << endl << flush;
   eventTest.Exit();
}      
catch( SwigException& e )
{
   string msg = e.getResult();
   eventTest.Check( false, string( "caught exception: " + msg ) );   
   eventTest.Exit();
}       
catch( ... )
{
   eventTest.Check( false ) << "unknown exception"
                            << endl << flush;
   eventTest.Exit();   
}

   
//------------------------------------------------------------------------------
//   
//: Reads ILWD format LdasContainer element from the file.
// 
//!param: const char* name - File name.
//
//!return: LdasContainer* - A pointer to the ILWD format container.
//   
LdasContainer* readILwd( const char* env, const char* name )
{
   ifstream ilwdStream( filePath( env, name ).c_str() );
   
   if( !ilwdStream )
   {
      string msg( "File does not exist: " );
      msg += filePath( env, name );
      throw EventException( msg );   
   }
   
   // Read file header
   if( !ILwd::readHeader( ilwdStream ) )   
   {
      string msg( "Not ILWD format input file: " );
      msg += name;
      throw EventException( msg );
   }
   
   ILwd::Reader r( ilwdStream );
   r.skipWhiteSpace();   

   LdasElement* elem( LdasElement::createElement( r ) );
   
   ilwdStream.close();
   
   if( elem == 0 || elem->getElementId() != ID_ILWD )
   {
      string msg( "Malformed ILWD input file: " );
      msg += name;
      throw EventException( msg );   
   }
   Registry::elementRegistry.registerObject( elem );
   return dynamic_cast< LdasContainer* >( elem );   
}
