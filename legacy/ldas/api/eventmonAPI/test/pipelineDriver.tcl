#!/ldcg/bin/tclsh

## ******************************************************** 
##  pipeline cmd test driver Version 1.0
##
##  contains general procs and main line for sending
##  any stage of pipe line cmds through ldas
##  
## ******************************************************** 

source /ldas_outgoing/LDASapi.rsc
set auto_path "$::LDAS/lib $auto_path"
puts "auto_path=$auto_path"

set API eventmon
package require generic

set localhost [ exec uname -n ]
set ::MGRKEY foo 
set PUBDIR /ldas_outgoing/jobs

proc addLogEntry { args } { return {} }

proc getILwd { filename jobid } {

    puts "file=$filename"
    set filwd "nofd"
    if  { [ catch { 
        set filwd [ openILwdFile $filename "r" ]
        set st [ clock clicks ]
        set datap [ readElement $filwd ] 
        ilwd::setjob $datap $jobid
        set dt [ expr { ([ clock clicks ] - $st) / 1000000.0 } ]
        ;##puts "read and created ilwd object in $dt secs"
        closeILwdFile $filwd 
        } errmsg ] } {
        catch { closeIlwdFile $filwd }
        return -code error $errmsg  
    }
    return $datap       
}

proc send_data { clientp datap } {

	if	{ [ catch {	
		;##	puts "sending element $datap"
        sendElementObject $clientp $datap
        ;## must do this right after the send
        closeDataSocket $clientp
    } err ] } {
	    puts stderr "send_data err: $err"
        catch { closeDataSocket $clientp }
    }	
}

proc retry_connect {} {
    set clientp ""
	for { set retry 0 } { $retry < 1000 } { incr retry 1 } {
	 		set clientp [ createDataSocket 0 $::localhost ]
        ;## connect to the server
        	catch { connectDataSocket $clientp $::server $::dataport } err
			if	{ [ isSocketConnected $clientp ] } {
                puts stderr "$retry time, connect status: [ isSocketConnected $clientp ]"	
				break
			} 
	}
    return $clientp
}

## ******************************************************** 
##
## Name: getSockData
##
## Description:
## read response for a cmd issued to emergency socket of an API
##
## Usage:
##
## Comments:
proc getSockData { sid var } {
	if	{ [ catch {
	    set seqpt "cmd::result $sid"
	    set $var [ cmd::result $sid ]
    } err ] } {
        set $var $err
	}
    catch { close $sid }
}

## ******************************************************** 
##
##
## getEmergData 
## 
## Description 
## connects to ldas mgr at site and gets cntlmonAPI operator port
## number at a site  
## Parameters 
## host - ldas manager host
## port - ldas manager port number 
## 
## Usage 
## set target [ getcntlmonPort ldas-dev.ligo.caltech.edu 10002 ]
## 
## Comments:
## uniqid does not have :: in front 
## get data from an API's emergency socket 
## msg format e.g. set msg "\{puts \$cid \[ validService cntlmon operator \]\}"
 
proc getEmergData { host port msg } {

    set uniqid [ key::time ]
    set ::$uniqid {}
    if 	{ [ catch { 
        set sid [ socketTimeout $host $port 5000 ]
    } err ] } {
        catch { close $sid }
        return -code error "connect error $host@$port: $err"
    }
    if  { [ catch {  
        fconfigure $sid -buffering line
        puts $sid "$::MGRKEY $msg"
        fileevent $sid readable \
            [ list getSockData $sid ::$uniqid ] 
        vwait ::$uniqid
    } err ] } {
        catch { close $sid }
        return -code error "connect error $host@$port: $err"
    }
    set retval [ set ::$uniqid ]
    unset ::$uniqid
    return $retval   
}


## ******************************************************** 
##
## Name: socketTimeout 
##
## Description:
## make client socket connection with a timeout
##
## Usage:
##
## Comments:

proc socketTimeout { host port timeout } {
    set ::connected ""
    after $timeout {set ::connected timeout}
    set sid [ socket $host $port ]
    fileevent $sid w {set ::connected ok}
    vwait ::connected
    if  { ! [ string compare $::connected "timeout" ] } {
        return -code error timeout
    } else {
        return $sid
    }
}

proc setports { api } {
    ;## find out eventmonAPI ports
    set result [ getEmergData dataserver 10002 "puts \$cid \[array get ::${api} \]"  ]
    set result [ split $result \n ]
    array set ::${api} [ lindex $result 0 ]
    puts "$api [ array get ::${api} ]"
}

;## find out ports for eventmon, metadata and ligolw APIs
set APIs { eventmon metadata ligolw }
foreach api $APIs {
    setports $api
}

;## issue the user cmd to kick off the wrapper
set jobscript [ lindex $argv 0 ]
set dir ./data
;## source job script to start wrapper 
puts "running job from $jobscript"
set scriptname [ file tail $jobscript ]
regexp {([^\.]+)\.*} $scriptname -> scriptname
source $jobscript

if  { [ regexp {(NORMAL\d+)} $data -> jobid ] } {
    puts "My job number is $jobid "
}

after 2000

;## instruct the eventmonAPI to send data to ligolw and metadata
puts [ getEmergData $::eventmon(host) $::eventmon(emergency) \
"array set ::${jobid} \[ list -datacondtarget notmetadata -mddapi ligolw \]; set ::jobid ::${jobid}" ]

puts "[ getEmergData $::eventmon(host) $::eventmon(emergency) \
 "puts \$cid \[ set jobid $jobid; source $::LDAS/shared/macros/eventmon/getJobOpts.eventmon \]" ]\n"
 
;## instruct the ligolwAPI to convert the data to LW, dont split this line 
puts [ getEmergData $::ligolw(host) $::ligolw(emergency) \
"array set ::${jobid} \[ list -returnprotocol file:${scriptname}.xml -returnformat ligolw -mddapi ligolw \]; set ::jobid ::${jobid}" ]

puts "[ getEmergData $::ligolw(host) $::ligolw(emergency) \
"puts \$cid \[ set jobid $jobid; source $::LDAS/shared/macros/ligolw/getLWdoc.ligolw \]" ]\n"

;## instruct the metadataAPI to ingest the data

puts [ getEmergData  $::metadata(host) $::metadata(emergency) \
"array set ::${jobid} \[ list -subject \"testing eventmon\" -ingestdata port:tbd -database cit_test \]; set ::jobid ::${jobid}" ]

puts [ getEmergData $::metadata(host) $::metadata(emergency) \
"puts \$cid \[ set jobid $jobid; source $::LDAS/shared/macros/metadata/putMetaData.metadata \]" ]


after 2000
;## clean up in case job did not complete 
puts [ getEmergData $::eventmon(host) $::eventmon(emergency) \
"eventmon::killJobs $jobid \{\} "$jobid process error $err\"" ]

;## clean up in case job did not complete, to be replaced by killJobs 
puts [ getEmergData $::ligolw(host) $::ligolw(emergency) \
"catch { unset ::${jobid} }" ]

;## clean up in case job did not complete, to be replaced by killJobs 
puts [ getEmergData $::metadata(host) $::metadata(emergency) \
"catch { unset ::${jobid} }" ]
