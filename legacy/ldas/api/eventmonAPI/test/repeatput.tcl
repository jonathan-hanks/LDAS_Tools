#! /ldcg/bin/tclsh

set times [ lindex $argv 0 ]
set date [ clock format [ clock seconds ] -format "%m%d" ]

for { set i 0 } { $i < $times } { incr i 1 } {
	catch { exec ./putStandAlone.test dev_1 >& testlogs/putStandAlone.${date}_$i } err
	set  rc [ catch { exec grep -ub -E {--regexp=(Assertion|abort)} putStandAlone.log$i } err ]
	puts "i=$i, rc=$rc,err=$err"
	if	{ ! $rc } {
		exit
	}
}

foreach logfile [ lsort [ glob testlogs/putStandAlone.${date}_* ] ] {
    catch { exec tail -1 $logfile } err
    if	{ ! [ string equal "TEST PASSED" $err ] } {
    	puts "$logfile failed $err"
	set flag 1
    }
}  	

if  { [ info exist flag ] } {
    puts "TEST FAILED"
] else {
    puts "TEST PASSED"
}
