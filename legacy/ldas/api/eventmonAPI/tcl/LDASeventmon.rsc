## ********************************************************
## 
## Name: LDASeventmon.rsc;;
##
## This is the eventmon API specific resource file.  It contains
## resource information which is only used by the eventmon API.
##
## If this file is not found there the eventmon API and hence the
## eventmon API will be non-functional.
##
## For definition rules to support modification via cmonClient,
## see url
## http://ldas-sw.ligo.caltech.edu/cgi-bin/cvsweb.cgi/ldas/api/cntlmonAPI/tcl/client/cmonClient.rsc?rev=HEAD;content-type=text%2Fplain
## ********************************************************
#barecode

;## THIS SECTION IS NOT VIEWABLE VIA CMONCLIENT

;## cmonClient MODIFIABLE RESOURCES 

;## desc=max millisecs before user cmd times out 
set ::MAXMACROTIME [ expr ( $::MANAGER_ABORT_AFTER_N_SECONDS_IN_ONE_API * 1000 ) + 60000 ]

;## desc=state ilwd file format 
set ::STATEILWDFORMAT binary

;## desc=debugging, 2 dumps metadata 3 mdd & metadata 
set ::DEBUG 1

;## desc=delta for additional datasocket port 
set ::PORTDELTA 500

;## desc=maximum #job data items to process in 1 round
set ::MAXJOBSROUND 99

;## desc=multi dim data target 
set ::MDD_TARGETS (frame|ligolw)

;## desc=max database insertion rate in rows/sec of analyzed data (-1 = no limit)
set ::MAX_DB_INSERTIONS_PER_SEC 1000

;## desc=max time allowed for sleeping 
set ::EVENTMON_THREAD_TIMEOUT_SECS 50

;## desc=extra debug log for threads
set ::DEBUG_THREADS 0

;## desc=path to lsof executable
set ::PATH_TO_LSOF /usr/sbin/lsof

;## desc=virtual resource limit
array set ::RESOURCE_LIMIT [ list vmemoryuse default core default cputime default datasize default \
filesize default memorylocked default descriptors default maxproc default ]
