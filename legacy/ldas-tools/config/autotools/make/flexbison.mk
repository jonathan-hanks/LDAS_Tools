#------------------------------------------------------------------------
# YACC rules
#------------------------------------------------------------------------
%.hh %.cc: %.yy
	$(YACC) $(AM_YFLAGS) $(YFLAGS) $<
	protector="$(NAMESPACE)__$(*F)_HH"; \
	protector=`echo $$protector | tr '[:lower:]' '[:upper:]'`; \
	ccprotector="$(NAMESPACE)__$(*F)_CC"; \
	ccprotector=`echo $$ccprotector | tr '[:lower:]' '[:upper:]'`; \
	echo "DEBUG: protector: $$protector ccprotector: $$ccprotector" ; \
	sed -e 's/PARSER_HEADER_H/'$$protector'/g' \
	    -e '/^#ifdef '$$ccprotector'.*$$/,/^#endif .* '$$ccprotector'.*$$/w $(*F).tmp' \
	    -e '/^#ifdef '$$ccprotector'.*$$/,/^#endif .* '$$ccprotector'.*$$/d' \
	    $(*F).hh > $(*F).hh.tmp; \
	cat $(*F).tmp >> $(*F).hh.tmp; \
	mv $(*F).hh.tmp $(*F).hh; \
	awk '/^#include/ { print "#define '$$ccprotector'"; } {print $0; }' \
	  $(*F).cc > $(*F).cc.tmp ; \
	mv $(*F).cc.tmp $(*F).cc
	rm -f $(*F).tmp

#------------------------------------------------------------------------
# LEX rules
#------------------------------------------------------------------------

%.cc: %.ll
	if test $< = `basename $<`; \
	then $(LEX) $(FLEX_SKL) $(srcdir)/$< ;\
	else $(LEX) $(FLEX_SKL) $< ; \
	fi
	if test -f lex.yy.cc; \
	then \
	  mv lex.yy.cc $@; \
	fi

clean-local: clean-local-bison

clean-local-bison:
	rm -rf location.hh position.hh stack.hh
