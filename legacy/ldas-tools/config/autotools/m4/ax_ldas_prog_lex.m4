dnl======================================================================
dnl AX_LDAS_PROG_LEX
dnl   Check on how to setup lex
dnl Depends:
dnl======================================================================
AC_DEFUN([AX_LDAS_PROG_LEX],
[ AM_PROG_LEX
  case x$LEX in
  x/*)
    ;;
  *)
    AC_PATH_PROGS(LEX,flex lex)
    ;;
  esac
  case x$LEX in
  x/usr/bin*)
    ;;
  *)
    tmp="`echo $LEX | sed -e 's,/bin/.*$,/include,g'`"
    echo $LEX_CPPFLAGS | grep "${tmp}" > /dev/null 2>&1
    case $? in
    0)
      ;;
    *)
      LEX_CPPFLAGS="${LEX_CPPFLAGS} -I${tmp}"
    esac
    ;;
  esac
  $LEX --version > /dev/null 2>&1
  AS_CASE([$?],
          [0],[LDAS_CHECK_HEADER_FLEX_LEXER_H
	       dnl------------------------------------------------------
	       dnl If the version of flex installed on the destination
	       dnl  machine is different from what generated the
	       dnl  sources, then remove the sources so they will be
	       dnl  regenerated.
	       dnl------------------------------------------------------
    	       save_CPPFLAGS=${CPPFLAGS}
	       CPPFLAGS="${LEX_CPPFLAGS} ${CPPFLAGS}"
    	       AC_EGREP_CPP([yy_current_buffer],
    	                    [ #include <FlexLexer.h>
    		            ],
    		            [ AC_DEFINE([HAVE_YY_CURRENT_BUFFER],
                                        [1],
			                [Defined if yy_current_buffer is in yyFlexLexer class])
    		            ],
    		            [ AC_DEFINE([YY_CURRENT_BUFFER_EQUIV],
                                        [(yy_buffer_stack[[yy_buffer_stack_top]])],
                                        [Defined as the equivelent for yy_current_buffer])
    		            ])
	       AC_LANG_PUSH([C++])
	       AC_COMPILE_IFELSE([AC_LANG_PROGRAM([
	                          #include <FlexLexer.h>
				  ],
				  [
				    return 0;
				  ])
		                 ],
				 [_ldas_flex_lexer_h_broken="0"],
				 [_ldas_flex_lexer_h_broken="1"])
	       AC_LANG_POP([C++])
    	       CPPFLAGS=${save_CPPFLAGS}
   	       unset save_CPPFLAGS
	       AS_IF([test x$_ldas_flex_lexer_h_broken != "x1"],
	             [ldas_using_flex="1"])
	       ])
  AC_SUBST([LEX_CPPFLAGS])
  AM_CONDITIONAL([LDAS_USING_FLEX],[test "x$ldas_using_flex" = "x1"])
])
