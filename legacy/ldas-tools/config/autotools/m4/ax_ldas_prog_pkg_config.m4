# ===========================================================================
# ===========================================================================
#
# SYNOPSIS
#
#   AX_LDAS_PROG_PKG_CONFIG
#
# DESCRIPTION
#
#
# LICENSE
#

AC_DEFUN([AX_LDAS_PROG_PKG_CONFIG],
[ PKG_PROG_PKG_CONFIG
  AC_SUBST([PKG_CONFIG_PATH])
  AC_ARG_VAR([PKG_CONFIG_PATH],[directories to add to pkg-config's search path])
])
