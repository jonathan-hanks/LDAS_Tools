dnl ---------------------------------------------------------------------
dnl  LDAS_CXX_TEMPLATE_ALIASES
dnl     Check if C++ supports template aliasing
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_TEMPLATE_ALIASES_VIA_USING],
[
AC_MSG_CHECKING([if the C++ compiler supports template aliasing via using directive])
AC_LANG_PUSH([C++])
AC_TRY_COMPILE([
#include <memory>
namespace MySpace {
#define SharedPtr shared_ptr
  using std::SharedPtr;
}
],[
 MySpace::SharedPtr< int >	a;
],[
  AC_DEFINE([HAVE_CXX_TEMPLATE_ALIASES_VIA_USING],[1],[Defined if C++ supports template aliasing via using directive])
  AC_MSG_RESULT([yes])
  HAVE_CXX_TEMPLATE_ALIASES_VIA_USING=1
],[
  AC_MSG_RESULT([no])
  HAVE_CXX_TEMPLATE_ALIASES_VIA_USING=0
])
AC_LANG_POP([C++])
AC_SUBST([HAVE_CXX_TEMPLATE_ALIASES_VIA_USING])
])

dnl ---------------------------------------------------------------------
dnl  LDAS_CXX_TEMPLATE_ALIASES
dnl     Check if C++ supports template aliasing
dnl ---------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_TEMPLATE_AS_TEMPLATE_PARAMETER],
[ AC_MSG_CHECKING([if the C++ compiler supports template as template parameter])
  AC_LANG_PUSH([C++])
  AC_TRY_COMPILE([
  #include <algorithm>

  template< class T >
  inline T* DeletePtr( T* Ptr )
  {
    delete Ptr;
    return (T*)0;
  }

  template< class O, template<class> class C >
  inline void Purge( C< O* >& Container )
  {
    std::transform( Container.begin( ), Container.end( ),
		    Container.begin( ), DeletePtr< O > );
  }  
  ],[
  std::list< int* >	s;

  Purge( s );
  ],[
  AC_DEFINE([SUPPORTED_TEMPLATES_AS_TEMPLATE_PARAMETERS],[1],[Defined if C++ supports templates as template parameters])
  AC_MSG_RESULT([yes])
  SUPPORTED_TEMPLATES_AS_TEMPLATE_PARAMETERS=1
  ],[
  AC_MSG_RESULT([no])
  SUPPORTED_TEMPLATES_AS_TEMPLATE_PARAMETERS=0
  ])
  AC_SUBST([SUPPORTED_TEMPLATES_AS_TEMPLATE_PARAMETERS])
  AC_LANG_POP([C++])
])

#------------------------------------------------------------------------
#
#------------------------------------------------------------------------
AC_DEFUN([LDAS_CXX_EXCEPTIONS_RETHROW],[
  dnl -------------------------------------------------------------------------
  dnl Check how well compiler handles exceptions
  dnl -------------------------------------------------------------------------

  AC_MSG_CHECKING(if compiler supports rethrowing of exceptions)
  AC_TRY_RUN([
  #include <iostream>
  #include <stdexcept>

  //
  // Extended bad_exception class with info on exception no handled in the
  // throw specifier
  //

  class unhandled: public std::bad_exception
  {
  public:
    unhandled( const std::string& what )
      : m_what( what )
    {
    }
  
    ~unhandled() throw() {}
  
    virtual const char*
    what() const throw()
    {
      return m_what.c_str();
    }
  
  private:
    std::string m_what;
  };

  //
  // New handler for unexpected exceptions
  //

  void
  handle_unexpected( void )
  {
    try {
      throw;
    }
    catch( const std::logic_error& e )
    {
	exit( 0 );
    }
  }

  //
  // Problem do to logic_error not being in throw specification
  //

  void
  c3( void ) throw ( std::bad_exception )
  {
    throw std::logic_error("c3");
  }

  int
  main()
  {
    std::set_unexpected( handle_unexpected );
  
    try {
      c3();
    }
    catch ( const std::logic_error& e )
    {
      exit (1);
    }
    catch ( const std::bad_exception& e )
    {
      exit (0);
    }
    return( 1 );
  }
  ],[AC_DEFINE([HAVE_RETHROW_WORKING_IN_BAD_EXCEPTION],
               [1],
	       [Defined if retrow works in bad_exception])
  AC_MSG_RESULT(yes)],[AC_MSG_RESULT(no)])

])
