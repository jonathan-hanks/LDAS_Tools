AC_DEFUN([LDAS_STRUCT_PROCFS_PSINFO],[dnl
  AC_MSG_CHECKING([if process info is in /proc/<pid>/psinfo])
  AC_LANG_PUSH([C])
  AC_TRY_RUN([
  #include <unistd.h>
  #include <stdio.h>


  int
  main()
  {
    int  retval;
    char filename[ 1024 ];

    #if SIZEOF_INT == SIZEOF_PID_T
    sprintf( filename, "/proc/%d/psinfo", (int)(getpid( )) );
    #else /* SIZEOF_INT == SIZEOF_PID_T */
    sprintf( filename, "/proc/%ld/psinfo", getpid( ) );
    #endif /* SIZEOF_INT == SIZEOF_PID_T */

    retval = access( filename, R_OK );
    
    return retval;
  }
  ],[AC_MSG_RESULT(yes)
     AC_DEFINE([HAVE_PROCFS_PSINFO],[1],[Defined if /proc/<pid>/psinfo exists])
  ],[AC_MSG_RESULT(no)])
  AC_LANG_POP([C])
])

AC_DEFUN([LDAS_STRUCT_PROCFS_STAT],[dnl
  AC_MSG_CHECKING([if process info is in /proc/<pid>/stat])
  AC_LANG_PUSH([C++])
  AC_TRY_RUN([
  #include <unistd.h>

  #include <fstream>
  #include <sstream>

  main()
  {
    std::ostringstream filename;

    filename << "/proc/"
    #if SIZEOF_INT == SIZEOF_PID_T
  	     << (int)::getpid( )
    #else /* SIZEOF_INT == SIZEOF_PID_T */
  	     << ::getpid( )
    #endif /* SIZEOF_INT == SIZEOF_PID_T */
       	    << "/stat";
    std::ifstream f( filename.str( ).c_str( ) );
    const int retval =
      ( ( f.is_open( ) ) ? 0 : 1 );
    f.close( );
    return retval;
  }
  ],[AC_MSG_RESULT(yes)
     AC_DEFINE([HAVE_PROCFS_STAT],[1],[Defined if /proc/<pid>/stat exists])
  ],[AC_MSG_RESULT(no)])
  AC_LANG_POP([C++])
])