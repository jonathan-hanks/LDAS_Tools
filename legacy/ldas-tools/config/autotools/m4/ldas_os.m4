dnl======================================================================
dnl LDAS_OS
dnl   Check for the operating system flavor
dnl======================================================================

AC_DEFUN([LDAS_OS],[
  dnl -------------------------------------------------------------------
  dnl  Information about the current system for which LDAS is being
  dnl    configured.
  dnl -------------------------------------------------------------------
  ldas_kernel=`uname -s`
  ldas_processor=`uname -p`
  ldas_hardware=`uname -m`
  dnl -------------------------------------------------------------------
  dnl  Identify various operating systems
  dnl -------------------------------------------------------------------
  AM_CONDITIONAL([LDAS_BUILD_OS_DARWIN],[test x$ldas_kernel = xDarwin])
]) dnl AC_DEFUN - LDAS_OS

dnl======================================================================
dnl Check for naming of library load path variable
dnl======================================================================
AC_DEFUN([LDAS_CHECK_OS_VARIABLE_LIBRARY_LOAD_PATH],[
  AC_REQUIRE([LDAS_OS])
  AS_CASE([${ldas_kernel}],
          [Darwin],[OS_LIBRARY_PATH=DYLD_LIBRARY_PATH],
	  [OS_LIBRARY_PATH=LD_LIBRARY_PATH])
  AC_SUBST([OS_LIBRARY_PATH])
  AC_MSG_CHECKING([for variable to use with dynamic loader])
  AC_MSG_RESULT([${OS_LIBRARY_PATH}])
])
