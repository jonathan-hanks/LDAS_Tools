dnl======================================================================
dnl AX_LDAS_PKG_CHECK
dnl   Check for LDAS packages
dnl======================================================================

AC_DEFUN([AX_LDAS_PKG_CHECK],[
  PKG_CHECK_EXISTS( [$1],[
  		    PKG_CHECK_MODULES(AS_TR_CPP($1),[$1])
		    dnl INCLUDES
		    AS_TR_CPP($1_INCLUDES)="`$PKG_CONFIG --cflags-only-I $1`"
		    AC_SUBST(AS_TR_CPP($1_INCLUDES))
		    dnl LIBDIRS
		    AS_TR_CPP($1_LIBDIRS)="`$PKG_CONFIG --variable=LIBDIRS $1`"
		    AC_SUBST(AS_TR_CPP($1_LIBDIRS))
		    dnl PYTHONPATH
		    AS_TR_CPP($1_PYTHONPATH)="`$PKG_CONFIG --variable=PYTHONPATH $1`"
		    AC_SUBST(AS_TR_CPP($1_PYTHONPATH))
		    dnl SWIGFLAGS
		    AS_TR_CPP($1_SWIGFLAGS)="`$PKG_CONFIG --variable=SWIGFLAGS $1`"
		    AC_SUBST(AS_TR_CPP($1_SWIGFLAGS))
  ])
]) dnl AC_DEFUN - AX_LDAS_PKG_CHECK
