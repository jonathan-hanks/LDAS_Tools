dnl======================================================================
dnl AX_LDAS_PROG_LATEX
dnl======================================================================
AC_DEFUN([AX_LDAS_PROG_LATEX],
[ AC_ARG_ENABLE([latex],
	         AC_HELP_STRING([--enable-latex],
                    	        [use latex (default is CHECK)]),
	         [ case "${enable_latex}" in
	           no)
		     ;;
                   *)
		     ;;
                   esac ],
	         [enable_latex="CHECK";] )

  case $enable_latex in
  no)
    unset LATEX
    ;;
  *)
    AC_PATH_PROGS( LATEX, latex )
    ;;
  esac
  dnl AC_MSG_CHECKING(for latex)
  dnl case x$LATEX in
  dnl x) 
  dnl   case $enable_latex in
  dnl   CHECK|no)
  dnl     AC_MSG_RESULT(no)
  dnl     ;;
  dnl   *)
  dnl     AC_MSG_ERROR(no)
  dnl     ;;
  dnl   esac
  dnl   ;;
  dnl *) AC_MSG_RESULT($LATEX)
  dnl   ;;
  dnl esac
  AM_CONDITIONAL(HAVE_LATEX, test x$LATEX != x)
])
