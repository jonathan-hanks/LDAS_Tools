# LDAS Tools Suite - NEXT

  - General
    - Removed support for Python 2
    - Added CI/CD support for Rocy Linux 9
    - Modified CI/CD pipeline for Debian Buster to be manual
    - Modified CI/CD pipeline for fedora35 to be manual
    - Modified CI/CD pipeline for fedora34 to be manual
    - Added CI/CD pipeline for Enterpise Linux 7
    - Renamed Debian Bullseye files to include release number
    - Added CI/CD pipeline for Debian 12 bookworm

# LDAS Tools Suite

  - General
    - Fixed ticket #64
      - Updated homepage URL
       - Updated bug URL

  - ldas-tools-framecpp -
    - Converted to CMake (closes #33)
    - Converted framecpp_sample to use boost::program_options
    - Converted unit tests to use Boost::Test
    - Added over arching directive to build only FrameCPP library and support packages (closes #63).

# LDAS Tools Suite - 20190202

  - ldas-tools-cmake - 1.0.7
    - Corrected cm_init to provide Autotools defines as strings
    - Added cx_target_test
    - Moved most cm_ functions from Autotools/ArchiveX to Autotools
  - ldas-tools-framecpp-swig - 2.6.8
     - Corrections for conda build on OSX
     - Parameterized python build rules to minimize copy/paste errors

