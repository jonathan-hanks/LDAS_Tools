#-----------
set(options -i -style=file --fallback-style=none)
find_program( CLANG_FORMAT_PROGRAM
  NAMES
  clang-format-3.8
  clang-format-mp-3.8
  clang-format
  )
message(STATUS ${CLANG_FORMAT_PROGRAM})
if ( CLANG_FORMAT_PROGRAM )
  file( GLOB_RECURSE glob
    RELATIVE ${SOURCE_DIR}
    *.c
    *.c.in
    *.cc
    *.cc.in
    *.cxx
    *.cpp
    *.h
    *.h.in
    *.hh
    *.hh.in
    *.hpp
    *.hxx
    )
  foreach(file ${glob})
    execute_process( COMMAND ${CLANG_FORMAT_PROGRAM} ${file} ${options})
  endforeach( )
endif( )
