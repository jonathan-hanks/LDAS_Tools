function(get_cmake_value RESULT DIR VAR)
  file(READ ${DIR}/CMakeLists.txt input)
  string(REPLACE "\n" ";" input "${input}")
  foreach( line ${input})
    if ( line MATCHES "set.*${VAR}" )
      string(REPLACE "set" "" line ${line})
      string(REPLACE "\(" "" line ${line})
      string(REPLACE "\)" "" line ${line})
      string(REPLACE "${VAR}" "" line ${line})
      string(STRIP "${line}" line)
      set(${RESULT} ${line})
      break()
    endif( )
  endforeach( )
  set(${RESULT} "${${RESULT}}" CACHE INTERNAL "" FORCE)
endfunction( )

function(get_cmake_values SUBDIR)
  list(LENGTH ARGN size)
  while( size GREATER 1 )
    list(GET ARGN 0 result)
    list(GET ARGN 1 var)
    if ( VERBOSE )
      message( WARNING "result: ${result} var: ${var}" )
    endif( )
    get_cmake_value( ${result} ${TOP_DIR}/${SUBDIR} ${var})
    list(REMOVE_AT ARGN 0 1)
    list(LENGTH ARGN size)
  endwhile( )
endfunction( )

get_cmake_values(
  ldastools_cmake
  ldastools_cmake_name     PROJECT_NAME
  ldastools_cmake_ver      PROJECT_VERSION
  ldastools_igwn_cmake_ver IGWN_CMAKE_VERSION
  )
get_cmake_values(
  diskcache
  diskcache_name LDAS_TOOLS_DISKCACHEAPI_PACKAGE_NAME
  diskcache_ver  LDAS_TOOLS_DISKCACHEAPI_VERSION
  )
get_cmake_values(
  diskcache-swig
  diskcache_swig_name LDAS_TOOLS_DISKCACHEAPI_SWIG_PACKAGE_NAME
  diskcache_swig_ver  LDAS_TOOLS_DISKCACHEAPI_SWIG_VERSION
  )
get_cmake_values(
  filters
  filters_name LDAS_TOOLS_FILTERS_PACKAGE_NAME
  filters_ver  LDAS_TOOLS_FILTERS_VERSION
  )
get_cmake_values(
  framecpp
  framecpp_name LDAS_TOOLS_FRAMECPP_PACKAGE_NAME
  framecpp_ver  LDAS_TOOLS_FRAMECPP_VERSION
  )
get_cmake_values(
  framecpp-swig
  framecpp_swig_name  LDAS_TOOLS_FRAMECPP_SWIG_PACKAGE_NAME
  framecpp_swig_ver   LDAS_TOOLS_FRAMECPP_SWIG_VERSION
  )
get_cmake_values(
  frameutils
  frameutils_name LDAS_TOOLS_FRAMEAPI_PACKAGE_NAME
  frameutils_ver  LDAS_TOOLS_FRAMEAPI_VERSION
  )
get_cmake_values(
  frameutils-swig
  frameutils_swig_name LDAS_TOOLS_FRAMEAPI_SWIG_PACKAGE_NAME
  frameutils_swig_ver  LDAS_TOOLS_FRAMEAPI_SWIG_VERSION
  )
get_cmake_values(
  ldasgen
  ldasgen_name LDAS_TOOLS_LDASGEN_PACKAGE_NAME
  ldasgen_ver  LDAS_TOOLS_LDASGEN_VERSION
  )
get_cmake_values(
  ldasgen-swig
  ldasgen_swig_name LDAS_TOOLS_LDASGEN_SWIG_PACKAGE_NAME
  ldasgen_swig_ver  LDAS_TOOLS_LDASGEN_SWIG_VERSION
  )
get_cmake_values(
  ldastoolsal
  ldastoolsal_name LDAS_TOOLS_AL_PACKAGE_NAME
  ldastoolsal_ver  LDAS_TOOLS_AL_VERSION
  )
get_cmake_values(
  ldastoolsal-swig
  ldastoolsal_swig_name    LDAS_TOOLS_AL_SWIG_PACKAGE_NAME
  ldastoolsal_swig_ver     LDAS_TOOLS_AL_SWIG_VERSION
  ldas_tools_swig_version   LDAS_TOOLS_SWIG_VERSION
  )
get_cmake_values(
  suite
  suite_name LDAS_TOOLS_SUITE_PACKAGE_NAME
  suite_ver  LDAS_TOOLS_SUITE_VERSION
  )
get_cmake_values(
  utilities
  utilities_name LDAS_TOOLS_UTILITIES_PACKAGE_NAME
  utilities_ver  LDAS_TOOLS_UTILITIES_VERSION
  )

if ( NOT VERBOSE )
  set( VERBOSITY ERROR_QUIET )
endif( )
execute_process(
  ${VERBOSITY}
  COMMAND ${CMAKE_COMMAND} -E
  echo
  "-DIGWN_CMAKE_VERSION=${ldastools_igwn_cmake_ver}"
  "-DLDAS_TOOLS_CMAKE_VERSION=${ldastools_cmake_ver}"
  "-DLDAS_TOOLS_DISKCACHEAPI_VERSION=${diskcache_ver}"
  "-DLDAS_TOOLS_DISKCACHEAPI_SWIG_VERSION=${disckcache_swig_ver}"
  "-DLDAS_TOOLS_FILTERS_VERSION=${filters_ver}"
  "-DLDAS_TOOLS_FRAMECPP_VERSION=${framecpp_ver}"
  "-DLDAS_TOOLS_FRAMECPP_SWIG_VERSION=${framecpp_swig_ver}"
  "-DLDAS_TOOLS_FRAMEAPI_VERSION=${frameutils_ver}"
  "-DLDAS_TOOLS_FRAMEAPI_SWIG_VERSION=${frameutils_swig_ver}"
  "-DLDAS_TOOLS_LDASGEN_VERSION=${ldasgen_ver}"
  "-DLDAS_TOOLS_LDASGEN_SWIG_VERSION=${ldasgen_swig_ver}"
  "-DLDAS_TOOLS_AL_VERSION=${ldastoolsal_ver}"
  "-DLDAS_TOOLS_AL_SWIG_VERSION=${ldastoolsal_swig_ver}"
  "-DLDAS_TOOLS_SWIG_VERSION=${ldas_tools_swig_version}"
  "-DLDAS_TOOLS_SUITE_VERSION=${suite_ver}"
  "-DLDAS_TOOLS_UTILITIES_VERSION=${utilities_ver}"
  "-DPROJECT_DESCRIPTION_LONG=This_is_a_long_description_for_the_project"
  )
