Using the Regular Expression C++ Classes.

The C++ classes Regex & RegexMatch wrap the POSIX regular expression methods.  Regex is used to hold a compiled regex pattern buffer, while RegexMatch holds information about a match.

Using the classes to perform a match is a three step process:

1. Create the Regex

The Regex class is constructed by passing the regular expression and any flags to the Regex Constructor, e.g.:

Regex re_name( "[_A-Z][_A-Z0-9]*", REG_EXTENDED | REG_ICASE );

The flags are identical to the flags defined by POSIX for compiling regular expressions (REG_EXTENDED, REG_ICASE, REG_NOSUB, & REG_NEWLINE).  The flags parameter is optional, it defaults to REG_EXTENDED if not given.

This constructor compiles the regular expression and stores it internally.  This pattern does not need to be recompiled unless the regex changes.  So, it is possible to declare a regex constant and static so that it is only compiled the first time it is needed.

2. Create the RegexMatch class

The RegexMatch class is used to store information about a regular expression match.  The constructor takes one parameter, the number of macthes to keep track of.