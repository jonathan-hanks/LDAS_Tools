# -*- mode: RPM-SPEC; indent-tabs-mode: nil -*-
%define name @PROJECT_NAME@
%define tarbasename %{name}
%define check_boost169 ( 0%{?rhel} && 0%{?rhel} <= 8 )
%define check_cmake3   ( 0%{?rhel} && 0%{?rhel} <= 7 )

#------------------------------------------------------------------------
# OS configuration
#------------------------------------------------------------------------

Summary: LDAS tools abstraction toolkit
Name: %{name}
Version: @PROJECT_VERSION@
Release: 1%{?dist}
License: GPLv2+
URL: @PACKAGE_URL@
Group: Application/Scientific
BuildRoot: %{buildroot}
Source0: @LDAS_TOOLS_SOURCE_URL@/ldas-tools-al-%{version}.tar.gz
Obsoletes: ldas-tools-general <= 2.6
%if %{check_boost169}
Requires: boost169 >= @BOOST_MINIMUM_VERSION@
%else
Requires: boost >= @BOOST_MINIMUM_VERSION@
%endif
Requires: zlib
BuildRequires: gcc, gcc-c++, glibc
BuildRequires: gawk
BuildRequires: make
BuildRequires: rpm-build
Buildrequires: ldas-tools-cmake >= @LDAS_TOOLS_CMAKE_VERSION@
%if %{check_boost169}
BuildRequires: boost169-devel >= @BOOST_MINIMUM_VERSION@, boost169 >= @BOOST_MINIMUM_VERSION@
%else
BuildRequires: boost-devel >= @BOOST_MINIMUM_VERSION@, boost >= @BOOST_MINIMUM_VERSION@
%endif
Buildrequires: pkgconfig
Buildrequires: zlib-devel
%if %{check_cmake3}
BuildRequires: cmake3 >= 3.6, cmake
%else
BuildRequires: cmake >= 3.6
%endif
Buildrequires: doxygen, graphviz


%description
This provides the runtime libraries for the abstaction library.

%package devel
Obsoletes: ldas-tools-general-devel <= 2.6
Group: Development/Scientific
Summary: LDAS tools abstraction toolkit development files
Requires: ldas-tools-al = @PROJECT_VERSION@
%if %{check_boost169}
Requires: boost169-devel >= @BOOST_MINIMUM_VERSION@, boost169 >= @BOOST_MINIMUM_VERSION@
%else
Requires: boost-devel >= @BOOST_MINIMUM_VERSION@, boost >= @BOOST_MINIMUM_VERSION@
%endif
%description devel
This provides the develpement files the abstraction library.
%prep

%setup -c -T -D -a 0 -n %{name}-%{version}

%build
#------------------------------------------------------------------------
# This works around a bug in the current rpmbuild system whereby the
#   PKG_CONFIG_PATH is set by the system and does not allow for
#   user preference.
# This work around should be fixed in RH 7.1 or so
#------------------------------------------------------------------------
export PKG_CONFIG_PATH="${LDASTOOLSDEV_PKG_CONFIG_PATH:-}${LDASTOOLSDEV_PKG_CONFIG_PATH:+:}$PKG_CONFIG_PATH"

%if %{check_boost169}
export BOOST_CONFIGURE_OPTIONS="-DBoost_NO_SYSTEM_PATHS=True -DBOOST_INCLUDEDIR=%{_includedir}/boost169 -DBOOST_LIBRARYDIR=%{_libdir}/boost169"
%endif
%if %{check_cmake3}
export CMAKE_PROGRAM=cmake3
%else
export CMAKE_PROGRAM=cmake
%endif

${CMAKE_PROGRAM} \
    -DCMAKE_INSTALL_PREFIX=%{_prefix} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
    -DCMAKE_INSTALL_DOCDIR=%{_docdir} \
    ${BOOST_CONFIGURE_OPTIONS} \
    %{tarbasename}-%{version}

make VERBOSE=1 %{?_smp_mflags}


%install
rm -rf %{buildroot}
#--------------------------------------------------------------
# install lscsoft specific files
#--------------------------------------------------------------
make V=1 install DESTDIR=%{buildroot}
#--------------------------------------------------------------
# Removed unwanted libtool files
#--------------------------------------------------------------
find %{buildroot} -name \*.la -exec rm -f {} \;
#--------------------------------------------------------------
# Remove unwanted python init files
#--------------------------------------------------------------
find %{buildroot} -name __init__.py* -exec rm -f {} \;

%post
ldconfig
%postun
ldconfig

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root,-)
%{_libdir}/libldastoolsal*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}/ldastoolsal
%{_libdir}/libldastoolsal.a
%{_libdir}/libldastoolsal*.so
%{_libdir}/pkgconfig/ldastoolsal.pc
%{_docdir}

%changelog
* Wed Sep 29 2021 Edward Maros <ed.maros@ligo.org> - 2.6.7-1
- Built for new release

* Fri Aug 13 2021 Edward Maros <ed.maros@ligo.org> - 2.6.6-1
- Built for new release

* Tue Mar 9 2021 Edward Maros <ed.maros@ligo.org> - 2.6.5-1
- Built for new release

* Wed Oct 23 2019 Edward Maros <ed.maros@ligo.org> - 2.6.4-1
- Built for new release

* Thu Dec 06 2018 Edward Maros <ed.maros@ligo.org> - 2.6.2-1
- Built for new release

* Tue Nov 27 2018 Edward Maros <ed.maros@ligo.org> - 2.6.1-1
- Built for new release

* Sat Oct 22 2016 Edward Maros <ed.maros@ligo.org> - 2.5.5-1
- Built for new release

* Mon Oct 10 2016 Edward Maros <ed.maros@ligo.org> - 2.5.4-1
- Built for new release

* Mon Sep 26 2016 Edward Maros <ed.maros@ligo.org> - 2.5.3-1
- Added ability to have verbose debugging of Threads class

* Wed Mar 23 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.4-1
- Made build be verbose

* Fri Mar 11 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.1-1
- Corrections for RPM build

* Thu Mar 03 2016 Edward Maros <ed.maros@ligo.org> - 2.4.99.0-1
- Breakout into separate source package

* Tue Oct 11 2011 Edward Maros <emaros@ligo.caltech.edu> - 1.19.13-1
- Initial build.
