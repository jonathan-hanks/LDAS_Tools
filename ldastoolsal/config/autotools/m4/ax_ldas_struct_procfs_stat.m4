#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#

AC_DEFUN([AX_LDAS_STRUCT_PROCFS_STAT],[dnl
  AC_MSG_CHECKING([if process info is in /proc/<pid>/stat])
  AC_LANG_PUSH([C++])
  AC_TRY_RUN([
  #include <unistd.h>

  #include <fstream>
  #include <sstream>

  main()
  {
    std::ostringstream filename;

    filename << "/proc/"
    #if SIZEOF_INT == SIZEOF_PID_T
  	     << (int)::getpid( )
    #else /* SIZEOF_INT == SIZEOF_PID_T */
  	     << ::getpid( )
    #endif /* SIZEOF_INT == SIZEOF_PID_T */
       	    << "/stat";
    std::ifstream f( filename.str( ).c_str( ) );
    const int retval =
      ( ( f.is_open( ) ) ? 0 : 1 );
    f.close( );
    return retval;
  }
  ],[AC_MSG_RESULT(yes)
     AC_DEFINE([HAVE_PROCFS_STAT],[1],[Defined if /proc/<pid>/stat exists])
  ],[AC_MSG_RESULT(no)])
  AC_LANG_POP([C++])
])
