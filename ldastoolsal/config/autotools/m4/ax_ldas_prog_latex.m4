dnl======================================================================
dnl AX_LDAS_PROG_LATEX
dnl======================================================================
#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#


AC_DEFUN([AX_LDAS_PROG_LATEX],
[ AC_ARG_ENABLE([latex],
	         AC_HELP_STRING([--enable-latex],
                    	        [use latex (default is CHECK)]),
	         [ case "${enable_latex}" in
	           no)
		     ;;
                   *)
		     ;;
                   esac ],
	         [enable_latex="CHECK";] )

  case $enable_latex in
  no)
    unset LATEX
    ;;
  *)
    AC_PATH_PROGS( LATEX, latex )
    ;;
  esac
  dnl AC_MSG_CHECKING(for latex)
  dnl case x$LATEX in
  dnl x) 
  dnl   case $enable_latex in
  dnl   CHECK|no)
  dnl     AC_MSG_RESULT(no)
  dnl     ;;
  dnl   *)
  dnl     AC_MSG_ERROR(no)
  dnl     ;;
  dnl   esac
  dnl   ;;
  dnl *) AC_MSG_RESULT($LATEX)
  dnl   ;;
  dnl esac
  AM_CONDITIONAL(HAVE_LATEX, test x$LATEX != x)
])
