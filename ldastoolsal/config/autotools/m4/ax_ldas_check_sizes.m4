#
# LICENSE
#
#  Copyright (C) 2018 California Institute of Technology
#
#  This program is free software; you may redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 (GPLv2) of the
#  License or at your discretion, any later version.
#
#  This program is distributed in the hope that it will be useful, but
#  without any warranty or even the implied warranty of merchantability
#  or fitness for a particular purpose. See the GNU General Public
#  License (GPLv2) for more details.
#
#  Neither the names of the California Institute of Technology (Caltech),
#  The Massachusetts Institute of Technology (M.I.T), The Laser
#  Interferometer Gravitational-Wave Observatory (LIGO), nor the names
#  of its contributors may be used to endorse or promote products derived
#  from this software without specific prior written permission.
# 
#  You should have received a copy of the licensing terms for this
#  software included in the file LICENSE located in the top-level
#  directory of this package. If you did not, you can view a copy at
#  http://dcc.ligo.org/M1500244/LICENSE
#
#

AC_DEFUN([AX_LDAS_CHECK_SIZES],[
AC_TYPE_SIZE_T
AC_CHECK_SIZEOF( short, 0 )
AC_CHECK_SIZEOF( int, 0 )
AC_CHECK_SIZEOF( long, 0 )
AC_CHECK_SIZEOF( long long, 0 )
AC_CHECK_SIZEOF( float, 0 )
AC_CHECK_SIZEOF( double, 0 )
AC_CHECK_SIZEOF( void*, 0 )
AC_CHECK_SIZEOF( pid_t, 0 )
AC_CHECK_SIZEOF( size_t, 0 )

dnl ---------------------------------------------------------------------------
dnl 2 byte integer
dnl ---------------------------------------------------------------------------
AC_EGREP_CPP([yes],
[#if SIZEOF_SHORT == 2
yes
#endif
],LDAS_2_BYTE_INT=short)
AC_SUBST(LDAS_2_BYTE_INT)

dnl ---------------------------------------------------------------------------
dnl 4 byte integer
dnl ---------------------------------------------------------------------------
AC_EGREP_CPP([yes],
[#if SIZEOF_INT == 4
yes
#endif
],LDAS_4_BYTE_INT=int,
AC_EGREP_CPP([yes],
[#if SIZEOF_LONG == 4
yes
#endif
],LDAS_4_BYTE_INT=long))
AC_SUBST(LDAS_4_BYTE_INT)

dnl ---------------------------------------------------------------------------
dnl 8 byte integer
dnl ---------------------------------------------------------------------------
AC_EGREP_CPP([yes],
[#if SIZEOF_INT == 8
yes
#endif
],LDAS_8_BYTE_INT=int,
AC_EGREP_CPP([yes],
[#if SIZEOF_LONG == 8
yes
#endif
],LDAS_8_BYTE_INT=long,
AC_EGREP_CPP([yes],
[#if SIZEOF_LONG_LONG == 8
yes
#endif
],LDAS_8_BYTE_INT="long long")))
AC_SUBST(LDAS_8_BYTE_INT)

dnl ---------------------------------------------------------------------------
dnl 4 byte real
dnl ---------------------------------------------------------------------------
AC_EGREP_CPP([yes],
[#if SIZEOF_FLOAT == 4
yes
#endif
],LDAS_4_BYTE_REAL=float
LDAS_4_BYTE_REAL_DIGITS=FLT_DIG)
AC_SUBST(LDAS_4_BYTE_REAL)
AC_SUBST(LDAS_4_BYTE_REAL_DIGITS)

dnl ---------------------------------------------------------------------------
dnl 8 byte real
dnl ---------------------------------------------------------------------------
AC_EGREP_CPP([yes],
[#if SIZEOF_DOUBLE == 8
yes
#endif
],LDAS_8_BYTE_REAL=double
LDAS_8_BYTE_REAL_DIGITS=DBL_DIG)
AC_SUBST(LDAS_8_BYTE_REAL)
AC_SUBST(LDAS_8_BYTE_REAL_DIGITS)

dnl ---------------------------------------------------------------------------
dnl memory pointer
dnl ---------------------------------------------------------------------------
AC_EGREP_CPP([yes],
[#if SIZEOF_VOIDP == 4
yes
#endif
],LDAS_VOIDP_INT_TYPE=INT_4U,
AC_EGREP_CPP([yes],
[#if SIZEOF_VOIDP == 8
yes
#endif
],LDAS_VOIDP_INT_TYPE=INT_8U))
AC_SUBST(LDAS_VOIDP_INT_TYPE)
])

