#------------------------------------------------------------------------
# 
#------------------------------------------------------------------------
COPYRIGHT_YEAR ?= $(shell date +%Y)
COPYRIGHT_HOLDER ?= California Institute of Technology
COPYRIGHT_PATH_OPT ?= --add-path
LICENSE_FILE ?= /usr/src/config/data/license.erb

copyright:
	docker run --rm --volume $(abs_top_srcdir):/usr/src \
	osterman/copyright-header:latest $(COPYRIGHT_DRY_RUN) \
	--license-file $(LICENSE_FILE) \
	$(COPYRIGHT_PATH_OPT) . \
	--guess-extension \
	--copyright-holder '${COPYRIGHT_HOLDER}' \
	--copyright-software '$(COPYRIGHT_SOFTWARE)' \
	--copyright-software-description '$(COPYRIGHT_SOFTWARE_DESCRIPTION)' \
	--copyright-year '$(COPYRIGHT_YEAR)' \
	--word-wrap 80 \
	--output-dir '/usr/src'