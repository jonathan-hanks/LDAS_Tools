//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <pthread.h>

#include <iostream>

#ifndef PTHREAD_STACK_MIN
#define PTHREAD_STACK_MIN "<unspecified>"
#endif /* PTHREAD_STACK_MIN */

int
main( int ArgC, char** ArgV )
{
    pthread_attr_t attr;
    size_t         stack_size;

    pthread_attr_init( &attr );

    pthread_attr_getstacksize( &attr, &stack_size );

    std::cout << "Default Thread Stack Size: " << stack_size << std::endl
              << "Minimum Thread Stack Size: " << PTHREAD_STACK_MIN
              << std::endl;
}
