/* * LDASTools AL - A library collection to provide an abstraction layer
 *
 * Copyright (C) 2018 California Institute of Technology
 *
 * LDASTools AL is free software; you may redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 (GPLv2) of the
 * License or at your discretion, any later version.
 *
 * LDASTools AL is distributed in the hope that it will be useful, but
 * without any warranty or even the implied warranty of merchantability
 * or fitness for a particular purpose. See the GNU General Public
 * License (GPLv2) for more details.
 *
 * Neither the names of the California Institute of Technology (Caltech),
 * The Massachusetts Institute of Technology (M.I.T), The Laser
 * Interferometer Gravitational-Wave Observatory (LIGO), nor the names
 * of its contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * You should have received a copy of the licensing terms for this
 * software included in the file LICENSE located in the top-level
 * directory of this package. If you did not, you can view a copy at
 * http://dcc.ligo.org/M1500244/LICENSE
 */

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define FUNC_NAME "doit"

void*
load( char* LibName )
{
    static char lib_buffer[ 2096 ];
    void*       retval = (void*)NULL;

    sprintf( lib_buffer, "%s/lib%s.so", ".", LibName );
    if ( access( lib_buffer, R_OK ) != 0 )
    {
        sprintf( lib_buffer, "%s/lib%s.so", ".libs", LibName );
    }
    retval = dlopen( lib_buffer, RTLD_NOW );
    return retval;
}

int
main( )
{
    void* dlhandle = load( "exception" );
    union
    {
        int ( *call )( );
        void* ref;
    } func;
    int retval = 0;

    if ( dlhandle == (void*)NULL )
    {
        fprintf( stderr,
                 "-- FAIL: Unable to load shared library for testing\n" );
        exit( 1 );
    }
    func.ref = dlsym( dlhandle, FUNC_NAME );
    if ( func.ref == NULL )
    {
        dlclose( dlhandle );
        fprintf( stderr, "-- FAIL: Unable to get function: %s\n", FUNC_NAME );
        exit( 1 );
    }
    retval = ( *( func.call ) )( );
    if ( retval == 0 )
    {
        dlclose( dlhandle );
        fprintf( stderr, "-- FAIL: calling function returned : %d\n", retval );
        exit( 1 );
    }
    dlclose( dlhandle );
    exit( 0 );
}
