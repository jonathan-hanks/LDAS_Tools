//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <unistd.h>
#include <signal.h>

#include "ldastoolsal/unittest.h"

#include "ldastoolsal/SignalHandler.hh"

using LDASTools::AL::SignalHandler;
typedef LDASTools::AL::SignalHandler::Callback    Callback;
typedef LDASTools::AL::SignalHandler::signal_type signal_type;

LDASTools::Testing::UnitTest Test;

void
sig_self( signal_type Signal )
{
    kill( getpid( ), SignalHandler::OSSignal( Signal ) );
}

void
TestCallback( )
{
    static const char* test_name = "TestCallback";

    class cb_ : public Callback
    {
    public:
        cb_( ) : value( 0 )
        {
        }

        virtual void
        SignalCallback( signal_type Signal )
        {
            ++value;
        }

        int value;
    } cb;

    static const signal_type sig = SignalHandler::SIGNAL_HANGUP;

    SignalHandler::Register( &cb, sig );
    sig_self( sig );
    Test.Check( cb.value == 1 ) << test_name << ":"
                                << " Callback has been activated"
                                << " ( " << cb.value << " =?= 1 )" << std::endl;
    SignalHandler::Unregister( &cb, sig );
}

void
TestCallbackAbort( )
{
    static const char* test_name = "TestCallbackAbort";

    class cb_ : public Callback
    {
    public:
        cb_( ) : value( 0 )
        {
        }

        virtual void
        SignalCallback( signal_type Signal )
        {
            ++value;
        }

        int value;
    } cb;

    static const signal_type sig = SignalHandler::SIGNAL_ABORT;

    SignalHandler::Register( &cb, sig );
    sig_self( sig );
    Test.Check( cb.value == 1 ) << test_name << ":"
                                << " Callback has been activated"
                                << " ( " << cb.value << " =?= 1 )" << std::endl;
    SignalHandler::Unregister( &cb, sig );
}

int
main( int argc, char** argv )
{
    Test.Init( argc, argv );

    TestCallback( );
    TestCallbackAbort( );

    Test.Exit( );

    return 0;
}
