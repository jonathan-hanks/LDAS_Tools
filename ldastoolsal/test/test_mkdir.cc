//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <unistd.h>

#include <sstream>

#include "ldastoolsal/unittest.h"

#include "ldastoolsal/MkDir.hh"

using LDASTools::Cmd::MkDir;

LDASTools::Testing::UnitTest Test;

static char CWD[ 4 * 1024 ];

void
test_existing_dir( )
{
    bool status = true;
    try
    {
        std::ostringstream dir_stream;
        MkDir              d( 0755 );

        dir_stream << CWD;

        d( dir_stream.str( ) );
    }
    catch ( const std::exception& except )
    {
        status = false;
        Test.Message( 30 ) << "Threw exception: " << except.what( )
                           << std::endl;
    }
    Test.Check( status ) << "Create existing directory" << std::endl;
}

void
test_immediate_dir( )
{
    bool status = true;
    try
    {
        std::ostringstream dir_stream;
        MkDir              d( 0755 );

        dir_stream << CWD << MkDir::PATH_SEP << "new_dir";

        d( dir_stream.str( ) );
    }
    catch ( const std::exception& except )
    {
        status = false;
    }
    Test.Check( status ) << "Create immediate sub dir" << std::endl;
}

void
test_recursive_dir( )
{
    bool status = true;
    try
    {
        std::ostringstream dir_stream;
        MkDir              d( 0755, MkDir::OPT_MAKE_PARENT_DIRECTORIES );

        dir_stream << CWD << MkDir::PATH_SEP << "rdir1" << MkDir::PATH_SEP
                   << "rdir2" << MkDir::PATH_SEP << "rdir3";

        d( dir_stream.str( ) );
    }
    catch ( const std::exception& except )
    {
        status = false;
        Test.Message( 30 ) << "Threw exception: " << except.what( )
                           << std::endl;
    }
    Test.Check( status ) << "Create recursive sub dir" << std::endl;
}

int
main( int ArgC, char** ArgV )
{
    //---------------------------------------------------------------------
    // Initialize the test structure
    //---------------------------------------------------------------------
    Test.Init( ArgC, ArgV );

    if ( getcwd( CWD, sizeof( CWD ) ) != CWD )
    {
        Test.Check( false )
            << "Unable to get the current working directory" << std::endl;
        goto LEAVE;
    }

    //---------------------------------------------------------------------
    // Run through a series of tests
    //---------------------------------------------------------------------

    test_immediate_dir( );
    test_recursive_dir( );
    test_existing_dir( );

    //---------------------------------------------------------------------
    // Exit with the appropriate exit status
    //---------------------------------------------------------------------
LEAVE:
    Test.Exit( );
    return 1;
}
