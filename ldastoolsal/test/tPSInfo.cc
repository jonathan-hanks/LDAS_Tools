//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <iostream>

#include "ldastoolsal/unittest.h"
#include "ldastoolsal/PSInfo.hh"

LDASTools::Testing::UnitTest Test;
using LDASTools::AL::PSInfo;

void
validate( )
{
    try
    {
        static const int sleep_time = 5;

        PSInfo              self;
        PSInfo::psinfo_type query;

        sleep( sleep_time );

        self( query );

        Test.Check( query.s_pid == getpid( ) )
            << "pid (" << query.s_pid << ")" << std::endl;
        Test.Check( query.s_state == 'R' )
            << "state(" << query.s_state << ")" << std::endl;
        Test.Check( query.s_user.length( ) > 0 )
            << "user(" << query.s_user << ")" << std::endl;
        Test.Check( query.s_vsz > 0 )
            << "vsz(" << query.s_vsz << ")" << std::endl;
        Test.Check( ( query.s_rsz > 0 ) && ( query.s_rsz < query.s_vsz ) )
            << "rsz(" << query.s_rsz << ")" << std::endl;
        Test.Check( ( query.s_pcpu >= 0.0 ) && ( query.s_pcpu <= 100.0 ) )
            << "pcpu(" << query.s_pcpu << ")" << std::endl;
        Test.Check( ( query.s_pmem >= 0.0 ) && ( query.s_pmem <= 100.0 ) )
            << "pmem(" << query.s_pmem << ")" << std::endl;
        Test.Check( ( query.s_etime >= ( sleep_time - 1 ) ) &&
                    ( query.s_etime <= ( sleep_time + 2 ) ) )
            << "etime(" << query.s_etime << ")" << std::endl;
        Test.Check( query.s_fname.length( ) > 0 )
            << "fname(" << query.s_fname << ")" << std::endl;
        Test.Check( query.s_args.length( ) > 0 )
            << "args(" << query.s_args << ")" << std::endl;
    }
    catch ( const std::exception& Exception )
    {
        Test.Check( false )
            << "Exception was thrown: " << Exception.what( ) << std::endl;
    }
}

int
main( int ArgC, char** ArgV )
{
    Test.Init( ArgC, ArgV );

#if !defined( __aarch64__ ) && !defined( __arm__ )

    validate( );

#endif

    Test.Exit( );
}
