//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include <string.h>
#include <errno.h>

#include <iostream>
#include <sstream>

#include <boost/shared_array.hpp>

#include "ldastoolsal/System.hh"

#if STRERROR_R_CHAR_P
typedef char* strerror_r_retval;
#define STRERROR_R_OK( a ) ( a != (strerror_r_retval)NULL )
#else /* STRERROR_R_CHAR_P */
typedef int strerror_r_retval;
#define STRERROR_R_OK( a ) ( a == 0 )
#endif /* STRERROR_R_CHAR_P */

namespace LDASTools
{
    namespace System
    {
        const std::string
        ErrnoMessage( )
        {
            static const int BUFFER_SIZE = 1024;

            boost::shared_array< char > buffer( new char[ BUFFER_SIZE ] );
            strerror_r_retval           error_string(
                strerror_r( errno, buffer.get( ), BUFFER_SIZE - 1 ) );

            if ( !STRERROR_R_OK( error_string ) )
            {
                std::cerr << "WARN: LDASTools::System::ErrnoMessage( ):"
                          << " strerror_r returned: ";
                switch ( errno )
                {
                case EINVAL:
                    std::cerr << "EINVAL";
                    break;
                case ERANGE:
                    std::cerr << "ERANGE";
                    break;
                default:
                    std::cerr << errno;
                    break;
                }
                std::cerr << std::endl;
                buffer[ 0 ] = '\0';
            }

            std::ostringstream msg;

            msg << " errno=" << errno << " ("
#if STRERROR_R_CHAR_P
                << error_string
#else /* STRERROR_R_CHAR_P */
                << buffer.get( )
#endif /* STRERROR_R_CHAR_P */
                << ")";

            return msg.str( );
        }
    } // namespace System
} // namespace LDASTools
