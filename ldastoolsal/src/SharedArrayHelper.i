
#if !defined(GENERAL__SHARED_ARRAY_HELPER_I)
#define GENERAL__SHARED_ARRAY_HELPER_I

// This is a helper file for LDAS_SHARED_ARRAY_CLASS and should not be included directly.

// The main implementation detail in using this smart pointer of a type is to customise the code generated
// to use a pointer to the smart pointer of the type, rather than the usual pointer to the underlying type.
// So for some type T, LDAS_SHARED_ARRAY_CLASS<T> * is used rather than T *.

// LDAS_SHARED_ARRAY_CLASS namespaces could be boost or std or std::tr1
// For example for std::tr1, use:
// #define SWIG_LDAS_SHARED_ARRAY_CLASS_NAMESPACE std
// #define SWIG_SHARED_ARRAY_SUBNAMESPACE tr1


#if !defined(LDAS_SHARED_ARRAY_CLASS)
#define LDAS_SHARED_ARRAY_CLASS SharedArray
#endif /* !defined(LDAS_SHARED_ARRAY_CLASS) */

#if !defined(LDAS_SHARED_ARRAY_NAMESPACE)
# define LDAS_SHARED_ARRAY_NAMESPACE General
#endif

#if defined(LDAS_SHARED_ARRAY_SUBNAMESPACE)
# define LDAS_SHARED_ARRAY_QNAMESPACE LDAS_SHARED_ARRAY_NAMESPACE::LDAS_SHARED_ARRAY_SUBNAMESPACE
#else
# define LDAS_SHARED_ARRAY_QNAMESPACE LDAS_SHARED_ARRAY_NAMESPACE
#endif

#define LDAS_SHARED_ARRAY_QCLASS LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS

namespace LDAS_SHARED_ARRAY_NAMESPACE {
#if defined(LDAS_SHARED_ARRAY_SUBNAMESPACE)
  namespace LDAS_SHARED_ARRAY_SUBNAMESPACE {
#endif
    template <class T> class LDAS_SHARED_ARRAY_CLASS {
    };
#if defined(LDAS_SHARED_ARRAY_SUBNAMESPACE)
  }
#endif
}

%fragment("SWIG_null_deleter", "header") {
struct SWIG_null_deleter {
  void operator() (void const *) const {
  }
};
%#define SWIG_NO_NULL_DELETER_0 , SWIG_null_deleter()
%#define SWIG_NO_NULL_DELETER_1
%#define SWIG_NO_NULL_DELETER_SWIG_POINTER_NEW
%#define SWIG_NO_NULL_DELETER_SWIG_POINTER_OWN
}


// Workaround empty first macro argument bug
#define SWIGEMPTYHACK
// Main user macro for defining LDAS_SHARED_ARRAY_CLASS typemaps for both const and non-const pointer types
%define %SharedArray(TYPE...)
%feature("smartarray", noblock=1) TYPE { LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > }
LDAS_SHARED_ARRAY_TYPEMAPS(SWIGEMPTYHACK, TYPE)
LDAS_SHARED_ARRAY_TYPEMAPS(const, TYPE)
%enddef

%define %SharedArrayTypedef(TYPE,LTYPE)
  typedef LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS<TYPE> LTYPE;
%enddef

#endif /* !defined( GENERAL__SHARED_ARRAY_HELPER_I ) */
