//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include "ldastoolsal/AtExit.hh"
#include "ldastoolsal/IOLock.hh"

namespace LDASTools
{
    namespace AL
    {
        IOLock::baton_type IOLock::m_key_ostream;

        IOLock::keys_ostream_type IOLock::m_keys_ostream;

        MutexLock::baton_type
        IOLock::GetKey( const std::ostream& Stream )
        {
            MutexLock lock( m_key_ostream, __FILE__, __LINE__ );

            baton_type                  key;
            keys_ostream_type::iterator cur;
            keys_ostream_type::iterator last;

            for ( cur = m_keys_ostream.begin( ), last = m_keys_ostream.end( );
                  cur != last;
                  ++cur )
            {
                if ( ( *cur )->key == ( &Stream ) )
                {
                    break;
                }
            }
            if ( cur == last )
            {
                m_keys_ostream.push_back( new lock_set );
                m_keys_ostream.back( )->key = &Stream;
                key = m_keys_ostream.back( )->lock;
            }
            else
            {
                key = ( *cur )->lock;
            }
            return key;
        }
    } // namespace AL
} // namespace LDASTools
