
%include <ldastoolsal/SharedArrayHelper.i>

// Set LDAS_SHARED_ARRAY_DISOWN to $disown if required, for example
// #define LDAS_SHARED_ARRAY_DISOWN $disown
#if !defined(LDAS_SHARED_ARRAY_DISOWN)
#define LDAS_SHARED_ARRAY_DISOWN 0
#endif

// Language specific macro implementing all the customisations for handling the smart pointer
%define LDAS_SHARED_ARRAY_TYPEMAPS(CONST, TYPE...)

// %naturalvar is as documented for member variables
%naturalvar TYPE;
%naturalvar LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >;

// destructor wrapper customisation
%feature("unref") TYPE 
//"if (debug_shared) { cout << \"deleting use_count: \" << (*smartarg1).use_count() << \" [\" << (boost::get_deleter<SWIG_null_deleter>(*smartarg1) ? std::string(\"CANNOT BE DETERMINED SAFELY\") : ( (*smartarg1).get() ? (*smartarg1)->getValue() : std::string(\"NULL ARRAY\") )) << \"]\" << endl << flush; }\n"
                               "(void)arg1; delete smartarg1;"

// Typemap customisations...

// plain value
%typemap(in) CONST TYPE (void *argp, int res = 0) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertarray_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (!argp) {
    %argument_nullref("$type", $symname, $argnum);
  } else {
    $1 = *(%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *)->get());
    if (newmem & SWIG_CAST_NEW_MEMORY) delete %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
  }
}
%typemap(out) CONST TYPE {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >(new $1_ltype(($1_ltype &)$1));
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

%typemap(varin) CONST TYPE {
  void *argp = 0;
  int newmem = 0;
  int res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %variable_fail(res, "$type", "$name");
  }
  if (!argp) {
    %argument_nullref("$type", $symname, $argnum);
  } else {
    $1 = *(%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *)->get());
    if (newmem & SWIG_CAST_NEW_MEMORY) delete %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
  }
}
%typemap(varout) CONST TYPE {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >(new $1_ltype(($1_ltype &)$1));
  %set_varoutput(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

// plain pointer
// Note: $disown not implemented by default as it will lead to a memory leak of the LDAS_SHARED_ARRAY_CLASS instance
%typemap(in) CONST TYPE * (void  *argp = 0, int res = 0, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > tempshared, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartarg = 0) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), LDAS_SHARED_ARRAY_DISOWN | %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (newmem & SWIG_CAST_NEW_MEMORY) {
    tempshared = *%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    delete %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    $1 = %const_cast(tempshared.get(), $1_ltype);
  } else {
    smartarg = %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    $1 = %const_cast((smartarg ? smartarg->get() : 0), $1_ltype);
  }
}
%typemap(out, fragment="SWIG_null_deleter") CONST TYPE * {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = $1 ? new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >($1 SWIG_NO_NULL_DELETER_$owner) : 0;
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), $owner | SWIG_POINTER_OWN));
}

%typemap(varin) CONST TYPE * {
  void *argp = 0;
  int newmem = 0;
  int res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %variable_fail(res, "$type", "$name");
  }
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > tempshared;
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartarg = 0;
  if (newmem & SWIG_CAST_NEW_MEMORY) {
    tempshared = *%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    delete %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    $1 = %const_cast(tempshared.get(), $1_ltype);
  } else {
    smartarg = %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    $1 = %const_cast((smartarg ? smartarg->get() : 0), $1_ltype);
  }
}
%typemap(varout, fragment="SWIG_null_deleter") CONST TYPE * {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = $1 ? new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >($1 SWIG_NO_NULL_DELETER_0) : 0;
  %set_varoutput(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

// plain reference
%typemap(in) CONST TYPE & (void  *argp = 0, int res = 0, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > tempshared) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (!argp) { %argument_nullref("$type", $symname, $argnum); }
  if (newmem & SWIG_CAST_NEW_MEMORY) {
    tempshared = *%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    delete %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    $1 = %const_cast(tempshared.get(), $1_ltype);
  } else {
    $1 = %const_cast(%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *)->get(), $1_ltype);
  }
}
%typemap(out, fragment="SWIG_null_deleter") CONST TYPE & {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >($1 SWIG_NO_NULL_DELETER_$owner);
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

%typemap(varin) CONST TYPE & {
  void *argp = 0;
  int newmem = 0;
  int res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %variable_fail(res, "$type", "$name");
  }
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > tempshared;
  if (!argp) { %argument_nullref("$type", $symname, $argnum); }
  if (newmem & SWIG_CAST_NEW_MEMORY) {
    tempshared = *%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    delete %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    $1 = *%const_cast(tempshared.get(), $1_ltype);
  } else {
    $1 = *%const_cast(%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *)->get(), $1_ltype);
  }
}
%typemap(varout, fragment="SWIG_null_deleter") CONST TYPE & {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >(&$1 SWIG_NO_NULL_DELETER_0);
  %set_varoutput(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

// plain pointer by reference
// Note: $disown not implemented by default as it will lead to a memory leak of the LDAS_SHARED_ARRAY_CLASS instance
%typemap(in) TYPE *CONST& (void  *argp = 0, int res = 0, $*1_ltype temp = 0, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > tempshared) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), LDAS_SHARED_ARRAY_DISOWN | %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (newmem & SWIG_CAST_NEW_MEMORY) {
    tempshared = *%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    delete %reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *);
    temp = %const_cast(tempshared.get(), $*1_ltype);
  } else {
    temp = %const_cast(%reinterpret_cast(argp, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *)->get(), $*1_ltype);
  }
  $1 = &temp;
}
%typemap(out, fragment="SWIG_null_deleter") TYPE *CONST& {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >(*$1 SWIG_NO_NULL_DELETER_$owner);
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

%typemap(varin) TYPE *CONST& %{
#error "varin typemap not implemented"
%}
%typemap(varout) TYPE *CONST& %{
#error "varout typemap not implemented"
%}

// LDAS_SHARED_ARRAY_CLASS by value
%typemap(in) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > (void *argp, int res = 0) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (argp) $1 = *(%reinterpret_cast(argp, $&ltype));
  if (newmem & SWIG_CAST_NEW_MEMORY) delete %reinterpret_cast(argp, $&ltype);
}
%typemap(out) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = $1 ? new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >($1) : 0;
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

%typemap(varin) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > {
  int newmem = 0;
  void *argp = 0;
  int res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %variable_fail(res, "$type", "$name");
  }
  $1 = argp ? *(%reinterpret_cast(argp, $&ltype)) : LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE >();
  if (newmem & SWIG_CAST_NEW_MEMORY) delete %reinterpret_cast(argp, $&ltype);
}
%typemap(varout) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = $1 ? new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >($1) : 0;
  %set_varoutput(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

// LDAS_SHARED_ARRAY_CLASS by reference
%typemap(in) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > & (void *argp, int res = 0, $*1_ltype tempshared) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (newmem & SWIG_CAST_NEW_MEMORY) {
    if (argp) tempshared = *%reinterpret_cast(argp, $ltype);
    delete %reinterpret_cast(argp, $ltype);
    $1 = &tempshared;
  } else {
    $1 = (argp) ? %reinterpret_cast(argp, $ltype) : &tempshared;
  }
}
%typemap(out) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > & {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = *$1 ? new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >(*$1) : 0;
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

%typemap(varin) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > & %{
#error "varin typemap not implemented"
%}
%typemap(varout) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > & %{
#error "varout typemap not implemented"
%}

// LDAS_SHARED_ARRAY_CLASS by pointer
%typemap(in) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > * (void *argp, int res = 0, $*1_ltype tempshared) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (newmem & SWIG_CAST_NEW_MEMORY) {
    if (argp) tempshared = *%reinterpret_cast(argp, $ltype);
    delete %reinterpret_cast(argp, $ltype);
    $1 = &tempshared;
  } else {
    $1 = (argp) ? %reinterpret_cast(argp, $ltype) : &tempshared;
  }
}
%typemap(out) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > * {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = $1 && *$1 ? new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >(*$1) : 0;
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
  if ($owner) delete $1;
}

%typemap(varin) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > * %{
#error "varin typemap not implemented"
%}
%typemap(varout) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > * %{
#error "varout typemap not implemented"
%}

// LDAS_SHARED_ARRAY_CLASS by pointer reference
%typemap(in) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *& (void *argp, int res = 0, LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > tempshared, $*1_ltype temp = 0) {
  int newmem = 0;
  res = SWIG_ConvertPtrAndOwn($input, &argp, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), %convertptr_flags, &newmem);
  if (!SWIG_IsOK(res)) {
    %argument_fail(res, "$type", $symname, $argnum); 
  }
  if (argp) tempshared = *%reinterpret_cast(argp, $*ltype);
  if (newmem & SWIG_CAST_NEW_MEMORY) delete %reinterpret_cast(argp, $*ltype);
  temp = &tempshared;
  $1 = &temp;
}
%typemap(out) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *& {
  LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *smartresult = *$1 && **$1 ? new LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >(**$1) : 0;
  %set_output(SWIG_NewPointerObj(%as_voidptr(smartresult), $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), SWIG_POINTER_OWN));
}

%typemap(varin) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *& %{
#error "varin typemap not implemented"
%}
%typemap(varout) LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *& %{
#error "varout typemap not implemented"
%}

// Typecheck typemaps
// Note: SWIG_ConvertPtr with void ** parameter set to 0 instead of using SWIG_ConvertPtrAndOwn, so that the casting 
// function is not called thereby avoiding a possible smart pointer copy constructor call when casting up the inheritance chain.
%typemap(typecheck,precedence=SWIG_TYPECHECK_POINTER,noblock=1) 
                      TYPE CONST,
                      TYPE CONST &,
                      TYPE CONST *,
                      TYPE *CONST&,
                      LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >,
                      LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > &,
                      LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *,
                      LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE > *& {
  int res = SWIG_ConvertPtr($input, 0, $descriptor(LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< TYPE > *), 0);
  $1 = SWIG_CheckState(res);
}


// various missing typemaps - If ever used (unlikely) ensure compilation error rather than runtime bug
%typemap(in) CONST TYPE[], CONST TYPE[ANY], CONST TYPE (CLASS::*) %{
#error "typemaps for $1_type not available"
%}
%typemap(out) CONST TYPE[], CONST TYPE[ANY], CONST TYPE (CLASS::*) %{
#error "typemaps for $1_type not available"
%}


%template() LDAS_SHARED_ARRAY_QNAMESPACE::LDAS_SHARED_ARRAY_CLASS< CONST TYPE >;

%enddef

