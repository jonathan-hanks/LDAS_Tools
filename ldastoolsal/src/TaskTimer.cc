//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include <time.h>

#include <iostream>
#include <sstream>

#include "ldastoolsal/TaskTimer.hh"

namespace
{
    std::string
    mk_name( int Seconds )
    {
        std::ostringstream retval;

        retval << "Timer: Seconds: " << Seconds;
        return retval.str( );
    }
} // namespace

namespace LDASTools
{
    namespace AL
    {
        //-------------------------------------------------------------------
        /// Initialize the instance of the class.
        //-------------------------------------------------------------------
        Timer::Timer( int Seconds, signal_type CancelSignal )
            : Task(
                  mk_name( Seconds ), Thread::CANCEL_BY_SIGNAL, CancelSignal ),
              m_timeout( Seconds ), m_err( false )
        {
        }

        //-------------------------------------------------------------------
        /// Return true if the timer expired before being cancelled.
        //-------------------------------------------------------------------
        bool
        Timer::Error( ) const
        {
            MutexLock l( Baton( ), __FILE__, __LINE__ );

            return m_err;
        }

        //-------------------------------------------------------------------
        /// For the timer task, the action is to start an interuptable
        /// sleep timer for the requested maximum time period.
        /// m_err is set to true if the sleep timer completes without
        /// any error.
        //-------------------------------------------------------------------
        void
        Timer::operator( )( )
        {
            struct timespec wakeup;

            {
                MutexLock l( Baton( ), __FILE__, __LINE__ );

                wakeup.tv_sec = m_timeout;
                wakeup.tv_nsec = 0;
            }

            int err = ( nanosleep( &wakeup, NULL ) != 0 );
            {
                MutexLock l( Baton( ), __FILE__, __LINE__ );

                m_err = err;
            }
        }
    } // namespace AL
} // namespace LDASTools
