//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

/* $Id: toss.cc,v 1.9 2008/01/12 01:22:25 emaros Exp $ */

#include <ldastoolsal_config.h>

#include <sstream>
#include <stdexcept>

#include "toss.hh"
#include "unimplemented_error.hh"

namespace LDASTools
{
    namespace Error
    {

        //-------------------------------------------------------------------
        /// This function simplifies the addition of key debugging information
        /// when throwing an exception.
        /// The availability of the information to the developer greatly
        /// increases the developer's ability to trouble shoot problems.
        //-------------------------------------------------------------------
        template < class T >
        void
        toss( const std::string& ClassName,
              const std::string& Filename,
              int                LineNumber,
              const std::string& Description )
        {
            std::ostringstream oss;
            oss << ClassName << "(" << Filename << ":" << LineNumber
                << "): " << Description;
            std::string what = oss.str( );
            throw T( what );
        }

        /// \cond IGNORE

#define INSTANTIATE( EXCEPT_TYPE )                                             \
    template void toss< EXCEPT_TYPE >( const std::string& cl,                  \
                                       const std::string& fl,                  \
                                       int                ln,                  \
                                       const std::string& de )

        INSTANTIATE( std::logic_error );
        INSTANTIATE( std::domain_error );
        INSTANTIATE( std::invalid_argument );
        INSTANTIATE( std::length_error );
        INSTANTIATE( std::out_of_range );
        INSTANTIATE( std::runtime_error );
        INSTANTIATE( std::range_error );
        INSTANTIATE( std::overflow_error );
        INSTANTIATE( std::underflow_error );
        INSTANTIATE( unimplemented_error );

#undef INSTANTIATE

        /// \endcond

    } // namespace Error
} // namespace LDASTools
