//
// LDASTools AL - A library collection to provide an abstraction layer
//
// Copyright (C) 2018 California Institute of Technology
//
// LDASTools AL is free software; you may redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 (GPLv2) of the
// License or at your discretion, any later version.
//
// LDASTools AL is distributed in the hope that it will be useful, but
// without any warranty or even the implied warranty of merchantability
// or fitness for a particular purpose. See the GNU General Public
// License (GPLv2) for more details.
//
// Neither the names of the California Institute of Technology (Caltech),
// The Massachusetts Institute of Technology (M.I.T), The Laser
// Interferometer Gravitational-Wave Observatory (LIGO), nor the names
// of its contributors may be used to endorse or promote products derived
// from this software without specific prior written permission.
//
// You should have received a copy of the licensing terms for this
// software included in the file LICENSE located in the top-level
// directory of this package. If you did not, you can view a copy at
// http://dcc.ligo.org/M1500244/LICENSE
//

#include <ldastoolsal_config.h>

#include "errorinfo.hh"

#include <cstring>

using std::strcmp;
using std::strcpy;
using std::strlen;

//-----------------------------------------------------------------------------
/// \brief  Constructor.
///
/// \param library
/// \param code
/// \param msg
/// \param info
/// \param file
/// \param line
///
ErrorInfo::ErrorInfo( int                library,
                      int                code,
                      const std::string& msg,
                      const std::string& info,
                      const char*        file,
                      size_t             line )
    : mLibrary( library ), mCode( code ), mMsg( msg ), mInfo( info ),
      mFile( 0 ), mLine( line )
{
    if ( file )
    {
        const size_t file_len( strlen( file ) + 1 );
        mFile = new char[ file_len ];

        strcpy( mFile, file );
    }
}

//-----------------------------------------------------------------------------
/// \brief  Copy Constructor
///
/// \param e
///
ErrorInfo::ErrorInfo( const ErrorInfo& e )
    : mLibrary( e.mLibrary ), mCode( e.mCode ), mMsg( e.mMsg ),
      mInfo( e.mInfo ), mFile( 0 ), mLine( e.mLine )
{
    if ( e.mFile )
    {
        const size_t file_len( strlen( e.mFile ) + 1 );
        mFile = new char[ file_len ];

        strcpy( mFile, e.mFile );
    }
}

//-----------------------------------------------------------------------------
/// \brief  Destructor.
///
ErrorInfo::~ErrorInfo( )
{
    delete[] mFile;
    mFile = 0;
}

//-----------------------------------------------------------------------------
/// \brief  Equal Comparison
///
/// \param e
///
/// \return bool
///
bool
ErrorInfo::operator==( const ErrorInfo& e ) const
{
    return ( mLibrary == e.mLibrary && mCode == e.mCode && mLine == e.mLine &&
             mMsg == e.mMsg && mInfo == e.mInfo &&
             strcmp( mFile, e.mFile ) == 0 );
}

//-----------------------------------------------------------------------------
/// \brief  Assignment Operator
///
/// \param e
///
/// \return const ErrorInfo&
///
const ErrorInfo&
ErrorInfo::operator=( const ErrorInfo& e )
{
    if ( this != &e )
    {
        mLibrary = e.mLibrary;
        mCode = e.mCode;
        mMsg = e.mMsg;
        mInfo = e.mInfo;

        delete[] mFile;
        mFile = 0;

        if ( e.mFile )
        {
            const size_t file_len( strlen( e.mFile ) + 1 );
            mFile = new char[ file_len ];

            strcpy( mFile, e.mFile );
        }

        mLine = e.mLine;
    }

    return *this;
}
