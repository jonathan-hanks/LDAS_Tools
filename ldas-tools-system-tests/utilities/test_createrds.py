#! /usr/bin/env python

import io
import getopt
import os
import os.path
import pexpect
import signal
import shutil
import subprocess
import sys
import time
import random
import re

root=os.path.dirname( os.path.dirname( os.path.abspath( sys.argv[0] ) ) )

sys.path.insert(1, ".." )
sys.path.insert(1, root )

import testing

import diskcache.dc as dc

Test = testing.Test

class data_sets( object ):
    def __init__( self,
                  run = 'O1'):

        self.run = run
        if ( self.run == 'O1' ):
            self.run_range = range( 11314, 11316 + 1 )
            self.run_range.extend( [ 11371, ] )

        self.__init_hoft( )

    def __init_hoft( self ):
        #----------------------------------------------------------------
        # O1:
        #     Each frame file has 1024 frames
        #     Each frame is 4 seconds in length
        #     Valid channel name is: H1:ODC-MASTER_CHANNEL_OUT_DQ
        #----------------------------------------------------------------
        mps = []
        for mp in self.run_range:
            mps.append( '%s/%s/hoft/H1/H-H1_HOFT_C00-%d' % ( testing.dir_archive, self.run, mp ) )
            mps.append( '%s/%s/hoft_C01/H1/H-H1_HOFT_C01-%d' % ( testing.dir_archive, self.run, mp ) )
        self.hoft = { \
            'mount_points' : mps,
            'frame_type' : 'H1_HOFT_C00',
            'ifo' : 'H',
            'channel' : 'H1:GDS-CALIB_STRAIN',
            'frames_per_file' : 1024,
            'seconds_per_frame' : 4
            }
        self.hoft_4k = { \
            'mount_points' : mps,
            'frame_type' : 'H1_HOFT_C01',
            'ifo' : 'H',
            'channel_list' : { 'H1:DCS-CALIB_STRAIN_C01!4',
                               'H1:DCS-CALIB_STATE_VECTOR_C01',
                               'H1:DCS-CALIB_KAPPA_A_REAL_C01',
                               'H1:DCS-CALIB_KAPPA_A_IMAGINARY_C01',
                               'H1:DCS-CALIB_KAPPA_TST_REAL_C01',
                               'H1:DCS-CALIB_KAPPA_TST_IMAGINARY_C01',
                               'H1:DCS-CALIB_KAPPA_PU_REAL_C01',
                               'H1:DCS-CALIB_KAPPA_PU_IMAGINARY_C01',
                               'H1:DCS-CALIB_KAPPA_C_C01',
                               'H1:DCS-CALIB_F_CC_C01'
                               },
            'seconds_per_frame' : 4096,
            'frames_per_file' : 1,
            'time_start' : 1137164288,
            'time_stop' : 1137168384
            }
        pass

DataSets = data_sets( )
        
class rds( object ):

    ( DESCRIPTION,
      FRAMES_PER_FILE,
      SECONDS_PER_FRAME
    ) = range( 1, 3 + 1)

    """
    Create a new instance of the object

    Keyword arguments:
    start -- Start time of the output data
    stop -- Stop time of the output data
    """
    def __init__( self, start, stop, diskcache_rsc,
                  directory_log = None,
                  directory_output_frames = None,
                  directory_output_md5sums = None,
                  frame_query = None ):
        global Test

        self.start = start
        self.stop = stop
        self.padding = None
        self.directory_log = directory_log
        self.directory_output_frames = directory_output_frames
        self.directory_output_md5sums = directory_output_md5sums
        self.frame_query = frame_query
        self.log_debug_level = 50

        self.cmd = testing.prog_create_rds
        self.diskcache_server = \
            dc.diskcache.server( diskcache_rsc, Test = Test )

    def eval( self ):
        global Test
        self.cmd += " " + " ".join( [ "--diskcache-host", "localhost",
                           "--diskcache-port", self.diskcache_server.resource.arg( 'server_port' )
                           ] )
        if ( self.directory_log ):
            self.cmd += \
                " --log-directory " + self.directory_log + \
                " --log-debug-level %d" % self.log_debug_level
            
        if ( self.directory_output_frames ):
            self.cmd += " " + " ".join( [ "--directory-output-frames",
                                          self.directory_output_frames
                                          ] )
        if ( self.directory_output_md5sums ):
            self.cmd += " " + " ".join( [ "--directory-output-md5sum",
                                          self.directory_output_md5sums
                                          ] )
        if ( self.frame_query ):
            self.cmd += " --frame-query '" + self.frame_query + "'"
        if ( not self.padding == None ):
            self.cmd += " --padding %d" % ( self.padding )

        Test.Message( "Command is: %s" \
                          % ( self.cmd ),
                      Level = 0 )
        subprocess.check_output( self.cmd,
                                 stderr=subprocess.STDOUT,
                                 shell = True )

    def append( self, option, value = None ):
        if ( option == rds.DESCRIPTION ):
            if ( not value == None ):
                self.cmd += " --description " + str( value )
        elif ( option == rds.FRAMES_PER_FILE ):
            if ( not value == None ):
                self.cmd += " --frames-per-file " + str( value )
        elif ( option == rds.SECONDS_PER_FRAME ):
            if ( not value == None ):
                self.cmd += " --seconds-per-frame " + str( value )
        else:
            pass

class ticket( object ):
    def __init__( self, name, classification = None, subcommand = None,
                  start = 1000, stop = 3000,
                  mount_points = DataSets.hoft[ 'mount_points' ],
                  frame_query = None,
                  variant = None ):
        """
        Create a new instance of the object

        Keyword arguments:
        Test -- Name of the test to executed (eg: 'space')
        """
        global Test

        self.diskcache_server = None

        self.name = name
        self.desc = self.name

        self.dir_top = os.path.join( os.getcwd( ), self.name )
        shutil.rmtree( self.dir_top, True )
        os.makedirs( self.dir_top, 0755 )
        self.dir_log = os.path.join( self.dir_top, "logs" )
        os.makedirs( self.dir_log, 0755 )
        self.dir_output_frames = os.path.join( self.dir_top, "frames" )
        os.makedirs( self.dir_output_frames, 0755 )
        self.dir_output_md5sums = os.path.join( self.dir_top, "md5sums" )
        os.makedirs( self.dir_output_md5sums, 0755 )

        self.classification = classification
        self.frame_query = frame_query
        self.variant = variant
        self.diskcache_rsc = \
            dc.diskcache_rsc( self.dir_log,
                              # Default to Hanford HofT frames
                              mount_points = mount_points,
                              output_ascii = "%s/cache.txt" % self.dir_top,
                              output_binary = "%s/cache.dat" % self.dir_top
                              )
        self.filename_diskcache_rsc = os.path.join( self.dir_top, 'diskcache.rsc' )

        self.start = start
        self.stop = stop
        self.padding = None
        self.subcommand = subcommand

        Test.Message( 'Creating new instance of %s' % self.name,
                      Level=10 )
        return
        
    def __del__(self):
        if ( self.diskcache_server ):
            self.diskcache_server.stop( )
        pass

    def eval( self ):
        global Test

        Test.Message( 'ENTRY: ticket::eval',
                      Level=5 )

        #----------------------------------------------------------------
        # Commit the diskcache server configuation to disk
        #----------------------------------------------------------------
        self.diskcache_rsc.write( self.filename_diskcache_rsc )
        #----------------------------------------------------------------
        # Start the server to have access to frame files
        #----------------------------------------------------------------
        if ( not self.diskcache_server ):
            self.diskcache_server = \
                dc.diskcache.server( self.diskcache_rsc, Test = Test )
        if ( not self.diskcache_server.isAlive( ) ):
            self.diskcache_server.start( )
            self.diskcache_server.waitOnFirstScan( )
        self.rds = rds( self.start, self.stop, self.diskcache_rsc,
                        directory_log = self.dir_log,
                        directory_output_frames = self.dir_output_frames,
                        directory_output_md5sums = self.dir_output_md5sums,
                        frame_query = self.frame_query )
        self.rds.padding = self.padding
        self._append_rds_options( )
        self.rds.eval( )
        Test.Message( 'EXIT: ticket::eval',
                      Level=5 )

    def fmt_frame_query( self, start, end,
                         channel = 'MISSING_CHANNEL',
                         ifo = 'MISSING_IFO',
                         frame_type = 'MISSING_FRAME_TYPE' ):
        #----------------------------------------------------------------
        # Format channels
        #----------------------------------------------------------------
        if ( isinstance( channel, basestring ) ):
            channels = channel
        else:
            channels = ",".join( channel )
        #----------------------------------------------------------------
        # Construct the query
        #----------------------------------------------------------------
        query = '{ %s %s {} %d-%d Chan(%s)}' % \
            ( frame_type, ifo, start, end, channels )
        return "{ %s }" % ( query )

    def _append_rds_options( self ):
        self.rds.append( rds.DESCRIPTION, self.desc )


    def _verify( self, cmd ):
        #----------------------------------------------------------------
        # Verify generated contents with that of a previously generated
        #   file.
        #----------------------------------------------------------------
        try:
            subprocess.check_call( cmd, shell=True )
        except subprocess.CalledProcessError as err:
            exit( 1 )

class ticket_3130( ticket ):
    def __init__( self, variant ):
        global DataSets
        
        #----------------------------------------------------------------
        # Initialize base
        #----------------------------------------------------------------
        super( ticket_3130, self ).__init__( "ticket_3130_" + variant,
                                             variant = variant )
        #----------------------------------------------------------------
        # Set default values
        #----------------------------------------------------------------
        self.log_debug_level = 50
        self.frames_per_file = None
        self.ds = DataSets.hoft
        self.frames_per_file = self.ds[ 'frames_per_file' ]
        self.start = 1131520000
        self.end = self.start + ( self.frames_per_file * self.ds[ 'seconds_per_frame' ] )
        self.padding = 16
        channel_name = self.ds[ 'channel' ]
        ifo = self.ds[ 'ifo' ]
        frame_type = self.ds[ 'frame_type' ]
        #----------------------------------------------------------------
        # overrides based on variant
        #----------------------------------------------------------------
        if ( self.variant == 'resample' \
                 or self.variant == 'resample_short_reduce' ):
            channel_name += '!4'
        if ( self.variant == 'short_reduce' \
                 or self.variant == 'resample_short_reduce' ):
            self.start = self.start + self.padding
            self.end = self.start + ( 2  * self.ds[ 'seconds_per_frame' ] )
            self.frames_per_file = ( self.end - self.start ) / self.ds[ 'seconds_per_frame' ]
        #----------------------------------------------------------------
        self.frame_query = self.fmt_frame_query( start = self.start,
                                                 end = self.end,
                                                 channel = channel_name,
                                                 ifo = ifo,
                                                 frame_type = frame_type )

    def eval( self ):
        dt = self.end - self.start
        #----------------------------------------------------------------
        # First pass
        #----------------------------------------------------------------
        self.eval_pass = 1
        s1_file = ( "%s%s-%d-%d.gwf" % ( "H-", self.desc, self.start, dt ) )
        super( ticket_3130, self ).eval( )
        #----------------------------------------------------------------
        # Second pass
        #----------------------------------------------------------------
        self.padding = None
        self.eval_pass = 2
        self.desc += "_v2"
        s2_file = ( "%s%s-%d-%d.gwf" % ( "H-", self.desc, self.start, dt ) )
        super( ticket_3130, self ).eval( )
        #----------------------------------------------------------------
        # Compare the two output frames
        #----------------------------------------------------------------
        verify_cmd = ( "%s %s/%s %s/%s" % ( testing.prog_framecpp_compare,
                                            self.dir_output_frames,
                                            s1_file,
                                            self.dir_output_frames,
                                            s2_file ) )
        print verify_cmd
        self._verify( verify_cmd )
        
    """
    This method allows for each ticket to customize the createRDS
    command just prior to its execution.
    """
    def _append_rds_options( self ):
        super( ticket_3130, self )._append_rds_options( )
        self.rds.append( rds.SECONDS_PER_FRAME, self.ds[ 'seconds_per_frame' ] )
        if ( not self.frames_per_file == None ):
            self.rds.append( rds.FRAMES_PER_FILE, self.frames_per_file )

class ticket_3131( ticket ):
    def __init__( self, variant ):
        global DataSets
        
        super( ticket_3131, self ).__init__( "ticket_3131_" + variant,
                                             variant = variant )

        #----------------------------------------------------------------
        # Set default values
        #----------------------------------------------------------------
        self.frames_per_file = 1
        self.ds = DataSets.hoft
        self.frames_per_file = self.ds[ 'frames_per_file' ]
        self.seconds_per_frame = 4096
        self.start = 1131520000
        self.end = self.start + ( self.frames_per_file * self.ds[ 'seconds_per_frame' ] )
        channel_name = self.ds[ 'channel' ]
        ifo = self.ds[ 'ifo' ]
        frame_type = self.ds[ 'frame_type' ]
        #----------------------------------------------------------------
        # overrides based on variant
        #----------------------------------------------------------------
        if ( self.variant == '64x64' ):
            #------------------------------------------------------------
            #------------------------------------------------------------
            self.frames_per_file = 64
            self.seconds_per_frame = 64
        elif ( self.variant == '64x64_resample' ):
            #------------------------------------------------------------
            #------------------------------------------------------------
            self.seconds_per_frame = 64
            self.frames_per_file = 64
            channel_name += "!4"
        elif ( self.variant == '32x64' ):
            #------------------------------------------------------------
            #------------------------------------------------------------
            self.start += 1024
            self.stop -= 1024
            self.seconds_per_frame = 64
            self.frames_per_file = 32
        elif ( self.variant == '32x64_resample' ):
            #------------------------------------------------------------
            #------------------------------------------------------------
            self.start += 1024
            self.stop -= 1024
            self.seconds_per_frame = 64
            self.frames_per_file = 32
            channel_name += "!4"
        elif ( self.variant == '1024x4' ):
            #------------------------------------------------------------
            #------------------------------------------------------------
            self.seconds_per_frame = 4
            self.frames_per_file = 1024
        elif ( self.variant == '1024x4_resample' ):
            #------------------------------------------------------------
            #------------------------------------------------------------
            self.seconds_per_frame = 4
            self.frames_per_file = 1024
            channel_name += "!4"

        self.frame_query = self.fmt_frame_query( start = self.start,
                                                 end = self.end,
                                                 channel = channel_name,
                                                 ifo = ifo,
                                                 frame_type = frame_type )

    def eval( self ):
        super( ticket_3131, self ).eval( )
        pass

    """
    This method allows for each ticket to customize the createRDS
    command just prior to its execution.
    """
    def _append_rds_options( self ):
        super( ticket_3131, self )._append_rds_options( )
        if ( not self.seconds_per_frame == None ):
            self.rds.append( rds.SECONDS_PER_FRAME,
                             self.seconds_per_frame)
        if ( not self.frames_per_file == None ):
            self.rds.append( rds.FRAMES_PER_FILE, self.frames_per_file )

class ticket_3132( ticket ):
    def __init__( self, variant='default' ):
        global DataSets
        
        super( ticket_3132, self ).__init__( "ticket_3132_" + variant, variant = variant )

        mps = []
        run = "ER8"

        for mp in range( 11260, 11262 ):
            mps.append( '%s/%s/hoft_C01/L1/L-L1_HOFT_C01-%d' % ( testing.dir_archive, run, mp ) )
        DataSets.t3132 = { \
            'mount_points' : mps,
            'frame_type' : 'L1_HOFT_C01',
            'ifo' : 'L',
            'channel' : 'L1:DCS-CALIB_STRAIN_C01',
            'frames_per_file' : 64,
            'seconds_per_frame' : 64
            }

        #----------------------------------------------------------------
        # Set default values
        # /ldas_usr/ldas/SunOS-i86pc/bin/ldas_create_rds
        # --frame-query '{ { L1_HOFT_C01 L {} 1126043648-1126047744 Chan(
        #      /usr1/ldas/createrds/ER8-STANDALONE-L1_HOFT_C01_4kHz/hoftL14kHzChanList.txt
        # ) } }'
        # --diskcache-host localhost --diskcache-port 11300
        # --description L1_HOFT_C01_4kHz
        # --compression-type zero_suppress_otherwise_gzip
        # --compression-level 1
        # --frames-per-file 1
        # --seconds-per-frame 4096
        # --allow-short-frames 1
        # --generate-frame-checksum 1
        # --verify-filename-metadata 0
        # --fill-missing-data-valid-array 0
        # --directory-output-md5sum-regexp s,frames,meta/frames,i
        # --log-directory
        #   /usr1/ldas/createrds/ER8-STANDALONE-L1_HOFT_C01_4kHz/ldas_create_rds_logs
        # --directory-output-frames
        #   /archive/frames/test/hoft_C01_4kHz/L1/L-L1_HOFT_C01_4kHz-11260
        #----------------------------------------------------------------
        self.ds = DataSets.t3132
        self.diskcache_rsc.mount_points = self.ds[ 'mount_points' ]
        self.diskcache_rsc.log_debug_level = 50
        self.frames_per_file = 1
        self.seconds_per_frame = 4096
        self.start = 1126043648
        self.end   = 1126047744
        channel_name = self.ds[ 'channel' ]
        ifo = self.ds[ 'ifo' ]
        frame_type = self.ds[ 'frame_type' ]
        #----------------------------------------------------------------
        # overrides based on variant
        #----------------------------------------------------------------
        if ( re.match( '^.*_resample_.*$', self.variant ) ):
            channel_name += '!4'
        if ( re.match( '^.*_padding_.*$', self.variant ) ):
            self.padding = 16
            
        self.frame_query = self.fmt_frame_query( start = self.start,
                                                 end = self.end,
                                                 channel = channel_name,
                                                 ifo = ifo,
                                                 frame_type = frame_type )

    def eval( self ):
        super( ticket_3132, self ).eval( )
        pass

    """
    This method allows for each ticket to customize the createRDS
    command just prior to its execution.
    """
    def _append_rds_options( self ):
        super( ticket_3132, self )._append_rds_options( )

class ticket_hoft_validation( ticket ):
    def __init__( self ):
        global DataSets
        
        variant='default'

        super( ticket_hoft_validation, self ).__init__( "ticket_hoft_validation_" + variant, variant = variant )
        #----------------------------------------------------------------
        # Set default values
        #----------------------------------------------------------------
        self.ds = DataSets.hoft_4k
        self.frames_per_file = self.ds[ 'frames_per_file' ]
        self.seconds_per_frame = self.ds[ 'seconds_per_frame' ]
        self.start = self.ds[ 'time_start' ]
        self.stop = self.ds[ 'time_stop' ]
        self.log_debug_level = 50
        self.frame_query = self.fmt_frame_query( self.start,
                                                 self.stop,
                                                 self.ds[ 'channel_list' ],
                                                 self.ds[ 'ifo' ],
                                                 self.ds[ 'frame_type' ] )
        print self.frame_query
        dt = self.stop - self.start
        pre = "H-H1_HOFT_C01"
        src_dir = testing.dir_archive
        src_file = ( "O1/hoft_C01/H1/%s-%d/%s-%d-%d.gwf" % ( pre, int( self.start/ 100000 ), pre, self.start, dt ) )
        dst_dir = self.dir_output_frames
        dst_file = ( "%s-%d-%d.gwf" % ( "H-ticket_hoft_validation_default", self.start, dt ) )
        self.verify_cmd = ( "%s %s/%s %s/%s" % ( testing.prog_framecpp_compare,
                                                 src_dir,
                                                 src_file,
                                                 dst_dir,
                                                 dst_file ) )

    def eval( self ):
        #----------------------------------------------------------------
        # Generate the frame file.
        #----------------------------------------------------------------
        super( ticket_hoft_validation, self ).eval( )
        self._verify( self.verify_cmd )

def main(argv):
    """
    This is the main entry point into the system testing of the standalone diskcache.
    """
    global Test
    retval = 0
    cmd = None
    subcommand = 0

    #--------------------------------------------------------------------
    # Setup for testing
    #--------------------------------------------------------------------
    argv = Test.Init( argv )

    #--------------------------------------------------------------------
    # Run some tests
    #--------------------------------------------------------------------
    try:
        (opts, args) = getopt.getopt( argv, "",
                                      [ 'hoft_validation',
                                        '3130=',
                                        '3131=',
                                        '3132='
                                        ] )
    except getopt.GetoptError as err:
        print str(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '--hoft_validation':
            cmd = ticket_hoft_validation( )
        elif opt == '--3130':
            cmd = ticket_3130( arg )
        elif opt == '--3131':
            cmd = ticket_3131( arg )
        elif opt == '--3132':
            cmd = ticket_3132( arg )
    if ( not cmd == None ):
        cmd.eval( )
    #--------------------------------------------------------------------
    # All done testing, return with the appropriate exit status
    #--------------------------------------------------------------------
    Test.Exit( )
    return( retval )

if __name__ == '__main__':
    exit( main( sys.argv[1:] ) )
