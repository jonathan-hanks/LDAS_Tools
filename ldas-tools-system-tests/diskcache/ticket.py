#! /usr/bin/env python

import io
import getopt
import os
import os.path
import pexpect
import signal
import shutil
import sys
import time
import re

sys.path.insert(1, ".." )

import testing
import dc

class ticket(object):
    def __init__(self, name, classification, Test ):
        """
        Create a new instance of the object

        Keyword arguments:
        Test -- Name of the test to executed (eg: 'space')
        """
        self.Test = Test

        self.Test.Message( 'Creating new instance of %s' % ( name ),
                           Level=10 )
        self.server = None
        self._rsc = None
        self._rds_levels = None
        self._test = name
        self._group = classification
        self._test_name = "ticket_" + self._group + "_" + str( self._test )

        self.pattern_scanned = ' scanned in \d+ ms '
        self.pattern_wrote = ' Wrote ASCII Cache File: .*[.]txt '

        self.Test.Message( self._test_name + " has been initialized",
                           Level=10 )
        
    def __del__(self):
        if self.server:
            self.server.stop( )
            del self.server

    def eval(self,stop_server = True):
        self.Test.Message( 'ENTRY: ticket::eval',
                           Level=5 )
        self.setup( )
        self.server = dc.diskcache.server( self._rsc, self.Test )
        if ( self.server ):
            self.server.start( )
            time.sleep( 10 )
            self.Test.Message( 'ticket::eval: Waiting on first scan and writing of Cache file',
                               Level=30 )
            self.server.waitOn( [ self.pattern_wrote,
                                  self.pattern_scanned ],
                                timeout=300 )
            if ( stop_server ):
                self.server.stop( )
                del self.server
        self.Test.Message( 'EXIT: ticket::eval',
                           Level=5 )


    def seed_data( self, outputdir, frame_type = '' ):
        return dc.seed_data( self.Test,
                             self._times,
                             self._times_stride,
                             self._group,
                             outputdir, frame_type )

    def setup(self):
        self.Test.Message( 'Setting up for test testing',
                           Level=10 )
        self.pseudo_root = os.path.join(os.getcwd(), self._test_name)
        try:
            shutil.rmtree(self.pseudo_root)
        except:
            pass
        if ( not os.path.exists( self.pseudo_root ) ):
            os.makedirs( self.pseudo_root, 0755 )
        dir_log = os.path.join(self.pseudo_root, "logs")
        if ( not os.path.exists( dir_log ) ):
            os.makedirs( dir_log, 0755 )
        frames_dir = os.path.join(self.pseudo_root, "frames")
        if ( self._test == 'space' ):
            self.Test.Message( 'Setting up for ' + self._test + ' test',
                               Level=20 )
        self.rsc_name = os.path.join( self.pseudo_root, 'diskcache.rsc' )
        ofilename_ascii = os.path.join( self.pseudo_root, "cache.txt" )
        ofilename_binary = os.path.join( self.pseudo_root, "cache.dat" )
        #----------------------------------------------------------------
        # Use the resource file to do a single scan
        #----------------------------------------------------------------
        self._rsc = dc.diskcache_rsc( dir_log,
                                      log_debug_level=40,
                                      output_data_dir=frames_dir,
                                      data_function=self.seed_data,
                                      use_dir_archive=False,
                                      stat_timeout=15,
                                      output_ascii=ofilename_ascii,
                                      output_binary=ofilename_binary )
        self._rsc.write( self.rsc_name )
