#! /usr/bin/env python

import io
import getopt
import os
import os.path
import pexpect
import signal
import shutil
import sys
import time
import re

sys.path.insert(1, ".." )

import testing
import dc

Test = testing.Test

sys.path.insert(1, "." )

import ticket
import test_nfs as nfs
import ticket_4739

def ticket_1842( SubTest ):
    #--------------------------------------------------------------------
    # Local Variables
    #--------------------------------------------------------------------
    global Test
    test_name = "ticket_1842_" + str(SubTest)
    Test.Message( "Entering ticket_1842", Level=10 )
    rsc_name = ""
    rsc = None
    server = None
    client = None
    #--------------------------------------------------------------------
    # Setup default values
    #--------------------------------------------------------------------
    pseudo_root = os.path.join(os.getcwd(), test_name)
    if ( not os.path.exists( pseudo_root ) ):
        os.makedirs( pseudo_root, 0755 )
    dir_log = os.path.join(pseudo_root, "logs")
    if ( not os.path.exists( dir_log ) ):
        os.makedirs( dir_log, 0755 )
    ofilename_ascii = os.path.join( pseudo_root, "cache.txt" )
    ofilename_binary = os.path.join( pseudo_root, "cache.dat" )
    #--------------------------------------------------------------------
    # Setup the resouce file to be used by the diskcache command
    #--------------------------------------------------------------------
    if ( SubTest == 1 ):
        #----------------------------------------------------------------
        # This test works on a large number of mount points
        #----------------------------------------------------------------
        rsc_name = os.path.join( pseudo_root, 'diskcache.rsc' )
        rsc = dc.diskcache_rsc( dir_log, size_dir_data=450, concurrency=4,
                                stat_timeout=180,
                                output_ascii=ofilename_ascii,
                                output_binary=ofilename_binary )
    #--------------------------------------------------------------------
    # Use the resource file to do a single scan
    #--------------------------------------------------------------------
    rsc.write( rsc_name )
    server = dc.diskcache.server( rsc, Test )
    server.start( )
    time.sleep( 30 )
    server.waitOnFirstScan( )
    #--------------------------------------------------------------------
    # Wrap everything up
    #--------------------------------------------------------------------
    if ( server ):
        server.stop( )
    pass

def ticket_1865( SubTest ):
    #--------------------------------------------------------------------
    # Local Variables
    #--------------------------------------------------------------------
    global Test
    test_name = "ticket_1865_" + str(SubTest)
    Test.Message( "Entering " + test_name, Level=10 )
    rsc_name = ""
    rsc = None
    server = None
    client = None
    second_run = False
    #--------------------------------------------------------------------
    # Setup default values
    #--------------------------------------------------------------------
    pseudo_root = os.path.join(os.getcwd(), test_name)
    if ( not os.path.exists( pseudo_root ) ):
        os.makedirs( pseudo_root, 0755 )
    dir_log = os.path.join(pseudo_root, "logs")
    frames_log = os.path.join(pseudo_root, "frames")
    if ( not os.path.exists( dir_log ) ):
        os.makedirs( dir_log, 0755 )
    ofilename_ascii = os.path.join( pseudo_root, "cache.txt" )
    ofilename_binary = os.path.join( pseudo_root, "cache.dat" )
    ofilename_binary_current = None
    #--------------------------------------------------------------------
    # Setup the resouce file to be used by the diskcache command
    #--------------------------------------------------------------------
    #--------------------------------------------------------------------
    # This test works on a large number of mount points
    #--------------------------------------------------------------------
    rsc_name = os.path.join( pseudo_root, 'diskcache.rsc' )
    rsc = dc.diskcache_rsc( dir_log,
                            use_dir_archive=False,
                            use_dir_data=False,
                            output_data_dir=frames_log,
                            data_function=ticket_1865_frames,
                            concurrency=4,
                            stat_timeout=180,
                            output_ascii=ofilename_ascii,
                            output_binary=ofilename_binary )
    if ( SubTest == 'no_cache' ):
        #----------------------------------------------------------------
        # Remove any cache files
        #----------------------------------------------------------------
        pass
    elif ( SubTest == 'cache' ):
        #----------------------------------------------------------------
        # Use cache file from previous run
        #----------------------------------------------------------------
        ofilename_binary_current = ofilename_binary
        second_run = True
    elif ( SubTest == 'cache_resync' ):
        #----------------------------------------------------------------
        # Use resync cache file from previous run
        #----------------------------------------------------------------
        ofilename_binary_current = ofilename_binary + '#Resync#'
        second_run = True
    #--------------------------------------------------------------------
    # Use the resource file to do a single scan
    #--------------------------------------------------------------------
    rsc.write( rsc_name )
    server = dc.diskcache.server( rsc, Test )
    if ( server ):
        server.start( )
        time.sleep( 30 )
        server.waitOnFirstScan( )
        server.stop( )
    if ( second_run ):
        #----------------------------------------------------------------
        # Run the diskcache a second time to see how the diskcache
        #   handles seeding.
        #----------------------------------------------------------------
        if ( ofilename_binary_current and os.path.isfile( ofilename_binary_current ) ):
            os.system( "touch " + ofilename_binary_current)
        server = dc.diskcache.server( rsc, Test )
        if ( server ):
            server.start( )
            time.sleep( 30 )
            server.waitOnFirstScan( )
            server.stop( )
    #--------------------------------------------------------------------
    # Check the output log for proper seed
    #--------------------------------------------------------------------
    Test.Check( dc.check_for_seed( os.path.join( dir_log,
                                                 'diskcache_server.log.txt'),
                                   ofilename_binary_current,
                                   Test ),
                test_name )
    Test.Message( "Leaving " + test_name, Level=10 )

def ticket_1865_frames( outputdir ):
    paths = []
    for mp in [ 1117000128, 1117100128, 1117200128 ]:
        mdir = ( "%s/Z-1865-%d" % ( outputdir, mp / 100000 ) )
        paths.append( mdir )
        try:
          os.makedirs( mdir, 0755 )
        except:
            pass
        for inc in range( 0, 100 ):
            fn = ( "%s/Z-1865-%d-256.gwf" % ( mdir, mp ) )
            mp += 256
            try:
                fh = open( fn, 'a' )
                fh.close( )
            except:
                pass
    return paths

class ticket_2160:
    def __init__(self, TestName):
        """
        Create a new instance of the object

        Keyword arguments:
        Test -- Name of the test to executed (eg: 'space')
        """
        global Test

        Test.Message( 'Creating new instance of ticket_2160',
                      Level=10 )
        self._rsc = None
        self._test = TestName
        self._test_name = "ticket_2160_" + str( TestName )
        Test.Message( self._test_name + " has been initialized",
                      Level=10 )
        return
        
    def eval(self):
        self.setup( )
        server = dc.diskcache.server( self._rsc, Test )
        if ( server ):
            server.start( )
            time.sleep( 30 )
            server.waitOnFirstScan( )
            server.stop( )


    def seed_data( self, outputdir ):
        global Test

        Test.Message( 'Creating Data Sets',
                      Level=20 )
        paths = []
        for mp in [ 1117000128, 1117100128, 1117200128,  1117300128, 1117400128, 1117500128]:
            mdir = ( "%s/Z-1865-%d" % ( outputdir, mp / 100000 ) )
            Test.Message( 'seed_data: directory: ' + mdir,
                          Level=20 )
            if ( ( not paths )
                 or ( len( paths ) <= 0 ) ):
                Test.Message( 'Adding space to path: ' + mdir,
                              Level=20 )
                paths.append( mdir + ' ' )
            else:
                paths.append( mdir );
            try:
                os.makedirs( mdir, 0755 )
            except:
                pass
            for inc in range( 0, 100 ):
                fn = ( "%s/Z-1865-%d-256.gwf" % ( mdir, mp ) )
                mp += 256
                try:
                    fh = open( fn, 'a' )
                    fh.close( )
                except:
                    pass
        return paths

    def setup(self):
        global Test

        Test.Message( 'Setting up for test testing',
                      Level=10 )
        pseudo_root = os.path.join(os.getcwd(), self._test_name)
        if ( not os.path.exists( pseudo_root ) ):
            os.makedirs( pseudo_root, 0755 )
        dir_log = os.path.join(pseudo_root, "logs")
        if ( not os.path.exists( dir_log ) ):
            os.makedirs( dir_log, 0755 )
        frames_dir = os.path.join(pseudo_root, "frames")
        if ( self._test == 'space' ):
            Test.Message( 'Setting up for ' + self._test + ' test',
                          Level=20 )
        rsc_name = os.path.join( pseudo_root, 'diskcache.rsc' )
        ofilename_ascii = os.path.join( pseudo_root, "cache.txt" )
        ofilename_binary = os.path.join( pseudo_root, "cache.dat" )
        #----------------------------------------------------------------
        # Use the resource file to do a single scan
        #----------------------------------------------------------------
        self._rsc = dc.diskcache_rsc( dir_log,
                                      output_data_dir=frames_dir,
                                      data_function=self.seed_data,
                                      use_dir_archive=False,
                                      stat_timeout=15,
                                      output_ascii=ofilename_ascii,
                                      output_binary=ofilename_binary )
        self._rsc.write( rsc_name )


#------------------------------------------------------------------------
# Ticket 2222
#
# This test verifies that the standalone diskcache handles the
# removal of directories.
#------------------------------------------------------------------------
class ticket_2222(ticket.ticket):
    def __init__(self, name):
        """
        Create a new instance of the object

        Keyword arguments:
        Test -- Name of the test to executed (eg: 'del_sub')
        """
        global Test

        super(self.__class__,self).__init__(name,'2222', Test = Test )

        self._del_dir = None
        self._rds_levels = [ 'R', 'RDS_L1', 'RDS_L2']
        self._times_stride = 100000
        self._times = range( 1117000128, 1117600128, self._times_stride )
        self._removers = {
            'del_sub' : self._remove_middle,
            'del_leaf' : self._remove_leaf,
            'del_mount_point' : self._remove_mount_point
            }
        self._verifiers = {
            'del_sub' : { 'type':self._rds_levels[0],
                          'start':self._times[0],
                          'stop':self._times[-1]
                          },
            'del_leaf' : { 'type':self._rds_levels[1],
                           'start':self._times[1],
                           'stop':self._times[2]
                           },
            'del_mount_point' : { 'type':self._rds_levels[2],
                                  'start':self._times[0],
                                  'stop':self._times[-1]
                                  }
            }
        return

    def eval(self):
        global Test

        try:
            #------------------------------------------------------------
            # Do the standard initialization and scan through once
            #------------------------------------------------------------
            super(self.__class__,self).eval( stop_server=False )
            if ( self.query_frame_files( ) ):
                #--------------------------------------------------------
                # Remove a directory of interest
                #--------------------------------------------------------
                Test.Message( 'Removing directory',
                              Level=5 )
                deldir = self._removers[self._test]( )
                time.sleep( 5 )
                Test.Message( 'Removing directory: %s' % ( str( deldir ) ),
                              Level=5 )
                #--------------------------------------------------------
                # Verify that the directory is no longer in the cache file
                #--------------------------------------------------------
                self.server.waitOn( [' Removing directory: ',],
                                    timeout=60 )
                self.server.waitOn( [ self.pattern_wrote,
                                      self.pattern_scanned ],
                                    timeout=120 )
                Test.Message( 'Scan completed',
                              Level=5 )
                self.query_frame_files( )
                Test.Message( 'Query completed',
                              Level=5 )
                Test.Check ( ( len( dc.grep( self._rsc.output_ascii, deldir, Test = Test ) ) <= 0 ),
                             "Validing removal from ascii cache file" )
        except Exception as e:
            Test.Check( False,
                        "2222: eval: Exception " + str(e) + " thrown: " + str( sys.exc_info()[0] ))
        if ( self.server ):
            self.server.stop
            del self.server

    def _remove_leaf(self):
        t = self._verifiers['del_leaf']['type']
        start = self._verifiers[self._test]['start']
        uframe_type = '_' + t
        dir = os.path.join( self._del_dir, 'Z',
                            ('Z-%s%s-%d') % (self._group,
                                             uframe_type,
                                             start / self._times_stride) )
        Test.Message( 'Removing leaf directory: ' + dir,
                      Level=10)
        shutil.rmtree(dir)
        return dir

    def _remove_middle(self):
        global Test

        dir = os.path.join( self._del_dir, 'Z' )
        Test.Message( 'Removing middle directory: ' + dir,
                      Level=10)
        shutil.rmtree(dir)
        return dir

    def _remove_mount_point(self):
        global Test

        Test.Message( 'Removing mount point directory: ' + self._del_dir,
                      Level=10)
        shutil.rmtree(self._del_dir)
        return self._del_dir

    def query_frame_files(self):
        global Test

        retval = True
        client = dc.diskcache.client( self._rsc, Test = Test)
        Test.Message( 'Created client to execute query',
                      Level=10 )
        (output, exitstatus) = \
            client.server_query( self._verifiers[self._test]['start'],
                                 self._verifiers[self._test]['stop'],
                                 ftype=self._group + '_' + self._verifiers[self._test]['type'] )
        Test.Message( 'Check: exitstatus: ' + str(exitstatus)
                      + ' output: ' + str( output ),
                      Level=10 )
        return retval

    def seed_data(self,outputdir):
        global Test

        paths = []
        if (self._rds_levels):
            for level in self._rds_levels:
                topdir = os.path.join( outputdir, level )
                paths.append( topdir )
                Test.Message( 'Check: seed_data: topdir: %s ' \
                                  % ( str(topdir) ),
                              Level=10 )
                if ( level == self._verifiers[self._test]['type'] ):
                    self._del_dir = topdir
                super(self.__class__,self).seed_data( os.path.join( topdir, 'Z' ),
                                                     frame_type = '_' + str( level ) )
        else:
            paths = super(self.__class__,self).seed_data(outputdir)
            self._del_dir = outputdir
        return paths

class ticket_signal:
    def __init__(self, TestName):
        """
        Create a new instance of the object

        Keyword arguments:
        Test -- Name of the test to executed (eg: 'space')
        """
        global Test

        Test.Message( 'Creating new instance of ticket_signal',
                      Level=10 )
        self._rsc = None
        self._test = TestName
        self._test_name = "ticket_signal_" + str( TestName )
        Test.Message( self._test_name + " has been initialized",
                      Level=10 )
        return
        
    def eval(self):
        self.setup( )
        server = dc.diskcache.server( self._rsc, Test )
        if ( server ):
            server.start( )
            time.sleep( 30 )
            Test.Message( 'Waiting for first Scan',
                          Level=20 )
            server.waitOnFirstScan( )
            Test.Message( 'Completed First Scan',
                          Level=20 )
            server.rereadConfiguration( )
            Test.Message( 'Requested re-reading of configuration file',
                          Level=20 )
            server.waitOn( 'Calling Daemon::SignalCallback' )
            Test.Message( 'Received confirmation that resource file was reread',
                          Level=20 )
            server.stop( )


    def seed_data( self, outputdir ):
        global Test

        Test.Message( 'Creating Data Sets',
                      Level=20 )
        paths = []
        for mp in [ 1117000128, 1117100128, 1117200128,  1117300128, 1117400128, 1117500128]:
            mdir = ( "%s/Z-1865-%d" % ( outputdir, mp / 100000 ) )
            Test.Message( 'seed_data: directory: ' + mdir,
                          Level=20 )
            if ( ( not paths )
                 or ( len( paths ) <= 0 ) ):
                Test.Message( 'Adding space to path: ' + mdir,
                              Level=20 )
                paths.append( mdir + ' ' )
            else:
                paths.append( mdir );
            try:
                os.makedirs( mdir, 0755 )
            except:
                pass
            for inc in range( 0, 100 ):
                fn = ( "%s/Z-1865-%d-256.gwf" % ( mdir, mp ) )
                mp += 256
                try:
                    fh = open( fn, 'a' )
                    fh.close( )
                except:
                    pass
        return paths

    def setup(self):
        global Test

        Test.Message( 'Setting up for test testing',
                      Level=10 )
        pseudo_root = os.path.join(os.getcwd(), self._test_name)
        if ( not os.path.exists( pseudo_root ) ):
            os.makedirs( pseudo_root, 0755 )
        dir_log = os.path.join(pseudo_root, "logs")
        if ( not os.path.exists( dir_log ) ):
            os.makedirs( dir_log, 0755 )
        frames_dir = os.path.join(pseudo_root, "frames")
        if ( self._test == 'space' ):
            Test.Message( 'Setting up for ' + self._test + ' test',
                          Level=20 )
        rsc_name = os.path.join( pseudo_root, 'diskcache.rsc' )
        ofilename_ascii = os.path.join( pseudo_root, "cache.txt" )
        ofilename_binary = os.path.join( pseudo_root, "cache.dat" )
        #----------------------------------------------------------------
        # Use the resource file to do a single scan
        #----------------------------------------------------------------
        self._rsc = dc.diskcache_rsc( dir_log,
                                      output_data_dir=frames_dir,
                                      data_function=self.seed_data,
                                      use_dir_archive=False,
                                      stat_timeout=15,
                                      output_ascii=ofilename_ascii,
                                      output_binary=ofilename_binary )
        self._rsc.write( rsc_name )

class ticket_variable( object ):
    def __init__(self, TestName):
        """
        Create a new instance of the object

        Keyword arguments:
        Test -- Name of the test to executed (eg: 'space')
        """
        global Test

        Test.Message( 'Creating new instance of ticket_signal',
                      Level=10 )
        self._rsc = None
        self._test = TestName
        self._test_name = "ticket_variable_" + str( TestName )
        Test.Message( self._test_name + " has been initialized",
                      Level=10 )
        return

    def eval(self):
        self.setup( )
        server = dc.diskcache.server( self._rsc )
        if ( server ):
            server.start( )
            time.sleep( 30 )
            self.rc_check_before( server )
            Test.Message( 'Waiting for first Scan',
                          Level=20 )
            server.waitOnFirstScan( )
            Test.Message( 'Completed First Scan',
                          Level=20 )
            #------------------------------------------------------------
            # Local modifications
            #------------------------------------------------------------
            self.rc_set_after( )
            self._rsc.write( self.rsc_name )
            #------------------------------------------------------------
            # Reread the configuration
            #------------------------------------------------------------
            server.waitOn( '{Daemon::ResetOnSignal}' )
            server.rereadConfiguration( )
            Test.Message( 'Requested re-reading of configuration file',
                          Level=20 )
            #------------------------------------------------------------
            # Check to see if the variable has been reset.
            #------------------------------------------------------------
            server.waitOn( 'Calling Daemon::SignalCallback' )
            Test.Message( 'Received confirmation that resource file was reread',
                          Level=20 )
            self.rc_check_after( server )
            server.stop( )


    def seed_data( self, outputdir ):
        global Test

        Test.Message( 'Creating Data Sets',
                      Level=20 )
        paths = []
        for mp in [ 1117000128, 1117100128, 1117200128,  1117300128, 1117400128, 1117500128]:
            mdir = ( "%s/Z-1865-%d" % ( outputdir, mp / 100000 ) )
            Test.Message( 'seed_data: directory: ' + mdir,
                          Level=20 )
            if ( ( not paths )
                 or ( len( paths ) <= 0 ) ):
                Test.Message( 'Adding space to path: ' + mdir,
                              Level=20 )
                paths.append( mdir + ' ' )
            else:
                paths.append( mdir );
            try:
                os.makedirs( mdir, 0755 )
            except:
                pass
            for inc in range( 0, 100 ):
                fn = ( "%s/Z-1865-%d-256.gwf" % ( mdir, mp ) )
                mp += 256
                try:
                    fh = open( fn, 'a' )
                    fh.close( )
                except:
                    pass
        return paths

    def setup(self):
        global Test

        Test.Message( 'Setting up for test testing',
                      Level=10 )
        pseudo_root = os.path.join(os.getcwd(), self._test_name)
        if ( not os.path.exists( pseudo_root ) ):
            os.makedirs( pseudo_root, 0755 )
        dir_log = os.path.join(pseudo_root, "logs")
        if ( not os.path.exists( dir_log ) ):
            os.makedirs( dir_log, 0755 )
        frames_dir = os.path.join(pseudo_root, "frames")
        if ( self._test == 'space' ):
            Test.Message( 'Setting up for ' + self._test + ' test',
                          Level=20 )
        self.rsc_name = os.path.join( pseudo_root, 'diskcache.rsc' )
        ofilename_ascii = os.path.join( pseudo_root, "cache.txt" )
        ofilename_binary = os.path.join( pseudo_root, "cache.dat" )
        #----------------------------------------------------------------
        # Use the resource file to do a single scan
        #----------------------------------------------------------------
        self._rsc = dc.diskcache_rsc( dir_log,
                                      output_data_dir=frames_dir,
                                      data_function=self.seed_data,
                                      use_dir_archive=False,
                                      stat_timeout=15,
                                      output_ascii=ofilename_ascii,
                                      output_binary=ofilename_binary )
        #----------------------------------------------------------------
        # Local modifications
        #----------------------------------------------------------------
        self.rc_set_before( )
        #----------------------------------------------------------------
        # Commit changes
        #----------------------------------------------------------------
        self._rsc.write( self.rsc_name )

    def rc_set_before( self ):
        if ( self._test == 'HOT_DIRECTORY_AGE' ):
            self._rsc.hot_directory_age = 30
        pass

    def rc_set_after( self ):
        if ( self._test == 'HOT_DIRECTORY_AGE' ):
            self._rsc.hot_directory_age = 60
        pass

    def rc_check_before( self, server ):
        if ( self._test == 'HOT_DIRECTORY_AGE' ):
            server.waitOn( 'Changing hotdirectory lower bound from: [0-9][0-9]* to: %d' % ( 30 ) )
        pass

    def rc_check_after( self, server ):
        if ( self._test == 'HOT_DIRECTORY_AGE' ):
            server.waitOn( 'Changing hotdirectory lower bound from: %d to: %d' % ( 30, 60 ) )
        pass

def main(argv):
    """
    This is the main entry point into the system testing of the standalone diskcache.
    """
    global Test
    retval = 0
    cmd = None
    subcommand = 0

    #--------------------------------------------------------------------
    # Setup for testing
    #--------------------------------------------------------------------
    argv = Test.Init( argv )

    #--------------------------------------------------------------------
    # Run some tests
    #--------------------------------------------------------------------
    try:
        (opts, args) = getopt.getopt( argv, "",
                                      [ '1842=',
                                        '1865=',
                                        '2160=',
                                        '2222=',
                                        '4739=',
                                        'nfs=',
                                        'signal=',
                                        'variable='
                                        ] )
    except getopt.GetoptError as err:
        print str(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '--1842':
            cmd = ticket_1842
            subcommand = int(arg)
        elif opt == '--1865':
            cmd = ticket_1865
            subcommand = arg
        elif opt == '--2160':
            c = ticket_2160( arg )
            print str( c )
            c.eval( )
        elif opt == '--2222':
            c = ticket_2222( arg )
            print str( c )
            c.eval( )
        elif opt == '--4739':
            c = ticket_4739.test( arg, Test )
            print str( c )
            c.eval( )
        elif opt == '--signal':
            c = ticket_signal( arg )
            print str( c )
            c.eval( )
        elif opt == '--variable':
            c = ticket_variable( arg )
            print str( c )
            c.eval( )
        elif opt == '--nfs':
            c = nfs.ticket_nfs( arg, Test )
            print str( c )
            c.eval( )
    if ( not cmd == None ):
        cmd( subcommand )
    #--------------------------------------------------------------------
    # All done testing, return with the appropriate exit status
    #--------------------------------------------------------------------
    Test.Exit( )
    return( retval )

if __name__ == '__main__':
    exit( main( sys.argv[1:] ) )
