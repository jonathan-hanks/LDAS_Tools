#! /usr/bin/env python

import getopt
import os
import os.path
import pexpect
import sys

sys.path.insert(1, ".." )

import testing
import packager

Test = testing.UnitTest( )

bin_packages = { "ldas-tools-framecpp": ["framecpp_checksum",
                                         "framecpp_compressor",
                                         "framecpp_dump_channel",
                                         "framecpp_dump_objects",
                                         "framecpp_dump_toc",
                                         "framecpp_fix_metadata",
                                         "framecpp_query",
                                         "framecpp_sample",
                                         "framecpp_verify",
                                         ],
                 "ldas-tools-utilities": ['ldas_create_rds',
                                          ],
                 "ldas-tools-diskcacheAPI": ['diskcache',
                                             'ldas-cache-dump-verify',
                                          ],
                 }

def verify( SubTest ):
    p = packager.system( )
    if ( p == None ):
        print "Did not allocate a packager"
        Test.Check( False, "Failed to find valid packager to test" )
        return
    #--------------------------------------------------------------------
    # Try tests
    #--------------------------------------------------------------------
    pkg = 'ldas-tools-' + SubTest
    p.verify_bin( Test, pkg, bin_packages[ pkg ] )
    pass

def main(argv):
    try:
        (opts, args) = getopt.getopt( argv, "", \
                                      ["test=",] )
    except getopt.GetoptError as err:
        print str(err)
        sys.exit(2)
    for opt, arg in opts:
        if opt == "--test":
            verify( arg )

if __name__ == "__main__":
    main( sys.argv[1:] )
    Test.Exit( )
