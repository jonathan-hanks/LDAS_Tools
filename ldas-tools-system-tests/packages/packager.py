#! /usr/bin/env python

import os
import sys
import subprocess

sys.path.insert(1, ".." )

import testing

#------------------------------------------------------------------------
# Abstract base class
#------------------------------------------------------------------------
class Packager(object):
    def __init__(self):
        pass

    def query_path( self, Test, Pkg, Binary ):
        raise NotImplementedError

    def verify_bin(self,Test,Pkg,Binaries):
        for bin in Binaries:
            path = self.query_path( Test, Pkg, bin )
            #------------------------------------------------------------
            # Check executability
            #------------------------------------------------------------
            stat = False
            if ( len( path ) > 0 ):
                IFNULL = open( os.devnull, 'rb' )
                OFNULL = open( os.devnull, 'wb' )
                try:
                    p1 = subprocess.check_call([path,'--help'],
                                               stdin=IFNULL,
                                               stdout=OFNULL,
                                               stderr=OFNULL)
                    stat = True
                except subprocess.CalledProcessError:
                    stat = False
                    IFNULL.close( )
                    OFNULL.close( )
            Test.Check( stat,
                        'Binary (' + bin + ') executes with no parameters' )
        pass

#------------------------------------------------------------------------
# RPM packaging rules
#------------------------------------------------------------------------
class RPM( Packager ):
    def __init__(self):
        super(RPM,self).__init__( )
        try:
            self.rpm = testing.prog_rpm
            if ( not self.rpm  or len( self.rpm ) <= 0 ):
                raise NotImplementedError
        except:
            raise NotImplementedError

    def query_path( self, Test, Pkg, Binary ):
        #----------------------------------------------------------------
        # Check if package contains binary
        #----------------------------------------------------------------
        p1 = subprocess.Popen([self.rpm,
                               '-ql',
                               Pkg],
                              stdout=subprocess.PIPE)
        p2 = subprocess.Popen(['egrep',
                               '-e',
                               '/' + Binary + '$'],
                              stdin=p1.stdout,
                              stdout=subprocess.PIPE)
        p1.stdout.close( )
        pgm,err = p2.communicate( )
        pgm = pgm.strip( );
        Test.Check( ( err == None ) and ( len( pgm ) > 0 ),
                    'Binary (' + Binary + ') appears in the package' )
        if ( err != None ):
            pgm = ""
        return pgm

#------------------------------------------------------------------------
# DEB packaging rules
#------------------------------------------------------------------------
class DEB( Packager ):
    def __init__(self):
        super(DEB,self).__init__()
        try:
            self.dpkg = testing.prog_dpkg
            if ( not self.dpkg  or len( self.dpkg ) <= 0 ):
                raise NotImplementedError
        except:
            raise NotImplementedError

    def query_path( self, Test, Pkg, Binary ):
        #----------------------------------------------------------------
        # Check if package contains binary
        #----------------------------------------------------------------
        p1 = subprocess.Popen([self.dpkg,
                               '-L',
                               Pkg],
                              stdout=subprocess.PIPE)
        p2 = subprocess.Popen(['egrep',
                               '-e',
                               '/' + Binary + '$'],
                              stdin=p1.stdout,
                              stdout=subprocess.PIPE)
        p1.stdout.close( )
        pgm,err = p2.communicate( )
        pgm = pgm.strip( );
        Test.Check( ( err == None ) and ( len( pgm ) > 0 ),
                    'Binary (' + Binary + ') appears in the package' )
        if ( err != None ):
            pgm = ""
        return pgm

#------------------------------------------------------------------------
# Helper routines
#------------------------------------------------------------------------
def system():
    retval = None
    for s in ['DEB','RPM']:
        try:
            pkgr = globals()[s]
            retval = pkgr( )
            break
        except:
            pass
    return retval

#------------------------------------------------------------------------
# Main
#------------------------------------------------------------------------
if __name__ == "__main__":
    pass
