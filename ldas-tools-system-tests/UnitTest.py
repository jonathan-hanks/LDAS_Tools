#------------------------------------------------------------------------
# Class for standardizing tests
#------------------------------------------------------------------------
import os
import sys

class UnitTest:
    def __init__(self):
        self.verbose = 0
        self.status = True
        self.LEADER_MESG = '-- MESG: '
        self.LEADER_PASS = '-- PASS: '
        self.LEADER_FAIL = '-- FAIL: '
        try:
            val = os.environ['TEST_VERBOSE_MODE']
            self.verbose = int( val )
        except: 
            pass

    def Check( self, PassFail, Argv ):
        """
        Record state of test

        Args:
            PassFail: Boolean representing the state of the test.

        Returns:
        Raises:
        """
        if ( not PassFail ):
            self.status = PassFail
        if ( self.IsVerbose(0) ):
            l = self.LEADER_PASS
            if ( not PassFail ):
                l = self.LEADER_FAIL
            self.Message( Argv,
                          Leader = l )

    def Init(self, argv):
        """
        Intialize the object with command line arguments

        Args:
            argv: Boolean representing the state of the test.
        """
        retval = argv
        return retval

    def IsVerbose( self, Level ):
        """
        Check if the verbosity level is at a level resulting in
        the outputing of messages.

        Args:
            Level: Level at which the message will be outputted.
        """
        if ( self.verbose >= Level ):
            return True
        return False

    def Message( self, fmt, Level=0, Leader='-- MESG: ', end=True ):
        """
        Display a message

        Args:
            fmt: String to be displayed
            Level: Level at which to display the message
            Leader: Print the leader text
            end: Print end of line
        """
        if ( self.IsVerbose( Level ) ):
            if ( Leader ):
                self._output( Leader )
            try:
                self._output( fmt )
                if ( end ):
                    self._output( os.linesep )
            except:
                pass
            self._flush( )

    def Exit(self):
        """
        Exit application with the appropriate return code base on
        the results of Check() calls.
        """
        e = 0
        if ( self.status ):
            e = 0
        else:
            e = 1
        exit( e )

    def _output( self, msg ):
        sys.stdout.write( msg )

    def _flush( self ):
        sys.stdout.flush( )
