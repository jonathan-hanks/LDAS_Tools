#------------------------------------------------------------------------
# These test are aimed at validating the diskcache
#------------------------------------------------------------------------
if ( NOT APPLE )
  add_test(_gstlal_1  ${CMAKE_CURRENT_SOURCE_DIR}/test_gstlal.py --loading 1)
  add_test(_gstlal_compile_  ${CMAKE_CURRENT_SOURCE_DIR}/test_gstlal.py --compile 1)
  # set_tests_properties(diskcache_1842_1  PROPERTIES TIMEOUT 36000)
endif ( NOT APPLE )

